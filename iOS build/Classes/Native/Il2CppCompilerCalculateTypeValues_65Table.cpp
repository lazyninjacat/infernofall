﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// NG.ScreenshotHelper
struct ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7;
// NG.ScreenshotHelper/<>c__DisplayClass40_0
struct U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385;
// NG.ScreenshotHelper/<>c__DisplayClass40_1
struct U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632;
// NG.ScreenshotHelper/DefaultsSetDelegate
struct DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E;
// NG.ScreenshotHelper/PathChangeDelegate
struct PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734;
// NG.ScreenshotHelper/ScreenChange
struct ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207;
// NG.ShotSize
struct ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83;
// ProgressBar
struct ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C;
// ProgressBarCircle
struct ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946;
// SimpleJSON.JSONArray
struct JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8;
// SimpleJSON.JSONNode
struct JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889;
// SimpleJSON.JSONObject
struct JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.String>
struct Action_1_t19CAF500829927B30EC94F39939F749E4919669E;
// System.Action`1<UnityEngine.Color[]>
struct Action_1_tCBDC1ED258556CD5BF0B5976FEA2DEE45A6EDC82;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>
struct Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172;
// System.Collections.Generic.Dictionary`2<System.String,System.Action>
struct Dictionary_2_t6869420B5ACFF0569A65586D45920B0F608DDB45;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<UniWebViewNativeResultPayload>>
struct Dictionary_2_tF7EF1EBF77E39D94C275304B89431CE3F887E216;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener>
struct Dictionary_2_tC8DAC4C5C0B35DE2FF589D35287ED2F7FBBEBD69;
// System.Collections.Generic.IEnumerable`1<YoutubeLight.VideoInfo>
struct IEnumerable_1_tF04983E8049CB72D0DA9372D16238E4C715D0A72;
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>
struct IEnumerator_1_t11861DC26A08B03D2890CA208605F3528D6E7FD9;
// System.Collections.Generic.List`1<NG.ShotSize>
struct List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56;
// System.Collections.Generic.List`1<SimpleJSON.JSONNode>
struct List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9;
// System.Collections.Generic.List`1<SubtitleItem>
struct List_1_tFE21EC83370D816A3AADFCB922A2CDAC1A5A5CA0;
// System.Collections.Generic.List`1<UnityEngine.Color[]>
struct List_1_t7510D1F773CF2FBB893ADF29BC6FC2E1D005A4F5;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Func`2<NG.ShotSize,System.Int32>
struct Func_2_t1A6DF2F8FC1CBC71ADDA0BF07FD79F36363F8D87;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Func`2<UnityEngine.Camera,System.Single>
struct Func_2_t57CA52C784BBE2FCDC9E750E77333DC45EA93916;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.StreamReader
struct StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.Mutex
struct Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// TextScroller
struct TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC;
// UniWebView
struct UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB;
// UniWebView/KeyCodeReceivedDelegate
struct KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C;
// UniWebView/MessageReceivedDelegate
struct MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582;
// UniWebView/OnWebContentProcessTerminatedDelegate
struct OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD;
// UniWebView/OrientationChangedDelegate
struct OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482;
// UniWebView/PageErrorReceivedDelegate
struct PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E;
// UniWebView/PageFinishedDelegate
struct PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702;
// UniWebView/PageStartedDelegate
struct PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78;
// UniWebView/ShouldCloseDelegate
struct ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998;
// UniWebViewNativeListener
struct UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// VW_Gameloop
struct VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15;
// VW_Start
struct VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69;
// YoutubeLight.VideoInfo
struct VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791;
// YoutubePlayer
struct YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9;
// YoutubeSubtitlesReader
struct YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA;
// YoutubeThumbnailData
struct YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA;
// YoutubeTumbnails
struct YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GAMEVIEWUTILS_T769B2DB6B20F2E1F61F5A8B58647DF8897124C84_H
#define GAMEVIEWUTILS_T769B2DB6B20F2E1F61F5A8B58647DF8897124C84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.GameViewUtils
struct  GameViewUtils_t769B2DB6B20F2E1F61F5A8B58647DF8897124C84  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEVIEWUTILS_T769B2DB6B20F2E1F61F5A8B58647DF8897124C84_H
#ifndef SSHDEBUG_TB10438BA824092372CD426813EB17B42F5FADAA7_H
#define SSHDEBUG_TB10438BA824092372CD426813EB17B42F5FADAA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.SSHDebug
struct  SSHDebug_tB10438BA824092372CD426813EB17B42F5FADAA7  : public RuntimeObject
{
public:

public:
};

struct SSHDebug_tB10438BA824092372CD426813EB17B42F5FADAA7_StaticFields
{
public:
	// System.String NG.SSHDebug::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(SSHDebug_tB10438BA824092372CD426813EB17B42F5FADAA7_StaticFields, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSHDEBUG_TB10438BA824092372CD426813EB17B42F5FADAA7_H
#ifndef SSHUTIL_T04817B6981190908F228203374494E42CDEA4C1B_H
#define SSHUTIL_T04817B6981190908F228203374494E42CDEA4C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.SSHUtil
struct  SSHUtil_t04817B6981190908F228203374494E42CDEA4C1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSHUTIL_T04817B6981190908F228203374494E42CDEA4C1B_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T591276796A0551304D8C26A0CE7BA15204639878_H
#define U3CU3EC__DISPLAYCLASS2_0_T591276796A0551304D8C26A0CE7BA15204639878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.SSHUtil_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t591276796A0551304D8C26A0CE7BA15204639878  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Color[]> NG.SSHUtil_<>c__DisplayClass2_0::textureColors
	List_1_t7510D1F773CF2FBB893ADF29BC6FC2E1D005A4F5 * ___textureColors_0;
	// System.Action`1<UnityEngine.Color[]> NG.SSHUtil_<>c__DisplayClass2_0::onComplete
	Action_1_tCBDC1ED258556CD5BF0B5976FEA2DEE45A6EDC82 * ___onComplete_1;

public:
	inline static int32_t get_offset_of_textureColors_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t591276796A0551304D8C26A0CE7BA15204639878, ___textureColors_0)); }
	inline List_1_t7510D1F773CF2FBB893ADF29BC6FC2E1D005A4F5 * get_textureColors_0() const { return ___textureColors_0; }
	inline List_1_t7510D1F773CF2FBB893ADF29BC6FC2E1D005A4F5 ** get_address_of_textureColors_0() { return &___textureColors_0; }
	inline void set_textureColors_0(List_1_t7510D1F773CF2FBB893ADF29BC6FC2E1D005A4F5 * value)
	{
		___textureColors_0 = value;
		Il2CppCodeGenWriteBarrier((&___textureColors_0), value);
	}

	inline static int32_t get_offset_of_onComplete_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t591276796A0551304D8C26A0CE7BA15204639878, ___onComplete_1)); }
	inline Action_1_tCBDC1ED258556CD5BF0B5976FEA2DEE45A6EDC82 * get_onComplete_1() const { return ___onComplete_1; }
	inline Action_1_tCBDC1ED258556CD5BF0B5976FEA2DEE45A6EDC82 ** get_address_of_onComplete_1() { return &___onComplete_1; }
	inline void set_onComplete_1(Action_1_tCBDC1ED258556CD5BF0B5976FEA2DEE45A6EDC82 * value)
	{
		___onComplete_1 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T591276796A0551304D8C26A0CE7BA15204639878_H
#ifndef U3CU3EC_T02381F501EC2C230EA56CE8D26DF847229FDDE12_H
#define U3CU3EC_T02381F501EC2C230EA56CE8D26DF847229FDDE12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_<>c
struct  U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields
{
public:
	// NG.ScreenshotHelper_<>c NG.ScreenshotHelper_<>c::<>9
	U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12 * ___U3CU3E9_0;
	// System.Action NG.ScreenshotHelper_<>c::<>9__25_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__25_0_1;
	// System.Func`2<UnityEngine.Camera,System.Single> NG.ScreenshotHelper_<>c::<>9__40_0
	Func_2_t57CA52C784BBE2FCDC9E750E77333DC45EA93916 * ___U3CU3E9__40_0_2;
	// System.Func`2<NG.ShotSize,System.Int32> NG.ScreenshotHelper_<>c::<>9__47_0
	Func_2_t1A6DF2F8FC1CBC71ADDA0BF07FD79F36363F8D87 * ___U3CU3E9__47_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields, ___U3CU3E9__25_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__25_0_1() const { return ___U3CU3E9__25_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__25_0_1() { return &___U3CU3E9__25_0_1; }
	inline void set_U3CU3E9__25_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__25_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields, ___U3CU3E9__40_0_2)); }
	inline Func_2_t57CA52C784BBE2FCDC9E750E77333DC45EA93916 * get_U3CU3E9__40_0_2() const { return ___U3CU3E9__40_0_2; }
	inline Func_2_t57CA52C784BBE2FCDC9E750E77333DC45EA93916 ** get_address_of_U3CU3E9__40_0_2() { return &___U3CU3E9__40_0_2; }
	inline void set_U3CU3E9__40_0_2(Func_2_t57CA52C784BBE2FCDC9E750E77333DC45EA93916 * value)
	{
		___U3CU3E9__40_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__47_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields, ___U3CU3E9__47_0_3)); }
	inline Func_2_t1A6DF2F8FC1CBC71ADDA0BF07FD79F36363F8D87 * get_U3CU3E9__47_0_3() const { return ___U3CU3E9__47_0_3; }
	inline Func_2_t1A6DF2F8FC1CBC71ADDA0BF07FD79F36363F8D87 ** get_address_of_U3CU3E9__47_0_3() { return &___U3CU3E9__47_0_3; }
	inline void set_U3CU3E9__47_0_3(Func_2_t1A6DF2F8FC1CBC71ADDA0BF07FD79F36363F8D87 * value)
	{
		___U3CU3E9__47_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__47_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T02381F501EC2C230EA56CE8D26DF847229FDDE12_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_TDBE0CB871FC680A21AE7514B3AF11393E06A7385_H
#define U3CU3EC__DISPLAYCLASS40_0_TDBE0CB871FC680A21AE7514B3AF11393E06A7385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385  : public RuntimeObject
{
public:
	// NG.ScreenshotHelper NG.ScreenshotHelper_<>c__DisplayClass40_0::<>4__this
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * ___U3CU3E4__this_0;
	// System.Int32 NG.ScreenshotHelper_<>c__DisplayClass40_0::currentIndex
	int32_t ___currentIndex_1;
	// System.Single NG.ScreenshotHelper_<>c__DisplayClass40_0::timeScaleStart
	float ___timeScaleStart_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385, ___U3CU3E4__this_0)); }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}

	inline static int32_t get_offset_of_timeScaleStart_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385, ___timeScaleStart_2)); }
	inline float get_timeScaleStart_2() const { return ___timeScaleStart_2; }
	inline float* get_address_of_timeScaleStart_2() { return &___timeScaleStart_2; }
	inline void set_timeScaleStart_2(float value)
	{
		___timeScaleStart_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_TDBE0CB871FC680A21AE7514B3AF11393E06A7385_H
#ifndef U3CU3EC__DISPLAYCLASS40_1_TA4E812E235C20F54100FA4C0B858669AE585D632_H
#define U3CU3EC__DISPLAYCLASS40_1_TA4E812E235C20F54100FA4C0B858669AE585D632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_<>c__DisplayClass40_1
struct  U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632  : public RuntimeObject
{
public:
	// NG.ShotSize NG.ScreenshotHelper_<>c__DisplayClass40_1::sshot
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * ___sshot_0;
	// System.String NG.ScreenshotHelper_<>c__DisplayClass40_1::shotFileName
	String_t* ___shotFileName_1;
	// NG.ScreenshotHelper_<>c__DisplayClass40_0 NG.ScreenshotHelper_<>c__DisplayClass40_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 * ___CSU24U3CU3E8__locals1_2;

public:
	inline static int32_t get_offset_of_sshot_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632, ___sshot_0)); }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * get_sshot_0() const { return ___sshot_0; }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 ** get_address_of_sshot_0() { return &___sshot_0; }
	inline void set_sshot_0(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * value)
	{
		___sshot_0 = value;
		Il2CppCodeGenWriteBarrier((&___sshot_0), value);
	}

	inline static int32_t get_offset_of_shotFileName_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632, ___shotFileName_1)); }
	inline String_t* get_shotFileName_1() const { return ___shotFileName_1; }
	inline String_t** get_address_of_shotFileName_1() { return &___shotFileName_1; }
	inline void set_shotFileName_1(String_t* value)
	{
		___shotFileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___shotFileName_1), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632, ___CSU24U3CU3E8__locals1_2)); }
	inline U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 * get_CSU24U3CU3E8__locals1_2() const { return ___CSU24U3CU3E8__locals1_2; }
	inline U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 ** get_address_of_CSU24U3CU3E8__locals1_2() { return &___CSU24U3CU3E8__locals1_2; }
	inline void set_CSU24U3CU3E8__locals1_2(U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 * value)
	{
		___CSU24U3CU3E8__locals1_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_1_TA4E812E235C20F54100FA4C0B858669AE585D632_H
#ifndef U3CU3EC__DISPLAYCLASS40_2_TBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF_H
#define U3CU3EC__DISPLAYCLASS40_2_TBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_<>c__DisplayClass40_2
struct  U3CU3Ec__DisplayClass40_2_tBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF  : public RuntimeObject
{
public:
	// UnityEngine.Color[] NG.ScreenshotHelper_<>c__DisplayClass40_2::result
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___result_0;
	// NG.ScreenshotHelper_<>c__DisplayClass40_1 NG.ScreenshotHelper_<>c__DisplayClass40_2::CSU24<>8__locals2
	U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632 * ___CSU24U3CU3E8__locals2_1;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_2_tBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF, ___result_0)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_result_0() const { return ___result_0; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_2_tBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632 * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632 ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632 * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_2_TBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF_H
#ifndef U3CFINISHROUTINEU3ED__42_TC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7_H
#define U3CFINISHROUTINEU3ED__42_TC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_<FinishRoutine>d__42
struct  U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7  : public RuntimeObject
{
public:
	// System.Int32 NG.ScreenshotHelper_<FinishRoutine>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NG.ScreenshotHelper_<FinishRoutine>d__42::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NG.ScreenshotHelper NG.ScreenshotHelper_<FinishRoutine>d__42::<>4__this
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * ___U3CU3E4__this_2;
	// System.Single NG.ScreenshotHelper_<FinishRoutine>d__42::timeScaleStart
	float ___timeScaleStart_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7, ___U3CU3E4__this_2)); }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_timeScaleStart_3() { return static_cast<int32_t>(offsetof(U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7, ___timeScaleStart_3)); }
	inline float get_timeScaleStart_3() const { return ___timeScaleStart_3; }
	inline float* get_address_of_timeScaleStart_3() { return &___timeScaleStart_3; }
	inline void set_timeScaleStart_3(float value)
	{
		___timeScaleStart_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINISHROUTINEU3ED__42_TC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7_H
#ifndef SHOTSIZE_T930D8996A44F429063B527BCE3DDEC5A57987A83_H
#define SHOTSIZE_T930D8996A44F429063B527BCE3DDEC5A57987A83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ShotSize
struct  ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83  : public RuntimeObject
{
public:
	// System.Int32 NG.ShotSize::width
	int32_t ___width_0;
	// System.Int32 NG.ShotSize::height
	int32_t ___height_1;
	// System.String NG.ShotSize::label
	String_t* ___label_2;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_label_2() { return static_cast<int32_t>(offsetof(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83, ___label_2)); }
	inline String_t* get_label_2() const { return ___label_2; }
	inline String_t** get_address_of_label_2() { return &___label_2; }
	inline void set_label_2(String_t* value)
	{
		___label_2 = value;
		Il2CppCodeGenWriteBarrier((&___label_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOTSIZE_T930D8996A44F429063B527BCE3DDEC5A57987A83_H
#ifndef SHOTSIZECOMPARER_T0F04CB046FFB41E86535F78C93355C89DA28DAE1_H
#define SHOTSIZECOMPARER_T0F04CB046FFB41E86535F78C93355C89DA28DAE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ShotSizeComparer
struct  ShotSizeComparer_t0F04CB046FFB41E86535F78C93355C89DA28DAE1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOTSIZECOMPARER_T0F04CB046FFB41E86535F78C93355C89DA28DAE1_H
#ifndef TEXTURESCALE_TF0F8ACB42A600B7A8E82714F3A57CA21361282B0_H
#define TEXTURESCALE_TF0F8ACB42A600B7A8E82714F3A57CA21361282B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.TextureScale
struct  TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0  : public RuntimeObject
{
public:

public:
};

struct TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields
{
public:
	// UnityEngine.Color[] NG.TextureScale::texColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___texColors_0;
	// UnityEngine.Color[] NG.TextureScale::newColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___newColors_1;
	// System.Int32 NG.TextureScale::w
	int32_t ___w_2;
	// System.Single NG.TextureScale::ratioX
	float ___ratioX_3;
	// System.Single NG.TextureScale::ratioY
	float ___ratioY_4;
	// System.Int32 NG.TextureScale::w2
	int32_t ___w2_5;
	// System.Int32 NG.TextureScale::finishCount
	int32_t ___finishCount_6;
	// System.Threading.Mutex NG.TextureScale::mutex
	Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * ___mutex_7;

public:
	inline static int32_t get_offset_of_texColors_0() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___texColors_0)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_texColors_0() const { return ___texColors_0; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_texColors_0() { return &___texColors_0; }
	inline void set_texColors_0(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___texColors_0 = value;
		Il2CppCodeGenWriteBarrier((&___texColors_0), value);
	}

	inline static int32_t get_offset_of_newColors_1() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___newColors_1)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_newColors_1() const { return ___newColors_1; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_newColors_1() { return &___newColors_1; }
	inline void set_newColors_1(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___newColors_1 = value;
		Il2CppCodeGenWriteBarrier((&___newColors_1), value);
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___w_2)); }
	inline int32_t get_w_2() const { return ___w_2; }
	inline int32_t* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(int32_t value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_ratioX_3() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___ratioX_3)); }
	inline float get_ratioX_3() const { return ___ratioX_3; }
	inline float* get_address_of_ratioX_3() { return &___ratioX_3; }
	inline void set_ratioX_3(float value)
	{
		___ratioX_3 = value;
	}

	inline static int32_t get_offset_of_ratioY_4() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___ratioY_4)); }
	inline float get_ratioY_4() const { return ___ratioY_4; }
	inline float* get_address_of_ratioY_4() { return &___ratioY_4; }
	inline void set_ratioY_4(float value)
	{
		___ratioY_4 = value;
	}

	inline static int32_t get_offset_of_w2_5() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___w2_5)); }
	inline int32_t get_w2_5() const { return ___w2_5; }
	inline int32_t* get_address_of_w2_5() { return &___w2_5; }
	inline void set_w2_5(int32_t value)
	{
		___w2_5 = value;
	}

	inline static int32_t get_offset_of_finishCount_6() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___finishCount_6)); }
	inline int32_t get_finishCount_6() const { return ___finishCount_6; }
	inline int32_t* get_address_of_finishCount_6() { return &___finishCount_6; }
	inline void set_finishCount_6(int32_t value)
	{
		___finishCount_6 = value;
	}

	inline static int32_t get_offset_of_mutex_7() { return static_cast<int32_t>(offsetof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields, ___mutex_7)); }
	inline Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * get_mutex_7() const { return ___mutex_7; }
	inline Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C ** get_address_of_mutex_7() { return &___mutex_7; }
	inline void set_mutex_7(Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * value)
	{
		___mutex_7 = value;
		Il2CppCodeGenWriteBarrier((&___mutex_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURESCALE_TF0F8ACB42A600B7A8E82714F3A57CA21361282B0_H
#ifndef THREADDATA_TA2D7E4102ADCA935E96280CD5B2F46496D064915_H
#define THREADDATA_TA2D7E4102ADCA935E96280CD5B2F46496D064915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.TextureScale_ThreadData
struct  ThreadData_tA2D7E4102ADCA935E96280CD5B2F46496D064915  : public RuntimeObject
{
public:
	// System.Int32 NG.TextureScale_ThreadData::start
	int32_t ___start_0;
	// System.Int32 NG.TextureScale_ThreadData::end
	int32_t ___end_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(ThreadData_tA2D7E4102ADCA935E96280CD5B2F46496D064915, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(ThreadData_tA2D7E4102ADCA935E96280CD5B2F46496D064915, ___end_1)); }
	inline int32_t get_end_1() const { return ___end_1; }
	inline int32_t* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(int32_t value)
	{
		___end_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADDATA_TA2D7E4102ADCA935E96280CD5B2F46496D064915_H
#ifndef JSON_T23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05_H
#define JSON_T23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSON
struct  JSON_t23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05_H
#ifndef JSONNODE_T0AA84017F5559ABABE7CB27656B1352F565B5889_H
#define JSONNODE_T0AA84017F5559ABABE7CB27656B1352F565B5889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode
struct  JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889  : public RuntimeObject
{
public:

public:
};

struct JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889_StaticFields
{
public:
	// System.Text.StringBuilder SimpleJSON.JSONNode::m_EscapeBuilder
	StringBuilder_t * ___m_EscapeBuilder_0;

public:
	inline static int32_t get_offset_of_m_EscapeBuilder_0() { return static_cast<int32_t>(offsetof(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889_StaticFields, ___m_EscapeBuilder_0)); }
	inline StringBuilder_t * get_m_EscapeBuilder_0() const { return ___m_EscapeBuilder_0; }
	inline StringBuilder_t ** get_address_of_m_EscapeBuilder_0() { return &___m_EscapeBuilder_0; }
	inline void set_m_EscapeBuilder_0(StringBuilder_t * value)
	{
		___m_EscapeBuilder_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_EscapeBuilder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNODE_T0AA84017F5559ABABE7CB27656B1352F565B5889_H
#ifndef U3CGET_CHILDRENU3ED__29_T6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF_H
#define U3CGET_CHILDRENU3ED__29_T6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode_<get_Children>d__29
struct  U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF  : public RuntimeObject
{
public:
	// System.Int32 SimpleJSON.JSONNode_<get_Children>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_Children>d__29::<>2__current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CU3E2__current_1;
	// System.Int32 SimpleJSON.JSONNode_<get_Children>d__29::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF, ___U3CU3E2__current_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_CHILDRENU3ED__29_T6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF_H
#ifndef U3CGET_DEEPCHILDRENU3ED__31_TF3B1807A1B4E53363B7B2360F72919A5B9571A94_H
#define U3CGET_DEEPCHILDRENU3ED__31_TF3B1807A1B4E53363B7B2360F72919A5B9571A94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode_<get_DeepChildren>d__31
struct  U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94  : public RuntimeObject
{
public:
	// System.Int32 SimpleJSON.JSONNode_<get_DeepChildren>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_DeepChildren>d__31::<>2__current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CU3E2__current_1;
	// System.Int32 SimpleJSON.JSONNode_<get_DeepChildren>d__31::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_DeepChildren>d__31::<>4__this
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_DeepChildren>d__31::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_DeepChildren>d__31::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94, ___U3CU3E2__current_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94, ___U3CU3E4__this_3)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_5() { return static_cast<int32_t>(offsetof(U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94, ___U3CU3E7__wrap2_5)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_5() const { return ___U3CU3E7__wrap2_5; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_5() { return &___U3CU3E7__wrap2_5; }
	inline void set_U3CU3E7__wrap2_5(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_DEEPCHILDRENU3ED__31_TF3B1807A1B4E53363B7B2360F72919A5B9571A94_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T0680462BA64C98F406466D423F160FF5561B71D2_H
#define U3CU3EC__DISPLAYCLASS17_0_T0680462BA64C98F406466D423F160FF5561B71D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t0680462BA64C98F406466D423F160FF5561B71D2  : public RuntimeObject
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONObject_<>c__DisplayClass17_0::aNode
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___aNode_0;

public:
	inline static int32_t get_offset_of_aNode_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t0680462BA64C98F406466D423F160FF5561B71D2, ___aNode_0)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_aNode_0() const { return ___aNode_0; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_aNode_0() { return &___aNode_0; }
	inline void set_aNode_0(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___aNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___aNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T0680462BA64C98F406466D423F160FF5561B71D2_H
#ifndef SUBTITLEITEM_T8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA_H
#define SUBTITLEITEM_T8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubtitleItem
struct  SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA  : public RuntimeObject
{
public:
	// System.Double SubtitleItem::<StartTime>k__BackingField
	double ___U3CStartTimeU3Ek__BackingField_0;
	// System.Double SubtitleItem::<EndTime>k__BackingField
	double ___U3CEndTimeU3Ek__BackingField_1;
	// System.String SubtitleItem::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CStartTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA, ___U3CStartTimeU3Ek__BackingField_0)); }
	inline double get_U3CStartTimeU3Ek__BackingField_0() const { return ___U3CStartTimeU3Ek__BackingField_0; }
	inline double* get_address_of_U3CStartTimeU3Ek__BackingField_0() { return &___U3CStartTimeU3Ek__BackingField_0; }
	inline void set_U3CStartTimeU3Ek__BackingField_0(double value)
	{
		___U3CStartTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEndTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA, ___U3CEndTimeU3Ek__BackingField_1)); }
	inline double get_U3CEndTimeU3Ek__BackingField_1() const { return ___U3CEndTimeU3Ek__BackingField_1; }
	inline double* get_address_of_U3CEndTimeU3Ek__BackingField_1() { return &___U3CEndTimeU3Ek__BackingField_1; }
	inline void set_U3CEndTimeU3Ek__BackingField_1(double value)
	{
		___U3CEndTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA, ___U3CtextU3Ek__BackingField_2)); }
	inline String_t* get_U3CtextU3Ek__BackingField_2() const { return ___U3CtextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_2() { return &___U3CtextU3Ek__BackingField_2; }
	inline void set_U3CtextU3Ek__BackingField_2(String_t* value)
	{
		___U3CtextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBTITLEITEM_T8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC__DISPLAYCLASS116_0_T5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8_H
#define U3CU3EC__DISPLAYCLASS116_0_T5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_<>c__DisplayClass116_0
struct  U3CU3Ec__DisplayClass116_0_t5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8  : public RuntimeObject
{
public:
	// System.Action`1<System.String> UniWebView_<>c__DisplayClass116_0::handler
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___handler_0;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass116_0_t5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8, ___handler_0)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_handler_0() const { return ___handler_0; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS116_0_T5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8_H
#ifndef UNIWEBVIEWHELPER_T8768407D2ECCF39080CB4D426A7AD65CC6F7A2D3_H
#define UNIWEBVIEWHELPER_T8768407D2ECCF39080CB4D426A7AD65CC6F7A2D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewHelper
struct  UniWebViewHelper_t8768407D2ECCF39080CB4D426A7AD65CC6F7A2D3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWHELPER_T8768407D2ECCF39080CB4D426A7AD65CC6F7A2D3_H
#ifndef UNIWEBVIEWINTERFACE_TE89E48E7F735126B06DE035E92DEFEFCA294E2CF_H
#define UNIWEBVIEWINTERFACE_TE89E48E7F735126B06DE035E92DEFEFCA294E2CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewInterface
struct  UniWebViewInterface_tE89E48E7F735126B06DE035E92DEFEFCA294E2CF  : public RuntimeObject
{
public:

public:
};

struct UniWebViewInterface_tE89E48E7F735126B06DE035E92DEFEFCA294E2CF_StaticFields
{
public:
	// System.Boolean UniWebViewInterface::correctPlatform
	bool ___correctPlatform_1;

public:
	inline static int32_t get_offset_of_correctPlatform_1() { return static_cast<int32_t>(offsetof(UniWebViewInterface_tE89E48E7F735126B06DE035E92DEFEFCA294E2CF_StaticFields, ___correctPlatform_1)); }
	inline bool get_correctPlatform_1() const { return ___correctPlatform_1; }
	inline bool* get_address_of_correctPlatform_1() { return &___correctPlatform_1; }
	inline void set_correctPlatform_1(bool value)
	{
		___correctPlatform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWINTERFACE_TE89E48E7F735126B06DE035E92DEFEFCA294E2CF_H
#ifndef UNIWEBVIEWNATIVERESULTPAYLOAD_T3A9397FEF18D4CB032484A81370D6BC52767730C_H
#define UNIWEBVIEWNATIVERESULTPAYLOAD_T3A9397FEF18D4CB032484A81370D6BC52767730C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeResultPayload
struct  UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C  : public RuntimeObject
{
public:
	// System.String UniWebViewNativeResultPayload::identifier
	String_t* ___identifier_0;
	// System.String UniWebViewNativeResultPayload::resultCode
	String_t* ___resultCode_1;
	// System.String UniWebViewNativeResultPayload::data
	String_t* ___data_2;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_resultCode_1() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C, ___resultCode_1)); }
	inline String_t* get_resultCode_1() const { return ___resultCode_1; }
	inline String_t** get_address_of_resultCode_1() { return &___resultCode_1; }
	inline void set_resultCode_1(String_t* value)
	{
		___resultCode_1 = value;
		Il2CppCodeGenWriteBarrier((&___resultCode_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C, ___data_2)); }
	inline String_t* get_data_2() const { return ___data_2; }
	inline String_t** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(String_t* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWNATIVERESULTPAYLOAD_T3A9397FEF18D4CB032484A81370D6BC52767730C_H
#ifndef U3CBEGINDESCRIPTIONTEXTSCROLLU3ED__88_T1ECBB76AABC2A65C8111948D4FC049DFA9B6177C_H
#define U3CBEGINDESCRIPTIONTEXTSCROLLU3ED__88_T1ECBB76AABC2A65C8111948D4FC049DFA9B6177C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<BeginDescriptionTextScroll>d__88
struct  U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<BeginDescriptionTextScroll>d__88::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<BeginDescriptionTextScroll>d__88::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<BeginDescriptionTextScroll>d__88::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINDESCRIPTIONTEXTSCROLLU3ED__88_T1ECBB76AABC2A65C8111948D4FC049DFA9B6177C_H
#ifndef U3CCLOSESOURCETEXTHELPERU3ED__93_T28244E3AB8E655AA874000D4E443804D3D088E57_H
#define U3CCLOSESOURCETEXTHELPERU3ED__93_T28244E3AB8E655AA874000D4E443804D3D088E57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<CloseSourceTextHelper>d__93
struct  U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<CloseSourceTextHelper>d__93::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<CloseSourceTextHelper>d__93::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<CloseSourceTextHelper>d__93::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSESOURCETEXTHELPERU3ED__93_T28244E3AB8E655AA874000D4E443804D3D088E57_H
#ifndef U3CGRADUALVOLUMEINCREASEU3ED__81_T7B70011EAEFD873F51A67EE4348908DFADE14A25_H
#define U3CGRADUALVOLUMEINCREASEU3ED__81_T7B70011EAEFD873F51A67EE4348908DFADE14A25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<GradualVolumeIncrease>d__81
struct  U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<GradualVolumeIncrease>d__81::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<GradualVolumeIncrease>d__81::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<GradualVolumeIncrease>d__81::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;
	// System.Int32 VW_Gameloop_<GradualVolumeIncrease>d__81::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGRADUALVOLUMEINCREASEU3ED__81_T7B70011EAEFD873F51A67EE4348908DFADE14A25_H
#ifndef U3CROOMTRANSITIONU3ED__110_T60433AB23B0DF65E560B903F37CADD712540FDD6_H
#define U3CROOMTRANSITIONU3ED__110_T60433AB23B0DF65E560B903F37CADD712540FDD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<RoomTransition>d__110
struct  U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<RoomTransition>d__110::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<RoomTransition>d__110::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<RoomTransition>d__110::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CROOMTRANSITIONU3ED__110_T60433AB23B0DF65E560B903F37CADD712540FDD6_H
#ifndef U3CSOURCETEXTHELPERU3ED__91_T86A887CED62D90FF840BB9F6CFE5A7A3BD99C378_H
#define U3CSOURCETEXTHELPERU3ED__91_T86A887CED62D90FF840BB9F6CFE5A7A3BD99C378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<SourceTextHelper>d__91
struct  U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<SourceTextHelper>d__91::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<SourceTextHelper>d__91::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<SourceTextHelper>d__91::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOURCETEXTHELPERU3ED__91_T86A887CED62D90FF840BB9F6CFE5A7A3BD99C378_H
#ifndef U3CSTARTFADEINU3ED__87_T89378F24AAFF304A27F5DFE24313266F12795C9B_H
#define U3CSTARTFADEINU3ED__87_T89378F24AAFF304A27F5DFE24313266F12795C9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<StartFadeIn>d__87
struct  U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<StartFadeIn>d__87::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<StartFadeIn>d__87::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<StartFadeIn>d__87::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFADEINU3ED__87_T89378F24AAFF304A27F5DFE24313266F12795C9B_H
#ifndef U3CWAITFORSOURCETEXTSCROLLU3ED__94_T6C579F4B055277F3523F8422EAEF35178ABF44FF_H
#define U3CWAITFORSOURCETEXTSCROLLU3ED__94_T6C579F4B055277F3523F8422EAEF35178ABF44FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop_<WaitForSourceTextScroll>d__94
struct  U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF  : public RuntimeObject
{
public:
	// System.Int32 VW_Gameloop_<WaitForSourceTextScroll>d__94::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Gameloop_<WaitForSourceTextScroll>d__94::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Gameloop VW_Gameloop_<WaitForSourceTextScroll>d__94::<>4__this
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF, ___U3CU3E4__this_2)); }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSOURCETEXTSCROLLU3ED__94_T6C579F4B055277F3523F8422EAEF35178ABF44FF_H
#ifndef U3CTRANSITIONU3ED__8_T3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B_H
#define U3CTRANSITIONU3ED__8_T3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Start_<Transition>d__8
struct  U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B  : public RuntimeObject
{
public:
	// System.Int32 VW_Start_<Transition>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VW_Start_<Transition>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VW_Start VW_Start_<Transition>d__8::<>4__this
	VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B, ___U3CU3E4__this_2)); }
	inline VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONU3ED__8_T3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B_H
#ifndef HTTPHELPERYOUTUBE_TD81DEC1D08F2E8275A2A347FCA5A6ED388E84EF0_H
#define HTTPHELPERYOUTUBE_TD81DEC1D08F2E8275A2A347FCA5A6ED388E84EF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.HTTPHelperYoutube
struct  HTTPHelperYoutube_tD81DEC1D08F2E8275A2A347FCA5A6ED388E84EF0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHELPERYOUTUBE_TD81DEC1D08F2E8275A2A347FCA5A6ED388E84EF0_H
#ifndef MAGICHANDS_T931494FE8B8B329EE92091FFC09073D63DDDCFFC_H
#define MAGICHANDS_T931494FE8B8B329EE92091FFC09073D63DDDCFFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.MagicHands
struct  MagicHands_t931494FE8B8B329EE92091FFC09073D63DDDCFFC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGICHANDS_T931494FE8B8B329EE92091FFC09073D63DDDCFFC_H
#ifndef YOUTUBESNIPPET_T1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40_H
#define YOUTUBESNIPPET_T1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeSnippet
struct  YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40  : public RuntimeObject
{
public:
	// System.String YoutubeSnippet::publishedAt
	String_t* ___publishedAt_0;
	// System.String YoutubeSnippet::channelId
	String_t* ___channelId_1;
	// System.String YoutubeSnippet::title
	String_t* ___title_2;
	// System.String YoutubeSnippet::description
	String_t* ___description_3;
	// YoutubeTumbnails YoutubeSnippet::thumbnails
	YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC * ___thumbnails_4;
	// System.String YoutubeSnippet::channelTitle
	String_t* ___channelTitle_5;
	// System.String[] YoutubeSnippet::tags
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tags_6;
	// System.String YoutubeSnippet::categoryId
	String_t* ___categoryId_7;

public:
	inline static int32_t get_offset_of_publishedAt_0() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___publishedAt_0)); }
	inline String_t* get_publishedAt_0() const { return ___publishedAt_0; }
	inline String_t** get_address_of_publishedAt_0() { return &___publishedAt_0; }
	inline void set_publishedAt_0(String_t* value)
	{
		___publishedAt_0 = value;
		Il2CppCodeGenWriteBarrier((&___publishedAt_0), value);
	}

	inline static int32_t get_offset_of_channelId_1() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___channelId_1)); }
	inline String_t* get_channelId_1() const { return ___channelId_1; }
	inline String_t** get_address_of_channelId_1() { return &___channelId_1; }
	inline void set_channelId_1(String_t* value)
	{
		___channelId_1 = value;
		Il2CppCodeGenWriteBarrier((&___channelId_1), value);
	}

	inline static int32_t get_offset_of_title_2() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___title_2)); }
	inline String_t* get_title_2() const { return ___title_2; }
	inline String_t** get_address_of_title_2() { return &___title_2; }
	inline void set_title_2(String_t* value)
	{
		___title_2 = value;
		Il2CppCodeGenWriteBarrier((&___title_2), value);
	}

	inline static int32_t get_offset_of_description_3() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___description_3)); }
	inline String_t* get_description_3() const { return ___description_3; }
	inline String_t** get_address_of_description_3() { return &___description_3; }
	inline void set_description_3(String_t* value)
	{
		___description_3 = value;
		Il2CppCodeGenWriteBarrier((&___description_3), value);
	}

	inline static int32_t get_offset_of_thumbnails_4() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___thumbnails_4)); }
	inline YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC * get_thumbnails_4() const { return ___thumbnails_4; }
	inline YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC ** get_address_of_thumbnails_4() { return &___thumbnails_4; }
	inline void set_thumbnails_4(YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC * value)
	{
		___thumbnails_4 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnails_4), value);
	}

	inline static int32_t get_offset_of_channelTitle_5() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___channelTitle_5)); }
	inline String_t* get_channelTitle_5() const { return ___channelTitle_5; }
	inline String_t** get_address_of_channelTitle_5() { return &___channelTitle_5; }
	inline void set_channelTitle_5(String_t* value)
	{
		___channelTitle_5 = value;
		Il2CppCodeGenWriteBarrier((&___channelTitle_5), value);
	}

	inline static int32_t get_offset_of_tags_6() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___tags_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tags_6() const { return ___tags_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tags_6() { return &___tags_6; }
	inline void set_tags_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___tags_6), value);
	}

	inline static int32_t get_offset_of_categoryId_7() { return static_cast<int32_t>(offsetof(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40, ___categoryId_7)); }
	inline String_t* get_categoryId_7() const { return ___categoryId_7; }
	inline String_t** get_address_of_categoryId_7() { return &___categoryId_7; }
	inline void set_categoryId_7(String_t* value)
	{
		___categoryId_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryId_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBESNIPPET_T1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40_H
#ifndef YOUTUBESTATISTICS_T0A77F85B28DCD4F915828E5C116C4BFFE316C3AC_H
#define YOUTUBESTATISTICS_T0A77F85B28DCD4F915828E5C116C4BFFE316C3AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeStatistics
struct  YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC  : public RuntimeObject
{
public:
	// System.String YoutubeStatistics::viewCount
	String_t* ___viewCount_0;
	// System.String YoutubeStatistics::likeCount
	String_t* ___likeCount_1;
	// System.String YoutubeStatistics::dislikeCount
	String_t* ___dislikeCount_2;
	// System.String YoutubeStatistics::favoriteCount
	String_t* ___favoriteCount_3;
	// System.String YoutubeStatistics::commentCount
	String_t* ___commentCount_4;

public:
	inline static int32_t get_offset_of_viewCount_0() { return static_cast<int32_t>(offsetof(YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC, ___viewCount_0)); }
	inline String_t* get_viewCount_0() const { return ___viewCount_0; }
	inline String_t** get_address_of_viewCount_0() { return &___viewCount_0; }
	inline void set_viewCount_0(String_t* value)
	{
		___viewCount_0 = value;
		Il2CppCodeGenWriteBarrier((&___viewCount_0), value);
	}

	inline static int32_t get_offset_of_likeCount_1() { return static_cast<int32_t>(offsetof(YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC, ___likeCount_1)); }
	inline String_t* get_likeCount_1() const { return ___likeCount_1; }
	inline String_t** get_address_of_likeCount_1() { return &___likeCount_1; }
	inline void set_likeCount_1(String_t* value)
	{
		___likeCount_1 = value;
		Il2CppCodeGenWriteBarrier((&___likeCount_1), value);
	}

	inline static int32_t get_offset_of_dislikeCount_2() { return static_cast<int32_t>(offsetof(YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC, ___dislikeCount_2)); }
	inline String_t* get_dislikeCount_2() const { return ___dislikeCount_2; }
	inline String_t** get_address_of_dislikeCount_2() { return &___dislikeCount_2; }
	inline void set_dislikeCount_2(String_t* value)
	{
		___dislikeCount_2 = value;
		Il2CppCodeGenWriteBarrier((&___dislikeCount_2), value);
	}

	inline static int32_t get_offset_of_favoriteCount_3() { return static_cast<int32_t>(offsetof(YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC, ___favoriteCount_3)); }
	inline String_t* get_favoriteCount_3() const { return ___favoriteCount_3; }
	inline String_t** get_address_of_favoriteCount_3() { return &___favoriteCount_3; }
	inline void set_favoriteCount_3(String_t* value)
	{
		___favoriteCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___favoriteCount_3), value);
	}

	inline static int32_t get_offset_of_commentCount_4() { return static_cast<int32_t>(offsetof(YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC, ___commentCount_4)); }
	inline String_t* get_commentCount_4() const { return ___commentCount_4; }
	inline String_t** get_address_of_commentCount_4() { return &___commentCount_4; }
	inline void set_commentCount_4(String_t* value)
	{
		___commentCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___commentCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBESTATISTICS_T0A77F85B28DCD4F915828E5C116C4BFFE316C3AC_H
#ifndef U3CU3EC_T604D3984D39829E479EF332158A235B2256C9526_H
#define U3CU3EC_T604D3984D39829E479EF332158A235B2256C9526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeSubtitlesReader_<>c
struct  U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields
{
public:
	// YoutubeSubtitlesReader_<>c YoutubeSubtitlesReader_<>c::<>9
	U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> YoutubeSubtitlesReader_<>c::<>9__14_0
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CU3E9__14_0_1;
	// System.Func`2<System.String,System.Boolean> YoutubeSubtitlesReader_<>c::<>9__14_1
	Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * ___U3CU3E9__14_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields, ___U3CU3E9__14_0_1)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CU3E9__14_0_1() const { return ___U3CU3E9__14_0_1; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CU3E9__14_0_1() { return &___U3CU3E9__14_0_1; }
	inline void set_U3CU3E9__14_0_1(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CU3E9__14_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields, ___U3CU3E9__14_1_2)); }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * get_U3CU3E9__14_1_2() const { return ___U3CU3E9__14_1_2; }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC ** get_address_of_U3CU3E9__14_1_2() { return &___U3CU3E9__14_1_2; }
	inline void set_U3CU3E9__14_1_2(Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * value)
	{
		___U3CU3E9__14_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T604D3984D39829E479EF332158A235B2256C9526_H
#ifndef U3CDOWNLOADSUBTITLEU3ED__12_TB284D98017933707139413117186754EEBE87F96_H
#define U3CDOWNLOADSUBTITLEU3ED__12_TB284D98017933707139413117186754EEBE87F96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeSubtitlesReader_<DownloadSubtitle>d__12
struct  U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96  : public RuntimeObject
{
public:
	// System.Int32 YoutubeSubtitlesReader_<DownloadSubtitle>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeSubtitlesReader_<DownloadSubtitle>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubeSubtitlesReader YoutubeSubtitlesReader_<DownloadSubtitle>d__12::<>4__this
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA * ___U3CU3E4__this_2;
	// UnityEngine.WWW YoutubeSubtitlesReader_<DownloadSubtitle>d__12::<url>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CurlU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96, ___U3CU3E4__this_2)); }
	inline YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96, ___U3CurlU3E5__2_3)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CurlU3E5__2_3() const { return ___U3CurlU3E5__2_3; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CurlU3E5__2_3() { return &___U3CurlU3E5__2_3; }
	inline void set_U3CurlU3E5__2_3(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CurlU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADSUBTITLEU3ED__12_TB284D98017933707139413117186754EEBE87F96_H
#ifndef U3CGETVTTSUBTITLEPARTSU3ED__15_T9B0878663BC0AC499121B0E09D892DEE336FF555_H
#define U3CGETVTTSUBTITLEPARTSU3ED__15_T9B0878663BC0AC499121B0E09D892DEE336FF555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15
struct  U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555  : public RuntimeObject
{
public:
	// System.Int32 YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.String YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15::<>2__current
	String_t* ___U3CU3E2__current_1;
	// System.Int32 YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Byte[] YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15::r
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___r_3;
	// System.Byte[] YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15::<>3__r
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CU3E3__r_4;
	// System.IO.StreamReader YoutubeSubtitlesReader_<GetVttSubTitleParts>d__15::<reader>5__2
	StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * ___U3CreaderU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555, ___U3CU3E2__current_1)); }
	inline String_t* get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline String_t** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(String_t* value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555, ___r_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_r_3() const { return ___r_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___r_3 = value;
		Il2CppCodeGenWriteBarrier((&___r_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__r_4() { return static_cast<int32_t>(offsetof(U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555, ___U3CU3E3__r_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CU3E3__r_4() const { return ___U3CU3E3__r_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CU3E3__r_4() { return &___U3CU3E3__r_4; }
	inline void set_U3CU3E3__r_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CU3E3__r_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__r_4), value);
	}

	inline static int32_t get_offset_of_U3CreaderU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555, ___U3CreaderU3E5__2_5)); }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * get_U3CreaderU3E5__2_5() const { return ___U3CreaderU3E5__2_5; }
	inline StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E ** get_address_of_U3CreaderU3E5__2_5() { return &___U3CreaderU3E5__2_5; }
	inline void set_U3CreaderU3E5__2_5(StreamReader_t62E68063760DCD2FC036AE132DE69C24B7ED001E * value)
	{
		___U3CreaderU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreaderU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETVTTSUBTITLEPARTSU3ED__15_T9B0878663BC0AC499121B0E09D892DEE336FF555_H
#ifndef YOUTUBETHUMBNAILDATA_T707283168497269B2FDBB9C7AB915706EFA408AA_H
#define YOUTUBETHUMBNAILDATA_T707283168497269B2FDBB9C7AB915706EFA408AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeThumbnailData
struct  YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA  : public RuntimeObject
{
public:
	// System.String YoutubeThumbnailData::url
	String_t* ___url_0;
	// System.String YoutubeThumbnailData::width
	String_t* ___width_1;
	// System.String YoutubeThumbnailData::height
	String_t* ___height_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA, ___width_1)); }
	inline String_t* get_width_1() const { return ___width_1; }
	inline String_t** get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(String_t* value)
	{
		___width_1 = value;
		Il2CppCodeGenWriteBarrier((&___width_1), value);
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA, ___height_2)); }
	inline String_t* get_height_2() const { return ___height_2; }
	inline String_t** get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(String_t* value)
	{
		___height_2 = value;
		Il2CppCodeGenWriteBarrier((&___height_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBETHUMBNAILDATA_T707283168497269B2FDBB9C7AB915706EFA408AA_H
#ifndef YOUTUBETUMBNAILS_T2DE44958571CFC58B8078BF5A760769F5A1C9CBC_H
#define YOUTUBETUMBNAILS_T2DE44958571CFC58B8078BF5A760769F5A1C9CBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeTumbnails
struct  YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC  : public RuntimeObject
{
public:
	// YoutubeThumbnailData YoutubeTumbnails::defaultThumbnail
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * ___defaultThumbnail_0;
	// YoutubeThumbnailData YoutubeTumbnails::mediumThumbnail
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * ___mediumThumbnail_1;
	// YoutubeThumbnailData YoutubeTumbnails::highThumbnail
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * ___highThumbnail_2;
	// YoutubeThumbnailData YoutubeTumbnails::standardThumbnail
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * ___standardThumbnail_3;

public:
	inline static int32_t get_offset_of_defaultThumbnail_0() { return static_cast<int32_t>(offsetof(YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC, ___defaultThumbnail_0)); }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * get_defaultThumbnail_0() const { return ___defaultThumbnail_0; }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA ** get_address_of_defaultThumbnail_0() { return &___defaultThumbnail_0; }
	inline void set_defaultThumbnail_0(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * value)
	{
		___defaultThumbnail_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultThumbnail_0), value);
	}

	inline static int32_t get_offset_of_mediumThumbnail_1() { return static_cast<int32_t>(offsetof(YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC, ___mediumThumbnail_1)); }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * get_mediumThumbnail_1() const { return ___mediumThumbnail_1; }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA ** get_address_of_mediumThumbnail_1() { return &___mediumThumbnail_1; }
	inline void set_mediumThumbnail_1(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * value)
	{
		___mediumThumbnail_1 = value;
		Il2CppCodeGenWriteBarrier((&___mediumThumbnail_1), value);
	}

	inline static int32_t get_offset_of_highThumbnail_2() { return static_cast<int32_t>(offsetof(YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC, ___highThumbnail_2)); }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * get_highThumbnail_2() const { return ___highThumbnail_2; }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA ** get_address_of_highThumbnail_2() { return &___highThumbnail_2; }
	inline void set_highThumbnail_2(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * value)
	{
		___highThumbnail_2 = value;
		Il2CppCodeGenWriteBarrier((&___highThumbnail_2), value);
	}

	inline static int32_t get_offset_of_standardThumbnail_3() { return static_cast<int32_t>(offsetof(YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC, ___standardThumbnail_3)); }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * get_standardThumbnail_3() const { return ___standardThumbnail_3; }
	inline YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA ** get_address_of_standardThumbnail_3() { return &___standardThumbnail_3; }
	inline void set_standardThumbnail_3(YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA * value)
	{
		___standardThumbnail_3 = value;
		Il2CppCodeGenWriteBarrier((&___standardThumbnail_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBETUMBNAILS_T2DE44958571CFC58B8078BF5A760769F5A1C9CBC_H
#ifndef SSH_INTVECTOR2_T143F779FCF76ABCE93E606325CD8A755B1A08464_H
#define SSH_INTVECTOR2_T143F779FCF76ABCE93E606325CD8A755B1A08464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.SSH_IntVector2
struct  SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464 
{
public:
	// System.Int32 NG.SSH_IntVector2::width
	int32_t ___width_0;
	// System.Int32 NG.SSH_IntVector2::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSH_INTVECTOR2_T143F779FCF76ABCE93E606325CD8A755B1A08464_H
#ifndef JSONARRAY_TA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8_H
#define JSONARRAY_TA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray
struct  JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.Collections.Generic.List`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::m_List
	List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 * ___m_List_1;
	// System.Boolean SimpleJSON.JSONArray::inline
	bool ___inline_2;

public:
	inline static int32_t get_offset_of_m_List_1() { return static_cast<int32_t>(offsetof(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8, ___m_List_1)); }
	inline List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 * get_m_List_1() const { return ___m_List_1; }
	inline List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 ** get_address_of_m_List_1() { return &___m_List_1; }
	inline void set_m_List_1(List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 * value)
	{
		___m_List_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_List_1), value);
	}

	inline static int32_t get_offset_of_inline_2() { return static_cast<int32_t>(offsetof(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8, ___inline_2)); }
	inline bool get_inline_2() const { return ___inline_2; }
	inline bool* get_address_of_inline_2() { return &___inline_2; }
	inline void set_inline_2(bool value)
	{
		___inline_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_TA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8_H
#ifndef JSONBOOL_TF2B768D59237D03145F065A9A93302A432E96F26_H
#define JSONBOOL_TF2B768D59237D03145F065A9A93302A432E96F26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONBool
struct  JSONBool_tF2B768D59237D03145F065A9A93302A432E96F26  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.Boolean SimpleJSON.JSONBool::m_Data
	bool ___m_Data_1;

public:
	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(JSONBool_tF2B768D59237D03145F065A9A93302A432E96F26, ___m_Data_1)); }
	inline bool get_m_Data_1() const { return ___m_Data_1; }
	inline bool* get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(bool value)
	{
		___m_Data_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONBOOL_TF2B768D59237D03145F065A9A93302A432E96F26_H
#ifndef JSONLAZYCREATOR_TC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C_H
#define JSONLAZYCREATOR_TC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONLazyCreator
struct  JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::m_Node
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___m_Node_1;
	// System.String SimpleJSON.JSONLazyCreator::m_Key
	String_t* ___m_Key_2;

public:
	inline static int32_t get_offset_of_m_Node_1() { return static_cast<int32_t>(offsetof(JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C, ___m_Node_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_m_Node_1() const { return ___m_Node_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_m_Node_1() { return &___m_Node_1; }
	inline void set_m_Node_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___m_Node_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Node_1), value);
	}

	inline static int32_t get_offset_of_m_Key_2() { return static_cast<int32_t>(offsetof(JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C, ___m_Key_2)); }
	inline String_t* get_m_Key_2() const { return ___m_Key_2; }
	inline String_t** get_address_of_m_Key_2() { return &___m_Key_2; }
	inline void set_m_Key_2(String_t* value)
	{
		___m_Key_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLAZYCREATOR_TC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C_H
#ifndef JSONNULL_T07D22ABA25ECFD395B5B309982B136D037067ACE_H
#define JSONNULL_T07D22ABA25ECFD395B5B309982B136D037067ACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNull
struct  JSONNull_t07D22ABA25ECFD395B5B309982B136D037067ACE  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNULL_T07D22ABA25ECFD395B5B309982B136D037067ACE_H
#ifndef JSONNUMBER_T75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839_H
#define JSONNUMBER_T75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNumber
struct  JSONNumber_t75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.Double SimpleJSON.JSONNumber::m_Data
	double ___m_Data_1;

public:
	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(JSONNumber_t75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839, ___m_Data_1)); }
	inline double get_m_Data_1() const { return ___m_Data_1; }
	inline double* get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(double value)
	{
		___m_Data_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNUMBER_T75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839_H
#ifndef JSONOBJECT_T6ED55195ECB193359B6772677198D259D2C86FC9_H
#define JSONOBJECT_T6ED55195ECB193359B6772677198D259D2C86FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject
struct  JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject::m_Dict
	Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 * ___m_Dict_1;
	// System.Boolean SimpleJSON.JSONObject::inline
	bool ___inline_2;

public:
	inline static int32_t get_offset_of_m_Dict_1() { return static_cast<int32_t>(offsetof(JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9, ___m_Dict_1)); }
	inline Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 * get_m_Dict_1() const { return ___m_Dict_1; }
	inline Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 ** get_address_of_m_Dict_1() { return &___m_Dict_1; }
	inline void set_m_Dict_1(Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 * value)
	{
		___m_Dict_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_1), value);
	}

	inline static int32_t get_offset_of_inline_2() { return static_cast<int32_t>(offsetof(JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9, ___inline_2)); }
	inline bool get_inline_2() const { return ___inline_2; }
	inline bool* get_address_of_inline_2() { return &___inline_2; }
	inline void set_inline_2(bool value)
	{
		___inline_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T6ED55195ECB193359B6772677198D259D2C86FC9_H
#ifndef JSONSTRING_TA15F893B4CF6A60092CBAC382BA65A8C55D0A850_H
#define JSONSTRING_TA15F893B4CF6A60092CBAC382BA65A8C55D0A850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONString
struct  JSONString_tA15F893B4CF6A60092CBAC382BA65A8C55D0A850  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.String SimpleJSON.JSONString::m_Data
	String_t* ___m_Data_1;

public:
	inline static int32_t get_offset_of_m_Data_1() { return static_cast<int32_t>(offsetof(JSONString_tA15F893B4CF6A60092CBAC382BA65A8C55D0A850, ___m_Data_1)); }
	inline String_t* get_m_Data_1() const { return ___m_Data_1; }
	inline String_t** get_address_of_m_Data_1() { return &___m_Data_1; }
	inline void set_m_Data_1(String_t* value)
	{
		___m_Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRING_TA15F893B4CF6A60092CBAC382BA65A8C55D0A850_H
#ifndef KEYVALUEPAIR_2_T8DDA772A7174B1AC403AA521D95BC70D9261827D_H
#define KEYVALUEPAIR_2_T8DDA772A7174B1AC403AA521D95BC70D9261827D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>
struct  KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D, ___value_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_value_1() const { return ___value_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T8DDA772A7174B1AC403AA521D95BC70D9261827D_H
#ifndef ENUMERATOR_TE48CDEFF62900F491854C455A4BE27A1C5B8CBD8_H
#define ENUMERATOR_TE48CDEFF62900F491854C455A4BE27A1C5B8CBD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<NG.ShotSize>
struct  Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8, ___list_0)); }
	inline List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * get_list_0() const { return ___list_0; }
	inline List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8, ___current_3)); }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * get_current_3() const { return ___current_3; }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TE48CDEFF62900F491854C455A4BE27A1C5B8CBD8_H
#ifndef ENUMERATOR_TA95B2B5AE9BCC4EC62783A3B9C12191370CA4411_H
#define ENUMERATOR_TA95B2B5AE9BCC4EC62783A3B9C12191370CA4411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>
struct  Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411, ___list_0)); }
	inline List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 * get_list_0() const { return ___list_0; }
	inline List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2AF81B5C4472E7C2F94BBCADB2B4D8570CCBF1B9 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411, ___current_3)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_current_3() const { return ___current_3; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TA95B2B5AE9BCC4EC62783A3B9C12191370CA4411_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef UNIWEBVIEWMESSAGE_TAC6CC5EC4233D8B92FB19548180D216E36A42EC3_H
#define UNIWEBVIEWMESSAGE_TAC6CC5EC4233D8B92FB19548180D216E36A42EC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewMessage
struct  UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3 
{
public:
	// System.String UniWebViewMessage::<RawMessage>k__BackingField
	String_t* ___U3CRawMessageU3Ek__BackingField_0;
	// System.String UniWebViewMessage::<Scheme>k__BackingField
	String_t* ___U3CSchemeU3Ek__BackingField_1;
	// System.String UniWebViewMessage::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniWebViewMessage::<Args>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CArgsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRawMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3, ___U3CRawMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CRawMessageU3Ek__BackingField_0() const { return ___U3CRawMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CRawMessageU3Ek__BackingField_0() { return &___U3CRawMessageU3Ek__BackingField_0; }
	inline void set_U3CRawMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CRawMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSchemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3, ___U3CSchemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CSchemeU3Ek__BackingField_1() const { return ___U3CSchemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSchemeU3Ek__BackingField_1() { return &___U3CSchemeU3Ek__BackingField_1; }
	inline void set_U3CSchemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CSchemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSchemeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3, ___U3CPathU3Ek__BackingField_2)); }
	inline String_t* get_U3CPathU3Ek__BackingField_2() const { return ___U3CPathU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_2() { return &___U3CPathU3Ek__BackingField_2; }
	inline void set_U3CPathU3Ek__BackingField_2(String_t* value)
	{
		___U3CPathU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3, ___U3CArgsU3Ek__BackingField_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CArgsU3Ek__BackingField_3() const { return ___U3CArgsU3Ek__BackingField_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CArgsU3Ek__BackingField_3() { return &___U3CArgsU3Ek__BackingField_3; }
	inline void set_U3CArgsU3Ek__BackingField_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CArgsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniWebViewMessage
struct UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3_marshaled_pinvoke
{
	char* ___U3CRawMessageU3Ek__BackingField_0;
	char* ___U3CSchemeU3Ek__BackingField_1;
	char* ___U3CPathU3Ek__BackingField_2;
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CArgsU3Ek__BackingField_3;
};
// Native definition for COM marshalling of UniWebViewMessage
struct UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3_marshaled_com
{
	Il2CppChar* ___U3CRawMessageU3Ek__BackingField_0;
	Il2CppChar* ___U3CSchemeU3Ek__BackingField_1;
	Il2CppChar* ___U3CPathU3Ek__BackingField_2;
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CArgsU3Ek__BackingField_3;
};
#endif // UNIWEBVIEWMESSAGE_TAC6CC5EC4233D8B92FB19548180D216E36A42EC3_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VIDEONOTAVAILABLEEXCEPTION_TBAC14AF14AC6B5917524405D4CE14D6F2B150DC1_H
#define VIDEONOTAVAILABLEEXCEPTION_TBAC14AF14AC6B5917524405D4CE14D6F2B150DC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.VideoNotAvailableException
struct  VideoNotAvailableException_tBAC14AF14AC6B5917524405D4CE14D6F2B150DC1  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEONOTAVAILABLEEXCEPTION_TBAC14AF14AC6B5917524405D4CE14D6F2B150DC1_H
#ifndef YOUTUBEPARSEEXCEPTION_T47C1A3E486CF3FA6F95CB02AEB58338DE2AB03D0_H
#define YOUTUBEPARSEEXCEPTION_T47C1A3E486CF3FA6F95CB02AEB58338DE2AB03D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.YoutubeParseException
struct  YoutubeParseException_t47C1A3E486CF3FA6F95CB02AEB58338DE2AB03D0  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEPARSEEXCEPTION_T47C1A3E486CF3FA6F95CB02AEB58338DE2AB03D0_H
#ifndef SSHORIENTATION_TD92267ED5F3D35B0FAE933B079E87614D5DE1164_H
#define SSHORIENTATION_TD92267ED5F3D35B0FAE933B079E87614D5DE1164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.SSHOrientation
struct  SSHOrientation_tD92267ED5F3D35B0FAE933B079E87614D5DE1164 
{
public:
	// System.Int32 NG.SSHOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SSHOrientation_tD92267ED5F3D35B0FAE933B079E87614D5DE1164, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSHORIENTATION_TD92267ED5F3D35B0FAE933B079E87614D5DE1164_H
#ifndef U3CTAKESCREENSHOTSU3ED__40_T80A59D0C872E13E19DD957758B5E7B200108A652_H
#define U3CTAKESCREENSHOTSU3ED__40_T80A59D0C872E13E19DD957758B5E7B200108A652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_<TakeScreenShots>d__40
struct  U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652  : public RuntimeObject
{
public:
	// System.Int32 NG.ScreenshotHelper_<TakeScreenShots>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NG.ScreenshotHelper_<TakeScreenShots>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// NG.ScreenshotHelper NG.ScreenshotHelper_<TakeScreenShots>d__40::<>4__this
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * ___U3CU3E4__this_2;
	// NG.ScreenshotHelper_<>c__DisplayClass40_0 NG.ScreenshotHelper_<TakeScreenShots>d__40::<>8__1
	U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 * ___U3CU3E8__1_3;
	// System.String NG.ScreenshotHelper_<TakeScreenShots>d__40::<fileName>5__2
	String_t* ___U3CfileNameU3E5__2_4;
	// System.Collections.Generic.List`1_Enumerator<NG.ShotSize> NG.ScreenshotHelper_<TakeScreenShots>d__40::<>7__wrap2
	Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8  ___U3CU3E7__wrap2_5;
	// NG.ShotSize NG.ScreenshotHelper_<TakeScreenShots>d__40::<shot>5__4
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * ___U3CshotU3E5__4_6;
	// NG.SSH_IntVector2 NG.ScreenshotHelper_<TakeScreenShots>d__40::<thisRes>5__5
	SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464  ___U3CthisResU3E5__5_7;
	// System.Int32 NG.ScreenshotHelper_<TakeScreenShots>d__40::<attempts>5__6
	int32_t ___U3CattemptsU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CU3E4__this_2)); }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CfileNameU3E5__2_4)); }
	inline String_t* get_U3CfileNameU3E5__2_4() const { return ___U3CfileNameU3E5__2_4; }
	inline String_t** get_address_of_U3CfileNameU3E5__2_4() { return &___U3CfileNameU3E5__2_4; }
	inline void set_U3CfileNameU3E5__2_4(String_t* value)
	{
		___U3CfileNameU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileNameU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_5() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CU3E7__wrap2_5)); }
	inline Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8  get_U3CU3E7__wrap2_5() const { return ___U3CU3E7__wrap2_5; }
	inline Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8 * get_address_of_U3CU3E7__wrap2_5() { return &___U3CU3E7__wrap2_5; }
	inline void set_U3CU3E7__wrap2_5(Enumerator_tE48CDEFF62900F491854C455A4BE27A1C5B8CBD8  value)
	{
		___U3CU3E7__wrap2_5 = value;
	}

	inline static int32_t get_offset_of_U3CshotU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CshotU3E5__4_6)); }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * get_U3CshotU3E5__4_6() const { return ___U3CshotU3E5__4_6; }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 ** get_address_of_U3CshotU3E5__4_6() { return &___U3CshotU3E5__4_6; }
	inline void set_U3CshotU3E5__4_6(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * value)
	{
		___U3CshotU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CshotU3E5__4_6), value);
	}

	inline static int32_t get_offset_of_U3CthisResU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CthisResU3E5__5_7)); }
	inline SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464  get_U3CthisResU3E5__5_7() const { return ___U3CthisResU3E5__5_7; }
	inline SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464 * get_address_of_U3CthisResU3E5__5_7() { return &___U3CthisResU3E5__5_7; }
	inline void set_U3CthisResU3E5__5_7(SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464  value)
	{
		___U3CthisResU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CattemptsU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652, ___U3CattemptsU3E5__6_8)); }
	inline int32_t get_U3CattemptsU3E5__6_8() const { return ___U3CattemptsU3E5__6_8; }
	inline int32_t* get_address_of_U3CattemptsU3E5__6_8() { return &___U3CattemptsU3E5__6_8; }
	inline void set_U3CattemptsU3E5__6_8(int32_t value)
	{
		___U3CattemptsU3E5__6_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTAKESCREENSHOTSU3ED__40_T80A59D0C872E13E19DD957758B5E7B200108A652_H
#ifndef TSSHTEXTUREFORMAT_T42FE145BFFEFD23B172E1E8B56503D404285EB29_H
#define TSSHTEXTUREFORMAT_T42FE145BFFEFD23B172E1E8B56503D404285EB29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_tSSHTextureFormat
struct  tSSHTextureFormat_t42FE145BFFEFD23B172E1E8B56503D404285EB29 
{
public:
	// System.Int32 NG.ScreenshotHelper_tSSHTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(tSSHTextureFormat_t42FE145BFFEFD23B172E1E8B56503D404285EB29, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSSHTEXTUREFORMAT_T42FE145BFFEFD23B172E1E8B56503D404285EB29_H
#ifndef U3CGETENUMERATORU3ED__19_T7D84C00216929B7C362244E89E6397BC3797F2AD_H
#define U3CGETENUMERATORU3ED__19_T7D84C00216929B7C362244E89E6397BC3797F2AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray_<GetEnumerator>d__19
struct  U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD  : public RuntimeObject
{
public:
	// System.Int32 SimpleJSON.JSONArray_<GetEnumerator>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleJSON.JSONArray_<GetEnumerator>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleJSON.JSONArray SimpleJSON.JSONArray_<GetEnumerator>d__19::<>4__this
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<GetEnumerator>d__19::<>7__wrap1
	Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411  ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD, ___U3CU3E4__this_2)); }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411 * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411  value)
	{
		___U3CU3E7__wrap1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__19_T7D84C00216929B7C362244E89E6397BC3797F2AD_H
#ifndef U3CGET_CHILDRENU3ED__18_T1468C16AAFB4A3E659B8D16A2330948790364AC3_H
#define U3CGET_CHILDRENU3ED__18_T1468C16AAFB4A3E659B8D16A2330948790364AC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray_<get_Children>d__18
struct  U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3  : public RuntimeObject
{
public:
	// System.Int32 SimpleJSON.JSONArray_<get_Children>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray_<get_Children>d__18::<>2__current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CU3E2__current_1;
	// System.Int32 SimpleJSON.JSONArray_<get_Children>d__18::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// SimpleJSON.JSONArray SimpleJSON.JSONArray_<get_Children>d__18::<>4__this
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<get_Children>d__18::<>7__wrap1
	Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411  ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3, ___U3CU3E2__current_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3, ___U3CU3E4__this_3)); }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3, ___U3CU3E7__wrap1_4)); }
	inline Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411  get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411 * get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(Enumerator_tA95B2B5AE9BCC4EC62783A3B9C12191370CA4411  value)
	{
		___U3CU3E7__wrap1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_CHILDRENU3ED__18_T1468C16AAFB4A3E659B8D16A2330948790364AC3_H
#ifndef JSONNODETYPE_TF5D7E10AB8DB7403C6B5F7485A21932E862BF313_H
#define JSONNODETYPE_TF5D7E10AB8DB7403C6B5F7485A21932E862BF313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNodeType
struct  JSONNodeType_tF5D7E10AB8DB7403C6B5F7485A21932E862BF313 
{
public:
	// System.Int32 SimpleJSON.JSONNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONNodeType_tF5D7E10AB8DB7403C6B5F7485A21932E862BF313, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNODETYPE_TF5D7E10AB8DB7403C6B5F7485A21932E862BF313_H
#ifndef JSONTEXTMODE_T894D3826A172EA1B28488FF102150AF240171F8A_H
#define JSONTEXTMODE_T894D3826A172EA1B28488FF102150AF240171F8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONTextMode
struct  JSONTextMode_t894D3826A172EA1B28488FF102150AF240171F8A 
{
public:
	// System.Int32 SimpleJSON.JSONTextMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JSONTextMode_t894D3826A172EA1B28488FF102150AF240171F8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTMODE_T894D3826A172EA1B28488FF102150AF240171F8A_H
#ifndef ENUMERATOR_T330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077_H
#define ENUMERATOR_T330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>
struct  Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077, ___dictionary_0)); }
	inline Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tC43AE3E4968C62B622A960DDD471888B5C2DA172 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077, ___current_3)); }
	inline KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t8DDA772A7174B1AC403AA521D95BC70D9261827D  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef SPECIALFOLDER_T5A2C2AE97BD6C2DF95061C8904B2225228AC9BA0_H
#define SPECIALFOLDER_T5A2C2AE97BD6C2DF95061C8904B2225228AC9BA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Environment_SpecialFolder
struct  SpecialFolder_t5A2C2AE97BD6C2DF95061C8904B2225228AC9BA0 
{
public:
	// System.Int32 System.Environment_SpecialFolder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialFolder_t5A2C2AE97BD6C2DF95061C8904B2225228AC9BA0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALFOLDER_T5A2C2AE97BD6C2DF95061C8904B2225228AC9BA0_H
#ifndef UNIWEBVIEWCONTENTINSETADJUSTMENTBEHAVIOR_T8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC_H
#define UNIWEBVIEWCONTENTINSETADJUSTMENTBEHAVIOR_T8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewContentInsetAdjustmentBehavior
struct  UniWebViewContentInsetAdjustmentBehavior_t8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC 
{
public:
	// System.Int32 UniWebViewContentInsetAdjustmentBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UniWebViewContentInsetAdjustmentBehavior_t8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWCONTENTINSETADJUSTMENTBEHAVIOR_T8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC_H
#ifndef LEVEL_TB3797A8D0D24AF315855A7B3D8DF86C22DFF2875_H
#define LEVEL_TB3797A8D0D24AF315855A7B3D8DF86C22DFF2875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewLogger_Level
struct  Level_tB3797A8D0D24AF315855A7B3D8DF86C22DFF2875 
{
public:
	// System.Int32 UniWebViewLogger_Level::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Level_tB3797A8D0D24AF315855A7B3D8DF86C22DFF2875, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_TB3797A8D0D24AF315855A7B3D8DF86C22DFF2875_H
#ifndef UNIWEBVIEWTOOLBARPOSITION_T32937D05C62981C715E721005693AC89DE374110_H
#define UNIWEBVIEWTOOLBARPOSITION_T32937D05C62981C715E721005693AC89DE374110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewToolbarPosition
struct  UniWebViewToolbarPosition_t32937D05C62981C715E721005693AC89DE374110 
{
public:
	// System.Int32 UniWebViewToolbarPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UniWebViewToolbarPosition_t32937D05C62981C715E721005693AC89DE374110, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTOOLBARPOSITION_T32937D05C62981C715E721005693AC89DE374110_H
#ifndef UNIWEBVIEWTRANSITIONEDGE_T5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3_H
#define UNIWEBVIEWTRANSITIONEDGE_T5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewTransitionEdge
struct  UniWebViewTransitionEdge_t5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3 
{
public:
	// System.Int32 UniWebViewTransitionEdge::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UniWebViewTransitionEdge_t5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWTRANSITIONEDGE_T5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#define SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T4AB8E2E02033B0EAEA0260B05B1D88DA8058BB51_H
#ifndef ADAPTIVETYPE_T2CE340BC1400B7593B10E7F316D7FB972B6258AC_H
#define ADAPTIVETYPE_T2CE340BC1400B7593B10E7F316D7FB972B6258AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.AdaptiveType
struct  AdaptiveType_t2CE340BC1400B7593B10E7F316D7FB972B6258AC 
{
public:
	// System.Int32 YoutubeLight.AdaptiveType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdaptiveType_t2CE340BC1400B7593B10E7F316D7FB972B6258AC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVETYPE_T2CE340BC1400B7593B10E7F316D7FB972B6258AC_H
#ifndef AUDIOTYPE_T5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F_H
#define AUDIOTYPE_T5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.AudioType
struct  AudioType_t5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F 
{
public:
	// System.Int32 YoutubeLight.AudioType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioType_t5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTYPE_T5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F_H
#ifndef DOWNLOADER_TCDC0917C49FCE183E7BCF542605C26DF14C670B2_H
#define DOWNLOADER_TCDC0917C49FCE183E7BCF542605C26DF14C670B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.Downloader
struct  Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2  : public RuntimeObject
{
public:
	// System.EventHandler YoutubeLight.Downloader::DownloadFinished
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___DownloadFinished_0;
	// System.EventHandler YoutubeLight.Downloader::DownloadStarted
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___DownloadStarted_1;
	// System.Nullable`1<System.Int32> YoutubeLight.Downloader::<BytesToDownload>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CBytesToDownloadU3Ek__BackingField_2;
	// System.String YoutubeLight.Downloader::<SavePath>k__BackingField
	String_t* ___U3CSavePathU3Ek__BackingField_3;
	// YoutubeLight.VideoInfo YoutubeLight.Downloader::<Video>k__BackingField
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791 * ___U3CVideoU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_DownloadFinished_0() { return static_cast<int32_t>(offsetof(Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2, ___DownloadFinished_0)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_DownloadFinished_0() const { return ___DownloadFinished_0; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_DownloadFinished_0() { return &___DownloadFinished_0; }
	inline void set_DownloadFinished_0(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___DownloadFinished_0 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadFinished_0), value);
	}

	inline static int32_t get_offset_of_DownloadStarted_1() { return static_cast<int32_t>(offsetof(Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2, ___DownloadStarted_1)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_DownloadStarted_1() const { return ___DownloadStarted_1; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_DownloadStarted_1() { return &___DownloadStarted_1; }
	inline void set_DownloadStarted_1(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___DownloadStarted_1 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadStarted_1), value);
	}

	inline static int32_t get_offset_of_U3CBytesToDownloadU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2, ___U3CBytesToDownloadU3Ek__BackingField_2)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CBytesToDownloadU3Ek__BackingField_2() const { return ___U3CBytesToDownloadU3Ek__BackingField_2; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CBytesToDownloadU3Ek__BackingField_2() { return &___U3CBytesToDownloadU3Ek__BackingField_2; }
	inline void set_U3CBytesToDownloadU3Ek__BackingField_2(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CBytesToDownloadU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSavePathU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2, ___U3CSavePathU3Ek__BackingField_3)); }
	inline String_t* get_U3CSavePathU3Ek__BackingField_3() const { return ___U3CSavePathU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CSavePathU3Ek__BackingField_3() { return &___U3CSavePathU3Ek__BackingField_3; }
	inline void set_U3CSavePathU3Ek__BackingField_3(String_t* value)
	{
		___U3CSavePathU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSavePathU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CVideoU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2, ___U3CVideoU3Ek__BackingField_4)); }
	inline VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791 * get_U3CVideoU3Ek__BackingField_4() const { return ___U3CVideoU3Ek__BackingField_4; }
	inline VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791 ** get_address_of_U3CVideoU3Ek__BackingField_4() { return &___U3CVideoU3Ek__BackingField_4; }
	inline void set_U3CVideoU3Ek__BackingField_4(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791 * value)
	{
		___U3CVideoU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVideoU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADER_TCDC0917C49FCE183E7BCF542605C26DF14C670B2_H
#ifndef VIDEOTYPE_T1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3_H
#define VIDEOTYPE_T1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.VideoType
struct  VideoType_t1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3 
{
public:
	// System.Int32 YoutubeLight.VideoType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoType_t1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTYPE_T1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3_H
#ifndef SSHPRESET_TEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF_H
#define SSHPRESET_TEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.SSHPreset
struct  SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF  : public RuntimeObject
{
public:
	// NG.SSHOrientation NG.SSHPreset::orientation
	int32_t ___orientation_0;
	// System.String NG.SSHPreset::lastSavePath
	String_t* ___lastSavePath_1;
	// NG.ScreenshotHelper_tSSHTextureFormat NG.SSHPreset::textureFormat
	int32_t ___textureFormat_2;
	// UnityEngine.KeyCode NG.SSHPreset::keyToPress
	int32_t ___keyToPress_3;
	// UnityEngine.KeyCode NG.SSHPreset::keyToHold
	int32_t ___keyToHold_4;
	// System.Environment_SpecialFolder NG.SSHPreset::buildPathRoot
	int32_t ___buildPathRoot_5;
	// System.String NG.SSHPreset::buildPathExtra
	String_t* ___buildPathExtra_6;
	// System.Collections.Generic.List`1<NG.ShotSize> NG.SSHPreset::sizes
	List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * ___sizes_7;

public:
	inline static int32_t get_offset_of_orientation_0() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___orientation_0)); }
	inline int32_t get_orientation_0() const { return ___orientation_0; }
	inline int32_t* get_address_of_orientation_0() { return &___orientation_0; }
	inline void set_orientation_0(int32_t value)
	{
		___orientation_0 = value;
	}

	inline static int32_t get_offset_of_lastSavePath_1() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___lastSavePath_1)); }
	inline String_t* get_lastSavePath_1() const { return ___lastSavePath_1; }
	inline String_t** get_address_of_lastSavePath_1() { return &___lastSavePath_1; }
	inline void set_lastSavePath_1(String_t* value)
	{
		___lastSavePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___lastSavePath_1), value);
	}

	inline static int32_t get_offset_of_textureFormat_2() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___textureFormat_2)); }
	inline int32_t get_textureFormat_2() const { return ___textureFormat_2; }
	inline int32_t* get_address_of_textureFormat_2() { return &___textureFormat_2; }
	inline void set_textureFormat_2(int32_t value)
	{
		___textureFormat_2 = value;
	}

	inline static int32_t get_offset_of_keyToPress_3() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___keyToPress_3)); }
	inline int32_t get_keyToPress_3() const { return ___keyToPress_3; }
	inline int32_t* get_address_of_keyToPress_3() { return &___keyToPress_3; }
	inline void set_keyToPress_3(int32_t value)
	{
		___keyToPress_3 = value;
	}

	inline static int32_t get_offset_of_keyToHold_4() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___keyToHold_4)); }
	inline int32_t get_keyToHold_4() const { return ___keyToHold_4; }
	inline int32_t* get_address_of_keyToHold_4() { return &___keyToHold_4; }
	inline void set_keyToHold_4(int32_t value)
	{
		___keyToHold_4 = value;
	}

	inline static int32_t get_offset_of_buildPathRoot_5() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___buildPathRoot_5)); }
	inline int32_t get_buildPathRoot_5() const { return ___buildPathRoot_5; }
	inline int32_t* get_address_of_buildPathRoot_5() { return &___buildPathRoot_5; }
	inline void set_buildPathRoot_5(int32_t value)
	{
		___buildPathRoot_5 = value;
	}

	inline static int32_t get_offset_of_buildPathExtra_6() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___buildPathExtra_6)); }
	inline String_t* get_buildPathExtra_6() const { return ___buildPathExtra_6; }
	inline String_t** get_address_of_buildPathExtra_6() { return &___buildPathExtra_6; }
	inline void set_buildPathExtra_6(String_t* value)
	{
		___buildPathExtra_6 = value;
		Il2CppCodeGenWriteBarrier((&___buildPathExtra_6), value);
	}

	inline static int32_t get_offset_of_sizes_7() { return static_cast<int32_t>(offsetof(SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF, ___sizes_7)); }
	inline List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * get_sizes_7() const { return ___sizes_7; }
	inline List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 ** get_address_of_sizes_7() { return &___sizes_7; }
	inline void set_sizes_7(List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * value)
	{
		___sizes_7 = value;
		Il2CppCodeGenWriteBarrier((&___sizes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSHPRESET_TEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF_H
#ifndef U3CGETENUMERATORU3ED__20_T91EA000CFDB6F9A15A550FB4536D59BDECA44061_H
#define U3CGETENUMERATORU3ED__20_T91EA000CFDB6F9A15A550FB4536D59BDECA44061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject_<GetEnumerator>d__20
struct  U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061  : public RuntimeObject
{
public:
	// System.Int32 SimpleJSON.JSONObject_<GetEnumerator>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleJSON.JSONObject_<GetEnumerator>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleJSON.JSONObject SimpleJSON.JSONObject_<GetEnumerator>d__20::<>4__this
	JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 * ___U3CU3E4__this_2;
	// System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject_<GetEnumerator>d__20::<>7__wrap1
	Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077  ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061, ___U3CU3E4__this_2)); }
	inline JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077 * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077  value)
	{
		___U3CU3E7__wrap1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__20_T91EA000CFDB6F9A15A550FB4536D59BDECA44061_H
#ifndef U3CGET_CHILDRENU3ED__19_T5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9_H
#define U3CGET_CHILDRENU3ED__19_T5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONObject_<get_Children>d__19
struct  U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9  : public RuntimeObject
{
public:
	// System.Int32 SimpleJSON.JSONObject_<get_Children>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONObject_<get_Children>d__19::<>2__current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CU3E2__current_1;
	// System.Int32 SimpleJSON.JSONObject_<get_Children>d__19::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// SimpleJSON.JSONObject SimpleJSON.JSONObject_<get_Children>d__19::<>4__this
	JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 * ___U3CU3E4__this_3;
	// System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONObject_<get_Children>d__19::<>7__wrap1
	Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077  ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9, ___U3CU3E2__current_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9, ___U3CU3E4__this_3)); }
	inline JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9, ___U3CU3E7__wrap1_4)); }
	inline Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077  get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077 * get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(Enumerator_t330F7F2AAB68E27B51B1D2CB4B357AFFEC25A077  value)
	{
		___U3CU3E7__wrap1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_CHILDRENU3ED__19_T5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef UNIWEBVIEWLOGGER_TE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_H
#define UNIWEBVIEWLOGGER_TE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewLogger
struct  UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36  : public RuntimeObject
{
public:
	// UniWebViewLogger_Level UniWebViewLogger::level
	int32_t ___level_1;

public:
	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}
};

struct UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_StaticFields
{
public:
	// UniWebViewLogger UniWebViewLogger::instance
	UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_StaticFields, ___instance_0)); }
	inline UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36 * get_instance_0() const { return ___instance_0; }
	inline UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWLOGGER_TE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef VIDEOINFO_T29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_H
#define VIDEOINFO_T29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLight.VideoInfo
struct  VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791  : public RuntimeObject
{
public:
	// YoutubeLight.AdaptiveType YoutubeLight.VideoInfo::<AdaptiveType>k__BackingField
	int32_t ___U3CAdaptiveTypeU3Ek__BackingField_1;
	// System.Int32 YoutubeLight.VideoInfo::<AudioBitrate>k__BackingField
	int32_t ___U3CAudioBitrateU3Ek__BackingField_2;
	// YoutubeLight.AudioType YoutubeLight.VideoInfo::<AudioType>k__BackingField
	int32_t ___U3CAudioTypeU3Ek__BackingField_3;
	// System.String YoutubeLight.VideoInfo::<DownloadUrl>k__BackingField
	String_t* ___U3CDownloadUrlU3Ek__BackingField_4;
	// System.Int32 YoutubeLight.VideoInfo::<FormatCode>k__BackingField
	int32_t ___U3CFormatCodeU3Ek__BackingField_5;
	// System.Boolean YoutubeLight.VideoInfo::<Is3D>k__BackingField
	bool ___U3CIs3DU3Ek__BackingField_6;
	// System.Boolean YoutubeLight.VideoInfo::<HDR>k__BackingField
	bool ___U3CHDRU3Ek__BackingField_7;
	// System.Boolean YoutubeLight.VideoInfo::<RequiresDecryption>k__BackingField
	bool ___U3CRequiresDecryptionU3Ek__BackingField_8;
	// System.Int32 YoutubeLight.VideoInfo::<Resolution>k__BackingField
	int32_t ___U3CResolutionU3Ek__BackingField_9;
	// System.String YoutubeLight.VideoInfo::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_10;
	// YoutubeLight.VideoType YoutubeLight.VideoInfo::<VideoType>k__BackingField
	int32_t ___U3CVideoTypeU3Ek__BackingField_11;
	// System.String YoutubeLight.VideoInfo::<HtmlPlayerVersion>k__BackingField
	String_t* ___U3CHtmlPlayerVersionU3Ek__BackingField_12;
	// System.String YoutubeLight.VideoInfo::<HtmlscriptName>k__BackingField
	String_t* ___U3CHtmlscriptNameU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CAdaptiveTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CAdaptiveTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CAdaptiveTypeU3Ek__BackingField_1() const { return ___U3CAdaptiveTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CAdaptiveTypeU3Ek__BackingField_1() { return &___U3CAdaptiveTypeU3Ek__BackingField_1; }
	inline void set_U3CAdaptiveTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CAdaptiveTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CAudioBitrateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CAudioBitrateU3Ek__BackingField_2)); }
	inline int32_t get_U3CAudioBitrateU3Ek__BackingField_2() const { return ___U3CAudioBitrateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CAudioBitrateU3Ek__BackingField_2() { return &___U3CAudioBitrateU3Ek__BackingField_2; }
	inline void set_U3CAudioBitrateU3Ek__BackingField_2(int32_t value)
	{
		___U3CAudioBitrateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAudioTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CAudioTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CAudioTypeU3Ek__BackingField_3() const { return ___U3CAudioTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CAudioTypeU3Ek__BackingField_3() { return &___U3CAudioTypeU3Ek__BackingField_3; }
	inline void set_U3CAudioTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CAudioTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDownloadUrlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CDownloadUrlU3Ek__BackingField_4)); }
	inline String_t* get_U3CDownloadUrlU3Ek__BackingField_4() const { return ___U3CDownloadUrlU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDownloadUrlU3Ek__BackingField_4() { return &___U3CDownloadUrlU3Ek__BackingField_4; }
	inline void set_U3CDownloadUrlU3Ek__BackingField_4(String_t* value)
	{
		___U3CDownloadUrlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDownloadUrlU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CFormatCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CFormatCodeU3Ek__BackingField_5)); }
	inline int32_t get_U3CFormatCodeU3Ek__BackingField_5() const { return ___U3CFormatCodeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CFormatCodeU3Ek__BackingField_5() { return &___U3CFormatCodeU3Ek__BackingField_5; }
	inline void set_U3CFormatCodeU3Ek__BackingField_5(int32_t value)
	{
		___U3CFormatCodeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIs3DU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CIs3DU3Ek__BackingField_6)); }
	inline bool get_U3CIs3DU3Ek__BackingField_6() const { return ___U3CIs3DU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIs3DU3Ek__BackingField_6() { return &___U3CIs3DU3Ek__BackingField_6; }
	inline void set_U3CIs3DU3Ek__BackingField_6(bool value)
	{
		___U3CIs3DU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CHDRU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CHDRU3Ek__BackingField_7)); }
	inline bool get_U3CHDRU3Ek__BackingField_7() const { return ___U3CHDRU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CHDRU3Ek__BackingField_7() { return &___U3CHDRU3Ek__BackingField_7; }
	inline void set_U3CHDRU3Ek__BackingField_7(bool value)
	{
		___U3CHDRU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CRequiresDecryptionU3Ek__BackingField_8)); }
	inline bool get_U3CRequiresDecryptionU3Ek__BackingField_8() const { return ___U3CRequiresDecryptionU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CRequiresDecryptionU3Ek__BackingField_8() { return &___U3CRequiresDecryptionU3Ek__BackingField_8; }
	inline void set_U3CRequiresDecryptionU3Ek__BackingField_8(bool value)
	{
		___U3CRequiresDecryptionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CResolutionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CResolutionU3Ek__BackingField_9)); }
	inline int32_t get_U3CResolutionU3Ek__BackingField_9() const { return ___U3CResolutionU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CResolutionU3Ek__BackingField_9() { return &___U3CResolutionU3Ek__BackingField_9; }
	inline void set_U3CResolutionU3Ek__BackingField_9(int32_t value)
	{
		___U3CResolutionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CTitleU3Ek__BackingField_10)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_10() const { return ___U3CTitleU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_10() { return &___U3CTitleU3Ek__BackingField_10; }
	inline void set_U3CTitleU3Ek__BackingField_10(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CVideoTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CVideoTypeU3Ek__BackingField_11)); }
	inline int32_t get_U3CVideoTypeU3Ek__BackingField_11() const { return ___U3CVideoTypeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CVideoTypeU3Ek__BackingField_11() { return &___U3CVideoTypeU3Ek__BackingField_11; }
	inline void set_U3CVideoTypeU3Ek__BackingField_11(int32_t value)
	{
		___U3CVideoTypeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CHtmlPlayerVersionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CHtmlPlayerVersionU3Ek__BackingField_12)); }
	inline String_t* get_U3CHtmlPlayerVersionU3Ek__BackingField_12() const { return ___U3CHtmlPlayerVersionU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CHtmlPlayerVersionU3Ek__BackingField_12() { return &___U3CHtmlPlayerVersionU3Ek__BackingField_12; }
	inline void set_U3CHtmlPlayerVersionU3Ek__BackingField_12(String_t* value)
	{
		___U3CHtmlPlayerVersionU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHtmlPlayerVersionU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CHtmlscriptNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791, ___U3CHtmlscriptNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CHtmlscriptNameU3Ek__BackingField_13() const { return ___U3CHtmlscriptNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CHtmlscriptNameU3Ek__BackingField_13() { return &___U3CHtmlscriptNameU3Ek__BackingField_13; }
	inline void set_U3CHtmlscriptNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CHtmlscriptNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHtmlscriptNameU3Ek__BackingField_13), value);
	}
};

struct VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_StaticFields
{
public:
	// System.Collections.Generic.IEnumerable`1<YoutubeLight.VideoInfo> YoutubeLight.VideoInfo::Defaults
	RuntimeObject* ___Defaults_0;

public:
	inline static int32_t get_offset_of_Defaults_0() { return static_cast<int32_t>(offsetof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_StaticFields, ___Defaults_0)); }
	inline RuntimeObject* get_Defaults_0() const { return ___Defaults_0; }
	inline RuntimeObject** get_address_of_Defaults_0() { return &___Defaults_0; }
	inline void set_Defaults_0(RuntimeObject* value)
	{
		___Defaults_0 = value;
		Il2CppCodeGenWriteBarrier((&___Defaults_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOINFO_T29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_H
#ifndef DEFAULTSSETDELEGATE_TFA7B425710CE014D62560A1262F5E6577871406E_H
#define DEFAULTSSETDELEGATE_TFA7B425710CE014D62560A1262F5E6577871406E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_DefaultsSetDelegate
struct  DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSSETDELEGATE_TFA7B425710CE014D62560A1262F5E6577871406E_H
#ifndef PATHCHANGEDELEGATE_TFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734_H
#define PATHCHANGEDELEGATE_TFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_PathChangeDelegate
struct  PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCHANGEDELEGATE_TFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734_H
#ifndef SCREENCHANGE_TE53DBA009CFC24E445F8EEF6154D4E11B9ACB207_H
#define SCREENCHANGE_TE53DBA009CFC24E445F8EEF6154D4E11B9ACB207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper_ScreenChange
struct  ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENCHANGE_TE53DBA009CFC24E445F8EEF6154D4E11B9ACB207_H
#ifndef KEYCODERECEIVEDDELEGATE_TA8930CEAF69FA713516FC48F6899262B9E8E1C6C_H
#define KEYCODERECEIVEDDELEGATE_TA8930CEAF69FA713516FC48F6899262B9E8E1C6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_KeyCodeReceivedDelegate
struct  KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODERECEIVEDDELEGATE_TA8930CEAF69FA713516FC48F6899262B9E8E1C6C_H
#ifndef MESSAGERECEIVEDDELEGATE_T30F573625B3F4C806D9073C9FB6773DC92922582_H
#define MESSAGERECEIVEDDELEGATE_T30F573625B3F4C806D9073C9FB6773DC92922582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_MessageReceivedDelegate
struct  MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERECEIVEDDELEGATE_T30F573625B3F4C806D9073C9FB6773DC92922582_H
#ifndef ONWEBCONTENTPROCESSTERMINATEDDELEGATE_TAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD_H
#define ONWEBCONTENTPROCESSTERMINATEDDELEGATE_TAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_OnWebContentProcessTerminatedDelegate
struct  OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBCONTENTPROCESSTERMINATEDDELEGATE_TAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD_H
#ifndef OREINTATIONCHANGEDDELEGATE_T42B38D7E92042A4E7C93015C2C675A483D7238E7_H
#define OREINTATIONCHANGEDDELEGATE_T42B38D7E92042A4E7C93015C2C675A483D7238E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_OreintationChangedDelegate
struct  OreintationChangedDelegate_t42B38D7E92042A4E7C93015C2C675A483D7238E7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OREINTATIONCHANGEDDELEGATE_T42B38D7E92042A4E7C93015C2C675A483D7238E7_H
#ifndef ORIENTATIONCHANGEDDELEGATE_T3D6957C9CC92E9341E30FCB84901049E0E8DA482_H
#define ORIENTATIONCHANGEDDELEGATE_T3D6957C9CC92E9341E30FCB84901049E0E8DA482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_OrientationChangedDelegate
struct  OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONCHANGEDDELEGATE_T3D6957C9CC92E9341E30FCB84901049E0E8DA482_H
#ifndef PAGEERRORRECEIVEDDELEGATE_T091963788F14CE6459BBB18AC29566778DE8CB4E_H
#define PAGEERRORRECEIVEDDELEGATE_T091963788F14CE6459BBB18AC29566778DE8CB4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_PageErrorReceivedDelegate
struct  PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEERRORRECEIVEDDELEGATE_T091963788F14CE6459BBB18AC29566778DE8CB4E_H
#ifndef PAGEFINISHEDDELEGATE_T86B321BA20AE98611CA5520C46D556970FF31702_H
#define PAGEFINISHEDDELEGATE_T86B321BA20AE98611CA5520C46D556970FF31702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_PageFinishedDelegate
struct  PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGEFINISHEDDELEGATE_T86B321BA20AE98611CA5520C46D556970FF31702_H
#ifndef PAGESTARTEDDELEGATE_T249FC70F5F447EFE3E8A30439569A96F05602E78_H
#define PAGESTARTEDDELEGATE_T249FC70F5F447EFE3E8A30439569A96F05602E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_PageStartedDelegate
struct  PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGESTARTEDDELEGATE_T249FC70F5F447EFE3E8A30439569A96F05602E78_H
#ifndef SHOULDCLOSEDELEGATE_T2E3349B793354B80E34E5AF4CB035C2370160998_H
#define SHOULDCLOSEDELEGATE_T2E3349B793354B80E34E5AF4CB035C2370160998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView_ShouldCloseDelegate
struct  ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOULDCLOSEDELEGATE_T2E3349B793354B80E34E5AF4CB035C2370160998_H
#ifndef UNITYSENDMESSAGEDELEGATE_T5D9F2A1448208AB6DB8BD0A4862C82C85C3F5D68_H
#define UNITYSENDMESSAGEDELEGATE_T5D9F2A1448208AB6DB8BD0A4862C82C85C3F5D68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewInterface_UnitySendMessageDelegate
struct  UnitySendMessageDelegate_t5D9F2A1448208AB6DB8BD0A4862C82C85C3F5D68  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSENDMESSAGEDELEGATE_T5D9F2A1448208AB6DB8BD0A4862C82C85C3F5D68_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DEMO_T9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23_H
#define DEMO_T9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Demo
struct  Demo_t9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ProgressBar Demo::Pb
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * ___Pb_4;
	// ProgressBarCircle Demo::PbC
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___PbC_5;

public:
	inline static int32_t get_offset_of_Pb_4() { return static_cast<int32_t>(offsetof(Demo_t9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23, ___Pb_4)); }
	inline ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * get_Pb_4() const { return ___Pb_4; }
	inline ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C ** get_address_of_Pb_4() { return &___Pb_4; }
	inline void set_Pb_4(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * value)
	{
		___Pb_4 = value;
		Il2CppCodeGenWriteBarrier((&___Pb_4), value);
	}

	inline static int32_t get_offset_of_PbC_5() { return static_cast<int32_t>(offsetof(Demo_t9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23, ___PbC_5)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_PbC_5() const { return ___PbC_5; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_PbC_5() { return &___PbC_5; }
	inline void set_PbC_5(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___PbC_5 = value;
		Il2CppCodeGenWriteBarrier((&___PbC_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMO_T9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23_H
#ifndef KEYPRESSEXAMPLE_T368A70B2AF38467D816EBC9FD40A7E394CD3AAF4_H
#define KEYPRESSEXAMPLE_T368A70B2AF38467D816EBC9FD40A7E394CD3AAF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.KeyPressExample
struct  KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// NG.ScreenshotHelper NG.KeyPressExample::screenshotHelper
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * ___screenshotHelper_4;
	// UnityEngine.KeyCode NG.KeyPressExample::keyToHold
	int32_t ___keyToHold_5;
	// UnityEngine.KeyCode NG.KeyPressExample::keyToPress
	int32_t ___keyToPress_6;

public:
	inline static int32_t get_offset_of_screenshotHelper_4() { return static_cast<int32_t>(offsetof(KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4, ___screenshotHelper_4)); }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * get_screenshotHelper_4() const { return ___screenshotHelper_4; }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 ** get_address_of_screenshotHelper_4() { return &___screenshotHelper_4; }
	inline void set_screenshotHelper_4(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * value)
	{
		___screenshotHelper_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenshotHelper_4), value);
	}

	inline static int32_t get_offset_of_keyToHold_5() { return static_cast<int32_t>(offsetof(KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4, ___keyToHold_5)); }
	inline int32_t get_keyToHold_5() const { return ___keyToHold_5; }
	inline int32_t* get_address_of_keyToHold_5() { return &___keyToHold_5; }
	inline void set_keyToHold_5(int32_t value)
	{
		___keyToHold_5 = value;
	}

	inline static int32_t get_offset_of_keyToPress_6() { return static_cast<int32_t>(offsetof(KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4, ___keyToPress_6)); }
	inline int32_t get_keyToPress_6() const { return ___keyToPress_6; }
	inline int32_t* get_address_of_keyToPress_6() { return &___keyToPress_6; }
	inline void set_keyToPress_6(int32_t value)
	{
		___keyToPress_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYPRESSEXAMPLE_T368A70B2AF38467D816EBC9FD40A7E394CD3AAF4_H
#ifndef SCREENSHOTHELPER_T64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_H
#define SCREENSHOTHELPER_T64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NG.ScreenshotHelper
struct  ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// NG.ScreenshotHelper_ScreenChange NG.ScreenshotHelper::OnScreenChanged
	ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207 * ___OnScreenChanged_4;
	// System.Collections.Generic.Queue`1<System.Action> NG.ScreenshotHelper::unityThreadQueue
	Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * ___unityThreadQueue_5;
	// System.Action NG.ScreenshotHelper::OnComplete
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnComplete_6;
	// System.Boolean NG.ScreenshotHelper::useRenderTexture
	bool ___useRenderTexture_7;
	// NG.ScreenshotHelper_tSSHTextureFormat NG.ScreenshotHelper::SSHTextureFormat
	int32_t ___SSHTextureFormat_8;
	// UnityEngine.Camera[] NG.ScreenshotHelper::cameras
	CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* ___cameras_9;
	// System.String NG.ScreenshotHelper::fileNamePrefix
	String_t* ___fileNamePrefix_10;
	// System.Boolean NG.ScreenshotHelper::openFolderWhenDone
	bool ___openFolderWhenDone_11;
	// System.Boolean NG.ScreenshotHelper::didPressEditorScreenshotButton
	bool ___didPressEditorScreenshotButton_12;
	// NG.SSHOrientation NG.ScreenshotHelper::orientation
	int32_t ___orientation_13;
	// System.Collections.Generic.List`1<NG.ShotSize> NG.ScreenshotHelper::shotInfo
	List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * ___shotInfo_14;
	// NG.ShotSize NG.ScreenshotHelper::maxRes
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * ___maxRes_15;
	// System.String NG.ScreenshotHelper::savePath
	String_t* ___savePath_16;
	// System.Environment_SpecialFolder NG.ScreenshotHelper::buildSavePathRoot
	int32_t ___buildSavePathRoot_17;
	// System.String NG.ScreenshotHelper::buildSavePathExtra
	String_t* ___buildSavePathExtra_18;
	// System.String NG.ScreenshotHelper::configFile
	String_t* ___configFile_19;
	// NG.ScreenshotHelper_PathChangeDelegate NG.ScreenshotHelper::PathChange
	PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734 * ___PathChange_21;
	// NG.ShotSize NG.ScreenshotHelper::initialRes
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * ___initialRes_22;
	// System.Int32 NG.ScreenshotHelper::shotCounter
	int32_t ___shotCounter_23;
	// System.Boolean NG.ScreenshotHelper::<IsTakingShots>k__BackingField
	bool ___U3CIsTakingShotsU3Ek__BackingField_24;
	// NG.ScreenshotHelper_DefaultsSetDelegate NG.ScreenshotHelper::DefaultsSet
	DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E * ___DefaultsSet_25;

public:
	inline static int32_t get_offset_of_OnScreenChanged_4() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___OnScreenChanged_4)); }
	inline ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207 * get_OnScreenChanged_4() const { return ___OnScreenChanged_4; }
	inline ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207 ** get_address_of_OnScreenChanged_4() { return &___OnScreenChanged_4; }
	inline void set_OnScreenChanged_4(ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207 * value)
	{
		___OnScreenChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnScreenChanged_4), value);
	}

	inline static int32_t get_offset_of_unityThreadQueue_5() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___unityThreadQueue_5)); }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * get_unityThreadQueue_5() const { return ___unityThreadQueue_5; }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA ** get_address_of_unityThreadQueue_5() { return &___unityThreadQueue_5; }
	inline void set_unityThreadQueue_5(Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * value)
	{
		___unityThreadQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___unityThreadQueue_5), value);
	}

	inline static int32_t get_offset_of_OnComplete_6() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___OnComplete_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnComplete_6() const { return ___OnComplete_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnComplete_6() { return &___OnComplete_6; }
	inline void set_OnComplete_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_6), value);
	}

	inline static int32_t get_offset_of_useRenderTexture_7() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___useRenderTexture_7)); }
	inline bool get_useRenderTexture_7() const { return ___useRenderTexture_7; }
	inline bool* get_address_of_useRenderTexture_7() { return &___useRenderTexture_7; }
	inline void set_useRenderTexture_7(bool value)
	{
		___useRenderTexture_7 = value;
	}

	inline static int32_t get_offset_of_SSHTextureFormat_8() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___SSHTextureFormat_8)); }
	inline int32_t get_SSHTextureFormat_8() const { return ___SSHTextureFormat_8; }
	inline int32_t* get_address_of_SSHTextureFormat_8() { return &___SSHTextureFormat_8; }
	inline void set_SSHTextureFormat_8(int32_t value)
	{
		___SSHTextureFormat_8 = value;
	}

	inline static int32_t get_offset_of_cameras_9() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___cameras_9)); }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* get_cameras_9() const { return ___cameras_9; }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9** get_address_of_cameras_9() { return &___cameras_9; }
	inline void set_cameras_9(CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* value)
	{
		___cameras_9 = value;
		Il2CppCodeGenWriteBarrier((&___cameras_9), value);
	}

	inline static int32_t get_offset_of_fileNamePrefix_10() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___fileNamePrefix_10)); }
	inline String_t* get_fileNamePrefix_10() const { return ___fileNamePrefix_10; }
	inline String_t** get_address_of_fileNamePrefix_10() { return &___fileNamePrefix_10; }
	inline void set_fileNamePrefix_10(String_t* value)
	{
		___fileNamePrefix_10 = value;
		Il2CppCodeGenWriteBarrier((&___fileNamePrefix_10), value);
	}

	inline static int32_t get_offset_of_openFolderWhenDone_11() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___openFolderWhenDone_11)); }
	inline bool get_openFolderWhenDone_11() const { return ___openFolderWhenDone_11; }
	inline bool* get_address_of_openFolderWhenDone_11() { return &___openFolderWhenDone_11; }
	inline void set_openFolderWhenDone_11(bool value)
	{
		___openFolderWhenDone_11 = value;
	}

	inline static int32_t get_offset_of_didPressEditorScreenshotButton_12() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___didPressEditorScreenshotButton_12)); }
	inline bool get_didPressEditorScreenshotButton_12() const { return ___didPressEditorScreenshotButton_12; }
	inline bool* get_address_of_didPressEditorScreenshotButton_12() { return &___didPressEditorScreenshotButton_12; }
	inline void set_didPressEditorScreenshotButton_12(bool value)
	{
		___didPressEditorScreenshotButton_12 = value;
	}

	inline static int32_t get_offset_of_orientation_13() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___orientation_13)); }
	inline int32_t get_orientation_13() const { return ___orientation_13; }
	inline int32_t* get_address_of_orientation_13() { return &___orientation_13; }
	inline void set_orientation_13(int32_t value)
	{
		___orientation_13 = value;
	}

	inline static int32_t get_offset_of_shotInfo_14() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___shotInfo_14)); }
	inline List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * get_shotInfo_14() const { return ___shotInfo_14; }
	inline List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 ** get_address_of_shotInfo_14() { return &___shotInfo_14; }
	inline void set_shotInfo_14(List_1_t02EC3AA985E1C128F6AA5CE890D915B8BFC79F56 * value)
	{
		___shotInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___shotInfo_14), value);
	}

	inline static int32_t get_offset_of_maxRes_15() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___maxRes_15)); }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * get_maxRes_15() const { return ___maxRes_15; }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 ** get_address_of_maxRes_15() { return &___maxRes_15; }
	inline void set_maxRes_15(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * value)
	{
		___maxRes_15 = value;
		Il2CppCodeGenWriteBarrier((&___maxRes_15), value);
	}

	inline static int32_t get_offset_of_savePath_16() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___savePath_16)); }
	inline String_t* get_savePath_16() const { return ___savePath_16; }
	inline String_t** get_address_of_savePath_16() { return &___savePath_16; }
	inline void set_savePath_16(String_t* value)
	{
		___savePath_16 = value;
		Il2CppCodeGenWriteBarrier((&___savePath_16), value);
	}

	inline static int32_t get_offset_of_buildSavePathRoot_17() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___buildSavePathRoot_17)); }
	inline int32_t get_buildSavePathRoot_17() const { return ___buildSavePathRoot_17; }
	inline int32_t* get_address_of_buildSavePathRoot_17() { return &___buildSavePathRoot_17; }
	inline void set_buildSavePathRoot_17(int32_t value)
	{
		___buildSavePathRoot_17 = value;
	}

	inline static int32_t get_offset_of_buildSavePathExtra_18() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___buildSavePathExtra_18)); }
	inline String_t* get_buildSavePathExtra_18() const { return ___buildSavePathExtra_18; }
	inline String_t** get_address_of_buildSavePathExtra_18() { return &___buildSavePathExtra_18; }
	inline void set_buildSavePathExtra_18(String_t* value)
	{
		___buildSavePathExtra_18 = value;
		Il2CppCodeGenWriteBarrier((&___buildSavePathExtra_18), value);
	}

	inline static int32_t get_offset_of_configFile_19() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___configFile_19)); }
	inline String_t* get_configFile_19() const { return ___configFile_19; }
	inline String_t** get_address_of_configFile_19() { return &___configFile_19; }
	inline void set_configFile_19(String_t* value)
	{
		___configFile_19 = value;
		Il2CppCodeGenWriteBarrier((&___configFile_19), value);
	}

	inline static int32_t get_offset_of_PathChange_21() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___PathChange_21)); }
	inline PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734 * get_PathChange_21() const { return ___PathChange_21; }
	inline PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734 ** get_address_of_PathChange_21() { return &___PathChange_21; }
	inline void set_PathChange_21(PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734 * value)
	{
		___PathChange_21 = value;
		Il2CppCodeGenWriteBarrier((&___PathChange_21), value);
	}

	inline static int32_t get_offset_of_initialRes_22() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___initialRes_22)); }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * get_initialRes_22() const { return ___initialRes_22; }
	inline ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 ** get_address_of_initialRes_22() { return &___initialRes_22; }
	inline void set_initialRes_22(ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83 * value)
	{
		___initialRes_22 = value;
		Il2CppCodeGenWriteBarrier((&___initialRes_22), value);
	}

	inline static int32_t get_offset_of_shotCounter_23() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___shotCounter_23)); }
	inline int32_t get_shotCounter_23() const { return ___shotCounter_23; }
	inline int32_t* get_address_of_shotCounter_23() { return &___shotCounter_23; }
	inline void set_shotCounter_23(int32_t value)
	{
		___shotCounter_23 = value;
	}

	inline static int32_t get_offset_of_U3CIsTakingShotsU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___U3CIsTakingShotsU3Ek__BackingField_24)); }
	inline bool get_U3CIsTakingShotsU3Ek__BackingField_24() const { return ___U3CIsTakingShotsU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CIsTakingShotsU3Ek__BackingField_24() { return &___U3CIsTakingShotsU3Ek__BackingField_24; }
	inline void set_U3CIsTakingShotsU3Ek__BackingField_24(bool value)
	{
		___U3CIsTakingShotsU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_DefaultsSet_25() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7, ___DefaultsSet_25)); }
	inline DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E * get_DefaultsSet_25() const { return ___DefaultsSet_25; }
	inline DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E ** get_address_of_DefaultsSet_25() { return &___DefaultsSet_25; }
	inline void set_DefaultsSet_25(DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E * value)
	{
		___DefaultsSet_25 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultsSet_25), value);
	}
};

struct ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_StaticFields
{
public:
	// NG.ScreenshotHelper NG.ScreenshotHelper::_instance
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * ____instance_20;

public:
	inline static int32_t get_offset_of__instance_20() { return static_cast<int32_t>(offsetof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_StaticFields, ____instance_20)); }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * get__instance_20() const { return ____instance_20; }
	inline ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 ** get_address_of__instance_20() { return &____instance_20; }
	inline void set__instance_20(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7 * value)
	{
		____instance_20 = value;
		Il2CppCodeGenWriteBarrier((&____instance_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOTHELPER_T64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_H
#ifndef PROGRESSBAR_TAEBA7E1A59C678A8651AEF196DDD334625C6164C_H
#define PROGRESSBAR_TAEBA7E1A59C678A8651AEF196DDD334625C6164C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar
struct  ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String ProgressBar::Title
	String_t* ___Title_4;
	// UnityEngine.Color ProgressBar::TitleColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___TitleColor_5;
	// UnityEngine.Font ProgressBar::TitleFont
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___TitleFont_6;
	// System.Int32 ProgressBar::TitleFontSize
	int32_t ___TitleFontSize_7;
	// UnityEngine.Color ProgressBar::BarColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BarColor_8;
	// UnityEngine.Color ProgressBar::BarBackGroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BarBackGroundColor_9;
	// UnityEngine.Sprite ProgressBar::BarBackGroundSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___BarBackGroundSprite_10;
	// System.Int32 ProgressBar::Alert
	int32_t ___Alert_11;
	// UnityEngine.Color ProgressBar::BarAlertColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BarAlertColor_12;
	// UnityEngine.AudioClip ProgressBar::sound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sound_13;
	// System.Boolean ProgressBar::repeat
	bool ___repeat_14;
	// System.Single ProgressBar::RepeatRate
	float ___RepeatRate_15;
	// UnityEngine.UI.Image ProgressBar::bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___bar_16;
	// UnityEngine.UI.Image ProgressBar::barBackground
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___barBackground_17;
	// System.Single ProgressBar::nextPlay
	float ___nextPlay_18;
	// UnityEngine.AudioSource ProgressBar::audiosource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audiosource_19;
	// UnityEngine.UI.Text ProgressBar::txtTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___txtTitle_20;
	// System.Single ProgressBar::barValue
	float ___barValue_21;

public:
	inline static int32_t get_offset_of_Title_4() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___Title_4)); }
	inline String_t* get_Title_4() const { return ___Title_4; }
	inline String_t** get_address_of_Title_4() { return &___Title_4; }
	inline void set_Title_4(String_t* value)
	{
		___Title_4 = value;
		Il2CppCodeGenWriteBarrier((&___Title_4), value);
	}

	inline static int32_t get_offset_of_TitleColor_5() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___TitleColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_TitleColor_5() const { return ___TitleColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_TitleColor_5() { return &___TitleColor_5; }
	inline void set_TitleColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___TitleColor_5 = value;
	}

	inline static int32_t get_offset_of_TitleFont_6() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___TitleFont_6)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_TitleFont_6() const { return ___TitleFont_6; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_TitleFont_6() { return &___TitleFont_6; }
	inline void set_TitleFont_6(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___TitleFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TitleFont_6), value);
	}

	inline static int32_t get_offset_of_TitleFontSize_7() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___TitleFontSize_7)); }
	inline int32_t get_TitleFontSize_7() const { return ___TitleFontSize_7; }
	inline int32_t* get_address_of_TitleFontSize_7() { return &___TitleFontSize_7; }
	inline void set_TitleFontSize_7(int32_t value)
	{
		___TitleFontSize_7 = value;
	}

	inline static int32_t get_offset_of_BarColor_8() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___BarColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BarColor_8() const { return ___BarColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BarColor_8() { return &___BarColor_8; }
	inline void set_BarColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BarColor_8 = value;
	}

	inline static int32_t get_offset_of_BarBackGroundColor_9() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___BarBackGroundColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BarBackGroundColor_9() const { return ___BarBackGroundColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BarBackGroundColor_9() { return &___BarBackGroundColor_9; }
	inline void set_BarBackGroundColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BarBackGroundColor_9 = value;
	}

	inline static int32_t get_offset_of_BarBackGroundSprite_10() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___BarBackGroundSprite_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_BarBackGroundSprite_10() const { return ___BarBackGroundSprite_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_BarBackGroundSprite_10() { return &___BarBackGroundSprite_10; }
	inline void set_BarBackGroundSprite_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___BarBackGroundSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___BarBackGroundSprite_10), value);
	}

	inline static int32_t get_offset_of_Alert_11() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___Alert_11)); }
	inline int32_t get_Alert_11() const { return ___Alert_11; }
	inline int32_t* get_address_of_Alert_11() { return &___Alert_11; }
	inline void set_Alert_11(int32_t value)
	{
		___Alert_11 = value;
	}

	inline static int32_t get_offset_of_BarAlertColor_12() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___BarAlertColor_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BarAlertColor_12() const { return ___BarAlertColor_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BarAlertColor_12() { return &___BarAlertColor_12; }
	inline void set_BarAlertColor_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BarAlertColor_12 = value;
	}

	inline static int32_t get_offset_of_sound_13() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___sound_13)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sound_13() const { return ___sound_13; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sound_13() { return &___sound_13; }
	inline void set_sound_13(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sound_13 = value;
		Il2CppCodeGenWriteBarrier((&___sound_13), value);
	}

	inline static int32_t get_offset_of_repeat_14() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___repeat_14)); }
	inline bool get_repeat_14() const { return ___repeat_14; }
	inline bool* get_address_of_repeat_14() { return &___repeat_14; }
	inline void set_repeat_14(bool value)
	{
		___repeat_14 = value;
	}

	inline static int32_t get_offset_of_RepeatRate_15() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___RepeatRate_15)); }
	inline float get_RepeatRate_15() const { return ___RepeatRate_15; }
	inline float* get_address_of_RepeatRate_15() { return &___RepeatRate_15; }
	inline void set_RepeatRate_15(float value)
	{
		___RepeatRate_15 = value;
	}

	inline static int32_t get_offset_of_bar_16() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___bar_16)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_bar_16() const { return ___bar_16; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_bar_16() { return &___bar_16; }
	inline void set_bar_16(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___bar_16 = value;
		Il2CppCodeGenWriteBarrier((&___bar_16), value);
	}

	inline static int32_t get_offset_of_barBackground_17() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___barBackground_17)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_barBackground_17() const { return ___barBackground_17; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_barBackground_17() { return &___barBackground_17; }
	inline void set_barBackground_17(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___barBackground_17 = value;
		Il2CppCodeGenWriteBarrier((&___barBackground_17), value);
	}

	inline static int32_t get_offset_of_nextPlay_18() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___nextPlay_18)); }
	inline float get_nextPlay_18() const { return ___nextPlay_18; }
	inline float* get_address_of_nextPlay_18() { return &___nextPlay_18; }
	inline void set_nextPlay_18(float value)
	{
		___nextPlay_18 = value;
	}

	inline static int32_t get_offset_of_audiosource_19() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___audiosource_19)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audiosource_19() const { return ___audiosource_19; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audiosource_19() { return &___audiosource_19; }
	inline void set_audiosource_19(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audiosource_19 = value;
		Il2CppCodeGenWriteBarrier((&___audiosource_19), value);
	}

	inline static int32_t get_offset_of_txtTitle_20() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___txtTitle_20)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_txtTitle_20() const { return ___txtTitle_20; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_txtTitle_20() { return &___txtTitle_20; }
	inline void set_txtTitle_20(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___txtTitle_20 = value;
		Il2CppCodeGenWriteBarrier((&___txtTitle_20), value);
	}

	inline static int32_t get_offset_of_barValue_21() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ___barValue_21)); }
	inline float get_barValue_21() const { return ___barValue_21; }
	inline float* get_address_of_barValue_21() { return &___barValue_21; }
	inline void set_barValue_21(float value)
	{
		___barValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBAR_TAEBA7E1A59C678A8651AEF196DDD334625C6164C_H
#ifndef PROGRESSBARCIRCLE_T5949285E73BE4AF5E381E1C9226F7094A8649946_H
#define PROGRESSBARCIRCLE_T5949285E73BE4AF5E381E1C9226F7094A8649946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBarCircle
struct  ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String ProgressBarCircle::Title
	String_t* ___Title_4;
	// UnityEngine.Color ProgressBarCircle::TitleColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___TitleColor_5;
	// UnityEngine.Font ProgressBarCircle::TitleFont
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___TitleFont_6;
	// UnityEngine.Color ProgressBarCircle::BarColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BarColor_7;
	// UnityEngine.Color ProgressBarCircle::BarBackGroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BarBackGroundColor_8;
	// UnityEngine.Color ProgressBarCircle::MaskColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___MaskColor_9;
	// UnityEngine.Sprite ProgressBarCircle::BarBackGroundSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___BarBackGroundSprite_10;
	// System.Int32 ProgressBarCircle::Alert
	int32_t ___Alert_11;
	// UnityEngine.Color ProgressBarCircle::BarAlertColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___BarAlertColor_12;
	// UnityEngine.AudioClip ProgressBarCircle::sound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sound_13;
	// System.Boolean ProgressBarCircle::repeat
	bool ___repeat_14;
	// System.Single ProgressBarCircle::RepearRate
	float ___RepearRate_15;
	// UnityEngine.UI.Image ProgressBarCircle::bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___bar_16;
	// UnityEngine.UI.Image ProgressBarCircle::barBackground
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___barBackground_17;
	// UnityEngine.UI.Image ProgressBarCircle::Mask
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Mask_18;
	// System.Single ProgressBarCircle::nextPlay
	float ___nextPlay_19;
	// UnityEngine.AudioSource ProgressBarCircle::audiosource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audiosource_20;
	// UnityEngine.UI.Text ProgressBarCircle::txtTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___txtTitle_21;
	// System.Single ProgressBarCircle::barValue
	float ___barValue_22;

public:
	inline static int32_t get_offset_of_Title_4() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___Title_4)); }
	inline String_t* get_Title_4() const { return ___Title_4; }
	inline String_t** get_address_of_Title_4() { return &___Title_4; }
	inline void set_Title_4(String_t* value)
	{
		___Title_4 = value;
		Il2CppCodeGenWriteBarrier((&___Title_4), value);
	}

	inline static int32_t get_offset_of_TitleColor_5() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___TitleColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_TitleColor_5() const { return ___TitleColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_TitleColor_5() { return &___TitleColor_5; }
	inline void set_TitleColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___TitleColor_5 = value;
	}

	inline static int32_t get_offset_of_TitleFont_6() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___TitleFont_6)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_TitleFont_6() const { return ___TitleFont_6; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_TitleFont_6() { return &___TitleFont_6; }
	inline void set_TitleFont_6(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___TitleFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TitleFont_6), value);
	}

	inline static int32_t get_offset_of_BarColor_7() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___BarColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BarColor_7() const { return ___BarColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BarColor_7() { return &___BarColor_7; }
	inline void set_BarColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BarColor_7 = value;
	}

	inline static int32_t get_offset_of_BarBackGroundColor_8() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___BarBackGroundColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BarBackGroundColor_8() const { return ___BarBackGroundColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BarBackGroundColor_8() { return &___BarBackGroundColor_8; }
	inline void set_BarBackGroundColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BarBackGroundColor_8 = value;
	}

	inline static int32_t get_offset_of_MaskColor_9() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___MaskColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_MaskColor_9() const { return ___MaskColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_MaskColor_9() { return &___MaskColor_9; }
	inline void set_MaskColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___MaskColor_9 = value;
	}

	inline static int32_t get_offset_of_BarBackGroundSprite_10() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___BarBackGroundSprite_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_BarBackGroundSprite_10() const { return ___BarBackGroundSprite_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_BarBackGroundSprite_10() { return &___BarBackGroundSprite_10; }
	inline void set_BarBackGroundSprite_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___BarBackGroundSprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___BarBackGroundSprite_10), value);
	}

	inline static int32_t get_offset_of_Alert_11() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___Alert_11)); }
	inline int32_t get_Alert_11() const { return ___Alert_11; }
	inline int32_t* get_address_of_Alert_11() { return &___Alert_11; }
	inline void set_Alert_11(int32_t value)
	{
		___Alert_11 = value;
	}

	inline static int32_t get_offset_of_BarAlertColor_12() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___BarAlertColor_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_BarAlertColor_12() const { return ___BarAlertColor_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_BarAlertColor_12() { return &___BarAlertColor_12; }
	inline void set_BarAlertColor_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___BarAlertColor_12 = value;
	}

	inline static int32_t get_offset_of_sound_13() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___sound_13)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sound_13() const { return ___sound_13; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sound_13() { return &___sound_13; }
	inline void set_sound_13(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sound_13 = value;
		Il2CppCodeGenWriteBarrier((&___sound_13), value);
	}

	inline static int32_t get_offset_of_repeat_14() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___repeat_14)); }
	inline bool get_repeat_14() const { return ___repeat_14; }
	inline bool* get_address_of_repeat_14() { return &___repeat_14; }
	inline void set_repeat_14(bool value)
	{
		___repeat_14 = value;
	}

	inline static int32_t get_offset_of_RepearRate_15() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___RepearRate_15)); }
	inline float get_RepearRate_15() const { return ___RepearRate_15; }
	inline float* get_address_of_RepearRate_15() { return &___RepearRate_15; }
	inline void set_RepearRate_15(float value)
	{
		___RepearRate_15 = value;
	}

	inline static int32_t get_offset_of_bar_16() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___bar_16)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_bar_16() const { return ___bar_16; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_bar_16() { return &___bar_16; }
	inline void set_bar_16(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___bar_16 = value;
		Il2CppCodeGenWriteBarrier((&___bar_16), value);
	}

	inline static int32_t get_offset_of_barBackground_17() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___barBackground_17)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_barBackground_17() const { return ___barBackground_17; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_barBackground_17() { return &___barBackground_17; }
	inline void set_barBackground_17(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___barBackground_17 = value;
		Il2CppCodeGenWriteBarrier((&___barBackground_17), value);
	}

	inline static int32_t get_offset_of_Mask_18() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___Mask_18)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Mask_18() const { return ___Mask_18; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Mask_18() { return &___Mask_18; }
	inline void set_Mask_18(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Mask_18 = value;
		Il2CppCodeGenWriteBarrier((&___Mask_18), value);
	}

	inline static int32_t get_offset_of_nextPlay_19() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___nextPlay_19)); }
	inline float get_nextPlay_19() const { return ___nextPlay_19; }
	inline float* get_address_of_nextPlay_19() { return &___nextPlay_19; }
	inline void set_nextPlay_19(float value)
	{
		___nextPlay_19 = value;
	}

	inline static int32_t get_offset_of_audiosource_20() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___audiosource_20)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audiosource_20() const { return ___audiosource_20; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audiosource_20() { return &___audiosource_20; }
	inline void set_audiosource_20(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audiosource_20 = value;
		Il2CppCodeGenWriteBarrier((&___audiosource_20), value);
	}

	inline static int32_t get_offset_of_txtTitle_21() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___txtTitle_21)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_txtTitle_21() const { return ___txtTitle_21; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_txtTitle_21() { return &___txtTitle_21; }
	inline void set_txtTitle_21(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___txtTitle_21 = value;
		Il2CppCodeGenWriteBarrier((&___txtTitle_21), value);
	}

	inline static int32_t get_offset_of_barValue_22() { return static_cast<int32_t>(offsetof(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946, ___barValue_22)); }
	inline float get_barValue_22() const { return ___barValue_22; }
	inline float* get_address_of_barValue_22() { return &___barValue_22; }
	inline void set_barValue_22(float value)
	{
		___barValue_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBARCIRCLE_T5949285E73BE4AF5E381E1C9226F7094A8649946_H
#ifndef TEXTSCROLLER_T8FECA395DC5E69F2222569AA6E4DB36A830797CC_H
#define TEXTSCROLLER_T8FECA395DC5E69F2222569AA6E4DB36A830797CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextScroller
struct  TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TextScroller::scrollSpeed
	float ___scrollSpeed_4;
	// System.Boolean TextScroller::scrolling
	bool ___scrolling_5;
	// System.Single TextScroller::stopScrollPositionY
	float ___stopScrollPositionY_6;
	// System.Single TextScroller::startScrollPositionY
	float ___startScrollPositionY_7;
	// System.Single TextScroller::restartTimeMarker
	float ___restartTimeMarker_8;
	// System.Boolean TextScroller::stopWithinView
	bool ___stopWithinView_9;
	// UnityEngine.Vector2 TextScroller::startPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPosition_10;
	// System.Single TextScroller::previousFloat
	float ___previousFloat_11;

public:
	inline static int32_t get_offset_of_scrollSpeed_4() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___scrollSpeed_4)); }
	inline float get_scrollSpeed_4() const { return ___scrollSpeed_4; }
	inline float* get_address_of_scrollSpeed_4() { return &___scrollSpeed_4; }
	inline void set_scrollSpeed_4(float value)
	{
		___scrollSpeed_4 = value;
	}

	inline static int32_t get_offset_of_scrolling_5() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___scrolling_5)); }
	inline bool get_scrolling_5() const { return ___scrolling_5; }
	inline bool* get_address_of_scrolling_5() { return &___scrolling_5; }
	inline void set_scrolling_5(bool value)
	{
		___scrolling_5 = value;
	}

	inline static int32_t get_offset_of_stopScrollPositionY_6() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___stopScrollPositionY_6)); }
	inline float get_stopScrollPositionY_6() const { return ___stopScrollPositionY_6; }
	inline float* get_address_of_stopScrollPositionY_6() { return &___stopScrollPositionY_6; }
	inline void set_stopScrollPositionY_6(float value)
	{
		___stopScrollPositionY_6 = value;
	}

	inline static int32_t get_offset_of_startScrollPositionY_7() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___startScrollPositionY_7)); }
	inline float get_startScrollPositionY_7() const { return ___startScrollPositionY_7; }
	inline float* get_address_of_startScrollPositionY_7() { return &___startScrollPositionY_7; }
	inline void set_startScrollPositionY_7(float value)
	{
		___startScrollPositionY_7 = value;
	}

	inline static int32_t get_offset_of_restartTimeMarker_8() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___restartTimeMarker_8)); }
	inline float get_restartTimeMarker_8() const { return ___restartTimeMarker_8; }
	inline float* get_address_of_restartTimeMarker_8() { return &___restartTimeMarker_8; }
	inline void set_restartTimeMarker_8(float value)
	{
		___restartTimeMarker_8 = value;
	}

	inline static int32_t get_offset_of_stopWithinView_9() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___stopWithinView_9)); }
	inline bool get_stopWithinView_9() const { return ___stopWithinView_9; }
	inline bool* get_address_of_stopWithinView_9() { return &___stopWithinView_9; }
	inline void set_stopWithinView_9(bool value)
	{
		___stopWithinView_9 = value;
	}

	inline static int32_t get_offset_of_startPosition_10() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___startPosition_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPosition_10() const { return ___startPosition_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPosition_10() { return &___startPosition_10; }
	inline void set_startPosition_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPosition_10 = value;
	}

	inline static int32_t get_offset_of_previousFloat_11() { return static_cast<int32_t>(offsetof(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC, ___previousFloat_11)); }
	inline float get_previousFloat_11() const { return ___previousFloat_11; }
	inline float* get_address_of_previousFloat_11() { return &___previousFloat_11; }
	inline void set_previousFloat_11(float value)
	{
		___previousFloat_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTSCROLLER_T8FECA395DC5E69F2222569AA6E4DB36A830797CC_H
#ifndef UNIWEBVIEW_T70C5C5815FC5A948185238830E0AB8B221E34BBB_H
#define UNIWEBVIEW_T70C5C5815FC5A948185238830E0AB8B221E34BBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebView
struct  UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UniWebView_PageStartedDelegate UniWebView::OnPageStarted
	PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78 * ___OnPageStarted_4;
	// UniWebView_PageFinishedDelegate UniWebView::OnPageFinished
	PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702 * ___OnPageFinished_5;
	// UniWebView_PageErrorReceivedDelegate UniWebView::OnPageErrorReceived
	PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E * ___OnPageErrorReceived_6;
	// UniWebView_MessageReceivedDelegate UniWebView::OnMessageReceived
	MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582 * ___OnMessageReceived_7;
	// UniWebView_ShouldCloseDelegate UniWebView::OnShouldClose
	ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998 * ___OnShouldClose_8;
	// UniWebView_KeyCodeReceivedDelegate UniWebView::OnKeyCodeReceived
	KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C * ___OnKeyCodeReceived_9;
	// UniWebView_OrientationChangedDelegate UniWebView::OnOrientationChanged
	OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 * ___OnOrientationChanged_10;
	// UniWebView_OnWebContentProcessTerminatedDelegate UniWebView::OnWebContentProcessTerminated
	OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD * ___OnWebContentProcessTerminated_11;
	// System.String UniWebView::id
	String_t* ___id_12;
	// UniWebViewNativeListener UniWebView::listener
	UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD * ___listener_13;
	// System.Boolean UniWebView::isPortrait
	bool ___isPortrait_14;
	// System.String UniWebView::urlOnStart
	String_t* ___urlOnStart_15;
	// System.Boolean UniWebView::showOnStart
	bool ___showOnStart_16;
	// System.Boolean UniWebView::fullScreen
	bool ___fullScreen_17;
	// System.Boolean UniWebView::useToolbar
	bool ___useToolbar_18;
	// UniWebViewToolbarPosition UniWebView::toolbarPosition
	int32_t ___toolbarPosition_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action> UniWebView::actions
	Dictionary_2_t6869420B5ACFF0569A65586D45920B0F608DDB45 * ___actions_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<UniWebViewNativeResultPayload>> UniWebView::payloadActions
	Dictionary_2_tF7EF1EBF77E39D94C275304B89431CE3F887E216 * ___payloadActions_21;
	// UnityEngine.Rect UniWebView::frame
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___frame_22;
	// UnityEngine.RectTransform UniWebView::referenceRectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___referenceRectTransform_23;
	// System.Boolean UniWebView::started
	bool ___started_24;
	// UnityEngine.Color UniWebView::backgroundColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___backgroundColor_25;
	// UniWebView_OrientationChangedDelegate UniWebView::OnOreintationChanged
	OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 * ___OnOreintationChanged_26;

public:
	inline static int32_t get_offset_of_OnPageStarted_4() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnPageStarted_4)); }
	inline PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78 * get_OnPageStarted_4() const { return ___OnPageStarted_4; }
	inline PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78 ** get_address_of_OnPageStarted_4() { return &___OnPageStarted_4; }
	inline void set_OnPageStarted_4(PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78 * value)
	{
		___OnPageStarted_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPageStarted_4), value);
	}

	inline static int32_t get_offset_of_OnPageFinished_5() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnPageFinished_5)); }
	inline PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702 * get_OnPageFinished_5() const { return ___OnPageFinished_5; }
	inline PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702 ** get_address_of_OnPageFinished_5() { return &___OnPageFinished_5; }
	inline void set_OnPageFinished_5(PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702 * value)
	{
		___OnPageFinished_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPageFinished_5), value);
	}

	inline static int32_t get_offset_of_OnPageErrorReceived_6() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnPageErrorReceived_6)); }
	inline PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E * get_OnPageErrorReceived_6() const { return ___OnPageErrorReceived_6; }
	inline PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E ** get_address_of_OnPageErrorReceived_6() { return &___OnPageErrorReceived_6; }
	inline void set_OnPageErrorReceived_6(PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E * value)
	{
		___OnPageErrorReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnPageErrorReceived_6), value);
	}

	inline static int32_t get_offset_of_OnMessageReceived_7() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnMessageReceived_7)); }
	inline MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582 * get_OnMessageReceived_7() const { return ___OnMessageReceived_7; }
	inline MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582 ** get_address_of_OnMessageReceived_7() { return &___OnMessageReceived_7; }
	inline void set_OnMessageReceived_7(MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582 * value)
	{
		___OnMessageReceived_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessageReceived_7), value);
	}

	inline static int32_t get_offset_of_OnShouldClose_8() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnShouldClose_8)); }
	inline ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998 * get_OnShouldClose_8() const { return ___OnShouldClose_8; }
	inline ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998 ** get_address_of_OnShouldClose_8() { return &___OnShouldClose_8; }
	inline void set_OnShouldClose_8(ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998 * value)
	{
		___OnShouldClose_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnShouldClose_8), value);
	}

	inline static int32_t get_offset_of_OnKeyCodeReceived_9() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnKeyCodeReceived_9)); }
	inline KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C * get_OnKeyCodeReceived_9() const { return ___OnKeyCodeReceived_9; }
	inline KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C ** get_address_of_OnKeyCodeReceived_9() { return &___OnKeyCodeReceived_9; }
	inline void set_OnKeyCodeReceived_9(KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C * value)
	{
		___OnKeyCodeReceived_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnKeyCodeReceived_9), value);
	}

	inline static int32_t get_offset_of_OnOrientationChanged_10() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnOrientationChanged_10)); }
	inline OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 * get_OnOrientationChanged_10() const { return ___OnOrientationChanged_10; }
	inline OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 ** get_address_of_OnOrientationChanged_10() { return &___OnOrientationChanged_10; }
	inline void set_OnOrientationChanged_10(OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 * value)
	{
		___OnOrientationChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnOrientationChanged_10), value);
	}

	inline static int32_t get_offset_of_OnWebContentProcessTerminated_11() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnWebContentProcessTerminated_11)); }
	inline OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD * get_OnWebContentProcessTerminated_11() const { return ___OnWebContentProcessTerminated_11; }
	inline OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD ** get_address_of_OnWebContentProcessTerminated_11() { return &___OnWebContentProcessTerminated_11; }
	inline void set_OnWebContentProcessTerminated_11(OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD * value)
	{
		___OnWebContentProcessTerminated_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnWebContentProcessTerminated_11), value);
	}

	inline static int32_t get_offset_of_id_12() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___id_12)); }
	inline String_t* get_id_12() const { return ___id_12; }
	inline String_t** get_address_of_id_12() { return &___id_12; }
	inline void set_id_12(String_t* value)
	{
		___id_12 = value;
		Il2CppCodeGenWriteBarrier((&___id_12), value);
	}

	inline static int32_t get_offset_of_listener_13() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___listener_13)); }
	inline UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD * get_listener_13() const { return ___listener_13; }
	inline UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD ** get_address_of_listener_13() { return &___listener_13; }
	inline void set_listener_13(UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD * value)
	{
		___listener_13 = value;
		Il2CppCodeGenWriteBarrier((&___listener_13), value);
	}

	inline static int32_t get_offset_of_isPortrait_14() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___isPortrait_14)); }
	inline bool get_isPortrait_14() const { return ___isPortrait_14; }
	inline bool* get_address_of_isPortrait_14() { return &___isPortrait_14; }
	inline void set_isPortrait_14(bool value)
	{
		___isPortrait_14 = value;
	}

	inline static int32_t get_offset_of_urlOnStart_15() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___urlOnStart_15)); }
	inline String_t* get_urlOnStart_15() const { return ___urlOnStart_15; }
	inline String_t** get_address_of_urlOnStart_15() { return &___urlOnStart_15; }
	inline void set_urlOnStart_15(String_t* value)
	{
		___urlOnStart_15 = value;
		Il2CppCodeGenWriteBarrier((&___urlOnStart_15), value);
	}

	inline static int32_t get_offset_of_showOnStart_16() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___showOnStart_16)); }
	inline bool get_showOnStart_16() const { return ___showOnStart_16; }
	inline bool* get_address_of_showOnStart_16() { return &___showOnStart_16; }
	inline void set_showOnStart_16(bool value)
	{
		___showOnStart_16 = value;
	}

	inline static int32_t get_offset_of_fullScreen_17() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___fullScreen_17)); }
	inline bool get_fullScreen_17() const { return ___fullScreen_17; }
	inline bool* get_address_of_fullScreen_17() { return &___fullScreen_17; }
	inline void set_fullScreen_17(bool value)
	{
		___fullScreen_17 = value;
	}

	inline static int32_t get_offset_of_useToolbar_18() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___useToolbar_18)); }
	inline bool get_useToolbar_18() const { return ___useToolbar_18; }
	inline bool* get_address_of_useToolbar_18() { return &___useToolbar_18; }
	inline void set_useToolbar_18(bool value)
	{
		___useToolbar_18 = value;
	}

	inline static int32_t get_offset_of_toolbarPosition_19() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___toolbarPosition_19)); }
	inline int32_t get_toolbarPosition_19() const { return ___toolbarPosition_19; }
	inline int32_t* get_address_of_toolbarPosition_19() { return &___toolbarPosition_19; }
	inline void set_toolbarPosition_19(int32_t value)
	{
		___toolbarPosition_19 = value;
	}

	inline static int32_t get_offset_of_actions_20() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___actions_20)); }
	inline Dictionary_2_t6869420B5ACFF0569A65586D45920B0F608DDB45 * get_actions_20() const { return ___actions_20; }
	inline Dictionary_2_t6869420B5ACFF0569A65586D45920B0F608DDB45 ** get_address_of_actions_20() { return &___actions_20; }
	inline void set_actions_20(Dictionary_2_t6869420B5ACFF0569A65586D45920B0F608DDB45 * value)
	{
		___actions_20 = value;
		Il2CppCodeGenWriteBarrier((&___actions_20), value);
	}

	inline static int32_t get_offset_of_payloadActions_21() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___payloadActions_21)); }
	inline Dictionary_2_tF7EF1EBF77E39D94C275304B89431CE3F887E216 * get_payloadActions_21() const { return ___payloadActions_21; }
	inline Dictionary_2_tF7EF1EBF77E39D94C275304B89431CE3F887E216 ** get_address_of_payloadActions_21() { return &___payloadActions_21; }
	inline void set_payloadActions_21(Dictionary_2_tF7EF1EBF77E39D94C275304B89431CE3F887E216 * value)
	{
		___payloadActions_21 = value;
		Il2CppCodeGenWriteBarrier((&___payloadActions_21), value);
	}

	inline static int32_t get_offset_of_frame_22() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___frame_22)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_frame_22() const { return ___frame_22; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_frame_22() { return &___frame_22; }
	inline void set_frame_22(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___frame_22 = value;
	}

	inline static int32_t get_offset_of_referenceRectTransform_23() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___referenceRectTransform_23)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_referenceRectTransform_23() const { return ___referenceRectTransform_23; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_referenceRectTransform_23() { return &___referenceRectTransform_23; }
	inline void set_referenceRectTransform_23(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___referenceRectTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___referenceRectTransform_23), value);
	}

	inline static int32_t get_offset_of_started_24() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___started_24)); }
	inline bool get_started_24() const { return ___started_24; }
	inline bool* get_address_of_started_24() { return &___started_24; }
	inline void set_started_24(bool value)
	{
		___started_24 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_25() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___backgroundColor_25)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_backgroundColor_25() const { return ___backgroundColor_25; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_backgroundColor_25() { return &___backgroundColor_25; }
	inline void set_backgroundColor_25(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___backgroundColor_25 = value;
	}

	inline static int32_t get_offset_of_OnOreintationChanged_26() { return static_cast<int32_t>(offsetof(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB, ___OnOreintationChanged_26)); }
	inline OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 * get_OnOreintationChanged_26() const { return ___OnOreintationChanged_26; }
	inline OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 ** get_address_of_OnOreintationChanged_26() { return &___OnOreintationChanged_26; }
	inline void set_OnOreintationChanged_26(OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482 * value)
	{
		___OnOreintationChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnOreintationChanged_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEW_T70C5C5815FC5A948185238830E0AB8B221E34BBB_H
#ifndef UNIWEBVIEWANDROIDSTATICLISTENER_T9987AA383ACDCD75BF174C6A87CBC88B83414A79_H
#define UNIWEBVIEWANDROIDSTATICLISTENER_T9987AA383ACDCD75BF174C6A87CBC88B83414A79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewAndroidStaticListener
struct  UniWebViewAndroidStaticListener_t9987AA383ACDCD75BF174C6A87CBC88B83414A79  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWANDROIDSTATICLISTENER_T9987AA383ACDCD75BF174C6A87CBC88B83414A79_H
#ifndef UNIWEBVIEWNATIVELISTENER_TFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_H
#define UNIWEBVIEWNATIVELISTENER_TFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniWebViewNativeListener
struct  UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UniWebView UniWebViewNativeListener::webView
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB * ___webView_5;

public:
	inline static int32_t get_offset_of_webView_5() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD, ___webView_5)); }
	inline UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB * get_webView_5() const { return ___webView_5; }
	inline UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB ** get_address_of_webView_5() { return &___webView_5; }
	inline void set_webView_5(UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB * value)
	{
		___webView_5 = value;
		Il2CppCodeGenWriteBarrier((&___webView_5), value);
	}
};

struct UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UniWebViewNativeListener> UniWebViewNativeListener::listeners
	Dictionary_2_tC8DAC4C5C0B35DE2FF589D35287ED2F7FBBEBD69 * ___listeners_4;

public:
	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_StaticFields, ___listeners_4)); }
	inline Dictionary_2_tC8DAC4C5C0B35DE2FF589D35287ED2F7FBBEBD69 * get_listeners_4() const { return ___listeners_4; }
	inline Dictionary_2_tC8DAC4C5C0B35DE2FF589D35287ED2F7FBBEBD69 ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(Dictionary_2_tC8DAC4C5C0B35DE2FF589D35287ED2F7FBBEBD69 * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIWEBVIEWNATIVELISTENER_TFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_H
#ifndef VW_GAMELOOP_T6DDC9F01B01BD65900BC934383C5DAD0792B6D15_H
#define VW_GAMELOOP_T6DDC9F01B01BD65900BC934383C5DAD0792B6D15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Gameloop
struct  VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubePlayer VW_Gameloop::ytplayer
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___ytplayer_4;
	// UnityEngine.GameObject VW_Gameloop::TransitionFadeOutPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___TransitionFadeOutPanel_5;
	// UnityEngine.GameObject VW_Gameloop::MapPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MapPanel_6;
	// UnityEngine.GameObject VW_Gameloop::endPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___endPanel_7;
	// TMPro.TextMeshProUGUI VW_Gameloop::endScoreTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___endScoreTextGUI_8;
	// TMPro.TextMeshProUGUI VW_Gameloop::sourceTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___sourceTextGUI_9;
	// UnityEngine.GameObject VW_Gameloop::sourceTextPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___sourceTextPanel_10;
	// TMPro.TextMeshProUGUI VW_Gameloop::descriptionTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___descriptionTextGUI_11;
	// UnityEngine.GameObject VW_Gameloop::descriptionPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___descriptionPanel_12;
	// TMPro.TextMeshProUGUI VW_Gameloop::rollOutcomeTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___rollOutcomeTextGUI_13;
	// TMPro.TextMeshProUGUI VW_Gameloop::promptTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___promptTextGUI_14;
	// TMPro.TextMeshProUGUI VW_Gameloop::virtueGainedTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___virtueGainedTextGUI_15;
	// TMPro.TextMeshProUGUI VW_Gameloop::virtueLostTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___virtueLostTextGUI_16;
	// TMPro.TextMeshProUGUI VW_Gameloop::virtueGainedIntTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___virtueGainedIntTextGUI_17;
	// TMPro.TextMeshProUGUI VW_Gameloop::virtueLostIntTextGUI
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___virtueLostIntTextGUI_18;
	// UnityEngine.GameObject VW_Gameloop::virtuesPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___virtuesPanel_19;
	// ProgressBarCircle VW_Gameloop::PrudenceBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___PrudenceBar_20;
	// ProgressBarCircle VW_Gameloop::TemperanceBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___TemperanceBar_21;
	// ProgressBarCircle VW_Gameloop::JusticeBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___JusticeBar_22;
	// ProgressBarCircle VW_Gameloop::FaithBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___FaithBar_23;
	// ProgressBarCircle VW_Gameloop::HopeBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___HopeBar_24;
	// ProgressBarCircle VW_Gameloop::CharityBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___CharityBar_25;
	// ProgressBarCircle VW_Gameloop::CourageBar
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * ___CourageBar_26;
	// UnityEngine.GameObject VW_Gameloop::optionButtonsPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___optionButtonsPanel_27;
	// UnityEngine.UI.Button VW_Gameloop::option1Button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___option1Button_28;
	// UnityEngine.UI.Button VW_Gameloop::option2Button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___option2Button_29;
	// UnityEngine.UI.Button VW_Gameloop::option3Button
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___option3Button_30;
	// UnityEngine.UI.Button VW_Gameloop::continueButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___continueButton_31;
	// UnityEngine.UI.Button VW_Gameloop::readSourceTextButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___readSourceTextButton_32;
	// UnityEngine.UI.Button VW_Gameloop::closeSourceTextButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___closeSourceTextButton_33;
	// UnityEngine.UI.Button VW_Gameloop::mapButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___mapButton_34;
	// UnityEngine.UI.Button VW_Gameloop::CloseMapButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___CloseMapButton_35;
	// UnityEngine.UI.Button VW_Gameloop::virtuesButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___virtuesButton_36;
	// UnityEngine.UI.Button VW_Gameloop::closeVirtuesButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___closeVirtuesButton_37;
	// TextScroller VW_Gameloop::sourceTextScroller
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC * ___sourceTextScroller_38;
	// TextScroller VW_Gameloop::descriptionTextScroller
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC * ___descriptionTextScroller_39;
	// UnityEngine.UI.RawImage VW_Gameloop::backgroundImgA
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___backgroundImgA_40;
	// UnityEngine.UI.RawImage VW_Gameloop::backgroundImgB
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___backgroundImgB_41;
	// System.Boolean VW_Gameloop::backgroundA
	bool ___backgroundA_42;
	// System.Int32 VW_Gameloop::Prudence
	int32_t ___Prudence_43;
	// System.Int32 VW_Gameloop::Temperance
	int32_t ___Temperance_44;
	// System.Int32 VW_Gameloop::Courage
	int32_t ___Courage_45;
	// System.Int32 VW_Gameloop::Justice
	int32_t ___Justice_46;
	// System.Int32 VW_Gameloop::Faith
	int32_t ___Faith_47;
	// System.Int32 VW_Gameloop::Hope
	int32_t ___Hope_48;
	// System.Int32 VW_Gameloop::Charity
	int32_t ___Charity_49;
	// System.String VW_Gameloop::sourceText
	String_t* ___sourceText_50;
	// System.Int32 VW_Gameloop::numberOfOptions
	int32_t ___numberOfOptions_51;
	// System.String VW_Gameloop::option1BadOutcomeText
	String_t* ___option1BadOutcomeText_52;
	// System.String VW_Gameloop::option2BadOutcomeText
	String_t* ___option2BadOutcomeText_53;
	// System.String VW_Gameloop::option3BadOutcomeText
	String_t* ___option3BadOutcomeText_54;
	// System.String VW_Gameloop::option1GoodOutcomeText
	String_t* ___option1GoodOutcomeText_55;
	// System.String VW_Gameloop::option2GoodOutcomeText
	String_t* ___option2GoodOutcomeText_56;
	// System.String VW_Gameloop::option3GoodOutcomeText
	String_t* ___option3GoodOutcomeText_57;
	// System.String VW_Gameloop::option1NeutralOutcomeText
	String_t* ___option1NeutralOutcomeText_58;
	// System.String VW_Gameloop::option2NeutralOutcomeText
	String_t* ___option2NeutralOutcomeText_59;
	// System.String VW_Gameloop::option3NeutralOutcomeText
	String_t* ___option3NeutralOutcomeText_60;
	// System.String VW_Gameloop::virtueGained1
	String_t* ___virtueGained1_61;
	// System.String VW_Gameloop::virtueGained1a
	String_t* ___virtueGained1a_62;
	// System.String VW_Gameloop::virtueGained2
	String_t* ___virtueGained2_63;
	// System.String VW_Gameloop::virtueGained2a
	String_t* ___virtueGained2a_64;
	// System.String VW_Gameloop::virtueGained3
	String_t* ___virtueGained3_65;
	// System.String VW_Gameloop::virtueGained3a
	String_t* ___virtueGained3a_66;
	// System.String VW_Gameloop::virtueLost1
	String_t* ___virtueLost1_67;
	// System.String VW_Gameloop::virtueLost1a
	String_t* ___virtueLost1a_68;
	// System.String VW_Gameloop::virtueLost2
	String_t* ___virtueLost2_69;
	// System.String VW_Gameloop::virtueLost2a
	String_t* ___virtueLost2a_70;
	// System.String VW_Gameloop::virtueLost3
	String_t* ___virtueLost3_71;
	// System.String VW_Gameloop::virtueLost3a
	String_t* ___virtueLost3a_72;
	// System.Int32 VW_Gameloop::audioStartTime
	int32_t ___audioStartTime_73;
	// System.Int32 VW_Gameloop::audioStopTime
	int32_t ___audioStopTime_74;
	// System.String VW_Gameloop::audioURL
	String_t* ___audioURL_75;
	// System.Int32 VW_Gameloop::currentRoomId
	int32_t ___currentRoomId_76;
	// System.Boolean VW_Gameloop::mapIsOn
	bool ___mapIsOn_77;
	// System.Boolean VW_Gameloop::firstRunDone
	bool ___firstRunDone_78;
	// System.String VW_Gameloop::nextBackgroundURI
	String_t* ___nextBackgroundURI_79;

public:
	inline static int32_t get_offset_of_ytplayer_4() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___ytplayer_4)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_ytplayer_4() const { return ___ytplayer_4; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_ytplayer_4() { return &___ytplayer_4; }
	inline void set_ytplayer_4(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___ytplayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___ytplayer_4), value);
	}

	inline static int32_t get_offset_of_TransitionFadeOutPanel_5() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___TransitionFadeOutPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_TransitionFadeOutPanel_5() const { return ___TransitionFadeOutPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_TransitionFadeOutPanel_5() { return &___TransitionFadeOutPanel_5; }
	inline void set_TransitionFadeOutPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___TransitionFadeOutPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___TransitionFadeOutPanel_5), value);
	}

	inline static int32_t get_offset_of_MapPanel_6() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___MapPanel_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MapPanel_6() const { return ___MapPanel_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MapPanel_6() { return &___MapPanel_6; }
	inline void set_MapPanel_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MapPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___MapPanel_6), value);
	}

	inline static int32_t get_offset_of_endPanel_7() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___endPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_endPanel_7() const { return ___endPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_endPanel_7() { return &___endPanel_7; }
	inline void set_endPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___endPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___endPanel_7), value);
	}

	inline static int32_t get_offset_of_endScoreTextGUI_8() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___endScoreTextGUI_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_endScoreTextGUI_8() const { return ___endScoreTextGUI_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_endScoreTextGUI_8() { return &___endScoreTextGUI_8; }
	inline void set_endScoreTextGUI_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___endScoreTextGUI_8 = value;
		Il2CppCodeGenWriteBarrier((&___endScoreTextGUI_8), value);
	}

	inline static int32_t get_offset_of_sourceTextGUI_9() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___sourceTextGUI_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_sourceTextGUI_9() const { return ___sourceTextGUI_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_sourceTextGUI_9() { return &___sourceTextGUI_9; }
	inline void set_sourceTextGUI_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___sourceTextGUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTextGUI_9), value);
	}

	inline static int32_t get_offset_of_sourceTextPanel_10() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___sourceTextPanel_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_sourceTextPanel_10() const { return ___sourceTextPanel_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_sourceTextPanel_10() { return &___sourceTextPanel_10; }
	inline void set_sourceTextPanel_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___sourceTextPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTextPanel_10), value);
	}

	inline static int32_t get_offset_of_descriptionTextGUI_11() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___descriptionTextGUI_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_descriptionTextGUI_11() const { return ___descriptionTextGUI_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_descriptionTextGUI_11() { return &___descriptionTextGUI_11; }
	inline void set_descriptionTextGUI_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___descriptionTextGUI_11 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionTextGUI_11), value);
	}

	inline static int32_t get_offset_of_descriptionPanel_12() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___descriptionPanel_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_descriptionPanel_12() const { return ___descriptionPanel_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_descriptionPanel_12() { return &___descriptionPanel_12; }
	inline void set_descriptionPanel_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___descriptionPanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionPanel_12), value);
	}

	inline static int32_t get_offset_of_rollOutcomeTextGUI_13() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___rollOutcomeTextGUI_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_rollOutcomeTextGUI_13() const { return ___rollOutcomeTextGUI_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_rollOutcomeTextGUI_13() { return &___rollOutcomeTextGUI_13; }
	inline void set_rollOutcomeTextGUI_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___rollOutcomeTextGUI_13 = value;
		Il2CppCodeGenWriteBarrier((&___rollOutcomeTextGUI_13), value);
	}

	inline static int32_t get_offset_of_promptTextGUI_14() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___promptTextGUI_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_promptTextGUI_14() const { return ___promptTextGUI_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_promptTextGUI_14() { return &___promptTextGUI_14; }
	inline void set_promptTextGUI_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___promptTextGUI_14 = value;
		Il2CppCodeGenWriteBarrier((&___promptTextGUI_14), value);
	}

	inline static int32_t get_offset_of_virtueGainedTextGUI_15() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGainedTextGUI_15)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_virtueGainedTextGUI_15() const { return ___virtueGainedTextGUI_15; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_virtueGainedTextGUI_15() { return &___virtueGainedTextGUI_15; }
	inline void set_virtueGainedTextGUI_15(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___virtueGainedTextGUI_15 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGainedTextGUI_15), value);
	}

	inline static int32_t get_offset_of_virtueLostTextGUI_16() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLostTextGUI_16)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_virtueLostTextGUI_16() const { return ___virtueLostTextGUI_16; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_virtueLostTextGUI_16() { return &___virtueLostTextGUI_16; }
	inline void set_virtueLostTextGUI_16(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___virtueLostTextGUI_16 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLostTextGUI_16), value);
	}

	inline static int32_t get_offset_of_virtueGainedIntTextGUI_17() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGainedIntTextGUI_17)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_virtueGainedIntTextGUI_17() const { return ___virtueGainedIntTextGUI_17; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_virtueGainedIntTextGUI_17() { return &___virtueGainedIntTextGUI_17; }
	inline void set_virtueGainedIntTextGUI_17(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___virtueGainedIntTextGUI_17 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGainedIntTextGUI_17), value);
	}

	inline static int32_t get_offset_of_virtueLostIntTextGUI_18() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLostIntTextGUI_18)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_virtueLostIntTextGUI_18() const { return ___virtueLostIntTextGUI_18; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_virtueLostIntTextGUI_18() { return &___virtueLostIntTextGUI_18; }
	inline void set_virtueLostIntTextGUI_18(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___virtueLostIntTextGUI_18 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLostIntTextGUI_18), value);
	}

	inline static int32_t get_offset_of_virtuesPanel_19() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtuesPanel_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_virtuesPanel_19() const { return ___virtuesPanel_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_virtuesPanel_19() { return &___virtuesPanel_19; }
	inline void set_virtuesPanel_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___virtuesPanel_19 = value;
		Il2CppCodeGenWriteBarrier((&___virtuesPanel_19), value);
	}

	inline static int32_t get_offset_of_PrudenceBar_20() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___PrudenceBar_20)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_PrudenceBar_20() const { return ___PrudenceBar_20; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_PrudenceBar_20() { return &___PrudenceBar_20; }
	inline void set_PrudenceBar_20(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___PrudenceBar_20 = value;
		Il2CppCodeGenWriteBarrier((&___PrudenceBar_20), value);
	}

	inline static int32_t get_offset_of_TemperanceBar_21() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___TemperanceBar_21)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_TemperanceBar_21() const { return ___TemperanceBar_21; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_TemperanceBar_21() { return &___TemperanceBar_21; }
	inline void set_TemperanceBar_21(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___TemperanceBar_21 = value;
		Il2CppCodeGenWriteBarrier((&___TemperanceBar_21), value);
	}

	inline static int32_t get_offset_of_JusticeBar_22() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___JusticeBar_22)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_JusticeBar_22() const { return ___JusticeBar_22; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_JusticeBar_22() { return &___JusticeBar_22; }
	inline void set_JusticeBar_22(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___JusticeBar_22 = value;
		Il2CppCodeGenWriteBarrier((&___JusticeBar_22), value);
	}

	inline static int32_t get_offset_of_FaithBar_23() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___FaithBar_23)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_FaithBar_23() const { return ___FaithBar_23; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_FaithBar_23() { return &___FaithBar_23; }
	inline void set_FaithBar_23(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___FaithBar_23 = value;
		Il2CppCodeGenWriteBarrier((&___FaithBar_23), value);
	}

	inline static int32_t get_offset_of_HopeBar_24() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___HopeBar_24)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_HopeBar_24() const { return ___HopeBar_24; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_HopeBar_24() { return &___HopeBar_24; }
	inline void set_HopeBar_24(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___HopeBar_24 = value;
		Il2CppCodeGenWriteBarrier((&___HopeBar_24), value);
	}

	inline static int32_t get_offset_of_CharityBar_25() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___CharityBar_25)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_CharityBar_25() const { return ___CharityBar_25; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_CharityBar_25() { return &___CharityBar_25; }
	inline void set_CharityBar_25(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___CharityBar_25 = value;
		Il2CppCodeGenWriteBarrier((&___CharityBar_25), value);
	}

	inline static int32_t get_offset_of_CourageBar_26() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___CourageBar_26)); }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * get_CourageBar_26() const { return ___CourageBar_26; }
	inline ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 ** get_address_of_CourageBar_26() { return &___CourageBar_26; }
	inline void set_CourageBar_26(ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946 * value)
	{
		___CourageBar_26 = value;
		Il2CppCodeGenWriteBarrier((&___CourageBar_26), value);
	}

	inline static int32_t get_offset_of_optionButtonsPanel_27() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___optionButtonsPanel_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_optionButtonsPanel_27() const { return ___optionButtonsPanel_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_optionButtonsPanel_27() { return &___optionButtonsPanel_27; }
	inline void set_optionButtonsPanel_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___optionButtonsPanel_27 = value;
		Il2CppCodeGenWriteBarrier((&___optionButtonsPanel_27), value);
	}

	inline static int32_t get_offset_of_option1Button_28() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option1Button_28)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_option1Button_28() const { return ___option1Button_28; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_option1Button_28() { return &___option1Button_28; }
	inline void set_option1Button_28(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___option1Button_28 = value;
		Il2CppCodeGenWriteBarrier((&___option1Button_28), value);
	}

	inline static int32_t get_offset_of_option2Button_29() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option2Button_29)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_option2Button_29() const { return ___option2Button_29; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_option2Button_29() { return &___option2Button_29; }
	inline void set_option2Button_29(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___option2Button_29 = value;
		Il2CppCodeGenWriteBarrier((&___option2Button_29), value);
	}

	inline static int32_t get_offset_of_option3Button_30() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option3Button_30)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_option3Button_30() const { return ___option3Button_30; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_option3Button_30() { return &___option3Button_30; }
	inline void set_option3Button_30(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___option3Button_30 = value;
		Il2CppCodeGenWriteBarrier((&___option3Button_30), value);
	}

	inline static int32_t get_offset_of_continueButton_31() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___continueButton_31)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_continueButton_31() const { return ___continueButton_31; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_continueButton_31() { return &___continueButton_31; }
	inline void set_continueButton_31(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___continueButton_31 = value;
		Il2CppCodeGenWriteBarrier((&___continueButton_31), value);
	}

	inline static int32_t get_offset_of_readSourceTextButton_32() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___readSourceTextButton_32)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_readSourceTextButton_32() const { return ___readSourceTextButton_32; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_readSourceTextButton_32() { return &___readSourceTextButton_32; }
	inline void set_readSourceTextButton_32(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___readSourceTextButton_32 = value;
		Il2CppCodeGenWriteBarrier((&___readSourceTextButton_32), value);
	}

	inline static int32_t get_offset_of_closeSourceTextButton_33() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___closeSourceTextButton_33)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_closeSourceTextButton_33() const { return ___closeSourceTextButton_33; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_closeSourceTextButton_33() { return &___closeSourceTextButton_33; }
	inline void set_closeSourceTextButton_33(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___closeSourceTextButton_33 = value;
		Il2CppCodeGenWriteBarrier((&___closeSourceTextButton_33), value);
	}

	inline static int32_t get_offset_of_mapButton_34() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___mapButton_34)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_mapButton_34() const { return ___mapButton_34; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_mapButton_34() { return &___mapButton_34; }
	inline void set_mapButton_34(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___mapButton_34 = value;
		Il2CppCodeGenWriteBarrier((&___mapButton_34), value);
	}

	inline static int32_t get_offset_of_CloseMapButton_35() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___CloseMapButton_35)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_CloseMapButton_35() const { return ___CloseMapButton_35; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_CloseMapButton_35() { return &___CloseMapButton_35; }
	inline void set_CloseMapButton_35(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___CloseMapButton_35 = value;
		Il2CppCodeGenWriteBarrier((&___CloseMapButton_35), value);
	}

	inline static int32_t get_offset_of_virtuesButton_36() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtuesButton_36)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_virtuesButton_36() const { return ___virtuesButton_36; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_virtuesButton_36() { return &___virtuesButton_36; }
	inline void set_virtuesButton_36(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___virtuesButton_36 = value;
		Il2CppCodeGenWriteBarrier((&___virtuesButton_36), value);
	}

	inline static int32_t get_offset_of_closeVirtuesButton_37() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___closeVirtuesButton_37)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_closeVirtuesButton_37() const { return ___closeVirtuesButton_37; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_closeVirtuesButton_37() { return &___closeVirtuesButton_37; }
	inline void set_closeVirtuesButton_37(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___closeVirtuesButton_37 = value;
		Il2CppCodeGenWriteBarrier((&___closeVirtuesButton_37), value);
	}

	inline static int32_t get_offset_of_sourceTextScroller_38() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___sourceTextScroller_38)); }
	inline TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC * get_sourceTextScroller_38() const { return ___sourceTextScroller_38; }
	inline TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC ** get_address_of_sourceTextScroller_38() { return &___sourceTextScroller_38; }
	inline void set_sourceTextScroller_38(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC * value)
	{
		___sourceTextScroller_38 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTextScroller_38), value);
	}

	inline static int32_t get_offset_of_descriptionTextScroller_39() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___descriptionTextScroller_39)); }
	inline TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC * get_descriptionTextScroller_39() const { return ___descriptionTextScroller_39; }
	inline TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC ** get_address_of_descriptionTextScroller_39() { return &___descriptionTextScroller_39; }
	inline void set_descriptionTextScroller_39(TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC * value)
	{
		___descriptionTextScroller_39 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionTextScroller_39), value);
	}

	inline static int32_t get_offset_of_backgroundImgA_40() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___backgroundImgA_40)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_backgroundImgA_40() const { return ___backgroundImgA_40; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_backgroundImgA_40() { return &___backgroundImgA_40; }
	inline void set_backgroundImgA_40(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___backgroundImgA_40 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundImgA_40), value);
	}

	inline static int32_t get_offset_of_backgroundImgB_41() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___backgroundImgB_41)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_backgroundImgB_41() const { return ___backgroundImgB_41; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_backgroundImgB_41() { return &___backgroundImgB_41; }
	inline void set_backgroundImgB_41(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___backgroundImgB_41 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundImgB_41), value);
	}

	inline static int32_t get_offset_of_backgroundA_42() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___backgroundA_42)); }
	inline bool get_backgroundA_42() const { return ___backgroundA_42; }
	inline bool* get_address_of_backgroundA_42() { return &___backgroundA_42; }
	inline void set_backgroundA_42(bool value)
	{
		___backgroundA_42 = value;
	}

	inline static int32_t get_offset_of_Prudence_43() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Prudence_43)); }
	inline int32_t get_Prudence_43() const { return ___Prudence_43; }
	inline int32_t* get_address_of_Prudence_43() { return &___Prudence_43; }
	inline void set_Prudence_43(int32_t value)
	{
		___Prudence_43 = value;
	}

	inline static int32_t get_offset_of_Temperance_44() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Temperance_44)); }
	inline int32_t get_Temperance_44() const { return ___Temperance_44; }
	inline int32_t* get_address_of_Temperance_44() { return &___Temperance_44; }
	inline void set_Temperance_44(int32_t value)
	{
		___Temperance_44 = value;
	}

	inline static int32_t get_offset_of_Courage_45() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Courage_45)); }
	inline int32_t get_Courage_45() const { return ___Courage_45; }
	inline int32_t* get_address_of_Courage_45() { return &___Courage_45; }
	inline void set_Courage_45(int32_t value)
	{
		___Courage_45 = value;
	}

	inline static int32_t get_offset_of_Justice_46() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Justice_46)); }
	inline int32_t get_Justice_46() const { return ___Justice_46; }
	inline int32_t* get_address_of_Justice_46() { return &___Justice_46; }
	inline void set_Justice_46(int32_t value)
	{
		___Justice_46 = value;
	}

	inline static int32_t get_offset_of_Faith_47() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Faith_47)); }
	inline int32_t get_Faith_47() const { return ___Faith_47; }
	inline int32_t* get_address_of_Faith_47() { return &___Faith_47; }
	inline void set_Faith_47(int32_t value)
	{
		___Faith_47 = value;
	}

	inline static int32_t get_offset_of_Hope_48() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Hope_48)); }
	inline int32_t get_Hope_48() const { return ___Hope_48; }
	inline int32_t* get_address_of_Hope_48() { return &___Hope_48; }
	inline void set_Hope_48(int32_t value)
	{
		___Hope_48 = value;
	}

	inline static int32_t get_offset_of_Charity_49() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___Charity_49)); }
	inline int32_t get_Charity_49() const { return ___Charity_49; }
	inline int32_t* get_address_of_Charity_49() { return &___Charity_49; }
	inline void set_Charity_49(int32_t value)
	{
		___Charity_49 = value;
	}

	inline static int32_t get_offset_of_sourceText_50() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___sourceText_50)); }
	inline String_t* get_sourceText_50() const { return ___sourceText_50; }
	inline String_t** get_address_of_sourceText_50() { return &___sourceText_50; }
	inline void set_sourceText_50(String_t* value)
	{
		___sourceText_50 = value;
		Il2CppCodeGenWriteBarrier((&___sourceText_50), value);
	}

	inline static int32_t get_offset_of_numberOfOptions_51() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___numberOfOptions_51)); }
	inline int32_t get_numberOfOptions_51() const { return ___numberOfOptions_51; }
	inline int32_t* get_address_of_numberOfOptions_51() { return &___numberOfOptions_51; }
	inline void set_numberOfOptions_51(int32_t value)
	{
		___numberOfOptions_51 = value;
	}

	inline static int32_t get_offset_of_option1BadOutcomeText_52() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option1BadOutcomeText_52)); }
	inline String_t* get_option1BadOutcomeText_52() const { return ___option1BadOutcomeText_52; }
	inline String_t** get_address_of_option1BadOutcomeText_52() { return &___option1BadOutcomeText_52; }
	inline void set_option1BadOutcomeText_52(String_t* value)
	{
		___option1BadOutcomeText_52 = value;
		Il2CppCodeGenWriteBarrier((&___option1BadOutcomeText_52), value);
	}

	inline static int32_t get_offset_of_option2BadOutcomeText_53() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option2BadOutcomeText_53)); }
	inline String_t* get_option2BadOutcomeText_53() const { return ___option2BadOutcomeText_53; }
	inline String_t** get_address_of_option2BadOutcomeText_53() { return &___option2BadOutcomeText_53; }
	inline void set_option2BadOutcomeText_53(String_t* value)
	{
		___option2BadOutcomeText_53 = value;
		Il2CppCodeGenWriteBarrier((&___option2BadOutcomeText_53), value);
	}

	inline static int32_t get_offset_of_option3BadOutcomeText_54() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option3BadOutcomeText_54)); }
	inline String_t* get_option3BadOutcomeText_54() const { return ___option3BadOutcomeText_54; }
	inline String_t** get_address_of_option3BadOutcomeText_54() { return &___option3BadOutcomeText_54; }
	inline void set_option3BadOutcomeText_54(String_t* value)
	{
		___option3BadOutcomeText_54 = value;
		Il2CppCodeGenWriteBarrier((&___option3BadOutcomeText_54), value);
	}

	inline static int32_t get_offset_of_option1GoodOutcomeText_55() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option1GoodOutcomeText_55)); }
	inline String_t* get_option1GoodOutcomeText_55() const { return ___option1GoodOutcomeText_55; }
	inline String_t** get_address_of_option1GoodOutcomeText_55() { return &___option1GoodOutcomeText_55; }
	inline void set_option1GoodOutcomeText_55(String_t* value)
	{
		___option1GoodOutcomeText_55 = value;
		Il2CppCodeGenWriteBarrier((&___option1GoodOutcomeText_55), value);
	}

	inline static int32_t get_offset_of_option2GoodOutcomeText_56() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option2GoodOutcomeText_56)); }
	inline String_t* get_option2GoodOutcomeText_56() const { return ___option2GoodOutcomeText_56; }
	inline String_t** get_address_of_option2GoodOutcomeText_56() { return &___option2GoodOutcomeText_56; }
	inline void set_option2GoodOutcomeText_56(String_t* value)
	{
		___option2GoodOutcomeText_56 = value;
		Il2CppCodeGenWriteBarrier((&___option2GoodOutcomeText_56), value);
	}

	inline static int32_t get_offset_of_option3GoodOutcomeText_57() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option3GoodOutcomeText_57)); }
	inline String_t* get_option3GoodOutcomeText_57() const { return ___option3GoodOutcomeText_57; }
	inline String_t** get_address_of_option3GoodOutcomeText_57() { return &___option3GoodOutcomeText_57; }
	inline void set_option3GoodOutcomeText_57(String_t* value)
	{
		___option3GoodOutcomeText_57 = value;
		Il2CppCodeGenWriteBarrier((&___option3GoodOutcomeText_57), value);
	}

	inline static int32_t get_offset_of_option1NeutralOutcomeText_58() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option1NeutralOutcomeText_58)); }
	inline String_t* get_option1NeutralOutcomeText_58() const { return ___option1NeutralOutcomeText_58; }
	inline String_t** get_address_of_option1NeutralOutcomeText_58() { return &___option1NeutralOutcomeText_58; }
	inline void set_option1NeutralOutcomeText_58(String_t* value)
	{
		___option1NeutralOutcomeText_58 = value;
		Il2CppCodeGenWriteBarrier((&___option1NeutralOutcomeText_58), value);
	}

	inline static int32_t get_offset_of_option2NeutralOutcomeText_59() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option2NeutralOutcomeText_59)); }
	inline String_t* get_option2NeutralOutcomeText_59() const { return ___option2NeutralOutcomeText_59; }
	inline String_t** get_address_of_option2NeutralOutcomeText_59() { return &___option2NeutralOutcomeText_59; }
	inline void set_option2NeutralOutcomeText_59(String_t* value)
	{
		___option2NeutralOutcomeText_59 = value;
		Il2CppCodeGenWriteBarrier((&___option2NeutralOutcomeText_59), value);
	}

	inline static int32_t get_offset_of_option3NeutralOutcomeText_60() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___option3NeutralOutcomeText_60)); }
	inline String_t* get_option3NeutralOutcomeText_60() const { return ___option3NeutralOutcomeText_60; }
	inline String_t** get_address_of_option3NeutralOutcomeText_60() { return &___option3NeutralOutcomeText_60; }
	inline void set_option3NeutralOutcomeText_60(String_t* value)
	{
		___option3NeutralOutcomeText_60 = value;
		Il2CppCodeGenWriteBarrier((&___option3NeutralOutcomeText_60), value);
	}

	inline static int32_t get_offset_of_virtueGained1_61() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGained1_61)); }
	inline String_t* get_virtueGained1_61() const { return ___virtueGained1_61; }
	inline String_t** get_address_of_virtueGained1_61() { return &___virtueGained1_61; }
	inline void set_virtueGained1_61(String_t* value)
	{
		___virtueGained1_61 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGained1_61), value);
	}

	inline static int32_t get_offset_of_virtueGained1a_62() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGained1a_62)); }
	inline String_t* get_virtueGained1a_62() const { return ___virtueGained1a_62; }
	inline String_t** get_address_of_virtueGained1a_62() { return &___virtueGained1a_62; }
	inline void set_virtueGained1a_62(String_t* value)
	{
		___virtueGained1a_62 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGained1a_62), value);
	}

	inline static int32_t get_offset_of_virtueGained2_63() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGained2_63)); }
	inline String_t* get_virtueGained2_63() const { return ___virtueGained2_63; }
	inline String_t** get_address_of_virtueGained2_63() { return &___virtueGained2_63; }
	inline void set_virtueGained2_63(String_t* value)
	{
		___virtueGained2_63 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGained2_63), value);
	}

	inline static int32_t get_offset_of_virtueGained2a_64() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGained2a_64)); }
	inline String_t* get_virtueGained2a_64() const { return ___virtueGained2a_64; }
	inline String_t** get_address_of_virtueGained2a_64() { return &___virtueGained2a_64; }
	inline void set_virtueGained2a_64(String_t* value)
	{
		___virtueGained2a_64 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGained2a_64), value);
	}

	inline static int32_t get_offset_of_virtueGained3_65() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGained3_65)); }
	inline String_t* get_virtueGained3_65() const { return ___virtueGained3_65; }
	inline String_t** get_address_of_virtueGained3_65() { return &___virtueGained3_65; }
	inline void set_virtueGained3_65(String_t* value)
	{
		___virtueGained3_65 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGained3_65), value);
	}

	inline static int32_t get_offset_of_virtueGained3a_66() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueGained3a_66)); }
	inline String_t* get_virtueGained3a_66() const { return ___virtueGained3a_66; }
	inline String_t** get_address_of_virtueGained3a_66() { return &___virtueGained3a_66; }
	inline void set_virtueGained3a_66(String_t* value)
	{
		___virtueGained3a_66 = value;
		Il2CppCodeGenWriteBarrier((&___virtueGained3a_66), value);
	}

	inline static int32_t get_offset_of_virtueLost1_67() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLost1_67)); }
	inline String_t* get_virtueLost1_67() const { return ___virtueLost1_67; }
	inline String_t** get_address_of_virtueLost1_67() { return &___virtueLost1_67; }
	inline void set_virtueLost1_67(String_t* value)
	{
		___virtueLost1_67 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLost1_67), value);
	}

	inline static int32_t get_offset_of_virtueLost1a_68() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLost1a_68)); }
	inline String_t* get_virtueLost1a_68() const { return ___virtueLost1a_68; }
	inline String_t** get_address_of_virtueLost1a_68() { return &___virtueLost1a_68; }
	inline void set_virtueLost1a_68(String_t* value)
	{
		___virtueLost1a_68 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLost1a_68), value);
	}

	inline static int32_t get_offset_of_virtueLost2_69() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLost2_69)); }
	inline String_t* get_virtueLost2_69() const { return ___virtueLost2_69; }
	inline String_t** get_address_of_virtueLost2_69() { return &___virtueLost2_69; }
	inline void set_virtueLost2_69(String_t* value)
	{
		___virtueLost2_69 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLost2_69), value);
	}

	inline static int32_t get_offset_of_virtueLost2a_70() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLost2a_70)); }
	inline String_t* get_virtueLost2a_70() const { return ___virtueLost2a_70; }
	inline String_t** get_address_of_virtueLost2a_70() { return &___virtueLost2a_70; }
	inline void set_virtueLost2a_70(String_t* value)
	{
		___virtueLost2a_70 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLost2a_70), value);
	}

	inline static int32_t get_offset_of_virtueLost3_71() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLost3_71)); }
	inline String_t* get_virtueLost3_71() const { return ___virtueLost3_71; }
	inline String_t** get_address_of_virtueLost3_71() { return &___virtueLost3_71; }
	inline void set_virtueLost3_71(String_t* value)
	{
		___virtueLost3_71 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLost3_71), value);
	}

	inline static int32_t get_offset_of_virtueLost3a_72() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___virtueLost3a_72)); }
	inline String_t* get_virtueLost3a_72() const { return ___virtueLost3a_72; }
	inline String_t** get_address_of_virtueLost3a_72() { return &___virtueLost3a_72; }
	inline void set_virtueLost3a_72(String_t* value)
	{
		___virtueLost3a_72 = value;
		Il2CppCodeGenWriteBarrier((&___virtueLost3a_72), value);
	}

	inline static int32_t get_offset_of_audioStartTime_73() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___audioStartTime_73)); }
	inline int32_t get_audioStartTime_73() const { return ___audioStartTime_73; }
	inline int32_t* get_address_of_audioStartTime_73() { return &___audioStartTime_73; }
	inline void set_audioStartTime_73(int32_t value)
	{
		___audioStartTime_73 = value;
	}

	inline static int32_t get_offset_of_audioStopTime_74() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___audioStopTime_74)); }
	inline int32_t get_audioStopTime_74() const { return ___audioStopTime_74; }
	inline int32_t* get_address_of_audioStopTime_74() { return &___audioStopTime_74; }
	inline void set_audioStopTime_74(int32_t value)
	{
		___audioStopTime_74 = value;
	}

	inline static int32_t get_offset_of_audioURL_75() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___audioURL_75)); }
	inline String_t* get_audioURL_75() const { return ___audioURL_75; }
	inline String_t** get_address_of_audioURL_75() { return &___audioURL_75; }
	inline void set_audioURL_75(String_t* value)
	{
		___audioURL_75 = value;
		Il2CppCodeGenWriteBarrier((&___audioURL_75), value);
	}

	inline static int32_t get_offset_of_currentRoomId_76() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___currentRoomId_76)); }
	inline int32_t get_currentRoomId_76() const { return ___currentRoomId_76; }
	inline int32_t* get_address_of_currentRoomId_76() { return &___currentRoomId_76; }
	inline void set_currentRoomId_76(int32_t value)
	{
		___currentRoomId_76 = value;
	}

	inline static int32_t get_offset_of_mapIsOn_77() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___mapIsOn_77)); }
	inline bool get_mapIsOn_77() const { return ___mapIsOn_77; }
	inline bool* get_address_of_mapIsOn_77() { return &___mapIsOn_77; }
	inline void set_mapIsOn_77(bool value)
	{
		___mapIsOn_77 = value;
	}

	inline static int32_t get_offset_of_firstRunDone_78() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___firstRunDone_78)); }
	inline bool get_firstRunDone_78() const { return ___firstRunDone_78; }
	inline bool* get_address_of_firstRunDone_78() { return &___firstRunDone_78; }
	inline void set_firstRunDone_78(bool value)
	{
		___firstRunDone_78 = value;
	}

	inline static int32_t get_offset_of_nextBackgroundURI_79() { return static_cast<int32_t>(offsetof(VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15, ___nextBackgroundURI_79)); }
	inline String_t* get_nextBackgroundURI_79() const { return ___nextBackgroundURI_79; }
	inline String_t** get_address_of_nextBackgroundURI_79() { return &___nextBackgroundURI_79; }
	inline void set_nextBackgroundURI_79(String_t* value)
	{
		___nextBackgroundURI_79 = value;
		Il2CppCodeGenWriteBarrier((&___nextBackgroundURI_79), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VW_GAMELOOP_T6DDC9F01B01BD65900BC934383C5DAD0792B6D15_H
#ifndef VW_START_TFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69_H
#define VW_START_TFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VW_Start
struct  VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject VW_Start::titleFire
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___titleFire_4;
	// UnityEngine.UI.Text VW_Start::titleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___titleText_5;
	// UnityEngine.UI.Button VW_Start::beginButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___beginButton_6;
	// UnityEngine.UI.Text VW_Start::beginButtonText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___beginButtonText_7;
	// UnityEngine.GameObject VW_Start::transitionPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___transitionPanel_8;

public:
	inline static int32_t get_offset_of_titleFire_4() { return static_cast<int32_t>(offsetof(VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69, ___titleFire_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_titleFire_4() const { return ___titleFire_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_titleFire_4() { return &___titleFire_4; }
	inline void set_titleFire_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___titleFire_4 = value;
		Il2CppCodeGenWriteBarrier((&___titleFire_4), value);
	}

	inline static int32_t get_offset_of_titleText_5() { return static_cast<int32_t>(offsetof(VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69, ___titleText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_titleText_5() const { return ___titleText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_titleText_5() { return &___titleText_5; }
	inline void set_titleText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___titleText_5 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_5), value);
	}

	inline static int32_t get_offset_of_beginButton_6() { return static_cast<int32_t>(offsetof(VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69, ___beginButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_beginButton_6() const { return ___beginButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_beginButton_6() { return &___beginButton_6; }
	inline void set_beginButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___beginButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___beginButton_6), value);
	}

	inline static int32_t get_offset_of_beginButtonText_7() { return static_cast<int32_t>(offsetof(VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69, ___beginButtonText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_beginButtonText_7() const { return ___beginButtonText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_beginButtonText_7() { return &___beginButtonText_7; }
	inline void set_beginButtonText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___beginButtonText_7 = value;
		Il2CppCodeGenWriteBarrier((&___beginButtonText_7), value);
	}

	inline static int32_t get_offset_of_transitionPanel_8() { return static_cast<int32_t>(offsetof(VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69, ___transitionPanel_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_transitionPanel_8() const { return ___transitionPanel_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_transitionPanel_8() { return &___transitionPanel_8; }
	inline void set_transitionPanel_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___transitionPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___transitionPanel_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VW_START_TFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69_H
#ifndef YOUTUBEDEMOUSAGE_TF5509F4FA3F398FFF610F6454713F918950E2D63_H
#define YOUTUBEDEMOUSAGE_TF5509F4FA3F398FFF610F6454713F918950E2D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeDemoUsage
struct  YoutubeDemoUsage_tF5509F4FA3F398FFF610F6454713F918950E2D63  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject YoutubeDemoUsage::mainUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainUI_4;
	// UnityEngine.UI.Text YoutubeDemoUsage::videoUrlInput
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoUrlInput_5;

public:
	inline static int32_t get_offset_of_mainUI_4() { return static_cast<int32_t>(offsetof(YoutubeDemoUsage_tF5509F4FA3F398FFF610F6454713F918950E2D63, ___mainUI_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainUI_4() const { return ___mainUI_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainUI_4() { return &___mainUI_4; }
	inline void set_mainUI_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainUI_4), value);
	}

	inline static int32_t get_offset_of_videoUrlInput_5() { return static_cast<int32_t>(offsetof(YoutubeDemoUsage_tF5509F4FA3F398FFF610F6454713F918950E2D63, ___videoUrlInput_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoUrlInput_5() const { return ___videoUrlInput_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoUrlInput_5() { return &___videoUrlInput_5; }
	inline void set_videoUrlInput_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoUrlInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoUrlInput_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEDEMOUSAGE_TF5509F4FA3F398FFF610F6454713F918950E2D63_H
#ifndef YOUTUBESUBTITLESREADER_TCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA_H
#define YOUTUBESUBTITLESREADER_TCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeSubtitlesReader
struct  YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] YoutubeSubtitlesReader::_delimiters
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____delimiters_4;
	// System.String YoutubeSubtitlesReader::videoID
	String_t* ___videoID_5;
	// System.String YoutubeSubtitlesReader::langCode
	String_t* ___langCode_6;
	// UnityEngine.Video.VideoPlayer YoutubeSubtitlesReader::videoPlayer
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * ___videoPlayer_7;
	// System.Boolean YoutubeSubtitlesReader::subtitleLoaded
	bool ___subtitleLoaded_8;
	// System.String YoutubeSubtitlesReader::currentTextLine
	String_t* ___currentTextLine_9;
	// UnityEngine.UI.Text YoutubeSubtitlesReader::uiSubtitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___uiSubtitle_10;
	// System.Collections.Generic.List`1<SubtitleItem> YoutubeSubtitlesReader::subtitleList
	List_1_tFE21EC83370D816A3AADFCB922A2CDAC1A5A5CA0 * ___subtitleList_11;

public:
	inline static int32_t get_offset_of__delimiters_4() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ____delimiters_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__delimiters_4() const { return ____delimiters_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__delimiters_4() { return &____delimiters_4; }
	inline void set__delimiters_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____delimiters_4 = value;
		Il2CppCodeGenWriteBarrier((&____delimiters_4), value);
	}

	inline static int32_t get_offset_of_videoID_5() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___videoID_5)); }
	inline String_t* get_videoID_5() const { return ___videoID_5; }
	inline String_t** get_address_of_videoID_5() { return &___videoID_5; }
	inline void set_videoID_5(String_t* value)
	{
		___videoID_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoID_5), value);
	}

	inline static int32_t get_offset_of_langCode_6() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___langCode_6)); }
	inline String_t* get_langCode_6() const { return ___langCode_6; }
	inline String_t** get_address_of_langCode_6() { return &___langCode_6; }
	inline void set_langCode_6(String_t* value)
	{
		___langCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___langCode_6), value);
	}

	inline static int32_t get_offset_of_videoPlayer_7() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___videoPlayer_7)); }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * get_videoPlayer_7() const { return ___videoPlayer_7; }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 ** get_address_of_videoPlayer_7() { return &___videoPlayer_7; }
	inline void set_videoPlayer_7(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * value)
	{
		___videoPlayer_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_7), value);
	}

	inline static int32_t get_offset_of_subtitleLoaded_8() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___subtitleLoaded_8)); }
	inline bool get_subtitleLoaded_8() const { return ___subtitleLoaded_8; }
	inline bool* get_address_of_subtitleLoaded_8() { return &___subtitleLoaded_8; }
	inline void set_subtitleLoaded_8(bool value)
	{
		___subtitleLoaded_8 = value;
	}

	inline static int32_t get_offset_of_currentTextLine_9() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___currentTextLine_9)); }
	inline String_t* get_currentTextLine_9() const { return ___currentTextLine_9; }
	inline String_t** get_address_of_currentTextLine_9() { return &___currentTextLine_9; }
	inline void set_currentTextLine_9(String_t* value)
	{
		___currentTextLine_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentTextLine_9), value);
	}

	inline static int32_t get_offset_of_uiSubtitle_10() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___uiSubtitle_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_uiSubtitle_10() const { return ___uiSubtitle_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_uiSubtitle_10() { return &___uiSubtitle_10; }
	inline void set_uiSubtitle_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___uiSubtitle_10 = value;
		Il2CppCodeGenWriteBarrier((&___uiSubtitle_10), value);
	}

	inline static int32_t get_offset_of_subtitleList_11() { return static_cast<int32_t>(offsetof(YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA, ___subtitleList_11)); }
	inline List_1_tFE21EC83370D816A3AADFCB922A2CDAC1A5A5CA0 * get_subtitleList_11() const { return ___subtitleList_11; }
	inline List_1_tFE21EC83370D816A3AADFCB922A2CDAC1A5A5CA0 ** get_address_of_subtitleList_11() { return &___subtitleList_11; }
	inline void set_subtitleList_11(List_1_tFE21EC83370D816A3AADFCB922A2CDAC1A5A5CA0 * value)
	{
		___subtitleList_11 = value;
		Il2CppCodeGenWriteBarrier((&___subtitleList_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBESUBTITLESREADER_TCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6500 = { sizeof (YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6500[8] = 
{
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_publishedAt_0(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_channelId_1(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_title_2(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_description_3(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_thumbnails_4(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_channelTitle_5(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_tags_6(),
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40::get_offset_of_categoryId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6501 = { sizeof (YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6501[4] = 
{
	YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC::get_offset_of_defaultThumbnail_0(),
	YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC::get_offset_of_mediumThumbnail_1(),
	YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC::get_offset_of_highThumbnail_2(),
	YoutubeTumbnails_t2DE44958571CFC58B8078BF5A760769F5A1C9CBC::get_offset_of_standardThumbnail_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6502 = { sizeof (YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6502[3] = 
{
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA::get_offset_of_url_0(),
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA::get_offset_of_width_1(),
	YoutubeThumbnailData_t707283168497269B2FDBB9C7AB915706EFA408AA::get_offset_of_height_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6503 = { sizeof (YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6503[5] = 
{
	YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC::get_offset_of_viewCount_0(),
	YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC::get_offset_of_likeCount_1(),
	YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC::get_offset_of_dislikeCount_2(),
	YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC::get_offset_of_favoriteCount_3(),
	YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC::get_offset_of_commentCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6504 = { sizeof (YoutubeDemoUsage_tF5509F4FA3F398FFF610F6454713F918950E2D63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6504[2] = 
{
	YoutubeDemoUsage_tF5509F4FA3F398FFF610F6454713F918950E2D63::get_offset_of_mainUI_4(),
	YoutubeDemoUsage_tF5509F4FA3F398FFF610F6454713F918950E2D63::get_offset_of_videoUrlInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6505 = { sizeof (YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6505[8] = 
{
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of__delimiters_4(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_videoID_5(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_langCode_6(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_videoPlayer_7(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_subtitleLoaded_8(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_currentTextLine_9(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_uiSubtitle_10(),
	YoutubeSubtitlesReader_tCC9372F0330160B0BCB4818B7ECCB1581A7FC0AA::get_offset_of_subtitleList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6506 = { sizeof (U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6506[4] = 
{
	U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadSubtitleU3Ed__12_tB284D98017933707139413117186754EEBE87F96::get_offset_of_U3CurlU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6507 = { sizeof (U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526), -1, sizeof(U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6507[3] = 
{
	U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields::get_offset_of_U3CU3E9__14_0_1(),
	U3CU3Ec_t604D3984D39829E479EF332158A235B2256C9526_StaticFields::get_offset_of_U3CU3E9__14_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6508 = { sizeof (U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6508[6] = 
{
	U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555::get_offset_of_U3CU3E1__state_0(),
	U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555::get_offset_of_U3CU3E2__current_1(),
	U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555::get_offset_of_r_3(),
	U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555::get_offset_of_U3CU3E3__r_4(),
	U3CGetVttSubTitlePartsU3Ed__15_t9B0878663BC0AC499121B0E09D892DEE336FF555::get_offset_of_U3CreaderU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6509 = { sizeof (SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6509[3] = 
{
	SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA::get_offset_of_U3CStartTimeU3Ek__BackingField_0(),
	SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA::get_offset_of_U3CEndTimeU3Ek__BackingField_1(),
	SubtitleItem_t8EDE48ED71AA5EBFDEB0B9BDAEF002A74A8A31BA::get_offset_of_U3CtextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6510 = { sizeof (Demo_t9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6510[2] = 
{
	Demo_t9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23::get_offset_of_Pb_4(),
	Demo_t9C12C150B401AD8BCB3C5F1C8D3C3D79AE991A23::get_offset_of_PbC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6511 = { sizeof (ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6511[18] = 
{
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_Title_4(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_TitleColor_5(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_TitleFont_6(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_TitleFontSize_7(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_BarColor_8(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_BarBackGroundColor_9(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_BarBackGroundSprite_10(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_Alert_11(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_BarAlertColor_12(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_sound_13(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_repeat_14(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_RepeatRate_15(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_bar_16(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_barBackground_17(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_nextPlay_18(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_audiosource_19(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_txtTitle_20(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of_barValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6512 = { sizeof (ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6512[19] = 
{
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_Title_4(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_TitleColor_5(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_TitleFont_6(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_BarColor_7(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_BarBackGroundColor_8(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_MaskColor_9(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_BarBackGroundSprite_10(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_Alert_11(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_BarAlertColor_12(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_sound_13(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_repeat_14(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_RepearRate_15(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_bar_16(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_barBackground_17(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_Mask_18(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_nextPlay_19(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_audiosource_20(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_txtTitle_21(),
	ProgressBarCircle_t5949285E73BE4AF5E381E1C9226F7094A8649946::get_offset_of_barValue_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6513 = { sizeof (TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6513[8] = 
{
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_scrollSpeed_4(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_scrolling_5(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_stopScrollPositionY_6(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_startScrollPositionY_7(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_restartTimeMarker_8(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_stopWithinView_9(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_startPosition_10(),
	TextScroller_t8FECA395DC5E69F2222569AA6E4DB36A830797CC::get_offset_of_previousFloat_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6514 = { sizeof (VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6514[76] = 
{
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_ytplayer_4(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_TransitionFadeOutPanel_5(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_MapPanel_6(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_endPanel_7(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_endScoreTextGUI_8(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_sourceTextGUI_9(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_sourceTextPanel_10(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_descriptionTextGUI_11(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_descriptionPanel_12(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_rollOutcomeTextGUI_13(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_promptTextGUI_14(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGainedTextGUI_15(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLostTextGUI_16(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGainedIntTextGUI_17(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLostIntTextGUI_18(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtuesPanel_19(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_PrudenceBar_20(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_TemperanceBar_21(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_JusticeBar_22(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_FaithBar_23(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_HopeBar_24(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_CharityBar_25(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_CourageBar_26(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_optionButtonsPanel_27(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option1Button_28(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option2Button_29(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option3Button_30(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_continueButton_31(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_readSourceTextButton_32(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_closeSourceTextButton_33(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_mapButton_34(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_CloseMapButton_35(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtuesButton_36(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_closeVirtuesButton_37(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_sourceTextScroller_38(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_descriptionTextScroller_39(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_backgroundImgA_40(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_backgroundImgB_41(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_backgroundA_42(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Prudence_43(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Temperance_44(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Courage_45(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Justice_46(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Faith_47(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Hope_48(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_Charity_49(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_sourceText_50(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_numberOfOptions_51(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option1BadOutcomeText_52(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option2BadOutcomeText_53(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option3BadOutcomeText_54(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option1GoodOutcomeText_55(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option2GoodOutcomeText_56(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option3GoodOutcomeText_57(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option1NeutralOutcomeText_58(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option2NeutralOutcomeText_59(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_option3NeutralOutcomeText_60(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGained1_61(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGained1a_62(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGained2_63(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGained2a_64(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGained3_65(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueGained3a_66(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLost1_67(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLost1a_68(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLost2_69(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLost2a_70(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLost3_71(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_virtueLost3a_72(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_audioStartTime_73(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_audioStopTime_74(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_audioURL_75(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_currentRoomId_76(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_mapIsOn_77(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_firstRunDone_78(),
	VW_Gameloop_t6DDC9F01B01BD65900BC934383C5DAD0792B6D15::get_offset_of_nextBackgroundURI_79(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6515 = { sizeof (U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6515[4] = 
{
	U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25::get_offset_of_U3CU3E1__state_0(),
	U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25::get_offset_of_U3CU3E2__current_1(),
	U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25::get_offset_of_U3CU3E4__this_2(),
	U3CGradualVolumeIncreaseU3Ed__81_t7B70011EAEFD873F51A67EE4348908DFADE14A25::get_offset_of_U3CiU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6516 = { sizeof (U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6516[3] = 
{
	U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B::get_offset_of_U3CU3E1__state_0(),
	U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B::get_offset_of_U3CU3E2__current_1(),
	U3CStartFadeInU3Ed__87_t89378F24AAFF304A27F5DFE24313266F12795C9B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6517 = { sizeof (U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6517[3] = 
{
	U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C::get_offset_of_U3CU3E1__state_0(),
	U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C::get_offset_of_U3CU3E2__current_1(),
	U3CBeginDescriptionTextScrollU3Ed__88_t1ECBB76AABC2A65C8111948D4FC049DFA9B6177C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6518 = { sizeof (U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6518[3] = 
{
	U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378::get_offset_of_U3CU3E1__state_0(),
	U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378::get_offset_of_U3CU3E2__current_1(),
	U3CSourceTextHelperU3Ed__91_t86A887CED62D90FF840BB9F6CFE5A7A3BD99C378::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6519 = { sizeof (U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6519[3] = 
{
	U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57::get_offset_of_U3CU3E1__state_0(),
	U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57::get_offset_of_U3CU3E2__current_1(),
	U3CCloseSourceTextHelperU3Ed__93_t28244E3AB8E655AA874000D4E443804D3D088E57::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6520 = { sizeof (U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6520[3] = 
{
	U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForSourceTextScrollU3Ed__94_t6C579F4B055277F3523F8422EAEF35178ABF44FF::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6521 = { sizeof (U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6521[3] = 
{
	U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6::get_offset_of_U3CU3E1__state_0(),
	U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6::get_offset_of_U3CU3E2__current_1(),
	U3CRoomTransitionU3Ed__110_t60433AB23B0DF65E560B903F37CADD712540FDD6::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6522 = { sizeof (VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6522[5] = 
{
	VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69::get_offset_of_titleFire_4(),
	VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69::get_offset_of_titleText_5(),
	VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69::get_offset_of_beginButton_6(),
	VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69::get_offset_of_beginButtonText_7(),
	VW_Start_tFF7464FDF8F1708EDFC3EA6DE84B29DBCC2B3D69::get_offset_of_transitionPanel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6523 = { sizeof (U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6523[3] = 
{
	U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B::get_offset_of_U3CU3E1__state_0(),
	U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B::get_offset_of_U3CU3E2__current_1(),
	U3CTransitionU3Ed__8_t3CBA95DE5362447AE5BB4BA8FC83F222D9ED734B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6524 = { sizeof (UniWebViewAndroidStaticListener_t9987AA383ACDCD75BF174C6A87CBC88B83414A79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6525 = { sizeof (UniWebViewInterface_tE89E48E7F735126B06DE035E92DEFEFCA294E2CF), -1, sizeof(UniWebViewInterface_tE89E48E7F735126B06DE035E92DEFEFCA294E2CF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6525[2] = 
{
	0,
	UniWebViewInterface_tE89E48E7F735126B06DE035E92DEFEFCA294E2CF_StaticFields::get_offset_of_correctPlatform_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6526 = { sizeof (UnitySendMessageDelegate_t5D9F2A1448208AB6DB8BD0A4862C82C85C3F5D68), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6527 = { sizeof (UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6527[23] = 
{
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnPageStarted_4(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnPageFinished_5(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnPageErrorReceived_6(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnMessageReceived_7(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnShouldClose_8(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnKeyCodeReceived_9(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnOrientationChanged_10(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnWebContentProcessTerminated_11(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_id_12(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_listener_13(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_isPortrait_14(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_urlOnStart_15(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_showOnStart_16(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_fullScreen_17(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_useToolbar_18(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_toolbarPosition_19(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_actions_20(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_payloadActions_21(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_frame_22(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_referenceRectTransform_23(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_started_24(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_backgroundColor_25(),
	UniWebView_t70C5C5815FC5A948185238830E0AB8B221E34BBB::get_offset_of_OnOreintationChanged_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6528 = { sizeof (PageStartedDelegate_t249FC70F5F447EFE3E8A30439569A96F05602E78), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6529 = { sizeof (PageFinishedDelegate_t86B321BA20AE98611CA5520C46D556970FF31702), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6530 = { sizeof (PageErrorReceivedDelegate_t091963788F14CE6459BBB18AC29566778DE8CB4E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6531 = { sizeof (MessageReceivedDelegate_t30F573625B3F4C806D9073C9FB6773DC92922582), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6532 = { sizeof (ShouldCloseDelegate_t2E3349B793354B80E34E5AF4CB035C2370160998), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6533 = { sizeof (KeyCodeReceivedDelegate_tA8930CEAF69FA713516FC48F6899262B9E8E1C6C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6534 = { sizeof (OrientationChangedDelegate_t3D6957C9CC92E9341E30FCB84901049E0E8DA482), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6535 = { sizeof (OnWebContentProcessTerminatedDelegate_tAE07B0FEE5481D5653C82395ADAA3DF1A0DA2ECD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6536 = { sizeof (OreintationChangedDelegate_t42B38D7E92042A4E7C93015C2C675A483D7238E7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6537 = { sizeof (U3CU3Ec__DisplayClass116_0_t5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6537[1] = 
{
	U3CU3Ec__DisplayClass116_0_t5D96BEB4D5AA3C288B4CAC28F112569DE73F53C8::get_offset_of_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6538 = { sizeof (UniWebViewContentInsetAdjustmentBehavior_t8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6538[5] = 
{
	UniWebViewContentInsetAdjustmentBehavior_t8904EE64EB078E94C3BF45CC31C56C1AC83D6AEC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6539 = { sizeof (UniWebViewHelper_t8768407D2ECCF39080CB4D426A7AD65CC6F7A2D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6540 = { sizeof (UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36), -1, sizeof(UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6540[2] = 
{
	UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36_StaticFields::get_offset_of_instance_0(),
	UniWebViewLogger_tE1C8304AB4AFC816AB06B4B2E032DCB4F4C05D36::get_offset_of_level_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6541 = { sizeof (Level_tB3797A8D0D24AF315855A7B3D8DF86C22DFF2875)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6541[6] = 
{
	Level_tB3797A8D0D24AF315855A7B3D8DF86C22DFF2875::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6542 = { sizeof (UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6542[4] = 
{
	UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3::get_offset_of_U3CRawMessageU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3::get_offset_of_U3CSchemeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3::get_offset_of_U3CPathU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UniWebViewMessage_tAC6CC5EC4233D8B92FB19548180D216E36A42EC3::get_offset_of_U3CArgsU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6543 = { sizeof (UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD), -1, sizeof(UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6543[2] = 
{
	UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD_StaticFields::get_offset_of_listeners_4(),
	UniWebViewNativeListener_tFED75D47CB6B119CB39D323A1B14EDE2BD5CDBDD::get_offset_of_webView_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6544 = { sizeof (UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6544[3] = 
{
	UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C::get_offset_of_identifier_0(),
	UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C::get_offset_of_resultCode_1(),
	UniWebViewNativeResultPayload_t3A9397FEF18D4CB032484A81370D6BC52767730C::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6545 = { sizeof (UniWebViewToolbarPosition_t32937D05C62981C715E721005693AC89DE374110)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6545[3] = 
{
	UniWebViewToolbarPosition_t32937D05C62981C715E721005693AC89DE374110::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6546 = { sizeof (UniWebViewTransitionEdge_t5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6546[6] = 
{
	UniWebViewTransitionEdge_t5DEFB71C1BEC8B01AB5C58ACAE4B10B6253B70F3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6547 = { sizeof (KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6547[3] = 
{
	KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4::get_offset_of_screenshotHelper_4(),
	KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4::get_offset_of_keyToHold_5(),
	KeyPressExample_t368A70B2AF38467D816EBC9FD40A7E394CD3AAF4::get_offset_of_keyToPress_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6548 = { sizeof (SSHOrientation_tD92267ED5F3D35B0FAE933B079E87614D5DE1164)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6548[4] = 
{
	SSHOrientation_tD92267ED5F3D35B0FAE933B079E87614D5DE1164::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6549 = { sizeof (SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464)+ sizeof (RuntimeObject), sizeof(SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6549[2] = 
{
	SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SSH_IntVector2_t143F779FCF76ABCE93E606325CD8A755B1A08464::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6550 = { sizeof (GameViewUtils_t769B2DB6B20F2E1F61F5A8B58647DF8897124C84), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6551 = { sizeof (ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6551[3] = 
{
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83::get_offset_of_width_0(),
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83::get_offset_of_height_1(),
	ShotSize_t930D8996A44F429063B527BCE3DDEC5A57987A83::get_offset_of_label_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6552 = { sizeof (ShotSizeComparer_t0F04CB046FFB41E86535F78C93355C89DA28DAE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6553 = { sizeof (SSHDebug_tB10438BA824092372CD426813EB17B42F5FADAA7), -1, sizeof(SSHDebug_tB10438BA824092372CD426813EB17B42F5FADAA7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6553[1] = 
{
	SSHDebug_tB10438BA824092372CD426813EB17B42F5FADAA7_StaticFields::get_offset_of_tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6554 = { sizeof (SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6554[8] = 
{
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_orientation_0(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_lastSavePath_1(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_textureFormat_2(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_keyToPress_3(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_keyToHold_4(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_buildPathRoot_5(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_buildPathExtra_6(),
	SSHPreset_tEDDD55576EEE70AF6AD8DF0040E02D76157CB8BF::get_offset_of_sizes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6555 = { sizeof (SSHUtil_t04817B6981190908F228203374494E42CDEA4C1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6556 = { sizeof (U3CU3Ec__DisplayClass2_0_t591276796A0551304D8C26A0CE7BA15204639878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6556[2] = 
{
	U3CU3Ec__DisplayClass2_0_t591276796A0551304D8C26A0CE7BA15204639878::get_offset_of_textureColors_0(),
	U3CU3Ec__DisplayClass2_0_t591276796A0551304D8C26A0CE7BA15204639878::get_offset_of_onComplete_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6557 = { sizeof (TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0), -1, sizeof(TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6557[8] = 
{
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_texColors_0(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_newColors_1(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_w_2(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_ratioX_3(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_ratioY_4(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_w2_5(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_finishCount_6(),
	TextureScale_tF0F8ACB42A600B7A8E82714F3A57CA21361282B0_StaticFields::get_offset_of_mutex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6558 = { sizeof (ThreadData_tA2D7E4102ADCA935E96280CD5B2F46496D064915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6558[2] = 
{
	ThreadData_tA2D7E4102ADCA935E96280CD5B2F46496D064915::get_offset_of_start_0(),
	ThreadData_tA2D7E4102ADCA935E96280CD5B2F46496D064915::get_offset_of_end_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6559 = { sizeof (ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7), -1, sizeof(ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6559[22] = 
{
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_OnScreenChanged_4(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_unityThreadQueue_5(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_OnComplete_6(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_useRenderTexture_7(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_SSHTextureFormat_8(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_cameras_9(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_fileNamePrefix_10(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_openFolderWhenDone_11(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_didPressEditorScreenshotButton_12(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_orientation_13(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_shotInfo_14(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_maxRes_15(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_savePath_16(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_buildSavePathRoot_17(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_buildSavePathExtra_18(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_configFile_19(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7_StaticFields::get_offset_of__instance_20(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_PathChange_21(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_initialRes_22(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_shotCounter_23(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_U3CIsTakingShotsU3Ek__BackingField_24(),
	ScreenshotHelper_t64DEB8A1C8EEEB8C6ECCAA8BA420986A0CE78BB7::get_offset_of_DefaultsSet_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6560 = { sizeof (ScreenChange_tE53DBA009CFC24E445F8EEF6154D4E11B9ACB207), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6561 = { sizeof (tSSHTextureFormat_t42FE145BFFEFD23B172E1E8B56503D404285EB29)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6561[5] = 
{
	tSSHTextureFormat_t42FE145BFFEFD23B172E1E8B56503D404285EB29::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6562 = { sizeof (PathChangeDelegate_tFBEAD748E3655C88CB4AF8C7DAE7A64FE5C60734), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6563 = { sizeof (DefaultsSetDelegate_tFA7B425710CE014D62560A1262F5E6577871406E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6564 = { sizeof (U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12), -1, sizeof(U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6564[4] = 
{
	U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields::get_offset_of_U3CU3E9__25_0_1(),
	U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields::get_offset_of_U3CU3E9__40_0_2(),
	U3CU3Ec_t02381F501EC2C230EA56CE8D26DF847229FDDE12_StaticFields::get_offset_of_U3CU3E9__47_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6565 = { sizeof (U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6565[3] = 
{
	U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385::get_offset_of_currentIndex_1(),
	U3CU3Ec__DisplayClass40_0_tDBE0CB871FC680A21AE7514B3AF11393E06A7385::get_offset_of_timeScaleStart_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6566 = { sizeof (U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6566[3] = 
{
	U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632::get_offset_of_sshot_0(),
	U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632::get_offset_of_shotFileName_1(),
	U3CU3Ec__DisplayClass40_1_tA4E812E235C20F54100FA4C0B858669AE585D632::get_offset_of_CSU24U3CU3E8__locals1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6567 = { sizeof (U3CU3Ec__DisplayClass40_2_tBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6567[2] = 
{
	U3CU3Ec__DisplayClass40_2_tBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF::get_offset_of_result_0(),
	U3CU3Ec__DisplayClass40_2_tBA7BE01A8273BB81242E0DE7DDB0F7BD2E86C0BF::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6568 = { sizeof (U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6568[9] = 
{
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CU3E1__state_0(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CU3E2__current_1(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CU3E4__this_2(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CU3E8__1_3(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CfileNameU3E5__2_4(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CU3E7__wrap2_5(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CshotU3E5__4_6(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CthisResU3E5__5_7(),
	U3CTakeScreenShotsU3Ed__40_t80A59D0C872E13E19DD957758B5E7B200108A652::get_offset_of_U3CattemptsU3E5__6_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6569 = { sizeof (U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6569[4] = 
{
	U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7::get_offset_of_U3CU3E1__state_0(),
	U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7::get_offset_of_U3CU3E2__current_1(),
	U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7::get_offset_of_U3CU3E4__this_2(),
	U3CFinishRoutineU3Ed__42_tC8625A0079DDCADEB859E7F1E6C7FB0BB7153AF7::get_offset_of_timeScaleStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6570 = { sizeof (VideoNotAvailableException_tBAC14AF14AC6B5917524405D4CE14D6F2B150DC1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6571 = { sizeof (YoutubeParseException_t47C1A3E486CF3FA6F95CB02AEB58338DE2AB03D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6572 = { sizeof (HTTPHelperYoutube_tD81DEC1D08F2E8275A2A347FCA5A6ED388E84EF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6573 = { sizeof (MagicHands_t931494FE8B8B329EE92091FFC09073D63DDDCFFC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6574 = { sizeof (AudioType_t5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6574[6] = 
{
	AudioType_t5EA4FEC4719CEE5E80F38FCF7B638764E178BA0F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6575 = { sizeof (VideoType_t1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6575[7] = 
{
	VideoType_t1E43D4C1DC7DF23EE4D9E8F7550F179D16272BC3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6576 = { sizeof (AdaptiveType_t2CE340BC1400B7593B10E7F316D7FB972B6258AC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6576[5] = 
{
	AdaptiveType_t2CE340BC1400B7593B10E7F316D7FB972B6258AC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6577 = { sizeof (VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791), -1, sizeof(VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6577[14] = 
{
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791_StaticFields::get_offset_of_Defaults_0(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CAdaptiveTypeU3Ek__BackingField_1(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CAudioBitrateU3Ek__BackingField_2(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CAudioTypeU3Ek__BackingField_3(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CDownloadUrlU3Ek__BackingField_4(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CFormatCodeU3Ek__BackingField_5(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CIs3DU3Ek__BackingField_6(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CHDRU3Ek__BackingField_7(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_8(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CResolutionU3Ek__BackingField_9(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CTitleU3Ek__BackingField_10(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CVideoTypeU3Ek__BackingField_11(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CHtmlPlayerVersionU3Ek__BackingField_12(),
	VideoInfo_t29A0CB97F5DAEBD86EEDFBE1B2AC3A46BE57A791::get_offset_of_U3CHtmlscriptNameU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6578 = { sizeof (Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6578[5] = 
{
	Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2::get_offset_of_DownloadFinished_0(),
	Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2::get_offset_of_DownloadStarted_1(),
	Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2::get_offset_of_U3CBytesToDownloadU3Ek__BackingField_2(),
	Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2::get_offset_of_U3CSavePathU3Ek__BackingField_3(),
	Downloader_tCDC0917C49FCE183E7BCF542605C26DF14C670B2::get_offset_of_U3CVideoU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6579 = { sizeof (JSONNodeType_tF5D7E10AB8DB7403C6B5F7485A21932E862BF313)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6579[8] = 
{
	JSONNodeType_tF5D7E10AB8DB7403C6B5F7485A21932E862BF313::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6580 = { sizeof (JSONTextMode_t894D3826A172EA1B28488FF102150AF240171F8A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6580[3] = 
{
	JSONTextMode_t894D3826A172EA1B28488FF102150AF240171F8A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6581 = { sizeof (JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889), -1, sizeof(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6581[1] = 
{
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889_StaticFields::get_offset_of_m_EscapeBuilder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6582 = { sizeof (U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6582[3] = 
{
	U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ChildrenU3Ed__29_t6F820BC389B0FF6CEEBB89C2AAF81A13192CB2CF::get_offset_of_U3CU3El__initialThreadId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6583 = { sizeof (U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6583[6] = 
{
	U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94::get_offset_of_U3CU3E1__state_0(),
	U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94::get_offset_of_U3CU3E2__current_1(),
	U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94::get_offset_of_U3CU3E4__this_3(),
	U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94::get_offset_of_U3CU3E7__wrap1_4(),
	U3Cget_DeepChildrenU3Ed__31_tF3B1807A1B4E53363B7B2360F72919A5B9571A94::get_offset_of_U3CU3E7__wrap2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6584 = { sizeof (JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6584[2] = 
{
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8::get_offset_of_m_List_1(),
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8::get_offset_of_inline_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6585 = { sizeof (U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6585[5] = 
{
	U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3::get_offset_of_U3CU3E4__this_3(),
	U3Cget_ChildrenU3Ed__18_t1468C16AAFB4A3E659B8D16A2330948790364AC3::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6586 = { sizeof (U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6586[4] = 
{
	U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__19_t7D84C00216929B7C362244E89E6397BC3797F2AD::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6587 = { sizeof (JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6587[2] = 
{
	JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9::get_offset_of_m_Dict_1(),
	JSONObject_t6ED55195ECB193359B6772677198D259D2C86FC9::get_offset_of_inline_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6588 = { sizeof (U3CU3Ec__DisplayClass17_0_t0680462BA64C98F406466D423F160FF5561B71D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6588[1] = 
{
	U3CU3Ec__DisplayClass17_0_t0680462BA64C98F406466D423F160FF5561B71D2::get_offset_of_aNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6589 = { sizeof (U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6589[5] = 
{
	U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9::get_offset_of_U3CU3E4__this_3(),
	U3Cget_ChildrenU3Ed__19_t5BB7FCD6C8F1EC4A7F66EBF34DCF664371B9DCC9::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6590 = { sizeof (U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6590[4] = 
{
	U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__20_t91EA000CFDB6F9A15A550FB4536D59BDECA44061::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6591 = { sizeof (JSONString_tA15F893B4CF6A60092CBAC382BA65A8C55D0A850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6591[1] = 
{
	JSONString_tA15F893B4CF6A60092CBAC382BA65A8C55D0A850::get_offset_of_m_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6592 = { sizeof (JSONNumber_t75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6592[1] = 
{
	JSONNumber_t75F6317D8C2A82D898F9F6D6B26DA86F2FD6E839::get_offset_of_m_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6593 = { sizeof (JSONBool_tF2B768D59237D03145F065A9A93302A432E96F26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6593[1] = 
{
	JSONBool_tF2B768D59237D03145F065A9A93302A432E96F26::get_offset_of_m_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6594 = { sizeof (JSONNull_t07D22ABA25ECFD395B5B309982B136D037067ACE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6595 = { sizeof (JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6595[2] = 
{
	JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C::get_offset_of_m_Node_1(),
	JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C::get_offset_of_m_Key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6596 = { sizeof (JSON_t23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
