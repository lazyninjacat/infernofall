﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.Caching.HTTPCacheFileInfo
struct HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D;
// BestHTTP.Decompression.Zlib.DeflateStream
struct DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA;
// BestHTTP.Extensions.BufferPoolMemoryStream
struct BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC;
// BestHTTP.Extensions.CircularBuffer`1<System.Int32>
struct CircularBuffer_1_tAC7579C8AD77EAE5D35A20D485487AAE74B97F42;
// BestHTTP.HTTPRequest
struct HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null
struct Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate
struct AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509
struct AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.BasicConstraints
struct BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList
struct CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry
struct CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames
struct GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder
struct Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator
struct V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator
struct V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator
struct V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator
struct V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator
struct X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t585F03B00DBE773170901D2ABF9576187FAF92B5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject
struct DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Adler32
struct Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate
struct Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate/Config[]
struct ConfigU5BU5D_t51532B61CE88D8BC4B7B20FDE7B891355BCA6AD4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks
struct InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes
struct InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree
struct InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate
struct Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree
struct StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream
struct ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree
struct ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateHolder
struct AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateIssuer
struct AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.IX509AttributeCertificate
struct IX509AttributeCertificate_t2D2A358669CE9492D8A8B2CC1F76D5219BE75D41;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser
struct PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector
struct X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate
struct X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificatePair
struct X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A;
// BestHTTP.ServerSentEvents.EventSource
struct EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6;
// BestHTTP.ServerSentEvents.Message
struct Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3;
// BestHTTP.ServerSentEvents.OnErrorDelegate
struct OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2;
// BestHTTP.ServerSentEvents.OnGeneralEventDelegate
struct OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B;
// BestHTTP.ServerSentEvents.OnMessageDelegate
struct OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D;
// BestHTTP.ServerSentEvents.OnRetryDelegate
struct OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46;
// BestHTTP.ServerSentEvents.OnStateChangedDelegate
struct OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE;
// BestHTTP.WebSocket.Extensions.IExtension[]
struct IExtensionU5BU5D_tC6A1064DBEB723D76B4E806BB9FE94F6900E17A7;
// BestHTTP.WebSocket.OnWebSocketBinaryDelegate
struct OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6;
// BestHTTP.WebSocket.OnWebSocketClosedDelegate
struct OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98;
// BestHTTP.WebSocket.OnWebSocketErrorDelegate
struct OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA;
// BestHTTP.WebSocket.OnWebSocketIncompleteFrameDelegate
struct OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695;
// BestHTTP.WebSocket.OnWebSocketMessageDelegate
struct OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0;
// BestHTTP.WebSocket.OnWebSocketOpenDelegate
struct OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002;
// BestHTTP.WebSocket.WebSocket
struct WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717;
// BestHTTP.WebSocket.WebSocketResponse
struct WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct Action_2_t70FEF88466F857EA7127F5A2A55995AB9ADB166F;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>
struct Action_2_tF33231D562AD317E2BE60E911D813B80837EA270;
// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.String>
struct Action_2_t323FA38EC9A2B95AE8D7517DF75ED6EA00E02099;
// System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>
struct Action_3_t5D70FD461D1B40C1CFFA7F46226A3F75F6F6DC3E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.WebSocket.Frames.WebSocketFrame>
struct ConcurrentQueue_1_t8AA22CA8D8AADD1F0E3ACBDCDD8DB501B8A3DCAB;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct ConcurrentQueue_1_tC3C7EA7D8366EFAB17771E9A04D39272AF812FCA;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.ServerSentEvents.OnEventDelegate>
struct Dictionary_2_t7E96B3C27B69CD061EC5587E11344675BB70A2E9;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26;
// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie>
struct List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25;
// System.Collections.Generic.List`1<BestHTTP.ServerSentEvents.Message>
struct List_1_t01F3A1966CC10BAC639FFB6D44AB4A07695ACBC9;
// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader>
struct List_1_tACEAA9B1C6B540445C9D9A97B84B613D3E81C0ED;
// System.Collections.ICollection
struct ICollection_tA3BAB2482E28132A7CA9E0E21393027353C28B54;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Globalization.CompareInfo
struct CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#define HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPResponse
struct  HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.HTTPResponse::<VersionMajor>k__BackingField
	int32_t ___U3CVersionMajorU3Ek__BackingField_3;
	// System.Int32 BestHTTP.HTTPResponse::<VersionMinor>k__BackingField
	int32_t ___U3CVersionMinorU3Ek__BackingField_4;
	// System.Int32 BestHTTP.HTTPResponse::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_5;
	// System.String BestHTTP.HTTPResponse::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_6;
	// System.Boolean BestHTTP.HTTPResponse::<IsStreamed>k__BackingField
	bool ___U3CIsStreamedU3Ek__BackingField_7;
	// System.Boolean BestHTTP.HTTPResponse::<IsFromCache>k__BackingField
	bool ___U3CIsFromCacheU3Ek__BackingField_8;
	// BestHTTP.Caching.HTTPCacheFileInfo BestHTTP.HTTPResponse::<CacheFileInfo>k__BackingField
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * ___U3CCacheFileInfoU3Ek__BackingField_9;
	// System.Boolean BestHTTP.HTTPResponse::<IsCacheOnly>k__BackingField
	bool ___U3CIsCacheOnlyU3Ek__BackingField_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> BestHTTP.HTTPResponse::<Headers>k__BackingField
	Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * ___U3CHeadersU3Ek__BackingField_11;
	// System.Byte[] BestHTTP.HTTPResponse::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_12;
	// System.Boolean BestHTTP.HTTPResponse::<IsUpgraded>k__BackingField
	bool ___U3CIsUpgradedU3Ek__BackingField_13;
	// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie> BestHTTP.HTTPResponse::<Cookies>k__BackingField
	List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * ___U3CCookiesU3Ek__BackingField_14;
	// System.String BestHTTP.HTTPResponse::dataAsText
	String_t* ___dataAsText_15;
	// UnityEngine.Texture2D BestHTTP.HTTPResponse::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_16;
	// System.Boolean BestHTTP.HTTPResponse::<IsClosedManually>k__BackingField
	bool ___U3CIsClosedManuallyU3Ek__BackingField_17;
	// System.Int64 BestHTTP.HTTPResponse::UnprocessedFragments
	int64_t ___UnprocessedFragments_18;
	// BestHTTP.HTTPRequest BestHTTP.HTTPResponse::baseRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___baseRequest_19;
	// System.IO.Stream BestHTTP.HTTPResponse::Stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_20;
	// System.Byte[] BestHTTP.HTTPResponse::fragmentBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fragmentBuffer_21;
	// System.Int32 BestHTTP.HTTPResponse::fragmentBufferDataLength
	int32_t ___fragmentBufferDataLength_22;
	// System.IO.Stream BestHTTP.HTTPResponse::cacheStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___cacheStream_23;
	// System.Int32 BestHTTP.HTTPResponse::allFragmentSize
	int32_t ___allFragmentSize_24;

public:
	inline static int32_t get_offset_of_U3CVersionMajorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CVersionMajorU3Ek__BackingField_3)); }
	inline int32_t get_U3CVersionMajorU3Ek__BackingField_3() const { return ___U3CVersionMajorU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CVersionMajorU3Ek__BackingField_3() { return &___U3CVersionMajorU3Ek__BackingField_3; }
	inline void set_U3CVersionMajorU3Ek__BackingField_3(int32_t value)
	{
		___U3CVersionMajorU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVersionMinorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CVersionMinorU3Ek__BackingField_4)); }
	inline int32_t get_U3CVersionMinorU3Ek__BackingField_4() const { return ___U3CVersionMinorU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CVersionMinorU3Ek__BackingField_4() { return &___U3CVersionMinorU3Ek__BackingField_4; }
	inline void set_U3CVersionMinorU3Ek__BackingField_4(int32_t value)
	{
		___U3CVersionMinorU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CStatusCodeU3Ek__BackingField_5)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_5() const { return ___U3CStatusCodeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_5() { return &___U3CStatusCodeU3Ek__BackingField_5; }
	inline void set_U3CStatusCodeU3Ek__BackingField_5(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CMessageU3Ek__BackingField_6)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_6() const { return ___U3CMessageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_6() { return &___U3CMessageU3Ek__BackingField_6; }
	inline void set_U3CMessageU3Ek__BackingField_6(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsStreamedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsStreamedU3Ek__BackingField_7)); }
	inline bool get_U3CIsStreamedU3Ek__BackingField_7() const { return ___U3CIsStreamedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsStreamedU3Ek__BackingField_7() { return &___U3CIsStreamedU3Ek__BackingField_7; }
	inline void set_U3CIsStreamedU3Ek__BackingField_7(bool value)
	{
		___U3CIsStreamedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsFromCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsFromCacheU3Ek__BackingField_8)); }
	inline bool get_U3CIsFromCacheU3Ek__BackingField_8() const { return ___U3CIsFromCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsFromCacheU3Ek__BackingField_8() { return &___U3CIsFromCacheU3Ek__BackingField_8; }
	inline void set_U3CIsFromCacheU3Ek__BackingField_8(bool value)
	{
		___U3CIsFromCacheU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCacheFileInfoU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CCacheFileInfoU3Ek__BackingField_9)); }
	inline HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * get_U3CCacheFileInfoU3Ek__BackingField_9() const { return ___U3CCacheFileInfoU3Ek__BackingField_9; }
	inline HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D ** get_address_of_U3CCacheFileInfoU3Ek__BackingField_9() { return &___U3CCacheFileInfoU3Ek__BackingField_9; }
	inline void set_U3CCacheFileInfoU3Ek__BackingField_9(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * value)
	{
		___U3CCacheFileInfoU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheFileInfoU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIsCacheOnlyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsCacheOnlyU3Ek__BackingField_10)); }
	inline bool get_U3CIsCacheOnlyU3Ek__BackingField_10() const { return ___U3CIsCacheOnlyU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsCacheOnlyU3Ek__BackingField_10() { return &___U3CIsCacheOnlyU3Ek__BackingField_10; }
	inline void set_U3CIsCacheOnlyU3Ek__BackingField_10(bool value)
	{
		___U3CIsCacheOnlyU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CHeadersU3Ek__BackingField_11)); }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * get_U3CHeadersU3Ek__BackingField_11() const { return ___U3CHeadersU3Ek__BackingField_11; }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 ** get_address_of_U3CHeadersU3Ek__BackingField_11() { return &___U3CHeadersU3Ek__BackingField_11; }
	inline void set_U3CHeadersU3Ek__BackingField_11(Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * value)
	{
		___U3CHeadersU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CDataU3Ek__BackingField_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CIsUpgradedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsUpgradedU3Ek__BackingField_13)); }
	inline bool get_U3CIsUpgradedU3Ek__BackingField_13() const { return ___U3CIsUpgradedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsUpgradedU3Ek__BackingField_13() { return &___U3CIsUpgradedU3Ek__BackingField_13; }
	inline void set_U3CIsUpgradedU3Ek__BackingField_13(bool value)
	{
		___U3CIsUpgradedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CCookiesU3Ek__BackingField_14)); }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * get_U3CCookiesU3Ek__BackingField_14() const { return ___U3CCookiesU3Ek__BackingField_14; }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 ** get_address_of_U3CCookiesU3Ek__BackingField_14() { return &___U3CCookiesU3Ek__BackingField_14; }
	inline void set_U3CCookiesU3Ek__BackingField_14(List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * value)
	{
		___U3CCookiesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookiesU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_dataAsText_15() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___dataAsText_15)); }
	inline String_t* get_dataAsText_15() const { return ___dataAsText_15; }
	inline String_t** get_address_of_dataAsText_15() { return &___dataAsText_15; }
	inline void set_dataAsText_15(String_t* value)
	{
		___dataAsText_15 = value;
		Il2CppCodeGenWriteBarrier((&___dataAsText_15), value);
	}

	inline static int32_t get_offset_of_texture_16() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___texture_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_16() const { return ___texture_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_16() { return &___texture_16; }
	inline void set_texture_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_16 = value;
		Il2CppCodeGenWriteBarrier((&___texture_16), value);
	}

	inline static int32_t get_offset_of_U3CIsClosedManuallyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsClosedManuallyU3Ek__BackingField_17)); }
	inline bool get_U3CIsClosedManuallyU3Ek__BackingField_17() const { return ___U3CIsClosedManuallyU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsClosedManuallyU3Ek__BackingField_17() { return &___U3CIsClosedManuallyU3Ek__BackingField_17; }
	inline void set_U3CIsClosedManuallyU3Ek__BackingField_17(bool value)
	{
		___U3CIsClosedManuallyU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_UnprocessedFragments_18() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___UnprocessedFragments_18)); }
	inline int64_t get_UnprocessedFragments_18() const { return ___UnprocessedFragments_18; }
	inline int64_t* get_address_of_UnprocessedFragments_18() { return &___UnprocessedFragments_18; }
	inline void set_UnprocessedFragments_18(int64_t value)
	{
		___UnprocessedFragments_18 = value;
	}

	inline static int32_t get_offset_of_baseRequest_19() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___baseRequest_19)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_baseRequest_19() const { return ___baseRequest_19; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_baseRequest_19() { return &___baseRequest_19; }
	inline void set_baseRequest_19(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___baseRequest_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseRequest_19), value);
	}

	inline static int32_t get_offset_of_Stream_20() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___Stream_20)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Stream_20() const { return ___Stream_20; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Stream_20() { return &___Stream_20; }
	inline void set_Stream_20(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Stream_20 = value;
		Il2CppCodeGenWriteBarrier((&___Stream_20), value);
	}

	inline static int32_t get_offset_of_fragmentBuffer_21() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___fragmentBuffer_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fragmentBuffer_21() const { return ___fragmentBuffer_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fragmentBuffer_21() { return &___fragmentBuffer_21; }
	inline void set_fragmentBuffer_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fragmentBuffer_21 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentBuffer_21), value);
	}

	inline static int32_t get_offset_of_fragmentBufferDataLength_22() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___fragmentBufferDataLength_22)); }
	inline int32_t get_fragmentBufferDataLength_22() const { return ___fragmentBufferDataLength_22; }
	inline int32_t* get_address_of_fragmentBufferDataLength_22() { return &___fragmentBufferDataLength_22; }
	inline void set_fragmentBufferDataLength_22(int32_t value)
	{
		___fragmentBufferDataLength_22 = value;
	}

	inline static int32_t get_offset_of_cacheStream_23() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___cacheStream_23)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_cacheStream_23() const { return ___cacheStream_23; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_cacheStream_23() { return &___cacheStream_23; }
	inline void set_cacheStream_23(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___cacheStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___cacheStream_23), value);
	}

	inline static int32_t get_offset_of_allFragmentSize_24() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___allFragmentSize_24)); }
	inline int32_t get_allFragmentSize_24() const { return ___allFragmentSize_24; }
	inline int32_t* get_address_of_allFragmentSize_24() { return &___allFragmentSize_24; }
	inline void set_allFragmentSize_24(int32_t value)
	{
		___allFragmentSize_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef ARRAYS_T0CA87B46BFFC75142B35010D1548C2E3F4264D59_H
#define ARRAYS_T0CA87B46BFFC75142B35010D1548C2E3F4264D59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Arrays
struct  Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59  : public RuntimeObject
{
public:

public:
};

struct Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Arrays::EmptyBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBytes_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Arrays::EmptyInts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EmptyInts_1;

public:
	inline static int32_t get_offset_of_EmptyBytes_0() { return static_cast<int32_t>(offsetof(Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59_StaticFields, ___EmptyBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBytes_0() const { return ___EmptyBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBytes_0() { return &___EmptyBytes_0; }
	inline void set_EmptyBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_0), value);
	}

	inline static int32_t get_offset_of_EmptyInts_1() { return static_cast<int32_t>(offsetof(Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59_StaticFields, ___EmptyInts_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EmptyInts_1() const { return ___EmptyInts_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EmptyInts_1() { return &___EmptyInts_1; }
	inline void set_EmptyInts_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EmptyInts_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyInts_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYS_T0CA87B46BFFC75142B35010D1548C2E3F4264D59_H
#ifndef BIGINTEGERS_T27C86ECF602A3E23D3F91044072014BF3BD0CB33_H
#define BIGINTEGERS_T27C86ECF602A3E23D3F91044072014BF3BD0CB33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.BigIntegers
struct  BigIntegers_t27C86ECF602A3E23D3F91044072014BF3BD0CB33  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGERS_T27C86ECF602A3E23D3F91044072014BF3BD0CB33_H
#ifndef ENUMS_T1C4E8BFC1B312878E9C44DA3F1D456A3892B581D_H
#define ENUMS_T1C4E8BFC1B312878E9C44DA3F1D456A3892B581D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Enums
struct  Enums_t1C4E8BFC1B312878E9C44DA3F1D456A3892B581D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMS_T1C4E8BFC1B312878E9C44DA3F1D456A3892B581D_H
#ifndef STREAMS_T2A1A958B8599E84F53102CACBF26991DEABAEEB2_H
#define STREAMS_T2A1A958B8599E84F53102CACBF26991DEABAEEB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Streams
struct  Streams_t2A1A958B8599E84F53102CACBF26991DEABAEEB2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMS_T2A1A958B8599E84F53102CACBF26991DEABAEEB2_H
#ifndef INTEGERS_T11000EAA288DC9AD1138E2BB196334D292921246_H
#define INTEGERS_T11000EAA288DC9AD1138E2BB196334D292921246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Integers
struct  Integers_t11000EAA288DC9AD1138E2BB196334D292921246  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGERS_T11000EAA288DC9AD1138E2BB196334D292921246_H
#ifndef IPADDRESS_TCD751E2DA79B3FA9B096C8EFD4C3BFE697DADCDA_H
#define IPADDRESS_TCD751E2DA79B3FA9B096C8EFD4C3BFE697DADCDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Net.IPAddress
struct  IPAddress_tCD751E2DA79B3FA9B096C8EFD4C3BFE697DADCDA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_TCD751E2DA79B3FA9B096C8EFD4C3BFE697DADCDA_H
#ifndef PLATFORM_T831F01D6ED4908474F1B5D3C147E1566CE9F33A6_H
#define PLATFORM_T831F01D6ED4908474F1B5D3C147E1566CE9F33A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Platform
struct  Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6  : public RuntimeObject
{
public:

public:
};

struct Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6_StaticFields
{
public:
	// System.Globalization.CompareInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Platform::InvariantCompareInfo
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___InvariantCompareInfo_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Platform::NewLine
	String_t* ___NewLine_1;

public:
	inline static int32_t get_offset_of_InvariantCompareInfo_0() { return static_cast<int32_t>(offsetof(Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6_StaticFields, ___InvariantCompareInfo_0)); }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * get_InvariantCompareInfo_0() const { return ___InvariantCompareInfo_0; }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 ** get_address_of_InvariantCompareInfo_0() { return &___InvariantCompareInfo_0; }
	inline void set_InvariantCompareInfo_0(CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * value)
	{
		___InvariantCompareInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___InvariantCompareInfo_0), value);
	}

	inline static int32_t get_offset_of_NewLine_1() { return static_cast<int32_t>(offsetof(Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6_StaticFields, ___NewLine_1)); }
	inline String_t* get_NewLine_1() const { return ___NewLine_1; }
	inline String_t** get_address_of_NewLine_1() { return &___NewLine_1; }
	inline void set_NewLine_1(String_t* value)
	{
		___NewLine_1 = value;
		Il2CppCodeGenWriteBarrier((&___NewLine_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T831F01D6ED4908474F1B5D3C147E1566CE9F33A6_H
#ifndef STRINGS_TE7B66D217DED931E74860F122791837EECA1F936_H
#define STRINGS_TE7B66D217DED931E74860F122791837EECA1F936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Strings
struct  Strings_tE7B66D217DED931E74860F122791837EECA1F936  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGS_TE7B66D217DED931E74860F122791837EECA1F936_H
#ifndef TIMES_TB5570DECE17559D80B13E430F6AE143ECFB50561_H
#define TIMES_TB5570DECE17559D80B13E430F6AE143ECFB50561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Times
struct  Times_tB5570DECE17559D80B13E430F6AE143ECFB50561  : public RuntimeObject
{
public:

public:
};

struct Times_tB5570DECE17559D80B13E430F6AE143ECFB50561_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Times::NanosecondsPerTick
	int64_t ___NanosecondsPerTick_0;

public:
	inline static int32_t get_offset_of_NanosecondsPerTick_0() { return static_cast<int32_t>(offsetof(Times_tB5570DECE17559D80B13E430F6AE143ECFB50561_StaticFields, ___NanosecondsPerTick_0)); }
	inline int64_t get_NanosecondsPerTick_0() const { return ___NanosecondsPerTick_0; }
	inline int64_t* get_address_of_NanosecondsPerTick_0() { return &___NanosecondsPerTick_0; }
	inline void set_NanosecondsPerTick_0(int64_t value)
	{
		___NanosecondsPerTick_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMES_TB5570DECE17559D80B13E430F6AE143ECFB50561_H
#ifndef ADLER32_T0BCA7796796DD2EE4AA97252B3E46854F69B2A39_H
#define ADLER32_T0BCA7796796DD2EE4AA97252B3E46854F69B2A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Adler32
struct  Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLER32_T0BCA7796796DD2EE4AA97252B3E46854F69B2A39_H
#ifndef DEFLATE_T73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_H
#define DEFLATE_T73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate
struct  Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::strm
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___strm_56;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::status
	int32_t ___status_57;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::pending_buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pending_buf_58;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::pending_out
	int32_t ___pending_out_59;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::pending
	int32_t ___pending_60;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::noheader
	int32_t ___noheader_61;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::data_type
	uint8_t ___data_type_62;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::method
	uint8_t ___method_63;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::last_flush
	int32_t ___last_flush_64;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::w_size
	int32_t ___w_size_65;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::w_bits
	int32_t ___w_bits_66;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::w_mask
	int32_t ___w_mask_67;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_68;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::window_size
	int32_t ___window_size_69;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::prev
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___prev_70;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::head
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___head_71;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::ins_h
	int32_t ___ins_h_72;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::hash_size
	int32_t ___hash_size_73;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::hash_bits
	int32_t ___hash_bits_74;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::hash_mask
	int32_t ___hash_mask_75;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::hash_shift
	int32_t ___hash_shift_76;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::block_start
	int32_t ___block_start_77;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::match_length
	int32_t ___match_length_78;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::prev_match
	int32_t ___prev_match_79;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::match_available
	int32_t ___match_available_80;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::strstart
	int32_t ___strstart_81;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::match_start
	int32_t ___match_start_82;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::lookahead
	int32_t ___lookahead_83;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::prev_length
	int32_t ___prev_length_84;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::max_chain_length
	int32_t ___max_chain_length_85;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::max_lazy_match
	int32_t ___max_lazy_match_86;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::level
	int32_t ___level_87;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::strategy
	int32_t ___strategy_88;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::good_match
	int32_t ___good_match_89;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::nice_match
	int32_t ___nice_match_90;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::dyn_ltree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_ltree_91;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::dyn_dtree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_dtree_92;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::bl_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bl_tree_93;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::l_desc
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * ___l_desc_94;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::d_desc
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * ___d_desc_95;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::bl_desc
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * ___bl_desc_96;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::bl_count
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bl_count_97;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::heap
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___heap_98;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::heap_len
	int32_t ___heap_len_99;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::heap_max
	int32_t ___heap_max_100;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::depth
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___depth_101;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::l_buf
	int32_t ___l_buf_102;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::lit_bufsize
	int32_t ___lit_bufsize_103;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::last_lit
	int32_t ___last_lit_104;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::d_buf
	int32_t ___d_buf_105;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::opt_len
	int32_t ___opt_len_106;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::static_len
	int32_t ___static_len_107;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::matches
	int32_t ___matches_108;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::last_eob_len
	int32_t ___last_eob_len_109;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::bi_buf
	uint32_t ___bi_buf_110;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::bi_valid
	int32_t ___bi_valid_111;

public:
	inline static int32_t get_offset_of_strm_56() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___strm_56)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_strm_56() const { return ___strm_56; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_strm_56() { return &___strm_56; }
	inline void set_strm_56(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___strm_56 = value;
		Il2CppCodeGenWriteBarrier((&___strm_56), value);
	}

	inline static int32_t get_offset_of_status_57() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___status_57)); }
	inline int32_t get_status_57() const { return ___status_57; }
	inline int32_t* get_address_of_status_57() { return &___status_57; }
	inline void set_status_57(int32_t value)
	{
		___status_57 = value;
	}

	inline static int32_t get_offset_of_pending_buf_58() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___pending_buf_58)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pending_buf_58() const { return ___pending_buf_58; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pending_buf_58() { return &___pending_buf_58; }
	inline void set_pending_buf_58(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pending_buf_58 = value;
		Il2CppCodeGenWriteBarrier((&___pending_buf_58), value);
	}

	inline static int32_t get_offset_of_pending_out_59() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___pending_out_59)); }
	inline int32_t get_pending_out_59() const { return ___pending_out_59; }
	inline int32_t* get_address_of_pending_out_59() { return &___pending_out_59; }
	inline void set_pending_out_59(int32_t value)
	{
		___pending_out_59 = value;
	}

	inline static int32_t get_offset_of_pending_60() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___pending_60)); }
	inline int32_t get_pending_60() const { return ___pending_60; }
	inline int32_t* get_address_of_pending_60() { return &___pending_60; }
	inline void set_pending_60(int32_t value)
	{
		___pending_60 = value;
	}

	inline static int32_t get_offset_of_noheader_61() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___noheader_61)); }
	inline int32_t get_noheader_61() const { return ___noheader_61; }
	inline int32_t* get_address_of_noheader_61() { return &___noheader_61; }
	inline void set_noheader_61(int32_t value)
	{
		___noheader_61 = value;
	}

	inline static int32_t get_offset_of_data_type_62() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___data_type_62)); }
	inline uint8_t get_data_type_62() const { return ___data_type_62; }
	inline uint8_t* get_address_of_data_type_62() { return &___data_type_62; }
	inline void set_data_type_62(uint8_t value)
	{
		___data_type_62 = value;
	}

	inline static int32_t get_offset_of_method_63() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___method_63)); }
	inline uint8_t get_method_63() const { return ___method_63; }
	inline uint8_t* get_address_of_method_63() { return &___method_63; }
	inline void set_method_63(uint8_t value)
	{
		___method_63 = value;
	}

	inline static int32_t get_offset_of_last_flush_64() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___last_flush_64)); }
	inline int32_t get_last_flush_64() const { return ___last_flush_64; }
	inline int32_t* get_address_of_last_flush_64() { return &___last_flush_64; }
	inline void set_last_flush_64(int32_t value)
	{
		___last_flush_64 = value;
	}

	inline static int32_t get_offset_of_w_size_65() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___w_size_65)); }
	inline int32_t get_w_size_65() const { return ___w_size_65; }
	inline int32_t* get_address_of_w_size_65() { return &___w_size_65; }
	inline void set_w_size_65(int32_t value)
	{
		___w_size_65 = value;
	}

	inline static int32_t get_offset_of_w_bits_66() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___w_bits_66)); }
	inline int32_t get_w_bits_66() const { return ___w_bits_66; }
	inline int32_t* get_address_of_w_bits_66() { return &___w_bits_66; }
	inline void set_w_bits_66(int32_t value)
	{
		___w_bits_66 = value;
	}

	inline static int32_t get_offset_of_w_mask_67() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___w_mask_67)); }
	inline int32_t get_w_mask_67() const { return ___w_mask_67; }
	inline int32_t* get_address_of_w_mask_67() { return &___w_mask_67; }
	inline void set_w_mask_67(int32_t value)
	{
		___w_mask_67 = value;
	}

	inline static int32_t get_offset_of_window_68() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___window_68)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_68() const { return ___window_68; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_68() { return &___window_68; }
	inline void set_window_68(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_68 = value;
		Il2CppCodeGenWriteBarrier((&___window_68), value);
	}

	inline static int32_t get_offset_of_window_size_69() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___window_size_69)); }
	inline int32_t get_window_size_69() const { return ___window_size_69; }
	inline int32_t* get_address_of_window_size_69() { return &___window_size_69; }
	inline void set_window_size_69(int32_t value)
	{
		___window_size_69 = value;
	}

	inline static int32_t get_offset_of_prev_70() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___prev_70)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_prev_70() const { return ___prev_70; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_prev_70() { return &___prev_70; }
	inline void set_prev_70(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___prev_70 = value;
		Il2CppCodeGenWriteBarrier((&___prev_70), value);
	}

	inline static int32_t get_offset_of_head_71() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___head_71)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_head_71() const { return ___head_71; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_head_71() { return &___head_71; }
	inline void set_head_71(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___head_71 = value;
		Il2CppCodeGenWriteBarrier((&___head_71), value);
	}

	inline static int32_t get_offset_of_ins_h_72() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___ins_h_72)); }
	inline int32_t get_ins_h_72() const { return ___ins_h_72; }
	inline int32_t* get_address_of_ins_h_72() { return &___ins_h_72; }
	inline void set_ins_h_72(int32_t value)
	{
		___ins_h_72 = value;
	}

	inline static int32_t get_offset_of_hash_size_73() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___hash_size_73)); }
	inline int32_t get_hash_size_73() const { return ___hash_size_73; }
	inline int32_t* get_address_of_hash_size_73() { return &___hash_size_73; }
	inline void set_hash_size_73(int32_t value)
	{
		___hash_size_73 = value;
	}

	inline static int32_t get_offset_of_hash_bits_74() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___hash_bits_74)); }
	inline int32_t get_hash_bits_74() const { return ___hash_bits_74; }
	inline int32_t* get_address_of_hash_bits_74() { return &___hash_bits_74; }
	inline void set_hash_bits_74(int32_t value)
	{
		___hash_bits_74 = value;
	}

	inline static int32_t get_offset_of_hash_mask_75() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___hash_mask_75)); }
	inline int32_t get_hash_mask_75() const { return ___hash_mask_75; }
	inline int32_t* get_address_of_hash_mask_75() { return &___hash_mask_75; }
	inline void set_hash_mask_75(int32_t value)
	{
		___hash_mask_75 = value;
	}

	inline static int32_t get_offset_of_hash_shift_76() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___hash_shift_76)); }
	inline int32_t get_hash_shift_76() const { return ___hash_shift_76; }
	inline int32_t* get_address_of_hash_shift_76() { return &___hash_shift_76; }
	inline void set_hash_shift_76(int32_t value)
	{
		___hash_shift_76 = value;
	}

	inline static int32_t get_offset_of_block_start_77() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___block_start_77)); }
	inline int32_t get_block_start_77() const { return ___block_start_77; }
	inline int32_t* get_address_of_block_start_77() { return &___block_start_77; }
	inline void set_block_start_77(int32_t value)
	{
		___block_start_77 = value;
	}

	inline static int32_t get_offset_of_match_length_78() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___match_length_78)); }
	inline int32_t get_match_length_78() const { return ___match_length_78; }
	inline int32_t* get_address_of_match_length_78() { return &___match_length_78; }
	inline void set_match_length_78(int32_t value)
	{
		___match_length_78 = value;
	}

	inline static int32_t get_offset_of_prev_match_79() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___prev_match_79)); }
	inline int32_t get_prev_match_79() const { return ___prev_match_79; }
	inline int32_t* get_address_of_prev_match_79() { return &___prev_match_79; }
	inline void set_prev_match_79(int32_t value)
	{
		___prev_match_79 = value;
	}

	inline static int32_t get_offset_of_match_available_80() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___match_available_80)); }
	inline int32_t get_match_available_80() const { return ___match_available_80; }
	inline int32_t* get_address_of_match_available_80() { return &___match_available_80; }
	inline void set_match_available_80(int32_t value)
	{
		___match_available_80 = value;
	}

	inline static int32_t get_offset_of_strstart_81() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___strstart_81)); }
	inline int32_t get_strstart_81() const { return ___strstart_81; }
	inline int32_t* get_address_of_strstart_81() { return &___strstart_81; }
	inline void set_strstart_81(int32_t value)
	{
		___strstart_81 = value;
	}

	inline static int32_t get_offset_of_match_start_82() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___match_start_82)); }
	inline int32_t get_match_start_82() const { return ___match_start_82; }
	inline int32_t* get_address_of_match_start_82() { return &___match_start_82; }
	inline void set_match_start_82(int32_t value)
	{
		___match_start_82 = value;
	}

	inline static int32_t get_offset_of_lookahead_83() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___lookahead_83)); }
	inline int32_t get_lookahead_83() const { return ___lookahead_83; }
	inline int32_t* get_address_of_lookahead_83() { return &___lookahead_83; }
	inline void set_lookahead_83(int32_t value)
	{
		___lookahead_83 = value;
	}

	inline static int32_t get_offset_of_prev_length_84() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___prev_length_84)); }
	inline int32_t get_prev_length_84() const { return ___prev_length_84; }
	inline int32_t* get_address_of_prev_length_84() { return &___prev_length_84; }
	inline void set_prev_length_84(int32_t value)
	{
		___prev_length_84 = value;
	}

	inline static int32_t get_offset_of_max_chain_length_85() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___max_chain_length_85)); }
	inline int32_t get_max_chain_length_85() const { return ___max_chain_length_85; }
	inline int32_t* get_address_of_max_chain_length_85() { return &___max_chain_length_85; }
	inline void set_max_chain_length_85(int32_t value)
	{
		___max_chain_length_85 = value;
	}

	inline static int32_t get_offset_of_max_lazy_match_86() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___max_lazy_match_86)); }
	inline int32_t get_max_lazy_match_86() const { return ___max_lazy_match_86; }
	inline int32_t* get_address_of_max_lazy_match_86() { return &___max_lazy_match_86; }
	inline void set_max_lazy_match_86(int32_t value)
	{
		___max_lazy_match_86 = value;
	}

	inline static int32_t get_offset_of_level_87() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___level_87)); }
	inline int32_t get_level_87() const { return ___level_87; }
	inline int32_t* get_address_of_level_87() { return &___level_87; }
	inline void set_level_87(int32_t value)
	{
		___level_87 = value;
	}

	inline static int32_t get_offset_of_strategy_88() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___strategy_88)); }
	inline int32_t get_strategy_88() const { return ___strategy_88; }
	inline int32_t* get_address_of_strategy_88() { return &___strategy_88; }
	inline void set_strategy_88(int32_t value)
	{
		___strategy_88 = value;
	}

	inline static int32_t get_offset_of_good_match_89() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___good_match_89)); }
	inline int32_t get_good_match_89() const { return ___good_match_89; }
	inline int32_t* get_address_of_good_match_89() { return &___good_match_89; }
	inline void set_good_match_89(int32_t value)
	{
		___good_match_89 = value;
	}

	inline static int32_t get_offset_of_nice_match_90() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___nice_match_90)); }
	inline int32_t get_nice_match_90() const { return ___nice_match_90; }
	inline int32_t* get_address_of_nice_match_90() { return &___nice_match_90; }
	inline void set_nice_match_90(int32_t value)
	{
		___nice_match_90 = value;
	}

	inline static int32_t get_offset_of_dyn_ltree_91() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___dyn_ltree_91)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_ltree_91() const { return ___dyn_ltree_91; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_ltree_91() { return &___dyn_ltree_91; }
	inline void set_dyn_ltree_91(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_ltree_91 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_ltree_91), value);
	}

	inline static int32_t get_offset_of_dyn_dtree_92() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___dyn_dtree_92)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_dtree_92() const { return ___dyn_dtree_92; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_dtree_92() { return &___dyn_dtree_92; }
	inline void set_dyn_dtree_92(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_dtree_92 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_dtree_92), value);
	}

	inline static int32_t get_offset_of_bl_tree_93() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___bl_tree_93)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bl_tree_93() const { return ___bl_tree_93; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bl_tree_93() { return &___bl_tree_93; }
	inline void set_bl_tree_93(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bl_tree_93 = value;
		Il2CppCodeGenWriteBarrier((&___bl_tree_93), value);
	}

	inline static int32_t get_offset_of_l_desc_94() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___l_desc_94)); }
	inline ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * get_l_desc_94() const { return ___l_desc_94; }
	inline ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 ** get_address_of_l_desc_94() { return &___l_desc_94; }
	inline void set_l_desc_94(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * value)
	{
		___l_desc_94 = value;
		Il2CppCodeGenWriteBarrier((&___l_desc_94), value);
	}

	inline static int32_t get_offset_of_d_desc_95() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___d_desc_95)); }
	inline ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * get_d_desc_95() const { return ___d_desc_95; }
	inline ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 ** get_address_of_d_desc_95() { return &___d_desc_95; }
	inline void set_d_desc_95(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * value)
	{
		___d_desc_95 = value;
		Il2CppCodeGenWriteBarrier((&___d_desc_95), value);
	}

	inline static int32_t get_offset_of_bl_desc_96() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___bl_desc_96)); }
	inline ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * get_bl_desc_96() const { return ___bl_desc_96; }
	inline ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 ** get_address_of_bl_desc_96() { return &___bl_desc_96; }
	inline void set_bl_desc_96(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36 * value)
	{
		___bl_desc_96 = value;
		Il2CppCodeGenWriteBarrier((&___bl_desc_96), value);
	}

	inline static int32_t get_offset_of_bl_count_97() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___bl_count_97)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bl_count_97() const { return ___bl_count_97; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bl_count_97() { return &___bl_count_97; }
	inline void set_bl_count_97(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bl_count_97 = value;
		Il2CppCodeGenWriteBarrier((&___bl_count_97), value);
	}

	inline static int32_t get_offset_of_heap_98() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___heap_98)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_heap_98() const { return ___heap_98; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_heap_98() { return &___heap_98; }
	inline void set_heap_98(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___heap_98 = value;
		Il2CppCodeGenWriteBarrier((&___heap_98), value);
	}

	inline static int32_t get_offset_of_heap_len_99() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___heap_len_99)); }
	inline int32_t get_heap_len_99() const { return ___heap_len_99; }
	inline int32_t* get_address_of_heap_len_99() { return &___heap_len_99; }
	inline void set_heap_len_99(int32_t value)
	{
		___heap_len_99 = value;
	}

	inline static int32_t get_offset_of_heap_max_100() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___heap_max_100)); }
	inline int32_t get_heap_max_100() const { return ___heap_max_100; }
	inline int32_t* get_address_of_heap_max_100() { return &___heap_max_100; }
	inline void set_heap_max_100(int32_t value)
	{
		___heap_max_100 = value;
	}

	inline static int32_t get_offset_of_depth_101() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___depth_101)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_depth_101() const { return ___depth_101; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_depth_101() { return &___depth_101; }
	inline void set_depth_101(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___depth_101 = value;
		Il2CppCodeGenWriteBarrier((&___depth_101), value);
	}

	inline static int32_t get_offset_of_l_buf_102() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___l_buf_102)); }
	inline int32_t get_l_buf_102() const { return ___l_buf_102; }
	inline int32_t* get_address_of_l_buf_102() { return &___l_buf_102; }
	inline void set_l_buf_102(int32_t value)
	{
		___l_buf_102 = value;
	}

	inline static int32_t get_offset_of_lit_bufsize_103() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___lit_bufsize_103)); }
	inline int32_t get_lit_bufsize_103() const { return ___lit_bufsize_103; }
	inline int32_t* get_address_of_lit_bufsize_103() { return &___lit_bufsize_103; }
	inline void set_lit_bufsize_103(int32_t value)
	{
		___lit_bufsize_103 = value;
	}

	inline static int32_t get_offset_of_last_lit_104() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___last_lit_104)); }
	inline int32_t get_last_lit_104() const { return ___last_lit_104; }
	inline int32_t* get_address_of_last_lit_104() { return &___last_lit_104; }
	inline void set_last_lit_104(int32_t value)
	{
		___last_lit_104 = value;
	}

	inline static int32_t get_offset_of_d_buf_105() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___d_buf_105)); }
	inline int32_t get_d_buf_105() const { return ___d_buf_105; }
	inline int32_t* get_address_of_d_buf_105() { return &___d_buf_105; }
	inline void set_d_buf_105(int32_t value)
	{
		___d_buf_105 = value;
	}

	inline static int32_t get_offset_of_opt_len_106() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___opt_len_106)); }
	inline int32_t get_opt_len_106() const { return ___opt_len_106; }
	inline int32_t* get_address_of_opt_len_106() { return &___opt_len_106; }
	inline void set_opt_len_106(int32_t value)
	{
		___opt_len_106 = value;
	}

	inline static int32_t get_offset_of_static_len_107() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___static_len_107)); }
	inline int32_t get_static_len_107() const { return ___static_len_107; }
	inline int32_t* get_address_of_static_len_107() { return &___static_len_107; }
	inline void set_static_len_107(int32_t value)
	{
		___static_len_107 = value;
	}

	inline static int32_t get_offset_of_matches_108() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___matches_108)); }
	inline int32_t get_matches_108() const { return ___matches_108; }
	inline int32_t* get_address_of_matches_108() { return &___matches_108; }
	inline void set_matches_108(int32_t value)
	{
		___matches_108 = value;
	}

	inline static int32_t get_offset_of_last_eob_len_109() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___last_eob_len_109)); }
	inline int32_t get_last_eob_len_109() const { return ___last_eob_len_109; }
	inline int32_t* get_address_of_last_eob_len_109() { return &___last_eob_len_109; }
	inline void set_last_eob_len_109(int32_t value)
	{
		___last_eob_len_109 = value;
	}

	inline static int32_t get_offset_of_bi_buf_110() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___bi_buf_110)); }
	inline uint32_t get_bi_buf_110() const { return ___bi_buf_110; }
	inline uint32_t* get_address_of_bi_buf_110() { return &___bi_buf_110; }
	inline void set_bi_buf_110(uint32_t value)
	{
		___bi_buf_110 = value;
	}

	inline static int32_t get_offset_of_bi_valid_111() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA, ___bi_valid_111)); }
	inline int32_t get_bi_valid_111() const { return ___bi_valid_111; }
	inline int32_t* get_address_of_bi_valid_111() { return &___bi_valid_111; }
	inline void set_bi_valid_111(int32_t value)
	{
		___bi_valid_111 = value;
	}
};

struct Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::config_table
	ConfigU5BU5D_t51532B61CE88D8BC4B7B20FDE7B891355BCA6AD4* ___config_table_7;
	// System.String[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate::z_errmsg
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___z_errmsg_8;

public:
	inline static int32_t get_offset_of_config_table_7() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_StaticFields, ___config_table_7)); }
	inline ConfigU5BU5D_t51532B61CE88D8BC4B7B20FDE7B891355BCA6AD4* get_config_table_7() const { return ___config_table_7; }
	inline ConfigU5BU5D_t51532B61CE88D8BC4B7B20FDE7B891355BCA6AD4** get_address_of_config_table_7() { return &___config_table_7; }
	inline void set_config_table_7(ConfigU5BU5D_t51532B61CE88D8BC4B7B20FDE7B891355BCA6AD4* value)
	{
		___config_table_7 = value;
		Il2CppCodeGenWriteBarrier((&___config_table_7), value);
	}

	inline static int32_t get_offset_of_z_errmsg_8() { return static_cast<int32_t>(offsetof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_StaticFields, ___z_errmsg_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_z_errmsg_8() const { return ___z_errmsg_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_z_errmsg_8() { return &___z_errmsg_8; }
	inline void set_z_errmsg_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___z_errmsg_8 = value;
		Il2CppCodeGenWriteBarrier((&___z_errmsg_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATE_T73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_H
#ifndef CONFIG_T569D9515884EFF9E53743422FFF723B4BFE2A4CC_H
#define CONFIG_T569D9515884EFF9E53743422FFF723B4BFE2A4CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config
struct  Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config::good_length
	int32_t ___good_length_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config::max_lazy
	int32_t ___max_lazy_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config::nice_length
	int32_t ___nice_length_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config::max_chain
	int32_t ___max_chain_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate_Config::func
	int32_t ___func_4;

public:
	inline static int32_t get_offset_of_good_length_0() { return static_cast<int32_t>(offsetof(Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC, ___good_length_0)); }
	inline int32_t get_good_length_0() const { return ___good_length_0; }
	inline int32_t* get_address_of_good_length_0() { return &___good_length_0; }
	inline void set_good_length_0(int32_t value)
	{
		___good_length_0 = value;
	}

	inline static int32_t get_offset_of_max_lazy_1() { return static_cast<int32_t>(offsetof(Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC, ___max_lazy_1)); }
	inline int32_t get_max_lazy_1() const { return ___max_lazy_1; }
	inline int32_t* get_address_of_max_lazy_1() { return &___max_lazy_1; }
	inline void set_max_lazy_1(int32_t value)
	{
		___max_lazy_1 = value;
	}

	inline static int32_t get_offset_of_nice_length_2() { return static_cast<int32_t>(offsetof(Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC, ___nice_length_2)); }
	inline int32_t get_nice_length_2() const { return ___nice_length_2; }
	inline int32_t* get_address_of_nice_length_2() { return &___nice_length_2; }
	inline void set_nice_length_2(int32_t value)
	{
		___nice_length_2 = value;
	}

	inline static int32_t get_offset_of_max_chain_3() { return static_cast<int32_t>(offsetof(Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC, ___max_chain_3)); }
	inline int32_t get_max_chain_3() const { return ___max_chain_3; }
	inline int32_t* get_address_of_max_chain_3() { return &___max_chain_3; }
	inline void set_max_chain_3(int32_t value)
	{
		___max_chain_3 = value;
	}

	inline static int32_t get_offset_of_func_4() { return static_cast<int32_t>(offsetof(Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC, ___func_4)); }
	inline int32_t get_func_4() const { return ___func_4; }
	inline int32_t* get_address_of_func_4() { return &___func_4; }
	inline void set_func_4(int32_t value)
	{
		___func_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T569D9515884EFF9E53743422FFF723B4BFE2A4CC_H
#ifndef INFBLOCKS_TA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_H
#define INFBLOCKS_TA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks
struct  InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::mode
	int32_t ___mode_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::left
	int32_t ___left_23;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::table
	int32_t ___table_24;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::index
	int32_t ___index_25;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::blens
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___blens_26;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::bb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bb_27;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::tb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tb_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::codes
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711 * ___codes_29;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::last
	int32_t ___last_30;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::bitk
	int32_t ___bitk_31;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::bitb
	int32_t ___bitb_32;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::hufts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hufts_33;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_34;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::end
	int32_t ___end_35;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::read
	int32_t ___read_36;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::write
	int32_t ___write_37;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::checkfn
	RuntimeObject * ___checkfn_38;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::check
	int64_t ___check_39;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::inftree
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1 * ___inftree_40;

public:
	inline static int32_t get_offset_of_mode_22() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___mode_22)); }
	inline int32_t get_mode_22() const { return ___mode_22; }
	inline int32_t* get_address_of_mode_22() { return &___mode_22; }
	inline void set_mode_22(int32_t value)
	{
		___mode_22 = value;
	}

	inline static int32_t get_offset_of_left_23() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___left_23)); }
	inline int32_t get_left_23() const { return ___left_23; }
	inline int32_t* get_address_of_left_23() { return &___left_23; }
	inline void set_left_23(int32_t value)
	{
		___left_23 = value;
	}

	inline static int32_t get_offset_of_table_24() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___table_24)); }
	inline int32_t get_table_24() const { return ___table_24; }
	inline int32_t* get_address_of_table_24() { return &___table_24; }
	inline void set_table_24(int32_t value)
	{
		___table_24 = value;
	}

	inline static int32_t get_offset_of_index_25() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___index_25)); }
	inline int32_t get_index_25() const { return ___index_25; }
	inline int32_t* get_address_of_index_25() { return &___index_25; }
	inline void set_index_25(int32_t value)
	{
		___index_25 = value;
	}

	inline static int32_t get_offset_of_blens_26() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___blens_26)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_blens_26() const { return ___blens_26; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_blens_26() { return &___blens_26; }
	inline void set_blens_26(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___blens_26 = value;
		Il2CppCodeGenWriteBarrier((&___blens_26), value);
	}

	inline static int32_t get_offset_of_bb_27() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___bb_27)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bb_27() const { return ___bb_27; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bb_27() { return &___bb_27; }
	inline void set_bb_27(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bb_27 = value;
		Il2CppCodeGenWriteBarrier((&___bb_27), value);
	}

	inline static int32_t get_offset_of_tb_28() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___tb_28)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tb_28() const { return ___tb_28; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tb_28() { return &___tb_28; }
	inline void set_tb_28(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tb_28 = value;
		Il2CppCodeGenWriteBarrier((&___tb_28), value);
	}

	inline static int32_t get_offset_of_codes_29() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___codes_29)); }
	inline InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711 * get_codes_29() const { return ___codes_29; }
	inline InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711 ** get_address_of_codes_29() { return &___codes_29; }
	inline void set_codes_29(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711 * value)
	{
		___codes_29 = value;
		Il2CppCodeGenWriteBarrier((&___codes_29), value);
	}

	inline static int32_t get_offset_of_last_30() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___last_30)); }
	inline int32_t get_last_30() const { return ___last_30; }
	inline int32_t* get_address_of_last_30() { return &___last_30; }
	inline void set_last_30(int32_t value)
	{
		___last_30 = value;
	}

	inline static int32_t get_offset_of_bitk_31() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___bitk_31)); }
	inline int32_t get_bitk_31() const { return ___bitk_31; }
	inline int32_t* get_address_of_bitk_31() { return &___bitk_31; }
	inline void set_bitk_31(int32_t value)
	{
		___bitk_31 = value;
	}

	inline static int32_t get_offset_of_bitb_32() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___bitb_32)); }
	inline int32_t get_bitb_32() const { return ___bitb_32; }
	inline int32_t* get_address_of_bitb_32() { return &___bitb_32; }
	inline void set_bitb_32(int32_t value)
	{
		___bitb_32 = value;
	}

	inline static int32_t get_offset_of_hufts_33() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___hufts_33)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hufts_33() const { return ___hufts_33; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hufts_33() { return &___hufts_33; }
	inline void set_hufts_33(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hufts_33 = value;
		Il2CppCodeGenWriteBarrier((&___hufts_33), value);
	}

	inline static int32_t get_offset_of_window_34() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___window_34)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_34() const { return ___window_34; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_34() { return &___window_34; }
	inline void set_window_34(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_34 = value;
		Il2CppCodeGenWriteBarrier((&___window_34), value);
	}

	inline static int32_t get_offset_of_end_35() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___end_35)); }
	inline int32_t get_end_35() const { return ___end_35; }
	inline int32_t* get_address_of_end_35() { return &___end_35; }
	inline void set_end_35(int32_t value)
	{
		___end_35 = value;
	}

	inline static int32_t get_offset_of_read_36() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___read_36)); }
	inline int32_t get_read_36() const { return ___read_36; }
	inline int32_t* get_address_of_read_36() { return &___read_36; }
	inline void set_read_36(int32_t value)
	{
		___read_36 = value;
	}

	inline static int32_t get_offset_of_write_37() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___write_37)); }
	inline int32_t get_write_37() const { return ___write_37; }
	inline int32_t* get_address_of_write_37() { return &___write_37; }
	inline void set_write_37(int32_t value)
	{
		___write_37 = value;
	}

	inline static int32_t get_offset_of_checkfn_38() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___checkfn_38)); }
	inline RuntimeObject * get_checkfn_38() const { return ___checkfn_38; }
	inline RuntimeObject ** get_address_of_checkfn_38() { return &___checkfn_38; }
	inline void set_checkfn_38(RuntimeObject * value)
	{
		___checkfn_38 = value;
		Il2CppCodeGenWriteBarrier((&___checkfn_38), value);
	}

	inline static int32_t get_offset_of_check_39() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___check_39)); }
	inline int64_t get_check_39() const { return ___check_39; }
	inline int64_t* get_address_of_check_39() { return &___check_39; }
	inline void set_check_39(int64_t value)
	{
		___check_39 = value;
	}

	inline static int32_t get_offset_of_inftree_40() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D, ___inftree_40)); }
	inline InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1 * get_inftree_40() const { return ___inftree_40; }
	inline InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1 ** get_address_of_inftree_40() { return &___inftree_40; }
	inline void set_inftree_40(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1 * value)
	{
		___inftree_40 = value;
		Il2CppCodeGenWriteBarrier((&___inftree_40), value);
	}
};

struct InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::inflate_mask
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___inflate_mask_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks::border
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___border_2;

public:
	inline static int32_t get_offset_of_inflate_mask_1() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_StaticFields, ___inflate_mask_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_inflate_mask_1() const { return ___inflate_mask_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_inflate_mask_1() { return &___inflate_mask_1; }
	inline void set_inflate_mask_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___inflate_mask_1 = value;
		Il2CppCodeGenWriteBarrier((&___inflate_mask_1), value);
	}

	inline static int32_t get_offset_of_border_2() { return static_cast<int32_t>(offsetof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_StaticFields, ___border_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_border_2() const { return ___border_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_border_2() { return &___border_2; }
	inline void set_border_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___border_2 = value;
		Il2CppCodeGenWriteBarrier((&___border_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFBLOCKS_TA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_H
#ifndef INFCODES_T738730CDE57F83544ADFE0C29244F33FE07AB711_H
#define INFCODES_T738730CDE57F83544ADFE0C29244F33FE07AB711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes
struct  InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::mode
	int32_t ___mode_20;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::len
	int32_t ___len_21;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::tree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tree_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::tree_index
	int32_t ___tree_index_23;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::need
	int32_t ___need_24;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::lit
	int32_t ___lit_25;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::get
	int32_t ___get_26;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::dist
	int32_t ___dist_27;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::lbits
	uint8_t ___lbits_28;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::dbits
	uint8_t ___dbits_29;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::ltree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ltree_30;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::ltree_index
	int32_t ___ltree_index_31;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::dtree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___dtree_32;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::dtree_index
	int32_t ___dtree_index_33;

public:
	inline static int32_t get_offset_of_mode_20() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___mode_20)); }
	inline int32_t get_mode_20() const { return ___mode_20; }
	inline int32_t* get_address_of_mode_20() { return &___mode_20; }
	inline void set_mode_20(int32_t value)
	{
		___mode_20 = value;
	}

	inline static int32_t get_offset_of_len_21() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___len_21)); }
	inline int32_t get_len_21() const { return ___len_21; }
	inline int32_t* get_address_of_len_21() { return &___len_21; }
	inline void set_len_21(int32_t value)
	{
		___len_21 = value;
	}

	inline static int32_t get_offset_of_tree_22() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___tree_22)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tree_22() const { return ___tree_22; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tree_22() { return &___tree_22; }
	inline void set_tree_22(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tree_22 = value;
		Il2CppCodeGenWriteBarrier((&___tree_22), value);
	}

	inline static int32_t get_offset_of_tree_index_23() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___tree_index_23)); }
	inline int32_t get_tree_index_23() const { return ___tree_index_23; }
	inline int32_t* get_address_of_tree_index_23() { return &___tree_index_23; }
	inline void set_tree_index_23(int32_t value)
	{
		___tree_index_23 = value;
	}

	inline static int32_t get_offset_of_need_24() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___need_24)); }
	inline int32_t get_need_24() const { return ___need_24; }
	inline int32_t* get_address_of_need_24() { return &___need_24; }
	inline void set_need_24(int32_t value)
	{
		___need_24 = value;
	}

	inline static int32_t get_offset_of_lit_25() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___lit_25)); }
	inline int32_t get_lit_25() const { return ___lit_25; }
	inline int32_t* get_address_of_lit_25() { return &___lit_25; }
	inline void set_lit_25(int32_t value)
	{
		___lit_25 = value;
	}

	inline static int32_t get_offset_of_get_26() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___get_26)); }
	inline int32_t get_get_26() const { return ___get_26; }
	inline int32_t* get_address_of_get_26() { return &___get_26; }
	inline void set_get_26(int32_t value)
	{
		___get_26 = value;
	}

	inline static int32_t get_offset_of_dist_27() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___dist_27)); }
	inline int32_t get_dist_27() const { return ___dist_27; }
	inline int32_t* get_address_of_dist_27() { return &___dist_27; }
	inline void set_dist_27(int32_t value)
	{
		___dist_27 = value;
	}

	inline static int32_t get_offset_of_lbits_28() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___lbits_28)); }
	inline uint8_t get_lbits_28() const { return ___lbits_28; }
	inline uint8_t* get_address_of_lbits_28() { return &___lbits_28; }
	inline void set_lbits_28(uint8_t value)
	{
		___lbits_28 = value;
	}

	inline static int32_t get_offset_of_dbits_29() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___dbits_29)); }
	inline uint8_t get_dbits_29() const { return ___dbits_29; }
	inline uint8_t* get_address_of_dbits_29() { return &___dbits_29; }
	inline void set_dbits_29(uint8_t value)
	{
		___dbits_29 = value;
	}

	inline static int32_t get_offset_of_ltree_30() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___ltree_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ltree_30() const { return ___ltree_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ltree_30() { return &___ltree_30; }
	inline void set_ltree_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ltree_30 = value;
		Il2CppCodeGenWriteBarrier((&___ltree_30), value);
	}

	inline static int32_t get_offset_of_ltree_index_31() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___ltree_index_31)); }
	inline int32_t get_ltree_index_31() const { return ___ltree_index_31; }
	inline int32_t* get_address_of_ltree_index_31() { return &___ltree_index_31; }
	inline void set_ltree_index_31(int32_t value)
	{
		___ltree_index_31 = value;
	}

	inline static int32_t get_offset_of_dtree_32() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___dtree_32)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_dtree_32() const { return ___dtree_32; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_dtree_32() { return &___dtree_32; }
	inline void set_dtree_32(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___dtree_32 = value;
		Il2CppCodeGenWriteBarrier((&___dtree_32), value);
	}

	inline static int32_t get_offset_of_dtree_index_33() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711, ___dtree_index_33)); }
	inline int32_t get_dtree_index_33() const { return ___dtree_index_33; }
	inline int32_t* get_address_of_dtree_index_33() { return &___dtree_index_33; }
	inline void set_dtree_index_33(int32_t value)
	{
		___dtree_index_33 = value;
	}
};

struct InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfCodes::inflate_mask
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___inflate_mask_0;

public:
	inline static int32_t get_offset_of_inflate_mask_0() { return static_cast<int32_t>(offsetof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711_StaticFields, ___inflate_mask_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_inflate_mask_0() const { return ___inflate_mask_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_inflate_mask_0() { return &___inflate_mask_0; }
	inline void set_inflate_mask_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___inflate_mask_0 = value;
		Il2CppCodeGenWriteBarrier((&___inflate_mask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFCODES_T738730CDE57F83544ADFE0C29244F33FE07AB711_H
#ifndef INFTREE_T50F048E4F49FDBE2632634B7B3F35226E77FBCD1_H
#define INFTREE_T50F048E4F49FDBE2632634B7B3F35226E77FBCD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree
struct  InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::hn
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hn_19;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::v
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___v_20;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::c
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___c_21;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::r
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___r_22;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::u
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___u_23;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::x
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___x_24;

public:
	inline static int32_t get_offset_of_hn_19() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1, ___hn_19)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hn_19() const { return ___hn_19; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hn_19() { return &___hn_19; }
	inline void set_hn_19(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hn_19 = value;
		Il2CppCodeGenWriteBarrier((&___hn_19), value);
	}

	inline static int32_t get_offset_of_v_20() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1, ___v_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_v_20() const { return ___v_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_v_20() { return &___v_20; }
	inline void set_v_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___v_20 = value;
		Il2CppCodeGenWriteBarrier((&___v_20), value);
	}

	inline static int32_t get_offset_of_c_21() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1, ___c_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_c_21() const { return ___c_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_c_21() { return &___c_21; }
	inline void set_c_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___c_21 = value;
		Il2CppCodeGenWriteBarrier((&___c_21), value);
	}

	inline static int32_t get_offset_of_r_22() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1, ___r_22)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_r_22() const { return ___r_22; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_r_22() { return &___r_22; }
	inline void set_r_22(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___r_22 = value;
		Il2CppCodeGenWriteBarrier((&___r_22), value);
	}

	inline static int32_t get_offset_of_u_23() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1, ___u_23)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_u_23() const { return ___u_23; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_u_23() { return &___u_23; }
	inline void set_u_23(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___u_23 = value;
		Il2CppCodeGenWriteBarrier((&___u_23), value);
	}

	inline static int32_t get_offset_of_x_24() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1, ___x_24)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_x_24() const { return ___x_24; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_x_24() { return &___x_24; }
	inline void set_x_24(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___x_24 = value;
		Il2CppCodeGenWriteBarrier((&___x_24), value);
	}
};

struct InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::fixed_tl
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fixed_tl_12;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::fixed_td
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fixed_td_13;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::cplens
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cplens_14;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::cplext
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cplext_15;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::cpdist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cpdist_16;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfTree::cpdext
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cpdext_17;

public:
	inline static int32_t get_offset_of_fixed_tl_12() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields, ___fixed_tl_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fixed_tl_12() const { return ___fixed_tl_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fixed_tl_12() { return &___fixed_tl_12; }
	inline void set_fixed_tl_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fixed_tl_12 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_tl_12), value);
	}

	inline static int32_t get_offset_of_fixed_td_13() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields, ___fixed_td_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fixed_td_13() const { return ___fixed_td_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fixed_td_13() { return &___fixed_td_13; }
	inline void set_fixed_td_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fixed_td_13 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_td_13), value);
	}

	inline static int32_t get_offset_of_cplens_14() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields, ___cplens_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cplens_14() const { return ___cplens_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cplens_14() { return &___cplens_14; }
	inline void set_cplens_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cplens_14 = value;
		Il2CppCodeGenWriteBarrier((&___cplens_14), value);
	}

	inline static int32_t get_offset_of_cplext_15() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields, ___cplext_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cplext_15() const { return ___cplext_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cplext_15() { return &___cplext_15; }
	inline void set_cplext_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cplext_15 = value;
		Il2CppCodeGenWriteBarrier((&___cplext_15), value);
	}

	inline static int32_t get_offset_of_cpdist_16() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields, ___cpdist_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cpdist_16() const { return ___cpdist_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cpdist_16() { return &___cpdist_16; }
	inline void set_cpdist_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cpdist_16 = value;
		Il2CppCodeGenWriteBarrier((&___cpdist_16), value);
	}

	inline static int32_t get_offset_of_cpdext_17() { return static_cast<int32_t>(offsetof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields, ___cpdext_17)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cpdext_17() const { return ___cpdext_17; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cpdext_17() { return &___cpdext_17; }
	inline void set_cpdext_17(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cpdext_17 = value;
		Il2CppCodeGenWriteBarrier((&___cpdext_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFTREE_T50F048E4F49FDBE2632634B7B3F35226E77FBCD1_H
#ifndef INFLATE_TA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_H
#define INFLATE_TA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate
struct  Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::mode
	int32_t ___mode_31;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::method
	int32_t ___method_32;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::was
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___was_33;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::need
	int64_t ___need_34;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::marker
	int32_t ___marker_35;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::nowrap
	int32_t ___nowrap_36;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::wbits
	int32_t ___wbits_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.InfBlocks BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::blocks
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D * ___blocks_38;

public:
	inline static int32_t get_offset_of_mode_31() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___mode_31)); }
	inline int32_t get_mode_31() const { return ___mode_31; }
	inline int32_t* get_address_of_mode_31() { return &___mode_31; }
	inline void set_mode_31(int32_t value)
	{
		___mode_31 = value;
	}

	inline static int32_t get_offset_of_method_32() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___method_32)); }
	inline int32_t get_method_32() const { return ___method_32; }
	inline int32_t* get_address_of_method_32() { return &___method_32; }
	inline void set_method_32(int32_t value)
	{
		___method_32 = value;
	}

	inline static int32_t get_offset_of_was_33() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___was_33)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_was_33() const { return ___was_33; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_was_33() { return &___was_33; }
	inline void set_was_33(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___was_33 = value;
		Il2CppCodeGenWriteBarrier((&___was_33), value);
	}

	inline static int32_t get_offset_of_need_34() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___need_34)); }
	inline int64_t get_need_34() const { return ___need_34; }
	inline int64_t* get_address_of_need_34() { return &___need_34; }
	inline void set_need_34(int64_t value)
	{
		___need_34 = value;
	}

	inline static int32_t get_offset_of_marker_35() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___marker_35)); }
	inline int32_t get_marker_35() const { return ___marker_35; }
	inline int32_t* get_address_of_marker_35() { return &___marker_35; }
	inline void set_marker_35(int32_t value)
	{
		___marker_35 = value;
	}

	inline static int32_t get_offset_of_nowrap_36() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___nowrap_36)); }
	inline int32_t get_nowrap_36() const { return ___nowrap_36; }
	inline int32_t* get_address_of_nowrap_36() { return &___nowrap_36; }
	inline void set_nowrap_36(int32_t value)
	{
		___nowrap_36 = value;
	}

	inline static int32_t get_offset_of_wbits_37() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___wbits_37)); }
	inline int32_t get_wbits_37() const { return ___wbits_37; }
	inline int32_t* get_address_of_wbits_37() { return &___wbits_37; }
	inline void set_wbits_37(int32_t value)
	{
		___wbits_37 = value;
	}

	inline static int32_t get_offset_of_blocks_38() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2, ___blocks_38)); }
	inline InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D * get_blocks_38() const { return ___blocks_38; }
	inline InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D ** get_address_of_blocks_38() { return &___blocks_38; }
	inline void set_blocks_38(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D * value)
	{
		___blocks_38 = value;
		Il2CppCodeGenWriteBarrier((&___blocks_38), value);
	}
};

struct Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate::mark
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mark_39;

public:
	inline static int32_t get_offset_of_mark_39() { return static_cast<int32_t>(offsetof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_StaticFields, ___mark_39)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mark_39() const { return ___mark_39; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mark_39() { return &___mark_39; }
	inline void set_mark_39(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mark_39 = value;
		Il2CppCodeGenWriteBarrier((&___mark_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATE_TA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_H
#ifndef JZLIB_TC545D38309B0B7087A0B67726615F5FC039BFDED_H
#define JZLIB_TC545D38309B0B7087A0B67726615F5FC039BFDED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.JZlib
struct  JZlib_tC545D38309B0B7087A0B67726615F5FC039BFDED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JZLIB_TC545D38309B0B7087A0B67726615F5FC039BFDED_H
#ifndef STATICTREE_T7FDF133D27FBB267A9B9110A180E976B3538E7BE_H
#define STATICTREE_T7FDF133D27FBB267A9B9110A180E976B3538E7BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree
struct  StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE  : public RuntimeObject
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::static_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___static_tree_12;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::extra_bits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_bits_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::extra_base
	int32_t ___extra_base_14;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::elems
	int32_t ___elems_15;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::max_length
	int32_t ___max_length_16;

public:
	inline static int32_t get_offset_of_static_tree_12() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE, ___static_tree_12)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_static_tree_12() const { return ___static_tree_12; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_static_tree_12() { return &___static_tree_12; }
	inline void set_static_tree_12(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___static_tree_12 = value;
		Il2CppCodeGenWriteBarrier((&___static_tree_12), value);
	}

	inline static int32_t get_offset_of_extra_bits_13() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE, ___extra_bits_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_bits_13() const { return ___extra_bits_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_bits_13() { return &___extra_bits_13; }
	inline void set_extra_bits_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_bits_13 = value;
		Il2CppCodeGenWriteBarrier((&___extra_bits_13), value);
	}

	inline static int32_t get_offset_of_extra_base_14() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE, ___extra_base_14)); }
	inline int32_t get_extra_base_14() const { return ___extra_base_14; }
	inline int32_t* get_address_of_extra_base_14() { return &___extra_base_14; }
	inline void set_extra_base_14(int32_t value)
	{
		___extra_base_14 = value;
	}

	inline static int32_t get_offset_of_elems_15() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE, ___elems_15)); }
	inline int32_t get_elems_15() const { return ___elems_15; }
	inline int32_t* get_address_of_elems_15() { return &___elems_15; }
	inline void set_elems_15(int32_t value)
	{
		___elems_15 = value;
	}

	inline static int32_t get_offset_of_max_length_16() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE, ___max_length_16)); }
	inline int32_t get_max_length_16() const { return ___max_length_16; }
	inline int32_t* get_address_of_max_length_16() { return &___max_length_16; }
	inline void set_max_length_16(int32_t value)
	{
		___max_length_16 = value;
	}
};

struct StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::static_ltree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___static_ltree_7;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::static_dtree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___static_dtree_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::static_l_desc
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * ___static_l_desc_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::static_d_desc
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * ___static_d_desc_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree::static_bl_desc
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * ___static_bl_desc_11;

public:
	inline static int32_t get_offset_of_static_ltree_7() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields, ___static_ltree_7)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_static_ltree_7() const { return ___static_ltree_7; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_static_ltree_7() { return &___static_ltree_7; }
	inline void set_static_ltree_7(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___static_ltree_7 = value;
		Il2CppCodeGenWriteBarrier((&___static_ltree_7), value);
	}

	inline static int32_t get_offset_of_static_dtree_8() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields, ___static_dtree_8)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_static_dtree_8() const { return ___static_dtree_8; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_static_dtree_8() { return &___static_dtree_8; }
	inline void set_static_dtree_8(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___static_dtree_8 = value;
		Il2CppCodeGenWriteBarrier((&___static_dtree_8), value);
	}

	inline static int32_t get_offset_of_static_l_desc_9() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields, ___static_l_desc_9)); }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * get_static_l_desc_9() const { return ___static_l_desc_9; }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE ** get_address_of_static_l_desc_9() { return &___static_l_desc_9; }
	inline void set_static_l_desc_9(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * value)
	{
		___static_l_desc_9 = value;
		Il2CppCodeGenWriteBarrier((&___static_l_desc_9), value);
	}

	inline static int32_t get_offset_of_static_d_desc_10() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields, ___static_d_desc_10)); }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * get_static_d_desc_10() const { return ___static_d_desc_10; }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE ** get_address_of_static_d_desc_10() { return &___static_d_desc_10; }
	inline void set_static_d_desc_10(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * value)
	{
		___static_d_desc_10 = value;
		Il2CppCodeGenWriteBarrier((&___static_d_desc_10), value);
	}

	inline static int32_t get_offset_of_static_bl_desc_11() { return static_cast<int32_t>(offsetof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields, ___static_bl_desc_11)); }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * get_static_bl_desc_11() const { return ___static_bl_desc_11; }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE ** get_address_of_static_bl_desc_11() { return &___static_bl_desc_11; }
	inline void set_static_bl_desc_11(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * value)
	{
		___static_bl_desc_11 = value;
		Il2CppCodeGenWriteBarrier((&___static_bl_desc_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICTREE_T7FDF133D27FBB267A9B9110A180E976B3538E7BE_H
#ifndef ZSTREAM_TF8878DF452A2750584EBDA42245AB6BFADF4FB7F_H
#define ZSTREAM_TF8878DF452A2750584EBDA42245AB6BFADF4FB7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream
struct  ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::next_in
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___next_in_17;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::next_in_index
	int32_t ___next_in_index_18;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::avail_in
	int32_t ___avail_in_19;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::total_in
	int64_t ___total_in_20;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::next_out
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___next_out_21;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::next_out_index
	int32_t ___next_out_index_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::avail_out
	int32_t ___avail_out_23;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::total_out
	int64_t ___total_out_24;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::msg
	String_t* ___msg_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Deflate BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::dstate
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA * ___dstate_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Inflate BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::istate
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2 * ___istate_27;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::data_type
	int32_t ___data_type_28;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::adler
	int64_t ___adler_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.Adler32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream::_adler
	Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39 * ____adler_30;

public:
	inline static int32_t get_offset_of_next_in_17() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___next_in_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_next_in_17() const { return ___next_in_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_next_in_17() { return &___next_in_17; }
	inline void set_next_in_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___next_in_17 = value;
		Il2CppCodeGenWriteBarrier((&___next_in_17), value);
	}

	inline static int32_t get_offset_of_next_in_index_18() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___next_in_index_18)); }
	inline int32_t get_next_in_index_18() const { return ___next_in_index_18; }
	inline int32_t* get_address_of_next_in_index_18() { return &___next_in_index_18; }
	inline void set_next_in_index_18(int32_t value)
	{
		___next_in_index_18 = value;
	}

	inline static int32_t get_offset_of_avail_in_19() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___avail_in_19)); }
	inline int32_t get_avail_in_19() const { return ___avail_in_19; }
	inline int32_t* get_address_of_avail_in_19() { return &___avail_in_19; }
	inline void set_avail_in_19(int32_t value)
	{
		___avail_in_19 = value;
	}

	inline static int32_t get_offset_of_total_in_20() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___total_in_20)); }
	inline int64_t get_total_in_20() const { return ___total_in_20; }
	inline int64_t* get_address_of_total_in_20() { return &___total_in_20; }
	inline void set_total_in_20(int64_t value)
	{
		___total_in_20 = value;
	}

	inline static int32_t get_offset_of_next_out_21() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___next_out_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_next_out_21() const { return ___next_out_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_next_out_21() { return &___next_out_21; }
	inline void set_next_out_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___next_out_21 = value;
		Il2CppCodeGenWriteBarrier((&___next_out_21), value);
	}

	inline static int32_t get_offset_of_next_out_index_22() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___next_out_index_22)); }
	inline int32_t get_next_out_index_22() const { return ___next_out_index_22; }
	inline int32_t* get_address_of_next_out_index_22() { return &___next_out_index_22; }
	inline void set_next_out_index_22(int32_t value)
	{
		___next_out_index_22 = value;
	}

	inline static int32_t get_offset_of_avail_out_23() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___avail_out_23)); }
	inline int32_t get_avail_out_23() const { return ___avail_out_23; }
	inline int32_t* get_address_of_avail_out_23() { return &___avail_out_23; }
	inline void set_avail_out_23(int32_t value)
	{
		___avail_out_23 = value;
	}

	inline static int32_t get_offset_of_total_out_24() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___total_out_24)); }
	inline int64_t get_total_out_24() const { return ___total_out_24; }
	inline int64_t* get_address_of_total_out_24() { return &___total_out_24; }
	inline void set_total_out_24(int64_t value)
	{
		___total_out_24 = value;
	}

	inline static int32_t get_offset_of_msg_25() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___msg_25)); }
	inline String_t* get_msg_25() const { return ___msg_25; }
	inline String_t** get_address_of_msg_25() { return &___msg_25; }
	inline void set_msg_25(String_t* value)
	{
		___msg_25 = value;
		Il2CppCodeGenWriteBarrier((&___msg_25), value);
	}

	inline static int32_t get_offset_of_dstate_26() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___dstate_26)); }
	inline Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA * get_dstate_26() const { return ___dstate_26; }
	inline Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA ** get_address_of_dstate_26() { return &___dstate_26; }
	inline void set_dstate_26(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA * value)
	{
		___dstate_26 = value;
		Il2CppCodeGenWriteBarrier((&___dstate_26), value);
	}

	inline static int32_t get_offset_of_istate_27() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___istate_27)); }
	inline Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2 * get_istate_27() const { return ___istate_27; }
	inline Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2 ** get_address_of_istate_27() { return &___istate_27; }
	inline void set_istate_27(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2 * value)
	{
		___istate_27 = value;
		Il2CppCodeGenWriteBarrier((&___istate_27), value);
	}

	inline static int32_t get_offset_of_data_type_28() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___data_type_28)); }
	inline int32_t get_data_type_28() const { return ___data_type_28; }
	inline int32_t* get_address_of_data_type_28() { return &___data_type_28; }
	inline void set_data_type_28(int32_t value)
	{
		___data_type_28 = value;
	}

	inline static int32_t get_offset_of_adler_29() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ___adler_29)); }
	inline int64_t get_adler_29() const { return ___adler_29; }
	inline int64_t* get_address_of_adler_29() { return &___adler_29; }
	inline void set_adler_29(int64_t value)
	{
		___adler_29 = value;
	}

	inline static int32_t get_offset_of__adler_30() { return static_cast<int32_t>(offsetof(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F, ____adler_30)); }
	inline Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39 * get__adler_30() const { return ____adler_30; }
	inline Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39 ** get_address_of__adler_30() { return &____adler_30; }
	inline void set__adler_30(Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39 * value)
	{
		____adler_30 = value;
		Il2CppCodeGenWriteBarrier((&____adler_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSTREAM_TF8878DF452A2750584EBDA42245AB6BFADF4FB7F_H
#ifndef ZTREE_TC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_H
#define ZTREE_TC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree
struct  ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36  : public RuntimeObject
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::dyn_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_tree_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::max_code
	int32_t ___max_code_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.StaticTree BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::stat_desc
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * ___stat_desc_24;

public:
	inline static int32_t get_offset_of_dyn_tree_22() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36, ___dyn_tree_22)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_tree_22() const { return ___dyn_tree_22; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_tree_22() { return &___dyn_tree_22; }
	inline void set_dyn_tree_22(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_tree_22 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_tree_22), value);
	}

	inline static int32_t get_offset_of_max_code_23() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36, ___max_code_23)); }
	inline int32_t get_max_code_23() const { return ___max_code_23; }
	inline int32_t* get_address_of_max_code_23() { return &___max_code_23; }
	inline void set_max_code_23(int32_t value)
	{
		___max_code_23 = value;
	}

	inline static int32_t get_offset_of_stat_desc_24() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36, ___stat_desc_24)); }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * get_stat_desc_24() const { return ___stat_desc_24; }
	inline StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE ** get_address_of_stat_desc_24() { return &___stat_desc_24; }
	inline void set_stat_desc_24(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE * value)
	{
		___stat_desc_24 = value;
		Il2CppCodeGenWriteBarrier((&___stat_desc_24), value);
	}
};

struct ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::extra_lbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_lbits_12;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::extra_dbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_dbits_13;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::extra_blbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_blbits_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::bl_order
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bl_order_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::_dist_code
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____dist_code_18;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::_length_code
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____length_code_19;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::base_length
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___base_length_20;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZTree::base_dist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___base_dist_21;

public:
	inline static int32_t get_offset_of_extra_lbits_12() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ___extra_lbits_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_lbits_12() const { return ___extra_lbits_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_lbits_12() { return &___extra_lbits_12; }
	inline void set_extra_lbits_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_lbits_12 = value;
		Il2CppCodeGenWriteBarrier((&___extra_lbits_12), value);
	}

	inline static int32_t get_offset_of_extra_dbits_13() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ___extra_dbits_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_dbits_13() const { return ___extra_dbits_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_dbits_13() { return &___extra_dbits_13; }
	inline void set_extra_dbits_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_dbits_13 = value;
		Il2CppCodeGenWriteBarrier((&___extra_dbits_13), value);
	}

	inline static int32_t get_offset_of_extra_blbits_14() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ___extra_blbits_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_blbits_14() const { return ___extra_blbits_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_blbits_14() { return &___extra_blbits_14; }
	inline void set_extra_blbits_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_blbits_14 = value;
		Il2CppCodeGenWriteBarrier((&___extra_blbits_14), value);
	}

	inline static int32_t get_offset_of_bl_order_15() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ___bl_order_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bl_order_15() const { return ___bl_order_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bl_order_15() { return &___bl_order_15; }
	inline void set_bl_order_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bl_order_15 = value;
		Il2CppCodeGenWriteBarrier((&___bl_order_15), value);
	}

	inline static int32_t get_offset_of__dist_code_18() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ____dist_code_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__dist_code_18() const { return ____dist_code_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__dist_code_18() { return &____dist_code_18; }
	inline void set__dist_code_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____dist_code_18 = value;
		Il2CppCodeGenWriteBarrier((&____dist_code_18), value);
	}

	inline static int32_t get_offset_of__length_code_19() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ____length_code_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__length_code_19() const { return ____length_code_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__length_code_19() { return &____length_code_19; }
	inline void set__length_code_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____length_code_19 = value;
		Il2CppCodeGenWriteBarrier((&____length_code_19), value);
	}

	inline static int32_t get_offset_of_base_length_20() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ___base_length_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_base_length_20() const { return ___base_length_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_base_length_20() { return &___base_length_20; }
	inline void set_base_length_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___base_length_20 = value;
		Il2CppCodeGenWriteBarrier((&___base_length_20), value);
	}

	inline static int32_t get_offset_of_base_dist_21() { return static_cast<int32_t>(offsetof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields, ___base_dist_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_base_dist_21() const { return ___base_dist_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_base_dist_21() { return &___base_dist_21; }
	inline void set_base_dist_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___base_dist_21 = value;
		Il2CppCodeGenWriteBarrier((&___base_dist_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZTREE_TC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_H
#ifndef ATTRIBUTECERTIFICATEHOLDER_T405580B68B684496CDEA08842CDBC2978E504D20_H
#define ATTRIBUTECERTIFICATEHOLDER_T405580B68B684496CDEA08842CDBC2978E504D20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateHolder
struct  AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Holder BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateHolder::holder
	Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * ___holder_0;

public:
	inline static int32_t get_offset_of_holder_0() { return static_cast<int32_t>(offsetof(AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20, ___holder_0)); }
	inline Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * get_holder_0() const { return ___holder_0; }
	inline Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D ** get_address_of_holder_0() { return &___holder_0; }
	inline void set_holder_0(Holder_t79EC89066F0307269E8B7D8EB8504506DF03F13D * value)
	{
		___holder_0 = value;
		Il2CppCodeGenWriteBarrier((&___holder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTECERTIFICATEHOLDER_T405580B68B684496CDEA08842CDBC2978E504D20_H
#ifndef ATTRIBUTECERTIFICATEISSUER_TC5816226F2B1BE412A49E980B499D0B6BC1A8EBF_H
#define ATTRIBUTECERTIFICATEISSUER_TC5816226F2B1BE412A49E980B499D0B6BC1A8EBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateIssuer
struct  AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateIssuer::form
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___form_0;

public:
	inline static int32_t get_offset_of_form_0() { return static_cast<int32_t>(offsetof(AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF, ___form_0)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_form_0() const { return ___form_0; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_form_0() { return &___form_0; }
	inline void set_form_0(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___form_0 = value;
		Il2CppCodeGenWriteBarrier((&___form_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTECERTIFICATEISSUER_TC5816226F2B1BE412A49E980B499D0B6BC1A8EBF_H
#ifndef X509EXTENSIONUTILITIES_T98D18816F30C017C13AF09CD68828E16F93A1967_H
#define X509EXTENSIONUTILITIES_T98D18816F30C017C13AF09CD68828E16F93A1967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Extension.X509ExtensionUtilities
struct  X509ExtensionUtilities_t98D18816F30C017C13AF09CD68828E16F93A1967  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONUTILITIES_T98D18816F30C017C13AF09CD68828E16F93A1967_H
#ifndef PEMPARSER_T08EEDB2B460C5FD8CC633530936F9E8F336F1BAE_H
#define PEMPARSER_T08EEDB2B460C5FD8CC633530936F9E8F336F1BAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser
struct  PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser::_header1
	String_t* ____header1_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser::_header2
	String_t* ____header2_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser::_footer1
	String_t* ____footer1_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser::_footer2
	String_t* ____footer2_3;

public:
	inline static int32_t get_offset_of__header1_0() { return static_cast<int32_t>(offsetof(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE, ____header1_0)); }
	inline String_t* get__header1_0() const { return ____header1_0; }
	inline String_t** get_address_of__header1_0() { return &____header1_0; }
	inline void set__header1_0(String_t* value)
	{
		____header1_0 = value;
		Il2CppCodeGenWriteBarrier((&____header1_0), value);
	}

	inline static int32_t get_offset_of__header2_1() { return static_cast<int32_t>(offsetof(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE, ____header2_1)); }
	inline String_t* get__header2_1() const { return ____header2_1; }
	inline String_t** get_address_of__header2_1() { return &____header2_1; }
	inline void set__header2_1(String_t* value)
	{
		____header2_1 = value;
		Il2CppCodeGenWriteBarrier((&____header2_1), value);
	}

	inline static int32_t get_offset_of__footer1_2() { return static_cast<int32_t>(offsetof(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE, ____footer1_2)); }
	inline String_t* get__footer1_2() const { return ____footer1_2; }
	inline String_t** get_address_of__footer1_2() { return &____footer1_2; }
	inline void set__footer1_2(String_t* value)
	{
		____footer1_2 = value;
		Il2CppCodeGenWriteBarrier((&____footer1_2), value);
	}

	inline static int32_t get_offset_of__footer2_3() { return static_cast<int32_t>(offsetof(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE, ____footer2_3)); }
	inline String_t* get__footer2_3() const { return ____footer2_3; }
	inline String_t** get_address_of__footer2_3() { return &____footer2_3; }
	inline void set__footer2_3(String_t* value)
	{
		____footer2_3 = value;
		Il2CppCodeGenWriteBarrier((&____footer2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMPARSER_T08EEDB2B460C5FD8CC633530936F9E8F336F1BAE_H
#ifndef PRINCIPALUTILITIES_TF8725038B210BBD7D9EE399FF876992694DCB523_H
#define PRINCIPALUTILITIES_TF8725038B210BBD7D9EE399FF876992694DCB523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PrincipalUtilities
struct  PrincipalUtilities_tF8725038B210BBD7D9EE399FF876992694DCB523  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINCIPALUTILITIES_TF8725038B210BBD7D9EE399FF876992694DCB523_H
#ifndef X509ATTRCERTSTORESELECTOR_T36EF57C6859BF61E884053CC25062F28BFD6D54C_H
#define X509ATTRCERTSTORESELECTOR_T36EF57C6859BF61E884053CC25062F28BFD6D54C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector
struct  X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.IX509AttributeCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::attributeCert
	RuntimeObject* ___attributeCert_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::attributeCertificateValid
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___attributeCertificateValid_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateHolder BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::holder
	AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20 * ___holder_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.AttributeCertificateIssuer BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::issuer
	AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF * ___issuer_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::serialNumber
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___serialNumber_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::targetNames
	RuntimeObject* ___targetNames_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509AttrCertStoreSelector::targetGroups
	RuntimeObject* ___targetGroups_6;

public:
	inline static int32_t get_offset_of_attributeCert_0() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___attributeCert_0)); }
	inline RuntimeObject* get_attributeCert_0() const { return ___attributeCert_0; }
	inline RuntimeObject** get_address_of_attributeCert_0() { return &___attributeCert_0; }
	inline void set_attributeCert_0(RuntimeObject* value)
	{
		___attributeCert_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCert_0), value);
	}

	inline static int32_t get_offset_of_attributeCertificateValid_1() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___attributeCertificateValid_1)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_attributeCertificateValid_1() const { return ___attributeCertificateValid_1; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_attributeCertificateValid_1() { return &___attributeCertificateValid_1; }
	inline void set_attributeCertificateValid_1(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___attributeCertificateValid_1 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCertificateValid_1), value);
	}

	inline static int32_t get_offset_of_holder_2() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___holder_2)); }
	inline AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20 * get_holder_2() const { return ___holder_2; }
	inline AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20 ** get_address_of_holder_2() { return &___holder_2; }
	inline void set_holder_2(AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20 * value)
	{
		___holder_2 = value;
		Il2CppCodeGenWriteBarrier((&___holder_2), value);
	}

	inline static int32_t get_offset_of_issuer_3() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___issuer_3)); }
	inline AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF * get_issuer_3() const { return ___issuer_3; }
	inline AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF ** get_address_of_issuer_3() { return &___issuer_3; }
	inline void set_issuer_3(AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF * value)
	{
		___issuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_3), value);
	}

	inline static int32_t get_offset_of_serialNumber_4() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___serialNumber_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_serialNumber_4() const { return ___serialNumber_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_serialNumber_4() { return &___serialNumber_4; }
	inline void set_serialNumber_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___serialNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_4), value);
	}

	inline static int32_t get_offset_of_targetNames_5() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___targetNames_5)); }
	inline RuntimeObject* get_targetNames_5() const { return ___targetNames_5; }
	inline RuntimeObject** get_address_of_targetNames_5() { return &___targetNames_5; }
	inline void set_targetNames_5(RuntimeObject* value)
	{
		___targetNames_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetNames_5), value);
	}

	inline static int32_t get_offset_of_targetGroups_6() { return static_cast<int32_t>(offsetof(X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C, ___targetGroups_6)); }
	inline RuntimeObject* get_targetGroups_6() const { return ___targetGroups_6; }
	inline RuntimeObject** get_address_of_targetGroups_6() { return &___targetGroups_6; }
	inline void set_targetGroups_6(RuntimeObject* value)
	{
		___targetGroups_6 = value;
		Il2CppCodeGenWriteBarrier((&___targetGroups_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ATTRCERTSTORESELECTOR_T36EF57C6859BF61E884053CC25062F28BFD6D54C_H
#ifndef X509CERTPAIRSTORESELECTOR_T3490268DE4B11D1AA36E3D7CA087E6B7B0372886_H
#define X509CERTPAIRSTORESELECTOR_T3490268DE4B11D1AA36E3D7CA087E6B7B0372886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertPairStoreSelector
struct  X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificatePair BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertPairStoreSelector::certPair
	X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A * ___certPair_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertPairStoreSelector::forwardSelector
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 * ___forwardSelector_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertPairStoreSelector::reverseSelector
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 * ___reverseSelector_2;

public:
	inline static int32_t get_offset_of_certPair_0() { return static_cast<int32_t>(offsetof(X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886, ___certPair_0)); }
	inline X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A * get_certPair_0() const { return ___certPair_0; }
	inline X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A ** get_address_of_certPair_0() { return &___certPair_0; }
	inline void set_certPair_0(X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A * value)
	{
		___certPair_0 = value;
		Il2CppCodeGenWriteBarrier((&___certPair_0), value);
	}

	inline static int32_t get_offset_of_forwardSelector_1() { return static_cast<int32_t>(offsetof(X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886, ___forwardSelector_1)); }
	inline X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 * get_forwardSelector_1() const { return ___forwardSelector_1; }
	inline X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 ** get_address_of_forwardSelector_1() { return &___forwardSelector_1; }
	inline void set_forwardSelector_1(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 * value)
	{
		___forwardSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&___forwardSelector_1), value);
	}

	inline static int32_t get_offset_of_reverseSelector_2() { return static_cast<int32_t>(offsetof(X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886, ___reverseSelector_2)); }
	inline X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 * get_reverseSelector_2() const { return ___reverseSelector_2; }
	inline X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 ** get_address_of_reverseSelector_2() { return &___reverseSelector_2; }
	inline void set_reverseSelector_2(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9 * value)
	{
		___reverseSelector_2 = value;
		Il2CppCodeGenWriteBarrier((&___reverseSelector_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTPAIRSTORESELECTOR_T3490268DE4B11D1AA36E3D7CA087E6B7B0372886_H
#ifndef X509CERTSTORESELECTOR_TB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9_H
#define X509CERTSTORESELECTOR_TB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector
struct  X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::authorityKeyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___authorityKeyIdentifier_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::basicConstraints
	int32_t ___basicConstraints_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::certificate
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___certificate_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::certificateValid
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___certificateValid_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::extendedKeyUsage
	RuntimeObject* ___extendedKeyUsage_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::ignoreX509NameOrdering
	bool ___ignoreX509NameOrdering_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_6;
	// System.Boolean[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::keyUsage
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___keyUsage_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::policy
	RuntimeObject* ___policy_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::privateKeyValid
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___privateKeyValid_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::serialNumber
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___serialNumber_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subjectKeyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___subjectKeyIdentifier_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subjectPublicKey
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___subjectPublicKey_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CertStoreSelector::subjectPublicKeyAlgID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___subjectPublicKeyAlgID_14;

public:
	inline static int32_t get_offset_of_authorityKeyIdentifier_0() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___authorityKeyIdentifier_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_authorityKeyIdentifier_0() const { return ___authorityKeyIdentifier_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_authorityKeyIdentifier_0() { return &___authorityKeyIdentifier_0; }
	inline void set_authorityKeyIdentifier_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___authorityKeyIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___authorityKeyIdentifier_0), value);
	}

	inline static int32_t get_offset_of_basicConstraints_1() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___basicConstraints_1)); }
	inline int32_t get_basicConstraints_1() const { return ___basicConstraints_1; }
	inline int32_t* get_address_of_basicConstraints_1() { return &___basicConstraints_1; }
	inline void set_basicConstraints_1(int32_t value)
	{
		___basicConstraints_1 = value;
	}

	inline static int32_t get_offset_of_certificate_2() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___certificate_2)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_certificate_2() const { return ___certificate_2; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_certificate_2() { return &___certificate_2; }
	inline void set_certificate_2(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___certificate_2 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_2), value);
	}

	inline static int32_t get_offset_of_certificateValid_3() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___certificateValid_3)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_certificateValid_3() const { return ___certificateValid_3; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_certificateValid_3() { return &___certificateValid_3; }
	inline void set_certificateValid_3(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___certificateValid_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificateValid_3), value);
	}

	inline static int32_t get_offset_of_extendedKeyUsage_4() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___extendedKeyUsage_4)); }
	inline RuntimeObject* get_extendedKeyUsage_4() const { return ___extendedKeyUsage_4; }
	inline RuntimeObject** get_address_of_extendedKeyUsage_4() { return &___extendedKeyUsage_4; }
	inline void set_extendedKeyUsage_4(RuntimeObject* value)
	{
		___extendedKeyUsage_4 = value;
		Il2CppCodeGenWriteBarrier((&___extendedKeyUsage_4), value);
	}

	inline static int32_t get_offset_of_ignoreX509NameOrdering_5() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___ignoreX509NameOrdering_5)); }
	inline bool get_ignoreX509NameOrdering_5() const { return ___ignoreX509NameOrdering_5; }
	inline bool* get_address_of_ignoreX509NameOrdering_5() { return &___ignoreX509NameOrdering_5; }
	inline void set_ignoreX509NameOrdering_5(bool value)
	{
		___ignoreX509NameOrdering_5 = value;
	}

	inline static int32_t get_offset_of_issuer_6() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___issuer_6)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_6() const { return ___issuer_6; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_6() { return &___issuer_6; }
	inline void set_issuer_6(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_6 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_6), value);
	}

	inline static int32_t get_offset_of_keyUsage_7() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___keyUsage_7)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_keyUsage_7() const { return ___keyUsage_7; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_keyUsage_7() { return &___keyUsage_7; }
	inline void set_keyUsage_7(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___keyUsage_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyUsage_7), value);
	}

	inline static int32_t get_offset_of_policy_8() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___policy_8)); }
	inline RuntimeObject* get_policy_8() const { return ___policy_8; }
	inline RuntimeObject** get_address_of_policy_8() { return &___policy_8; }
	inline void set_policy_8(RuntimeObject* value)
	{
		___policy_8 = value;
		Il2CppCodeGenWriteBarrier((&___policy_8), value);
	}

	inline static int32_t get_offset_of_privateKeyValid_9() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___privateKeyValid_9)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_privateKeyValid_9() const { return ___privateKeyValid_9; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_privateKeyValid_9() { return &___privateKeyValid_9; }
	inline void set_privateKeyValid_9(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___privateKeyValid_9 = value;
		Il2CppCodeGenWriteBarrier((&___privateKeyValid_9), value);
	}

	inline static int32_t get_offset_of_serialNumber_10() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___serialNumber_10)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_serialNumber_10() const { return ___serialNumber_10; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_serialNumber_10() { return &___serialNumber_10; }
	inline void set_serialNumber_10(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___serialNumber_10 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_10), value);
	}

	inline static int32_t get_offset_of_subject_11() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subject_11)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_11() const { return ___subject_11; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_11() { return &___subject_11; }
	inline void set_subject_11(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_11 = value;
		Il2CppCodeGenWriteBarrier((&___subject_11), value);
	}

	inline static int32_t get_offset_of_subjectKeyIdentifier_12() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subjectKeyIdentifier_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_subjectKeyIdentifier_12() const { return ___subjectKeyIdentifier_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_subjectKeyIdentifier_12() { return &___subjectKeyIdentifier_12; }
	inline void set_subjectKeyIdentifier_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___subjectKeyIdentifier_12 = value;
		Il2CppCodeGenWriteBarrier((&___subjectKeyIdentifier_12), value);
	}

	inline static int32_t get_offset_of_subjectPublicKey_13() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subjectPublicKey_13)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_subjectPublicKey_13() const { return ___subjectPublicKey_13; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_subjectPublicKey_13() { return &___subjectPublicKey_13; }
	inline void set_subjectPublicKey_13(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___subjectPublicKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKey_13), value);
	}

	inline static int32_t get_offset_of_subjectPublicKeyAlgID_14() { return static_cast<int32_t>(offsetof(X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9, ___subjectPublicKeyAlgID_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_subjectPublicKeyAlgID_14() const { return ___subjectPublicKeyAlgID_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_subjectPublicKeyAlgID_14() { return &___subjectPublicKeyAlgID_14; }
	inline void set_subjectPublicKeyAlgID_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___subjectPublicKeyAlgID_14 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKeyAlgID_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTSTORESELECTOR_TB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9_H
#ifndef X509COLLECTIONSTORE_T2F0493B4CE42FACCAAC81103079053B78FCD438C_H
#define X509COLLECTIONSTORE_T2F0493B4CE42FACCAAC81103079053B78FCD438C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CollectionStore
struct  X509CollectionStore_t2F0493B4CE42FACCAAC81103079053B78FCD438C  : public RuntimeObject
{
public:
	// System.Collections.ICollection BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CollectionStore::_local
	RuntimeObject* ____local_0;

public:
	inline static int32_t get_offset_of__local_0() { return static_cast<int32_t>(offsetof(X509CollectionStore_t2F0493B4CE42FACCAAC81103079053B78FCD438C, ____local_0)); }
	inline RuntimeObject* get__local_0() const { return ____local_0; }
	inline RuntimeObject** get_address_of__local_0() { return &____local_0; }
	inline void set__local_0(RuntimeObject* value)
	{
		____local_0 = value;
		Il2CppCodeGenWriteBarrier((&____local_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509COLLECTIONSTORE_T2F0493B4CE42FACCAAC81103079053B78FCD438C_H
#ifndef X509COLLECTIONSTOREPARAMETERS_TF34162D037E59181EE3392E3E74708609A546E4C_H
#define X509COLLECTIONSTOREPARAMETERS_TF34162D037E59181EE3392E3E74708609A546E4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CollectionStoreParameters
struct  X509CollectionStoreParameters_tF34162D037E59181EE3392E3E74708609A546E4C  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CollectionStoreParameters::collection
	RuntimeObject* ___collection_0;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(X509CollectionStoreParameters_tF34162D037E59181EE3392E3E74708609A546E4C, ___collection_0)); }
	inline RuntimeObject* get_collection_0() const { return ___collection_0; }
	inline RuntimeObject** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(RuntimeObject* value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509COLLECTIONSTOREPARAMETERS_TF34162D037E59181EE3392E3E74708609A546E4C_H
#ifndef X509CRLSTORESELECTOR_T3DED686129E05ADAFCD3B73273C728CEF48920F2_H
#define X509CRLSTORESELECTOR_T3DED686129E05ADAFCD3B73273C728CEF48920F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector
struct  X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::certificateChecking
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___certificateChecking_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::dateAndTime
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___dateAndTime_1;
	// System.Collections.ICollection BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::issuers
	RuntimeObject* ___issuers_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::maxCrlNumber
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___maxCrlNumber_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::minCrlNumber
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___minCrlNumber_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.IX509AttributeCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::attrCertChecking
	RuntimeObject* ___attrCertChecking_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::completeCrlEnabled
	bool ___completeCrlEnabled_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::deltaCrlIndicatorEnabled
	bool ___deltaCrlIndicatorEnabled_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::issuingDistributionPoint
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___issuingDistributionPoint_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::issuingDistributionPointEnabled
	bool ___issuingDistributionPointEnabled_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509CrlStoreSelector::maxBaseCrlNumber
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___maxBaseCrlNumber_10;

public:
	inline static int32_t get_offset_of_certificateChecking_0() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___certificateChecking_0)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_certificateChecking_0() const { return ___certificateChecking_0; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_certificateChecking_0() { return &___certificateChecking_0; }
	inline void set_certificateChecking_0(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___certificateChecking_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificateChecking_0), value);
	}

	inline static int32_t get_offset_of_dateAndTime_1() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___dateAndTime_1)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_dateAndTime_1() const { return ___dateAndTime_1; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_dateAndTime_1() { return &___dateAndTime_1; }
	inline void set_dateAndTime_1(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___dateAndTime_1 = value;
		Il2CppCodeGenWriteBarrier((&___dateAndTime_1), value);
	}

	inline static int32_t get_offset_of_issuers_2() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___issuers_2)); }
	inline RuntimeObject* get_issuers_2() const { return ___issuers_2; }
	inline RuntimeObject** get_address_of_issuers_2() { return &___issuers_2; }
	inline void set_issuers_2(RuntimeObject* value)
	{
		___issuers_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuers_2), value);
	}

	inline static int32_t get_offset_of_maxCrlNumber_3() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___maxCrlNumber_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_maxCrlNumber_3() const { return ___maxCrlNumber_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_maxCrlNumber_3() { return &___maxCrlNumber_3; }
	inline void set_maxCrlNumber_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___maxCrlNumber_3 = value;
		Il2CppCodeGenWriteBarrier((&___maxCrlNumber_3), value);
	}

	inline static int32_t get_offset_of_minCrlNumber_4() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___minCrlNumber_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_minCrlNumber_4() const { return ___minCrlNumber_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_minCrlNumber_4() { return &___minCrlNumber_4; }
	inline void set_minCrlNumber_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___minCrlNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___minCrlNumber_4), value);
	}

	inline static int32_t get_offset_of_attrCertChecking_5() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___attrCertChecking_5)); }
	inline RuntimeObject* get_attrCertChecking_5() const { return ___attrCertChecking_5; }
	inline RuntimeObject** get_address_of_attrCertChecking_5() { return &___attrCertChecking_5; }
	inline void set_attrCertChecking_5(RuntimeObject* value)
	{
		___attrCertChecking_5 = value;
		Il2CppCodeGenWriteBarrier((&___attrCertChecking_5), value);
	}

	inline static int32_t get_offset_of_completeCrlEnabled_6() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___completeCrlEnabled_6)); }
	inline bool get_completeCrlEnabled_6() const { return ___completeCrlEnabled_6; }
	inline bool* get_address_of_completeCrlEnabled_6() { return &___completeCrlEnabled_6; }
	inline void set_completeCrlEnabled_6(bool value)
	{
		___completeCrlEnabled_6 = value;
	}

	inline static int32_t get_offset_of_deltaCrlIndicatorEnabled_7() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___deltaCrlIndicatorEnabled_7)); }
	inline bool get_deltaCrlIndicatorEnabled_7() const { return ___deltaCrlIndicatorEnabled_7; }
	inline bool* get_address_of_deltaCrlIndicatorEnabled_7() { return &___deltaCrlIndicatorEnabled_7; }
	inline void set_deltaCrlIndicatorEnabled_7(bool value)
	{
		___deltaCrlIndicatorEnabled_7 = value;
	}

	inline static int32_t get_offset_of_issuingDistributionPoint_8() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___issuingDistributionPoint_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_issuingDistributionPoint_8() const { return ___issuingDistributionPoint_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_issuingDistributionPoint_8() { return &___issuingDistributionPoint_8; }
	inline void set_issuingDistributionPoint_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___issuingDistributionPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___issuingDistributionPoint_8), value);
	}

	inline static int32_t get_offset_of_issuingDistributionPointEnabled_9() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___issuingDistributionPointEnabled_9)); }
	inline bool get_issuingDistributionPointEnabled_9() const { return ___issuingDistributionPointEnabled_9; }
	inline bool* get_address_of_issuingDistributionPointEnabled_9() { return &___issuingDistributionPointEnabled_9; }
	inline void set_issuingDistributionPointEnabled_9(bool value)
	{
		___issuingDistributionPointEnabled_9 = value;
	}

	inline static int32_t get_offset_of_maxBaseCrlNumber_10() { return static_cast<int32_t>(offsetof(X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2, ___maxBaseCrlNumber_10)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_maxBaseCrlNumber_10() const { return ___maxBaseCrlNumber_10; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_maxBaseCrlNumber_10() { return &___maxBaseCrlNumber_10; }
	inline void set_maxBaseCrlNumber_10(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___maxBaseCrlNumber_10 = value;
		Il2CppCodeGenWriteBarrier((&___maxBaseCrlNumber_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRLSTORESELECTOR_T3DED686129E05ADAFCD3B73273C728CEF48920F2_H
#ifndef X509STOREFACTORY_TFCACCF97F646FBC473625C188F7596DBA53766B5_H
#define X509STOREFACTORY_TFCACCF97F646FBC473625C188F7596DBA53766B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509StoreFactory
struct  X509StoreFactory_tFCACCF97F646FBC473625C188F7596DBA53766B5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STOREFACTORY_TFCACCF97F646FBC473625C188F7596DBA53766B5_H
#ifndef SUBJECTPUBLICKEYINFOFACTORY_TD85601347B2AD762413F648CF79E31930E714D02_H
#define SUBJECTPUBLICKEYINFOFACTORY_TD85601347B2AD762413F648CF79E31930E714D02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.SubjectPublicKeyInfoFactory
struct  SubjectPublicKeyInfoFactory_tD85601347B2AD762413F648CF79E31930E714D02  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTPUBLICKEYINFOFACTORY_TD85601347B2AD762413F648CF79E31930E714D02_H
#ifndef X509ATTRCERTPARSER_T0AC7E90BA6F690FA6163B72876B6420BC0B6B145_H
#define X509ATTRCERTPARSER_T0AC7E90BA6F690FA6163B72876B6420BC0B6B145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509AttrCertParser
struct  X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509AttrCertParser::sData
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___sData_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509AttrCertParser::sDataObjectCount
	int32_t ___sDataObjectCount_2;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509AttrCertParser::currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___currentStream_3;

public:
	inline static int32_t get_offset_of_sData_1() { return static_cast<int32_t>(offsetof(X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145, ___sData_1)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_sData_1() const { return ___sData_1; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_sData_1() { return &___sData_1; }
	inline void set_sData_1(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___sData_1 = value;
		Il2CppCodeGenWriteBarrier((&___sData_1), value);
	}

	inline static int32_t get_offset_of_sDataObjectCount_2() { return static_cast<int32_t>(offsetof(X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145, ___sDataObjectCount_2)); }
	inline int32_t get_sDataObjectCount_2() const { return ___sDataObjectCount_2; }
	inline int32_t* get_address_of_sDataObjectCount_2() { return &___sDataObjectCount_2; }
	inline void set_sDataObjectCount_2(int32_t value)
	{
		___sDataObjectCount_2 = value;
	}

	inline static int32_t get_offset_of_currentStream_3() { return static_cast<int32_t>(offsetof(X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145, ___currentStream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_currentStream_3() const { return ___currentStream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_currentStream_3() { return &___currentStream_3; }
	inline void set_currentStream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___currentStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentStream_3), value);
	}
};

struct X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509AttrCertParser::PemAttrCertParser
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * ___PemAttrCertParser_0;

public:
	inline static int32_t get_offset_of_PemAttrCertParser_0() { return static_cast<int32_t>(offsetof(X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145_StaticFields, ___PemAttrCertParser_0)); }
	inline PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * get_PemAttrCertParser_0() const { return ___PemAttrCertParser_0; }
	inline PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE ** get_address_of_PemAttrCertParser_0() { return &___PemAttrCertParser_0; }
	inline void set_PemAttrCertParser_0(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * value)
	{
		___PemAttrCertParser_0 = value;
		Il2CppCodeGenWriteBarrier((&___PemAttrCertParser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ATTRCERTPARSER_T0AC7E90BA6F690FA6163B72876B6420BC0B6B145_H
#ifndef X509CERTPAIRPARSER_TC5DA02E4655BF842D80F18F9DC19862C4A4D07AD_H
#define X509CERTPAIRPARSER_TC5DA02E4655BF842D80F18F9DC19862C4A4D07AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertPairParser
struct  X509CertPairParser_tC5DA02E4655BF842D80F18F9DC19862C4A4D07AD  : public RuntimeObject
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertPairParser::currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___currentStream_0;

public:
	inline static int32_t get_offset_of_currentStream_0() { return static_cast<int32_t>(offsetof(X509CertPairParser_tC5DA02E4655BF842D80F18F9DC19862C4A4D07AD, ___currentStream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_currentStream_0() const { return ___currentStream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_currentStream_0() { return &___currentStream_0; }
	inline void set_currentStream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___currentStream_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentStream_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTPAIRPARSER_TC5DA02E4655BF842D80F18F9DC19862C4A4D07AD_H
#ifndef X509CERTIFICATEPAIR_T155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A_H
#define X509CERTIFICATEPAIR_T155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificatePair
struct  X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificatePair::forward
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___forward_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificatePair::reverse
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___reverse_1;

public:
	inline static int32_t get_offset_of_forward_0() { return static_cast<int32_t>(offsetof(X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A, ___forward_0)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_forward_0() const { return ___forward_0; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_forward_0() { return &___forward_0; }
	inline void set_forward_0(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___forward_0 = value;
		Il2CppCodeGenWriteBarrier((&___forward_0), value);
	}

	inline static int32_t get_offset_of_reverse_1() { return static_cast<int32_t>(offsetof(X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A, ___reverse_1)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_reverse_1() const { return ___reverse_1; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_reverse_1() { return &___reverse_1; }
	inline void set_reverse_1(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___reverse_1 = value;
		Il2CppCodeGenWriteBarrier((&___reverse_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEPAIR_T155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A_H
#ifndef X509CERTIFICATEPARSER_T8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_H
#define X509CERTIFICATEPARSER_T8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificateParser
struct  X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificateParser::sData
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___sData_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificateParser::sDataObjectCount
	int32_t ___sDataObjectCount_2;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificateParser::currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___currentStream_3;

public:
	inline static int32_t get_offset_of_sData_1() { return static_cast<int32_t>(offsetof(X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA, ___sData_1)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_sData_1() const { return ___sData_1; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_sData_1() { return &___sData_1; }
	inline void set_sData_1(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___sData_1 = value;
		Il2CppCodeGenWriteBarrier((&___sData_1), value);
	}

	inline static int32_t get_offset_of_sDataObjectCount_2() { return static_cast<int32_t>(offsetof(X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA, ___sDataObjectCount_2)); }
	inline int32_t get_sDataObjectCount_2() const { return ___sDataObjectCount_2; }
	inline int32_t* get_address_of_sDataObjectCount_2() { return &___sDataObjectCount_2; }
	inline void set_sDataObjectCount_2(int32_t value)
	{
		___sDataObjectCount_2 = value;
	}

	inline static int32_t get_offset_of_currentStream_3() { return static_cast<int32_t>(offsetof(X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA, ___currentStream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_currentStream_3() const { return ___currentStream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_currentStream_3() { return &___currentStream_3; }
	inline void set_currentStream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___currentStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentStream_3), value);
	}
};

struct X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CertificateParser::PemCertParser
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * ___PemCertParser_0;

public:
	inline static int32_t get_offset_of_PemCertParser_0() { return static_cast<int32_t>(offsetof(X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_StaticFields, ___PemCertParser_0)); }
	inline PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * get_PemCertParser_0() const { return ___PemCertParser_0; }
	inline PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE ** get_address_of_PemCertParser_0() { return &___PemCertParser_0; }
	inline void set_PemCertParser_0(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * value)
	{
		___PemCertParser_0 = value;
		Il2CppCodeGenWriteBarrier((&___PemCertParser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEPARSER_T8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_H
#ifndef X509CRLPARSER_T795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_H
#define X509CRLPARSER_T795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlParser
struct  X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlParser::lazyAsn1
	bool ___lazyAsn1_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlParser::sCrlData
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___sCrlData_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlParser::sCrlDataObjectCount
	int32_t ___sCrlDataObjectCount_3;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlParser::currentCrlStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___currentCrlStream_4;

public:
	inline static int32_t get_offset_of_lazyAsn1_1() { return static_cast<int32_t>(offsetof(X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0, ___lazyAsn1_1)); }
	inline bool get_lazyAsn1_1() const { return ___lazyAsn1_1; }
	inline bool* get_address_of_lazyAsn1_1() { return &___lazyAsn1_1; }
	inline void set_lazyAsn1_1(bool value)
	{
		___lazyAsn1_1 = value;
	}

	inline static int32_t get_offset_of_sCrlData_2() { return static_cast<int32_t>(offsetof(X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0, ___sCrlData_2)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_sCrlData_2() const { return ___sCrlData_2; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_sCrlData_2() { return &___sCrlData_2; }
	inline void set_sCrlData_2(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___sCrlData_2 = value;
		Il2CppCodeGenWriteBarrier((&___sCrlData_2), value);
	}

	inline static int32_t get_offset_of_sCrlDataObjectCount_3() { return static_cast<int32_t>(offsetof(X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0, ___sCrlDataObjectCount_3)); }
	inline int32_t get_sCrlDataObjectCount_3() const { return ___sCrlDataObjectCount_3; }
	inline int32_t* get_address_of_sCrlDataObjectCount_3() { return &___sCrlDataObjectCount_3; }
	inline void set_sCrlDataObjectCount_3(int32_t value)
	{
		___sCrlDataObjectCount_3 = value;
	}

	inline static int32_t get_offset_of_currentCrlStream_4() { return static_cast<int32_t>(offsetof(X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0, ___currentCrlStream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_currentCrlStream_4() const { return ___currentCrlStream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_currentCrlStream_4() { return &___currentCrlStream_4; }
	inline void set_currentCrlStream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___currentCrlStream_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentCrlStream_4), value);
	}
};

struct X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.PemParser BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlParser::PemCrlParser
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * ___PemCrlParser_0;

public:
	inline static int32_t get_offset_of_PemCrlParser_0() { return static_cast<int32_t>(offsetof(X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_StaticFields, ___PemCrlParser_0)); }
	inline PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * get_PemCrlParser_0() const { return ___PemCrlParser_0; }
	inline PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE ** get_address_of_PemCrlParser_0() { return &___PemCrlParser_0; }
	inline void set_PemCrlParser_0(PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE * value)
	{
		___PemCrlParser_0 = value;
		Il2CppCodeGenWriteBarrier((&___PemCrlParser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRLPARSER_T795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_H
#ifndef X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#define X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509ExtensionBase
struct  X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#ifndef X509SIGNATUREUTILITIES_T10BDD147690143446B4C9F074750F2ACD2677FBE_H
#define X509SIGNATUREUTILITIES_T10BDD147690143446B4C9F074750F2ACD2677FBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509SignatureUtilities
struct  X509SignatureUtilities_t10BDD147690143446B4C9F074750F2ACD2677FBE  : public RuntimeObject
{
public:

public:
};

struct X509SignatureUtilities_t10BDD147690143446B4C9F074750F2ACD2677FBE_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509SignatureUtilities::derNull
	Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * ___derNull_0;

public:
	inline static int32_t get_offset_of_derNull_0() { return static_cast<int32_t>(offsetof(X509SignatureUtilities_t10BDD147690143446B4C9F074750F2ACD2677FBE_StaticFields, ___derNull_0)); }
	inline Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * get_derNull_0() const { return ___derNull_0; }
	inline Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 ** get_address_of_derNull_0() { return &___derNull_0; }
	inline void set_derNull_0(Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * value)
	{
		___derNull_0 = value;
		Il2CppCodeGenWriteBarrier((&___derNull_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SIGNATUREUTILITIES_T10BDD147690143446B4C9F074750F2ACD2677FBE_H
#ifndef X509UTILITIES_TA2469B19FB181B4E80B991F7B9910788C60D2F5C_H
#define X509UTILITIES_TA2469B19FB181B4E80B991F7B9910788C60D2F5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Utilities
struct  X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C  : public RuntimeObject
{
public:

public:
};

struct X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Utilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Utilities::exParams
	RuntimeObject* ___exParams_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Utilities::noParams
	RuntimeObject* ___noParams_2;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_exParams_1() { return static_cast<int32_t>(offsetof(X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields, ___exParams_1)); }
	inline RuntimeObject* get_exParams_1() const { return ___exParams_1; }
	inline RuntimeObject** get_address_of_exParams_1() { return &___exParams_1; }
	inline void set_exParams_1(RuntimeObject* value)
	{
		___exParams_1 = value;
		Il2CppCodeGenWriteBarrier((&___exParams_1), value);
	}

	inline static int32_t get_offset_of_noParams_2() { return static_cast<int32_t>(offsetof(X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields, ___noParams_2)); }
	inline RuntimeObject* get_noParams_2() const { return ___noParams_2; }
	inline RuntimeObject** get_address_of_noParams_2() { return &___noParams_2; }
	inline void set_noParams_2(RuntimeObject* value)
	{
		___noParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___noParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509UTILITIES_TA2469B19FB181B4E80B991F7B9910788C60D2F5C_H
#ifndef X509V1CERTIFICATEGENERATOR_T41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC_H
#define X509V1CERTIFICATEGENERATOR_T41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V1CertificateGenerator
struct  X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V1TbsCertificateGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V1CertificateGenerator::tbsGen
	V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53 * ___tbsGen_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V1CertificateGenerator::sigOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sigOID_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V1CertificateGenerator::sigAlgId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgId_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V1CertificateGenerator::signatureAlgorithm
	String_t* ___signatureAlgorithm_3;

public:
	inline static int32_t get_offset_of_tbsGen_0() { return static_cast<int32_t>(offsetof(X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC, ___tbsGen_0)); }
	inline V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53 * get_tbsGen_0() const { return ___tbsGen_0; }
	inline V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53 ** get_address_of_tbsGen_0() { return &___tbsGen_0; }
	inline void set_tbsGen_0(V1TbsCertificateGenerator_t33F4E88EA04FE65CD2041D654F6F9C6D8B92FA53 * value)
	{
		___tbsGen_0 = value;
		Il2CppCodeGenWriteBarrier((&___tbsGen_0), value);
	}

	inline static int32_t get_offset_of_sigOID_1() { return static_cast<int32_t>(offsetof(X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC, ___sigOID_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sigOID_1() const { return ___sigOID_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sigOID_1() { return &___sigOID_1; }
	inline void set_sigOID_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sigOID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sigOID_1), value);
	}

	inline static int32_t get_offset_of_sigAlgId_2() { return static_cast<int32_t>(offsetof(X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC, ___sigAlgId_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgId_2() const { return ___sigAlgId_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgId_2() { return &___sigAlgId_2; }
	inline void set_sigAlgId_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgId_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgId_2), value);
	}

	inline static int32_t get_offset_of_signatureAlgorithm_3() { return static_cast<int32_t>(offsetof(X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC, ___signatureAlgorithm_3)); }
	inline String_t* get_signatureAlgorithm_3() const { return ___signatureAlgorithm_3; }
	inline String_t** get_address_of_signatureAlgorithm_3() { return &___signatureAlgorithm_3; }
	inline void set_signatureAlgorithm_3(String_t* value)
	{
		___signatureAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509V1CERTIFICATEGENERATOR_T41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC_H
#ifndef X509V2ATTRIBUTECERTIFICATEGENERATOR_TFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4_H
#define X509V2ATTRIBUTECERTIFICATEGENERATOR_TFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificateGenerator
struct  X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificateGenerator::extGenerator
	X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * ___extGenerator_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2AttributeCertificateInfoGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificateGenerator::acInfoGen
	V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7 * ___acInfoGen_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificateGenerator::sigOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sigOID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificateGenerator::sigAlgId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgId_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificateGenerator::signatureAlgorithm
	String_t* ___signatureAlgorithm_4;

public:
	inline static int32_t get_offset_of_extGenerator_0() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4, ___extGenerator_0)); }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * get_extGenerator_0() const { return ___extGenerator_0; }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 ** get_address_of_extGenerator_0() { return &___extGenerator_0; }
	inline void set_extGenerator_0(X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * value)
	{
		___extGenerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___extGenerator_0), value);
	}

	inline static int32_t get_offset_of_acInfoGen_1() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4, ___acInfoGen_1)); }
	inline V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7 * get_acInfoGen_1() const { return ___acInfoGen_1; }
	inline V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7 ** get_address_of_acInfoGen_1() { return &___acInfoGen_1; }
	inline void set_acInfoGen_1(V2AttributeCertificateInfoGenerator_t3F66539768809639E7E5464B5FAAB97106042FF7 * value)
	{
		___acInfoGen_1 = value;
		Il2CppCodeGenWriteBarrier((&___acInfoGen_1), value);
	}

	inline static int32_t get_offset_of_sigOID_2() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4, ___sigOID_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sigOID_2() const { return ___sigOID_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sigOID_2() { return &___sigOID_2; }
	inline void set_sigOID_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sigOID_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigOID_2), value);
	}

	inline static int32_t get_offset_of_sigAlgId_3() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4, ___sigAlgId_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgId_3() const { return ___sigAlgId_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgId_3() { return &___sigAlgId_3; }
	inline void set_sigAlgId_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgId_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgId_3), value);
	}

	inline static int32_t get_offset_of_signatureAlgorithm_4() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4, ___signatureAlgorithm_4)); }
	inline String_t* get_signatureAlgorithm_4() const { return ___signatureAlgorithm_4; }
	inline String_t** get_address_of_signatureAlgorithm_4() { return &___signatureAlgorithm_4; }
	inline void set_signatureAlgorithm_4(String_t* value)
	{
		___signatureAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509V2ATTRIBUTECERTIFICATEGENERATOR_TFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4_H
#ifndef X509V2CRLGENERATOR_TDBF91738285ED28BF6A415C4A6FD1120DEA012D6_H
#define X509V2CRLGENERATOR_TDBF91738285ED28BF6A415C4A6FD1120DEA012D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2CrlGenerator
struct  X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2CrlGenerator::extGenerator
	X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * ___extGenerator_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V2TbsCertListGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2CrlGenerator::tbsGen
	V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D * ___tbsGen_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2CrlGenerator::sigOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sigOID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2CrlGenerator::sigAlgId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgId_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2CrlGenerator::signatureAlgorithm
	String_t* ___signatureAlgorithm_4;

public:
	inline static int32_t get_offset_of_extGenerator_0() { return static_cast<int32_t>(offsetof(X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6, ___extGenerator_0)); }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * get_extGenerator_0() const { return ___extGenerator_0; }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 ** get_address_of_extGenerator_0() { return &___extGenerator_0; }
	inline void set_extGenerator_0(X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * value)
	{
		___extGenerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___extGenerator_0), value);
	}

	inline static int32_t get_offset_of_tbsGen_1() { return static_cast<int32_t>(offsetof(X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6, ___tbsGen_1)); }
	inline V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D * get_tbsGen_1() const { return ___tbsGen_1; }
	inline V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D ** get_address_of_tbsGen_1() { return &___tbsGen_1; }
	inline void set_tbsGen_1(V2TbsCertListGenerator_t01974F84CB55935CA00B1719E3385C8F277C356D * value)
	{
		___tbsGen_1 = value;
		Il2CppCodeGenWriteBarrier((&___tbsGen_1), value);
	}

	inline static int32_t get_offset_of_sigOID_2() { return static_cast<int32_t>(offsetof(X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6, ___sigOID_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sigOID_2() const { return ___sigOID_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sigOID_2() { return &___sigOID_2; }
	inline void set_sigOID_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sigOID_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigOID_2), value);
	}

	inline static int32_t get_offset_of_sigAlgId_3() { return static_cast<int32_t>(offsetof(X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6, ___sigAlgId_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgId_3() const { return ___sigAlgId_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgId_3() { return &___sigAlgId_3; }
	inline void set_sigAlgId_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgId_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgId_3), value);
	}

	inline static int32_t get_offset_of_signatureAlgorithm_4() { return static_cast<int32_t>(offsetof(X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6, ___signatureAlgorithm_4)); }
	inline String_t* get_signatureAlgorithm_4() const { return ___signatureAlgorithm_4; }
	inline String_t** get_address_of_signatureAlgorithm_4() { return &___signatureAlgorithm_4; }
	inline void set_signatureAlgorithm_4(String_t* value)
	{
		___signatureAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509V2CRLGENERATOR_TDBF91738285ED28BF6A415C4A6FD1120DEA012D6_H
#ifndef X509V3CERTIFICATEGENERATOR_T644480B3A02B878F584248CB2362B514175A7148_H
#define X509V3CERTIFICATEGENERATOR_T644480B3A02B878F584248CB2362B514175A7148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V3CertificateGenerator
struct  X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V3CertificateGenerator::extGenerator
	X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * ___extGenerator_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.V3TbsCertificateGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V3CertificateGenerator::tbsGen
	V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16 * ___tbsGen_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V3CertificateGenerator::sigOid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sigOid_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V3CertificateGenerator::sigAlgId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgId_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V3CertificateGenerator::signatureAlgorithm
	String_t* ___signatureAlgorithm_4;

public:
	inline static int32_t get_offset_of_extGenerator_0() { return static_cast<int32_t>(offsetof(X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148, ___extGenerator_0)); }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * get_extGenerator_0() const { return ___extGenerator_0; }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 ** get_address_of_extGenerator_0() { return &___extGenerator_0; }
	inline void set_extGenerator_0(X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * value)
	{
		___extGenerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___extGenerator_0), value);
	}

	inline static int32_t get_offset_of_tbsGen_1() { return static_cast<int32_t>(offsetof(X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148, ___tbsGen_1)); }
	inline V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16 * get_tbsGen_1() const { return ___tbsGen_1; }
	inline V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16 ** get_address_of_tbsGen_1() { return &___tbsGen_1; }
	inline void set_tbsGen_1(V3TbsCertificateGenerator_tB3A4A9391AE49D47FE5A24FE16A6F14D49043B16 * value)
	{
		___tbsGen_1 = value;
		Il2CppCodeGenWriteBarrier((&___tbsGen_1), value);
	}

	inline static int32_t get_offset_of_sigOid_2() { return static_cast<int32_t>(offsetof(X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148, ___sigOid_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sigOid_2() const { return ___sigOid_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sigOid_2() { return &___sigOid_2; }
	inline void set_sigOid_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sigOid_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigOid_2), value);
	}

	inline static int32_t get_offset_of_sigAlgId_3() { return static_cast<int32_t>(offsetof(X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148, ___sigAlgId_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgId_3() const { return ___sigAlgId_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgId_3() { return &___sigAlgId_3; }
	inline void set_sigAlgId_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgId_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgId_3), value);
	}

	inline static int32_t get_offset_of_signatureAlgorithm_4() { return static_cast<int32_t>(offsetof(X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148, ___signatureAlgorithm_4)); }
	inline String_t* get_signatureAlgorithm_4() const { return ___signatureAlgorithm_4; }
	inline String_t** get_address_of_signatureAlgorithm_4() { return &___signatureAlgorithm_4; }
	inline void set_signatureAlgorithm_4(String_t* value)
	{
		___signatureAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509V3CERTIFICATEGENERATOR_T644480B3A02B878F584248CB2362B514175A7148_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef HOSTCONNECTIONKEY_TA8107B3A146F89B83919F1CB13120EA675438A83_H
#define HOSTCONNECTIONKEY_TA8107B3A146F89B83919F1CB13120EA675438A83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.HostConnectionKey
struct  HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83 
{
public:
	// System.String BestHTTP.Core.HostConnectionKey::Host
	String_t* ___Host_0;
	// System.String BestHTTP.Core.HostConnectionKey::Connection
	String_t* ___Connection_1;

public:
	inline static int32_t get_offset_of_Host_0() { return static_cast<int32_t>(offsetof(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83, ___Host_0)); }
	inline String_t* get_Host_0() const { return ___Host_0; }
	inline String_t** get_address_of_Host_0() { return &___Host_0; }
	inline void set_Host_0(String_t* value)
	{
		___Host_0 = value;
		Il2CppCodeGenWriteBarrier((&___Host_0), value);
	}

	inline static int32_t get_offset_of_Connection_1() { return static_cast<int32_t>(offsetof(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83, ___Connection_1)); }
	inline String_t* get_Connection_1() const { return ___Connection_1; }
	inline String_t** get_address_of_Connection_1() { return &___Connection_1; }
	inline void set_Connection_1(String_t* value)
	{
		___Connection_1 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Core.HostConnectionKey
struct HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83_marshaled_pinvoke
{
	char* ___Host_0;
	char* ___Connection_1;
};
// Native definition for COM marshalling of BestHTTP.Core.HostConnectionKey
struct HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83_marshaled_com
{
	Il2CppChar* ___Host_0;
	Il2CppChar* ___Connection_1;
};
#endif // HOSTCONNECTIONKEY_TA8107B3A146F89B83919F1CB13120EA675438A83_H
#ifndef AUTHORITYKEYIDENTIFIER_T720D49360890539809676FFD0BE6209BA9BC93EA_H
#define AUTHORITYKEYIDENTIFIER_T720D49360890539809676FFD0BE6209BA9BC93EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier
struct  AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier::keyidentifier
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___keyidentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralNames BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier::certissuer
	GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * ___certissuer_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AuthorityKeyIdentifier::certserno
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certserno_4;

public:
	inline static int32_t get_offset_of_keyidentifier_2() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA, ___keyidentifier_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_keyidentifier_2() const { return ___keyidentifier_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_keyidentifier_2() { return &___keyidentifier_2; }
	inline void set_keyidentifier_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___keyidentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyidentifier_2), value);
	}

	inline static int32_t get_offset_of_certissuer_3() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA, ___certissuer_3)); }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * get_certissuer_3() const { return ___certissuer_3; }
	inline GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD ** get_address_of_certissuer_3() { return &___certissuer_3; }
	inline void set_certissuer_3(GeneralNames_t8FC331E0DBB7C3881577692B6083FA269A8BE8FD * value)
	{
		___certissuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___certissuer_3), value);
	}

	inline static int32_t get_offset_of_certserno_4() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA, ___certserno_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certserno_4() const { return ___certserno_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certserno_4() { return &___certserno_4; }
	inline void set_certserno_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certserno_4 = value;
		Il2CppCodeGenWriteBarrier((&___certserno_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYKEYIDENTIFIER_T720D49360890539809676FFD0BE6209BA9BC93EA_H
#ifndef SUBJECTKEYIDENTIFIER_TF922F6F32716AB7578A5809B185B104DD0D538D7_H
#define SUBJECTKEYIDENTIFIER_TF922F6F32716AB7578A5809B185B104DD0D538D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectKeyIdentifier
struct  SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectKeyIdentifier::keyIdentifier
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyIdentifier_2;

public:
	inline static int32_t get_offset_of_keyIdentifier_2() { return static_cast<int32_t>(offsetof(SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7, ___keyIdentifier_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyIdentifier_2() const { return ___keyIdentifier_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyIdentifier_2() { return &___keyIdentifier_2; }
	inline void set_keyIdentifier_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTKEYIDENTIFIER_TF922F6F32716AB7578A5809B185B104DD0D538D7_H
#ifndef X509STOREEXCEPTION_T1D46DCD3C70CB529F5CF3A4417C1B13EEDE6ACC0_H
#define X509STOREEXCEPTION_T1D46DCD3C70CB529F5CF3A4417C1B13EEDE6ACC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.X509StoreException
struct  X509StoreException_t1D46DCD3C70CB529F5CF3A4417C1B13EEDE6ACC0  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STOREEXCEPTION_T1D46DCD3C70CB529F5CF3A4417C1B13EEDE6ACC0_H
#ifndef X509ATTRIBUTE_T3B556C69F15E13F3E1629F7AF75DFD0A85C27363_H
#define X509ATTRIBUTE_T3B556C69F15E13F3E1629F7AF75DFD0A85C27363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Attribute
struct  X509Attribute_t3B556C69F15E13F3E1629F7AF75DFD0A85C27363  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeX509 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Attribute::attr
	AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76 * ___attr_2;

public:
	inline static int32_t get_offset_of_attr_2() { return static_cast<int32_t>(offsetof(X509Attribute_t3B556C69F15E13F3E1629F7AF75DFD0A85C27363, ___attr_2)); }
	inline AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76 * get_attr_2() const { return ___attr_2; }
	inline AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76 ** get_address_of_attr_2() { return &___attr_2; }
	inline void set_attr_2(AttributeX509_t27D2C1B196ACCEF883E0042D12A53A7A9AC8CE76 * value)
	{
		___attr_2 = value;
		Il2CppCodeGenWriteBarrier((&___attr_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ATTRIBUTE_T3B556C69F15E13F3E1629F7AF75DFD0A85C27363_H
#ifndef X509KEYUSAGE_T9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6_H
#define X509KEYUSAGE_T9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509KeyUsage
struct  X509KeyUsage_t9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509KeyUsage::usage
	int32_t ___usage_11;

public:
	inline static int32_t get_offset_of_usage_11() { return static_cast<int32_t>(offsetof(X509KeyUsage_t9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6, ___usage_11)); }
	inline int32_t get_usage_11() const { return ___usage_11; }
	inline int32_t* get_address_of_usage_11() { return &___usage_11; }
	inline void set_usage_11(int32_t value)
	{
		___usage_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGE_T9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6_H
#ifndef RAWFRAMEDATA_T607B8D48C32318464F03713BFFCE3177962E5804_H
#define RAWFRAMEDATA_T607B8D48C32318464F03713BFFCE3177962E5804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.Frames.RawFrameData
struct  RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804 
{
public:
	// System.Byte[] BestHTTP.WebSocket.Frames.RawFrameData::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.Int32 BestHTTP.WebSocket.Frames.RawFrameData::Length
	int32_t ___Length_1;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWFRAMEDATA_T607B8D48C32318464F03713BFFCE3177962E5804_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#define UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_tAE45CEF73BF720100519F6867F32145D075F928E 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_tAE45CEF73BF720100519F6867F32145D075F928E, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_TAE45CEF73BF720100519F6867F32145D075F928E_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COMPRESSIONLEVEL_T84967AC2C882EA97443AE1BFB4278A7F916C352A_H
#define COMPRESSIONLEVEL_T84967AC2C882EA97443AE1BFB4278A7F916C352A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.CompressionLevel
struct  CompressionLevel_t84967AC2C882EA97443AE1BFB4278A7F916C352A 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.CompressionLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionLevel_t84967AC2C882EA97443AE1BFB4278A7F916C352A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONLEVEL_T84967AC2C882EA97443AE1BFB4278A7F916C352A_H
#ifndef BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#define BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseInputStream
struct  BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseInputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTSTREAM_TFB934C38CB5B81842A543CE8228DDE5235F1E2E4_H
#ifndef BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#define BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifndef FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#define FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.FilterStream
struct  FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.FilterStream::s
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___s_5;

public:
	inline static int32_t get_offset_of_s_5() { return static_cast<int32_t>(offsetof(FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF, ___s_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_s_5() const { return ___s_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_s_5() { return &___s_5; }
	inline void set_s_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___s_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERSTREAM_T45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF_H
#ifndef ZDEFLATEROUTPUTSTREAM_T606844849BFF5642A1C56ACC71A8085D66B52282_H
#define ZDEFLATEROUTPUTSTREAM_T606844849BFF5642A1C56ACC71A8085D66B52282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZDeflaterOutputStream
struct  ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZDeflaterOutputStream::z
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___z_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZDeflaterOutputStream::flushLevel
	int32_t ___flushLevel_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZDeflaterOutputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZDeflaterOutputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_9;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZDeflaterOutputStream::outp
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___outp_10;

public:
	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282, ___z_5)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_z_5() const { return ___z_5; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___z_5 = value;
		Il2CppCodeGenWriteBarrier((&___z_5), value);
	}

	inline static int32_t get_offset_of_flushLevel_6() { return static_cast<int32_t>(offsetof(ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282, ___flushLevel_6)); }
	inline int32_t get_flushLevel_6() const { return ___flushLevel_6; }
	inline int32_t* get_address_of_flushLevel_6() { return &___flushLevel_6; }
	inline void set_flushLevel_6(int32_t value)
	{
		___flushLevel_6 = value;
	}

	inline static int32_t get_offset_of_buf_8() { return static_cast<int32_t>(offsetof(ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282, ___buf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_8() const { return ___buf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_8() { return &___buf_8; }
	inline void set_buf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf_8), value);
	}

	inline static int32_t get_offset_of_buf1_9() { return static_cast<int32_t>(offsetof(ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282, ___buf1_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_9() const { return ___buf1_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_9() { return &___buf1_9; }
	inline void set_buf1_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_9 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_9), value);
	}

	inline static int32_t get_offset_of_outp_10() { return static_cast<int32_t>(offsetof(ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282, ___outp_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_outp_10() const { return ___outp_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_outp_10() { return &___outp_10; }
	inline void set_outp_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___outp_10 = value;
		Il2CppCodeGenWriteBarrier((&___outp_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZDEFLATEROUTPUTSTREAM_T606844849BFF5642A1C56ACC71A8085D66B52282_H
#ifndef ZINFLATERINPUTSTREAM_T86FF5C9A563D7A54743BB47012DF006BAE869540_H
#define ZINFLATERINPUTSTREAM_T86FF5C9A563D7A54743BB47012DF006BAE869540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream
struct  ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream::z
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___z_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream::flushLevel
	int32_t ___flushLevel_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_9;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream::inp
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inp_10;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInflaterInputStream::nomoreinput
	bool ___nomoreinput_11;

public:
	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540, ___z_5)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_z_5() const { return ___z_5; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___z_5 = value;
		Il2CppCodeGenWriteBarrier((&___z_5), value);
	}

	inline static int32_t get_offset_of_flushLevel_6() { return static_cast<int32_t>(offsetof(ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540, ___flushLevel_6)); }
	inline int32_t get_flushLevel_6() const { return ___flushLevel_6; }
	inline int32_t* get_address_of_flushLevel_6() { return &___flushLevel_6; }
	inline void set_flushLevel_6(int32_t value)
	{
		___flushLevel_6 = value;
	}

	inline static int32_t get_offset_of_buf_8() { return static_cast<int32_t>(offsetof(ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540, ___buf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_8() const { return ___buf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_8() { return &___buf_8; }
	inline void set_buf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf_8), value);
	}

	inline static int32_t get_offset_of_buf1_9() { return static_cast<int32_t>(offsetof(ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540, ___buf1_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_9() const { return ___buf1_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_9() { return &___buf1_9; }
	inline void set_buf1_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_9 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_9), value);
	}

	inline static int32_t get_offset_of_inp_10() { return static_cast<int32_t>(offsetof(ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540, ___inp_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inp_10() const { return ___inp_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inp_10() { return &___inp_10; }
	inline void set_inp_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inp_10 = value;
		Il2CppCodeGenWriteBarrier((&___inp_10), value);
	}

	inline static int32_t get_offset_of_nomoreinput_11() { return static_cast<int32_t>(offsetof(ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540, ___nomoreinput_11)); }
	inline bool get_nomoreinput_11() const { return ___nomoreinput_11; }
	inline bool* get_address_of_nomoreinput_11() { return &___nomoreinput_11; }
	inline void set_nomoreinput_11(bool value)
	{
		___nomoreinput_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZINFLATERINPUTSTREAM_T86FF5C9A563D7A54743BB47012DF006BAE869540_H
#ifndef ZINPUTSTREAM_TCE3B181A7C7822F11CAA7564FEC6374AB61B676F_H
#define ZINPUTSTREAM_TCE3B181A7C7822F11CAA7564FEC6374AB61B676F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream
struct  ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::z
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___z_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::flushLevel
	int32_t ___flushLevel_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::compress
	bool ___compress_10;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::input
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::closed
	bool ___closed_12;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZInputStream::nomoreinput
	bool ___nomoreinput_13;

public:
	inline static int32_t get_offset_of_z_6() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___z_6)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_z_6() const { return ___z_6; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_z_6() { return &___z_6; }
	inline void set_z_6(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___z_6 = value;
		Il2CppCodeGenWriteBarrier((&___z_6), value);
	}

	inline static int32_t get_offset_of_flushLevel_7() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___flushLevel_7)); }
	inline int32_t get_flushLevel_7() const { return ___flushLevel_7; }
	inline int32_t* get_address_of_flushLevel_7() { return &___flushLevel_7; }
	inline void set_flushLevel_7(int32_t value)
	{
		___flushLevel_7 = value;
	}

	inline static int32_t get_offset_of_buf_8() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___buf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_8() const { return ___buf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_8() { return &___buf_8; }
	inline void set_buf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf_8), value);
	}

	inline static int32_t get_offset_of_buf1_9() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___buf1_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_9() const { return ___buf1_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_9() { return &___buf1_9; }
	inline void set_buf1_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_9 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_9), value);
	}

	inline static int32_t get_offset_of_compress_10() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___compress_10)); }
	inline bool get_compress_10() const { return ___compress_10; }
	inline bool* get_address_of_compress_10() { return &___compress_10; }
	inline void set_compress_10(bool value)
	{
		___compress_10 = value;
	}

	inline static int32_t get_offset_of_input_11() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___input_11)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_input_11() const { return ___input_11; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_input_11() { return &___input_11; }
	inline void set_input_11(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___input_11 = value;
		Il2CppCodeGenWriteBarrier((&___input_11), value);
	}

	inline static int32_t get_offset_of_closed_12() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___closed_12)); }
	inline bool get_closed_12() const { return ___closed_12; }
	inline bool* get_address_of_closed_12() { return &___closed_12; }
	inline void set_closed_12(bool value)
	{
		___closed_12 = value;
	}

	inline static int32_t get_offset_of_nomoreinput_13() { return static_cast<int32_t>(offsetof(ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F, ___nomoreinput_13)); }
	inline bool get_nomoreinput_13() const { return ___nomoreinput_13; }
	inline bool* get_address_of_nomoreinput_13() { return &___nomoreinput_13; }
	inline void set_nomoreinput_13(bool value)
	{
		___nomoreinput_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZINPUTSTREAM_TCE3B181A7C7822F11CAA7564FEC6374AB61B676F_H
#ifndef ZOUTPUTSTREAM_T504E5B441A273254366752284D503186AF93F77A_H
#define ZOUTPUTSTREAM_T504E5B441A273254366752284D503186AF93F77A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream
struct  ZOutputStream_t504E5B441A273254366752284D503186AF93F77A  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::z
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___z_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::flushLevel
	int32_t ___flushLevel_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::compress
	bool ___compress_10;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::output
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___output_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::closed
	bool ___closed_12;

public:
	inline static int32_t get_offset_of_z_6() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___z_6)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_z_6() const { return ___z_6; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_z_6() { return &___z_6; }
	inline void set_z_6(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___z_6 = value;
		Il2CppCodeGenWriteBarrier((&___z_6), value);
	}

	inline static int32_t get_offset_of_flushLevel_7() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___flushLevel_7)); }
	inline int32_t get_flushLevel_7() const { return ___flushLevel_7; }
	inline int32_t* get_address_of_flushLevel_7() { return &___flushLevel_7; }
	inline void set_flushLevel_7(int32_t value)
	{
		___flushLevel_7 = value;
	}

	inline static int32_t get_offset_of_buf_8() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___buf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_8() const { return ___buf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_8() { return &___buf_8; }
	inline void set_buf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf_8), value);
	}

	inline static int32_t get_offset_of_buf1_9() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___buf1_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_9() const { return ___buf1_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_9() { return &___buf1_9; }
	inline void set_buf1_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_9 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_9), value);
	}

	inline static int32_t get_offset_of_compress_10() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___compress_10)); }
	inline bool get_compress_10() const { return ___compress_10; }
	inline bool* get_address_of_compress_10() { return &___compress_10; }
	inline void set_compress_10(bool value)
	{
		___compress_10 = value;
	}

	inline static int32_t get_offset_of_output_11() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___output_11)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_output_11() const { return ___output_11; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_output_11() { return &___output_11; }
	inline void set_output_11(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___output_11 = value;
		Il2CppCodeGenWriteBarrier((&___output_11), value);
	}

	inline static int32_t get_offset_of_closed_12() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___closed_12)); }
	inline bool get_closed_12() const { return ___closed_12; }
	inline bool* get_address_of_closed_12() { return &___closed_12; }
	inline void set_closed_12(bool value)
	{
		___closed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOUTPUTSTREAM_T504E5B441A273254366752284D503186AF93F77A_H
#ifndef AUTHORITYKEYIDENTIFIERSTRUCTURE_T6AE8052DF734F9E5F6FF1EB0E54CB16E7164E642_H
#define AUTHORITYKEYIDENTIFIERSTRUCTURE_T6AE8052DF734F9E5F6FF1EB0E54CB16E7164E642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Extension.AuthorityKeyIdentifierStructure
struct  AuthorityKeyIdentifierStructure_t6AE8052DF734F9E5F6FF1EB0E54CB16E7164E642  : public AuthorityKeyIdentifier_t720D49360890539809676FFD0BE6209BA9BC93EA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYKEYIDENTIFIERSTRUCTURE_T6AE8052DF734F9E5F6FF1EB0E54CB16E7164E642_H
#ifndef SUBJECTKEYIDENTIFIERSTRUCTURE_T7DB46DFB875613D5FEAC43667EE895A920C8C575_H
#define SUBJECTKEYIDENTIFIERSTRUCTURE_T7DB46DFB875613D5FEAC43667EE895A920C8C575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Extension.SubjectKeyIdentifierStructure
struct  SubjectKeyIdentifierStructure_t7DB46DFB875613D5FEAC43667EE895A920C8C575  : public SubjectKeyIdentifier_tF922F6F32716AB7578A5809B185B104DD0D538D7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTKEYIDENTIFIERSTRUCTURE_T7DB46DFB875613D5FEAC43667EE895A920C8C575_H
#ifndef NOSUCHSTOREEXCEPTION_T624010015CF3D94AD307936B95C2F323391FC495_H
#define NOSUCHSTOREEXCEPTION_T624010015CF3D94AD307936B95C2F323391FC495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.NoSuchStoreException
struct  NoSuchStoreException_t624010015CF3D94AD307936B95C2F323391FC495  : public X509StoreException_t1D46DCD3C70CB529F5CF3A4417C1B13EEDE6ACC0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOSUCHSTOREEXCEPTION_T624010015CF3D94AD307936B95C2F323391FC495_H
#ifndef X509CERTIFICATE_T0F915BB244D50EA295819D8FBDEDD50B178C789A_H
#define X509CERTIFICATE_T0F915BB244D50EA295819D8FBDEDD50B178C789A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate
struct  X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::c
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * ___c_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.BasicConstraints BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::basicConstraints
	BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217 * ___basicConstraints_1;
	// System.Boolean[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::keyUsage
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___keyUsage_2;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::cacheLock
	RuntimeObject * ___cacheLock_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::publicKeyValue
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___publicKeyValue_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::hashValueSet
	bool ___hashValueSet_5;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate::hashValue
	int32_t ___hashValue_6;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___c_0)); }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * get_c_0() const { return ___c_0; }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_basicConstraints_1() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___basicConstraints_1)); }
	inline BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217 * get_basicConstraints_1() const { return ___basicConstraints_1; }
	inline BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217 ** get_address_of_basicConstraints_1() { return &___basicConstraints_1; }
	inline void set_basicConstraints_1(BasicConstraints_t1F4EBA54689CD8E1522DAC83DC632226F189D217 * value)
	{
		___basicConstraints_1 = value;
		Il2CppCodeGenWriteBarrier((&___basicConstraints_1), value);
	}

	inline static int32_t get_offset_of_keyUsage_2() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___keyUsage_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_keyUsage_2() const { return ___keyUsage_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_keyUsage_2() { return &___keyUsage_2; }
	inline void set_keyUsage_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___keyUsage_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyUsage_2), value);
	}

	inline static int32_t get_offset_of_cacheLock_3() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___cacheLock_3)); }
	inline RuntimeObject * get_cacheLock_3() const { return ___cacheLock_3; }
	inline RuntimeObject ** get_address_of_cacheLock_3() { return &___cacheLock_3; }
	inline void set_cacheLock_3(RuntimeObject * value)
	{
		___cacheLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___cacheLock_3), value);
	}

	inline static int32_t get_offset_of_publicKeyValue_4() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___publicKeyValue_4)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_publicKeyValue_4() const { return ___publicKeyValue_4; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_publicKeyValue_4() { return &___publicKeyValue_4; }
	inline void set_publicKeyValue_4(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___publicKeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyValue_4), value);
	}

	inline static int32_t get_offset_of_hashValueSet_5() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___hashValueSet_5)); }
	inline bool get_hashValueSet_5() const { return ___hashValueSet_5; }
	inline bool* get_address_of_hashValueSet_5() { return &___hashValueSet_5; }
	inline void set_hashValueSet_5(bool value)
	{
		___hashValueSet_5 = value;
	}

	inline static int32_t get_offset_of_hashValue_6() { return static_cast<int32_t>(offsetof(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A, ___hashValue_6)); }
	inline int32_t get_hashValue_6() const { return ___hashValue_6; }
	inline int32_t* get_address_of_hashValue_6() { return &___hashValue_6; }
	inline void set_hashValue_6(int32_t value)
	{
		___hashValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T0F915BB244D50EA295819D8FBDEDD50B178C789A_H
#ifndef X509CRL_T520B5625C6B26501AD20216DD2F96852CAE17485_H
#define X509CRL_T520B5625C6B26501AD20216DD2F96852CAE17485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl
struct  X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CertificateList BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl::c
	CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 * ___c_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl::sigAlgName
	String_t* ___sigAlgName_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl::sigAlgParams
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sigAlgParams_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl::isIndirect
	bool ___isIndirect_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl::hashValueSet
	bool ___hashValueSet_4;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Crl::hashValue
	int32_t ___hashValue_5;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485, ___c_0)); }
	inline CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 * get_c_0() const { return ___c_0; }
	inline CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(CertificateList_t6BD785802EAE6A420673C7F5FF1EAA25EDEF5AF8 * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_sigAlgName_1() { return static_cast<int32_t>(offsetof(X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485, ___sigAlgName_1)); }
	inline String_t* get_sigAlgName_1() const { return ___sigAlgName_1; }
	inline String_t** get_address_of_sigAlgName_1() { return &___sigAlgName_1; }
	inline void set_sigAlgName_1(String_t* value)
	{
		___sigAlgName_1 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgName_1), value);
	}

	inline static int32_t get_offset_of_sigAlgParams_2() { return static_cast<int32_t>(offsetof(X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485, ___sigAlgParams_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sigAlgParams_2() const { return ___sigAlgParams_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sigAlgParams_2() { return &___sigAlgParams_2; }
	inline void set_sigAlgParams_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sigAlgParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgParams_2), value);
	}

	inline static int32_t get_offset_of_isIndirect_3() { return static_cast<int32_t>(offsetof(X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485, ___isIndirect_3)); }
	inline bool get_isIndirect_3() const { return ___isIndirect_3; }
	inline bool* get_address_of_isIndirect_3() { return &___isIndirect_3; }
	inline void set_isIndirect_3(bool value)
	{
		___isIndirect_3 = value;
	}

	inline static int32_t get_offset_of_hashValueSet_4() { return static_cast<int32_t>(offsetof(X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485, ___hashValueSet_4)); }
	inline bool get_hashValueSet_4() const { return ___hashValueSet_4; }
	inline bool* get_address_of_hashValueSet_4() { return &___hashValueSet_4; }
	inline void set_hashValueSet_4(bool value)
	{
		___hashValueSet_4 = value;
	}

	inline static int32_t get_offset_of_hashValue_5() { return static_cast<int32_t>(offsetof(X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485, ___hashValue_5)); }
	inline int32_t get_hashValue_5() const { return ___hashValue_5; }
	inline int32_t* get_address_of_hashValue_5() { return &___hashValue_5; }
	inline void set_hashValue_5(int32_t value)
	{
		___hashValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRL_T520B5625C6B26501AD20216DD2F96852CAE17485_H
#ifndef X509CRLENTRY_T7F0EC211082F01063A226A53F6FBD47EBC245B15_H
#define X509CRLENTRY_T7F0EC211082F01063A226A53F6FBD47EBC245B15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry
struct  X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlEntry BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry::c
	CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C * ___c_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry::isIndirect
	bool ___isIndirect_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry::previousCertificateIssuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___previousCertificateIssuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry::certificateIssuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___certificateIssuer_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry::hashValueSet
	bool ___hashValueSet_4;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509CrlEntry::hashValue
	int32_t ___hashValue_5;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15, ___c_0)); }
	inline CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C * get_c_0() const { return ___c_0; }
	inline CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(CrlEntry_t6D850A322CED0E23E960207D7E59AD5493D5F63C * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_isIndirect_1() { return static_cast<int32_t>(offsetof(X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15, ___isIndirect_1)); }
	inline bool get_isIndirect_1() const { return ___isIndirect_1; }
	inline bool* get_address_of_isIndirect_1() { return &___isIndirect_1; }
	inline void set_isIndirect_1(bool value)
	{
		___isIndirect_1 = value;
	}

	inline static int32_t get_offset_of_previousCertificateIssuer_2() { return static_cast<int32_t>(offsetof(X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15, ___previousCertificateIssuer_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_previousCertificateIssuer_2() const { return ___previousCertificateIssuer_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_previousCertificateIssuer_2() { return &___previousCertificateIssuer_2; }
	inline void set_previousCertificateIssuer_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___previousCertificateIssuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___previousCertificateIssuer_2), value);
	}

	inline static int32_t get_offset_of_certificateIssuer_3() { return static_cast<int32_t>(offsetof(X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15, ___certificateIssuer_3)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_certificateIssuer_3() const { return ___certificateIssuer_3; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_certificateIssuer_3() { return &___certificateIssuer_3; }
	inline void set_certificateIssuer_3(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___certificateIssuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificateIssuer_3), value);
	}

	inline static int32_t get_offset_of_hashValueSet_4() { return static_cast<int32_t>(offsetof(X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15, ___hashValueSet_4)); }
	inline bool get_hashValueSet_4() const { return ___hashValueSet_4; }
	inline bool* get_address_of_hashValueSet_4() { return &___hashValueSet_4; }
	inline void set_hashValueSet_4(bool value)
	{
		___hashValueSet_4 = value;
	}

	inline static int32_t get_offset_of_hashValue_5() { return static_cast<int32_t>(offsetof(X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15, ___hashValue_5)); }
	inline int32_t get_hashValue_5() const { return ___hashValue_5; }
	inline int32_t* get_address_of_hashValue_5() { return &___hashValue_5; }
	inline void set_hashValue_5(int32_t value)
	{
		___hashValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRLENTRY_T7F0EC211082F01063A226A53F6FBD47EBC245B15_H
#ifndef X509V2ATTRIBUTECERTIFICATE_T9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD_H
#define X509V2ATTRIBUTECERTIFICATE_T9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificate
struct  X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificate::cert
	AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * ___cert_0;
	// System.DateTime BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificate::notBefore
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___notBefore_1;
	// System.DateTime BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509V2AttributeCertificate::notAfter
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___notAfter_2;

public:
	inline static int32_t get_offset_of_cert_0() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD, ___cert_0)); }
	inline AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * get_cert_0() const { return ___cert_0; }
	inline AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B ** get_address_of_cert_0() { return &___cert_0; }
	inline void set_cert_0(AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * value)
	{
		___cert_0 = value;
		Il2CppCodeGenWriteBarrier((&___cert_0), value);
	}

	inline static int32_t get_offset_of_notBefore_1() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD, ___notBefore_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_notBefore_1() const { return ___notBefore_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_notBefore_1() { return &___notBefore_1; }
	inline void set_notBefore_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___notBefore_1 = value;
	}

	inline static int32_t get_offset_of_notAfter_2() { return static_cast<int32_t>(offsetof(X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD, ___notAfter_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_notAfter_2() const { return ___notAfter_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_notAfter_2() { return &___notAfter_2; }
	inline void set_notAfter_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___notAfter_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509V2ATTRIBUTECERTIFICATE_T9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD_H
#ifndef STATES_T0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70_H
#define STATES_T0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.States
struct  States_t0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70 
{
public:
	// System.Int32 BestHTTP.ServerSentEvents.States::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(States_t0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATES_T0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70_H
#ifndef WEBSOCKETFRAMETYPES_T73771AC782F5FFF8E3D97BF652383FE4AEEFE85E_H
#define WEBSOCKETFRAMETYPES_T73771AC782F5FFF8E3D97BF652383FE4AEEFE85E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.Frames.WebSocketFrameTypes
struct  WebSocketFrameTypes_t73771AC782F5FFF8E3D97BF652383FE4AEEFE85E 
{
public:
	// System.Byte BestHTTP.WebSocket.Frames.WebSocketFrameTypes::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketFrameTypes_t73771AC782F5FFF8E3D97BF652383FE4AEEFE85E, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETFRAMETYPES_T73771AC782F5FFF8E3D97BF652383FE4AEEFE85E_H
#ifndef WEBSOCKETSTATES_TEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460_H
#define WEBSOCKETSTATES_TEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocketStates
struct  WebSocketStates_tEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460 
{
public:
	// System.Byte BestHTTP.WebSocket.WebSocketStates::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketStates_tEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTATES_TEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460_H
#ifndef WEBSOCKETSTAUSCODES_T4233376B407476F1BA66FA16F70C8F8CEB784435_H
#define WEBSOCKETSTAUSCODES_T4233376B407476F1BA66FA16F70C8F8CEB784435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocketStausCodes
struct  WebSocketStausCodes_t4233376B407476F1BA66FA16F70C8F8CEB784435 
{
public:
	// System.UInt16 BestHTTP.WebSocket.WebSocketStausCodes::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketStausCodes_t4233376B407476F1BA66FA16F70C8F8CEB784435, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTAUSCODES_T4233376B407476F1BA66FA16F70C8F8CEB784435_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef INVALIDCASTEXCEPTION_T91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_H
#define INVALIDCASTEXCEPTION_T91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef MEMORYINPUTSTREAM_TF61D08F10AC0A9453ADF2922D2FA54C1C077C86E_H
#define MEMORYINPUTSTREAM_TF61D08F10AC0A9453ADF2922D2FA54C1C077C86E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.MemoryInputStream
struct  MemoryInputStream_tF61D08F10AC0A9453ADF2922D2FA54C1C077C86E  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYINPUTSTREAM_TF61D08F10AC0A9453ADF2922D2FA54C1C077C86E_H
#ifndef MEMORYOUTPUTSTREAM_T468D82831460535904A154438A5E8B56DF0119C2_H
#define MEMORYOUTPUTSTREAM_T468D82831460535904A154438A5E8B56DF0119C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.MemoryOutputStream
struct  MemoryOutputStream_t468D82831460535904A154438A5E8B56DF0119C2  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYOUTPUTSTREAM_T468D82831460535904A154438A5E8B56DF0119C2_H
#ifndef NULLOUTPUTSTREAM_TE2F7DB7E68F95E648F210FF1A0D54EF99FFB2A7A_H
#define NULLOUTPUTSTREAM_TE2F7DB7E68F95E648F210FF1A0D54EF99FFB2A7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.NullOutputStream
struct  NullOutputStream_tE2F7DB7E68F95E648F210FF1A0D54EF99FFB2A7A  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLOUTPUTSTREAM_TE2F7DB7E68F95E648F210FF1A0D54EF99FFB2A7A_H
#ifndef PUSHBACKSTREAM_T685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0_H
#define PUSHBACKSTREAM_T685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.PushbackStream
struct  PushbackStream_t685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0  : public FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.PushbackStream::buf
	int32_t ___buf_6;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(PushbackStream_t685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0, ___buf_6)); }
	inline int32_t get_buf_6() const { return ___buf_6; }
	inline int32_t* get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(int32_t value)
	{
		___buf_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUSHBACKSTREAM_T685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0_H
#ifndef STREAMOVERFLOWEXCEPTION_T33C848DA996CBC4B61E660106E8D5D12DEC111F8_H
#define STREAMOVERFLOWEXCEPTION_T33C848DA996CBC4B61E660106E8D5D12DEC111F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.StreamOverflowException
struct  StreamOverflowException_t33C848DA996CBC4B61E660106E8D5D12DEC111F8  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMOVERFLOWEXCEPTION_T33C848DA996CBC4B61E660106E8D5D12DEC111F8_H
#ifndef TEEINPUTSTREAM_TD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28_H
#define TEEINPUTSTREAM_TD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.TeeInputStream
struct  TeeInputStream_tD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28  : public BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.TeeInputStream::input
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input_6;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.TeeInputStream::tee
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___tee_7;

public:
	inline static int32_t get_offset_of_input_6() { return static_cast<int32_t>(offsetof(TeeInputStream_tD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28, ___input_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_input_6() const { return ___input_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_input_6() { return &___input_6; }
	inline void set_input_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___input_6 = value;
		Il2CppCodeGenWriteBarrier((&___input_6), value);
	}

	inline static int32_t get_offset_of_tee_7() { return static_cast<int32_t>(offsetof(TeeInputStream_tD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28, ___tee_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_tee_7() const { return ___tee_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_tee_7() { return &___tee_7; }
	inline void set_tee_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___tee_7 = value;
		Il2CppCodeGenWriteBarrier((&___tee_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEEINPUTSTREAM_TD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28_H
#ifndef TEEOUTPUTSTREAM_T0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36_H
#define TEEOUTPUTSTREAM_T0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.TeeOutputStream
struct  TeeOutputStream_t0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.TeeOutputStream::output
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___output_6;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.TeeOutputStream::tee
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___tee_7;

public:
	inline static int32_t get_offset_of_output_6() { return static_cast<int32_t>(offsetof(TeeOutputStream_t0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36, ___output_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_output_6() const { return ___output_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_output_6() { return &___output_6; }
	inline void set_output_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___output_6 = value;
		Il2CppCodeGenWriteBarrier((&___output_6), value);
	}

	inline static int32_t get_offset_of_tee_7() { return static_cast<int32_t>(offsetof(TeeOutputStream_t0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36, ___tee_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_tee_7() const { return ___tee_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_tee_7() { return &___tee_7; }
	inline void set_tee_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___tee_7 = value;
		Il2CppCodeGenWriteBarrier((&___tee_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEEOUTPUTSTREAM_T0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36_H
#ifndef MEMOABLERESETEXCEPTION_T8B342C58F680A5D1891FB58655C695831D0DAF61_H
#define MEMOABLERESETEXCEPTION_T8B342C58F680A5D1891FB58655C695831D0DAF61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.MemoableResetException
struct  MemoableResetException_t8B342C58F680A5D1891FB58655C695831D0DAF61  : public InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMOABLERESETEXCEPTION_T8B342C58F680A5D1891FB58655C695831D0DAF61_H
#ifndef EVENTSOURCE_TC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6_H
#define EVENTSOURCE_TC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.EventSource
struct  EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.ServerSentEvents.EventSource::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_0;
	// BestHTTP.ServerSentEvents.States BestHTTP.ServerSentEvents.EventSource::_state
	int32_t ____state_1;
	// System.TimeSpan BestHTTP.ServerSentEvents.EventSource::<ReconnectionTime>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CReconnectionTimeU3Ek__BackingField_2;
	// System.String BestHTTP.ServerSentEvents.EventSource::<LastEventId>k__BackingField
	String_t* ___U3CLastEventIdU3Ek__BackingField_3;
	// BestHTTP.Core.HostConnectionKey BestHTTP.ServerSentEvents.EventSource::<ConnectionKey>k__BackingField
	HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83  ___U3CConnectionKeyU3Ek__BackingField_4;
	// BestHTTP.HTTPRequest BestHTTP.ServerSentEvents.EventSource::<InternalRequest>k__BackingField
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___U3CInternalRequestU3Ek__BackingField_5;
	// BestHTTP.ServerSentEvents.OnGeneralEventDelegate BestHTTP.ServerSentEvents.EventSource::OnOpen
	OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B * ___OnOpen_6;
	// BestHTTP.ServerSentEvents.OnMessageDelegate BestHTTP.ServerSentEvents.EventSource::OnMessage
	OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D * ___OnMessage_7;
	// BestHTTP.ServerSentEvents.OnErrorDelegate BestHTTP.ServerSentEvents.EventSource::OnError
	OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2 * ___OnError_8;
	// BestHTTP.ServerSentEvents.OnRetryDelegate BestHTTP.ServerSentEvents.EventSource::OnRetry
	OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 * ___OnRetry_9;
	// BestHTTP.ServerSentEvents.OnGeneralEventDelegate BestHTTP.ServerSentEvents.EventSource::OnClosed
	OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B * ___OnClosed_10;
	// BestHTTP.ServerSentEvents.OnStateChangedDelegate BestHTTP.ServerSentEvents.EventSource::OnStateChanged
	OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE * ___OnStateChanged_11;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.ServerSentEvents.OnEventDelegate> BestHTTP.ServerSentEvents.EventSource::EventTable
	Dictionary_2_t7E96B3C27B69CD061EC5587E11344675BB70A2E9 * ___EventTable_12;
	// System.Byte BestHTTP.ServerSentEvents.EventSource::RetryCount
	uint8_t ___RetryCount_13;
	// System.DateTime BestHTTP.ServerSentEvents.EventSource::RetryCalled
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___RetryCalled_14;
	// System.Byte[] BestHTTP.ServerSentEvents.EventSource::LineBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___LineBuffer_15;
	// System.Int32 BestHTTP.ServerSentEvents.EventSource::LineBufferPos
	int32_t ___LineBufferPos_16;
	// BestHTTP.ServerSentEvents.Message BestHTTP.ServerSentEvents.EventSource::CurrentMessage
	Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3 * ___CurrentMessage_17;
	// System.Collections.Generic.List`1<BestHTTP.ServerSentEvents.Message> BestHTTP.ServerSentEvents.EventSource::CompletedMessages
	List_1_t01F3A1966CC10BAC639FFB6D44AB4A07695ACBC9 * ___CompletedMessages_18;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___U3CUriU3Ek__BackingField_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_0() const { return ___U3CUriU3Ek__BackingField_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_0() { return &___U3CUriU3Ek__BackingField_0; }
	inline void set_U3CUriU3Ek__BackingField_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of__state_1() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ____state_1)); }
	inline int32_t get__state_1() const { return ____state_1; }
	inline int32_t* get_address_of__state_1() { return &____state_1; }
	inline void set__state_1(int32_t value)
	{
		____state_1 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___U3CReconnectionTimeU3Ek__BackingField_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CReconnectionTimeU3Ek__BackingField_2() const { return ___U3CReconnectionTimeU3Ek__BackingField_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CReconnectionTimeU3Ek__BackingField_2() { return &___U3CReconnectionTimeU3Ek__BackingField_2; }
	inline void set_U3CReconnectionTimeU3Ek__BackingField_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CReconnectionTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLastEventIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___U3CLastEventIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CLastEventIdU3Ek__BackingField_3() const { return ___U3CLastEventIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CLastEventIdU3Ek__BackingField_3() { return &___U3CLastEventIdU3Ek__BackingField_3; }
	inline void set_U3CLastEventIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CLastEventIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastEventIdU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CConnectionKeyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___U3CConnectionKeyU3Ek__BackingField_4)); }
	inline HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83  get_U3CConnectionKeyU3Ek__BackingField_4() const { return ___U3CConnectionKeyU3Ek__BackingField_4; }
	inline HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83 * get_address_of_U3CConnectionKeyU3Ek__BackingField_4() { return &___U3CConnectionKeyU3Ek__BackingField_4; }
	inline void set_U3CConnectionKeyU3Ek__BackingField_4(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83  value)
	{
		___U3CConnectionKeyU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInternalRequestU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___U3CInternalRequestU3Ek__BackingField_5)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_U3CInternalRequestU3Ek__BackingField_5() const { return ___U3CInternalRequestU3Ek__BackingField_5; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_U3CInternalRequestU3Ek__BackingField_5() { return &___U3CInternalRequestU3Ek__BackingField_5; }
	inline void set_U3CInternalRequestU3Ek__BackingField_5(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___U3CInternalRequestU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalRequestU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_OnOpen_6() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___OnOpen_6)); }
	inline OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B * get_OnOpen_6() const { return ___OnOpen_6; }
	inline OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B ** get_address_of_OnOpen_6() { return &___OnOpen_6; }
	inline void set_OnOpen_6(OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B * value)
	{
		___OnOpen_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpen_6), value);
	}

	inline static int32_t get_offset_of_OnMessage_7() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___OnMessage_7)); }
	inline OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D * get_OnMessage_7() const { return ___OnMessage_7; }
	inline OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D ** get_address_of_OnMessage_7() { return &___OnMessage_7; }
	inline void set_OnMessage_7(OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D * value)
	{
		___OnMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessage_7), value);
	}

	inline static int32_t get_offset_of_OnError_8() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___OnError_8)); }
	inline OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2 * get_OnError_8() const { return ___OnError_8; }
	inline OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2 ** get_address_of_OnError_8() { return &___OnError_8; }
	inline void set_OnError_8(OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2 * value)
	{
		___OnError_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_8), value);
	}

	inline static int32_t get_offset_of_OnRetry_9() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___OnRetry_9)); }
	inline OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 * get_OnRetry_9() const { return ___OnRetry_9; }
	inline OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 ** get_address_of_OnRetry_9() { return &___OnRetry_9; }
	inline void set_OnRetry_9(OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 * value)
	{
		___OnRetry_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnRetry_9), value);
	}

	inline static int32_t get_offset_of_OnClosed_10() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___OnClosed_10)); }
	inline OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B * get_OnClosed_10() const { return ___OnClosed_10; }
	inline OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B ** get_address_of_OnClosed_10() { return &___OnClosed_10; }
	inline void set_OnClosed_10(OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B * value)
	{
		___OnClosed_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_10), value);
	}

	inline static int32_t get_offset_of_OnStateChanged_11() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___OnStateChanged_11)); }
	inline OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE * get_OnStateChanged_11() const { return ___OnStateChanged_11; }
	inline OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE ** get_address_of_OnStateChanged_11() { return &___OnStateChanged_11; }
	inline void set_OnStateChanged_11(OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE * value)
	{
		___OnStateChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_11), value);
	}

	inline static int32_t get_offset_of_EventTable_12() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___EventTable_12)); }
	inline Dictionary_2_t7E96B3C27B69CD061EC5587E11344675BB70A2E9 * get_EventTable_12() const { return ___EventTable_12; }
	inline Dictionary_2_t7E96B3C27B69CD061EC5587E11344675BB70A2E9 ** get_address_of_EventTable_12() { return &___EventTable_12; }
	inline void set_EventTable_12(Dictionary_2_t7E96B3C27B69CD061EC5587E11344675BB70A2E9 * value)
	{
		___EventTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___EventTable_12), value);
	}

	inline static int32_t get_offset_of_RetryCount_13() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___RetryCount_13)); }
	inline uint8_t get_RetryCount_13() const { return ___RetryCount_13; }
	inline uint8_t* get_address_of_RetryCount_13() { return &___RetryCount_13; }
	inline void set_RetryCount_13(uint8_t value)
	{
		___RetryCount_13 = value;
	}

	inline static int32_t get_offset_of_RetryCalled_14() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___RetryCalled_14)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_RetryCalled_14() const { return ___RetryCalled_14; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_RetryCalled_14() { return &___RetryCalled_14; }
	inline void set_RetryCalled_14(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___RetryCalled_14 = value;
	}

	inline static int32_t get_offset_of_LineBuffer_15() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___LineBuffer_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_LineBuffer_15() const { return ___LineBuffer_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_LineBuffer_15() { return &___LineBuffer_15; }
	inline void set_LineBuffer_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___LineBuffer_15 = value;
		Il2CppCodeGenWriteBarrier((&___LineBuffer_15), value);
	}

	inline static int32_t get_offset_of_LineBufferPos_16() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___LineBufferPos_16)); }
	inline int32_t get_LineBufferPos_16() const { return ___LineBufferPos_16; }
	inline int32_t* get_address_of_LineBufferPos_16() { return &___LineBufferPos_16; }
	inline void set_LineBufferPos_16(int32_t value)
	{
		___LineBufferPos_16 = value;
	}

	inline static int32_t get_offset_of_CurrentMessage_17() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___CurrentMessage_17)); }
	inline Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3 * get_CurrentMessage_17() const { return ___CurrentMessage_17; }
	inline Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3 ** get_address_of_CurrentMessage_17() { return &___CurrentMessage_17; }
	inline void set_CurrentMessage_17(Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3 * value)
	{
		___CurrentMessage_17 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentMessage_17), value);
	}

	inline static int32_t get_offset_of_CompletedMessages_18() { return static_cast<int32_t>(offsetof(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6, ___CompletedMessages_18)); }
	inline List_1_t01F3A1966CC10BAC639FFB6D44AB4A07695ACBC9 * get_CompletedMessages_18() const { return ___CompletedMessages_18; }
	inline List_1_t01F3A1966CC10BAC639FFB6D44AB4A07695ACBC9 ** get_address_of_CompletedMessages_18() { return &___CompletedMessages_18; }
	inline void set_CompletedMessages_18(List_1_t01F3A1966CC10BAC639FFB6D44AB4A07695ACBC9 * value)
	{
		___CompletedMessages_18 = value;
		Il2CppCodeGenWriteBarrier((&___CompletedMessages_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSOURCE_TC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6_H
#ifndef MESSAGE_T43D771510972B893CF0C3A63B3F0C1406157ECB3_H
#define MESSAGE_T43D771510972B893CF0C3A63B3F0C1406157ECB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.Message
struct  Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3  : public RuntimeObject
{
public:
	// System.String BestHTTP.ServerSentEvents.Message::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_0;
	// System.String BestHTTP.ServerSentEvents.Message::<Event>k__BackingField
	String_t* ___U3CEventU3Ek__BackingField_1;
	// System.String BestHTTP.ServerSentEvents.Message::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.ServerSentEvents.Message::<Retry>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CRetryU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3, ___U3CIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CEventU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3, ___U3CEventU3Ek__BackingField_1)); }
	inline String_t* get_U3CEventU3Ek__BackingField_1() const { return ___U3CEventU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEventU3Ek__BackingField_1() { return &___U3CEventU3Ek__BackingField_1; }
	inline void set_U3CEventU3Ek__BackingField_1(String_t* value)
	{
		___U3CEventU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3, ___U3CDataU3Ek__BackingField_2)); }
	inline String_t* get_U3CDataU3Ek__BackingField_2() const { return ___U3CDataU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_2() { return &___U3CDataU3Ek__BackingField_2; }
	inline void set_U3CDataU3Ek__BackingField_2(String_t* value)
	{
		___U3CDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRetryU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3, ___U3CRetryU3Ek__BackingField_3)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CRetryU3Ek__BackingField_3() const { return ___U3CRetryU3Ek__BackingField_3; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CRetryU3Ek__BackingField_3() { return &___U3CRetryU3Ek__BackingField_3; }
	inline void set_U3CRetryU3Ek__BackingField_3(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CRetryU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_T43D771510972B893CF0C3A63B3F0C1406157ECB3_H
#ifndef PERMESSAGECOMPRESSION_TFE288EAE69EEC2B5E84202F9D158676C35B3139F_H
#define PERMESSAGECOMPRESSION_TFE288EAE69EEC2B5E84202F9D158676C35B3139F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.Extensions.PerMessageCompression
struct  PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::<ClientNoContextTakeover>k__BackingField
	bool ___U3CClientNoContextTakeoverU3Ek__BackingField_2;
	// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::<ServerNoContextTakeover>k__BackingField
	bool ___U3CServerNoContextTakeoverU3Ek__BackingField_3;
	// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::<ClientMaxWindowBits>k__BackingField
	int32_t ___U3CClientMaxWindowBitsU3Ek__BackingField_4;
	// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::<ServerMaxWindowBits>k__BackingField
	int32_t ___U3CServerMaxWindowBitsU3Ek__BackingField_5;
	// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.WebSocket.Extensions.PerMessageCompression::<Level>k__BackingField
	int32_t ___U3CLevelU3Ek__BackingField_6;
	// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::<MinimumDataLegthToCompress>k__BackingField
	int32_t ___U3CMinimumDataLegthToCompressU3Ek__BackingField_7;
	// BestHTTP.Extensions.BufferPoolMemoryStream BestHTTP.WebSocket.Extensions.PerMessageCompression::compressorOutputStream
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * ___compressorOutputStream_8;
	// BestHTTP.Decompression.Zlib.DeflateStream BestHTTP.WebSocket.Extensions.PerMessageCompression::compressorDeflateStream
	DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA * ___compressorDeflateStream_9;
	// BestHTTP.Extensions.BufferPoolMemoryStream BestHTTP.WebSocket.Extensions.PerMessageCompression::decompressorInputStream
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * ___decompressorInputStream_10;
	// BestHTTP.Extensions.BufferPoolMemoryStream BestHTTP.WebSocket.Extensions.PerMessageCompression::decompressorOutputStream
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * ___decompressorOutputStream_11;
	// BestHTTP.Decompression.Zlib.DeflateStream BestHTTP.WebSocket.Extensions.PerMessageCompression::decompressorDeflateStream
	DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA * ___decompressorDeflateStream_12;

public:
	inline static int32_t get_offset_of_U3CClientNoContextTakeoverU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___U3CClientNoContextTakeoverU3Ek__BackingField_2)); }
	inline bool get_U3CClientNoContextTakeoverU3Ek__BackingField_2() const { return ___U3CClientNoContextTakeoverU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CClientNoContextTakeoverU3Ek__BackingField_2() { return &___U3CClientNoContextTakeoverU3Ek__BackingField_2; }
	inline void set_U3CClientNoContextTakeoverU3Ek__BackingField_2(bool value)
	{
		___U3CClientNoContextTakeoverU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CServerNoContextTakeoverU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___U3CServerNoContextTakeoverU3Ek__BackingField_3)); }
	inline bool get_U3CServerNoContextTakeoverU3Ek__BackingField_3() const { return ___U3CServerNoContextTakeoverU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CServerNoContextTakeoverU3Ek__BackingField_3() { return &___U3CServerNoContextTakeoverU3Ek__BackingField_3; }
	inline void set_U3CServerNoContextTakeoverU3Ek__BackingField_3(bool value)
	{
		___U3CServerNoContextTakeoverU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CClientMaxWindowBitsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___U3CClientMaxWindowBitsU3Ek__BackingField_4)); }
	inline int32_t get_U3CClientMaxWindowBitsU3Ek__BackingField_4() const { return ___U3CClientMaxWindowBitsU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CClientMaxWindowBitsU3Ek__BackingField_4() { return &___U3CClientMaxWindowBitsU3Ek__BackingField_4; }
	inline void set_U3CClientMaxWindowBitsU3Ek__BackingField_4(int32_t value)
	{
		___U3CClientMaxWindowBitsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CServerMaxWindowBitsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___U3CServerMaxWindowBitsU3Ek__BackingField_5)); }
	inline int32_t get_U3CServerMaxWindowBitsU3Ek__BackingField_5() const { return ___U3CServerMaxWindowBitsU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CServerMaxWindowBitsU3Ek__BackingField_5() { return &___U3CServerMaxWindowBitsU3Ek__BackingField_5; }
	inline void set_U3CServerMaxWindowBitsU3Ek__BackingField_5(int32_t value)
	{
		___U3CServerMaxWindowBitsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CLevelU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___U3CLevelU3Ek__BackingField_6)); }
	inline int32_t get_U3CLevelU3Ek__BackingField_6() const { return ___U3CLevelU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CLevelU3Ek__BackingField_6() { return &___U3CLevelU3Ek__BackingField_6; }
	inline void set_U3CLevelU3Ek__BackingField_6(int32_t value)
	{
		___U3CLevelU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___U3CMinimumDataLegthToCompressU3Ek__BackingField_7)); }
	inline int32_t get_U3CMinimumDataLegthToCompressU3Ek__BackingField_7() const { return ___U3CMinimumDataLegthToCompressU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_7() { return &___U3CMinimumDataLegthToCompressU3Ek__BackingField_7; }
	inline void set_U3CMinimumDataLegthToCompressU3Ek__BackingField_7(int32_t value)
	{
		___U3CMinimumDataLegthToCompressU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_compressorOutputStream_8() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___compressorOutputStream_8)); }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * get_compressorOutputStream_8() const { return ___compressorOutputStream_8; }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC ** get_address_of_compressorOutputStream_8() { return &___compressorOutputStream_8; }
	inline void set_compressorOutputStream_8(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * value)
	{
		___compressorOutputStream_8 = value;
		Il2CppCodeGenWriteBarrier((&___compressorOutputStream_8), value);
	}

	inline static int32_t get_offset_of_compressorDeflateStream_9() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___compressorDeflateStream_9)); }
	inline DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA * get_compressorDeflateStream_9() const { return ___compressorDeflateStream_9; }
	inline DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA ** get_address_of_compressorDeflateStream_9() { return &___compressorDeflateStream_9; }
	inline void set_compressorDeflateStream_9(DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA * value)
	{
		___compressorDeflateStream_9 = value;
		Il2CppCodeGenWriteBarrier((&___compressorDeflateStream_9), value);
	}

	inline static int32_t get_offset_of_decompressorInputStream_10() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___decompressorInputStream_10)); }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * get_decompressorInputStream_10() const { return ___decompressorInputStream_10; }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC ** get_address_of_decompressorInputStream_10() { return &___decompressorInputStream_10; }
	inline void set_decompressorInputStream_10(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * value)
	{
		___decompressorInputStream_10 = value;
		Il2CppCodeGenWriteBarrier((&___decompressorInputStream_10), value);
	}

	inline static int32_t get_offset_of_decompressorOutputStream_11() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___decompressorOutputStream_11)); }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * get_decompressorOutputStream_11() const { return ___decompressorOutputStream_11; }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC ** get_address_of_decompressorOutputStream_11() { return &___decompressorOutputStream_11; }
	inline void set_decompressorOutputStream_11(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * value)
	{
		___decompressorOutputStream_11 = value;
		Il2CppCodeGenWriteBarrier((&___decompressorOutputStream_11), value);
	}

	inline static int32_t get_offset_of_decompressorDeflateStream_12() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F, ___decompressorDeflateStream_12)); }
	inline DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA * get_decompressorDeflateStream_12() const { return ___decompressorDeflateStream_12; }
	inline DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA ** get_address_of_decompressorDeflateStream_12() { return &___decompressorDeflateStream_12; }
	inline void set_decompressorDeflateStream_12(DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA * value)
	{
		___decompressorDeflateStream_12 = value;
		Il2CppCodeGenWriteBarrier((&___decompressorDeflateStream_12), value);
	}
};

struct PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F_StaticFields
{
public:
	// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::Trailer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Trailer_1;

public:
	inline static int32_t get_offset_of_Trailer_1() { return static_cast<int32_t>(offsetof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F_StaticFields, ___Trailer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Trailer_1() const { return ___Trailer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Trailer_1() { return &___Trailer_1; }
	inline void set_Trailer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Trailer_1 = value;
		Il2CppCodeGenWriteBarrier((&___Trailer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERMESSAGECOMPRESSION_TFE288EAE69EEC2B5E84202F9D158676C35B3139F_H
#ifndef WEBSOCKETFRAME_T493C361BF280BEB44AC6F1973F3A9684A3DDB2F0_H
#define WEBSOCKETFRAME_T493C361BF280BEB44AC6F1973F3A9684A3DDB2F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.Frames.WebSocketFrame
struct  WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0  : public RuntimeObject
{
public:
	// BestHTTP.WebSocket.Frames.WebSocketFrameTypes BestHTTP.WebSocket.Frames.WebSocketFrame::<Type>k__BackingField
	uint8_t ___U3CTypeU3Ek__BackingField_0;
	// System.Boolean BestHTTP.WebSocket.Frames.WebSocketFrame::<IsFinal>k__BackingField
	bool ___U3CIsFinalU3Ek__BackingField_1;
	// System.Byte BestHTTP.WebSocket.Frames.WebSocketFrame::<Header>k__BackingField
	uint8_t ___U3CHeaderU3Ek__BackingField_2;
	// System.Byte[] BestHTTP.WebSocket.Frames.WebSocketFrame::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_3;
	// System.Int32 BestHTTP.WebSocket.Frames.WebSocketFrame::<DataLength>k__BackingField
	int32_t ___U3CDataLengthU3Ek__BackingField_4;
	// System.Boolean BestHTTP.WebSocket.Frames.WebSocketFrame::<UseExtensions>k__BackingField
	bool ___U3CUseExtensionsU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0, ___U3CTypeU3Ek__BackingField_0)); }
	inline uint8_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline uint8_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(uint8_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsFinalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0, ___U3CIsFinalU3Ek__BackingField_1)); }
	inline bool get_U3CIsFinalU3Ek__BackingField_1() const { return ___U3CIsFinalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsFinalU3Ek__BackingField_1() { return &___U3CIsFinalU3Ek__BackingField_1; }
	inline void set_U3CIsFinalU3Ek__BackingField_1(bool value)
	{
		___U3CIsFinalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHeaderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0, ___U3CHeaderU3Ek__BackingField_2)); }
	inline uint8_t get_U3CHeaderU3Ek__BackingField_2() const { return ___U3CHeaderU3Ek__BackingField_2; }
	inline uint8_t* get_address_of_U3CHeaderU3Ek__BackingField_2() { return &___U3CHeaderU3Ek__BackingField_2; }
	inline void set_U3CHeaderU3Ek__BackingField_2(uint8_t value)
	{
		___U3CHeaderU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0, ___U3CDataU3Ek__BackingField_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_3() const { return ___U3CDataU3Ek__BackingField_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_3() { return &___U3CDataU3Ek__BackingField_3; }
	inline void set_U3CDataU3Ek__BackingField_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDataLengthU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0, ___U3CDataLengthU3Ek__BackingField_4)); }
	inline int32_t get_U3CDataLengthU3Ek__BackingField_4() const { return ___U3CDataLengthU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDataLengthU3Ek__BackingField_4() { return &___U3CDataLengthU3Ek__BackingField_4; }
	inline void set_U3CDataLengthU3Ek__BackingField_4(int32_t value)
	{
		___U3CDataLengthU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CUseExtensionsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0, ___U3CUseExtensionsU3Ek__BackingField_5)); }
	inline bool get_U3CUseExtensionsU3Ek__BackingField_5() const { return ___U3CUseExtensionsU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CUseExtensionsU3Ek__BackingField_5() { return &___U3CUseExtensionsU3Ek__BackingField_5; }
	inline void set_U3CUseExtensionsU3Ek__BackingField_5(bool value)
	{
		___U3CUseExtensionsU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETFRAME_T493C361BF280BEB44AC6F1973F3A9684A3DDB2F0_H
#ifndef WEBSOCKETFRAMEREADER_TDD98E162F188C44922FB698F15B7B3E9B6ADECC2_H
#define WEBSOCKETFRAMEREADER_TDD98E162F188C44922FB698F15B7B3E9B6ADECC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.Frames.WebSocketFrameReader
struct  WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2 
{
public:
	// System.Byte BestHTTP.WebSocket.Frames.WebSocketFrameReader::<Header>k__BackingField
	uint8_t ___U3CHeaderU3Ek__BackingField_0;
	// System.Boolean BestHTTP.WebSocket.Frames.WebSocketFrameReader::<IsFinal>k__BackingField
	bool ___U3CIsFinalU3Ek__BackingField_1;
	// BestHTTP.WebSocket.Frames.WebSocketFrameTypes BestHTTP.WebSocket.Frames.WebSocketFrameReader::<Type>k__BackingField
	uint8_t ___U3CTypeU3Ek__BackingField_2;
	// System.Boolean BestHTTP.WebSocket.Frames.WebSocketFrameReader::<HasMask>k__BackingField
	bool ___U3CHasMaskU3Ek__BackingField_3;
	// System.UInt64 BestHTTP.WebSocket.Frames.WebSocketFrameReader::<Length>k__BackingField
	uint64_t ___U3CLengthU3Ek__BackingField_4;
	// System.Byte[] BestHTTP.WebSocket.Frames.WebSocketFrameReader::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_5;
	// System.String BestHTTP.WebSocket.Frames.WebSocketFrameReader::<DataAsText>k__BackingField
	String_t* ___U3CDataAsTextU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CHeaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CHeaderU3Ek__BackingField_0)); }
	inline uint8_t get_U3CHeaderU3Ek__BackingField_0() const { return ___U3CHeaderU3Ek__BackingField_0; }
	inline uint8_t* get_address_of_U3CHeaderU3Ek__BackingField_0() { return &___U3CHeaderU3Ek__BackingField_0; }
	inline void set_U3CHeaderU3Ek__BackingField_0(uint8_t value)
	{
		___U3CHeaderU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsFinalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CIsFinalU3Ek__BackingField_1)); }
	inline bool get_U3CIsFinalU3Ek__BackingField_1() const { return ___U3CIsFinalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsFinalU3Ek__BackingField_1() { return &___U3CIsFinalU3Ek__BackingField_1; }
	inline void set_U3CIsFinalU3Ek__BackingField_1(bool value)
	{
		___U3CIsFinalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CTypeU3Ek__BackingField_2)); }
	inline uint8_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline uint8_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(uint8_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHasMaskU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CHasMaskU3Ek__BackingField_3)); }
	inline bool get_U3CHasMaskU3Ek__BackingField_3() const { return ___U3CHasMaskU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CHasMaskU3Ek__BackingField_3() { return &___U3CHasMaskU3Ek__BackingField_3; }
	inline void set_U3CHasMaskU3Ek__BackingField_3(bool value)
	{
		___U3CHasMaskU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CLengthU3Ek__BackingField_4)); }
	inline uint64_t get_U3CLengthU3Ek__BackingField_4() const { return ___U3CLengthU3Ek__BackingField_4; }
	inline uint64_t* get_address_of_U3CLengthU3Ek__BackingField_4() { return &___U3CLengthU3Ek__BackingField_4; }
	inline void set_U3CLengthU3Ek__BackingField_4(uint64_t value)
	{
		___U3CLengthU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CDataU3Ek__BackingField_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_5() const { return ___U3CDataU3Ek__BackingField_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_5() { return &___U3CDataU3Ek__BackingField_5; }
	inline void set_U3CDataU3Ek__BackingField_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CDataAsTextU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2, ___U3CDataAsTextU3Ek__BackingField_6)); }
	inline String_t* get_U3CDataAsTextU3Ek__BackingField_6() const { return ___U3CDataAsTextU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CDataAsTextU3Ek__BackingField_6() { return &___U3CDataAsTextU3Ek__BackingField_6; }
	inline void set_U3CDataAsTextU3Ek__BackingField_6(String_t* value)
	{
		___U3CDataAsTextU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataAsTextU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.WebSocket.Frames.WebSocketFrameReader
struct WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2_marshaled_pinvoke
{
	uint8_t ___U3CHeaderU3Ek__BackingField_0;
	int32_t ___U3CIsFinalU3Ek__BackingField_1;
	uint8_t ___U3CTypeU3Ek__BackingField_2;
	int32_t ___U3CHasMaskU3Ek__BackingField_3;
	uint64_t ___U3CLengthU3Ek__BackingField_4;
	uint8_t* ___U3CDataU3Ek__BackingField_5;
	char* ___U3CDataAsTextU3Ek__BackingField_6;
};
// Native definition for COM marshalling of BestHTTP.WebSocket.Frames.WebSocketFrameReader
struct WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2_marshaled_com
{
	uint8_t ___U3CHeaderU3Ek__BackingField_0;
	int32_t ___U3CIsFinalU3Ek__BackingField_1;
	uint8_t ___U3CTypeU3Ek__BackingField_2;
	int32_t ___U3CHasMaskU3Ek__BackingField_3;
	uint64_t ___U3CLengthU3Ek__BackingField_4;
	uint8_t* ___U3CDataU3Ek__BackingField_5;
	Il2CppChar* ___U3CDataAsTextU3Ek__BackingField_6;
};
#endif // WEBSOCKETFRAMEREADER_TDD98E162F188C44922FB698F15B7B3E9B6ADECC2_H
#ifndef WEBSOCKET_T9ED526C25EE94964B52B25B5A107A561B35FA717_H
#define WEBSOCKET_T9ED526C25EE94964B52B25B5A107A561B35FA717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocket
struct  WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717  : public RuntimeObject
{
public:
	// BestHTTP.WebSocket.WebSocketStates BestHTTP.WebSocket.WebSocket::<State>k__BackingField
	uint8_t ___U3CStateU3Ek__BackingField_0;
	// System.Boolean BestHTTP.WebSocket.WebSocket::<StartPingThread>k__BackingField
	bool ___U3CStartPingThreadU3Ek__BackingField_1;
	// System.Int32 BestHTTP.WebSocket.WebSocket::<PingFrequency>k__BackingField
	int32_t ___U3CPingFrequencyU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.WebSocket.WebSocket::<CloseAfterNoMesssage>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CCloseAfterNoMesssageU3Ek__BackingField_3;
	// BestHTTP.HTTPRequest BestHTTP.WebSocket.WebSocket::<InternalRequest>k__BackingField
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___U3CInternalRequestU3Ek__BackingField_4;
	// BestHTTP.WebSocket.Extensions.IExtension[] BestHTTP.WebSocket.WebSocket::<Extensions>k__BackingField
	IExtensionU5BU5D_tC6A1064DBEB723D76B4E806BB9FE94F6900E17A7* ___U3CExtensionsU3Ek__BackingField_5;
	// BestHTTP.WebSocket.OnWebSocketOpenDelegate BestHTTP.WebSocket.WebSocket::OnOpen
	OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002 * ___OnOpen_6;
	// BestHTTP.WebSocket.OnWebSocketMessageDelegate BestHTTP.WebSocket.WebSocket::OnMessage
	OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0 * ___OnMessage_7;
	// BestHTTP.WebSocket.OnWebSocketBinaryDelegate BestHTTP.WebSocket.WebSocket::OnBinary
	OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6 * ___OnBinary_8;
	// BestHTTP.WebSocket.OnWebSocketClosedDelegate BestHTTP.WebSocket.WebSocket::OnClosed
	OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98 * ___OnClosed_9;
	// BestHTTP.WebSocket.OnWebSocketErrorDelegate BestHTTP.WebSocket.WebSocket::OnError
	OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA * ___OnError_10;
	// BestHTTP.WebSocket.OnWebSocketIncompleteFrameDelegate BestHTTP.WebSocket.WebSocket::OnIncompleteFrame
	OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695 * ___OnIncompleteFrame_11;
	// System.Boolean BestHTTP.WebSocket.WebSocket::requestSent
	bool ___requestSent_12;
	// BestHTTP.WebSocket.WebSocketResponse BestHTTP.WebSocket.WebSocket::webSocket
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3 * ___webSocket_13;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___U3CStateU3Ek__BackingField_0)); }
	inline uint8_t get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline uint8_t* get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(uint8_t value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStartPingThreadU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___U3CStartPingThreadU3Ek__BackingField_1)); }
	inline bool get_U3CStartPingThreadU3Ek__BackingField_1() const { return ___U3CStartPingThreadU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CStartPingThreadU3Ek__BackingField_1() { return &___U3CStartPingThreadU3Ek__BackingField_1; }
	inline void set_U3CStartPingThreadU3Ek__BackingField_1(bool value)
	{
		___U3CStartPingThreadU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPingFrequencyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___U3CPingFrequencyU3Ek__BackingField_2)); }
	inline int32_t get_U3CPingFrequencyU3Ek__BackingField_2() const { return ___U3CPingFrequencyU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CPingFrequencyU3Ek__BackingField_2() { return &___U3CPingFrequencyU3Ek__BackingField_2; }
	inline void set_U3CPingFrequencyU3Ek__BackingField_2(int32_t value)
	{
		___U3CPingFrequencyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCloseAfterNoMesssageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___U3CCloseAfterNoMesssageU3Ek__BackingField_3)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CCloseAfterNoMesssageU3Ek__BackingField_3() const { return ___U3CCloseAfterNoMesssageU3Ek__BackingField_3; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CCloseAfterNoMesssageU3Ek__BackingField_3() { return &___U3CCloseAfterNoMesssageU3Ek__BackingField_3; }
	inline void set_U3CCloseAfterNoMesssageU3Ek__BackingField_3(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CCloseAfterNoMesssageU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CInternalRequestU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___U3CInternalRequestU3Ek__BackingField_4)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_U3CInternalRequestU3Ek__BackingField_4() const { return ___U3CInternalRequestU3Ek__BackingField_4; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_U3CInternalRequestU3Ek__BackingField_4() { return &___U3CInternalRequestU3Ek__BackingField_4; }
	inline void set_U3CInternalRequestU3Ek__BackingField_4(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___U3CInternalRequestU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalRequestU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CExtensionsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___U3CExtensionsU3Ek__BackingField_5)); }
	inline IExtensionU5BU5D_tC6A1064DBEB723D76B4E806BB9FE94F6900E17A7* get_U3CExtensionsU3Ek__BackingField_5() const { return ___U3CExtensionsU3Ek__BackingField_5; }
	inline IExtensionU5BU5D_tC6A1064DBEB723D76B4E806BB9FE94F6900E17A7** get_address_of_U3CExtensionsU3Ek__BackingField_5() { return &___U3CExtensionsU3Ek__BackingField_5; }
	inline void set_U3CExtensionsU3Ek__BackingField_5(IExtensionU5BU5D_tC6A1064DBEB723D76B4E806BB9FE94F6900E17A7* value)
	{
		___U3CExtensionsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_OnOpen_6() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___OnOpen_6)); }
	inline OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002 * get_OnOpen_6() const { return ___OnOpen_6; }
	inline OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002 ** get_address_of_OnOpen_6() { return &___OnOpen_6; }
	inline void set_OnOpen_6(OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002 * value)
	{
		___OnOpen_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpen_6), value);
	}

	inline static int32_t get_offset_of_OnMessage_7() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___OnMessage_7)); }
	inline OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0 * get_OnMessage_7() const { return ___OnMessage_7; }
	inline OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0 ** get_address_of_OnMessage_7() { return &___OnMessage_7; }
	inline void set_OnMessage_7(OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0 * value)
	{
		___OnMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessage_7), value);
	}

	inline static int32_t get_offset_of_OnBinary_8() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___OnBinary_8)); }
	inline OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6 * get_OnBinary_8() const { return ___OnBinary_8; }
	inline OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6 ** get_address_of_OnBinary_8() { return &___OnBinary_8; }
	inline void set_OnBinary_8(OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6 * value)
	{
		___OnBinary_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnBinary_8), value);
	}

	inline static int32_t get_offset_of_OnClosed_9() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___OnClosed_9)); }
	inline OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98 * get_OnClosed_9() const { return ___OnClosed_9; }
	inline OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98 ** get_address_of_OnClosed_9() { return &___OnClosed_9; }
	inline void set_OnClosed_9(OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98 * value)
	{
		___OnClosed_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_9), value);
	}

	inline static int32_t get_offset_of_OnError_10() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___OnError_10)); }
	inline OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA * get_OnError_10() const { return ___OnError_10; }
	inline OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA ** get_address_of_OnError_10() { return &___OnError_10; }
	inline void set_OnError_10(OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA * value)
	{
		___OnError_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_10), value);
	}

	inline static int32_t get_offset_of_OnIncompleteFrame_11() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___OnIncompleteFrame_11)); }
	inline OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695 * get_OnIncompleteFrame_11() const { return ___OnIncompleteFrame_11; }
	inline OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695 ** get_address_of_OnIncompleteFrame_11() { return &___OnIncompleteFrame_11; }
	inline void set_OnIncompleteFrame_11(OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695 * value)
	{
		___OnIncompleteFrame_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnIncompleteFrame_11), value);
	}

	inline static int32_t get_offset_of_requestSent_12() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___requestSent_12)); }
	inline bool get_requestSent_12() const { return ___requestSent_12; }
	inline bool* get_address_of_requestSent_12() { return &___requestSent_12; }
	inline void set_requestSent_12(bool value)
	{
		___requestSent_12 = value;
	}

	inline static int32_t get_offset_of_webSocket_13() { return static_cast<int32_t>(offsetof(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717, ___webSocket_13)); }
	inline WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3 * get_webSocket_13() const { return ___webSocket_13; }
	inline WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3 ** get_address_of_webSocket_13() { return &___webSocket_13; }
	inline void set_webSocket_13(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3 * value)
	{
		___webSocket_13 = value;
		Il2CppCodeGenWriteBarrier((&___webSocket_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T9ED526C25EE94964B52B25B5A107A561B35FA717_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ONERRORDELEGATE_T9E82A1F01A812C06D1C802221B388190DF7460A2_H
#define ONERRORDELEGATE_T9E82A1F01A812C06D1C802221B388190DF7460A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.OnErrorDelegate
struct  OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORDELEGATE_T9E82A1F01A812C06D1C802221B388190DF7460A2_H
#ifndef ONEVENTDELEGATE_T3CFFA35D4F0B715CD8AB5E6514D4B9E91DE0F813_H
#define ONEVENTDELEGATE_T3CFFA35D4F0B715CD8AB5E6514D4B9E91DE0F813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.OnEventDelegate
struct  OnEventDelegate_t3CFFA35D4F0B715CD8AB5E6514D4B9E91DE0F813  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEVENTDELEGATE_T3CFFA35D4F0B715CD8AB5E6514D4B9E91DE0F813_H
#ifndef ONGENERALEVENTDELEGATE_TF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B_H
#define ONGENERALEVENTDELEGATE_TF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.OnGeneralEventDelegate
struct  OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGENERALEVENTDELEGATE_TF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B_H
#ifndef ONMESSAGEDELEGATE_T3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D_H
#define ONMESSAGEDELEGATE_T3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.OnMessageDelegate
struct  OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMESSAGEDELEGATE_T3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D_H
#ifndef ONRETRYDELEGATE_TE6F4EF61343A96A4CA48BDCB5943C8F989F91F46_H
#define ONRETRYDELEGATE_TE6F4EF61343A96A4CA48BDCB5943C8F989F91F46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.OnRetryDelegate
struct  OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRETRYDELEGATE_TE6F4EF61343A96A4CA48BDCB5943C8F989F91F46_H
#ifndef ONSTATECHANGEDDELEGATE_T5662E384D5C358DCA5775397519E8975F3DA1ADE_H
#define ONSTATECHANGEDDELEGATE_T5662E384D5C358DCA5775397519E8975F3DA1ADE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ServerSentEvents.OnStateChangedDelegate
struct  OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATECHANGEDDELEGATE_T5662E384D5C358DCA5775397519E8975F3DA1ADE_H
#ifndef ONWEBSOCKETBINARYDELEGATE_TED50D70E50C37A2A49AF22818A50CFE952F3DAD6_H
#define ONWEBSOCKETBINARYDELEGATE_TED50D70E50C37A2A49AF22818A50CFE952F3DAD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.OnWebSocketBinaryDelegate
struct  OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBSOCKETBINARYDELEGATE_TED50D70E50C37A2A49AF22818A50CFE952F3DAD6_H
#ifndef ONWEBSOCKETCLOSEDDELEGATE_T6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98_H
#define ONWEBSOCKETCLOSEDDELEGATE_T6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.OnWebSocketClosedDelegate
struct  OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBSOCKETCLOSEDDELEGATE_T6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98_H
#ifndef ONWEBSOCKETERRORDELEGATE_T4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA_H
#define ONWEBSOCKETERRORDELEGATE_T4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.OnWebSocketErrorDelegate
struct  OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBSOCKETERRORDELEGATE_T4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA_H
#ifndef ONWEBSOCKETINCOMPLETEFRAMEDELEGATE_TD3147F18AA5B1F8E144FE93DCB52BB70035A1695_H
#define ONWEBSOCKETINCOMPLETEFRAMEDELEGATE_TD3147F18AA5B1F8E144FE93DCB52BB70035A1695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.OnWebSocketIncompleteFrameDelegate
struct  OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBSOCKETINCOMPLETEFRAMEDELEGATE_TD3147F18AA5B1F8E144FE93DCB52BB70035A1695_H
#ifndef ONWEBSOCKETMESSAGEDELEGATE_T8910D49B1773AF8E8534824F816A1B8ACB60ECB0_H
#define ONWEBSOCKETMESSAGEDELEGATE_T8910D49B1773AF8E8534824F816A1B8ACB60ECB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.OnWebSocketMessageDelegate
struct  OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBSOCKETMESSAGEDELEGATE_T8910D49B1773AF8E8534824F816A1B8ACB60ECB0_H
#ifndef ONWEBSOCKETOPENDELEGATE_T5A0F7F1B48A143D5B44A178C5111CD3B04FFE002_H
#define ONWEBSOCKETOPENDELEGATE_T5A0F7F1B48A143D5B44A178C5111CD3B04FFE002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.OnWebSocketOpenDelegate
struct  OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWEBSOCKETOPENDELEGATE_T5A0F7F1B48A143D5B44A178C5111CD3B04FFE002_H
#ifndef WEBSOCKETRESPONSE_T377DAE617A82BCEA661252342C9E5B376C156CB3_H
#define WEBSOCKETRESPONSE_T377DAE617A82BCEA661252342C9E5B376C156CB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocketResponse
struct  WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3  : public HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB
{
public:
	// BestHTTP.WebSocket.WebSocket BestHTTP.WebSocket.WebSocketResponse::<WebSocket>k__BackingField
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * ___U3CWebSocketU3Ek__BackingField_26;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.String> BestHTTP.WebSocket.WebSocketResponse::OnText
	Action_2_t323FA38EC9A2B95AE8D7517DF75ED6EA00E02099 * ___OnText_27;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]> BestHTTP.WebSocket.WebSocketResponse::OnBinary
	Action_2_tF33231D562AD317E2BE60E911D813B80837EA270 * ___OnBinary_28;
	// System.Action`2<BestHTTP.WebSocket.WebSocketResponse,BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::OnIncompleteFrame
	Action_2_t70FEF88466F857EA7127F5A2A55995AB9ADB166F * ___OnIncompleteFrame_29;
	// System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String> BestHTTP.WebSocket.WebSocketResponse::OnClosed
	Action_3_t5D70FD461D1B40C1CFFA7F46226A3F75F6F6DC3E * ___OnClosed_30;
	// BestHTTP.Core.HostConnectionKey BestHTTP.WebSocket.WebSocketResponse::<ConnectionKey>k__BackingField
	HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83  ___U3CConnectionKeyU3Ek__BackingField_31;
	// System.TimeSpan BestHTTP.WebSocket.WebSocketResponse::<PingFrequnecy>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CPingFrequnecyU3Ek__BackingField_32;
	// System.UInt16 BestHTTP.WebSocket.WebSocketResponse::<MaxFragmentSize>k__BackingField
	uint16_t ___U3CMaxFragmentSizeU3Ek__BackingField_33;
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::_bufferedAmount
	int32_t ____bufferedAmount_34;
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::<Latency>k__BackingField
	int32_t ___U3CLatencyU3Ek__BackingField_35;
	// System.DateTime BestHTTP.WebSocket.WebSocketResponse::lastMessage
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastMessage_36;
	// System.Collections.Generic.List`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::IncompleteFrames
	List_1_tACEAA9B1C6B540445C9D9A97B84B613D3E81C0ED * ___IncompleteFrames_37;
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.WebSocket.Frames.WebSocketFrameReader> BestHTTP.WebSocket.WebSocketResponse::CompletedFrames
	ConcurrentQueue_1_tC3C7EA7D8366EFAB17771E9A04D39272AF812FCA * ___CompletedFrames_38;
	// BestHTTP.WebSocket.Frames.WebSocketFrameReader BestHTTP.WebSocket.WebSocketResponse::CloseFrame
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2  ___CloseFrame_39;
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.WebSocket.Frames.WebSocketFrame> BestHTTP.WebSocket.WebSocketResponse::unsentFrames
	ConcurrentQueue_1_t8AA22CA8D8AADD1F0E3ACBDCDD8DB501B8A3DCAB * ___unsentFrames_40;
	// System.Threading.AutoResetEvent modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.WebSocket.WebSocketResponse::newFrameSignal
	AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * ___newFrameSignal_41;
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::sendThreadCreated
	int32_t ___sendThreadCreated_42;
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::closedThreads
	int32_t ___closedThreads_43;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.WebSocket.WebSocketResponse::closeSent
	bool ___closeSent_44;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.WebSocket.WebSocketResponse::closed
	bool ___closed_45;
	// System.DateTime BestHTTP.WebSocket.WebSocketResponse::lastPing
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastPing_46;
	// BestHTTP.Extensions.CircularBuffer`1<System.Int32> BestHTTP.WebSocket.WebSocketResponse::rtts
	CircularBuffer_1_tAC7579C8AD77EAE5D35A20D485487AAE74B97F42 * ___rtts_47;
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::closedThreadCount
	int32_t ___closedThreadCount_48;

public:
	inline static int32_t get_offset_of_U3CWebSocketU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___U3CWebSocketU3Ek__BackingField_26)); }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * get_U3CWebSocketU3Ek__BackingField_26() const { return ___U3CWebSocketU3Ek__BackingField_26; }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 ** get_address_of_U3CWebSocketU3Ek__BackingField_26() { return &___U3CWebSocketU3Ek__BackingField_26; }
	inline void set_U3CWebSocketU3Ek__BackingField_26(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * value)
	{
		___U3CWebSocketU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebSocketU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_OnText_27() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___OnText_27)); }
	inline Action_2_t323FA38EC9A2B95AE8D7517DF75ED6EA00E02099 * get_OnText_27() const { return ___OnText_27; }
	inline Action_2_t323FA38EC9A2B95AE8D7517DF75ED6EA00E02099 ** get_address_of_OnText_27() { return &___OnText_27; }
	inline void set_OnText_27(Action_2_t323FA38EC9A2B95AE8D7517DF75ED6EA00E02099 * value)
	{
		___OnText_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnText_27), value);
	}

	inline static int32_t get_offset_of_OnBinary_28() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___OnBinary_28)); }
	inline Action_2_tF33231D562AD317E2BE60E911D813B80837EA270 * get_OnBinary_28() const { return ___OnBinary_28; }
	inline Action_2_tF33231D562AD317E2BE60E911D813B80837EA270 ** get_address_of_OnBinary_28() { return &___OnBinary_28; }
	inline void set_OnBinary_28(Action_2_tF33231D562AD317E2BE60E911D813B80837EA270 * value)
	{
		___OnBinary_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnBinary_28), value);
	}

	inline static int32_t get_offset_of_OnIncompleteFrame_29() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___OnIncompleteFrame_29)); }
	inline Action_2_t70FEF88466F857EA7127F5A2A55995AB9ADB166F * get_OnIncompleteFrame_29() const { return ___OnIncompleteFrame_29; }
	inline Action_2_t70FEF88466F857EA7127F5A2A55995AB9ADB166F ** get_address_of_OnIncompleteFrame_29() { return &___OnIncompleteFrame_29; }
	inline void set_OnIncompleteFrame_29(Action_2_t70FEF88466F857EA7127F5A2A55995AB9ADB166F * value)
	{
		___OnIncompleteFrame_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnIncompleteFrame_29), value);
	}

	inline static int32_t get_offset_of_OnClosed_30() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___OnClosed_30)); }
	inline Action_3_t5D70FD461D1B40C1CFFA7F46226A3F75F6F6DC3E * get_OnClosed_30() const { return ___OnClosed_30; }
	inline Action_3_t5D70FD461D1B40C1CFFA7F46226A3F75F6F6DC3E ** get_address_of_OnClosed_30() { return &___OnClosed_30; }
	inline void set_OnClosed_30(Action_3_t5D70FD461D1B40C1CFFA7F46226A3F75F6F6DC3E * value)
	{
		___OnClosed_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_30), value);
	}

	inline static int32_t get_offset_of_U3CConnectionKeyU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___U3CConnectionKeyU3Ek__BackingField_31)); }
	inline HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83  get_U3CConnectionKeyU3Ek__BackingField_31() const { return ___U3CConnectionKeyU3Ek__BackingField_31; }
	inline HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83 * get_address_of_U3CConnectionKeyU3Ek__BackingField_31() { return &___U3CConnectionKeyU3Ek__BackingField_31; }
	inline void set_U3CConnectionKeyU3Ek__BackingField_31(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83  value)
	{
		___U3CConnectionKeyU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CPingFrequnecyU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___U3CPingFrequnecyU3Ek__BackingField_32)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CPingFrequnecyU3Ek__BackingField_32() const { return ___U3CPingFrequnecyU3Ek__BackingField_32; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CPingFrequnecyU3Ek__BackingField_32() { return &___U3CPingFrequnecyU3Ek__BackingField_32; }
	inline void set_U3CPingFrequnecyU3Ek__BackingField_32(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CPingFrequnecyU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CMaxFragmentSizeU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___U3CMaxFragmentSizeU3Ek__BackingField_33)); }
	inline uint16_t get_U3CMaxFragmentSizeU3Ek__BackingField_33() const { return ___U3CMaxFragmentSizeU3Ek__BackingField_33; }
	inline uint16_t* get_address_of_U3CMaxFragmentSizeU3Ek__BackingField_33() { return &___U3CMaxFragmentSizeU3Ek__BackingField_33; }
	inline void set_U3CMaxFragmentSizeU3Ek__BackingField_33(uint16_t value)
	{
		___U3CMaxFragmentSizeU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of__bufferedAmount_34() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ____bufferedAmount_34)); }
	inline int32_t get__bufferedAmount_34() const { return ____bufferedAmount_34; }
	inline int32_t* get_address_of__bufferedAmount_34() { return &____bufferedAmount_34; }
	inline void set__bufferedAmount_34(int32_t value)
	{
		____bufferedAmount_34 = value;
	}

	inline static int32_t get_offset_of_U3CLatencyU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___U3CLatencyU3Ek__BackingField_35)); }
	inline int32_t get_U3CLatencyU3Ek__BackingField_35() const { return ___U3CLatencyU3Ek__BackingField_35; }
	inline int32_t* get_address_of_U3CLatencyU3Ek__BackingField_35() { return &___U3CLatencyU3Ek__BackingField_35; }
	inline void set_U3CLatencyU3Ek__BackingField_35(int32_t value)
	{
		___U3CLatencyU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_lastMessage_36() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___lastMessage_36)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastMessage_36() const { return ___lastMessage_36; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastMessage_36() { return &___lastMessage_36; }
	inline void set_lastMessage_36(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastMessage_36 = value;
	}

	inline static int32_t get_offset_of_IncompleteFrames_37() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___IncompleteFrames_37)); }
	inline List_1_tACEAA9B1C6B540445C9D9A97B84B613D3E81C0ED * get_IncompleteFrames_37() const { return ___IncompleteFrames_37; }
	inline List_1_tACEAA9B1C6B540445C9D9A97B84B613D3E81C0ED ** get_address_of_IncompleteFrames_37() { return &___IncompleteFrames_37; }
	inline void set_IncompleteFrames_37(List_1_tACEAA9B1C6B540445C9D9A97B84B613D3E81C0ED * value)
	{
		___IncompleteFrames_37 = value;
		Il2CppCodeGenWriteBarrier((&___IncompleteFrames_37), value);
	}

	inline static int32_t get_offset_of_CompletedFrames_38() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___CompletedFrames_38)); }
	inline ConcurrentQueue_1_tC3C7EA7D8366EFAB17771E9A04D39272AF812FCA * get_CompletedFrames_38() const { return ___CompletedFrames_38; }
	inline ConcurrentQueue_1_tC3C7EA7D8366EFAB17771E9A04D39272AF812FCA ** get_address_of_CompletedFrames_38() { return &___CompletedFrames_38; }
	inline void set_CompletedFrames_38(ConcurrentQueue_1_tC3C7EA7D8366EFAB17771E9A04D39272AF812FCA * value)
	{
		___CompletedFrames_38 = value;
		Il2CppCodeGenWriteBarrier((&___CompletedFrames_38), value);
	}

	inline static int32_t get_offset_of_CloseFrame_39() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___CloseFrame_39)); }
	inline WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2  get_CloseFrame_39() const { return ___CloseFrame_39; }
	inline WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2 * get_address_of_CloseFrame_39() { return &___CloseFrame_39; }
	inline void set_CloseFrame_39(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2  value)
	{
		___CloseFrame_39 = value;
	}

	inline static int32_t get_offset_of_unsentFrames_40() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___unsentFrames_40)); }
	inline ConcurrentQueue_1_t8AA22CA8D8AADD1F0E3ACBDCDD8DB501B8A3DCAB * get_unsentFrames_40() const { return ___unsentFrames_40; }
	inline ConcurrentQueue_1_t8AA22CA8D8AADD1F0E3ACBDCDD8DB501B8A3DCAB ** get_address_of_unsentFrames_40() { return &___unsentFrames_40; }
	inline void set_unsentFrames_40(ConcurrentQueue_1_t8AA22CA8D8AADD1F0E3ACBDCDD8DB501B8A3DCAB * value)
	{
		___unsentFrames_40 = value;
		Il2CppCodeGenWriteBarrier((&___unsentFrames_40), value);
	}

	inline static int32_t get_offset_of_newFrameSignal_41() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___newFrameSignal_41)); }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * get_newFrameSignal_41() const { return ___newFrameSignal_41; }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 ** get_address_of_newFrameSignal_41() { return &___newFrameSignal_41; }
	inline void set_newFrameSignal_41(AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * value)
	{
		___newFrameSignal_41 = value;
		Il2CppCodeGenWriteBarrier((&___newFrameSignal_41), value);
	}

	inline static int32_t get_offset_of_sendThreadCreated_42() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___sendThreadCreated_42)); }
	inline int32_t get_sendThreadCreated_42() const { return ___sendThreadCreated_42; }
	inline int32_t* get_address_of_sendThreadCreated_42() { return &___sendThreadCreated_42; }
	inline void set_sendThreadCreated_42(int32_t value)
	{
		___sendThreadCreated_42 = value;
	}

	inline static int32_t get_offset_of_closedThreads_43() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___closedThreads_43)); }
	inline int32_t get_closedThreads_43() const { return ___closedThreads_43; }
	inline int32_t* get_address_of_closedThreads_43() { return &___closedThreads_43; }
	inline void set_closedThreads_43(int32_t value)
	{
		___closedThreads_43 = value;
	}

	inline static int32_t get_offset_of_closeSent_44() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___closeSent_44)); }
	inline bool get_closeSent_44() const { return ___closeSent_44; }
	inline bool* get_address_of_closeSent_44() { return &___closeSent_44; }
	inline void set_closeSent_44(bool value)
	{
		___closeSent_44 = value;
	}

	inline static int32_t get_offset_of_closed_45() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___closed_45)); }
	inline bool get_closed_45() const { return ___closed_45; }
	inline bool* get_address_of_closed_45() { return &___closed_45; }
	inline void set_closed_45(bool value)
	{
		___closed_45 = value;
	}

	inline static int32_t get_offset_of_lastPing_46() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___lastPing_46)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastPing_46() const { return ___lastPing_46; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastPing_46() { return &___lastPing_46; }
	inline void set_lastPing_46(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastPing_46 = value;
	}

	inline static int32_t get_offset_of_rtts_47() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___rtts_47)); }
	inline CircularBuffer_1_tAC7579C8AD77EAE5D35A20D485487AAE74B97F42 * get_rtts_47() const { return ___rtts_47; }
	inline CircularBuffer_1_tAC7579C8AD77EAE5D35A20D485487AAE74B97F42 ** get_address_of_rtts_47() { return &___rtts_47; }
	inline void set_rtts_47(CircularBuffer_1_tAC7579C8AD77EAE5D35A20D485487AAE74B97F42 * value)
	{
		___rtts_47 = value;
		Il2CppCodeGenWriteBarrier((&___rtts_47), value);
	}

	inline static int32_t get_offset_of_closedThreadCount_48() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3, ___closedThreadCount_48)); }
	inline int32_t get_closedThreadCount_48() const { return ___closedThreadCount_48; }
	inline int32_t* get_address_of_closedThreadCount_48() { return &___closedThreadCount_48; }
	inline void set_closedThreadCount_48(int32_t value)
	{
		___closedThreadCount_48 = value;
	}
};

struct WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3_StaticFields
{
public:
	// System.Int32 BestHTTP.WebSocket.WebSocketResponse::RTTBufferCapacity
	int32_t ___RTTBufferCapacity_25;

public:
	inline static int32_t get_offset_of_RTTBufferCapacity_25() { return static_cast<int32_t>(offsetof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3_StaticFields, ___RTTBufferCapacity_25)); }
	inline int32_t get_RTTBufferCapacity_25() const { return ___RTTBufferCapacity_25; }
	inline int32_t* get_address_of_RTTBufferCapacity_25() { return &___RTTBufferCapacity_25; }
	inline void set_RTTBufferCapacity_25(int32_t value)
	{
		___RTTBufferCapacity_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETRESPONSE_T377DAE617A82BCEA661252342C9E5B376C156CB3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (OnWebSocketOpenDelegate_t5A0F7F1B48A143D5B44A178C5111CD3B04FFE002), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (OnWebSocketMessageDelegate_t8910D49B1773AF8E8534824F816A1B8ACB60ECB0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (OnWebSocketBinaryDelegate_tED50D70E50C37A2A49AF22818A50CFE952F3DAD6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (OnWebSocketClosedDelegate_t6B5DBB85F1CB518AF381D301FBE5BB9DFE65BB98), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (OnWebSocketErrorDelegate_t4032FB25EA4F01DD9AD581FD52F0B8BA0B9DD3EA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (OnWebSocketIncompleteFrameDelegate_tD3147F18AA5B1F8E144FE93DCB52BB70035A1695), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3906[14] = 
{
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_U3CStateU3Ek__BackingField_0(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_U3CStartPingThreadU3Ek__BackingField_1(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_U3CPingFrequencyU3Ek__BackingField_2(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_U3CCloseAfterNoMesssageU3Ek__BackingField_3(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_U3CInternalRequestU3Ek__BackingField_4(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_U3CExtensionsU3Ek__BackingField_5(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_OnOpen_6(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_OnMessage_7(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_OnBinary_8(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_OnClosed_9(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_OnError_10(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_OnIncompleteFrame_11(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_requestSent_12(),
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717::get_offset_of_webSocket_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3), -1, sizeof(WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3907[24] = 
{
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3_StaticFields::get_offset_of_RTTBufferCapacity_25(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_U3CWebSocketU3Ek__BackingField_26(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_OnText_27(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_OnBinary_28(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_OnIncompleteFrame_29(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_OnClosed_30(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_U3CConnectionKeyU3Ek__BackingField_31(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_U3CPingFrequnecyU3Ek__BackingField_32(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_U3CMaxFragmentSizeU3Ek__BackingField_33(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of__bufferedAmount_34(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_U3CLatencyU3Ek__BackingField_35(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_lastMessage_36(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_IncompleteFrames_37(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_CompletedFrames_38(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_CloseFrame_39(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_unsentFrames_40(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_newFrameSignal_41(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_sendThreadCreated_42(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_closedThreads_43(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_closeSent_44(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_closed_45(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_lastPing_46(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_rtts_47(),
	WebSocketResponse_t377DAE617A82BCEA661252342C9E5B376C156CB3::get_offset_of_closedThreadCount_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (WebSocketStausCodes_t4233376B407476F1BA66FA16F70C8F8CEB784435)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3908[14] = 
{
	WebSocketStausCodes_t4233376B407476F1BA66FA16F70C8F8CEB784435::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804)+ sizeof (RuntimeObject), sizeof(RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3909[2] = 
{
	RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804::get_offset_of_Data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawFrameData_t607B8D48C32318464F03713BFFCE3177962E5804::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3910[6] = 
{
	WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0::get_offset_of_U3CIsFinalU3Ek__BackingField_1(),
	WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0::get_offset_of_U3CHeaderU3Ek__BackingField_2(),
	WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0::get_offset_of_U3CDataU3Ek__BackingField_3(),
	WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0::get_offset_of_U3CDataLengthU3Ek__BackingField_4(),
	WebSocketFrame_t493C361BF280BEB44AC6F1973F3A9684A3DDB2F0::get_offset_of_U3CUseExtensionsU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2)+ sizeof (RuntimeObject), sizeof(WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3911[7] = 
{
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CHeaderU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CIsFinalU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CTypeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CHasMaskU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CLengthU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CDataU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WebSocketFrameReader_tDD98E162F188C44922FB698F15B7B3E9B6ADECC2::get_offset_of_U3CDataAsTextU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (WebSocketFrameTypes_t73771AC782F5FFF8E3D97BF652383FE4AEEFE85E)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3912[7] = 
{
	WebSocketFrameTypes_t73771AC782F5FFF8E3D97BF652383FE4AEEFE85E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F), -1, sizeof(PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3914[13] = 
{
	0,
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F_StaticFields::get_offset_of_Trailer_1(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_U3CClientNoContextTakeoverU3Ek__BackingField_2(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_U3CServerNoContextTakeoverU3Ek__BackingField_3(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_U3CClientMaxWindowBitsU3Ek__BackingField_4(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_U3CServerMaxWindowBitsU3Ek__BackingField_5(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_U3CLevelU3Ek__BackingField_6(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_U3CMinimumDataLegthToCompressU3Ek__BackingField_7(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_compressorOutputStream_8(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_compressorDeflateStream_9(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_decompressorInputStream_10(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_decompressorOutputStream_11(),
	PerMessageCompression_tFE288EAE69EEC2B5E84202F9D158676C35B3139F::get_offset_of_decompressorDeflateStream_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (States_t0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3915[7] = 
{
	States_t0092CB1B8E4907408CBCAE537BA58F8AE6B9EB70::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (OnGeneralEventDelegate_tF3741A566EE666F0F1375A432ED6FCFB7B7B3A9B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (OnMessageDelegate_t3A083AC122C22EA38C6377C3BFA2FF2C2DF4F69D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (OnErrorDelegate_t9E82A1F01A812C06D1C802221B388190DF7460A2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (OnEventDelegate_t3CFFA35D4F0B715CD8AB5E6514D4B9E91DE0F813), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (OnStateChangedDelegate_t5662E384D5C358DCA5775397519E8975F3DA1ADE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[19] = 
{
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_U3CUriU3Ek__BackingField_0(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of__state_1(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_U3CReconnectionTimeU3Ek__BackingField_2(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_U3CLastEventIdU3Ek__BackingField_3(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_U3CConnectionKeyU3Ek__BackingField_4(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_U3CInternalRequestU3Ek__BackingField_5(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_OnOpen_6(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_OnMessage_7(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_OnError_8(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_OnRetry_9(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_OnClosed_10(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_OnStateChanged_11(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_EventTable_12(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_RetryCount_13(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_RetryCalled_14(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_LineBuffer_15(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_LineBufferPos_16(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_CurrentMessage_17(),
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6::get_offset_of_CompletedMessages_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3923[4] = 
{
	Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3::get_offset_of_U3CIdU3Ek__BackingField_0(),
	Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3::get_offset_of_U3CEventU3Ek__BackingField_1(),
	Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3::get_offset_of_U3CDataU3Ek__BackingField_2(),
	Message_t43D771510972B893CF0C3A63B3F0C1406157ECB3::get_offset_of_U3CRetryU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3924[1] = 
{
	AttributeCertificateHolder_t405580B68B684496CDEA08842CDBC2978E504D20::get_offset_of_holder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[1] = 
{
	AttributeCertificateIssuer_tC5816226F2B1BE412A49E980B499D0B6BC1A8EBF::get_offset_of_form_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3928[4] = 
{
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE::get_offset_of__header1_0(),
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE::get_offset_of__header2_1(),
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE::get_offset_of__footer1_2(),
	PemParser_t08EEDB2B460C5FD8CC633530936F9E8F336F1BAE::get_offset_of__footer2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (PrincipalUtilities_tF8725038B210BBD7D9EE399FF876992694DCB523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (SubjectPublicKeyInfoFactory_tD85601347B2AD762413F648CF79E31930E714D02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145), -1, sizeof(X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3931[4] = 
{
	X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145_StaticFields::get_offset_of_PemAttrCertParser_0(),
	X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145::get_offset_of_sData_1(),
	X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145::get_offset_of_sDataObjectCount_2(),
	X509AttrCertParser_t0AC7E90BA6F690FA6163B72876B6420BC0B6B145::get_offset_of_currentStream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (X509Attribute_t3B556C69F15E13F3E1629F7AF75DFD0A85C27363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[1] = 
{
	X509Attribute_t3B556C69F15E13F3E1629F7AF75DFD0A85C27363::get_offset_of_attr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3933[7] = 
{
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_c_0(),
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_basicConstraints_1(),
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_keyUsage_2(),
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_cacheLock_3(),
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_publicKeyValue_4(),
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_hashValueSet_5(),
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A::get_offset_of_hashValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3934[2] = 
{
	X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A::get_offset_of_forward_0(),
	X509CertificatePair_t155597D8F62FCEA9553FCF9EA4EB9D4F30AB7C0A::get_offset_of_reverse_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA), -1, sizeof(X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3935[4] = 
{
	X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA_StaticFields::get_offset_of_PemCertParser_0(),
	X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA::get_offset_of_sData_1(),
	X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA::get_offset_of_sDataObjectCount_2(),
	X509CertificateParser_t8B967D072C4979DBD835A0B3706DC3BF91E8BBEA::get_offset_of_currentStream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (X509CertPairParser_tC5DA02E4655BF842D80F18F9DC19862C4A4D07AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3936[1] = 
{
	X509CertPairParser_tC5DA02E4655BF842D80F18F9DC19862C4A4D07AD::get_offset_of_currentStream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3937[6] = 
{
	X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485::get_offset_of_c_0(),
	X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485::get_offset_of_sigAlgName_1(),
	X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485::get_offset_of_sigAlgParams_2(),
	X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485::get_offset_of_isIndirect_3(),
	X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485::get_offset_of_hashValueSet_4(),
	X509Crl_t520B5625C6B26501AD20216DD2F96852CAE17485::get_offset_of_hashValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3938[6] = 
{
	X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15::get_offset_of_c_0(),
	X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15::get_offset_of_isIndirect_1(),
	X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15::get_offset_of_previousCertificateIssuer_2(),
	X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15::get_offset_of_certificateIssuer_3(),
	X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15::get_offset_of_hashValueSet_4(),
	X509CrlEntry_t7F0EC211082F01063A226A53F6FBD47EBC245B15::get_offset_of_hashValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0), -1, sizeof(X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3939[5] = 
{
	X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0_StaticFields::get_offset_of_PemCrlParser_0(),
	X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0::get_offset_of_lazyAsn1_1(),
	X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0::get_offset_of_sCrlData_2(),
	X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0::get_offset_of_sCrlDataObjectCount_3(),
	X509CrlParser_t795B37D4C0B1D4149CFCC9FB6CEFF5FF9CF1EAF0::get_offset_of_currentCrlStream_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (X509KeyUsage_t9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	X509KeyUsage_t9FD4C4C5EB6ABF28F5B96544E58090AECAEF4EE6::get_offset_of_usage_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (X509SignatureUtilities_t10BDD147690143446B4C9F074750F2ACD2677FBE), -1, sizeof(X509SignatureUtilities_t10BDD147690143446B4C9F074750F2ACD2677FBE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3942[1] = 
{
	X509SignatureUtilities_t10BDD147690143446B4C9F074750F2ACD2677FBE_StaticFields::get_offset_of_derNull_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C), -1, sizeof(X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3943[3] = 
{
	X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields::get_offset_of_algorithms_0(),
	X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields::get_offset_of_exParams_1(),
	X509Utilities_tA2469B19FB181B4E80B991F7B9910788C60D2F5C_StaticFields::get_offset_of_noParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3944[4] = 
{
	X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC::get_offset_of_tbsGen_0(),
	X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC::get_offset_of_sigOID_1(),
	X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC::get_offset_of_sigAlgId_2(),
	X509V1CertificateGenerator_t41AD5F62F8501A8E4AA6A91386584BAF3DC54AAC::get_offset_of_signatureAlgorithm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3945[3] = 
{
	X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD::get_offset_of_cert_0(),
	X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD::get_offset_of_notBefore_1(),
	X509V2AttributeCertificate_t9817BB48E64D8B26AAFC7E81D9EC4A14F560C9CD::get_offset_of_notAfter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3946[5] = 
{
	X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4::get_offset_of_extGenerator_0(),
	X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4::get_offset_of_acInfoGen_1(),
	X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4::get_offset_of_sigOID_2(),
	X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4::get_offset_of_sigAlgId_3(),
	X509V2AttributeCertificateGenerator_tFD2DE9AB85A54FE0D183A4584459D8F5A1ACBCF4::get_offset_of_signatureAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3947[5] = 
{
	X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6::get_offset_of_extGenerator_0(),
	X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6::get_offset_of_tbsGen_1(),
	X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6::get_offset_of_sigOID_2(),
	X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6::get_offset_of_sigAlgId_3(),
	X509V2CrlGenerator_tDBF91738285ED28BF6A415C4A6FD1120DEA012D6::get_offset_of_signatureAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3948[5] = 
{
	X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148::get_offset_of_extGenerator_0(),
	X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148::get_offset_of_tbsGen_1(),
	X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148::get_offset_of_sigOid_2(),
	X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148::get_offset_of_sigAlgId_3(),
	X509V3CertificateGenerator_t644480B3A02B878F584248CB2362B514175A7148::get_offset_of_signatureAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (NoSuchStoreException_t624010015CF3D94AD307936B95C2F323391FC495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3953[7] = 
{
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_attributeCert_0(),
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_attributeCertificateValid_1(),
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_holder_2(),
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_issuer_3(),
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_serialNumber_4(),
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_targetNames_5(),
	X509AttrCertStoreSelector_t36EF57C6859BF61E884053CC25062F28BFD6D54C::get_offset_of_targetGroups_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3954[3] = 
{
	X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886::get_offset_of_certPair_0(),
	X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886::get_offset_of_forwardSelector_1(),
	X509CertPairStoreSelector_t3490268DE4B11D1AA36E3D7CA087E6B7B0372886::get_offset_of_reverseSelector_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3955[15] = 
{
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_authorityKeyIdentifier_0(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_basicConstraints_1(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_certificate_2(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_certificateValid_3(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_extendedKeyUsage_4(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_ignoreX509NameOrdering_5(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_issuer_6(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_keyUsage_7(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_policy_8(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_privateKeyValid_9(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_serialNumber_10(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_subject_11(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_subjectKeyIdentifier_12(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_subjectPublicKey_13(),
	X509CertStoreSelector_tB83E9EDC21C540D4E44F3B7D291AB47B9D6D2CE9::get_offset_of_subjectPublicKeyAlgID_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (X509CollectionStore_t2F0493B4CE42FACCAAC81103079053B78FCD438C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[1] = 
{
	X509CollectionStore_t2F0493B4CE42FACCAAC81103079053B78FCD438C::get_offset_of__local_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (X509CollectionStoreParameters_tF34162D037E59181EE3392E3E74708609A546E4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[1] = 
{
	X509CollectionStoreParameters_tF34162D037E59181EE3392E3E74708609A546E4C::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[11] = 
{
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_certificateChecking_0(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_dateAndTime_1(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_issuers_2(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_maxCrlNumber_3(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_minCrlNumber_4(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_attrCertChecking_5(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_completeCrlEnabled_6(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_deltaCrlIndicatorEnabled_7(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_issuingDistributionPoint_8(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_issuingDistributionPointEnabled_9(),
	X509CrlStoreSelector_t3DED686129E05ADAFCD3B73273C728CEF48920F2::get_offset_of_maxBaseCrlNumber_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (X509StoreException_t1D46DCD3C70CB529F5CF3A4417C1B13EEDE6ACC0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (X509StoreFactory_tFCACCF97F646FBC473625C188F7596DBA53766B5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (AuthorityKeyIdentifierStructure_t6AE8052DF734F9E5F6FF1EB0E54CB16E7164E642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (SubjectKeyIdentifierStructure_t7DB46DFB875613D5FEAC43667EE895A920C8C575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (X509ExtensionUtilities_t98D18816F30C017C13AF09CD68828E16F93A1967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59), -1, sizeof(Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3964[2] = 
{
	Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59_StaticFields::get_offset_of_EmptyBytes_0(),
	Arrays_t0CA87B46BFFC75142B35010D1548C2E3F4264D59_StaticFields::get_offset_of_EmptyInts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (BigIntegers_t27C86ECF602A3E23D3F91044072014BF3BD0CB33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (Enums_t1C4E8BFC1B312878E9C44DA3F1D456A3892B581D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (Integers_t11000EAA288DC9AD1138E2BB196334D292921246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (MemoableResetException_t8B342C58F680A5D1891FB58655C695831D0DAF61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6), -1, sizeof(Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3970[2] = 
{
	Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6_StaticFields::get_offset_of_InvariantCompareInfo_0(),
	Platform_t831F01D6ED4908474F1B5D3C147E1566CE9F33A6_StaticFields::get_offset_of_NewLine_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (Strings_tE7B66D217DED931E74860F122791837EECA1F936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (Times_tB5570DECE17559D80B13E430F6AE143ECFB50561), -1, sizeof(Times_tB5570DECE17559D80B13E430F6AE143ECFB50561_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3972[1] = 
{
	Times_tB5570DECE17559D80B13E430F6AE143ECFB50561_StaticFields::get_offset_of_NanosecondsPerTick_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (Adler32_t0BCA7796796DD2EE4AA97252B3E46854F69B2A39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3973[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA), -1, sizeof(Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3974[112] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_StaticFields::get_offset_of_config_table_7(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA_StaticFields::get_offset_of_z_errmsg_8(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_strm_56(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_status_57(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_pending_buf_58(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_pending_out_59(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_pending_60(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_noheader_61(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_data_type_62(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_method_63(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_last_flush_64(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_w_size_65(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_w_bits_66(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_w_mask_67(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_window_68(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_window_size_69(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_prev_70(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_head_71(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_ins_h_72(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_hash_size_73(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_hash_bits_74(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_hash_mask_75(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_hash_shift_76(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_block_start_77(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_match_length_78(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_prev_match_79(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_match_available_80(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_strstart_81(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_match_start_82(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_lookahead_83(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_prev_length_84(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_max_chain_length_85(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_max_lazy_match_86(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_level_87(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_strategy_88(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_good_match_89(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_nice_match_90(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_dyn_ltree_91(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_dyn_dtree_92(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_bl_tree_93(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_l_desc_94(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_d_desc_95(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_bl_desc_96(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_bl_count_97(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_heap_98(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_heap_len_99(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_heap_max_100(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_depth_101(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_l_buf_102(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_lit_bufsize_103(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_last_lit_104(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_d_buf_105(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_opt_len_106(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_static_len_107(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_matches_108(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_last_eob_len_109(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_bi_buf_110(),
	Deflate_t73F72125D02C8DE86B30F16DF1A6C8087E9B9EDA::get_offset_of_bi_valid_111(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[5] = 
{
	Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC::get_offset_of_good_length_0(),
	Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC::get_offset_of_max_lazy_1(),
	Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC::get_offset_of_nice_length_2(),
	Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC::get_offset_of_max_chain_3(),
	Config_t569D9515884EFF9E53743422FFF723B4BFE2A4CC::get_offset_of_func_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D), -1, sizeof(InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3976[41] = 
{
	0,
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_StaticFields::get_offset_of_inflate_mask_1(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D_StaticFields::get_offset_of_border_2(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_mode_22(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_left_23(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_table_24(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_index_25(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_blens_26(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_bb_27(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_tb_28(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_codes_29(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_last_30(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_bitk_31(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_bitb_32(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_hufts_33(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_window_34(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_end_35(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_read_36(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_write_37(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_checkfn_38(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_check_39(),
	InfBlocks_tA1C83A244F2BE3FD379E18E5A7DBFEB5CB16669D::get_offset_of_inftree_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711), -1, sizeof(InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3977[34] = 
{
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711_StaticFields::get_offset_of_inflate_mask_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_mode_20(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_len_21(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_tree_22(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_tree_index_23(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_need_24(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_lit_25(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_get_26(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_dist_27(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_lbits_28(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_dbits_29(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_ltree_30(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_ltree_index_31(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_dtree_32(),
	InfCodes_t738730CDE57F83544ADFE0C29244F33FE07AB711::get_offset_of_dtree_index_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2), -1, sizeof(Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3978[40] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_mode_31(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_method_32(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_was_33(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_need_34(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_marker_35(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_nowrap_36(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_wbits_37(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2::get_offset_of_blocks_38(),
	Inflate_tA7F860F1043EA81B508B38F83F0F5C7724C4B0C2_StaticFields::get_offset_of_mark_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1), -1, sizeof(InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3979[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields::get_offset_of_fixed_tl_12(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields::get_offset_of_fixed_td_13(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields::get_offset_of_cplens_14(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields::get_offset_of_cplext_15(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields::get_offset_of_cpdist_16(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1_StaticFields::get_offset_of_cpdext_17(),
	0,
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1::get_offset_of_hn_19(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1::get_offset_of_v_20(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1::get_offset_of_c_21(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1::get_offset_of_r_22(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1::get_offset_of_u_23(),
	InfTree_t50F048E4F49FDBE2632634B7B3F35226E77FBCD1::get_offset_of_x_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (JZlib_tC545D38309B0B7087A0B67726615F5FC039BFDED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3980[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE), -1, sizeof(StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3981[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields::get_offset_of_static_ltree_7(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields::get_offset_of_static_dtree_8(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields::get_offset_of_static_l_desc_9(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields::get_offset_of_static_d_desc_10(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE_StaticFields::get_offset_of_static_bl_desc_11(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE::get_offset_of_static_tree_12(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE::get_offset_of_extra_bits_13(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE::get_offset_of_extra_base_14(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE::get_offset_of_elems_15(),
	StaticTree_t7FDF133D27FBB267A9B9110A180E976B3538E7BE::get_offset_of_max_length_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3982[6] = 
{
	ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282::get_offset_of_z_5(),
	ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282::get_offset_of_flushLevel_6(),
	0,
	ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282::get_offset_of_buf_8(),
	ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282::get_offset_of_buf1_9(),
	ZDeflaterOutputStream_t606844849BFF5642A1C56ACC71A8085D66B52282::get_offset_of_outp_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3983[7] = 
{
	ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540::get_offset_of_z_5(),
	ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540::get_offset_of_flushLevel_6(),
	0,
	ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540::get_offset_of_buf_8(),
	ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540::get_offset_of_buf1_9(),
	ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540::get_offset_of_inp_10(),
	ZInflaterInputStream_t86FF5C9A563D7A54743BB47012DF006BAE869540::get_offset_of_nomoreinput_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3984[9] = 
{
	0,
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_z_6(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_flushLevel_7(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_buf_8(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_buf1_9(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_compress_10(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_input_11(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_closed_12(),
	ZInputStream_tCE3B181A7C7822F11CAA7564FEC6374AB61B676F::get_offset_of_nomoreinput_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (ZOutputStream_t504E5B441A273254366752284D503186AF93F77A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3985[8] = 
{
	0,
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_z_6(),
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_flushLevel_7(),
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_buf_8(),
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_buf1_9(),
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_compress_10(),
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_output_11(),
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A::get_offset_of_closed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3986[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_next_in_17(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_next_in_index_18(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_avail_in_19(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_total_in_20(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_next_out_21(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_next_out_index_22(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_avail_out_23(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_total_out_24(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_msg_25(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_dstate_26(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_istate_27(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_data_type_28(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of_adler_29(),
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F::get_offset_of__adler_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36), -1, sizeof(ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3987[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of_extra_lbits_12(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of_extra_dbits_13(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of_extra_blbits_14(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of_bl_order_15(),
	0,
	0,
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of__dist_code_18(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of__length_code_19(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of_base_length_20(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36_StaticFields::get_offset_of_base_dist_21(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36::get_offset_of_dyn_tree_22(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36::get_offset_of_max_code_23(),
	ZTree_tC412ADAA4ACB25CF573BA601A6C7970BF7D51B36::get_offset_of_stat_desc_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (IPAddress_tCD751E2DA79B3FA9B096C8EFD4C3BFE697DADCDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3989[1] = 
{
	BaseInputStream_tFB934C38CB5B81842A543CE8228DDE5235F1E2E4::get_offset_of_closed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3990[1] = 
{
	BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9::get_offset_of_closed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3991[1] = 
{
	FilterStream_t45FCEEC640FED6C0631CB5AC1F2C65268CE1B9AF::get_offset_of_s_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (MemoryInputStream_tF61D08F10AC0A9453ADF2922D2FA54C1C077C86E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (MemoryOutputStream_t468D82831460535904A154438A5E8B56DF0119C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (NullOutputStream_tE2F7DB7E68F95E648F210FF1A0D54EF99FFB2A7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (PushbackStream_t685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3995[1] = 
{
	PushbackStream_t685D16DC4113D5CFE3E7081A6B0BC38D9A3160D0::get_offset_of_buf_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (StreamOverflowException_t33C848DA996CBC4B61E660106E8D5D12DEC111F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (Streams_t2A1A958B8599E84F53102CACBF26991DEABAEEB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3997[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (TeeInputStream_tD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[2] = 
{
	TeeInputStream_tD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28::get_offset_of_input_6(),
	TeeInputStream_tD6CEC6D8F98C872A46EE698E186C63A7E7F5DE28::get_offset_of_tee_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (TeeOutputStream_t0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3999[2] = 
{
	TeeOutputStream_t0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36::get_offset_of_output_6(),
	TeeOutputStream_t0B2B00AC4C09928C963C8D9767BEEB9FD1B5AF36::get_offset_of_tee_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
