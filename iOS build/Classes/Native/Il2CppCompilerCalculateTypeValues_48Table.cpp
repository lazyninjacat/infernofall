﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest
struct Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine
struct SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine
struct ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine
struct Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_t391586A9268E9DABF6C6158169C448C4243FA87C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher
struct IBufferedCipher_tF573FBDFB2ABDB6CF7049013D91BED1EABEEAA3A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters
struct ICipherParameters_t1E1B86752982ED2CFC2683B4C1A0BC0CDB77345B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest
struct IDigest_t4779036D052ECF08BAC3DF71ED71EF04D7866C09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac
struct IMac_tD02303ADA25C4494D9C2B119B486DF1D9302B33C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner
struct ISigner_tB728365C47647B1E736C0C715DACEBA9341F6CDD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac
struct HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher
struct MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher
struct CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.IGcmExponentiator
struct IGcmExponentiator_t610CEB4FD03068CA4A641C5E44255E796A4922C0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.IGcmMultiplier
struct IGcmMultiplier_tD9C5722D8E74C04B13D61BC9C0077B36F7F85C93;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher
struct GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher
struct SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding
struct IBlockCipherPadding_t480DA3A1FAC2CCBCFC08B3AEA35E4338731AB7FA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters
struct DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyGenerationParameters
struct DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyGenerationParameters
struct ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyGenerationParameters
struct Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyGenerationParameters
struct NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV
struct ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyGenerationParameters
struct RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[]
struct BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t585F03B00DBE773170901D2ABF9576187FAF92B5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IMemoable
struct IMemoable_t90C62D4A075B9E2A77AF505D3B53EC935D5B4591;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt32[][]
struct UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A;
// System.UInt32[][][]
struct UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#define BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedCipherBase
struct  BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F  : public RuntimeObject
{
public:

public:
};

struct BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedCipherBase::EmptyBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBuffer_0;

public:
	inline static int32_t get_offset_of_EmptyBuffer_0() { return static_cast<int32_t>(offsetof(BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields, ___EmptyBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBuffer_0() const { return ___EmptyBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBuffer_0() { return &___EmptyBuffer_0; }
	inline void set_EmptyBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#ifndef CIPHERKEYGENERATOR_T5159EB217F5FC7C4B39BB4EA981381BCC3689BBB_H
#define CIPHERKEYGENERATOR_T5159EB217F5FC7C4B39BB4EA981381BCC3689BBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator
struct  CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::strength
	int32_t ___strength_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::uninitialised
	bool ___uninitialised_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::defaultStrength
	int32_t ___defaultStrength_3;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___strength_1)); }
	inline int32_t get_strength_1() const { return ___strength_1; }
	inline int32_t* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(int32_t value)
	{
		___strength_1 = value;
	}

	inline static int32_t get_offset_of_uninitialised_2() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___uninitialised_2)); }
	inline bool get_uninitialised_2() const { return ___uninitialised_2; }
	inline bool* get_address_of_uninitialised_2() { return &___uninitialised_2; }
	inline void set_uninitialised_2(bool value)
	{
		___uninitialised_2 = value;
	}

	inline static int32_t get_offset_of_defaultStrength_3() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___defaultStrength_3)); }
	inline int32_t get_defaultStrength_3() const { return ___defaultStrength_3; }
	inline int32_t* get_address_of_defaultStrength_3() { return &___defaultStrength_3; }
	inline void set_defaultStrength_3(int32_t value)
	{
		___defaultStrength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERKEYGENERATOR_T5159EB217F5FC7C4B39BB4EA981381BCC3689BBB_H
#ifndef AESENGINE_TEEF495CDA35E3E0765D955ABC5332DD85674AF83_H
#define AESENGINE_TEEF495CDA35E3E0765D955ABC5332DD85674AF83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine
struct  AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::ROUNDS
	int32_t ___ROUNDS_10;
	// System.UInt32[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::WorkingKey
	UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* ___WorkingKey_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::C0
	uint32_t ___C0_12;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::C1
	uint32_t ___C1_13;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::C2
	uint32_t ___C2_14;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::C3
	uint32_t ___C3_15;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::forEncryption
	bool ___forEncryption_16;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::s
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_17;

public:
	inline static int32_t get_offset_of_ROUNDS_10() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___ROUNDS_10)); }
	inline int32_t get_ROUNDS_10() const { return ___ROUNDS_10; }
	inline int32_t* get_address_of_ROUNDS_10() { return &___ROUNDS_10; }
	inline void set_ROUNDS_10(int32_t value)
	{
		___ROUNDS_10 = value;
	}

	inline static int32_t get_offset_of_WorkingKey_11() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___WorkingKey_11)); }
	inline UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* get_WorkingKey_11() const { return ___WorkingKey_11; }
	inline UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A** get_address_of_WorkingKey_11() { return &___WorkingKey_11; }
	inline void set_WorkingKey_11(UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* value)
	{
		___WorkingKey_11 = value;
		Il2CppCodeGenWriteBarrier((&___WorkingKey_11), value);
	}

	inline static int32_t get_offset_of_C0_12() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___C0_12)); }
	inline uint32_t get_C0_12() const { return ___C0_12; }
	inline uint32_t* get_address_of_C0_12() { return &___C0_12; }
	inline void set_C0_12(uint32_t value)
	{
		___C0_12 = value;
	}

	inline static int32_t get_offset_of_C1_13() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___C1_13)); }
	inline uint32_t get_C1_13() const { return ___C1_13; }
	inline uint32_t* get_address_of_C1_13() { return &___C1_13; }
	inline void set_C1_13(uint32_t value)
	{
		___C1_13 = value;
	}

	inline static int32_t get_offset_of_C2_14() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___C2_14)); }
	inline uint32_t get_C2_14() const { return ___C2_14; }
	inline uint32_t* get_address_of_C2_14() { return &___C2_14; }
	inline void set_C2_14(uint32_t value)
	{
		___C2_14 = value;
	}

	inline static int32_t get_offset_of_C3_15() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___C3_15)); }
	inline uint32_t get_C3_15() const { return ___C3_15; }
	inline uint32_t* get_address_of_C3_15() { return &___C3_15; }
	inline void set_C3_15(uint32_t value)
	{
		___C3_15 = value;
	}

	inline static int32_t get_offset_of_forEncryption_16() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___forEncryption_16)); }
	inline bool get_forEncryption_16() const { return ___forEncryption_16; }
	inline bool* get_address_of_forEncryption_16() { return &___forEncryption_16; }
	inline void set_forEncryption_16(bool value)
	{
		___forEncryption_16 = value;
	}

	inline static int32_t get_offset_of_s_17() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83, ___s_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_17() const { return ___s_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_17() { return &___s_17; }
	inline void set_s_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_17), value);
	}
};

struct AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::Si
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Si_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::rcon
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rcon_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::T0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T0_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesEngine::Tinv0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Tinv0_4;

public:
	inline static int32_t get_offset_of_S_0() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields, ___S_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_0() const { return ___S_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_0() { return &___S_0; }
	inline void set_S_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_0 = value;
		Il2CppCodeGenWriteBarrier((&___S_0), value);
	}

	inline static int32_t get_offset_of_Si_1() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields, ___Si_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Si_1() const { return ___Si_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Si_1() { return &___Si_1; }
	inline void set_Si_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Si_1 = value;
		Il2CppCodeGenWriteBarrier((&___Si_1), value);
	}

	inline static int32_t get_offset_of_rcon_2() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields, ___rcon_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rcon_2() const { return ___rcon_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rcon_2() { return &___rcon_2; }
	inline void set_rcon_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___rcon_2), value);
	}

	inline static int32_t get_offset_of_T0_3() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields, ___T0_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T0_3() const { return ___T0_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T0_3() { return &___T0_3; }
	inline void set_T0_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T0_3 = value;
		Il2CppCodeGenWriteBarrier((&___T0_3), value);
	}

	inline static int32_t get_offset_of_Tinv0_4() { return static_cast<int32_t>(offsetof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields, ___Tinv0_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Tinv0_4() const { return ___Tinv0_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Tinv0_4() { return &___Tinv0_4; }
	inline void set_Tinv0_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Tinv0_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tinv0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESENGINE_TEEF495CDA35E3E0765D955ABC5332DD85674AF83_H
#ifndef AESFASTENGINE_T608EDE48725809335256492FA9E6313BED86E2E9_H
#define AESFASTENGINE_T608EDE48725809335256492FA9E6313BED86E2E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine
struct  AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::ROUNDS
	int32_t ___ROUNDS_16;
	// System.UInt32[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::WorkingKey
	UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* ___WorkingKey_17;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::C0
	uint32_t ___C0_18;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::C1
	uint32_t ___C1_19;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::C2
	uint32_t ___C2_20;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::C3
	uint32_t ___C3_21;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::forEncryption
	bool ___forEncryption_22;

public:
	inline static int32_t get_offset_of_ROUNDS_16() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___ROUNDS_16)); }
	inline int32_t get_ROUNDS_16() const { return ___ROUNDS_16; }
	inline int32_t* get_address_of_ROUNDS_16() { return &___ROUNDS_16; }
	inline void set_ROUNDS_16(int32_t value)
	{
		___ROUNDS_16 = value;
	}

	inline static int32_t get_offset_of_WorkingKey_17() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___WorkingKey_17)); }
	inline UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* get_WorkingKey_17() const { return ___WorkingKey_17; }
	inline UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A** get_address_of_WorkingKey_17() { return &___WorkingKey_17; }
	inline void set_WorkingKey_17(UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* value)
	{
		___WorkingKey_17 = value;
		Il2CppCodeGenWriteBarrier((&___WorkingKey_17), value);
	}

	inline static int32_t get_offset_of_C0_18() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___C0_18)); }
	inline uint32_t get_C0_18() const { return ___C0_18; }
	inline uint32_t* get_address_of_C0_18() { return &___C0_18; }
	inline void set_C0_18(uint32_t value)
	{
		___C0_18 = value;
	}

	inline static int32_t get_offset_of_C1_19() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___C1_19)); }
	inline uint32_t get_C1_19() const { return ___C1_19; }
	inline uint32_t* get_address_of_C1_19() { return &___C1_19; }
	inline void set_C1_19(uint32_t value)
	{
		___C1_19 = value;
	}

	inline static int32_t get_offset_of_C2_20() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___C2_20)); }
	inline uint32_t get_C2_20() const { return ___C2_20; }
	inline uint32_t* get_address_of_C2_20() { return &___C2_20; }
	inline void set_C2_20(uint32_t value)
	{
		___C2_20 = value;
	}

	inline static int32_t get_offset_of_C3_21() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___C3_21)); }
	inline uint32_t get_C3_21() const { return ___C3_21; }
	inline uint32_t* get_address_of_C3_21() { return &___C3_21; }
	inline void set_C3_21(uint32_t value)
	{
		___C3_21 = value;
	}

	inline static int32_t get_offset_of_forEncryption_22() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9, ___forEncryption_22)); }
	inline bool get_forEncryption_22() const { return ___forEncryption_22; }
	inline bool* get_address_of_forEncryption_22() { return &___forEncryption_22; }
	inline void set_forEncryption_22(bool value)
	{
		___forEncryption_22 = value;
	}
};

struct AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::Si
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Si_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::rcon
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rcon_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::T0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T0_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::T1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T1_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::T2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T2_5;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::T3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T3_6;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::Tinv0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Tinv0_7;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::Tinv1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Tinv1_8;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::Tinv2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Tinv2_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesFastEngine::Tinv3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Tinv3_10;

public:
	inline static int32_t get_offset_of_S_0() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___S_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_0() const { return ___S_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_0() { return &___S_0; }
	inline void set_S_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_0 = value;
		Il2CppCodeGenWriteBarrier((&___S_0), value);
	}

	inline static int32_t get_offset_of_Si_1() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___Si_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Si_1() const { return ___Si_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Si_1() { return &___Si_1; }
	inline void set_Si_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Si_1 = value;
		Il2CppCodeGenWriteBarrier((&___Si_1), value);
	}

	inline static int32_t get_offset_of_rcon_2() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___rcon_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rcon_2() const { return ___rcon_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rcon_2() { return &___rcon_2; }
	inline void set_rcon_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___rcon_2), value);
	}

	inline static int32_t get_offset_of_T0_3() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___T0_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T0_3() const { return ___T0_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T0_3() { return &___T0_3; }
	inline void set_T0_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T0_3 = value;
		Il2CppCodeGenWriteBarrier((&___T0_3), value);
	}

	inline static int32_t get_offset_of_T1_4() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___T1_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T1_4() const { return ___T1_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T1_4() { return &___T1_4; }
	inline void set_T1_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T1_4 = value;
		Il2CppCodeGenWriteBarrier((&___T1_4), value);
	}

	inline static int32_t get_offset_of_T2_5() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___T2_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T2_5() const { return ___T2_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T2_5() { return &___T2_5; }
	inline void set_T2_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T2_5 = value;
		Il2CppCodeGenWriteBarrier((&___T2_5), value);
	}

	inline static int32_t get_offset_of_T3_6() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___T3_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T3_6() const { return ___T3_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T3_6() { return &___T3_6; }
	inline void set_T3_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T3_6 = value;
		Il2CppCodeGenWriteBarrier((&___T3_6), value);
	}

	inline static int32_t get_offset_of_Tinv0_7() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___Tinv0_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Tinv0_7() const { return ___Tinv0_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Tinv0_7() { return &___Tinv0_7; }
	inline void set_Tinv0_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Tinv0_7 = value;
		Il2CppCodeGenWriteBarrier((&___Tinv0_7), value);
	}

	inline static int32_t get_offset_of_Tinv1_8() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___Tinv1_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Tinv1_8() const { return ___Tinv1_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Tinv1_8() { return &___Tinv1_8; }
	inline void set_Tinv1_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Tinv1_8 = value;
		Il2CppCodeGenWriteBarrier((&___Tinv1_8), value);
	}

	inline static int32_t get_offset_of_Tinv2_9() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___Tinv2_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Tinv2_9() const { return ___Tinv2_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Tinv2_9() { return &___Tinv2_9; }
	inline void set_Tinv2_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Tinv2_9 = value;
		Il2CppCodeGenWriteBarrier((&___Tinv2_9), value);
	}

	inline static int32_t get_offset_of_Tinv3_10() { return static_cast<int32_t>(offsetof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields, ___Tinv3_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Tinv3_10() const { return ___Tinv3_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Tinv3_10() { return &___Tinv3_10; }
	inline void set_Tinv3_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Tinv3_10 = value;
		Il2CppCodeGenWriteBarrier((&___Tinv3_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESFASTENGINE_T608EDE48725809335256492FA9E6313BED86E2E9_H
#ifndef AESLIGHTENGINE_T8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_H
#define AESLIGHTENGINE_T8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine
struct  AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::ROUNDS
	int32_t ___ROUNDS_8;
	// System.UInt32[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::WorkingKey
	UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* ___WorkingKey_9;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::C0
	uint32_t ___C0_10;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::C1
	uint32_t ___C1_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::C2
	uint32_t ___C2_12;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::C3
	uint32_t ___C3_13;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::forEncryption
	bool ___forEncryption_14;

public:
	inline static int32_t get_offset_of_ROUNDS_8() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___ROUNDS_8)); }
	inline int32_t get_ROUNDS_8() const { return ___ROUNDS_8; }
	inline int32_t* get_address_of_ROUNDS_8() { return &___ROUNDS_8; }
	inline void set_ROUNDS_8(int32_t value)
	{
		___ROUNDS_8 = value;
	}

	inline static int32_t get_offset_of_WorkingKey_9() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___WorkingKey_9)); }
	inline UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* get_WorkingKey_9() const { return ___WorkingKey_9; }
	inline UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A** get_address_of_WorkingKey_9() { return &___WorkingKey_9; }
	inline void set_WorkingKey_9(UInt32U5BU5DU5BU5D_tB88C5BD87BC67FF7E5287CF5CE46AD642DAA318A* value)
	{
		___WorkingKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___WorkingKey_9), value);
	}

	inline static int32_t get_offset_of_C0_10() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___C0_10)); }
	inline uint32_t get_C0_10() const { return ___C0_10; }
	inline uint32_t* get_address_of_C0_10() { return &___C0_10; }
	inline void set_C0_10(uint32_t value)
	{
		___C0_10 = value;
	}

	inline static int32_t get_offset_of_C1_11() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___C1_11)); }
	inline uint32_t get_C1_11() const { return ___C1_11; }
	inline uint32_t* get_address_of_C1_11() { return &___C1_11; }
	inline void set_C1_11(uint32_t value)
	{
		___C1_11 = value;
	}

	inline static int32_t get_offset_of_C2_12() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___C2_12)); }
	inline uint32_t get_C2_12() const { return ___C2_12; }
	inline uint32_t* get_address_of_C2_12() { return &___C2_12; }
	inline void set_C2_12(uint32_t value)
	{
		___C2_12 = value;
	}

	inline static int32_t get_offset_of_C3_13() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___C3_13)); }
	inline uint32_t get_C3_13() const { return ___C3_13; }
	inline uint32_t* get_address_of_C3_13() { return &___C3_13; }
	inline void set_C3_13(uint32_t value)
	{
		___C3_13 = value;
	}

	inline static int32_t get_offset_of_forEncryption_14() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B, ___forEncryption_14)); }
	inline bool get_forEncryption_14() const { return ___forEncryption_14; }
	inline bool* get_address_of_forEncryption_14() { return &___forEncryption_14; }
	inline void set_forEncryption_14(bool value)
	{
		___forEncryption_14 = value;
	}
};

struct AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::Si
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Si_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesLightEngine::rcon
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rcon_2;

public:
	inline static int32_t get_offset_of_S_0() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields, ___S_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_0() const { return ___S_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_0() { return &___S_0; }
	inline void set_S_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_0 = value;
		Il2CppCodeGenWriteBarrier((&___S_0), value);
	}

	inline static int32_t get_offset_of_Si_1() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields, ___Si_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Si_1() const { return ___Si_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Si_1() { return &___Si_1; }
	inline void set_Si_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Si_1 = value;
		Il2CppCodeGenWriteBarrier((&___Si_1), value);
	}

	inline static int32_t get_offset_of_rcon_2() { return static_cast<int32_t>(offsetof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields, ___rcon_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rcon_2() const { return ___rcon_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rcon_2() { return &___rcon_2; }
	inline void set_rcon_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___rcon_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESLIGHTENGINE_T8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_H
#ifndef BLOWFISHENGINE_T25C089870C7A85A148516CEBFBAD3FFDC71FD208_H
#define BLOWFISHENGINE_T25C089870C7A85A148516CEBFBAD3FFDC71FD208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine
struct  BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::S0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S0_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::S1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S1_10;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::S2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S2_11;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::S3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S3_12;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_13;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::encrypting
	bool ___encrypting_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_15;

public:
	inline static int32_t get_offset_of_S0_9() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___S0_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S0_9() const { return ___S0_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S0_9() { return &___S0_9; }
	inline void set_S0_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S0_9 = value;
		Il2CppCodeGenWriteBarrier((&___S0_9), value);
	}

	inline static int32_t get_offset_of_S1_10() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___S1_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S1_10() const { return ___S1_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S1_10() { return &___S1_10; }
	inline void set_S1_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S1_10 = value;
		Il2CppCodeGenWriteBarrier((&___S1_10), value);
	}

	inline static int32_t get_offset_of_S2_11() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___S2_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S2_11() const { return ___S2_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S2_11() { return &___S2_11; }
	inline void set_S2_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S2_11 = value;
		Il2CppCodeGenWriteBarrier((&___S2_11), value);
	}

	inline static int32_t get_offset_of_S3_12() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___S3_12)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S3_12() const { return ___S3_12; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S3_12() { return &___S3_12; }
	inline void set_S3_12(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S3_12 = value;
		Il2CppCodeGenWriteBarrier((&___S3_12), value);
	}

	inline static int32_t get_offset_of_P_13() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___P_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_13() const { return ___P_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_13() { return &___P_13; }
	inline void set_P_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_13 = value;
		Il2CppCodeGenWriteBarrier((&___P_13), value);
	}

	inline static int32_t get_offset_of_encrypting_14() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___encrypting_14)); }
	inline bool get_encrypting_14() const { return ___encrypting_14; }
	inline bool* get_address_of_encrypting_14() { return &___encrypting_14; }
	inline void set_encrypting_14(bool value)
	{
		___encrypting_14 = value;
	}

	inline static int32_t get_offset_of_workingKey_15() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208, ___workingKey_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_15() const { return ___workingKey_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_15() { return &___workingKey_15; }
	inline void set_workingKey_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_15 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_15), value);
	}
};

struct BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::KP
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KP_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::KS0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS0_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::KS1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS1_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::KS2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS2_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::KS3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS3_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::ROUNDS
	int32_t ___ROUNDS_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::SBOX_SK
	int32_t ___SBOX_SK_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.BlowfishEngine::P_SZ
	int32_t ___P_SZ_8;

public:
	inline static int32_t get_offset_of_KP_0() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___KP_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KP_0() const { return ___KP_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KP_0() { return &___KP_0; }
	inline void set_KP_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KP_0 = value;
		Il2CppCodeGenWriteBarrier((&___KP_0), value);
	}

	inline static int32_t get_offset_of_KS0_1() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___KS0_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS0_1() const { return ___KS0_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS0_1() { return &___KS0_1; }
	inline void set_KS0_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS0_1 = value;
		Il2CppCodeGenWriteBarrier((&___KS0_1), value);
	}

	inline static int32_t get_offset_of_KS1_2() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___KS1_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS1_2() const { return ___KS1_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS1_2() { return &___KS1_2; }
	inline void set_KS1_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS1_2 = value;
		Il2CppCodeGenWriteBarrier((&___KS1_2), value);
	}

	inline static int32_t get_offset_of_KS2_3() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___KS2_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS2_3() const { return ___KS2_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS2_3() { return &___KS2_3; }
	inline void set_KS2_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS2_3 = value;
		Il2CppCodeGenWriteBarrier((&___KS2_3), value);
	}

	inline static int32_t get_offset_of_KS3_4() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___KS3_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS3_4() const { return ___KS3_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS3_4() { return &___KS3_4; }
	inline void set_KS3_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS3_4 = value;
		Il2CppCodeGenWriteBarrier((&___KS3_4), value);
	}

	inline static int32_t get_offset_of_ROUNDS_5() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___ROUNDS_5)); }
	inline int32_t get_ROUNDS_5() const { return ___ROUNDS_5; }
	inline int32_t* get_address_of_ROUNDS_5() { return &___ROUNDS_5; }
	inline void set_ROUNDS_5(int32_t value)
	{
		___ROUNDS_5 = value;
	}

	inline static int32_t get_offset_of_SBOX_SK_7() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___SBOX_SK_7)); }
	inline int32_t get_SBOX_SK_7() const { return ___SBOX_SK_7; }
	inline int32_t* get_address_of_SBOX_SK_7() { return &___SBOX_SK_7; }
	inline void set_SBOX_SK_7(int32_t value)
	{
		___SBOX_SK_7 = value;
	}

	inline static int32_t get_offset_of_P_SZ_8() { return static_cast<int32_t>(offsetof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields, ___P_SZ_8)); }
	inline int32_t get_P_SZ_8() const { return ___P_SZ_8; }
	inline int32_t* get_address_of_P_SZ_8() { return &___P_SZ_8; }
	inline void set_P_SZ_8(int32_t value)
	{
		___P_SZ_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOWFISHENGINE_T25C089870C7A85A148516CEBFBAD3FFDC71FD208_H
#ifndef CAMELLIAENGINE_T0413457FBFB622FF3F4A2C8B318120ED0F566571_H
#define CAMELLIAENGINE_T0413457FBFB622FF3F4A2C8B318120ED0F566571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine
struct  CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::initialised
	bool ___initialised_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::_keyIs128
	bool ____keyIs128_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::subkey
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___subkey_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::kw
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___kw_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::ke
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___ke_5;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::state
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state_6;

public:
	inline static int32_t get_offset_of_initialised_0() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571, ___initialised_0)); }
	inline bool get_initialised_0() const { return ___initialised_0; }
	inline bool* get_address_of_initialised_0() { return &___initialised_0; }
	inline void set_initialised_0(bool value)
	{
		___initialised_0 = value;
	}

	inline static int32_t get_offset_of__keyIs128_1() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571, ____keyIs128_1)); }
	inline bool get__keyIs128_1() const { return ____keyIs128_1; }
	inline bool* get_address_of__keyIs128_1() { return &____keyIs128_1; }
	inline void set__keyIs128_1(bool value)
	{
		____keyIs128_1 = value;
	}

	inline static int32_t get_offset_of_subkey_3() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571, ___subkey_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_subkey_3() const { return ___subkey_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_subkey_3() { return &___subkey_3; }
	inline void set_subkey_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___subkey_3 = value;
		Il2CppCodeGenWriteBarrier((&___subkey_3), value);
	}

	inline static int32_t get_offset_of_kw_4() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571, ___kw_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_kw_4() const { return ___kw_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_kw_4() { return &___kw_4; }
	inline void set_kw_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___kw_4 = value;
		Il2CppCodeGenWriteBarrier((&___kw_4), value);
	}

	inline static int32_t get_offset_of_ke_5() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571, ___ke_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_ke_5() const { return ___ke_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_ke_5() { return &___ke_5; }
	inline void set_ke_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___ke_5 = value;
		Il2CppCodeGenWriteBarrier((&___ke_5), value);
	}

	inline static int32_t get_offset_of_state_6() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571, ___state_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_state_6() const { return ___state_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_state_6() { return &___state_6; }
	inline void set_state_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___state_6 = value;
		Il2CppCodeGenWriteBarrier((&___state_6), value);
	}
};

struct CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SIGMA
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SIGMA_7;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX1_1110
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX1_1110_8;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX4_4404
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX4_4404_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX2_0222
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX2_0222_10;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaEngine::SBOX3_3033
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SBOX3_3033_11;

public:
	inline static int32_t get_offset_of_SIGMA_7() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields, ___SIGMA_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SIGMA_7() const { return ___SIGMA_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SIGMA_7() { return &___SIGMA_7; }
	inline void set_SIGMA_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SIGMA_7 = value;
		Il2CppCodeGenWriteBarrier((&___SIGMA_7), value);
	}

	inline static int32_t get_offset_of_SBOX1_1110_8() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields, ___SBOX1_1110_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX1_1110_8() const { return ___SBOX1_1110_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX1_1110_8() { return &___SBOX1_1110_8; }
	inline void set_SBOX1_1110_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX1_1110_8 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX1_1110_8), value);
	}

	inline static int32_t get_offset_of_SBOX4_4404_9() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields, ___SBOX4_4404_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX4_4404_9() const { return ___SBOX4_4404_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX4_4404_9() { return &___SBOX4_4404_9; }
	inline void set_SBOX4_4404_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX4_4404_9 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX4_4404_9), value);
	}

	inline static int32_t get_offset_of_SBOX2_0222_10() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields, ___SBOX2_0222_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX2_0222_10() const { return ___SBOX2_0222_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX2_0222_10() { return &___SBOX2_0222_10; }
	inline void set_SBOX2_0222_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX2_0222_10 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX2_0222_10), value);
	}

	inline static int32_t get_offset_of_SBOX3_3033_11() { return static_cast<int32_t>(offsetof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields, ___SBOX3_3033_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SBOX3_3033_11() const { return ___SBOX3_3033_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SBOX3_3033_11() { return &___SBOX3_3033_11; }
	inline void set_SBOX3_3033_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SBOX3_3033_11 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX3_3033_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMELLIAENGINE_T0413457FBFB622FF3F4A2C8B318120ED0F566571_H
#ifndef CAMELLIALIGHTENGINE_T22B7A005A9C52F017AE98C6AE86662DB975D9A23_H
#define CAMELLIALIGHTENGINE_T22B7A005A9C52F017AE98C6AE86662DB975D9A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine
struct  CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::initialised
	bool ___initialised_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::_keyis128
	bool ____keyis128_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::subkey
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___subkey_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::kw
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___kw_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::ke
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___ke_5;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::state
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state_6;

public:
	inline static int32_t get_offset_of_initialised_1() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23, ___initialised_1)); }
	inline bool get_initialised_1() const { return ___initialised_1; }
	inline bool* get_address_of_initialised_1() { return &___initialised_1; }
	inline void set_initialised_1(bool value)
	{
		___initialised_1 = value;
	}

	inline static int32_t get_offset_of__keyis128_2() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23, ____keyis128_2)); }
	inline bool get__keyis128_2() const { return ____keyis128_2; }
	inline bool* get_address_of__keyis128_2() { return &____keyis128_2; }
	inline void set__keyis128_2(bool value)
	{
		____keyis128_2 = value;
	}

	inline static int32_t get_offset_of_subkey_3() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23, ___subkey_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_subkey_3() const { return ___subkey_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_subkey_3() { return &___subkey_3; }
	inline void set_subkey_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___subkey_3 = value;
		Il2CppCodeGenWriteBarrier((&___subkey_3), value);
	}

	inline static int32_t get_offset_of_kw_4() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23, ___kw_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_kw_4() const { return ___kw_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_kw_4() { return &___kw_4; }
	inline void set_kw_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___kw_4 = value;
		Il2CppCodeGenWriteBarrier((&___kw_4), value);
	}

	inline static int32_t get_offset_of_ke_5() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23, ___ke_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_ke_5() const { return ___ke_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_ke_5() { return &___ke_5; }
	inline void set_ke_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___ke_5 = value;
		Il2CppCodeGenWriteBarrier((&___ke_5), value);
	}

	inline static int32_t get_offset_of_state_6() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23, ___state_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_state_6() const { return ___state_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_state_6() { return &___state_6; }
	inline void set_state_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___state_6 = value;
		Il2CppCodeGenWriteBarrier((&___state_6), value);
	}
};

struct CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::SIGMA
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SIGMA_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaLightEngine::SBOX1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SBOX1_8;

public:
	inline static int32_t get_offset_of_SIGMA_7() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23_StaticFields, ___SIGMA_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SIGMA_7() const { return ___SIGMA_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SIGMA_7() { return &___SIGMA_7; }
	inline void set_SIGMA_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SIGMA_7 = value;
		Il2CppCodeGenWriteBarrier((&___SIGMA_7), value);
	}

	inline static int32_t get_offset_of_SBOX1_8() { return static_cast<int32_t>(offsetof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23_StaticFields, ___SBOX1_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SBOX1_8() const { return ___SBOX1_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SBOX1_8() { return &___SBOX1_8; }
	inline void set_SBOX1_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SBOX1_8 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMELLIALIGHTENGINE_T22B7A005A9C52F017AE98C6AE86662DB975D9A23_H
#ifndef CAST5ENGINE_TE9B00DAD2678F047F1B44D7FC25A430D4202DE79_H
#define CAST5ENGINE_TE9B00DAD2678F047F1B44D7FC25A430D4202DE79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine
struct  Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::_Kr
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____Kr_11;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::_Km
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____Km_12;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::_encrypting
	bool ____encrypting_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::_workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____workingKey_14;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::_rounds
	int32_t ____rounds_15;

public:
	inline static int32_t get_offset_of__Kr_11() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79, ____Kr_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__Kr_11() const { return ____Kr_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__Kr_11() { return &____Kr_11; }
	inline void set__Kr_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____Kr_11 = value;
		Il2CppCodeGenWriteBarrier((&____Kr_11), value);
	}

	inline static int32_t get_offset_of__Km_12() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79, ____Km_12)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__Km_12() const { return ____Km_12; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__Km_12() { return &____Km_12; }
	inline void set__Km_12(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____Km_12 = value;
		Il2CppCodeGenWriteBarrier((&____Km_12), value);
	}

	inline static int32_t get_offset_of__encrypting_13() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79, ____encrypting_13)); }
	inline bool get__encrypting_13() const { return ____encrypting_13; }
	inline bool* get_address_of__encrypting_13() { return &____encrypting_13; }
	inline void set__encrypting_13(bool value)
	{
		____encrypting_13 = value;
	}

	inline static int32_t get_offset_of__workingKey_14() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79, ____workingKey_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__workingKey_14() const { return ____workingKey_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__workingKey_14() { return &____workingKey_14; }
	inline void set__workingKey_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____workingKey_14 = value;
		Il2CppCodeGenWriteBarrier((&____workingKey_14), value);
	}

	inline static int32_t get_offset_of__rounds_15() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79, ____rounds_15)); }
	inline int32_t get__rounds_15() const { return ____rounds_15; }
	inline int32_t* get_address_of__rounds_15() { return &____rounds_15; }
	inline void set__rounds_15(int32_t value)
	{
		____rounds_15 = value;
	}
};

struct Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S1_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S2_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S3_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S4
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S4_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S5
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S5_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S6
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S6_5;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S7
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S7_6;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::S8
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S8_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::MAX_ROUNDS
	int32_t ___MAX_ROUNDS_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast5Engine::RED_ROUNDS
	int32_t ___RED_ROUNDS_9;

public:
	inline static int32_t get_offset_of_S1_0() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S1_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S1_0() const { return ___S1_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S1_0() { return &___S1_0; }
	inline void set_S1_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S1_0 = value;
		Il2CppCodeGenWriteBarrier((&___S1_0), value);
	}

	inline static int32_t get_offset_of_S2_1() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S2_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S2_1() const { return ___S2_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S2_1() { return &___S2_1; }
	inline void set_S2_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S2_1 = value;
		Il2CppCodeGenWriteBarrier((&___S2_1), value);
	}

	inline static int32_t get_offset_of_S3_2() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S3_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S3_2() const { return ___S3_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S3_2() { return &___S3_2; }
	inline void set_S3_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S3_2 = value;
		Il2CppCodeGenWriteBarrier((&___S3_2), value);
	}

	inline static int32_t get_offset_of_S4_3() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S4_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S4_3() const { return ___S4_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S4_3() { return &___S4_3; }
	inline void set_S4_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S4_3 = value;
		Il2CppCodeGenWriteBarrier((&___S4_3), value);
	}

	inline static int32_t get_offset_of_S5_4() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S5_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S5_4() const { return ___S5_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S5_4() { return &___S5_4; }
	inline void set_S5_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S5_4 = value;
		Il2CppCodeGenWriteBarrier((&___S5_4), value);
	}

	inline static int32_t get_offset_of_S6_5() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S6_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S6_5() const { return ___S6_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S6_5() { return &___S6_5; }
	inline void set_S6_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S6_5 = value;
		Il2CppCodeGenWriteBarrier((&___S6_5), value);
	}

	inline static int32_t get_offset_of_S7_6() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S7_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S7_6() const { return ___S7_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S7_6() { return &___S7_6; }
	inline void set_S7_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S7_6 = value;
		Il2CppCodeGenWriteBarrier((&___S7_6), value);
	}

	inline static int32_t get_offset_of_S8_7() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___S8_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S8_7() const { return ___S8_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S8_7() { return &___S8_7; }
	inline void set_S8_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S8_7 = value;
		Il2CppCodeGenWriteBarrier((&___S8_7), value);
	}

	inline static int32_t get_offset_of_MAX_ROUNDS_8() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___MAX_ROUNDS_8)); }
	inline int32_t get_MAX_ROUNDS_8() const { return ___MAX_ROUNDS_8; }
	inline int32_t* get_address_of_MAX_ROUNDS_8() { return &___MAX_ROUNDS_8; }
	inline void set_MAX_ROUNDS_8(int32_t value)
	{
		___MAX_ROUNDS_8 = value;
	}

	inline static int32_t get_offset_of_RED_ROUNDS_9() { return static_cast<int32_t>(offsetof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields, ___RED_ROUNDS_9)); }
	inline int32_t get_RED_ROUNDS_9() const { return ___RED_ROUNDS_9; }
	inline int32_t* get_address_of_RED_ROUNDS_9() { return &___RED_ROUNDS_9; }
	inline void set_RED_ROUNDS_9(int32_t value)
	{
		___RED_ROUNDS_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAST5ENGINE_TE9B00DAD2678F047F1B44D7FC25A430D4202DE79_H
#ifndef DESEDEWRAPENGINE_T6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_H
#define DESEDEWRAPENGINE_T6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine
struct  DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::engine
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * ___engine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::param
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___param_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::paramPlusIV
	ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * ___paramPlusIV_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::forWrapping
	bool ___forWrapping_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::sha1
	RuntimeObject* ___sha1_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_7;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___engine_0)); }
	inline CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * get_engine_0() const { return ___engine_0; }
	inline CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F ** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_param_1() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___param_1)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_param_1() const { return ___param_1; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_param_1() { return &___param_1; }
	inline void set_param_1(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___param_1 = value;
		Il2CppCodeGenWriteBarrier((&___param_1), value);
	}

	inline static int32_t get_offset_of_paramPlusIV_2() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___paramPlusIV_2)); }
	inline ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * get_paramPlusIV_2() const { return ___paramPlusIV_2; }
	inline ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF ** get_address_of_paramPlusIV_2() { return &___paramPlusIV_2; }
	inline void set_paramPlusIV_2(ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * value)
	{
		___paramPlusIV_2 = value;
		Il2CppCodeGenWriteBarrier((&___paramPlusIV_2), value);
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___iv_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_3() const { return ___iv_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}

	inline static int32_t get_offset_of_forWrapping_4() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___forWrapping_4)); }
	inline bool get_forWrapping_4() const { return ___forWrapping_4; }
	inline bool* get_address_of_forWrapping_4() { return &___forWrapping_4; }
	inline void set_forWrapping_4(bool value)
	{
		___forWrapping_4 = value;
	}

	inline static int32_t get_offset_of_sha1_6() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___sha1_6)); }
	inline RuntimeObject* get_sha1_6() const { return ___sha1_6; }
	inline RuntimeObject** get_address_of_sha1_6() { return &___sha1_6; }
	inline void set_sha1_6(RuntimeObject* value)
	{
		___sha1_6 = value;
		Il2CppCodeGenWriteBarrier((&___sha1_6), value);
	}

	inline static int32_t get_offset_of_digest_7() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3, ___digest_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_7() const { return ___digest_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_7() { return &___digest_7; }
	inline void set_digest_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_7 = value;
		Il2CppCodeGenWriteBarrier((&___digest_7), value);
	}
};

struct DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeWrapEngine::IV2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV2_5;

public:
	inline static int32_t get_offset_of_IV2_5() { return static_cast<int32_t>(offsetof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_StaticFields, ___IV2_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV2_5() const { return ___IV2_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV2_5() { return &___IV2_5; }
	inline void set_IV2_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV2_5 = value;
		Il2CppCodeGenWriteBarrier((&___IV2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESEDEWRAPENGINE_T6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_H
#ifndef DESENGINE_T452535648DB7C5A7F34799A278A6BDA120C7ADA3_H
#define DESENGINE_T452535648DB7C5A7F34799A278A6BDA120C7ADA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine
struct  DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_1;

public:
	inline static int32_t get_offset_of_workingKey_1() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3, ___workingKey_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_1() const { return ___workingKey_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_1() { return &___workingKey_1; }
	inline void set_workingKey_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_1), value);
	}
};

struct DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::bytebit
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bytebit_2;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::bigbyte
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bigbyte_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::pc1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pc1_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::totrot
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___totrot_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::pc2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pc2_6;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP1_7;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP2_8;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP3_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP4
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP4_10;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP5
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP5_11;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP6
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP6_12;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP7
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP7_13;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEngine::SP8
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SP8_14;

public:
	inline static int32_t get_offset_of_bytebit_2() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___bytebit_2)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bytebit_2() const { return ___bytebit_2; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bytebit_2() { return &___bytebit_2; }
	inline void set_bytebit_2(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bytebit_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytebit_2), value);
	}

	inline static int32_t get_offset_of_bigbyte_3() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___bigbyte_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bigbyte_3() const { return ___bigbyte_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bigbyte_3() { return &___bigbyte_3; }
	inline void set_bigbyte_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bigbyte_3 = value;
		Il2CppCodeGenWriteBarrier((&___bigbyte_3), value);
	}

	inline static int32_t get_offset_of_pc1_4() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___pc1_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pc1_4() const { return ___pc1_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pc1_4() { return &___pc1_4; }
	inline void set_pc1_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pc1_4 = value;
		Il2CppCodeGenWriteBarrier((&___pc1_4), value);
	}

	inline static int32_t get_offset_of_totrot_5() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___totrot_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_totrot_5() const { return ___totrot_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_totrot_5() { return &___totrot_5; }
	inline void set_totrot_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___totrot_5 = value;
		Il2CppCodeGenWriteBarrier((&___totrot_5), value);
	}

	inline static int32_t get_offset_of_pc2_6() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___pc2_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pc2_6() const { return ___pc2_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pc2_6() { return &___pc2_6; }
	inline void set_pc2_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pc2_6 = value;
		Il2CppCodeGenWriteBarrier((&___pc2_6), value);
	}

	inline static int32_t get_offset_of_SP1_7() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP1_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP1_7() const { return ___SP1_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP1_7() { return &___SP1_7; }
	inline void set_SP1_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP1_7 = value;
		Il2CppCodeGenWriteBarrier((&___SP1_7), value);
	}

	inline static int32_t get_offset_of_SP2_8() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP2_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP2_8() const { return ___SP2_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP2_8() { return &___SP2_8; }
	inline void set_SP2_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP2_8 = value;
		Il2CppCodeGenWriteBarrier((&___SP2_8), value);
	}

	inline static int32_t get_offset_of_SP3_9() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP3_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP3_9() const { return ___SP3_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP3_9() { return &___SP3_9; }
	inline void set_SP3_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP3_9 = value;
		Il2CppCodeGenWriteBarrier((&___SP3_9), value);
	}

	inline static int32_t get_offset_of_SP4_10() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP4_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP4_10() const { return ___SP4_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP4_10() { return &___SP4_10; }
	inline void set_SP4_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP4_10 = value;
		Il2CppCodeGenWriteBarrier((&___SP4_10), value);
	}

	inline static int32_t get_offset_of_SP5_11() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP5_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP5_11() const { return ___SP5_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP5_11() { return &___SP5_11; }
	inline void set_SP5_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP5_11 = value;
		Il2CppCodeGenWriteBarrier((&___SP5_11), value);
	}

	inline static int32_t get_offset_of_SP6_12() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP6_12)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP6_12() const { return ___SP6_12; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP6_12() { return &___SP6_12; }
	inline void set_SP6_12(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP6_12 = value;
		Il2CppCodeGenWriteBarrier((&___SP6_12), value);
	}

	inline static int32_t get_offset_of_SP7_13() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP7_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP7_13() const { return ___SP7_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP7_13() { return &___SP7_13; }
	inline void set_SP7_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP7_13 = value;
		Il2CppCodeGenWriteBarrier((&___SP7_13), value);
	}

	inline static int32_t get_offset_of_SP8_14() { return static_cast<int32_t>(offsetof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields, ___SP8_14)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SP8_14() const { return ___SP8_14; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SP8_14() { return &___SP8_14; }
	inline void set_SP8_14(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SP8_14 = value;
		Il2CppCodeGenWriteBarrier((&___SP8_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESENGINE_T452535648DB7C5A7F34799A278A6BDA120C7ADA3_H
#ifndef RFC3394WRAPENGINE_T8255799FEF932C0736E276CD77596F43027A454B_H
#define RFC3394WRAPENGINE_T8255799FEF932C0736E276CD77596F43027A454B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine
struct  Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::engine
	RuntimeObject* ___engine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::param
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___param_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::forWrapping
	bool ___forWrapping_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_3;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___engine_0)); }
	inline RuntimeObject* get_engine_0() const { return ___engine_0; }
	inline RuntimeObject** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(RuntimeObject* value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_param_1() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___param_1)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_param_1() const { return ___param_1; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_param_1() { return &___param_1; }
	inline void set_param_1(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___param_1 = value;
		Il2CppCodeGenWriteBarrier((&___param_1), value);
	}

	inline static int32_t get_offset_of_forWrapping_2() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___forWrapping_2)); }
	inline bool get_forWrapping_2() const { return ___forWrapping_2; }
	inline bool* get_address_of_forWrapping_2() { return &___forWrapping_2; }
	inline void set_forWrapping_2(bool value)
	{
		___forWrapping_2 = value;
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___iv_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_3() const { return ___iv_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC3394WRAPENGINE_T8255799FEF932C0736E276CD77596F43027A454B_H
#ifndef SALSA20ENGINE_T521B446CFC278026CC81B6017B3797C847F42C69_H
#define SALSA20ENGINE_T521B446CFC278026CC81B6017B3797C847F42C69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine
struct  Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::rounds
	int32_t ___rounds_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::index
	int32_t ___index_6;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::engineState
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___engineState_7;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::keyStream
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyStream_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::initialised
	bool ___initialised_10;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW0
	uint32_t ___cW0_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW1
	uint32_t ___cW1_12;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW2
	uint32_t ___cW2_13;

public:
	inline static int32_t get_offset_of_rounds_5() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___rounds_5)); }
	inline int32_t get_rounds_5() const { return ___rounds_5; }
	inline int32_t* get_address_of_rounds_5() { return &___rounds_5; }
	inline void set_rounds_5(int32_t value)
	{
		___rounds_5 = value;
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}

	inline static int32_t get_offset_of_engineState_7() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___engineState_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_engineState_7() const { return ___engineState_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_engineState_7() { return &___engineState_7; }
	inline void set_engineState_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___engineState_7 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_7), value);
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___x_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_8() const { return ___x_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_8 = value;
		Il2CppCodeGenWriteBarrier((&___x_8), value);
	}

	inline static int32_t get_offset_of_keyStream_9() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___keyStream_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyStream_9() const { return ___keyStream_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyStream_9() { return &___keyStream_9; }
	inline void set_keyStream_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyStream_9 = value;
		Il2CppCodeGenWriteBarrier((&___keyStream_9), value);
	}

	inline static int32_t get_offset_of_initialised_10() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___initialised_10)); }
	inline bool get_initialised_10() const { return ___initialised_10; }
	inline bool* get_address_of_initialised_10() { return &___initialised_10; }
	inline void set_initialised_10(bool value)
	{
		___initialised_10 = value;
	}

	inline static int32_t get_offset_of_cW0_11() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___cW0_11)); }
	inline uint32_t get_cW0_11() const { return ___cW0_11; }
	inline uint32_t* get_address_of_cW0_11() { return &___cW0_11; }
	inline void set_cW0_11(uint32_t value)
	{
		___cW0_11 = value;
	}

	inline static int32_t get_offset_of_cW1_12() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___cW1_12)); }
	inline uint32_t get_cW1_12() const { return ___cW1_12; }
	inline uint32_t* get_address_of_cW1_12() { return &___cW1_12; }
	inline void set_cW1_12(uint32_t value)
	{
		___cW1_12 = value;
	}

	inline static int32_t get_offset_of_cW2_13() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___cW2_13)); }
	inline uint32_t get_cW2_13() const { return ___cW2_13; }
	inline uint32_t* get_address_of_cW2_13() { return &___cW2_13; }
	inline void set_cW2_13(uint32_t value)
	{
		___cW2_13 = value;
	}
};

struct Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::DEFAULT_ROUNDS
	int32_t ___DEFAULT_ROUNDS_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::TAU_SIGMA
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___TAU_SIGMA_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::sigma
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sigma_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::tau
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tau_4;

public:
	inline static int32_t get_offset_of_DEFAULT_ROUNDS_0() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___DEFAULT_ROUNDS_0)); }
	inline int32_t get_DEFAULT_ROUNDS_0() const { return ___DEFAULT_ROUNDS_0; }
	inline int32_t* get_address_of_DEFAULT_ROUNDS_0() { return &___DEFAULT_ROUNDS_0; }
	inline void set_DEFAULT_ROUNDS_0(int32_t value)
	{
		___DEFAULT_ROUNDS_0 = value;
	}

	inline static int32_t get_offset_of_TAU_SIGMA_2() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___TAU_SIGMA_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_TAU_SIGMA_2() const { return ___TAU_SIGMA_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_TAU_SIGMA_2() { return &___TAU_SIGMA_2; }
	inline void set_TAU_SIGMA_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___TAU_SIGMA_2 = value;
		Il2CppCodeGenWriteBarrier((&___TAU_SIGMA_2), value);
	}

	inline static int32_t get_offset_of_sigma_3() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___sigma_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sigma_3() const { return ___sigma_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sigma_3() { return &___sigma_3; }
	inline void set_sigma_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sigma_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigma_3), value);
	}

	inline static int32_t get_offset_of_tau_4() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___tau_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tau_4() const { return ___tau_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tau_4() { return &___tau_4; }
	inline void set_tau_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tau_4 = value;
		Il2CppCodeGenWriteBarrier((&___tau_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALSA20ENGINE_T521B446CFC278026CC81B6017B3797C847F42C69_H
#ifndef BCRYPT_T7E5B1A375859E3A4E470BF5319421D67311D6594_H
#define BCRYPT_T7E5B1A375859E3A4E470BF5319421D67311D6594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt
struct  BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::S
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___S_12;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_13;

public:
	inline static int32_t get_offset_of_S_12() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594, ___S_12)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_S_12() const { return ___S_12; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_S_12() { return &___S_12; }
	inline void set_S_12(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___S_12 = value;
		Il2CppCodeGenWriteBarrier((&___S_12), value);
	}

	inline static int32_t get_offset_of_P_13() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594, ___P_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_13() const { return ___P_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_13() { return &___P_13; }
	inline void set_P_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_13 = value;
		Il2CppCodeGenWriteBarrier((&___P_13), value);
	}
};

struct BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::MAGIC_STRING
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___MAGIC_STRING_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::KP
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KP_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::KS0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS0_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::KS1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS1_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::KS2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS2_5;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BCrypt::KS3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KS3_6;

public:
	inline static int32_t get_offset_of_MAGIC_STRING_0() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields, ___MAGIC_STRING_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_MAGIC_STRING_0() const { return ___MAGIC_STRING_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_MAGIC_STRING_0() { return &___MAGIC_STRING_0; }
	inline void set_MAGIC_STRING_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___MAGIC_STRING_0 = value;
		Il2CppCodeGenWriteBarrier((&___MAGIC_STRING_0), value);
	}

	inline static int32_t get_offset_of_KP_2() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields, ___KP_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KP_2() const { return ___KP_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KP_2() { return &___KP_2; }
	inline void set_KP_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KP_2 = value;
		Il2CppCodeGenWriteBarrier((&___KP_2), value);
	}

	inline static int32_t get_offset_of_KS0_3() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields, ___KS0_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS0_3() const { return ___KS0_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS0_3() { return &___KS0_3; }
	inline void set_KS0_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS0_3 = value;
		Il2CppCodeGenWriteBarrier((&___KS0_3), value);
	}

	inline static int32_t get_offset_of_KS1_4() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields, ___KS1_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS1_4() const { return ___KS1_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS1_4() { return &___KS1_4; }
	inline void set_KS1_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS1_4 = value;
		Il2CppCodeGenWriteBarrier((&___KS1_4), value);
	}

	inline static int32_t get_offset_of_KS2_5() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields, ___KS2_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS2_5() const { return ___KS2_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS2_5() { return &___KS2_5; }
	inline void set_KS2_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS2_5 = value;
		Il2CppCodeGenWriteBarrier((&___KS2_5), value);
	}

	inline static int32_t get_offset_of_KS3_6() { return static_cast<int32_t>(offsetof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields, ___KS3_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KS3_6() const { return ___KS3_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KS3_6() { return &___KS3_6; }
	inline void set_KS3_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KS3_6 = value;
		Il2CppCodeGenWriteBarrier((&___KS3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BCRYPT_T7E5B1A375859E3A4E470BF5319421D67311D6594_H
#ifndef BASEKDFBYTESGENERATOR_T7DD462075FDA679816049B99D7935341B0B73B69_H
#define BASEKDFBYTESGENERATOR_T7DD462075FDA679816049B99D7935341B0B73B69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BaseKdfBytesGenerator
struct  BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BaseKdfBytesGenerator::counterStart
	int32_t ___counterStart_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BaseKdfBytesGenerator::digest
	RuntimeObject* ___digest_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BaseKdfBytesGenerator::shared
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___shared_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.BaseKdfBytesGenerator::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_3;

public:
	inline static int32_t get_offset_of_counterStart_0() { return static_cast<int32_t>(offsetof(BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69, ___counterStart_0)); }
	inline int32_t get_counterStart_0() const { return ___counterStart_0; }
	inline int32_t* get_address_of_counterStart_0() { return &___counterStart_0; }
	inline void set_counterStart_0(int32_t value)
	{
		___counterStart_0 = value;
	}

	inline static int32_t get_offset_of_digest_1() { return static_cast<int32_t>(offsetof(BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69, ___digest_1)); }
	inline RuntimeObject* get_digest_1() const { return ___digest_1; }
	inline RuntimeObject** get_address_of_digest_1() { return &___digest_1; }
	inline void set_digest_1(RuntimeObject* value)
	{
		___digest_1 = value;
		Il2CppCodeGenWriteBarrier((&___digest_1), value);
	}

	inline static int32_t get_offset_of_shared_2() { return static_cast<int32_t>(offsetof(BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69, ___shared_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_shared_2() const { return ___shared_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_shared_2() { return &___shared_2; }
	inline void set_shared_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___shared_2 = value;
		Il2CppCodeGenWriteBarrier((&___shared_2), value);
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69, ___iv_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_3() const { return ___iv_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEKDFBYTESGENERATOR_T7DD462075FDA679816049B99D7935341B0B73B69_H
#ifndef DHBASICKEYPAIRGENERATOR_T2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F_H
#define DHBASICKEYPAIRGENERATOR_T2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHBasicKeyPairGenerator
struct  DHBasicKeyPairGenerator_t2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHBasicKeyPairGenerator::param
	DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 * ___param_0;

public:
	inline static int32_t get_offset_of_param_0() { return static_cast<int32_t>(offsetof(DHBasicKeyPairGenerator_t2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F, ___param_0)); }
	inline DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 * get_param_0() const { return ___param_0; }
	inline DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 ** get_address_of_param_0() { return &___param_0; }
	inline void set_param_0(DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 * value)
	{
		___param_0 = value;
		Il2CppCodeGenWriteBarrier((&___param_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHBASICKEYPAIRGENERATOR_T2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F_H
#ifndef DHKEYGENERATORHELPER_T1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_H
#define DHKEYGENERATORHELPER_T1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHKeyGeneratorHelper
struct  DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E  : public RuntimeObject
{
public:

public:
};

struct DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHKeyGeneratorHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHKeyGeneratorHelper::Instance
	DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_StaticFields, ___Instance_0)); }
	inline DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E * get_Instance_0() const { return ___Instance_0; }
	inline DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYGENERATORHELPER_T1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_H
#ifndef DHKEYPAIRGENERATOR_TEA77957666E1206C449FB24F859215E2AC1CDC40_H
#define DHKEYPAIRGENERATOR_TEA77957666E1206C449FB24F859215E2AC1CDC40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHKeyPairGenerator
struct  DHKeyPairGenerator_tEA77957666E1206C449FB24F859215E2AC1CDC40  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHKeyPairGenerator::param
	DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 * ___param_0;

public:
	inline static int32_t get_offset_of_param_0() { return static_cast<int32_t>(offsetof(DHKeyPairGenerator_tEA77957666E1206C449FB24F859215E2AC1CDC40, ___param_0)); }
	inline DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 * get_param_0() const { return ___param_0; }
	inline DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 ** get_address_of_param_0() { return &___param_0; }
	inline void set_param_0(DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960 * value)
	{
		___param_0 = value;
		Il2CppCodeGenWriteBarrier((&___param_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYPAIRGENERATOR_TEA77957666E1206C449FB24F859215E2AC1CDC40_H
#ifndef DHPARAMETERSGENERATOR_T8D80E260053A815C62C85CA4387FEB74A443E4BF_H
#define DHPARAMETERSGENERATOR_T8D80E260053A815C62C85CA4387FEB74A443E4BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersGenerator
struct  DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersGenerator::size
	int32_t ___size_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersGenerator::certainty
	int32_t ___certainty_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_2;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_certainty_1() { return static_cast<int32_t>(offsetof(DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF, ___certainty_1)); }
	inline int32_t get_certainty_1() const { return ___certainty_1; }
	inline int32_t* get_address_of_certainty_1() { return &___certainty_1; }
	inline void set_certainty_1(int32_t value)
	{
		___certainty_1 = value;
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF, ___random_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPARAMETERSGENERATOR_T8D80E260053A815C62C85CA4387FEB74A443E4BF_H
#ifndef DHPARAMETERSHELPER_TBE454481990F499CFB42254E2F2322CBBEB0B193_H
#define DHPARAMETERSHELPER_TBE454481990F499CFB42254E2F2322CBBEB0B193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersHelper
struct  DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193  : public RuntimeObject
{
public:

public:
};

struct DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersHelper::Six
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Six_0;
	// System.Int32[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersHelper::primeLists
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___primeLists_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersHelper::primeProducts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___primeProducts_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DHParametersHelper::BigPrimeProducts
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___BigPrimeProducts_3;

public:
	inline static int32_t get_offset_of_Six_0() { return static_cast<int32_t>(offsetof(DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields, ___Six_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Six_0() const { return ___Six_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Six_0() { return &___Six_0; }
	inline void set_Six_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Six_0 = value;
		Il2CppCodeGenWriteBarrier((&___Six_0), value);
	}

	inline static int32_t get_offset_of_primeLists_1() { return static_cast<int32_t>(offsetof(DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields, ___primeLists_1)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_primeLists_1() const { return ___primeLists_1; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_primeLists_1() { return &___primeLists_1; }
	inline void set_primeLists_1(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___primeLists_1 = value;
		Il2CppCodeGenWriteBarrier((&___primeLists_1), value);
	}

	inline static int32_t get_offset_of_primeProducts_2() { return static_cast<int32_t>(offsetof(DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields, ___primeProducts_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_primeProducts_2() const { return ___primeProducts_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_primeProducts_2() { return &___primeProducts_2; }
	inline void set_primeProducts_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___primeProducts_2 = value;
		Il2CppCodeGenWriteBarrier((&___primeProducts_2), value);
	}

	inline static int32_t get_offset_of_BigPrimeProducts_3() { return static_cast<int32_t>(offsetof(DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields, ___BigPrimeProducts_3)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_BigPrimeProducts_3() const { return ___BigPrimeProducts_3; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_BigPrimeProducts_3() { return &___BigPrimeProducts_3; }
	inline void set_BigPrimeProducts_3(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___BigPrimeProducts_3 = value;
		Il2CppCodeGenWriteBarrier((&___BigPrimeProducts_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPARAMETERSHELPER_TBE454481990F499CFB42254E2F2322CBBEB0B193_H
#ifndef DSAKEYPAIRGENERATOR_T1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_H
#define DSAKEYPAIRGENERATOR_T1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaKeyPairGenerator
struct  DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaKeyPairGenerator::param
	DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707 * ___param_1;

public:
	inline static int32_t get_offset_of_param_1() { return static_cast<int32_t>(offsetof(DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1, ___param_1)); }
	inline DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707 * get_param_1() const { return ___param_1; }
	inline DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707 ** get_address_of_param_1() { return &___param_1; }
	inline void set_param_1(DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707 * value)
	{
		___param_1 = value;
		Il2CppCodeGenWriteBarrier((&___param_1), value);
	}
};

struct DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaKeyPairGenerator::One
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___One_0;

public:
	inline static int32_t get_offset_of_One_0() { return static_cast<int32_t>(offsetof(DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_StaticFields, ___One_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_One_0() const { return ___One_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_One_0() { return &___One_0; }
	inline void set_One_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___One_0 = value;
		Il2CppCodeGenWriteBarrier((&___One_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAKEYPAIRGENERATOR_T1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_H
#ifndef DSAPARAMETERSGENERATOR_T8802061036F845CC0F4F01EB6BA2E8C6F2225B30_H
#define DSAPARAMETERSGENERATOR_T8802061036F845CC0F4F01EB6BA2E8C6F2225B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator
struct  DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::digest
	RuntimeObject* ___digest_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::L
	int32_t ___L_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::N
	int32_t ___N_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::certainty
	int32_t ___certainty_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::use186_3
	bool ___use186_3_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DsaParametersGenerator::usageIndex
	int32_t ___usageIndex_6;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___digest_0)); }
	inline RuntimeObject* get_digest_0() const { return ___digest_0; }
	inline RuntimeObject** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(RuntimeObject* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_L_1() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___L_1)); }
	inline int32_t get_L_1() const { return ___L_1; }
	inline int32_t* get_address_of_L_1() { return &___L_1; }
	inline void set_L_1(int32_t value)
	{
		___L_1 = value;
	}

	inline static int32_t get_offset_of_N_2() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___N_2)); }
	inline int32_t get_N_2() const { return ___N_2; }
	inline int32_t* get_address_of_N_2() { return &___N_2; }
	inline void set_N_2(int32_t value)
	{
		___N_2 = value;
	}

	inline static int32_t get_offset_of_certainty_3() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___certainty_3)); }
	inline int32_t get_certainty_3() const { return ___certainty_3; }
	inline int32_t* get_address_of_certainty_3() { return &___certainty_3; }
	inline void set_certainty_3(int32_t value)
	{
		___certainty_3 = value;
	}

	inline static int32_t get_offset_of_random_4() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___random_4)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_4() const { return ___random_4; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_4() { return &___random_4; }
	inline void set_random_4(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_4 = value;
		Il2CppCodeGenWriteBarrier((&___random_4), value);
	}

	inline static int32_t get_offset_of_use186_3_5() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___use186_3_5)); }
	inline bool get_use186_3_5() const { return ___use186_3_5; }
	inline bool* get_address_of_use186_3_5() { return &___use186_3_5; }
	inline void set_use186_3_5(bool value)
	{
		___use186_3_5 = value;
	}

	inline static int32_t get_offset_of_usageIndex_6() { return static_cast<int32_t>(offsetof(DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30, ___usageIndex_6)); }
	inline int32_t get_usageIndex_6() const { return ___usageIndex_6; }
	inline int32_t* get_address_of_usageIndex_6() { return &___usageIndex_6; }
	inline void set_usageIndex_6(int32_t value)
	{
		___usageIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPARAMETERSGENERATOR_T8802061036F845CC0F4F01EB6BA2E8C6F2225B30_H
#ifndef ECKEYPAIRGENERATOR_T4B867B9C0AC747F88F69D7C583AE01273733A1F2_H
#define ECKEYPAIRGENERATOR_T4B867B9C0AC747F88F69D7C583AE01273733A1F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator
struct  ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::algorithm
	String_t* ___algorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::parameters
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * ___parameters_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___publicKeyParamSet_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2, ___algorithm_0)); }
	inline String_t* get_algorithm_0() const { return ___algorithm_0; }
	inline String_t** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(String_t* value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2, ___parameters_1)); }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * get_parameters_1() const { return ___parameters_1; }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_2() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2, ___publicKeyParamSet_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_publicKeyParamSet_2() const { return ___publicKeyParamSet_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_publicKeyParamSet_2() { return &___publicKeyParamSet_2; }
	inline void set_publicKeyParamSet_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___publicKeyParamSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECKEYPAIRGENERATOR_T4B867B9C0AC747F88F69D7C583AE01273733A1F2_H
#ifndef ED25519KEYPAIRGENERATOR_T4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C_H
#define ED25519KEYPAIRGENERATOR_T4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Ed25519KeyPairGenerator
struct  Ed25519KeyPairGenerator_t4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Ed25519KeyPairGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(Ed25519KeyPairGenerator_t4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519KEYPAIRGENERATOR_T4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C_H
#ifndef ED448KEYPAIRGENERATOR_T17F93BB097D0EBB7FCEEB12E158E160F9F7D8996_H
#define ED448KEYPAIRGENERATOR_T17F93BB097D0EBB7FCEEB12E158E160F9F7D8996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Ed448KeyPairGenerator
struct  Ed448KeyPairGenerator_t17F93BB097D0EBB7FCEEB12E158E160F9F7D8996  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Ed448KeyPairGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(Ed448KeyPairGenerator_t17F93BB097D0EBB7FCEEB12E158E160F9F7D8996, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448KEYPAIRGENERATOR_T17F93BB097D0EBB7FCEEB12E158E160F9F7D8996_H
#ifndef ELGAMALKEYPAIRGENERATOR_T3EA05DEEAB701DDCEA24D3B29546206DDDA176C3_H
#define ELGAMALKEYPAIRGENERATOR_T3EA05DEEAB701DDCEA24D3B29546206DDDA176C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ElGamalKeyPairGenerator
struct  ElGamalKeyPairGenerator_t3EA05DEEAB701DDCEA24D3B29546206DDDA176C3  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ElGamalKeyPairGenerator::param
	ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0 * ___param_0;

public:
	inline static int32_t get_offset_of_param_0() { return static_cast<int32_t>(offsetof(ElGamalKeyPairGenerator_t3EA05DEEAB701DDCEA24D3B29546206DDDA176C3, ___param_0)); }
	inline ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0 * get_param_0() const { return ___param_0; }
	inline ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0 ** get_address_of_param_0() { return &___param_0; }
	inline void set_param_0(ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0 * value)
	{
		___param_0 = value;
		Il2CppCodeGenWriteBarrier((&___param_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALKEYPAIRGENERATOR_T3EA05DEEAB701DDCEA24D3B29546206DDDA176C3_H
#ifndef ELGAMALPARAMETERSGENERATOR_TFD91A3674A3141C952AE1BF5D4BD27591E684FED_H
#define ELGAMALPARAMETERSGENERATOR_TFD91A3674A3141C952AE1BF5D4BD27591E684FED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ElGamalParametersGenerator
struct  ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ElGamalParametersGenerator::size
	int32_t ___size_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ElGamalParametersGenerator::certainty
	int32_t ___certainty_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.ElGamalParametersGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_2;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_certainty_1() { return static_cast<int32_t>(offsetof(ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED, ___certainty_1)); }
	inline int32_t get_certainty_1() const { return ___certainty_1; }
	inline int32_t* get_address_of_certainty_1() { return &___certainty_1; }
	inline void set_certainty_1(int32_t value)
	{
		___certainty_1 = value;
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED, ___random_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPARAMETERSGENERATOR_TFD91A3674A3141C952AE1BF5D4BD27591E684FED_H
#ifndef GOST3410KEYPAIRGENERATOR_T1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2_H
#define GOST3410KEYPAIRGENERATOR_T1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Gost3410KeyPairGenerator
struct  Gost3410KeyPairGenerator_t1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Gost3410KeyPairGenerator::param
	Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8 * ___param_0;

public:
	inline static int32_t get_offset_of_param_0() { return static_cast<int32_t>(offsetof(Gost3410KeyPairGenerator_t1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2, ___param_0)); }
	inline Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8 * get_param_0() const { return ___param_0; }
	inline Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8 ** get_address_of_param_0() { return &___param_0; }
	inline void set_param_0(Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8 * value)
	{
		___param_0 = value;
		Il2CppCodeGenWriteBarrier((&___param_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410KEYPAIRGENERATOR_T1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2_H
#ifndef GOST3410PARAMETERSGENERATOR_T9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9_H
#define GOST3410PARAMETERSGENERATOR_T9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Gost3410ParametersGenerator
struct  Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Gost3410ParametersGenerator::size
	int32_t ___size_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Gost3410ParametersGenerator::typeproc
	int32_t ___typeproc_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Gost3410ParametersGenerator::init_random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___init_random_2;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_typeproc_1() { return static_cast<int32_t>(offsetof(Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9, ___typeproc_1)); }
	inline int32_t get_typeproc_1() const { return ___typeproc_1; }
	inline int32_t* get_address_of_typeproc_1() { return &___typeproc_1; }
	inline void set_typeproc_1(int32_t value)
	{
		___typeproc_1 = value;
	}

	inline static int32_t get_offset_of_init_random_2() { return static_cast<int32_t>(offsetof(Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9, ___init_random_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_init_random_2() const { return ___init_random_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_init_random_2() { return &___init_random_2; }
	inline void set_init_random_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___init_random_2 = value;
		Il2CppCodeGenWriteBarrier((&___init_random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PARAMETERSGENERATOR_T9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9_H
#ifndef HKDFBYTESGENERATOR_T0FFEFF9305285BFEC5321B15A80BB0BEE1992D14_H
#define HKDFBYTESGENERATOR_T0FFEFF9305285BFEC5321B15A80BB0BEE1992D14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.HkdfBytesGenerator
struct  HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.HkdfBytesGenerator::hMacHash
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 * ___hMacHash_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.HkdfBytesGenerator::hashLen
	int32_t ___hashLen_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.HkdfBytesGenerator::info
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___info_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.HkdfBytesGenerator::currentT
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___currentT_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.HkdfBytesGenerator::generatedBytes
	int32_t ___generatedBytes_4;

public:
	inline static int32_t get_offset_of_hMacHash_0() { return static_cast<int32_t>(offsetof(HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14, ___hMacHash_0)); }
	inline HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 * get_hMacHash_0() const { return ___hMacHash_0; }
	inline HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 ** get_address_of_hMacHash_0() { return &___hMacHash_0; }
	inline void set_hMacHash_0(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 * value)
	{
		___hMacHash_0 = value;
		Il2CppCodeGenWriteBarrier((&___hMacHash_0), value);
	}

	inline static int32_t get_offset_of_hashLen_1() { return static_cast<int32_t>(offsetof(HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14, ___hashLen_1)); }
	inline int32_t get_hashLen_1() const { return ___hashLen_1; }
	inline int32_t* get_address_of_hashLen_1() { return &___hashLen_1; }
	inline void set_hashLen_1(int32_t value)
	{
		___hashLen_1 = value;
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14, ___info_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_info_2() const { return ___info_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}

	inline static int32_t get_offset_of_currentT_3() { return static_cast<int32_t>(offsetof(HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14, ___currentT_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_currentT_3() const { return ___currentT_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_currentT_3() { return &___currentT_3; }
	inline void set_currentT_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___currentT_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentT_3), value);
	}

	inline static int32_t get_offset_of_generatedBytes_4() { return static_cast<int32_t>(offsetof(HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14, ___generatedBytes_4)); }
	inline int32_t get_generatedBytes_4() const { return ___generatedBytes_4; }
	inline int32_t* get_address_of_generatedBytes_4() { return &___generatedBytes_4; }
	inline void set_generatedBytes_4(int32_t value)
	{
		___generatedBytes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HKDFBYTESGENERATOR_T0FFEFF9305285BFEC5321B15A80BB0BEE1992D14_H
#ifndef KDFCOUNTERBYTESGENERATOR_TC4A47F440088987896A3B7C964093578F33EA591_H
#define KDFCOUNTERBYTESGENERATOR_TC4A47F440088987896A3B7C964093578F33EA591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator
struct  KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::prf
	RuntimeObject* ___prf_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::h
	int32_t ___h_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::fixedInputDataCtrPrefix
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputDataCtrPrefix_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::fixedInputData_afterCtr
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputData_afterCtr_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::maxSizeExcl
	int32_t ___maxSizeExcl_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::ios
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ios_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::generatedBytes
	int32_t ___generatedBytes_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::k
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___k_9;

public:
	inline static int32_t get_offset_of_prf_2() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___prf_2)); }
	inline RuntimeObject* get_prf_2() const { return ___prf_2; }
	inline RuntimeObject** get_address_of_prf_2() { return &___prf_2; }
	inline void set_prf_2(RuntimeObject* value)
	{
		___prf_2 = value;
		Il2CppCodeGenWriteBarrier((&___prf_2), value);
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___h_3)); }
	inline int32_t get_h_3() const { return ___h_3; }
	inline int32_t* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(int32_t value)
	{
		___h_3 = value;
	}

	inline static int32_t get_offset_of_fixedInputDataCtrPrefix_4() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___fixedInputDataCtrPrefix_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputDataCtrPrefix_4() const { return ___fixedInputDataCtrPrefix_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputDataCtrPrefix_4() { return &___fixedInputDataCtrPrefix_4; }
	inline void set_fixedInputDataCtrPrefix_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputDataCtrPrefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputDataCtrPrefix_4), value);
	}

	inline static int32_t get_offset_of_fixedInputData_afterCtr_5() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___fixedInputData_afterCtr_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputData_afterCtr_5() const { return ___fixedInputData_afterCtr_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputData_afterCtr_5() { return &___fixedInputData_afterCtr_5; }
	inline void set_fixedInputData_afterCtr_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputData_afterCtr_5 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputData_afterCtr_5), value);
	}

	inline static int32_t get_offset_of_maxSizeExcl_6() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___maxSizeExcl_6)); }
	inline int32_t get_maxSizeExcl_6() const { return ___maxSizeExcl_6; }
	inline int32_t* get_address_of_maxSizeExcl_6() { return &___maxSizeExcl_6; }
	inline void set_maxSizeExcl_6(int32_t value)
	{
		___maxSizeExcl_6 = value;
	}

	inline static int32_t get_offset_of_ios_7() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___ios_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ios_7() const { return ___ios_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ios_7() { return &___ios_7; }
	inline void set_ios_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ios_7 = value;
		Il2CppCodeGenWriteBarrier((&___ios_7), value);
	}

	inline static int32_t get_offset_of_generatedBytes_8() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___generatedBytes_8)); }
	inline int32_t get_generatedBytes_8() const { return ___generatedBytes_8; }
	inline int32_t* get_address_of_generatedBytes_8() { return &___generatedBytes_8; }
	inline void set_generatedBytes_8(int32_t value)
	{
		___generatedBytes_8 = value;
	}

	inline static int32_t get_offset_of_k_9() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591, ___k_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_k_9() const { return ___k_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_k_9() { return &___k_9; }
	inline void set_k_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___k_9 = value;
		Il2CppCodeGenWriteBarrier((&___k_9), value);
	}
};

struct KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::IntegerMax
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___IntegerMax_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfCounterBytesGenerator::Two
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Two_1;

public:
	inline static int32_t get_offset_of_IntegerMax_0() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591_StaticFields, ___IntegerMax_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_IntegerMax_0() const { return ___IntegerMax_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_IntegerMax_0() { return &___IntegerMax_0; }
	inline void set_IntegerMax_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___IntegerMax_0 = value;
		Il2CppCodeGenWriteBarrier((&___IntegerMax_0), value);
	}

	inline static int32_t get_offset_of_Two_1() { return static_cast<int32_t>(offsetof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591_StaticFields, ___Two_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Two_1() const { return ___Two_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Two_1() { return &___Two_1; }
	inline void set_Two_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Two_1 = value;
		Il2CppCodeGenWriteBarrier((&___Two_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFCOUNTERBYTESGENERATOR_TC4A47F440088987896A3B7C964093578F33EA591_H
#ifndef KDFDOUBLEPIPELINEITERATIONBYTESGENERATOR_T4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_H
#define KDFDOUBLEPIPELINEITERATIONBYTESGENERATOR_T4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator
struct  KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::prf
	RuntimeObject* ___prf_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::h
	int32_t ___h_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::fixedInputData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputData_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::maxSizeExcl
	int32_t ___maxSizeExcl_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::ios
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ios_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::useCounter
	bool ___useCounter_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::generatedBytes
	int32_t ___generatedBytes_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::a
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___a_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::k
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___k_10;

public:
	inline static int32_t get_offset_of_prf_2() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___prf_2)); }
	inline RuntimeObject* get_prf_2() const { return ___prf_2; }
	inline RuntimeObject** get_address_of_prf_2() { return &___prf_2; }
	inline void set_prf_2(RuntimeObject* value)
	{
		___prf_2 = value;
		Il2CppCodeGenWriteBarrier((&___prf_2), value);
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___h_3)); }
	inline int32_t get_h_3() const { return ___h_3; }
	inline int32_t* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(int32_t value)
	{
		___h_3 = value;
	}

	inline static int32_t get_offset_of_fixedInputData_4() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___fixedInputData_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputData_4() const { return ___fixedInputData_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputData_4() { return &___fixedInputData_4; }
	inline void set_fixedInputData_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputData_4 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputData_4), value);
	}

	inline static int32_t get_offset_of_maxSizeExcl_5() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___maxSizeExcl_5)); }
	inline int32_t get_maxSizeExcl_5() const { return ___maxSizeExcl_5; }
	inline int32_t* get_address_of_maxSizeExcl_5() { return &___maxSizeExcl_5; }
	inline void set_maxSizeExcl_5(int32_t value)
	{
		___maxSizeExcl_5 = value;
	}

	inline static int32_t get_offset_of_ios_6() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___ios_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ios_6() const { return ___ios_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ios_6() { return &___ios_6; }
	inline void set_ios_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ios_6 = value;
		Il2CppCodeGenWriteBarrier((&___ios_6), value);
	}

	inline static int32_t get_offset_of_useCounter_7() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___useCounter_7)); }
	inline bool get_useCounter_7() const { return ___useCounter_7; }
	inline bool* get_address_of_useCounter_7() { return &___useCounter_7; }
	inline void set_useCounter_7(bool value)
	{
		___useCounter_7 = value;
	}

	inline static int32_t get_offset_of_generatedBytes_8() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___generatedBytes_8)); }
	inline int32_t get_generatedBytes_8() const { return ___generatedBytes_8; }
	inline int32_t* get_address_of_generatedBytes_8() { return &___generatedBytes_8; }
	inline void set_generatedBytes_8(int32_t value)
	{
		___generatedBytes_8 = value;
	}

	inline static int32_t get_offset_of_a_9() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___a_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_a_9() const { return ___a_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_a_9() { return &___a_9; }
	inline void set_a_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___a_9 = value;
		Il2CppCodeGenWriteBarrier((&___a_9), value);
	}

	inline static int32_t get_offset_of_k_10() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A, ___k_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_k_10() const { return ___k_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_k_10() { return &___k_10; }
	inline void set_k_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___k_10 = value;
		Il2CppCodeGenWriteBarrier((&___k_10), value);
	}
};

struct KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::IntegerMax
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___IntegerMax_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfDoublePipelineIterationBytesGenerator::Two
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Two_1;

public:
	inline static int32_t get_offset_of_IntegerMax_0() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_StaticFields, ___IntegerMax_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_IntegerMax_0() const { return ___IntegerMax_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_IntegerMax_0() { return &___IntegerMax_0; }
	inline void set_IntegerMax_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___IntegerMax_0 = value;
		Il2CppCodeGenWriteBarrier((&___IntegerMax_0), value);
	}

	inline static int32_t get_offset_of_Two_1() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_StaticFields, ___Two_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Two_1() const { return ___Two_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Two_1() { return &___Two_1; }
	inline void set_Two_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Two_1 = value;
		Il2CppCodeGenWriteBarrier((&___Two_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFDOUBLEPIPELINEITERATIONBYTESGENERATOR_T4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_H
#ifndef KDFFEEDBACKBYTESGENERATOR_TEE00F916C9203DF57369639E830FADA57B12770C_H
#define KDFFEEDBACKBYTESGENERATOR_TEE00F916C9203DF57369639E830FADA57B12770C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator
struct  KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::prf
	RuntimeObject* ___prf_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::h
	int32_t ___h_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::fixedInputData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputData_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::maxSizeExcl
	int32_t ___maxSizeExcl_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::ios
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ios_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_7;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::useCounter
	bool ___useCounter_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::generatedBytes
	int32_t ___generatedBytes_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::k
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___k_10;

public:
	inline static int32_t get_offset_of_prf_2() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___prf_2)); }
	inline RuntimeObject* get_prf_2() const { return ___prf_2; }
	inline RuntimeObject** get_address_of_prf_2() { return &___prf_2; }
	inline void set_prf_2(RuntimeObject* value)
	{
		___prf_2 = value;
		Il2CppCodeGenWriteBarrier((&___prf_2), value);
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___h_3)); }
	inline int32_t get_h_3() const { return ___h_3; }
	inline int32_t* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(int32_t value)
	{
		___h_3 = value;
	}

	inline static int32_t get_offset_of_fixedInputData_4() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___fixedInputData_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputData_4() const { return ___fixedInputData_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputData_4() { return &___fixedInputData_4; }
	inline void set_fixedInputData_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputData_4 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputData_4), value);
	}

	inline static int32_t get_offset_of_maxSizeExcl_5() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___maxSizeExcl_5)); }
	inline int32_t get_maxSizeExcl_5() const { return ___maxSizeExcl_5; }
	inline int32_t* get_address_of_maxSizeExcl_5() { return &___maxSizeExcl_5; }
	inline void set_maxSizeExcl_5(int32_t value)
	{
		___maxSizeExcl_5 = value;
	}

	inline static int32_t get_offset_of_ios_6() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___ios_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ios_6() const { return ___ios_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ios_6() { return &___ios_6; }
	inline void set_ios_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ios_6 = value;
		Il2CppCodeGenWriteBarrier((&___ios_6), value);
	}

	inline static int32_t get_offset_of_iv_7() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___iv_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_7() const { return ___iv_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_7() { return &___iv_7; }
	inline void set_iv_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_7 = value;
		Il2CppCodeGenWriteBarrier((&___iv_7), value);
	}

	inline static int32_t get_offset_of_useCounter_8() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___useCounter_8)); }
	inline bool get_useCounter_8() const { return ___useCounter_8; }
	inline bool* get_address_of_useCounter_8() { return &___useCounter_8; }
	inline void set_useCounter_8(bool value)
	{
		___useCounter_8 = value;
	}

	inline static int32_t get_offset_of_generatedBytes_9() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___generatedBytes_9)); }
	inline int32_t get_generatedBytes_9() const { return ___generatedBytes_9; }
	inline int32_t* get_address_of_generatedBytes_9() { return &___generatedBytes_9; }
	inline void set_generatedBytes_9(int32_t value)
	{
		___generatedBytes_9 = value;
	}

	inline static int32_t get_offset_of_k_10() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C, ___k_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_k_10() const { return ___k_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_k_10() { return &___k_10; }
	inline void set_k_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___k_10 = value;
		Il2CppCodeGenWriteBarrier((&___k_10), value);
	}
};

struct KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::IntegerMax
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___IntegerMax_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.KdfFeedbackBytesGenerator::Two
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Two_1;

public:
	inline static int32_t get_offset_of_IntegerMax_0() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C_StaticFields, ___IntegerMax_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_IntegerMax_0() const { return ___IntegerMax_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_IntegerMax_0() { return &___IntegerMax_0; }
	inline void set_IntegerMax_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___IntegerMax_0 = value;
		Il2CppCodeGenWriteBarrier((&___IntegerMax_0), value);
	}

	inline static int32_t get_offset_of_Two_1() { return static_cast<int32_t>(offsetof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C_StaticFields, ___Two_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Two_1() const { return ___Two_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Two_1() { return &___Two_1; }
	inline void set_Two_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Two_1 = value;
		Il2CppCodeGenWriteBarrier((&___Two_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFFEEDBACKBYTESGENERATOR_TEE00F916C9203DF57369639E830FADA57B12770C_H
#ifndef MGF1BYTESGENERATOR_T2420894B5A1ADDE6415397E791D1F27AD6DDE7AD_H
#define MGF1BYTESGENERATOR_T2420894B5A1ADDE6415397E791D1F27AD6DDE7AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Mgf1BytesGenerator
struct  Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Mgf1BytesGenerator::digest
	RuntimeObject* ___digest_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Mgf1BytesGenerator::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Mgf1BytesGenerator::hLen
	int32_t ___hLen_2;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD, ___digest_0)); }
	inline RuntimeObject* get_digest_0() const { return ___digest_0; }
	inline RuntimeObject** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(RuntimeObject* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_seed_1() { return static_cast<int32_t>(offsetof(Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD, ___seed_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_1() const { return ___seed_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_1() { return &___seed_1; }
	inline void set_seed_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_1 = value;
		Il2CppCodeGenWriteBarrier((&___seed_1), value);
	}

	inline static int32_t get_offset_of_hLen_2() { return static_cast<int32_t>(offsetof(Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD, ___hLen_2)); }
	inline int32_t get_hLen_2() const { return ___hLen_2; }
	inline int32_t* get_address_of_hLen_2() { return &___hLen_2; }
	inline void set_hLen_2(int32_t value)
	{
		___hLen_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MGF1BYTESGENERATOR_T2420894B5A1ADDE6415397E791D1F27AD6DDE7AD_H
#ifndef NACCACHESTERNKEYPAIRGENERATOR_TFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_H
#define NACCACHESTERNKEYPAIRGENERATOR_TFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.NaccacheSternKeyPairGenerator
struct  NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.NaccacheSternKeyPairGenerator::param
	NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09 * ___param_1;

public:
	inline static int32_t get_offset_of_param_1() { return static_cast<int32_t>(offsetof(NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2, ___param_1)); }
	inline NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09 * get_param_1() const { return ___param_1; }
	inline NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09 ** get_address_of_param_1() { return &___param_1; }
	inline void set_param_1(NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09 * value)
	{
		___param_1 = value;
		Il2CppCodeGenWriteBarrier((&___param_1), value);
	}
};

struct NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.NaccacheSternKeyPairGenerator::smallPrimes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___smallPrimes_0;

public:
	inline static int32_t get_offset_of_smallPrimes_0() { return static_cast<int32_t>(offsetof(NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_StaticFields, ___smallPrimes_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_smallPrimes_0() const { return ___smallPrimes_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_smallPrimes_0() { return &___smallPrimes_0; }
	inline void set_smallPrimes_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___smallPrimes_0 = value;
		Il2CppCodeGenWriteBarrier((&___smallPrimes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NACCACHESTERNKEYPAIRGENERATOR_TFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_H
#ifndef OPENBSDBCRYPT_T26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_H
#define OPENBSDBCRYPT_T26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenBsdBCrypt
struct  OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B  : public RuntimeObject
{
public:

public:
};

struct OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenBsdBCrypt::EncodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EncodingTable_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenBsdBCrypt::DecodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DecodingTable_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenBsdBCrypt::DefaultVersion
	String_t* ___DefaultVersion_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenBsdBCrypt::AllowedVersions
	RuntimeObject* ___AllowedVersions_3;

public:
	inline static int32_t get_offset_of_EncodingTable_0() { return static_cast<int32_t>(offsetof(OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields, ___EncodingTable_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EncodingTable_0() const { return ___EncodingTable_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EncodingTable_0() { return &___EncodingTable_0; }
	inline void set_EncodingTable_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EncodingTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___EncodingTable_0), value);
	}

	inline static int32_t get_offset_of_DecodingTable_1() { return static_cast<int32_t>(offsetof(OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields, ___DecodingTable_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DecodingTable_1() const { return ___DecodingTable_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DecodingTable_1() { return &___DecodingTable_1; }
	inline void set_DecodingTable_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DecodingTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___DecodingTable_1), value);
	}

	inline static int32_t get_offset_of_DefaultVersion_2() { return static_cast<int32_t>(offsetof(OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields, ___DefaultVersion_2)); }
	inline String_t* get_DefaultVersion_2() const { return ___DefaultVersion_2; }
	inline String_t** get_address_of_DefaultVersion_2() { return &___DefaultVersion_2; }
	inline void set_DefaultVersion_2(String_t* value)
	{
		___DefaultVersion_2 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultVersion_2), value);
	}

	inline static int32_t get_offset_of_AllowedVersions_3() { return static_cast<int32_t>(offsetof(OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields, ___AllowedVersions_3)); }
	inline RuntimeObject* get_AllowedVersions_3() const { return ___AllowedVersions_3; }
	inline RuntimeObject** get_address_of_AllowedVersions_3() { return &___AllowedVersions_3; }
	inline void set_AllowedVersions_3(RuntimeObject* value)
	{
		___AllowedVersions_3 = value;
		Il2CppCodeGenWriteBarrier((&___AllowedVersions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENBSDBCRYPT_T26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_H
#ifndef RSABLINDINGFACTORGENERATOR_TB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC_H
#define RSABLINDINGFACTORGENERATOR_TB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaBlindingFactorGenerator
struct  RsaBlindingFactorGenerator_tB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaBlindingFactorGenerator::key
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___key_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaBlindingFactorGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(RsaBlindingFactorGenerator_tB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC, ___key_0)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_key_0() const { return ___key_0; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(RsaBlindingFactorGenerator_tB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC, ___random_1)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSABLINDINGFACTORGENERATOR_TB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC_H
#ifndef RSAKEYPAIRGENERATOR_TFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_H
#define RSAKEYPAIRGENERATOR_TFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator
struct  RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyGenerationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator::parameters
	RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD * ___parameters_6;

public:
	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6, ___parameters_6)); }
	inline RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD * get_parameters_6() const { return ___parameters_6; }
	inline RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_6), value);
	}
};

struct RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator::SPECIAL_E_VALUES
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SPECIAL_E_VALUES_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator::SPECIAL_E_HIGHEST
	int32_t ___SPECIAL_E_HIGHEST_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator::SPECIAL_E_BITS
	int32_t ___SPECIAL_E_BITS_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator::One
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___One_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.RsaKeyPairGenerator::DefaultPublicExponent
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___DefaultPublicExponent_4;

public:
	inline static int32_t get_offset_of_SPECIAL_E_VALUES_0() { return static_cast<int32_t>(offsetof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields, ___SPECIAL_E_VALUES_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SPECIAL_E_VALUES_0() const { return ___SPECIAL_E_VALUES_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SPECIAL_E_VALUES_0() { return &___SPECIAL_E_VALUES_0; }
	inline void set_SPECIAL_E_VALUES_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SPECIAL_E_VALUES_0 = value;
		Il2CppCodeGenWriteBarrier((&___SPECIAL_E_VALUES_0), value);
	}

	inline static int32_t get_offset_of_SPECIAL_E_HIGHEST_1() { return static_cast<int32_t>(offsetof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields, ___SPECIAL_E_HIGHEST_1)); }
	inline int32_t get_SPECIAL_E_HIGHEST_1() const { return ___SPECIAL_E_HIGHEST_1; }
	inline int32_t* get_address_of_SPECIAL_E_HIGHEST_1() { return &___SPECIAL_E_HIGHEST_1; }
	inline void set_SPECIAL_E_HIGHEST_1(int32_t value)
	{
		___SPECIAL_E_HIGHEST_1 = value;
	}

	inline static int32_t get_offset_of_SPECIAL_E_BITS_2() { return static_cast<int32_t>(offsetof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields, ___SPECIAL_E_BITS_2)); }
	inline int32_t get_SPECIAL_E_BITS_2() const { return ___SPECIAL_E_BITS_2; }
	inline int32_t* get_address_of_SPECIAL_E_BITS_2() { return &___SPECIAL_E_BITS_2; }
	inline void set_SPECIAL_E_BITS_2(int32_t value)
	{
		___SPECIAL_E_BITS_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields, ___One_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_One_3() const { return ___One_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___One_3 = value;
		Il2CppCodeGenWriteBarrier((&___One_3), value);
	}

	inline static int32_t get_offset_of_DefaultPublicExponent_4() { return static_cast<int32_t>(offsetof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields, ___DefaultPublicExponent_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_DefaultPublicExponent_4() const { return ___DefaultPublicExponent_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_DefaultPublicExponent_4() { return &___DefaultPublicExponent_4; }
	inline void set_DefaultPublicExponent_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___DefaultPublicExponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultPublicExponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAKEYPAIRGENERATOR_TFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_H
#ifndef SCRYPT_TD2CF0085A3C6D2620548680FE62963E57EC24BF3_H
#define SCRYPT_TD2CF0085A3C6D2620548680FE62963E57EC24BF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.SCrypt
struct  SCrypt_tD2CF0085A3C6D2620548680FE62963E57EC24BF3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRYPT_TD2CF0085A3C6D2620548680FE62963E57EC24BF3_H
#ifndef X25519KEYPAIRGENERATOR_T67D9253D5AA27650BD98151680DD5394082CA4C4_H
#define X25519KEYPAIRGENERATOR_T67D9253D5AA27650BD98151680DD5394082CA4C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.X25519KeyPairGenerator
struct  X25519KeyPairGenerator_t67D9253D5AA27650BD98151680DD5394082CA4C4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.X25519KeyPairGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(X25519KeyPairGenerator_t67D9253D5AA27650BD98151680DD5394082CA4C4, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519KEYPAIRGENERATOR_T67D9253D5AA27650BD98151680DD5394082CA4C4_H
#ifndef X448KEYPAIRGENERATOR_T828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9_H
#define X448KEYPAIRGENERATOR_T828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.X448KeyPairGenerator
struct  X448KeyPairGenerator_t828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.X448KeyPairGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(X448KeyPairGenerator_t828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448KEYPAIRGENERATOR_T828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9_H
#ifndef CMAC_T8B265ABC6FE71967AE134E343F57EF5C29A75590_H
#define CMAC_T8B265ABC6FE71967AE134E343F57EF5C29A75590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac
struct  CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::ZEROES
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ZEROES_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::bufOff
	int32_t ___bufOff_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::cipher
	RuntimeObject* ___cipher_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::macSize
	int32_t ___macSize_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::L
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___L_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::Lu
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Lu_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CMac::Lu2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Lu2_10;

public:
	inline static int32_t get_offset_of_ZEROES_2() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___ZEROES_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ZEROES_2() const { return ___ZEROES_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ZEROES_2() { return &___ZEROES_2; }
	inline void set_ZEROES_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ZEROES_2 = value;
		Il2CppCodeGenWriteBarrier((&___ZEROES_2), value);
	}

	inline static int32_t get_offset_of_mac_3() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___mac_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_3() const { return ___mac_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_3() { return &___mac_3; }
	inline void set_mac_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_3 = value;
		Il2CppCodeGenWriteBarrier((&___mac_3), value);
	}

	inline static int32_t get_offset_of_buf_4() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___buf_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_4() const { return ___buf_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_4() { return &___buf_4; }
	inline void set_buf_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_4 = value;
		Il2CppCodeGenWriteBarrier((&___buf_4), value);
	}

	inline static int32_t get_offset_of_bufOff_5() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___bufOff_5)); }
	inline int32_t get_bufOff_5() const { return ___bufOff_5; }
	inline int32_t* get_address_of_bufOff_5() { return &___bufOff_5; }
	inline void set_bufOff_5(int32_t value)
	{
		___bufOff_5 = value;
	}

	inline static int32_t get_offset_of_cipher_6() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___cipher_6)); }
	inline RuntimeObject* get_cipher_6() const { return ___cipher_6; }
	inline RuntimeObject** get_address_of_cipher_6() { return &___cipher_6; }
	inline void set_cipher_6(RuntimeObject* value)
	{
		___cipher_6 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_6), value);
	}

	inline static int32_t get_offset_of_macSize_7() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___macSize_7)); }
	inline int32_t get_macSize_7() const { return ___macSize_7; }
	inline int32_t* get_address_of_macSize_7() { return &___macSize_7; }
	inline void set_macSize_7(int32_t value)
	{
		___macSize_7 = value;
	}

	inline static int32_t get_offset_of_L_8() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___L_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_L_8() const { return ___L_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_L_8() { return &___L_8; }
	inline void set_L_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___L_8 = value;
		Il2CppCodeGenWriteBarrier((&___L_8), value);
	}

	inline static int32_t get_offset_of_Lu_9() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___Lu_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Lu_9() const { return ___Lu_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Lu_9() { return &___Lu_9; }
	inline void set_Lu_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Lu_9 = value;
		Il2CppCodeGenWriteBarrier((&___Lu_9), value);
	}

	inline static int32_t get_offset_of_Lu2_10() { return static_cast<int32_t>(offsetof(CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590, ___Lu2_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Lu2_10() const { return ___Lu2_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Lu2_10() { return &___Lu2_10; }
	inline void set_Lu2_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Lu2_10 = value;
		Il2CppCodeGenWriteBarrier((&___Lu2_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMAC_T8B265ABC6FE71967AE134E343F57EF5C29A75590_H
#ifndef CBCBLOCKCIPHERMAC_T3276591D96748DEDC482C29892223BA18C859E78_H
#define CBCBLOCKCIPHERMAC_T3276591D96748DEDC482C29892223BA18C859E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac
struct  CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::bufOff
	int32_t ___bufOff_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::cipher
	RuntimeObject* ___cipher_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::padding
	RuntimeObject* ___padding_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::macSize
	int32_t ___macSize_4;

public:
	inline static int32_t get_offset_of_buf_0() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78, ___buf_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_0() const { return ___buf_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_0() { return &___buf_0; }
	inline void set_buf_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_0 = value;
		Il2CppCodeGenWriteBarrier((&___buf_0), value);
	}

	inline static int32_t get_offset_of_bufOff_1() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78, ___bufOff_1)); }
	inline int32_t get_bufOff_1() const { return ___bufOff_1; }
	inline int32_t* get_address_of_bufOff_1() { return &___bufOff_1; }
	inline void set_bufOff_1(int32_t value)
	{
		___bufOff_1 = value;
	}

	inline static int32_t get_offset_of_cipher_2() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78, ___cipher_2)); }
	inline RuntimeObject* get_cipher_2() const { return ___cipher_2; }
	inline RuntimeObject** get_address_of_cipher_2() { return &___cipher_2; }
	inline void set_cipher_2(RuntimeObject* value)
	{
		___cipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_2), value);
	}

	inline static int32_t get_offset_of_padding_3() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78, ___padding_3)); }
	inline RuntimeObject* get_padding_3() const { return ___padding_3; }
	inline RuntimeObject** get_address_of_padding_3() { return &___padding_3; }
	inline void set_padding_3(RuntimeObject* value)
	{
		___padding_3 = value;
		Il2CppCodeGenWriteBarrier((&___padding_3), value);
	}

	inline static int32_t get_offset_of_macSize_4() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78, ___macSize_4)); }
	inline int32_t get_macSize_4() const { return ___macSize_4; }
	inline int32_t* get_address_of_macSize_4() { return &___macSize_4; }
	inline void set_macSize_4(int32_t value)
	{
		___macSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBCBLOCKCIPHERMAC_T3276591D96748DEDC482C29892223BA18C859E78_H
#ifndef CFBBLOCKCIPHERMAC_T051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A_H
#define CFBBLOCKCIPHERMAC_T051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac
struct  CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac::bufOff
	int32_t ___bufOff_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac::cipher
	MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF * ___cipher_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac::padding
	RuntimeObject* ___padding_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.CfbBlockCipherMac::macSize
	int32_t ___macSize_5;

public:
	inline static int32_t get_offset_of_mac_0() { return static_cast<int32_t>(offsetof(CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A, ___mac_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_0() const { return ___mac_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_0() { return &___mac_0; }
	inline void set_mac_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_0 = value;
		Il2CppCodeGenWriteBarrier((&___mac_0), value);
	}

	inline static int32_t get_offset_of_Buffer_1() { return static_cast<int32_t>(offsetof(CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A, ___Buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_1() const { return ___Buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_1() { return &___Buffer_1; }
	inline void set_Buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_1), value);
	}

	inline static int32_t get_offset_of_bufOff_2() { return static_cast<int32_t>(offsetof(CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A, ___bufOff_2)); }
	inline int32_t get_bufOff_2() const { return ___bufOff_2; }
	inline int32_t* get_address_of_bufOff_2() { return &___bufOff_2; }
	inline void set_bufOff_2(int32_t value)
	{
		___bufOff_2 = value;
	}

	inline static int32_t get_offset_of_cipher_3() { return static_cast<int32_t>(offsetof(CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A, ___cipher_3)); }
	inline MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF * get_cipher_3() const { return ___cipher_3; }
	inline MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF ** get_address_of_cipher_3() { return &___cipher_3; }
	inline void set_cipher_3(MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF * value)
	{
		___cipher_3 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_3), value);
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A, ___padding_4)); }
	inline RuntimeObject* get_padding_4() const { return ___padding_4; }
	inline RuntimeObject** get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(RuntimeObject* value)
	{
		___padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___padding_4), value);
	}

	inline static int32_t get_offset_of_macSize_5() { return static_cast<int32_t>(offsetof(CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A, ___macSize_5)); }
	inline int32_t get_macSize_5() const { return ___macSize_5; }
	inline int32_t* get_address_of_macSize_5() { return &___macSize_5; }
	inline void set_macSize_5(int32_t value)
	{
		___macSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFBBLOCKCIPHERMAC_T051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A_H
#ifndef DSTU7564MAC_TB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED_H
#define DSTU7564MAC_TB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7564Mac
struct  Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7564Mac::engine
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E * ___engine_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7564Mac::macSize
	int32_t ___macSize_1;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7564Mac::inputLength
	uint64_t ___inputLength_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7564Mac::paddedKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___paddedKey_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7564Mac::invertedKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___invertedKey_4;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED, ___engine_0)); }
	inline Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E * get_engine_0() const { return ___engine_0; }
	inline Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E ** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E * value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_macSize_1() { return static_cast<int32_t>(offsetof(Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED, ___macSize_1)); }
	inline int32_t get_macSize_1() const { return ___macSize_1; }
	inline int32_t* get_address_of_macSize_1() { return &___macSize_1; }
	inline void set_macSize_1(int32_t value)
	{
		___macSize_1 = value;
	}

	inline static int32_t get_offset_of_inputLength_2() { return static_cast<int32_t>(offsetof(Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED, ___inputLength_2)); }
	inline uint64_t get_inputLength_2() const { return ___inputLength_2; }
	inline uint64_t* get_address_of_inputLength_2() { return &___inputLength_2; }
	inline void set_inputLength_2(uint64_t value)
	{
		___inputLength_2 = value;
	}

	inline static int32_t get_offset_of_paddedKey_3() { return static_cast<int32_t>(offsetof(Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED, ___paddedKey_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_paddedKey_3() const { return ___paddedKey_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_paddedKey_3() { return &___paddedKey_3; }
	inline void set_paddedKey_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___paddedKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___paddedKey_3), value);
	}

	inline static int32_t get_offset_of_invertedKey_4() { return static_cast<int32_t>(offsetof(Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED, ___invertedKey_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_invertedKey_4() const { return ___invertedKey_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_invertedKey_4() { return &___invertedKey_4; }
	inline void set_invertedKey_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___invertedKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___invertedKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSTU7564MAC_TB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED_H
#ifndef DSTU7624MAC_T50A4296A65DDD0C7017484DCC76D157AA9B3B270_H
#define DSTU7624MAC_T50A4296A65DDD0C7017484DCC76D157AA9B3B270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac
struct  Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::macSize
	int32_t ___macSize_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::engine
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 * ___engine_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::blockSize
	int32_t ___blockSize_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::c
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___c_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::cTemp
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cTemp_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::kDelta
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___kDelta_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Dstu7624Mac::bufOff
	int32_t ___bufOff_7;

public:
	inline static int32_t get_offset_of_macSize_0() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___macSize_0)); }
	inline int32_t get_macSize_0() const { return ___macSize_0; }
	inline int32_t* get_address_of_macSize_0() { return &___macSize_0; }
	inline void set_macSize_0(int32_t value)
	{
		___macSize_0 = value;
	}

	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___engine_1)); }
	inline Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 * get_engine_1() const { return ___engine_1; }
	inline Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 ** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 * value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}

	inline static int32_t get_offset_of_blockSize_2() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___blockSize_2)); }
	inline int32_t get_blockSize_2() const { return ___blockSize_2; }
	inline int32_t* get_address_of_blockSize_2() { return &___blockSize_2; }
	inline void set_blockSize_2(int32_t value)
	{
		___blockSize_2 = value;
	}

	inline static int32_t get_offset_of_c_3() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___c_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_c_3() const { return ___c_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_c_3() { return &___c_3; }
	inline void set_c_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___c_3 = value;
		Il2CppCodeGenWriteBarrier((&___c_3), value);
	}

	inline static int32_t get_offset_of_cTemp_4() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___cTemp_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cTemp_4() const { return ___cTemp_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cTemp_4() { return &___cTemp_4; }
	inline void set_cTemp_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cTemp_4 = value;
		Il2CppCodeGenWriteBarrier((&___cTemp_4), value);
	}

	inline static int32_t get_offset_of_kDelta_5() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___kDelta_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_kDelta_5() const { return ___kDelta_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_kDelta_5() { return &___kDelta_5; }
	inline void set_kDelta_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___kDelta_5 = value;
		Il2CppCodeGenWriteBarrier((&___kDelta_5), value);
	}

	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_6), value);
	}

	inline static int32_t get_offset_of_bufOff_7() { return static_cast<int32_t>(offsetof(Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270, ___bufOff_7)); }
	inline int32_t get_bufOff_7() const { return ___bufOff_7; }
	inline int32_t* get_address_of_bufOff_7() { return &___bufOff_7; }
	inline void set_bufOff_7(int32_t value)
	{
		___bufOff_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSTU7624MAC_T50A4296A65DDD0C7017484DCC76D157AA9B3B270_H
#ifndef GMAC_TCF90FDDB6B2077C95B99C8057E9364A66B97231B_H
#define GMAC_TCF90FDDB6B2077C95B99C8057E9364A66B97231B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.GMac
struct  GMac_tCF90FDDB6B2077C95B99C8057E9364A66B97231B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.GMac::cipher
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617 * ___cipher_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.GMac::macSizeBits
	int32_t ___macSizeBits_1;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(GMac_tCF90FDDB6B2077C95B99C8057E9364A66B97231B, ___cipher_0)); }
	inline GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617 * get_cipher_0() const { return ___cipher_0; }
	inline GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617 ** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617 * value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_macSizeBits_1() { return static_cast<int32_t>(offsetof(GMac_tCF90FDDB6B2077C95B99C8057E9364A66B97231B, ___macSizeBits_1)); }
	inline int32_t get_macSizeBits_1() const { return ___macSizeBits_1; }
	inline int32_t* get_address_of_macSizeBits_1() { return &___macSizeBits_1; }
	inline void set_macSizeBits_1(int32_t value)
	{
		___macSizeBits_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMAC_TCF90FDDB6B2077C95B99C8057E9364A66B97231B_H
#ifndef GOST28147MAC_TCCD420038880F2CB63BB73CF32BACF49570EC589_H
#define GOST28147MAC_TCCD420038880F2CB63BB73CF32BACF49570EC589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac
struct  Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::bufOff
	int32_t ___bufOff_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::firstStep
	bool ___firstStep_5;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::macIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macIV_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Gost28147Mac::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_8;

public:
	inline static int32_t get_offset_of_bufOff_2() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___bufOff_2)); }
	inline int32_t get_bufOff_2() const { return ___bufOff_2; }
	inline int32_t* get_address_of_bufOff_2() { return &___bufOff_2; }
	inline void set_bufOff_2(int32_t value)
	{
		___bufOff_2 = value;
	}

	inline static int32_t get_offset_of_buf_3() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___buf_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_3() const { return ___buf_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_3() { return &___buf_3; }
	inline void set_buf_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_3 = value;
		Il2CppCodeGenWriteBarrier((&___buf_3), value);
	}

	inline static int32_t get_offset_of_mac_4() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___mac_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_4() const { return ___mac_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_4() { return &___mac_4; }
	inline void set_mac_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_4 = value;
		Il2CppCodeGenWriteBarrier((&___mac_4), value);
	}

	inline static int32_t get_offset_of_firstStep_5() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___firstStep_5)); }
	inline bool get_firstStep_5() const { return ___firstStep_5; }
	inline bool* get_address_of_firstStep_5() { return &___firstStep_5; }
	inline void set_firstStep_5(bool value)
	{
		___firstStep_5 = value;
	}

	inline static int32_t get_offset_of_workingKey_6() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___workingKey_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_6() const { return ___workingKey_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_6() { return &___workingKey_6; }
	inline void set_workingKey_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_6), value);
	}

	inline static int32_t get_offset_of_macIV_7() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___macIV_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macIV_7() const { return ___macIV_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macIV_7() { return &___macIV_7; }
	inline void set_macIV_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macIV_7 = value;
		Il2CppCodeGenWriteBarrier((&___macIV_7), value);
	}

	inline static int32_t get_offset_of_S_8() { return static_cast<int32_t>(offsetof(Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589, ___S_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_8() const { return ___S_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_8() { return &___S_8; }
	inline void set_S_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_8 = value;
		Il2CppCodeGenWriteBarrier((&___S_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST28147MAC_TCCD420038880F2CB63BB73CF32BACF49570EC589_H
#ifndef HMAC_T7ED75AC4D2CE80BFEF6AE133869205F57E378045_H
#define HMAC_T7ED75AC4D2CE80BFEF6AE133869205F57E378045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac
struct  HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::digest
	RuntimeObject* ___digest_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::digestSize
	int32_t ___digestSize_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::blockLength
	int32_t ___blockLength_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IMemoable BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::ipadState
	RuntimeObject* ___ipadState_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IMemoable BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::opadState
	RuntimeObject* ___opadState_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::inputPad
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___inputPad_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac::outputBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___outputBuf_8;

public:
	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___digest_2)); }
	inline RuntimeObject* get_digest_2() const { return ___digest_2; }
	inline RuntimeObject** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(RuntimeObject* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}

	inline static int32_t get_offset_of_digestSize_3() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___digestSize_3)); }
	inline int32_t get_digestSize_3() const { return ___digestSize_3; }
	inline int32_t* get_address_of_digestSize_3() { return &___digestSize_3; }
	inline void set_digestSize_3(int32_t value)
	{
		___digestSize_3 = value;
	}

	inline static int32_t get_offset_of_blockLength_4() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___blockLength_4)); }
	inline int32_t get_blockLength_4() const { return ___blockLength_4; }
	inline int32_t* get_address_of_blockLength_4() { return &___blockLength_4; }
	inline void set_blockLength_4(int32_t value)
	{
		___blockLength_4 = value;
	}

	inline static int32_t get_offset_of_ipadState_5() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___ipadState_5)); }
	inline RuntimeObject* get_ipadState_5() const { return ___ipadState_5; }
	inline RuntimeObject** get_address_of_ipadState_5() { return &___ipadState_5; }
	inline void set_ipadState_5(RuntimeObject* value)
	{
		___ipadState_5 = value;
		Il2CppCodeGenWriteBarrier((&___ipadState_5), value);
	}

	inline static int32_t get_offset_of_opadState_6() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___opadState_6)); }
	inline RuntimeObject* get_opadState_6() const { return ___opadState_6; }
	inline RuntimeObject** get_address_of_opadState_6() { return &___opadState_6; }
	inline void set_opadState_6(RuntimeObject* value)
	{
		___opadState_6 = value;
		Il2CppCodeGenWriteBarrier((&___opadState_6), value);
	}

	inline static int32_t get_offset_of_inputPad_7() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___inputPad_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_inputPad_7() const { return ___inputPad_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_inputPad_7() { return &___inputPad_7; }
	inline void set_inputPad_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___inputPad_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputPad_7), value);
	}

	inline static int32_t get_offset_of_outputBuf_8() { return static_cast<int32_t>(offsetof(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045, ___outputBuf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_outputBuf_8() const { return ___outputBuf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_outputBuf_8() { return &___outputBuf_8; }
	inline void set_outputBuf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___outputBuf_8 = value;
		Il2CppCodeGenWriteBarrier((&___outputBuf_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_T7ED75AC4D2CE80BFEF6AE133869205F57E378045_H
#ifndef ISO9797ALG3MAC_T1CCC19E2C9BCF8767A635662637E1D3CE468AEC0_H
#define ISO9797ALG3MAC_T1CCC19E2C9BCF8767A635662637E1D3CE468AEC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac
struct  ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::bufOff
	int32_t ___bufOff_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::cipher
	RuntimeObject* ___cipher_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::padding
	RuntimeObject* ___padding_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::macSize
	int32_t ___macSize_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::lastKey2
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___lastKey2_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.ISO9797Alg3Mac::lastKey3
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___lastKey3_7;

public:
	inline static int32_t get_offset_of_mac_0() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___mac_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_0() const { return ___mac_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_0() { return &___mac_0; }
	inline void set_mac_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_0 = value;
		Il2CppCodeGenWriteBarrier((&___mac_0), value);
	}

	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___buf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_1() const { return ___buf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_1 = value;
		Il2CppCodeGenWriteBarrier((&___buf_1), value);
	}

	inline static int32_t get_offset_of_bufOff_2() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___bufOff_2)); }
	inline int32_t get_bufOff_2() const { return ___bufOff_2; }
	inline int32_t* get_address_of_bufOff_2() { return &___bufOff_2; }
	inline void set_bufOff_2(int32_t value)
	{
		___bufOff_2 = value;
	}

	inline static int32_t get_offset_of_cipher_3() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___cipher_3)); }
	inline RuntimeObject* get_cipher_3() const { return ___cipher_3; }
	inline RuntimeObject** get_address_of_cipher_3() { return &___cipher_3; }
	inline void set_cipher_3(RuntimeObject* value)
	{
		___cipher_3 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_3), value);
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___padding_4)); }
	inline RuntimeObject* get_padding_4() const { return ___padding_4; }
	inline RuntimeObject** get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(RuntimeObject* value)
	{
		___padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___padding_4), value);
	}

	inline static int32_t get_offset_of_macSize_5() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___macSize_5)); }
	inline int32_t get_macSize_5() const { return ___macSize_5; }
	inline int32_t* get_address_of_macSize_5() { return &___macSize_5; }
	inline void set_macSize_5(int32_t value)
	{
		___macSize_5 = value;
	}

	inline static int32_t get_offset_of_lastKey2_6() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___lastKey2_6)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_lastKey2_6() const { return ___lastKey2_6; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_lastKey2_6() { return &___lastKey2_6; }
	inline void set_lastKey2_6(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___lastKey2_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastKey2_6), value);
	}

	inline static int32_t get_offset_of_lastKey3_7() { return static_cast<int32_t>(offsetof(ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0, ___lastKey3_7)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_lastKey3_7() const { return ___lastKey3_7; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_lastKey3_7() { return &___lastKey3_7; }
	inline void set_lastKey3_7(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___lastKey3_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastKey3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO9797ALG3MAC_T1CCC19E2C9BCF8767A635662637E1D3CE468AEC0_H
#ifndef MACCFBBLOCKCIPHER_TDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF_H
#define MACCFBBLOCKCIPHER_TDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher
struct  MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher::cfbV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cfbV_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher::cfbOutV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cfbOutV_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher::blockSize
	int32_t ___blockSize_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.MacCFBBlockCipher::cipher
	RuntimeObject* ___cipher_4;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_cfbV_1() { return static_cast<int32_t>(offsetof(MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF, ___cfbV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cfbV_1() const { return ___cfbV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cfbV_1() { return &___cfbV_1; }
	inline void set_cfbV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cfbV_1 = value;
		Il2CppCodeGenWriteBarrier((&___cfbV_1), value);
	}

	inline static int32_t get_offset_of_cfbOutV_2() { return static_cast<int32_t>(offsetof(MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF, ___cfbOutV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cfbOutV_2() const { return ___cfbOutV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cfbOutV_2() { return &___cfbOutV_2; }
	inline void set_cfbOutV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cfbOutV_2 = value;
		Il2CppCodeGenWriteBarrier((&___cfbOutV_2), value);
	}

	inline static int32_t get_offset_of_blockSize_3() { return static_cast<int32_t>(offsetof(MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF, ___blockSize_3)); }
	inline int32_t get_blockSize_3() const { return ___blockSize_3; }
	inline int32_t* get_address_of_blockSize_3() { return &___blockSize_3; }
	inline void set_blockSize_3(int32_t value)
	{
		___blockSize_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACCFBBLOCKCIPHER_TDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF_H
#ifndef POLY1305_TD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77_H
#define POLY1305_TD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305
struct  Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::cipher
	RuntimeObject* ___cipher_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::singleByte
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___singleByte_2;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::r0
	uint32_t ___r0_3;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::r1
	uint32_t ___r1_4;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::r2
	uint32_t ___r2_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::r3
	uint32_t ___r3_6;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::r4
	uint32_t ___r4_7;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::s1
	uint32_t ___s1_8;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::s2
	uint32_t ___s2_9;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::s3
	uint32_t ___s3_10;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::s4
	uint32_t ___s4_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::k0
	uint32_t ___k0_12;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::k1
	uint32_t ___k1_13;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::k2
	uint32_t ___k2_14;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::k3
	uint32_t ___k3_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::currentBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___currentBlock_16;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::currentBlockOffset
	int32_t ___currentBlockOffset_17;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::h0
	uint32_t ___h0_18;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::h1
	uint32_t ___h1_19;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::h2
	uint32_t ___h2_20;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::h3
	uint32_t ___h3_21;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.Poly1305::h4
	uint32_t ___h4_22;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}

	inline static int32_t get_offset_of_singleByte_2() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___singleByte_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_singleByte_2() const { return ___singleByte_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_singleByte_2() { return &___singleByte_2; }
	inline void set_singleByte_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___singleByte_2 = value;
		Il2CppCodeGenWriteBarrier((&___singleByte_2), value);
	}

	inline static int32_t get_offset_of_r0_3() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___r0_3)); }
	inline uint32_t get_r0_3() const { return ___r0_3; }
	inline uint32_t* get_address_of_r0_3() { return &___r0_3; }
	inline void set_r0_3(uint32_t value)
	{
		___r0_3 = value;
	}

	inline static int32_t get_offset_of_r1_4() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___r1_4)); }
	inline uint32_t get_r1_4() const { return ___r1_4; }
	inline uint32_t* get_address_of_r1_4() { return &___r1_4; }
	inline void set_r1_4(uint32_t value)
	{
		___r1_4 = value;
	}

	inline static int32_t get_offset_of_r2_5() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___r2_5)); }
	inline uint32_t get_r2_5() const { return ___r2_5; }
	inline uint32_t* get_address_of_r2_5() { return &___r2_5; }
	inline void set_r2_5(uint32_t value)
	{
		___r2_5 = value;
	}

	inline static int32_t get_offset_of_r3_6() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___r3_6)); }
	inline uint32_t get_r3_6() const { return ___r3_6; }
	inline uint32_t* get_address_of_r3_6() { return &___r3_6; }
	inline void set_r3_6(uint32_t value)
	{
		___r3_6 = value;
	}

	inline static int32_t get_offset_of_r4_7() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___r4_7)); }
	inline uint32_t get_r4_7() const { return ___r4_7; }
	inline uint32_t* get_address_of_r4_7() { return &___r4_7; }
	inline void set_r4_7(uint32_t value)
	{
		___r4_7 = value;
	}

	inline static int32_t get_offset_of_s1_8() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___s1_8)); }
	inline uint32_t get_s1_8() const { return ___s1_8; }
	inline uint32_t* get_address_of_s1_8() { return &___s1_8; }
	inline void set_s1_8(uint32_t value)
	{
		___s1_8 = value;
	}

	inline static int32_t get_offset_of_s2_9() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___s2_9)); }
	inline uint32_t get_s2_9() const { return ___s2_9; }
	inline uint32_t* get_address_of_s2_9() { return &___s2_9; }
	inline void set_s2_9(uint32_t value)
	{
		___s2_9 = value;
	}

	inline static int32_t get_offset_of_s3_10() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___s3_10)); }
	inline uint32_t get_s3_10() const { return ___s3_10; }
	inline uint32_t* get_address_of_s3_10() { return &___s3_10; }
	inline void set_s3_10(uint32_t value)
	{
		___s3_10 = value;
	}

	inline static int32_t get_offset_of_s4_11() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___s4_11)); }
	inline uint32_t get_s4_11() const { return ___s4_11; }
	inline uint32_t* get_address_of_s4_11() { return &___s4_11; }
	inline void set_s4_11(uint32_t value)
	{
		___s4_11 = value;
	}

	inline static int32_t get_offset_of_k0_12() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___k0_12)); }
	inline uint32_t get_k0_12() const { return ___k0_12; }
	inline uint32_t* get_address_of_k0_12() { return &___k0_12; }
	inline void set_k0_12(uint32_t value)
	{
		___k0_12 = value;
	}

	inline static int32_t get_offset_of_k1_13() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___k1_13)); }
	inline uint32_t get_k1_13() const { return ___k1_13; }
	inline uint32_t* get_address_of_k1_13() { return &___k1_13; }
	inline void set_k1_13(uint32_t value)
	{
		___k1_13 = value;
	}

	inline static int32_t get_offset_of_k2_14() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___k2_14)); }
	inline uint32_t get_k2_14() const { return ___k2_14; }
	inline uint32_t* get_address_of_k2_14() { return &___k2_14; }
	inline void set_k2_14(uint32_t value)
	{
		___k2_14 = value;
	}

	inline static int32_t get_offset_of_k3_15() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___k3_15)); }
	inline uint32_t get_k3_15() const { return ___k3_15; }
	inline uint32_t* get_address_of_k3_15() { return &___k3_15; }
	inline void set_k3_15(uint32_t value)
	{
		___k3_15 = value;
	}

	inline static int32_t get_offset_of_currentBlock_16() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___currentBlock_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_currentBlock_16() const { return ___currentBlock_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_currentBlock_16() { return &___currentBlock_16; }
	inline void set_currentBlock_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___currentBlock_16 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlock_16), value);
	}

	inline static int32_t get_offset_of_currentBlockOffset_17() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___currentBlockOffset_17)); }
	inline int32_t get_currentBlockOffset_17() const { return ___currentBlockOffset_17; }
	inline int32_t* get_address_of_currentBlockOffset_17() { return &___currentBlockOffset_17; }
	inline void set_currentBlockOffset_17(int32_t value)
	{
		___currentBlockOffset_17 = value;
	}

	inline static int32_t get_offset_of_h0_18() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___h0_18)); }
	inline uint32_t get_h0_18() const { return ___h0_18; }
	inline uint32_t* get_address_of_h0_18() { return &___h0_18; }
	inline void set_h0_18(uint32_t value)
	{
		___h0_18 = value;
	}

	inline static int32_t get_offset_of_h1_19() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___h1_19)); }
	inline uint32_t get_h1_19() const { return ___h1_19; }
	inline uint32_t* get_address_of_h1_19() { return &___h1_19; }
	inline void set_h1_19(uint32_t value)
	{
		___h1_19 = value;
	}

	inline static int32_t get_offset_of_h2_20() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___h2_20)); }
	inline uint32_t get_h2_20() const { return ___h2_20; }
	inline uint32_t* get_address_of_h2_20() { return &___h2_20; }
	inline void set_h2_20(uint32_t value)
	{
		___h2_20 = value;
	}

	inline static int32_t get_offset_of_h3_21() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___h3_21)); }
	inline uint32_t get_h3_21() const { return ___h3_21; }
	inline uint32_t* get_address_of_h3_21() { return &___h3_21; }
	inline void set_h3_21(uint32_t value)
	{
		___h3_21 = value;
	}

	inline static int32_t get_offset_of_h4_22() { return static_cast<int32_t>(offsetof(Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77, ___h4_22)); }
	inline uint32_t get_h4_22() const { return ___h4_22; }
	inline uint32_t* get_address_of_h4_22() { return &___h4_22; }
	inline void set_h4_22(uint32_t value)
	{
		___h4_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLY1305_TD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77_H
#ifndef SIPHASH_TA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE_H
#define SIPHASH_TA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash
struct  SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::c
	int32_t ___c_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::d
	int32_t ___d_1;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::k0
	int64_t ___k0_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::k1
	int64_t ___k1_3;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::v0
	int64_t ___v0_4;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::v1
	int64_t ___v1_5;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::v2
	int64_t ___v2_6;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::v3
	int64_t ___v3_7;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::m
	int64_t ___m_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::wordPos
	int32_t ___wordPos_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SipHash::wordCount
	int32_t ___wordCount_10;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___c_0)); }
	inline int32_t get_c_0() const { return ___c_0; }
	inline int32_t* get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(int32_t value)
	{
		___c_0 = value;
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___d_1)); }
	inline int32_t get_d_1() const { return ___d_1; }
	inline int32_t* get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(int32_t value)
	{
		___d_1 = value;
	}

	inline static int32_t get_offset_of_k0_2() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___k0_2)); }
	inline int64_t get_k0_2() const { return ___k0_2; }
	inline int64_t* get_address_of_k0_2() { return &___k0_2; }
	inline void set_k0_2(int64_t value)
	{
		___k0_2 = value;
	}

	inline static int32_t get_offset_of_k1_3() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___k1_3)); }
	inline int64_t get_k1_3() const { return ___k1_3; }
	inline int64_t* get_address_of_k1_3() { return &___k1_3; }
	inline void set_k1_3(int64_t value)
	{
		___k1_3 = value;
	}

	inline static int32_t get_offset_of_v0_4() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___v0_4)); }
	inline int64_t get_v0_4() const { return ___v0_4; }
	inline int64_t* get_address_of_v0_4() { return &___v0_4; }
	inline void set_v0_4(int64_t value)
	{
		___v0_4 = value;
	}

	inline static int32_t get_offset_of_v1_5() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___v1_5)); }
	inline int64_t get_v1_5() const { return ___v1_5; }
	inline int64_t* get_address_of_v1_5() { return &___v1_5; }
	inline void set_v1_5(int64_t value)
	{
		___v1_5 = value;
	}

	inline static int32_t get_offset_of_v2_6() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___v2_6)); }
	inline int64_t get_v2_6() const { return ___v2_6; }
	inline int64_t* get_address_of_v2_6() { return &___v2_6; }
	inline void set_v2_6(int64_t value)
	{
		___v2_6 = value;
	}

	inline static int32_t get_offset_of_v3_7() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___v3_7)); }
	inline int64_t get_v3_7() const { return ___v3_7; }
	inline int64_t* get_address_of_v3_7() { return &___v3_7; }
	inline void set_v3_7(int64_t value)
	{
		___v3_7 = value;
	}

	inline static int32_t get_offset_of_m_8() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___m_8)); }
	inline int64_t get_m_8() const { return ___m_8; }
	inline int64_t* get_address_of_m_8() { return &___m_8; }
	inline void set_m_8(int64_t value)
	{
		___m_8 = value;
	}

	inline static int32_t get_offset_of_wordPos_9() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___wordPos_9)); }
	inline int32_t get_wordPos_9() const { return ___wordPos_9; }
	inline int32_t* get_address_of_wordPos_9() { return &___wordPos_9; }
	inline void set_wordPos_9(int32_t value)
	{
		___wordPos_9 = value;
	}

	inline static int32_t get_offset_of_wordCount_10() { return static_cast<int32_t>(offsetof(SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE, ___wordCount_10)); }
	inline int32_t get_wordCount_10() const { return ___wordCount_10; }
	inline int32_t* get_address_of_wordCount_10() { return &___wordCount_10; }
	inline void set_wordCount_10(int32_t value)
	{
		___wordCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIPHASH_TA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE_H
#ifndef SKEINMAC_T5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72_H
#define SKEINMAC_T5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SkeinMac
struct  SkeinMac_t5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.SkeinMac::engine
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * ___engine_3;

public:
	inline static int32_t get_offset_of_engine_3() { return static_cast<int32_t>(offsetof(SkeinMac_t5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72, ___engine_3)); }
	inline SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * get_engine_3() const { return ___engine_3; }
	inline SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 ** get_address_of_engine_3() { return &___engine_3; }
	inline void set_engine_3(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * value)
	{
		___engine_3 = value;
		Il2CppCodeGenWriteBarrier((&___engine_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEINMAC_T5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72_H
#ifndef VMPCMAC_TCFDA77323497B45AD20B11669E0B2169B390893D_H
#define VMPCMAC_TCFDA77323497B45AD20B11669E0B2169B390893D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac
struct  VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::g
	uint8_t ___g_0;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::n
	uint8_t ___n_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::P
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___P_2;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::s
	uint8_t ___s_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::T
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___T_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::workingIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingIV_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_6;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::x1
	uint8_t ___x1_7;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::x2
	uint8_t ___x2_8;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::x3
	uint8_t ___x3_9;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.VmpcMac::x4
	uint8_t ___x4_10;

public:
	inline static int32_t get_offset_of_g_0() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___g_0)); }
	inline uint8_t get_g_0() const { return ___g_0; }
	inline uint8_t* get_address_of_g_0() { return &___g_0; }
	inline void set_g_0(uint8_t value)
	{
		___g_0 = value;
	}

	inline static int32_t get_offset_of_n_1() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___n_1)); }
	inline uint8_t get_n_1() const { return ___n_1; }
	inline uint8_t* get_address_of_n_1() { return &___n_1; }
	inline void set_n_1(uint8_t value)
	{
		___n_1 = value;
	}

	inline static int32_t get_offset_of_P_2() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___P_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_P_2() const { return ___P_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_P_2() { return &___P_2; }
	inline void set_P_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___P_2 = value;
		Il2CppCodeGenWriteBarrier((&___P_2), value);
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___s_3)); }
	inline uint8_t get_s_3() const { return ___s_3; }
	inline uint8_t* get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(uint8_t value)
	{
		___s_3 = value;
	}

	inline static int32_t get_offset_of_T_4() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___T_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_T_4() const { return ___T_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_T_4() { return &___T_4; }
	inline void set_T_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___T_4 = value;
		Il2CppCodeGenWriteBarrier((&___T_4), value);
	}

	inline static int32_t get_offset_of_workingIV_5() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___workingIV_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingIV_5() const { return ___workingIV_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingIV_5() { return &___workingIV_5; }
	inline void set_workingIV_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingIV_5 = value;
		Il2CppCodeGenWriteBarrier((&___workingIV_5), value);
	}

	inline static int32_t get_offset_of_workingKey_6() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___workingKey_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_6() const { return ___workingKey_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_6() { return &___workingKey_6; }
	inline void set_workingKey_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_6), value);
	}

	inline static int32_t get_offset_of_x1_7() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___x1_7)); }
	inline uint8_t get_x1_7() const { return ___x1_7; }
	inline uint8_t* get_address_of_x1_7() { return &___x1_7; }
	inline void set_x1_7(uint8_t value)
	{
		___x1_7 = value;
	}

	inline static int32_t get_offset_of_x2_8() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___x2_8)); }
	inline uint8_t get_x2_8() const { return ___x2_8; }
	inline uint8_t* get_address_of_x2_8() { return &___x2_8; }
	inline void set_x2_8(uint8_t value)
	{
		___x2_8 = value;
	}

	inline static int32_t get_offset_of_x3_9() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___x3_9)); }
	inline uint8_t get_x3_9() const { return ___x3_9; }
	inline uint8_t* get_address_of_x3_9() { return &___x3_9; }
	inline void set_x3_9(uint8_t value)
	{
		___x3_9 = value;
	}

	inline static int32_t get_offset_of_x4_10() { return static_cast<int32_t>(offsetof(VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D, ___x4_10)); }
	inline uint8_t get_x4_10() const { return ___x4_10; }
	inline uint8_t* get_address_of_x4_10() { return &___x4_10; }
	inline void set_x4_10(uint8_t value)
	{
		___x4_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VMPCMAC_TCFDA77323497B45AD20B11669E0B2169B390893D_H
#ifndef CBCBLOCKCIPHER_T7565E6778810D01349A96CDFEDDE39A56F47907F_H
#define CBCBLOCKCIPHER_T7565E6778810D01349A96CDFEDDE39A56F47907F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher
struct  CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::cbcV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cbcV_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::cbcNextV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cbcNextV_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::blockSize
	int32_t ___blockSize_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::cipher
	RuntimeObject* ___cipher_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::encrypting
	bool ___encrypting_5;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_cbcV_1() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F, ___cbcV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cbcV_1() const { return ___cbcV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cbcV_1() { return &___cbcV_1; }
	inline void set_cbcV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cbcV_1 = value;
		Il2CppCodeGenWriteBarrier((&___cbcV_1), value);
	}

	inline static int32_t get_offset_of_cbcNextV_2() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F, ___cbcNextV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cbcNextV_2() const { return ___cbcNextV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cbcNextV_2() { return &___cbcNextV_2; }
	inline void set_cbcNextV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cbcNextV_2 = value;
		Il2CppCodeGenWriteBarrier((&___cbcNextV_2), value);
	}

	inline static int32_t get_offset_of_blockSize_3() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F, ___blockSize_3)); }
	inline int32_t get_blockSize_3() const { return ___blockSize_3; }
	inline int32_t* get_address_of_blockSize_3() { return &___blockSize_3; }
	inline void set_blockSize_3(int32_t value)
	{
		___blockSize_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}

	inline static int32_t get_offset_of_encrypting_5() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F, ___encrypting_5)); }
	inline bool get_encrypting_5() const { return ___encrypting_5; }
	inline bool* get_address_of_encrypting_5() { return &___encrypting_5; }
	inline void set_encrypting_5(bool value)
	{
		___encrypting_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBCBLOCKCIPHER_T7565E6778810D01349A96CDFEDDE39A56F47907F_H
#ifndef CCMBLOCKCIPHER_TE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_H
#define CCMBLOCKCIPHER_TE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher
struct  CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::cipher
	RuntimeObject* ___cipher_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::forEncryption
	bool ___forEncryption_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::macSize
	int32_t ___macSize_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::keyParam
	RuntimeObject* ___keyParam_7;
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::associatedText
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___associatedText_8;
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::data
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___data_9;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}

	inline static int32_t get_offset_of_macBlock_2() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___macBlock_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_2() const { return ___macBlock_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_2() { return &___macBlock_2; }
	inline void set_macBlock_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_2 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_2), value);
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_nonce_4() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___nonce_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_4() const { return ___nonce_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_4() { return &___nonce_4; }
	inline void set_nonce_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_4 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_4), value);
	}

	inline static int32_t get_offset_of_initialAssociatedText_5() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___initialAssociatedText_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_5() const { return ___initialAssociatedText_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_5() { return &___initialAssociatedText_5; }
	inline void set_initialAssociatedText_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_5), value);
	}

	inline static int32_t get_offset_of_macSize_6() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___macSize_6)); }
	inline int32_t get_macSize_6() const { return ___macSize_6; }
	inline int32_t* get_address_of_macSize_6() { return &___macSize_6; }
	inline void set_macSize_6(int32_t value)
	{
		___macSize_6 = value;
	}

	inline static int32_t get_offset_of_keyParam_7() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___keyParam_7)); }
	inline RuntimeObject* get_keyParam_7() const { return ___keyParam_7; }
	inline RuntimeObject** get_address_of_keyParam_7() { return &___keyParam_7; }
	inline void set_keyParam_7(RuntimeObject* value)
	{
		___keyParam_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyParam_7), value);
	}

	inline static int32_t get_offset_of_associatedText_8() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___associatedText_8)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_associatedText_8() const { return ___associatedText_8; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_associatedText_8() { return &___associatedText_8; }
	inline void set_associatedText_8(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___associatedText_8 = value;
		Il2CppCodeGenWriteBarrier((&___associatedText_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566, ___data_9)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_data_9() const { return ___data_9; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}
};

struct CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::BlockSize
	int32_t ___BlockSize_0;

public:
	inline static int32_t get_offset_of_BlockSize_0() { return static_cast<int32_t>(offsetof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_StaticFields, ___BlockSize_0)); }
	inline int32_t get_BlockSize_0() const { return ___BlockSize_0; }
	inline int32_t* get_address_of_BlockSize_0() { return &___BlockSize_0; }
	inline void set_BlockSize_0(int32_t value)
	{
		___BlockSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCMBLOCKCIPHER_TE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_H
#ifndef CFBBLOCKCIPHER_TD592CA890547E1C2332BA80349F15ABAF98277BF_H
#define CFBBLOCKCIPHER_TD592CA890547E1C2332BA80349F15ABAF98277BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher
struct  CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher::cfbV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cfbV_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher::cfbOutV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cfbOutV_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher::encrypting
	bool ___encrypting_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher::blockSize
	int32_t ___blockSize_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CfbBlockCipher::cipher
	RuntimeObject* ___cipher_5;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_cfbV_1() { return static_cast<int32_t>(offsetof(CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF, ___cfbV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cfbV_1() const { return ___cfbV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cfbV_1() { return &___cfbV_1; }
	inline void set_cfbV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cfbV_1 = value;
		Il2CppCodeGenWriteBarrier((&___cfbV_1), value);
	}

	inline static int32_t get_offset_of_cfbOutV_2() { return static_cast<int32_t>(offsetof(CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF, ___cfbOutV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cfbOutV_2() const { return ___cfbOutV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cfbOutV_2() { return &___cfbOutV_2; }
	inline void set_cfbOutV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cfbOutV_2 = value;
		Il2CppCodeGenWriteBarrier((&___cfbOutV_2), value);
	}

	inline static int32_t get_offset_of_encrypting_3() { return static_cast<int32_t>(offsetof(CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF, ___encrypting_3)); }
	inline bool get_encrypting_3() const { return ___encrypting_3; }
	inline bool* get_address_of_encrypting_3() { return &___encrypting_3; }
	inline void set_encrypting_3(bool value)
	{
		___encrypting_3 = value;
	}

	inline static int32_t get_offset_of_blockSize_4() { return static_cast<int32_t>(offsetof(CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF, ___blockSize_4)); }
	inline int32_t get_blockSize_4() const { return ___blockSize_4; }
	inline int32_t* get_address_of_blockSize_4() { return &___blockSize_4; }
	inline void set_blockSize_4(int32_t value)
	{
		___blockSize_4 = value;
	}

	inline static int32_t get_offset_of_cipher_5() { return static_cast<int32_t>(offsetof(CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF, ___cipher_5)); }
	inline RuntimeObject* get_cipher_5() const { return ___cipher_5; }
	inline RuntimeObject** get_address_of_cipher_5() { return &___cipher_5; }
	inline void set_cipher_5(RuntimeObject* value)
	{
		___cipher_5 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFBBLOCKCIPHER_TD592CA890547E1C2332BA80349F15ABAF98277BF_H
#ifndef EAXBLOCKCIPHER_T447B0C3BA5D6726C9807EE3606C7E1139C040565_H
#define EAXBLOCKCIPHER_T447B0C3BA5D6726C9807EE3606C7E1139C040565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher
struct  EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::cipher
	SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82 * ___cipher_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::forEncryption
	bool ___forEncryption_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::blockSize
	int32_t ___blockSize_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::mac
	RuntimeObject* ___mac_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::nonceMac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonceMac_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::associatedTextMac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___associatedTextMac_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::macSize
	int32_t ___macSize_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::bufBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBlock_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::bufOff
	int32_t ___bufOff_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::cipherInitialized
	bool ___cipherInitialized_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_11;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___cipher_0)); }
	inline SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82 * get_cipher_0() const { return ___cipher_0; }
	inline SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82 ** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82 * value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_forEncryption_1() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___forEncryption_1)); }
	inline bool get_forEncryption_1() const { return ___forEncryption_1; }
	inline bool* get_address_of_forEncryption_1() { return &___forEncryption_1; }
	inline void set_forEncryption_1(bool value)
	{
		___forEncryption_1 = value;
	}

	inline static int32_t get_offset_of_blockSize_2() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___blockSize_2)); }
	inline int32_t get_blockSize_2() const { return ___blockSize_2; }
	inline int32_t* get_address_of_blockSize_2() { return &___blockSize_2; }
	inline void set_blockSize_2(int32_t value)
	{
		___blockSize_2 = value;
	}

	inline static int32_t get_offset_of_mac_3() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___mac_3)); }
	inline RuntimeObject* get_mac_3() const { return ___mac_3; }
	inline RuntimeObject** get_address_of_mac_3() { return &___mac_3; }
	inline void set_mac_3(RuntimeObject* value)
	{
		___mac_3 = value;
		Il2CppCodeGenWriteBarrier((&___mac_3), value);
	}

	inline static int32_t get_offset_of_nonceMac_4() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___nonceMac_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonceMac_4() const { return ___nonceMac_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonceMac_4() { return &___nonceMac_4; }
	inline void set_nonceMac_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonceMac_4 = value;
		Il2CppCodeGenWriteBarrier((&___nonceMac_4), value);
	}

	inline static int32_t get_offset_of_associatedTextMac_5() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___associatedTextMac_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_associatedTextMac_5() const { return ___associatedTextMac_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_associatedTextMac_5() { return &___associatedTextMac_5; }
	inline void set_associatedTextMac_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___associatedTextMac_5 = value;
		Il2CppCodeGenWriteBarrier((&___associatedTextMac_5), value);
	}

	inline static int32_t get_offset_of_macBlock_6() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___macBlock_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_6() const { return ___macBlock_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_6() { return &___macBlock_6; }
	inline void set_macBlock_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_6), value);
	}

	inline static int32_t get_offset_of_macSize_7() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___macSize_7)); }
	inline int32_t get_macSize_7() const { return ___macSize_7; }
	inline int32_t* get_address_of_macSize_7() { return &___macSize_7; }
	inline void set_macSize_7(int32_t value)
	{
		___macSize_7 = value;
	}

	inline static int32_t get_offset_of_bufBlock_8() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___bufBlock_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBlock_8() const { return ___bufBlock_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBlock_8() { return &___bufBlock_8; }
	inline void set_bufBlock_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___bufBlock_8), value);
	}

	inline static int32_t get_offset_of_bufOff_9() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___bufOff_9)); }
	inline int32_t get_bufOff_9() const { return ___bufOff_9; }
	inline int32_t* get_address_of_bufOff_9() { return &___bufOff_9; }
	inline void set_bufOff_9(int32_t value)
	{
		___bufOff_9 = value;
	}

	inline static int32_t get_offset_of_cipherInitialized_10() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___cipherInitialized_10)); }
	inline bool get_cipherInitialized_10() const { return ___cipherInitialized_10; }
	inline bool* get_address_of_cipherInitialized_10() { return &___cipherInitialized_10; }
	inline void set_cipherInitialized_10(bool value)
	{
		___cipherInitialized_10 = value;
	}

	inline static int32_t get_offset_of_initialAssociatedText_11() { return static_cast<int32_t>(offsetof(EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565, ___initialAssociatedText_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_11() const { return ___initialAssociatedText_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_11() { return &___initialAssociatedText_11; }
	inline void set_initialAssociatedText_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_11 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EAXBLOCKCIPHER_T447B0C3BA5D6726C9807EE3606C7E1139C040565_H
#ifndef GOFBBLOCKCIPHER_T19C1560AF8627473DB62AA470715100C5A17312F_H
#define GOFBBLOCKCIPHER_T19C1560AF8627473DB62AA470715100C5A17312F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher
struct  GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::ofbV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ofbV_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::ofbOutV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ofbOutV_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::blockSize
	int32_t ___blockSize_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::cipher
	RuntimeObject* ___cipher_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::firstStep
	bool ___firstStep_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::N3
	int32_t ___N3_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GOfbBlockCipher::N4
	int32_t ___N4_7;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_ofbV_1() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___ofbV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ofbV_1() const { return ___ofbV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ofbV_1() { return &___ofbV_1; }
	inline void set_ofbV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ofbV_1 = value;
		Il2CppCodeGenWriteBarrier((&___ofbV_1), value);
	}

	inline static int32_t get_offset_of_ofbOutV_2() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___ofbOutV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ofbOutV_2() const { return ___ofbOutV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ofbOutV_2() { return &___ofbOutV_2; }
	inline void set_ofbOutV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ofbOutV_2 = value;
		Il2CppCodeGenWriteBarrier((&___ofbOutV_2), value);
	}

	inline static int32_t get_offset_of_blockSize_3() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___blockSize_3)); }
	inline int32_t get_blockSize_3() const { return ___blockSize_3; }
	inline int32_t* get_address_of_blockSize_3() { return &___blockSize_3; }
	inline void set_blockSize_3(int32_t value)
	{
		___blockSize_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}

	inline static int32_t get_offset_of_firstStep_5() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___firstStep_5)); }
	inline bool get_firstStep_5() const { return ___firstStep_5; }
	inline bool* get_address_of_firstStep_5() { return &___firstStep_5; }
	inline void set_firstStep_5(bool value)
	{
		___firstStep_5 = value;
	}

	inline static int32_t get_offset_of_N3_6() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___N3_6)); }
	inline int32_t get_N3_6() const { return ___N3_6; }
	inline int32_t* get_address_of_N3_6() { return &___N3_6; }
	inline void set_N3_6(int32_t value)
	{
		___N3_6 = value;
	}

	inline static int32_t get_offset_of_N4_7() { return static_cast<int32_t>(offsetof(GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F, ___N4_7)); }
	inline int32_t get_N4_7() const { return ___N4_7; }
	inline int32_t* get_address_of_N4_7() { return &___N4_7; }
	inline void set_N4_7(int32_t value)
	{
		___N4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOFBBLOCKCIPHER_T19C1560AF8627473DB62AA470715100C5A17312F_H
#ifndef BASICGCMEXPONENTIATOR_T98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9_H
#define BASICGCMEXPONENTIATOR_T98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.BasicGcmExponentiator
struct  BasicGcmExponentiator_t98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.BasicGcmExponentiator::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(BasicGcmExponentiator_t98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9, ___x_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_0() const { return ___x_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICGCMEXPONENTIATOR_T98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9_H
#ifndef BASICGCMMULTIPLIER_TB23AC3A27441B45DA64CCDD14F6D39CC16071F8F_H
#define BASICGCMMULTIPLIER_TB23AC3A27441B45DA64CCDD14F6D39CC16071F8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.BasicGcmMultiplier
struct  BasicGcmMultiplier_tB23AC3A27441B45DA64CCDD14F6D39CC16071F8F  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.BasicGcmMultiplier::H
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___H_0;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(BasicGcmMultiplier_tB23AC3A27441B45DA64CCDD14F6D39CC16071F8F, ___H_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_H_0() const { return ___H_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___H_0 = value;
		Il2CppCodeGenWriteBarrier((&___H_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICGCMMULTIPLIER_TB23AC3A27441B45DA64CCDD14F6D39CC16071F8F_H
#ifndef GCMUTILITIES_TDFE47B9588D38C941F589EB8E98623F967ADA5E5_H
#define GCMUTILITIES_TDFE47B9588D38C941F589EB8E98623F967ADA5E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.GcmUtilities
struct  GcmUtilities_tDFE47B9588D38C941F589EB8E98623F967ADA5E5  : public RuntimeObject
{
public:

public:
};

struct GcmUtilities_tDFE47B9588D38C941F589EB8E98623F967ADA5E5_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.GcmUtilities::LOOKUP
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___LOOKUP_2;

public:
	inline static int32_t get_offset_of_LOOKUP_2() { return static_cast<int32_t>(offsetof(GcmUtilities_tDFE47B9588D38C941F589EB8E98623F967ADA5E5_StaticFields, ___LOOKUP_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_LOOKUP_2() const { return ___LOOKUP_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_LOOKUP_2() { return &___LOOKUP_2; }
	inline void set_LOOKUP_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___LOOKUP_2 = value;
		Il2CppCodeGenWriteBarrier((&___LOOKUP_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCMUTILITIES_TDFE47B9588D38C941F589EB8E98623F967ADA5E5_H
#ifndef TABLES1KGCMEXPONENTIATOR_T44381C85CB7FA23565458D84B1B13B4A9E4039BB_H
#define TABLES1KGCMEXPONENTIATOR_T44381C85CB7FA23565458D84B1B13B4A9E4039BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables1kGcmExponentiator
struct  Tables1kGcmExponentiator_t44381C85CB7FA23565458D84B1B13B4A9E4039BB  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables1kGcmExponentiator::lookupPowX2
	RuntimeObject* ___lookupPowX2_0;

public:
	inline static int32_t get_offset_of_lookupPowX2_0() { return static_cast<int32_t>(offsetof(Tables1kGcmExponentiator_t44381C85CB7FA23565458D84B1B13B4A9E4039BB, ___lookupPowX2_0)); }
	inline RuntimeObject* get_lookupPowX2_0() const { return ___lookupPowX2_0; }
	inline RuntimeObject** get_address_of_lookupPowX2_0() { return &___lookupPowX2_0; }
	inline void set_lookupPowX2_0(RuntimeObject* value)
	{
		___lookupPowX2_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPowX2_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLES1KGCMEXPONENTIATOR_T44381C85CB7FA23565458D84B1B13B4A9E4039BB_H
#ifndef TABLES64KGCMMULTIPLIER_TCCCFA1324D15CAC32A87B008277969FDBB67FAD1_H
#define TABLES64KGCMMULTIPLIER_TCCCFA1324D15CAC32A87B008277969FDBB67FAD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables64kGcmMultiplier
struct  Tables64kGcmMultiplier_tCCCFA1324D15CAC32A87B008277969FDBB67FAD1  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables64kGcmMultiplier::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_0;
	// System.UInt32[][][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables64kGcmMultiplier::M
	UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D* ___M_1;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(Tables64kGcmMultiplier_tCCCFA1324D15CAC32A87B008277969FDBB67FAD1, ___H_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_0() const { return ___H_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_0 = value;
		Il2CppCodeGenWriteBarrier((&___H_0), value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(Tables64kGcmMultiplier_tCCCFA1324D15CAC32A87B008277969FDBB67FAD1, ___M_1)); }
	inline UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D* get_M_1() const { return ___M_1; }
	inline UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D* value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier((&___M_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLES64KGCMMULTIPLIER_TCCCFA1324D15CAC32A87B008277969FDBB67FAD1_H
#ifndef TABLES8KGCMMULTIPLIER_TB21DA22C4B09E018603436F510743ACAB692CDEA_H
#define TABLES8KGCMMULTIPLIER_TB21DA22C4B09E018603436F510743ACAB692CDEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier
struct  Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_0;
	// System.UInt32[][][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier::M
	UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D* ___M_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier::z
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___z_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA, ___H_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_0() const { return ___H_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_0 = value;
		Il2CppCodeGenWriteBarrier((&___H_0), value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA, ___M_1)); }
	inline UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D* get_M_1() const { return ___M_1; }
	inline UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(UInt32U5BU5DU5BU5DU5BU5D_t2A16B10701C79B68EF4BDC725B7BD4D55301615D* value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier((&___M_1), value);
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA, ___z_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_z_2() const { return ___z_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___z_2 = value;
		Il2CppCodeGenWriteBarrier((&___z_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLES8KGCMMULTIPLIER_TB21DA22C4B09E018603436F510743ACAB692CDEA_H
#ifndef GCMBLOCKCIPHER_T2043D4B231A20F63B6ACA49A27AA3898F1DD1617_H
#define GCMBLOCKCIPHER_T2043D4B231A20F63B6ACA49A27AA3898F1DD1617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher
struct  GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::ctrBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ctrBlock_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::cipher
	RuntimeObject* ___cipher_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.IGcmMultiplier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::multiplier
	RuntimeObject* ___multiplier_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.Gcm.IGcmExponentiator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::exp
	RuntimeObject* ___exp_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::forEncryption
	bool ___forEncryption_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::initialised
	bool ___initialised_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::macSize
	int32_t ___macSize_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::lastKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___lastKey_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::J0
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___J0_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::bufBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBlock_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::S_at
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_at_16;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::S_atPre
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_atPre_17;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::counter
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counter_18;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::blocksRemaining
	uint32_t ___blocksRemaining_19;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::bufOff
	int32_t ___bufOff_20;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::totalLength
	uint64_t ___totalLength_21;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___atBlock_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atBlockPos
	int32_t ___atBlockPos_23;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atLength
	uint64_t ___atLength_24;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atLengthPre
	uint64_t ___atLengthPre_25;

public:
	inline static int32_t get_offset_of_ctrBlock_1() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___ctrBlock_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ctrBlock_1() const { return ___ctrBlock_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ctrBlock_1() { return &___ctrBlock_1; }
	inline void set_ctrBlock_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ctrBlock_1 = value;
		Il2CppCodeGenWriteBarrier((&___ctrBlock_1), value);
	}

	inline static int32_t get_offset_of_cipher_2() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___cipher_2)); }
	inline RuntimeObject* get_cipher_2() const { return ___cipher_2; }
	inline RuntimeObject** get_address_of_cipher_2() { return &___cipher_2; }
	inline void set_cipher_2(RuntimeObject* value)
	{
		___cipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_2), value);
	}

	inline static int32_t get_offset_of_multiplier_3() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___multiplier_3)); }
	inline RuntimeObject* get_multiplier_3() const { return ___multiplier_3; }
	inline RuntimeObject** get_address_of_multiplier_3() { return &___multiplier_3; }
	inline void set_multiplier_3(RuntimeObject* value)
	{
		___multiplier_3 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_3), value);
	}

	inline static int32_t get_offset_of_exp_4() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___exp_4)); }
	inline RuntimeObject* get_exp_4() const { return ___exp_4; }
	inline RuntimeObject** get_address_of_exp_4() { return &___exp_4; }
	inline void set_exp_4(RuntimeObject* value)
	{
		___exp_4 = value;
		Il2CppCodeGenWriteBarrier((&___exp_4), value);
	}

	inline static int32_t get_offset_of_forEncryption_5() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___forEncryption_5)); }
	inline bool get_forEncryption_5() const { return ___forEncryption_5; }
	inline bool* get_address_of_forEncryption_5() { return &___forEncryption_5; }
	inline void set_forEncryption_5(bool value)
	{
		___forEncryption_5 = value;
	}

	inline static int32_t get_offset_of_initialised_6() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___initialised_6)); }
	inline bool get_initialised_6() const { return ___initialised_6; }
	inline bool* get_address_of_initialised_6() { return &___initialised_6; }
	inline void set_initialised_6(bool value)
	{
		___initialised_6 = value;
	}

	inline static int32_t get_offset_of_macSize_7() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___macSize_7)); }
	inline int32_t get_macSize_7() const { return ___macSize_7; }
	inline int32_t* get_address_of_macSize_7() { return &___macSize_7; }
	inline void set_macSize_7(int32_t value)
	{
		___macSize_7 = value;
	}

	inline static int32_t get_offset_of_lastKey_8() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___lastKey_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_lastKey_8() const { return ___lastKey_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_lastKey_8() { return &___lastKey_8; }
	inline void set_lastKey_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___lastKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastKey_8), value);
	}

	inline static int32_t get_offset_of_nonce_9() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___nonce_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_9() const { return ___nonce_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_9() { return &___nonce_9; }
	inline void set_nonce_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_9 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_9), value);
	}

	inline static int32_t get_offset_of_initialAssociatedText_10() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___initialAssociatedText_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_10() const { return ___initialAssociatedText_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_10() { return &___initialAssociatedText_10; }
	inline void set_initialAssociatedText_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_10 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_10), value);
	}

	inline static int32_t get_offset_of_H_11() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___H_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_11() const { return ___H_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_11() { return &___H_11; }
	inline void set_H_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_11 = value;
		Il2CppCodeGenWriteBarrier((&___H_11), value);
	}

	inline static int32_t get_offset_of_J0_12() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___J0_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_J0_12() const { return ___J0_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_J0_12() { return &___J0_12; }
	inline void set_J0_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___J0_12 = value;
		Il2CppCodeGenWriteBarrier((&___J0_12), value);
	}

	inline static int32_t get_offset_of_bufBlock_13() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___bufBlock_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBlock_13() const { return ___bufBlock_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBlock_13() { return &___bufBlock_13; }
	inline void set_bufBlock_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBlock_13 = value;
		Il2CppCodeGenWriteBarrier((&___bufBlock_13), value);
	}

	inline static int32_t get_offset_of_macBlock_14() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___macBlock_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_14() const { return ___macBlock_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_14() { return &___macBlock_14; }
	inline void set_macBlock_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_14 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_14), value);
	}

	inline static int32_t get_offset_of_S_15() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___S_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_15() const { return ___S_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_15() { return &___S_15; }
	inline void set_S_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_15 = value;
		Il2CppCodeGenWriteBarrier((&___S_15), value);
	}

	inline static int32_t get_offset_of_S_at_16() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___S_at_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_at_16() const { return ___S_at_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_at_16() { return &___S_at_16; }
	inline void set_S_at_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_at_16 = value;
		Il2CppCodeGenWriteBarrier((&___S_at_16), value);
	}

	inline static int32_t get_offset_of_S_atPre_17() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___S_atPre_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_atPre_17() const { return ___S_atPre_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_atPre_17() { return &___S_atPre_17; }
	inline void set_S_atPre_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_atPre_17 = value;
		Il2CppCodeGenWriteBarrier((&___S_atPre_17), value);
	}

	inline static int32_t get_offset_of_counter_18() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___counter_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counter_18() const { return ___counter_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counter_18() { return &___counter_18; }
	inline void set_counter_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counter_18 = value;
		Il2CppCodeGenWriteBarrier((&___counter_18), value);
	}

	inline static int32_t get_offset_of_blocksRemaining_19() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___blocksRemaining_19)); }
	inline uint32_t get_blocksRemaining_19() const { return ___blocksRemaining_19; }
	inline uint32_t* get_address_of_blocksRemaining_19() { return &___blocksRemaining_19; }
	inline void set_blocksRemaining_19(uint32_t value)
	{
		___blocksRemaining_19 = value;
	}

	inline static int32_t get_offset_of_bufOff_20() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___bufOff_20)); }
	inline int32_t get_bufOff_20() const { return ___bufOff_20; }
	inline int32_t* get_address_of_bufOff_20() { return &___bufOff_20; }
	inline void set_bufOff_20(int32_t value)
	{
		___bufOff_20 = value;
	}

	inline static int32_t get_offset_of_totalLength_21() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___totalLength_21)); }
	inline uint64_t get_totalLength_21() const { return ___totalLength_21; }
	inline uint64_t* get_address_of_totalLength_21() { return &___totalLength_21; }
	inline void set_totalLength_21(uint64_t value)
	{
		___totalLength_21 = value;
	}

	inline static int32_t get_offset_of_atBlock_22() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___atBlock_22)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_atBlock_22() const { return ___atBlock_22; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_atBlock_22() { return &___atBlock_22; }
	inline void set_atBlock_22(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___atBlock_22 = value;
		Il2CppCodeGenWriteBarrier((&___atBlock_22), value);
	}

	inline static int32_t get_offset_of_atBlockPos_23() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___atBlockPos_23)); }
	inline int32_t get_atBlockPos_23() const { return ___atBlockPos_23; }
	inline int32_t* get_address_of_atBlockPos_23() { return &___atBlockPos_23; }
	inline void set_atBlockPos_23(int32_t value)
	{
		___atBlockPos_23 = value;
	}

	inline static int32_t get_offset_of_atLength_24() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___atLength_24)); }
	inline uint64_t get_atLength_24() const { return ___atLength_24; }
	inline uint64_t* get_address_of_atLength_24() { return &___atLength_24; }
	inline void set_atLength_24(uint64_t value)
	{
		___atLength_24 = value;
	}

	inline static int32_t get_offset_of_atLengthPre_25() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617, ___atLengthPre_25)); }
	inline uint64_t get_atLengthPre_25() const { return ___atLengthPre_25; }
	inline uint64_t* get_address_of_atLengthPre_25() { return &___atLengthPre_25; }
	inline void set_atLengthPre_25(uint64_t value)
	{
		___atLengthPre_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCMBLOCKCIPHER_T2043D4B231A20F63B6ACA49A27AA3898F1DD1617_H
#ifndef KCCMBLOCKCIPHER_TC7D34D53F34E965A43F0EA7CDA363C3A97A88900_H
#define KCCMBLOCKCIPHER_TC7D34D53F34E965A43F0EA7CDA363C3A97A88900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher
struct  KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::engine
	RuntimeObject* ___engine_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::macSize
	int32_t ___macSize_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::forEncryption
	bool ___forEncryption_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::G1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___G1_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::s
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::counter
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counter_14;
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::associatedText
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___associatedText_15;
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::data
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___data_16;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::Nb_
	int32_t ___Nb__17;

public:
	inline static int32_t get_offset_of_engine_4() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___engine_4)); }
	inline RuntimeObject* get_engine_4() const { return ___engine_4; }
	inline RuntimeObject** get_address_of_engine_4() { return &___engine_4; }
	inline void set_engine_4(RuntimeObject* value)
	{
		___engine_4 = value;
		Il2CppCodeGenWriteBarrier((&___engine_4), value);
	}

	inline static int32_t get_offset_of_macSize_5() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___macSize_5)); }
	inline int32_t get_macSize_5() const { return ___macSize_5; }
	inline int32_t* get_address_of_macSize_5() { return &___macSize_5; }
	inline void set_macSize_5(int32_t value)
	{
		___macSize_5 = value;
	}

	inline static int32_t get_offset_of_forEncryption_6() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___forEncryption_6)); }
	inline bool get_forEncryption_6() const { return ___forEncryption_6; }
	inline bool* get_address_of_forEncryption_6() { return &___forEncryption_6; }
	inline void set_forEncryption_6(bool value)
	{
		___forEncryption_6 = value;
	}

	inline static int32_t get_offset_of_initialAssociatedText_7() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___initialAssociatedText_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_7() const { return ___initialAssociatedText_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_7() { return &___initialAssociatedText_7; }
	inline void set_initialAssociatedText_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_7 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_7), value);
	}

	inline static int32_t get_offset_of_mac_8() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___mac_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_8() const { return ___mac_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_8() { return &___mac_8; }
	inline void set_mac_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_8 = value;
		Il2CppCodeGenWriteBarrier((&___mac_8), value);
	}

	inline static int32_t get_offset_of_macBlock_9() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___macBlock_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_9() const { return ___macBlock_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_9() { return &___macBlock_9; }
	inline void set_macBlock_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_9 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_9), value);
	}

	inline static int32_t get_offset_of_nonce_10() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___nonce_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_10() const { return ___nonce_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_10() { return &___nonce_10; }
	inline void set_nonce_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_10 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_10), value);
	}

	inline static int32_t get_offset_of_G1_11() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___G1_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_G1_11() const { return ___G1_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_G1_11() { return &___G1_11; }
	inline void set_G1_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___G1_11 = value;
		Il2CppCodeGenWriteBarrier((&___G1_11), value);
	}

	inline static int32_t get_offset_of_buffer_12() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___buffer_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_12() const { return ___buffer_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_12() { return &___buffer_12; }
	inline void set_buffer_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_12 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_12), value);
	}

	inline static int32_t get_offset_of_s_13() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___s_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_13() const { return ___s_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_13() { return &___s_13; }
	inline void set_s_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_13), value);
	}

	inline static int32_t get_offset_of_counter_14() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___counter_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counter_14() const { return ___counter_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counter_14() { return &___counter_14; }
	inline void set_counter_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counter_14 = value;
		Il2CppCodeGenWriteBarrier((&___counter_14), value);
	}

	inline static int32_t get_offset_of_associatedText_15() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___associatedText_15)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_associatedText_15() const { return ___associatedText_15; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_associatedText_15() { return &___associatedText_15; }
	inline void set_associatedText_15(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___associatedText_15 = value;
		Il2CppCodeGenWriteBarrier((&___associatedText_15), value);
	}

	inline static int32_t get_offset_of_data_16() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___data_16)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_data_16() const { return ___data_16; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_data_16() { return &___data_16; }
	inline void set_data_16(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___data_16 = value;
		Il2CppCodeGenWriteBarrier((&___data_16), value);
	}

	inline static int32_t get_offset_of_Nb__17() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900, ___Nb__17)); }
	inline int32_t get_Nb__17() const { return ___Nb__17; }
	inline int32_t* get_address_of_Nb__17() { return &___Nb__17; }
	inline void set_Nb__17(int32_t value)
	{
		___Nb__17 = value;
	}
};

struct KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::BYTES_IN_INT
	int32_t ___BYTES_IN_INT_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::BITS_IN_BYTE
	int32_t ___BITS_IN_BYTE_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::MAX_MAC_BIT_LENGTH
	int32_t ___MAX_MAC_BIT_LENGTH_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCcmBlockCipher::MIN_MAC_BIT_LENGTH
	int32_t ___MIN_MAC_BIT_LENGTH_3;

public:
	inline static int32_t get_offset_of_BYTES_IN_INT_0() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields, ___BYTES_IN_INT_0)); }
	inline int32_t get_BYTES_IN_INT_0() const { return ___BYTES_IN_INT_0; }
	inline int32_t* get_address_of_BYTES_IN_INT_0() { return &___BYTES_IN_INT_0; }
	inline void set_BYTES_IN_INT_0(int32_t value)
	{
		___BYTES_IN_INT_0 = value;
	}

	inline static int32_t get_offset_of_BITS_IN_BYTE_1() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields, ___BITS_IN_BYTE_1)); }
	inline int32_t get_BITS_IN_BYTE_1() const { return ___BITS_IN_BYTE_1; }
	inline int32_t* get_address_of_BITS_IN_BYTE_1() { return &___BITS_IN_BYTE_1; }
	inline void set_BITS_IN_BYTE_1(int32_t value)
	{
		___BITS_IN_BYTE_1 = value;
	}

	inline static int32_t get_offset_of_MAX_MAC_BIT_LENGTH_2() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields, ___MAX_MAC_BIT_LENGTH_2)); }
	inline int32_t get_MAX_MAC_BIT_LENGTH_2() const { return ___MAX_MAC_BIT_LENGTH_2; }
	inline int32_t* get_address_of_MAX_MAC_BIT_LENGTH_2() { return &___MAX_MAC_BIT_LENGTH_2; }
	inline void set_MAX_MAC_BIT_LENGTH_2(int32_t value)
	{
		___MAX_MAC_BIT_LENGTH_2 = value;
	}

	inline static int32_t get_offset_of_MIN_MAC_BIT_LENGTH_3() { return static_cast<int32_t>(offsetof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields, ___MIN_MAC_BIT_LENGTH_3)); }
	inline int32_t get_MIN_MAC_BIT_LENGTH_3() const { return ___MIN_MAC_BIT_LENGTH_3; }
	inline int32_t* get_address_of_MIN_MAC_BIT_LENGTH_3() { return &___MIN_MAC_BIT_LENGTH_3; }
	inline void set_MIN_MAC_BIT_LENGTH_3(int32_t value)
	{
		___MIN_MAC_BIT_LENGTH_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KCCMBLOCKCIPHER_TC7D34D53F34E965A43F0EA7CDA363C3A97A88900_H
#ifndef KCTRBLOCKCIPHER_TF7A921C1AAE447AE33B498257D85EFDBFEED4D3E_H
#define KCTRBLOCKCIPHER_TF7A921C1AAE447AE33B498257D85EFDBFEED4D3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher
struct  KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::ofbV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ofbV_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::ofbOutV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ofbOutV_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::initialised
	bool ___initialised_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::byteCount
	int32_t ___byteCount_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::blockSize
	int32_t ___blockSize_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.KCtrBlockCipher::cipher
	RuntimeObject* ___cipher_6;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_ofbV_1() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___ofbV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ofbV_1() const { return ___ofbV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ofbV_1() { return &___ofbV_1; }
	inline void set_ofbV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ofbV_1 = value;
		Il2CppCodeGenWriteBarrier((&___ofbV_1), value);
	}

	inline static int32_t get_offset_of_ofbOutV_2() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___ofbOutV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ofbOutV_2() const { return ___ofbOutV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ofbOutV_2() { return &___ofbOutV_2; }
	inline void set_ofbOutV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ofbOutV_2 = value;
		Il2CppCodeGenWriteBarrier((&___ofbOutV_2), value);
	}

	inline static int32_t get_offset_of_initialised_3() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___initialised_3)); }
	inline bool get_initialised_3() const { return ___initialised_3; }
	inline bool* get_address_of_initialised_3() { return &___initialised_3; }
	inline void set_initialised_3(bool value)
	{
		___initialised_3 = value;
	}

	inline static int32_t get_offset_of_byteCount_4() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___byteCount_4)); }
	inline int32_t get_byteCount_4() const { return ___byteCount_4; }
	inline int32_t* get_address_of_byteCount_4() { return &___byteCount_4; }
	inline void set_byteCount_4(int32_t value)
	{
		___byteCount_4 = value;
	}

	inline static int32_t get_offset_of_blockSize_5() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___blockSize_5)); }
	inline int32_t get_blockSize_5() const { return ___blockSize_5; }
	inline int32_t* get_address_of_blockSize_5() { return &___blockSize_5; }
	inline void set_blockSize_5(int32_t value)
	{
		___blockSize_5 = value;
	}

	inline static int32_t get_offset_of_cipher_6() { return static_cast<int32_t>(offsetof(KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E, ___cipher_6)); }
	inline RuntimeObject* get_cipher_6() const { return ___cipher_6; }
	inline RuntimeObject** get_address_of_cipher_6() { return &___cipher_6; }
	inline void set_cipher_6(RuntimeObject* value)
	{
		___cipher_6 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KCTRBLOCKCIPHER_TF7A921C1AAE447AE33B498257D85EFDBFEED4D3E_H
#ifndef OCBBLOCKCIPHER_TF3ECF68C06A035387844499BD6542DC461D2B041_H
#define OCBBLOCKCIPHER_TF3ECF68C06A035387844499BD6542DC461D2B041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher
struct  OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::hashCipher
	RuntimeObject* ___hashCipher_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::mainCipher
	RuntimeObject* ___mainCipher_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::forEncryption
	bool ___forEncryption_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::macSize
	int32_t ___macSize_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_5;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::L
	RuntimeObject* ___L_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::L_Asterisk
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___L_Asterisk_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::L_Dollar
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___L_Dollar_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::KtopInput
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___KtopInput_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::Stretch
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Stretch_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::OffsetMAIN_0
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OffsetMAIN_0_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::hashBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___hashBlock_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::mainBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mainBlock_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::hashBlockPos
	int32_t ___hashBlockPos_14;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::mainBlockPos
	int32_t ___mainBlockPos_15;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::hashBlockCount
	int64_t ___hashBlockCount_16;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::mainBlockCount
	int64_t ___mainBlockCount_17;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::OffsetHASH
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OffsetHASH_18;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::Sum
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sum_19;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::OffsetMAIN
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OffsetMAIN_20;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::Checksum
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Checksum_21;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OcbBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_22;

public:
	inline static int32_t get_offset_of_hashCipher_1() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___hashCipher_1)); }
	inline RuntimeObject* get_hashCipher_1() const { return ___hashCipher_1; }
	inline RuntimeObject** get_address_of_hashCipher_1() { return &___hashCipher_1; }
	inline void set_hashCipher_1(RuntimeObject* value)
	{
		___hashCipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___hashCipher_1), value);
	}

	inline static int32_t get_offset_of_mainCipher_2() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___mainCipher_2)); }
	inline RuntimeObject* get_mainCipher_2() const { return ___mainCipher_2; }
	inline RuntimeObject** get_address_of_mainCipher_2() { return &___mainCipher_2; }
	inline void set_mainCipher_2(RuntimeObject* value)
	{
		___mainCipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCipher_2), value);
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_macSize_4() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___macSize_4)); }
	inline int32_t get_macSize_4() const { return ___macSize_4; }
	inline int32_t* get_address_of_macSize_4() { return &___macSize_4; }
	inline void set_macSize_4(int32_t value)
	{
		___macSize_4 = value;
	}

	inline static int32_t get_offset_of_initialAssociatedText_5() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___initialAssociatedText_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_5() const { return ___initialAssociatedText_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_5() { return &___initialAssociatedText_5; }
	inline void set_initialAssociatedText_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_5), value);
	}

	inline static int32_t get_offset_of_L_6() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___L_6)); }
	inline RuntimeObject* get_L_6() const { return ___L_6; }
	inline RuntimeObject** get_address_of_L_6() { return &___L_6; }
	inline void set_L_6(RuntimeObject* value)
	{
		___L_6 = value;
		Il2CppCodeGenWriteBarrier((&___L_6), value);
	}

	inline static int32_t get_offset_of_L_Asterisk_7() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___L_Asterisk_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_L_Asterisk_7() const { return ___L_Asterisk_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_L_Asterisk_7() { return &___L_Asterisk_7; }
	inline void set_L_Asterisk_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___L_Asterisk_7 = value;
		Il2CppCodeGenWriteBarrier((&___L_Asterisk_7), value);
	}

	inline static int32_t get_offset_of_L_Dollar_8() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___L_Dollar_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_L_Dollar_8() const { return ___L_Dollar_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_L_Dollar_8() { return &___L_Dollar_8; }
	inline void set_L_Dollar_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___L_Dollar_8 = value;
		Il2CppCodeGenWriteBarrier((&___L_Dollar_8), value);
	}

	inline static int32_t get_offset_of_KtopInput_9() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___KtopInput_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_KtopInput_9() const { return ___KtopInput_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_KtopInput_9() { return &___KtopInput_9; }
	inline void set_KtopInput_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___KtopInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___KtopInput_9), value);
	}

	inline static int32_t get_offset_of_Stretch_10() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___Stretch_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Stretch_10() const { return ___Stretch_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Stretch_10() { return &___Stretch_10; }
	inline void set_Stretch_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Stretch_10 = value;
		Il2CppCodeGenWriteBarrier((&___Stretch_10), value);
	}

	inline static int32_t get_offset_of_OffsetMAIN_0_11() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___OffsetMAIN_0_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OffsetMAIN_0_11() const { return ___OffsetMAIN_0_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OffsetMAIN_0_11() { return &___OffsetMAIN_0_11; }
	inline void set_OffsetMAIN_0_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OffsetMAIN_0_11 = value;
		Il2CppCodeGenWriteBarrier((&___OffsetMAIN_0_11), value);
	}

	inline static int32_t get_offset_of_hashBlock_12() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___hashBlock_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_hashBlock_12() const { return ___hashBlock_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_hashBlock_12() { return &___hashBlock_12; }
	inline void set_hashBlock_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___hashBlock_12 = value;
		Il2CppCodeGenWriteBarrier((&___hashBlock_12), value);
	}

	inline static int32_t get_offset_of_mainBlock_13() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___mainBlock_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mainBlock_13() const { return ___mainBlock_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mainBlock_13() { return &___mainBlock_13; }
	inline void set_mainBlock_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mainBlock_13 = value;
		Il2CppCodeGenWriteBarrier((&___mainBlock_13), value);
	}

	inline static int32_t get_offset_of_hashBlockPos_14() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___hashBlockPos_14)); }
	inline int32_t get_hashBlockPos_14() const { return ___hashBlockPos_14; }
	inline int32_t* get_address_of_hashBlockPos_14() { return &___hashBlockPos_14; }
	inline void set_hashBlockPos_14(int32_t value)
	{
		___hashBlockPos_14 = value;
	}

	inline static int32_t get_offset_of_mainBlockPos_15() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___mainBlockPos_15)); }
	inline int32_t get_mainBlockPos_15() const { return ___mainBlockPos_15; }
	inline int32_t* get_address_of_mainBlockPos_15() { return &___mainBlockPos_15; }
	inline void set_mainBlockPos_15(int32_t value)
	{
		___mainBlockPos_15 = value;
	}

	inline static int32_t get_offset_of_hashBlockCount_16() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___hashBlockCount_16)); }
	inline int64_t get_hashBlockCount_16() const { return ___hashBlockCount_16; }
	inline int64_t* get_address_of_hashBlockCount_16() { return &___hashBlockCount_16; }
	inline void set_hashBlockCount_16(int64_t value)
	{
		___hashBlockCount_16 = value;
	}

	inline static int32_t get_offset_of_mainBlockCount_17() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___mainBlockCount_17)); }
	inline int64_t get_mainBlockCount_17() const { return ___mainBlockCount_17; }
	inline int64_t* get_address_of_mainBlockCount_17() { return &___mainBlockCount_17; }
	inline void set_mainBlockCount_17(int64_t value)
	{
		___mainBlockCount_17 = value;
	}

	inline static int32_t get_offset_of_OffsetHASH_18() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___OffsetHASH_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OffsetHASH_18() const { return ___OffsetHASH_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OffsetHASH_18() { return &___OffsetHASH_18; }
	inline void set_OffsetHASH_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OffsetHASH_18 = value;
		Il2CppCodeGenWriteBarrier((&___OffsetHASH_18), value);
	}

	inline static int32_t get_offset_of_Sum_19() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___Sum_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sum_19() const { return ___Sum_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sum_19() { return &___Sum_19; }
	inline void set_Sum_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sum_19 = value;
		Il2CppCodeGenWriteBarrier((&___Sum_19), value);
	}

	inline static int32_t get_offset_of_OffsetMAIN_20() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___OffsetMAIN_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OffsetMAIN_20() const { return ___OffsetMAIN_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OffsetMAIN_20() { return &___OffsetMAIN_20; }
	inline void set_OffsetMAIN_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OffsetMAIN_20 = value;
		Il2CppCodeGenWriteBarrier((&___OffsetMAIN_20), value);
	}

	inline static int32_t get_offset_of_Checksum_21() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___Checksum_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Checksum_21() const { return ___Checksum_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Checksum_21() { return &___Checksum_21; }
	inline void set_Checksum_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Checksum_21 = value;
		Il2CppCodeGenWriteBarrier((&___Checksum_21), value);
	}

	inline static int32_t get_offset_of_macBlock_22() { return static_cast<int32_t>(offsetof(OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041, ___macBlock_22)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_22() const { return ___macBlock_22; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_22() { return &___macBlock_22; }
	inline void set_macBlock_22(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_22 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCBBLOCKCIPHER_TF3ECF68C06A035387844499BD6542DC461D2B041_H
#ifndef OFBBLOCKCIPHER_T4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A_H
#define OFBBLOCKCIPHER_T4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OfbBlockCipher
struct  OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OfbBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OfbBlockCipher::ofbV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ofbV_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OfbBlockCipher::ofbOutV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ofbOutV_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OfbBlockCipher::blockSize
	int32_t ___blockSize_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OfbBlockCipher::cipher
	RuntimeObject* ___cipher_4;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_ofbV_1() { return static_cast<int32_t>(offsetof(OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A, ___ofbV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ofbV_1() const { return ___ofbV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ofbV_1() { return &___ofbV_1; }
	inline void set_ofbV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ofbV_1 = value;
		Il2CppCodeGenWriteBarrier((&___ofbV_1), value);
	}

	inline static int32_t get_offset_of_ofbOutV_2() { return static_cast<int32_t>(offsetof(OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A, ___ofbOutV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ofbOutV_2() const { return ___ofbOutV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ofbOutV_2() { return &___ofbOutV_2; }
	inline void set_ofbOutV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ofbOutV_2 = value;
		Il2CppCodeGenWriteBarrier((&___ofbOutV_2), value);
	}

	inline static int32_t get_offset_of_blockSize_3() { return static_cast<int32_t>(offsetof(OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A, ___blockSize_3)); }
	inline int32_t get_blockSize_3() const { return ___blockSize_3; }
	inline int32_t* get_address_of_blockSize_3() { return &___blockSize_3; }
	inline void set_blockSize_3(int32_t value)
	{
		___blockSize_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFBBLOCKCIPHER_T4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A_H
#ifndef OPENPGPCFBBLOCKCIPHER_T03349ADA5EEC19330A18213E4C0755BEA1FA2DD6_H
#define OPENPGPCFBBLOCKCIPHER_T03349ADA5EEC19330A18213E4C0755BEA1FA2DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher
struct  OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::FR
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___FR_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::FRE
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___FRE_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::cipher
	RuntimeObject* ___cipher_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::blockSize
	int32_t ___blockSize_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::count
	int32_t ___count_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.OpenPgpCfbBlockCipher::forEncryption
	bool ___forEncryption_6;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_FR_1() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___FR_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_FR_1() const { return ___FR_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_FR_1() { return &___FR_1; }
	inline void set_FR_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___FR_1 = value;
		Il2CppCodeGenWriteBarrier((&___FR_1), value);
	}

	inline static int32_t get_offset_of_FRE_2() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___FRE_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_FRE_2() const { return ___FRE_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_FRE_2() { return &___FRE_2; }
	inline void set_FRE_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___FRE_2 = value;
		Il2CppCodeGenWriteBarrier((&___FRE_2), value);
	}

	inline static int32_t get_offset_of_cipher_3() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___cipher_3)); }
	inline RuntimeObject* get_cipher_3() const { return ___cipher_3; }
	inline RuntimeObject** get_address_of_cipher_3() { return &___cipher_3; }
	inline void set_cipher_3(RuntimeObject* value)
	{
		___cipher_3 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_3), value);
	}

	inline static int32_t get_offset_of_blockSize_4() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___blockSize_4)); }
	inline int32_t get_blockSize_4() const { return ___blockSize_4; }
	inline int32_t* get_address_of_blockSize_4() { return &___blockSize_4; }
	inline void set_blockSize_4(int32_t value)
	{
		___blockSize_4 = value;
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_forEncryption_6() { return static_cast<int32_t>(offsetof(OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6, ___forEncryption_6)); }
	inline bool get_forEncryption_6() const { return ___forEncryption_6; }
	inline bool* get_address_of_forEncryption_6() { return &___forEncryption_6; }
	inline void set_forEncryption_6(bool value)
	{
		___forEncryption_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENPGPCFBBLOCKCIPHER_T03349ADA5EEC19330A18213E4C0755BEA1FA2DD6_H
#ifndef SICBLOCKCIPHER_TC863A5B7F210F81589F9A7F4539977C54243FC82_H
#define SICBLOCKCIPHER_TC863A5B7F210F81589F9A7F4539977C54243FC82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher
struct  SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher::cipher
	RuntimeObject* ___cipher_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher::blockSize
	int32_t ___blockSize_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher::counter
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counter_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher::counterOut
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counterOut_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.SicBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_4;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82, ___cipher_0)); }
	inline RuntimeObject* get_cipher_0() const { return ___cipher_0; }
	inline RuntimeObject** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(RuntimeObject* value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_blockSize_1() { return static_cast<int32_t>(offsetof(SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82, ___blockSize_1)); }
	inline int32_t get_blockSize_1() const { return ___blockSize_1; }
	inline int32_t* get_address_of_blockSize_1() { return &___blockSize_1; }
	inline void set_blockSize_1(int32_t value)
	{
		___blockSize_1 = value;
	}

	inline static int32_t get_offset_of_counter_2() { return static_cast<int32_t>(offsetof(SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82, ___counter_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counter_2() const { return ___counter_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counter_2() { return &___counter_2; }
	inline void set_counter_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counter_2 = value;
		Il2CppCodeGenWriteBarrier((&___counter_2), value);
	}

	inline static int32_t get_offset_of_counterOut_3() { return static_cast<int32_t>(offsetof(SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82, ___counterOut_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counterOut_3() const { return ___counterOut_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counterOut_3() { return &___counterOut_3; }
	inline void set_counterOut_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counterOut_3 = value;
		Il2CppCodeGenWriteBarrier((&___counterOut_3), value);
	}

	inline static int32_t get_offset_of_IV_4() { return static_cast<int32_t>(offsetof(SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82, ___IV_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_4() const { return ___IV_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_4() { return &___IV_4; }
	inline void set_IV_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_4 = value;
		Il2CppCodeGenWriteBarrier((&___IV_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SICBLOCKCIPHER_TC863A5B7F210F81589F9A7F4539977C54243FC82_H
#ifndef GENERICKEY_T0444645EAE8A462B539CCABBDB50D60BFB560D4D_H
#define GENERICKEY_T0444645EAE8A462B539CCABBDB50D60BFB560D4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.GenericKey
struct  GenericKey_t0444645EAE8A462B539CCABBDB50D60BFB560D4D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.GenericKey::algorithmIdentifier
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algorithmIdentifier_0;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.GenericKey::representation
	RuntimeObject * ___representation_1;

public:
	inline static int32_t get_offset_of_algorithmIdentifier_0() { return static_cast<int32_t>(offsetof(GenericKey_t0444645EAE8A462B539CCABBDB50D60BFB560D4D, ___algorithmIdentifier_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algorithmIdentifier_0() const { return ___algorithmIdentifier_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algorithmIdentifier_0() { return &___algorithmIdentifier_0; }
	inline void set_algorithmIdentifier_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algorithmIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithmIdentifier_0), value);
	}

	inline static int32_t get_offset_of_representation_1() { return static_cast<int32_t>(offsetof(GenericKey_t0444645EAE8A462B539CCABBDB50D60BFB560D4D, ___representation_1)); }
	inline RuntimeObject * get_representation_1() const { return ___representation_1; }
	inline RuntimeObject ** get_address_of_representation_1() { return &___representation_1; }
	inline void set_representation_1(RuntimeObject * value)
	{
		___representation_1 = value;
		Il2CppCodeGenWriteBarrier((&___representation_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICKEY_T0444645EAE8A462B539CCABBDB50D60BFB560D4D_H
#ifndef PBEPARAMETERSGENERATOR_T8393B43188C7630FB3E775E3120C5DE0A232437E_H
#define PBEPARAMETERSGENERATOR_T8393B43188C7630FB3E775E3120C5DE0A232437E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator
struct  PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator::mPassword
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPassword_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator::mSalt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSalt_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator::mIterationCount
	int32_t ___mIterationCount_2;

public:
	inline static int32_t get_offset_of_mPassword_0() { return static_cast<int32_t>(offsetof(PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E, ___mPassword_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPassword_0() const { return ___mPassword_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPassword_0() { return &___mPassword_0; }
	inline void set_mPassword_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPassword_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPassword_0), value);
	}

	inline static int32_t get_offset_of_mSalt_1() { return static_cast<int32_t>(offsetof(PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E, ___mSalt_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSalt_1() const { return ___mSalt_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSalt_1() { return &___mSalt_1; }
	inline void set_mSalt_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSalt_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSalt_1), value);
	}

	inline static int32_t get_offset_of_mIterationCount_2() { return static_cast<int32_t>(offsetof(PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E, ___mIterationCount_2)); }
	inline int32_t get_mIterationCount_2() const { return ___mIterationCount_2; }
	inline int32_t* get_address_of_mIterationCount_2() { return &___mIterationCount_2; }
	inline void set_mIterationCount_2(int32_t value)
	{
		___mIterationCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBEPARAMETERSGENERATOR_T8393B43188C7630FB3E775E3120C5DE0A232437E_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#define BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher
struct  BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::bufOff
	int32_t ___bufOff_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::forEncryption
	bool ___forEncryption_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::cipher
	RuntimeObject* ___cipher_4;

public:
	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___buf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_1() const { return ___buf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_1 = value;
		Il2CppCodeGenWriteBarrier((&___buf_1), value);
	}

	inline static int32_t get_offset_of_bufOff_2() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___bufOff_2)); }
	inline int32_t get_bufOff_2() const { return ___bufOff_2; }
	inline int32_t* get_address_of_bufOff_2() { return &___bufOff_2; }
	inline void set_bufOff_2(int32_t value)
	{
		___bufOff_2 = value;
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#ifndef AESWRAPENGINE_T0F47D1A44813BB28E0BB4B8FA27BA5777E9388A2_H
#define AESWRAPENGINE_T0F47D1A44813BB28E0BB4B8FA27BA5777E9388A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.AesWrapEngine
struct  AesWrapEngine_t0F47D1A44813BB28E0BB4B8FA27BA5777E9388A2  : public Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESWRAPENGINE_T0F47D1A44813BB28E0BB4B8FA27BA5777E9388A2_H
#ifndef CAMELLIAWRAPENGINE_TD718F292DB4CCA10E30B743E5A7B5F8AB3662038_H
#define CAMELLIAWRAPENGINE_TD718F292DB4CCA10E30B743E5A7B5F8AB3662038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.CamelliaWrapEngine
struct  CamelliaWrapEngine_tD718F292DB4CCA10E30B743E5A7B5F8AB3662038  : public Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMELLIAWRAPENGINE_TD718F292DB4CCA10E30B743E5A7B5F8AB3662038_H
#ifndef CAST6ENGINE_T2D3679CA3D5FE6D340DBFC102FA027626F3489FE_H
#define CAST6ENGINE_T2D3679CA3D5FE6D340DBFC102FA027626F3489FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast6Engine
struct  Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE  : public Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast6Engine::_Kr
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____Kr_18;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast6Engine::_Km
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____Km_19;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast6Engine::_Tr
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____Tr_20;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast6Engine::_Tm
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____Tm_21;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Cast6Engine::_workingKey
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____workingKey_22;

public:
	inline static int32_t get_offset_of__Kr_18() { return static_cast<int32_t>(offsetof(Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE, ____Kr_18)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__Kr_18() const { return ____Kr_18; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__Kr_18() { return &____Kr_18; }
	inline void set__Kr_18(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____Kr_18 = value;
		Il2CppCodeGenWriteBarrier((&____Kr_18), value);
	}

	inline static int32_t get_offset_of__Km_19() { return static_cast<int32_t>(offsetof(Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE, ____Km_19)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__Km_19() const { return ____Km_19; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__Km_19() { return &____Km_19; }
	inline void set__Km_19(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____Km_19 = value;
		Il2CppCodeGenWriteBarrier((&____Km_19), value);
	}

	inline static int32_t get_offset_of__Tr_20() { return static_cast<int32_t>(offsetof(Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE, ____Tr_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__Tr_20() const { return ____Tr_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__Tr_20() { return &____Tr_20; }
	inline void set__Tr_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____Tr_20 = value;
		Il2CppCodeGenWriteBarrier((&____Tr_20), value);
	}

	inline static int32_t get_offset_of__Tm_21() { return static_cast<int32_t>(offsetof(Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE, ____Tm_21)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__Tm_21() const { return ____Tm_21; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__Tm_21() { return &____Tm_21; }
	inline void set__Tm_21(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____Tm_21 = value;
		Il2CppCodeGenWriteBarrier((&____Tm_21), value);
	}

	inline static int32_t get_offset_of__workingKey_22() { return static_cast<int32_t>(offsetof(Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE, ____workingKey_22)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__workingKey_22() const { return ____workingKey_22; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__workingKey_22() { return &____workingKey_22; }
	inline void set__workingKey_22(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____workingKey_22 = value;
		Il2CppCodeGenWriteBarrier((&____workingKey_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAST6ENGINE_T2D3679CA3D5FE6D340DBFC102FA027626F3489FE_H
#ifndef CHACHA7539ENGINE_T290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD_H
#define CHACHA7539ENGINE_T290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine
struct  ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD  : public Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHACHA7539ENGINE_T290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD_H
#ifndef CHACHAENGINE_TCF48CB190F4E47C3B3564AAEA03B967F1A01FA95_H
#define CHACHAENGINE_TCF48CB190F4E47C3B3564AAEA03B967F1A01FA95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaChaEngine
struct  ChaChaEngine_tCF48CB190F4E47C3B3564AAEA03B967F1A01FA95  : public Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHACHAENGINE_TCF48CB190F4E47C3B3564AAEA03B967F1A01FA95_H
#ifndef DESEDEENGINE_TF32D61F13406697BEA74EA9BCF5A978489EE0160_H
#define DESEDEENGINE_TF32D61F13406697BEA74EA9BCF5A978489EE0160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeEngine
struct  DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160  : public DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeEngine::workingKey1
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey1_15;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeEngine::workingKey2
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey2_16;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeEngine::workingKey3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey3_17;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.DesEdeEngine::forEncryption
	bool ___forEncryption_18;

public:
	inline static int32_t get_offset_of_workingKey1_15() { return static_cast<int32_t>(offsetof(DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160, ___workingKey1_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey1_15() const { return ___workingKey1_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey1_15() { return &___workingKey1_15; }
	inline void set_workingKey1_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey1_15 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey1_15), value);
	}

	inline static int32_t get_offset_of_workingKey2_16() { return static_cast<int32_t>(offsetof(DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160, ___workingKey2_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey2_16() const { return ___workingKey2_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey2_16() { return &___workingKey2_16; }
	inline void set_workingKey2_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey2_16 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey2_16), value);
	}

	inline static int32_t get_offset_of_workingKey3_17() { return static_cast<int32_t>(offsetof(DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160, ___workingKey3_17)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey3_17() const { return ___workingKey3_17; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey3_17() { return &___workingKey3_17; }
	inline void set_workingKey3_17(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey3_17 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey3_17), value);
	}

	inline static int32_t get_offset_of_forEncryption_18() { return static_cast<int32_t>(offsetof(DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160, ___forEncryption_18)); }
	inline bool get_forEncryption_18() const { return ___forEncryption_18; }
	inline bool* get_address_of_forEncryption_18() { return &___forEncryption_18; }
	inline void set_forEncryption_18(bool value)
	{
		___forEncryption_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESEDEENGINE_TF32D61F13406697BEA74EA9BCF5A978489EE0160_H
#ifndef DESKEYGENERATOR_TE440F13C9477F9601B7A7255AD0F95176D028432_H
#define DESKEYGENERATOR_TE440F13C9477F9601B7A7255AD0F95176D028432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DesKeyGenerator
struct  DesKeyGenerator_tE440F13C9477F9601B7A7255AD0F95176D028432  : public CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKEYGENERATOR_TE440F13C9477F9601B7A7255AD0F95176D028432_H
#ifndef KDF1BYTESGENERATOR_TF244EEE7F3045AA629C5B57C005DBC56A22E5023_H
#define KDF1BYTESGENERATOR_TF244EEE7F3045AA629C5B57C005DBC56A22E5023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Kdf1BytesGenerator
struct  Kdf1BytesGenerator_tF244EEE7F3045AA629C5B57C005DBC56A22E5023  : public BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDF1BYTESGENERATOR_TF244EEE7F3045AA629C5B57C005DBC56A22E5023_H
#ifndef KDF2BYTESGENERATOR_T03FD38BD7C24347AF82DA450FF45C0DD665DA1BE_H
#define KDF2BYTESGENERATOR_T03FD38BD7C24347AF82DA450FF45C0DD665DA1BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Kdf2BytesGenerator
struct  Kdf2BytesGenerator_t03FD38BD7C24347AF82DA450FF45C0DD665DA1BE  : public BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDF2BYTESGENERATOR_T03FD38BD7C24347AF82DA450FF45C0DD665DA1BE_H
#ifndef OPENSSLPBEPARAMETERSGENERATOR_T6F71231D7A73996E9AE981A032FE6D8B83D06B8F_H
#define OPENSSLPBEPARAMETERSGENERATOR_T6F71231D7A73996E9AE981A032FE6D8B83D06B8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenSslPbeParametersGenerator
struct  OpenSslPbeParametersGenerator_t6F71231D7A73996E9AE981A032FE6D8B83D06B8F  : public PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.OpenSslPbeParametersGenerator::digest
	RuntimeObject* ___digest_3;

public:
	inline static int32_t get_offset_of_digest_3() { return static_cast<int32_t>(offsetof(OpenSslPbeParametersGenerator_t6F71231D7A73996E9AE981A032FE6D8B83D06B8F, ___digest_3)); }
	inline RuntimeObject* get_digest_3() const { return ___digest_3; }
	inline RuntimeObject** get_address_of_digest_3() { return &___digest_3; }
	inline void set_digest_3(RuntimeObject* value)
	{
		___digest_3 = value;
		Il2CppCodeGenWriteBarrier((&___digest_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENSSLPBEPARAMETERSGENERATOR_T6F71231D7A73996E9AE981A032FE6D8B83D06B8F_H
#ifndef PKCS12PARAMETERSGENERATOR_T53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46_H
#define PKCS12PARAMETERSGENERATOR_T53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs12ParametersGenerator
struct  Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46  : public PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs12ParametersGenerator::digest
	RuntimeObject* ___digest_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs12ParametersGenerator::u
	int32_t ___u_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs12ParametersGenerator::v
	int32_t ___v_8;

public:
	inline static int32_t get_offset_of_digest_6() { return static_cast<int32_t>(offsetof(Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46, ___digest_6)); }
	inline RuntimeObject* get_digest_6() const { return ___digest_6; }
	inline RuntimeObject** get_address_of_digest_6() { return &___digest_6; }
	inline void set_digest_6(RuntimeObject* value)
	{
		___digest_6 = value;
		Il2CppCodeGenWriteBarrier((&___digest_6), value);
	}

	inline static int32_t get_offset_of_u_7() { return static_cast<int32_t>(offsetof(Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46, ___u_7)); }
	inline int32_t get_u_7() const { return ___u_7; }
	inline int32_t* get_address_of_u_7() { return &___u_7; }
	inline void set_u_7(int32_t value)
	{
		___u_7 = value;
	}

	inline static int32_t get_offset_of_v_8() { return static_cast<int32_t>(offsetof(Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46, ___v_8)); }
	inline int32_t get_v_8() const { return ___v_8; }
	inline int32_t* get_address_of_v_8() { return &___v_8; }
	inline void set_v_8(int32_t value)
	{
		___v_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12PARAMETERSGENERATOR_T53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46_H
#ifndef PKCS5S1PARAMETERSGENERATOR_T9D4A268220EF5475B363ECC9CB9AE389001CCC6A_H
#define PKCS5S1PARAMETERSGENERATOR_T9D4A268220EF5475B363ECC9CB9AE389001CCC6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs5S1ParametersGenerator
struct  Pkcs5S1ParametersGenerator_t9D4A268220EF5475B363ECC9CB9AE389001CCC6A  : public PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs5S1ParametersGenerator::digest
	RuntimeObject* ___digest_3;

public:
	inline static int32_t get_offset_of_digest_3() { return static_cast<int32_t>(offsetof(Pkcs5S1ParametersGenerator_t9D4A268220EF5475B363ECC9CB9AE389001CCC6A, ___digest_3)); }
	inline RuntimeObject* get_digest_3() const { return ___digest_3; }
	inline RuntimeObject** get_address_of_digest_3() { return &___digest_3; }
	inline void set_digest_3(RuntimeObject* value)
	{
		___digest_3 = value;
		Il2CppCodeGenWriteBarrier((&___digest_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS5S1PARAMETERSGENERATOR_T9D4A268220EF5475B363ECC9CB9AE389001CCC6A_H
#ifndef PKCS5S2PARAMETERSGENERATOR_TEBA58589AED1C9CBECB5E6B15F0949213380225A_H
#define PKCS5S2PARAMETERSGENERATOR_TEBA58589AED1C9CBECB5E6B15F0949213380225A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs5S2ParametersGenerator
struct  Pkcs5S2ParametersGenerator_tEBA58589AED1C9CBECB5E6B15F0949213380225A  : public PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs5S2ParametersGenerator::hMac
	RuntimeObject* ___hMac_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Pkcs5S2ParametersGenerator::state
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___state_4;

public:
	inline static int32_t get_offset_of_hMac_3() { return static_cast<int32_t>(offsetof(Pkcs5S2ParametersGenerator_tEBA58589AED1C9CBECB5E6B15F0949213380225A, ___hMac_3)); }
	inline RuntimeObject* get_hMac_3() const { return ___hMac_3; }
	inline RuntimeObject** get_address_of_hMac_3() { return &___hMac_3; }
	inline void set_hMac_3(RuntimeObject* value)
	{
		___hMac_3 = value;
		Il2CppCodeGenWriteBarrier((&___hMac_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(Pkcs5S2ParametersGenerator_tEBA58589AED1C9CBECB5E6B15F0949213380225A, ___state_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_state_4() const { return ___state_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS5S2PARAMETERSGENERATOR_TEBA58589AED1C9CBECB5E6B15F0949213380225A_H
#ifndef POLY1305KEYGENERATOR_T007CED9BE05565B5D2FBA5604B7F8292E0B9022E_H
#define POLY1305KEYGENERATOR_T007CED9BE05565B5D2FBA5604B7F8292E0B9022E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.Poly1305KeyGenerator
struct  Poly1305KeyGenerator_t007CED9BE05565B5D2FBA5604B7F8292E0B9022E  : public CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLY1305KEYGENERATOR_T007CED9BE05565B5D2FBA5604B7F8292E0B9022E_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef DESEDEKEYGENERATOR_T3C5856CA9C893F5CDFE0278117E6AABE95C3C9C3_H
#define DESEDEKEYGENERATOR_T3C5856CA9C893F5CDFE0278117E6AABE95C3C9C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Generators.DesEdeKeyGenerator
struct  DesEdeKeyGenerator_t3C5856CA9C893F5CDFE0278117E6AABE95C3C9C3  : public DesKeyGenerator_tE440F13C9477F9601B7A7255AD0F95176D028432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESEDEKEYGENERATOR_T3C5856CA9C893F5CDFE0278117E6AABE95C3C9C3_H
#ifndef CIPHERSTREAM_T13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5_H
#define CIPHERSTREAM_T13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream
struct  CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream::inCipher
	RuntimeObject* ___inCipher_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream::outCipher
	RuntimeObject* ___outCipher_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream::mInBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mInBuf_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream::mInPos
	int32_t ___mInPos_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream::inStreamEnded
	bool ___inStreamEnded_10;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_inCipher_6() { return static_cast<int32_t>(offsetof(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5, ___inCipher_6)); }
	inline RuntimeObject* get_inCipher_6() const { return ___inCipher_6; }
	inline RuntimeObject** get_address_of_inCipher_6() { return &___inCipher_6; }
	inline void set_inCipher_6(RuntimeObject* value)
	{
		___inCipher_6 = value;
		Il2CppCodeGenWriteBarrier((&___inCipher_6), value);
	}

	inline static int32_t get_offset_of_outCipher_7() { return static_cast<int32_t>(offsetof(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5, ___outCipher_7)); }
	inline RuntimeObject* get_outCipher_7() const { return ___outCipher_7; }
	inline RuntimeObject** get_address_of_outCipher_7() { return &___outCipher_7; }
	inline void set_outCipher_7(RuntimeObject* value)
	{
		___outCipher_7 = value;
		Il2CppCodeGenWriteBarrier((&___outCipher_7), value);
	}

	inline static int32_t get_offset_of_mInBuf_8() { return static_cast<int32_t>(offsetof(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5, ___mInBuf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mInBuf_8() const { return ___mInBuf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mInBuf_8() { return &___mInBuf_8; }
	inline void set_mInBuf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mInBuf_8 = value;
		Il2CppCodeGenWriteBarrier((&___mInBuf_8), value);
	}

	inline static int32_t get_offset_of_mInPos_9() { return static_cast<int32_t>(offsetof(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5, ___mInPos_9)); }
	inline int32_t get_mInPos_9() const { return ___mInPos_9; }
	inline int32_t* get_address_of_mInPos_9() { return &___mInPos_9; }
	inline void set_mInPos_9(int32_t value)
	{
		___mInPos_9 = value;
	}

	inline static int32_t get_offset_of_inStreamEnded_10() { return static_cast<int32_t>(offsetof(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5, ___inStreamEnded_10)); }
	inline bool get_inStreamEnded_10() const { return ___inStreamEnded_10; }
	inline bool* get_address_of_inStreamEnded_10() { return &___inStreamEnded_10; }
	inline void set_inStreamEnded_10(bool value)
	{
		___inStreamEnded_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSTREAM_T13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5_H
#ifndef DIGESTSTREAM_T249C17722CC25452D9F2073E3B64F95EC8E38214_H
#define DIGESTSTREAM_T249C17722CC25452D9F2073E3B64F95EC8E38214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.DigestStream
struct  DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.DigestStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.DigestStream::inDigest
	RuntimeObject* ___inDigest_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.DigestStream::outDigest
	RuntimeObject* ___outDigest_7;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_inDigest_6() { return static_cast<int32_t>(offsetof(DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214, ___inDigest_6)); }
	inline RuntimeObject* get_inDigest_6() const { return ___inDigest_6; }
	inline RuntimeObject** get_address_of_inDigest_6() { return &___inDigest_6; }
	inline void set_inDigest_6(RuntimeObject* value)
	{
		___inDigest_6 = value;
		Il2CppCodeGenWriteBarrier((&___inDigest_6), value);
	}

	inline static int32_t get_offset_of_outDigest_7() { return static_cast<int32_t>(offsetof(DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214, ___outDigest_7)); }
	inline RuntimeObject* get_outDigest_7() const { return ___outDigest_7; }
	inline RuntimeObject** get_address_of_outDigest_7() { return &___outDigest_7; }
	inline void set_outDigest_7(RuntimeObject* value)
	{
		___outDigest_7 = value;
		Il2CppCodeGenWriteBarrier((&___outDigest_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSTREAM_T249C17722CC25452D9F2073E3B64F95EC8E38214_H
#ifndef MACSTREAM_T9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F_H
#define MACSTREAM_T9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacStream
struct  MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacStream::inMac
	RuntimeObject* ___inMac_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacStream::outMac
	RuntimeObject* ___outMac_7;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_inMac_6() { return static_cast<int32_t>(offsetof(MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F, ___inMac_6)); }
	inline RuntimeObject* get_inMac_6() const { return ___inMac_6; }
	inline RuntimeObject** get_address_of_inMac_6() { return &___inMac_6; }
	inline void set_inMac_6(RuntimeObject* value)
	{
		___inMac_6 = value;
		Il2CppCodeGenWriteBarrier((&___inMac_6), value);
	}

	inline static int32_t get_offset_of_outMac_7() { return static_cast<int32_t>(offsetof(MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F, ___outMac_7)); }
	inline RuntimeObject* get_outMac_7() const { return ___outMac_7; }
	inline RuntimeObject** get_address_of_outMac_7() { return &___outMac_7; }
	inline void set_outMac_7(RuntimeObject* value)
	{
		___outMac_7 = value;
		Il2CppCodeGenWriteBarrier((&___outMac_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACSTREAM_T9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F_H
#ifndef SIGNERSTREAM_T9C12CE9CFAFDF737A522BFA866D610C769F7E513_H
#define SIGNERSTREAM_T9C12CE9CFAFDF737A522BFA866D610C769F7E513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerStream
struct  SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerStream::inSigner
	RuntimeObject* ___inSigner_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerStream::outSigner
	RuntimeObject* ___outSigner_7;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_inSigner_6() { return static_cast<int32_t>(offsetof(SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513, ___inSigner_6)); }
	inline RuntimeObject* get_inSigner_6() const { return ___inSigner_6; }
	inline RuntimeObject** get_address_of_inSigner_6() { return &___inSigner_6; }
	inline void set_inSigner_6(RuntimeObject* value)
	{
		___inSigner_6 = value;
		Il2CppCodeGenWriteBarrier((&___inSigner_6), value);
	}

	inline static int32_t get_offset_of_outSigner_7() { return static_cast<int32_t>(offsetof(SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513, ___outSigner_7)); }
	inline RuntimeObject* get_outSigner_7() const { return ___outSigner_7; }
	inline RuntimeObject** get_address_of_outSigner_7() { return &___outSigner_7; }
	inline void set_outSigner_7(RuntimeObject* value)
	{
		___outSigner_7 = value;
		Il2CppCodeGenWriteBarrier((&___outSigner_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERSTREAM_T9C12CE9CFAFDF737A522BFA866D610C769F7E513_H
#ifndef STATE_T0F3C54C1F893255688EAE067C45EF1B35F5C8B14_H
#define STATE_T0F3C54C1F893255688EAE067C45EF1B35F5C8B14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305_State
struct  State_t0F3C54C1F893255688EAE067C45EF1B35F5C8B14 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t0F3C54C1F893255688EAE067C45EF1B35F5C8B14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T0F3C54C1F893255688EAE067C45EF1B35F5C8B14_H
#ifndef CTSBLOCKCIPHER_T508DA759BBA3B235251005AC99764010145B63B8_H
#define CTSBLOCKCIPHER_T508DA759BBA3B235251005AC99764010145B63B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CtsBlockCipher
struct  CtsBlockCipher_t508DA759BBA3B235251005AC99764010145B63B8  : public BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CtsBlockCipher::blockSize
	int32_t ___blockSize_5;

public:
	inline static int32_t get_offset_of_blockSize_5() { return static_cast<int32_t>(offsetof(CtsBlockCipher_t508DA759BBA3B235251005AC99764010145B63B8, ___blockSize_5)); }
	inline int32_t get_blockSize_5() const { return ___blockSize_5; }
	inline int32_t* get_address_of_blockSize_5() { return &___blockSize_5; }
	inline void set_blockSize_5(int32_t value)
	{
		___blockSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTSBLOCKCIPHER_T508DA759BBA3B235251005AC99764010145B63B8_H
#ifndef TAG_T0371589AE01EE5764F106484F85BFB68634190E2_H
#define TAG_T0371589AE01EE5764F106484F85BFB68634190E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher_Tag
struct  Tag_t0371589AE01EE5764F106484F85BFB68634190E2 
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.EaxBlockCipher_Tag::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tag_t0371589AE01EE5764F106484F85BFB68634190E2, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAG_T0371589AE01EE5764F106484F85BFB68634190E2_H
#ifndef BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#define BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifndef DIGESTSINK_TE8C771EFC05C3869CC8718EEB22192E09C147B2C_H
#define DIGESTSINK_TE8C771EFC05C3869CC8718EEB22192E09C147B2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.DigestSink
struct  DigestSink_tE8C771EFC05C3869CC8718EEB22192E09C147B2C  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.DigestSink::mDigest
	RuntimeObject* ___mDigest_6;

public:
	inline static int32_t get_offset_of_mDigest_6() { return static_cast<int32_t>(offsetof(DigestSink_tE8C771EFC05C3869CC8718EEB22192E09C147B2C, ___mDigest_6)); }
	inline RuntimeObject* get_mDigest_6() const { return ___mDigest_6; }
	inline RuntimeObject** get_address_of_mDigest_6() { return &___mDigest_6; }
	inline void set_mDigest_6(RuntimeObject* value)
	{
		___mDigest_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSINK_TE8C771EFC05C3869CC8718EEB22192E09C147B2C_H
#ifndef MACSINK_TFAC174835C49820F15602AF337F88F6F496DC568_H
#define MACSINK_TFAC174835C49820F15602AF337F88F6F496DC568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacSink
struct  MacSink_tFAC174835C49820F15602AF337F88F6F496DC568  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacSink::mMac
	RuntimeObject* ___mMac_6;

public:
	inline static int32_t get_offset_of_mMac_6() { return static_cast<int32_t>(offsetof(MacSink_tFAC174835C49820F15602AF337F88F6F496DC568, ___mMac_6)); }
	inline RuntimeObject* get_mMac_6() const { return ___mMac_6; }
	inline RuntimeObject** get_address_of_mMac_6() { return &___mMac_6; }
	inline void set_mMac_6(RuntimeObject* value)
	{
		___mMac_6 = value;
		Il2CppCodeGenWriteBarrier((&___mMac_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACSINK_TFAC174835C49820F15602AF337F88F6F496DC568_H
#ifndef SIGNERSINK_T8C4F195F439F894E5F574AB9D73C0FB58112E4BA_H
#define SIGNERSINK_T8C4F195F439F894E5F574AB9D73C0FB58112E4BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerSink
struct  SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerSink::mSigner
	RuntimeObject* ___mSigner_6;

public:
	inline static int32_t get_offset_of_mSigner_6() { return static_cast<int32_t>(offsetof(SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA, ___mSigner_6)); }
	inline RuntimeObject* get_mSigner_6() const { return ___mSigner_6; }
	inline RuntimeObject** get_address_of_mSigner_6() { return &___mSigner_6; }
	inline void set_mSigner_6(RuntimeObject* value)
	{
		___mSigner_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSigner_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERSINK_T8C4F195F439F894E5F574AB9D73C0FB58112E4BA_H
#ifndef CHACHA20POLY1305_TE4249E44DA37ED83AB1D64308E18E9F97FD765B7_H
#define CHACHA20POLY1305_TE4249E44DA37ED83AB1D64308E18E9F97FD765B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305
struct  ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mChacha20
	ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * ___mChacha20_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mPoly1305
	RuntimeObject* ___mPoly1305_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mKey_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mNonce_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mBuf_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mMac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mMac_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mInitialAad
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mInitialAad_13;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mAadCount
	uint64_t ___mAadCount_14;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mDataCount
	uint64_t ___mDataCount_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305_State BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mState
	int32_t ___mState_16;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::mBufPos
	int32_t ___mBufPos_17;

public:
	inline static int32_t get_offset_of_mChacha20_7() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mChacha20_7)); }
	inline ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * get_mChacha20_7() const { return ___mChacha20_7; }
	inline ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD ** get_address_of_mChacha20_7() { return &___mChacha20_7; }
	inline void set_mChacha20_7(ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * value)
	{
		___mChacha20_7 = value;
		Il2CppCodeGenWriteBarrier((&___mChacha20_7), value);
	}

	inline static int32_t get_offset_of_mPoly1305_8() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mPoly1305_8)); }
	inline RuntimeObject* get_mPoly1305_8() const { return ___mPoly1305_8; }
	inline RuntimeObject** get_address_of_mPoly1305_8() { return &___mPoly1305_8; }
	inline void set_mPoly1305_8(RuntimeObject* value)
	{
		___mPoly1305_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPoly1305_8), value);
	}

	inline static int32_t get_offset_of_mKey_9() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mKey_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mKey_9() const { return ___mKey_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mKey_9() { return &___mKey_9; }
	inline void set_mKey_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___mKey_9), value);
	}

	inline static int32_t get_offset_of_mNonce_10() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mNonce_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mNonce_10() const { return ___mNonce_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mNonce_10() { return &___mNonce_10; }
	inline void set_mNonce_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mNonce_10 = value;
		Il2CppCodeGenWriteBarrier((&___mNonce_10), value);
	}

	inline static int32_t get_offset_of_mBuf_11() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mBuf_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mBuf_11() const { return ___mBuf_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mBuf_11() { return &___mBuf_11; }
	inline void set_mBuf_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mBuf_11 = value;
		Il2CppCodeGenWriteBarrier((&___mBuf_11), value);
	}

	inline static int32_t get_offset_of_mMac_12() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mMac_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mMac_12() const { return ___mMac_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mMac_12() { return &___mMac_12; }
	inline void set_mMac_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mMac_12 = value;
		Il2CppCodeGenWriteBarrier((&___mMac_12), value);
	}

	inline static int32_t get_offset_of_mInitialAad_13() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mInitialAad_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mInitialAad_13() const { return ___mInitialAad_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mInitialAad_13() { return &___mInitialAad_13; }
	inline void set_mInitialAad_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mInitialAad_13 = value;
		Il2CppCodeGenWriteBarrier((&___mInitialAad_13), value);
	}

	inline static int32_t get_offset_of_mAadCount_14() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mAadCount_14)); }
	inline uint64_t get_mAadCount_14() const { return ___mAadCount_14; }
	inline uint64_t* get_address_of_mAadCount_14() { return &___mAadCount_14; }
	inline void set_mAadCount_14(uint64_t value)
	{
		___mAadCount_14 = value;
	}

	inline static int32_t get_offset_of_mDataCount_15() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mDataCount_15)); }
	inline uint64_t get_mDataCount_15() const { return ___mDataCount_15; }
	inline uint64_t* get_address_of_mDataCount_15() { return &___mDataCount_15; }
	inline void set_mDataCount_15(uint64_t value)
	{
		___mDataCount_15 = value;
	}

	inline static int32_t get_offset_of_mState_16() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mState_16)); }
	inline int32_t get_mState_16() const { return ___mState_16; }
	inline int32_t* get_address_of_mState_16() { return &___mState_16; }
	inline void set_mState_16(int32_t value)
	{
		___mState_16 = value;
	}

	inline static int32_t get_offset_of_mBufPos_17() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7, ___mBufPos_17)); }
	inline int32_t get_mBufPos_17() const { return ___mBufPos_17; }
	inline int32_t* get_address_of_mBufPos_17() { return &___mBufPos_17; }
	inline void set_mBufPos_17(int32_t value)
	{
		___mBufPos_17 = value;
	}
};

struct ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.ChaCha20Poly1305::Zeroes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Zeroes_4;

public:
	inline static int32_t get_offset_of_Zeroes_4() { return static_cast<int32_t>(offsetof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7_StaticFields, ___Zeroes_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Zeroes_4() const { return ___Zeroes_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Zeroes_4() { return &___Zeroes_4; }
	inline void set_Zeroes_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Zeroes_4 = value;
		Il2CppCodeGenWriteBarrier((&___Zeroes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHACHA20POLY1305_TE4249E44DA37ED83AB1D64308E18E9F97FD765B7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (GenericKey_t0444645EAE8A462B539CCABBDB50D60BFB560D4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4800[2] = 
{
	GenericKey_t0444645EAE8A462B539CCABBDB50D60BFB560D4D::get_offset_of_algorithmIdentifier_0(),
	GenericKey_t0444645EAE8A462B539CCABBDB50D60BFB560D4D::get_offset_of_representation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4801[6] = 
{
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F::get_offset_of_IV_0(),
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F::get_offset_of_cbcV_1(),
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F::get_offset_of_cbcNextV_2(),
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F::get_offset_of_blockSize_3(),
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F::get_offset_of_cipher_4(),
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F::get_offset_of_encrypting_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566), -1, sizeof(CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4802[10] = 
{
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566_StaticFields::get_offset_of_BlockSize_0(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_cipher_1(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_macBlock_2(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_forEncryption_3(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_nonce_4(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_initialAssociatedText_5(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_macSize_6(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_keyParam_7(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_associatedText_8(),
	CcmBlockCipher_tE68A2FAE71110D8ED9088FCBB1B83FB9A238A566::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4803[6] = 
{
	CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF::get_offset_of_IV_0(),
	CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF::get_offset_of_cfbV_1(),
	CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF::get_offset_of_cfbOutV_2(),
	CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF::get_offset_of_encrypting_3(),
	CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF::get_offset_of_blockSize_4(),
	CfbBlockCipher_tD592CA890547E1C2332BA80349F15ABAF98277BF::get_offset_of_cipher_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7), -1, sizeof(ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4804[18] = 
{
	0,
	0,
	0,
	0,
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7_StaticFields::get_offset_of_Zeroes_4(),
	0,
	0,
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mChacha20_7(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mPoly1305_8(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mKey_9(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mNonce_10(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mBuf_11(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mMac_12(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mInitialAad_13(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mAadCount_14(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mDataCount_15(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mState_16(),
	ChaCha20Poly1305_tE4249E44DA37ED83AB1D64308E18E9F97FD765B7::get_offset_of_mBufPos_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (State_t0F3C54C1F893255688EAE067C45EF1B35F5C8B14)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4805[10] = 
{
	State_t0F3C54C1F893255688EAE067C45EF1B35F5C8B14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (CtsBlockCipher_t508DA759BBA3B235251005AC99764010145B63B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4806[1] = 
{
	CtsBlockCipher_t508DA759BBA3B235251005AC99764010145B63B8::get_offset_of_blockSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4807[12] = 
{
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_cipher_0(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_forEncryption_1(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_blockSize_2(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_mac_3(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_nonceMac_4(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_associatedTextMac_5(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_macBlock_6(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_macSize_7(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_bufBlock_8(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_bufOff_9(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_cipherInitialized_10(),
	EaxBlockCipher_t447B0C3BA5D6726C9807EE3606C7E1139C040565::get_offset_of_initialAssociatedText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (Tag_t0371589AE01EE5764F106484F85BFB68634190E2)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4808[4] = 
{
	Tag_t0371589AE01EE5764F106484F85BFB68634190E2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4809[26] = 
{
	0,
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_ctrBlock_1(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_cipher_2(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_multiplier_3(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_exp_4(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_forEncryption_5(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_initialised_6(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_macSize_7(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_lastKey_8(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_nonce_9(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_initialAssociatedText_10(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_H_11(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_J0_12(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_bufBlock_13(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_macBlock_14(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_S_15(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_S_at_16(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_S_atPre_17(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_counter_18(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_blocksRemaining_19(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_bufOff_20(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_totalLength_21(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_atBlock_22(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_atBlockPos_23(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_atLength_24(),
	GcmBlockCipher_t2043D4B231A20F63B6ACA49A27AA3898F1DD1617::get_offset_of_atLengthPre_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4810[10] = 
{
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_IV_0(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_ofbV_1(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_ofbOutV_2(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_blockSize_3(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_cipher_4(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_firstStep_5(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_N3_6(),
	GOfbBlockCipher_t19C1560AF8627473DB62AA470715100C5A17312F::get_offset_of_N4_7(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900), -1, sizeof(KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4813[18] = 
{
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields::get_offset_of_BYTES_IN_INT_0(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields::get_offset_of_BITS_IN_BYTE_1(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields::get_offset_of_MAX_MAC_BIT_LENGTH_2(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900_StaticFields::get_offset_of_MIN_MAC_BIT_LENGTH_3(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_engine_4(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_macSize_5(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_forEncryption_6(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_initialAssociatedText_7(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_mac_8(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_macBlock_9(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_nonce_10(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_G1_11(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_buffer_12(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_s_13(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_counter_14(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_associatedText_15(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_data_16(),
	KCcmBlockCipher_tC7D34D53F34E965A43F0EA7CDA363C3A97A88900::get_offset_of_Nb__17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4814[7] = 
{
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_IV_0(),
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_ofbV_1(),
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_ofbOutV_2(),
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_initialised_3(),
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_byteCount_4(),
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_blockSize_5(),
	KCtrBlockCipher_tF7A921C1AAE447AE33B498257D85EFDBFEED4D3E::get_offset_of_cipher_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4815[23] = 
{
	0,
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_hashCipher_1(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_mainCipher_2(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_forEncryption_3(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_macSize_4(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_initialAssociatedText_5(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_L_6(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_L_Asterisk_7(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_L_Dollar_8(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_KtopInput_9(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_Stretch_10(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_OffsetMAIN_0_11(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_hashBlock_12(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_mainBlock_13(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_hashBlockPos_14(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_mainBlockPos_15(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_hashBlockCount_16(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_mainBlockCount_17(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_OffsetHASH_18(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_Sum_19(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_OffsetMAIN_20(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_Checksum_21(),
	OcbBlockCipher_tF3ECF68C06A035387844499BD6542DC461D2B041::get_offset_of_macBlock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4816[5] = 
{
	OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A::get_offset_of_IV_0(),
	OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A::get_offset_of_ofbV_1(),
	OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A::get_offset_of_ofbOutV_2(),
	OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A::get_offset_of_blockSize_3(),
	OfbBlockCipher_t4AC0FCA518EC76D5B8AC6C702E7F5D9B4D60351A::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4817[7] = 
{
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_IV_0(),
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_FR_1(),
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_FRE_2(),
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_cipher_3(),
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_blockSize_4(),
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_count_5(),
	OpenPgpCfbBlockCipher_t03349ADA5EEC19330A18213E4C0755BEA1FA2DD6::get_offset_of_forEncryption_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4818[5] = 
{
	SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82::get_offset_of_cipher_0(),
	SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82::get_offset_of_blockSize_1(),
	SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82::get_offset_of_counter_2(),
	SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82::get_offset_of_counterOut_3(),
	SicBlockCipher_tC863A5B7F210F81589F9A7F4539977C54243FC82::get_offset_of_IV_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (BasicGcmExponentiator_t98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4819[1] = 
{
	BasicGcmExponentiator_t98F1E96E09CADC9CCDC95B16C33E042C51A7BCE9::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (BasicGcmMultiplier_tB23AC3A27441B45DA64CCDD14F6D39CC16071F8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4820[1] = 
{
	BasicGcmMultiplier_tB23AC3A27441B45DA64CCDD14F6D39CC16071F8F::get_offset_of_H_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (GcmUtilities_tDFE47B9588D38C941F589EB8E98623F967ADA5E5), -1, sizeof(GcmUtilities_tDFE47B9588D38C941F589EB8E98623F967ADA5E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4821[3] = 
{
	0,
	0,
	GcmUtilities_tDFE47B9588D38C941F589EB8E98623F967ADA5E5_StaticFields::get_offset_of_LOOKUP_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (Tables1kGcmExponentiator_t44381C85CB7FA23565458D84B1B13B4A9E4039BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4824[1] = 
{
	Tables1kGcmExponentiator_t44381C85CB7FA23565458D84B1B13B4A9E4039BB::get_offset_of_lookupPowX2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (Tables64kGcmMultiplier_tCCCFA1324D15CAC32A87B008277969FDBB67FAD1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4825[2] = 
{
	Tables64kGcmMultiplier_tCCCFA1324D15CAC32A87B008277969FDBB67FAD1::get_offset_of_H_0(),
	Tables64kGcmMultiplier_tCCCFA1324D15CAC32A87B008277969FDBB67FAD1::get_offset_of_M_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4826[3] = 
{
	Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA::get_offset_of_H_0(),
	Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA::get_offset_of_M_1(),
	Tables8kGcmMultiplier_tB21DA22C4B09E018603436F510743ACAB692CDEA::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4827[5] = 
{
	CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78::get_offset_of_buf_0(),
	CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78::get_offset_of_bufOff_1(),
	CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78::get_offset_of_cipher_2(),
	CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78::get_offset_of_padding_3(),
	CbcBlockCipherMac_t3276591D96748DEDC482C29892223BA18C859E78::get_offset_of_macSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4828[5] = 
{
	MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF::get_offset_of_IV_0(),
	MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF::get_offset_of_cfbV_1(),
	MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF::get_offset_of_cfbOutV_2(),
	MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF::get_offset_of_blockSize_3(),
	MacCFBBlockCipher_tDF1501DB77AE0B3B442851D8B0D9C19CB9F6A6DF::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4829[6] = 
{
	CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A::get_offset_of_mac_0(),
	CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A::get_offset_of_Buffer_1(),
	CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A::get_offset_of_bufOff_2(),
	CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A::get_offset_of_cipher_3(),
	CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A::get_offset_of_padding_4(),
	CfbBlockCipherMac_t051D9F3F7C73FA15443D825A4FDAD975F3BBDC4A::get_offset_of_macSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4830[11] = 
{
	0,
	0,
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_ZEROES_2(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_mac_3(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_buf_4(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_bufOff_5(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_cipher_6(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_macSize_7(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_L_8(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_Lu_9(),
	CMac_t8B265ABC6FE71967AE134E343F57EF5C29A75590::get_offset_of_Lu2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4831[5] = 
{
	Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED::get_offset_of_engine_0(),
	Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED::get_offset_of_macSize_1(),
	Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED::get_offset_of_inputLength_2(),
	Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED::get_offset_of_paddedKey_3(),
	Dstu7564Mac_tB00938CBF2DB9828C0E87B39A4427BE2F46FB2ED::get_offset_of_invertedKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4832[8] = 
{
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_macSize_0(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_engine_1(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_blockSize_2(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_c_3(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_cTemp_4(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_kDelta_5(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_buf_6(),
	Dstu7624Mac_t50A4296A65DDD0C7017484DCC76D157AA9B3B270::get_offset_of_bufOff_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (GMac_tCF90FDDB6B2077C95B99C8057E9364A66B97231B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4833[2] = 
{
	GMac_tCF90FDDB6B2077C95B99C8057E9364A66B97231B::get_offset_of_cipher_0(),
	GMac_tCF90FDDB6B2077C95B99C8057E9364A66B97231B::get_offset_of_macSizeBits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4834[9] = 
{
	0,
	0,
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_bufOff_2(),
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_buf_3(),
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_mac_4(),
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_firstStep_5(),
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_workingKey_6(),
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_macIV_7(),
	Gost28147Mac_tCCD420038880F2CB63BB73CF32BACF49570EC589::get_offset_of_S_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4835[9] = 
{
	0,
	0,
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_digest_2(),
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_digestSize_3(),
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_blockLength_4(),
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_ipadState_5(),
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_opadState_6(),
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_inputPad_7(),
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045::get_offset_of_outputBuf_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4836[8] = 
{
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_mac_0(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_buf_1(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_bufOff_2(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_cipher_3(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_padding_4(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_macSize_5(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_lastKey2_6(),
	ISO9797Alg3Mac_t1CCC19E2C9BCF8767A635662637E1D3CE468AEC0::get_offset_of_lastKey3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4837[23] = 
{
	0,
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_cipher_1(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_singleByte_2(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_r0_3(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_r1_4(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_r2_5(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_r3_6(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_r4_7(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_s1_8(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_s2_9(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_s3_10(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_s4_11(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_k0_12(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_k1_13(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_k2_14(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_k3_15(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_currentBlock_16(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_currentBlockOffset_17(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_h0_18(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_h1_19(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_h2_20(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_h3_21(),
	Poly1305_tD4AE1899FA0EDE1DB2690E064DB01C7D0C7D6A77::get_offset_of_h4_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4838[11] = 
{
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_c_0(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_d_1(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_k0_2(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_k1_3(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_v0_4(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_v1_5(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_v2_6(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_v3_7(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_m_8(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_wordPos_9(),
	SipHash_tA4DEBBC3ACE6DA69BDA948F9963614EDDB9E5ECE::get_offset_of_wordCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (SkeinMac_t5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4839[4] = 
{
	0,
	0,
	0,
	SkeinMac_t5E0BF4139DA8BFA79D6FD6BEB3688491D6FB4E72::get_offset_of_engine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4840[11] = 
{
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_g_0(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_n_1(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_P_2(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_s_3(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_T_4(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_workingIV_5(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_workingKey_6(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_x1_7(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_x2_8(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_x3_9(),
	VmpcMac_tCFDA77323497B45AD20B11669E0B2169B390893D::get_offset_of_x4_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4841[6] = 
{
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5::get_offset_of_stream_5(),
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5::get_offset_of_inCipher_6(),
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5::get_offset_of_outCipher_7(),
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5::get_offset_of_mInBuf_8(),
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5::get_offset_of_mInPos_9(),
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5::get_offset_of_inStreamEnded_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (DigestSink_tE8C771EFC05C3869CC8718EEB22192E09C147B2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4842[1] = 
{
	DigestSink_tE8C771EFC05C3869CC8718EEB22192E09C147B2C::get_offset_of_mDigest_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4843[3] = 
{
	DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214::get_offset_of_stream_5(),
	DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214::get_offset_of_inDigest_6(),
	DigestStream_t249C17722CC25452D9F2073E3B64F95EC8E38214::get_offset_of_outDigest_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (MacSink_tFAC174835C49820F15602AF337F88F6F496DC568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4844[1] = 
{
	MacSink_tFAC174835C49820F15602AF337F88F6F496DC568::get_offset_of_mMac_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4845[3] = 
{
	MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F::get_offset_of_stream_5(),
	MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F::get_offset_of_inMac_6(),
	MacStream_t9EE28578914186DA1B70EE7A51D9A4D87B4C5C4F::get_offset_of_outMac_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4846[1] = 
{
	SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA::get_offset_of_mSigner_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4847[3] = 
{
	SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513::get_offset_of_stream_5(),
	SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513::get_offset_of_inSigner_6(),
	SignerStream_t9C12CE9CFAFDF737A522BFA866D610C769F7E513::get_offset_of_outSigner_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4848[4] = 
{
	BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69::get_offset_of_counterStart_0(),
	BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69::get_offset_of_digest_1(),
	BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69::get_offset_of_shared_2(),
	BaseKdfBytesGenerator_t7DD462075FDA679816049B99D7935341B0B73B69::get_offset_of_iv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594), -1, sizeof(BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4849[18] = 
{
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields::get_offset_of_MAGIC_STRING_0(),
	0,
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields::get_offset_of_KP_2(),
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields::get_offset_of_KS0_3(),
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields::get_offset_of_KS1_4(),
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields::get_offset_of_KS2_5(),
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594_StaticFields::get_offset_of_KS3_6(),
	0,
	0,
	0,
	0,
	0,
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594::get_offset_of_S_12(),
	BCrypt_t7E5B1A375859E3A4E470BF5319421D67311D6594::get_offset_of_P_13(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (DesEdeKeyGenerator_t3C5856CA9C893F5CDFE0278117E6AABE95C3C9C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (DesKeyGenerator_tE440F13C9477F9601B7A7255AD0F95176D028432), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (DHBasicKeyPairGenerator_t2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4852[1] = 
{
	DHBasicKeyPairGenerator_t2D6AEC5A63BF703DDEBE1F213C471FE079A6A40F::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E), -1, sizeof(DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4853[1] = 
{
	DHKeyGeneratorHelper_t1DC07BF0E7A0C9884D5B945F2C329684D5414B1E_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (DHKeyPairGenerator_tEA77957666E1206C449FB24F859215E2AC1CDC40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4854[1] = 
{
	DHKeyPairGenerator_tEA77957666E1206C449FB24F859215E2AC1CDC40::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4855[3] = 
{
	DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF::get_offset_of_size_0(),
	DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF::get_offset_of_certainty_1(),
	DHParametersGenerator_t8D80E260053A815C62C85CA4387FEB74A443E4BF::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193), -1, sizeof(DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4856[4] = 
{
	DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields::get_offset_of_Six_0(),
	DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields::get_offset_of_primeLists_1(),
	DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields::get_offset_of_primeProducts_2(),
	DHParametersHelper_tBE454481990F499CFB42254E2F2322CBBEB0B193_StaticFields::get_offset_of_BigPrimeProducts_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1), -1, sizeof(DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4857[2] = 
{
	DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1_StaticFields::get_offset_of_One_0(),
	DsaKeyPairGenerator_t1F60F90A22BF997CBC066E2FEE5C8015DF981AF1::get_offset_of_param_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4858[7] = 
{
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_digest_0(),
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_L_1(),
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_N_2(),
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_certainty_3(),
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_random_4(),
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_use186_3_5(),
	DsaParametersGenerator_t8802061036F845CC0F4F01EB6BA2E8C6F2225B30::get_offset_of_usageIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4859[4] = 
{
	ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2::get_offset_of_algorithm_0(),
	ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2::get_offset_of_parameters_1(),
	ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2::get_offset_of_publicKeyParamSet_2(),
	ECKeyPairGenerator_t4B867B9C0AC747F88F69D7C583AE01273733A1F2::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (Ed25519KeyPairGenerator_t4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4860[1] = 
{
	Ed25519KeyPairGenerator_t4E37D7F9F738E8E9AFABFFA6BF54D78661C6BE9C::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (Ed448KeyPairGenerator_t17F93BB097D0EBB7FCEEB12E158E160F9F7D8996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4861[1] = 
{
	Ed448KeyPairGenerator_t17F93BB097D0EBB7FCEEB12E158E160F9F7D8996::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (ElGamalKeyPairGenerator_t3EA05DEEAB701DDCEA24D3B29546206DDDA176C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4862[1] = 
{
	ElGamalKeyPairGenerator_t3EA05DEEAB701DDCEA24D3B29546206DDDA176C3::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4863[3] = 
{
	ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED::get_offset_of_size_0(),
	ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED::get_offset_of_certainty_1(),
	ElGamalParametersGenerator_tFD91A3674A3141C952AE1BF5D4BD27591E684FED::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (Gost3410KeyPairGenerator_t1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4864[1] = 
{
	Gost3410KeyPairGenerator_t1AAB6958F60EE0CFD2938067AA75AC2D1D900BC2::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4865[3] = 
{
	Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9::get_offset_of_size_0(),
	Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9::get_offset_of_typeproc_1(),
	Gost3410ParametersGenerator_t9A0AF4CE709211B92ABAF5E738F38A55DFF40BD9::get_offset_of_init_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4866[5] = 
{
	HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14::get_offset_of_hMacHash_0(),
	HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14::get_offset_of_hashLen_1(),
	HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14::get_offset_of_info_2(),
	HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14::get_offset_of_currentT_3(),
	HkdfBytesGenerator_t0FFEFF9305285BFEC5321B15A80BB0BEE1992D14::get_offset_of_generatedBytes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (Kdf1BytesGenerator_tF244EEE7F3045AA629C5B57C005DBC56A22E5023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { sizeof (Kdf2BytesGenerator_t03FD38BD7C24347AF82DA450FF45C0DD665DA1BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591), -1, sizeof(KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4869[10] = 
{
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591_StaticFields::get_offset_of_IntegerMax_0(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591_StaticFields::get_offset_of_Two_1(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_prf_2(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_h_3(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_fixedInputDataCtrPrefix_4(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_fixedInputData_afterCtr_5(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_maxSizeExcl_6(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_ios_7(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_generatedBytes_8(),
	KdfCounterBytesGenerator_tC4A47F440088987896A3B7C964093578F33EA591::get_offset_of_k_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A), -1, sizeof(KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4870[11] = 
{
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_StaticFields::get_offset_of_IntegerMax_0(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A_StaticFields::get_offset_of_Two_1(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_prf_2(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_h_3(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_fixedInputData_4(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_maxSizeExcl_5(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_ios_6(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_useCounter_7(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_generatedBytes_8(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_a_9(),
	KdfDoublePipelineIterationBytesGenerator_t4C0CBE8361CB338BCC3D374EB400DED0FDA4B39A::get_offset_of_k_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C), -1, sizeof(KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4871[11] = 
{
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C_StaticFields::get_offset_of_IntegerMax_0(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C_StaticFields::get_offset_of_Two_1(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_prf_2(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_h_3(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_fixedInputData_4(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_maxSizeExcl_5(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_ios_6(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_iv_7(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_useCounter_8(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_generatedBytes_9(),
	KdfFeedbackBytesGenerator_tEE00F916C9203DF57369639E830FADA57B12770C::get_offset_of_k_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4872[3] = 
{
	Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD::get_offset_of_digest_0(),
	Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD::get_offset_of_seed_1(),
	Mgf1BytesGenerator_t2420894B5A1ADDE6415397E791D1F27AD6DDE7AD::get_offset_of_hLen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2), -1, sizeof(NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4873[2] = 
{
	NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2_StaticFields::get_offset_of_smallPrimes_0(),
	NaccacheSternKeyPairGenerator_tFBC8CD0979761449AE39A75AC4FA1BE9F44F0DA2::get_offset_of_param_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B), -1, sizeof(OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4874[4] = 
{
	OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields::get_offset_of_EncodingTable_0(),
	OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields::get_offset_of_DecodingTable_1(),
	OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields::get_offset_of_DefaultVersion_2(),
	OpenBsdBCrypt_t26AFB8A84A7F0E3BFD60C408AD49DD215CFB914B_StaticFields::get_offset_of_AllowedVersions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (OpenSslPbeParametersGenerator_t6F71231D7A73996E9AE981A032FE6D8B83D06B8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4875[1] = 
{
	OpenSslPbeParametersGenerator_t6F71231D7A73996E9AE981A032FE6D8B83D06B8F::get_offset_of_digest_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4876[6] = 
{
	0,
	0,
	0,
	Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46::get_offset_of_digest_6(),
	Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46::get_offset_of_u_7(),
	Pkcs12ParametersGenerator_t53D29DA4F9AA909BC930A169DCFC48DEBEA2CD46::get_offset_of_v_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { sizeof (Pkcs5S1ParametersGenerator_t9D4A268220EF5475B363ECC9CB9AE389001CCC6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4877[1] = 
{
	Pkcs5S1ParametersGenerator_t9D4A268220EF5475B363ECC9CB9AE389001CCC6A::get_offset_of_digest_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { sizeof (Pkcs5S2ParametersGenerator_tEBA58589AED1C9CBECB5E6B15F0949213380225A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4878[2] = 
{
	Pkcs5S2ParametersGenerator_tEBA58589AED1C9CBECB5E6B15F0949213380225A::get_offset_of_hMac_3(),
	Pkcs5S2ParametersGenerator_tEBA58589AED1C9CBECB5E6B15F0949213380225A::get_offset_of_state_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { sizeof (Poly1305KeyGenerator_t007CED9BE05565B5D2FBA5604B7F8292E0B9022E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4879[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { sizeof (RsaBlindingFactorGenerator_tB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4880[2] = 
{
	RsaBlindingFactorGenerator_tB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC::get_offset_of_key_0(),
	RsaBlindingFactorGenerator_tB1F160F5D4D7A7C0A06B861E9971036E29DEEDBC::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { sizeof (RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6), -1, sizeof(RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4881[7] = 
{
	RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields::get_offset_of_SPECIAL_E_VALUES_0(),
	RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields::get_offset_of_SPECIAL_E_HIGHEST_1(),
	RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields::get_offset_of_SPECIAL_E_BITS_2(),
	RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields::get_offset_of_One_3(),
	RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6_StaticFields::get_offset_of_DefaultPublicExponent_4(),
	0,
	RsaKeyPairGenerator_tFC66B6282E1EE95CFB47FA6570ED06AA4C19FCE6::get_offset_of_parameters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (SCrypt_tD2CF0085A3C6D2620548680FE62963E57EC24BF3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (X25519KeyPairGenerator_t67D9253D5AA27650BD98151680DD5394082CA4C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4883[1] = 
{
	X25519KeyPairGenerator_t67D9253D5AA27650BD98151680DD5394082CA4C4::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { sizeof (X448KeyPairGenerator_t828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4884[1] = 
{
	X448KeyPairGenerator_t828E8AA6C8F85B2A5B92AD49CF085FF0FA62B4B9::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83), -1, sizeof(AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4885[19] = 
{
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields::get_offset_of_S_0(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields::get_offset_of_Si_1(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields::get_offset_of_rcon_2(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields::get_offset_of_T0_3(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83_StaticFields::get_offset_of_Tinv0_4(),
	0,
	0,
	0,
	0,
	0,
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_ROUNDS_10(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_WorkingKey_11(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_C0_12(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_C1_13(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_C2_14(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_C3_15(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_forEncryption_16(),
	AesEngine_tEEF495CDA35E3E0765D955ABC5332DD85674AF83::get_offset_of_s_17(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9), -1, sizeof(AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4886[24] = 
{
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_S_0(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_Si_1(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_rcon_2(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_T0_3(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_T1_4(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_T2_5(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_T3_6(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_Tinv0_7(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_Tinv1_8(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_Tinv2_9(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9_StaticFields::get_offset_of_Tinv3_10(),
	0,
	0,
	0,
	0,
	0,
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_ROUNDS_16(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_WorkingKey_17(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_C0_18(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_C1_19(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_C2_20(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_C3_21(),
	AesFastEngine_t608EDE48725809335256492FA9E6313BED86E2E9::get_offset_of_forEncryption_22(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B), -1, sizeof(AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4887[16] = 
{
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields::get_offset_of_S_0(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields::get_offset_of_Si_1(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B_StaticFields::get_offset_of_rcon_2(),
	0,
	0,
	0,
	0,
	0,
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_ROUNDS_8(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_WorkingKey_9(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_C0_10(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_C1_11(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_C2_12(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_C3_13(),
	AesLightEngine_t8714964F9E28CF622ABD37A8AE7BCA3B9942E45B::get_offset_of_forEncryption_14(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (AesWrapEngine_t0F47D1A44813BB28E0BB4B8FA27BA5777E9388A2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208), -1, sizeof(BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4889[16] = 
{
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_KP_0(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_KS0_1(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_KS1_2(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_KS2_3(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_KS3_4(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_ROUNDS_5(),
	0,
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_SBOX_SK_7(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208_StaticFields::get_offset_of_P_SZ_8(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_S0_9(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_S1_10(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_S2_11(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_S3_12(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_P_13(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_encrypting_14(),
	BlowfishEngine_t25C089870C7A85A148516CEBFBAD3FFDC71FD208::get_offset_of_workingKey_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571), -1, sizeof(CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4890[12] = 
{
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571::get_offset_of_initialised_0(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571::get_offset_of__keyIs128_1(),
	0,
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571::get_offset_of_subkey_3(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571::get_offset_of_kw_4(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571::get_offset_of_ke_5(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571::get_offset_of_state_6(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields::get_offset_of_SIGMA_7(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields::get_offset_of_SBOX1_1110_8(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields::get_offset_of_SBOX4_4404_9(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields::get_offset_of_SBOX2_0222_10(),
	CamelliaEngine_t0413457FBFB622FF3F4A2C8B318120ED0F566571_StaticFields::get_offset_of_SBOX3_3033_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23), -1, sizeof(CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4891[9] = 
{
	0,
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23::get_offset_of_initialised_1(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23::get_offset_of__keyis128_2(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23::get_offset_of_subkey_3(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23::get_offset_of_kw_4(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23::get_offset_of_ke_5(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23::get_offset_of_state_6(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23_StaticFields::get_offset_of_SIGMA_7(),
	CamelliaLightEngine_t22B7A005A9C52F017AE98C6AE86662DB975D9A23_StaticFields::get_offset_of_SBOX1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (CamelliaWrapEngine_tD718F292DB4CCA10E30B743E5A7B5F8AB3662038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79), -1, sizeof(Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4893[16] = 
{
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S1_0(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S2_1(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S3_2(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S4_3(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S5_4(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S6_5(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S7_6(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_S8_7(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_MAX_ROUNDS_8(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79_StaticFields::get_offset_of_RED_ROUNDS_9(),
	0,
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79::get_offset_of__Kr_11(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79::get_offset_of__Km_12(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79::get_offset_of__encrypting_13(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79::get_offset_of__workingKey_14(),
	Cast5Engine_tE9B00DAD2678F047F1B44D7FC25A430D4202DE79::get_offset_of__rounds_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4894[7] = 
{
	0,
	0,
	Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE::get_offset_of__Kr_18(),
	Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE::get_offset_of__Km_19(),
	Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE::get_offset_of__Tr_20(),
	Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE::get_offset_of__Tm_21(),
	Cast6Engine_t2D3679CA3D5FE6D340DBFC102FA027626F3489FE::get_offset_of__workingKey_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (ChaChaEngine_tCF48CB190F4E47C3B3564AAEA03B967F1A01FA95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4897[4] = 
{
	DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160::get_offset_of_workingKey1_15(),
	DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160::get_offset_of_workingKey2_16(),
	DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160::get_offset_of_workingKey3_17(),
	DesEdeEngine_tF32D61F13406697BEA74EA9BCF5A978489EE0160::get_offset_of_forEncryption_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3), -1, sizeof(DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4898[8] = 
{
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_engine_0(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_param_1(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_paramPlusIV_2(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_iv_3(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_forWrapping_4(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3_StaticFields::get_offset_of_IV2_5(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_sha1_6(),
	DesEdeWrapEngine_t6FE9F55493E324A3DD8DD0072A4BCC24419C9EB3::get_offset_of_digest_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3), -1, sizeof(DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4899[15] = 
{
	0,
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3::get_offset_of_workingKey_1(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_bytebit_2(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_bigbyte_3(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_pc1_4(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_totrot_5(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_pc2_6(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP1_7(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP2_8(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP3_9(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP4_10(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP5_11(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP6_12(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP7_13(),
	DesEngine_t452535648DB7C5A7F34799A278A6BDA120C7ADA3_StaticFields::get_offset_of_SP8_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
