﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.Authentication.Credentials
struct Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78;
// BestHTTP.Caching.HTTPCacheFileInfo
struct HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D;
// BestHTTP.Connections.HTTP2.HTTP2PluginSettings
struct HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63;
// BestHTTP.Cookies.Cookie
struct Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE;
// BestHTTP.Extensions.HeartbeatManager
struct HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4;
// BestHTTP.Forms.HTTPFormBase
struct HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21;
// BestHTTP.HTTPRequest
struct HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837;
// BestHTTP.HTTPResponse
struct HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB;
// BestHTTP.Logger.ILogger
struct ILogger_t7674D28C7BC33DCD413F013E54BE3165A00F50A0;
// BestHTTP.OnBeforeHeaderSendDelegate
struct OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3;
// BestHTTP.OnBeforeRedirectionDelegate
struct OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D;
// BestHTTP.OnDownloadProgressDelegate
struct OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB;
// BestHTTP.OnRequestFinishedDelegate
struct OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251;
// BestHTTP.OnStreamingDataDelegate
struct OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52;
// BestHTTP.OnUploadProgressDelegate
struct OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A;
// BestHTTP.PlatformSupport.FileSystem.IIOService
struct IIOService_tAC889D7D660BDC95642817EAA033972D680216F3;
// BestHTTP.Proxy
struct Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ICertificateVerifyer
struct ICertificateVerifyer_t0BEEC3081E81604C8BE28A353578CCDF4657E202;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.IClientCredentialsProvider
struct IClientCredentialsProvider_t9EB3BF72C6EAC87294D093690BCC4B064FADC715;
// LitJson.ExporterFunc
struct ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D;
// LitJson.FsmContext
struct FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA;
// LitJson.IJsonWrapper
struct IJsonWrapper_t3FBFA376A6D2E04152F730CD3B2B9263BE9438FF;
// LitJson.ImporterFunc
struct ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097;
// LitJson.JsonWriter
struct JsonWriter_t1356A50FAE30049796024595C2745168258D79C0;
// LitJson.Lexer
struct Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7;
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t8A0E5BADE79D84D5B61D034A2605D78039C0524A;
// LitJson.WrapperFactory
struct WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984;
// LitJson.WriterContext
struct WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF;
// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs
struct NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`2<BestHTTP.HTTPRequest,BestHTTP.HTTPResponse>
struct Action_2_tCBA8C57FA428D737E3D2CC5441F0E3E250BCC3BA;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>
struct IDictionary_2_t7B23E29157468C53BB7F16C63AD5D7B5E3AC9159;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>
struct IDictionary_2_tA7B740B999F852AC286B8AF34180EDC6FFADF9B3;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_tDE1640262AC58F00DAF9076D04966F0A70B9E712;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>
struct IDictionary_2_t16BB1B50E391833E31FE96CE809B6196B6E98CB1;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>
struct IDictionary_2_tE82BAD96D267B8A56C93E42455FA790F80D570E0;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>
struct IDictionary_2_t8580C8B4B989F34D0F1E3136C8CC323956947AE1;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_tF79CB5A4BDC0BD94284F507C357D73D3DD2614B7;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct IDictionary_2_t6D3EA94113A5C720E032BB52D87D1AE3B615C45E;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>
struct IDictionary_2_t0D66B45A41E869DD010F591E841212A948A6D0BC;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IEnumerator_1_t8AF84849871294EB18F60C1FBEE1BD511EE2A7A6;
// System.Collections.Generic.IList`1<LitJson.JsonData>
struct IList_1_t93C988B184CF5013032344B4F0990D4C28ADAF4E;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IList_1_t7134A39CEDE45EF4D0DADA00F48BECE0C0D7CDD7;
// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie>
struct List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.Generic.Stack`1<LitJson.WriterContext>
struct Stack_1_t69BD995CB670BA59110DE1F9C8E48DBDE299B46E;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`1<System.String>
struct Func_1_tA732247549D3531F543287C316CF7F7F02F061F7;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3;
// System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean>
struct Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t06431062CF438D12908F0B93305795CB645DCCA8;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T10CF0CFC203D043C6935309D8C8487C6975F47D8_H
#define U3CMODULEU3E_T10CF0CFC203D043C6935309D8C8487C6975F47D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t10CF0CFC203D043C6935309D8C8487C6975F47D8 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T10CF0CFC203D043C6935309D8C8487C6975F47D8_H
#ifndef U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#define U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASYNCEXTENSIONS_T7CA94A7D15C8B1690C0E15C822F193911CBB8848_H
#define ASYNCEXTENSIONS_T7CA94A7D15C8B1690C0E15C822F193911CBB8848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.AsyncExtensions
struct  AsyncExtensions_t7CA94A7D15C8B1690C0E15C822F193911CBB8848  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCEXTENSIONS_T7CA94A7D15C8B1690C0E15C822F193911CBB8848_H
#ifndef HTTPRANGE_TB1278C9AEFC75DD73592AD8FD237FB270D4EB04A_H
#define HTTPRANGE_TB1278C9AEFC75DD73592AD8FD237FB270D4EB04A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRange
struct  HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.HTTPRange::<FirstBytePos>k__BackingField
	int64_t ___U3CFirstBytePosU3Ek__BackingField_0;
	// System.Int64 BestHTTP.HTTPRange::<LastBytePos>k__BackingField
	int64_t ___U3CLastBytePosU3Ek__BackingField_1;
	// System.Int64 BestHTTP.HTTPRange::<ContentLength>k__BackingField
	int64_t ___U3CContentLengthU3Ek__BackingField_2;
	// System.Boolean BestHTTP.HTTPRange::<IsValid>k__BackingField
	bool ___U3CIsValidU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFirstBytePosU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A, ___U3CFirstBytePosU3Ek__BackingField_0)); }
	inline int64_t get_U3CFirstBytePosU3Ek__BackingField_0() const { return ___U3CFirstBytePosU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CFirstBytePosU3Ek__BackingField_0() { return &___U3CFirstBytePosU3Ek__BackingField_0; }
	inline void set_U3CFirstBytePosU3Ek__BackingField_0(int64_t value)
	{
		___U3CFirstBytePosU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLastBytePosU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A, ___U3CLastBytePosU3Ek__BackingField_1)); }
	inline int64_t get_U3CLastBytePosU3Ek__BackingField_1() const { return ___U3CLastBytePosU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CLastBytePosU3Ek__BackingField_1() { return &___U3CLastBytePosU3Ek__BackingField_1; }
	inline void set_U3CLastBytePosU3Ek__BackingField_1(int64_t value)
	{
		___U3CLastBytePosU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CContentLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A, ___U3CContentLengthU3Ek__BackingField_2)); }
	inline int64_t get_U3CContentLengthU3Ek__BackingField_2() const { return ___U3CContentLengthU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3CContentLengthU3Ek__BackingField_2() { return &___U3CContentLengthU3Ek__BackingField_2; }
	inline void set_U3CContentLengthU3Ek__BackingField_2(int64_t value)
	{
		___U3CContentLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsValidU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A, ___U3CIsValidU3Ek__BackingField_3)); }
	inline bool get_U3CIsValidU3Ek__BackingField_3() const { return ___U3CIsValidU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsValidU3Ek__BackingField_3() { return &___U3CIsValidU3Ek__BackingField_3; }
	inline void set_U3CIsValidU3Ek__BackingField_3(bool value)
	{
		___U3CIsValidU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRANGE_TB1278C9AEFC75DD73592AD8FD237FB270D4EB04A_H
#ifndef U3CU3EC__DISPLAYCLASS223_0_T5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E_H
#define U3CU3EC__DISPLAYCLASS223_0_T5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequest_<>c__DisplayClass223_0
struct  U3CU3Ec__DisplayClass223_0_t5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E  : public RuntimeObject
{
public:
	// BestHTTP.Cookies.Cookie BestHTTP.HTTPRequest_<>c__DisplayClass223_0::customCookie
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE * ___customCookie_0;

public:
	inline static int32_t get_offset_of_customCookie_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass223_0_t5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E, ___customCookie_0)); }
	inline Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE * get_customCookie_0() const { return ___customCookie_0; }
	inline Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE ** get_address_of_customCookie_0() { return &___customCookie_0; }
	inline void set_customCookie_0(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE * value)
	{
		___customCookie_0 = value;
		Il2CppCodeGenWriteBarrier((&___customCookie_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS223_0_T5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E_H
#ifndef U3CU3EC__DISPLAYCLASS224_0_TF228B863E1B372FEEEC87493BDA173A1E52D78A7_H
#define U3CU3EC__DISPLAYCLASS224_0_TF228B863E1B372FEEEC87493BDA173A1E52D78A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequest_<>c__DisplayClass224_0
struct  U3CU3Ec__DisplayClass224_0_tF228B863E1B372FEEEC87493BDA173A1E52D78A7  : public RuntimeObject
{
public:
	// BestHTTP.HTTPRequest BestHTTP.HTTPRequest_<>c__DisplayClass224_0::<>4__this
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___U3CU3E4__this_0;
	// System.IO.Stream BestHTTP.HTTPRequest_<>c__DisplayClass224_0::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass224_0_tF228B863E1B372FEEEC87493BDA173A1E52D78A7, ___U3CU3E4__this_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass224_0_tF228B863E1B372FEEEC87493BDA173A1E52D78A7, ___stream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_1() const { return ___stream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS224_0_TF228B863E1B372FEEEC87493BDA173A1E52D78A7_H
#ifndef HTTPREQUESTASYNCEXTENSIONS_T760B678D5F16BCF75A432565A8506B790D9D9899_H
#define HTTPREQUESTASYNCEXTENSIONS_T760B678D5F16BCF75A432565A8506B790D9D9899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestAsyncExtensions
struct  HTTPRequestAsyncExtensions_t760B678D5F16BCF75A432565A8506B790D9D9899  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTASYNCEXTENSIONS_T760B678D5F16BCF75A432565A8506B790D9D9899_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_TE04FEADAA94381DDC7B05CB9849BBF0C650928EE_H
#define U3CU3EC__DISPLAYCLASS0_0_TE04FEADAA94381DDC7B05CB9849BBF0C650928EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_tE04FEADAA94381DDC7B05CB9849BBF0C650928EE  : public RuntimeObject
{
public:
	// BestHTTP.HTTPRequest BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass0_0::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tE04FEADAA94381DDC7B05CB9849BBF0C650928EE, ___request_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_0() const { return ___request_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_TE04FEADAA94381DDC7B05CB9849BBF0C650928EE_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TF5DF96B51CD32D46621B827561F6856FA2908597_H
#define U3CU3EC__DISPLAYCLASS1_0_TF5DF96B51CD32D46621B827561F6856FA2908597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tF5DF96B51CD32D46621B827561F6856FA2908597  : public RuntimeObject
{
public:
	// BestHTTP.HTTPRequest BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass1_0::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tF5DF96B51CD32D46621B827561F6856FA2908597, ___request_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_0() const { return ___request_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TF5DF96B51CD32D46621B827561F6856FA2908597_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T23D0047A0A2BA75CBB759A8A6AC37B2026277211_H
#define U3CU3EC__DISPLAYCLASS2_0_T23D0047A0A2BA75CBB759A8A6AC37B2026277211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t23D0047A0A2BA75CBB759A8A6AC37B2026277211  : public RuntimeObject
{
public:
	// BestHTTP.HTTPRequest BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass2_0::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t23D0047A0A2BA75CBB759A8A6AC37B2026277211, ___request_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_0() const { return ___request_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T23D0047A0A2BA75CBB759A8A6AC37B2026277211_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T380EA9C2C1052BC7C20F58EB901EFE003D8FFC78_H
#define U3CU3EC__DISPLAYCLASS3_0_T380EA9C2C1052BC7C20F58EB901EFE003D8FFC78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t380EA9C2C1052BC7C20F58EB901EFE003D8FFC78  : public RuntimeObject
{
public:
	// BestHTTP.HTTPRequest BestHTTP.HTTPRequestAsyncExtensions_<>c__DisplayClass3_0::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t380EA9C2C1052BC7C20F58EB901EFE003D8FFC78, ___request_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_0() const { return ___request_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T380EA9C2C1052BC7C20F58EB901EFE003D8FFC78_H
#ifndef HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#define HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPResponse
struct  HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.HTTPResponse::<VersionMajor>k__BackingField
	int32_t ___U3CVersionMajorU3Ek__BackingField_3;
	// System.Int32 BestHTTP.HTTPResponse::<VersionMinor>k__BackingField
	int32_t ___U3CVersionMinorU3Ek__BackingField_4;
	// System.Int32 BestHTTP.HTTPResponse::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_5;
	// System.String BestHTTP.HTTPResponse::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_6;
	// System.Boolean BestHTTP.HTTPResponse::<IsStreamed>k__BackingField
	bool ___U3CIsStreamedU3Ek__BackingField_7;
	// System.Boolean BestHTTP.HTTPResponse::<IsFromCache>k__BackingField
	bool ___U3CIsFromCacheU3Ek__BackingField_8;
	// BestHTTP.Caching.HTTPCacheFileInfo BestHTTP.HTTPResponse::<CacheFileInfo>k__BackingField
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * ___U3CCacheFileInfoU3Ek__BackingField_9;
	// System.Boolean BestHTTP.HTTPResponse::<IsCacheOnly>k__BackingField
	bool ___U3CIsCacheOnlyU3Ek__BackingField_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> BestHTTP.HTTPResponse::<Headers>k__BackingField
	Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * ___U3CHeadersU3Ek__BackingField_11;
	// System.Byte[] BestHTTP.HTTPResponse::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_12;
	// System.Boolean BestHTTP.HTTPResponse::<IsUpgraded>k__BackingField
	bool ___U3CIsUpgradedU3Ek__BackingField_13;
	// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie> BestHTTP.HTTPResponse::<Cookies>k__BackingField
	List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * ___U3CCookiesU3Ek__BackingField_14;
	// System.String BestHTTP.HTTPResponse::dataAsText
	String_t* ___dataAsText_15;
	// UnityEngine.Texture2D BestHTTP.HTTPResponse::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_16;
	// System.Boolean BestHTTP.HTTPResponse::<IsClosedManually>k__BackingField
	bool ___U3CIsClosedManuallyU3Ek__BackingField_17;
	// System.Int64 BestHTTP.HTTPResponse::UnprocessedFragments
	int64_t ___UnprocessedFragments_18;
	// BestHTTP.HTTPRequest BestHTTP.HTTPResponse::baseRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___baseRequest_19;
	// System.IO.Stream BestHTTP.HTTPResponse::Stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_20;
	// System.Byte[] BestHTTP.HTTPResponse::fragmentBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fragmentBuffer_21;
	// System.Int32 BestHTTP.HTTPResponse::fragmentBufferDataLength
	int32_t ___fragmentBufferDataLength_22;
	// System.IO.Stream BestHTTP.HTTPResponse::cacheStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___cacheStream_23;
	// System.Int32 BestHTTP.HTTPResponse::allFragmentSize
	int32_t ___allFragmentSize_24;

public:
	inline static int32_t get_offset_of_U3CVersionMajorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CVersionMajorU3Ek__BackingField_3)); }
	inline int32_t get_U3CVersionMajorU3Ek__BackingField_3() const { return ___U3CVersionMajorU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CVersionMajorU3Ek__BackingField_3() { return &___U3CVersionMajorU3Ek__BackingField_3; }
	inline void set_U3CVersionMajorU3Ek__BackingField_3(int32_t value)
	{
		___U3CVersionMajorU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVersionMinorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CVersionMinorU3Ek__BackingField_4)); }
	inline int32_t get_U3CVersionMinorU3Ek__BackingField_4() const { return ___U3CVersionMinorU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CVersionMinorU3Ek__BackingField_4() { return &___U3CVersionMinorU3Ek__BackingField_4; }
	inline void set_U3CVersionMinorU3Ek__BackingField_4(int32_t value)
	{
		___U3CVersionMinorU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CStatusCodeU3Ek__BackingField_5)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_5() const { return ___U3CStatusCodeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_5() { return &___U3CStatusCodeU3Ek__BackingField_5; }
	inline void set_U3CStatusCodeU3Ek__BackingField_5(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CMessageU3Ek__BackingField_6)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_6() const { return ___U3CMessageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_6() { return &___U3CMessageU3Ek__BackingField_6; }
	inline void set_U3CMessageU3Ek__BackingField_6(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsStreamedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsStreamedU3Ek__BackingField_7)); }
	inline bool get_U3CIsStreamedU3Ek__BackingField_7() const { return ___U3CIsStreamedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsStreamedU3Ek__BackingField_7() { return &___U3CIsStreamedU3Ek__BackingField_7; }
	inline void set_U3CIsStreamedU3Ek__BackingField_7(bool value)
	{
		___U3CIsStreamedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsFromCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsFromCacheU3Ek__BackingField_8)); }
	inline bool get_U3CIsFromCacheU3Ek__BackingField_8() const { return ___U3CIsFromCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsFromCacheU3Ek__BackingField_8() { return &___U3CIsFromCacheU3Ek__BackingField_8; }
	inline void set_U3CIsFromCacheU3Ek__BackingField_8(bool value)
	{
		___U3CIsFromCacheU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCacheFileInfoU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CCacheFileInfoU3Ek__BackingField_9)); }
	inline HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * get_U3CCacheFileInfoU3Ek__BackingField_9() const { return ___U3CCacheFileInfoU3Ek__BackingField_9; }
	inline HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D ** get_address_of_U3CCacheFileInfoU3Ek__BackingField_9() { return &___U3CCacheFileInfoU3Ek__BackingField_9; }
	inline void set_U3CCacheFileInfoU3Ek__BackingField_9(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * value)
	{
		___U3CCacheFileInfoU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheFileInfoU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIsCacheOnlyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsCacheOnlyU3Ek__BackingField_10)); }
	inline bool get_U3CIsCacheOnlyU3Ek__BackingField_10() const { return ___U3CIsCacheOnlyU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsCacheOnlyU3Ek__BackingField_10() { return &___U3CIsCacheOnlyU3Ek__BackingField_10; }
	inline void set_U3CIsCacheOnlyU3Ek__BackingField_10(bool value)
	{
		___U3CIsCacheOnlyU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CHeadersU3Ek__BackingField_11)); }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * get_U3CHeadersU3Ek__BackingField_11() const { return ___U3CHeadersU3Ek__BackingField_11; }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 ** get_address_of_U3CHeadersU3Ek__BackingField_11() { return &___U3CHeadersU3Ek__BackingField_11; }
	inline void set_U3CHeadersU3Ek__BackingField_11(Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * value)
	{
		___U3CHeadersU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CDataU3Ek__BackingField_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CIsUpgradedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsUpgradedU3Ek__BackingField_13)); }
	inline bool get_U3CIsUpgradedU3Ek__BackingField_13() const { return ___U3CIsUpgradedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsUpgradedU3Ek__BackingField_13() { return &___U3CIsUpgradedU3Ek__BackingField_13; }
	inline void set_U3CIsUpgradedU3Ek__BackingField_13(bool value)
	{
		___U3CIsUpgradedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CCookiesU3Ek__BackingField_14)); }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * get_U3CCookiesU3Ek__BackingField_14() const { return ___U3CCookiesU3Ek__BackingField_14; }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 ** get_address_of_U3CCookiesU3Ek__BackingField_14() { return &___U3CCookiesU3Ek__BackingField_14; }
	inline void set_U3CCookiesU3Ek__BackingField_14(List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * value)
	{
		___U3CCookiesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookiesU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_dataAsText_15() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___dataAsText_15)); }
	inline String_t* get_dataAsText_15() const { return ___dataAsText_15; }
	inline String_t** get_address_of_dataAsText_15() { return &___dataAsText_15; }
	inline void set_dataAsText_15(String_t* value)
	{
		___dataAsText_15 = value;
		Il2CppCodeGenWriteBarrier((&___dataAsText_15), value);
	}

	inline static int32_t get_offset_of_texture_16() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___texture_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_16() const { return ___texture_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_16() { return &___texture_16; }
	inline void set_texture_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_16 = value;
		Il2CppCodeGenWriteBarrier((&___texture_16), value);
	}

	inline static int32_t get_offset_of_U3CIsClosedManuallyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsClosedManuallyU3Ek__BackingField_17)); }
	inline bool get_U3CIsClosedManuallyU3Ek__BackingField_17() const { return ___U3CIsClosedManuallyU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsClosedManuallyU3Ek__BackingField_17() { return &___U3CIsClosedManuallyU3Ek__BackingField_17; }
	inline void set_U3CIsClosedManuallyU3Ek__BackingField_17(bool value)
	{
		___U3CIsClosedManuallyU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_UnprocessedFragments_18() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___UnprocessedFragments_18)); }
	inline int64_t get_UnprocessedFragments_18() const { return ___UnprocessedFragments_18; }
	inline int64_t* get_address_of_UnprocessedFragments_18() { return &___UnprocessedFragments_18; }
	inline void set_UnprocessedFragments_18(int64_t value)
	{
		___UnprocessedFragments_18 = value;
	}

	inline static int32_t get_offset_of_baseRequest_19() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___baseRequest_19)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_baseRequest_19() const { return ___baseRequest_19; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_baseRequest_19() { return &___baseRequest_19; }
	inline void set_baseRequest_19(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___baseRequest_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseRequest_19), value);
	}

	inline static int32_t get_offset_of_Stream_20() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___Stream_20)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Stream_20() const { return ___Stream_20; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Stream_20() { return &___Stream_20; }
	inline void set_Stream_20(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Stream_20 = value;
		Il2CppCodeGenWriteBarrier((&___Stream_20), value);
	}

	inline static int32_t get_offset_of_fragmentBuffer_21() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___fragmentBuffer_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fragmentBuffer_21() const { return ___fragmentBuffer_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fragmentBuffer_21() { return &___fragmentBuffer_21; }
	inline void set_fragmentBuffer_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fragmentBuffer_21 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentBuffer_21), value);
	}

	inline static int32_t get_offset_of_fragmentBufferDataLength_22() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___fragmentBufferDataLength_22)); }
	inline int32_t get_fragmentBufferDataLength_22() const { return ___fragmentBufferDataLength_22; }
	inline int32_t* get_address_of_fragmentBufferDataLength_22() { return &___fragmentBufferDataLength_22; }
	inline void set_fragmentBufferDataLength_22(int32_t value)
	{
		___fragmentBufferDataLength_22 = value;
	}

	inline static int32_t get_offset_of_cacheStream_23() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___cacheStream_23)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_cacheStream_23() const { return ___cacheStream_23; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_cacheStream_23() { return &___cacheStream_23; }
	inline void set_cacheStream_23(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___cacheStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___cacheStream_23), value);
	}

	inline static int32_t get_offset_of_allFragmentSize_24() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___allFragmentSize_24)); }
	inline int32_t get_allFragmentSize_24() const { return ___allFragmentSize_24; }
	inline int32_t* get_address_of_allFragmentSize_24() { return &___allFragmentSize_24; }
	inline void set_allFragmentSize_24(int32_t value)
	{
		___allFragmentSize_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#ifndef PROXY_T69197B0B3D1610ED7580B856BB07DB3FB5916DD9_H
#define PROXY_T69197B0B3D1610ED7580B856BB07DB3FB5916DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Proxy
struct  Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.Proxy::<Address>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CAddressU3Ek__BackingField_0;
	// BestHTTP.Authentication.Credentials BestHTTP.Proxy::<Credentials>k__BackingField
	Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 * ___U3CCredentialsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAddressU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9, ___U3CAddressU3Ek__BackingField_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CAddressU3Ek__BackingField_0() const { return ___U3CAddressU3Ek__BackingField_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CAddressU3Ek__BackingField_0() { return &___U3CAddressU3Ek__BackingField_0; }
	inline void set_U3CAddressU3Ek__BackingField_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CAddressU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAddressU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9, ___U3CCredentialsU3Ek__BackingField_1)); }
	inline Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 * get_U3CCredentialsU3Ek__BackingField_1() const { return ___U3CCredentialsU3Ek__BackingField_1; }
	inline Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 ** get_address_of_U3CCredentialsU3Ek__BackingField_1() { return &___U3CCredentialsU3Ek__BackingField_1; }
	inline void set_U3CCredentialsU3Ek__BackingField_1(Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 * value)
	{
		___U3CCredentialsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXY_T69197B0B3D1610ED7580B856BB07DB3FB5916DD9_H
#ifndef FSMCONTEXT_T9382B92C612C76D5F2B28F7A309A63C72C7A8BCA_H
#define FSMCONTEXT_T9382B92C612C76D5F2B28F7A309A63C72C7A8BCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.FsmContext
struct  FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA  : public RuntimeObject
{
public:
	// System.Boolean LitJson.FsmContext::Return
	bool ___Return_0;
	// System.Int32 LitJson.FsmContext::NextState
	int32_t ___NextState_1;
	// LitJson.Lexer LitJson.FsmContext::L
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 * ___L_2;
	// System.Int32 LitJson.FsmContext::StateStack
	int32_t ___StateStack_3;

public:
	inline static int32_t get_offset_of_Return_0() { return static_cast<int32_t>(offsetof(FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA, ___Return_0)); }
	inline bool get_Return_0() const { return ___Return_0; }
	inline bool* get_address_of_Return_0() { return &___Return_0; }
	inline void set_Return_0(bool value)
	{
		___Return_0 = value;
	}

	inline static int32_t get_offset_of_NextState_1() { return static_cast<int32_t>(offsetof(FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA, ___NextState_1)); }
	inline int32_t get_NextState_1() const { return ___NextState_1; }
	inline int32_t* get_address_of_NextState_1() { return &___NextState_1; }
	inline void set_NextState_1(int32_t value)
	{
		___NextState_1 = value;
	}

	inline static int32_t get_offset_of_L_2() { return static_cast<int32_t>(offsetof(FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA, ___L_2)); }
	inline Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 * get_L_2() const { return ___L_2; }
	inline Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 ** get_address_of_L_2() { return &___L_2; }
	inline void set_L_2(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 * value)
	{
		___L_2 = value;
		Il2CppCodeGenWriteBarrier((&___L_2), value);
	}

	inline static int32_t get_offset_of_StateStack_3() { return static_cast<int32_t>(offsetof(FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA, ___StateStack_3)); }
	inline int32_t get_StateStack_3() const { return ___StateStack_3; }
	inline int32_t* get_address_of_StateStack_3() { return &___StateStack_3; }
	inline void set_StateStack_3(int32_t value)
	{
		___StateStack_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMCONTEXT_T9382B92C612C76D5F2B28F7A309A63C72C7A8BCA_H
#ifndef JSONMAPPER_T1B490A9D1CD428D13C499CF81303AC5804391675_H
#define JSONMAPPER_T1B490A9D1CD428D13C499CF81303AC5804391675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper
struct  JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675  : public RuntimeObject
{
public:

public:
};

struct JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields
{
public:
	// System.Int32 LitJson.JsonMapper::max_nesting_depth
	int32_t ___max_nesting_depth_0;
	// System.IFormatProvider LitJson.JsonMapper::datetime_format
	RuntimeObject* ___datetime_format_1;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::base_exporters_table
	RuntimeObject* ___base_exporters_table_2;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::custom_exporters_table
	RuntimeObject* ___custom_exporters_table_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::base_importers_table
	RuntimeObject* ___base_importers_table_4;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::custom_importers_table
	RuntimeObject* ___custom_importers_table_5;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata> LitJson.JsonMapper::array_metadata
	RuntimeObject* ___array_metadata_6;
	// System.Object LitJson.JsonMapper::array_metadata_lock
	RuntimeObject * ___array_metadata_lock_7;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>> LitJson.JsonMapper::conv_ops
	RuntimeObject* ___conv_ops_8;
	// System.Object LitJson.JsonMapper::conv_ops_lock
	RuntimeObject * ___conv_ops_lock_9;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata> LitJson.JsonMapper::object_metadata
	RuntimeObject* ___object_metadata_10;
	// System.Object LitJson.JsonMapper::object_metadata_lock
	RuntimeObject * ___object_metadata_lock_11;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>> LitJson.JsonMapper::type_properties
	RuntimeObject* ___type_properties_12;
	// System.Object LitJson.JsonMapper::type_properties_lock
	RuntimeObject * ___type_properties_lock_13;
	// LitJson.JsonWriter LitJson.JsonMapper::static_writer
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0 * ___static_writer_14;
	// System.Object LitJson.JsonMapper::static_writer_lock
	RuntimeObject * ___static_writer_lock_15;

public:
	inline static int32_t get_offset_of_max_nesting_depth_0() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___max_nesting_depth_0)); }
	inline int32_t get_max_nesting_depth_0() const { return ___max_nesting_depth_0; }
	inline int32_t* get_address_of_max_nesting_depth_0() { return &___max_nesting_depth_0; }
	inline void set_max_nesting_depth_0(int32_t value)
	{
		___max_nesting_depth_0 = value;
	}

	inline static int32_t get_offset_of_datetime_format_1() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___datetime_format_1)); }
	inline RuntimeObject* get_datetime_format_1() const { return ___datetime_format_1; }
	inline RuntimeObject** get_address_of_datetime_format_1() { return &___datetime_format_1; }
	inline void set_datetime_format_1(RuntimeObject* value)
	{
		___datetime_format_1 = value;
		Il2CppCodeGenWriteBarrier((&___datetime_format_1), value);
	}

	inline static int32_t get_offset_of_base_exporters_table_2() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___base_exporters_table_2)); }
	inline RuntimeObject* get_base_exporters_table_2() const { return ___base_exporters_table_2; }
	inline RuntimeObject** get_address_of_base_exporters_table_2() { return &___base_exporters_table_2; }
	inline void set_base_exporters_table_2(RuntimeObject* value)
	{
		___base_exporters_table_2 = value;
		Il2CppCodeGenWriteBarrier((&___base_exporters_table_2), value);
	}

	inline static int32_t get_offset_of_custom_exporters_table_3() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___custom_exporters_table_3)); }
	inline RuntimeObject* get_custom_exporters_table_3() const { return ___custom_exporters_table_3; }
	inline RuntimeObject** get_address_of_custom_exporters_table_3() { return &___custom_exporters_table_3; }
	inline void set_custom_exporters_table_3(RuntimeObject* value)
	{
		___custom_exporters_table_3 = value;
		Il2CppCodeGenWriteBarrier((&___custom_exporters_table_3), value);
	}

	inline static int32_t get_offset_of_base_importers_table_4() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___base_importers_table_4)); }
	inline RuntimeObject* get_base_importers_table_4() const { return ___base_importers_table_4; }
	inline RuntimeObject** get_address_of_base_importers_table_4() { return &___base_importers_table_4; }
	inline void set_base_importers_table_4(RuntimeObject* value)
	{
		___base_importers_table_4 = value;
		Il2CppCodeGenWriteBarrier((&___base_importers_table_4), value);
	}

	inline static int32_t get_offset_of_custom_importers_table_5() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___custom_importers_table_5)); }
	inline RuntimeObject* get_custom_importers_table_5() const { return ___custom_importers_table_5; }
	inline RuntimeObject** get_address_of_custom_importers_table_5() { return &___custom_importers_table_5; }
	inline void set_custom_importers_table_5(RuntimeObject* value)
	{
		___custom_importers_table_5 = value;
		Il2CppCodeGenWriteBarrier((&___custom_importers_table_5), value);
	}

	inline static int32_t get_offset_of_array_metadata_6() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___array_metadata_6)); }
	inline RuntimeObject* get_array_metadata_6() const { return ___array_metadata_6; }
	inline RuntimeObject** get_address_of_array_metadata_6() { return &___array_metadata_6; }
	inline void set_array_metadata_6(RuntimeObject* value)
	{
		___array_metadata_6 = value;
		Il2CppCodeGenWriteBarrier((&___array_metadata_6), value);
	}

	inline static int32_t get_offset_of_array_metadata_lock_7() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___array_metadata_lock_7)); }
	inline RuntimeObject * get_array_metadata_lock_7() const { return ___array_metadata_lock_7; }
	inline RuntimeObject ** get_address_of_array_metadata_lock_7() { return &___array_metadata_lock_7; }
	inline void set_array_metadata_lock_7(RuntimeObject * value)
	{
		___array_metadata_lock_7 = value;
		Il2CppCodeGenWriteBarrier((&___array_metadata_lock_7), value);
	}

	inline static int32_t get_offset_of_conv_ops_8() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___conv_ops_8)); }
	inline RuntimeObject* get_conv_ops_8() const { return ___conv_ops_8; }
	inline RuntimeObject** get_address_of_conv_ops_8() { return &___conv_ops_8; }
	inline void set_conv_ops_8(RuntimeObject* value)
	{
		___conv_ops_8 = value;
		Il2CppCodeGenWriteBarrier((&___conv_ops_8), value);
	}

	inline static int32_t get_offset_of_conv_ops_lock_9() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___conv_ops_lock_9)); }
	inline RuntimeObject * get_conv_ops_lock_9() const { return ___conv_ops_lock_9; }
	inline RuntimeObject ** get_address_of_conv_ops_lock_9() { return &___conv_ops_lock_9; }
	inline void set_conv_ops_lock_9(RuntimeObject * value)
	{
		___conv_ops_lock_9 = value;
		Il2CppCodeGenWriteBarrier((&___conv_ops_lock_9), value);
	}

	inline static int32_t get_offset_of_object_metadata_10() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___object_metadata_10)); }
	inline RuntimeObject* get_object_metadata_10() const { return ___object_metadata_10; }
	inline RuntimeObject** get_address_of_object_metadata_10() { return &___object_metadata_10; }
	inline void set_object_metadata_10(RuntimeObject* value)
	{
		___object_metadata_10 = value;
		Il2CppCodeGenWriteBarrier((&___object_metadata_10), value);
	}

	inline static int32_t get_offset_of_object_metadata_lock_11() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___object_metadata_lock_11)); }
	inline RuntimeObject * get_object_metadata_lock_11() const { return ___object_metadata_lock_11; }
	inline RuntimeObject ** get_address_of_object_metadata_lock_11() { return &___object_metadata_lock_11; }
	inline void set_object_metadata_lock_11(RuntimeObject * value)
	{
		___object_metadata_lock_11 = value;
		Il2CppCodeGenWriteBarrier((&___object_metadata_lock_11), value);
	}

	inline static int32_t get_offset_of_type_properties_12() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___type_properties_12)); }
	inline RuntimeObject* get_type_properties_12() const { return ___type_properties_12; }
	inline RuntimeObject** get_address_of_type_properties_12() { return &___type_properties_12; }
	inline void set_type_properties_12(RuntimeObject* value)
	{
		___type_properties_12 = value;
		Il2CppCodeGenWriteBarrier((&___type_properties_12), value);
	}

	inline static int32_t get_offset_of_type_properties_lock_13() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___type_properties_lock_13)); }
	inline RuntimeObject * get_type_properties_lock_13() const { return ___type_properties_lock_13; }
	inline RuntimeObject ** get_address_of_type_properties_lock_13() { return &___type_properties_lock_13; }
	inline void set_type_properties_lock_13(RuntimeObject * value)
	{
		___type_properties_lock_13 = value;
		Il2CppCodeGenWriteBarrier((&___type_properties_lock_13), value);
	}

	inline static int32_t get_offset_of_static_writer_14() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___static_writer_14)); }
	inline JsonWriter_t1356A50FAE30049796024595C2745168258D79C0 * get_static_writer_14() const { return ___static_writer_14; }
	inline JsonWriter_t1356A50FAE30049796024595C2745168258D79C0 ** get_address_of_static_writer_14() { return &___static_writer_14; }
	inline void set_static_writer_14(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0 * value)
	{
		___static_writer_14 = value;
		Il2CppCodeGenWriteBarrier((&___static_writer_14), value);
	}

	inline static int32_t get_offset_of_static_writer_lock_15() { return static_cast<int32_t>(offsetof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields, ___static_writer_lock_15)); }
	inline RuntimeObject * get_static_writer_lock_15() const { return ___static_writer_lock_15; }
	inline RuntimeObject ** get_address_of_static_writer_lock_15() { return &___static_writer_lock_15; }
	inline void set_static_writer_lock_15(RuntimeObject * value)
	{
		___static_writer_lock_15 = value;
		Il2CppCodeGenWriteBarrier((&___static_writer_lock_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONMAPPER_T1B490A9D1CD428D13C499CF81303AC5804391675_H
#ifndef U3CU3EC_T521AD7E039E69019D41B747BCA400B95B5469A5D_H
#define U3CU3EC_T521AD7E039E69019D41B747BCA400B95B5469A5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper_<>c
struct  U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields
{
public:
	// LitJson.JsonMapper_<>c LitJson.JsonMapper_<>c::<>9
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D * ___U3CU3E9_0;
	// LitJson.WrapperFactory LitJson.JsonMapper_<>c::<>9__25_0
	WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * ___U3CU3E9__25_0_1;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_0
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_0_2;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_1
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_1_3;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_2
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_2_4;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_3
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_3_5;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_4
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_4_6;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_5
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_5_7;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_6
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_6_8;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_7
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_7_9;
	// LitJson.ExporterFunc LitJson.JsonMapper_<>c::<>9__26_8
	ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * ___U3CU3E9__26_8_10;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_0
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_0_11;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_1
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_1_12;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_2
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_2_13;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_3
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_3_14;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_4
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_4_15;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_5
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_5_16;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_6
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_6_17;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_7
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_7_18;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_8
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_8_19;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_9
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_9_20;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_10
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_10_21;
	// LitJson.ImporterFunc LitJson.JsonMapper_<>c::<>9__27_11
	ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * ___U3CU3E9__27_11_22;
	// LitJson.WrapperFactory LitJson.JsonMapper_<>c::<>9__32_0
	WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * ___U3CU3E9__32_0_23;
	// LitJson.WrapperFactory LitJson.JsonMapper_<>c::<>9__33_0
	WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * ___U3CU3E9__33_0_24;
	// LitJson.WrapperFactory LitJson.JsonMapper_<>c::<>9__34_0
	WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * ___U3CU3E9__34_0_25;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__25_0_1)); }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * get_U3CU3E9__25_0_1() const { return ___U3CU3E9__25_0_1; }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 ** get_address_of_U3CU3E9__25_0_1() { return &___U3CU3E9__25_0_1; }
	inline void set_U3CU3E9__25_0_1(WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * value)
	{
		___U3CU3E9__25_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_0_2)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_0_2() const { return ___U3CU3E9__26_0_2; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_0_2() { return &___U3CU3E9__26_0_2; }
	inline void set_U3CU3E9__26_0_2(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_1_3)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_1_3() const { return ___U3CU3E9__26_1_3; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_1_3() { return &___U3CU3E9__26_1_3; }
	inline void set_U3CU3E9__26_1_3(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_2_4)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_2_4() const { return ___U3CU3E9__26_2_4; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_2_4() { return &___U3CU3E9__26_2_4; }
	inline void set_U3CU3E9__26_2_4(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_3_5)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_3_5() const { return ___U3CU3E9__26_3_5; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_3_5() { return &___U3CU3E9__26_3_5; }
	inline void set_U3CU3E9__26_3_5(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_4_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_4_6)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_4_6() const { return ___U3CU3E9__26_4_6; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_4_6() { return &___U3CU3E9__26_4_6; }
	inline void set_U3CU3E9__26_4_6(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_5_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_5_7)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_5_7() const { return ___U3CU3E9__26_5_7; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_5_7() { return &___U3CU3E9__26_5_7; }
	inline void set_U3CU3E9__26_5_7(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_6_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_6_8)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_6_8() const { return ___U3CU3E9__26_6_8; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_6_8() { return &___U3CU3E9__26_6_8; }
	inline void set_U3CU3E9__26_6_8(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_6_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_7_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_7_9)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_7_9() const { return ___U3CU3E9__26_7_9; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_7_9() { return &___U3CU3E9__26_7_9; }
	inline void set_U3CU3E9__26_7_9(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_7_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_8_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__26_8_10)); }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * get_U3CU3E9__26_8_10() const { return ___U3CU3E9__26_8_10; }
	inline ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D ** get_address_of_U3CU3E9__26_8_10() { return &___U3CU3E9__26_8_10; }
	inline void set_U3CU3E9__26_8_10(ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D * value)
	{
		___U3CU3E9__26_8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_8_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_0_11)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_0_11() const { return ___U3CU3E9__27_0_11; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_0_11() { return &___U3CU3E9__27_0_11; }
	inline void set_U3CU3E9__27_0_11(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_1_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_1_12)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_1_12() const { return ___U3CU3E9__27_1_12; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_1_12() { return &___U3CU3E9__27_1_12; }
	inline void set_U3CU3E9__27_1_12(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_1_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_2_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_2_13)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_2_13() const { return ___U3CU3E9__27_2_13; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_2_13() { return &___U3CU3E9__27_2_13; }
	inline void set_U3CU3E9__27_2_13(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_2_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_2_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_3_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_3_14)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_3_14() const { return ___U3CU3E9__27_3_14; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_3_14() { return &___U3CU3E9__27_3_14; }
	inline void set_U3CU3E9__27_3_14(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_3_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_3_14), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_4_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_4_15)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_4_15() const { return ___U3CU3E9__27_4_15; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_4_15() { return &___U3CU3E9__27_4_15; }
	inline void set_U3CU3E9__27_4_15(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_4_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_4_15), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_5_16() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_5_16)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_5_16() const { return ___U3CU3E9__27_5_16; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_5_16() { return &___U3CU3E9__27_5_16; }
	inline void set_U3CU3E9__27_5_16(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_5_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_5_16), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_6_17() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_6_17)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_6_17() const { return ___U3CU3E9__27_6_17; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_6_17() { return &___U3CU3E9__27_6_17; }
	inline void set_U3CU3E9__27_6_17(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_6_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_6_17), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_7_18() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_7_18)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_7_18() const { return ___U3CU3E9__27_7_18; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_7_18() { return &___U3CU3E9__27_7_18; }
	inline void set_U3CU3E9__27_7_18(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_7_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_7_18), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_8_19() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_8_19)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_8_19() const { return ___U3CU3E9__27_8_19; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_8_19() { return &___U3CU3E9__27_8_19; }
	inline void set_U3CU3E9__27_8_19(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_8_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_8_19), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_9_20() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_9_20)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_9_20() const { return ___U3CU3E9__27_9_20; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_9_20() { return &___U3CU3E9__27_9_20; }
	inline void set_U3CU3E9__27_9_20(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_9_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_9_20), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_10_21() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_10_21)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_10_21() const { return ___U3CU3E9__27_10_21; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_10_21() { return &___U3CU3E9__27_10_21; }
	inline void set_U3CU3E9__27_10_21(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_10_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_10_21), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_11_22() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__27_11_22)); }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * get_U3CU3E9__27_11_22() const { return ___U3CU3E9__27_11_22; }
	inline ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 ** get_address_of_U3CU3E9__27_11_22() { return &___U3CU3E9__27_11_22; }
	inline void set_U3CU3E9__27_11_22(ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097 * value)
	{
		___U3CU3E9__27_11_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_11_22), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_23() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__32_0_23)); }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * get_U3CU3E9__32_0_23() const { return ___U3CU3E9__32_0_23; }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 ** get_address_of_U3CU3E9__32_0_23() { return &___U3CU3E9__32_0_23; }
	inline void set_U3CU3E9__32_0_23(WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * value)
	{
		___U3CU3E9__32_0_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_23), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_24() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__33_0_24)); }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * get_U3CU3E9__33_0_24() const { return ___U3CU3E9__33_0_24; }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 ** get_address_of_U3CU3E9__33_0_24() { return &___U3CU3E9__33_0_24; }
	inline void set_U3CU3E9__33_0_24(WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * value)
	{
		___U3CU3E9__33_0_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_0_24), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_25() { return static_cast<int32_t>(offsetof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields, ___U3CU3E9__34_0_25)); }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * get_U3CU3E9__34_0_25() const { return ___U3CU3E9__34_0_25; }
	inline WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 ** get_address_of_U3CU3E9__34_0_25() { return &___U3CU3E9__34_0_25; }
	inline void set_U3CU3E9__34_0_25(WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984 * value)
	{
		___U3CU3E9__34_0_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T521AD7E039E69019D41B747BCA400B95B5469A5D_H
#ifndef JSONMOCKWRAPPER_TE70C09363921A27A88CD21BAFC9ED3DAFD15E2A8_H
#define JSONMOCKWRAPPER_TE70C09363921A27A88CD21BAFC9ED3DAFD15E2A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMockWrapper
struct  JsonMockWrapper_tE70C09363921A27A88CD21BAFC9ED3DAFD15E2A8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONMOCKWRAPPER_TE70C09363921A27A88CD21BAFC9ED3DAFD15E2A8_H
#ifndef JSONWRITER_T1356A50FAE30049796024595C2745168258D79C0_H
#define JSONWRITER_T1356A50FAE30049796024595C2745168258D79C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonWriter
struct  JsonWriter_t1356A50FAE30049796024595C2745168258D79C0  : public RuntimeObject
{
public:
	// LitJson.WriterContext LitJson.JsonWriter::context
	WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF * ___context_1;
	// System.Collections.Generic.Stack`1<LitJson.WriterContext> LitJson.JsonWriter::ctx_stack
	Stack_1_t69BD995CB670BA59110DE1F9C8E48DBDE299B46E * ___ctx_stack_2;
	// System.Boolean LitJson.JsonWriter::has_reached_end
	bool ___has_reached_end_3;
	// System.Char[] LitJson.JsonWriter::hex_seq
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___hex_seq_4;
	// System.Int32 LitJson.JsonWriter::indentation
	int32_t ___indentation_5;
	// System.Int32 LitJson.JsonWriter::indent_value
	int32_t ___indent_value_6;
	// System.Text.StringBuilder LitJson.JsonWriter::inst_string_builder
	StringBuilder_t * ___inst_string_builder_7;
	// System.Boolean LitJson.JsonWriter::pretty_print
	bool ___pretty_print_8;
	// System.Boolean LitJson.JsonWriter::validate
	bool ___validate_9;
	// System.IO.TextWriter LitJson.JsonWriter::writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___writer_10;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___context_1)); }
	inline WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF * get_context_1() const { return ___context_1; }
	inline WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_ctx_stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___ctx_stack_2)); }
	inline Stack_1_t69BD995CB670BA59110DE1F9C8E48DBDE299B46E * get_ctx_stack_2() const { return ___ctx_stack_2; }
	inline Stack_1_t69BD995CB670BA59110DE1F9C8E48DBDE299B46E ** get_address_of_ctx_stack_2() { return &___ctx_stack_2; }
	inline void set_ctx_stack_2(Stack_1_t69BD995CB670BA59110DE1F9C8E48DBDE299B46E * value)
	{
		___ctx_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_stack_2), value);
	}

	inline static int32_t get_offset_of_has_reached_end_3() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___has_reached_end_3)); }
	inline bool get_has_reached_end_3() const { return ___has_reached_end_3; }
	inline bool* get_address_of_has_reached_end_3() { return &___has_reached_end_3; }
	inline void set_has_reached_end_3(bool value)
	{
		___has_reached_end_3 = value;
	}

	inline static int32_t get_offset_of_hex_seq_4() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___hex_seq_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_hex_seq_4() const { return ___hex_seq_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_hex_seq_4() { return &___hex_seq_4; }
	inline void set_hex_seq_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___hex_seq_4 = value;
		Il2CppCodeGenWriteBarrier((&___hex_seq_4), value);
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indent_value_6() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___indent_value_6)); }
	inline int32_t get_indent_value_6() const { return ___indent_value_6; }
	inline int32_t* get_address_of_indent_value_6() { return &___indent_value_6; }
	inline void set_indent_value_6(int32_t value)
	{
		___indent_value_6 = value;
	}

	inline static int32_t get_offset_of_inst_string_builder_7() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___inst_string_builder_7)); }
	inline StringBuilder_t * get_inst_string_builder_7() const { return ___inst_string_builder_7; }
	inline StringBuilder_t ** get_address_of_inst_string_builder_7() { return &___inst_string_builder_7; }
	inline void set_inst_string_builder_7(StringBuilder_t * value)
	{
		___inst_string_builder_7 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_builder_7), value);
	}

	inline static int32_t get_offset_of_pretty_print_8() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___pretty_print_8)); }
	inline bool get_pretty_print_8() const { return ___pretty_print_8; }
	inline bool* get_address_of_pretty_print_8() { return &___pretty_print_8; }
	inline void set_pretty_print_8(bool value)
	{
		___pretty_print_8 = value;
	}

	inline static int32_t get_offset_of_validate_9() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___validate_9)); }
	inline bool get_validate_9() const { return ___validate_9; }
	inline bool* get_address_of_validate_9() { return &___validate_9; }
	inline void set_validate_9(bool value)
	{
		___validate_9 = value;
	}

	inline static int32_t get_offset_of_writer_10() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0, ___writer_10)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_writer_10() const { return ___writer_10; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_writer_10() { return &___writer_10; }
	inline void set_writer_10(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___writer_10 = value;
		Il2CppCodeGenWriteBarrier((&___writer_10), value);
	}
};

struct JsonWriter_t1356A50FAE30049796024595C2745168258D79C0_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo LitJson.JsonWriter::number_format
	NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * ___number_format_0;

public:
	inline static int32_t get_offset_of_number_format_0() { return static_cast<int32_t>(offsetof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0_StaticFields, ___number_format_0)); }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * get_number_format_0() const { return ___number_format_0; }
	inline NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 ** get_address_of_number_format_0() { return &___number_format_0; }
	inline void set_number_format_0(NumberFormatInfo_tFDF57037EBC5BC833D0A53EF0327B805994860A8 * value)
	{
		___number_format_0 = value;
		Il2CppCodeGenWriteBarrier((&___number_format_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T1356A50FAE30049796024595C2745168258D79C0_H
#ifndef LEXER_TF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_H
#define LEXER_TF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Lexer
struct  Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7  : public RuntimeObject
{
public:
	// System.Boolean LitJson.Lexer::allow_comments
	bool ___allow_comments_2;
	// System.Boolean LitJson.Lexer::allow_single_quoted_strings
	bool ___allow_single_quoted_strings_3;
	// System.Boolean LitJson.Lexer::end_of_input
	bool ___end_of_input_4;
	// LitJson.FsmContext LitJson.Lexer::fsm_context
	FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA * ___fsm_context_5;
	// System.Int32 LitJson.Lexer::input_buffer
	int32_t ___input_buffer_6;
	// System.Int32 LitJson.Lexer::input_char
	int32_t ___input_char_7;
	// System.IO.TextReader LitJson.Lexer::reader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___reader_8;
	// System.Int32 LitJson.Lexer::state
	int32_t ___state_9;
	// System.Text.StringBuilder LitJson.Lexer::string_buffer
	StringBuilder_t * ___string_buffer_10;
	// System.String LitJson.Lexer::string_value
	String_t* ___string_value_11;
	// System.Int32 LitJson.Lexer::token
	int32_t ___token_12;
	// System.Int32 LitJson.Lexer::unichar
	int32_t ___unichar_13;

public:
	inline static int32_t get_offset_of_allow_comments_2() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___allow_comments_2)); }
	inline bool get_allow_comments_2() const { return ___allow_comments_2; }
	inline bool* get_address_of_allow_comments_2() { return &___allow_comments_2; }
	inline void set_allow_comments_2(bool value)
	{
		___allow_comments_2 = value;
	}

	inline static int32_t get_offset_of_allow_single_quoted_strings_3() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___allow_single_quoted_strings_3)); }
	inline bool get_allow_single_quoted_strings_3() const { return ___allow_single_quoted_strings_3; }
	inline bool* get_address_of_allow_single_quoted_strings_3() { return &___allow_single_quoted_strings_3; }
	inline void set_allow_single_quoted_strings_3(bool value)
	{
		___allow_single_quoted_strings_3 = value;
	}

	inline static int32_t get_offset_of_end_of_input_4() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___end_of_input_4)); }
	inline bool get_end_of_input_4() const { return ___end_of_input_4; }
	inline bool* get_address_of_end_of_input_4() { return &___end_of_input_4; }
	inline void set_end_of_input_4(bool value)
	{
		___end_of_input_4 = value;
	}

	inline static int32_t get_offset_of_fsm_context_5() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___fsm_context_5)); }
	inline FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA * get_fsm_context_5() const { return ___fsm_context_5; }
	inline FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA ** get_address_of_fsm_context_5() { return &___fsm_context_5; }
	inline void set_fsm_context_5(FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA * value)
	{
		___fsm_context_5 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_context_5), value);
	}

	inline static int32_t get_offset_of_input_buffer_6() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___input_buffer_6)); }
	inline int32_t get_input_buffer_6() const { return ___input_buffer_6; }
	inline int32_t* get_address_of_input_buffer_6() { return &___input_buffer_6; }
	inline void set_input_buffer_6(int32_t value)
	{
		___input_buffer_6 = value;
	}

	inline static int32_t get_offset_of_input_char_7() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___input_char_7)); }
	inline int32_t get_input_char_7() const { return ___input_char_7; }
	inline int32_t* get_address_of_input_char_7() { return &___input_char_7; }
	inline void set_input_char_7(int32_t value)
	{
		___input_char_7 = value;
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___reader_8)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_reader_8() const { return ___reader_8; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___state_9)); }
	inline int32_t get_state_9() const { return ___state_9; }
	inline int32_t* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(int32_t value)
	{
		___state_9 = value;
	}

	inline static int32_t get_offset_of_string_buffer_10() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___string_buffer_10)); }
	inline StringBuilder_t * get_string_buffer_10() const { return ___string_buffer_10; }
	inline StringBuilder_t ** get_address_of_string_buffer_10() { return &___string_buffer_10; }
	inline void set_string_buffer_10(StringBuilder_t * value)
	{
		___string_buffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___string_buffer_10), value);
	}

	inline static int32_t get_offset_of_string_value_11() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___string_value_11)); }
	inline String_t* get_string_value_11() const { return ___string_value_11; }
	inline String_t** get_address_of_string_value_11() { return &___string_value_11; }
	inline void set_string_value_11(String_t* value)
	{
		___string_value_11 = value;
		Il2CppCodeGenWriteBarrier((&___string_value_11), value);
	}

	inline static int32_t get_offset_of_token_12() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___token_12)); }
	inline int32_t get_token_12() const { return ___token_12; }
	inline int32_t* get_address_of_token_12() { return &___token_12; }
	inline void set_token_12(int32_t value)
	{
		___token_12 = value;
	}

	inline static int32_t get_offset_of_unichar_13() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7, ___unichar_13)); }
	inline int32_t get_unichar_13() const { return ___unichar_13; }
	inline int32_t* get_address_of_unichar_13() { return &___unichar_13; }
	inline void set_unichar_13(int32_t value)
	{
		___unichar_13 = value;
	}
};

struct Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_StaticFields
{
public:
	// System.Int32[] LitJson.Lexer::fsm_return_table
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fsm_return_table_0;
	// LitJson.Lexer_StateHandler[] LitJson.Lexer::fsm_handler_table
	StateHandlerU5BU5D_t8A0E5BADE79D84D5B61D034A2605D78039C0524A* ___fsm_handler_table_1;

public:
	inline static int32_t get_offset_of_fsm_return_table_0() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_StaticFields, ___fsm_return_table_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fsm_return_table_0() const { return ___fsm_return_table_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fsm_return_table_0() { return &___fsm_return_table_0; }
	inline void set_fsm_return_table_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fsm_return_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_return_table_0), value);
	}

	inline static int32_t get_offset_of_fsm_handler_table_1() { return static_cast<int32_t>(offsetof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_StaticFields, ___fsm_handler_table_1)); }
	inline StateHandlerU5BU5D_t8A0E5BADE79D84D5B61D034A2605D78039C0524A* get_fsm_handler_table_1() const { return ___fsm_handler_table_1; }
	inline StateHandlerU5BU5D_t8A0E5BADE79D84D5B61D034A2605D78039C0524A** get_address_of_fsm_handler_table_1() { return &___fsm_handler_table_1; }
	inline void set_fsm_handler_table_1(StateHandlerU5BU5D_t8A0E5BADE79D84D5B61D034A2605D78039C0524A* value)
	{
		___fsm_handler_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_handler_table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_TF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_H
#ifndef ORDEREDDICTIONARYENUMERATOR_T190D292E0F6DACF487A8C2AA0904EDAE0456AFE2_H
#define ORDEREDDICTIONARYENUMERATOR_T190D292E0F6DACF487A8C2AA0904EDAE0456AFE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.OrderedDictionaryEnumerator
struct  OrderedDictionaryEnumerator_t190D292E0F6DACF487A8C2AA0904EDAE0456AFE2  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.OrderedDictionaryEnumerator::list_enumerator
	RuntimeObject* ___list_enumerator_0;

public:
	inline static int32_t get_offset_of_list_enumerator_0() { return static_cast<int32_t>(offsetof(OrderedDictionaryEnumerator_t190D292E0F6DACF487A8C2AA0904EDAE0456AFE2, ___list_enumerator_0)); }
	inline RuntimeObject* get_list_enumerator_0() const { return ___list_enumerator_0; }
	inline RuntimeObject** get_address_of_list_enumerator_0() { return &___list_enumerator_0; }
	inline void set_list_enumerator_0(RuntimeObject* value)
	{
		___list_enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDEREDDICTIONARYENUMERATOR_T190D292E0F6DACF487A8C2AA0904EDAE0456AFE2_H
#ifndef WRITERCONTEXT_T544FC7907F836B87A67DDB96E4199A03D14625CF_H
#define WRITERCONTEXT_T544FC7907F836B87A67DDB96E4199A03D14625CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.WriterContext
struct  WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF  : public RuntimeObject
{
public:
	// System.Int32 LitJson.WriterContext::Count
	int32_t ___Count_0;
	// System.Boolean LitJson.WriterContext::InArray
	bool ___InArray_1;
	// System.Boolean LitJson.WriterContext::InObject
	bool ___InObject_2;
	// System.Boolean LitJson.WriterContext::ExpectingValue
	bool ___ExpectingValue_3;
	// System.Int32 LitJson.WriterContext::Padding
	int32_t ___Padding_4;

public:
	inline static int32_t get_offset_of_Count_0() { return static_cast<int32_t>(offsetof(WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF, ___Count_0)); }
	inline int32_t get_Count_0() const { return ___Count_0; }
	inline int32_t* get_address_of_Count_0() { return &___Count_0; }
	inline void set_Count_0(int32_t value)
	{
		___Count_0 = value;
	}

	inline static int32_t get_offset_of_InArray_1() { return static_cast<int32_t>(offsetof(WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF, ___InArray_1)); }
	inline bool get_InArray_1() const { return ___InArray_1; }
	inline bool* get_address_of_InArray_1() { return &___InArray_1; }
	inline void set_InArray_1(bool value)
	{
		___InArray_1 = value;
	}

	inline static int32_t get_offset_of_InObject_2() { return static_cast<int32_t>(offsetof(WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF, ___InObject_2)); }
	inline bool get_InObject_2() const { return ___InObject_2; }
	inline bool* get_address_of_InObject_2() { return &___InObject_2; }
	inline void set_InObject_2(bool value)
	{
		___InObject_2 = value;
	}

	inline static int32_t get_offset_of_ExpectingValue_3() { return static_cast<int32_t>(offsetof(WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF, ___ExpectingValue_3)); }
	inline bool get_ExpectingValue_3() const { return ___ExpectingValue_3; }
	inline bool* get_address_of_ExpectingValue_3() { return &___ExpectingValue_3; }
	inline void set_ExpectingValue_3(bool value)
	{
		___ExpectingValue_3 = value;
	}

	inline static int32_t get_offset_of_Padding_4() { return static_cast<int32_t>(offsetof(WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF, ___Padding_4)); }
	inline int32_t get_Padding_4() const { return ___Padding_4; }
	inline int32_t* get_address_of_Padding_4() { return &___Padding_4; }
	inline void set_Padding_4(int32_t value)
	{
		___Padding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITERCONTEXT_T544FC7907F836B87A67DDB96E4199A03D14625CF_H
#ifndef READONLYLIST_T8D6A67A2711D0EB37EF4376A3F5FCEB020565555_H
#define READONLYLIST_T8D6A67A2711D0EB37EF4376A3F5FCEB020565555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.Specialized.ReadOnlyList
struct  ReadOnlyList_t8D6A67A2711D0EB37EF4376A3F5FCEB020565555  : public RuntimeObject
{
public:
	// System.Collections.IList PlatformSupport.Collections.Specialized.ReadOnlyList::_list
	RuntimeObject* ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(ReadOnlyList_t8D6A67A2711D0EB37EF4376A3F5FCEB020565555, ____list_0)); }
	inline RuntimeObject* get__list_0() const { return ____list_0; }
	inline RuntimeObject** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(RuntimeObject* value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYLIST_T8D6A67A2711D0EB37EF4376A3F5FCEB020565555_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#define VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.UI.VertexHelperExtension
struct  VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___rectTransform_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifndef LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#define LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_ToRebuild_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mgU24cache0
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache0
	Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache1
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache2
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache3
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache4
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifndef LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#define LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache0
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache1
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache2
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache3
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache4
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache5
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache6
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache7
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifndef REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#define REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifndef SECURITY_T3DAE904F4BB1A19CC48BC7B72BA001A4E2066B0C_H
#define SECURITY_T3DAE904F4BB1A19CC48BC7B72BA001A4E2066B0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// crypto.Security
struct  Security_t3DAE904F4BB1A19CC48BC7B72BA001A4E2066B0C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITY_T3DAE904F4BB1A19CC48BC7B72BA001A4E2066B0C_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef HTTPPROXY_T701328FB5A83431E29FCED469AEF8AD519C2EEDA_H
#define HTTPPROXY_T701328FB5A83431E29FCED469AEF8AD519C2EEDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPProxy
struct  HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA  : public Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9
{
public:
	// System.Boolean BestHTTP.HTTPProxy::<IsTransparent>k__BackingField
	bool ___U3CIsTransparentU3Ek__BackingField_2;
	// System.Boolean BestHTTP.HTTPProxy::<SendWholeUri>k__BackingField
	bool ___U3CSendWholeUriU3Ek__BackingField_3;
	// System.Boolean BestHTTP.HTTPProxy::<NonTransparentForHTTPS>k__BackingField
	bool ___U3CNonTransparentForHTTPSU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsTransparentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA, ___U3CIsTransparentU3Ek__BackingField_2)); }
	inline bool get_U3CIsTransparentU3Ek__BackingField_2() const { return ___U3CIsTransparentU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsTransparentU3Ek__BackingField_2() { return &___U3CIsTransparentU3Ek__BackingField_2; }
	inline void set_U3CIsTransparentU3Ek__BackingField_2(bool value)
	{
		___U3CIsTransparentU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CSendWholeUriU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA, ___U3CSendWholeUriU3Ek__BackingField_3)); }
	inline bool get_U3CSendWholeUriU3Ek__BackingField_3() const { return ___U3CSendWholeUriU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CSendWholeUriU3Ek__BackingField_3() { return &___U3CSendWholeUriU3Ek__BackingField_3; }
	inline void set_U3CSendWholeUriU3Ek__BackingField_3(bool value)
	{
		___U3CSendWholeUriU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CNonTransparentForHTTPSU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA, ___U3CNonTransparentForHTTPSU3Ek__BackingField_4)); }
	inline bool get_U3CNonTransparentForHTTPSU3Ek__BackingField_4() const { return ___U3CNonTransparentForHTTPSU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CNonTransparentForHTTPSU3Ek__BackingField_4() { return &___U3CNonTransparentForHTTPSU3Ek__BackingField_4; }
	inline void set_U3CNonTransparentForHTTPSU3Ek__BackingField_4(bool value)
	{
		___U3CNonTransparentForHTTPSU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPROXY_T701328FB5A83431E29FCED469AEF8AD519C2EEDA_H
#ifndef SOCKSPROXY_T2806A10A000F70E4CEA7258EF9BA64C3A1E0A76A_H
#define SOCKSPROXY_T2806A10A000F70E4CEA7258EF9BA64C3A1E0A76A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SOCKSProxy
struct  SOCKSProxy_t2806A10A000F70E4CEA7258EF9BA64C3A1E0A76A  : public Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKSPROXY_T2806A10A000F70E4CEA7258EF9BA64C3A1E0A76A_H
#ifndef ARRAYMETADATA_T343028BCC6F54CE69728922FE052E194A558F360_H
#define ARRAYMETADATA_T343028BCC6F54CE69728922FE052E194A558F360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ArrayMetadata
struct  ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360 
{
public:
	// System.Type LitJson.ArrayMetadata::element_type
	Type_t * ___element_type_0;
	// System.Boolean LitJson.ArrayMetadata::is_array
	bool ___is_array_1;
	// System.Boolean LitJson.ArrayMetadata::is_list
	bool ___is_list_2;

public:
	inline static int32_t get_offset_of_element_type_0() { return static_cast<int32_t>(offsetof(ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360, ___element_type_0)); }
	inline Type_t * get_element_type_0() const { return ___element_type_0; }
	inline Type_t ** get_address_of_element_type_0() { return &___element_type_0; }
	inline void set_element_type_0(Type_t * value)
	{
		___element_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_type_0), value);
	}

	inline static int32_t get_offset_of_is_array_1() { return static_cast<int32_t>(offsetof(ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360, ___is_array_1)); }
	inline bool get_is_array_1() const { return ___is_array_1; }
	inline bool* get_address_of_is_array_1() { return &___is_array_1; }
	inline void set_is_array_1(bool value)
	{
		___is_array_1 = value;
	}

	inline static int32_t get_offset_of_is_list_2() { return static_cast<int32_t>(offsetof(ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360, ___is_list_2)); }
	inline bool get_is_list_2() const { return ___is_list_2; }
	inline bool* get_address_of_is_list_2() { return &___is_list_2; }
	inline void set_is_list_2(bool value)
	{
		___is_list_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LitJson.ArrayMetadata
struct ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360_marshaled_pinvoke
{
	Type_t * ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
// Native definition for COM marshalling of LitJson.ArrayMetadata
struct ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360_marshaled_com
{
	Type_t * ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
#endif // ARRAYMETADATA_T343028BCC6F54CE69728922FE052E194A558F360_H
#ifndef JSONEXCEPTION_T69F2F742960FA18FF528C4C415B6E6B7DB8F0323_H
#define JSONEXCEPTION_T69F2F742960FA18FF528C4C415B6E6B7DB8F0323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonException
struct  JsonException_t69F2F742960FA18FF528C4C415B6E6B7DB8F0323  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T69F2F742960FA18FF528C4C415B6E6B7DB8F0323_H
#ifndef OBJECTMETADATA_T2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98_H
#define OBJECTMETADATA_T2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ObjectMetadata
struct  ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98 
{
public:
	// System.Type LitJson.ObjectMetadata::element_type
	Type_t * ___element_type_0;
	// System.Boolean LitJson.ObjectMetadata::is_dictionary
	bool ___is_dictionary_1;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::properties
	RuntimeObject* ___properties_2;

public:
	inline static int32_t get_offset_of_element_type_0() { return static_cast<int32_t>(offsetof(ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98, ___element_type_0)); }
	inline Type_t * get_element_type_0() const { return ___element_type_0; }
	inline Type_t ** get_address_of_element_type_0() { return &___element_type_0; }
	inline void set_element_type_0(Type_t * value)
	{
		___element_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_type_0), value);
	}

	inline static int32_t get_offset_of_is_dictionary_1() { return static_cast<int32_t>(offsetof(ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98, ___is_dictionary_1)); }
	inline bool get_is_dictionary_1() const { return ___is_dictionary_1; }
	inline bool* get_address_of_is_dictionary_1() { return &___is_dictionary_1; }
	inline void set_is_dictionary_1(bool value)
	{
		___is_dictionary_1 = value;
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98, ___properties_2)); }
	inline RuntimeObject* get_properties_2() const { return ___properties_2; }
	inline RuntimeObject** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(RuntimeObject* value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LitJson.ObjectMetadata
struct ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98_marshaled_pinvoke
{
	Type_t * ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
// Native definition for COM marshalling of LitJson.ObjectMetadata
struct ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98_marshaled_com
{
	Type_t * ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
#endif // OBJECTMETADATA_T2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98_H
#ifndef PROPERTYMETADATA_TD5575EA22560EE44D9A460B40D71A8E56951AD32_H
#define PROPERTYMETADATA_TD5575EA22560EE44D9A460B40D71A8E56951AD32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.PropertyMetadata
struct  PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32 
{
public:
	// System.Reflection.MemberInfo LitJson.PropertyMetadata::Info
	MemberInfo_t * ___Info_0;
	// System.Boolean LitJson.PropertyMetadata::IsField
	bool ___IsField_1;
	// System.Type LitJson.PropertyMetadata::Type
	Type_t * ___Type_2;

public:
	inline static int32_t get_offset_of_Info_0() { return static_cast<int32_t>(offsetof(PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32, ___Info_0)); }
	inline MemberInfo_t * get_Info_0() const { return ___Info_0; }
	inline MemberInfo_t ** get_address_of_Info_0() { return &___Info_0; }
	inline void set_Info_0(MemberInfo_t * value)
	{
		___Info_0 = value;
		Il2CppCodeGenWriteBarrier((&___Info_0), value);
	}

	inline static int32_t get_offset_of_IsField_1() { return static_cast<int32_t>(offsetof(PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32, ___IsField_1)); }
	inline bool get_IsField_1() const { return ___IsField_1; }
	inline bool* get_address_of_IsField_1() { return &___IsField_1; }
	inline void set_IsField_1(bool value)
	{
		___IsField_1 = value;
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of LitJson.PropertyMetadata
struct PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32_marshaled_pinvoke
{
	MemberInfo_t * ___Info_0;
	int32_t ___IsField_1;
	Type_t * ___Type_2;
};
// Native definition for COM marshalling of LitJson.PropertyMetadata
struct PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32_marshaled_com
{
	MemberInfo_t * ___Info_0;
	int32_t ___IsField_1;
	Type_t * ___Type_2;
};
#endif // PROPERTYMETADATA_TD5575EA22560EE44D9A460B40D71A8E56951AD32_H
#ifndef EMBEDDEDATTRIBUTE_T1153BBC45B381753B14574A9E276E3C7B9C63CAE_H
#define EMBEDDEDATTRIBUTE_T1153BBC45B381753B14574A9E276E3C7B9C63CAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Microsoft.CodeAnalysis.EmbeddedAttribute
struct  EmbeddedAttribute_t1153BBC45B381753B14574A9E276E3C7B9C63CAE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMBEDDEDATTRIBUTE_T1153BBC45B381753B14574A9E276E3C7B9C63CAE_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ISREADONLYATTRIBUTE_TD9D90EFF5545DBE43B58F5B76A4191B55489B1AB_H
#define ISREADONLYATTRIBUTE_TD9D90EFF5545DBE43B58F5B76A4191B55489B1AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct  IsReadOnlyAttribute_tD9D90EFF5545DBE43B58F5B76A4191B55489B1AB  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISREADONLYATTRIBUTE_TD9D90EFF5545DBE43B58F5B76A4191B55489B1AB_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#define SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.SupportedProtocols
struct  SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2 
{
public:
	// System.Int32 BestHTTP.Connections.SupportedProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#ifndef HTTPFORMUSAGE_TA7C75083C6FD2D3A73090E99E6C03C1645632CDB_H
#define HTTPFORMUSAGE_TA7C75083C6FD2D3A73090E99E6C03C1645632CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Forms.HTTPFormUsage
struct  HTTPFormUsage_tA7C75083C6FD2D3A73090E99E6C03C1645632CDB 
{
public:
	// System.Int32 BestHTTP.Forms.HTTPFormUsage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTPFormUsage_tA7C75083C6FD2D3A73090E99E6C03C1645632CDB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPFORMUSAGE_TA7C75083C6FD2D3A73090E99E6C03C1645632CDB_H
#ifndef HTTPMETHODS_TC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF_H
#define HTTPMETHODS_TC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPMethods
struct  HTTPMethods_tC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF 
{
public:
	// System.Byte BestHTTP.HTTPMethods::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTPMethods_tC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMETHODS_TC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF_H
#ifndef HTTPREQUESTSTATES_T42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E_H
#define HTTPREQUESTSTATES_T42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestStates
struct  HTTPRequestStates_t42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E 
{
public:
	// System.Int32 BestHTTP.HTTPRequestStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTPRequestStates_t42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTSTATES_T42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E_H
#ifndef SOCKSADDRESSTYPES_TAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91_H
#define SOCKSADDRESSTYPES_TAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SOCKSAddressTypes
struct  SOCKSAddressTypes_tAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91 
{
public:
	// System.Int32 BestHTTP.SOCKSAddressTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SOCKSAddressTypes_tAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKSADDRESSTYPES_TAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91_H
#ifndef SOCKSMETHODS_T361BE71276EE28AB85843D6B56430F8DE2A2D96F_H
#define SOCKSMETHODS_T361BE71276EE28AB85843D6B56430F8DE2A2D96F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SOCKSMethods
struct  SOCKSMethods_t361BE71276EE28AB85843D6B56430F8DE2A2D96F 
{
public:
	// System.Byte BestHTTP.SOCKSMethods::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SOCKSMethods_t361BE71276EE28AB85843D6B56430F8DE2A2D96F, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKSMETHODS_T361BE71276EE28AB85843D6B56430F8DE2A2D96F_H
#ifndef SOCKSREPLIES_T1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9_H
#define SOCKSREPLIES_T1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SOCKSReplies
struct  SOCKSReplies_t1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9 
{
public:
	// System.Byte BestHTTP.SOCKSReplies::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SOCKSReplies_t1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKSREPLIES_T1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9_H
#ifndef SOCKSVERSIONS_T4FFB7BF1E7FAE869954B000547FCDBA3928A3982_H
#define SOCKSVERSIONS_T4FFB7BF1E7FAE869954B000547FCDBA3928A3982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SOCKSVersions
struct  SOCKSVersions_t4FFB7BF1E7FAE869954B000547FCDBA3928A3982 
{
public:
	// System.Byte BestHTTP.SOCKSVersions::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SOCKSVersions_t4FFB7BF1E7FAE869954B000547FCDBA3928A3982, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKSVERSIONS_T4FFB7BF1E7FAE869954B000547FCDBA3928A3982_H
#ifndef SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#define SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ShutdownTypes
struct  ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A 
{
public:
	// System.Int32 BestHTTP.ShutdownTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#ifndef WEBSOCKETSTATES_TEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460_H
#define WEBSOCKETSTATES_TEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.WebSocket.WebSocketStates
struct  WebSocketStates_tEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460 
{
public:
	// System.Byte BestHTTP.WebSocket.WebSocketStates::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebSocketStates_tEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTATES_TEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460_H
#ifndef CONDITION_T2BD17CEF1AA9B94C3097DA7170A96A5356A5023F_H
#define CONDITION_T2BD17CEF1AA9B94C3097DA7170A96A5356A5023F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Condition
struct  Condition_t2BD17CEF1AA9B94C3097DA7170A96A5356A5023F 
{
public:
	// System.Int32 LitJson.Condition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Condition_t2BD17CEF1AA9B94C3097DA7170A96A5356A5023F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITION_T2BD17CEF1AA9B94C3097DA7170A96A5356A5023F_H
#ifndef JSONTOKEN_T38920084F71838929071031D5129A8F2F7E05F98_H
#define JSONTOKEN_T38920084F71838929071031D5129A8F2F7E05F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonToken
struct  JsonToken_t38920084F71838929071031D5129A8F2F7E05F98 
{
public:
	// System.Int32 LitJson.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_t38920084F71838929071031D5129A8F2F7E05F98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T38920084F71838929071031D5129A8F2F7E05F98_H
#ifndef JSONTYPE_TEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731_H
#define JSONTYPE_TEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonType
struct  JsonType_tEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731 
{
public:
	// System.Int32 LitJson.JsonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonType_tEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPE_TEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731_H
#ifndef PARSERTOKEN_TD8E3691C8674AA12C50D4F24927091A65ED60CBC_H
#define PARSERTOKEN_TD8E3691C8674AA12C50D4F24927091A65ED60CBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ParserToken
struct  ParserToken_tD8E3691C8674AA12C50D4F24927091A65ED60CBC 
{
public:
	// System.Int32 LitJson.ParserToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParserToken_tD8E3691C8674AA12C50D4F24927091A65ED60CBC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTOKEN_TD8E3691C8674AA12C50D4F24927091A65ED60CBC_H
#ifndef NOTIFYCOLLECTIONCHANGEDACTION_T56484E7A9A3F7F0D831548351033C2E73D7700FA_H
#define NOTIFYCOLLECTIONCHANGEDACTION_T56484E7A9A3F7F0D831548351033C2E73D7700FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction
struct  NotifyCollectionChangedAction_t56484E7A9A3F7F0D831548351033C2E73D7700FA 
{
public:
	// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedAction_t56484E7A9A3F7F0D831548351033C2E73D7700FA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDACTION_T56484E7A9A3F7F0D831548351033C2E73D7700FA_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#define RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef HTTPMANAGER_T0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_H
#define HTTPMANAGER_T0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPManager
struct  HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A  : public RuntimeObject
{
public:

public:
};

struct HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2PluginSettings BestHTTP.HTTPManager::HTTP2Settings
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63 * ___HTTP2Settings_0;
	// System.Byte BestHTTP.HTTPManager::maxConnectionPerServer
	uint8_t ___maxConnectionPerServer_1;
	// System.Boolean BestHTTP.HTTPManager::<KeepAliveDefaultValue>k__BackingField
	bool ___U3CKeepAliveDefaultValueU3Ek__BackingField_2;
	// System.Boolean BestHTTP.HTTPManager::<IsCachingDisabled>k__BackingField
	bool ___U3CIsCachingDisabledU3Ek__BackingField_3;
	// System.TimeSpan BestHTTP.HTTPManager::<MaxConnectionIdleTime>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CMaxConnectionIdleTimeU3Ek__BackingField_4;
	// System.Boolean BestHTTP.HTTPManager::<IsCookiesEnabled>k__BackingField
	bool ___U3CIsCookiesEnabledU3Ek__BackingField_5;
	// System.UInt32 BestHTTP.HTTPManager::<CookieJarSize>k__BackingField
	uint32_t ___U3CCookieJarSizeU3Ek__BackingField_6;
	// System.Boolean BestHTTP.HTTPManager::<EnablePrivateBrowsing>k__BackingField
	bool ___U3CEnablePrivateBrowsingU3Ek__BackingField_7;
	// System.TimeSpan BestHTTP.HTTPManager::<ConnectTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CConnectTimeoutU3Ek__BackingField_8;
	// System.TimeSpan BestHTTP.HTTPManager::<RequestTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CRequestTimeoutU3Ek__BackingField_9;
	// System.Func`1<System.String> BestHTTP.HTTPManager::<RootCacheFolderProvider>k__BackingField
	Func_1_tA732247549D3531F543287C316CF7F7F02F061F7 * ___U3CRootCacheFolderProviderU3Ek__BackingField_10;
	// BestHTTP.Proxy BestHTTP.HTTPManager::<Proxy>k__BackingField
	Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 * ___U3CProxyU3Ek__BackingField_11;
	// BestHTTP.Extensions.HeartbeatManager BestHTTP.HTTPManager::heartbeats
	HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4 * ___heartbeats_12;
	// BestHTTP.Logger.ILogger BestHTTP.HTTPManager::logger
	RuntimeObject* ___logger_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ICertificateVerifyer BestHTTP.HTTPManager::<DefaultCertificateVerifyer>k__BackingField
	RuntimeObject* ___U3CDefaultCertificateVerifyerU3Ek__BackingField_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.IClientCredentialsProvider BestHTTP.HTTPManager::<DefaultClientCredentialsProvider>k__BackingField
	RuntimeObject* ___U3CDefaultClientCredentialsProviderU3Ek__BackingField_15;
	// System.Boolean BestHTTP.HTTPManager::<UseAlternateSSLDefaultValue>k__BackingField
	bool ___U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16;
	// System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean> BestHTTP.HTTPManager::<DefaultCertificationValidator>k__BackingField
	Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E * ___U3CDefaultCertificationValidatorU3Ek__BackingField_17;
	// System.Int32 BestHTTP.HTTPManager::SendBufferSize
	int32_t ___SendBufferSize_18;
	// System.Int32 BestHTTP.HTTPManager::ReceiveBufferSize
	int32_t ___ReceiveBufferSize_19;
	// BestHTTP.PlatformSupport.FileSystem.IIOService BestHTTP.HTTPManager::IOService
	RuntimeObject* ___IOService_20;
	// System.Int32 BestHTTP.HTTPManager::<MaxPathLength>k__BackingField
	int32_t ___U3CMaxPathLengthU3Ek__BackingField_21;
	// System.String BestHTTP.HTTPManager::UserAgent
	String_t* ___UserAgent_22;
	// System.Boolean BestHTTP.HTTPManager::<IsQuitting>k__BackingField
	bool ___U3CIsQuittingU3Ek__BackingField_23;
	// System.Boolean BestHTTP.HTTPManager::IsSetupCalled
	bool ___IsSetupCalled_24;

public:
	inline static int32_t get_offset_of_HTTP2Settings_0() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___HTTP2Settings_0)); }
	inline HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63 * get_HTTP2Settings_0() const { return ___HTTP2Settings_0; }
	inline HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63 ** get_address_of_HTTP2Settings_0() { return &___HTTP2Settings_0; }
	inline void set_HTTP2Settings_0(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63 * value)
	{
		___HTTP2Settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___HTTP2Settings_0), value);
	}

	inline static int32_t get_offset_of_maxConnectionPerServer_1() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___maxConnectionPerServer_1)); }
	inline uint8_t get_maxConnectionPerServer_1() const { return ___maxConnectionPerServer_1; }
	inline uint8_t* get_address_of_maxConnectionPerServer_1() { return &___maxConnectionPerServer_1; }
	inline void set_maxConnectionPerServer_1(uint8_t value)
	{
		___maxConnectionPerServer_1 = value;
	}

	inline static int32_t get_offset_of_U3CKeepAliveDefaultValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CKeepAliveDefaultValueU3Ek__BackingField_2)); }
	inline bool get_U3CKeepAliveDefaultValueU3Ek__BackingField_2() const { return ___U3CKeepAliveDefaultValueU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CKeepAliveDefaultValueU3Ek__BackingField_2() { return &___U3CKeepAliveDefaultValueU3Ek__BackingField_2; }
	inline void set_U3CKeepAliveDefaultValueU3Ek__BackingField_2(bool value)
	{
		___U3CKeepAliveDefaultValueU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsCachingDisabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CIsCachingDisabledU3Ek__BackingField_3)); }
	inline bool get_U3CIsCachingDisabledU3Ek__BackingField_3() const { return ___U3CIsCachingDisabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsCachingDisabledU3Ek__BackingField_3() { return &___U3CIsCachingDisabledU3Ek__BackingField_3; }
	inline void set_U3CIsCachingDisabledU3Ek__BackingField_3(bool value)
	{
		___U3CIsCachingDisabledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CMaxConnectionIdleTimeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CMaxConnectionIdleTimeU3Ek__BackingField_4)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CMaxConnectionIdleTimeU3Ek__BackingField_4() const { return ___U3CMaxConnectionIdleTimeU3Ek__BackingField_4; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CMaxConnectionIdleTimeU3Ek__BackingField_4() { return &___U3CMaxConnectionIdleTimeU3Ek__BackingField_4; }
	inline void set_U3CMaxConnectionIdleTimeU3Ek__BackingField_4(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CMaxConnectionIdleTimeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CIsCookiesEnabledU3Ek__BackingField_5)); }
	inline bool get_U3CIsCookiesEnabledU3Ek__BackingField_5() const { return ___U3CIsCookiesEnabledU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsCookiesEnabledU3Ek__BackingField_5() { return &___U3CIsCookiesEnabledU3Ek__BackingField_5; }
	inline void set_U3CIsCookiesEnabledU3Ek__BackingField_5(bool value)
	{
		___U3CIsCookiesEnabledU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CCookieJarSizeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CCookieJarSizeU3Ek__BackingField_6)); }
	inline uint32_t get_U3CCookieJarSizeU3Ek__BackingField_6() const { return ___U3CCookieJarSizeU3Ek__BackingField_6; }
	inline uint32_t* get_address_of_U3CCookieJarSizeU3Ek__BackingField_6() { return &___U3CCookieJarSizeU3Ek__BackingField_6; }
	inline void set_U3CCookieJarSizeU3Ek__BackingField_6(uint32_t value)
	{
		___U3CCookieJarSizeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CEnablePrivateBrowsingU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CEnablePrivateBrowsingU3Ek__BackingField_7)); }
	inline bool get_U3CEnablePrivateBrowsingU3Ek__BackingField_7() const { return ___U3CEnablePrivateBrowsingU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CEnablePrivateBrowsingU3Ek__BackingField_7() { return &___U3CEnablePrivateBrowsingU3Ek__BackingField_7; }
	inline void set_U3CEnablePrivateBrowsingU3Ek__BackingField_7(bool value)
	{
		___U3CEnablePrivateBrowsingU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CConnectTimeoutU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CConnectTimeoutU3Ek__BackingField_8)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CConnectTimeoutU3Ek__BackingField_8() const { return ___U3CConnectTimeoutU3Ek__BackingField_8; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CConnectTimeoutU3Ek__BackingField_8() { return &___U3CConnectTimeoutU3Ek__BackingField_8; }
	inline void set_U3CConnectTimeoutU3Ek__BackingField_8(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CConnectTimeoutU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CRequestTimeoutU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CRequestTimeoutU3Ek__BackingField_9)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CRequestTimeoutU3Ek__BackingField_9() const { return ___U3CRequestTimeoutU3Ek__BackingField_9; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CRequestTimeoutU3Ek__BackingField_9() { return &___U3CRequestTimeoutU3Ek__BackingField_9; }
	inline void set_U3CRequestTimeoutU3Ek__BackingField_9(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CRequestTimeoutU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRootCacheFolderProviderU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CRootCacheFolderProviderU3Ek__BackingField_10)); }
	inline Func_1_tA732247549D3531F543287C316CF7F7F02F061F7 * get_U3CRootCacheFolderProviderU3Ek__BackingField_10() const { return ___U3CRootCacheFolderProviderU3Ek__BackingField_10; }
	inline Func_1_tA732247549D3531F543287C316CF7F7F02F061F7 ** get_address_of_U3CRootCacheFolderProviderU3Ek__BackingField_10() { return &___U3CRootCacheFolderProviderU3Ek__BackingField_10; }
	inline void set_U3CRootCacheFolderProviderU3Ek__BackingField_10(Func_1_tA732247549D3531F543287C316CF7F7F02F061F7 * value)
	{
		___U3CRootCacheFolderProviderU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootCacheFolderProviderU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CProxyU3Ek__BackingField_11)); }
	inline Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 * get_U3CProxyU3Ek__BackingField_11() const { return ___U3CProxyU3Ek__BackingField_11; }
	inline Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 ** get_address_of_U3CProxyU3Ek__BackingField_11() { return &___U3CProxyU3Ek__BackingField_11; }
	inline void set_U3CProxyU3Ek__BackingField_11(Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 * value)
	{
		___U3CProxyU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_heartbeats_12() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___heartbeats_12)); }
	inline HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4 * get_heartbeats_12() const { return ___heartbeats_12; }
	inline HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4 ** get_address_of_heartbeats_12() { return &___heartbeats_12; }
	inline void set_heartbeats_12(HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4 * value)
	{
		___heartbeats_12 = value;
		Il2CppCodeGenWriteBarrier((&___heartbeats_12), value);
	}

	inline static int32_t get_offset_of_logger_13() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___logger_13)); }
	inline RuntimeObject* get_logger_13() const { return ___logger_13; }
	inline RuntimeObject** get_address_of_logger_13() { return &___logger_13; }
	inline void set_logger_13(RuntimeObject* value)
	{
		___logger_13 = value;
		Il2CppCodeGenWriteBarrier((&___logger_13), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCertificateVerifyerU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CDefaultCertificateVerifyerU3Ek__BackingField_14)); }
	inline RuntimeObject* get_U3CDefaultCertificateVerifyerU3Ek__BackingField_14() const { return ___U3CDefaultCertificateVerifyerU3Ek__BackingField_14; }
	inline RuntimeObject** get_address_of_U3CDefaultCertificateVerifyerU3Ek__BackingField_14() { return &___U3CDefaultCertificateVerifyerU3Ek__BackingField_14; }
	inline void set_U3CDefaultCertificateVerifyerU3Ek__BackingField_14(RuntimeObject* value)
	{
		___U3CDefaultCertificateVerifyerU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCertificateVerifyerU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CDefaultClientCredentialsProviderU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CDefaultClientCredentialsProviderU3Ek__BackingField_15)); }
	inline RuntimeObject* get_U3CDefaultClientCredentialsProviderU3Ek__BackingField_15() const { return ___U3CDefaultClientCredentialsProviderU3Ek__BackingField_15; }
	inline RuntimeObject** get_address_of_U3CDefaultClientCredentialsProviderU3Ek__BackingField_15() { return &___U3CDefaultClientCredentialsProviderU3Ek__BackingField_15; }
	inline void set_U3CDefaultClientCredentialsProviderU3Ek__BackingField_15(RuntimeObject* value)
	{
		___U3CDefaultClientCredentialsProviderU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultClientCredentialsProviderU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16)); }
	inline bool get_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16() const { return ___U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16() { return &___U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16; }
	inline void set_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16(bool value)
	{
		___U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultCertificationValidatorU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CDefaultCertificationValidatorU3Ek__BackingField_17)); }
	inline Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E * get_U3CDefaultCertificationValidatorU3Ek__BackingField_17() const { return ___U3CDefaultCertificationValidatorU3Ek__BackingField_17; }
	inline Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E ** get_address_of_U3CDefaultCertificationValidatorU3Ek__BackingField_17() { return &___U3CDefaultCertificationValidatorU3Ek__BackingField_17; }
	inline void set_U3CDefaultCertificationValidatorU3Ek__BackingField_17(Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E * value)
	{
		___U3CDefaultCertificationValidatorU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCertificationValidatorU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_SendBufferSize_18() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___SendBufferSize_18)); }
	inline int32_t get_SendBufferSize_18() const { return ___SendBufferSize_18; }
	inline int32_t* get_address_of_SendBufferSize_18() { return &___SendBufferSize_18; }
	inline void set_SendBufferSize_18(int32_t value)
	{
		___SendBufferSize_18 = value;
	}

	inline static int32_t get_offset_of_ReceiveBufferSize_19() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___ReceiveBufferSize_19)); }
	inline int32_t get_ReceiveBufferSize_19() const { return ___ReceiveBufferSize_19; }
	inline int32_t* get_address_of_ReceiveBufferSize_19() { return &___ReceiveBufferSize_19; }
	inline void set_ReceiveBufferSize_19(int32_t value)
	{
		___ReceiveBufferSize_19 = value;
	}

	inline static int32_t get_offset_of_IOService_20() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___IOService_20)); }
	inline RuntimeObject* get_IOService_20() const { return ___IOService_20; }
	inline RuntimeObject** get_address_of_IOService_20() { return &___IOService_20; }
	inline void set_IOService_20(RuntimeObject* value)
	{
		___IOService_20 = value;
		Il2CppCodeGenWriteBarrier((&___IOService_20), value);
	}

	inline static int32_t get_offset_of_U3CMaxPathLengthU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CMaxPathLengthU3Ek__BackingField_21)); }
	inline int32_t get_U3CMaxPathLengthU3Ek__BackingField_21() const { return ___U3CMaxPathLengthU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CMaxPathLengthU3Ek__BackingField_21() { return &___U3CMaxPathLengthU3Ek__BackingField_21; }
	inline void set_U3CMaxPathLengthU3Ek__BackingField_21(int32_t value)
	{
		___U3CMaxPathLengthU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_UserAgent_22() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___UserAgent_22)); }
	inline String_t* get_UserAgent_22() const { return ___UserAgent_22; }
	inline String_t** get_address_of_UserAgent_22() { return &___UserAgent_22; }
	inline void set_UserAgent_22(String_t* value)
	{
		___UserAgent_22 = value;
		Il2CppCodeGenWriteBarrier((&___UserAgent_22), value);
	}

	inline static int32_t get_offset_of_U3CIsQuittingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___U3CIsQuittingU3Ek__BackingField_23)); }
	inline bool get_U3CIsQuittingU3Ek__BackingField_23() const { return ___U3CIsQuittingU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CIsQuittingU3Ek__BackingField_23() { return &___U3CIsQuittingU3Ek__BackingField_23; }
	inline void set_U3CIsQuittingU3Ek__BackingField_23(bool value)
	{
		___U3CIsQuittingU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_IsSetupCalled_24() { return static_cast<int32_t>(offsetof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields, ___IsSetupCalled_24)); }
	inline bool get_IsSetupCalled_24() const { return ___IsSetupCalled_24; }
	inline bool* get_address_of_IsSetupCalled_24() { return &___IsSetupCalled_24; }
	inline void set_IsSetupCalled_24(bool value)
	{
		___IsSetupCalled_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMANAGER_T0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_H
#ifndef HTTPREQUEST_T0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_H
#define HTTPREQUEST_T0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequest
struct  HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.HTTPRequest::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_3;
	// BestHTTP.HTTPMethods BestHTTP.HTTPRequest::<MethodType>k__BackingField
	uint8_t ___U3CMethodTypeU3Ek__BackingField_4;
	// System.Byte[] BestHTTP.HTTPRequest::<RawData>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CRawDataU3Ek__BackingField_5;
	// System.IO.Stream BestHTTP.HTTPRequest::<UploadStream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CUploadStreamU3Ek__BackingField_6;
	// System.Boolean BestHTTP.HTTPRequest::<DisposeUploadStream>k__BackingField
	bool ___U3CDisposeUploadStreamU3Ek__BackingField_7;
	// System.Boolean BestHTTP.HTTPRequest::<UseUploadStreamLength>k__BackingField
	bool ___U3CUseUploadStreamLengthU3Ek__BackingField_8;
	// BestHTTP.OnUploadProgressDelegate BestHTTP.HTTPRequest::OnUploadProgress
	OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A * ___OnUploadProgress_9;
	// System.Boolean BestHTTP.HTTPRequest::<StreamChunksImmediately>k__BackingField
	bool ___U3CStreamChunksImmediatelyU3Ek__BackingField_10;
	// System.Int32 BestHTTP.HTTPRequest::<MaxFragmentQueueLength>k__BackingField
	int32_t ___U3CMaxFragmentQueueLengthU3Ek__BackingField_11;
	// BestHTTP.OnRequestFinishedDelegate BestHTTP.HTTPRequest::<Callback>k__BackingField
	OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 * ___U3CCallbackU3Ek__BackingField_12;
	// System.DateTime BestHTTP.HTTPRequest::<ProcessingStarted>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CProcessingStartedU3Ek__BackingField_13;
	// BestHTTP.OnStreamingDataDelegate BestHTTP.HTTPRequest::OnStreamingData
	OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52 * ___OnStreamingData_14;
	// System.Action`2<BestHTTP.HTTPRequest,BestHTTP.HTTPResponse> BestHTTP.HTTPRequest::OnHeadersReceived
	Action_2_tCBA8C57FA428D737E3D2CC5441F0E3E250BCC3BA * ___OnHeadersReceived_15;
	// System.Int32 BestHTTP.HTTPRequest::<Retries>k__BackingField
	int32_t ___U3CRetriesU3Ek__BackingField_16;
	// System.Int32 BestHTTP.HTTPRequest::<MaxRetries>k__BackingField
	int32_t ___U3CMaxRetriesU3Ek__BackingField_17;
	// System.Boolean BestHTTP.HTTPRequest::<IsCancellationRequested>k__BackingField
	bool ___U3CIsCancellationRequestedU3Ek__BackingField_18;
	// BestHTTP.OnDownloadProgressDelegate BestHTTP.HTTPRequest::OnDownloadProgress
	OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB * ___OnDownloadProgress_19;
	// BestHTTP.OnRequestFinishedDelegate BestHTTP.HTTPRequest::OnUpgraded
	OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 * ___OnUpgraded_20;
	// System.Boolean BestHTTP.HTTPRequest::<IsRedirected>k__BackingField
	bool ___U3CIsRedirectedU3Ek__BackingField_21;
	// System.Uri BestHTTP.HTTPRequest::<RedirectUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CRedirectUriU3Ek__BackingField_22;
	// BestHTTP.HTTPResponse BestHTTP.HTTPRequest::<Response>k__BackingField
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * ___U3CResponseU3Ek__BackingField_23;
	// BestHTTP.HTTPResponse BestHTTP.HTTPRequest::<ProxyResponse>k__BackingField
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * ___U3CProxyResponseU3Ek__BackingField_24;
	// System.Exception BestHTTP.HTTPRequest::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_25;
	// System.Object BestHTTP.HTTPRequest::<Tag>k__BackingField
	RuntimeObject * ___U3CTagU3Ek__BackingField_26;
	// BestHTTP.Authentication.Credentials BestHTTP.HTTPRequest::<Credentials>k__BackingField
	Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 * ___U3CCredentialsU3Ek__BackingField_27;
	// BestHTTP.Proxy BestHTTP.HTTPRequest::<Proxy>k__BackingField
	Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 * ___U3CProxyU3Ek__BackingField_28;
	// System.Int32 BestHTTP.HTTPRequest::<MaxRedirects>k__BackingField
	int32_t ___U3CMaxRedirectsU3Ek__BackingField_29;
	// System.Boolean BestHTTP.HTTPRequest::<UseAlternateSSL>k__BackingField
	bool ___U3CUseAlternateSSLU3Ek__BackingField_30;
	// System.Boolean BestHTTP.HTTPRequest::<IsCookiesEnabled>k__BackingField
	bool ___U3CIsCookiesEnabledU3Ek__BackingField_31;
	// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie> BestHTTP.HTTPRequest::customCookies
	List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * ___customCookies_32;
	// BestHTTP.Forms.HTTPFormUsage BestHTTP.HTTPRequest::<FormUsage>k__BackingField
	int32_t ___U3CFormUsageU3Ek__BackingField_33;
	// BestHTTP.HTTPRequestStates modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.HTTPRequest::_state
	int32_t ____state_34;
	// System.Int32 BestHTTP.HTTPRequest::<RedirectCount>k__BackingField
	int32_t ___U3CRedirectCountU3Ek__BackingField_35;
	// System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean> BestHTTP.HTTPRequest::CustomCertificationValidator
	Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E * ___CustomCertificationValidator_36;
	// System.TimeSpan BestHTTP.HTTPRequest::<ConnectTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CConnectTimeoutU3Ek__BackingField_37;
	// System.TimeSpan BestHTTP.HTTPRequest::<Timeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CTimeoutU3Ek__BackingField_38;
	// System.Boolean BestHTTP.HTTPRequest::<EnableTimoutForStreaming>k__BackingField
	bool ___U3CEnableTimoutForStreamingU3Ek__BackingField_39;
	// System.Boolean BestHTTP.HTTPRequest::<EnableSafeReadOnUnknownContentLength>k__BackingField
	bool ___U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ICertificateVerifyer BestHTTP.HTTPRequest::<CustomCertificateVerifyer>k__BackingField
	RuntimeObject* ___U3CCustomCertificateVerifyerU3Ek__BackingField_41;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.IClientCredentialsProvider BestHTTP.HTTPRequest::<CustomClientCredentialsProvider>k__BackingField
	RuntimeObject* ___U3CCustomClientCredentialsProviderU3Ek__BackingField_42;
	// System.Collections.Generic.List`1<System.String> BestHTTP.HTTPRequest::<CustomTLSServerNameList>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CCustomTLSServerNameListU3Ek__BackingField_43;
	// BestHTTP.Connections.SupportedProtocols BestHTTP.HTTPRequest::<ProtocolHandler>k__BackingField
	int32_t ___U3CProtocolHandlerU3Ek__BackingField_44;
	// BestHTTP.OnBeforeRedirectionDelegate BestHTTP.HTTPRequest::onBeforeRedirection
	OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D * ___onBeforeRedirection_45;
	// BestHTTP.OnBeforeHeaderSendDelegate BestHTTP.HTTPRequest::_onBeforeHeaderSend
	OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3 * ____onBeforeHeaderSend_46;
	// System.Boolean BestHTTP.HTTPRequest::isKeepAlive
	bool ___isKeepAlive_47;
	// System.Boolean BestHTTP.HTTPRequest::disableCache
	bool ___disableCache_48;
	// System.Boolean BestHTTP.HTTPRequest::cacheOnly
	bool ___cacheOnly_49;
	// System.Int32 BestHTTP.HTTPRequest::streamFragmentSize
	int32_t ___streamFragmentSize_50;
	// System.Boolean BestHTTP.HTTPRequest::useStreaming
	bool ___useStreaming_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> BestHTTP.HTTPRequest::<Headers>k__BackingField
	Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * ___U3CHeadersU3Ek__BackingField_52;
	// BestHTTP.Forms.HTTPFormBase BestHTTP.HTTPRequest::FieldCollector
	HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 * ___FieldCollector_53;
	// BestHTTP.Forms.HTTPFormBase BestHTTP.HTTPRequest::FormImpl
	HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 * ___FormImpl_54;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CUriU3Ek__BackingField_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_3() const { return ___U3CUriU3Ek__BackingField_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_3() { return &___U3CUriU3Ek__BackingField_3; }
	inline void set_U3CUriU3Ek__BackingField_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMethodTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CMethodTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CMethodTypeU3Ek__BackingField_4() const { return ___U3CMethodTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CMethodTypeU3Ek__BackingField_4() { return &___U3CMethodTypeU3Ek__BackingField_4; }
	inline void set_U3CMethodTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CMethodTypeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRawDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CRawDataU3Ek__BackingField_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CRawDataU3Ek__BackingField_5() const { return ___U3CRawDataU3Ek__BackingField_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CRawDataU3Ek__BackingField_5() { return &___U3CRawDataU3Ek__BackingField_5; }
	inline void set_U3CRawDataU3Ek__BackingField_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CRawDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawDataU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CUploadStreamU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CUploadStreamU3Ek__BackingField_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CUploadStreamU3Ek__BackingField_6() const { return ___U3CUploadStreamU3Ek__BackingField_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CUploadStreamU3Ek__BackingField_6() { return &___U3CUploadStreamU3Ek__BackingField_6; }
	inline void set_U3CUploadStreamU3Ek__BackingField_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CUploadStreamU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUploadStreamU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CDisposeUploadStreamU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CDisposeUploadStreamU3Ek__BackingField_7)); }
	inline bool get_U3CDisposeUploadStreamU3Ek__BackingField_7() const { return ___U3CDisposeUploadStreamU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CDisposeUploadStreamU3Ek__BackingField_7() { return &___U3CDisposeUploadStreamU3Ek__BackingField_7; }
	inline void set_U3CDisposeUploadStreamU3Ek__BackingField_7(bool value)
	{
		___U3CDisposeUploadStreamU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUseUploadStreamLengthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CUseUploadStreamLengthU3Ek__BackingField_8)); }
	inline bool get_U3CUseUploadStreamLengthU3Ek__BackingField_8() const { return ___U3CUseUploadStreamLengthU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CUseUploadStreamLengthU3Ek__BackingField_8() { return &___U3CUseUploadStreamLengthU3Ek__BackingField_8; }
	inline void set_U3CUseUploadStreamLengthU3Ek__BackingField_8(bool value)
	{
		___U3CUseUploadStreamLengthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_OnUploadProgress_9() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___OnUploadProgress_9)); }
	inline OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A * get_OnUploadProgress_9() const { return ___OnUploadProgress_9; }
	inline OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A ** get_address_of_OnUploadProgress_9() { return &___OnUploadProgress_9; }
	inline void set_OnUploadProgress_9(OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A * value)
	{
		___OnUploadProgress_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnUploadProgress_9), value);
	}

	inline static int32_t get_offset_of_U3CStreamChunksImmediatelyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CStreamChunksImmediatelyU3Ek__BackingField_10)); }
	inline bool get_U3CStreamChunksImmediatelyU3Ek__BackingField_10() const { return ___U3CStreamChunksImmediatelyU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CStreamChunksImmediatelyU3Ek__BackingField_10() { return &___U3CStreamChunksImmediatelyU3Ek__BackingField_10; }
	inline void set_U3CStreamChunksImmediatelyU3Ek__BackingField_10(bool value)
	{
		___U3CStreamChunksImmediatelyU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CMaxFragmentQueueLengthU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CMaxFragmentQueueLengthU3Ek__BackingField_11)); }
	inline int32_t get_U3CMaxFragmentQueueLengthU3Ek__BackingField_11() const { return ___U3CMaxFragmentQueueLengthU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CMaxFragmentQueueLengthU3Ek__BackingField_11() { return &___U3CMaxFragmentQueueLengthU3Ek__BackingField_11; }
	inline void set_U3CMaxFragmentQueueLengthU3Ek__BackingField_11(int32_t value)
	{
		___U3CMaxFragmentQueueLengthU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CProcessingStartedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CProcessingStartedU3Ek__BackingField_13)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CProcessingStartedU3Ek__BackingField_13() const { return ___U3CProcessingStartedU3Ek__BackingField_13; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CProcessingStartedU3Ek__BackingField_13() { return &___U3CProcessingStartedU3Ek__BackingField_13; }
	inline void set_U3CProcessingStartedU3Ek__BackingField_13(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CProcessingStartedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_OnStreamingData_14() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___OnStreamingData_14)); }
	inline OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52 * get_OnStreamingData_14() const { return ___OnStreamingData_14; }
	inline OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52 ** get_address_of_OnStreamingData_14() { return &___OnStreamingData_14; }
	inline void set_OnStreamingData_14(OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52 * value)
	{
		___OnStreamingData_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnStreamingData_14), value);
	}

	inline static int32_t get_offset_of_OnHeadersReceived_15() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___OnHeadersReceived_15)); }
	inline Action_2_tCBA8C57FA428D737E3D2CC5441F0E3E250BCC3BA * get_OnHeadersReceived_15() const { return ___OnHeadersReceived_15; }
	inline Action_2_tCBA8C57FA428D737E3D2CC5441F0E3E250BCC3BA ** get_address_of_OnHeadersReceived_15() { return &___OnHeadersReceived_15; }
	inline void set_OnHeadersReceived_15(Action_2_tCBA8C57FA428D737E3D2CC5441F0E3E250BCC3BA * value)
	{
		___OnHeadersReceived_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeadersReceived_15), value);
	}

	inline static int32_t get_offset_of_U3CRetriesU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CRetriesU3Ek__BackingField_16)); }
	inline int32_t get_U3CRetriesU3Ek__BackingField_16() const { return ___U3CRetriesU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CRetriesU3Ek__BackingField_16() { return &___U3CRetriesU3Ek__BackingField_16; }
	inline void set_U3CRetriesU3Ek__BackingField_16(int32_t value)
	{
		___U3CRetriesU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CMaxRetriesU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CMaxRetriesU3Ek__BackingField_17)); }
	inline int32_t get_U3CMaxRetriesU3Ek__BackingField_17() const { return ___U3CMaxRetriesU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CMaxRetriesU3Ek__BackingField_17() { return &___U3CMaxRetriesU3Ek__BackingField_17; }
	inline void set_U3CMaxRetriesU3Ek__BackingField_17(int32_t value)
	{
		___U3CMaxRetriesU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CIsCancellationRequestedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CIsCancellationRequestedU3Ek__BackingField_18)); }
	inline bool get_U3CIsCancellationRequestedU3Ek__BackingField_18() const { return ___U3CIsCancellationRequestedU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsCancellationRequestedU3Ek__BackingField_18() { return &___U3CIsCancellationRequestedU3Ek__BackingField_18; }
	inline void set_U3CIsCancellationRequestedU3Ek__BackingField_18(bool value)
	{
		___U3CIsCancellationRequestedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_OnDownloadProgress_19() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___OnDownloadProgress_19)); }
	inline OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB * get_OnDownloadProgress_19() const { return ___OnDownloadProgress_19; }
	inline OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB ** get_address_of_OnDownloadProgress_19() { return &___OnDownloadProgress_19; }
	inline void set_OnDownloadProgress_19(OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB * value)
	{
		___OnDownloadProgress_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnDownloadProgress_19), value);
	}

	inline static int32_t get_offset_of_OnUpgraded_20() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___OnUpgraded_20)); }
	inline OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 * get_OnUpgraded_20() const { return ___OnUpgraded_20; }
	inline OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 ** get_address_of_OnUpgraded_20() { return &___OnUpgraded_20; }
	inline void set_OnUpgraded_20(OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251 * value)
	{
		___OnUpgraded_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpgraded_20), value);
	}

	inline static int32_t get_offset_of_U3CIsRedirectedU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CIsRedirectedU3Ek__BackingField_21)); }
	inline bool get_U3CIsRedirectedU3Ek__BackingField_21() const { return ___U3CIsRedirectedU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CIsRedirectedU3Ek__BackingField_21() { return &___U3CIsRedirectedU3Ek__BackingField_21; }
	inline void set_U3CIsRedirectedU3Ek__BackingField_21(bool value)
	{
		___U3CIsRedirectedU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CRedirectUriU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CRedirectUriU3Ek__BackingField_22)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CRedirectUriU3Ek__BackingField_22() const { return ___U3CRedirectUriU3Ek__BackingField_22; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CRedirectUriU3Ek__BackingField_22() { return &___U3CRedirectUriU3Ek__BackingField_22; }
	inline void set_U3CRedirectUriU3Ek__BackingField_22(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CRedirectUriU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRedirectUriU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CResponseU3Ek__BackingField_23)); }
	inline HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * get_U3CResponseU3Ek__BackingField_23() const { return ___U3CResponseU3Ek__BackingField_23; }
	inline HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB ** get_address_of_U3CResponseU3Ek__BackingField_23() { return &___U3CResponseU3Ek__BackingField_23; }
	inline void set_U3CResponseU3Ek__BackingField_23(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * value)
	{
		___U3CResponseU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CProxyResponseU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CProxyResponseU3Ek__BackingField_24)); }
	inline HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * get_U3CProxyResponseU3Ek__BackingField_24() const { return ___U3CProxyResponseU3Ek__BackingField_24; }
	inline HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB ** get_address_of_U3CProxyResponseU3Ek__BackingField_24() { return &___U3CProxyResponseU3Ek__BackingField_24; }
	inline void set_U3CProxyResponseU3Ek__BackingField_24(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * value)
	{
		___U3CProxyResponseU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyResponseU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CExceptionU3Ek__BackingField_25)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_25() const { return ___U3CExceptionU3Ek__BackingField_25; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_25() { return &___U3CExceptionU3Ek__BackingField_25; }
	inline void set_U3CExceptionU3Ek__BackingField_25(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CTagU3Ek__BackingField_26)); }
	inline RuntimeObject * get_U3CTagU3Ek__BackingField_26() const { return ___U3CTagU3Ek__BackingField_26; }
	inline RuntimeObject ** get_address_of_U3CTagU3Ek__BackingField_26() { return &___U3CTagU3Ek__BackingField_26; }
	inline void set_U3CTagU3Ek__BackingField_26(RuntimeObject * value)
	{
		___U3CTagU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CCredentialsU3Ek__BackingField_27)); }
	inline Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 * get_U3CCredentialsU3Ek__BackingField_27() const { return ___U3CCredentialsU3Ek__BackingField_27; }
	inline Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 ** get_address_of_U3CCredentialsU3Ek__BackingField_27() { return &___U3CCredentialsU3Ek__BackingField_27; }
	inline void set_U3CCredentialsU3Ek__BackingField_27(Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78 * value)
	{
		___U3CCredentialsU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CProxyU3Ek__BackingField_28)); }
	inline Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 * get_U3CProxyU3Ek__BackingField_28() const { return ___U3CProxyU3Ek__BackingField_28; }
	inline Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 ** get_address_of_U3CProxyU3Ek__BackingField_28() { return &___U3CProxyU3Ek__BackingField_28; }
	inline void set_U3CProxyU3Ek__BackingField_28(Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9 * value)
	{
		___U3CProxyU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CMaxRedirectsU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CMaxRedirectsU3Ek__BackingField_29)); }
	inline int32_t get_U3CMaxRedirectsU3Ek__BackingField_29() const { return ___U3CMaxRedirectsU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CMaxRedirectsU3Ek__BackingField_29() { return &___U3CMaxRedirectsU3Ek__BackingField_29; }
	inline void set_U3CMaxRedirectsU3Ek__BackingField_29(int32_t value)
	{
		___U3CMaxRedirectsU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CUseAlternateSSLU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CUseAlternateSSLU3Ek__BackingField_30)); }
	inline bool get_U3CUseAlternateSSLU3Ek__BackingField_30() const { return ___U3CUseAlternateSSLU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CUseAlternateSSLU3Ek__BackingField_30() { return &___U3CUseAlternateSSLU3Ek__BackingField_30; }
	inline void set_U3CUseAlternateSSLU3Ek__BackingField_30(bool value)
	{
		___U3CUseAlternateSSLU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CIsCookiesEnabledU3Ek__BackingField_31)); }
	inline bool get_U3CIsCookiesEnabledU3Ek__BackingField_31() const { return ___U3CIsCookiesEnabledU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CIsCookiesEnabledU3Ek__BackingField_31() { return &___U3CIsCookiesEnabledU3Ek__BackingField_31; }
	inline void set_U3CIsCookiesEnabledU3Ek__BackingField_31(bool value)
	{
		___U3CIsCookiesEnabledU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_customCookies_32() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___customCookies_32)); }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * get_customCookies_32() const { return ___customCookies_32; }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 ** get_address_of_customCookies_32() { return &___customCookies_32; }
	inline void set_customCookies_32(List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * value)
	{
		___customCookies_32 = value;
		Il2CppCodeGenWriteBarrier((&___customCookies_32), value);
	}

	inline static int32_t get_offset_of_U3CFormUsageU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CFormUsageU3Ek__BackingField_33)); }
	inline int32_t get_U3CFormUsageU3Ek__BackingField_33() const { return ___U3CFormUsageU3Ek__BackingField_33; }
	inline int32_t* get_address_of_U3CFormUsageU3Ek__BackingField_33() { return &___U3CFormUsageU3Ek__BackingField_33; }
	inline void set_U3CFormUsageU3Ek__BackingField_33(int32_t value)
	{
		___U3CFormUsageU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of__state_34() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ____state_34)); }
	inline int32_t get__state_34() const { return ____state_34; }
	inline int32_t* get_address_of__state_34() { return &____state_34; }
	inline void set__state_34(int32_t value)
	{
		____state_34 = value;
	}

	inline static int32_t get_offset_of_U3CRedirectCountU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CRedirectCountU3Ek__BackingField_35)); }
	inline int32_t get_U3CRedirectCountU3Ek__BackingField_35() const { return ___U3CRedirectCountU3Ek__BackingField_35; }
	inline int32_t* get_address_of_U3CRedirectCountU3Ek__BackingField_35() { return &___U3CRedirectCountU3Ek__BackingField_35; }
	inline void set_U3CRedirectCountU3Ek__BackingField_35(int32_t value)
	{
		___U3CRedirectCountU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_CustomCertificationValidator_36() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___CustomCertificationValidator_36)); }
	inline Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E * get_CustomCertificationValidator_36() const { return ___CustomCertificationValidator_36; }
	inline Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E ** get_address_of_CustomCertificationValidator_36() { return &___CustomCertificationValidator_36; }
	inline void set_CustomCertificationValidator_36(Func_4_t13C0D1234312C3CD80AC24E8ED1029242227909E * value)
	{
		___CustomCertificationValidator_36 = value;
		Il2CppCodeGenWriteBarrier((&___CustomCertificationValidator_36), value);
	}

	inline static int32_t get_offset_of_U3CConnectTimeoutU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CConnectTimeoutU3Ek__BackingField_37)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CConnectTimeoutU3Ek__BackingField_37() const { return ___U3CConnectTimeoutU3Ek__BackingField_37; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CConnectTimeoutU3Ek__BackingField_37() { return &___U3CConnectTimeoutU3Ek__BackingField_37; }
	inline void set_U3CConnectTimeoutU3Ek__BackingField_37(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CConnectTimeoutU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CTimeoutU3Ek__BackingField_38)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CTimeoutU3Ek__BackingField_38() const { return ___U3CTimeoutU3Ek__BackingField_38; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CTimeoutU3Ek__BackingField_38() { return &___U3CTimeoutU3Ek__BackingField_38; }
	inline void set_U3CTimeoutU3Ek__BackingField_38(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CTimeoutU3Ek__BackingField_38 = value;
	}

	inline static int32_t get_offset_of_U3CEnableTimoutForStreamingU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CEnableTimoutForStreamingU3Ek__BackingField_39)); }
	inline bool get_U3CEnableTimoutForStreamingU3Ek__BackingField_39() const { return ___U3CEnableTimoutForStreamingU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CEnableTimoutForStreamingU3Ek__BackingField_39() { return &___U3CEnableTimoutForStreamingU3Ek__BackingField_39; }
	inline void set_U3CEnableTimoutForStreamingU3Ek__BackingField_39(bool value)
	{
		___U3CEnableTimoutForStreamingU3Ek__BackingField_39 = value;
	}

	inline static int32_t get_offset_of_U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40)); }
	inline bool get_U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40() const { return ___U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40; }
	inline bool* get_address_of_U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40() { return &___U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40; }
	inline void set_U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40(bool value)
	{
		___U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_U3CCustomCertificateVerifyerU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CCustomCertificateVerifyerU3Ek__BackingField_41)); }
	inline RuntimeObject* get_U3CCustomCertificateVerifyerU3Ek__BackingField_41() const { return ___U3CCustomCertificateVerifyerU3Ek__BackingField_41; }
	inline RuntimeObject** get_address_of_U3CCustomCertificateVerifyerU3Ek__BackingField_41() { return &___U3CCustomCertificateVerifyerU3Ek__BackingField_41; }
	inline void set_U3CCustomCertificateVerifyerU3Ek__BackingField_41(RuntimeObject* value)
	{
		___U3CCustomCertificateVerifyerU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomCertificateVerifyerU3Ek__BackingField_41), value);
	}

	inline static int32_t get_offset_of_U3CCustomClientCredentialsProviderU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CCustomClientCredentialsProviderU3Ek__BackingField_42)); }
	inline RuntimeObject* get_U3CCustomClientCredentialsProviderU3Ek__BackingField_42() const { return ___U3CCustomClientCredentialsProviderU3Ek__BackingField_42; }
	inline RuntimeObject** get_address_of_U3CCustomClientCredentialsProviderU3Ek__BackingField_42() { return &___U3CCustomClientCredentialsProviderU3Ek__BackingField_42; }
	inline void set_U3CCustomClientCredentialsProviderU3Ek__BackingField_42(RuntimeObject* value)
	{
		___U3CCustomClientCredentialsProviderU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomClientCredentialsProviderU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_U3CCustomTLSServerNameListU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CCustomTLSServerNameListU3Ek__BackingField_43)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CCustomTLSServerNameListU3Ek__BackingField_43() const { return ___U3CCustomTLSServerNameListU3Ek__BackingField_43; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CCustomTLSServerNameListU3Ek__BackingField_43() { return &___U3CCustomTLSServerNameListU3Ek__BackingField_43; }
	inline void set_U3CCustomTLSServerNameListU3Ek__BackingField_43(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CCustomTLSServerNameListU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomTLSServerNameListU3Ek__BackingField_43), value);
	}

	inline static int32_t get_offset_of_U3CProtocolHandlerU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CProtocolHandlerU3Ek__BackingField_44)); }
	inline int32_t get_U3CProtocolHandlerU3Ek__BackingField_44() const { return ___U3CProtocolHandlerU3Ek__BackingField_44; }
	inline int32_t* get_address_of_U3CProtocolHandlerU3Ek__BackingField_44() { return &___U3CProtocolHandlerU3Ek__BackingField_44; }
	inline void set_U3CProtocolHandlerU3Ek__BackingField_44(int32_t value)
	{
		___U3CProtocolHandlerU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_onBeforeRedirection_45() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___onBeforeRedirection_45)); }
	inline OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D * get_onBeforeRedirection_45() const { return ___onBeforeRedirection_45; }
	inline OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D ** get_address_of_onBeforeRedirection_45() { return &___onBeforeRedirection_45; }
	inline void set_onBeforeRedirection_45(OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D * value)
	{
		___onBeforeRedirection_45 = value;
		Il2CppCodeGenWriteBarrier((&___onBeforeRedirection_45), value);
	}

	inline static int32_t get_offset_of__onBeforeHeaderSend_46() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ____onBeforeHeaderSend_46)); }
	inline OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3 * get__onBeforeHeaderSend_46() const { return ____onBeforeHeaderSend_46; }
	inline OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3 ** get_address_of__onBeforeHeaderSend_46() { return &____onBeforeHeaderSend_46; }
	inline void set__onBeforeHeaderSend_46(OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3 * value)
	{
		____onBeforeHeaderSend_46 = value;
		Il2CppCodeGenWriteBarrier((&____onBeforeHeaderSend_46), value);
	}

	inline static int32_t get_offset_of_isKeepAlive_47() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___isKeepAlive_47)); }
	inline bool get_isKeepAlive_47() const { return ___isKeepAlive_47; }
	inline bool* get_address_of_isKeepAlive_47() { return &___isKeepAlive_47; }
	inline void set_isKeepAlive_47(bool value)
	{
		___isKeepAlive_47 = value;
	}

	inline static int32_t get_offset_of_disableCache_48() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___disableCache_48)); }
	inline bool get_disableCache_48() const { return ___disableCache_48; }
	inline bool* get_address_of_disableCache_48() { return &___disableCache_48; }
	inline void set_disableCache_48(bool value)
	{
		___disableCache_48 = value;
	}

	inline static int32_t get_offset_of_cacheOnly_49() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___cacheOnly_49)); }
	inline bool get_cacheOnly_49() const { return ___cacheOnly_49; }
	inline bool* get_address_of_cacheOnly_49() { return &___cacheOnly_49; }
	inline void set_cacheOnly_49(bool value)
	{
		___cacheOnly_49 = value;
	}

	inline static int32_t get_offset_of_streamFragmentSize_50() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___streamFragmentSize_50)); }
	inline int32_t get_streamFragmentSize_50() const { return ___streamFragmentSize_50; }
	inline int32_t* get_address_of_streamFragmentSize_50() { return &___streamFragmentSize_50; }
	inline void set_streamFragmentSize_50(int32_t value)
	{
		___streamFragmentSize_50 = value;
	}

	inline static int32_t get_offset_of_useStreaming_51() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___useStreaming_51)); }
	inline bool get_useStreaming_51() const { return ___useStreaming_51; }
	inline bool* get_address_of_useStreaming_51() { return &___useStreaming_51; }
	inline void set_useStreaming_51(bool value)
	{
		___useStreaming_51 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___U3CHeadersU3Ek__BackingField_52)); }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * get_U3CHeadersU3Ek__BackingField_52() const { return ___U3CHeadersU3Ek__BackingField_52; }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 ** get_address_of_U3CHeadersU3Ek__BackingField_52() { return &___U3CHeadersU3Ek__BackingField_52; }
	inline void set_U3CHeadersU3Ek__BackingField_52(Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * value)
	{
		___U3CHeadersU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_52), value);
	}

	inline static int32_t get_offset_of_FieldCollector_53() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___FieldCollector_53)); }
	inline HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 * get_FieldCollector_53() const { return ___FieldCollector_53; }
	inline HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 ** get_address_of_FieldCollector_53() { return &___FieldCollector_53; }
	inline void set_FieldCollector_53(HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 * value)
	{
		___FieldCollector_53 = value;
		Il2CppCodeGenWriteBarrier((&___FieldCollector_53), value);
	}

	inline static int32_t get_offset_of_FormImpl_54() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837, ___FormImpl_54)); }
	inline HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 * get_FormImpl_54() const { return ___FormImpl_54; }
	inline HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 ** get_address_of_FormImpl_54() { return &___FormImpl_54; }
	inline void set_FormImpl_54(HTTPFormBase_t175DC4359F6B8A66520DCA85B0F0C0A0F6064B21 * value)
	{
		___FormImpl_54 = value;
		Il2CppCodeGenWriteBarrier((&___FormImpl_54), value);
	}
};

struct HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields
{
public:
	// System.Byte[] BestHTTP.HTTPRequest::EOL
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EOL_0;
	// System.String[] BestHTTP.HTTPRequest::MethodNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___MethodNames_1;
	// System.Int32 BestHTTP.HTTPRequest::UploadChunkSize
	int32_t ___UploadChunkSize_2;

public:
	inline static int32_t get_offset_of_EOL_0() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields, ___EOL_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EOL_0() const { return ___EOL_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EOL_0() { return &___EOL_0; }
	inline void set_EOL_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EOL_0 = value;
		Il2CppCodeGenWriteBarrier((&___EOL_0), value);
	}

	inline static int32_t get_offset_of_MethodNames_1() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields, ___MethodNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_MethodNames_1() const { return ___MethodNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_MethodNames_1() { return &___MethodNames_1; }
	inline void set_MethodNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___MethodNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___MethodNames_1), value);
	}

	inline static int32_t get_offset_of_UploadChunkSize_2() { return static_cast<int32_t>(offsetof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields, ___UploadChunkSize_2)); }
	inline int32_t get_UploadChunkSize_2() const { return ___UploadChunkSize_2; }
	inline int32_t* get_address_of_UploadChunkSize_2() { return &___UploadChunkSize_2; }
	inline void set_UploadChunkSize_2(int32_t value)
	{
		___UploadChunkSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUEST_T0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_H
#ifndef JSONDATA_T117B9981357FBA575D8A6A902450D4331DFD4DA4_H
#define JSONDATA_T117B9981357FBA575D8A6A902450D4331DFD4DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonData
struct  JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<LitJson.JsonData> LitJson.JsonData::inst_array
	RuntimeObject* ___inst_array_0;
	// System.Boolean LitJson.JsonData::inst_boolean
	bool ___inst_boolean_1;
	// System.Double LitJson.JsonData::inst_double
	double ___inst_double_2;
	// System.Int32 LitJson.JsonData::inst_int
	int32_t ___inst_int_3;
	// System.Int64 LitJson.JsonData::inst_long
	int64_t ___inst_long_4;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData> LitJson.JsonData::inst_object
	RuntimeObject* ___inst_object_5;
	// System.String LitJson.JsonData::inst_string
	String_t* ___inst_string_6;
	// System.String LitJson.JsonData::json
	String_t* ___json_7;
	// LitJson.JsonType LitJson.JsonData::type
	int32_t ___type_8;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.JsonData::object_list
	RuntimeObject* ___object_list_9;

public:
	inline static int32_t get_offset_of_inst_array_0() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_array_0)); }
	inline RuntimeObject* get_inst_array_0() const { return ___inst_array_0; }
	inline RuntimeObject** get_address_of_inst_array_0() { return &___inst_array_0; }
	inline void set_inst_array_0(RuntimeObject* value)
	{
		___inst_array_0 = value;
		Il2CppCodeGenWriteBarrier((&___inst_array_0), value);
	}

	inline static int32_t get_offset_of_inst_boolean_1() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_boolean_1)); }
	inline bool get_inst_boolean_1() const { return ___inst_boolean_1; }
	inline bool* get_address_of_inst_boolean_1() { return &___inst_boolean_1; }
	inline void set_inst_boolean_1(bool value)
	{
		___inst_boolean_1 = value;
	}

	inline static int32_t get_offset_of_inst_double_2() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_double_2)); }
	inline double get_inst_double_2() const { return ___inst_double_2; }
	inline double* get_address_of_inst_double_2() { return &___inst_double_2; }
	inline void set_inst_double_2(double value)
	{
		___inst_double_2 = value;
	}

	inline static int32_t get_offset_of_inst_int_3() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_int_3)); }
	inline int32_t get_inst_int_3() const { return ___inst_int_3; }
	inline int32_t* get_address_of_inst_int_3() { return &___inst_int_3; }
	inline void set_inst_int_3(int32_t value)
	{
		___inst_int_3 = value;
	}

	inline static int32_t get_offset_of_inst_long_4() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_long_4)); }
	inline int64_t get_inst_long_4() const { return ___inst_long_4; }
	inline int64_t* get_address_of_inst_long_4() { return &___inst_long_4; }
	inline void set_inst_long_4(int64_t value)
	{
		___inst_long_4 = value;
	}

	inline static int32_t get_offset_of_inst_object_5() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_object_5)); }
	inline RuntimeObject* get_inst_object_5() const { return ___inst_object_5; }
	inline RuntimeObject** get_address_of_inst_object_5() { return &___inst_object_5; }
	inline void set_inst_object_5(RuntimeObject* value)
	{
		___inst_object_5 = value;
		Il2CppCodeGenWriteBarrier((&___inst_object_5), value);
	}

	inline static int32_t get_offset_of_inst_string_6() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___inst_string_6)); }
	inline String_t* get_inst_string_6() const { return ___inst_string_6; }
	inline String_t** get_address_of_inst_string_6() { return &___inst_string_6; }
	inline void set_inst_string_6(String_t* value)
	{
		___inst_string_6 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_6), value);
	}

	inline static int32_t get_offset_of_json_7() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___json_7)); }
	inline String_t* get_json_7() const { return ___json_7; }
	inline String_t** get_address_of_json_7() { return &___json_7; }
	inline void set_json_7(String_t* value)
	{
		___json_7 = value;
		Il2CppCodeGenWriteBarrier((&___json_7), value);
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___type_8)); }
	inline int32_t get_type_8() const { return ___type_8; }
	inline int32_t* get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(int32_t value)
	{
		___type_8 = value;
	}

	inline static int32_t get_offset_of_object_list_9() { return static_cast<int32_t>(offsetof(JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4, ___object_list_9)); }
	inline RuntimeObject* get_object_list_9() const { return ___object_list_9; }
	inline RuntimeObject** get_address_of_object_list_9() { return &___object_list_9; }
	inline void set_object_list_9(RuntimeObject* value)
	{
		___object_list_9 = value;
		Il2CppCodeGenWriteBarrier((&___object_list_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATA_T117B9981357FBA575D8A6A902450D4331DFD4DA4_H
#ifndef JSONREADER_TDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_H
#define JSONREADER_TDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonReader
struct  JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<System.Int32> LitJson.JsonReader::automaton_stack
	Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 * ___automaton_stack_1;
	// System.Int32 LitJson.JsonReader::current_input
	int32_t ___current_input_2;
	// System.Int32 LitJson.JsonReader::current_symbol
	int32_t ___current_symbol_3;
	// System.Boolean LitJson.JsonReader::end_of_json
	bool ___end_of_json_4;
	// System.Boolean LitJson.JsonReader::end_of_input
	bool ___end_of_input_5;
	// LitJson.Lexer LitJson.JsonReader::lexer
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 * ___lexer_6;
	// System.Boolean LitJson.JsonReader::parser_in_string
	bool ___parser_in_string_7;
	// System.Boolean LitJson.JsonReader::parser_return
	bool ___parser_return_8;
	// System.Boolean LitJson.JsonReader::read_started
	bool ___read_started_9;
	// System.IO.TextReader LitJson.JsonReader::reader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___reader_10;
	// System.Boolean LitJson.JsonReader::reader_is_owned
	bool ___reader_is_owned_11;
	// System.Boolean LitJson.JsonReader::skip_non_members
	bool ___skip_non_members_12;
	// System.Object LitJson.JsonReader::token_value
	RuntimeObject * ___token_value_13;
	// LitJson.JsonToken LitJson.JsonReader::token
	int32_t ___token_14;

public:
	inline static int32_t get_offset_of_automaton_stack_1() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___automaton_stack_1)); }
	inline Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 * get_automaton_stack_1() const { return ___automaton_stack_1; }
	inline Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 ** get_address_of_automaton_stack_1() { return &___automaton_stack_1; }
	inline void set_automaton_stack_1(Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 * value)
	{
		___automaton_stack_1 = value;
		Il2CppCodeGenWriteBarrier((&___automaton_stack_1), value);
	}

	inline static int32_t get_offset_of_current_input_2() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___current_input_2)); }
	inline int32_t get_current_input_2() const { return ___current_input_2; }
	inline int32_t* get_address_of_current_input_2() { return &___current_input_2; }
	inline void set_current_input_2(int32_t value)
	{
		___current_input_2 = value;
	}

	inline static int32_t get_offset_of_current_symbol_3() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___current_symbol_3)); }
	inline int32_t get_current_symbol_3() const { return ___current_symbol_3; }
	inline int32_t* get_address_of_current_symbol_3() { return &___current_symbol_3; }
	inline void set_current_symbol_3(int32_t value)
	{
		___current_symbol_3 = value;
	}

	inline static int32_t get_offset_of_end_of_json_4() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___end_of_json_4)); }
	inline bool get_end_of_json_4() const { return ___end_of_json_4; }
	inline bool* get_address_of_end_of_json_4() { return &___end_of_json_4; }
	inline void set_end_of_json_4(bool value)
	{
		___end_of_json_4 = value;
	}

	inline static int32_t get_offset_of_end_of_input_5() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___end_of_input_5)); }
	inline bool get_end_of_input_5() const { return ___end_of_input_5; }
	inline bool* get_address_of_end_of_input_5() { return &___end_of_input_5; }
	inline void set_end_of_input_5(bool value)
	{
		___end_of_input_5 = value;
	}

	inline static int32_t get_offset_of_lexer_6() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___lexer_6)); }
	inline Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 * get_lexer_6() const { return ___lexer_6; }
	inline Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 ** get_address_of_lexer_6() { return &___lexer_6; }
	inline void set_lexer_6(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7 * value)
	{
		___lexer_6 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_6), value);
	}

	inline static int32_t get_offset_of_parser_in_string_7() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___parser_in_string_7)); }
	inline bool get_parser_in_string_7() const { return ___parser_in_string_7; }
	inline bool* get_address_of_parser_in_string_7() { return &___parser_in_string_7; }
	inline void set_parser_in_string_7(bool value)
	{
		___parser_in_string_7 = value;
	}

	inline static int32_t get_offset_of_parser_return_8() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___parser_return_8)); }
	inline bool get_parser_return_8() const { return ___parser_return_8; }
	inline bool* get_address_of_parser_return_8() { return &___parser_return_8; }
	inline void set_parser_return_8(bool value)
	{
		___parser_return_8 = value;
	}

	inline static int32_t get_offset_of_read_started_9() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___read_started_9)); }
	inline bool get_read_started_9() const { return ___read_started_9; }
	inline bool* get_address_of_read_started_9() { return &___read_started_9; }
	inline void set_read_started_9(bool value)
	{
		___read_started_9 = value;
	}

	inline static int32_t get_offset_of_reader_10() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___reader_10)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_reader_10() const { return ___reader_10; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_reader_10() { return &___reader_10; }
	inline void set_reader_10(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___reader_10 = value;
		Il2CppCodeGenWriteBarrier((&___reader_10), value);
	}

	inline static int32_t get_offset_of_reader_is_owned_11() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___reader_is_owned_11)); }
	inline bool get_reader_is_owned_11() const { return ___reader_is_owned_11; }
	inline bool* get_address_of_reader_is_owned_11() { return &___reader_is_owned_11; }
	inline void set_reader_is_owned_11(bool value)
	{
		___reader_is_owned_11 = value;
	}

	inline static int32_t get_offset_of_skip_non_members_12() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___skip_non_members_12)); }
	inline bool get_skip_non_members_12() const { return ___skip_non_members_12; }
	inline bool* get_address_of_skip_non_members_12() { return &___skip_non_members_12; }
	inline void set_skip_non_members_12(bool value)
	{
		___skip_non_members_12 = value;
	}

	inline static int32_t get_offset_of_token_value_13() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___token_value_13)); }
	inline RuntimeObject * get_token_value_13() const { return ___token_value_13; }
	inline RuntimeObject ** get_address_of_token_value_13() { return &___token_value_13; }
	inline void set_token_value_13(RuntimeObject * value)
	{
		___token_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___token_value_13), value);
	}

	inline static int32_t get_offset_of_token_14() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA, ___token_14)); }
	inline int32_t get_token_14() const { return ___token_14; }
	inline int32_t* get_address_of_token_14() { return &___token_14; }
	inline void set_token_14(int32_t value)
	{
		___token_14 = value;
	}
};

struct JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>> LitJson.JsonReader::parse_table
	RuntimeObject* ___parse_table_0;

public:
	inline static int32_t get_offset_of_parse_table_0() { return static_cast<int32_t>(offsetof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_StaticFields, ___parse_table_0)); }
	inline RuntimeObject* get_parse_table_0() const { return ___parse_table_0; }
	inline RuntimeObject** get_address_of_parse_table_0() { return &___parse_table_0; }
	inline void set_parse_table_0(RuntimeObject* value)
	{
		___parse_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___parse_table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_TDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_H
#ifndef NOTIFYCOLLECTIONCHANGEDEVENTARGS_TE84B4C63E7BA6C5D10AC43C832375FBD88360F79_H
#define NOTIFYCOLLECTIONCHANGEDEVENTARGS_TE84B4C63E7BA6C5D10AC43C832375FBD88360F79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs
struct  NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// PlatformSupport.Collections.Specialized.NotifyCollectionChangedAction PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_action
	int32_t ____action_1;
	// System.Collections.IList PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_newItems
	RuntimeObject* ____newItems_2;
	// System.Collections.IList PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_oldItems
	RuntimeObject* ____oldItems_3;
	// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_newStartingIndex
	int32_t ____newStartingIndex_4;
	// System.Int32 PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventArgs::_oldStartingIndex
	int32_t ____oldStartingIndex_5;

public:
	inline static int32_t get_offset_of__action_1() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79, ____action_1)); }
	inline int32_t get__action_1() const { return ____action_1; }
	inline int32_t* get_address_of__action_1() { return &____action_1; }
	inline void set__action_1(int32_t value)
	{
		____action_1 = value;
	}

	inline static int32_t get_offset_of__newItems_2() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79, ____newItems_2)); }
	inline RuntimeObject* get__newItems_2() const { return ____newItems_2; }
	inline RuntimeObject** get_address_of__newItems_2() { return &____newItems_2; }
	inline void set__newItems_2(RuntimeObject* value)
	{
		____newItems_2 = value;
		Il2CppCodeGenWriteBarrier((&____newItems_2), value);
	}

	inline static int32_t get_offset_of__oldItems_3() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79, ____oldItems_3)); }
	inline RuntimeObject* get__oldItems_3() const { return ____oldItems_3; }
	inline RuntimeObject** get_address_of__oldItems_3() { return &____oldItems_3; }
	inline void set__oldItems_3(RuntimeObject* value)
	{
		____oldItems_3 = value;
		Il2CppCodeGenWriteBarrier((&____oldItems_3), value);
	}

	inline static int32_t get_offset_of__newStartingIndex_4() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79, ____newStartingIndex_4)); }
	inline int32_t get__newStartingIndex_4() const { return ____newStartingIndex_4; }
	inline int32_t* get_address_of__newStartingIndex_4() { return &____newStartingIndex_4; }
	inline void set__newStartingIndex_4(int32_t value)
	{
		____newStartingIndex_4 = value;
	}

	inline static int32_t get_offset_of__oldStartingIndex_5() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79, ____oldStartingIndex_5)); }
	inline int32_t get__oldStartingIndex_5() const { return ____oldStartingIndex_5; }
	inline int32_t* get_address_of__oldStartingIndex_5() { return &____oldStartingIndex_5; }
	inline void set__oldStartingIndex_5(int32_t value)
	{
		____oldStartingIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDEVENTARGS_TE84B4C63E7BA6C5D10AC43C832375FBD88360F79_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ONBEFOREHEADERSENDDELEGATE_TB604059248AD5E92077D992D4460B4E38D3980D3_H
#define ONBEFOREHEADERSENDDELEGATE_TB604059248AD5E92077D992D4460B4E38D3980D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnBeforeHeaderSendDelegate
struct  OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONBEFOREHEADERSENDDELEGATE_TB604059248AD5E92077D992D4460B4E38D3980D3_H
#ifndef ONBEFOREREDIRECTIONDELEGATE_T9D2909A0B36C07BCA1E27A8CC246C62845CC438D_H
#define ONBEFOREREDIRECTIONDELEGATE_T9D2909A0B36C07BCA1E27A8CC246C62845CC438D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnBeforeRedirectionDelegate
struct  OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONBEFOREREDIRECTIONDELEGATE_T9D2909A0B36C07BCA1E27A8CC246C62845CC438D_H
#ifndef ONDOWNLOADPROGRESSDELEGATE_T4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB_H
#define ONDOWNLOADPROGRESSDELEGATE_T4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnDownloadProgressDelegate
struct  OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDOWNLOADPROGRESSDELEGATE_T4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB_H
#ifndef ONHEADERENUMERATIONDELEGATE_T22243CC85C4C3622AA7B21FFB79A4D7004E5F074_H
#define ONHEADERENUMERATIONDELEGATE_T22243CC85C4C3622AA7B21FFB79A4D7004E5F074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnHeaderEnumerationDelegate
struct  OnHeaderEnumerationDelegate_t22243CC85C4C3622AA7B21FFB79A4D7004E5F074  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONHEADERENUMERATIONDELEGATE_T22243CC85C4C3622AA7B21FFB79A4D7004E5F074_H
#ifndef ONREQUESTFINISHEDDELEGATE_T08C45555D0911A1245384E8074FF2CDEC69AE251_H
#define ONREQUESTFINISHEDDELEGATE_T08C45555D0911A1245384E8074FF2CDEC69AE251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnRequestFinishedDelegate
struct  OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREQUESTFINISHEDDELEGATE_T08C45555D0911A1245384E8074FF2CDEC69AE251_H
#ifndef ONSTREAMINGDATADELEGATE_T7A995E4977BDE22AB9968D84CFCF41634583FE52_H
#define ONSTREAMINGDATADELEGATE_T7A995E4977BDE22AB9968D84CFCF41634583FE52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnStreamingDataDelegate
struct  OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTREAMINGDATADELEGATE_T7A995E4977BDE22AB9968D84CFCF41634583FE52_H
#ifndef ONUPLOADPROGRESSDELEGATE_T5071CF0BEA611F75180C907F47BB034EFA18163A_H
#define ONUPLOADPROGRESSDELEGATE_T5071CF0BEA611F75180C907F47BB034EFA18163A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.OnUploadProgressDelegate
struct  OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPLOADPROGRESSDELEGATE_T5071CF0BEA611F75180C907F47BB034EFA18163A_H
#ifndef EXPORTERFUNC_T587D3B4CC21C10533AC28A5B29EC82BF4240F82D_H
#define EXPORTERFUNC_T587D3B4CC21C10533AC28A5B29EC82BF4240F82D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ExporterFunc
struct  ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPORTERFUNC_T587D3B4CC21C10533AC28A5B29EC82BF4240F82D_H
#ifndef IMPORTERFUNC_T0C9307B5606BE6701350A0A5B24236DD53A10097_H
#define IMPORTERFUNC_T0C9307B5606BE6701350A0A5B24236DD53A10097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ImporterFunc
struct  ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTERFUNC_T0C9307B5606BE6701350A0A5B24236DD53A10097_H
#ifndef STATEHANDLER_TF342477732DF9FD60FF5A8A29A5C239E8AAA571E_H
#define STATEHANDLER_TF342477732DF9FD60FF5A8A29A5C239E8AAA571E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Lexer_StateHandler
struct  StateHandler_tF342477732DF9FD60FF5A8A29A5C239E8AAA571E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEHANDLER_TF342477732DF9FD60FF5A8A29A5C239E8AAA571E_H
#ifndef WRAPPERFACTORY_T0C6B1FAD14ED686C97D26029B669DBC40B98F984_H
#define WRAPPERFACTORY_T0C6B1FAD14ED686C97D26029B669DBC40B98F984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.WrapperFactory
struct  WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERFACTORY_T0C6B1FAD14ED686C97D26029B669DBC40B98F984_H
#ifndef NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T5533E4A54A433E14EC94A0B88DE35522FBA510DF_H
#define NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T5533E4A54A433E14EC94A0B88DE35522FBA510DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformSupport.Collections.Specialized.NotifyCollectionChangedEventHandler
struct  NotifyCollectionChangedEventHandler_t5533E4A54A433E14EC94A0B88DE35522FBA510DF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T5533E4A54A433E14EC94A0B88DE35522FBA510DF_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#define GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#define RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifndef RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#define RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifndef RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#define RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef HTTPUPDATEDELEGATOR_TFC0932EE397132F544B1BA83EF60D10E516D637D_H
#define HTTPUPDATEDELEGATOR_TFC0932EE397132F544B1BA83EF60D10E516D637D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPUpdateDelegator
struct  HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields
{
public:
	// BestHTTP.HTTPUpdateDelegator BestHTTP.HTTPUpdateDelegator::<Instance>k__BackingField
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D * ___U3CInstanceU3Ek__BackingField_4;
	// System.Boolean BestHTTP.HTTPUpdateDelegator::<IsCreated>k__BackingField
	bool ___U3CIsCreatedU3Ek__BackingField_5;
	// System.Boolean BestHTTP.HTTPUpdateDelegator::<IsThreaded>k__BackingField
	bool ___U3CIsThreadedU3Ek__BackingField_6;
	// System.Boolean BestHTTP.HTTPUpdateDelegator::<IsThreadRunning>k__BackingField
	bool ___U3CIsThreadRunningU3Ek__BackingField_7;
	// System.Int32 BestHTTP.HTTPUpdateDelegator::<ThreadFrequencyInMS>k__BackingField
	int32_t ___U3CThreadFrequencyInMSU3Ek__BackingField_8;
	// System.Func`1<System.Boolean> BestHTTP.HTTPUpdateDelegator::OnBeforeApplicationQuit
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___OnBeforeApplicationQuit_9;
	// System.Action`1<System.Boolean> BestHTTP.HTTPUpdateDelegator::OnApplicationForegroundStateChanged
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___OnApplicationForegroundStateChanged_10;
	// System.Boolean BestHTTP.HTTPUpdateDelegator::IsSetupCalled
	bool ___IsSetupCalled_11;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CIsCreatedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___U3CIsCreatedU3Ek__BackingField_5)); }
	inline bool get_U3CIsCreatedU3Ek__BackingField_5() const { return ___U3CIsCreatedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsCreatedU3Ek__BackingField_5() { return &___U3CIsCreatedU3Ek__BackingField_5; }
	inline void set_U3CIsCreatedU3Ek__BackingField_5(bool value)
	{
		___U3CIsCreatedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsThreadedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___U3CIsThreadedU3Ek__BackingField_6)); }
	inline bool get_U3CIsThreadedU3Ek__BackingField_6() const { return ___U3CIsThreadedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsThreadedU3Ek__BackingField_6() { return &___U3CIsThreadedU3Ek__BackingField_6; }
	inline void set_U3CIsThreadedU3Ek__BackingField_6(bool value)
	{
		___U3CIsThreadedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsThreadRunningU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___U3CIsThreadRunningU3Ek__BackingField_7)); }
	inline bool get_U3CIsThreadRunningU3Ek__BackingField_7() const { return ___U3CIsThreadRunningU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsThreadRunningU3Ek__BackingField_7() { return &___U3CIsThreadRunningU3Ek__BackingField_7; }
	inline void set_U3CIsThreadRunningU3Ek__BackingField_7(bool value)
	{
		___U3CIsThreadRunningU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CThreadFrequencyInMSU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___U3CThreadFrequencyInMSU3Ek__BackingField_8)); }
	inline int32_t get_U3CThreadFrequencyInMSU3Ek__BackingField_8() const { return ___U3CThreadFrequencyInMSU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CThreadFrequencyInMSU3Ek__BackingField_8() { return &___U3CThreadFrequencyInMSU3Ek__BackingField_8; }
	inline void set_U3CThreadFrequencyInMSU3Ek__BackingField_8(int32_t value)
	{
		___U3CThreadFrequencyInMSU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_OnBeforeApplicationQuit_9() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___OnBeforeApplicationQuit_9)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_OnBeforeApplicationQuit_9() const { return ___OnBeforeApplicationQuit_9; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_OnBeforeApplicationQuit_9() { return &___OnBeforeApplicationQuit_9; }
	inline void set_OnBeforeApplicationQuit_9(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___OnBeforeApplicationQuit_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnBeforeApplicationQuit_9), value);
	}

	inline static int32_t get_offset_of_OnApplicationForegroundStateChanged_10() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___OnApplicationForegroundStateChanged_10)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_OnApplicationForegroundStateChanged_10() const { return ___OnApplicationForegroundStateChanged_10; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_OnApplicationForegroundStateChanged_10() { return &___OnApplicationForegroundStateChanged_10; }
	inline void set_OnApplicationForegroundStateChanged_10(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___OnApplicationForegroundStateChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationForegroundStateChanged_10), value);
	}

	inline static int32_t get_offset_of_IsSetupCalled_11() { return static_cast<int32_t>(offsetof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields, ___IsSetupCalled_11)); }
	inline bool get_IsSetupCalled_11() const { return ___IsSetupCalled_11; }
	inline bool* get_address_of_IsSetupCalled_11() { return &___IsSetupCalled_11; }
	inline void set_IsSetupCalled_11(bool value)
	{
		___IsSetupCalled_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPUPDATEDELEGATOR_TFC0932EE397132F544B1BA83EF60D10E516D637D_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#define LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Padding_4)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalMinSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalPreferredSize_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalFlexibleSize_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_RectChildren_11)); }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#define HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB  : public LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_14;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_15;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_16;

public:
	inline static int32_t get_offset_of_m_Spacing_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_Spacing_12)); }
	inline float get_m_Spacing_12() const { return ___m_Spacing_12; }
	inline float* get_address_of_m_Spacing_12() { return &___m_Spacing_12; }
	inline void set_m_Spacing_12(float value)
	{
		___m_Spacing_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandWidth_13)); }
	inline bool get_m_ChildForceExpandWidth_13() const { return ___m_ChildForceExpandWidth_13; }
	inline bool* get_address_of_m_ChildForceExpandWidth_13() { return &___m_ChildForceExpandWidth_13; }
	inline void set_m_ChildForceExpandWidth_13(bool value)
	{
		___m_ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandHeight_14)); }
	inline bool get_m_ChildForceExpandHeight_14() const { return ___m_ChildForceExpandHeight_14; }
	inline bool* get_address_of_m_ChildForceExpandHeight_14() { return &___m_ChildForceExpandHeight_14; }
	inline void set_m_ChildForceExpandHeight_14(bool value)
	{
		___m_ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_15() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlWidth_15)); }
	inline bool get_m_ChildControlWidth_15() const { return ___m_ChildControlWidth_15; }
	inline bool* get_address_of_m_ChildControlWidth_15() { return &___m_ChildControlWidth_15; }
	inline void set_m_ChildControlWidth_15(bool value)
	{
		___m_ChildControlWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_16() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlHeight_16)); }
	inline bool get_m_ChildControlHeight_16() const { return ___m_ChildControlHeight_16; }
	inline bool* get_address_of_m_ChildControlHeight_16() { return &___m_ChildControlHeight_16; }
	inline void set_m_ChildControlHeight_16(bool value)
	{
		___m_ChildControlHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifndef VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#define VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3800[8] = 
{
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Padding_4(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_ChildAlignment_5(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Rect_6(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Tracker_7(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalMinSize_8(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalPreferredSize_9(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalFlexibleSize_10(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_RectChildren_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3801[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD), -1, sizeof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3802[9] = 
{
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5), -1, sizeof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3803[8] = 
{
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3806[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3807[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3808[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3809[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3816[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3825[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (U3CModuleU3E_t10CF0CFC203D043C6935309D8C8487C6975F47D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (EmbeddedAttribute_t1153BBC45B381753B14574A9E276E3C7B9C63CAE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (IsReadOnlyAttribute_tD9D90EFF5545DBE43B58F5B76A4191B55489B1AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (Security_t3DAE904F4BB1A19CC48BC7B72BA001A4E2066B0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (NotifyCollectionChangedEventHandler_t5533E4A54A433E14EC94A0B88DE35522FBA510DF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (NotifyCollectionChangedAction_t56484E7A9A3F7F0D831548351033C2E73D7700FA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3834[6] = 
{
	NotifyCollectionChangedAction_t56484E7A9A3F7F0D831548351033C2E73D7700FA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3835[5] = 
{
	NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79::get_offset_of__action_1(),
	NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79::get_offset_of__newItems_2(),
	NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79::get_offset_of__oldItems_3(),
	NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79::get_offset_of__newStartingIndex_4(),
	NotifyCollectionChangedEventArgs_tE84B4C63E7BA6C5D10AC43C832375FBD88360F79::get_offset_of__oldStartingIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (ReadOnlyList_t8D6A67A2711D0EB37EF4376A3F5FCEB020565555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3836[1] = 
{
	ReadOnlyList_t8D6A67A2711D0EB37EF4376A3F5FCEB020565555::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3837[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (JsonType_tEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3838[9] = 
{
	JsonType_tEB13B1FD81DAD5A9AB955D2B9067B32DFD0E9731::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[10] = 
{
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_array_0(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_boolean_1(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_double_2(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_int_3(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_long_4(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_object_5(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_inst_string_6(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_json_7(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_type_8(),
	JsonData_t117B9981357FBA575D8A6A902450D4331DFD4DA4::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (OrderedDictionaryEnumerator_t190D292E0F6DACF487A8C2AA0904EDAE0456AFE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3842[1] = 
{
	OrderedDictionaryEnumerator_t190D292E0F6DACF487A8C2AA0904EDAE0456AFE2::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (JsonException_t69F2F742960FA18FF528C4C415B6E6B7DB8F0323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[3] = 
{
	PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropertyMetadata_tD5575EA22560EE44D9A460B40D71A8E56951AD32::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3845[3] = 
{
	ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArrayMetadata_t343028BCC6F54CE69728922FE052E194A558F360::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3846[3] = 
{
	ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectMetadata_t2F9B9ED23E3289CFE4FC6049B8C26FD67986AA98::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (ExporterFunc_t587D3B4CC21C10533AC28A5B29EC82BF4240F82D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (ImporterFunc_t0C9307B5606BE6701350A0A5B24236DD53A10097), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (WrapperFactory_t0C6B1FAD14ED686C97D26029B669DBC40B98F984), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675), -1, sizeof(JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3852[16] = 
{
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_max_nesting_depth_0(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_datetime_format_1(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_base_exporters_table_2(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_custom_exporters_table_3(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_base_importers_table_4(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_custom_importers_table_5(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_array_metadata_6(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_array_metadata_lock_7(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_conv_ops_8(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_conv_ops_lock_9(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_object_metadata_10(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_object_metadata_lock_11(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_type_properties_12(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_type_properties_lock_13(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_static_writer_14(),
	JsonMapper_t1B490A9D1CD428D13C499CF81303AC5804391675_StaticFields::get_offset_of_static_writer_lock_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D), -1, sizeof(U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3853[26] = 
{
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__25_0_1(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_0_2(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_1_3(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_2_4(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_3_5(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_4_6(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_5_7(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_6_8(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_7_9(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__26_8_10(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_0_11(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_1_12(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_2_13(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_3_14(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_4_15(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_5_16(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_6_17(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_7_18(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_8_19(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_9_20(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_10_21(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__27_11_22(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__32_0_23(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__33_0_24(),
	U3CU3Ec_t521AD7E039E69019D41B747BCA400B95B5469A5D_StaticFields::get_offset_of_U3CU3E9__34_0_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3855[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (JsonMockWrapper_tE70C09363921A27A88CD21BAFC9ED3DAFD15E2A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (JsonToken_t38920084F71838929071031D5129A8F2F7E05F98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3857[13] = 
{
	JsonToken_t38920084F71838929071031D5129A8F2F7E05F98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA), -1, sizeof(JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3858[15] = 
{
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA_StaticFields::get_offset_of_parse_table_0(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_automaton_stack_1(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_current_input_2(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_current_symbol_3(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_end_of_json_4(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_end_of_input_5(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_lexer_6(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_parser_in_string_7(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_parser_return_8(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_read_started_9(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_reader_10(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_reader_is_owned_11(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_skip_non_members_12(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_token_value_13(),
	JsonReader_tDF8C32CE65026A8CEAFADA3D7C47B49FE6860DAA::get_offset_of_token_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (Condition_t2BD17CEF1AA9B94C3097DA7170A96A5356A5023F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3859[6] = 
{
	Condition_t2BD17CEF1AA9B94C3097DA7170A96A5356A5023F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3860[5] = 
{
	WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF::get_offset_of_Count_0(),
	WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF::get_offset_of_InArray_1(),
	WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF::get_offset_of_InObject_2(),
	WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF::get_offset_of_ExpectingValue_3(),
	WriterContext_t544FC7907F836B87A67DDB96E4199A03D14625CF::get_offset_of_Padding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (JsonWriter_t1356A50FAE30049796024595C2745168258D79C0), -1, sizeof(JsonWriter_t1356A50FAE30049796024595C2745168258D79C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3861[11] = 
{
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_context_1(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_ctx_stack_2(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_has_reached_end_3(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_hex_seq_4(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_indentation_5(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_indent_value_6(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_inst_string_builder_7(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_pretty_print_8(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_validate_9(),
	JsonWriter_t1356A50FAE30049796024595C2745168258D79C0::get_offset_of_writer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[4] = 
{
	FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA::get_offset_of_Return_0(),
	FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA::get_offset_of_NextState_1(),
	FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA::get_offset_of_L_2(),
	FsmContext_t9382B92C612C76D5F2B28F7A309A63C72C7A8BCA::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7), -1, sizeof(Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3863[14] = 
{
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_allow_comments_2(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_end_of_input_4(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_fsm_context_5(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_input_buffer_6(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_input_char_7(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_reader_8(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_state_9(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_string_buffer_10(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_string_value_11(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_token_12(),
	Lexer_tF904E5BC4EB7FDCF9DCD56EF255F229CA91367A7::get_offset_of_unichar_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (StateHandler_tF342477732DF9FD60FF5A8A29A5C239E8AAA571E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (ParserToken_tD8E3691C8674AA12C50D4F24927091A65ED60CBC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3865[20] = 
{
	ParserToken_tD8E3691C8674AA12C50D4F24927091A65ED60CBC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (AsyncExtensions_t7CA94A7D15C8B1690C0E15C822F193911CBB8848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (HTTPRequestAsyncExtensions_t760B678D5F16BCF75A432565A8506B790D9D9899), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (U3CU3Ec__DisplayClass0_0_tE04FEADAA94381DDC7B05CB9849BBF0C650928EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3869[1] = 
{
	U3CU3Ec__DisplayClass0_0_tE04FEADAA94381DDC7B05CB9849BBF0C650928EE::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (U3CU3Ec__DisplayClass1_0_tF5DF96B51CD32D46621B827561F6856FA2908597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3870[1] = 
{
	U3CU3Ec__DisplayClass1_0_tF5DF96B51CD32D46621B827561F6856FA2908597::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (U3CU3Ec__DisplayClass2_0_t23D0047A0A2BA75CBB759A8A6AC37B2026277211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3871[1] = 
{
	U3CU3Ec__DisplayClass2_0_t23D0047A0A2BA75CBB759A8A6AC37B2026277211::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (U3CU3Ec__DisplayClass3_0_t380EA9C2C1052BC7C20F58EB901EFE003D8FFC78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3872[1] = 
{
	U3CU3Ec__DisplayClass3_0_t380EA9C2C1052BC7C20F58EB901EFE003D8FFC78::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3873[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3874[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3875[4] = 
{
	ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A), -1, sizeof(HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3876[25] = 
{
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_HTTP2Settings_0(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_maxConnectionPerServer_1(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CKeepAliveDefaultValueU3Ek__BackingField_2(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CIsCachingDisabledU3Ek__BackingField_3(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CMaxConnectionIdleTimeU3Ek__BackingField_4(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_5(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CCookieJarSizeU3Ek__BackingField_6(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CEnablePrivateBrowsingU3Ek__BackingField_7(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_8(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CRequestTimeoutU3Ek__BackingField_9(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CRootCacheFolderProviderU3Ek__BackingField_10(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CProxyU3Ek__BackingField_11(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_heartbeats_12(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_logger_13(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CDefaultCertificateVerifyerU3Ek__BackingField_14(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CDefaultClientCredentialsProviderU3Ek__BackingField_15(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_16(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CDefaultCertificationValidatorU3Ek__BackingField_17(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_SendBufferSize_18(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_ReceiveBufferSize_19(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_IOService_20(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CMaxPathLengthU3Ek__BackingField_21(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_UserAgent_22(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_U3CIsQuittingU3Ek__BackingField_23(),
	HTTPManager_t0FA48E706816D1DC46B177FC33D1F8EDF40B3A5A_StaticFields::get_offset_of_IsSetupCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (HTTPMethods_tC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3877[9] = 
{
	HTTPMethods_tC626FFCC1DB8460A1C899EB08479A7D5C08F5AAF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[4] = 
{
	HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A::get_offset_of_U3CFirstBytePosU3Ek__BackingField_0(),
	HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A::get_offset_of_U3CLastBytePosU3Ek__BackingField_1(),
	HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A::get_offset_of_U3CContentLengthU3Ek__BackingField_2(),
	HTTPRange_tB1278C9AEFC75DD73592AD8FD237FB270D4EB04A::get_offset_of_U3CIsValidU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (HTTPRequestStates_t42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3879[8] = 
{
	HTTPRequestStates_t42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (OnRequestFinishedDelegate_t08C45555D0911A1245384E8074FF2CDEC69AE251), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (OnDownloadProgressDelegate_t4887197783B2EEACDCDDDD4CFA31278C7EC0B1BB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (OnUploadProgressDelegate_t5071CF0BEA611F75180C907F47BB034EFA18163A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (OnBeforeRedirectionDelegate_t9D2909A0B36C07BCA1E27A8CC246C62845CC438D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (OnHeaderEnumerationDelegate_t22243CC85C4C3622AA7B21FFB79A4D7004E5F074), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (OnBeforeHeaderSendDelegate_tB604059248AD5E92077D992D4460B4E38D3980D3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (OnStreamingDataDelegate_t7A995E4977BDE22AB9968D84CFCF41634583FE52), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837), -1, sizeof(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3887[55] = 
{
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields::get_offset_of_EOL_0(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields::get_offset_of_MethodNames_1(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837_StaticFields::get_offset_of_UploadChunkSize_2(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CUriU3Ek__BackingField_3(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CMethodTypeU3Ek__BackingField_4(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CRawDataU3Ek__BackingField_5(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CUploadStreamU3Ek__BackingField_6(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CDisposeUploadStreamU3Ek__BackingField_7(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CUseUploadStreamLengthU3Ek__BackingField_8(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_OnUploadProgress_9(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CStreamChunksImmediatelyU3Ek__BackingField_10(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CMaxFragmentQueueLengthU3Ek__BackingField_11(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CProcessingStartedU3Ek__BackingField_13(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_OnStreamingData_14(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_OnHeadersReceived_15(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CRetriesU3Ek__BackingField_16(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CMaxRetriesU3Ek__BackingField_17(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CIsCancellationRequestedU3Ek__BackingField_18(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_OnDownloadProgress_19(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_OnUpgraded_20(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CIsRedirectedU3Ek__BackingField_21(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CRedirectUriU3Ek__BackingField_22(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CResponseU3Ek__BackingField_23(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CProxyResponseU3Ek__BackingField_24(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CExceptionU3Ek__BackingField_25(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CTagU3Ek__BackingField_26(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CCredentialsU3Ek__BackingField_27(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CProxyU3Ek__BackingField_28(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CMaxRedirectsU3Ek__BackingField_29(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CUseAlternateSSLU3Ek__BackingField_30(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_31(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_customCookies_32(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CFormUsageU3Ek__BackingField_33(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of__state_34(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CRedirectCountU3Ek__BackingField_35(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_CustomCertificationValidator_36(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_37(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CTimeoutU3Ek__BackingField_38(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CEnableTimoutForStreamingU3Ek__BackingField_39(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CEnableSafeReadOnUnknownContentLengthU3Ek__BackingField_40(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CCustomCertificateVerifyerU3Ek__BackingField_41(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CCustomClientCredentialsProviderU3Ek__BackingField_42(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CCustomTLSServerNameListU3Ek__BackingField_43(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CProtocolHandlerU3Ek__BackingField_44(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_onBeforeRedirection_45(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of__onBeforeHeaderSend_46(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_isKeepAlive_47(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_disableCache_48(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_cacheOnly_49(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_streamFragmentSize_50(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_useStreaming_51(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_U3CHeadersU3Ek__BackingField_52(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_FieldCollector_53(),
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837::get_offset_of_FormImpl_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (U3CU3Ec__DisplayClass223_0_t5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3888[1] = 
{
	U3CU3Ec__DisplayClass223_0_t5A41DF8B9B57BBBA4EC1109A2471DDA6FE45E69E::get_offset_of_customCookie_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (U3CU3Ec__DisplayClass224_0_tF228B863E1B372FEEEC87493BDA173A1E52D78A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3889[2] = 
{
	U3CU3Ec__DisplayClass224_0_tF228B863E1B372FEEEC87493BDA173A1E52D78A7::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass224_0_tF228B863E1B372FEEEC87493BDA173A1E52D78A7::get_offset_of_stream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3890[25] = 
{
	0,
	0,
	0,
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CVersionMajorU3Ek__BackingField_3(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CVersionMinorU3Ek__BackingField_4(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CStatusCodeU3Ek__BackingField_5(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CMessageU3Ek__BackingField_6(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CIsStreamedU3Ek__BackingField_7(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CIsFromCacheU3Ek__BackingField_8(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CCacheFileInfoU3Ek__BackingField_9(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CIsCacheOnlyU3Ek__BackingField_10(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CHeadersU3Ek__BackingField_11(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CDataU3Ek__BackingField_12(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CIsUpgradedU3Ek__BackingField_13(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CCookiesU3Ek__BackingField_14(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_dataAsText_15(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_texture_16(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_U3CIsClosedManuallyU3Ek__BackingField_17(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_UnprocessedFragments_18(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_baseRequest_19(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_Stream_20(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_fragmentBuffer_21(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_fragmentBufferDataLength_22(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_cacheStream_23(),
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB::get_offset_of_allFragmentSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D), -1, sizeof(HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3891[8] = 
{
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_U3CIsCreatedU3Ek__BackingField_5(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_U3CIsThreadedU3Ek__BackingField_6(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_U3CIsThreadRunningU3Ek__BackingField_7(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_U3CThreadFrequencyInMSU3Ek__BackingField_8(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_OnBeforeApplicationQuit_9(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_OnApplicationForegroundStateChanged_10(),
	HTTPUpdateDelegator_tFC0932EE397132F544B1BA83EF60D10E516D637D_StaticFields::get_offset_of_IsSetupCalled_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3892[2] = 
{
	Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9::get_offset_of_U3CAddressU3Ek__BackingField_0(),
	Proxy_t69197B0B3D1610ED7580B856BB07DB3FB5916DD9::get_offset_of_U3CCredentialsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3893[3] = 
{
	HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA::get_offset_of_U3CIsTransparentU3Ek__BackingField_2(),
	HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA::get_offset_of_U3CSendWholeUriU3Ek__BackingField_3(),
	HTTPProxy_t701328FB5A83431E29FCED469AEF8AD519C2EEDA::get_offset_of_U3CNonTransparentForHTTPSU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (SOCKSVersions_t4FFB7BF1E7FAE869954B000547FCDBA3928A3982)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3894[3] = 
{
	SOCKSVersions_t4FFB7BF1E7FAE869954B000547FCDBA3928A3982::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (SOCKSMethods_t361BE71276EE28AB85843D6B56430F8DE2A2D96F)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3895[5] = 
{
	SOCKSMethods_t361BE71276EE28AB85843D6B56430F8DE2A2D96F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (SOCKSReplies_t1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3896[10] = 
{
	SOCKSReplies_t1051A6C29DDC38157CD6EE7A7E0B5E2F5F6B3FE9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (SOCKSAddressTypes_tAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3897[4] = 
{
	SOCKSAddressTypes_tAEABB08446E408D32C9EF6C3D3CA3CC491DB6F91::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (SOCKSProxy_t2806A10A000F70E4CEA7258EF9BA64C3A1E0A76A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (WebSocketStates_tEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3899[6] = 
{
	WebSocketStates_tEAAB12C7837B7B1CCDB6DDF1227FF79F84B33460::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
