﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse
struct BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID
struct CertID_tD10877523D531F998848FBD48D854308134F2918;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus
struct CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspRequest
struct OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponse
struct OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Request
struct Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID
struct ResponderID_t26B99AFE9E4E05948990F461880598D304592346;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData
struct ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.RevokedInfo
struct RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse
struct SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher
struct IBufferedCipher_tF573FBDFB2ABDB6CF7049013D91BED1EABEEAA3A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_t3F0A2BB86CE5BD61188A9989250C19FDFF733F50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[]
struct BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t6071986B64066FBF97315592BE971355FA584A39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[]
struct ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism
struct ECEndomorphism_t6EB799DC03F118DC4DC962E5E5B94F009F586948;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve
struct F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mPoint
struct F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpPoint
struct FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray
struct LongArray_t878F80FE9906AAC7F3471E383219621561440608;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier
struct ECMultiplier_t3D3844C8A426F0454D623B3F3AA875FE6896AB11;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField
struct IFiniteField_tFCD218E0A913FF4E2BB6D10F42F69C3979B748D5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IPolynomial
struct IPolynomial_tC7DB0462AA69131EDB29441CB537E4326BCAEF65;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateID
struct CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RespID
struct RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t585F03B00DBE773170901D2ABF9576187FAF92B5;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.UInt16[]
struct UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BIGINTEGER_T8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_H
#define BIGINTEGER_T8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct  BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::magnitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___magnitude_31;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::sign
	int32_t ___sign_32;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::nBits
	int32_t ___nBits_33;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::nBitLength
	int32_t ___nBitLength_34;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::mQuote
	int32_t ___mQuote_35;

public:
	inline static int32_t get_offset_of_magnitude_31() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50, ___magnitude_31)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_magnitude_31() const { return ___magnitude_31; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_magnitude_31() { return &___magnitude_31; }
	inline void set_magnitude_31(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___magnitude_31 = value;
		Il2CppCodeGenWriteBarrier((&___magnitude_31), value);
	}

	inline static int32_t get_offset_of_sign_32() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50, ___sign_32)); }
	inline int32_t get_sign_32() const { return ___sign_32; }
	inline int32_t* get_address_of_sign_32() { return &___sign_32; }
	inline void set_sign_32(int32_t value)
	{
		___sign_32 = value;
	}

	inline static int32_t get_offset_of_nBits_33() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50, ___nBits_33)); }
	inline int32_t get_nBits_33() const { return ___nBits_33; }
	inline int32_t* get_address_of_nBits_33() { return &___nBits_33; }
	inline void set_nBits_33(int32_t value)
	{
		___nBits_33 = value;
	}

	inline static int32_t get_offset_of_nBitLength_34() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50, ___nBitLength_34)); }
	inline int32_t get_nBitLength_34() const { return ___nBitLength_34; }
	inline int32_t* get_address_of_nBitLength_34() { return &___nBitLength_34; }
	inline void set_nBitLength_34(int32_t value)
	{
		___nBitLength_34 = value;
	}

	inline static int32_t get_offset_of_mQuote_35() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50, ___mQuote_35)); }
	inline int32_t get_mQuote_35() const { return ___mQuote_35; }
	inline int32_t* get_address_of_mQuote_35() { return &___mQuote_35; }
	inline void set_mQuote_35(int32_t value)
	{
		___mQuote_35 = value;
	}
};

struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields
{
public:
	// System.Int32[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::primeLists
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___primeLists_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::primeProducts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___primeProducts_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::ZeroMagnitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ZeroMagnitude_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::ZeroEncoding
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ZeroEncoding_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::SMALL_CONSTANTS
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___SMALL_CONSTANTS_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::Zero
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Zero_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::One
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___One_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::Two
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Two_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::Three
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Three_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::Four
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Four_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::Ten
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Ten_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::BitLengthTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___BitLengthTable_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix2_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix2E
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix2E_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix8
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix8_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix8E
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix8E_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix10
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix10_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix10E
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix10E_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix16
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix16_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::radix16E
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___radix16E_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::RandomSource
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___RandomSource_26;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger::ExpWindowThresholds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ExpWindowThresholds_27;

public:
	inline static int32_t get_offset_of_primeLists_0() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___primeLists_0)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_primeLists_0() const { return ___primeLists_0; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_primeLists_0() { return &___primeLists_0; }
	inline void set_primeLists_0(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___primeLists_0 = value;
		Il2CppCodeGenWriteBarrier((&___primeLists_0), value);
	}

	inline static int32_t get_offset_of_primeProducts_1() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___primeProducts_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_primeProducts_1() const { return ___primeProducts_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_primeProducts_1() { return &___primeProducts_1; }
	inline void set_primeProducts_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___primeProducts_1 = value;
		Il2CppCodeGenWriteBarrier((&___primeProducts_1), value);
	}

	inline static int32_t get_offset_of_ZeroMagnitude_4() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___ZeroMagnitude_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ZeroMagnitude_4() const { return ___ZeroMagnitude_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ZeroMagnitude_4() { return &___ZeroMagnitude_4; }
	inline void set_ZeroMagnitude_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ZeroMagnitude_4 = value;
		Il2CppCodeGenWriteBarrier((&___ZeroMagnitude_4), value);
	}

	inline static int32_t get_offset_of_ZeroEncoding_5() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___ZeroEncoding_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ZeroEncoding_5() const { return ___ZeroEncoding_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ZeroEncoding_5() { return &___ZeroEncoding_5; }
	inline void set_ZeroEncoding_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ZeroEncoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___ZeroEncoding_5), value);
	}

	inline static int32_t get_offset_of_SMALL_CONSTANTS_6() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___SMALL_CONSTANTS_6)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_SMALL_CONSTANTS_6() const { return ___SMALL_CONSTANTS_6; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_SMALL_CONSTANTS_6() { return &___SMALL_CONSTANTS_6; }
	inline void set_SMALL_CONSTANTS_6(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___SMALL_CONSTANTS_6 = value;
		Il2CppCodeGenWriteBarrier((&___SMALL_CONSTANTS_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___Zero_7)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Zero_7() const { return ___Zero_7; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Zero_7 = value;
		Il2CppCodeGenWriteBarrier((&___Zero_7), value);
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___One_8)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_One_8() const { return ___One_8; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___One_8 = value;
		Il2CppCodeGenWriteBarrier((&___One_8), value);
	}

	inline static int32_t get_offset_of_Two_9() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___Two_9)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Two_9() const { return ___Two_9; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Two_9() { return &___Two_9; }
	inline void set_Two_9(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Two_9 = value;
		Il2CppCodeGenWriteBarrier((&___Two_9), value);
	}

	inline static int32_t get_offset_of_Three_10() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___Three_10)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Three_10() const { return ___Three_10; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Three_10() { return &___Three_10; }
	inline void set_Three_10(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Three_10 = value;
		Il2CppCodeGenWriteBarrier((&___Three_10), value);
	}

	inline static int32_t get_offset_of_Four_11() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___Four_11)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Four_11() const { return ___Four_11; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Four_11() { return &___Four_11; }
	inline void set_Four_11(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Four_11 = value;
		Il2CppCodeGenWriteBarrier((&___Four_11), value);
	}

	inline static int32_t get_offset_of_Ten_12() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___Ten_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Ten_12() const { return ___Ten_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Ten_12() { return &___Ten_12; }
	inline void set_Ten_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Ten_12 = value;
		Il2CppCodeGenWriteBarrier((&___Ten_12), value);
	}

	inline static int32_t get_offset_of_BitLengthTable_13() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___BitLengthTable_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_BitLengthTable_13() const { return ___BitLengthTable_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_BitLengthTable_13() { return &___BitLengthTable_13; }
	inline void set_BitLengthTable_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___BitLengthTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___BitLengthTable_13), value);
	}

	inline static int32_t get_offset_of_radix2_18() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix2_18)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix2_18() const { return ___radix2_18; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix2_18() { return &___radix2_18; }
	inline void set_radix2_18(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix2_18 = value;
		Il2CppCodeGenWriteBarrier((&___radix2_18), value);
	}

	inline static int32_t get_offset_of_radix2E_19() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix2E_19)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix2E_19() const { return ___radix2E_19; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix2E_19() { return &___radix2E_19; }
	inline void set_radix2E_19(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix2E_19 = value;
		Il2CppCodeGenWriteBarrier((&___radix2E_19), value);
	}

	inline static int32_t get_offset_of_radix8_20() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix8_20)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix8_20() const { return ___radix8_20; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix8_20() { return &___radix8_20; }
	inline void set_radix8_20(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix8_20 = value;
		Il2CppCodeGenWriteBarrier((&___radix8_20), value);
	}

	inline static int32_t get_offset_of_radix8E_21() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix8E_21)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix8E_21() const { return ___radix8E_21; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix8E_21() { return &___radix8E_21; }
	inline void set_radix8E_21(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix8E_21 = value;
		Il2CppCodeGenWriteBarrier((&___radix8E_21), value);
	}

	inline static int32_t get_offset_of_radix10_22() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix10_22)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix10_22() const { return ___radix10_22; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix10_22() { return &___radix10_22; }
	inline void set_radix10_22(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix10_22 = value;
		Il2CppCodeGenWriteBarrier((&___radix10_22), value);
	}

	inline static int32_t get_offset_of_radix10E_23() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix10E_23)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix10E_23() const { return ___radix10E_23; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix10E_23() { return &___radix10E_23; }
	inline void set_radix10E_23(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix10E_23 = value;
		Il2CppCodeGenWriteBarrier((&___radix10E_23), value);
	}

	inline static int32_t get_offset_of_radix16_24() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix16_24)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix16_24() const { return ___radix16_24; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix16_24() { return &___radix16_24; }
	inline void set_radix16_24(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix16_24 = value;
		Il2CppCodeGenWriteBarrier((&___radix16_24), value);
	}

	inline static int32_t get_offset_of_radix16E_25() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___radix16E_25)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_radix16E_25() const { return ___radix16E_25; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_radix16E_25() { return &___radix16E_25; }
	inline void set_radix16E_25(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___radix16E_25 = value;
		Il2CppCodeGenWriteBarrier((&___radix16E_25), value);
	}

	inline static int32_t get_offset_of_RandomSource_26() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___RandomSource_26)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_RandomSource_26() const { return ___RandomSource_26; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_RandomSource_26() { return &___RandomSource_26; }
	inline void set_RandomSource_26(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___RandomSource_26 = value;
		Il2CppCodeGenWriteBarrier((&___RandomSource_26), value);
	}

	inline static int32_t get_offset_of_ExpWindowThresholds_27() { return static_cast<int32_t>(offsetof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields, ___ExpWindowThresholds_27)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ExpWindowThresholds_27() const { return ___ExpWindowThresholds_27; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ExpWindowThresholds_27() { return &___ExpWindowThresholds_27; }
	inline void set_ExpWindowThresholds_27(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ExpWindowThresholds_27 = value;
		Il2CppCodeGenWriteBarrier((&___ExpWindowThresholds_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_H
#ifndef ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#define ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractECLookupTable
struct  AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#ifndef ECALGORITHMS_T11E89F322556281C63AFEB0ABF2CD99398D53811_H
#define ECALGORITHMS_T11E89F322556281C63AFEB0ABF2CD99398D53811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECAlgorithms
struct  ECAlgorithms_t11E89F322556281C63AFEB0ABF2CD99398D53811  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECALGORITHMS_T11E89F322556281C63AFEB0ABF2CD99398D53811_H
#ifndef ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#define ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct  ECCurve_t6071986B64066FBF97315592BE971355FA584A39  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_field
	RuntimeObject* ___m_field_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_a
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_a_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_b
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_b_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_order
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_order_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_cofactor
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_cofactor_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_coord
	int32_t ___m_coord_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_endomorphism
	RuntimeObject* ___m_endomorphism_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_multiplier
	RuntimeObject* ___m_multiplier_15;

public:
	inline static int32_t get_offset_of_m_field_8() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_field_8)); }
	inline RuntimeObject* get_m_field_8() const { return ___m_field_8; }
	inline RuntimeObject** get_address_of_m_field_8() { return &___m_field_8; }
	inline void set_m_field_8(RuntimeObject* value)
	{
		___m_field_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_field_8), value);
	}

	inline static int32_t get_offset_of_m_a_9() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_a_9)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_a_9() const { return ___m_a_9; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_a_9() { return &___m_a_9; }
	inline void set_m_a_9(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_a_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_a_9), value);
	}

	inline static int32_t get_offset_of_m_b_10() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_b_10)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_b_10() const { return ___m_b_10; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_b_10() { return &___m_b_10; }
	inline void set_m_b_10(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_b_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_b_10), value);
	}

	inline static int32_t get_offset_of_m_order_11() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_order_11)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_order_11() const { return ___m_order_11; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_order_11() { return &___m_order_11; }
	inline void set_m_order_11(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_order_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_order_11), value);
	}

	inline static int32_t get_offset_of_m_cofactor_12() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_cofactor_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_cofactor_12() const { return ___m_cofactor_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_cofactor_12() { return &___m_cofactor_12; }
	inline void set_m_cofactor_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_cofactor_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_cofactor_12), value);
	}

	inline static int32_t get_offset_of_m_coord_13() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_coord_13)); }
	inline int32_t get_m_coord_13() const { return ___m_coord_13; }
	inline int32_t* get_address_of_m_coord_13() { return &___m_coord_13; }
	inline void set_m_coord_13(int32_t value)
	{
		___m_coord_13 = value;
	}

	inline static int32_t get_offset_of_m_endomorphism_14() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_endomorphism_14)); }
	inline RuntimeObject* get_m_endomorphism_14() const { return ___m_endomorphism_14; }
	inline RuntimeObject** get_address_of_m_endomorphism_14() { return &___m_endomorphism_14; }
	inline void set_m_endomorphism_14(RuntimeObject* value)
	{
		___m_endomorphism_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_14), value);
	}

	inline static int32_t get_offset_of_m_multiplier_15() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_multiplier_15)); }
	inline RuntimeObject* get_m_multiplier_15() const { return ___m_multiplier_15; }
	inline RuntimeObject** get_address_of_m_multiplier_15() { return &___m_multiplier_15; }
	inline void set_m_multiplier_15(RuntimeObject* value)
	{
		___m_multiplier_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_multiplier_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#ifndef CONFIG_T040FF96A3A9BD07BD318D7C066D46D76EFF581A6_H
#define CONFIG_T040FF96A3A9BD07BD318D7C066D46D76EFF581A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_Config
struct  Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_Config::outer
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___outer_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_Config::coord
	int32_t ___coord_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_Config::endomorphism
	RuntimeObject* ___endomorphism_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_Config::multiplier
	RuntimeObject* ___multiplier_3;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6, ___outer_0)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_outer_0() const { return ___outer_0; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_coord_1() { return static_cast<int32_t>(offsetof(Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6, ___coord_1)); }
	inline int32_t get_coord_1() const { return ___coord_1; }
	inline int32_t* get_address_of_coord_1() { return &___coord_1; }
	inline void set_coord_1(int32_t value)
	{
		___coord_1 = value;
	}

	inline static int32_t get_offset_of_endomorphism_2() { return static_cast<int32_t>(offsetof(Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6, ___endomorphism_2)); }
	inline RuntimeObject* get_endomorphism_2() const { return ___endomorphism_2; }
	inline RuntimeObject** get_address_of_endomorphism_2() { return &___endomorphism_2; }
	inline void set_endomorphism_2(RuntimeObject* value)
	{
		___endomorphism_2 = value;
		Il2CppCodeGenWriteBarrier((&___endomorphism_2), value);
	}

	inline static int32_t get_offset_of_multiplier_3() { return static_cast<int32_t>(offsetof(Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6, ___multiplier_3)); }
	inline RuntimeObject* get_multiplier_3() const { return ___multiplier_3; }
	inline RuntimeObject** get_address_of_multiplier_3() { return &___multiplier_3; }
	inline void set_multiplier_3(RuntimeObject* value)
	{
		___multiplier_3 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T040FF96A3A9BD07BD318D7C066D46D76EFF581A6_H
#ifndef ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#define ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct  ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#ifndef ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#define ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct  ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___m_curve_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_x
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_x_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_y
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_y_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_zs
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___m_zs_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_withCompression
	bool ___m_withCompression_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_preCompTable
	RuntimeObject* ___m_preCompTable_6;

public:
	inline static int32_t get_offset_of_m_curve_1() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_curve_1)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_m_curve_1() const { return ___m_curve_1; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_m_curve_1() { return &___m_curve_1; }
	inline void set_m_curve_1(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___m_curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_1), value);
	}

	inline static int32_t get_offset_of_m_x_2() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_x_2)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_x_2() const { return ___m_x_2; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_x_2() { return &___m_x_2; }
	inline void set_m_x_2(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_x_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_x_2), value);
	}

	inline static int32_t get_offset_of_m_y_3() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_y_3)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_y_3() const { return ___m_y_3; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_y_3() { return &___m_y_3; }
	inline void set_m_y_3(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_y_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_y_3), value);
	}

	inline static int32_t get_offset_of_m_zs_4() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_zs_4)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_m_zs_4() const { return ___m_zs_4; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_m_zs_4() { return &___m_zs_4; }
	inline void set_m_zs_4(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___m_zs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_zs_4), value);
	}

	inline static int32_t get_offset_of_m_withCompression_5() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_withCompression_5)); }
	inline bool get_m_withCompression_5() const { return ___m_withCompression_5; }
	inline bool* get_address_of_m_withCompression_5() { return &___m_withCompression_5; }
	inline void set_m_withCompression_5(bool value)
	{
		___m_withCompression_5 = value;
	}

	inline static int32_t get_offset_of_m_preCompTable_6() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_preCompTable_6)); }
	inline RuntimeObject* get_m_preCompTable_6() const { return ___m_preCompTable_6; }
	inline RuntimeObject** get_address_of_m_preCompTable_6() { return &___m_preCompTable_6; }
	inline void set_m_preCompTable_6(RuntimeObject* value)
	{
		___m_preCompTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompTable_6), value);
	}
};

struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::EMPTY_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___EMPTY_ZS_0;

public:
	inline static int32_t get_offset_of_EMPTY_ZS_0() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields, ___EMPTY_ZS_0)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_EMPTY_ZS_0() const { return ___EMPTY_ZS_0; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_EMPTY_ZS_0() { return &___EMPTY_ZS_0; }
	inline void set_EMPTY_ZS_0(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___EMPTY_ZS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_ZS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#ifndef VALIDITYCALLBACK_TC00272AC76262C63C252D0A6149A9C5CD424DBFD_H
#define VALIDITYCALLBACK_TC00272AC76262C63C252D0A6149A9C5CD424DBFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint_ValidityCallback
struct  ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint_ValidityCallback::m_outer
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_outer_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint_ValidityCallback::m_decompressed
	bool ___m_decompressed_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint_ValidityCallback::m_checkOrder
	bool ___m_checkOrder_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD, ___m_outer_0)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_outer_0() const { return ___m_outer_0; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_decompressed_1() { return static_cast<int32_t>(offsetof(ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD, ___m_decompressed_1)); }
	inline bool get_m_decompressed_1() const { return ___m_decompressed_1; }
	inline bool* get_address_of_m_decompressed_1() { return &___m_decompressed_1; }
	inline void set_m_decompressed_1(bool value)
	{
		___m_decompressed_1 = value;
	}

	inline static int32_t get_offset_of_m_checkOrder_2() { return static_cast<int32_t>(offsetof(ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD, ___m_checkOrder_2)); }
	inline bool get_m_checkOrder_2() const { return ___m_checkOrder_2; }
	inline bool* get_address_of_m_checkOrder_2() { return &___m_checkOrder_2; }
	inline void set_m_checkOrder_2(bool value)
	{
		___m_checkOrder_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDITYCALLBACK_TC00272AC76262C63C252D0A6149A9C5CD424DBFD_H
#ifndef LONGARRAY_T878F80FE9906AAC7F3471E383219621561440608_H
#define LONGARRAY_T878F80FE9906AAC7F3471E383219621561440608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray
struct  LongArray_t878F80FE9906AAC7F3471E383219621561440608  : public RuntimeObject
{
public:
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::m_ints
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___m_ints_7;

public:
	inline static int32_t get_offset_of_m_ints_7() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608, ___m_ints_7)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_m_ints_7() const { return ___m_ints_7; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_m_ints_7() { return &___m_ints_7; }
	inline void set_m_ints_7(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___m_ints_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ints_7), value);
	}
};

struct LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields
{
public:
	// System.UInt16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE2_TABLE
	UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* ___INTERLEAVE2_TABLE_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE3_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INTERLEAVE3_TABLE_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE4_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INTERLEAVE4_TABLE_2;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE5_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INTERLEAVE5_TABLE_3;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE7_TABLE
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___INTERLEAVE7_TABLE_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray::BitLengths
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___BitLengths_6;

public:
	inline static int32_t get_offset_of_INTERLEAVE2_TABLE_0() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields, ___INTERLEAVE2_TABLE_0)); }
	inline UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* get_INTERLEAVE2_TABLE_0() const { return ___INTERLEAVE2_TABLE_0; }
	inline UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E** get_address_of_INTERLEAVE2_TABLE_0() { return &___INTERLEAVE2_TABLE_0; }
	inline void set_INTERLEAVE2_TABLE_0(UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* value)
	{
		___INTERLEAVE2_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE2_TABLE_0), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE3_TABLE_1() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields, ___INTERLEAVE3_TABLE_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INTERLEAVE3_TABLE_1() const { return ___INTERLEAVE3_TABLE_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INTERLEAVE3_TABLE_1() { return &___INTERLEAVE3_TABLE_1; }
	inline void set_INTERLEAVE3_TABLE_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INTERLEAVE3_TABLE_1 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE3_TABLE_1), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE4_TABLE_2() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields, ___INTERLEAVE4_TABLE_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INTERLEAVE4_TABLE_2() const { return ___INTERLEAVE4_TABLE_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INTERLEAVE4_TABLE_2() { return &___INTERLEAVE4_TABLE_2; }
	inline void set_INTERLEAVE4_TABLE_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INTERLEAVE4_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE4_TABLE_2), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE5_TABLE_3() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields, ___INTERLEAVE5_TABLE_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INTERLEAVE5_TABLE_3() const { return ___INTERLEAVE5_TABLE_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INTERLEAVE5_TABLE_3() { return &___INTERLEAVE5_TABLE_3; }
	inline void set_INTERLEAVE5_TABLE_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INTERLEAVE5_TABLE_3 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE5_TABLE_3), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE7_TABLE_4() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields, ___INTERLEAVE7_TABLE_4)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_INTERLEAVE7_TABLE_4() const { return ___INTERLEAVE7_TABLE_4; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_INTERLEAVE7_TABLE_4() { return &___INTERLEAVE7_TABLE_4; }
	inline void set_INTERLEAVE7_TABLE_4(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___INTERLEAVE7_TABLE_4 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE7_TABLE_4), value);
	}

	inline static int32_t get_offset_of_BitLengths_6() { return static_cast<int32_t>(offsetof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields, ___BitLengths_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_BitLengths_6() const { return ___BitLengths_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_BitLengths_6() { return &___BitLengths_6; }
	inline void set_BitLengths_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___BitLengths_6 = value;
		Il2CppCodeGenWriteBarrier((&___BitLengths_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGARRAY_T878F80FE9906AAC7F3471E383219621561440608_H
#ifndef FINITEFIELDS_T9B91A06C88254F3E33D113F3A45313664AF353F8_H
#define FINITEFIELDS_T9B91A06C88254F3E33D113F3A45313664AF353F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.FiniteFields
struct  FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8  : public RuntimeObject
{
public:

public:
};

struct FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.FiniteFields::GF_2
	RuntimeObject* ___GF_2_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.FiniteFields::GF_3
	RuntimeObject* ___GF_3_1;

public:
	inline static int32_t get_offset_of_GF_2_0() { return static_cast<int32_t>(offsetof(FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8_StaticFields, ___GF_2_0)); }
	inline RuntimeObject* get_GF_2_0() const { return ___GF_2_0; }
	inline RuntimeObject** get_address_of_GF_2_0() { return &___GF_2_0; }
	inline void set_GF_2_0(RuntimeObject* value)
	{
		___GF_2_0 = value;
		Il2CppCodeGenWriteBarrier((&___GF_2_0), value);
	}

	inline static int32_t get_offset_of_GF_3_1() { return static_cast<int32_t>(offsetof(FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8_StaticFields, ___GF_3_1)); }
	inline RuntimeObject* get_GF_3_1() const { return ___GF_3_1; }
	inline RuntimeObject** get_address_of_GF_3_1() { return &___GF_3_1; }
	inline void set_GF_3_1(RuntimeObject* value)
	{
		___GF_3_1 = value;
		Il2CppCodeGenWriteBarrier((&___GF_3_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINITEFIELDS_T9B91A06C88254F3E33D113F3A45313664AF353F8_H
#ifndef GF2POLYNOMIAL_T0E753AD232DBC83D58438DC30DD26E17FD51F58F_H
#define GF2POLYNOMIAL_T0E753AD232DBC83D58438DC30DD26E17FD51F58F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.GF2Polynomial
struct  GF2Polynomial_t0E753AD232DBC83D58438DC30DD26E17FD51F58F  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.GF2Polynomial::exponents
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___exponents_0;

public:
	inline static int32_t get_offset_of_exponents_0() { return static_cast<int32_t>(offsetof(GF2Polynomial_t0E753AD232DBC83D58438DC30DD26E17FD51F58F, ___exponents_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_exponents_0() const { return ___exponents_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_exponents_0() { return &___exponents_0; }
	inline void set_exponents_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___exponents_0 = value;
		Il2CppCodeGenWriteBarrier((&___exponents_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GF2POLYNOMIAL_T0E753AD232DBC83D58438DC30DD26E17FD51F58F_H
#ifndef GENERICPOLYNOMIALEXTENSIONFIELD_T0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C_H
#define GENERICPOLYNOMIALEXTENSIONFIELD_T0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.GenericPolynomialExtensionField
struct  GenericPolynomialExtensionField_t0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.GenericPolynomialExtensionField::subfield
	RuntimeObject* ___subfield_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IPolynomial BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.GenericPolynomialExtensionField::minimalPolynomial
	RuntimeObject* ___minimalPolynomial_1;

public:
	inline static int32_t get_offset_of_subfield_0() { return static_cast<int32_t>(offsetof(GenericPolynomialExtensionField_t0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C, ___subfield_0)); }
	inline RuntimeObject* get_subfield_0() const { return ___subfield_0; }
	inline RuntimeObject** get_address_of_subfield_0() { return &___subfield_0; }
	inline void set_subfield_0(RuntimeObject* value)
	{
		___subfield_0 = value;
		Il2CppCodeGenWriteBarrier((&___subfield_0), value);
	}

	inline static int32_t get_offset_of_minimalPolynomial_1() { return static_cast<int32_t>(offsetof(GenericPolynomialExtensionField_t0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C, ___minimalPolynomial_1)); }
	inline RuntimeObject* get_minimalPolynomial_1() const { return ___minimalPolynomial_1; }
	inline RuntimeObject** get_address_of_minimalPolynomial_1() { return &___minimalPolynomial_1; }
	inline void set_minimalPolynomial_1(RuntimeObject* value)
	{
		___minimalPolynomial_1 = value;
		Il2CppCodeGenWriteBarrier((&___minimalPolynomial_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICPOLYNOMIALEXTENSIONFIELD_T0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C_H
#ifndef PRIMEFIELD_T4DB7E8264AE9D6AA7516390551D79F703CF401E4_H
#define PRIMEFIELD_T4DB7E8264AE9D6AA7516390551D79F703CF401E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.PrimeField
struct  PrimeField_t4DB7E8264AE9D6AA7516390551D79F703CF401E4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.PrimeField::characteristic
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___characteristic_0;

public:
	inline static int32_t get_offset_of_characteristic_0() { return static_cast<int32_t>(offsetof(PrimeField_t4DB7E8264AE9D6AA7516390551D79F703CF401E4, ___characteristic_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_characteristic_0() const { return ___characteristic_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_characteristic_0() { return &___characteristic_0; }
	inline void set_characteristic_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___characteristic_0 = value;
		Il2CppCodeGenWriteBarrier((&___characteristic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMEFIELD_T4DB7E8264AE9D6AA7516390551D79F703CF401E4_H
#ifndef PRIMES_T4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_H
#define PRIMES_T4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes
struct  Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F  : public RuntimeObject
{
public:

public:
};

struct Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes::SmallFactorLimit
	int32_t ___SmallFactorLimit_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes::One
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___One_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes::Two
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Two_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes::Three
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Three_3;

public:
	inline static int32_t get_offset_of_SmallFactorLimit_0() { return static_cast<int32_t>(offsetof(Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields, ___SmallFactorLimit_0)); }
	inline int32_t get_SmallFactorLimit_0() const { return ___SmallFactorLimit_0; }
	inline int32_t* get_address_of_SmallFactorLimit_0() { return &___SmallFactorLimit_0; }
	inline void set_SmallFactorLimit_0(int32_t value)
	{
		___SmallFactorLimit_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields, ___One_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_One_1() const { return ___One_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___One_1 = value;
		Il2CppCodeGenWriteBarrier((&___One_1), value);
	}

	inline static int32_t get_offset_of_Two_2() { return static_cast<int32_t>(offsetof(Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields, ___Two_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Two_2() const { return ___Two_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Two_2() { return &___Two_2; }
	inline void set_Two_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Two_2 = value;
		Il2CppCodeGenWriteBarrier((&___Two_2), value);
	}

	inline static int32_t get_offset_of_Three_3() { return static_cast<int32_t>(offsetof(Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields, ___Three_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Three_3() const { return ___Three_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Three_3() { return &___Three_3; }
	inline void set_Three_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Three_3 = value;
		Il2CppCodeGenWriteBarrier((&___Three_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMES_T4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_H
#ifndef MROUTPUT_TDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C_H
#define MROUTPUT_TDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_MROutput
struct  MROutput_tDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_MROutput::mProvablyComposite
	bool ___mProvablyComposite_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_MROutput::mFactor
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___mFactor_1;

public:
	inline static int32_t get_offset_of_mProvablyComposite_0() { return static_cast<int32_t>(offsetof(MROutput_tDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C, ___mProvablyComposite_0)); }
	inline bool get_mProvablyComposite_0() const { return ___mProvablyComposite_0; }
	inline bool* get_address_of_mProvablyComposite_0() { return &___mProvablyComposite_0; }
	inline void set_mProvablyComposite_0(bool value)
	{
		___mProvablyComposite_0 = value;
	}

	inline static int32_t get_offset_of_mFactor_1() { return static_cast<int32_t>(offsetof(MROutput_tDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C, ___mFactor_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_mFactor_1() const { return ___mFactor_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_mFactor_1() { return &___mFactor_1; }
	inline void set_mFactor_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___mFactor_1 = value;
		Il2CppCodeGenWriteBarrier((&___mFactor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MROUTPUT_TDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C_H
#ifndef STOUTPUT_T46CD7181B9FC3F219A7A997001EC67E48E9F5574_H
#define STOUTPUT_T46CD7181B9FC3F219A7A997001EC67E48E9F5574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_STOutput
struct  STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_STOutput::mPrime
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___mPrime_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_STOutput::mPrimeSeed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPrimeSeed_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Primes_STOutput::mPrimeGenCounter
	int32_t ___mPrimeGenCounter_2;

public:
	inline static int32_t get_offset_of_mPrime_0() { return static_cast<int32_t>(offsetof(STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574, ___mPrime_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_mPrime_0() const { return ___mPrime_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_mPrime_0() { return &___mPrime_0; }
	inline void set_mPrime_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___mPrime_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPrime_0), value);
	}

	inline static int32_t get_offset_of_mPrimeSeed_1() { return static_cast<int32_t>(offsetof(STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574, ___mPrimeSeed_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPrimeSeed_1() const { return ___mPrimeSeed_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPrimeSeed_1() { return &___mPrimeSeed_1; }
	inline void set_mPrimeSeed_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPrimeSeed_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimeSeed_1), value);
	}

	inline static int32_t get_offset_of_mPrimeGenCounter_2() { return static_cast<int32_t>(offsetof(STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574, ___mPrimeGenCounter_2)); }
	inline int32_t get_mPrimeGenCounter_2() const { return ___mPrimeGenCounter_2; }
	inline int32_t* get_address_of_mPrimeGenCounter_2() { return &___mPrimeGenCounter_2; }
	inline void set_mPrimeGenCounter_2(int32_t value)
	{
		___mPrimeGenCounter_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOUTPUT_T46CD7181B9FC3F219A7A997001EC67E48E9F5574_H
#ifndef INTERLEAVE_TA373474BEE1BC6785262AE17E15CCE733134DDFE_H
#define INTERLEAVE_TA373474BEE1BC6785262AE17E15CCE733134DDFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Interleave
struct  Interleave_tA373474BEE1BC6785262AE17E15CCE733134DDFE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERLEAVE_TA373474BEE1BC6785262AE17E15CCE733134DDFE_H
#ifndef MOD_T6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_H
#define MOD_T6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Mod
struct  Mod_t6F0DA656D37CECD55F8BD60C3DC7215057ECDC68  : public RuntimeObject
{
public:

public:
};

struct Mod_t6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Mod::RandomSource
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___RandomSource_0;

public:
	inline static int32_t get_offset_of_RandomSource_0() { return static_cast<int32_t>(offsetof(Mod_t6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_StaticFields, ___RandomSource_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_RandomSource_0() const { return ___RandomSource_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_RandomSource_0() { return &___RandomSource_0; }
	inline void set_RandomSource_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___RandomSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___RandomSource_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOD_T6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_H
#ifndef NAT_TBA1B4ED8AE03FC99A5113B6A556531A0405A80E8_H
#define NAT_TBA1B4ED8AE03FC99A5113B6A556531A0405A80E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat
struct  Nat_tBA1B4ED8AE03FC99A5113B6A556531A0405A80E8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT_TBA1B4ED8AE03FC99A5113B6A556531A0405A80E8_H
#ifndef NAT128_TFB9E14007EA78309ADB34B9F6B3C3E78F4B6D5DF_H
#define NAT128_TFB9E14007EA78309ADB34B9F6B3C3E78F4B6D5DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat128
struct  Nat128_tFB9E14007EA78309ADB34B9F6B3C3E78F4B6D5DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT128_TFB9E14007EA78309ADB34B9F6B3C3E78F4B6D5DF_H
#ifndef NAT160_TADDCBC1B4AFE651800677D49921F198DE061B116_H
#define NAT160_TADDCBC1B4AFE651800677D49921F198DE061B116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat160
struct  Nat160_tADDCBC1B4AFE651800677D49921F198DE061B116  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT160_TADDCBC1B4AFE651800677D49921F198DE061B116_H
#ifndef NAT192_TC116EB7A2010B82493A999B67D6D349787347C05_H
#define NAT192_TC116EB7A2010B82493A999B67D6D349787347C05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat192
struct  Nat192_tC116EB7A2010B82493A999B67D6D349787347C05  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT192_TC116EB7A2010B82493A999B67D6D349787347C05_H
#ifndef NAT224_T28E36542839A9EE392CD56E9A6BFC8BA95E85CC0_H
#define NAT224_T28E36542839A9EE392CD56E9A6BFC8BA95E85CC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat224
struct  Nat224_t28E36542839A9EE392CD56E9A6BFC8BA95E85CC0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT224_T28E36542839A9EE392CD56E9A6BFC8BA95E85CC0_H
#ifndef NAT256_TDAFE94DD1BD93EC69650B7E8C70D88A7F772E83B_H
#define NAT256_TDAFE94DD1BD93EC69650B7E8C70D88A7F772E83B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat256
struct  Nat256_tDAFE94DD1BD93EC69650B7E8C70D88A7F772E83B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT256_TDAFE94DD1BD93EC69650B7E8C70D88A7F772E83B_H
#ifndef NAT320_TEE477C41E23A3D78467EF55F2652676749127DDA_H
#define NAT320_TEE477C41E23A3D78467EF55F2652676749127DDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat320
struct  Nat320_tEE477C41E23A3D78467EF55F2652676749127DDA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT320_TEE477C41E23A3D78467EF55F2652676749127DDA_H
#ifndef NAT384_T0488ECFFD96A0DC33C1AC2455ADB66673274D100_H
#define NAT384_T0488ECFFD96A0DC33C1AC2455ADB66673274D100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat384
struct  Nat384_t0488ECFFD96A0DC33C1AC2455ADB66673274D100  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT384_T0488ECFFD96A0DC33C1AC2455ADB66673274D100_H
#ifndef NAT448_T78C4572FF9544EFF52207C50FFB75B3472D30756_H
#define NAT448_T78C4572FF9544EFF52207C50FFB75B3472D30756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat448
struct  Nat448_t78C4572FF9544EFF52207C50FFB75B3472D30756  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT448_T78C4572FF9544EFF52207C50FFB75B3472D30756_H
#ifndef NAT512_TC7DE3A98ED3CFE6DF6E8F032C2BE455CD17677B4_H
#define NAT512_TC7DE3A98ED3CFE6DF6E8F032C2BE455CD17677B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat512
struct  Nat512_tC7DE3A98ED3CFE6DF6E8F032C2BE455CD17677B4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT512_TC7DE3A98ED3CFE6DF6E8F032C2BE455CD17677B4_H
#ifndef NAT576_TD271E6DD50AA86C81D535554E052F43E56D839FA_H
#define NAT576_TD271E6DD50AA86C81D535554E052F43E56D839FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Raw.Nat576
struct  Nat576_tD271E6DD50AA86C81D535554E052F43E56D839FA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT576_TD271E6DD50AA86C81D535554E052F43E56D839FA_H
#ifndef BASICOCSPRESPGENERATOR_T2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546_H
#define BASICOCSPRESPGENERATOR_T2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator
struct  BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator::list
	RuntimeObject* ___list_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator::responseExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___responseExtensions_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RespID BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator::responderID
	RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53 * ___responderID_2;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_responseExtensions_1() { return static_cast<int32_t>(offsetof(BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546, ___responseExtensions_1)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_responseExtensions_1() const { return ___responseExtensions_1; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_responseExtensions_1() { return &___responseExtensions_1; }
	inline void set_responseExtensions_1(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___responseExtensions_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseExtensions_1), value);
	}

	inline static int32_t get_offset_of_responderID_2() { return static_cast<int32_t>(offsetof(BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546, ___responderID_2)); }
	inline RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53 * get_responderID_2() const { return ___responderID_2; }
	inline RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53 ** get_address_of_responderID_2() { return &___responderID_2; }
	inline void set_responderID_2(RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53 * value)
	{
		___responderID_2 = value;
		Il2CppCodeGenWriteBarrier((&___responderID_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICOCSPRESPGENERATOR_T2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546_H
#ifndef RESPONSEOBJECT_T3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4_H
#define RESPONSEOBJECT_T3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator_ResponseObject
struct  ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateID BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator_ResponseObject::certId
	CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA * ___certId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator_ResponseObject::certStatus
	CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 * ___certStatus_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator_ResponseObject::thisUpdate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___thisUpdate_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator_ResponseObject::nextUpdate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___nextUpdate_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspRespGenerator_ResponseObject::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_4;

public:
	inline static int32_t get_offset_of_certId_0() { return static_cast<int32_t>(offsetof(ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4, ___certId_0)); }
	inline CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA * get_certId_0() const { return ___certId_0; }
	inline CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA ** get_address_of_certId_0() { return &___certId_0; }
	inline void set_certId_0(CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA * value)
	{
		___certId_0 = value;
		Il2CppCodeGenWriteBarrier((&___certId_0), value);
	}

	inline static int32_t get_offset_of_certStatus_1() { return static_cast<int32_t>(offsetof(ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4, ___certStatus_1)); }
	inline CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 * get_certStatus_1() const { return ___certStatus_1; }
	inline CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 ** get_address_of_certStatus_1() { return &___certStatus_1; }
	inline void set_certStatus_1(CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 * value)
	{
		___certStatus_1 = value;
		Il2CppCodeGenWriteBarrier((&___certStatus_1), value);
	}

	inline static int32_t get_offset_of_thisUpdate_2() { return static_cast<int32_t>(offsetof(ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4, ___thisUpdate_2)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_thisUpdate_2() const { return ___thisUpdate_2; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_thisUpdate_2() { return &___thisUpdate_2; }
	inline void set_thisUpdate_2(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___thisUpdate_2 = value;
		Il2CppCodeGenWriteBarrier((&___thisUpdate_2), value);
	}

	inline static int32_t get_offset_of_nextUpdate_3() { return static_cast<int32_t>(offsetof(ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4, ___nextUpdate_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_nextUpdate_3() const { return ___nextUpdate_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_nextUpdate_3() { return &___nextUpdate_3; }
	inline void set_nextUpdate_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___nextUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___nextUpdate_3), value);
	}

	inline static int32_t get_offset_of_extensions_4() { return static_cast<int32_t>(offsetof(ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4, ___extensions_4)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_4() const { return ___extensions_4; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_4() { return &___extensions_4; }
	inline void set_extensions_4(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_4 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEOBJECT_T3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4_H
#ifndef CERTIFICATEID_T15EDD341582906F7ED7C1A5858B032D639E45BAA_H
#define CERTIFICATEID_T15EDD341582906F7ED7C1A5858B032D639E45BAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateID
struct  CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateID::id
	CertID_tD10877523D531F998848FBD48D854308134F2918 * ___id_1;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA, ___id_1)); }
	inline CertID_tD10877523D531F998848FBD48D854308134F2918 * get_id_1() const { return ___id_1; }
	inline CertID_tD10877523D531F998848FBD48D854308134F2918 ** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(CertID_tD10877523D531F998848FBD48D854308134F2918 * value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEID_T15EDD341582906F7ED7C1A5858B032D639E45BAA_H
#ifndef CERTIFICATESTATUS_T1CEB114499D83E64B84058693FE7C47DD31A3B16_H
#define CERTIFICATESTATUS_T1CEB114499D83E64B84058693FE7C47DD31A3B16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateStatus
struct  CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16  : public RuntimeObject
{
public:

public:
};

struct CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateStatus::Good
	CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16 * ___Good_0;

public:
	inline static int32_t get_offset_of_Good_0() { return static_cast<int32_t>(offsetof(CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16_StaticFields, ___Good_0)); }
	inline CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16 * get_Good_0() const { return ___Good_0; }
	inline CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16 ** get_address_of_Good_0() { return &___Good_0; }
	inline void set_Good_0(CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16 * value)
	{
		___Good_0 = value;
		Il2CppCodeGenWriteBarrier((&___Good_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESTATUS_T1CEB114499D83E64B84058693FE7C47DD31A3B16_H
#ifndef OCSPRESPGENERATOR_T61091485652FA692A350F567B8B7E7F8C8C06981_H
#define OCSPRESPGENERATOR_T61091485652FA692A350F567B8B7E7F8C8C06981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OCSPRespGenerator
struct  OCSPRespGenerator_t61091485652FA692A350F567B8B7E7F8C8C06981  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPGENERATOR_T61091485652FA692A350F567B8B7E7F8C8C06981_H
#ifndef OCSPREQGENERATOR_TF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60_H
#define OCSPREQGENERATOR_TF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator
struct  OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator::list
	RuntimeObject* ___list_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator::requestorName
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___requestorName_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator::requestExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___requestExtensions_2;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_requestorName_1() { return static_cast<int32_t>(offsetof(OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60, ___requestorName_1)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_requestorName_1() const { return ___requestorName_1; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_requestorName_1() { return &___requestorName_1; }
	inline void set_requestorName_1(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___requestorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___requestorName_1), value);
	}

	inline static int32_t get_offset_of_requestExtensions_2() { return static_cast<int32_t>(offsetof(OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60, ___requestExtensions_2)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_requestExtensions_2() const { return ___requestExtensions_2; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_requestExtensions_2() { return &___requestExtensions_2; }
	inline void set_requestExtensions_2(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___requestExtensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___requestExtensions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPREQGENERATOR_TF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60_H
#ifndef REQUESTOBJECT_T3D7261AFDB1C76702114959E238215DBD73CD897_H
#define REQUESTOBJECT_T3D7261AFDB1C76702114959E238215DBD73CD897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator_RequestObject
struct  RequestObject_t3D7261AFDB1C76702114959E238215DBD73CD897  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.CertificateID BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator_RequestObject::certId
	CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA * ___certId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReqGenerator_RequestObject::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_1;

public:
	inline static int32_t get_offset_of_certId_0() { return static_cast<int32_t>(offsetof(RequestObject_t3D7261AFDB1C76702114959E238215DBD73CD897, ___certId_0)); }
	inline CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA * get_certId_0() const { return ___certId_0; }
	inline CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA ** get_address_of_certId_0() { return &___certId_0; }
	inline void set_certId_0(CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA * value)
	{
		___certId_0 = value;
		Il2CppCodeGenWriteBarrier((&___certId_0), value);
	}

	inline static int32_t get_offset_of_extensions_1() { return static_cast<int32_t>(offsetof(RequestObject_t3D7261AFDB1C76702114959E238215DBD73CD897, ___extensions_1)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_1() const { return ___extensions_1; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_1() { return &___extensions_1; }
	inline void set_extensions_1(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTOBJECT_T3D7261AFDB1C76702114959E238215DBD73CD897_H
#ifndef OCSPRESP_TEAE3E79062DD83DE1B850EE7D926754790F08A50_H
#define OCSPRESP_TEAE3E79062DD83DE1B850EE7D926754790F08A50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspResp
struct  OcspResp_tEAE3E79062DD83DE1B850EE7D926754790F08A50  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponse BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspResp::resp
	OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4 * ___resp_0;

public:
	inline static int32_t get_offset_of_resp_0() { return static_cast<int32_t>(offsetof(OcspResp_tEAE3E79062DD83DE1B850EE7D926754790F08A50, ___resp_0)); }
	inline OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4 * get_resp_0() const { return ___resp_0; }
	inline OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4 ** get_address_of_resp_0() { return &___resp_0; }
	inline void set_resp_0(OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4 * value)
	{
		___resp_0 = value;
		Il2CppCodeGenWriteBarrier((&___resp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESP_TEAE3E79062DD83DE1B850EE7D926754790F08A50_H
#ifndef OCSPRESPSTATUS_T8B857323CD2F1CDFD2A758D37774419F16AA6C39_H
#define OCSPRESPSTATUS_T8B857323CD2F1CDFD2A758D37774419F16AA6C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspRespStatus
struct  OcspRespStatus_t8B857323CD2F1CDFD2A758D37774419F16AA6C39  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPSTATUS_T8B857323CD2F1CDFD2A758D37774419F16AA6C39_H
#ifndef OCSPUTILITIES_T3B5CEB42937A585213DAF5D35881BE2119E596B5_H
#define OCSPUTILITIES_T3B5CEB42937A585213DAF5D35881BE2119E596B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspUtilities
struct  OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5  : public RuntimeObject
{
public:

public:
};

struct OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspUtilities::oids
	RuntimeObject* ___oids_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspUtilities::noParams
	RuntimeObject* ___noParams_2;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_oids_1() { return static_cast<int32_t>(offsetof(OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields, ___oids_1)); }
	inline RuntimeObject* get_oids_1() const { return ___oids_1; }
	inline RuntimeObject** get_address_of_oids_1() { return &___oids_1; }
	inline void set_oids_1(RuntimeObject* value)
	{
		___oids_1 = value;
		Il2CppCodeGenWriteBarrier((&___oids_1), value);
	}

	inline static int32_t get_offset_of_noParams_2() { return static_cast<int32_t>(offsetof(OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields, ___noParams_2)); }
	inline RuntimeObject* get_noParams_2() const { return ___noParams_2; }
	inline RuntimeObject** get_address_of_noParams_2() { return &___noParams_2; }
	inline void set_noParams_2(RuntimeObject* value)
	{
		___noParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___noParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPUTILITIES_T3B5CEB42937A585213DAF5D35881BE2119E596B5_H
#ifndef RESPID_T3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53_H
#define RESPID_T3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RespID
struct  RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RespID::id
	ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53, ___id_0)); }
	inline ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * get_id_0() const { return ___id_0; }
	inline ResponderID_t26B99AFE9E4E05948990F461880598D304592346 ** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPID_T3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53_H
#ifndef DIGESTUTILITIES_TD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_H
#define DIGESTUTILITIES_TD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.DigestUtilities
struct  DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491  : public RuntimeObject
{
public:

public:
};

struct DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.DigestUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.DigestUtilities::oids
	RuntimeObject* ___oids_1;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_oids_1() { return static_cast<int32_t>(offsetof(DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_StaticFields, ___oids_1)); }
	inline RuntimeObject* get_oids_1() const { return ___oids_1; }
	inline RuntimeObject** get_address_of_oids_1() { return &___oids_1; }
	inline void set_oids_1(RuntimeObject* value)
	{
		___oids_1 = value;
		Il2CppCodeGenWriteBarrier((&___oids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTUTILITIES_TD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_H
#ifndef DOTNETUTILITIES_TF7BA78D7CFF5F05B7E2AFE864ABF54D3499AA086_H
#define DOTNETUTILITIES_TF7BA78D7CFF5F05B7E2AFE864ABF54D3499AA086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.DotNetUtilities
struct  DotNetUtilities_tF7BA78D7CFF5F05B7E2AFE864ABF54D3499AA086  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTNETUTILITIES_TF7BA78D7CFF5F05B7E2AFE864ABF54D3499AA086_H
#ifndef GENERATORUTILITIES_T114A04F4672440A48260E104943C652B34206137_H
#define GENERATORUTILITIES_T114A04F4672440A48260E104943C652B34206137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.GeneratorUtilities
struct  GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137  : public RuntimeObject
{
public:

public:
};

struct GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.GeneratorUtilities::kgAlgorithms
	RuntimeObject* ___kgAlgorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.GeneratorUtilities::kpgAlgorithms
	RuntimeObject* ___kpgAlgorithms_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.GeneratorUtilities::defaultKeySizes
	RuntimeObject* ___defaultKeySizes_2;

public:
	inline static int32_t get_offset_of_kgAlgorithms_0() { return static_cast<int32_t>(offsetof(GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields, ___kgAlgorithms_0)); }
	inline RuntimeObject* get_kgAlgorithms_0() const { return ___kgAlgorithms_0; }
	inline RuntimeObject** get_address_of_kgAlgorithms_0() { return &___kgAlgorithms_0; }
	inline void set_kgAlgorithms_0(RuntimeObject* value)
	{
		___kgAlgorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___kgAlgorithms_0), value);
	}

	inline static int32_t get_offset_of_kpgAlgorithms_1() { return static_cast<int32_t>(offsetof(GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields, ___kpgAlgorithms_1)); }
	inline RuntimeObject* get_kpgAlgorithms_1() const { return ___kpgAlgorithms_1; }
	inline RuntimeObject** get_address_of_kpgAlgorithms_1() { return &___kpgAlgorithms_1; }
	inline void set_kpgAlgorithms_1(RuntimeObject* value)
	{
		___kpgAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___kpgAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_defaultKeySizes_2() { return static_cast<int32_t>(offsetof(GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields, ___defaultKeySizes_2)); }
	inline RuntimeObject* get_defaultKeySizes_2() const { return ___defaultKeySizes_2; }
	inline RuntimeObject** get_address_of_defaultKeySizes_2() { return &___defaultKeySizes_2; }
	inline void set_defaultKeySizes_2(RuntimeObject* value)
	{
		___defaultKeySizes_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultKeySizes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATORUTILITIES_T114A04F4672440A48260E104943C652B34206137_H
#ifndef MACUTILITIES_TBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_H
#define MACUTILITIES_TBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.MacUtilities
struct  MacUtilities_tBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9  : public RuntimeObject
{
public:

public:
};

struct MacUtilities_tBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.MacUtilities::algorithms
	RuntimeObject* ___algorithms_0;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(MacUtilities_tBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACUTILITIES_TBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_H
#ifndef PARAMETERUTILITIES_T06ABACFB71DBD0F86F16299DD5F6236FAE272217_H
#define PARAMETERUTILITIES_T06ABACFB71DBD0F86F16299DD5F6236FAE272217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.ParameterUtilities
struct  ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217  : public RuntimeObject
{
public:

public:
};

struct ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.ParameterUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.ParameterUtilities::basicIVSizes
	RuntimeObject* ___basicIVSizes_1;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_basicIVSizes_1() { return static_cast<int32_t>(offsetof(ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217_StaticFields, ___basicIVSizes_1)); }
	inline RuntimeObject* get_basicIVSizes_1() const { return ___basicIVSizes_1; }
	inline RuntimeObject** get_address_of_basicIVSizes_1() { return &___basicIVSizes_1; }
	inline void set_basicIVSizes_1(RuntimeObject* value)
	{
		___basicIVSizes_1 = value;
		Il2CppCodeGenWriteBarrier((&___basicIVSizes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERUTILITIES_T06ABACFB71DBD0F86F16299DD5F6236FAE272217_H
#ifndef PBEUTILITIES_T1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_H
#define PBEUTILITIES_T1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PbeUtilities
struct  PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3  : public RuntimeObject
{
public:

public:
};

struct PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PbeUtilities::algorithms
	RuntimeObject* ___algorithms_4;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PbeUtilities::algorithmType
	RuntimeObject* ___algorithmType_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PbeUtilities::oids
	RuntimeObject* ___oids_6;

public:
	inline static int32_t get_offset_of_algorithms_4() { return static_cast<int32_t>(offsetof(PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields, ___algorithms_4)); }
	inline RuntimeObject* get_algorithms_4() const { return ___algorithms_4; }
	inline RuntimeObject** get_address_of_algorithms_4() { return &___algorithms_4; }
	inline void set_algorithms_4(RuntimeObject* value)
	{
		___algorithms_4 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_4), value);
	}

	inline static int32_t get_offset_of_algorithmType_5() { return static_cast<int32_t>(offsetof(PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields, ___algorithmType_5)); }
	inline RuntimeObject* get_algorithmType_5() const { return ___algorithmType_5; }
	inline RuntimeObject** get_address_of_algorithmType_5() { return &___algorithmType_5; }
	inline void set_algorithmType_5(RuntimeObject* value)
	{
		___algorithmType_5 = value;
		Il2CppCodeGenWriteBarrier((&___algorithmType_5), value);
	}

	inline static int32_t get_offset_of_oids_6() { return static_cast<int32_t>(offsetof(PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields, ___oids_6)); }
	inline RuntimeObject* get_oids_6() const { return ___oids_6; }
	inline RuntimeObject** get_address_of_oids_6() { return &___oids_6; }
	inline void set_oids_6(RuntimeObject* value)
	{
		___oids_6 = value;
		Il2CppCodeGenWriteBarrier((&___oids_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBEUTILITIES_T1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_H
#ifndef PRIVATEKEYFACTORY_T90D5BA0ECED77B8C83C3F9ECB915D44D3AD50555_H
#define PRIVATEKEYFACTORY_T90D5BA0ECED77B8C83C3F9ECB915D44D3AD50555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PrivateKeyFactory
struct  PrivateKeyFactory_t90D5BA0ECED77B8C83C3F9ECB915D44D3AD50555  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYFACTORY_T90D5BA0ECED77B8C83C3F9ECB915D44D3AD50555_H
#ifndef PUBLICKEYFACTORY_T2D224D40667D7FB1A484A2B1D172015C9F8344F0_H
#define PUBLICKEYFACTORY_T2D224D40667D7FB1A484A2B1D172015C9F8344F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PublicKeyFactory
struct  PublicKeyFactory_t2D224D40667D7FB1A484A2B1D172015C9F8344F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEYFACTORY_T2D224D40667D7FB1A484A2B1D172015C9F8344F0_H
#ifndef SIGNERUTILITIES_TD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_H
#define SIGNERUTILITIES_TD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SignerUtilities
struct  SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B  : public RuntimeObject
{
public:

public:
};

struct SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SignerUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SignerUtilities::oids
	RuntimeObject* ___oids_1;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_oids_1() { return static_cast<int32_t>(offsetof(SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_StaticFields, ___oids_1)); }
	inline RuntimeObject* get_oids_1() const { return ___oids_1; }
	inline RuntimeObject** get_address_of_oids_1() { return &___oids_1; }
	inline void set_oids_1(RuntimeObject* value)
	{
		___oids_1 = value;
		Il2CppCodeGenWriteBarrier((&___oids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERUTILITIES_TD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_H
#ifndef WRAPPERUTILITIES_T7CCFA86B917628908B41BBFF3459D7DF9F928FB3_H
#define WRAPPERUTILITIES_T7CCFA86B917628908B41BBFF3459D7DF9F928FB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities
struct  WrapperUtilities_t7CCFA86B917628908B41BBFF3459D7DF9F928FB3  : public RuntimeObject
{
public:

public:
};

struct WrapperUtilities_t7CCFA86B917628908B41BBFF3459D7DF9F928FB3_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities::algorithms
	RuntimeObject* ___algorithms_0;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(WrapperUtilities_t7CCFA86B917628908B41BBFF3459D7DF9F928FB3_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERUTILITIES_T7CCFA86B917628908B41BBFF3459D7DF9F928FB3_H
#ifndef BUFFEREDCIPHERWRAPPER_TFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6_H
#define BUFFEREDCIPHERWRAPPER_TFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities_BufferedCipherWrapper
struct  BufferedCipherWrapper_tFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities_BufferedCipherWrapper::cipher
	RuntimeObject* ___cipher_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities_BufferedCipherWrapper::forWrapping
	bool ___forWrapping_1;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(BufferedCipherWrapper_tFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6, ___cipher_0)); }
	inline RuntimeObject* get_cipher_0() const { return ___cipher_0; }
	inline RuntimeObject** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(RuntimeObject* value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_forWrapping_1() { return static_cast<int32_t>(offsetof(BufferedCipherWrapper_tFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6, ___forWrapping_1)); }
	inline bool get_forWrapping_1() const { return ___forWrapping_1; }
	inline bool* get_address_of_forWrapping_1() { return &___forWrapping_1; }
	inline void set_forWrapping_1(bool value)
	{
		___forWrapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDCIPHERWRAPPER_TFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6_H
#ifndef X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#define X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509ExtensionBase
struct  X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#define RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Random
struct  Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_0;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_1;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SeedArray_2;

public:
	inline static int32_t get_offset_of_inext_0() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inext_0)); }
	inline int32_t get_inext_0() const { return ___inext_0; }
	inline int32_t* get_address_of_inext_0() { return &___inext_0; }
	inline void set_inext_0(int32_t value)
	{
		___inext_0 = value;
	}

	inline static int32_t get_offset_of_inextp_1() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inextp_1)); }
	inline int32_t get_inextp_1() const { return ___inextp_1; }
	inline int32_t* get_address_of_inextp_1() { return &___inextp_1; }
	inline void set_inextp_1(int32_t value)
	{
		___inextp_1 = value;
	}

	inline static int32_t get_offset_of_SeedArray_2() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___SeedArray_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SeedArray_2() const { return ___SeedArray_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SeedArray_2() { return &___SeedArray_2; }
	inline void set_SeedArray_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SeedArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___SeedArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ABSTRACTF2MCURVE_T6B0E18B7670A83808724BFAB503DA1B72EE06187_H
#define ABSTRACTF2MCURVE_T6B0E18B7670A83808724BFAB503DA1B72EE06187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mCurve
struct  AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187  : public ECCurve_t6071986B64066FBF97315592BE971355FA584A39
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mCurve::si
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___si_16;

public:
	inline static int32_t get_offset_of_si_16() { return static_cast<int32_t>(offsetof(AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187, ___si_16)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_si_16() const { return ___si_16; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_si_16() { return &___si_16; }
	inline void set_si_16(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___si_16 = value;
		Il2CppCodeGenWriteBarrier((&___si_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MCURVE_T6B0E18B7670A83808724BFAB503DA1B72EE06187_H
#ifndef ABSTRACTF2MFIELDELEMENT_T7781A419D272BDEF84C6D55FD05F4A7413CD2C50_H
#define ABSTRACTF2MFIELDELEMENT_T7781A419D272BDEF84C6D55FD05F4A7413CD2C50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mFieldElement
struct  AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50  : public ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MFIELDELEMENT_T7781A419D272BDEF84C6D55FD05F4A7413CD2C50_H
#ifndef ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#define ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpCurve
struct  AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96  : public ECCurve_t6071986B64066FBF97315592BE971355FA584A39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#ifndef ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#define ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpFieldElement
struct  AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A  : public ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#ifndef DEFAULTLOOKUPTABLE_T58033721A4617DCEF6BE2C0727CC7D79D008B974_H
#define DEFAULTLOOKUPTABLE_T58033721A4617DCEF6BE2C0727CC7D79D008B974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_DefaultLookupTable
struct  DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_DefaultLookupTable::m_outer
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___m_outer_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_DefaultLookupTable::m_table
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve_DefaultLookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974, ___m_outer_0)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_m_outer_0() const { return ___m_outer_0; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974, ___m_table_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_table_1() const { return ___m_table_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTLOOKUPTABLE_T58033721A4617DCEF6BE2C0727CC7D79D008B974_H
#ifndef ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#define ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointBase
struct  ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB  : public ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#ifndef DEFAULTF2MLOOKUPTABLE_T8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2_H
#define DEFAULTF2MLOOKUPTABLE_T8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve_DefaultF2mLookupTable
struct  DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve_DefaultF2mLookupTable::m_outer
	F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74 * ___m_outer_0;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve_DefaultF2mLookupTable::m_table
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve_DefaultF2mLookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2, ___m_outer_0)); }
	inline F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74 * get_m_outer_0() const { return ___m_outer_0; }
	inline F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2, ___m_table_1)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_m_table_1() const { return ___m_table_1; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTF2MLOOKUPTABLE_T8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2_H
#ifndef BASICOCSPRESP_T2A3C88955A8368F95ADEEC5586D3F27341A6000D_H
#define BASICOCSPRESP_T2A3C88955A8368F95ADEEC5586D3F27341A6000D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspResp
struct  BasicOcspResp_t2A3C88955A8368F95ADEEC5586D3F27341A6000D  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspResp::resp
	BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED * ___resp_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.BasicOcspResp::data
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * ___data_1;

public:
	inline static int32_t get_offset_of_resp_0() { return static_cast<int32_t>(offsetof(BasicOcspResp_t2A3C88955A8368F95ADEEC5586D3F27341A6000D, ___resp_0)); }
	inline BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED * get_resp_0() const { return ___resp_0; }
	inline BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED ** get_address_of_resp_0() { return &___resp_0; }
	inline void set_resp_0(BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED * value)
	{
		___resp_0 = value;
		Il2CppCodeGenWriteBarrier((&___resp_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(BasicOcspResp_t2A3C88955A8368F95ADEEC5586D3F27341A6000D, ___data_1)); }
	inline ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * get_data_1() const { return ___data_1; }
	inline ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICOCSPRESP_T2A3C88955A8368F95ADEEC5586D3F27341A6000D_H
#ifndef OCSCPRESPSTATUS_T6A6E12E6999F48E76CAAB2E7D748D7847328397D_H
#define OCSCPRESPSTATUS_T6A6E12E6999F48E76CAAB2E7D748D7847328397D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcscpRespStatus
struct  OcscpRespStatus_t6A6E12E6999F48E76CAAB2E7D748D7847328397D  : public OcspRespStatus_t8B857323CD2F1CDFD2A758D37774419F16AA6C39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSCPRESPSTATUS_T6A6E12E6999F48E76CAAB2E7D748D7847328397D_H
#ifndef OCSPEXCEPTION_TB71E32252369B949E00E8F7BA4EE09BDEE398E40_H
#define OCSPEXCEPTION_TB71E32252369B949E00E8F7BA4EE09BDEE398E40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspException
struct  OcspException_tB71E32252369B949E00E8F7BA4EE09BDEE398E40  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPEXCEPTION_TB71E32252369B949E00E8F7BA4EE09BDEE398E40_H
#ifndef OCSPREQ_T4EAFC3977FEA343F2B62DF99F5A3072A74962997_H
#define OCSPREQ_T4EAFC3977FEA343F2B62DF99F5A3072A74962997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReq
struct  OcspReq_t4EAFC3977FEA343F2B62DF99F5A3072A74962997  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.OcspReq::req
	OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C * ___req_0;

public:
	inline static int32_t get_offset_of_req_0() { return static_cast<int32_t>(offsetof(OcspReq_t4EAFC3977FEA343F2B62DF99F5A3072A74962997, ___req_0)); }
	inline OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C * get_req_0() const { return ___req_0; }
	inline OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C ** get_address_of_req_0() { return &___req_0; }
	inline void set_req_0(OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C * value)
	{
		___req_0 = value;
		Il2CppCodeGenWriteBarrier((&___req_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPREQ_T4EAFC3977FEA343F2B62DF99F5A3072A74962997_H
#ifndef REQ_T4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08_H
#define REQ_T4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.Req
struct  Req_t4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Request BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.Req::req
	Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2 * ___req_0;

public:
	inline static int32_t get_offset_of_req_0() { return static_cast<int32_t>(offsetof(Req_t4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08, ___req_0)); }
	inline Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2 * get_req_0() const { return ___req_0; }
	inline Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2 ** get_address_of_req_0() { return &___req_0; }
	inline void set_req_0(Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2 * value)
	{
		___req_0 = value;
		Il2CppCodeGenWriteBarrier((&___req_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQ_T4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08_H
#ifndef RESPDATA_T53918D0631EB03D312DEF2964FC0957902A28885_H
#define RESPDATA_T53918D0631EB03D312DEF2964FC0957902A28885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RespData
struct  RespData_t53918D0631EB03D312DEF2964FC0957902A28885  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RespData::data
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(RespData_t53918D0631EB03D312DEF2964FC0957902A28885, ___data_0)); }
	inline ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * get_data_0() const { return ___data_0; }
	inline ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPDATA_T53918D0631EB03D312DEF2964FC0957902A28885_H
#ifndef REVOKEDSTATUS_T4BA9176EAE029F31F962664003623174B833961B_H
#define REVOKEDSTATUS_T4BA9176EAE029F31F962664003623174B833961B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RevokedStatus
struct  RevokedStatus_t4BA9176EAE029F31F962664003623174B833961B  : public CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.RevokedInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.RevokedStatus::info
	RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B * ___info_1;

public:
	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(RevokedStatus_t4BA9176EAE029F31F962664003623174B833961B, ___info_1)); }
	inline RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B * get_info_1() const { return ___info_1; }
	inline RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B ** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B * value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVOKEDSTATUS_T4BA9176EAE029F31F962664003623174B833961B_H
#ifndef SINGLERESP_T804CB324BCE98626E0798F4E1D9BD1D1AA586297_H
#define SINGLERESP_T804CB324BCE98626E0798F4E1D9BD1D1AA586297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.SingleResp
struct  SingleResp_t804CB324BCE98626E0798F4E1D9BD1D1AA586297  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.SingleResp::resp
	SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5 * ___resp_0;

public:
	inline static int32_t get_offset_of_resp_0() { return static_cast<int32_t>(offsetof(SingleResp_t804CB324BCE98626E0798F4E1D9BD1D1AA586297, ___resp_0)); }
	inline SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5 * get_resp_0() const { return ___resp_0; }
	inline SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5 ** get_address_of_resp_0() { return &___resp_0; }
	inline void set_resp_0(SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5 * value)
	{
		___resp_0 = value;
		Il2CppCodeGenWriteBarrier((&___resp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERESP_T804CB324BCE98626E0798F4E1D9BD1D1AA586297_H
#ifndef UNKNOWNSTATUS_TA45DE3108E915A37A617EDFE686801E52673CF36_H
#define UNKNOWNSTATUS_TA45DE3108E915A37A617EDFE686801E52673CF36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Ocsp.UnknownStatus
struct  UnknownStatus_tA45DE3108E915A37A617EDFE686801E52673CF36  : public CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNKNOWNSTATUS_TA45DE3108E915A37A617EDFE686801E52673CF36_H
#ifndef GENERALSECURITYEXCEPTION_T716664C0B62297FAAEE029B451BFB68534483ABA_H
#define GENERALSECURITYEXCEPTION_T716664C0B62297FAAEE029B451BFB68534483ABA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.GeneralSecurityException
struct  GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALSECURITYEXCEPTION_T716664C0B62297FAAEE029B451BFB68534483ABA_H
#ifndef SECURERANDOM_T5520D5E8543D2000539BD07077884C7FB17A9570_H
#define SECURERANDOM_T5520D5E8543D2000539BD07077884C7FB17A9570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct  SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570  : public Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::generator
	RuntimeObject* ___generator_5;

public:
	inline static int32_t get_offset_of_generator_5() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570, ___generator_5)); }
	inline RuntimeObject* get_generator_5() const { return ___generator_5; }
	inline RuntimeObject** get_address_of_generator_5() { return &___generator_5; }
	inline void set_generator_5(RuntimeObject* value)
	{
		___generator_5 = value;
		Il2CppCodeGenWriteBarrier((&___generator_5), value);
	}
};

struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::counter
	int64_t ___counter_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::master
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___master_4;
	// System.Double BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::DoubleScale
	double ___DoubleScale_6;

public:
	inline static int32_t get_offset_of_counter_3() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields, ___counter_3)); }
	inline int64_t get_counter_3() const { return ___counter_3; }
	inline int64_t* get_address_of_counter_3() { return &___counter_3; }
	inline void set_counter_3(int64_t value)
	{
		___counter_3 = value;
	}

	inline static int32_t get_offset_of_master_4() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields, ___master_4)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_master_4() const { return ___master_4; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_master_4() { return &___master_4; }
	inline void set_master_4(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___master_4 = value;
		Il2CppCodeGenWriteBarrier((&___master_4), value);
	}

	inline static int32_t get_offset_of_DoubleScale_6() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields, ___DoubleScale_6)); }
	inline double get_DoubleScale_6() const { return ___DoubleScale_6; }
	inline double* get_address_of_DoubleScale_6() { return &___DoubleScale_6; }
	inline void set_DoubleScale_6(double value)
	{
		___DoubleScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURERANDOM_T5520D5E8543D2000539BD07077884C7FB17A9570_H
#ifndef SECURITYUTILITYEXCEPTION_TF71E915CBE6769C6E9A84392FC5824B55396B460_H
#define SECURITYUTILITYEXCEPTION_TF71E915CBE6769C6E9A84392FC5824B55396B460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecurityUtilityException
struct  SecurityUtilityException_tF71E915CBE6769C6E9A84392FC5824B55396B460  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYUTILITYEXCEPTION_TF71E915CBE6769C6E9A84392FC5824B55396B460_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef ABSTRACTF2MPOINT_T16788EF8771AEC999392932B2C0237BC015F7FAF_H
#define ABSTRACTF2MPOINT_T16788EF8771AEC999392932B2C0237BC015F7FAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mPoint
struct  AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF  : public ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MPOINT_T16788EF8771AEC999392932B2C0237BC015F7FAF_H
#ifndef ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#define ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpPoint
struct  AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519  : public ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#ifndef F2MCURVE_T2EA747317BF12D710E175E1A2378579E810A4E74_H
#define F2MCURVE_T2EA747317BF12D710E175E1A2378579E810A4E74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve
struct  F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve::m
	int32_t ___m_18;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve::k1
	int32_t ___k1_19;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve::k2
	int32_t ___k2_20;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve::k3
	int32_t ___k3_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mCurve::m_infinity
	F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB * ___m_infinity_22;

public:
	inline static int32_t get_offset_of_m_18() { return static_cast<int32_t>(offsetof(F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74, ___m_18)); }
	inline int32_t get_m_18() const { return ___m_18; }
	inline int32_t* get_address_of_m_18() { return &___m_18; }
	inline void set_m_18(int32_t value)
	{
		___m_18 = value;
	}

	inline static int32_t get_offset_of_k1_19() { return static_cast<int32_t>(offsetof(F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74, ___k1_19)); }
	inline int32_t get_k1_19() const { return ___k1_19; }
	inline int32_t* get_address_of_k1_19() { return &___k1_19; }
	inline void set_k1_19(int32_t value)
	{
		___k1_19 = value;
	}

	inline static int32_t get_offset_of_k2_20() { return static_cast<int32_t>(offsetof(F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74, ___k2_20)); }
	inline int32_t get_k2_20() const { return ___k2_20; }
	inline int32_t* get_address_of_k2_20() { return &___k2_20; }
	inline void set_k2_20(int32_t value)
	{
		___k2_20 = value;
	}

	inline static int32_t get_offset_of_k3_21() { return static_cast<int32_t>(offsetof(F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74, ___k3_21)); }
	inline int32_t get_k3_21() const { return ___k3_21; }
	inline int32_t* get_address_of_k3_21() { return &___k3_21; }
	inline void set_k3_21(int32_t value)
	{
		___k3_21 = value;
	}

	inline static int32_t get_offset_of_m_infinity_22() { return static_cast<int32_t>(offsetof(F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74, ___m_infinity_22)); }
	inline F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB * get_m_infinity_22() const { return ___m_infinity_22; }
	inline F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB ** get_address_of_m_infinity_22() { return &___m_infinity_22; }
	inline void set_m_infinity_22(F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB * value)
	{
		___m_infinity_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F2MCURVE_T2EA747317BF12D710E175E1A2378579E810A4E74_H
#ifndef F2MFIELDELEMENT_TDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35_H
#define F2MFIELDELEMENT_TDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mFieldElement
struct  F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mFieldElement::representation
	int32_t ___representation_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mFieldElement::m
	int32_t ___m_4;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mFieldElement::ks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ks_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.LongArray BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mFieldElement::x
	LongArray_t878F80FE9906AAC7F3471E383219621561440608 * ___x_6;

public:
	inline static int32_t get_offset_of_representation_3() { return static_cast<int32_t>(offsetof(F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35, ___representation_3)); }
	inline int32_t get_representation_3() const { return ___representation_3; }
	inline int32_t* get_address_of_representation_3() { return &___representation_3; }
	inline void set_representation_3(int32_t value)
	{
		___representation_3 = value;
	}

	inline static int32_t get_offset_of_m_4() { return static_cast<int32_t>(offsetof(F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35, ___m_4)); }
	inline int32_t get_m_4() const { return ___m_4; }
	inline int32_t* get_address_of_m_4() { return &___m_4; }
	inline void set_m_4(int32_t value)
	{
		___m_4 = value;
	}

	inline static int32_t get_offset_of_ks_5() { return static_cast<int32_t>(offsetof(F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35, ___ks_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ks_5() const { return ___ks_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ks_5() { return &___ks_5; }
	inline void set_ks_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ks_5 = value;
		Il2CppCodeGenWriteBarrier((&___ks_5), value);
	}

	inline static int32_t get_offset_of_x_6() { return static_cast<int32_t>(offsetof(F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35, ___x_6)); }
	inline LongArray_t878F80FE9906AAC7F3471E383219621561440608 * get_x_6() const { return ___x_6; }
	inline LongArray_t878F80FE9906AAC7F3471E383219621561440608 ** get_address_of_x_6() { return &___x_6; }
	inline void set_x_6(LongArray_t878F80FE9906AAC7F3471E383219621561440608 * value)
	{
		___x_6 = value;
		Il2CppCodeGenWriteBarrier((&___x_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F2MFIELDELEMENT_TDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35_H
#ifndef FPCURVE_T67E0069B782651A939C038309FE22F5F4273C6B1_H
#define FPCURVE_T67E0069B782651A939C038309FE22F5F4273C6B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpCurve
struct  FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpCurve::m_q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_q_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpCurve::m_r
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_r_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpCurve::m_infinity
	FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191 * ___m_infinity_19;

public:
	inline static int32_t get_offset_of_m_q_17() { return static_cast<int32_t>(offsetof(FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1, ___m_q_17)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_q_17() const { return ___m_q_17; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_q_17() { return &___m_q_17; }
	inline void set_m_q_17(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_q_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_q_17), value);
	}

	inline static int32_t get_offset_of_m_r_18() { return static_cast<int32_t>(offsetof(FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1, ___m_r_18)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_r_18() const { return ___m_r_18; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_r_18() { return &___m_r_18; }
	inline void set_m_r_18(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_r_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_r_18), value);
	}

	inline static int32_t get_offset_of_m_infinity_19() { return static_cast<int32_t>(offsetof(FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1, ___m_infinity_19)); }
	inline FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191 * get_m_infinity_19() const { return ___m_infinity_19; }
	inline FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191 ** get_address_of_m_infinity_19() { return &___m_infinity_19; }
	inline void set_m_infinity_19(FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191 * value)
	{
		___m_infinity_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPCURVE_T67E0069B782651A939C038309FE22F5F4273C6B1_H
#ifndef FPFIELDELEMENT_TA0762B05C9DFADA0C28EC78C0B3A53407443CB91_H
#define FPFIELDELEMENT_TA0762B05C9DFADA0C28EC78C0B3A53407443CB91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpFieldElement
struct  FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpFieldElement::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpFieldElement::r
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___r_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpFieldElement::x
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91, ___q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_0() const { return ___q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91, ___r_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_r_1() const { return ___r_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___r_1 = value;
		Il2CppCodeGenWriteBarrier((&___r_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91, ___x_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x_2() const { return ___x_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPFIELDELEMENT_TA0762B05C9DFADA0C28EC78C0B3A53407443CB91_H
#ifndef CERTIFICATEEXCEPTION_TBC17D77D9645478DD20EE923F99749D8F8A042BF_H
#define CERTIFICATEEXCEPTION_TBC17D77D9645478DD20EE923F99749D8F8A042BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.Certificates.CertificateException
struct  CertificateException_tBC17D77D9645478DD20EE923F99749D8F8A042BF  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEEXCEPTION_TBC17D77D9645478DD20EE923F99749D8F8A042BF_H
#ifndef CRLEXCEPTION_TC73F6AB9D89F89D775F99ADD911B1911A39BD812_H
#define CRLEXCEPTION_TC73F6AB9D89F89D775F99ADD911B1911A39BD812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.Certificates.CrlException
struct  CrlException_tC73F6AB9D89F89D775F99ADD911B1911A39BD812  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLEXCEPTION_TC73F6AB9D89F89D775F99ADD911B1911A39BD812_H
#ifndef CIPHERALGORITHM_T26FD6099544541CD8FB307CF87A3EB92185E5239_H
#define CIPHERALGORITHM_T26FD6099544541CD8FB307CF87A3EB92185E5239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities_CipherAlgorithm
struct  CipherAlgorithm_t26FD6099544541CD8FB307CF87A3EB92185E5239 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities_CipherAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherAlgorithm_t26FD6099544541CD8FB307CF87A3EB92185E5239, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERALGORITHM_T26FD6099544541CD8FB307CF87A3EB92185E5239_H
#ifndef CIPHERMODE_TAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E_H
#define CIPHERMODE_TAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities_CipherMode
struct  CipherMode_tAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities_CipherMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherMode_tAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_TAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E_H
#ifndef CIPHERPADDING_T22390D0C828B277661E4BF86F8CBA41C1CBD6A52_H
#define CIPHERPADDING_T22390D0C828B277661E4BF86F8CBA41C1CBD6A52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities_CipherPadding
struct  CipherPadding_t22390D0C828B277661E4BF86F8CBA41C1CBD6A52 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities_CipherPadding::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CipherPadding_t22390D0C828B277661E4BF86F8CBA41C1CBD6A52, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERPADDING_T22390D0C828B277661E4BF86F8CBA41C1CBD6A52_H
#ifndef DIGESTALGORITHM_TD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3_H
#define DIGESTALGORITHM_TD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.DigestUtilities_DigestAlgorithm
struct  DigestAlgorithm_tD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.DigestUtilities_DigestAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DigestAlgorithm_tD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTALGORITHM_TD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3_H
#ifndef KEYEXCEPTION_T28C91D6E025D1CCD18D8C3F2E540F696B116F2B8_H
#define KEYEXCEPTION_T28C91D6E025D1CCD18D8C3F2E540F696B116F2B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.KeyException
struct  KeyException_t28C91D6E025D1CCD18D8C3F2E540F696B116F2B8  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEXCEPTION_T28C91D6E025D1CCD18D8C3F2E540F696B116F2B8_H
#ifndef NOSUCHALGORITHMEXCEPTION_TE240FD914C3418884B6398326764757C0CD0D10D_H
#define NOSUCHALGORITHMEXCEPTION_TE240FD914C3418884B6398326764757C0CD0D10D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.NoSuchAlgorithmException
struct  NoSuchAlgorithmException_tE240FD914C3418884B6398326764757C0CD0D10D  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOSUCHALGORITHMEXCEPTION_TE240FD914C3418884B6398326764757C0CD0D10D_H
#ifndef SIGNATUREEXCEPTION_T15C91B48DB64F82B3CAB961AD74F93D4E44529C3_H
#define SIGNATUREEXCEPTION_T15C91B48DB64F82B3CAB961AD74F93D4E44529C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SignatureException
struct  SignatureException_t15C91B48DB64F82B3CAB961AD74F93D4E44529C3  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREEXCEPTION_T15C91B48DB64F82B3CAB961AD74F93D4E44529C3_H
#ifndef WRAPALGORITHM_T6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2_H
#define WRAPALGORITHM_T6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities_WrapAlgorithm
struct  WrapAlgorithm_t6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.WrapperUtilities_WrapAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapAlgorithm_t6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPALGORITHM_T6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2_H
#ifndef F2MPOINT_TB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB_H
#define F2MPOINT_TB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.F2mPoint
struct  F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F2MPOINT_TB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB_H
#ifndef FPPOINT_TAD4EC909038763E8DCF780C84CBC6A1851761191_H
#define FPPOINT_TAD4EC909038763E8DCF780C84CBC6A1851761191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.FpPoint
struct  FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPPOINT_TAD4EC909038763E8DCF780C84CBC6A1851761191_H
#ifndef CERTIFICATEENCODINGEXCEPTION_TDF5AB464C292DD2F51553A821E01495B489DE721_H
#define CERTIFICATEENCODINGEXCEPTION_TDF5AB464C292DD2F51553A821E01495B489DE721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.Certificates.CertificateEncodingException
struct  CertificateEncodingException_tDF5AB464C292DD2F51553A821E01495B489DE721  : public CertificateException_tBC17D77D9645478DD20EE923F99749D8F8A042BF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEENCODINGEXCEPTION_TDF5AB464C292DD2F51553A821E01495B489DE721_H
#ifndef CERTIFICATEEXPIREDEXCEPTION_T68954B1152D2E15784F87627A3B29F231BC96D2A_H
#define CERTIFICATEEXPIREDEXCEPTION_T68954B1152D2E15784F87627A3B29F231BC96D2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.Certificates.CertificateExpiredException
struct  CertificateExpiredException_t68954B1152D2E15784F87627A3B29F231BC96D2A  : public CertificateException_tBC17D77D9645478DD20EE923F99749D8F8A042BF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEEXPIREDEXCEPTION_T68954B1152D2E15784F87627A3B29F231BC96D2A_H
#ifndef CERTIFICATENOTYETVALIDEXCEPTION_T7C2547C5CA9776E8697D936A74C7D715AF978B7A_H
#define CERTIFICATENOTYETVALIDEXCEPTION_T7C2547C5CA9776E8697D936A74C7D715AF978B7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.Certificates.CertificateNotYetValidException
struct  CertificateNotYetValidException_t7C2547C5CA9776E8697D936A74C7D715AF978B7A  : public CertificateException_tBC17D77D9645478DD20EE923F99749D8F8A042BF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATENOTYETVALIDEXCEPTION_T7C2547C5CA9776E8697D936A74C7D715AF978B7A_H
#ifndef CERTIFICATEPARSINGEXCEPTION_T447BDF32046170F03F50C242B210F45E3DD4FB7B_H
#define CERTIFICATEPARSINGEXCEPTION_T447BDF32046170F03F50C242B210F45E3DD4FB7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.Certificates.CertificateParsingException
struct  CertificateParsingException_t447BDF32046170F03F50C242B210F45E3DD4FB7B  : public CertificateException_tBC17D77D9645478DD20EE923F99749D8F8A042BF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEPARSINGEXCEPTION_T447BDF32046170F03F50C242B210F45E3DD4FB7B_H
#ifndef INVALIDKEYEXCEPTION_TDD40A8560BEF6830276F4A2EEEB4015A5F8E09B6_H
#define INVALIDKEYEXCEPTION_TDD40A8560BEF6830276F4A2EEEB4015A5F8E09B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.InvalidKeyException
struct  InvalidKeyException_tDD40A8560BEF6830276F4A2EEEB4015A5F8E09B6  : public KeyException_t28C91D6E025D1CCD18D8C3F2E540F696B116F2B8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDKEYEXCEPTION_TDD40A8560BEF6830276F4A2EEEB4015A5F8E09B6_H
#ifndef INVALIDPARAMETEREXCEPTION_T98C55130FFA5E278D873986C334C273DB024E094_H
#define INVALIDPARAMETEREXCEPTION_T98C55130FFA5E278D873986C334C273DB024E094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.InvalidParameterException
struct  InvalidParameterException_t98C55130FFA5E278D873986C334C273DB024E094  : public KeyException_t28C91D6E025D1CCD18D8C3F2E540F696B116F2B8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPARAMETEREXCEPTION_T98C55130FFA5E278D873986C334C273DB024E094_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (CipherAlgorithm_t26FD6099544541CD8FB307CF87A3EB92185E5239)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4100[40] = 
{
	CipherAlgorithm_t26FD6099544541CD8FB307CF87A3EB92185E5239::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { sizeof (CipherMode_tAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4101[15] = 
{
	CipherMode_tAA8B52C51532F2DBB544B3AD2FC8BE748DC9241E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (CipherPadding_t22390D0C828B277661E4BF86F8CBA41C1CBD6A52)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4102[33] = 
{
	CipherPadding_t22390D0C828B277661E4BF86F8CBA41C1CBD6A52::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491), -1, sizeof(DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4103[2] = 
{
	DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_StaticFields::get_offset_of_algorithms_0(),
	DigestUtilities_tD5BD702ECE2CAA9DD2411FADCF1612DFD9A17491_StaticFields::get_offset_of_oids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { sizeof (DigestAlgorithm_tD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4104[44] = 
{
	DigestAlgorithm_tD081C0F3FFFD2EBFF2F2A891E8B7335BED2E4AD3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { sizeof (DotNetUtilities_tF7BA78D7CFF5F05B7E2AFE864ABF54D3499AA086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { sizeof (GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137), -1, sizeof(GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4107[3] = 
{
	GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields::get_offset_of_kgAlgorithms_0(),
	GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields::get_offset_of_kpgAlgorithms_1(),
	GeneratorUtilities_t114A04F4672440A48260E104943C652B34206137_StaticFields::get_offset_of_defaultKeySizes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { sizeof (InvalidKeyException_tDD40A8560BEF6830276F4A2EEEB4015A5F8E09B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (InvalidParameterException_t98C55130FFA5E278D873986C334C273DB024E094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (KeyException_t28C91D6E025D1CCD18D8C3F2E540F696B116F2B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { sizeof (MacUtilities_tBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9), -1, sizeof(MacUtilities_tBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4111[1] = 
{
	MacUtilities_tBE9D8C7F13F4B33B07139EA524C7EB91D592A9A9_StaticFields::get_offset_of_algorithms_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (NoSuchAlgorithmException_tE240FD914C3418884B6398326764757C0CD0D10D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217), -1, sizeof(ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4113[2] = 
{
	ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217_StaticFields::get_offset_of_algorithms_0(),
	ParameterUtilities_t06ABACFB71DBD0F86F16299DD5F6236FAE272217_StaticFields::get_offset_of_basicIVSizes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3), -1, sizeof(PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4114[7] = 
{
	0,
	0,
	0,
	0,
	PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields::get_offset_of_algorithms_4(),
	PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields::get_offset_of_algorithmType_5(),
	PbeUtilities_t1FBE7ED48B3F54E6B4FBBCF6868B6942CD9571B3_StaticFields::get_offset_of_oids_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { sizeof (PrivateKeyFactory_t90D5BA0ECED77B8C83C3F9ECB915D44D3AD50555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (PublicKeyFactory_t2D224D40667D7FB1A484A2B1D172015C9F8344F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570), -1, sizeof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4117[4] = 
{
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields::get_offset_of_counter_3(),
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields::get_offset_of_master_4(),
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570::get_offset_of_generator_5(),
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields::get_offset_of_DoubleScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { sizeof (SecurityUtilityException_tF71E915CBE6769C6E9A84392FC5824B55396B460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { sizeof (SignatureException_t15C91B48DB64F82B3CAB961AD74F93D4E44529C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B), -1, sizeof(SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4120[2] = 
{
	SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_StaticFields::get_offset_of_algorithms_0(),
	SignerUtilities_tD6AACBEB1CEA3FC553A3FF20849B1ADAB3860B5B_StaticFields::get_offset_of_oids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { sizeof (WrapperUtilities_t7CCFA86B917628908B41BBFF3459D7DF9F928FB3), -1, sizeof(WrapperUtilities_t7CCFA86B917628908B41BBFF3459D7DF9F928FB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4121[1] = 
{
	WrapperUtilities_t7CCFA86B917628908B41BBFF3459D7DF9F928FB3_StaticFields::get_offset_of_algorithms_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { sizeof (WrapAlgorithm_t6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4122[9] = 
{
	WrapAlgorithm_t6FF5F5F7EAB6F8F446EC565F0A8E4B02025E8EA2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (BufferedCipherWrapper_tFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4123[2] = 
{
	BufferedCipherWrapper_tFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6::get_offset_of_cipher_0(),
	BufferedCipherWrapper_tFD357BC47E2EEEDD1D28D5FCA846E9B283D28EB6::get_offset_of_forWrapping_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (CertificateEncodingException_tDF5AB464C292DD2F51553A821E01495B489DE721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (CertificateException_tBC17D77D9645478DD20EE923F99749D8F8A042BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (CertificateExpiredException_t68954B1152D2E15784F87627A3B29F231BC96D2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (CertificateNotYetValidException_t7C2547C5CA9776E8697D936A74C7D715AF978B7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (CertificateParsingException_t447BDF32046170F03F50C242B210F45E3DD4FB7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (CrlException_tC73F6AB9D89F89D775F99ADD911B1911A39BD812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { sizeof (BasicOcspResp_t2A3C88955A8368F95ADEEC5586D3F27341A6000D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4130[2] = 
{
	BasicOcspResp_t2A3C88955A8368F95ADEEC5586D3F27341A6000D::get_offset_of_resp_0(),
	BasicOcspResp_t2A3C88955A8368F95ADEEC5586D3F27341A6000D::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { sizeof (BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4131[3] = 
{
	BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546::get_offset_of_list_0(),
	BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546::get_offset_of_responseExtensions_1(),
	BasicOcspRespGenerator_t2104C13C53B7A8C29B6E59F99C3F9F62BA7BE546::get_offset_of_responderID_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { sizeof (ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4132[5] = 
{
	ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4::get_offset_of_certId_0(),
	ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4::get_offset_of_certStatus_1(),
	ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4::get_offset_of_thisUpdate_2(),
	ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4::get_offset_of_nextUpdate_3(),
	ResponseObject_t3E9A6429FB8AB23B423B04F0B066F0FBA1092AA4::get_offset_of_extensions_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { sizeof (CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4133[2] = 
{
	0,
	CertificateID_t15EDD341582906F7ED7C1A5858B032D639E45BAA::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { sizeof (CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16), -1, sizeof(CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4134[1] = 
{
	CertificateStatus_t1CEB114499D83E64B84058693FE7C47DD31A3B16_StaticFields::get_offset_of_Good_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (OcspException_tB71E32252369B949E00E8F7BA4EE09BDEE398E40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (OcspReq_t4EAFC3977FEA343F2B62DF99F5A3072A74962997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4136[1] = 
{
	OcspReq_t4EAFC3977FEA343F2B62DF99F5A3072A74962997::get_offset_of_req_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4137[3] = 
{
	OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60::get_offset_of_list_0(),
	OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60::get_offset_of_requestorName_1(),
	OcspReqGenerator_tF9E7E6C7B5A3B3E3CE95184705E2405C1D0BCD60::get_offset_of_requestExtensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (RequestObject_t3D7261AFDB1C76702114959E238215DBD73CD897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4138[2] = 
{
	RequestObject_t3D7261AFDB1C76702114959E238215DBD73CD897::get_offset_of_certId_0(),
	RequestObject_t3D7261AFDB1C76702114959E238215DBD73CD897::get_offset_of_extensions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (OcspResp_tEAE3E79062DD83DE1B850EE7D926754790F08A50), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4139[1] = 
{
	OcspResp_tEAE3E79062DD83DE1B850EE7D926754790F08A50::get_offset_of_resp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (OCSPRespGenerator_t61091485652FA692A350F567B8B7E7F8C8C06981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4140[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (OcscpRespStatus_t6A6E12E6999F48E76CAAB2E7D748D7847328397D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { sizeof (OcspRespStatus_t8B857323CD2F1CDFD2A758D37774419F16AA6C39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4142[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { sizeof (OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5), -1, sizeof(OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4143[3] = 
{
	OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields::get_offset_of_algorithms_0(),
	OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields::get_offset_of_oids_1(),
	OcspUtilities_t3B5CEB42937A585213DAF5D35881BE2119E596B5_StaticFields::get_offset_of_noParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { sizeof (Req_t4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4144[1] = 
{
	Req_t4607F3A9BE53AF3E2BDE545D3DD3E61056CD2D08::get_offset_of_req_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { sizeof (RespData_t53918D0631EB03D312DEF2964FC0957902A28885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4145[1] = 
{
	RespData_t53918D0631EB03D312DEF2964FC0957902A28885::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { sizeof (RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4146[1] = 
{
	RespID_t3CA53921D64BD58A2C58BB7303CFCBDF8CFCBC53::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { sizeof (RevokedStatus_t4BA9176EAE029F31F962664003623174B833961B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4147[1] = 
{
	RevokedStatus_t4BA9176EAE029F31F962664003623174B833961B::get_offset_of_info_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { sizeof (SingleResp_t804CB324BCE98626E0798F4E1D9BD1D1AA586297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4148[1] = 
{
	SingleResp_t804CB324BCE98626E0798F4E1D9BD1D1AA586297::get_offset_of_resp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { sizeof (UnknownStatus_tA45DE3108E915A37A617EDFE686801E52673CF36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50), -1, sizeof(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4150[36] = 
{
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_primeLists_0(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_primeProducts_1(),
	0,
	0,
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_ZeroMagnitude_4(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_ZeroEncoding_5(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_SMALL_CONSTANTS_6(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_Zero_7(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_One_8(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_Two_9(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_Three_10(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_Four_11(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_Ten_12(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_BitLengthTable_13(),
	0,
	0,
	0,
	0,
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix2_18(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix2E_19(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix8_20(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix8E_21(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix10_22(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix10E_23(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix16_24(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_radix16E_25(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_RandomSource_26(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50_StaticFields::get_offset_of_ExpWindowThresholds_27(),
	0,
	0,
	0,
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50::get_offset_of_magnitude_31(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50::get_offset_of_sign_32(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50::get_offset_of_nBits_33(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50::get_offset_of_nBitLength_34(),
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50::get_offset_of_mQuote_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F), -1, sizeof(Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4151[4] = 
{
	Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields::get_offset_of_SmallFactorLimit_0(),
	Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields::get_offset_of_One_1(),
	Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields::get_offset_of_Two_2(),
	Primes_t4FD7C76E5D7EF9A01055A49953C6C9D6F8627E0F_StaticFields::get_offset_of_Three_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (MROutput_tDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4152[2] = 
{
	MROutput_tDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C::get_offset_of_mProvablyComposite_0(),
	MROutput_tDDBF8F30CA2BA875A2B608E6DF60F328C3F3C00C::get_offset_of_mFactor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4153[3] = 
{
	STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574::get_offset_of_mPrime_0(),
	STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574::get_offset_of_mPrimeSeed_1(),
	STOutput_t46CD7181B9FC3F219A7A997001EC67E48E9F5574::get_offset_of_mPrimeGenCounter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (Interleave_tA373474BEE1BC6785262AE17E15CCE733134DDFE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4154[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { sizeof (Mod_t6F0DA656D37CECD55F8BD60C3DC7215057ECDC68), -1, sizeof(Mod_t6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4155[1] = 
{
	Mod_t6F0DA656D37CECD55F8BD60C3DC7215057ECDC68_StaticFields::get_offset_of_RandomSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (Nat_tBA1B4ED8AE03FC99A5113B6A556531A0405A80E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4156[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { sizeof (Nat128_tFB9E14007EA78309ADB34B9F6B3C3E78F4B6D5DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4157[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (Nat160_tADDCBC1B4AFE651800677D49921F198DE061B116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4158[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (Nat192_tC116EB7A2010B82493A999B67D6D349787347C05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4159[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (Nat224_t28E36542839A9EE392CD56E9A6BFC8BA95E85CC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4160[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (Nat256_tDAFE94DD1BD93EC69650B7E8C70D88A7F772E83B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4161[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (Nat320_tEE477C41E23A3D78467EF55F2652676749127DDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (Nat384_t0488ECFFD96A0DC33C1AC2455ADB66673274D100), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (Nat448_t78C4572FF9544EFF52207C50FFB75B3472D30756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (Nat512_tC7DE3A98ED3CFE6DF6E8F032C2BE455CD17677B4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { sizeof (Nat576_tD271E6DD50AA86C81D535554E052F43E56D839FA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { sizeof (FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8), -1, sizeof(FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4167[2] = 
{
	FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8_StaticFields::get_offset_of_GF_2_0(),
	FiniteFields_t9B91A06C88254F3E33D113F3A45313664AF353F8_StaticFields::get_offset_of_GF_3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (GenericPolynomialExtensionField_t0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4168[2] = 
{
	GenericPolynomialExtensionField_t0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C::get_offset_of_subfield_0(),
	GenericPolynomialExtensionField_t0B020B991D7C14D09239DA9D0D1DFB782EEE5C5C::get_offset_of_minimalPolynomial_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (GF2Polynomial_t0E753AD232DBC83D58438DC30DD26E17FD51F58F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4169[1] = 
{
	GF2Polynomial_t0E753AD232DBC83D58438DC30DD26E17FD51F58F::get_offset_of_exponents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (PrimeField_t4DB7E8264AE9D6AA7516390551D79F703CF401E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4174[1] = 
{
	PrimeField_t4DB7E8264AE9D6AA7516390551D79F703CF401E4::get_offset_of_characteristic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { sizeof (ECAlgorithms_t11E89F322556281C63AFEB0ABF2CD99398D53811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { sizeof (ECCurve_t6071986B64066FBF97315592BE971355FA584A39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4177[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_field_8(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_a_9(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_b_10(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_order_11(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_cofactor_12(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_coord_13(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_endomorphism_14(),
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39::get_offset_of_m_multiplier_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { sizeof (Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4178[4] = 
{
	Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6::get_offset_of_outer_0(),
	Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6::get_offset_of_coord_1(),
	Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6::get_offset_of_endomorphism_2(),
	Config_t040FF96A3A9BD07BD318D7C066D46D76EFF581A6::get_offset_of_multiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4179[3] = 
{
	DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974::get_offset_of_m_outer_0(),
	DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974::get_offset_of_m_table_1(),
	DefaultLookupTable_t58033721A4617DCEF6BE2C0727CC7D79D008B974::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4181[4] = 
{
	0,
	FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1::get_offset_of_m_q_17(),
	FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1::get_offset_of_m_r_18(),
	FpCurve_t67E0069B782651A939C038309FE22F5F4273C6B1::get_offset_of_m_infinity_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4182[1] = 
{
	AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187::get_offset_of_si_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4183[6] = 
{
	0,
	F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74::get_offset_of_m_18(),
	F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74::get_offset_of_k1_19(),
	F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74::get_offset_of_k2_20(),
	F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74::get_offset_of_k3_21(),
	F2mCurve_t2EA747317BF12D710E175E1A2378579E810A4E74::get_offset_of_m_infinity_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4184[3] = 
{
	DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2::get_offset_of_m_outer_0(),
	DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2::get_offset_of_m_table_1(),
	DefaultF2mLookupTable_t8D20E169DFAD18F08A5EB1EDC0488B4CC1C0A1B2::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4187[3] = 
{
	FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91::get_offset_of_q_0(),
	FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91::get_offset_of_r_1(),
	FpFieldElement_tA0762B05C9DFADA0C28EC78C0B3A53407443CB91::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4189[7] = 
{
	0,
	0,
	0,
	F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35::get_offset_of_representation_3(),
	F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35::get_offset_of_m_4(),
	F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35::get_offset_of_ks_5(),
	F2mFieldElement_tDAB8123A7FE76ED731D9C592E41B4FFFFCFDEA35::get_offset_of_x_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E), -1, sizeof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4191[7] = 
{
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields::get_offset_of_EMPTY_ZS_0(),
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E::get_offset_of_m_curve_1(),
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E::get_offset_of_m_x_2(),
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E::get_offset_of_m_y_3(),
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E::get_offset_of_m_zs_4(),
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E::get_offset_of_m_withCompression_5(),
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E::get_offset_of_m_preCompTable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4192[3] = 
{
	ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD::get_offset_of_m_outer_0(),
	ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD::get_offset_of_m_decompressed_1(),
	ValidityCallback_tC00272AC76262C63C252D0A6149A9C5CD424DBFD::get_offset_of_m_checkOrder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195 = { sizeof (FpPoint_tAD4EC909038763E8DCF780C84CBC6A1851761191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196 = { sizeof (AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197 = { sizeof (F2mPoint_tB243AA141E9C1EDB3B6A0A616A5ED2DD501C48AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199 = { sizeof (LongArray_t878F80FE9906AAC7F3471E383219621561440608), -1, sizeof(LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4199[8] = 
{
	LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields::get_offset_of_INTERLEAVE2_TABLE_0(),
	LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields::get_offset_of_INTERLEAVE3_TABLE_1(),
	LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields::get_offset_of_INTERLEAVE4_TABLE_2(),
	LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields::get_offset_of_INTERLEAVE5_TABLE_3(),
	LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields::get_offset_of_INTERLEAVE7_TABLE_4(),
	0,
	LongArray_t878F80FE9906AAC7F3471E383219621561440608_StaticFields::get_offset_of_BitLengths_6(),
	LongArray_t878F80FE9906AAC7F3471E383219621561440608::get_offset_of_m_ints_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
