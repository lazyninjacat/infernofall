﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.JsonConverter
struct JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t54F68FC2F7C268CA67090069816699D6427B1711;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623;
// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522;
// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3;
// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A;
// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54;
// Newtonsoft.Json.Serialization.IAttributeProvider
struct IAttributeProvider_t7F07F5BC91B7AFBF9CCED6FDF83DBB41F2972D21;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_tC743A5579C4FEE7AABBE8063B8A13C44C524E10C;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t8CE53BA9C98C0CE46845BBEB4FEF453F676C0743;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t036DE89D342377A09D9BA90B5CA22F326C570E09;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D;
// Newtonsoft.Json.Utilities.PropertyNameTable
struct PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C;
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>
struct ThreadSafeStore_2_tEFC711003B7C7BD4AD5E17A00C44BBA99C0B573D;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>
struct ThreadSafeStore_2_tC8BA10A2953BE54DF4D6E7B5D6D4D8368BD9E216;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>
struct ThreadSafeStore_2_t309E8D78F56F1F657D0BE3006FC386A27954D3F6;
// Newtonsoft.Json.Utilities.TypeInformation[]
struct TypeInformationU5BU5D_tB3015365C0317DD37DD1BFB2A7BA79C8AB6EA7C5;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>
struct Dictionary_2_tF6B8E54C757DD806FAA7F1D339CCBEFF479B2CF8;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>
struct Dictionary_2_t1620483B111170AB5F6CE317EB940AE6A968909F;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType>
struct Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t4005A15A0C68CD41BEDE0863380069CB0C668945;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>
struct IDictionary_2_tECD9AF11A72F7B71E010A0595EB12D24238D486B;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t00C11E28587644E33CB7F154833508D63BFAC5B2;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty>
struct IList_1_t2CF473D357E157A45BDFC94108FF47228E70BD6F;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct IList_1_t9B22655AFF504060624BC452ED9E6B257AEF3A2B;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct IList_1_tC7EAA70C1362B15CBB553EEEE046436A96ADB7E3;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonProperty>
struct List_1_t2100AEEAF4CF6BDF67FCB0331C25090DB2221E52;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Exception
struct Exception_t;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32>
struct Func_2_t76C989F293D686B64AD7509DBA9D549D1E7A826D;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422;
// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1;
// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String>
struct Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>
struct Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Predicate`1<System.Object>
struct Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T0D5A68439D79EEC6571107944976EC2794DB9BE7_H
#define U3CU3EC_T0D5A68439D79EEC6571107944976EC2794DB9BE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c
struct  U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__30_0
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__30_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__30_1
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__30_1_2;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__33_0
	Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * ___U3CU3E9__33_0_3;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__33_1
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__33_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__36_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__36_0_5;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__60_0
	Func_2_t76C989F293D686B64AD7509DBA9D549D1E7A826D * ___U3CU3E9__60_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9__30_0_1)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__30_0_1() const { return ___U3CU3E9__30_0_1; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__30_0_1() { return &___U3CU3E9__30_0_1; }
	inline void set_U3CU3E9__30_0_1(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__30_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9__30_1_2)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__30_1_2() const { return ___U3CU3E9__30_1_2; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__30_1_2() { return &___U3CU3E9__30_1_2; }
	inline void set_U3CU3E9__30_1_2(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__30_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9__33_0_3)); }
	inline Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * get_U3CU3E9__33_0_3() const { return ___U3CU3E9__33_0_3; }
	inline Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A ** get_address_of_U3CU3E9__33_0_3() { return &___U3CU3E9__33_0_3; }
	inline void set_U3CU3E9__33_0_3(Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * value)
	{
		___U3CU3E9__33_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9__33_1_4)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__33_1_4() const { return ___U3CU3E9__33_1_4; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__33_1_4() { return &___U3CU3E9__33_1_4; }
	inline void set_U3CU3E9__33_1_4(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__33_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9__36_0_5)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__36_0_5() const { return ___U3CU3E9__36_0_5; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__36_0_5() { return &___U3CU3E9__36_0_5; }
	inline void set_U3CU3E9__36_0_5(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__36_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__60_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields, ___U3CU3E9__60_0_6)); }
	inline Func_2_t76C989F293D686B64AD7509DBA9D549D1E7A826D * get_U3CU3E9__60_0_6() const { return ___U3CU3E9__60_0_6; }
	inline Func_2_t76C989F293D686B64AD7509DBA9D549D1E7A826D ** get_address_of_U3CU3E9__60_0_6() { return &___U3CU3E9__60_0_6; }
	inline void set_U3CU3E9__60_0_6(Func_2_t76C989F293D686B64AD7509DBA9D549D1E7A826D * value)
	{
		___U3CU3E9__60_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__60_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0D5A68439D79EEC6571107944976EC2794DB9BE7_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_T05CFC72840791A32124346EA7AE6829BCFB04522_H
#define U3CU3EC__DISPLAYCLASS34_0_T05CFC72840791A32124346EA7AE6829BCFB04522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_0::getExtensionDataDictionary
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___getExtensionDataDictionary_0;
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_0::member
	MemberInfo_t * ___member_1;

public:
	inline static int32_t get_offset_of_getExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522, ___getExtensionDataDictionary_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_getExtensionDataDictionary_0() const { return ___getExtensionDataDictionary_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_getExtensionDataDictionary_0() { return &___getExtensionDataDictionary_0; }
	inline void set_getExtensionDataDictionary_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___getExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___getExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522, ___member_1)); }
	inline MemberInfo_t * get_member_1() const { return ___member_1; }
	inline MemberInfo_t ** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(MemberInfo_t * value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier((&___member_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_T05CFC72840791A32124346EA7AE6829BCFB04522_H
#ifndef U3CU3EC__DISPLAYCLASS34_1_TEA82263B1CBA8DFF05AB4047B64C316CE91D43BE_H
#define U3CU3EC__DISPLAYCLASS34_1_TEA82263B1CBA8DFF05AB4047B64C316CE91D43BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_1
struct  U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE  : public RuntimeObject
{
public:
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_1::setExtensionDataDictionary
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___setExtensionDataDictionary_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_1::createExtensionDataDictionary
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___createExtensionDataDictionary_1;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_1::setExtensionDataDictionaryValue
	MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * ___setExtensionDataDictionaryValue_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_0 Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_setExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE, ___setExtensionDataDictionary_0)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_setExtensionDataDictionary_0() const { return ___setExtensionDataDictionary_0; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_setExtensionDataDictionary_0() { return &___setExtensionDataDictionary_0; }
	inline void set_setExtensionDataDictionary_0(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___setExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_createExtensionDataDictionary_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE, ___createExtensionDataDictionary_1)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_createExtensionDataDictionary_1() const { return ___createExtensionDataDictionary_1; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_createExtensionDataDictionary_1() { return &___createExtensionDataDictionary_1; }
	inline void set_createExtensionDataDictionary_1(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___createExtensionDataDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___createExtensionDataDictionary_1), value);
	}

	inline static int32_t get_offset_of_setExtensionDataDictionaryValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE, ___setExtensionDataDictionaryValue_2)); }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * get_setExtensionDataDictionaryValue_2() const { return ___setExtensionDataDictionaryValue_2; }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D ** get_address_of_setExtensionDataDictionaryValue_2() { return &___setExtensionDataDictionaryValue_2; }
	inline void set_setExtensionDataDictionaryValue_2(MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * value)
	{
		___setExtensionDataDictionaryValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionaryValue_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_1_TEA82263B1CBA8DFF05AB4047B64C316CE91D43BE_H
#ifndef U3CU3EC__DISPLAYCLASS34_2_TB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD_H
#define U3CU3EC__DISPLAYCLASS34_2_TB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_2
struct  U3CU3Ec__DisplayClass34_2_tB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_2::createEnumerableWrapper
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ___createEnumerableWrapper_0;
	// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_0 Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass34_2::CSU24<>8__locals2
	U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 * ___CSU24U3CU3E8__locals2_1;

public:
	inline static int32_t get_offset_of_createEnumerableWrapper_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_2_tB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD, ___createEnumerableWrapper_0)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get_createEnumerableWrapper_0() const { return ___createEnumerableWrapper_0; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of_createEnumerableWrapper_0() { return &___createEnumerableWrapper_0; }
	inline void set_createEnumerableWrapper_0(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		___createEnumerableWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___createEnumerableWrapper_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_2_tB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522 * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_2_TB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD_H
#ifndef U3CU3EC__DISPLAYCLASS64_0_T41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F_H
#define U3CU3EC__DISPLAYCLASS64_0_T41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass64_0
struct  U3CU3Ec__DisplayClass64_0_t41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass64_0::shouldSerializeCall
	MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * ___shouldSerializeCall_0;

public:
	inline static int32_t get_offset_of_shouldSerializeCall_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F, ___shouldSerializeCall_0)); }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * get_shouldSerializeCall_0() const { return ___shouldSerializeCall_0; }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D ** get_address_of_shouldSerializeCall_0() { return &___shouldSerializeCall_0; }
	inline void set_shouldSerializeCall_0(MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * value)
	{
		___shouldSerializeCall_0 = value;
		Il2CppCodeGenWriteBarrier((&___shouldSerializeCall_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS64_0_T41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F_H
#ifndef U3CU3EC__DISPLAYCLASS65_0_TCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD_H
#define U3CU3EC__DISPLAYCLASS65_0_TCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_tCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass65_0::specifiedPropertyGet
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___specifiedPropertyGet_0;

public:
	inline static int32_t get_offset_of_specifiedPropertyGet_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_tCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD, ___specifiedPropertyGet_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_specifiedPropertyGet_0() const { return ___specifiedPropertyGet_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_specifiedPropertyGet_0() { return &___specifiedPropertyGet_0; }
	inline void set_specifiedPropertyGet_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___specifiedPropertyGet_0 = value;
		Il2CppCodeGenWriteBarrier((&___specifiedPropertyGet_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS65_0_TCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD_H
#ifndef DEFAULTCONTRACTRESOLVERSTATE_TA339D97C01BEA6BCD9EC6EB02BD96536687167FB_H
#define DEFAULTCONTRACTRESOLVERSTATE_TA339D97C01BEA6BCD9EC6EB02BD96536687167FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct  DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract> Newtonsoft.Json.Serialization.DefaultContractResolverState::ContractCache
	Dictionary_2_tF6B8E54C757DD806FAA7F1D339CCBEFF479B2CF8 * ___ContractCache_0;
	// Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.Serialization.DefaultContractResolverState::NameTable
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C * ___NameTable_1;

public:
	inline static int32_t get_offset_of_ContractCache_0() { return static_cast<int32_t>(offsetof(DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB, ___ContractCache_0)); }
	inline Dictionary_2_tF6B8E54C757DD806FAA7F1D339CCBEFF479B2CF8 * get_ContractCache_0() const { return ___ContractCache_0; }
	inline Dictionary_2_tF6B8E54C757DD806FAA7F1D339CCBEFF479B2CF8 ** get_address_of_ContractCache_0() { return &___ContractCache_0; }
	inline void set_ContractCache_0(Dictionary_2_tF6B8E54C757DD806FAA7F1D339CCBEFF479B2CF8 * value)
	{
		___ContractCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___ContractCache_0), value);
	}

	inline static int32_t get_offset_of_NameTable_1() { return static_cast<int32_t>(offsetof(DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB, ___NameTable_1)); }
	inline PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C * get_NameTable_1() const { return ___NameTable_1; }
	inline PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C ** get_address_of_NameTable_1() { return &___NameTable_1; }
	inline void set_NameTable_1(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C * value)
	{
		___NameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___NameTable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVERSTATE_TA339D97C01BEA6BCD9EC6EB02BD96536687167FB_H
#ifndef DEFAULTREFERENCERESOLVER_TA4B664F91DA305784CF2177EC9D821AFA2596DC6_H
#define DEFAULTREFERENCERESOLVER_TA4B664F91DA305784CF2177EC9D821AFA2596DC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultReferenceResolver
struct  DefaultReferenceResolver_tA4B664F91DA305784CF2177EC9D821AFA2596DC6  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.DefaultReferenceResolver::_referenceCount
	int32_t ____referenceCount_0;

public:
	inline static int32_t get_offset_of__referenceCount_0() { return static_cast<int32_t>(offsetof(DefaultReferenceResolver_tA4B664F91DA305784CF2177EC9D821AFA2596DC6, ____referenceCount_0)); }
	inline int32_t get__referenceCount_0() const { return ____referenceCount_0; }
	inline int32_t* get_address_of__referenceCount_0() { return &____referenceCount_0; }
	inline void set__referenceCount_0(int32_t value)
	{
		____referenceCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTREFERENCERESOLVER_TA4B664F91DA305784CF2177EC9D821AFA2596DC6_H
#ifndef ERRORCONTEXT_T6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3_H
#define ERRORCONTEXT_T6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ErrorContext
struct  ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.ErrorContext::<Traced>k__BackingField
	bool ___U3CTracedU3Ek__BackingField_0;
	// System.Exception Newtonsoft.Json.Serialization.ErrorContext::<Error>k__BackingField
	Exception_t * ___U3CErrorU3Ek__BackingField_1;
	// System.Object Newtonsoft.Json.Serialization.ErrorContext::<OriginalObject>k__BackingField
	RuntimeObject * ___U3COriginalObjectU3Ek__BackingField_2;
	// System.Object Newtonsoft.Json.Serialization.ErrorContext::<Member>k__BackingField
	RuntimeObject * ___U3CMemberU3Ek__BackingField_3;
	// System.String Newtonsoft.Json.Serialization.ErrorContext::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Serialization.ErrorContext::<Handled>k__BackingField
	bool ___U3CHandledU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTracedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3, ___U3CTracedU3Ek__BackingField_0)); }
	inline bool get_U3CTracedU3Ek__BackingField_0() const { return ___U3CTracedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTracedU3Ek__BackingField_0() { return &___U3CTracedU3Ek__BackingField_0; }
	inline void set_U3CTracedU3Ek__BackingField_0(bool value)
	{
		___U3CTracedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3, ___U3CErrorU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COriginalObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3, ___U3COriginalObjectU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3COriginalObjectU3Ek__BackingField_2() const { return ___U3COriginalObjectU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3COriginalObjectU3Ek__BackingField_2() { return &___U3COriginalObjectU3Ek__BackingField_2; }
	inline void set_U3COriginalObjectU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3COriginalObjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalObjectU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMemberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3, ___U3CMemberU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CMemberU3Ek__BackingField_3() const { return ___U3CMemberU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CMemberU3Ek__BackingField_3() { return &___U3CMemberU3Ek__BackingField_3; }
	inline void set_U3CMemberU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CMemberU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3, ___U3CPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CPathU3Ek__BackingField_4() const { return ___U3CPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_4() { return &___U3CPathU3Ek__BackingField_4; }
	inline void set_U3CPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHandledU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3, ___U3CHandledU3Ek__BackingField_5)); }
	inline bool get_U3CHandledU3Ek__BackingField_5() const { return ___U3CHandledU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CHandledU3Ek__BackingField_5() { return &___U3CHandledU3Ek__BackingField_5; }
	inline void set_U3CHandledU3Ek__BackingField_5(bool value)
	{
		___U3CHandledU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCONTEXT_T6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T2CFF1A4653CDB0903516611434AAFC6202263764_H
#define U3CU3EC__DISPLAYCLASS73_0_T2CFF1A4653CDB0903516611434AAFC6202263764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t2CFF1A4653CDB0903516611434AAFC6202263764  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass73_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t2CFF1A4653CDB0903516611434AAFC6202263764, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T2CFF1A4653CDB0903516611434AAFC6202263764_H
#ifndef U3CU3EC__DISPLAYCLASS74_0_T23A7FAFFDEC10D634154C6366C9C3FCF88072BA8_H
#define U3CU3EC__DISPLAYCLASS74_0_T23A7FAFFDEC10D634154C6366C9C3FCF88072BA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass74_0
struct  U3CU3Ec__DisplayClass74_0_t23A7FAFFDEC10D634154C6366C9C3FCF88072BA8  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass74_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_t23A7FAFFDEC10D634154C6366C9C3FCF88072BA8, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS74_0_T23A7FAFFDEC10D634154C6366C9C3FCF88072BA8_H
#ifndef JSONFORMATTERCONVERTER_T4638A0FFF2F878CDDCE8AD13250E5BB850116AB3_H
#define JSONFORMATTERCONVERTER_T4638A0FFF2F878CDDCE8AD13250E5BB850116AB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct  JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonFormatterConverter::_reader
	JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D * ____reader_0;
	// Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.JsonFormatterConverter::_contract
	JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D * ____contract_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonFormatterConverter::_member
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * ____member_2;

public:
	inline static int32_t get_offset_of__reader_0() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3, ____reader_0)); }
	inline JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D * get__reader_0() const { return ____reader_0; }
	inline JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D ** get_address_of__reader_0() { return &____reader_0; }
	inline void set__reader_0(JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D * value)
	{
		____reader_0 = value;
		Il2CppCodeGenWriteBarrier((&____reader_0), value);
	}

	inline static int32_t get_offset_of__contract_1() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3, ____contract_1)); }
	inline JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D * get__contract_1() const { return ____contract_1; }
	inline JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D ** get_address_of__contract_1() { return &____contract_1; }
	inline void set__contract_1(JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D * value)
	{
		____contract_1 = value;
		Il2CppCodeGenWriteBarrier((&____contract_1), value);
	}

	inline static int32_t get_offset_of__member_2() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3, ____member_2)); }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * get__member_2() const { return ____member_2; }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D ** get_address_of__member_2() { return &____member_2; }
	inline void set__member_2(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * value)
	{
		____member_2 = value;
		Il2CppCodeGenWriteBarrier((&____member_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONFORMATTERCONVERTER_T4638A0FFF2F878CDDCE8AD13250E5BB850116AB3_H
#ifndef JSONSERIALIZERINTERNALBASE_TFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C_H
#define JSONSERIALIZERINTERNALBASE_TFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * ____currentErrorContext_0;
	// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC * ____mappings_1;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * ___Serializer_2;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ____currentErrorContext_0)); }
	inline ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ____mappings_1)); }
	inline BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ___Serializer_2)); }
	inline JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_TFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C_H
#ifndef REFERENCEEQUALSEQUALITYCOMPARER_T5D69D6F0AAB906CE06A1F252E53639ABBC5B0553_H
#define REFERENCEEQUALSEQUALITYCOMPARER_T5D69D6F0AAB906CE06A1F252E53639ABBC5B0553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer
struct  ReferenceEqualsEqualityComparer_t5D69D6F0AAB906CE06A1F252E53639ABBC5B0553  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEEQUALSEQUALITYCOMPARER_T5D69D6F0AAB906CE06A1F252E53639ABBC5B0553_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T1E775CFB32AC53B47C94128ACB65F907AF911746_H
#define U3CU3EC__DISPLAYCLASS36_0_T1E775CFB32AC53B47C94128ACB65F907AF911746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t1E775CFB32AC53B47C94128ACB65F907AF911746  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass36_0::property
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1E775CFB32AC53B47C94128ACB65F907AF911746, ___property_0)); }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * get_property_0() const { return ___property_0; }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T1E775CFB32AC53B47C94128ACB65F907AF911746_H
#ifndef BASE64ENCODER_TFDD110DA0806C006694929CBAF56F0A47277A1CB_H
#define BASE64ENCODER_TFDD110DA0806C006694929CBAF56F0A47277A1CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.Base64Encoder
struct  Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB  : public RuntimeObject
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.Base64Encoder::_charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charsLine_0;
	// System.IO.TextWriter Newtonsoft.Json.Utilities.Base64Encoder::_writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____leftOverBytes_2;
	// System.Int32 Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytesCount
	int32_t ____leftOverBytesCount_3;

public:
	inline static int32_t get_offset_of__charsLine_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____charsLine_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charsLine_0() const { return ____charsLine_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charsLine_0() { return &____charsLine_0; }
	inline void set__charsLine_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charsLine_0 = value;
		Il2CppCodeGenWriteBarrier((&____charsLine_0), value);
	}

	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____writer_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get__writer_1() const { return ____writer_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__leftOverBytes_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____leftOverBytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__leftOverBytes_2() const { return ____leftOverBytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__leftOverBytes_2() { return &____leftOverBytes_2; }
	inline void set__leftOverBytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____leftOverBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____leftOverBytes_2), value);
	}

	inline static int32_t get_offset_of__leftOverBytesCount_3() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____leftOverBytesCount_3)); }
	inline int32_t get__leftOverBytesCount_3() const { return ____leftOverBytesCount_3; }
	inline int32_t* get_address_of__leftOverBytesCount_3() { return &____leftOverBytesCount_3; }
	inline void set__leftOverBytesCount_3(int32_t value)
	{
		____leftOverBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_TFDD110DA0806C006694929CBAF56F0A47277A1CB_H
#ifndef BUFFERUTILS_T3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05_H
#define BUFFERUTILS_T3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.BufferUtils
struct  BufferUtils_t3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERUTILS_T3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05_H
#ifndef COLLECTIONUTILS_TD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E_H
#define COLLECTIONUTILS_TD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.CollectionUtils
struct  CollectionUtils_tD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILS_TD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E_H
#ifndef CONVERTUTILS_TBC2BAD4D5698238865AD4858E2E440C6CC822D1C_H
#define CONVERTUTILS_TBC2BAD4D5698238865AD4858E2E440C6CC822D1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils
struct  ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C  : public RuntimeObject
{
public:

public:
};

struct ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode> Newtonsoft.Json.Utilities.ConvertUtils::TypeCodeMap
	Dictionary_2_t4005A15A0C68CD41BEDE0863380069CB0C668945 * ___TypeCodeMap_0;
	// Newtonsoft.Json.Utilities.TypeInformation[] Newtonsoft.Json.Utilities.ConvertUtils::PrimitiveTypeCodes
	TypeInformationU5BU5D_tB3015365C0317DD37DD1BFB2A7BA79C8AB6EA7C5* ___PrimitiveTypeCodes_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey,System.Func`2<System.Object,System.Object>> Newtonsoft.Json.Utilities.ConvertUtils::CastConverters
	ThreadSafeStore_2_tC8BA10A2953BE54DF4D6E7B5D6D4D8368BD9E216 * ___CastConverters_2;

public:
	inline static int32_t get_offset_of_TypeCodeMap_0() { return static_cast<int32_t>(offsetof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields, ___TypeCodeMap_0)); }
	inline Dictionary_2_t4005A15A0C68CD41BEDE0863380069CB0C668945 * get_TypeCodeMap_0() const { return ___TypeCodeMap_0; }
	inline Dictionary_2_t4005A15A0C68CD41BEDE0863380069CB0C668945 ** get_address_of_TypeCodeMap_0() { return &___TypeCodeMap_0; }
	inline void set_TypeCodeMap_0(Dictionary_2_t4005A15A0C68CD41BEDE0863380069CB0C668945 * value)
	{
		___TypeCodeMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeCodeMap_0), value);
	}

	inline static int32_t get_offset_of_PrimitiveTypeCodes_1() { return static_cast<int32_t>(offsetof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields, ___PrimitiveTypeCodes_1)); }
	inline TypeInformationU5BU5D_tB3015365C0317DD37DD1BFB2A7BA79C8AB6EA7C5* get_PrimitiveTypeCodes_1() const { return ___PrimitiveTypeCodes_1; }
	inline TypeInformationU5BU5D_tB3015365C0317DD37DD1BFB2A7BA79C8AB6EA7C5** get_address_of_PrimitiveTypeCodes_1() { return &___PrimitiveTypeCodes_1; }
	inline void set_PrimitiveTypeCodes_1(TypeInformationU5BU5D_tB3015365C0317DD37DD1BFB2A7BA79C8AB6EA7C5* value)
	{
		___PrimitiveTypeCodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveTypeCodes_1), value);
	}

	inline static int32_t get_offset_of_CastConverters_2() { return static_cast<int32_t>(offsetof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields, ___CastConverters_2)); }
	inline ThreadSafeStore_2_tC8BA10A2953BE54DF4D6E7B5D6D4D8368BD9E216 * get_CastConverters_2() const { return ___CastConverters_2; }
	inline ThreadSafeStore_2_tC8BA10A2953BE54DF4D6E7B5D6D4D8368BD9E216 ** get_address_of_CastConverters_2() { return &___CastConverters_2; }
	inline void set_CastConverters_2(ThreadSafeStore_2_tC8BA10A2953BE54DF4D6E7B5D6D4D8368BD9E216 * value)
	{
		___CastConverters_2 = value;
		Il2CppCodeGenWriteBarrier((&___CastConverters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTUTILS_TBC2BAD4D5698238865AD4858E2E440C6CC822D1C_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_TFECE00A2F854C63EFBA43CDD07B838BAA7225ACB_H
#define U3CU3EC__DISPLAYCLASS9_0_TFECE00A2F854C63EFBA43CDD07B838BAA7225ACB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tFECE00A2F854C63EFBA43CDD07B838BAA7225ACB  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass9_0::call
	MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tFECE00A2F854C63EFBA43CDD07B838BAA7225ACB, ___call_0)); }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_TFECE00A2F854C63EFBA43CDD07B838BAA7225ACB_H
#ifndef DATETIMEUTILS_TC9DEADC007FF0B226DAB46E416D1AC38E74BD028_H
#define DATETIMEUTILS_TC9DEADC007FF0B226DAB46E416D1AC38E74BD028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeUtils
struct  DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields
{
public:
	// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::InitialJavaScriptDateTicks
	int64_t ___InitialJavaScriptDateTicks_0;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_1;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_2;

public:
	inline static int32_t get_offset_of_InitialJavaScriptDateTicks_0() { return static_cast<int32_t>(offsetof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields, ___InitialJavaScriptDateTicks_0)); }
	inline int64_t get_InitialJavaScriptDateTicks_0() const { return ___InitialJavaScriptDateTicks_0; }
	inline int64_t* get_address_of_InitialJavaScriptDateTicks_0() { return &___InitialJavaScriptDateTicks_0; }
	inline void set_InitialJavaScriptDateTicks_0(int64_t value)
	{
		___InitialJavaScriptDateTicks_0 = value;
	}

	inline static int32_t get_offset_of_DaysToMonth365_1() { return static_cast<int32_t>(offsetof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields, ___DaysToMonth365_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_1() const { return ___DaysToMonth365_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_1() { return &___DaysToMonth365_1; }
	inline void set_DaysToMonth365_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_1), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_2() { return static_cast<int32_t>(offsetof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields, ___DaysToMonth366_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_2() const { return ___DaysToMonth366_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_2() { return &___DaysToMonth366_2; }
	inline void set_DaysToMonth366_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_2 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILS_TC9DEADC007FF0B226DAB46E416D1AC38E74BD028_H
#ifndef ENUMUTILS_T197EC73DB7EC5D919B8031377D5CC2D69A598CF2_H
#define ENUMUTILS_T197EC73DB7EC5D919B8031377D5CC2D69A598CF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils
struct  EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2  : public RuntimeObject
{
public:

public:
};

struct EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>> Newtonsoft.Json.Utilities.EnumUtils::EnumMemberNamesPerType
	ThreadSafeStore_2_t309E8D78F56F1F657D0BE3006FC386A27954D3F6 * ___EnumMemberNamesPerType_0;

public:
	inline static int32_t get_offset_of_EnumMemberNamesPerType_0() { return static_cast<int32_t>(offsetof(EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields, ___EnumMemberNamesPerType_0)); }
	inline ThreadSafeStore_2_t309E8D78F56F1F657D0BE3006FC386A27954D3F6 * get_EnumMemberNamesPerType_0() const { return ___EnumMemberNamesPerType_0; }
	inline ThreadSafeStore_2_t309E8D78F56F1F657D0BE3006FC386A27954D3F6 ** get_address_of_EnumMemberNamesPerType_0() { return &___EnumMemberNamesPerType_0; }
	inline void set_EnumMemberNamesPerType_0(ThreadSafeStore_2_t309E8D78F56F1F657D0BE3006FC386A27954D3F6 * value)
	{
		___EnumMemberNamesPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___EnumMemberNamesPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMUTILS_T197EC73DB7EC5D919B8031377D5CC2D69A598CF2_H
#ifndef U3CU3EC_TB3365424AD7B3D3C0D5964309CEEEEDDF8370308_H
#define U3CU3EC_TB3365424AD7B3D3C0D5964309CEEEEDDF8370308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils_<>c
struct  U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.EnumUtils_<>c Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 * ___U3CU3E9_0;
	// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String> Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9__1_0
	Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * ___U3CU3E9__1_0_1;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9__5_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB3365424AD7B3D3C0D5964309CEEEEDDF8370308_H
#ifndef JAVASCRIPTUTILS_TD9991015BF82F53ADF4B86734CC1CE7C18277854_H
#define JAVASCRIPTUTILS_TD9991015BF82F53ADF4B86734CC1CE7C18277854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JavaScriptUtils
struct  JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854  : public RuntimeObject
{
public:

public:
};

struct JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields
{
public:
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::SingleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___SingleQuoteCharEscapeFlags_0;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::DoubleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___DoubleQuoteCharEscapeFlags_1;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::HtmlCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___HtmlCharEscapeFlags_2;

public:
	inline static int32_t get_offset_of_SingleQuoteCharEscapeFlags_0() { return static_cast<int32_t>(offsetof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields, ___SingleQuoteCharEscapeFlags_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_SingleQuoteCharEscapeFlags_0() const { return ___SingleQuoteCharEscapeFlags_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_SingleQuoteCharEscapeFlags_0() { return &___SingleQuoteCharEscapeFlags_0; }
	inline void set_SingleQuoteCharEscapeFlags_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___SingleQuoteCharEscapeFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&___SingleQuoteCharEscapeFlags_0), value);
	}

	inline static int32_t get_offset_of_DoubleQuoteCharEscapeFlags_1() { return static_cast<int32_t>(offsetof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields, ___DoubleQuoteCharEscapeFlags_1)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_DoubleQuoteCharEscapeFlags_1() const { return ___DoubleQuoteCharEscapeFlags_1; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_DoubleQuoteCharEscapeFlags_1() { return &___DoubleQuoteCharEscapeFlags_1; }
	inline void set_DoubleQuoteCharEscapeFlags_1(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___DoubleQuoteCharEscapeFlags_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleQuoteCharEscapeFlags_1), value);
	}

	inline static int32_t get_offset_of_HtmlCharEscapeFlags_2() { return static_cast<int32_t>(offsetof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields, ___HtmlCharEscapeFlags_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_HtmlCharEscapeFlags_2() const { return ___HtmlCharEscapeFlags_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_HtmlCharEscapeFlags_2() { return &___HtmlCharEscapeFlags_2; }
	inline void set_HtmlCharEscapeFlags_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___HtmlCharEscapeFlags_2 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlCharEscapeFlags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTUTILS_TD9991015BF82F53ADF4B86734CC1CE7C18277854_H
#ifndef JSONTOKENUTILS_T13B5051387BE40F092E6758510FA5DDBED6367A0_H
#define JSONTOKENUTILS_T13B5051387BE40F092E6758510FA5DDBED6367A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JsonTokenUtils
struct  JsonTokenUtils_t13B5051387BE40F092E6758510FA5DDBED6367A0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKENUTILS_T13B5051387BE40F092E6758510FA5DDBED6367A0_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_TE5332B31FAA71C25921E7A07F71206772D69ADC4_H
#define U3CU3EC__DISPLAYCLASS3_0_TE5332B31FAA71C25921E7A07F71206772D69ADC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::c
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___c_0;
	// System.Reflection.MethodBase Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::method
	MethodBase_t * ___method_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4, ___c_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_c_0() const { return ___c_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4, ___method_1)); }
	inline MethodBase_t * get_method_1() const { return ___method_1; }
	inline MethodBase_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodBase_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_TE5332B31FAA71C25921E7A07F71206772D69ADC4_H
#ifndef MATHUTILS_TD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822_H
#define MATHUTILS_TD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MathUtils
struct  MathUtils_tD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_TD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822_H
#ifndef MISCELLANEOUSUTILS_TF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D_H
#define MISCELLANEOUSUTILS_TF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MiscellaneousUtils
struct  MiscellaneousUtils_tF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCELLANEOUSUTILS_TF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D_H
#ifndef PROPERTYNAMETABLE_TED7B465C0E33BE522A8820C254DBA116A05DED5C_H
#define PROPERTYNAMETABLE_TED7B465C0E33BE522A8820C254DBA116A05DED5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable
struct  PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_count
	int32_t ____count_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable_Entry[] Newtonsoft.Json.Utilities.PropertyNameTable::_entries
	EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A* ____entries_2;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_mask
	int32_t ____mask_3;

public:
	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__entries_2() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C, ____entries_2)); }
	inline EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A* get__entries_2() const { return ____entries_2; }
	inline EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A** get_address_of__entries_2() { return &____entries_2; }
	inline void set__entries_2(EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A* value)
	{
		____entries_2 = value;
		Il2CppCodeGenWriteBarrier((&____entries_2), value);
	}

	inline static int32_t get_offset_of__mask_3() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C, ____mask_3)); }
	inline int32_t get__mask_3() const { return ____mask_3; }
	inline int32_t* get_address_of__mask_3() { return &____mask_3; }
	inline void set__mask_3(int32_t value)
	{
		____mask_3 = value;
	}
};

struct PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::HashCodeRandomizer
	int32_t ___HashCodeRandomizer_0;

public:
	inline static int32_t get_offset_of_HashCodeRandomizer_0() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields, ___HashCodeRandomizer_0)); }
	inline int32_t get_HashCodeRandomizer_0() const { return ___HashCodeRandomizer_0; }
	inline int32_t* get_address_of_HashCodeRandomizer_0() { return &___HashCodeRandomizer_0; }
	inline void set_HashCodeRandomizer_0(int32_t value)
	{
		___HashCodeRandomizer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMETABLE_TED7B465C0E33BE522A8820C254DBA116A05DED5C_H
#ifndef ENTRY_T3580EAE80AC671315E3C9C50976A31A55F723E35_H
#define ENTRY_T3580EAE80AC671315E3C9C50976A31A55F723E35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable_Entry
struct  Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.PropertyNameTable_Entry::Value
	String_t* ___Value_0;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable_Entry::HashCode
	int32_t ___HashCode_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable_Entry Newtonsoft.Json.Utilities.PropertyNameTable_Entry::Next
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 * ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35, ___Next_2)); }
	inline Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 * get_Next_2() const { return ___Next_2; }
	inline Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3580EAE80AC671315E3C9C50976A31A55F723E35_H
#ifndef REFLECTIONDELEGATEFACTORY_T75B5A24FB6BC80F694FD9590D67B803C718164A7_H
#define REFLECTIONDELEGATEFACTORY_T75B5A24FB6BC80F694FD9590D67B803C718164A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct  ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONDELEGATEFACTORY_T75B5A24FB6BC80F694FD9590D67B803C718164A7_H
#ifndef REFLECTIONMEMBER_T6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028_H
#define REFLECTIONMEMBER_T6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionMember
struct  ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.ReflectionMember::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_0;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Getter>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetterU3Ek__BackingField_1;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Setter>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetterU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028, ___U3CMemberTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_0() const { return ___U3CMemberTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_0() { return &___U3CMemberTypeU3Ek__BackingField_0; }
	inline void set_U3CMemberTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGetterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028, ___U3CGetterU3Ek__BackingField_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetterU3Ek__BackingField_1() const { return ___U3CGetterU3Ek__BackingField_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetterU3Ek__BackingField_1() { return &___U3CGetterU3Ek__BackingField_1; }
	inline void set_U3CGetterU3Ek__BackingField_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetterU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSetterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028, ___U3CSetterU3Ek__BackingField_2)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetterU3Ek__BackingField_2() const { return ___U3CSetterU3Ek__BackingField_2; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetterU3Ek__BackingField_2() { return &___U3CSetterU3Ek__BackingField_2; }
	inline void set_U3CSetterU3Ek__BackingField_2(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetterU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMEMBER_T6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028_H
#ifndef REFLECTIONOBJECT_T3EDC4C62AF6FE6E5246ED34686ED44305157331D_H
#define REFLECTIONOBJECT_T3EDC4C62AF6FE6E5246ED34686ED44305157331D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject
struct  ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::<Creator>k__BackingField
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ___U3CCreatorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::<Members>k__BackingField
	RuntimeObject* ___U3CMembersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D, ___U3CCreatorU3Ek__BackingField_0)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get_U3CCreatorU3Ek__BackingField_0() const { return ___U3CCreatorU3Ek__BackingField_0; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of_U3CCreatorU3Ek__BackingField_0() { return &___U3CCreatorU3Ek__BackingField_0; }
	inline void set_U3CCreatorU3Ek__BackingField_0(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		___U3CCreatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D, ___U3CMembersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CMembersU3Ek__BackingField_1() const { return ___U3CMembersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CMembersU3Ek__BackingField_1() { return &___U3CMembersU3Ek__BackingField_1; }
	inline void set_U3CMembersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CMembersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMembersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONOBJECT_T3EDC4C62AF6FE6E5246ED34686ED44305157331D_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T47E49B27524CC2B7F71A57F312C167AF21D0AE3F_H
#define U3CU3EC__DISPLAYCLASS13_0_T47E49B27524CC2B7F71A57F312C167AF21D0AE3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F  : public RuntimeObject
{
public:
	// System.Func`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_0::ctor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___ctor_0;

public:
	inline static int32_t get_offset_of_ctor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F, ___ctor_0)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_ctor_0() const { return ___ctor_0; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_ctor_0() { return &___ctor_0; }
	inline void set_ctor_0(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___ctor_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T47E49B27524CC2B7F71A57F312C167AF21D0AE3F_H
#ifndef U3CU3EC__DISPLAYCLASS13_1_T3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF_H
#define U3CU3EC__DISPLAYCLASS13_1_T3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_1
struct  U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_1::call
	MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF, ___call_0)); }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_1_T3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF_H
#ifndef U3CU3EC__DISPLAYCLASS13_2_T2998AB8DAD021CA1644A3C6568545C2CC1153FFC_H
#define U3CU3EC__DISPLAYCLASS13_2_T2998AB8DAD021CA1644A3C6568545C2CC1153FFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_2
struct  U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_2::call
	MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC, ___call_0)); }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t6DD0C89807E335755682DC84F96ECD55CFDEC73D * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_2_T2998AB8DAD021CA1644A3C6568545C2CC1153FFC_H
#ifndef REFLECTIONUTILS_T82A3691233682309E0C44776B06934B52C73126C_H
#define REFLECTIONUTILS_T82A3691233682309E0C44776B06934B52C73126C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields
{
public:
	// System.Type[] Newtonsoft.Json.Utilities.ReflectionUtils::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T82A3691233682309E0C44776B06934B52C73126C_H
#ifndef U3CU3EC_TB59F290C0541332FDC4EFBA63CA7763266112695_H
#define U3CU3EC_TB59F290C0541332FDC4EFBA63CA7763266112695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c
struct  U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionUtils_<>c Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__10_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__10_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.String> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__29_0
	Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * ___U3CU3E9__29_0_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__37_0
	Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * ___U3CU3E9__37_0_3;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__39_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__39_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__29_0_2)); }
	inline Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * get_U3CU3E9__29_0_2() const { return ___U3CU3E9__29_0_2; }
	inline Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 ** get_address_of_U3CU3E9__29_0_2() { return &___U3CU3E9__29_0_2; }
	inline void set_U3CU3E9__29_0_2(Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * value)
	{
		___U3CU3E9__29_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__37_0_3)); }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * get_U3CU3E9__37_0_3() const { return ___U3CU3E9__37_0_3; }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 ** get_address_of_U3CU3E9__37_0_3() { return &___U3CU3E9__37_0_3; }
	inline void set_U3CU3E9__37_0_3(Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * value)
	{
		___U3CU3E9__37_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__39_0_4)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__39_0_4() const { return ___U3CU3E9__39_0_4; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__39_0_4() { return &___U3CU3E9__39_0_4; }
	inline void set_U3CU3E9__39_0_4(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__39_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__39_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB59F290C0541332FDC4EFBA63CA7763266112695_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_TFE47362D037053FFE594182FD02D4ADC250F7D47_H
#define U3CU3EC__DISPLAYCLASS42_0_TFE47362D037053FFE594182FD02D4ADC250F7D47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass42_0::subTypeProperty
	PropertyInfo_t * ___subTypeProperty_0;

public:
	inline static int32_t get_offset_of_subTypeProperty_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47, ___subTypeProperty_0)); }
	inline PropertyInfo_t * get_subTypeProperty_0() const { return ___subTypeProperty_0; }
	inline PropertyInfo_t ** get_address_of_subTypeProperty_0() { return &___subTypeProperty_0; }
	inline void set_subTypeProperty_0(PropertyInfo_t * value)
	{
		___subTypeProperty_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypeProperty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_TFE47362D037053FFE594182FD02D4ADC250F7D47_H
#ifndef STRINGREFERENCEEXTENSIONS_T932DAA64E777B822FD41227E19E46718B156F23C_H
#define STRINGREFERENCEEXTENSIONS_T932DAA64E777B822FD41227E19E46718B156F23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReferenceExtensions
struct  StringReferenceExtensions_t932DAA64E777B822FD41227E19E46718B156F23C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREFERENCEEXTENSIONS_T932DAA64E777B822FD41227E19E46718B156F23C_H
#ifndef STRINGUTILS_T5B73278499B8A8A66F9A2E01219BAAAD2B1FD011_H
#define STRINGUTILS_T5B73278499B8A8A66F9A2E01219BAAAD2B1FD011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils
struct  StringUtils_t5B73278499B8A8A66F9A2E01219BAAAD2B1FD011  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T5B73278499B8A8A66F9A2E01219BAAAD2B1FD011_H
#ifndef TYPEEXTENSIONS_T7164B8FEB0BFBA07C6FC57687C128C608660954B_H
#define TYPEEXTENSIONS_T7164B8FEB0BFBA07C6FC57687C128C608660954B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions
struct  TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T7164B8FEB0BFBA07C6FC57687C128C608660954B_H
#ifndef VALIDATIONUTILS_T1BC13C7423D7B21E9C058B217AAA1F0D737BB299_H
#define VALIDATIONUTILS_T1BC13C7423D7B21E9C058B217AAA1F0D737BB299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ValidationUtils
struct  ValidationUtils_t1BC13C7423D7B21E9C058B217AAA1F0D737BB299  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONUTILS_T1BC13C7423D7B21E9C058B217AAA1F0D737BB299_H
#ifndef COLLECTION_1_T64B71A6F8B9FB396F157956715B7994333DD9AEB_H
#define COLLECTION_1_T64B71A6F8B9FB396F157956715B7994333DD9AEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Serialization.JsonProperty>
struct  Collection_1_t64B71A6F8B9FB396F157956715B7994333DD9AEB  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t64B71A6F8B9FB396F157956715B7994333DD9AEB, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t64B71A6F8B9FB396F157956715B7994333DD9AEB, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T64B71A6F8B9FB396F157956715B7994333DD9AEB_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#define SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationBinder
struct  SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DEFAULTSERIALIZATIONBINDER_TA8808BA6AD2615BCBE34253055B79008F9BC4C16_H
#define DEFAULTSERIALIZATIONBINDER_TA8808BA6AD2615BCBE34253055B79008F9BC4C16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder
struct  DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16  : public SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder_TypeNameKey,System.Type> Newtonsoft.Json.Serialization.DefaultSerializationBinder::_typeCache
	ThreadSafeStore_2_tEFC711003B7C7BD4AD5E17A00C44BBA99C0B573D * ____typeCache_1;

public:
	inline static int32_t get_offset_of__typeCache_1() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16, ____typeCache_1)); }
	inline ThreadSafeStore_2_tEFC711003B7C7BD4AD5E17A00C44BBA99C0B573D * get__typeCache_1() const { return ____typeCache_1; }
	inline ThreadSafeStore_2_tEFC711003B7C7BD4AD5E17A00C44BBA99C0B573D ** get_address_of__typeCache_1() { return &____typeCache_1; }
	inline void set__typeCache_1(ThreadSafeStore_2_tEFC711003B7C7BD4AD5E17A00C44BBA99C0B573D * value)
	{
		____typeCache_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeCache_1), value);
	}
};

struct DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultSerializationBinder Newtonsoft.Json.Serialization.DefaultSerializationBinder::Instance
	DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16_StaticFields, ___Instance_0)); }
	inline DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16 * get_Instance_0() const { return ___Instance_0; }
	inline DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSERIALIZATIONBINDER_TA8808BA6AD2615BCBE34253055B79008F9BC4C16_H
#ifndef TYPENAMEKEY_TDAACDE517FD535C0637BDA68F01B7A44135A6879_H
#define TYPENAMEKEY_TDAACDE517FD535C0637BDA68F01B7A44135A6879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder_TypeNameKey
struct  TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879 
{
public:
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder_TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder_TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_TDAACDE517FD535C0637BDA68F01B7A44135A6879_H
#ifndef ERROREVENTARGS_TF61F8C43A0EA33560C42850788318497AE985A3E_H
#define ERROREVENTARGS_TF61F8C43A0EA33560C42850788318497AE985A3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ErrorEventArgs
struct  ErrorEventArgs_tF61F8C43A0EA33560C42850788318497AE985A3E  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Object Newtonsoft.Json.Serialization.ErrorEventArgs::<CurrentObject>k__BackingField
	RuntimeObject * ___U3CCurrentObjectU3Ek__BackingField_1;
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.ErrorEventArgs::<ErrorContext>k__BackingField
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * ___U3CErrorContextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_tF61F8C43A0EA33560C42850788318497AE985A3E, ___U3CCurrentObjectU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CCurrentObjectU3Ek__BackingField_1() const { return ___U3CCurrentObjectU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CCurrentObjectU3Ek__BackingField_1() { return &___U3CCurrentObjectU3Ek__BackingField_1; }
	inline void set_U3CCurrentObjectU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CCurrentObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentObjectU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CErrorContextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_tF61F8C43A0EA33560C42850788318497AE985A3E, ___U3CErrorContextU3Ek__BackingField_2)); }
	inline ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * get_U3CErrorContextU3Ek__BackingField_2() const { return ___U3CErrorContextU3Ek__BackingField_2; }
	inline ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 ** get_address_of_U3CErrorContextU3Ek__BackingField_2() { return &___U3CErrorContextU3Ek__BackingField_2; }
	inline void set_U3CErrorContextU3Ek__BackingField_2(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * value)
	{
		___U3CErrorContextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorContextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_TF61F8C43A0EA33560C42850788318497AE985A3E_H
#ifndef JSONSERIALIZERINTERNALREADER_T8DF542A0658E40C69F7C9A5F8D013D794B9AD78D_H
#define JSONSERIALIZERINTERNALREADER_T8DF542A0658E40C69F7C9A5F8D013D794B9AD78D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct  JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D  : public JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALREADER_T8DF542A0658E40C69F7C9A5F8D013D794B9AD78D_H
#ifndef RESOLVERCONTRACTKEY_TAF54DD0CBE88065705D0CF2B4676DAD2977328EB_H
#define RESOLVERCONTRACTKEY_TAF54DD0CBE88065705D0CF2B4676DAD2977328EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ResolverContractKey
struct  ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB 
{
public:
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_resolverType
	Type_t * ____resolverType_0;
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_contractType
	Type_t * ____contractType_1;

public:
	inline static int32_t get_offset_of__resolverType_0() { return static_cast<int32_t>(offsetof(ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB, ____resolverType_0)); }
	inline Type_t * get__resolverType_0() const { return ____resolverType_0; }
	inline Type_t ** get_address_of__resolverType_0() { return &____resolverType_0; }
	inline void set__resolverType_0(Type_t * value)
	{
		____resolverType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resolverType_0), value);
	}

	inline static int32_t get_offset_of__contractType_1() { return static_cast<int32_t>(offsetof(ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB, ____contractType_1)); }
	inline Type_t * get__contractType_1() const { return ____contractType_1; }
	inline Type_t ** get_address_of__contractType_1() { return &____contractType_1; }
	inline void set__contractType_1(Type_t * value)
	{
		____contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB_marshaled_pinvoke
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB_marshaled_com
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
#endif // RESOLVERCONTRACTKEY_TAF54DD0CBE88065705D0CF2B4676DAD2977328EB_H
#ifndef TYPECONVERTKEY_T7785799B96525F101538E1CE1C8E78659B3C6E51_H
#define TYPECONVERTKEY_T7785799B96525F101538E1CE1C8E78659B3C6E51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey
struct  TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T7785799B96525F101538E1CE1C8E78659B3C6E51_H
#ifndef LATEBOUNDREFLECTIONDELEGATEFACTORY_T342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_H
#define LATEBOUNDREFLECTIONDELEGATEFACTORY_T342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct  LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7  : public ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7
{
public:

public:
};

struct LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::_instance
	LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields, ____instance_0)); }
	inline LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 * get__instance_0() const { return ____instance_0; }
	inline LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEBOUNDREFLECTIONDELEGATEFACTORY_T342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_H
#ifndef STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#define STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringBuffer
struct  StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____buffer_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4, ____buffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#ifndef STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#define STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReference
struct  StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringReference::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#ifndef KEYEDCOLLECTION_2_T4E603F3BBA04445094F37EAD307CF17A169CBF77_H
#define KEYEDCOLLECTION_2_T4E603F3BBA04445094F37EAD307CF17A169CBF77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>
struct  KeyedCollection_2_t4E603F3BBA04445094F37EAD307CF17A169CBF77  : public Collection_1_t64B71A6F8B9FB396F157956715B7994333DD9AEB
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.ObjectModel.KeyedCollection`2::comparer
	RuntimeObject* ___comparer_2;
	// System.Collections.Generic.Dictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2::dict
	Dictionary_2_t1620483B111170AB5F6CE317EB940AE6A968909F * ___dict_3;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::keyCount
	int32_t ___keyCount_4;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::threshold
	int32_t ___threshold_5;

public:
	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t4E603F3BBA04445094F37EAD307CF17A169CBF77, ___comparer_2)); }
	inline RuntimeObject* get_comparer_2() const { return ___comparer_2; }
	inline RuntimeObject** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(RuntimeObject* value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_2), value);
	}

	inline static int32_t get_offset_of_dict_3() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t4E603F3BBA04445094F37EAD307CF17A169CBF77, ___dict_3)); }
	inline Dictionary_2_t1620483B111170AB5F6CE317EB940AE6A968909F * get_dict_3() const { return ___dict_3; }
	inline Dictionary_2_t1620483B111170AB5F6CE317EB940AE6A968909F ** get_address_of_dict_3() { return &___dict_3; }
	inline void set_dict_3(Dictionary_2_t1620483B111170AB5F6CE317EB940AE6A968909F * value)
	{
		___dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___dict_3), value);
	}

	inline static int32_t get_offset_of_keyCount_4() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t4E603F3BBA04445094F37EAD307CF17A169CBF77, ___keyCount_4)); }
	inline int32_t get_keyCount_4() const { return ___keyCount_4; }
	inline int32_t* get_address_of_keyCount_4() { return &___keyCount_4; }
	inline void set_keyCount_4(int32_t value)
	{
		___keyCount_4 = value;
	}

	inline static int32_t get_offset_of_threshold_5() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t4E603F3BBA04445094F37EAD307CF17A169CBF77, ___threshold_5)); }
	inline int32_t get_threshold_5() const { return ___threshold_5; }
	inline int32_t* get_address_of_threshold_5() { return &___threshold_5; }
	inline void set_threshold_5(int32_t value)
	{
		___threshold_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDCOLLECTION_2_T4E603F3BBA04445094F37EAD307CF17A169CBF77_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#define DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#ifndef MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#define MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#ifndef NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#define NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifndef OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#define OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifndef READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#define READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#ifndef REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#define REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifndef REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#define REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#ifndef JSONCONTRACTTYPE_T5DEDE285EB2CD0B99C86F0F53728B7433E66516F_H
#define JSONCONTRACTTYPE_T5DEDE285EB2CD0B99C86F0F53728B7433E66516F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContractType
struct  JsonContractType_t5DEDE285EB2CD0B99C86F0F53728B7433E66516F 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonContractType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContractType_t5DEDE285EB2CD0B99C86F0F53728B7433E66516F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T5DEDE285EB2CD0B99C86F0F53728B7433E66516F_H
#ifndef JSONPROPERTYCOLLECTION_TDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD_H
#define JSONPROPERTYCOLLECTION_TDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct  JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD  : public KeyedCollection_2_t4E603F3BBA04445094F37EAD307CF17A169CBF77
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonPropertyCollection::_type
	Type_t * ____type_6;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonPropertyCollection::_list
	List_1_t2100AEEAF4CF6BDF67FCB0331C25090DB2221E52 * ____list_7;

public:
	inline static int32_t get_offset_of__type_6() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD, ____type_6)); }
	inline Type_t * get__type_6() const { return ____type_6; }
	inline Type_t ** get_address_of__type_6() { return &____type_6; }
	inline void set__type_6(Type_t * value)
	{
		____type_6 = value;
		Il2CppCodeGenWriteBarrier((&____type_6), value);
	}

	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD, ____list_7)); }
	inline List_1_t2100AEEAF4CF6BDF67FCB0331C25090DB2221E52 * get__list_7() const { return ____list_7; }
	inline List_1_t2100AEEAF4CF6BDF67FCB0331C25090DB2221E52 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(List_1_t2100AEEAF4CF6BDF67FCB0331C25090DB2221E52 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYCOLLECTION_TDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD_H
#ifndef PROPERTYPRESENCE_T7CF971410D059BAB8A0C53A74F7F6369C1AA991F_H
#define PROPERTYPRESENCE_T7CF971410D059BAB8A0C53A74F7F6369C1AA991F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence
struct  PropertyPresence_t7CF971410D059BAB8A0C53A74F7F6369C1AA991F 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyPresence_t7CF971410D059BAB8A0C53A74F7F6369C1AA991F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYPRESENCE_T7CF971410D059BAB8A0C53A74F7F6369C1AA991F_H
#ifndef TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#define TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifndef CONVERTRESULT_T98FEB5BCA2057684971BF398D291A95A55CF6256_H
#define CONVERTRESULT_T98FEB5BCA2057684971BF398D291A95A55CF6256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_ConvertResult
struct  ConvertResult_t98FEB5BCA2057684971BF398D291A95A55CF6256 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ConvertUtils_ConvertResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConvertResult_t98FEB5BCA2057684971BF398D291A95A55CF6256, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTRESULT_T98FEB5BCA2057684971BF398D291A95A55CF6256_H
#ifndef PARSERESULT_T104A186E35E63BAC5313B1CF610BBADECCDFF2AB_H
#define PARSERESULT_T104A186E35E63BAC5313B1CF610BBADECCDFF2AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParseResult
struct  ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParseResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERESULT_T104A186E35E63BAC5313B1CF610BBADECCDFF2AB_H
#ifndef PARSERTIMEZONE_T8D191D9C90FEC1F49FB3CA4ED245E867A189ED64_H
#define PARSERTIMEZONE_T8D191D9C90FEC1F49FB3CA4ED245E867A189ED64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParserTimeZone
struct  ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParserTimeZone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTIMEZONE_T8D191D9C90FEC1F49FB3CA4ED245E867A189ED64_H
#ifndef PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#define PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef DEFAULTCONTRACTRESOLVER_TD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_H
#define DEFAULTCONTRACTRESOLVER_TD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver
struct  DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::_instanceState
	DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB * ____instanceState_4;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::_sharedCache
	bool ____sharedCache_5;
	// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::<DefaultMembersSearchFlags>k__BackingField
	int32_t ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<SerializeCompilerGeneratedMembers>k__BackingField
	bool ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableInterface>k__BackingField
	bool ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableAttribute>k__BackingField
	bool ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__instanceState_4() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44, ____instanceState_4)); }
	inline DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB * get__instanceState_4() const { return ____instanceState_4; }
	inline DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB ** get_address_of__instanceState_4() { return &____instanceState_4; }
	inline void set__instanceState_4(DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB * value)
	{
		____instanceState_4 = value;
		Il2CppCodeGenWriteBarrier((&____instanceState_4), value);
	}

	inline static int32_t get_offset_of__sharedCache_5() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44, ____sharedCache_5)); }
	inline bool get__sharedCache_5() const { return ____sharedCache_5; }
	inline bool* get_address_of__sharedCache_5() { return &____sharedCache_5; }
	inline void set__sharedCache_5(bool value)
	{
		____sharedCache_5 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44, ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6)); }
	inline int32_t get_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() const { return ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() { return &___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6; }
	inline void set_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(int32_t value)
	{
		___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44, ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7)); }
	inline bool get_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() const { return ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() { return &___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7; }
	inline void set_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(bool value)
	{
		___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44, ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() const { return ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() { return &___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8; }
	inline void set_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44, ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9)); }
	inline bool get_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() const { return ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() { return &___U3CIgnoreSerializableAttributeU3Ek__BackingField_9; }
	inline void set_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(bool value)
	{
		___U3CIgnoreSerializableAttributeU3Ek__BackingField_9 = value;
	}
};

struct DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::_instance
	RuntimeObject* ____instance_0;
	// Newtonsoft.Json.JsonConverter[] Newtonsoft.Json.Serialization.DefaultContractResolver::BuiltInConverters
	JsonConverterU5BU5D_t54F68FC2F7C268CA67090069816699D6427B1711* ___BuiltInConverters_1;
	// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver::TypeContractCacheLock
	RuntimeObject * ___TypeContractCacheLock_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::_sharedState
	DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB * ____sharedState_3;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields, ____instance_0)); }
	inline RuntimeObject* get__instance_0() const { return ____instance_0; }
	inline RuntimeObject** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject* value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of_BuiltInConverters_1() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields, ___BuiltInConverters_1)); }
	inline JsonConverterU5BU5D_t54F68FC2F7C268CA67090069816699D6427B1711* get_BuiltInConverters_1() const { return ___BuiltInConverters_1; }
	inline JsonConverterU5BU5D_t54F68FC2F7C268CA67090069816699D6427B1711** get_address_of_BuiltInConverters_1() { return &___BuiltInConverters_1; }
	inline void set_BuiltInConverters_1(JsonConverterU5BU5D_t54F68FC2F7C268CA67090069816699D6427B1711* value)
	{
		___BuiltInConverters_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltInConverters_1), value);
	}

	inline static int32_t get_offset_of_TypeContractCacheLock_2() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields, ___TypeContractCacheLock_2)); }
	inline RuntimeObject * get_TypeContractCacheLock_2() const { return ___TypeContractCacheLock_2; }
	inline RuntimeObject ** get_address_of_TypeContractCacheLock_2() { return &___TypeContractCacheLock_2; }
	inline void set_TypeContractCacheLock_2(RuntimeObject * value)
	{
		___TypeContractCacheLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeContractCacheLock_2), value);
	}

	inline static int32_t get_offset_of__sharedState_3() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields, ____sharedState_3)); }
	inline DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB * get__sharedState_3() const { return ____sharedState_3; }
	inline DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB ** get_address_of__sharedState_3() { return &____sharedState_3; }
	inline void set__sharedState_3(DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB * value)
	{
		____sharedState_3 = value;
		Il2CppCodeGenWriteBarrier((&____sharedState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVER_TD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_H
#ifndef JSONCONTRACT_T3521BC52C95CBCD89F85AF8A46062314C12D0843_H
#define JSONCONTRACT_T3521BC52C95CBCD89F85AF8A46062314C12D0843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract
struct  JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Newtonsoft.Json.ReadType Newtonsoft.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Newtonsoft.Json.Serialization.JsonContractType Newtonsoft.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_16;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CConverterU3Ek__BackingField_17;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onDeserializedCallbacks_9)); }
	inline List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_T3521BC52C95CBCD89F85AF8A46062314C12D0843_H
#ifndef DATETIMEPARSER_T8ACDADBD4CD122642DC91291733504AD8202D6EE_H
#define DATETIMEPARSER_T8ACDADBD4CD122642DC91291733504AD8202D6EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeParser
struct  DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Year
	int32_t ___Year_0;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Month
	int32_t ___Month_1;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Day
	int32_t ___Day_2;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Hour
	int32_t ___Hour_3;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Minute
	int32_t ___Minute_4;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Second
	int32_t ___Second_5;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Fraction
	int32_t ___Fraction_6;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneHour
	int32_t ___ZoneHour_7;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneMinute
	int32_t ___ZoneMinute_8;
	// Newtonsoft.Json.Utilities.ParserTimeZone Newtonsoft.Json.Utilities.DateTimeParser::Zone
	int32_t ___Zone_9;
	// System.Char[] Newtonsoft.Json.Utilities.DateTimeParser::_text
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____text_10;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::_end
	int32_t ____end_11;

public:
	inline static int32_t get_offset_of_Year_0() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Year_0)); }
	inline int32_t get_Year_0() const { return ___Year_0; }
	inline int32_t* get_address_of_Year_0() { return &___Year_0; }
	inline void set_Year_0(int32_t value)
	{
		___Year_0 = value;
	}

	inline static int32_t get_offset_of_Month_1() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Month_1)); }
	inline int32_t get_Month_1() const { return ___Month_1; }
	inline int32_t* get_address_of_Month_1() { return &___Month_1; }
	inline void set_Month_1(int32_t value)
	{
		___Month_1 = value;
	}

	inline static int32_t get_offset_of_Day_2() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Day_2)); }
	inline int32_t get_Day_2() const { return ___Day_2; }
	inline int32_t* get_address_of_Day_2() { return &___Day_2; }
	inline void set_Day_2(int32_t value)
	{
		___Day_2 = value;
	}

	inline static int32_t get_offset_of_Hour_3() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Hour_3)); }
	inline int32_t get_Hour_3() const { return ___Hour_3; }
	inline int32_t* get_address_of_Hour_3() { return &___Hour_3; }
	inline void set_Hour_3(int32_t value)
	{
		___Hour_3 = value;
	}

	inline static int32_t get_offset_of_Minute_4() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Minute_4)); }
	inline int32_t get_Minute_4() const { return ___Minute_4; }
	inline int32_t* get_address_of_Minute_4() { return &___Minute_4; }
	inline void set_Minute_4(int32_t value)
	{
		___Minute_4 = value;
	}

	inline static int32_t get_offset_of_Second_5() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Second_5)); }
	inline int32_t get_Second_5() const { return ___Second_5; }
	inline int32_t* get_address_of_Second_5() { return &___Second_5; }
	inline void set_Second_5(int32_t value)
	{
		___Second_5 = value;
	}

	inline static int32_t get_offset_of_Fraction_6() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Fraction_6)); }
	inline int32_t get_Fraction_6() const { return ___Fraction_6; }
	inline int32_t* get_address_of_Fraction_6() { return &___Fraction_6; }
	inline void set_Fraction_6(int32_t value)
	{
		___Fraction_6 = value;
	}

	inline static int32_t get_offset_of_ZoneHour_7() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___ZoneHour_7)); }
	inline int32_t get_ZoneHour_7() const { return ___ZoneHour_7; }
	inline int32_t* get_address_of_ZoneHour_7() { return &___ZoneHour_7; }
	inline void set_ZoneHour_7(int32_t value)
	{
		___ZoneHour_7 = value;
	}

	inline static int32_t get_offset_of_ZoneMinute_8() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___ZoneMinute_8)); }
	inline int32_t get_ZoneMinute_8() const { return ___ZoneMinute_8; }
	inline int32_t* get_address_of_ZoneMinute_8() { return &___ZoneMinute_8; }
	inline void set_ZoneMinute_8(int32_t value)
	{
		___ZoneMinute_8 = value;
	}

	inline static int32_t get_offset_of_Zone_9() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Zone_9)); }
	inline int32_t get_Zone_9() const { return ___Zone_9; }
	inline int32_t* get_address_of_Zone_9() { return &___Zone_9; }
	inline void set_Zone_9(int32_t value)
	{
		___Zone_9 = value;
	}

	inline static int32_t get_offset_of__text_10() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ____text_10)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__text_10() const { return ____text_10; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__text_10() { return &____text_10; }
	inline void set__text_10(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____text_10 = value;
		Il2CppCodeGenWriteBarrier((&____text_10), value);
	}

	inline static int32_t get_offset_of__end_11() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ____end_11)); }
	inline int32_t get__end_11() const { return ____end_11; }
	inline int32_t* get_address_of__end_11() { return &____end_11; }
	inline void set__end_11(int32_t value)
	{
		____end_11 = value;
	}
};

struct DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields
{
public:
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeParser::Power10
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Power10_12;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy
	int32_t ___Lzyyyy_13;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_
	int32_t ___Lzyyyy__14;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_15;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__16;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_17;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_18;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH
	int32_t ___LzHH_19;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_
	int32_t ___LzHH__20;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm
	int32_t ___LzHH_mm_21;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_
	int32_t ___LzHH_mm__22;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_23;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_
	int32_t ___Lz__24;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_zz
	int32_t ___Lz_zz_25;

public:
	inline static int32_t get_offset_of_Power10_12() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Power10_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Power10_12() const { return ___Power10_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Power10_12() { return &___Power10_12; }
	inline void set_Power10_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Power10_12 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_12), value);
	}

	inline static int32_t get_offset_of_Lzyyyy_13() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_13)); }
	inline int32_t get_Lzyyyy_13() const { return ___Lzyyyy_13; }
	inline int32_t* get_address_of_Lzyyyy_13() { return &___Lzyyyy_13; }
	inline void set_Lzyyyy_13(int32_t value)
	{
		___Lzyyyy_13 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__14() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy__14)); }
	inline int32_t get_Lzyyyy__14() const { return ___Lzyyyy__14; }
	inline int32_t* get_address_of_Lzyyyy__14() { return &___Lzyyyy__14; }
	inline void set_Lzyyyy__14(int32_t value)
	{
		___Lzyyyy__14 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_15() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM_15)); }
	inline int32_t get_Lzyyyy_MM_15() const { return ___Lzyyyy_MM_15; }
	inline int32_t* get_address_of_Lzyyyy_MM_15() { return &___Lzyyyy_MM_15; }
	inline void set_Lzyyyy_MM_15(int32_t value)
	{
		___Lzyyyy_MM_15 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__16() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM__16)); }
	inline int32_t get_Lzyyyy_MM__16() const { return ___Lzyyyy_MM__16; }
	inline int32_t* get_address_of_Lzyyyy_MM__16() { return &___Lzyyyy_MM__16; }
	inline void set_Lzyyyy_MM__16(int32_t value)
	{
		___Lzyyyy_MM__16 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_17() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM_dd_17)); }
	inline int32_t get_Lzyyyy_MM_dd_17() const { return ___Lzyyyy_MM_dd_17; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_17() { return &___Lzyyyy_MM_dd_17; }
	inline void set_Lzyyyy_MM_dd_17(int32_t value)
	{
		___Lzyyyy_MM_dd_17 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_18() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM_ddT_18)); }
	inline int32_t get_Lzyyyy_MM_ddT_18() const { return ___Lzyyyy_MM_ddT_18; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_18() { return &___Lzyyyy_MM_ddT_18; }
	inline void set_Lzyyyy_MM_ddT_18(int32_t value)
	{
		___Lzyyyy_MM_ddT_18 = value;
	}

	inline static int32_t get_offset_of_LzHH_19() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_19)); }
	inline int32_t get_LzHH_19() const { return ___LzHH_19; }
	inline int32_t* get_address_of_LzHH_19() { return &___LzHH_19; }
	inline void set_LzHH_19(int32_t value)
	{
		___LzHH_19 = value;
	}

	inline static int32_t get_offset_of_LzHH__20() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH__20)); }
	inline int32_t get_LzHH__20() const { return ___LzHH__20; }
	inline int32_t* get_address_of_LzHH__20() { return &___LzHH__20; }
	inline void set_LzHH__20(int32_t value)
	{
		___LzHH__20 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_21() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_mm_21)); }
	inline int32_t get_LzHH_mm_21() const { return ___LzHH_mm_21; }
	inline int32_t* get_address_of_LzHH_mm_21() { return &___LzHH_mm_21; }
	inline void set_LzHH_mm_21(int32_t value)
	{
		___LzHH_mm_21 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__22() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_mm__22)); }
	inline int32_t get_LzHH_mm__22() const { return ___LzHH_mm__22; }
	inline int32_t* get_address_of_LzHH_mm__22() { return &___LzHH_mm__22; }
	inline void set_LzHH_mm__22(int32_t value)
	{
		___LzHH_mm__22 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_23() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_mm_ss_23)); }
	inline int32_t get_LzHH_mm_ss_23() const { return ___LzHH_mm_ss_23; }
	inline int32_t* get_address_of_LzHH_mm_ss_23() { return &___LzHH_mm_ss_23; }
	inline void set_LzHH_mm_ss_23(int32_t value)
	{
		___LzHH_mm_ss_23 = value;
	}

	inline static int32_t get_offset_of_Lz__24() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lz__24)); }
	inline int32_t get_Lz__24() const { return ___Lz__24; }
	inline int32_t* get_address_of_Lz__24() { return &___Lz__24; }
	inline void set_Lz__24(int32_t value)
	{
		___Lz__24 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_25() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lz_zz_25)); }
	inline int32_t get_Lz_zz_25() const { return ___Lz_zz_25; }
	inline int32_t* get_address_of_Lz_zz_25() { return &___Lz_zz_25; }
	inline void set_Lz_zz_25(int32_t value)
	{
		___Lz_zz_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_marshaled_pinvoke
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_marshaled_com
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
#endif // DATETIMEPARSER_T8ACDADBD4CD122642DC91291733504AD8202D6EE_H
#ifndef TYPEINFORMATION_T4C41A70CA0C11647DFC962998B7D6B65DE6AAF75_H
#define TYPEINFORMATION_T4C41A70CA0C11647DFC962998B7D6B65DE6AAF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeInformation
struct  TypeInformation_t4C41A70CA0C11647DFC962998B7D6B65DE6AAF75  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.TypeInformation::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.TypeInformation::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TypeInformation_t4C41A70CA0C11647DFC962998B7D6B65DE6AAF75, ___U3CTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TypeInformation_t4C41A70CA0C11647DFC962998B7D6B65DE6AAF75, ___U3CTypeCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_1() const { return ___U3CTypeCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_1() { return &___U3CTypeCodeU3Ek__BackingField_1; }
	inline void set_U3CTypeCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFORMATION_T4C41A70CA0C11647DFC962998B7D6B65DE6AAF75_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_TF4FB621C8DBE38E3A39FED564E870F44A4D8E718_H
#define NULLABLE_1_TF4FB621C8DBE38E3A39FED564E870F44A4D8E718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct  Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TF4FB621C8DBE38E3A39FED564E870F44A4D8E718_H
#ifndef NULLABLE_1_T03E506703700988207428BB69043E307A2C53E53_H
#define NULLABLE_1_T03E506703700988207428BB69043E307A2C53E53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct  Nullable_1_t03E506703700988207428BB69043E307A2C53E53 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t03E506703700988207428BB69043E307A2C53E53, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t03E506703700988207428BB69043E307A2C53E53, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T03E506703700988207428BB69043E307A2C53E53_H
#ifndef NULLABLE_1_T898BA8195CF65B48365CBABBCEB55A053E136531_H
#define NULLABLE_1_T898BA8195CF65B48365CBABBCEB55A053E136531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct  Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T898BA8195CF65B48365CBABBCEB55A053E136531_H
#ifndef NULLABLE_1_T1C5FD937327E0ADA20126B4AC9A9A84A94847955_H
#define NULLABLE_1_T1C5FD937327E0ADA20126B4AC9A9A84A94847955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1C5FD937327E0ADA20126B4AC9A9A84A94847955_H
#ifndef NULLABLE_1_T3CC1103ACDEB987D97A844292CD0AA18E91DC10E_H
#define NULLABLE_1_T3CC1103ACDEB987D97A844292CD0AA18E91DC10E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Required>
struct  Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3CC1103ACDEB987D97A844292CD0AA18E91DC10E_H
#ifndef NULLABLE_1_T970BCD6CF10834F61EDF79EEC3622D952F9F75F9_H
#define NULLABLE_1_T970BCD6CF10834F61EDF79EEC3622D952F9F75F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence>
struct  Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T970BCD6CF10834F61EDF79EEC3622D952F9F75F9_H
#ifndef NULLABLE_1_TA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A_H
#define NULLABLE_1_TA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef EXTENSIONDATAGETTER_T370275A84D1A4EF423E9EDE4CB1B91A504DCF18A_H
#define EXTENSIONDATAGETTER_T370275A84D1A4EF423E9EDE4CB1B91A504DCF18A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct  ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATAGETTER_T370275A84D1A4EF423E9EDE4CB1B91A504DCF18A_H
#ifndef EXTENSIONDATASETTER_T0D72CEA2D2E406643881A00817138AF8DFC4CB54_H
#define EXTENSIONDATASETTER_T0D72CEA2D2E406643881A00817138AF8DFC4CB54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct  ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATASETTER_T0D72CEA2D2E406643881A00817138AF8DFC4CB54_H
#ifndef JSONCONTAINERCONTRACT_T48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C_H
#define JSONCONTAINERCONTRACT_T48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContainerContract
struct  JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C  : public JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843
{
public:
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_itemContract
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * ____itemContract_21;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_finalItemContract
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * ____finalItemContract_22;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::<ItemConverter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CItemConverterU3Ek__BackingField_23;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_24;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemTypeNameHandling>k__BackingField
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ___U3CItemTypeNameHandlingU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of__itemContract_21() { return static_cast<int32_t>(offsetof(JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C, ____itemContract_21)); }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * get__itemContract_21() const { return ____itemContract_21; }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 ** get_address_of__itemContract_21() { return &____itemContract_21; }
	inline void set__itemContract_21(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * value)
	{
		____itemContract_21 = value;
		Il2CppCodeGenWriteBarrier((&____itemContract_21), value);
	}

	inline static int32_t get_offset_of__finalItemContract_22() { return static_cast<int32_t>(offsetof(JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C, ____finalItemContract_22)); }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * get__finalItemContract_22() const { return ____finalItemContract_22; }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 ** get_address_of__finalItemContract_22() { return &____finalItemContract_22; }
	inline void set__finalItemContract_22(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * value)
	{
		____finalItemContract_22 = value;
		Il2CppCodeGenWriteBarrier((&____finalItemContract_22), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C, ___U3CItemConverterU3Ek__BackingField_23)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CItemConverterU3Ek__BackingField_23() const { return ___U3CItemConverterU3Ek__BackingField_23; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CItemConverterU3Ek__BackingField_23() { return &___U3CItemConverterU3Ek__BackingField_23; }
	inline void set_U3CItemConverterU3Ek__BackingField_23(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CItemConverterU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C, ___U3CItemIsReferenceU3Ek__BackingField_24)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_24() const { return ___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_24() { return &___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_24(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C, ___U3CItemTypeNameHandlingU3Ek__BackingField_26)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get_U3CItemTypeNameHandlingU3Ek__BackingField_26() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_26(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERCONTRACT_T48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C_H
#ifndef JSONLINQCONTRACT_TD9B072F70D8B1713C79BDA3BAD0F9AFC5DDCCAE0_H
#define JSONLINQCONTRACT_TD9B072F70D8B1713C79BDA3BAD0F9AFC5DDCCAE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonLinqContract
struct  JsonLinqContract_tD9B072F70D8B1713C79BDA3BAD0F9AFC5DDCCAE0  : public JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLINQCONTRACT_TD9B072F70D8B1713C79BDA3BAD0F9AFC5DDCCAE0_H
#ifndef JSONPRIMITIVECONTRACT_TF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_H
#define JSONPRIMITIVECONTRACT_TF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C  : public JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843
{
public:
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType> Newtonsoft.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_TF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_H
#ifndef JSONPROPERTY_T421287D0DB34CB88B164E6CC05B075AE6FCB128D_H
#define JSONPROPERTY_T421287D0DB34CB88B164E6CC05B075AE6FCB128D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonProperty
struct  JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D  : public RuntimeObject
{
public:
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonProperty::_required
	Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  ____required_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_hasExplicitDefaultValue
	bool ____hasExplicitDefaultValue_1;
	// System.Object Newtonsoft.Json.Serialization.JsonProperty::_defaultValue
	RuntimeObject * ____defaultValue_2;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_hasGeneratedDefaultValue
	bool ____hasGeneratedDefaultValue_3;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::_propertyName
	String_t* ____propertyName_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_skipPropertyNameEscape
	bool ____skipPropertyNameEscape_5;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::_propertyType
	Type_t * ____propertyType_6;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::<PropertyContract>k__BackingField
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * ___U3CPropertyContractU3Ek__BackingField_7;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::<DeclaringType>k__BackingField
	Type_t * ___U3CDeclaringTypeU3Ek__BackingField_8;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::<Order>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3COrderU3Ek__BackingField_9;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::<UnderlyingName>k__BackingField
	String_t* ___U3CUnderlyingNameU3Ek__BackingField_10;
	// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::<ValueProvider>k__BackingField
	RuntimeObject* ___U3CValueProviderU3Ek__BackingField_11;
	// Newtonsoft.Json.Serialization.IAttributeProvider Newtonsoft.Json.Serialization.JsonProperty::<AttributeProvider>k__BackingField
	RuntimeObject* ___U3CAttributeProviderU3Ek__BackingField_12;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<Converter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CConverterU3Ek__BackingField_13;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<MemberConverter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CMemberConverterU3Ek__BackingField_14;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Ignored>k__BackingField
	bool ___U3CIgnoredU3Ek__BackingField_15;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Readable>k__BackingField
	bool ___U3CReadableU3Ek__BackingField_16;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Writable>k__BackingField
	bool ___U3CWritableU3Ek__BackingField_17;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<HasMemberAttribute>k__BackingField
	bool ___U3CHasMemberAttributeU3Ek__BackingField_18;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_19;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<NullValueHandling>k__BackingField
	Nullable_1_t03E506703700988207428BB69043E307A2C53E53  ___U3CNullValueHandlingU3Ek__BackingField_20;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<DefaultValueHandling>k__BackingField
	Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  ___U3CDefaultValueHandlingU3Ek__BackingField_21;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ReferenceLoopHandling>k__BackingField
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ___U3CReferenceLoopHandlingU3Ek__BackingField_22;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::<ObjectCreationHandling>k__BackingField
	Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  ___U3CObjectCreationHandlingU3Ek__BackingField_23;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<TypeNameHandling>k__BackingField
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ___U3CTypeNameHandlingU3Ek__BackingField_24;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldSerialize>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CShouldSerializeU3Ek__BackingField_25;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldDeserialize>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CShouldDeserializeU3Ek__BackingField_26;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<GetIsSpecified>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CGetIsSpecifiedU3Ek__BackingField_27;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::<SetIsSpecified>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetIsSpecifiedU3Ek__BackingField_28;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<ItemConverter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CItemConverterU3Ek__BackingField_29;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_30;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<ItemTypeNameHandling>k__BackingField
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ___U3CItemTypeNameHandlingU3Ek__BackingField_31;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__required_0() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____required_0)); }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  get__required_0() const { return ____required_0; }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E * get_address_of__required_0() { return &____required_0; }
	inline void set__required_0(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  value)
	{
		____required_0 = value;
	}

	inline static int32_t get_offset_of__hasExplicitDefaultValue_1() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____hasExplicitDefaultValue_1)); }
	inline bool get__hasExplicitDefaultValue_1() const { return ____hasExplicitDefaultValue_1; }
	inline bool* get_address_of__hasExplicitDefaultValue_1() { return &____hasExplicitDefaultValue_1; }
	inline void set__hasExplicitDefaultValue_1(bool value)
	{
		____hasExplicitDefaultValue_1 = value;
	}

	inline static int32_t get_offset_of__defaultValue_2() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____defaultValue_2)); }
	inline RuntimeObject * get__defaultValue_2() const { return ____defaultValue_2; }
	inline RuntimeObject ** get_address_of__defaultValue_2() { return &____defaultValue_2; }
	inline void set__defaultValue_2(RuntimeObject * value)
	{
		____defaultValue_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_2), value);
	}

	inline static int32_t get_offset_of__hasGeneratedDefaultValue_3() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____hasGeneratedDefaultValue_3)); }
	inline bool get__hasGeneratedDefaultValue_3() const { return ____hasGeneratedDefaultValue_3; }
	inline bool* get_address_of__hasGeneratedDefaultValue_3() { return &____hasGeneratedDefaultValue_3; }
	inline void set__hasGeneratedDefaultValue_3(bool value)
	{
		____hasGeneratedDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of__propertyName_4() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____propertyName_4)); }
	inline String_t* get__propertyName_4() const { return ____propertyName_4; }
	inline String_t** get_address_of__propertyName_4() { return &____propertyName_4; }
	inline void set__propertyName_4(String_t* value)
	{
		____propertyName_4 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_4), value);
	}

	inline static int32_t get_offset_of__skipPropertyNameEscape_5() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____skipPropertyNameEscape_5)); }
	inline bool get__skipPropertyNameEscape_5() const { return ____skipPropertyNameEscape_5; }
	inline bool* get_address_of__skipPropertyNameEscape_5() { return &____skipPropertyNameEscape_5; }
	inline void set__skipPropertyNameEscape_5(bool value)
	{
		____skipPropertyNameEscape_5 = value;
	}

	inline static int32_t get_offset_of__propertyType_6() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ____propertyType_6)); }
	inline Type_t * get__propertyType_6() const { return ____propertyType_6; }
	inline Type_t ** get_address_of__propertyType_6() { return &____propertyType_6; }
	inline void set__propertyType_6(Type_t * value)
	{
		____propertyType_6 = value;
		Il2CppCodeGenWriteBarrier((&____propertyType_6), value);
	}

	inline static int32_t get_offset_of_U3CPropertyContractU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CPropertyContractU3Ek__BackingField_7)); }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * get_U3CPropertyContractU3Ek__BackingField_7() const { return ___U3CPropertyContractU3Ek__BackingField_7; }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 ** get_address_of_U3CPropertyContractU3Ek__BackingField_7() { return &___U3CPropertyContractU3Ek__BackingField_7; }
	inline void set_U3CPropertyContractU3Ek__BackingField_7(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * value)
	{
		___U3CPropertyContractU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyContractU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CDeclaringTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CDeclaringTypeU3Ek__BackingField_8() const { return ___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CDeclaringTypeU3Ek__BackingField_8() { return &___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline void set_U3CDeclaringTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CDeclaringTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclaringTypeU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3COrderU3Ek__BackingField_9)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3COrderU3Ek__BackingField_9() const { return ___U3COrderU3Ek__BackingField_9; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3COrderU3Ek__BackingField_9() { return &___U3COrderU3Ek__BackingField_9; }
	inline void set_U3COrderU3Ek__BackingField_9(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3COrderU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CUnderlyingNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CUnderlyingNameU3Ek__BackingField_10() const { return ___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUnderlyingNameU3Ek__BackingField_10() { return &___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline void set_U3CUnderlyingNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CUnderlyingNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingNameU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CValueProviderU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CValueProviderU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CValueProviderU3Ek__BackingField_11() const { return ___U3CValueProviderU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CValueProviderU3Ek__BackingField_11() { return &___U3CValueProviderU3Ek__BackingField_11; }
	inline void set_U3CValueProviderU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CValueProviderU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueProviderU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CAttributeProviderU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CAttributeProviderU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CAttributeProviderU3Ek__BackingField_12() const { return ___U3CAttributeProviderU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CAttributeProviderU3Ek__BackingField_12() { return &___U3CAttributeProviderU3Ek__BackingField_12; }
	inline void set_U3CAttributeProviderU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CAttributeProviderU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttributeProviderU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CConverterU3Ek__BackingField_13)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CConverterU3Ek__BackingField_13() const { return ___U3CConverterU3Ek__BackingField_13; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CConverterU3Ek__BackingField_13() { return &___U3CConverterU3Ek__BackingField_13; }
	inline void set_U3CConverterU3Ek__BackingField_13(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CConverterU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMemberConverterU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CMemberConverterU3Ek__BackingField_14)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CMemberConverterU3Ek__BackingField_14() const { return ___U3CMemberConverterU3Ek__BackingField_14; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CMemberConverterU3Ek__BackingField_14() { return &___U3CMemberConverterU3Ek__BackingField_14; }
	inline void set_U3CMemberConverterU3Ek__BackingField_14(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CMemberConverterU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberConverterU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CIgnoredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CIgnoredU3Ek__BackingField_15)); }
	inline bool get_U3CIgnoredU3Ek__BackingField_15() const { return ___U3CIgnoredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIgnoredU3Ek__BackingField_15() { return &___U3CIgnoredU3Ek__BackingField_15; }
	inline void set_U3CIgnoredU3Ek__BackingField_15(bool value)
	{
		___U3CIgnoredU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CReadableU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CReadableU3Ek__BackingField_16)); }
	inline bool get_U3CReadableU3Ek__BackingField_16() const { return ___U3CReadableU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CReadableU3Ek__BackingField_16() { return &___U3CReadableU3Ek__BackingField_16; }
	inline void set_U3CReadableU3Ek__BackingField_16(bool value)
	{
		___U3CReadableU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CWritableU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CWritableU3Ek__BackingField_17)); }
	inline bool get_U3CWritableU3Ek__BackingField_17() const { return ___U3CWritableU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CWritableU3Ek__BackingField_17() { return &___U3CWritableU3Ek__BackingField_17; }
	inline void set_U3CWritableU3Ek__BackingField_17(bool value)
	{
		___U3CWritableU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CHasMemberAttributeU3Ek__BackingField_18)); }
	inline bool get_U3CHasMemberAttributeU3Ek__BackingField_18() const { return ___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return &___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline void set_U3CHasMemberAttributeU3Ek__BackingField_18(bool value)
	{
		___U3CHasMemberAttributeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CIsReferenceU3Ek__BackingField_19)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_19() const { return ___U3CIsReferenceU3Ek__BackingField_19; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_19() { return &___U3CIsReferenceU3Ek__BackingField_19; }
	inline void set_U3CIsReferenceU3Ek__BackingField_19(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CNullValueHandlingU3Ek__BackingField_20)); }
	inline Nullable_1_t03E506703700988207428BB69043E307A2C53E53  get_U3CNullValueHandlingU3Ek__BackingField_20() const { return ___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline Nullable_1_t03E506703700988207428BB69043E307A2C53E53 * get_address_of_U3CNullValueHandlingU3Ek__BackingField_20() { return &___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline void set_U3CNullValueHandlingU3Ek__BackingField_20(Nullable_1_t03E506703700988207428BB69043E307A2C53E53  value)
	{
		___U3CNullValueHandlingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CDefaultValueHandlingU3Ek__BackingField_21)); }
	inline Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  get_U3CDefaultValueHandlingU3Ek__BackingField_21() const { return ___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718 * get_address_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return &___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline void set_U3CDefaultValueHandlingU3Ek__BackingField_21(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  value)
	{
		___U3CDefaultValueHandlingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CReferenceLoopHandlingU3Ek__BackingField_22)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get_U3CReferenceLoopHandlingU3Ek__BackingField_22() const { return ___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return &___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline void set_U3CReferenceLoopHandlingU3Ek__BackingField_22(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		___U3CReferenceLoopHandlingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CObjectCreationHandlingU3Ek__BackingField_23)); }
	inline Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  get_U3CObjectCreationHandlingU3Ek__BackingField_23() const { return ___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531 * get_address_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return &___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline void set_U3CObjectCreationHandlingU3Ek__BackingField_23(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  value)
	{
		___U3CObjectCreationHandlingU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CTypeNameHandlingU3Ek__BackingField_24)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get_U3CTypeNameHandlingU3Ek__BackingField_24() const { return ___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return &___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline void set_U3CTypeNameHandlingU3Ek__BackingField_24(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		___U3CTypeNameHandlingU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CShouldSerializeU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CShouldSerializeU3Ek__BackingField_25)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CShouldSerializeU3Ek__BackingField_25() const { return ___U3CShouldSerializeU3Ek__BackingField_25; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CShouldSerializeU3Ek__BackingField_25() { return &___U3CShouldSerializeU3Ek__BackingField_25; }
	inline void set_U3CShouldSerializeU3Ek__BackingField_25(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CShouldSerializeU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldSerializeU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CShouldDeserializeU3Ek__BackingField_26)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CShouldDeserializeU3Ek__BackingField_26() const { return ___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CShouldDeserializeU3Ek__BackingField_26() { return &___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline void set_U3CShouldDeserializeU3Ek__BackingField_26(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CShouldDeserializeU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldDeserializeU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CGetIsSpecifiedU3Ek__BackingField_27)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CGetIsSpecifiedU3Ek__BackingField_27() const { return ___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return &___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline void set_U3CGetIsSpecifiedU3Ek__BackingField_27(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CGetIsSpecifiedU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetIsSpecifiedU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CSetIsSpecifiedU3Ek__BackingField_28)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetIsSpecifiedU3Ek__BackingField_28() const { return ___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return &___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline void set_U3CSetIsSpecifiedU3Ek__BackingField_28(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetIsSpecifiedU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetIsSpecifiedU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CItemConverterU3Ek__BackingField_29)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CItemConverterU3Ek__BackingField_29() const { return ___U3CItemConverterU3Ek__BackingField_29; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CItemConverterU3Ek__BackingField_29() { return &___U3CItemConverterU3Ek__BackingField_29; }
	inline void set_U3CItemConverterU3Ek__BackingField_29(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CItemConverterU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CItemIsReferenceU3Ek__BackingField_30)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_30() const { return ___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_30() { return &___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_30(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CItemTypeNameHandlingU3Ek__BackingField_31)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get_U3CItemTypeNameHandlingU3Ek__BackingField_31() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_31(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTY_T421287D0DB34CB88B164E6CC05B075AE6FCB128D_H
#ifndef CREATORPROPERTYCONTEXT_T4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638_H
#define CREATORPROPERTYCONTEXT_T4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext
struct  CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Name
	String_t* ___Name_0;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Property
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * ___Property_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::ConstructorProperty
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * ___ConstructorProperty_2;
	// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Presence
	Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9  ___Presence_3;
	// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Value
	RuntimeObject * ___Value_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Used
	bool ___Used_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Property_1() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638, ___Property_1)); }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * get_Property_1() const { return ___Property_1; }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D ** get_address_of_Property_1() { return &___Property_1; }
	inline void set_Property_1(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * value)
	{
		___Property_1 = value;
		Il2CppCodeGenWriteBarrier((&___Property_1), value);
	}

	inline static int32_t get_offset_of_ConstructorProperty_2() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638, ___ConstructorProperty_2)); }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * get_ConstructorProperty_2() const { return ___ConstructorProperty_2; }
	inline JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D ** get_address_of_ConstructorProperty_2() { return &___ConstructorProperty_2; }
	inline void set_ConstructorProperty_2(JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D * value)
	{
		___ConstructorProperty_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorProperty_2), value);
	}

	inline static int32_t get_offset_of_Presence_3() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638, ___Presence_3)); }
	inline Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9  get_Presence_3() const { return ___Presence_3; }
	inline Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9 * get_address_of_Presence_3() { return &___Presence_3; }
	inline void set_Presence_3(Nullable_1_t970BCD6CF10834F61EDF79EEC3622D952F9F75F9  value)
	{
		___Presence_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638, ___Value_4)); }
	inline RuntimeObject * get_Value_4() const { return ___Value_4; }
	inline RuntimeObject ** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(RuntimeObject * value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_Used_5() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638, ___Used_5)); }
	inline bool get_Used_5() const { return ___Used_5; }
	inline bool* get_address_of_Used_5() { return &___Used_5; }
	inline void set_Used_5(bool value)
	{
		___Used_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORPROPERTYCONTEXT_T4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638_H
#ifndef SERIALIZATIONCALLBACK_TFE7119577B4EFF6B7660455C3E4E6BDCC5903645_H
#define SERIALIZATIONCALLBACK_TFE7119577B4EFF6B7660455C3E4E6BDCC5903645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.SerializationCallback
struct  SerializationCallback_tFE7119577B4EFF6B7660455C3E4E6BDCC5903645  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONCALLBACK_TFE7119577B4EFF6B7660455C3E4E6BDCC5903645_H
#ifndef SERIALIZATIONERRORCALLBACK_TF201D8CB3F33689A2A659423EF2145A7EA2E55BB_H
#define SERIALIZATIONERRORCALLBACK_TF201D8CB3F33689A2A659423EF2145A7EA2E55BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.SerializationErrorCallback
struct  SerializationErrorCallback_tF201D8CB3F33689A2A659423EF2145A7EA2E55BB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONERRORCALLBACK_TF201D8CB3F33689A2A659423EF2145A7EA2E55BB_H
#ifndef JSONARRAYCONTRACT_T88724369596C0CCADA013CFA08D2EBD00E0F1F0A_H
#define JSONARRAYCONTRACT_T88724369596C0CCADA013CFA08D2EBD00E0F1F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonArrayContract
struct  JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A  : public JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::<CollectionItemType>k__BackingField
	Type_t * ___U3CCollectionItemTypeU3Ek__BackingField_27;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<IsMultidimensionalArray>k__BackingField
	bool ___U3CIsMultidimensionalArrayU3Ek__BackingField_28;
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_29;
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::_genericWrapperType
	Type_t * ____genericWrapperType_30;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_genericWrapperCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____genericWrapperCreator_31;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_genericTemporaryCollectionCreator
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ____genericTemporaryCollectionCreator_32;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<IsArray>k__BackingField
	bool ___U3CIsArrayU3Ek__BackingField_33;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_34;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<CanDeserialize>k__BackingField
	bool ___U3CCanDeserializeU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonArrayContract::_parameterizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parameterizedConstructor_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_parameterizedCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____parameterizedCreator_37;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_overrideCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____overrideCreator_38;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ___U3CCollectionItemTypeU3Ek__BackingField_27)); }
	inline Type_t * get_U3CCollectionItemTypeU3Ek__BackingField_27() const { return ___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline Type_t ** get_address_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return &___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline void set_U3CCollectionItemTypeU3Ek__BackingField_27(Type_t * value)
	{
		___U3CCollectionItemTypeU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionItemTypeU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ___U3CIsMultidimensionalArrayU3Ek__BackingField_28)); }
	inline bool get_U3CIsMultidimensionalArrayU3Ek__BackingField_28() const { return ___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return &___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline void set_U3CIsMultidimensionalArrayU3Ek__BackingField_28(bool value)
	{
		___U3CIsMultidimensionalArrayU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_29() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____genericCollectionDefinitionType_29)); }
	inline Type_t * get__genericCollectionDefinitionType_29() const { return ____genericCollectionDefinitionType_29; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_29() { return &____genericCollectionDefinitionType_29; }
	inline void set__genericCollectionDefinitionType_29(Type_t * value)
	{
		____genericCollectionDefinitionType_29 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_29), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_30() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____genericWrapperType_30)); }
	inline Type_t * get__genericWrapperType_30() const { return ____genericWrapperType_30; }
	inline Type_t ** get_address_of__genericWrapperType_30() { return &____genericWrapperType_30; }
	inline void set__genericWrapperType_30(Type_t * value)
	{
		____genericWrapperType_30 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_30), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_31() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____genericWrapperCreator_31)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__genericWrapperCreator_31() const { return ____genericWrapperCreator_31; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__genericWrapperCreator_31() { return &____genericWrapperCreator_31; }
	inline void set__genericWrapperCreator_31(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____genericWrapperCreator_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_31), value);
	}

	inline static int32_t get_offset_of__genericTemporaryCollectionCreator_32() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____genericTemporaryCollectionCreator_32)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get__genericTemporaryCollectionCreator_32() const { return ____genericTemporaryCollectionCreator_32; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of__genericTemporaryCollectionCreator_32() { return &____genericTemporaryCollectionCreator_32; }
	inline void set__genericTemporaryCollectionCreator_32(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		____genericTemporaryCollectionCreator_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryCollectionCreator_32), value);
	}

	inline static int32_t get_offset_of_U3CIsArrayU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ___U3CIsArrayU3Ek__BackingField_33)); }
	inline bool get_U3CIsArrayU3Ek__BackingField_33() const { return ___U3CIsArrayU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CIsArrayU3Ek__BackingField_33() { return &___U3CIsArrayU3Ek__BackingField_33; }
	inline void set_U3CIsArrayU3Ek__BackingField_33(bool value)
	{
		___U3CIsArrayU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ___U3CShouldCreateWrapperU3Ek__BackingField_34)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_34() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return &___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_34(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CCanDeserializeU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ___U3CCanDeserializeU3Ek__BackingField_35)); }
	inline bool get_U3CCanDeserializeU3Ek__BackingField_35() const { return ___U3CCanDeserializeU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CCanDeserializeU3Ek__BackingField_35() { return &___U3CCanDeserializeU3Ek__BackingField_35; }
	inline void set_U3CCanDeserializeU3Ek__BackingField_35(bool value)
	{
		___U3CCanDeserializeU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_37() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____parameterizedCreator_37)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__parameterizedCreator_37() const { return ____parameterizedCreator_37; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__parameterizedCreator_37() { return &____parameterizedCreator_37; }
	inline void set__parameterizedCreator_37(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____parameterizedCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_37), value);
	}

	inline static int32_t get_offset_of__overrideCreator_38() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ____overrideCreator_38)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__overrideCreator_38() const { return ____overrideCreator_38; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__overrideCreator_38() { return &____overrideCreator_38; }
	inline void set__overrideCreator_38(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____overrideCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYCONTRACT_T88724369596C0CCADA013CFA08D2EBD00E0F1F0A_H
#ifndef JSONDICTIONARYCONTRACT_T9190C9F253005ABD14670B38A1BD1A20B9808D74_H
#define JSONDICTIONARYCONTRACT_T9190C9F253005ABD14670B38A1BD1A20B9808D74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct  JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74  : public JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C
{
public:
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryKeyResolver>k__BackingField
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CDictionaryKeyResolverU3Ek__BackingField_27;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryKeyType>k__BackingField
	Type_t * ___U3CDictionaryKeyTypeU3Ek__BackingField_28;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryValueType>k__BackingField
	Type_t * ___U3CDictionaryValueTypeU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::<KeyContract>k__BackingField
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * ___U3CKeyContractU3Ek__BackingField_30;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_31;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericWrapperType
	Type_t * ____genericWrapperType_32;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericWrapperCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____genericWrapperCreator_33;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericTemporaryDictionaryCreator
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ____genericTemporaryDictionaryCreator_34;
	// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonDictionaryContract::_parameterizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parameterizedConstructor_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_overrideCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____overrideCreator_37;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_parameterizedCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____parameterizedCreator_38;
	// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ___U3CDictionaryKeyResolverU3Ek__BackingField_27)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CDictionaryKeyResolverU3Ek__BackingField_27() const { return ___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return &___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline void set_U3CDictionaryKeyResolverU3Ek__BackingField_27(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CDictionaryKeyResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ___U3CDictionaryKeyTypeU3Ek__BackingField_28)); }
	inline Type_t * get_U3CDictionaryKeyTypeU3Ek__BackingField_28() const { return ___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline Type_t ** get_address_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return &___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline void set_U3CDictionaryKeyTypeU3Ek__BackingField_28(Type_t * value)
	{
		___U3CDictionaryKeyTypeU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyTypeU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ___U3CDictionaryValueTypeU3Ek__BackingField_29)); }
	inline Type_t * get_U3CDictionaryValueTypeU3Ek__BackingField_29() const { return ___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline Type_t ** get_address_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return &___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline void set_U3CDictionaryValueTypeU3Ek__BackingField_29(Type_t * value)
	{
		___U3CDictionaryValueTypeU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryValueTypeU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CKeyContractU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ___U3CKeyContractU3Ek__BackingField_30)); }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * get_U3CKeyContractU3Ek__BackingField_30() const { return ___U3CKeyContractU3Ek__BackingField_30; }
	inline JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 ** get_address_of_U3CKeyContractU3Ek__BackingField_30() { return &___U3CKeyContractU3Ek__BackingField_30; }
	inline void set_U3CKeyContractU3Ek__BackingField_30(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843 * value)
	{
		___U3CKeyContractU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyContractU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_31() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____genericCollectionDefinitionType_31)); }
	inline Type_t * get__genericCollectionDefinitionType_31() const { return ____genericCollectionDefinitionType_31; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_31() { return &____genericCollectionDefinitionType_31; }
	inline void set__genericCollectionDefinitionType_31(Type_t * value)
	{
		____genericCollectionDefinitionType_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_31), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_32() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____genericWrapperType_32)); }
	inline Type_t * get__genericWrapperType_32() const { return ____genericWrapperType_32; }
	inline Type_t ** get_address_of__genericWrapperType_32() { return &____genericWrapperType_32; }
	inline void set__genericWrapperType_32(Type_t * value)
	{
		____genericWrapperType_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_32), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_33() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____genericWrapperCreator_33)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__genericWrapperCreator_33() const { return ____genericWrapperCreator_33; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__genericWrapperCreator_33() { return &____genericWrapperCreator_33; }
	inline void set__genericWrapperCreator_33(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____genericWrapperCreator_33 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_33), value);
	}

	inline static int32_t get_offset_of__genericTemporaryDictionaryCreator_34() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____genericTemporaryDictionaryCreator_34)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get__genericTemporaryDictionaryCreator_34() const { return ____genericTemporaryDictionaryCreator_34; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of__genericTemporaryDictionaryCreator_34() { return &____genericTemporaryDictionaryCreator_34; }
	inline void set__genericTemporaryDictionaryCreator_34(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		____genericTemporaryDictionaryCreator_34 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryDictionaryCreator_34), value);
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ___U3CShouldCreateWrapperU3Ek__BackingField_35)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_35() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return &___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_35(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__overrideCreator_37() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____overrideCreator_37)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__overrideCreator_37() const { return ____overrideCreator_37; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__overrideCreator_37() { return &____overrideCreator_37; }
	inline void set__overrideCreator_37(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____overrideCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_37), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_38() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ____parameterizedCreator_38)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__parameterizedCreator_38() const { return ____parameterizedCreator_38; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__parameterizedCreator_38() { return &____parameterizedCreator_38; }
	inline void set__parameterizedCreator_38(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____parameterizedCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYCONTRACT_T9190C9F253005ABD14670B38A1BD1A20B9808D74_H
#ifndef JSONISERIALIZABLECONTRACT_T2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D_H
#define JSONISERIALIZABLECONTRACT_T2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonISerializableContract
struct  JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D  : public JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::<ISerializableCreator>k__BackingField
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ___U3CISerializableCreatorU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D, ___U3CISerializableCreatorU3Ek__BackingField_27)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get_U3CISerializableCreatorU3Ek__BackingField_27() const { return ___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of_U3CISerializableCreatorU3Ek__BackingField_27() { return &___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline void set_U3CISerializableCreatorU3Ek__BackingField_27(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		___U3CISerializableCreatorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CISerializableCreatorU3Ek__BackingField_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONISERIALIZABLECONTRACT_T2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D_H
#ifndef JSONOBJECTCONTRACT_T7A6A9F25351F07BA750863725B6FF2FF4616CF6B_H
#define JSONOBJECTCONTRACT_T7A6A9F25351F07BA750863725B6FF2FF4616CF6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonObjectContract
struct  JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B  : public JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::<MemberSerialization>k__BackingField
	int32_t ___U3CMemberSerializationU3Ek__BackingField_27;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::<ItemRequired>k__BackingField
	Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  ___U3CItemRequiredU3Ek__BackingField_28;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::<Properties>k__BackingField
	JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD * ___U3CPropertiesU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataSetter>k__BackingField
	ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54 * ___U3CExtensionDataSetterU3Ek__BackingField_30;
	// Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataGetter>k__BackingField
	ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A * ___U3CExtensionDataGetterU3Ek__BackingField_31;
	// System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::ExtensionDataIsJToken
	bool ___ExtensionDataIsJToken_32;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonObjectContract::_hasRequiredOrDefaultValueProperties
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____hasRequiredOrDefaultValueProperties_33;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonObjectContract::_parametrizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parametrizedConstructor_34;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonObjectContract::_overrideConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____overrideConstructor_35;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::_overrideCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____overrideCreator_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::_parameterizedCreator
	ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * ____parameterizedCreator_37;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::_creatorParameters
	JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD * ____creatorParameters_38;
	// System.Type Newtonsoft.Json.Serialization.JsonObjectContract::_extensionDataValueType
	Type_t * ____extensionDataValueType_39;

public:
	inline static int32_t get_offset_of_U3CMemberSerializationU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ___U3CMemberSerializationU3Ek__BackingField_27)); }
	inline int32_t get_U3CMemberSerializationU3Ek__BackingField_27() const { return ___U3CMemberSerializationU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CMemberSerializationU3Ek__BackingField_27() { return &___U3CMemberSerializationU3Ek__BackingField_27; }
	inline void set_U3CMemberSerializationU3Ek__BackingField_27(int32_t value)
	{
		___U3CMemberSerializationU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CItemRequiredU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ___U3CItemRequiredU3Ek__BackingField_28)); }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  get_U3CItemRequiredU3Ek__BackingField_28() const { return ___U3CItemRequiredU3Ek__BackingField_28; }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E * get_address_of_U3CItemRequiredU3Ek__BackingField_28() { return &___U3CItemRequiredU3Ek__BackingField_28; }
	inline void set_U3CItemRequiredU3Ek__BackingField_28(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  value)
	{
		___U3CItemRequiredU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ___U3CPropertiesU3Ek__BackingField_29)); }
	inline JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD * get_U3CPropertiesU3Ek__BackingField_29() const { return ___U3CPropertiesU3Ek__BackingField_29; }
	inline JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD ** get_address_of_U3CPropertiesU3Ek__BackingField_29() { return &___U3CPropertiesU3Ek__BackingField_29; }
	inline void set_U3CPropertiesU3Ek__BackingField_29(JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD * value)
	{
		___U3CPropertiesU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ___U3CExtensionDataSetterU3Ek__BackingField_30)); }
	inline ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54 * get_U3CExtensionDataSetterU3Ek__BackingField_30() const { return ___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54 ** get_address_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return &___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline void set_U3CExtensionDataSetterU3Ek__BackingField_30(ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54 * value)
	{
		___U3CExtensionDataSetterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataSetterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ___U3CExtensionDataGetterU3Ek__BackingField_31)); }
	inline ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A * get_U3CExtensionDataGetterU3Ek__BackingField_31() const { return ___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A ** get_address_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return &___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline void set_U3CExtensionDataGetterU3Ek__BackingField_31(ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A * value)
	{
		___U3CExtensionDataGetterU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataGetterU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_ExtensionDataIsJToken_32() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ___ExtensionDataIsJToken_32)); }
	inline bool get_ExtensionDataIsJToken_32() const { return ___ExtensionDataIsJToken_32; }
	inline bool* get_address_of_ExtensionDataIsJToken_32() { return &___ExtensionDataIsJToken_32; }
	inline void set_ExtensionDataIsJToken_32(bool value)
	{
		___ExtensionDataIsJToken_32 = value;
	}

	inline static int32_t get_offset_of__hasRequiredOrDefaultValueProperties_33() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____hasRequiredOrDefaultValueProperties_33)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__hasRequiredOrDefaultValueProperties_33() const { return ____hasRequiredOrDefaultValueProperties_33; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__hasRequiredOrDefaultValueProperties_33() { return &____hasRequiredOrDefaultValueProperties_33; }
	inline void set__hasRequiredOrDefaultValueProperties_33(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____hasRequiredOrDefaultValueProperties_33 = value;
	}

	inline static int32_t get_offset_of__parametrizedConstructor_34() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____parametrizedConstructor_34)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parametrizedConstructor_34() const { return ____parametrizedConstructor_34; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parametrizedConstructor_34() { return &____parametrizedConstructor_34; }
	inline void set__parametrizedConstructor_34(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parametrizedConstructor_34 = value;
		Il2CppCodeGenWriteBarrier((&____parametrizedConstructor_34), value);
	}

	inline static int32_t get_offset_of__overrideConstructor_35() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____overrideConstructor_35)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__overrideConstructor_35() const { return ____overrideConstructor_35; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__overrideConstructor_35() { return &____overrideConstructor_35; }
	inline void set__overrideConstructor_35(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____overrideConstructor_35 = value;
		Il2CppCodeGenWriteBarrier((&____overrideConstructor_35), value);
	}

	inline static int32_t get_offset_of__overrideCreator_36() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____overrideCreator_36)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__overrideCreator_36() const { return ____overrideCreator_36; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__overrideCreator_36() { return &____overrideCreator_36; }
	inline void set__overrideCreator_36(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____overrideCreator_36 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_36), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_37() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____parameterizedCreator_37)); }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * get__parameterizedCreator_37() const { return ____parameterizedCreator_37; }
	inline ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A ** get_address_of__parameterizedCreator_37() { return &____parameterizedCreator_37; }
	inline void set__parameterizedCreator_37(ObjectConstructor_1_tD3E70D3AFA084E5EC881C876749064CD264C998A * value)
	{
		____parameterizedCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_37), value);
	}

	inline static int32_t get_offset_of__creatorParameters_38() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____creatorParameters_38)); }
	inline JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD * get__creatorParameters_38() const { return ____creatorParameters_38; }
	inline JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD ** get_address_of__creatorParameters_38() { return &____creatorParameters_38; }
	inline void set__creatorParameters_38(JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD * value)
	{
		____creatorParameters_38 = value;
		Il2CppCodeGenWriteBarrier((&____creatorParameters_38), value);
	}

	inline static int32_t get_offset_of__extensionDataValueType_39() { return static_cast<int32_t>(offsetof(JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B, ____extensionDataValueType_39)); }
	inline Type_t * get__extensionDataValueType_39() const { return ____extensionDataValueType_39; }
	inline Type_t ** get_address_of__extensionDataValueType_39() { return &____extensionDataValueType_39; }
	inline void set__extensionDataValueType_39(Type_t * value)
	{
		____extensionDataValueType_39 = value;
		Il2CppCodeGenWriteBarrier((&____extensionDataValueType_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTCONTRACT_T7A6A9F25351F07BA750863725B6FF2FF4616CF6B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6100 = { sizeof (Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6100[4] = 
{
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__charsLine_0(),
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__writer_1(),
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__leftOverBytes_2(),
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6101 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6101[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6102 = { sizeof (CollectionUtils_tD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6103 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6104[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6105 = { sizeof (PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6105[43] = 
{
	PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6106 = { sizeof (TypeInformation_t4C41A70CA0C11647DFC962998B7D6B65DE6AAF75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6106[2] = 
{
	TypeInformation_t4C41A70CA0C11647DFC962998B7D6B65DE6AAF75::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeInformation_t4C41A70CA0C11647DFC962998B7D6B65DE6AAF75::get_offset_of_U3CTypeCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6107 = { sizeof (ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6107[5] = 
{
	ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6108 = { sizeof (ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C), -1, sizeof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6108[3] = 
{
	ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields::get_offset_of_PrimitiveTypeCodes_1(),
	ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields::get_offset_of_CastConverters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6109 = { sizeof (TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6109[2] = 
{
	TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6110 = { sizeof (ConvertResult_t98FEB5BCA2057684971BF398D291A95A55CF6256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6110[5] = 
{
	ConvertResult_t98FEB5BCA2057684971BF398D291A95A55CF6256::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6111 = { sizeof (U3CU3Ec__DisplayClass9_0_tFECE00A2F854C63EFBA43CDD07B838BAA7225ACB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6111[1] = 
{
	U3CU3Ec__DisplayClass9_0_tFECE00A2F854C63EFBA43CDD07B838BAA7225ACB::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6112 = { sizeof (ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6112[5] = 
{
	ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6113 = { sizeof (DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE)+ sizeof (RuntimeObject), sizeof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_marshaled_pinvoke), sizeof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6113[26] = 
{
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of__text_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of__end_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6114 = { sizeof (DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028), -1, sizeof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6114[3] = 
{
	DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6115 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6116 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6116[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6117[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6118 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6118[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6119 = { sizeof (EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2), -1, sizeof(EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6119[1] = 
{
	EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6120 = { sizeof (U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308), -1, sizeof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6120[3] = 
{
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6121 = { sizeof (BufferUtils_t3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6122 = { sizeof (JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854), -1, sizeof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6122[3] = 
{
	JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6123 = { sizeof (JsonTokenUtils_t13B5051387BE40F092E6758510FA5DDBED6367A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6124 = { sizeof (LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7), -1, sizeof(LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6124[1] = 
{
	LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6125 = { sizeof (U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6125[2] = 
{
	U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6126 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6126[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6127[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6128 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6128[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6129 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6129[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6130 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6130[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6131 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6131[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6132 = { sizeof (MathUtils_tD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6133 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6134 = { sizeof (MiscellaneousUtils_tF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6135 = { sizeof (PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C), -1, sizeof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6135[4] = 
{
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C::get_offset_of__count_1(),
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C::get_offset_of__entries_2(),
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6136 = { sizeof (Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6136[3] = 
{
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35::get_offset_of_Value_0(),
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35::get_offset_of_HashCode_1(),
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6137 = { sizeof (ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6138 = { sizeof (ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6138[3] = 
{
	ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6139 = { sizeof (ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6139[2] = 
{
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6140 = { sizeof (U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6140[1] = 
{
	U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6141 = { sizeof (U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6141[1] = 
{
	U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6142 = { sizeof (U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6142[1] = 
{
	U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6143 = { sizeof (ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C), -1, sizeof(ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6143[1] = 
{
	ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6144 = { sizeof (U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695), -1, sizeof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6144[5] = 
{
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__29_0_2(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__39_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6145 = { sizeof (U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6145[1] = 
{
	U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6146 = { sizeof (StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4)+ sizeof (RuntimeObject), sizeof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6146[2] = 
{
	StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4::get_offset_of__position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6147 = { sizeof (StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9)+ sizeof (RuntimeObject), sizeof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6147[3] = 
{
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9::get_offset_of__length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6148 = { sizeof (StringReferenceExtensions_t932DAA64E777B822FD41227E19E46718B156F23C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6149 = { sizeof (StringUtils_t5B73278499B8A8A66F9A2E01219BAAAD2B1FD011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6150 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6150[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6151 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6151[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6152 = { sizeof (TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6153 = { sizeof (ValidationUtils_t1BC13C7423D7B21E9C058B217AAA1F0D737BB299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6154 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6154[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6155 = { sizeof (ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6155[2] = 
{
	ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB::get_offset_of__resolverType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ResolverContractKey_tAF54DD0CBE88065705D0CF2B4676DAD2977328EB::get_offset_of__contractType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6156 = { sizeof (DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6156[2] = 
{
	DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB::get_offset_of_ContractCache_0(),
	DefaultContractResolverState_tA339D97C01BEA6BCD9EC6EB02BD96536687167FB::get_offset_of_NameTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6157 = { sizeof (DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44), -1, sizeof(DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6157[10] = 
{
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields::get_offset_of__instance_0(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields::get_offset_of_TypeContractCacheLock_2(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44_StaticFields::get_offset_of__sharedState_3(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44::get_offset_of__instanceState_4(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44::get_offset_of__sharedCache_5(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44::get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(),
	DefaultContractResolver_tD600F00ED9A6C5C8A0593B872F6C7E5143F8AF44::get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6158 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6158[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6159 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6159[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6160 = { sizeof (U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7), -1, sizeof(U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6160[7] = 
{
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9__30_0_1(),
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9__30_1_2(),
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9__33_0_3(),
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9__33_1_4(),
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9__36_0_5(),
	U3CU3Ec_t0D5A68439D79EEC6571107944976EC2794DB9BE7_StaticFields::get_offset_of_U3CU3E9__60_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6161 = { sizeof (U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6161[2] = 
{
	U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522::get_offset_of_getExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass34_0_t05CFC72840791A32124346EA7AE6829BCFB04522::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6162 = { sizeof (U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6162[4] = 
{
	U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE::get_offset_of_setExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE::get_offset_of_createExtensionDataDictionary_1(),
	U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE::get_offset_of_setExtensionDataDictionaryValue_2(),
	U3CU3Ec__DisplayClass34_1_tEA82263B1CBA8DFF05AB4047B64C316CE91D43BE::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6163 = { sizeof (U3CU3Ec__DisplayClass34_2_tB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6163[2] = 
{
	U3CU3Ec__DisplayClass34_2_tB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD::get_offset_of_createEnumerableWrapper_0(),
	U3CU3Ec__DisplayClass34_2_tB2BD20FF13E5E44F96C597C8E8601BDCC88CB5FD::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6164 = { sizeof (U3CU3Ec__DisplayClass64_0_t41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6164[1] = 
{
	U3CU3Ec__DisplayClass64_0_t41C5291D8DC13812C1E8CCE4BEBCEF0773AA5E7F::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6165 = { sizeof (U3CU3Ec__DisplayClass65_0_tCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6165[1] = 
{
	U3CU3Ec__DisplayClass65_0_tCC1B212FAA37967BE2A4E93D7E4D3E66776FADDD::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6166 = { sizeof (DefaultReferenceResolver_tA4B664F91DA305784CF2177EC9D821AFA2596DC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6166[1] = 
{
	DefaultReferenceResolver_tA4B664F91DA305784CF2177EC9D821AFA2596DC6::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6167 = { sizeof (DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16), -1, sizeof(DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6167[2] = 
{
	DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_tA8808BA6AD2615BCBE34253055B79008F9BC4C16::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6168 = { sizeof (TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879)+ sizeof (RuntimeObject), sizeof(TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6168[2] = 
{
	TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeNameKey_tDAACDE517FD535C0637BDA68F01B7A44135A6879::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6169 = { sizeof (ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6169[6] = 
{
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3::get_offset_of_U3CTracedU3Ek__BackingField_0(),
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3::get_offset_of_U3COriginalObjectU3Ek__BackingField_2(),
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3::get_offset_of_U3CMemberU3Ek__BackingField_3(),
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3::get_offset_of_U3CPathU3Ek__BackingField_4(),
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3::get_offset_of_U3CHandledU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6170 = { sizeof (ErrorEventArgs_tF61F8C43A0EA33560C42850788318497AE985A3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6170[2] = 
{
	ErrorEventArgs_tF61F8C43A0EA33560C42850788318497AE985A3E::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_tF61F8C43A0EA33560C42850788318497AE985A3E::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6171 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6172 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6173 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6174 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6175 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6176 = { sizeof (JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6176[13] = 
{
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__genericCollectionDefinitionType_29(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__genericWrapperType_30(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__genericWrapperCreator_31(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__genericTemporaryCollectionCreator_32(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of_U3CIsArrayU3Ek__BackingField_33(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of_U3CCanDeserializeU3Ek__BackingField_35(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__parameterizedConstructor_36(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__parameterizedCreator_37(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of__overrideCreator_38(),
	JsonArrayContract_t88724369596C0CCADA013CFA08D2EBD00E0F1F0A::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6177 = { sizeof (JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6177[6] = 
{
	JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C::get_offset_of__itemContract_21(),
	JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C::get_offset_of__finalItemContract_22(),
	JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C::get_offset_of_U3CItemConverterU3Ek__BackingField_23(),
	JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24(),
	JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(),
	JsonContainerContract_t48470C10C979F1FD8A86F4AB49FD86A72B6FCE4C::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6178 = { sizeof (JsonContractType_t5DEDE285EB2CD0B99C86F0F53728B7433E66516F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6178[10] = 
{
	JsonContractType_t5DEDE285EB2CD0B99C86F0F53728B7433E66516F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6179 = { sizeof (SerializationCallback_tFE7119577B4EFF6B7660455C3E4E6BDCC5903645), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6180 = { sizeof (SerializationErrorCallback_tF201D8CB3F33689A2A659423EF2145A7EA2E55BB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6181 = { sizeof (ExtensionDataSetter_t0D72CEA2D2E406643881A00817138AF8DFC4CB54), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6182 = { sizeof (ExtensionDataGetter_t370275A84D1A4EF423E9EDE4CB1B91A504DCF18A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6183 = { sizeof (JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6183[21] = 
{
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_IsNullable_0(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_IsConvertable_1(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_IsEnum_2(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_NonNullableUnderlyingType_3(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_InternalReadType_4(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_ContractType_5(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_IsReadOnlyOrFixedSize_6(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_IsSealed_7(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_IsInstantiable_8(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of__onDeserializedCallbacks_9(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of__onDeserializingCallbacks_10(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of__onSerializedCallbacks_11(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of__onSerializingCallbacks_12(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of__onErrorCallbacks_13(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of__createdType_14(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_U3CIsReferenceU3Ek__BackingField_16(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_U3CConverterU3Ek__BackingField_17(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_U3CInternalConverterU3Ek__BackingField_18(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19(),
	JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6184 = { sizeof (U3CU3Ec__DisplayClass73_0_t2CFF1A4653CDB0903516611434AAFC6202263764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6184[1] = 
{
	U3CU3Ec__DisplayClass73_0_t2CFF1A4653CDB0903516611434AAFC6202263764::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6185 = { sizeof (U3CU3Ec__DisplayClass74_0_t23A7FAFFDEC10D634154C6366C9C3FCF88072BA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6185[1] = 
{
	U3CU3Ec__DisplayClass74_0_t23A7FAFFDEC10D634154C6366C9C3FCF88072BA8::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6186 = { sizeof (JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6186[13] = 
{
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of_U3CKeyContractU3Ek__BackingField_30(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__genericCollectionDefinitionType_31(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__genericWrapperType_32(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__genericWrapperCreator_33(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__genericTemporaryDictionaryCreator_34(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__parameterizedConstructor_36(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__overrideCreator_37(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of__parameterizedCreator_38(),
	JsonDictionaryContract_t9190C9F253005ABD14670B38A1BD1A20B9808D74::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6187 = { sizeof (JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6187[3] = 
{
	JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3::get_offset_of__reader_0(),
	JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3::get_offset_of__contract_1(),
	JsonFormatterConverter_t4638A0FFF2F878CDDCE8AD13250E5BB850116AB3::get_offset_of__member_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6188 = { sizeof (JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6188[1] = 
{
	JsonISerializableContract_t2FBD87FDDFDC31C92CD18BC96FD03894A5F4BB5D::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6189 = { sizeof (JsonLinqContract_tD9B072F70D8B1713C79BDA3BAD0F9AFC5DDCCAE0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6190 = { sizeof (JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6190[13] = 
{
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of_U3CMemberSerializationU3Ek__BackingField_27(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of_U3CItemRequiredU3Ek__BackingField_28(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of_U3CPropertiesU3Ek__BackingField_29(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of_ExtensionDataIsJToken_32(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__hasRequiredOrDefaultValueProperties_33(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__parametrizedConstructor_34(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__overrideConstructor_35(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__overrideCreator_36(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__parameterizedCreator_37(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__creatorParameters_38(),
	JsonObjectContract_t7A6A9F25351F07BA750863725B6FF2FF4616CF6B::get_offset_of__extensionDataValueType_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6191 = { sizeof (JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C), -1, sizeof(JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6191[2] = 
{
	JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C::get_offset_of_U3CTypeCodeU3Ek__BackingField_21(),
	JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_StaticFields::get_offset_of_ReadTypeMap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6192 = { sizeof (JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6192[33] = 
{
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__required_0(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__hasExplicitDefaultValue_1(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__defaultValue_2(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__hasGeneratedDefaultValue_3(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__propertyName_4(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__skipPropertyNameEscape_5(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of__propertyType_6(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CPropertyContractU3Ek__BackingField_7(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3COrderU3Ek__BackingField_9(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CValueProviderU3Ek__BackingField_11(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CAttributeProviderU3Ek__BackingField_12(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CConverterU3Ek__BackingField_13(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CMemberConverterU3Ek__BackingField_14(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CIgnoredU3Ek__BackingField_15(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CReadableU3Ek__BackingField_16(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CWritableU3Ek__BackingField_17(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CIsReferenceU3Ek__BackingField_19(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CShouldSerializeU3Ek__BackingField_25(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CItemConverterU3Ek__BackingField_29(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31(),
	JsonProperty_t421287D0DB34CB88B164E6CC05B075AE6FCB128D::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6193 = { sizeof (JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6193[2] = 
{
	JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD::get_offset_of__type_6(),
	JsonPropertyCollection_tDBD84A07D67BE611FB1A14AA2B24FA9DFDA56CDD::get_offset_of__list_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6194 = { sizeof (JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6194[5] = 
{
	JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C::get_offset_of_Serializer_2(),
	JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C::get_offset_of_TraceWriter_3(),
	JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C::get_offset_of_InternalSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6195 = { sizeof (ReferenceEqualsEqualityComparer_t5D69D6F0AAB906CE06A1F252E53639ABBC5B0553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6196 = { sizeof (JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6197 = { sizeof (PropertyPresence_t7CF971410D059BAB8A0C53A74F7F6369C1AA991F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6197[4] = 
{
	PropertyPresence_t7CF971410D059BAB8A0C53A74F7F6369C1AA991F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6198 = { sizeof (CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6198[6] = 
{
	CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638::get_offset_of_Name_0(),
	CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638::get_offset_of_Property_1(),
	CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638::get_offset_of_Presence_3(),
	CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638::get_offset_of_Value_4(),
	CreatorPropertyContext_t4FA325BE91D37B3CB21D837A2F5FCA9B4FC86638::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6199 = { sizeof (U3CU3Ec__DisplayClass36_0_t1E775CFB32AC53B47C94128ACB65F907AF911746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6199[1] = 
{
	U3CU3Ec__DisplayClass36_0_t1E775CFB32AC53B47C94128ACB65F907AF911746::get_offset_of_property_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
