﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.Caching.HTTPCacheFileInfo
struct HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D;
// BestHTTP.Connections.HTTP2.FramesAsStreamView
struct FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957;
// BestHTTP.Connections.HTTP2.HPACKEncoder
struct HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9;
// BestHTTP.Connections.HTTP2.HTTP2Response
struct HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE;
// BestHTTP.Connections.HTTP2.HTTP2SettingsManager
struct HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1;
// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry
struct HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7;
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry[]
struct TableEntryU5BU5D_t64B010C2A2FF090453C365DCCE6B894DE1509060;
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode[]
struct TreeNodeU5BU5D_t8F0ABFA7494FC10BF7611FAF6D081E9F6ED3F591;
// BestHTTP.Connections.HTTPConnection
struct HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9;
// BestHTTP.Decompression.GZipDecompressor
struct GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C;
// BestHTTP.Extensions.BufferSegmentStream
struct BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD;
// BestHTTP.Extensions.CircularBuffer`1<System.Double>
struct CircularBuffer_1_t79717186CCBB087CB40F3596A551A5A6F3858AF4;
// BestHTTP.Futures.Future`1<System.Boolean>
struct Future_1_t205FF123D3DF6CFB0B06E431BB815461424A7FB5;
// BestHTTP.HTTPRequest
struct HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837;
// BestHTTP.SignalRCore.HubConnection
struct HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0;
// BestHTTP.SignalRCore.HubOptions
struct HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458;
// BestHTTP.SignalRCore.IAuthenticationProvider
struct IAuthenticationProvider_t7000DF8CE775C6DBEA1C9C85FEF139DD375E11D2;
// BestHTTP.SignalRCore.IEncoder
struct IEncoder_t9E478E3F98C5C187E74E4D43023109F86C5FED4E;
// BestHTTP.SignalRCore.IProtocol
struct IProtocol_t425A2E88F9C7B80C6F0C8F1A747331C8F24B96CA;
// BestHTTP.SignalRCore.IRetryPolicy
struct IRetryPolicy_t6D034C691CEA28838960F1B9BBE805CE848A7CC3;
// BestHTTP.SignalRCore.ITransport
struct ITransport_tA94688BFD85A43F06EDB089CE5566AFE44E95D08;
// BestHTTP.SignalRCore.Messages.NegotiationResult
struct NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B;
// BestHTTP.SocketIO.Events.EventTable
struct EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F;
// BestHTTP.SocketIO.Events.SocketIOCallback[]
struct SocketIOCallbackU5BU5D_tB05DA025023C5E308BF7C6D93EA8595B338E041C;
// BestHTTP.SocketIO.HandshakeData
struct HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B;
// BestHTTP.SocketIO.JsonEncoders.IJsonEncoder
struct IJsonEncoder_t7FD1EB8D92A7761EE9550AA108C4C965FEA047AF;
// BestHTTP.SocketIO.Packet
struct Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B;
// BestHTTP.SocketIO.Socket
struct Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1;
// BestHTTP.SocketIO.SocketManager
struct SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322;
// BestHTTP.SocketIO.SocketOptions
struct SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228;
// BestHTTP.SocketIO.Transports.ITransport
struct ITransport_t2606428B43226E6E7ED84506DE6DA4215628047E;
// BestHTTP.WebSocket.WebSocket
struct WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717;
// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>
struct ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<BestHTTP.SignalRCore.HubConnection>
struct Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A;
// System.Action`1<System.Object[]>
struct Action_1_tEC9A54F57BB76CD226507E616D5D5A556A534798;
// System.Action`2<BestHTTP.Connections.HTTP2.HTTP2Handler,BestHTTP.HTTPRequest>
struct Action_2_t1D61181EF69111858FB91377A34DB4A72BBFBAFC;
// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String>
struct Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8;
// System.Action`2<BestHTTP.SignalRCore.TransportStates,BestHTTP.SignalRCore.TransportStates>
struct Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB;
// System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents>
struct Action_3_t2832E599EEEF3D8A44915D97BE5C8FBAE64EEDBD;
// System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri>
struct Action_3_t9E65FFC85A28FD135EFD112A772153B36D31E88A;
// System.Action`4<BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry,BestHTTP.Connections.HTTP2.HTTP2Settings,System.UInt32,System.UInt32>
struct Action_4_tD5AF77A6B3321DC818D2277334CC18D40AD628CC;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload>
struct ConcurrentQueue_1_tE59B4F2A24E20EB19A99339D509125D17614447F;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.HTTPRequest>
struct ConcurrentQueue_1_t1E22D69C92FE4BDD0DC96212335470CB30633235;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.PlatformSupport.Memory.BufferSegment>
struct ConcurrentQueue_1_t88A1AB5B72716BCA269B9ED8E2E19D550729581F;
// System.Collections.Generic.Dictionary`2<System.Int32,BestHTTP.SocketIO.Events.SocketIOAckCallback>
struct Dictionary_2_tB7DFA8D440B5E5C6F203D239BC1B5052E191FFA2;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Action`1<BestHTTP.SignalRCore.Messages.Message>>
struct Dictionary_2_t2ADC9CF37AE5CE341D4CFCF3991D3EF77ABB40EA;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.Authentication.Digest>
struct Dictionary_2_t92E1961146533F2AE6E3F48990231E031E4538E2;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SignalRCore.Subscription>
struct Dictionary_2_t7AFA59AAA480E7CFC1DB40F6BD09F67BFD324AE6;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SocketIO.Socket>
struct Dictionary_2_t2AD984ED67F12B1BA302EC41EDF94B77F1F66CC6;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<BestHTTP.SocketIO.Events.EventDescriptor>>
struct Dictionary_2_t7FE30E00FB54BCA5B530D0CF1D376F77838E266F;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26;
// System.Collections.Generic.Dictionary`2<System.UInt64,BestHTTP.Caching.HTTPCacheFileInfo>
struct Dictionary_2_t7B04DC9561E22E6F566C188535357C603C230965;
// System.Collections.Generic.Dictionary`2<System.Uri,BestHTTP.Caching.HTTPCacheFileInfo>
struct Dictionary_2_t3D7BB244513DA6111E20C925DF8D2CB2371B3D6C;
// System.Collections.Generic.List`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload>
struct List_1_t81EF1D41B45FE7683A924495B47003A131DED228;
// System.Collections.Generic.List`1<BestHTTP.Connections.HTTP2.HTTP2Stream>
struct List_1_t4DF90480877FA6ACD8927F4CBE481A0818FEDA06;
// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie>
struct List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.CallbackDescriptor>
struct List_1_tB4F97622006C9DF92F34633E2EDBC8ACEE3883A5;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.Message>
struct List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.TransportTypes>
struct List_1_tF36DB673FE6C6CD0C8E3148DFE3661FBD2C32574;
// System.Collections.Generic.List`1<BestHTTP.SocketIO.Events.SocketIOCallback>
struct List_1_t70634F31D27CDB5DE9D014091D7F1865129DB7C8;
// System.Collections.Generic.List`1<BestHTTP.SocketIO.Packet>
struct List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC;
// System.Collections.Generic.List`1<BestHTTP.SocketIO.Socket>
struct List_1_t6C9B151BA50582B180AF8BEEFA47DF94516E0ED2;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t54B0C0103C4E102A12FB66CAFDE587719C3794FF;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<BestHTTP.Connections.HTTP2.HTTP2Settings,System.UInt32>>
struct List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.Queue`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload>
struct Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.Messages.Message,System.Boolean>
struct Func_3_tF116A057DB6F20BD34CFC1B7A1CDFD8809C6FDCD;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Nullable`1<System.TimeSpan>[]
struct Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.String>
struct Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DIGESTSTORE_TD5E7E3051DF26A15CA74F2686685D7F7931261C0_H
#define DIGESTSTORE_TD5E7E3051DF26A15CA74F2686685D7F7931261C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Authentication.DigestStore
struct  DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0  : public RuntimeObject
{
public:

public:
};

struct DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.Authentication.Digest> BestHTTP.Authentication.DigestStore::Digests
	Dictionary_2_t92E1961146533F2AE6E3F48990231E031E4538E2 * ___Digests_0;
	// System.Threading.ReaderWriterLockSlim BestHTTP.Authentication.DigestStore::rwLock
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___rwLock_1;
	// System.String[] BestHTTP.Authentication.DigestStore::SupportedAlgorithms
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___SupportedAlgorithms_2;

public:
	inline static int32_t get_offset_of_Digests_0() { return static_cast<int32_t>(offsetof(DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields, ___Digests_0)); }
	inline Dictionary_2_t92E1961146533F2AE6E3F48990231E031E4538E2 * get_Digests_0() const { return ___Digests_0; }
	inline Dictionary_2_t92E1961146533F2AE6E3F48990231E031E4538E2 ** get_address_of_Digests_0() { return &___Digests_0; }
	inline void set_Digests_0(Dictionary_2_t92E1961146533F2AE6E3F48990231E031E4538E2 * value)
	{
		___Digests_0 = value;
		Il2CppCodeGenWriteBarrier((&___Digests_0), value);
	}

	inline static int32_t get_offset_of_rwLock_1() { return static_cast<int32_t>(offsetof(DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields, ___rwLock_1)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_rwLock_1() const { return ___rwLock_1; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_rwLock_1() { return &___rwLock_1; }
	inline void set_rwLock_1(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___rwLock_1 = value;
		Il2CppCodeGenWriteBarrier((&___rwLock_1), value);
	}

	inline static int32_t get_offset_of_SupportedAlgorithms_2() { return static_cast<int32_t>(offsetof(DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields, ___SupportedAlgorithms_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_SupportedAlgorithms_2() const { return ___SupportedAlgorithms_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_SupportedAlgorithms_2() { return &___SupportedAlgorithms_2; }
	inline void set_SupportedAlgorithms_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___SupportedAlgorithms_2 = value;
		Il2CppCodeGenWriteBarrier((&___SupportedAlgorithms_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSTORE_TD5E7E3051DF26A15CA74F2686685D7F7931261C0_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TA04FD0F40FD2047C724B7245C972DC1C8E4D99FD_H
#define U3CU3EC__DISPLAYCLASS6_0_TA04FD0F40FD2047C724B7245C972DC1C8E4D99FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Authentication.DigestStore_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tA04FD0F40FD2047C724B7245C972DC1C8E4D99FD  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Authentication.DigestStore_<>c__DisplayClass6_0::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tA04FD0F40FD2047C724B7245C972DC1C8E4D99FD, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TA04FD0F40FD2047C724B7245C972DC1C8E4D99FD_H
#ifndef U3CU3EC_TD1D4542DB29646843CA82E03D8A886AD27AEE46F_H
#define U3CU3EC_TD1D4542DB29646843CA82E03D8A886AD27AEE46F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Caching.HTTPCacheService_<>c
struct  U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields
{
public:
	// BestHTTP.Caching.HTTPCacheService_<>c BestHTTP.Caching.HTTPCacheService_<>c::<>9
	U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F * ___U3CU3E9_0;
	// System.Predicate`1<System.String> BestHTTP.Caching.HTTPCacheService_<>c::<>9__32_0
	Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * ___U3CU3E9__32_0_1;
	// System.Predicate`1<System.String> BestHTTP.Caching.HTTPCacheService_<>c::<>9__32_1
	Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * ___U3CU3E9__32_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields, ___U3CU3E9__32_0_1)); }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * get_U3CU3E9__32_0_1() const { return ___U3CU3E9__32_0_1; }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 ** get_address_of_U3CU3E9__32_0_1() { return &___U3CU3E9__32_0_1; }
	inline void set_U3CU3E9__32_0_1(Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * value)
	{
		___U3CU3E9__32_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields, ___U3CU3E9__32_1_2)); }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * get_U3CU3E9__32_1_2() const { return ___U3CU3E9__32_1_2; }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 ** get_address_of_U3CU3E9__32_1_2() { return &___U3CU3E9__32_1_2; }
	inline void set_U3CU3E9__32_1_2(Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * value)
	{
		___U3CU3E9__32_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD1D4542DB29646843CA82E03D8A886AD27AEE46F_H
#ifndef URICOMPARER_T70A570490E418B242A3EC3C8184A269672408F6A_H
#define URICOMPARER_T70A570490E418B242A3EC3C8184A269672408F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Caching.UriComparer
struct  UriComparer_t70A570490E418B242A3EC3C8184A269672408F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URICOMPARER_T70A570490E418B242A3EC3C8184A269672408F6A_H
#ifndef U3CU3EC_T0B57B13CF88F89C0175DB484E429C854D81C74D4_H
#define U3CU3EC_T0B57B13CF88F89C0175DB484E429C854D81C74D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2Handler_<>c
struct  U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4_StaticFields
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2Handler_<>c BestHTTP.Connections.HTTP2.HTTP2Handler_<>c::<>9
	U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4 * ___U3CU3E9_0;
	// System.Action`2<BestHTTP.Connections.HTTP2.HTTP2Handler,BestHTTP.HTTPRequest> BestHTTP.Connections.HTTP2.HTTP2Handler_<>c::<>9__34_0
	Action_2_t1D61181EF69111858FB91377A34DB4A72BBFBAFC * ___U3CU3E9__34_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4_StaticFields, ___U3CU3E9__34_0_1)); }
	inline Action_2_t1D61181EF69111858FB91377A34DB4A72BBFBAFC * get_U3CU3E9__34_0_1() const { return ___U3CU3E9__34_0_1; }
	inline Action_2_t1D61181EF69111858FB91377A34DB4A72BBFBAFC ** get_address_of_U3CU3E9__34_0_1() { return &___U3CU3E9__34_0_1; }
	inline void set_U3CU3E9__34_0_1(Action_2_t1D61181EF69111858FB91377A34DB4A72BBFBAFC * value)
	{
		___U3CU3E9__34_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0B57B13CF88F89C0175DB484E429C854D81C74D4_H
#ifndef HTTP2SETTINGSREGISTRY_TD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7_H
#define HTTP2SETTINGSREGISTRY_TD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry
struct  HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_0;
	// System.Action`4<BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry,BestHTTP.Connections.HTTP2.HTTP2Settings,System.UInt32,System.UInt32> BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry::OnSettingChangedEvent
	Action_4_tD5AF77A6B3321DC818D2277334CC18D40AD628CC * ___OnSettingChangedEvent_1;
	// System.UInt32[] BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry::values
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___values_2;
	// System.Boolean[] BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry::changeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___changeFlags_3;
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry::<IsChanged>k__BackingField
	bool ___U3CIsChangedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsReadOnlyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7, ___U3CIsReadOnlyU3Ek__BackingField_0)); }
	inline bool get_U3CIsReadOnlyU3Ek__BackingField_0() const { return ___U3CIsReadOnlyU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsReadOnlyU3Ek__BackingField_0() { return &___U3CIsReadOnlyU3Ek__BackingField_0; }
	inline void set_U3CIsReadOnlyU3Ek__BackingField_0(bool value)
	{
		___U3CIsReadOnlyU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_OnSettingChangedEvent_1() { return static_cast<int32_t>(offsetof(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7, ___OnSettingChangedEvent_1)); }
	inline Action_4_tD5AF77A6B3321DC818D2277334CC18D40AD628CC * get_OnSettingChangedEvent_1() const { return ___OnSettingChangedEvent_1; }
	inline Action_4_tD5AF77A6B3321DC818D2277334CC18D40AD628CC ** get_address_of_OnSettingChangedEvent_1() { return &___OnSettingChangedEvent_1; }
	inline void set_OnSettingChangedEvent_1(Action_4_tD5AF77A6B3321DC818D2277334CC18D40AD628CC * value)
	{
		___OnSettingChangedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnSettingChangedEvent_1), value);
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7, ___values_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_values_2() const { return ___values_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}

	inline static int32_t get_offset_of_changeFlags_3() { return static_cast<int32_t>(offsetof(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7, ___changeFlags_3)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_changeFlags_3() const { return ___changeFlags_3; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_changeFlags_3() { return &___changeFlags_3; }
	inline void set_changeFlags_3(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___changeFlags_3 = value;
		Il2CppCodeGenWriteBarrier((&___changeFlags_3), value);
	}

	inline static int32_t get_offset_of_U3CIsChangedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7, ___U3CIsChangedU3Ek__BackingField_4)); }
	inline bool get_U3CIsChangedU3Ek__BackingField_4() const { return ___U3CIsChangedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsChangedU3Ek__BackingField_4() { return &___U3CIsChangedU3Ek__BackingField_4; }
	inline void set_U3CIsChangedU3Ek__BackingField_4(bool value)
	{
		___U3CIsChangedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2SETTINGSREGISTRY_TD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7_H
#ifndef HUFFMANENCODER_TD12C454A95D6965729BE74C6D44A008D1BF86611_H
#define HUFFMANENCODER_TD12C454A95D6965729BE74C6D44A008D1BF86611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HuffmanEncoder
struct  HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611  : public RuntimeObject
{
public:

public:
};

struct HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611_StaticFields
{
public:
	// BestHTTP.Connections.HTTP2.HuffmanEncoder_TableEntry[] BestHTTP.Connections.HTTP2.HuffmanEncoder::StaticTable
	TableEntryU5BU5D_t64B010C2A2FF090453C365DCCE6B894DE1509060* ___StaticTable_1;
	// BestHTTP.Connections.HTTP2.HuffmanEncoder_TreeNode[] BestHTTP.Connections.HTTP2.HuffmanEncoder::HuffmanTree
	TreeNodeU5BU5D_t8F0ABFA7494FC10BF7611FAF6D081E9F6ED3F591* ___HuffmanTree_2;

public:
	inline static int32_t get_offset_of_StaticTable_1() { return static_cast<int32_t>(offsetof(HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611_StaticFields, ___StaticTable_1)); }
	inline TableEntryU5BU5D_t64B010C2A2FF090453C365DCCE6B894DE1509060* get_StaticTable_1() const { return ___StaticTable_1; }
	inline TableEntryU5BU5D_t64B010C2A2FF090453C365DCCE6B894DE1509060** get_address_of_StaticTable_1() { return &___StaticTable_1; }
	inline void set_StaticTable_1(TableEntryU5BU5D_t64B010C2A2FF090453C365DCCE6B894DE1509060* value)
	{
		___StaticTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___StaticTable_1), value);
	}

	inline static int32_t get_offset_of_HuffmanTree_2() { return static_cast<int32_t>(offsetof(HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611_StaticFields, ___HuffmanTree_2)); }
	inline TreeNodeU5BU5D_t8F0ABFA7494FC10BF7611FAF6D081E9F6ED3F591* get_HuffmanTree_2() const { return ___HuffmanTree_2; }
	inline TreeNodeU5BU5D_t8F0ABFA7494FC10BF7611FAF6D081E9F6ED3F591** get_address_of_HuffmanTree_2() { return &___HuffmanTree_2; }
	inline void set_HuffmanTree_2(TreeNodeU5BU5D_t8F0ABFA7494FC10BF7611FAF6D081E9F6ED3F591* value)
	{
		___HuffmanTree_2 = value;
		Il2CppCodeGenWriteBarrier((&___HuffmanTree_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUFFMANENCODER_TD12C454A95D6965729BE74C6D44A008D1BF86611_H
#ifndef HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#define HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPResponse
struct  HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.HTTPResponse::<VersionMajor>k__BackingField
	int32_t ___U3CVersionMajorU3Ek__BackingField_3;
	// System.Int32 BestHTTP.HTTPResponse::<VersionMinor>k__BackingField
	int32_t ___U3CVersionMinorU3Ek__BackingField_4;
	// System.Int32 BestHTTP.HTTPResponse::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_5;
	// System.String BestHTTP.HTTPResponse::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_6;
	// System.Boolean BestHTTP.HTTPResponse::<IsStreamed>k__BackingField
	bool ___U3CIsStreamedU3Ek__BackingField_7;
	// System.Boolean BestHTTP.HTTPResponse::<IsFromCache>k__BackingField
	bool ___U3CIsFromCacheU3Ek__BackingField_8;
	// BestHTTP.Caching.HTTPCacheFileInfo BestHTTP.HTTPResponse::<CacheFileInfo>k__BackingField
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * ___U3CCacheFileInfoU3Ek__BackingField_9;
	// System.Boolean BestHTTP.HTTPResponse::<IsCacheOnly>k__BackingField
	bool ___U3CIsCacheOnlyU3Ek__BackingField_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> BestHTTP.HTTPResponse::<Headers>k__BackingField
	Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * ___U3CHeadersU3Ek__BackingField_11;
	// System.Byte[] BestHTTP.HTTPResponse::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_12;
	// System.Boolean BestHTTP.HTTPResponse::<IsUpgraded>k__BackingField
	bool ___U3CIsUpgradedU3Ek__BackingField_13;
	// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie> BestHTTP.HTTPResponse::<Cookies>k__BackingField
	List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * ___U3CCookiesU3Ek__BackingField_14;
	// System.String BestHTTP.HTTPResponse::dataAsText
	String_t* ___dataAsText_15;
	// UnityEngine.Texture2D BestHTTP.HTTPResponse::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_16;
	// System.Boolean BestHTTP.HTTPResponse::<IsClosedManually>k__BackingField
	bool ___U3CIsClosedManuallyU3Ek__BackingField_17;
	// System.Int64 BestHTTP.HTTPResponse::UnprocessedFragments
	int64_t ___UnprocessedFragments_18;
	// BestHTTP.HTTPRequest BestHTTP.HTTPResponse::baseRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___baseRequest_19;
	// System.IO.Stream BestHTTP.HTTPResponse::Stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_20;
	// System.Byte[] BestHTTP.HTTPResponse::fragmentBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fragmentBuffer_21;
	// System.Int32 BestHTTP.HTTPResponse::fragmentBufferDataLength
	int32_t ___fragmentBufferDataLength_22;
	// System.IO.Stream BestHTTP.HTTPResponse::cacheStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___cacheStream_23;
	// System.Int32 BestHTTP.HTTPResponse::allFragmentSize
	int32_t ___allFragmentSize_24;

public:
	inline static int32_t get_offset_of_U3CVersionMajorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CVersionMajorU3Ek__BackingField_3)); }
	inline int32_t get_U3CVersionMajorU3Ek__BackingField_3() const { return ___U3CVersionMajorU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CVersionMajorU3Ek__BackingField_3() { return &___U3CVersionMajorU3Ek__BackingField_3; }
	inline void set_U3CVersionMajorU3Ek__BackingField_3(int32_t value)
	{
		___U3CVersionMajorU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVersionMinorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CVersionMinorU3Ek__BackingField_4)); }
	inline int32_t get_U3CVersionMinorU3Ek__BackingField_4() const { return ___U3CVersionMinorU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CVersionMinorU3Ek__BackingField_4() { return &___U3CVersionMinorU3Ek__BackingField_4; }
	inline void set_U3CVersionMinorU3Ek__BackingField_4(int32_t value)
	{
		___U3CVersionMinorU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CStatusCodeU3Ek__BackingField_5)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_5() const { return ___U3CStatusCodeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_5() { return &___U3CStatusCodeU3Ek__BackingField_5; }
	inline void set_U3CStatusCodeU3Ek__BackingField_5(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CMessageU3Ek__BackingField_6)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_6() const { return ___U3CMessageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_6() { return &___U3CMessageU3Ek__BackingField_6; }
	inline void set_U3CMessageU3Ek__BackingField_6(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsStreamedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsStreamedU3Ek__BackingField_7)); }
	inline bool get_U3CIsStreamedU3Ek__BackingField_7() const { return ___U3CIsStreamedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsStreamedU3Ek__BackingField_7() { return &___U3CIsStreamedU3Ek__BackingField_7; }
	inline void set_U3CIsStreamedU3Ek__BackingField_7(bool value)
	{
		___U3CIsStreamedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsFromCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsFromCacheU3Ek__BackingField_8)); }
	inline bool get_U3CIsFromCacheU3Ek__BackingField_8() const { return ___U3CIsFromCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsFromCacheU3Ek__BackingField_8() { return &___U3CIsFromCacheU3Ek__BackingField_8; }
	inline void set_U3CIsFromCacheU3Ek__BackingField_8(bool value)
	{
		___U3CIsFromCacheU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCacheFileInfoU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CCacheFileInfoU3Ek__BackingField_9)); }
	inline HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * get_U3CCacheFileInfoU3Ek__BackingField_9() const { return ___U3CCacheFileInfoU3Ek__BackingField_9; }
	inline HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D ** get_address_of_U3CCacheFileInfoU3Ek__BackingField_9() { return &___U3CCacheFileInfoU3Ek__BackingField_9; }
	inline void set_U3CCacheFileInfoU3Ek__BackingField_9(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D * value)
	{
		___U3CCacheFileInfoU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheFileInfoU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIsCacheOnlyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsCacheOnlyU3Ek__BackingField_10)); }
	inline bool get_U3CIsCacheOnlyU3Ek__BackingField_10() const { return ___U3CIsCacheOnlyU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsCacheOnlyU3Ek__BackingField_10() { return &___U3CIsCacheOnlyU3Ek__BackingField_10; }
	inline void set_U3CIsCacheOnlyU3Ek__BackingField_10(bool value)
	{
		___U3CIsCacheOnlyU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CHeadersU3Ek__BackingField_11)); }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * get_U3CHeadersU3Ek__BackingField_11() const { return ___U3CHeadersU3Ek__BackingField_11; }
	inline Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 ** get_address_of_U3CHeadersU3Ek__BackingField_11() { return &___U3CHeadersU3Ek__BackingField_11; }
	inline void set_U3CHeadersU3Ek__BackingField_11(Dictionary_2_t4EED052EB194F834A8DDF529C1A032B9EB853A26 * value)
	{
		___U3CHeadersU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CDataU3Ek__BackingField_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CIsUpgradedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsUpgradedU3Ek__BackingField_13)); }
	inline bool get_U3CIsUpgradedU3Ek__BackingField_13() const { return ___U3CIsUpgradedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsUpgradedU3Ek__BackingField_13() { return &___U3CIsUpgradedU3Ek__BackingField_13; }
	inline void set_U3CIsUpgradedU3Ek__BackingField_13(bool value)
	{
		___U3CIsUpgradedU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CCookiesU3Ek__BackingField_14)); }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * get_U3CCookiesU3Ek__BackingField_14() const { return ___U3CCookiesU3Ek__BackingField_14; }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 ** get_address_of_U3CCookiesU3Ek__BackingField_14() { return &___U3CCookiesU3Ek__BackingField_14; }
	inline void set_U3CCookiesU3Ek__BackingField_14(List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * value)
	{
		___U3CCookiesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookiesU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_dataAsText_15() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___dataAsText_15)); }
	inline String_t* get_dataAsText_15() const { return ___dataAsText_15; }
	inline String_t** get_address_of_dataAsText_15() { return &___dataAsText_15; }
	inline void set_dataAsText_15(String_t* value)
	{
		___dataAsText_15 = value;
		Il2CppCodeGenWriteBarrier((&___dataAsText_15), value);
	}

	inline static int32_t get_offset_of_texture_16() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___texture_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_16() const { return ___texture_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_16() { return &___texture_16; }
	inline void set_texture_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_16 = value;
		Il2CppCodeGenWriteBarrier((&___texture_16), value);
	}

	inline static int32_t get_offset_of_U3CIsClosedManuallyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___U3CIsClosedManuallyU3Ek__BackingField_17)); }
	inline bool get_U3CIsClosedManuallyU3Ek__BackingField_17() const { return ___U3CIsClosedManuallyU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsClosedManuallyU3Ek__BackingField_17() { return &___U3CIsClosedManuallyU3Ek__BackingField_17; }
	inline void set_U3CIsClosedManuallyU3Ek__BackingField_17(bool value)
	{
		___U3CIsClosedManuallyU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_UnprocessedFragments_18() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___UnprocessedFragments_18)); }
	inline int64_t get_UnprocessedFragments_18() const { return ___UnprocessedFragments_18; }
	inline int64_t* get_address_of_UnprocessedFragments_18() { return &___UnprocessedFragments_18; }
	inline void set_UnprocessedFragments_18(int64_t value)
	{
		___UnprocessedFragments_18 = value;
	}

	inline static int32_t get_offset_of_baseRequest_19() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___baseRequest_19)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_baseRequest_19() const { return ___baseRequest_19; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_baseRequest_19() { return &___baseRequest_19; }
	inline void set_baseRequest_19(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___baseRequest_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseRequest_19), value);
	}

	inline static int32_t get_offset_of_Stream_20() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___Stream_20)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Stream_20() const { return ___Stream_20; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Stream_20() { return &___Stream_20; }
	inline void set_Stream_20(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Stream_20 = value;
		Il2CppCodeGenWriteBarrier((&___Stream_20), value);
	}

	inline static int32_t get_offset_of_fragmentBuffer_21() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___fragmentBuffer_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fragmentBuffer_21() const { return ___fragmentBuffer_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fragmentBuffer_21() { return &___fragmentBuffer_21; }
	inline void set_fragmentBuffer_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fragmentBuffer_21 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentBuffer_21), value);
	}

	inline static int32_t get_offset_of_fragmentBufferDataLength_22() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___fragmentBufferDataLength_22)); }
	inline int32_t get_fragmentBufferDataLength_22() const { return ___fragmentBufferDataLength_22; }
	inline int32_t* get_address_of_fragmentBufferDataLength_22() { return &___fragmentBufferDataLength_22; }
	inline void set_fragmentBufferDataLength_22(int32_t value)
	{
		___fragmentBufferDataLength_22 = value;
	}

	inline static int32_t get_offset_of_cacheStream_23() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___cacheStream_23)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_cacheStream_23() const { return ___cacheStream_23; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_cacheStream_23() { return &___cacheStream_23; }
	inline void set_cacheStream_23(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___cacheStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___cacheStream_23), value);
	}

	inline static int32_t get_offset_of_allFragmentSize_24() { return static_cast<int32_t>(offsetof(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB, ___allFragmentSize_24)); }
	inline int32_t get_allFragmentSize_24() const { return ___allFragmentSize_24; }
	inline int32_t* get_address_of_allFragmentSize_24() { return &___allFragmentSize_24; }
	inline void set_allFragmentSize_24(int32_t value)
	{
		___allFragmentSize_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSE_T884F650C82582A1F54E9A53B1AA131F76D12DDDB_H
#ifndef DEFAULTRETRYPOLICY_T8D890270E46A09EBED2A8351FD9D578863E9763A_H
#define DEFAULTRETRYPOLICY_T8D890270E46A09EBED2A8351FD9D578863E9763A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.DefaultRetryPolicy
struct  DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A  : public RuntimeObject
{
public:
	// System.Nullable`1<System.TimeSpan>[] BestHTTP.SignalRCore.DefaultRetryPolicy::backoffTimes
	Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B* ___backoffTimes_1;

public:
	inline static int32_t get_offset_of_backoffTimes_1() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A, ___backoffTimes_1)); }
	inline Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B* get_backoffTimes_1() const { return ___backoffTimes_1; }
	inline Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B** get_address_of_backoffTimes_1() { return &___backoffTimes_1; }
	inline void set_backoffTimes_1(Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B* value)
	{
		___backoffTimes_1 = value;
		Il2CppCodeGenWriteBarrier((&___backoffTimes_1), value);
	}
};

struct DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A_StaticFields
{
public:
	// System.Nullable`1<System.TimeSpan>[] BestHTTP.SignalRCore.DefaultRetryPolicy::DefaultBackoffTimes
	Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B* ___DefaultBackoffTimes_0;

public:
	inline static int32_t get_offset_of_DefaultBackoffTimes_0() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A_StaticFields, ___DefaultBackoffTimes_0)); }
	inline Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B* get_DefaultBackoffTimes_0() const { return ___DefaultBackoffTimes_0; }
	inline Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B** get_address_of_DefaultBackoffTimes_0() { return &___DefaultBackoffTimes_0; }
	inline void set_DefaultBackoffTimes_0(Nullable_1U5BU5D_tD2AEED429B5A2102866CF287697C909DEB78776B* value)
	{
		___DefaultBackoffTimes_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultBackoffTimes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTRETRYPOLICY_T8D890270E46A09EBED2A8351FD9D578863E9763A_H
#ifndef U3CU3EC__DISPLAYCLASS81_0_T388C24614AFACC9BC9527C572218EB94A20D2BED_H
#define U3CU3EC__DISPLAYCLASS81_0_T388C24614AFACC9BC9527C572218EB94A20D2BED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.HubConnection_<>c__DisplayClass81_0
struct  U3CU3Ec__DisplayClass81_0_t388C24614AFACC9BC9527C572218EB94A20D2BED  : public RuntimeObject
{
public:
	// BestHTTP.Futures.Future`1<System.Boolean> BestHTTP.SignalRCore.HubConnection_<>c__DisplayClass81_0::future
	Future_1_t205FF123D3DF6CFB0B06E431BB815461424A7FB5 * ___future_0;

public:
	inline static int32_t get_offset_of_future_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t388C24614AFACC9BC9527C572218EB94A20D2BED, ___future_0)); }
	inline Future_1_t205FF123D3DF6CFB0B06E431BB815461424A7FB5 * get_future_0() const { return ___future_0; }
	inline Future_1_t205FF123D3DF6CFB0B06E431BB815461424A7FB5 ** get_address_of_future_0() { return &___future_0; }
	inline void set_future_0(Future_1_t205FF123D3DF6CFB0B06E431BB815461424A7FB5 * value)
	{
		___future_0 = value;
		Il2CppCodeGenWriteBarrier((&___future_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS81_0_T388C24614AFACC9BC9527C572218EB94A20D2BED_H
#ifndef U3CU3EC__DISPLAYCLASS86_0_T2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD_H
#define U3CU3EC__DISPLAYCLASS86_0_T2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.HubConnection_<>c__DisplayClass86_0
struct  U3CU3Ec__DisplayClass86_0_t2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD  : public RuntimeObject
{
public:
	// System.Action BestHTTP.SignalRCore.HubConnection_<>c__DisplayClass86_0::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass86_0_t2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD, ___callback_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_0() const { return ___callback_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS86_0_T2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD_H
#ifndef HUBCONNECTIONEXTENSIONS_TCC6FEFA722402BE7DD02215B6BF42FF566214B31_H
#define HUBCONNECTIONEXTENSIONS_TCC6FEFA722402BE7DD02215B6BF42FF566214B31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.HubConnectionExtensions
struct  HubConnectionExtensions_tCC6FEFA722402BE7DD02215B6BF42FF566214B31  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUBCONNECTIONEXTENSIONS_TCC6FEFA722402BE7DD02215B6BF42FF566214B31_H
#ifndef JSONPROTOCOL_TC91D79DEB95133DAC9A2D01382F286AB846F9A13_H
#define JSONPROTOCOL_TC91D79DEB95133DAC9A2D01382F286AB846F9A13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.JsonProtocol
struct  JsonProtocol_tC91D79DEB95133DAC9A2D01382F286AB846F9A13  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.IEncoder BestHTTP.SignalRCore.JsonProtocol::<Encoder>k__BackingField
	RuntimeObject* ___U3CEncoderU3Ek__BackingField_1;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.SignalRCore.JsonProtocol::<Connection>k__BackingField
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___U3CConnectionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CEncoderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonProtocol_tC91D79DEB95133DAC9A2D01382F286AB846F9A13, ___U3CEncoderU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CEncoderU3Ek__BackingField_1() const { return ___U3CEncoderU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CEncoderU3Ek__BackingField_1() { return &___U3CEncoderU3Ek__BackingField_1; }
	inline void set_U3CEncoderU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CEncoderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEncoderU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsonProtocol_tC91D79DEB95133DAC9A2D01382F286AB846F9A13, ___U3CConnectionU3Ek__BackingField_2)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_U3CConnectionU3Ek__BackingField_2() const { return ___U3CConnectionU3Ek__BackingField_2; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_U3CConnectionU3Ek__BackingField_2() { return &___U3CConnectionU3Ek__BackingField_2; }
	inline void set_U3CConnectionU3Ek__BackingField_2(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___U3CConnectionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROTOCOL_TC91D79DEB95133DAC9A2D01382F286AB846F9A13_H
#ifndef SUBSCRIPTION_TB5D06FD88B601F3A83353605DAA91FC4ECB0158E_H
#define SUBSCRIPTION_TB5D06FD88B601F3A83353605DAA91FC4ECB0158E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Subscription
struct  Subscription_tB5D06FD88B601F3A83353605DAA91FC4ECB0158E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.CallbackDescriptor> BestHTTP.SignalRCore.Subscription::callbacks
	List_1_tB4F97622006C9DF92F34633E2EDBC8ACEE3883A5 * ___callbacks_0;
	// System.Threading.ReaderWriterLockSlim BestHTTP.SignalRCore.Subscription::lockSlim
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___lockSlim_1;

public:
	inline static int32_t get_offset_of_callbacks_0() { return static_cast<int32_t>(offsetof(Subscription_tB5D06FD88B601F3A83353605DAA91FC4ECB0158E, ___callbacks_0)); }
	inline List_1_tB4F97622006C9DF92F34633E2EDBC8ACEE3883A5 * get_callbacks_0() const { return ___callbacks_0; }
	inline List_1_tB4F97622006C9DF92F34633E2EDBC8ACEE3883A5 ** get_address_of_callbacks_0() { return &___callbacks_0; }
	inline void set_callbacks_0(List_1_tB4F97622006C9DF92F34633E2EDBC8ACEE3883A5 * value)
	{
		___callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbacks_0), value);
	}

	inline static int32_t get_offset_of_lockSlim_1() { return static_cast<int32_t>(offsetof(Subscription_tB5D06FD88B601F3A83353605DAA91FC4ECB0158E, ___lockSlim_1)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_lockSlim_1() const { return ___lockSlim_1; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_lockSlim_1() { return &___lockSlim_1; }
	inline void set_lockSlim_1(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___lockSlim_1 = value;
		Il2CppCodeGenWriteBarrier((&___lockSlim_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIPTION_TB5D06FD88B601F3A83353605DAA91FC4ECB0158E_H
#ifndef UPLOADITEMCONTROLLEREXTENSIONS_TA32388807D27315414494327CA79D39F5FA0D3E6_H
#define UPLOADITEMCONTROLLEREXTENSIONS_TA32388807D27315414494327CA79D39F5FA0D3E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.UploadItemControllerExtensions
struct  UploadItemControllerExtensions_tA32388807D27315414494327CA79D39F5FA0D3E6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADITEMCONTROLLEREXTENSIONS_TA32388807D27315414494327CA79D39F5FA0D3E6_H
#ifndef EVENTDESCRIPTOR_TC46E7837EBF07087525A7F1C7FE162BA921885B2_H
#define EVENTDESCRIPTOR_TC46E7837EBF07087525A7F1C7FE162BA921885B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Events.EventDescriptor
struct  EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<BestHTTP.SocketIO.Events.SocketIOCallback> BestHTTP.SocketIO.Events.EventDescriptor::<Callbacks>k__BackingField
	List_1_t70634F31D27CDB5DE9D014091D7F1865129DB7C8 * ___U3CCallbacksU3Ek__BackingField_0;
	// System.Boolean BestHTTP.SocketIO.Events.EventDescriptor::<OnlyOnce>k__BackingField
	bool ___U3COnlyOnceU3Ek__BackingField_1;
	// System.Boolean BestHTTP.SocketIO.Events.EventDescriptor::<AutoDecodePayload>k__BackingField
	bool ___U3CAutoDecodePayloadU3Ek__BackingField_2;
	// BestHTTP.SocketIO.Events.SocketIOCallback[] BestHTTP.SocketIO.Events.EventDescriptor::CallbackArray
	SocketIOCallbackU5BU5D_tB05DA025023C5E308BF7C6D93EA8595B338E041C* ___CallbackArray_3;

public:
	inline static int32_t get_offset_of_U3CCallbacksU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2, ___U3CCallbacksU3Ek__BackingField_0)); }
	inline List_1_t70634F31D27CDB5DE9D014091D7F1865129DB7C8 * get_U3CCallbacksU3Ek__BackingField_0() const { return ___U3CCallbacksU3Ek__BackingField_0; }
	inline List_1_t70634F31D27CDB5DE9D014091D7F1865129DB7C8 ** get_address_of_U3CCallbacksU3Ek__BackingField_0() { return &___U3CCallbacksU3Ek__BackingField_0; }
	inline void set_U3CCallbacksU3Ek__BackingField_0(List_1_t70634F31D27CDB5DE9D014091D7F1865129DB7C8 * value)
	{
		___U3CCallbacksU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbacksU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COnlyOnceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2, ___U3COnlyOnceU3Ek__BackingField_1)); }
	inline bool get_U3COnlyOnceU3Ek__BackingField_1() const { return ___U3COnlyOnceU3Ek__BackingField_1; }
	inline bool* get_address_of_U3COnlyOnceU3Ek__BackingField_1() { return &___U3COnlyOnceU3Ek__BackingField_1; }
	inline void set_U3COnlyOnceU3Ek__BackingField_1(bool value)
	{
		___U3COnlyOnceU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2, ___U3CAutoDecodePayloadU3Ek__BackingField_2)); }
	inline bool get_U3CAutoDecodePayloadU3Ek__BackingField_2() const { return ___U3CAutoDecodePayloadU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAutoDecodePayloadU3Ek__BackingField_2() { return &___U3CAutoDecodePayloadU3Ek__BackingField_2; }
	inline void set_U3CAutoDecodePayloadU3Ek__BackingField_2(bool value)
	{
		___U3CAutoDecodePayloadU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_CallbackArray_3() { return static_cast<int32_t>(offsetof(EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2, ___CallbackArray_3)); }
	inline SocketIOCallbackU5BU5D_tB05DA025023C5E308BF7C6D93EA8595B338E041C* get_CallbackArray_3() const { return ___CallbackArray_3; }
	inline SocketIOCallbackU5BU5D_tB05DA025023C5E308BF7C6D93EA8595B338E041C** get_address_of_CallbackArray_3() { return &___CallbackArray_3; }
	inline void set_CallbackArray_3(SocketIOCallbackU5BU5D_tB05DA025023C5E308BF7C6D93EA8595B338E041C* value)
	{
		___CallbackArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___CallbackArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDESCRIPTOR_TC46E7837EBF07087525A7F1C7FE162BA921885B2_H
#ifndef EVENTNAMES_T9772245AFC17A8E11D57918312B10753AC7D7861_H
#define EVENTNAMES_T9772245AFC17A8E11D57918312B10753AC7D7861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Events.EventNames
struct  EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861  : public RuntimeObject
{
public:

public:
};

struct EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields
{
public:
	// System.String[] BestHTTP.SocketIO.Events.EventNames::SocketIONames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___SocketIONames_7;
	// System.String[] BestHTTP.SocketIO.Events.EventNames::TransportNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___TransportNames_8;
	// System.String[] BestHTTP.SocketIO.Events.EventNames::BlacklistedEvents
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___BlacklistedEvents_9;

public:
	inline static int32_t get_offset_of_SocketIONames_7() { return static_cast<int32_t>(offsetof(EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields, ___SocketIONames_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_SocketIONames_7() const { return ___SocketIONames_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_SocketIONames_7() { return &___SocketIONames_7; }
	inline void set_SocketIONames_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___SocketIONames_7 = value;
		Il2CppCodeGenWriteBarrier((&___SocketIONames_7), value);
	}

	inline static int32_t get_offset_of_TransportNames_8() { return static_cast<int32_t>(offsetof(EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields, ___TransportNames_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_TransportNames_8() const { return ___TransportNames_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_TransportNames_8() { return &___TransportNames_8; }
	inline void set_TransportNames_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___TransportNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___TransportNames_8), value);
	}

	inline static int32_t get_offset_of_BlacklistedEvents_9() { return static_cast<int32_t>(offsetof(EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields, ___BlacklistedEvents_9)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_BlacklistedEvents_9() const { return ___BlacklistedEvents_9; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_BlacklistedEvents_9() { return &___BlacklistedEvents_9; }
	inline void set_BlacklistedEvents_9(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___BlacklistedEvents_9 = value;
		Il2CppCodeGenWriteBarrier((&___BlacklistedEvents_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTNAMES_T9772245AFC17A8E11D57918312B10753AC7D7861_H
#ifndef EVENTTABLE_T89D3B094ECFC84485A9DC1509690A130356ECB8F_H
#define EVENTTABLE_T89D3B094ECFC84485A9DC1509690A130356ECB8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Events.EventTable
struct  EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.Socket BestHTTP.SocketIO.Events.EventTable::<Socket>k__BackingField
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1 * ___U3CSocketU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<BestHTTP.SocketIO.Events.EventDescriptor>> BestHTTP.SocketIO.Events.EventTable::Table
	Dictionary_2_t7FE30E00FB54BCA5B530D0CF1D376F77838E266F * ___Table_1;

public:
	inline static int32_t get_offset_of_U3CSocketU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F, ___U3CSocketU3Ek__BackingField_0)); }
	inline Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1 * get_U3CSocketU3Ek__BackingField_0() const { return ___U3CSocketU3Ek__BackingField_0; }
	inline Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1 ** get_address_of_U3CSocketU3Ek__BackingField_0() { return &___U3CSocketU3Ek__BackingField_0; }
	inline void set_U3CSocketU3Ek__BackingField_0(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1 * value)
	{
		___U3CSocketU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_Table_1() { return static_cast<int32_t>(offsetof(EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F, ___Table_1)); }
	inline Dictionary_2_t7FE30E00FB54BCA5B530D0CF1D376F77838E266F * get_Table_1() const { return ___Table_1; }
	inline Dictionary_2_t7FE30E00FB54BCA5B530D0CF1D376F77838E266F ** get_address_of_Table_1() { return &___Table_1; }
	inline void set_Table_1(Dictionary_2_t7FE30E00FB54BCA5B530D0CF1D376F77838E266F * value)
	{
		___Table_1 = value;
		Il2CppCodeGenWriteBarrier((&___Table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTABLE_T89D3B094ECFC84485A9DC1509690A130356ECB8F_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8_H
#define U3CU3EC__DISPLAYCLASS6_0_T13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Events.EventTable_<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SocketIO.Events.EventTable_<>c__DisplayClass6_0::onlyOnce
	bool ___onlyOnce_0;
	// System.Boolean BestHTTP.SocketIO.Events.EventTable_<>c__DisplayClass6_0::autoDecodePayload
	bool ___autoDecodePayload_1;

public:
	inline static int32_t get_offset_of_onlyOnce_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8, ___onlyOnce_0)); }
	inline bool get_onlyOnce_0() const { return ___onlyOnce_0; }
	inline bool* get_address_of_onlyOnce_0() { return &___onlyOnce_0; }
	inline void set_onlyOnce_0(bool value)
	{
		___onlyOnce_0 = value;
	}

	inline static int32_t get_offset_of_autoDecodePayload_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8, ___autoDecodePayload_1)); }
	inline bool get_autoDecodePayload_1() const { return ___autoDecodePayload_1; }
	inline bool* get_address_of_autoDecodePayload_1() { return &___autoDecodePayload_1; }
	inline void set_autoDecodePayload_1(bool value)
	{
		___autoDecodePayload_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8_H
#ifndef DEFAULTJSONENCODER_T7F42AC52EAEA6AF5C859579445C2FFBCD173E85F_H
#define DEFAULTJSONENCODER_T7F42AC52EAEA6AF5C859579445C2FFBCD173E85F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.JsonEncoders.DefaultJSonEncoder
struct  DefaultJSonEncoder_t7F42AC52EAEA6AF5C859579445C2FFBCD173E85F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTJSONENCODER_T7F42AC52EAEA6AF5C859579445C2FFBCD173E85F_H
#ifndef LITJSONENCODER_T765B1C61CF89738149398AA193741ABE337F60E9_H
#define LITJSONENCODER_T765B1C61CF89738149398AA193741ABE337F60E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.JsonEncoders.LitJsonEncoder
struct  LitJsonEncoder_t765B1C61CF89738149398AA193741ABE337F60E9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITJSONENCODER_T765B1C61CF89738149398AA193741ABE337F60E9_H
#ifndef SOCKET_TAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1_H
#define SOCKET_TAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Socket
struct  Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.SocketManager BestHTTP.SocketIO.Socket::<Manager>k__BackingField
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * ___U3CManagerU3Ek__BackingField_0;
	// System.String BestHTTP.SocketIO.Socket::<Namespace>k__BackingField
	String_t* ___U3CNamespaceU3Ek__BackingField_1;
	// System.String BestHTTP.SocketIO.Socket::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_2;
	// System.Boolean BestHTTP.SocketIO.Socket::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_3;
	// System.Boolean BestHTTP.SocketIO.Socket::<AutoDecodePayload>k__BackingField
	bool ___U3CAutoDecodePayloadU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,BestHTTP.SocketIO.Events.SocketIOAckCallback> BestHTTP.SocketIO.Socket::AckCallbacks
	Dictionary_2_tB7DFA8D440B5E5C6F203D239BC1B5052E191FFA2 * ___AckCallbacks_5;
	// BestHTTP.SocketIO.Events.EventTable BestHTTP.SocketIO.Socket::EventCallbacks
	EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F * ___EventCallbacks_6;
	// System.Collections.Generic.List`1<System.Object> BestHTTP.SocketIO.Socket::arguments
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___arguments_7;

public:
	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___U3CManagerU3Ek__BackingField_0)); }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * get_U3CManagerU3Ek__BackingField_0() const { return ___U3CManagerU3Ek__BackingField_0; }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 ** get_address_of_U3CManagerU3Ek__BackingField_0() { return &___U3CManagerU3Ek__BackingField_0; }
	inline void set_U3CManagerU3Ek__BackingField_0(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * value)
	{
		___U3CManagerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManagerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___U3CNamespaceU3Ek__BackingField_1)); }
	inline String_t* get_U3CNamespaceU3Ek__BackingField_1() const { return ___U3CNamespaceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNamespaceU3Ek__BackingField_1() { return &___U3CNamespaceU3Ek__BackingField_1; }
	inline void set_U3CNamespaceU3Ek__BackingField_1(String_t* value)
	{
		___U3CNamespaceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamespaceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___U3CIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CIdU3Ek__BackingField_2() const { return ___U3CIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_2() { return &___U3CIdU3Ek__BackingField_2; }
	inline void set_U3CIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___U3CIsOpenU3Ek__BackingField_3)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_3() const { return ___U3CIsOpenU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_3() { return &___U3CIsOpenU3Ek__BackingField_3; }
	inline void set_U3CIsOpenU3Ek__BackingField_3(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___U3CAutoDecodePayloadU3Ek__BackingField_4)); }
	inline bool get_U3CAutoDecodePayloadU3Ek__BackingField_4() const { return ___U3CAutoDecodePayloadU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CAutoDecodePayloadU3Ek__BackingField_4() { return &___U3CAutoDecodePayloadU3Ek__BackingField_4; }
	inline void set_U3CAutoDecodePayloadU3Ek__BackingField_4(bool value)
	{
		___U3CAutoDecodePayloadU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_AckCallbacks_5() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___AckCallbacks_5)); }
	inline Dictionary_2_tB7DFA8D440B5E5C6F203D239BC1B5052E191FFA2 * get_AckCallbacks_5() const { return ___AckCallbacks_5; }
	inline Dictionary_2_tB7DFA8D440B5E5C6F203D239BC1B5052E191FFA2 ** get_address_of_AckCallbacks_5() { return &___AckCallbacks_5; }
	inline void set_AckCallbacks_5(Dictionary_2_tB7DFA8D440B5E5C6F203D239BC1B5052E191FFA2 * value)
	{
		___AckCallbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&___AckCallbacks_5), value);
	}

	inline static int32_t get_offset_of_EventCallbacks_6() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___EventCallbacks_6)); }
	inline EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F * get_EventCallbacks_6() const { return ___EventCallbacks_6; }
	inline EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F ** get_address_of_EventCallbacks_6() { return &___EventCallbacks_6; }
	inline void set_EventCallbacks_6(EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F * value)
	{
		___EventCallbacks_6 = value;
		Il2CppCodeGenWriteBarrier((&___EventCallbacks_6), value);
	}

	inline static int32_t get_offset_of_arguments_7() { return static_cast<int32_t>(offsetof(Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1, ___arguments_7)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_arguments_7() const { return ___arguments_7; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_arguments_7() { return &___arguments_7; }
	inline void set_arguments_7(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___arguments_7 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKET_TAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef HTTP2RESPONSE_T945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE_H
#define HTTP2RESPONSE_T945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2Response
struct  HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE  : public HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB
{
public:
	// System.Int32 BestHTTP.Connections.HTTP2.HTTP2Response::<ExpectedContentLength>k__BackingField
	int32_t ___U3CExpectedContentLengthU3Ek__BackingField_25;
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2Response::<IsCompressed>k__BackingField
	bool ___U3CIsCompressedU3Ek__BackingField_26;
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2Response::isPrepared
	bool ___isPrepared_27;
	// BestHTTP.Decompression.GZipDecompressor BestHTTP.Connections.HTTP2.HTTP2Response::decompressor
	GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C * ___decompressor_28;

public:
	inline static int32_t get_offset_of_U3CExpectedContentLengthU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE, ___U3CExpectedContentLengthU3Ek__BackingField_25)); }
	inline int32_t get_U3CExpectedContentLengthU3Ek__BackingField_25() const { return ___U3CExpectedContentLengthU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CExpectedContentLengthU3Ek__BackingField_25() { return &___U3CExpectedContentLengthU3Ek__BackingField_25; }
	inline void set_U3CExpectedContentLengthU3Ek__BackingField_25(int32_t value)
	{
		___U3CExpectedContentLengthU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CIsCompressedU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE, ___U3CIsCompressedU3Ek__BackingField_26)); }
	inline bool get_U3CIsCompressedU3Ek__BackingField_26() const { return ___U3CIsCompressedU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CIsCompressedU3Ek__BackingField_26() { return &___U3CIsCompressedU3Ek__BackingField_26; }
	inline void set_U3CIsCompressedU3Ek__BackingField_26(bool value)
	{
		___U3CIsCompressedU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_isPrepared_27() { return static_cast<int32_t>(offsetof(HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE, ___isPrepared_27)); }
	inline bool get_isPrepared_27() const { return ___isPrepared_27; }
	inline bool* get_address_of_isPrepared_27() { return &___isPrepared_27; }
	inline void set_isPrepared_27(bool value)
	{
		___isPrepared_27 = value;
	}

	inline static int32_t get_offset_of_decompressor_28() { return static_cast<int32_t>(offsetof(HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE, ___decompressor_28)); }
	inline GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C * get_decompressor_28() const { return ___decompressor_28; }
	inline GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C ** get_address_of_decompressor_28() { return &___decompressor_28; }
	inline void set_decompressor_28(GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C * value)
	{
		___decompressor_28 = value;
		Il2CppCodeGenWriteBarrier((&___decompressor_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2RESPONSE_T945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE_H
#ifndef TABLEENTRY_T4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF_H
#define TABLEENTRY_T4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HuffmanEncoder_TableEntry
struct  TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF 
{
public:
	// System.UInt32 BestHTTP.Connections.HTTP2.HuffmanEncoder_TableEntry::Code
	uint32_t ___Code_0;
	// System.Byte BestHTTP.Connections.HTTP2.HuffmanEncoder_TableEntry::Bits
	uint8_t ___Bits_1;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF, ___Code_0)); }
	inline uint32_t get_Code_0() const { return ___Code_0; }
	inline uint32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(uint32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Bits_1() { return static_cast<int32_t>(offsetof(TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF, ___Bits_1)); }
	inline uint8_t get_Bits_1() const { return ___Bits_1; }
	inline uint8_t* get_address_of_Bits_1() { return &___Bits_1; }
	inline void set_Bits_1(uint8_t value)
	{
		___Bits_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLEENTRY_T4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF_H
#ifndef TREENODE_T8B033BB2C8AD6721A4F346F34833134A600D4AB3_H
#define TREENODE_T8B033BB2C8AD6721A4F346F34833134A600D4AB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HuffmanEncoder_TreeNode
struct  TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3 
{
public:
	// System.UInt16 BestHTTP.Connections.HTTP2.HuffmanEncoder_TreeNode::NextZeroIdx
	uint16_t ___NextZeroIdx_0;
	// System.UInt16 BestHTTP.Connections.HTTP2.HuffmanEncoder_TreeNode::NextOneIdx
	uint16_t ___NextOneIdx_1;
	// System.UInt16 BestHTTP.Connections.HTTP2.HuffmanEncoder_TreeNode::Value
	uint16_t ___Value_2;

public:
	inline static int32_t get_offset_of_NextZeroIdx_0() { return static_cast<int32_t>(offsetof(TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3, ___NextZeroIdx_0)); }
	inline uint16_t get_NextZeroIdx_0() const { return ___NextZeroIdx_0; }
	inline uint16_t* get_address_of_NextZeroIdx_0() { return &___NextZeroIdx_0; }
	inline void set_NextZeroIdx_0(uint16_t value)
	{
		___NextZeroIdx_0 = value;
	}

	inline static int32_t get_offset_of_NextOneIdx_1() { return static_cast<int32_t>(offsetof(TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3, ___NextOneIdx_1)); }
	inline uint16_t get_NextOneIdx_1() const { return ___NextOneIdx_1; }
	inline uint16_t* get_address_of_NextOneIdx_1() { return &___NextOneIdx_1; }
	inline void set_NextOneIdx_1(uint16_t value)
	{
		___NextOneIdx_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3, ___Value_2)); }
	inline uint16_t get_Value_2() const { return ___Value_2; }
	inline uint16_t* get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(uint16_t value)
	{
		___Value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREENODE_T8B033BB2C8AD6721A4F346F34833134A600D4AB3_H
#ifndef CALLBACKDESCRIPTOR_T6C67B7EC8DDD49C7236E4850164791B251D986EE_H
#define CALLBACKDESCRIPTOR_T6C67B7EC8DDD49C7236E4850164791B251D986EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.CallbackDescriptor
struct  CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE 
{
public:
	// System.Type[] BestHTTP.SignalRCore.CallbackDescriptor::ParamTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___ParamTypes_0;
	// System.Action`1<System.Object[]> BestHTTP.SignalRCore.CallbackDescriptor::Callback
	Action_1_tEC9A54F57BB76CD226507E616D5D5A556A534798 * ___Callback_1;

public:
	inline static int32_t get_offset_of_ParamTypes_0() { return static_cast<int32_t>(offsetof(CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE, ___ParamTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_ParamTypes_0() const { return ___ParamTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_ParamTypes_0() { return &___ParamTypes_0; }
	inline void set_ParamTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___ParamTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___ParamTypes_0), value);
	}

	inline static int32_t get_offset_of_Callback_1() { return static_cast<int32_t>(offsetof(CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE, ___Callback_1)); }
	inline Action_1_tEC9A54F57BB76CD226507E616D5D5A556A534798 * get_Callback_1() const { return ___Callback_1; }
	inline Action_1_tEC9A54F57BB76CD226507E616D5D5A556A534798 ** get_address_of_Callback_1() { return &___Callback_1; }
	inline void set_Callback_1(Action_1_tEC9A54F57BB76CD226507E616D5D5A556A534798 * value)
	{
		___Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.CallbackDescriptor
struct CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE_marshaled_pinvoke
{
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___ParamTypes_0;
	Il2CppMethodPointer ___Callback_1;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.CallbackDescriptor
struct CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE_marshaled_com
{
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___ParamTypes_0;
	Il2CppMethodPointer ___Callback_1;
};
#endif // CALLBACKDESCRIPTOR_T6C67B7EC8DDD49C7236E4850164791B251D986EE_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_TBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7_H
#define NULLABLE_1_TBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Byte>
struct  Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7 
{
public:
	// T System.Nullable`1::value
	uint8_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7, ___value_0)); }
	inline uint8_t get_value_0() const { return ___value_0; }
	inline uint8_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint8_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7_H
#ifndef NULLABLE_1_T93F4507FF6707E69354E128E51498F80A7C40E5C_H
#define NULLABLE_1_T93F4507FF6707E69354E128E51498F80A7C40E5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.UInt32>
struct  Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C 
{
public:
	// T System.Nullable`1::value
	uint32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C, ___value_0)); }
	inline uint32_t get_value_0() const { return ___value_0; }
	inline uint32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T93F4507FF6707E69354E128E51498F80A7C40E5C_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef AUTHENTICATIONTYPES_T68B01D8972AAC6FA5506A0D64014031442F6754F_H
#define AUTHENTICATIONTYPES_T68B01D8972AAC6FA5506A0D64014031442F6754F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Authentication.AuthenticationTypes
struct  AuthenticationTypes_t68B01D8972AAC6FA5506A0D64014031442F6754F 
{
public:
	// System.Int32 BestHTTP.Authentication.AuthenticationTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationTypes_t68B01D8972AAC6FA5506A0D64014031442F6754F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONTYPES_T68B01D8972AAC6FA5506A0D64014031442F6754F_H
#ifndef HTTPCACHEFILEINFO_T47ADD76D6FCBE8C2640ADB6532577203ACC21A3D_H
#define HTTPCACHEFILEINFO_T47ADD76D6FCBE8C2640ADB6532577203ACC21A3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Caching.HTTPCacheFileInfo
struct  HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.Caching.HTTPCacheFileInfo::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_0;
	// System.DateTime BestHTTP.Caching.HTTPCacheFileInfo::<LastAccess>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CLastAccessU3Ek__BackingField_1;
	// System.Int32 BestHTTP.Caching.HTTPCacheFileInfo::<BodyLength>k__BackingField
	int32_t ___U3CBodyLengthU3Ek__BackingField_2;
	// System.String BestHTTP.Caching.HTTPCacheFileInfo::<ETag>k__BackingField
	String_t* ___U3CETagU3Ek__BackingField_3;
	// System.String BestHTTP.Caching.HTTPCacheFileInfo::<LastModified>k__BackingField
	String_t* ___U3CLastModifiedU3Ek__BackingField_4;
	// System.DateTime BestHTTP.Caching.HTTPCacheFileInfo::<Expires>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CExpiresU3Ek__BackingField_5;
	// System.Int64 BestHTTP.Caching.HTTPCacheFileInfo::<Age>k__BackingField
	int64_t ___U3CAgeU3Ek__BackingField_6;
	// System.Int64 BestHTTP.Caching.HTTPCacheFileInfo::<MaxAge>k__BackingField
	int64_t ___U3CMaxAgeU3Ek__BackingField_7;
	// System.DateTime BestHTTP.Caching.HTTPCacheFileInfo::<Date>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CDateU3Ek__BackingField_8;
	// System.Boolean BestHTTP.Caching.HTTPCacheFileInfo::<MustRevalidate>k__BackingField
	bool ___U3CMustRevalidateU3Ek__BackingField_9;
	// System.DateTime BestHTTP.Caching.HTTPCacheFileInfo::<Received>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CReceivedU3Ek__BackingField_10;
	// System.String BestHTTP.Caching.HTTPCacheFileInfo::<ConstructedPath>k__BackingField
	String_t* ___U3CConstructedPathU3Ek__BackingField_11;
	// System.UInt64 BestHTTP.Caching.HTTPCacheFileInfo::<MappedNameIDX>k__BackingField
	uint64_t ___U3CMappedNameIDXU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CUriU3Ek__BackingField_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_0() const { return ___U3CUriU3Ek__BackingField_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_0() { return &___U3CUriU3Ek__BackingField_0; }
	inline void set_U3CUriU3Ek__BackingField_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLastAccessU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CLastAccessU3Ek__BackingField_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CLastAccessU3Ek__BackingField_1() const { return ___U3CLastAccessU3Ek__BackingField_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CLastAccessU3Ek__BackingField_1() { return &___U3CLastAccessU3Ek__BackingField_1; }
	inline void set_U3CLastAccessU3Ek__BackingField_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CLastAccessU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CBodyLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CBodyLengthU3Ek__BackingField_2)); }
	inline int32_t get_U3CBodyLengthU3Ek__BackingField_2() const { return ___U3CBodyLengthU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CBodyLengthU3Ek__BackingField_2() { return &___U3CBodyLengthU3Ek__BackingField_2; }
	inline void set_U3CBodyLengthU3Ek__BackingField_2(int32_t value)
	{
		___U3CBodyLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CETagU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CETagU3Ek__BackingField_3)); }
	inline String_t* get_U3CETagU3Ek__BackingField_3() const { return ___U3CETagU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CETagU3Ek__BackingField_3() { return &___U3CETagU3Ek__BackingField_3; }
	inline void set_U3CETagU3Ek__BackingField_3(String_t* value)
	{
		___U3CETagU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CETagU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CLastModifiedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CLastModifiedU3Ek__BackingField_4)); }
	inline String_t* get_U3CLastModifiedU3Ek__BackingField_4() const { return ___U3CLastModifiedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CLastModifiedU3Ek__BackingField_4() { return &___U3CLastModifiedU3Ek__BackingField_4; }
	inline void set_U3CLastModifiedU3Ek__BackingField_4(String_t* value)
	{
		___U3CLastModifiedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastModifiedU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CExpiresU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CExpiresU3Ek__BackingField_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CExpiresU3Ek__BackingField_5() const { return ___U3CExpiresU3Ek__BackingField_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CExpiresU3Ek__BackingField_5() { return &___U3CExpiresU3Ek__BackingField_5; }
	inline void set_U3CExpiresU3Ek__BackingField_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CExpiresU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CAgeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CAgeU3Ek__BackingField_6)); }
	inline int64_t get_U3CAgeU3Ek__BackingField_6() const { return ___U3CAgeU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CAgeU3Ek__BackingField_6() { return &___U3CAgeU3Ek__BackingField_6; }
	inline void set_U3CAgeU3Ek__BackingField_6(int64_t value)
	{
		___U3CAgeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CMaxAgeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CMaxAgeU3Ek__BackingField_7)); }
	inline int64_t get_U3CMaxAgeU3Ek__BackingField_7() const { return ___U3CMaxAgeU3Ek__BackingField_7; }
	inline int64_t* get_address_of_U3CMaxAgeU3Ek__BackingField_7() { return &___U3CMaxAgeU3Ek__BackingField_7; }
	inline void set_U3CMaxAgeU3Ek__BackingField_7(int64_t value)
	{
		___U3CMaxAgeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CDateU3Ek__BackingField_8)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CDateU3Ek__BackingField_8() const { return ___U3CDateU3Ek__BackingField_8; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CDateU3Ek__BackingField_8() { return &___U3CDateU3Ek__BackingField_8; }
	inline void set_U3CDateU3Ek__BackingField_8(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CDateU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMustRevalidateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CMustRevalidateU3Ek__BackingField_9)); }
	inline bool get_U3CMustRevalidateU3Ek__BackingField_9() const { return ___U3CMustRevalidateU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CMustRevalidateU3Ek__BackingField_9() { return &___U3CMustRevalidateU3Ek__BackingField_9; }
	inline void set_U3CMustRevalidateU3Ek__BackingField_9(bool value)
	{
		___U3CMustRevalidateU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CReceivedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CReceivedU3Ek__BackingField_10)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CReceivedU3Ek__BackingField_10() const { return ___U3CReceivedU3Ek__BackingField_10; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CReceivedU3Ek__BackingField_10() { return &___U3CReceivedU3Ek__BackingField_10; }
	inline void set_U3CReceivedU3Ek__BackingField_10(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CReceivedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CConstructedPathU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CConstructedPathU3Ek__BackingField_11)); }
	inline String_t* get_U3CConstructedPathU3Ek__BackingField_11() const { return ___U3CConstructedPathU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CConstructedPathU3Ek__BackingField_11() { return &___U3CConstructedPathU3Ek__BackingField_11; }
	inline void set_U3CConstructedPathU3Ek__BackingField_11(String_t* value)
	{
		___U3CConstructedPathU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConstructedPathU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CMappedNameIDXU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D, ___U3CMappedNameIDXU3Ek__BackingField_12)); }
	inline uint64_t get_U3CMappedNameIDXU3Ek__BackingField_12() const { return ___U3CMappedNameIDXU3Ek__BackingField_12; }
	inline uint64_t* get_address_of_U3CMappedNameIDXU3Ek__BackingField_12() { return &___U3CMappedNameIDXU3Ek__BackingField_12; }
	inline void set_U3CMappedNameIDXU3Ek__BackingField_12(uint64_t value)
	{
		___U3CMappedNameIDXU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCACHEFILEINFO_T47ADD76D6FCBE8C2640ADB6532577203ACC21A3D_H
#ifndef HTTPCACHESERVICE_TE27EEBB1F659D5314249E31CD5086F8713454461_H
#define HTTPCACHESERVICE_TE27EEBB1F659D5314249E31CD5086F8713454461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Caching.HTTPCacheService
struct  HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461  : public RuntimeObject
{
public:

public:
};

struct HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields
{
public:
	// System.Boolean BestHTTP.Caching.HTTPCacheService::isSupported
	bool ___isSupported_1;
	// System.Boolean BestHTTP.Caching.HTTPCacheService::IsSupportCheckDone
	bool ___IsSupportCheckDone_2;
	// System.Collections.Generic.Dictionary`2<System.Uri,BestHTTP.Caching.HTTPCacheFileInfo> BestHTTP.Caching.HTTPCacheService::library
	Dictionary_2_t3D7BB244513DA6111E20C925DF8D2CB2371B3D6C * ___library_3;
	// System.Threading.ReaderWriterLockSlim BestHTTP.Caching.HTTPCacheService::rwLock
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___rwLock_4;
	// System.Collections.Generic.Dictionary`2<System.UInt64,BestHTTP.Caching.HTTPCacheFileInfo> BestHTTP.Caching.HTTPCacheService::UsedIndexes
	Dictionary_2_t7B04DC9561E22E6F566C188535357C603C230965 * ___UsedIndexes_5;
	// System.String BestHTTP.Caching.HTTPCacheService::<CacheFolder>k__BackingField
	String_t* ___U3CCacheFolderU3Ek__BackingField_6;
	// System.String BestHTTP.Caching.HTTPCacheService::<LibraryPath>k__BackingField
	String_t* ___U3CLibraryPathU3Ek__BackingField_7;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.Caching.HTTPCacheService::InClearThread
	bool ___InClearThread_8;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.Caching.HTTPCacheService::InMaintainenceThread
	bool ___InMaintainenceThread_9;
	// System.UInt64 BestHTTP.Caching.HTTPCacheService::NextNameIDX
	uint64_t ___NextNameIDX_10;

public:
	inline static int32_t get_offset_of_isSupported_1() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___isSupported_1)); }
	inline bool get_isSupported_1() const { return ___isSupported_1; }
	inline bool* get_address_of_isSupported_1() { return &___isSupported_1; }
	inline void set_isSupported_1(bool value)
	{
		___isSupported_1 = value;
	}

	inline static int32_t get_offset_of_IsSupportCheckDone_2() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___IsSupportCheckDone_2)); }
	inline bool get_IsSupportCheckDone_2() const { return ___IsSupportCheckDone_2; }
	inline bool* get_address_of_IsSupportCheckDone_2() { return &___IsSupportCheckDone_2; }
	inline void set_IsSupportCheckDone_2(bool value)
	{
		___IsSupportCheckDone_2 = value;
	}

	inline static int32_t get_offset_of_library_3() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___library_3)); }
	inline Dictionary_2_t3D7BB244513DA6111E20C925DF8D2CB2371B3D6C * get_library_3() const { return ___library_3; }
	inline Dictionary_2_t3D7BB244513DA6111E20C925DF8D2CB2371B3D6C ** get_address_of_library_3() { return &___library_3; }
	inline void set_library_3(Dictionary_2_t3D7BB244513DA6111E20C925DF8D2CB2371B3D6C * value)
	{
		___library_3 = value;
		Il2CppCodeGenWriteBarrier((&___library_3), value);
	}

	inline static int32_t get_offset_of_rwLock_4() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___rwLock_4)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_rwLock_4() const { return ___rwLock_4; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_rwLock_4() { return &___rwLock_4; }
	inline void set_rwLock_4(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___rwLock_4 = value;
		Il2CppCodeGenWriteBarrier((&___rwLock_4), value);
	}

	inline static int32_t get_offset_of_UsedIndexes_5() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___UsedIndexes_5)); }
	inline Dictionary_2_t7B04DC9561E22E6F566C188535357C603C230965 * get_UsedIndexes_5() const { return ___UsedIndexes_5; }
	inline Dictionary_2_t7B04DC9561E22E6F566C188535357C603C230965 ** get_address_of_UsedIndexes_5() { return &___UsedIndexes_5; }
	inline void set_UsedIndexes_5(Dictionary_2_t7B04DC9561E22E6F566C188535357C603C230965 * value)
	{
		___UsedIndexes_5 = value;
		Il2CppCodeGenWriteBarrier((&___UsedIndexes_5), value);
	}

	inline static int32_t get_offset_of_U3CCacheFolderU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___U3CCacheFolderU3Ek__BackingField_6)); }
	inline String_t* get_U3CCacheFolderU3Ek__BackingField_6() const { return ___U3CCacheFolderU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CCacheFolderU3Ek__BackingField_6() { return &___U3CCacheFolderU3Ek__BackingField_6; }
	inline void set_U3CCacheFolderU3Ek__BackingField_6(String_t* value)
	{
		___U3CCacheFolderU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheFolderU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CLibraryPathU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___U3CLibraryPathU3Ek__BackingField_7)); }
	inline String_t* get_U3CLibraryPathU3Ek__BackingField_7() const { return ___U3CLibraryPathU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CLibraryPathU3Ek__BackingField_7() { return &___U3CLibraryPathU3Ek__BackingField_7; }
	inline void set_U3CLibraryPathU3Ek__BackingField_7(String_t* value)
	{
		___U3CLibraryPathU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLibraryPathU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_InClearThread_8() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___InClearThread_8)); }
	inline bool get_InClearThread_8() const { return ___InClearThread_8; }
	inline bool* get_address_of_InClearThread_8() { return &___InClearThread_8; }
	inline void set_InClearThread_8(bool value)
	{
		___InClearThread_8 = value;
	}

	inline static int32_t get_offset_of_InMaintainenceThread_9() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___InMaintainenceThread_9)); }
	inline bool get_InMaintainenceThread_9() const { return ___InMaintainenceThread_9; }
	inline bool* get_address_of_InMaintainenceThread_9() { return &___InMaintainenceThread_9; }
	inline void set_InMaintainenceThread_9(bool value)
	{
		___InMaintainenceThread_9 = value;
	}

	inline static int32_t get_offset_of_NextNameIDX_10() { return static_cast<int32_t>(offsetof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields, ___NextNameIDX_10)); }
	inline uint64_t get_NextNameIDX_10() const { return ___NextNameIDX_10; }
	inline uint64_t* get_address_of_NextNameIDX_10() { return &___NextNameIDX_10; }
	inline void set_NextNameIDX_10(uint64_t value)
	{
		___NextNameIDX_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCACHESERVICE_TE27EEBB1F659D5314249E31CD5086F8713454461_H
#ifndef HTTP2CONTINUATIONFLAGS_T888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14_H
#define HTTP2CONTINUATIONFLAGS_T888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2ContinuationFlags
struct  HTTP2ContinuationFlags_t888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2ContinuationFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2ContinuationFlags_t888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2CONTINUATIONFLAGS_T888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14_H
#ifndef HTTP2FRAMETYPES_T89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4_H
#define HTTP2FRAMETYPES_T89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2FrameTypes
struct  HTTP2FrameTypes_t89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2FrameTypes::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2FrameTypes_t89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2FRAMETYPES_T89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4_H
#ifndef HTTP2SETTINGS_TE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4_H
#define HTTP2SETTINGS_TE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2Settings
struct  HTTP2Settings_tE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4 
{
public:
	// System.UInt16 BestHTTP.Connections.HTTP2.HTTP2Settings::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2Settings_tE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2SETTINGS_TE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4_H
#ifndef HTTP2SETTINGSMANAGER_T44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_H
#define HTTP2SETTINGSMANAGER_T44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2SettingsManager
struct  HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1  : public RuntimeObject
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry BestHTTP.Connections.HTTP2.HTTP2SettingsManager::<MySettings>k__BackingField
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * ___U3CMySettingsU3Ek__BackingField_1;
	// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry BestHTTP.Connections.HTTP2.HTTP2SettingsManager::<InitiatedMySettings>k__BackingField
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * ___U3CInitiatedMySettingsU3Ek__BackingField_2;
	// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry BestHTTP.Connections.HTTP2.HTTP2SettingsManager::<RemoteSettings>k__BackingField
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * ___U3CRemoteSettingsU3Ek__BackingField_3;
	// System.DateTime BestHTTP.Connections.HTTP2.HTTP2SettingsManager::<SettingsChangesSentAt>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CSettingsChangesSentAtU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CMySettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1, ___U3CMySettingsU3Ek__BackingField_1)); }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * get_U3CMySettingsU3Ek__BackingField_1() const { return ___U3CMySettingsU3Ek__BackingField_1; }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 ** get_address_of_U3CMySettingsU3Ek__BackingField_1() { return &___U3CMySettingsU3Ek__BackingField_1; }
	inline void set_U3CMySettingsU3Ek__BackingField_1(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * value)
	{
		___U3CMySettingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMySettingsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CInitiatedMySettingsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1, ___U3CInitiatedMySettingsU3Ek__BackingField_2)); }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * get_U3CInitiatedMySettingsU3Ek__BackingField_2() const { return ___U3CInitiatedMySettingsU3Ek__BackingField_2; }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 ** get_address_of_U3CInitiatedMySettingsU3Ek__BackingField_2() { return &___U3CInitiatedMySettingsU3Ek__BackingField_2; }
	inline void set_U3CInitiatedMySettingsU3Ek__BackingField_2(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * value)
	{
		___U3CInitiatedMySettingsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInitiatedMySettingsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRemoteSettingsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1, ___U3CRemoteSettingsU3Ek__BackingField_3)); }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * get_U3CRemoteSettingsU3Ek__BackingField_3() const { return ___U3CRemoteSettingsU3Ek__BackingField_3; }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 ** get_address_of_U3CRemoteSettingsU3Ek__BackingField_3() { return &___U3CRemoteSettingsU3Ek__BackingField_3; }
	inline void set_U3CRemoteSettingsU3Ek__BackingField_3(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * value)
	{
		___U3CRemoteSettingsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRemoteSettingsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CSettingsChangesSentAtU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1, ___U3CSettingsChangesSentAtU3Ek__BackingField_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CSettingsChangesSentAtU3Ek__BackingField_4() const { return ___U3CSettingsChangesSentAtU3Ek__BackingField_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CSettingsChangesSentAtU3Ek__BackingField_4() { return &___U3CSettingsChangesSentAtU3Ek__BackingField_4; }
	inline void set_U3CSettingsChangesSentAtU3Ek__BackingField_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CSettingsChangesSentAtU3Ek__BackingField_4 = value;
	}
};

struct HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_StaticFields
{
public:
	// System.Int32 BestHTTP.Connections.HTTP2.HTTP2SettingsManager::SettingsCount
	int32_t ___SettingsCount_0;

public:
	inline static int32_t get_offset_of_SettingsCount_0() { return static_cast<int32_t>(offsetof(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_StaticFields, ___SettingsCount_0)); }
	inline int32_t get_SettingsCount_0() const { return ___SettingsCount_0; }
	inline int32_t* get_address_of_SettingsCount_0() { return &___SettingsCount_0; }
	inline void set_SettingsCount_0(int32_t value)
	{
		___SettingsCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2SETTINGSMANAGER_T44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_H
#ifndef HTTP2STREAMSTATES_T6D51897ED29F3396AE28175A80B69EFC95405CDE_H
#define HTTP2STREAMSTATES_T6D51897ED29F3396AE28175A80B69EFC95405CDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2StreamStates
struct  HTTP2StreamStates_t6D51897ED29F3396AE28175A80B69EFC95405CDE 
{
public:
	// System.Int32 BestHTTP.Connections.HTTP2.HTTP2StreamStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2StreamStates_t6D51897ED29F3396AE28175A80B69EFC95405CDE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2STREAMSTATES_T6D51897ED29F3396AE28175A80B69EFC95405CDE_H
#ifndef SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#define SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ShutdownTypes
struct  ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A 
{
public:
	// System.Int32 BestHTTP.ShutdownTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#ifndef CONNECTIONSTATES_TF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0_H
#define CONNECTIONSTATES_TF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.ConnectionStates
struct  ConnectionStates_tF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0 
{
public:
	// System.Int32 BestHTTP.SignalRCore.ConnectionStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionStates_tF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATES_TF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0_H
#ifndef TRANSFERMODES_T6A8657C1EAC4109096A1BB541E3F77C4A937CB34_H
#define TRANSFERMODES_T6A8657C1EAC4109096A1BB541E3F77C4A937CB34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.TransferModes
struct  TransferModes_t6A8657C1EAC4109096A1BB541E3F77C4A937CB34 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransferModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransferModes_t6A8657C1EAC4109096A1BB541E3F77C4A937CB34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFERMODES_T6A8657C1EAC4109096A1BB541E3F77C4A937CB34_H
#ifndef TRANSPORTEVENTS_TE721CE38C312660C7E23857550A5BDB598EB5A98_H
#define TRANSPORTEVENTS_TE721CE38C312660C7E23857550A5BDB598EB5A98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.TransportEvents
struct  TransportEvents_tE721CE38C312660C7E23857550A5BDB598EB5A98 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransportEvents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportEvents_tE721CE38C312660C7E23857550A5BDB598EB5A98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTEVENTS_TE721CE38C312660C7E23857550A5BDB598EB5A98_H
#ifndef TRANSPORTSTATES_TC5546B62EF1DD9E9220FEC265E5D626E6856A06E_H
#define TRANSPORTSTATES_TC5546B62EF1DD9E9220FEC265E5D626E6856A06E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.TransportStates
struct  TransportStates_tC5546B62EF1DD9E9220FEC265E5D626E6856A06E 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransportStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportStates_tC5546B62EF1DD9E9220FEC265E5D626E6856A06E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTSTATES_TC5546B62EF1DD9E9220FEC265E5D626E6856A06E_H
#ifndef TRANSPORTTYPES_TCA87C50DC625A9ED1115309688D187F9C628396B_H
#define TRANSPORTTYPES_TCA87C50DC625A9ED1115309688D187F9C628396B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.TransportTypes
struct  TransportTypes_tCA87C50DC625A9ED1115309688D187F9C628396B 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransportTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportTypes_tCA87C50DC625A9ED1115309688D187F9C628396B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTTYPES_TCA87C50DC625A9ED1115309688D187F9C628396B_H
#ifndef PAYLOADTYPES_T6536D7156E34641F8CF357C6087A035F341F4B68_H
#define PAYLOADTYPES_T6536D7156E34641F8CF357C6087A035F341F4B68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Packet_PayloadTypes
struct  PayloadTypes_t6536D7156E34641F8CF357C6087A035F341F4B68 
{
public:
	// System.Byte BestHTTP.SocketIO.Packet_PayloadTypes::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PayloadTypes_t6536D7156E34641F8CF357C6087A035F341F4B68, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADTYPES_T6536D7156E34641F8CF357C6087A035F341F4B68_H
#ifndef SOCKETIOERRORS_T3AAD8AC7186EBE750D529DA3FD23E77FE25E0579_H
#define SOCKETIOERRORS_T3AAD8AC7186EBE750D529DA3FD23E77FE25E0579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.SocketIOErrors
struct  SocketIOErrors_t3AAD8AC7186EBE750D529DA3FD23E77FE25E0579 
{
public:
	// System.Int32 BestHTTP.SocketIO.SocketIOErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketIOErrors_t3AAD8AC7186EBE750D529DA3FD23E77FE25E0579, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETIOERRORS_T3AAD8AC7186EBE750D529DA3FD23E77FE25E0579_H
#ifndef SOCKETIOEVENTTYPES_TAC846076D43C0805F1B05A636B02BADA2B1FE26A_H
#define SOCKETIOEVENTTYPES_TAC846076D43C0805F1B05A636B02BADA2B1FE26A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.SocketIOEventTypes
struct  SocketIOEventTypes_tAC846076D43C0805F1B05A636B02BADA2B1FE26A 
{
public:
	// System.Int32 BestHTTP.SocketIO.SocketIOEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SocketIOEventTypes_tAC846076D43C0805F1B05A636B02BADA2B1FE26A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETIOEVENTTYPES_TAC846076D43C0805F1B05A636B02BADA2B1FE26A_H
#ifndef STATES_T779BD738F612179D3FF3407A1A8B06D565BF5F23_H
#define STATES_T779BD738F612179D3FF3407A1A8B06D565BF5F23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.SocketManager_States
struct  States_t779BD738F612179D3FF3407A1A8B06D565BF5F23 
{
public:
	// System.Int32 BestHTTP.SocketIO.SocketManager_States::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(States_t779BD738F612179D3FF3407A1A8B06D565BF5F23, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATES_T779BD738F612179D3FF3407A1A8B06D565BF5F23_H
#ifndef TRANSPORTEVENTTYPES_T1BF9D523FF224030531EBC5C8A43879ACC28642F_H
#define TRANSPORTEVENTTYPES_T1BF9D523FF224030531EBC5C8A43879ACC28642F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.TransportEventTypes
struct  TransportEventTypes_t1BF9D523FF224030531EBC5C8A43879ACC28642F 
{
public:
	// System.Int32 BestHTTP.SocketIO.TransportEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportEventTypes_t1BF9D523FF224030531EBC5C8A43879ACC28642F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTEVENTTYPES_T1BF9D523FF224030531EBC5C8A43879ACC28642F_H
#ifndef PAYLOADTYPES_T74B79C4FC24732633B7D82C90FFD62E5052F6D9A_H
#define PAYLOADTYPES_T74B79C4FC24732633B7D82C90FFD62E5052F6D9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Transports.PollingTransport_PayloadTypes
struct  PayloadTypes_t74B79C4FC24732633B7D82C90FFD62E5052F6D9A 
{
public:
	// System.Byte BestHTTP.SocketIO.Transports.PollingTransport_PayloadTypes::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PayloadTypes_t74B79C4FC24732633B7D82C90FFD62E5052F6D9A, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADTYPES_T74B79C4FC24732633B7D82C90FFD62E5052F6D9A_H
#ifndef TRANSPORTSTATES_TF990329133AB20D4128C755417FF2AD931C5DD56_H
#define TRANSPORTSTATES_TF990329133AB20D4128C755417FF2AD931C5DD56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Transports.TransportStates
struct  TransportStates_tF990329133AB20D4128C755417FF2AD931C5DD56 
{
public:
	// System.Int32 BestHTTP.SocketIO.Transports.TransportStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportStates_tF990329133AB20D4128C755417FF2AD931C5DD56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTSTATES_TF990329133AB20D4128C755417FF2AD931C5DD56_H
#ifndef TRANSPORTTYPES_T396DD398362609B9A346607C3E06C532A2B9F100_H
#define TRANSPORTTYPES_T396DD398362609B9A346607C3E06C532A2B9F100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Transports.TransportTypes
struct  TransportTypes_t396DD398362609B9A346607C3E06C532A2B9F100 
{
public:
	// System.Int32 BestHTTP.SocketIO.Transports.TransportTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportTypes_t396DD398362609B9A346607C3E06C532A2B9F100, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTTYPES_T396DD398362609B9A346607C3E06C532A2B9F100_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef CREDENTIALS_T3D6400EA93AC11D8F88F75B6135864C46CD8BB78_H
#define CREDENTIALS_T3D6400EA93AC11D8F88F75B6135864C46CD8BB78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Authentication.Credentials
struct  Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78  : public RuntimeObject
{
public:
	// BestHTTP.Authentication.AuthenticationTypes BestHTTP.Authentication.Credentials::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_0;
	// System.String BestHTTP.Authentication.Credentials::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_1;
	// System.String BestHTTP.Authentication.Credentials::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78, ___U3CTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CUserNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78, ___U3CUserNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CUserNameU3Ek__BackingField_1() const { return ___U3CUserNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUserNameU3Ek__BackingField_1() { return &___U3CUserNameU3Ek__BackingField_1; }
	inline void set_U3CUserNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CUserNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78, ___U3CPasswordU3Ek__BackingField_2)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_2() const { return ___U3CPasswordU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_2() { return &___U3CPasswordU3Ek__BackingField_2; }
	inline void set_U3CPasswordU3Ek__BackingField_2(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPasswordU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALS_T3D6400EA93AC11D8F88F75B6135864C46CD8BB78_H
#ifndef DIGEST_TA5E41B88A3B435AC408A918248D6176185A38A6D_H
#define DIGEST_TA5E41B88A3B435AC408A918248D6176185A38A6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Authentication.Digest
struct  Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.Authentication.Digest::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_0;
	// BestHTTP.Authentication.AuthenticationTypes BestHTTP.Authentication.Digest::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
	// System.String BestHTTP.Authentication.Digest::<Realm>k__BackingField
	String_t* ___U3CRealmU3Ek__BackingField_2;
	// System.Boolean BestHTTP.Authentication.Digest::<Stale>k__BackingField
	bool ___U3CStaleU3Ek__BackingField_3;
	// System.String BestHTTP.Authentication.Digest::<Nonce>k__BackingField
	String_t* ___U3CNonceU3Ek__BackingField_4;
	// System.String BestHTTP.Authentication.Digest::<Opaque>k__BackingField
	String_t* ___U3COpaqueU3Ek__BackingField_5;
	// System.String BestHTTP.Authentication.Digest::<Algorithm>k__BackingField
	String_t* ___U3CAlgorithmU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<System.String> BestHTTP.Authentication.Digest::<ProtectedUris>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CProtectedUrisU3Ek__BackingField_7;
	// System.String BestHTTP.Authentication.Digest::<QualityOfProtections>k__BackingField
	String_t* ___U3CQualityOfProtectionsU3Ek__BackingField_8;
	// System.Int32 BestHTTP.Authentication.Digest::<NonceCount>k__BackingField
	int32_t ___U3CNonceCountU3Ek__BackingField_9;
	// System.String BestHTTP.Authentication.Digest::<HA1Sess>k__BackingField
	String_t* ___U3CHA1SessU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CUriU3Ek__BackingField_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_0() const { return ___U3CUriU3Ek__BackingField_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_0() { return &___U3CUriU3Ek__BackingField_0; }
	inline void set_U3CUriU3Ek__BackingField_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRealmU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CRealmU3Ek__BackingField_2)); }
	inline String_t* get_U3CRealmU3Ek__BackingField_2() const { return ___U3CRealmU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRealmU3Ek__BackingField_2() { return &___U3CRealmU3Ek__BackingField_2; }
	inline void set_U3CRealmU3Ek__BackingField_2(String_t* value)
	{
		___U3CRealmU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRealmU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CStaleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CStaleU3Ek__BackingField_3)); }
	inline bool get_U3CStaleU3Ek__BackingField_3() const { return ___U3CStaleU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CStaleU3Ek__BackingField_3() { return &___U3CStaleU3Ek__BackingField_3; }
	inline void set_U3CStaleU3Ek__BackingField_3(bool value)
	{
		___U3CStaleU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CNonceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CNonceU3Ek__BackingField_4)); }
	inline String_t* get_U3CNonceU3Ek__BackingField_4() const { return ___U3CNonceU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CNonceU3Ek__BackingField_4() { return &___U3CNonceU3Ek__BackingField_4; }
	inline void set_U3CNonceU3Ek__BackingField_4(String_t* value)
	{
		___U3CNonceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNonceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3COpaqueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3COpaqueU3Ek__BackingField_5)); }
	inline String_t* get_U3COpaqueU3Ek__BackingField_5() const { return ___U3COpaqueU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3COpaqueU3Ek__BackingField_5() { return &___U3COpaqueU3Ek__BackingField_5; }
	inline void set_U3COpaqueU3Ek__BackingField_5(String_t* value)
	{
		___U3COpaqueU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3COpaqueU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CAlgorithmU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CAlgorithmU3Ek__BackingField_6)); }
	inline String_t* get_U3CAlgorithmU3Ek__BackingField_6() const { return ___U3CAlgorithmU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CAlgorithmU3Ek__BackingField_6() { return &___U3CAlgorithmU3Ek__BackingField_6; }
	inline void set_U3CAlgorithmU3Ek__BackingField_6(String_t* value)
	{
		___U3CAlgorithmU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAlgorithmU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CProtectedUrisU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CProtectedUrisU3Ek__BackingField_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CProtectedUrisU3Ek__BackingField_7() const { return ___U3CProtectedUrisU3Ek__BackingField_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CProtectedUrisU3Ek__BackingField_7() { return &___U3CProtectedUrisU3Ek__BackingField_7; }
	inline void set_U3CProtectedUrisU3Ek__BackingField_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CProtectedUrisU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtectedUrisU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CQualityOfProtectionsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CQualityOfProtectionsU3Ek__BackingField_8)); }
	inline String_t* get_U3CQualityOfProtectionsU3Ek__BackingField_8() const { return ___U3CQualityOfProtectionsU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CQualityOfProtectionsU3Ek__BackingField_8() { return &___U3CQualityOfProtectionsU3Ek__BackingField_8; }
	inline void set_U3CQualityOfProtectionsU3Ek__BackingField_8(String_t* value)
	{
		___U3CQualityOfProtectionsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQualityOfProtectionsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CNonceCountU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CNonceCountU3Ek__BackingField_9)); }
	inline int32_t get_U3CNonceCountU3Ek__BackingField_9() const { return ___U3CNonceCountU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CNonceCountU3Ek__BackingField_9() { return &___U3CNonceCountU3Ek__BackingField_9; }
	inline void set_U3CNonceCountU3Ek__BackingField_9(int32_t value)
	{
		___U3CNonceCountU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CHA1SessU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D, ___U3CHA1SessU3Ek__BackingField_10)); }
	inline String_t* get_U3CHA1SessU3Ek__BackingField_10() const { return ___U3CHA1SessU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CHA1SessU3Ek__BackingField_10() { return &___U3CHA1SessU3Ek__BackingField_10; }
	inline void set_U3CHA1SessU3Ek__BackingField_10(String_t* value)
	{
		___U3CHA1SessU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHA1SessU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGEST_TA5E41B88A3B435AC408A918248D6176185A38A6D_H
#ifndef HTTPCACHEMAINTANANCEPARAMS_T8B2271A5ACE741730682A51E9A9A4091DCF1A047_H
#define HTTPCACHEMAINTANANCEPARAMS_T8B2271A5ACE741730682A51E9A9A4091DCF1A047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Caching.HTTPCacheMaintananceParams
struct  HTTPCacheMaintananceParams_t8B2271A5ACE741730682A51E9A9A4091DCF1A047  : public RuntimeObject
{
public:
	// System.TimeSpan BestHTTP.Caching.HTTPCacheMaintananceParams::<DeleteOlder>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CDeleteOlderU3Ek__BackingField_0;
	// System.UInt64 BestHTTP.Caching.HTTPCacheMaintananceParams::<MaxCacheSize>k__BackingField
	uint64_t ___U3CMaxCacheSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDeleteOlderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HTTPCacheMaintananceParams_t8B2271A5ACE741730682A51E9A9A4091DCF1A047, ___U3CDeleteOlderU3Ek__BackingField_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CDeleteOlderU3Ek__BackingField_0() const { return ___U3CDeleteOlderU3Ek__BackingField_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CDeleteOlderU3Ek__BackingField_0() { return &___U3CDeleteOlderU3Ek__BackingField_0; }
	inline void set_U3CDeleteOlderU3Ek__BackingField_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CDeleteOlderU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxCacheSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTPCacheMaintananceParams_t8B2271A5ACE741730682A51E9A9A4091DCF1A047, ___U3CMaxCacheSizeU3Ek__BackingField_1)); }
	inline uint64_t get_U3CMaxCacheSizeU3Ek__BackingField_1() const { return ___U3CMaxCacheSizeU3Ek__BackingField_1; }
	inline uint64_t* get_address_of_U3CMaxCacheSizeU3Ek__BackingField_1() { return &___U3CMaxCacheSizeU3Ek__BackingField_1; }
	inline void set_U3CMaxCacheSizeU3Ek__BackingField_1(uint64_t value)
	{
		___U3CMaxCacheSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCACHEMAINTANANCEPARAMS_T8B2271A5ACE741730682A51E9A9A4091DCF1A047_H
#ifndef HTTP2FRAMEHEADERANDPAYLOAD_T00C90549534F75E3FCB383BA13C3E6A3A47803B6_H
#define HTTP2FRAMEHEADERANDPAYLOAD_T00C90549534F75E3FCB383BA13C3E6A3A47803B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload
struct  HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 
{
public:
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::PayloadLength
	uint32_t ___PayloadLength_0;
	// BestHTTP.Connections.HTTP2.HTTP2FrameTypes BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::Type
	uint8_t ___Type_1;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::Flags
	uint8_t ___Flags_2;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::StreamId
	uint32_t ___StreamId_3;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::Payload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Payload_4;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::PayloadOffset
	uint32_t ___PayloadOffset_5;
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload::DontUseMemPool
	bool ___DontUseMemPool_6;

public:
	inline static int32_t get_offset_of_PayloadLength_0() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___PayloadLength_0)); }
	inline uint32_t get_PayloadLength_0() const { return ___PayloadLength_0; }
	inline uint32_t* get_address_of_PayloadLength_0() { return &___PayloadLength_0; }
	inline void set_PayloadLength_0(uint32_t value)
	{
		___PayloadLength_0 = value;
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___Type_1)); }
	inline uint8_t get_Type_1() const { return ___Type_1; }
	inline uint8_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(uint8_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Flags_2() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___Flags_2)); }
	inline uint8_t get_Flags_2() const { return ___Flags_2; }
	inline uint8_t* get_address_of_Flags_2() { return &___Flags_2; }
	inline void set_Flags_2(uint8_t value)
	{
		___Flags_2 = value;
	}

	inline static int32_t get_offset_of_StreamId_3() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___StreamId_3)); }
	inline uint32_t get_StreamId_3() const { return ___StreamId_3; }
	inline uint32_t* get_address_of_StreamId_3() { return &___StreamId_3; }
	inline void set_StreamId_3(uint32_t value)
	{
		___StreamId_3 = value;
	}

	inline static int32_t get_offset_of_Payload_4() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___Payload_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Payload_4() const { return ___Payload_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Payload_4() { return &___Payload_4; }
	inline void set_Payload_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Payload_4 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_4), value);
	}

	inline static int32_t get_offset_of_PayloadOffset_5() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___PayloadOffset_5)); }
	inline uint32_t get_PayloadOffset_5() const { return ___PayloadOffset_5; }
	inline uint32_t* get_address_of_PayloadOffset_5() { return &___PayloadOffset_5; }
	inline void set_PayloadOffset_5(uint32_t value)
	{
		___PayloadOffset_5 = value;
	}

	inline static int32_t get_offset_of_DontUseMemPool_6() { return static_cast<int32_t>(offsetof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6, ___DontUseMemPool_6)); }
	inline bool get_DontUseMemPool_6() const { return ___DontUseMemPool_6; }
	inline bool* get_address_of_DontUseMemPool_6() { return &___DontUseMemPool_6; }
	inline void set_DontUseMemPool_6(bool value)
	{
		___DontUseMemPool_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload
struct HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke
{
	uint32_t ___PayloadLength_0;
	uint8_t ___Type_1;
	uint8_t ___Flags_2;
	uint32_t ___StreamId_3;
	uint8_t* ___Payload_4;
	uint32_t ___PayloadOffset_5;
	int32_t ___DontUseMemPool_6;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload
struct HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com
{
	uint32_t ___PayloadLength_0;
	uint8_t ___Type_1;
	uint8_t ___Flags_2;
	uint32_t ___StreamId_3;
	uint8_t* ___Payload_4;
	uint32_t ___PayloadOffset_5;
	int32_t ___DontUseMemPool_6;
};
#endif // HTTP2FRAMEHEADERANDPAYLOAD_T00C90549534F75E3FCB383BA13C3E6A3A47803B6_H
#ifndef HTTP2HANDLER_T687724372BD4C276BA8177D6838340726B708846_H
#define HTTP2HANDLER_T687724372BD4C276BA8177D6838340726B708846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2Handler
struct  HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846  : public RuntimeObject
{
public:
	// System.Double BestHTTP.Connections.HTTP2.HTTP2Handler::<Latency>k__BackingField
	double ___U3CLatencyU3Ek__BackingField_2;
	// System.DateTime BestHTTP.Connections.HTTP2.HTTP2Handler::lastPingSent
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastPingSent_3;
	// System.TimeSpan BestHTTP.Connections.HTTP2.HTTP2Handler::pingFrequency
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___pingFrequency_4;
	// BestHTTP.Extensions.CircularBuffer`1<System.Double> BestHTTP.Connections.HTTP2.HTTP2Handler::rtts
	CircularBuffer_1_t79717186CCBB087CB40F3596A551A5A6F3858AF4 * ___rtts_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.Connections.HTTP2.HTTP2Handler::isThreadsStarted
	bool ___isThreadsStarted_7;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.Connections.HTTP2.HTTP2Handler::isRunning
	bool ___isRunning_8;
	// System.Threading.AutoResetEvent BestHTTP.Connections.HTTP2.HTTP2Handler::newFrameSignal
	AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * ___newFrameSignal_9;
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.HTTPRequest> BestHTTP.Connections.HTTP2.HTTP2Handler::requestQueue
	ConcurrentQueue_1_t1E22D69C92FE4BDD0DC96212335470CB30633235 * ___requestQueue_10;
	// System.Collections.Generic.List`1<BestHTTP.Connections.HTTP2.HTTP2Stream> BestHTTP.Connections.HTTP2.HTTP2Handler::clientInitiatedStreams
	List_1_t4DF90480877FA6ACD8927F4CBE481A0818FEDA06 * ___clientInitiatedStreams_11;
	// BestHTTP.Connections.HTTP2.HPACKEncoder BestHTTP.Connections.HTTP2.HTTP2Handler::HPACKEncoder
	HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * ___HPACKEncoder_12;
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload> BestHTTP.Connections.HTTP2.HTTP2Handler::newFrames
	ConcurrentQueue_1_tE59B4F2A24E20EB19A99339D509125D17614447F * ___newFrames_13;
	// System.Collections.Generic.List`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload> BestHTTP.Connections.HTTP2.HTTP2Handler::outgoingFrames
	List_1_t81EF1D41B45FE7683A924495B47003A131DED228 * ___outgoingFrames_14;
	// BestHTTP.Connections.HTTP2.HTTP2SettingsManager BestHTTP.Connections.HTTP2.HTTP2Handler::settings
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * ___settings_15;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Handler::remoteWindow
	uint32_t ___remoteWindow_16;
	// System.DateTime BestHTTP.Connections.HTTP2.HTTP2Handler::lastInteraction
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastInteraction_17;
	// System.DateTime BestHTTP.Connections.HTTP2.HTTP2Handler::goAwaySentAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___goAwaySentAt_18;
	// BestHTTP.Connections.HTTPConnection BestHTTP.Connections.HTTP2.HTTP2Handler::conn
	HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 * ___conn_19;
	// System.Int32 BestHTTP.Connections.HTTP2.HTTP2Handler::threadExitCount
	int32_t ___threadExitCount_20;
	// BestHTTP.ShutdownTypes BestHTTP.Connections.HTTP2.HTTP2Handler::<ShutdownType>k__BackingField
	int32_t ___U3CShutdownTypeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CLatencyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___U3CLatencyU3Ek__BackingField_2)); }
	inline double get_U3CLatencyU3Ek__BackingField_2() const { return ___U3CLatencyU3Ek__BackingField_2; }
	inline double* get_address_of_U3CLatencyU3Ek__BackingField_2() { return &___U3CLatencyU3Ek__BackingField_2; }
	inline void set_U3CLatencyU3Ek__BackingField_2(double value)
	{
		___U3CLatencyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_lastPingSent_3() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___lastPingSent_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastPingSent_3() const { return ___lastPingSent_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastPingSent_3() { return &___lastPingSent_3; }
	inline void set_lastPingSent_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastPingSent_3 = value;
	}

	inline static int32_t get_offset_of_pingFrequency_4() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___pingFrequency_4)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_pingFrequency_4() const { return ___pingFrequency_4; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_pingFrequency_4() { return &___pingFrequency_4; }
	inline void set_pingFrequency_4(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___pingFrequency_4 = value;
	}

	inline static int32_t get_offset_of_rtts_6() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___rtts_6)); }
	inline CircularBuffer_1_t79717186CCBB087CB40F3596A551A5A6F3858AF4 * get_rtts_6() const { return ___rtts_6; }
	inline CircularBuffer_1_t79717186CCBB087CB40F3596A551A5A6F3858AF4 ** get_address_of_rtts_6() { return &___rtts_6; }
	inline void set_rtts_6(CircularBuffer_1_t79717186CCBB087CB40F3596A551A5A6F3858AF4 * value)
	{
		___rtts_6 = value;
		Il2CppCodeGenWriteBarrier((&___rtts_6), value);
	}

	inline static int32_t get_offset_of_isThreadsStarted_7() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___isThreadsStarted_7)); }
	inline bool get_isThreadsStarted_7() const { return ___isThreadsStarted_7; }
	inline bool* get_address_of_isThreadsStarted_7() { return &___isThreadsStarted_7; }
	inline void set_isThreadsStarted_7(bool value)
	{
		___isThreadsStarted_7 = value;
	}

	inline static int32_t get_offset_of_isRunning_8() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___isRunning_8)); }
	inline bool get_isRunning_8() const { return ___isRunning_8; }
	inline bool* get_address_of_isRunning_8() { return &___isRunning_8; }
	inline void set_isRunning_8(bool value)
	{
		___isRunning_8 = value;
	}

	inline static int32_t get_offset_of_newFrameSignal_9() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___newFrameSignal_9)); }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * get_newFrameSignal_9() const { return ___newFrameSignal_9; }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 ** get_address_of_newFrameSignal_9() { return &___newFrameSignal_9; }
	inline void set_newFrameSignal_9(AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * value)
	{
		___newFrameSignal_9 = value;
		Il2CppCodeGenWriteBarrier((&___newFrameSignal_9), value);
	}

	inline static int32_t get_offset_of_requestQueue_10() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___requestQueue_10)); }
	inline ConcurrentQueue_1_t1E22D69C92FE4BDD0DC96212335470CB30633235 * get_requestQueue_10() const { return ___requestQueue_10; }
	inline ConcurrentQueue_1_t1E22D69C92FE4BDD0DC96212335470CB30633235 ** get_address_of_requestQueue_10() { return &___requestQueue_10; }
	inline void set_requestQueue_10(ConcurrentQueue_1_t1E22D69C92FE4BDD0DC96212335470CB30633235 * value)
	{
		___requestQueue_10 = value;
		Il2CppCodeGenWriteBarrier((&___requestQueue_10), value);
	}

	inline static int32_t get_offset_of_clientInitiatedStreams_11() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___clientInitiatedStreams_11)); }
	inline List_1_t4DF90480877FA6ACD8927F4CBE481A0818FEDA06 * get_clientInitiatedStreams_11() const { return ___clientInitiatedStreams_11; }
	inline List_1_t4DF90480877FA6ACD8927F4CBE481A0818FEDA06 ** get_address_of_clientInitiatedStreams_11() { return &___clientInitiatedStreams_11; }
	inline void set_clientInitiatedStreams_11(List_1_t4DF90480877FA6ACD8927F4CBE481A0818FEDA06 * value)
	{
		___clientInitiatedStreams_11 = value;
		Il2CppCodeGenWriteBarrier((&___clientInitiatedStreams_11), value);
	}

	inline static int32_t get_offset_of_HPACKEncoder_12() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___HPACKEncoder_12)); }
	inline HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * get_HPACKEncoder_12() const { return ___HPACKEncoder_12; }
	inline HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 ** get_address_of_HPACKEncoder_12() { return &___HPACKEncoder_12; }
	inline void set_HPACKEncoder_12(HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * value)
	{
		___HPACKEncoder_12 = value;
		Il2CppCodeGenWriteBarrier((&___HPACKEncoder_12), value);
	}

	inline static int32_t get_offset_of_newFrames_13() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___newFrames_13)); }
	inline ConcurrentQueue_1_tE59B4F2A24E20EB19A99339D509125D17614447F * get_newFrames_13() const { return ___newFrames_13; }
	inline ConcurrentQueue_1_tE59B4F2A24E20EB19A99339D509125D17614447F ** get_address_of_newFrames_13() { return &___newFrames_13; }
	inline void set_newFrames_13(ConcurrentQueue_1_tE59B4F2A24E20EB19A99339D509125D17614447F * value)
	{
		___newFrames_13 = value;
		Il2CppCodeGenWriteBarrier((&___newFrames_13), value);
	}

	inline static int32_t get_offset_of_outgoingFrames_14() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___outgoingFrames_14)); }
	inline List_1_t81EF1D41B45FE7683A924495B47003A131DED228 * get_outgoingFrames_14() const { return ___outgoingFrames_14; }
	inline List_1_t81EF1D41B45FE7683A924495B47003A131DED228 ** get_address_of_outgoingFrames_14() { return &___outgoingFrames_14; }
	inline void set_outgoingFrames_14(List_1_t81EF1D41B45FE7683A924495B47003A131DED228 * value)
	{
		___outgoingFrames_14 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingFrames_14), value);
	}

	inline static int32_t get_offset_of_settings_15() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___settings_15)); }
	inline HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * get_settings_15() const { return ___settings_15; }
	inline HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 ** get_address_of_settings_15() { return &___settings_15; }
	inline void set_settings_15(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * value)
	{
		___settings_15 = value;
		Il2CppCodeGenWriteBarrier((&___settings_15), value);
	}

	inline static int32_t get_offset_of_remoteWindow_16() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___remoteWindow_16)); }
	inline uint32_t get_remoteWindow_16() const { return ___remoteWindow_16; }
	inline uint32_t* get_address_of_remoteWindow_16() { return &___remoteWindow_16; }
	inline void set_remoteWindow_16(uint32_t value)
	{
		___remoteWindow_16 = value;
	}

	inline static int32_t get_offset_of_lastInteraction_17() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___lastInteraction_17)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastInteraction_17() const { return ___lastInteraction_17; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastInteraction_17() { return &___lastInteraction_17; }
	inline void set_lastInteraction_17(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastInteraction_17 = value;
	}

	inline static int32_t get_offset_of_goAwaySentAt_18() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___goAwaySentAt_18)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_goAwaySentAt_18() const { return ___goAwaySentAt_18; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_goAwaySentAt_18() { return &___goAwaySentAt_18; }
	inline void set_goAwaySentAt_18(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___goAwaySentAt_18 = value;
	}

	inline static int32_t get_offset_of_conn_19() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___conn_19)); }
	inline HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 * get_conn_19() const { return ___conn_19; }
	inline HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 ** get_address_of_conn_19() { return &___conn_19; }
	inline void set_conn_19(HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 * value)
	{
		___conn_19 = value;
		Il2CppCodeGenWriteBarrier((&___conn_19), value);
	}

	inline static int32_t get_offset_of_threadExitCount_20() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___threadExitCount_20)); }
	inline int32_t get_threadExitCount_20() const { return ___threadExitCount_20; }
	inline int32_t* get_address_of_threadExitCount_20() { return &___threadExitCount_20; }
	inline void set_threadExitCount_20(int32_t value)
	{
		___threadExitCount_20 = value;
	}

	inline static int32_t get_offset_of_U3CShutdownTypeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846, ___U3CShutdownTypeU3Ek__BackingField_21)); }
	inline int32_t get_U3CShutdownTypeU3Ek__BackingField_21() const { return ___U3CShutdownTypeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CShutdownTypeU3Ek__BackingField_21() { return &___U3CShutdownTypeU3Ek__BackingField_21; }
	inline void set_U3CShutdownTypeU3Ek__BackingField_21(int32_t value)
	{
		___U3CShutdownTypeU3Ek__BackingField_21 = value;
	}
};

struct HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846_StaticFields
{
public:
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2Handler::MAGIC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___MAGIC_0;
	// System.Int32 BestHTTP.Connections.HTTP2.HTTP2Handler::RTTBufferCapacity
	int32_t ___RTTBufferCapacity_5;

public:
	inline static int32_t get_offset_of_MAGIC_0() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846_StaticFields, ___MAGIC_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_MAGIC_0() const { return ___MAGIC_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_MAGIC_0() { return &___MAGIC_0; }
	inline void set_MAGIC_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___MAGIC_0 = value;
		Il2CppCodeGenWriteBarrier((&___MAGIC_0), value);
	}

	inline static int32_t get_offset_of_RTTBufferCapacity_5() { return static_cast<int32_t>(offsetof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846_StaticFields, ___RTTBufferCapacity_5)); }
	inline int32_t get_RTTBufferCapacity_5() const { return ___RTTBufferCapacity_5; }
	inline int32_t* get_address_of_RTTBufferCapacity_5() { return &___RTTBufferCapacity_5; }
	inline void set_RTTBufferCapacity_5(int32_t value)
	{
		___RTTBufferCapacity_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2HANDLER_T687724372BD4C276BA8177D6838340726B708846_H
#ifndef HTTP2PLUGINSETTINGS_T6E2972409DBFEF2F6CE748A1A9F120A52498EE63_H
#define HTTP2PLUGINSETTINGS_T6E2972409DBFEF2F6CE748A1A9F120A52498EE63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2PluginSettings
struct  HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63  : public RuntimeObject
{
public:
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PluginSettings::HeaderTableSize
	uint32_t ___HeaderTableSize_0;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PluginSettings::MaxConcurrentStreams
	uint32_t ___MaxConcurrentStreams_1;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PluginSettings::InitialStreamWindowSize
	uint32_t ___InitialStreamWindowSize_2;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PluginSettings::InitialConnectionWindowSize
	uint32_t ___InitialConnectionWindowSize_3;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PluginSettings::MaxFrameSize
	uint32_t ___MaxFrameSize_4;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PluginSettings::MaxHeaderListSize
	uint32_t ___MaxHeaderListSize_5;
	// System.TimeSpan BestHTTP.Connections.HTTP2.HTTP2PluginSettings::MaxIdleTime
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxIdleTime_6;
	// System.TimeSpan BestHTTP.Connections.HTTP2.HTTP2PluginSettings::PingFrequency
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___PingFrequency_7;

public:
	inline static int32_t get_offset_of_HeaderTableSize_0() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___HeaderTableSize_0)); }
	inline uint32_t get_HeaderTableSize_0() const { return ___HeaderTableSize_0; }
	inline uint32_t* get_address_of_HeaderTableSize_0() { return &___HeaderTableSize_0; }
	inline void set_HeaderTableSize_0(uint32_t value)
	{
		___HeaderTableSize_0 = value;
	}

	inline static int32_t get_offset_of_MaxConcurrentStreams_1() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___MaxConcurrentStreams_1)); }
	inline uint32_t get_MaxConcurrentStreams_1() const { return ___MaxConcurrentStreams_1; }
	inline uint32_t* get_address_of_MaxConcurrentStreams_1() { return &___MaxConcurrentStreams_1; }
	inline void set_MaxConcurrentStreams_1(uint32_t value)
	{
		___MaxConcurrentStreams_1 = value;
	}

	inline static int32_t get_offset_of_InitialStreamWindowSize_2() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___InitialStreamWindowSize_2)); }
	inline uint32_t get_InitialStreamWindowSize_2() const { return ___InitialStreamWindowSize_2; }
	inline uint32_t* get_address_of_InitialStreamWindowSize_2() { return &___InitialStreamWindowSize_2; }
	inline void set_InitialStreamWindowSize_2(uint32_t value)
	{
		___InitialStreamWindowSize_2 = value;
	}

	inline static int32_t get_offset_of_InitialConnectionWindowSize_3() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___InitialConnectionWindowSize_3)); }
	inline uint32_t get_InitialConnectionWindowSize_3() const { return ___InitialConnectionWindowSize_3; }
	inline uint32_t* get_address_of_InitialConnectionWindowSize_3() { return &___InitialConnectionWindowSize_3; }
	inline void set_InitialConnectionWindowSize_3(uint32_t value)
	{
		___InitialConnectionWindowSize_3 = value;
	}

	inline static int32_t get_offset_of_MaxFrameSize_4() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___MaxFrameSize_4)); }
	inline uint32_t get_MaxFrameSize_4() const { return ___MaxFrameSize_4; }
	inline uint32_t* get_address_of_MaxFrameSize_4() { return &___MaxFrameSize_4; }
	inline void set_MaxFrameSize_4(uint32_t value)
	{
		___MaxFrameSize_4 = value;
	}

	inline static int32_t get_offset_of_MaxHeaderListSize_5() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___MaxHeaderListSize_5)); }
	inline uint32_t get_MaxHeaderListSize_5() const { return ___MaxHeaderListSize_5; }
	inline uint32_t* get_address_of_MaxHeaderListSize_5() { return &___MaxHeaderListSize_5; }
	inline void set_MaxHeaderListSize_5(uint32_t value)
	{
		___MaxHeaderListSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxIdleTime_6() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___MaxIdleTime_6)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxIdleTime_6() const { return ___MaxIdleTime_6; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxIdleTime_6() { return &___MaxIdleTime_6; }
	inline void set_MaxIdleTime_6(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxIdleTime_6 = value;
	}

	inline static int32_t get_offset_of_PingFrequency_7() { return static_cast<int32_t>(offsetof(HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63, ___PingFrequency_7)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_PingFrequency_7() const { return ___PingFrequency_7; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_PingFrequency_7() { return &___PingFrequency_7; }
	inline void set_PingFrequency_7(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___PingFrequency_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2PLUGINSETTINGS_T6E2972409DBFEF2F6CE748A1A9F120A52498EE63_H
#ifndef HTTP2STREAM_T38D20F5421551C9D095E63974F5AE1B25E301BEF_H
#define HTTP2STREAM_T38D20F5421551C9D095E63974F5AE1B25E301BEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2Stream
struct  HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF  : public RuntimeObject
{
public:
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Stream::<Id>k__BackingField
	uint32_t ___U3CIdU3Ek__BackingField_1;
	// BestHTTP.Connections.HTTP2.HTTP2StreamStates BestHTTP.Connections.HTTP2.HTTP2Stream::_state
	int32_t ____state_2;
	// BestHTTP.HTTPRequest BestHTTP.Connections.HTTP2.HTTP2Stream::<AssignedRequest>k__BackingField
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___U3CAssignedRequestU3Ek__BackingField_3;
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2Stream::isStreamedDownload
	bool ___isStreamedDownload_4;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Stream::downloaded
	uint32_t ___downloaded_5;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2Stream::bodyToSend
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bodyToSend_6;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Stream::bodyToSendOffset
	uint32_t ___bodyToSendOffset_7;
	// BestHTTP.Connections.HTTP2.HTTP2SettingsManager BestHTTP.Connections.HTTP2.HTTP2Stream::settings
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * ___settings_8;
	// BestHTTP.Connections.HTTP2.HPACKEncoder BestHTTP.Connections.HTTP2.HTTP2Stream::encoder
	HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * ___encoder_9;
	// System.Collections.Generic.Queue`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload> BestHTTP.Connections.HTTP2.HTTP2Stream::outgoing
	Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 * ___outgoing_10;
	// System.Collections.Generic.Queue`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload> BestHTTP.Connections.HTTP2.HTTP2Stream::incomingFrames
	Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 * ___incomingFrames_11;
	// BestHTTP.Connections.HTTP2.FramesAsStreamView BestHTTP.Connections.HTTP2.HTTP2Stream::currentFramesView
	FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957 * ___currentFramesView_12;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Stream::localWindow
	uint32_t ___localWindow_13;
	// System.Int64 BestHTTP.Connections.HTTP2.HTTP2Stream::remoteWindow
	int64_t ___remoteWindow_14;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Stream::windowUpdateThreshold
	uint32_t ___windowUpdateThreshold_15;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2Stream::sentData
	uint32_t ___sentData_16;
	// System.Boolean BestHTTP.Connections.HTTP2.HTTP2Stream::isRSTFrameSent
	bool ___isRSTFrameSent_17;
	// BestHTTP.Connections.HTTP2.HTTP2Response BestHTTP.Connections.HTTP2.HTTP2Stream::response
	HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE * ___response_18;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___U3CIdU3Ek__BackingField_1)); }
	inline uint32_t get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(uint32_t value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ____state_2)); }
	inline int32_t get__state_2() const { return ____state_2; }
	inline int32_t* get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(int32_t value)
	{
		____state_2 = value;
	}

	inline static int32_t get_offset_of_U3CAssignedRequestU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___U3CAssignedRequestU3Ek__BackingField_3)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_U3CAssignedRequestU3Ek__BackingField_3() const { return ___U3CAssignedRequestU3Ek__BackingField_3; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_U3CAssignedRequestU3Ek__BackingField_3() { return &___U3CAssignedRequestU3Ek__BackingField_3; }
	inline void set_U3CAssignedRequestU3Ek__BackingField_3(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___U3CAssignedRequestU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssignedRequestU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_isStreamedDownload_4() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___isStreamedDownload_4)); }
	inline bool get_isStreamedDownload_4() const { return ___isStreamedDownload_4; }
	inline bool* get_address_of_isStreamedDownload_4() { return &___isStreamedDownload_4; }
	inline void set_isStreamedDownload_4(bool value)
	{
		___isStreamedDownload_4 = value;
	}

	inline static int32_t get_offset_of_downloaded_5() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___downloaded_5)); }
	inline uint32_t get_downloaded_5() const { return ___downloaded_5; }
	inline uint32_t* get_address_of_downloaded_5() { return &___downloaded_5; }
	inline void set_downloaded_5(uint32_t value)
	{
		___downloaded_5 = value;
	}

	inline static int32_t get_offset_of_bodyToSend_6() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___bodyToSend_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bodyToSend_6() const { return ___bodyToSend_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bodyToSend_6() { return &___bodyToSend_6; }
	inline void set_bodyToSend_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bodyToSend_6 = value;
		Il2CppCodeGenWriteBarrier((&___bodyToSend_6), value);
	}

	inline static int32_t get_offset_of_bodyToSendOffset_7() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___bodyToSendOffset_7)); }
	inline uint32_t get_bodyToSendOffset_7() const { return ___bodyToSendOffset_7; }
	inline uint32_t* get_address_of_bodyToSendOffset_7() { return &___bodyToSendOffset_7; }
	inline void set_bodyToSendOffset_7(uint32_t value)
	{
		___bodyToSendOffset_7 = value;
	}

	inline static int32_t get_offset_of_settings_8() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___settings_8)); }
	inline HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * get_settings_8() const { return ___settings_8; }
	inline HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 ** get_address_of_settings_8() { return &___settings_8; }
	inline void set_settings_8(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * value)
	{
		___settings_8 = value;
		Il2CppCodeGenWriteBarrier((&___settings_8), value);
	}

	inline static int32_t get_offset_of_encoder_9() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___encoder_9)); }
	inline HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * get_encoder_9() const { return ___encoder_9; }
	inline HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 ** get_address_of_encoder_9() { return &___encoder_9; }
	inline void set_encoder_9(HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * value)
	{
		___encoder_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_9), value);
	}

	inline static int32_t get_offset_of_outgoing_10() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___outgoing_10)); }
	inline Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 * get_outgoing_10() const { return ___outgoing_10; }
	inline Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 ** get_address_of_outgoing_10() { return &___outgoing_10; }
	inline void set_outgoing_10(Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 * value)
	{
		___outgoing_10 = value;
		Il2CppCodeGenWriteBarrier((&___outgoing_10), value);
	}

	inline static int32_t get_offset_of_incomingFrames_11() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___incomingFrames_11)); }
	inline Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 * get_incomingFrames_11() const { return ___incomingFrames_11; }
	inline Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 ** get_address_of_incomingFrames_11() { return &___incomingFrames_11; }
	inline void set_incomingFrames_11(Queue_1_t6E0A3E37F77886A049FEB6FB3F1BD0BADEE385C9 * value)
	{
		___incomingFrames_11 = value;
		Il2CppCodeGenWriteBarrier((&___incomingFrames_11), value);
	}

	inline static int32_t get_offset_of_currentFramesView_12() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___currentFramesView_12)); }
	inline FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957 * get_currentFramesView_12() const { return ___currentFramesView_12; }
	inline FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957 ** get_address_of_currentFramesView_12() { return &___currentFramesView_12; }
	inline void set_currentFramesView_12(FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957 * value)
	{
		___currentFramesView_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentFramesView_12), value);
	}

	inline static int32_t get_offset_of_localWindow_13() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___localWindow_13)); }
	inline uint32_t get_localWindow_13() const { return ___localWindow_13; }
	inline uint32_t* get_address_of_localWindow_13() { return &___localWindow_13; }
	inline void set_localWindow_13(uint32_t value)
	{
		___localWindow_13 = value;
	}

	inline static int32_t get_offset_of_remoteWindow_14() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___remoteWindow_14)); }
	inline int64_t get_remoteWindow_14() const { return ___remoteWindow_14; }
	inline int64_t* get_address_of_remoteWindow_14() { return &___remoteWindow_14; }
	inline void set_remoteWindow_14(int64_t value)
	{
		___remoteWindow_14 = value;
	}

	inline static int32_t get_offset_of_windowUpdateThreshold_15() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___windowUpdateThreshold_15)); }
	inline uint32_t get_windowUpdateThreshold_15() const { return ___windowUpdateThreshold_15; }
	inline uint32_t* get_address_of_windowUpdateThreshold_15() { return &___windowUpdateThreshold_15; }
	inline void set_windowUpdateThreshold_15(uint32_t value)
	{
		___windowUpdateThreshold_15 = value;
	}

	inline static int32_t get_offset_of_sentData_16() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___sentData_16)); }
	inline uint32_t get_sentData_16() const { return ___sentData_16; }
	inline uint32_t* get_address_of_sentData_16() { return &___sentData_16; }
	inline void set_sentData_16(uint32_t value)
	{
		___sentData_16 = value;
	}

	inline static int32_t get_offset_of_isRSTFrameSent_17() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___isRSTFrameSent_17)); }
	inline bool get_isRSTFrameSent_17() const { return ___isRSTFrameSent_17; }
	inline bool* get_address_of_isRSTFrameSent_17() { return &___isRSTFrameSent_17; }
	inline void set_isRSTFrameSent_17(bool value)
	{
		___isRSTFrameSent_17 = value;
	}

	inline static int32_t get_offset_of_response_18() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF, ___response_18)); }
	inline HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE * get_response_18() const { return ___response_18; }
	inline HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE ** get_address_of_response_18() { return &___response_18; }
	inline void set_response_18(HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE * value)
	{
		___response_18 = value;
		Il2CppCodeGenWriteBarrier((&___response_18), value);
	}
};

struct HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF_StaticFields
{
public:
	// System.Int64 BestHTTP.Connections.HTTP2.HTTP2Stream::LastStreamId
	int64_t ___LastStreamId_0;

public:
	inline static int32_t get_offset_of_LastStreamId_0() { return static_cast<int32_t>(offsetof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF_StaticFields, ___LastStreamId_0)); }
	inline int64_t get_LastStreamId_0() const { return ___LastStreamId_0; }
	inline int64_t* get_address_of_LastStreamId_0() { return &___LastStreamId_0; }
	inline void set_LastStreamId_0(int64_t value)
	{
		___LastStreamId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2STREAM_T38D20F5421551C9D095E63974F5AE1B25E301BEF_H
#ifndef HUBOPTIONS_TCF020F4BD272719B187050645A2370E46E9AF458_H
#define HUBOPTIONS_TCF020F4BD272719B187050645A2370E46E9AF458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.HubOptions
struct  HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SignalRCore.HubOptions::<SkipNegotiation>k__BackingField
	bool ___U3CSkipNegotiationU3Ek__BackingField_0;
	// BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.HubOptions::<PreferedTransport>k__BackingField
	int32_t ___U3CPreferedTransportU3Ek__BackingField_1;
	// System.TimeSpan BestHTTP.SignalRCore.HubOptions::<PingInterval>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CPingIntervalU3Ek__BackingField_2;
	// System.Int32 BestHTTP.SignalRCore.HubOptions::<MaxRedirects>k__BackingField
	int32_t ___U3CMaxRedirectsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSkipNegotiationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458, ___U3CSkipNegotiationU3Ek__BackingField_0)); }
	inline bool get_U3CSkipNegotiationU3Ek__BackingField_0() const { return ___U3CSkipNegotiationU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSkipNegotiationU3Ek__BackingField_0() { return &___U3CSkipNegotiationU3Ek__BackingField_0; }
	inline void set_U3CSkipNegotiationU3Ek__BackingField_0(bool value)
	{
		___U3CSkipNegotiationU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CPreferedTransportU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458, ___U3CPreferedTransportU3Ek__BackingField_1)); }
	inline int32_t get_U3CPreferedTransportU3Ek__BackingField_1() const { return ___U3CPreferedTransportU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPreferedTransportU3Ek__BackingField_1() { return &___U3CPreferedTransportU3Ek__BackingField_1; }
	inline void set_U3CPreferedTransportU3Ek__BackingField_1(int32_t value)
	{
		___U3CPreferedTransportU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPingIntervalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458, ___U3CPingIntervalU3Ek__BackingField_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CPingIntervalU3Ek__BackingField_2() const { return ___U3CPingIntervalU3Ek__BackingField_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CPingIntervalU3Ek__BackingField_2() { return &___U3CPingIntervalU3Ek__BackingField_2; }
	inline void set_U3CPingIntervalU3Ek__BackingField_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CPingIntervalU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMaxRedirectsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458, ___U3CMaxRedirectsU3Ek__BackingField_3)); }
	inline int32_t get_U3CMaxRedirectsU3Ek__BackingField_3() const { return ___U3CMaxRedirectsU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CMaxRedirectsU3Ek__BackingField_3() { return &___U3CMaxRedirectsU3Ek__BackingField_3; }
	inline void set_U3CMaxRedirectsU3Ek__BackingField_3(int32_t value)
	{
		___U3CMaxRedirectsU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUBOPTIONS_TCF020F4BD272719B187050645A2370E46E9AF458_H
#ifndef RETRYCONTEXT_TA533507D4EFCF4312516B1589F945530D90BB2E2_H
#define RETRYCONTEXT_TA533507D4EFCF4312516B1589F945530D90BB2E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.RetryContext
struct  RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2 
{
public:
	// System.UInt32 BestHTTP.SignalRCore.RetryContext::PreviousRetryCount
	uint32_t ___PreviousRetryCount_0;
	// System.TimeSpan BestHTTP.SignalRCore.RetryContext::ElapsedTime
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___ElapsedTime_1;
	// System.String BestHTTP.SignalRCore.RetryContext::RetryReason
	String_t* ___RetryReason_2;

public:
	inline static int32_t get_offset_of_PreviousRetryCount_0() { return static_cast<int32_t>(offsetof(RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2, ___PreviousRetryCount_0)); }
	inline uint32_t get_PreviousRetryCount_0() const { return ___PreviousRetryCount_0; }
	inline uint32_t* get_address_of_PreviousRetryCount_0() { return &___PreviousRetryCount_0; }
	inline void set_PreviousRetryCount_0(uint32_t value)
	{
		___PreviousRetryCount_0 = value;
	}

	inline static int32_t get_offset_of_ElapsedTime_1() { return static_cast<int32_t>(offsetof(RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2, ___ElapsedTime_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_ElapsedTime_1() const { return ___ElapsedTime_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_ElapsedTime_1() { return &___ElapsedTime_1; }
	inline void set_ElapsedTime_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___ElapsedTime_1 = value;
	}

	inline static int32_t get_offset_of_RetryReason_2() { return static_cast<int32_t>(offsetof(RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2, ___RetryReason_2)); }
	inline String_t* get_RetryReason_2() const { return ___RetryReason_2; }
	inline String_t** get_address_of_RetryReason_2() { return &___RetryReason_2; }
	inline void set_RetryReason_2(String_t* value)
	{
		___RetryReason_2 = value;
		Il2CppCodeGenWriteBarrier((&___RetryReason_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.RetryContext
struct RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2_marshaled_pinvoke
{
	uint32_t ___PreviousRetryCount_0;
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___ElapsedTime_1;
	char* ___RetryReason_2;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.RetryContext
struct RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2_marshaled_com
{
	uint32_t ___PreviousRetryCount_0;
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___ElapsedTime_1;
	Il2CppChar* ___RetryReason_2;
};
#endif // RETRYCONTEXT_TA533507D4EFCF4312516B1589F945530D90BB2E2_H
#ifndef TRANSPORTBASE_TD1E09622468E1FC60AC579AD943D006B0BCEC75E_H
#define TRANSPORTBASE_TD1E09622468E1FC60AC579AD943D006B0BCEC75E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Transports.TransportBase
struct  TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.TransportStates BestHTTP.SignalRCore.Transports.TransportBase::_state
	int32_t ____state_0;
	// System.String BestHTTP.SignalRCore.Transports.TransportBase::<ErrorReason>k__BackingField
	String_t* ___U3CErrorReasonU3Ek__BackingField_1;
	// System.Action`2<BestHTTP.SignalRCore.TransportStates,BestHTTP.SignalRCore.TransportStates> BestHTTP.SignalRCore.Transports.TransportBase::OnStateChanged
	Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB * ___OnStateChanged_2;
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.Message> BestHTTP.SignalRCore.Transports.TransportBase::messages
	List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 * ___messages_3;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.SignalRCore.Transports.TransportBase::connection
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___connection_4;
	// System.Text.StringBuilder BestHTTP.SignalRCore.Transports.TransportBase::queryBuilder
	StringBuilder_t * ___queryBuilder_5;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ____state_0)); }
	inline int32_t get__state_0() const { return ____state_0; }
	inline int32_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(int32_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorReasonU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___U3CErrorReasonU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorReasonU3Ek__BackingField_1() const { return ___U3CErrorReasonU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorReasonU3Ek__BackingField_1() { return &___U3CErrorReasonU3Ek__BackingField_1; }
	inline void set_U3CErrorReasonU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorReasonU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorReasonU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_OnStateChanged_2() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___OnStateChanged_2)); }
	inline Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB * get_OnStateChanged_2() const { return ___OnStateChanged_2; }
	inline Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB ** get_address_of_OnStateChanged_2() { return &___OnStateChanged_2; }
	inline void set_OnStateChanged_2(Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB * value)
	{
		___OnStateChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_2), value);
	}

	inline static int32_t get_offset_of_messages_3() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___messages_3)); }
	inline List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 * get_messages_3() const { return ___messages_3; }
	inline List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 ** get_address_of_messages_3() { return &___messages_3; }
	inline void set_messages_3(List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 * value)
	{
		___messages_3 = value;
		Il2CppCodeGenWriteBarrier((&___messages_3), value);
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___connection_4)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_connection_4() const { return ___connection_4; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier((&___connection_4), value);
	}

	inline static int32_t get_offset_of_queryBuilder_5() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___queryBuilder_5)); }
	inline StringBuilder_t * get_queryBuilder_5() const { return ___queryBuilder_5; }
	inline StringBuilder_t ** get_address_of_queryBuilder_5() { return &___queryBuilder_5; }
	inline void set_queryBuilder_5(StringBuilder_t * value)
	{
		___queryBuilder_5 = value;
		Il2CppCodeGenWriteBarrier((&___queryBuilder_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTBASE_TD1E09622468E1FC60AC579AD943D006B0BCEC75E_H
#ifndef ERROR_TE44CD06EF17517E90824CE5DBE0E9C13F5817DD0_H
#define ERROR_TE44CD06EF17517E90824CE5DBE0E9C13F5817DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Error
struct  Error_tE44CD06EF17517E90824CE5DBE0E9C13F5817DD0  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.SocketIOErrors BestHTTP.SocketIO.Error::<Code>k__BackingField
	int32_t ___U3CCodeU3Ek__BackingField_0;
	// System.String BestHTTP.SocketIO.Error::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Error_tE44CD06EF17517E90824CE5DBE0E9C13F5817DD0, ___U3CCodeU3Ek__BackingField_0)); }
	inline int32_t get_U3CCodeU3Ek__BackingField_0() const { return ___U3CCodeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCodeU3Ek__BackingField_0() { return &___U3CCodeU3Ek__BackingField_0; }
	inline void set_U3CCodeU3Ek__BackingField_0(int32_t value)
	{
		___U3CCodeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Error_tE44CD06EF17517E90824CE5DBE0E9C13F5817DD0, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROR_TE44CD06EF17517E90824CE5DBE0E9C13F5817DD0_H
#ifndef HANDSHAKEDATA_T47B487FAE84B9C8264577C5490AE69424BB3694B_H
#define HANDSHAKEDATA_T47B487FAE84B9C8264577C5490AE69424BB3694B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.HandshakeData
struct  HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B  : public RuntimeObject
{
public:
	// System.String BestHTTP.SocketIO.HandshakeData::<Sid>k__BackingField
	String_t* ___U3CSidU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> BestHTTP.SocketIO.HandshakeData::<Upgrades>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CUpgradesU3Ek__BackingField_1;
	// System.TimeSpan BestHTTP.SocketIO.HandshakeData::<PingInterval>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CPingIntervalU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.SocketIO.HandshakeData::<PingTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CPingTimeoutU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B, ___U3CSidU3Ek__BackingField_0)); }
	inline String_t* get_U3CSidU3Ek__BackingField_0() const { return ___U3CSidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSidU3Ek__BackingField_0() { return &___U3CSidU3Ek__BackingField_0; }
	inline void set_U3CSidU3Ek__BackingField_0(String_t* value)
	{
		___U3CSidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUpgradesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B, ___U3CUpgradesU3Ek__BackingField_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CUpgradesU3Ek__BackingField_1() const { return ___U3CUpgradesU3Ek__BackingField_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CUpgradesU3Ek__BackingField_1() { return &___U3CUpgradesU3Ek__BackingField_1; }
	inline void set_U3CUpgradesU3Ek__BackingField_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CUpgradesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpgradesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPingIntervalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B, ___U3CPingIntervalU3Ek__BackingField_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CPingIntervalU3Ek__BackingField_2() const { return ___U3CPingIntervalU3Ek__BackingField_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CPingIntervalU3Ek__BackingField_2() { return &___U3CPingIntervalU3Ek__BackingField_2; }
	inline void set_U3CPingIntervalU3Ek__BackingField_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CPingIntervalU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPingTimeoutU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B, ___U3CPingTimeoutU3Ek__BackingField_3)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CPingTimeoutU3Ek__BackingField_3() const { return ___U3CPingTimeoutU3Ek__BackingField_3; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CPingTimeoutU3Ek__BackingField_3() { return &___U3CPingTimeoutU3Ek__BackingField_3; }
	inline void set_U3CPingTimeoutU3Ek__BackingField_3(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CPingTimeoutU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEDATA_T47B487FAE84B9C8264577C5490AE69424BB3694B_H
#ifndef PACKET_TAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B_H
#define PACKET_TAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Packet
struct  Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.TransportEventTypes BestHTTP.SocketIO.Packet::<TransportEvent>k__BackingField
	int32_t ___U3CTransportEventU3Ek__BackingField_1;
	// BestHTTP.SocketIO.SocketIOEventTypes BestHTTP.SocketIO.Packet::<SocketIOEvent>k__BackingField
	int32_t ___U3CSocketIOEventU3Ek__BackingField_2;
	// System.Int32 BestHTTP.SocketIO.Packet::<AttachmentCount>k__BackingField
	int32_t ___U3CAttachmentCountU3Ek__BackingField_3;
	// System.Int32 BestHTTP.SocketIO.Packet::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_4;
	// System.String BestHTTP.SocketIO.Packet::<Namespace>k__BackingField
	String_t* ___U3CNamespaceU3Ek__BackingField_5;
	// System.String BestHTTP.SocketIO.Packet::<Payload>k__BackingField
	String_t* ___U3CPayloadU3Ek__BackingField_6;
	// System.String BestHTTP.SocketIO.Packet::<EventName>k__BackingField
	String_t* ___U3CEventNameU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<System.Byte[]> BestHTTP.SocketIO.Packet::attachments
	List_1_t54B0C0103C4E102A12FB66CAFDE587719C3794FF * ___attachments_8;
	// System.Boolean BestHTTP.SocketIO.Packet::<IsDecoded>k__BackingField
	bool ___U3CIsDecodedU3Ek__BackingField_9;
	// System.Object[] BestHTTP.SocketIO.Packet::<DecodedArgs>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CDecodedArgsU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTransportEventU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CTransportEventU3Ek__BackingField_1)); }
	inline int32_t get_U3CTransportEventU3Ek__BackingField_1() const { return ___U3CTransportEventU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTransportEventU3Ek__BackingField_1() { return &___U3CTransportEventU3Ek__BackingField_1; }
	inline void set_U3CTransportEventU3Ek__BackingField_1(int32_t value)
	{
		___U3CTransportEventU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSocketIOEventU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CSocketIOEventU3Ek__BackingField_2)); }
	inline int32_t get_U3CSocketIOEventU3Ek__BackingField_2() const { return ___U3CSocketIOEventU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSocketIOEventU3Ek__BackingField_2() { return &___U3CSocketIOEventU3Ek__BackingField_2; }
	inline void set_U3CSocketIOEventU3Ek__BackingField_2(int32_t value)
	{
		___U3CSocketIOEventU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAttachmentCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CAttachmentCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CAttachmentCountU3Ek__BackingField_3() const { return ___U3CAttachmentCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CAttachmentCountU3Ek__BackingField_3() { return &___U3CAttachmentCountU3Ek__BackingField_3; }
	inline void set_U3CAttachmentCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CAttachmentCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CIdU3Ek__BackingField_4)); }
	inline int32_t get_U3CIdU3Ek__BackingField_4() const { return ___U3CIdU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_4() { return &___U3CIdU3Ek__BackingField_4; }
	inline void set_U3CIdU3Ek__BackingField_4(int32_t value)
	{
		___U3CIdU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CNamespaceU3Ek__BackingField_5)); }
	inline String_t* get_U3CNamespaceU3Ek__BackingField_5() const { return ___U3CNamespaceU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CNamespaceU3Ek__BackingField_5() { return &___U3CNamespaceU3Ek__BackingField_5; }
	inline void set_U3CNamespaceU3Ek__BackingField_5(String_t* value)
	{
		___U3CNamespaceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamespaceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPayloadU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CPayloadU3Ek__BackingField_6)); }
	inline String_t* get_U3CPayloadU3Ek__BackingField_6() const { return ___U3CPayloadU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CPayloadU3Ek__BackingField_6() { return &___U3CPayloadU3Ek__BackingField_6; }
	inline void set_U3CPayloadU3Ek__BackingField_6(String_t* value)
	{
		___U3CPayloadU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPayloadU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CEventNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CEventNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CEventNameU3Ek__BackingField_7() const { return ___U3CEventNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CEventNameU3Ek__BackingField_7() { return &___U3CEventNameU3Ek__BackingField_7; }
	inline void set_U3CEventNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CEventNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventNameU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_attachments_8() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___attachments_8)); }
	inline List_1_t54B0C0103C4E102A12FB66CAFDE587719C3794FF * get_attachments_8() const { return ___attachments_8; }
	inline List_1_t54B0C0103C4E102A12FB66CAFDE587719C3794FF ** get_address_of_attachments_8() { return &___attachments_8; }
	inline void set_attachments_8(List_1_t54B0C0103C4E102A12FB66CAFDE587719C3794FF * value)
	{
		___attachments_8 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_8), value);
	}

	inline static int32_t get_offset_of_U3CIsDecodedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CIsDecodedU3Ek__BackingField_9)); }
	inline bool get_U3CIsDecodedU3Ek__BackingField_9() const { return ___U3CIsDecodedU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsDecodedU3Ek__BackingField_9() { return &___U3CIsDecodedU3Ek__BackingField_9; }
	inline void set_U3CIsDecodedU3Ek__BackingField_9(bool value)
	{
		___U3CIsDecodedU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CDecodedArgsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B, ___U3CDecodedArgsU3Ek__BackingField_10)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CDecodedArgsU3Ek__BackingField_10() const { return ___U3CDecodedArgsU3Ek__BackingField_10; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CDecodedArgsU3Ek__BackingField_10() { return &___U3CDecodedArgsU3Ek__BackingField_10; }
	inline void set_U3CDecodedArgsU3Ek__BackingField_10(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CDecodedArgsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDecodedArgsU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PACKET_TAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B_H
#ifndef SOCKETMANAGER_T45E03E3DB4B05E935AA29985865B063BEADC7322_H
#define SOCKETMANAGER_T45E03E3DB4B05E935AA29985865B063BEADC7322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.SocketManager
struct  SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.SocketManager_States BestHTTP.SocketIO.SocketManager::state
	int32_t ___state_2;
	// BestHTTP.SocketIO.SocketOptions BestHTTP.SocketIO.SocketManager::<Options>k__BackingField
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228 * ___U3COptionsU3Ek__BackingField_3;
	// System.Uri BestHTTP.SocketIO.SocketManager::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_4;
	// BestHTTP.SocketIO.HandshakeData BestHTTP.SocketIO.SocketManager::<Handshake>k__BackingField
	HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B * ___U3CHandshakeU3Ek__BackingField_5;
	// BestHTTP.SocketIO.Transports.ITransport BestHTTP.SocketIO.SocketManager::<Transport>k__BackingField
	RuntimeObject* ___U3CTransportU3Ek__BackingField_6;
	// System.UInt64 BestHTTP.SocketIO.SocketManager::<RequestCounter>k__BackingField
	uint64_t ___U3CRequestCounterU3Ek__BackingField_7;
	// System.Int32 BestHTTP.SocketIO.SocketManager::<ReconnectAttempts>k__BackingField
	int32_t ___U3CReconnectAttemptsU3Ek__BackingField_8;
	// BestHTTP.SocketIO.JsonEncoders.IJsonEncoder BestHTTP.SocketIO.SocketManager::<Encoder>k__BackingField
	RuntimeObject* ___U3CEncoderU3Ek__BackingField_9;
	// System.Int32 BestHTTP.SocketIO.SocketManager::nextAckId
	int32_t ___nextAckId_10;
	// BestHTTP.SocketIO.SocketManager_States BestHTTP.SocketIO.SocketManager::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_11;
	// BestHTTP.SocketIO.Transports.ITransport BestHTTP.SocketIO.SocketManager::<UpgradingTransport>k__BackingField
	RuntimeObject* ___U3CUpgradingTransportU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SocketIO.Socket> BestHTTP.SocketIO.SocketManager::Namespaces
	Dictionary_2_t2AD984ED67F12B1BA302EC41EDF94B77F1F66CC6 * ___Namespaces_13;
	// System.Collections.Generic.List`1<BestHTTP.SocketIO.Socket> BestHTTP.SocketIO.SocketManager::Sockets
	List_1_t6C9B151BA50582B180AF8BEEFA47DF94516E0ED2 * ___Sockets_14;
	// System.Collections.Generic.List`1<BestHTTP.SocketIO.Packet> BestHTTP.SocketIO.SocketManager::OfflinePackets
	List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC * ___OfflinePackets_15;
	// System.DateTime BestHTTP.SocketIO.SocketManager::LastHeartbeat
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___LastHeartbeat_16;
	// System.DateTime BestHTTP.SocketIO.SocketManager::ReconnectAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___ReconnectAt_17;
	// System.DateTime BestHTTP.SocketIO.SocketManager::ConnectionStarted
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___ConnectionStarted_18;
	// System.Boolean BestHTTP.SocketIO.SocketManager::closing
	bool ___closing_19;
	// System.Boolean BestHTTP.SocketIO.SocketManager::IsWaitingPong
	bool ___IsWaitingPong_20;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3COptionsU3Ek__BackingField_3)); }
	inline SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228 * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228 ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228 * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CUriU3Ek__BackingField_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_4() const { return ___U3CUriU3Ek__BackingField_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_4() { return &___U3CUriU3Ek__BackingField_4; }
	inline void set_U3CUriU3Ek__BackingField_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHandshakeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CHandshakeU3Ek__BackingField_5)); }
	inline HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B * get_U3CHandshakeU3Ek__BackingField_5() const { return ___U3CHandshakeU3Ek__BackingField_5; }
	inline HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B ** get_address_of_U3CHandshakeU3Ek__BackingField_5() { return &___U3CHandshakeU3Ek__BackingField_5; }
	inline void set_U3CHandshakeU3Ek__BackingField_5(HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B * value)
	{
		___U3CHandshakeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHandshakeU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CTransportU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CTransportU3Ek__BackingField_6() const { return ___U3CTransportU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CTransportU3Ek__BackingField_6() { return &___U3CTransportU3Ek__BackingField_6; }
	inline void set_U3CTransportU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CTransportU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTransportU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CRequestCounterU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CRequestCounterU3Ek__BackingField_7)); }
	inline uint64_t get_U3CRequestCounterU3Ek__BackingField_7() const { return ___U3CRequestCounterU3Ek__BackingField_7; }
	inline uint64_t* get_address_of_U3CRequestCounterU3Ek__BackingField_7() { return &___U3CRequestCounterU3Ek__BackingField_7; }
	inline void set_U3CRequestCounterU3Ek__BackingField_7(uint64_t value)
	{
		___U3CRequestCounterU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectAttemptsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CReconnectAttemptsU3Ek__BackingField_8)); }
	inline int32_t get_U3CReconnectAttemptsU3Ek__BackingField_8() const { return ___U3CReconnectAttemptsU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CReconnectAttemptsU3Ek__BackingField_8() { return &___U3CReconnectAttemptsU3Ek__BackingField_8; }
	inline void set_U3CReconnectAttemptsU3Ek__BackingField_8(int32_t value)
	{
		___U3CReconnectAttemptsU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CEncoderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CEncoderU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CEncoderU3Ek__BackingField_9() const { return ___U3CEncoderU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CEncoderU3Ek__BackingField_9() { return &___U3CEncoderU3Ek__BackingField_9; }
	inline void set_U3CEncoderU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CEncoderU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEncoderU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_nextAckId_10() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___nextAckId_10)); }
	inline int32_t get_nextAckId_10() const { return ___nextAckId_10; }
	inline int32_t* get_address_of_nextAckId_10() { return &___nextAckId_10; }
	inline void set_nextAckId_10(int32_t value)
	{
		___nextAckId_10 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CPreviousStateU3Ek__BackingField_11)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_11() const { return ___U3CPreviousStateU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_11() { return &___U3CPreviousStateU3Ek__BackingField_11; }
	inline void set_U3CPreviousStateU3Ek__BackingField_11(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CUpgradingTransportU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___U3CUpgradingTransportU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CUpgradingTransportU3Ek__BackingField_12() const { return ___U3CUpgradingTransportU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CUpgradingTransportU3Ek__BackingField_12() { return &___U3CUpgradingTransportU3Ek__BackingField_12; }
	inline void set_U3CUpgradingTransportU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CUpgradingTransportU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpgradingTransportU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_Namespaces_13() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___Namespaces_13)); }
	inline Dictionary_2_t2AD984ED67F12B1BA302EC41EDF94B77F1F66CC6 * get_Namespaces_13() const { return ___Namespaces_13; }
	inline Dictionary_2_t2AD984ED67F12B1BA302EC41EDF94B77F1F66CC6 ** get_address_of_Namespaces_13() { return &___Namespaces_13; }
	inline void set_Namespaces_13(Dictionary_2_t2AD984ED67F12B1BA302EC41EDF94B77F1F66CC6 * value)
	{
		___Namespaces_13 = value;
		Il2CppCodeGenWriteBarrier((&___Namespaces_13), value);
	}

	inline static int32_t get_offset_of_Sockets_14() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___Sockets_14)); }
	inline List_1_t6C9B151BA50582B180AF8BEEFA47DF94516E0ED2 * get_Sockets_14() const { return ___Sockets_14; }
	inline List_1_t6C9B151BA50582B180AF8BEEFA47DF94516E0ED2 ** get_address_of_Sockets_14() { return &___Sockets_14; }
	inline void set_Sockets_14(List_1_t6C9B151BA50582B180AF8BEEFA47DF94516E0ED2 * value)
	{
		___Sockets_14 = value;
		Il2CppCodeGenWriteBarrier((&___Sockets_14), value);
	}

	inline static int32_t get_offset_of_OfflinePackets_15() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___OfflinePackets_15)); }
	inline List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC * get_OfflinePackets_15() const { return ___OfflinePackets_15; }
	inline List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC ** get_address_of_OfflinePackets_15() { return &___OfflinePackets_15; }
	inline void set_OfflinePackets_15(List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC * value)
	{
		___OfflinePackets_15 = value;
		Il2CppCodeGenWriteBarrier((&___OfflinePackets_15), value);
	}

	inline static int32_t get_offset_of_LastHeartbeat_16() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___LastHeartbeat_16)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_LastHeartbeat_16() const { return ___LastHeartbeat_16; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_LastHeartbeat_16() { return &___LastHeartbeat_16; }
	inline void set_LastHeartbeat_16(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___LastHeartbeat_16 = value;
	}

	inline static int32_t get_offset_of_ReconnectAt_17() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___ReconnectAt_17)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_ReconnectAt_17() const { return ___ReconnectAt_17; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_ReconnectAt_17() { return &___ReconnectAt_17; }
	inline void set_ReconnectAt_17(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___ReconnectAt_17 = value;
	}

	inline static int32_t get_offset_of_ConnectionStarted_18() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___ConnectionStarted_18)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_ConnectionStarted_18() const { return ___ConnectionStarted_18; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_ConnectionStarted_18() { return &___ConnectionStarted_18; }
	inline void set_ConnectionStarted_18(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___ConnectionStarted_18 = value;
	}

	inline static int32_t get_offset_of_closing_19() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___closing_19)); }
	inline bool get_closing_19() const { return ___closing_19; }
	inline bool* get_address_of_closing_19() { return &___closing_19; }
	inline void set_closing_19(bool value)
	{
		___closing_19 = value;
	}

	inline static int32_t get_offset_of_IsWaitingPong_20() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322, ___IsWaitingPong_20)); }
	inline bool get_IsWaitingPong_20() const { return ___IsWaitingPong_20; }
	inline bool* get_address_of_IsWaitingPong_20() { return &___IsWaitingPong_20; }
	inline void set_IsWaitingPong_20(bool value)
	{
		___IsWaitingPong_20 = value;
	}
};

struct SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322_StaticFields
{
public:
	// BestHTTP.SocketIO.JsonEncoders.IJsonEncoder BestHTTP.SocketIO.SocketManager::DefaultEncoder
	RuntimeObject* ___DefaultEncoder_0;

public:
	inline static int32_t get_offset_of_DefaultEncoder_0() { return static_cast<int32_t>(offsetof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322_StaticFields, ___DefaultEncoder_0)); }
	inline RuntimeObject* get_DefaultEncoder_0() const { return ___DefaultEncoder_0; }
	inline RuntimeObject** get_address_of_DefaultEncoder_0() { return &___DefaultEncoder_0; }
	inline void set_DefaultEncoder_0(RuntimeObject* value)
	{
		___DefaultEncoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultEncoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETMANAGER_T45E03E3DB4B05E935AA29985865B063BEADC7322_H
#ifndef SOCKETOPTIONS_TB72716A15B56E075578905181A1659A85AAAB228_H
#define SOCKETOPTIONS_TB72716A15B56E075578905181A1659A85AAAB228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.SocketOptions
struct  SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.Transports.TransportTypes BestHTTP.SocketIO.SocketOptions::<ConnectWith>k__BackingField
	int32_t ___U3CConnectWithU3Ek__BackingField_0;
	// System.Boolean BestHTTP.SocketIO.SocketOptions::<Reconnection>k__BackingField
	bool ___U3CReconnectionU3Ek__BackingField_1;
	// System.Int32 BestHTTP.SocketIO.SocketOptions::<ReconnectionAttempts>k__BackingField
	int32_t ___U3CReconnectionAttemptsU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.SocketIO.SocketOptions::<ReconnectionDelay>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CReconnectionDelayU3Ek__BackingField_3;
	// System.TimeSpan BestHTTP.SocketIO.SocketOptions::<ReconnectionDelayMax>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CReconnectionDelayMaxU3Ek__BackingField_4;
	// System.Single BestHTTP.SocketIO.SocketOptions::randomizationFactor
	float ___randomizationFactor_5;
	// System.TimeSpan BestHTTP.SocketIO.SocketOptions::<Timeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CTimeoutU3Ek__BackingField_6;
	// System.Boolean BestHTTP.SocketIO.SocketOptions::<AutoConnect>k__BackingField
	bool ___U3CAutoConnectU3Ek__BackingField_7;
	// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String> BestHTTP.SocketIO.SocketOptions::additionalQueryParams
	ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE * ___additionalQueryParams_8;
	// System.Boolean BestHTTP.SocketIO.SocketOptions::<QueryParamsOnlyForHandshake>k__BackingField
	bool ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9;
	// System.String BestHTTP.SocketIO.SocketOptions::BuiltQueryParams
	String_t* ___BuiltQueryParams_10;

public:
	inline static int32_t get_offset_of_U3CConnectWithU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CConnectWithU3Ek__BackingField_0)); }
	inline int32_t get_U3CConnectWithU3Ek__BackingField_0() const { return ___U3CConnectWithU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CConnectWithU3Ek__BackingField_0() { return &___U3CConnectWithU3Ek__BackingField_0; }
	inline void set_U3CConnectWithU3Ek__BackingField_0(int32_t value)
	{
		___U3CConnectWithU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CReconnectionU3Ek__BackingField_1)); }
	inline bool get_U3CReconnectionU3Ek__BackingField_1() const { return ___U3CReconnectionU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CReconnectionU3Ek__BackingField_1() { return &___U3CReconnectionU3Ek__BackingField_1; }
	inline void set_U3CReconnectionU3Ek__BackingField_1(bool value)
	{
		___U3CReconnectionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CReconnectionAttemptsU3Ek__BackingField_2)); }
	inline int32_t get_U3CReconnectionAttemptsU3Ek__BackingField_2() const { return ___U3CReconnectionAttemptsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CReconnectionAttemptsU3Ek__BackingField_2() { return &___U3CReconnectionAttemptsU3Ek__BackingField_2; }
	inline void set_U3CReconnectionAttemptsU3Ek__BackingField_2(int32_t value)
	{
		___U3CReconnectionAttemptsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionDelayU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CReconnectionDelayU3Ek__BackingField_3)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CReconnectionDelayU3Ek__BackingField_3() const { return ___U3CReconnectionDelayU3Ek__BackingField_3; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CReconnectionDelayU3Ek__BackingField_3() { return &___U3CReconnectionDelayU3Ek__BackingField_3; }
	inline void set_U3CReconnectionDelayU3Ek__BackingField_3(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CReconnectionDelayU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionDelayMaxU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CReconnectionDelayMaxU3Ek__BackingField_4)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CReconnectionDelayMaxU3Ek__BackingField_4() const { return ___U3CReconnectionDelayMaxU3Ek__BackingField_4; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CReconnectionDelayMaxU3Ek__BackingField_4() { return &___U3CReconnectionDelayMaxU3Ek__BackingField_4; }
	inline void set_U3CReconnectionDelayMaxU3Ek__BackingField_4(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CReconnectionDelayMaxU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_randomizationFactor_5() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___randomizationFactor_5)); }
	inline float get_randomizationFactor_5() const { return ___randomizationFactor_5; }
	inline float* get_address_of_randomizationFactor_5() { return &___randomizationFactor_5; }
	inline void set_randomizationFactor_5(float value)
	{
		___randomizationFactor_5 = value;
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CTimeoutU3Ek__BackingField_6)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CTimeoutU3Ek__BackingField_6() const { return ___U3CTimeoutU3Ek__BackingField_6; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CTimeoutU3Ek__BackingField_6() { return &___U3CTimeoutU3Ek__BackingField_6; }
	inline void set_U3CTimeoutU3Ek__BackingField_6(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CTimeoutU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAutoConnectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CAutoConnectU3Ek__BackingField_7)); }
	inline bool get_U3CAutoConnectU3Ek__BackingField_7() const { return ___U3CAutoConnectU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CAutoConnectU3Ek__BackingField_7() { return &___U3CAutoConnectU3Ek__BackingField_7; }
	inline void set_U3CAutoConnectU3Ek__BackingField_7(bool value)
	{
		___U3CAutoConnectU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_additionalQueryParams_8() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___additionalQueryParams_8)); }
	inline ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE * get_additionalQueryParams_8() const { return ___additionalQueryParams_8; }
	inline ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE ** get_address_of_additionalQueryParams_8() { return &___additionalQueryParams_8; }
	inline void set_additionalQueryParams_8(ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE * value)
	{
		___additionalQueryParams_8 = value;
		Il2CppCodeGenWriteBarrier((&___additionalQueryParams_8), value);
	}

	inline static int32_t get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9)); }
	inline bool get_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9() const { return ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9() { return &___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9; }
	inline void set_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9(bool value)
	{
		___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_BuiltQueryParams_10() { return static_cast<int32_t>(offsetof(SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228, ___BuiltQueryParams_10)); }
	inline String_t* get_BuiltQueryParams_10() const { return ___BuiltQueryParams_10; }
	inline String_t** get_address_of_BuiltQueryParams_10() { return &___BuiltQueryParams_10; }
	inline void set_BuiltQueryParams_10(String_t* value)
	{
		___BuiltQueryParams_10 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltQueryParams_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONS_TB72716A15B56E075578905181A1659A85AAAB228_H
#ifndef POLLINGTRANSPORT_TEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9_H
#define POLLINGTRANSPORT_TEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Transports.PollingTransport
struct  PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.Transports.TransportStates BestHTTP.SocketIO.Transports.PollingTransport::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_0;
	// BestHTTP.SocketIO.SocketManager BestHTTP.SocketIO.Transports.PollingTransport::<Manager>k__BackingField
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * ___U3CManagerU3Ek__BackingField_1;
	// BestHTTP.HTTPRequest BestHTTP.SocketIO.Transports.PollingTransport::LastRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___LastRequest_2;
	// BestHTTP.HTTPRequest BestHTTP.SocketIO.Transports.PollingTransport::PollRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___PollRequest_3;
	// BestHTTP.SocketIO.Packet BestHTTP.SocketIO.Transports.PollingTransport::PacketWithAttachment
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B * ___PacketWithAttachment_4;
	// System.Collections.Generic.List`1<BestHTTP.SocketIO.Packet> BestHTTP.SocketIO.Transports.PollingTransport::lonelyPacketList
	List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC * ___lonelyPacketList_5;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9, ___U3CStateU3Ek__BackingField_0)); }
	inline int32_t get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(int32_t value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9, ___U3CManagerU3Ek__BackingField_1)); }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * get_U3CManagerU3Ek__BackingField_1() const { return ___U3CManagerU3Ek__BackingField_1; }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 ** get_address_of_U3CManagerU3Ek__BackingField_1() { return &___U3CManagerU3Ek__BackingField_1; }
	inline void set_U3CManagerU3Ek__BackingField_1(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * value)
	{
		___U3CManagerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManagerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_LastRequest_2() { return static_cast<int32_t>(offsetof(PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9, ___LastRequest_2)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_LastRequest_2() const { return ___LastRequest_2; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_LastRequest_2() { return &___LastRequest_2; }
	inline void set_LastRequest_2(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___LastRequest_2 = value;
		Il2CppCodeGenWriteBarrier((&___LastRequest_2), value);
	}

	inline static int32_t get_offset_of_PollRequest_3() { return static_cast<int32_t>(offsetof(PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9, ___PollRequest_3)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_PollRequest_3() const { return ___PollRequest_3; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_PollRequest_3() { return &___PollRequest_3; }
	inline void set_PollRequest_3(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___PollRequest_3 = value;
		Il2CppCodeGenWriteBarrier((&___PollRequest_3), value);
	}

	inline static int32_t get_offset_of_PacketWithAttachment_4() { return static_cast<int32_t>(offsetof(PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9, ___PacketWithAttachment_4)); }
	inline Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B * get_PacketWithAttachment_4() const { return ___PacketWithAttachment_4; }
	inline Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B ** get_address_of_PacketWithAttachment_4() { return &___PacketWithAttachment_4; }
	inline void set_PacketWithAttachment_4(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B * value)
	{
		___PacketWithAttachment_4 = value;
		Il2CppCodeGenWriteBarrier((&___PacketWithAttachment_4), value);
	}

	inline static int32_t get_offset_of_lonelyPacketList_5() { return static_cast<int32_t>(offsetof(PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9, ___lonelyPacketList_5)); }
	inline List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC * get_lonelyPacketList_5() const { return ___lonelyPacketList_5; }
	inline List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC ** get_address_of_lonelyPacketList_5() { return &___lonelyPacketList_5; }
	inline void set_lonelyPacketList_5(List_1_t69C59719E2768B965D27EA3BA6E468D1DC9BD4FC * value)
	{
		___lonelyPacketList_5 = value;
		Il2CppCodeGenWriteBarrier((&___lonelyPacketList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLLINGTRANSPORT_TEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9_H
#ifndef WEBSOCKETTRANSPORT_T172705C4D15FA5B498A95DC03197FC25B759F44C_H
#define WEBSOCKETTRANSPORT_T172705C4D15FA5B498A95DC03197FC25B759F44C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Transports.WebSocketTransport
struct  WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C  : public RuntimeObject
{
public:
	// BestHTTP.SocketIO.Transports.TransportStates BestHTTP.SocketIO.Transports.WebSocketTransport::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_0;
	// BestHTTP.SocketIO.SocketManager BestHTTP.SocketIO.Transports.WebSocketTransport::<Manager>k__BackingField
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * ___U3CManagerU3Ek__BackingField_1;
	// BestHTTP.WebSocket.WebSocket BestHTTP.SocketIO.Transports.WebSocketTransport::<Implementation>k__BackingField
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * ___U3CImplementationU3Ek__BackingField_2;
	// BestHTTP.SocketIO.Packet BestHTTP.SocketIO.Transports.WebSocketTransport::PacketWithAttachment
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B * ___PacketWithAttachment_3;
	// System.Byte[] BestHTTP.SocketIO.Transports.WebSocketTransport::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_4;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C, ___U3CStateU3Ek__BackingField_0)); }
	inline int32_t get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(int32_t value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C, ___U3CManagerU3Ek__BackingField_1)); }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * get_U3CManagerU3Ek__BackingField_1() const { return ___U3CManagerU3Ek__BackingField_1; }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 ** get_address_of_U3CManagerU3Ek__BackingField_1() { return &___U3CManagerU3Ek__BackingField_1; }
	inline void set_U3CManagerU3Ek__BackingField_1(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * value)
	{
		___U3CManagerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManagerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CImplementationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C, ___U3CImplementationU3Ek__BackingField_2)); }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * get_U3CImplementationU3Ek__BackingField_2() const { return ___U3CImplementationU3Ek__BackingField_2; }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 ** get_address_of_U3CImplementationU3Ek__BackingField_2() { return &___U3CImplementationU3Ek__BackingField_2; }
	inline void set_U3CImplementationU3Ek__BackingField_2(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * value)
	{
		___U3CImplementationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImplementationU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_PacketWithAttachment_3() { return static_cast<int32_t>(offsetof(WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C, ___PacketWithAttachment_3)); }
	inline Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B * get_PacketWithAttachment_3() const { return ___PacketWithAttachment_3; }
	inline Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B ** get_address_of_PacketWithAttachment_3() { return &___PacketWithAttachment_3; }
	inline void set_PacketWithAttachment_3(Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B * value)
	{
		___PacketWithAttachment_3 = value;
		Il2CppCodeGenWriteBarrier((&___PacketWithAttachment_3), value);
	}

	inline static int32_t get_offset_of_Buffer_4() { return static_cast<int32_t>(offsetof(WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C, ___Buffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_4() const { return ___Buffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_4() { return &___Buffer_4; }
	inline void set_Buffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETTRANSPORT_T172705C4D15FA5B498A95DC03197FC25B759F44C_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef HTTP2ALTSVCFRAME_T60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749_H
#define HTTP2ALTSVCFRAME_T60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2AltSVCFrame
struct  HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2AltSVCFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.String BestHTTP.Connections.HTTP2.HTTP2AltSVCFrame::Origin
	String_t* ___Origin_1;
	// System.String BestHTTP.Connections.HTTP2.HTTP2AltSVCFrame::AltSvcFieldValue
	String_t* ___AltSvcFieldValue_2;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_Origin_1() { return static_cast<int32_t>(offsetof(HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749, ___Origin_1)); }
	inline String_t* get_Origin_1() const { return ___Origin_1; }
	inline String_t** get_address_of_Origin_1() { return &___Origin_1; }
	inline void set_Origin_1(String_t* value)
	{
		___Origin_1 = value;
		Il2CppCodeGenWriteBarrier((&___Origin_1), value);
	}

	inline static int32_t get_offset_of_AltSvcFieldValue_2() { return static_cast<int32_t>(offsetof(HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749, ___AltSvcFieldValue_2)); }
	inline String_t* get_AltSvcFieldValue_2() const { return ___AltSvcFieldValue_2; }
	inline String_t** get_address_of_AltSvcFieldValue_2() { return &___AltSvcFieldValue_2; }
	inline void set_AltSvcFieldValue_2(String_t* value)
	{
		___AltSvcFieldValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___AltSvcFieldValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2AltSVCFrame
struct HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	char* ___Origin_1;
	char* ___AltSvcFieldValue_2;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2AltSVCFrame
struct HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	Il2CppChar* ___Origin_1;
	Il2CppChar* ___AltSvcFieldValue_2;
};
#endif // HTTP2ALTSVCFRAME_T60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749_H
#ifndef HTTP2CONTINUATIONFRAME_TE3F847723987F55AA05EAFF75241367D7057C93A_H
#define HTTP2CONTINUATIONFRAME_TE3F847723987F55AA05EAFF75241367D7057C93A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2ContinuationFrame
struct  HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2ContinuationFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2ContinuationFrame::HeaderBlockFragment
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___HeaderBlockFragment_1;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_HeaderBlockFragment_1() { return static_cast<int32_t>(offsetof(HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A, ___HeaderBlockFragment_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_HeaderBlockFragment_1() const { return ___HeaderBlockFragment_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_HeaderBlockFragment_1() { return &___HeaderBlockFragment_1; }
	inline void set_HeaderBlockFragment_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___HeaderBlockFragment_1 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderBlockFragment_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2ContinuationFrame
struct HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	uint8_t* ___HeaderBlockFragment_1;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2ContinuationFrame
struct HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	uint8_t* ___HeaderBlockFragment_1;
};
#endif // HTTP2CONTINUATIONFRAME_TE3F847723987F55AA05EAFF75241367D7057C93A_H
#ifndef HTTP2DATAFRAME_TEC2983D8AEF409BE4F2A15546C5A0E18F402D422_H
#define HTTP2DATAFRAME_TEC2983D8AEF409BE4F2A15546C5A0E18F402D422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2DataFrame
struct  HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2DataFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Nullable`1<System.Byte> BestHTTP.Connections.HTTP2.HTTP2DataFrame::PadLength
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2DataFrame::DataIdx
	uint32_t ___DataIdx_2;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2DataFrame::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_3;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2DataFrame::DataLength
	uint32_t ___DataLength_4;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_PadLength_1() { return static_cast<int32_t>(offsetof(HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422, ___PadLength_1)); }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  get_PadLength_1() const { return ___PadLength_1; }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7 * get_address_of_PadLength_1() { return &___PadLength_1; }
	inline void set_PadLength_1(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  value)
	{
		___PadLength_1 = value;
	}

	inline static int32_t get_offset_of_DataIdx_2() { return static_cast<int32_t>(offsetof(HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422, ___DataIdx_2)); }
	inline uint32_t get_DataIdx_2() const { return ___DataIdx_2; }
	inline uint32_t* get_address_of_DataIdx_2() { return &___DataIdx_2; }
	inline void set_DataIdx_2(uint32_t value)
	{
		___DataIdx_2 = value;
	}

	inline static int32_t get_offset_of_Data_3() { return static_cast<int32_t>(offsetof(HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422, ___Data_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_3() const { return ___Data_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_3() { return &___Data_3; }
	inline void set_Data_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_3 = value;
		Il2CppCodeGenWriteBarrier((&___Data_3), value);
	}

	inline static int32_t get_offset_of_DataLength_4() { return static_cast<int32_t>(offsetof(HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422, ___DataLength_4)); }
	inline uint32_t get_DataLength_4() const { return ___DataLength_4; }
	inline uint32_t* get_address_of_DataLength_4() { return &___DataLength_4; }
	inline void set_DataLength_4(uint32_t value)
	{
		___DataLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2DataFrame
struct HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	uint32_t ___DataIdx_2;
	uint8_t* ___Data_3;
	uint32_t ___DataLength_4;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2DataFrame
struct HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	uint32_t ___DataIdx_2;
	uint8_t* ___Data_3;
	uint32_t ___DataLength_4;
};
#endif // HTTP2DATAFRAME_TEC2983D8AEF409BE4F2A15546C5A0E18F402D422_H
#ifndef HTTP2GOAWAYFRAME_T3BBF69C99EA74EB7458A23980776E86CD58DECBD_H
#define HTTP2GOAWAYFRAME_T3BBF69C99EA74EB7458A23980776E86CD58DECBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame
struct  HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame::ReservedBit
	uint8_t ___ReservedBit_1;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame::LastStreamId
	uint32_t ___LastStreamId_2;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame::ErrorCode
	uint32_t ___ErrorCode_3;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame::AdditionalDebugData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___AdditionalDebugData_4;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame::AdditionalDebugDataLength
	uint32_t ___AdditionalDebugDataLength_5;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_ReservedBit_1() { return static_cast<int32_t>(offsetof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD, ___ReservedBit_1)); }
	inline uint8_t get_ReservedBit_1() const { return ___ReservedBit_1; }
	inline uint8_t* get_address_of_ReservedBit_1() { return &___ReservedBit_1; }
	inline void set_ReservedBit_1(uint8_t value)
	{
		___ReservedBit_1 = value;
	}

	inline static int32_t get_offset_of_LastStreamId_2() { return static_cast<int32_t>(offsetof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD, ___LastStreamId_2)); }
	inline uint32_t get_LastStreamId_2() const { return ___LastStreamId_2; }
	inline uint32_t* get_address_of_LastStreamId_2() { return &___LastStreamId_2; }
	inline void set_LastStreamId_2(uint32_t value)
	{
		___LastStreamId_2 = value;
	}

	inline static int32_t get_offset_of_ErrorCode_3() { return static_cast<int32_t>(offsetof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD, ___ErrorCode_3)); }
	inline uint32_t get_ErrorCode_3() const { return ___ErrorCode_3; }
	inline uint32_t* get_address_of_ErrorCode_3() { return &___ErrorCode_3; }
	inline void set_ErrorCode_3(uint32_t value)
	{
		___ErrorCode_3 = value;
	}

	inline static int32_t get_offset_of_AdditionalDebugData_4() { return static_cast<int32_t>(offsetof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD, ___AdditionalDebugData_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_AdditionalDebugData_4() const { return ___AdditionalDebugData_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_AdditionalDebugData_4() { return &___AdditionalDebugData_4; }
	inline void set_AdditionalDebugData_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___AdditionalDebugData_4 = value;
		Il2CppCodeGenWriteBarrier((&___AdditionalDebugData_4), value);
	}

	inline static int32_t get_offset_of_AdditionalDebugDataLength_5() { return static_cast<int32_t>(offsetof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD, ___AdditionalDebugDataLength_5)); }
	inline uint32_t get_AdditionalDebugDataLength_5() const { return ___AdditionalDebugDataLength_5; }
	inline uint32_t* get_address_of_AdditionalDebugDataLength_5() { return &___AdditionalDebugDataLength_5; }
	inline void set_AdditionalDebugDataLength_5(uint32_t value)
	{
		___AdditionalDebugDataLength_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame
struct HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	uint8_t ___ReservedBit_1;
	uint32_t ___LastStreamId_2;
	uint32_t ___ErrorCode_3;
	uint8_t* ___AdditionalDebugData_4;
	uint32_t ___AdditionalDebugDataLength_5;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2GoAwayFrame
struct HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	uint8_t ___ReservedBit_1;
	uint32_t ___LastStreamId_2;
	uint32_t ___ErrorCode_3;
	uint8_t* ___AdditionalDebugData_4;
	uint32_t ___AdditionalDebugDataLength_5;
};
#endif // HTTP2GOAWAYFRAME_T3BBF69C99EA74EB7458A23980776E86CD58DECBD_H
#ifndef HTTP2HEADERSFRAME_TA41B2C271AB61127E7AE870491AE8053A1ED3706_H
#define HTTP2HEADERSFRAME_TA41B2C271AB61127E7AE870491AE8053A1ED3706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2HeadersFrame
struct  HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Nullable`1<System.Byte> BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::PadLength
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	// System.Nullable`1<System.Byte> BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::IsExclusive
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___IsExclusive_2;
	// System.Nullable`1<System.UInt32> BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::StreamDependency
	Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C  ___StreamDependency_3;
	// System.Nullable`1<System.Byte> BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::Weight
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___Weight_4;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::HeaderBlockFragmentIdx
	uint32_t ___HeaderBlockFragmentIdx_5;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::HeaderBlockFragment
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___HeaderBlockFragment_6;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2HeadersFrame::HeaderBlockFragmentLength
	uint32_t ___HeaderBlockFragmentLength_7;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_PadLength_1() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___PadLength_1)); }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  get_PadLength_1() const { return ___PadLength_1; }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7 * get_address_of_PadLength_1() { return &___PadLength_1; }
	inline void set_PadLength_1(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  value)
	{
		___PadLength_1 = value;
	}

	inline static int32_t get_offset_of_IsExclusive_2() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___IsExclusive_2)); }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  get_IsExclusive_2() const { return ___IsExclusive_2; }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7 * get_address_of_IsExclusive_2() { return &___IsExclusive_2; }
	inline void set_IsExclusive_2(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  value)
	{
		___IsExclusive_2 = value;
	}

	inline static int32_t get_offset_of_StreamDependency_3() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___StreamDependency_3)); }
	inline Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C  get_StreamDependency_3() const { return ___StreamDependency_3; }
	inline Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C * get_address_of_StreamDependency_3() { return &___StreamDependency_3; }
	inline void set_StreamDependency_3(Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C  value)
	{
		___StreamDependency_3 = value;
	}

	inline static int32_t get_offset_of_Weight_4() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___Weight_4)); }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  get_Weight_4() const { return ___Weight_4; }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7 * get_address_of_Weight_4() { return &___Weight_4; }
	inline void set_Weight_4(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  value)
	{
		___Weight_4 = value;
	}

	inline static int32_t get_offset_of_HeaderBlockFragmentIdx_5() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___HeaderBlockFragmentIdx_5)); }
	inline uint32_t get_HeaderBlockFragmentIdx_5() const { return ___HeaderBlockFragmentIdx_5; }
	inline uint32_t* get_address_of_HeaderBlockFragmentIdx_5() { return &___HeaderBlockFragmentIdx_5; }
	inline void set_HeaderBlockFragmentIdx_5(uint32_t value)
	{
		___HeaderBlockFragmentIdx_5 = value;
	}

	inline static int32_t get_offset_of_HeaderBlockFragment_6() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___HeaderBlockFragment_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_HeaderBlockFragment_6() const { return ___HeaderBlockFragment_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_HeaderBlockFragment_6() { return &___HeaderBlockFragment_6; }
	inline void set_HeaderBlockFragment_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___HeaderBlockFragment_6 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderBlockFragment_6), value);
	}

	inline static int32_t get_offset_of_HeaderBlockFragmentLength_7() { return static_cast<int32_t>(offsetof(HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706, ___HeaderBlockFragmentLength_7)); }
	inline uint32_t get_HeaderBlockFragmentLength_7() const { return ___HeaderBlockFragmentLength_7; }
	inline uint32_t* get_address_of_HeaderBlockFragmentLength_7() { return &___HeaderBlockFragmentLength_7; }
	inline void set_HeaderBlockFragmentLength_7(uint32_t value)
	{
		___HeaderBlockFragmentLength_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2HeadersFrame
struct HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___IsExclusive_2;
	Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C  ___StreamDependency_3;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___Weight_4;
	uint32_t ___HeaderBlockFragmentIdx_5;
	uint8_t* ___HeaderBlockFragment_6;
	uint32_t ___HeaderBlockFragmentLength_7;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2HeadersFrame
struct HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___IsExclusive_2;
	Nullable_1_t93F4507FF6707E69354E128E51498F80A7C40E5C  ___StreamDependency_3;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___Weight_4;
	uint32_t ___HeaderBlockFragmentIdx_5;
	uint8_t* ___HeaderBlockFragment_6;
	uint32_t ___HeaderBlockFragmentLength_7;
};
#endif // HTTP2HEADERSFRAME_TA41B2C271AB61127E7AE870491AE8053A1ED3706_H
#ifndef HTTP2PINGFRAME_TEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451_H
#define HTTP2PINGFRAME_TEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2PingFrame
struct  HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2PingFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2PingFrame::OpaqueData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OpaqueData_1;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2PingFrame::OpaqueDataLength
	uint8_t ___OpaqueDataLength_2;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_OpaqueData_1() { return static_cast<int32_t>(offsetof(HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451, ___OpaqueData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OpaqueData_1() const { return ___OpaqueData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OpaqueData_1() { return &___OpaqueData_1; }
	inline void set_OpaqueData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OpaqueData_1 = value;
		Il2CppCodeGenWriteBarrier((&___OpaqueData_1), value);
	}

	inline static int32_t get_offset_of_OpaqueDataLength_2() { return static_cast<int32_t>(offsetof(HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451, ___OpaqueDataLength_2)); }
	inline uint8_t get_OpaqueDataLength_2() const { return ___OpaqueDataLength_2; }
	inline uint8_t* get_address_of_OpaqueDataLength_2() { return &___OpaqueDataLength_2; }
	inline void set_OpaqueDataLength_2(uint8_t value)
	{
		___OpaqueDataLength_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2PingFrame
struct HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	uint8_t* ___OpaqueData_1;
	uint8_t ___OpaqueDataLength_2;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2PingFrame
struct HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	uint8_t* ___OpaqueData_1;
	uint8_t ___OpaqueDataLength_2;
};
#endif // HTTP2PINGFRAME_TEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451_H
#ifndef HTTP2PRIORITYFRAME_T8B40DD189AFE90A57CDDA11811E0E7E31BCCC700_H
#define HTTP2PRIORITYFRAME_T8B40DD189AFE90A57CDDA11811E0E7E31BCCC700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2PriorityFrame
struct  HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2PriorityFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2PriorityFrame::IsExclusive
	uint8_t ___IsExclusive_1;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PriorityFrame::StreamDependency
	uint32_t ___StreamDependency_2;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2PriorityFrame::Weight
	uint8_t ___Weight_3;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_IsExclusive_1() { return static_cast<int32_t>(offsetof(HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700, ___IsExclusive_1)); }
	inline uint8_t get_IsExclusive_1() const { return ___IsExclusive_1; }
	inline uint8_t* get_address_of_IsExclusive_1() { return &___IsExclusive_1; }
	inline void set_IsExclusive_1(uint8_t value)
	{
		___IsExclusive_1 = value;
	}

	inline static int32_t get_offset_of_StreamDependency_2() { return static_cast<int32_t>(offsetof(HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700, ___StreamDependency_2)); }
	inline uint32_t get_StreamDependency_2() const { return ___StreamDependency_2; }
	inline uint32_t* get_address_of_StreamDependency_2() { return &___StreamDependency_2; }
	inline void set_StreamDependency_2(uint32_t value)
	{
		___StreamDependency_2 = value;
	}

	inline static int32_t get_offset_of_Weight_3() { return static_cast<int32_t>(offsetof(HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700, ___Weight_3)); }
	inline uint8_t get_Weight_3() const { return ___Weight_3; }
	inline uint8_t* get_address_of_Weight_3() { return &___Weight_3; }
	inline void set_Weight_3(uint8_t value)
	{
		___Weight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2PriorityFrame
struct HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	uint8_t ___IsExclusive_1;
	uint32_t ___StreamDependency_2;
	uint8_t ___Weight_3;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2PriorityFrame
struct HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	uint8_t ___IsExclusive_1;
	uint32_t ___StreamDependency_2;
	uint8_t ___Weight_3;
};
#endif // HTTP2PRIORITYFRAME_T8B40DD189AFE90A57CDDA11811E0E7E31BCCC700_H
#ifndef HTTP2PUSHPROMISEFRAME_T058298E3FF24E9762E2A247CA97524BA4422B616_H
#define HTTP2PUSHPROMISEFRAME_T058298E3FF24E9762E2A247CA97524BA4422B616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame
struct  HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Nullable`1<System.Byte> BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::PadLength
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::ReservedBit
	uint8_t ___ReservedBit_2;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::PromisedStreamId
	uint32_t ___PromisedStreamId_3;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::HeaderBlockFragmentIdx
	uint32_t ___HeaderBlockFragmentIdx_4;
	// System.Byte[] BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::HeaderBlockFragment
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___HeaderBlockFragment_5;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame::HeaderBlockFragmentLength
	uint32_t ___HeaderBlockFragmentLength_6;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_PadLength_1() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___PadLength_1)); }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  get_PadLength_1() const { return ___PadLength_1; }
	inline Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7 * get_address_of_PadLength_1() { return &___PadLength_1; }
	inline void set_PadLength_1(Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  value)
	{
		___PadLength_1 = value;
	}

	inline static int32_t get_offset_of_ReservedBit_2() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___ReservedBit_2)); }
	inline uint8_t get_ReservedBit_2() const { return ___ReservedBit_2; }
	inline uint8_t* get_address_of_ReservedBit_2() { return &___ReservedBit_2; }
	inline void set_ReservedBit_2(uint8_t value)
	{
		___ReservedBit_2 = value;
	}

	inline static int32_t get_offset_of_PromisedStreamId_3() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___PromisedStreamId_3)); }
	inline uint32_t get_PromisedStreamId_3() const { return ___PromisedStreamId_3; }
	inline uint32_t* get_address_of_PromisedStreamId_3() { return &___PromisedStreamId_3; }
	inline void set_PromisedStreamId_3(uint32_t value)
	{
		___PromisedStreamId_3 = value;
	}

	inline static int32_t get_offset_of_HeaderBlockFragmentIdx_4() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___HeaderBlockFragmentIdx_4)); }
	inline uint32_t get_HeaderBlockFragmentIdx_4() const { return ___HeaderBlockFragmentIdx_4; }
	inline uint32_t* get_address_of_HeaderBlockFragmentIdx_4() { return &___HeaderBlockFragmentIdx_4; }
	inline void set_HeaderBlockFragmentIdx_4(uint32_t value)
	{
		___HeaderBlockFragmentIdx_4 = value;
	}

	inline static int32_t get_offset_of_HeaderBlockFragment_5() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___HeaderBlockFragment_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_HeaderBlockFragment_5() const { return ___HeaderBlockFragment_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_HeaderBlockFragment_5() { return &___HeaderBlockFragment_5; }
	inline void set_HeaderBlockFragment_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___HeaderBlockFragment_5 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderBlockFragment_5), value);
	}

	inline static int32_t get_offset_of_HeaderBlockFragmentLength_6() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616, ___HeaderBlockFragmentLength_6)); }
	inline uint32_t get_HeaderBlockFragmentLength_6() const { return ___HeaderBlockFragmentLength_6; }
	inline uint32_t* get_address_of_HeaderBlockFragmentLength_6() { return &___HeaderBlockFragmentLength_6; }
	inline void set_HeaderBlockFragmentLength_6(uint32_t value)
	{
		___HeaderBlockFragmentLength_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame
struct HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	uint8_t ___ReservedBit_2;
	uint32_t ___PromisedStreamId_3;
	uint32_t ___HeaderBlockFragmentIdx_4;
	uint8_t* ___HeaderBlockFragment_5;
	uint32_t ___HeaderBlockFragmentLength_6;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2PushPromiseFrame
struct HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	Nullable_1_tBC4350ACF5D672E4DCC60FFA688C6BDB1602A7A7  ___PadLength_1;
	uint8_t ___ReservedBit_2;
	uint32_t ___PromisedStreamId_3;
	uint32_t ___HeaderBlockFragmentIdx_4;
	uint8_t* ___HeaderBlockFragment_5;
	uint32_t ___HeaderBlockFragmentLength_6;
};
#endif // HTTP2PUSHPROMISEFRAME_T058298E3FF24E9762E2A247CA97524BA4422B616_H
#ifndef HTTP2RSTSTREAMFRAME_TE31D38A37BD09DCFB0B0D609657C49956B45DB11_H
#define HTTP2RSTSTREAMFRAME_TE31D38A37BD09DCFB0B0D609657C49956B45DB11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2RSTStreamFrame
struct  HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2RSTStreamFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2RSTStreamFrame::ErrorCode
	uint32_t ___ErrorCode_1;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_ErrorCode_1() { return static_cast<int32_t>(offsetof(HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11, ___ErrorCode_1)); }
	inline uint32_t get_ErrorCode_1() const { return ___ErrorCode_1; }
	inline uint32_t* get_address_of_ErrorCode_1() { return &___ErrorCode_1; }
	inline void set_ErrorCode_1(uint32_t value)
	{
		___ErrorCode_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2RSTStreamFrame
struct HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	uint32_t ___ErrorCode_1;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2RSTStreamFrame
struct HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	uint32_t ___ErrorCode_1;
};
#endif // HTTP2RSTSTREAMFRAME_TE31D38A37BD09DCFB0B0D609657C49956B45DB11_H
#ifndef HTTP2SETTINGSFRAME_TA239D404B3C409C6D529BEDFD63BDBCF7C106954_H
#define HTTP2SETTINGSFRAME_TA239D404B3C409C6D529BEDFD63BDBCF7C106954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2SettingsFrame
struct  HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2SettingsFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<BestHTTP.Connections.HTTP2.HTTP2Settings,System.UInt32>> BestHTTP.Connections.HTTP2.HTTP2SettingsFrame::Settings
	List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F * ___Settings_1;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_Settings_1() { return static_cast<int32_t>(offsetof(HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954, ___Settings_1)); }
	inline List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F * get_Settings_1() const { return ___Settings_1; }
	inline List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F ** get_address_of_Settings_1() { return &___Settings_1; }
	inline void set_Settings_1(List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F * value)
	{
		___Settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___Settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2SettingsFrame
struct HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F * ___Settings_1;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2SettingsFrame
struct HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	List_1_t80AED2CEF6D7C71B3AB87C68EF7967C82129AF7F * ___Settings_1;
};
#endif // HTTP2SETTINGSFRAME_TA239D404B3C409C6D529BEDFD63BDBCF7C106954_H
#ifndef HTTP2WINDOWUPDATEFRAME_T4761558CB178BFEA16DDF820EEF0838687C79209_H
#define HTTP2WINDOWUPDATEFRAME_T4761558CB178BFEA16DDF820EEF0838687C79209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2WindowUpdateFrame
struct  HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209 
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload BestHTTP.Connections.HTTP2.HTTP2WindowUpdateFrame::Header
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  ___Header_0;
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2WindowUpdateFrame::ReservedBit
	uint8_t ___ReservedBit_1;
	// System.UInt32 BestHTTP.Connections.HTTP2.HTTP2WindowUpdateFrame::WindowSizeIncrement
	uint32_t ___WindowSizeIncrement_2;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209, ___Header_0)); }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  get_Header_0() const { return ___Header_0; }
	inline HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6 * get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6  value)
	{
		___Header_0 = value;
	}

	inline static int32_t get_offset_of_ReservedBit_1() { return static_cast<int32_t>(offsetof(HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209, ___ReservedBit_1)); }
	inline uint8_t get_ReservedBit_1() const { return ___ReservedBit_1; }
	inline uint8_t* get_address_of_ReservedBit_1() { return &___ReservedBit_1; }
	inline void set_ReservedBit_1(uint8_t value)
	{
		___ReservedBit_1 = value;
	}

	inline static int32_t get_offset_of_WindowSizeIncrement_2() { return static_cast<int32_t>(offsetof(HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209, ___WindowSizeIncrement_2)); }
	inline uint32_t get_WindowSizeIncrement_2() const { return ___WindowSizeIncrement_2; }
	inline uint32_t* get_address_of_WindowSizeIncrement_2() { return &___WindowSizeIncrement_2; }
	inline void set_WindowSizeIncrement_2(uint32_t value)
	{
		___WindowSizeIncrement_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Connections.HTTP2.HTTP2WindowUpdateFrame
struct HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209_marshaled_pinvoke
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke ___Header_0;
	uint8_t ___ReservedBit_1;
	uint32_t ___WindowSizeIncrement_2;
};
// Native definition for COM marshalling of BestHTTP.Connections.HTTP2.HTTP2WindowUpdateFrame
struct HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209_marshaled_com
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_com ___Header_0;
	uint8_t ___ReservedBit_1;
	uint32_t ___WindowSizeIncrement_2;
};
#endif // HTTP2WINDOWUPDATEFRAME_T4761558CB178BFEA16DDF820EEF0838687C79209_H
#ifndef HUBCONNECTION_TBC0BBEF230E3B51F7D537916AB544958E3771AE0_H
#define HUBCONNECTION_TBC0BBEF230E3B51F7D537916AB544958E3771AE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.HubConnection
struct  HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.SignalRCore.HubConnection::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_1;
	// BestHTTP.SignalRCore.ConnectionStates BestHTTP.SignalRCore.HubConnection::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_2;
	// BestHTTP.SignalRCore.ITransport BestHTTP.SignalRCore.HubConnection::<Transport>k__BackingField
	RuntimeObject* ___U3CTransportU3Ek__BackingField_3;
	// BestHTTP.SignalRCore.IProtocol BestHTTP.SignalRCore.HubConnection::<Protocol>k__BackingField
	RuntimeObject* ___U3CProtocolU3Ek__BackingField_4;
	// System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri> BestHTTP.SignalRCore.HubConnection::OnRedirected
	Action_3_t9E65FFC85A28FD135EFD112A772153B36D31E88A * ___OnRedirected_5;
	// System.Action`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::OnConnected
	Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * ___OnConnected_6;
	// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String> BestHTTP.SignalRCore.HubConnection::OnError
	Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 * ___OnError_7;
	// System.Action`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::OnClosed
	Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * ___OnClosed_8;
	// System.Func`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.Messages.Message,System.Boolean> BestHTTP.SignalRCore.HubConnection::OnMessage
	Func_3_tF116A057DB6F20BD34CFC1B7A1CDFD8809C6FDCD * ___OnMessage_9;
	// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String> BestHTTP.SignalRCore.HubConnection::OnReconnecting
	Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 * ___OnReconnecting_10;
	// System.Action`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::OnReconnected
	Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * ___OnReconnected_11;
	// System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents> BestHTTP.SignalRCore.HubConnection::OnTransportEvent
	Action_3_t2832E599EEEF3D8A44915D97BE5C8FBAE64EEDBD * ___OnTransportEvent_12;
	// BestHTTP.SignalRCore.IAuthenticationProvider BestHTTP.SignalRCore.HubConnection::<AuthenticationProvider>k__BackingField
	RuntimeObject* ___U3CAuthenticationProviderU3Ek__BackingField_13;
	// BestHTTP.SignalRCore.Messages.NegotiationResult BestHTTP.SignalRCore.HubConnection::<NegotiationResult>k__BackingField
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B * ___U3CNegotiationResultU3Ek__BackingField_14;
	// BestHTTP.SignalRCore.HubOptions BestHTTP.SignalRCore.HubConnection::<Options>k__BackingField
	HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458 * ___U3COptionsU3Ek__BackingField_15;
	// System.Int32 BestHTTP.SignalRCore.HubConnection::<RedirectCount>k__BackingField
	int32_t ___U3CRedirectCountU3Ek__BackingField_16;
	// BestHTTP.SignalRCore.IRetryPolicy BestHTTP.SignalRCore.HubConnection::<ReconnectPolicy>k__BackingField
	RuntimeObject* ___U3CReconnectPolicyU3Ek__BackingField_17;
	// System.Int64 BestHTTP.SignalRCore.HubConnection::lastInvocationId
	int64_t ___lastInvocationId_18;
	// System.Int32 BestHTTP.SignalRCore.HubConnection::lastStreamId
	int32_t ___lastStreamId_19;
	// System.Collections.Generic.Dictionary`2<System.Int64,System.Action`1<BestHTTP.SignalRCore.Messages.Message>> BestHTTP.SignalRCore.HubConnection::invocations
	Dictionary_2_t2ADC9CF37AE5CE341D4CFCF3991D3EF77ABB40EA * ___invocations_20;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SignalRCore.Subscription> BestHTTP.SignalRCore.HubConnection::subscriptions
	Dictionary_2_t7AFA59AAA480E7CFC1DB40F6BD09F67BFD324AE6 * ___subscriptions_21;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::lastMessageSent
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastMessageSent_22;
	// BestHTTP.SignalRCore.RetryContext BestHTTP.SignalRCore.HubConnection::currentContext
	RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2  ___currentContext_23;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::reconnectStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___reconnectStartTime_24;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::reconnectAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___reconnectAt_25;
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.TransportTypes> BestHTTP.SignalRCore.HubConnection::triedoutTransports
	List_1_tF36DB673FE6C6CD0C8E3148DFE3661FBD2C32574 * ___triedoutTransports_26;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CUriU3Ek__BackingField_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_1() const { return ___U3CUriU3Ek__BackingField_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_1() { return &___U3CUriU3Ek__BackingField_1; }
	inline void set_U3CUriU3Ek__BackingField_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CTransportU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CTransportU3Ek__BackingField_3() const { return ___U3CTransportU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CTransportU3Ek__BackingField_3() { return &___U3CTransportU3Ek__BackingField_3; }
	inline void set_U3CTransportU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CTransportU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTransportU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CProtocolU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CProtocolU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CProtocolU3Ek__BackingField_4() const { return ___U3CProtocolU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CProtocolU3Ek__BackingField_4() { return &___U3CProtocolU3Ek__BackingField_4; }
	inline void set_U3CProtocolU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CProtocolU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_OnRedirected_5() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnRedirected_5)); }
	inline Action_3_t9E65FFC85A28FD135EFD112A772153B36D31E88A * get_OnRedirected_5() const { return ___OnRedirected_5; }
	inline Action_3_t9E65FFC85A28FD135EFD112A772153B36D31E88A ** get_address_of_OnRedirected_5() { return &___OnRedirected_5; }
	inline void set_OnRedirected_5(Action_3_t9E65FFC85A28FD135EFD112A772153B36D31E88A * value)
	{
		___OnRedirected_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnRedirected_5), value);
	}

	inline static int32_t get_offset_of_OnConnected_6() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnConnected_6)); }
	inline Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * get_OnConnected_6() const { return ___OnConnected_6; }
	inline Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A ** get_address_of_OnConnected_6() { return &___OnConnected_6; }
	inline void set_OnConnected_6(Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * value)
	{
		___OnConnected_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnConnected_6), value);
	}

	inline static int32_t get_offset_of_OnError_7() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnError_7)); }
	inline Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 * get_OnError_7() const { return ___OnError_7; }
	inline Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 ** get_address_of_OnError_7() { return &___OnError_7; }
	inline void set_OnError_7(Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 * value)
	{
		___OnError_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_7), value);
	}

	inline static int32_t get_offset_of_OnClosed_8() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnClosed_8)); }
	inline Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * get_OnClosed_8() const { return ___OnClosed_8; }
	inline Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A ** get_address_of_OnClosed_8() { return &___OnClosed_8; }
	inline void set_OnClosed_8(Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * value)
	{
		___OnClosed_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_8), value);
	}

	inline static int32_t get_offset_of_OnMessage_9() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnMessage_9)); }
	inline Func_3_tF116A057DB6F20BD34CFC1B7A1CDFD8809C6FDCD * get_OnMessage_9() const { return ___OnMessage_9; }
	inline Func_3_tF116A057DB6F20BD34CFC1B7A1CDFD8809C6FDCD ** get_address_of_OnMessage_9() { return &___OnMessage_9; }
	inline void set_OnMessage_9(Func_3_tF116A057DB6F20BD34CFC1B7A1CDFD8809C6FDCD * value)
	{
		___OnMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessage_9), value);
	}

	inline static int32_t get_offset_of_OnReconnecting_10() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnReconnecting_10)); }
	inline Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 * get_OnReconnecting_10() const { return ___OnReconnecting_10; }
	inline Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 ** get_address_of_OnReconnecting_10() { return &___OnReconnecting_10; }
	inline void set_OnReconnecting_10(Action_2_t2529390C8F4B40E710241C24EBB1869D12ADDEE8 * value)
	{
		___OnReconnecting_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnReconnecting_10), value);
	}

	inline static int32_t get_offset_of_OnReconnected_11() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnReconnected_11)); }
	inline Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * get_OnReconnected_11() const { return ___OnReconnected_11; }
	inline Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A ** get_address_of_OnReconnected_11() { return &___OnReconnected_11; }
	inline void set_OnReconnected_11(Action_1_t46854FC1CA67608FE67982DD58F47AB6605D834A * value)
	{
		___OnReconnected_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnReconnected_11), value);
	}

	inline static int32_t get_offset_of_OnTransportEvent_12() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___OnTransportEvent_12)); }
	inline Action_3_t2832E599EEEF3D8A44915D97BE5C8FBAE64EEDBD * get_OnTransportEvent_12() const { return ___OnTransportEvent_12; }
	inline Action_3_t2832E599EEEF3D8A44915D97BE5C8FBAE64EEDBD ** get_address_of_OnTransportEvent_12() { return &___OnTransportEvent_12; }
	inline void set_OnTransportEvent_12(Action_3_t2832E599EEEF3D8A44915D97BE5C8FBAE64EEDBD * value)
	{
		___OnTransportEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnTransportEvent_12), value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CAuthenticationProviderU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CAuthenticationProviderU3Ek__BackingField_13() const { return ___U3CAuthenticationProviderU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CAuthenticationProviderU3Ek__BackingField_13() { return &___U3CAuthenticationProviderU3Ek__BackingField_13; }
	inline void set_U3CAuthenticationProviderU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CAuthenticationProviderU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticationProviderU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CNegotiationResultU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CNegotiationResultU3Ek__BackingField_14)); }
	inline NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B * get_U3CNegotiationResultU3Ek__BackingField_14() const { return ___U3CNegotiationResultU3Ek__BackingField_14; }
	inline NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B ** get_address_of_U3CNegotiationResultU3Ek__BackingField_14() { return &___U3CNegotiationResultU3Ek__BackingField_14; }
	inline void set_U3CNegotiationResultU3Ek__BackingField_14(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B * value)
	{
		___U3CNegotiationResultU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNegotiationResultU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3COptionsU3Ek__BackingField_15)); }
	inline HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458 * get_U3COptionsU3Ek__BackingField_15() const { return ___U3COptionsU3Ek__BackingField_15; }
	inline HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458 ** get_address_of_U3COptionsU3Ek__BackingField_15() { return &___U3COptionsU3Ek__BackingField_15; }
	inline void set_U3COptionsU3Ek__BackingField_15(HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458 * value)
	{
		___U3COptionsU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CRedirectCountU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CRedirectCountU3Ek__BackingField_16)); }
	inline int32_t get_U3CRedirectCountU3Ek__BackingField_16() const { return ___U3CRedirectCountU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CRedirectCountU3Ek__BackingField_16() { return &___U3CRedirectCountU3Ek__BackingField_16; }
	inline void set_U3CRedirectCountU3Ek__BackingField_16(int32_t value)
	{
		___U3CRedirectCountU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectPolicyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___U3CReconnectPolicyU3Ek__BackingField_17)); }
	inline RuntimeObject* get_U3CReconnectPolicyU3Ek__BackingField_17() const { return ___U3CReconnectPolicyU3Ek__BackingField_17; }
	inline RuntimeObject** get_address_of_U3CReconnectPolicyU3Ek__BackingField_17() { return &___U3CReconnectPolicyU3Ek__BackingField_17; }
	inline void set_U3CReconnectPolicyU3Ek__BackingField_17(RuntimeObject* value)
	{
		___U3CReconnectPolicyU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReconnectPolicyU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_lastInvocationId_18() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___lastInvocationId_18)); }
	inline int64_t get_lastInvocationId_18() const { return ___lastInvocationId_18; }
	inline int64_t* get_address_of_lastInvocationId_18() { return &___lastInvocationId_18; }
	inline void set_lastInvocationId_18(int64_t value)
	{
		___lastInvocationId_18 = value;
	}

	inline static int32_t get_offset_of_lastStreamId_19() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___lastStreamId_19)); }
	inline int32_t get_lastStreamId_19() const { return ___lastStreamId_19; }
	inline int32_t* get_address_of_lastStreamId_19() { return &___lastStreamId_19; }
	inline void set_lastStreamId_19(int32_t value)
	{
		___lastStreamId_19 = value;
	}

	inline static int32_t get_offset_of_invocations_20() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___invocations_20)); }
	inline Dictionary_2_t2ADC9CF37AE5CE341D4CFCF3991D3EF77ABB40EA * get_invocations_20() const { return ___invocations_20; }
	inline Dictionary_2_t2ADC9CF37AE5CE341D4CFCF3991D3EF77ABB40EA ** get_address_of_invocations_20() { return &___invocations_20; }
	inline void set_invocations_20(Dictionary_2_t2ADC9CF37AE5CE341D4CFCF3991D3EF77ABB40EA * value)
	{
		___invocations_20 = value;
		Il2CppCodeGenWriteBarrier((&___invocations_20), value);
	}

	inline static int32_t get_offset_of_subscriptions_21() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___subscriptions_21)); }
	inline Dictionary_2_t7AFA59AAA480E7CFC1DB40F6BD09F67BFD324AE6 * get_subscriptions_21() const { return ___subscriptions_21; }
	inline Dictionary_2_t7AFA59AAA480E7CFC1DB40F6BD09F67BFD324AE6 ** get_address_of_subscriptions_21() { return &___subscriptions_21; }
	inline void set_subscriptions_21(Dictionary_2_t7AFA59AAA480E7CFC1DB40F6BD09F67BFD324AE6 * value)
	{
		___subscriptions_21 = value;
		Il2CppCodeGenWriteBarrier((&___subscriptions_21), value);
	}

	inline static int32_t get_offset_of_lastMessageSent_22() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___lastMessageSent_22)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastMessageSent_22() const { return ___lastMessageSent_22; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastMessageSent_22() { return &___lastMessageSent_22; }
	inline void set_lastMessageSent_22(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastMessageSent_22 = value;
	}

	inline static int32_t get_offset_of_currentContext_23() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___currentContext_23)); }
	inline RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2  get_currentContext_23() const { return ___currentContext_23; }
	inline RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2 * get_address_of_currentContext_23() { return &___currentContext_23; }
	inline void set_currentContext_23(RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2  value)
	{
		___currentContext_23 = value;
	}

	inline static int32_t get_offset_of_reconnectStartTime_24() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___reconnectStartTime_24)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_reconnectStartTime_24() const { return ___reconnectStartTime_24; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_reconnectStartTime_24() { return &___reconnectStartTime_24; }
	inline void set_reconnectStartTime_24(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___reconnectStartTime_24 = value;
	}

	inline static int32_t get_offset_of_reconnectAt_25() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___reconnectAt_25)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_reconnectAt_25() const { return ___reconnectAt_25; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_reconnectAt_25() { return &___reconnectAt_25; }
	inline void set_reconnectAt_25(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___reconnectAt_25 = value;
	}

	inline static int32_t get_offset_of_triedoutTransports_26() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0, ___triedoutTransports_26)); }
	inline List_1_tF36DB673FE6C6CD0C8E3148DFE3661FBD2C32574 * get_triedoutTransports_26() const { return ___triedoutTransports_26; }
	inline List_1_tF36DB673FE6C6CD0C8E3148DFE3661FBD2C32574 ** get_address_of_triedoutTransports_26() { return &___triedoutTransports_26; }
	inline void set_triedoutTransports_26(List_1_tF36DB673FE6C6CD0C8E3148DFE3661FBD2C32574 * value)
	{
		___triedoutTransports_26 = value;
		Il2CppCodeGenWriteBarrier((&___triedoutTransports_26), value);
	}
};

struct HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0_StaticFields
{
public:
	// System.Object[] BestHTTP.SignalRCore.HubConnection::EmptyArgs
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___EmptyArgs_0;

public:
	inline static int32_t get_offset_of_EmptyArgs_0() { return static_cast<int32_t>(offsetof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0_StaticFields, ___EmptyArgs_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_EmptyArgs_0() const { return ___EmptyArgs_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_EmptyArgs_0() { return &___EmptyArgs_0; }
	inline void set_EmptyArgs_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___EmptyArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArgs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUBCONNECTION_TBC0BBEF230E3B51F7D537916AB544958E3771AE0_H
#ifndef ONAUTHENTICATIONFAILEDDELEGATE_T6B861C5E3BFE161A494E605848645655F51C9BF8_H
#define ONAUTHENTICATIONFAILEDDELEGATE_T6B861C5E3BFE161A494E605848645655F51C9BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate
struct  OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONAUTHENTICATIONFAILEDDELEGATE_T6B861C5E3BFE161A494E605848645655F51C9BF8_H
#ifndef ONAUTHENTICATIONSUCCEDEDDELEGATE_TEA42FDE767C73D79D089586F7D35643CD23D5333_H
#define ONAUTHENTICATIONSUCCEDEDDELEGATE_TEA42FDE767C73D79D089586F7D35643CD23D5333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate
struct  OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONAUTHENTICATIONSUCCEDEDDELEGATE_TEA42FDE767C73D79D089586F7D35643CD23D5333_H
#ifndef LONGPOLLINGTRANSPORT_T894E422C7137922F41BD8BD0879C7F1EBB638200_H
#define LONGPOLLINGTRANSPORT_T894E422C7137922F41BD8BD0879C7F1EBB638200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Transports.LongPollingTransport
struct  LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200  : public TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E
{
public:
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.PlatformSupport.Memory.BufferSegment> BestHTTP.SignalRCore.Transports.LongPollingTransport::outgoingMessages
	ConcurrentQueue_1_t88A1AB5B72716BCA269B9ED8E2E19D550729581F * ___outgoingMessages_7;
	// System.Int32 BestHTTP.SignalRCore.Transports.LongPollingTransport::sendingInProgress
	int32_t ___sendingInProgress_8;
	// BestHTTP.Extensions.BufferSegmentStream BestHTTP.SignalRCore.Transports.LongPollingTransport::stream
	BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD * ___stream_9;

public:
	inline static int32_t get_offset_of_outgoingMessages_7() { return static_cast<int32_t>(offsetof(LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200, ___outgoingMessages_7)); }
	inline ConcurrentQueue_1_t88A1AB5B72716BCA269B9ED8E2E19D550729581F * get_outgoingMessages_7() const { return ___outgoingMessages_7; }
	inline ConcurrentQueue_1_t88A1AB5B72716BCA269B9ED8E2E19D550729581F ** get_address_of_outgoingMessages_7() { return &___outgoingMessages_7; }
	inline void set_outgoingMessages_7(ConcurrentQueue_1_t88A1AB5B72716BCA269B9ED8E2E19D550729581F * value)
	{
		___outgoingMessages_7 = value;
		Il2CppCodeGenWriteBarrier((&___outgoingMessages_7), value);
	}

	inline static int32_t get_offset_of_sendingInProgress_8() { return static_cast<int32_t>(offsetof(LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200, ___sendingInProgress_8)); }
	inline int32_t get_sendingInProgress_8() const { return ___sendingInProgress_8; }
	inline int32_t* get_address_of_sendingInProgress_8() { return &___sendingInProgress_8; }
	inline void set_sendingInProgress_8(int32_t value)
	{
		___sendingInProgress_8 = value;
	}

	inline static int32_t get_offset_of_stream_9() { return static_cast<int32_t>(offsetof(LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200, ___stream_9)); }
	inline BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD * get_stream_9() const { return ___stream_9; }
	inline BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD ** get_address_of_stream_9() { return &___stream_9; }
	inline void set_stream_9(BufferSegmentStream_t6700EF58E2487E58AD20C3AB39746822031822DD * value)
	{
		___stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___stream_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPOLLINGTRANSPORT_T894E422C7137922F41BD8BD0879C7F1EBB638200_H
#ifndef SOCKETIOACKCALLBACK_TF19743C46B63B95B0C77BE7B24290C414F5D2E87_H
#define SOCKETIOACKCALLBACK_TF19743C46B63B95B0C77BE7B24290C414F5D2E87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Events.SocketIOAckCallback
struct  SocketIOAckCallback_tF19743C46B63B95B0C77BE7B24290C414F5D2E87  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETIOACKCALLBACK_TF19743C46B63B95B0C77BE7B24290C414F5D2E87_H
#ifndef SOCKETIOCALLBACK_T90E2FD268FCCB480BBDACFCD4B73425A47818B81_H
#define SOCKETIOCALLBACK_T90E2FD268FCCB480BBDACFCD4B73425A47818B81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SocketIO.Events.SocketIOCallback
struct  SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETIOCALLBACK_T90E2FD268FCCB480BBDACFCD4B73425A47818B81_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5800 = { sizeof (HTTP2ContinuationFlags_t888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5800[3] = 
{
	HTTP2ContinuationFlags_t888D830D7BBCFD28EC9E0C5F4DB4C24E6B130C14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5801 = { sizeof (HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6)+ sizeof (RuntimeObject), sizeof(HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5801[7] = 
{
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_PayloadLength_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_Flags_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_StreamId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_Payload_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_PayloadOffset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2FrameHeaderAndPayload_t00C90549534F75E3FCB383BA13C3E6A3A47803B6::get_offset_of_DontUseMemPool_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5802 = { sizeof (HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5802[2] = 
{
	HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2SettingsFrame_tA239D404B3C409C6D529BEDFD63BDBCF7C106954::get_offset_of_Settings_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5803 = { sizeof (HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5803[5] = 
{
	HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422::get_offset_of_PadLength_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422::get_offset_of_DataIdx_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422::get_offset_of_Data_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2DataFrame_tEC2983D8AEF409BE4F2A15546C5A0E18F402D422::get_offset_of_DataLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5804 = { sizeof (HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5804[8] = 
{
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_PadLength_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_IsExclusive_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_StreamDependency_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_Weight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_HeaderBlockFragmentIdx_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_HeaderBlockFragment_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2HeadersFrame_tA41B2C271AB61127E7AE870491AE8053A1ED3706::get_offset_of_HeaderBlockFragmentLength_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5805 = { sizeof (HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700)+ sizeof (RuntimeObject), sizeof(HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5805[4] = 
{
	HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700::get_offset_of_IsExclusive_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700::get_offset_of_StreamDependency_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PriorityFrame_t8B40DD189AFE90A57CDDA11811E0E7E31BCCC700::get_offset_of_Weight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5806 = { sizeof (HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11)+ sizeof (RuntimeObject), sizeof(HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5806[2] = 
{
	HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2RSTStreamFrame_tE31D38A37BD09DCFB0B0D609657C49956B45DB11::get_offset_of_ErrorCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5807 = { sizeof (HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5807[7] = 
{
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_PadLength_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_ReservedBit_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_PromisedStreamId_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_HeaderBlockFragmentIdx_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_HeaderBlockFragment_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PushPromiseFrame_t058298E3FF24E9762E2A247CA97524BA4422B616::get_offset_of_HeaderBlockFragmentLength_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5808 = { sizeof (HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451)+ sizeof (RuntimeObject), sizeof(HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5808[3] = 
{
	HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451::get_offset_of_OpaqueData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2PingFrame_tEE9B93FCDE61A69ABDEB3C2A8F0BDF678CB10451::get_offset_of_OpaqueDataLength_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5809 = { sizeof (HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD)+ sizeof (RuntimeObject), sizeof(HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5809[6] = 
{
	HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD::get_offset_of_ReservedBit_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD::get_offset_of_LastStreamId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD::get_offset_of_ErrorCode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD::get_offset_of_AdditionalDebugData_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2GoAwayFrame_t3BBF69C99EA74EB7458A23980776E86CD58DECBD::get_offset_of_AdditionalDebugDataLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5810 = { sizeof (HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209)+ sizeof (RuntimeObject), sizeof(HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5810[3] = 
{
	HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209::get_offset_of_ReservedBit_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2WindowUpdateFrame_t4761558CB178BFEA16DDF820EEF0838687C79209::get_offset_of_WindowSizeIncrement_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5811 = { sizeof (HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A)+ sizeof (RuntimeObject), sizeof(HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5811[2] = 
{
	HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2ContinuationFrame_tE3F847723987F55AA05EAFF75241367D7057C93A::get_offset_of_HeaderBlockFragment_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5812 = { sizeof (HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749)+ sizeof (RuntimeObject), sizeof(HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5812[3] = 
{
	HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749::get_offset_of_Header_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749::get_offset_of_Origin_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HTTP2AltSVCFrame_t60B37F8E2BF8CA921A0EDC8BBA0C4AB44D69F749::get_offset_of_AltSvcFieldValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5813 = { sizeof (HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846), -1, sizeof(HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5813[22] = 
{
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846_StaticFields::get_offset_of_MAGIC_0(),
	0,
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_U3CLatencyU3Ek__BackingField_2(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_lastPingSent_3(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_pingFrequency_4(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846_StaticFields::get_offset_of_RTTBufferCapacity_5(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_rtts_6(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_isThreadsStarted_7(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_isRunning_8(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_newFrameSignal_9(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_requestQueue_10(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_clientInitiatedStreams_11(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_HPACKEncoder_12(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_newFrames_13(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_outgoingFrames_14(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_settings_15(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_remoteWindow_16(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_lastInteraction_17(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_goAwaySentAt_18(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_conn_19(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_threadExitCount_20(),
	HTTP2Handler_t687724372BD4C276BA8177D6838340726B708846::get_offset_of_U3CShutdownTypeU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5814 = { sizeof (U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4), -1, sizeof(U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5814[2] = 
{
	U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0B57B13CF88F89C0175DB484E429C854D81C74D4_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5815 = { sizeof (HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5815[8] = 
{
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_HeaderTableSize_0(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_MaxConcurrentStreams_1(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_InitialStreamWindowSize_2(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_InitialConnectionWindowSize_3(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_MaxFrameSize_4(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_MaxHeaderListSize_5(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_MaxIdleTime_6(),
	HTTP2PluginSettings_t6E2972409DBFEF2F6CE748A1A9F120A52498EE63::get_offset_of_PingFrequency_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5816 = { sizeof (HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5816[4] = 
{
	HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE::get_offset_of_U3CExpectedContentLengthU3Ek__BackingField_25(),
	HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE::get_offset_of_U3CIsCompressedU3Ek__BackingField_26(),
	HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE::get_offset_of_isPrepared_27(),
	HTTP2Response_t945A20F0DCE1C5EB4CBF6678729C0BD00A2BC9BE::get_offset_of_decompressor_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5817 = { sizeof (HTTP2Settings_tE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5817[7] = 
{
	HTTP2Settings_tE90AD683D98106D2F2B5468076EE5DD4AFA1C0F4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5818 = { sizeof (HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5818[5] = 
{
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7::get_offset_of_U3CIsReadOnlyU3Ek__BackingField_0(),
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7::get_offset_of_OnSettingChangedEvent_1(),
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7::get_offset_of_values_2(),
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7::get_offset_of_changeFlags_3(),
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7::get_offset_of_U3CIsChangedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5819 = { sizeof (HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1), -1, sizeof(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5819[5] = 
{
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1_StaticFields::get_offset_of_SettingsCount_0(),
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1::get_offset_of_U3CMySettingsU3Ek__BackingField_1(),
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1::get_offset_of_U3CInitiatedMySettingsU3Ek__BackingField_2(),
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1::get_offset_of_U3CRemoteSettingsU3Ek__BackingField_3(),
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1::get_offset_of_U3CSettingsChangesSentAtU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5820 = { sizeof (HTTP2StreamStates_t6D51897ED29F3396AE28175A80B69EFC95405CDE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5820[6] = 
{
	HTTP2StreamStates_t6D51897ED29F3396AE28175A80B69EFC95405CDE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5821 = { sizeof (HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF), -1, sizeof(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5821[19] = 
{
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF_StaticFields::get_offset_of_LastStreamId_0(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_U3CIdU3Ek__BackingField_1(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of__state_2(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_U3CAssignedRequestU3Ek__BackingField_3(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_isStreamedDownload_4(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_downloaded_5(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_bodyToSend_6(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_bodyToSendOffset_7(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_settings_8(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_encoder_9(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_outgoing_10(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_incomingFrames_11(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_currentFramesView_12(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_localWindow_13(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_remoteWindow_14(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_windowUpdateThreshold_15(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_sentData_16(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_isRSTFrameSent_17(),
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF::get_offset_of_response_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5822 = { sizeof (HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611), -1, sizeof(HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5822[3] = 
{
	0,
	HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611_StaticFields::get_offset_of_StaticTable_1(),
	HuffmanEncoder_tD12C454A95D6965729BE74C6D44A008D1BF86611_StaticFields::get_offset_of_HuffmanTree_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5823 = { sizeof (TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF)+ sizeof (RuntimeObject), sizeof(TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF ), 0, 0 };
extern const int32_t g_FieldOffsetTable5823[2] = 
{
	TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF::get_offset_of_Code_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TableEntry_t4ED64D4777BC3DDF941F19AC0269A9CD7F4355FF::get_offset_of_Bits_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5824 = { sizeof (TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3)+ sizeof (RuntimeObject), sizeof(TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5824[3] = 
{
	TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3::get_offset_of_NextZeroIdx_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3::get_offset_of_NextOneIdx_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TreeNode_t8B033BB2C8AD6721A4F346F34833134A600D4AB3::get_offset_of_Value_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5825 = { sizeof (HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5825[13] = 
{
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CUriU3Ek__BackingField_0(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CLastAccessU3Ek__BackingField_1(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CBodyLengthU3Ek__BackingField_2(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CETagU3Ek__BackingField_3(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CLastModifiedU3Ek__BackingField_4(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CAgeU3Ek__BackingField_6(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CMaxAgeU3Ek__BackingField_7(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CDateU3Ek__BackingField_8(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CMustRevalidateU3Ek__BackingField_9(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CReceivedU3Ek__BackingField_10(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CConstructedPathU3Ek__BackingField_11(),
	HTTPCacheFileInfo_t47ADD76D6FCBE8C2640ADB6532577203ACC21A3D::get_offset_of_U3CMappedNameIDXU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5826 = { sizeof (HTTPCacheMaintananceParams_t8B2271A5ACE741730682A51E9A9A4091DCF1A047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5826[2] = 
{
	HTTPCacheMaintananceParams_t8B2271A5ACE741730682A51E9A9A4091DCF1A047::get_offset_of_U3CDeleteOlderU3Ek__BackingField_0(),
	HTTPCacheMaintananceParams_t8B2271A5ACE741730682A51E9A9A4091DCF1A047::get_offset_of_U3CMaxCacheSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5827 = { sizeof (UriComparer_t70A570490E418B242A3EC3C8184A269672408F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5828 = { sizeof (HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461), -1, sizeof(HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5828[11] = 
{
	0,
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_isSupported_1(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_IsSupportCheckDone_2(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_library_3(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_rwLock_4(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_UsedIndexes_5(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_U3CCacheFolderU3Ek__BackingField_6(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_U3CLibraryPathU3Ek__BackingField_7(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_InClearThread_8(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_InMaintainenceThread_9(),
	HTTPCacheService_tE27EEBB1F659D5314249E31CD5086F8713454461_StaticFields::get_offset_of_NextNameIDX_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5829 = { sizeof (U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F), -1, sizeof(U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5829[3] = 
{
	U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields::get_offset_of_U3CU3E9__32_0_1(),
	U3CU3Ec_tD1D4542DB29646843CA82E03D8A886AD27AEE46F_StaticFields::get_offset_of_U3CU3E9__32_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5830 = { sizeof (AuthenticationTypes_t68B01D8972AAC6FA5506A0D64014031442F6754F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5830[4] = 
{
	AuthenticationTypes_t68B01D8972AAC6FA5506A0D64014031442F6754F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5831 = { sizeof (Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5831[3] = 
{
	Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78::get_offset_of_U3CUserNameU3Ek__BackingField_1(),
	Credentials_t3D6400EA93AC11D8F88F75B6135864C46CD8BB78::get_offset_of_U3CPasswordU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5832 = { sizeof (Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5832[11] = 
{
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CUriU3Ek__BackingField_0(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CRealmU3Ek__BackingField_2(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CStaleU3Ek__BackingField_3(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CNonceU3Ek__BackingField_4(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3COpaqueU3Ek__BackingField_5(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CAlgorithmU3Ek__BackingField_6(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CProtectedUrisU3Ek__BackingField_7(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CQualityOfProtectionsU3Ek__BackingField_8(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CNonceCountU3Ek__BackingField_9(),
	Digest_tA5E41B88A3B435AC408A918248D6176185A38A6D::get_offset_of_U3CHA1SessU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5833 = { sizeof (DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0), -1, sizeof(DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5833[3] = 
{
	DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields::get_offset_of_Digests_0(),
	DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields::get_offset_of_rwLock_1(),
	DigestStore_tD5E7E3051DF26A15CA74F2686685D7F7931261C0_StaticFields::get_offset_of_SupportedAlgorithms_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5834 = { sizeof (U3CU3Ec__DisplayClass6_0_tA04FD0F40FD2047C724B7245C972DC1C8E4D99FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5834[1] = 
{
	U3CU3Ec__DisplayClass6_0_tA04FD0F40FD2047C724B7245C972DC1C8E4D99FD::get_offset_of_i_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5835 = { sizeof (TransportEventTypes_t1BF9D523FF224030531EBC5C8A43879ACC28642F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5835[9] = 
{
	TransportEventTypes_t1BF9D523FF224030531EBC5C8A43879ACC28642F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5836 = { sizeof (SocketIOEventTypes_tAC846076D43C0805F1B05A636B02BADA2B1FE26A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5836[9] = 
{
	SocketIOEventTypes_tAC846076D43C0805F1B05A636B02BADA2B1FE26A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5837 = { sizeof (SocketIOErrors_t3AAD8AC7186EBE750D529DA3FD23E77FE25E0579)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5837[9] = 
{
	SocketIOErrors_t3AAD8AC7186EBE750D529DA3FD23E77FE25E0579::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5838 = { sizeof (Error_tE44CD06EF17517E90824CE5DBE0E9C13F5817DD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5838[2] = 
{
	Error_tE44CD06EF17517E90824CE5DBE0E9C13F5817DD0::get_offset_of_U3CCodeU3Ek__BackingField_0(),
	Error_tE44CD06EF17517E90824CE5DBE0E9C13F5817DD0::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5839 = { sizeof (HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5839[4] = 
{
	HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B::get_offset_of_U3CSidU3Ek__BackingField_0(),
	HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B::get_offset_of_U3CUpgradesU3Ek__BackingField_1(),
	HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B::get_offset_of_U3CPingIntervalU3Ek__BackingField_2(),
	HandshakeData_t47B487FAE84B9C8264577C5490AE69424BB3694B::get_offset_of_U3CPingTimeoutU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5841 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5842 = { sizeof (Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5842[11] = 
{
	0,
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CTransportEventU3Ek__BackingField_1(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CSocketIOEventU3Ek__BackingField_2(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CAttachmentCountU3Ek__BackingField_3(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CIdU3Ek__BackingField_4(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CNamespaceU3Ek__BackingField_5(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CPayloadU3Ek__BackingField_6(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CEventNameU3Ek__BackingField_7(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_attachments_8(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CIsDecodedU3Ek__BackingField_9(),
	Packet_tAB5AE8A6DAEAC3850BD884D98FD064F87F7C391B::get_offset_of_U3CDecodedArgsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5843 = { sizeof (PayloadTypes_t6536D7156E34641F8CF357C6087A035F341F4B68)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5843[3] = 
{
	PayloadTypes_t6536D7156E34641F8CF357C6087A035F341F4B68::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5844 = { sizeof (Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5844[8] = 
{
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_U3CManagerU3Ek__BackingField_0(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_U3CNamespaceU3Ek__BackingField_1(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_U3CIdU3Ek__BackingField_2(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_U3CIsOpenU3Ek__BackingField_3(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_4(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_AckCallbacks_5(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_EventCallbacks_6(),
	Socket_tAA79128CAF3EFD5DC71B032189ADDCCF3F56BDB1::get_offset_of_arguments_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5845 = { sizeof (SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322), -1, sizeof(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5845[21] = 
{
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322_StaticFields::get_offset_of_DefaultEncoder_0(),
	0,
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_state_2(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3COptionsU3Ek__BackingField_3(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CUriU3Ek__BackingField_4(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CHandshakeU3Ek__BackingField_5(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CTransportU3Ek__BackingField_6(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CRequestCounterU3Ek__BackingField_7(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CReconnectAttemptsU3Ek__BackingField_8(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CEncoderU3Ek__BackingField_9(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_nextAckId_10(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CPreviousStateU3Ek__BackingField_11(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_U3CUpgradingTransportU3Ek__BackingField_12(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_Namespaces_13(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_Sockets_14(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_OfflinePackets_15(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_LastHeartbeat_16(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_ReconnectAt_17(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_ConnectionStarted_18(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_closing_19(),
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322::get_offset_of_IsWaitingPong_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5846 = { sizeof (States_t779BD738F612179D3FF3407A1A8B06D565BF5F23)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5846[7] = 
{
	States_t779BD738F612179D3FF3407A1A8B06D565BF5F23::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5847 = { sizeof (SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5847[11] = 
{
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CConnectWithU3Ek__BackingField_0(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CReconnectionU3Ek__BackingField_1(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_2(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CReconnectionDelayU3Ek__BackingField_3(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CReconnectionDelayMaxU3Ek__BackingField_4(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_randomizationFactor_5(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CTimeoutU3Ek__BackingField_6(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CAutoConnectU3Ek__BackingField_7(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_additionalQueryParams_8(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_9(),
	SocketOptions_tB72716A15B56E075578905181A1659A85AAAB228::get_offset_of_BuiltQueryParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5848 = { sizeof (TransportTypes_t396DD398362609B9A346607C3E06C532A2B9F100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5848[3] = 
{
	TransportTypes_t396DD398362609B9A346607C3E06C532A2B9F100::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5849 = { sizeof (TransportStates_tF990329133AB20D4128C755417FF2AD931C5DD56)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5849[6] = 
{
	TransportStates_tF990329133AB20D4128C755417FF2AD931C5DD56::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5851 = { sizeof (PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5851[6] = 
{
	PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9::get_offset_of_U3CStateU3Ek__BackingField_0(),
	PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9::get_offset_of_U3CManagerU3Ek__BackingField_1(),
	PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9::get_offset_of_LastRequest_2(),
	PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9::get_offset_of_PollRequest_3(),
	PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9::get_offset_of_PacketWithAttachment_4(),
	PollingTransport_tEEE2D9FAE52C6A1FCDFE500B99D1C9CF9A5120D9::get_offset_of_lonelyPacketList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5852 = { sizeof (PayloadTypes_t74B79C4FC24732633B7D82C90FFD62E5052F6D9A)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5852[3] = 
{
	PayloadTypes_t74B79C4FC24732633B7D82C90FFD62E5052F6D9A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5853 = { sizeof (WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5853[5] = 
{
	WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C::get_offset_of_U3CStateU3Ek__BackingField_0(),
	WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C::get_offset_of_U3CManagerU3Ek__BackingField_1(),
	WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C::get_offset_of_U3CImplementationU3Ek__BackingField_2(),
	WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C::get_offset_of_PacketWithAttachment_3(),
	WebSocketTransport_t172705C4D15FA5B498A95DC03197FC25B759F44C::get_offset_of_Buffer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5854 = { sizeof (SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5855 = { sizeof (SocketIOAckCallback_tF19743C46B63B95B0C77BE7B24290C414F5D2E87), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5856 = { sizeof (EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5856[4] = 
{
	EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2::get_offset_of_U3CCallbacksU3Ek__BackingField_0(),
	EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2::get_offset_of_U3COnlyOnceU3Ek__BackingField_1(),
	EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2::get_offset_of_U3CAutoDecodePayloadU3Ek__BackingField_2(),
	EventDescriptor_tC46E7837EBF07087525A7F1C7FE162BA921885B2::get_offset_of_CallbackArray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5857 = { sizeof (EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861), -1, sizeof(EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5857[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields::get_offset_of_SocketIONames_7(),
	EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields::get_offset_of_TransportNames_8(),
	EventNames_t9772245AFC17A8E11D57918312B10753AC7D7861_StaticFields::get_offset_of_BlacklistedEvents_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5858 = { sizeof (EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5858[2] = 
{
	EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F::get_offset_of_U3CSocketU3Ek__BackingField_0(),
	EventTable_t89D3B094ECFC84485A9DC1509690A130356ECB8F::get_offset_of_Table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5859 = { sizeof (U3CU3Ec__DisplayClass6_0_t13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5859[2] = 
{
	U3CU3Ec__DisplayClass6_0_t13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8::get_offset_of_onlyOnce_0(),
	U3CU3Ec__DisplayClass6_0_t13CA27B333DCE0E44F02BCC941E8D9A606CAEEE8::get_offset_of_autoDecodePayload_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5860 = { sizeof (LitJsonEncoder_t765B1C61CF89738149398AA193741ABE337F60E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5861 = { sizeof (DefaultJSonEncoder_t7F42AC52EAEA6AF5C859579445C2FFBCD173E85F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5862 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5863 = { sizeof (HubConnectionExtensions_tCC6FEFA722402BE7DD02215B6BF42FF566214B31), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5864 = { sizeof (UploadItemControllerExtensions_tA32388807D27315414494327CA79D39F5FA0D3E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5865 = { sizeof (TransportTypes_tCA87C50DC625A9ED1115309688D187F9C628396B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5865[3] = 
{
	TransportTypes_tCA87C50DC625A9ED1115309688D187F9C628396B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5866 = { sizeof (TransferModes_t6A8657C1EAC4109096A1BB541E3F77C4A937CB34)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5866[3] = 
{
	TransferModes_t6A8657C1EAC4109096A1BB541E3F77C4A937CB34::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5867 = { sizeof (TransportStates_tC5546B62EF1DD9E9220FEC265E5D626E6856A06E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5867[7] = 
{
	TransportStates_tC5546B62EF1DD9E9220FEC265E5D626E6856A06E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5868 = { sizeof (ConnectionStates_tF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5868[9] = 
{
	ConnectionStates_tF1ED01C037597C4F7DCD46E49AFA0C5804FC69D0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5869 = { sizeof (TransportEvents_tE721CE38C312660C7E23857550A5BDB598EB5A98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5869[6] = 
{
	TransportEvents_tE721CE38C312660C7E23857550A5BDB598EB5A98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5872 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5872[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5873 = { sizeof (CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5873[2] = 
{
	CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE::get_offset_of_ParamTypes_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CallbackDescriptor_t6C67B7EC8DDD49C7236E4850164791B251D986EE::get_offset_of_Callback_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5874 = { sizeof (Subscription_tB5D06FD88B601F3A83353605DAA91FC4ECB0158E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5874[2] = 
{
	Subscription_tB5D06FD88B601F3A83353605DAA91FC4ECB0158E::get_offset_of_callbacks_0(),
	Subscription_tB5D06FD88B601F3A83353605DAA91FC4ECB0158E::get_offset_of_lockSlim_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5875 = { sizeof (HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5875[4] = 
{
	HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458::get_offset_of_U3CSkipNegotiationU3Ek__BackingField_0(),
	HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458::get_offset_of_U3CPreferedTransportU3Ek__BackingField_1(),
	HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458::get_offset_of_U3CPingIntervalU3Ek__BackingField_2(),
	HubOptions_tCF020F4BD272719B187050645A2370E46E9AF458::get_offset_of_U3CMaxRedirectsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5876 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5877 = { sizeof (RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2)+ sizeof (RuntimeObject), sizeof(RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5877[3] = 
{
	RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2::get_offset_of_PreviousRetryCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2::get_offset_of_ElapsedTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RetryContext_tA533507D4EFCF4312516B1589F945530D90BB2E2::get_offset_of_RetryReason_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5878 = { sizeof (DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A), -1, sizeof(DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5878[2] = 
{
	DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A_StaticFields::get_offset_of_DefaultBackoffTimes_0(),
	DefaultRetryPolicy_t8D890270E46A09EBED2A8351FD9D578863E9763A::get_offset_of_backoffTimes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5879 = { sizeof (HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0), -1, sizeof(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5879[27] = 
{
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0_StaticFields::get_offset_of_EmptyArgs_0(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CUriU3Ek__BackingField_1(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CStateU3Ek__BackingField_2(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CTransportU3Ek__BackingField_3(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CProtocolU3Ek__BackingField_4(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnRedirected_5(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnConnected_6(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnError_7(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnClosed_8(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnMessage_9(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnReconnecting_10(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnReconnected_11(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_OnTransportEvent_12(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_13(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CNegotiationResultU3Ek__BackingField_14(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3COptionsU3Ek__BackingField_15(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CRedirectCountU3Ek__BackingField_16(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_U3CReconnectPolicyU3Ek__BackingField_17(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_lastInvocationId_18(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_lastStreamId_19(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_invocations_20(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_subscriptions_21(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_lastMessageSent_22(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_currentContext_23(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_reconnectStartTime_24(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_reconnectAt_25(),
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0::get_offset_of_triedoutTransports_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5880 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5880[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5881 = { sizeof (U3CU3Ec__DisplayClass81_0_t388C24614AFACC9BC9527C572218EB94A20D2BED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5881[1] = 
{
	U3CU3Ec__DisplayClass81_0_t388C24614AFACC9BC9527C572218EB94A20D2BED::get_offset_of_future_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5882 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5882[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5883 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5883[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5884 = { sizeof (U3CU3Ec__DisplayClass86_0_t2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5884[1] = 
{
	U3CU3Ec__DisplayClass86_0_t2C495C3C4643C33BA0458CEB6809C8B0DB4C95FD::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5885 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5885[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5886 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5886[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5887 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5887[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5888 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5888[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5889 = { sizeof (OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5890 = { sizeof (OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5891 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5892 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5893 = { sizeof (JsonProtocol_tC91D79DEB95133DAC9A2D01382F286AB846F9A13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5893[3] = 
{
	0,
	JsonProtocol_tC91D79DEB95133DAC9A2D01382F286AB846F9A13::get_offset_of_U3CEncoderU3Ek__BackingField_1(),
	JsonProtocol_tC91D79DEB95133DAC9A2D01382F286AB846F9A13::get_offset_of_U3CConnectionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5894 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5895 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5895[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5896 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5896[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5897 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5897[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5898 = { sizeof (LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5898[4] = 
{
	0,
	LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200::get_offset_of_outgoingMessages_7(),
	LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200::get_offset_of_sendingInProgress_8(),
	LongPollingTransport_t894E422C7137922F41BD8BD0879C7F1EBB638200::get_offset_of_stream_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5899 = { sizeof (TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5899[6] = 
{
	TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E::get_offset_of__state_0(),
	TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E::get_offset_of_U3CErrorReasonU3Ek__BackingField_1(),
	TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E::get_offset_of_OnStateChanged_2(),
	TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E::get_offset_of_messages_3(),
	TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E::get_offset_of_connection_4(),
	TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E::get_offset_of_queryBuilder_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
