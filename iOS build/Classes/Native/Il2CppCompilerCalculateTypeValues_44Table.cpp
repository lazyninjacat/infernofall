﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber
struct IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure
struct TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure[]
struct X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper
struct CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine
struct ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine
struct IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_t5E47CAAC0C7B909FEE19C9C4D4342AFC8B15F719;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBasicAgreement
struct IBasicAgreement_tE49895CA0590A50A2F09F9B68F2927DCC10742B1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_t391586A9268E9DABF6C6158169C448C4243FA87C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest
struct IDigest_t4779036D052ECF08BAC3DF71ED71EF04D7866C09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IKeyWrapper
struct IKeyWrapper_t04E00864E3E09ADE25D8EA669A75F58DC5C7CA50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IStreamCipher
struct IStreamCipher_t76B7033CC7AD20CF07EA49A55A57FFD726E22467;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher
struct IAeadBlockCipher_tB707B139ECFEF6D78BD117C85A84E2F2BD106B84;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadCipher
struct IAeadCipher_t119FD2327023DE16994955371BCF0C004CE23968;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_t3F0A2BB86CE5BD61188A9989250C19FDFF733F50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue
struct ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate
struct Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipherFactory
struct TlsCipherFactory_t0FC331767EE833BF93E6B80F483626F0819BFBB5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContext
struct TlsClientContext_tAE2C092193457C8B9EAEE44A2CCA3EAF262B251F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCloseable
struct TlsCloseable_tE72C78755E8271CE1D8F5ACA0F26F5C14D30D61C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_t31D810C946EFADA4C750C42CAD416485053A047D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContext
struct TlsServerContext_t40456A024D5F5C32225B02951995C4D8E01E1430;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession
struct TlsSession_t572BDC5B5EC5BC5CFD4950D91A4EB16A421845E1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef KEYTRANSRECIPIENTINFOGENERATOR_T2A9824564CB74998707741E748634E314C7CF453_H
#define KEYTRANSRECIPIENTINFOGENERATOR_T2A9824564CB74998707741E748634E314C7CF453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator
struct  KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.TbsCertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::recipientTbsCert
	TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * ___recipientTbsCert_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::recipientPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___recipientPublicKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::subjectKeyIdentifier
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___subjectKeyIdentifier_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::info
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___info_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::issuerAndSerialNumber
	IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * ___issuerAndSerialNumber_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_6;

public:
	inline static int32_t get_offset_of_recipientTbsCert_1() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___recipientTbsCert_1)); }
	inline TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * get_recipientTbsCert_1() const { return ___recipientTbsCert_1; }
	inline TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 ** get_address_of_recipientTbsCert_1() { return &___recipientTbsCert_1; }
	inline void set_recipientTbsCert_1(TbsCertificateStructure_t3C3DF26D84C0CFC3BCDB6E36C6F4DDA97067CD39 * value)
	{
		___recipientTbsCert_1 = value;
		Il2CppCodeGenWriteBarrier((&___recipientTbsCert_1), value);
	}

	inline static int32_t get_offset_of_recipientPublicKey_2() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___recipientPublicKey_2)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_recipientPublicKey_2() const { return ___recipientPublicKey_2; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_recipientPublicKey_2() { return &___recipientPublicKey_2; }
	inline void set_recipientPublicKey_2(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___recipientPublicKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___recipientPublicKey_2), value);
	}

	inline static int32_t get_offset_of_subjectKeyIdentifier_3() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___subjectKeyIdentifier_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_subjectKeyIdentifier_3() const { return ___subjectKeyIdentifier_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_subjectKeyIdentifier_3() { return &___subjectKeyIdentifier_3; }
	inline void set_subjectKeyIdentifier_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___subjectKeyIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___subjectKeyIdentifier_3), value);
	}

	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___info_4)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_info_4() const { return ___info_4; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier((&___info_4), value);
	}

	inline static int32_t get_offset_of_issuerAndSerialNumber_5() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___issuerAndSerialNumber_5)); }
	inline IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * get_issuerAndSerialNumber_5() const { return ___issuerAndSerialNumber_5; }
	inline IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC ** get_address_of_issuerAndSerialNumber_5() { return &___issuerAndSerialNumber_5; }
	inline void set_issuerAndSerialNumber_5(IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * value)
	{
		___issuerAndSerialNumber_5 = value;
		Il2CppCodeGenWriteBarrier((&___issuerAndSerialNumber_5), value);
	}

	inline static int32_t get_offset_of_random_6() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453, ___random_6)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_6() const { return ___random_6; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_6() { return &___random_6; }
	inline void set_random_6(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_6 = value;
		Il2CppCodeGenWriteBarrier((&___random_6), value);
	}
};

struct KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.KeyTransRecipientInfoGenerator::Helper
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453_StaticFields, ___Helper_0)); }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * get_Helper_0() const { return ___Helper_0; }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYTRANSRECIPIENTINFOGENERATOR_T2A9824564CB74998707741E748634E314C7CF453_H
#ifndef ASYMMETRICCIPHERKEYPAIR_T4630D28256FA535784C97BBD4D410AB0DC19FC24_H
#define ASYMMETRICCIPHERKEYPAIR_T4630D28256FA535784C97BBD4D410AB0DC19FC24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair
struct  AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair::publicParameter
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___publicParameter_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair::privateParameter
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___privateParameter_1;

public:
	inline static int32_t get_offset_of_publicParameter_0() { return static_cast<int32_t>(offsetof(AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24, ___publicParameter_0)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_publicParameter_0() const { return ___publicParameter_0; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_publicParameter_0() { return &___publicParameter_0; }
	inline void set_publicParameter_0(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___publicParameter_0 = value;
		Il2CppCodeGenWriteBarrier((&___publicParameter_0), value);
	}

	inline static int32_t get_offset_of_privateParameter_1() { return static_cast<int32_t>(offsetof(AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24, ___privateParameter_1)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_privateParameter_1() const { return ___privateParameter_1; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_privateParameter_1() { return &___privateParameter_1; }
	inline void set_privateParameter_1(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___privateParameter_1 = value;
		Il2CppCodeGenWriteBarrier((&___privateParameter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICCIPHERKEYPAIR_T4630D28256FA535784C97BBD4D410AB0DC19FC24_H
#ifndef ASYMMETRICKEYPARAMETER_T4F2CA810DA69334DB5E985540B64958EE26DF316_H
#define ASYMMETRICKEYPARAMETER_T4F2CA810DA69334DB5E985540B64958EE26DF316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct  AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter::privateKey
	bool ___privateKey_0;

public:
	inline static int32_t get_offset_of_privateKey_0() { return static_cast<int32_t>(offsetof(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316, ___privateKey_0)); }
	inline bool get_privateKey_0() const { return ___privateKey_0; }
	inline bool* get_address_of_privateKey_0() { return &___privateKey_0; }
	inline void set_privateKey_0(bool value)
	{
		___privateKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICKEYPARAMETER_T4F2CA810DA69334DB5E985540B64958EE26DF316_H
#ifndef BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#define BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedCipherBase
struct  BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F  : public RuntimeObject
{
public:

public:
};

struct BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedCipherBase::EmptyBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBuffer_0;

public:
	inline static int32_t get_offset_of_EmptyBuffer_0() { return static_cast<int32_t>(offsetof(BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields, ___EmptyBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBuffer_0() const { return ___EmptyBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBuffer_0() { return &___EmptyBuffer_0; }
	inline void set_EmptyBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#ifndef CHECK_T288CF9AB9B725A839BDB9C23FBD55EA6E9712821_H
#define CHECK_T288CF9AB9B725A839BDB9C23FBD55EA6E9712821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Check
struct  Check_t288CF9AB9B725A839BDB9C23FBD55EA6E9712821  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T288CF9AB9B725A839BDB9C23FBD55EA6E9712821_H
#ifndef CIPHERKEYGENERATOR_T5159EB217F5FC7C4B39BB4EA981381BCC3689BBB_H
#define CIPHERKEYGENERATOR_T5159EB217F5FC7C4B39BB4EA981381BCC3689BBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator
struct  CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::strength
	int32_t ___strength_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::uninitialised
	bool ___uninitialised_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CipherKeyGenerator::defaultStrength
	int32_t ___defaultStrength_3;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___strength_1)); }
	inline int32_t get_strength_1() const { return ___strength_1; }
	inline int32_t* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(int32_t value)
	{
		___strength_1 = value;
	}

	inline static int32_t get_offset_of_uninitialised_2() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___uninitialised_2)); }
	inline bool get_uninitialised_2() const { return ___uninitialised_2; }
	inline bool* get_address_of_uninitialised_2() { return &___uninitialised_2; }
	inline void set_uninitialised_2(bool value)
	{
		___uninitialised_2 = value;
	}

	inline static int32_t get_offset_of_defaultStrength_3() { return static_cast<int32_t>(offsetof(CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB, ___defaultStrength_3)); }
	inline int32_t get_defaultStrength_3() const { return ___defaultStrength_3; }
	inline int32_t* get_address_of_defaultStrength_3() { return &___defaultStrength_3; }
	inline void set_defaultStrength_3(int32_t value)
	{
		___defaultStrength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERKEYGENERATOR_T5159EB217F5FC7C4B39BB4EA981381BCC3689BBB_H
#ifndef KEYGENERATIONPARAMETERS_TC65EF43115EA8E516AD7410DCF741410A28744DC_H
#define KEYGENERATIONPARAMETERS_TC65EF43115EA8E516AD7410DCF741410A28744DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.KeyGenerationParameters
struct  KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.KeyGenerationParameters::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.KeyGenerationParameters::strength
	int32_t ___strength_1;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC, ___strength_1)); }
	inline int32_t get_strength_1() const { return ___strength_1; }
	inline int32_t* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(int32_t value)
	{
		___strength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATIONPARAMETERS_TC65EF43115EA8E516AD7410DCF741410A28744DC_H
#ifndef PBEPARAMETERSGENERATOR_T8393B43188C7630FB3E775E3120C5DE0A232437E_H
#define PBEPARAMETERSGENERATOR_T8393B43188C7630FB3E775E3120C5DE0A232437E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator
struct  PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator::mPassword
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPassword_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator::mSalt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSalt_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.PbeParametersGenerator::mIterationCount
	int32_t ___mIterationCount_2;

public:
	inline static int32_t get_offset_of_mPassword_0() { return static_cast<int32_t>(offsetof(PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E, ___mPassword_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPassword_0() const { return ___mPassword_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPassword_0() { return &___mPassword_0; }
	inline void set_mPassword_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPassword_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPassword_0), value);
	}

	inline static int32_t get_offset_of_mSalt_1() { return static_cast<int32_t>(offsetof(PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E, ___mSalt_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSalt_1() const { return ___mSalt_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSalt_1() { return &___mSalt_1; }
	inline void set_mSalt_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSalt_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSalt_1), value);
	}

	inline static int32_t get_offset_of_mIterationCount_2() { return static_cast<int32_t>(offsetof(PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E, ___mIterationCount_2)); }
	inline int32_t get_mIterationCount_2() const { return ___mIterationCount_2; }
	inline int32_t* get_address_of_mIterationCount_2() { return &___mIterationCount_2; }
	inline void set_mIterationCount_2(int32_t value)
	{
		___mIterationCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBEPARAMETERSGENERATOR_T8393B43188C7630FB3E775E3120C5DE0A232437E_H
#ifndef SIMPLEBLOCKRESULT_TC5C259D1E128A1CE1F90F6C22B72F05254A5E285_H
#define SIMPLEBLOCKRESULT_TC5C259D1E128A1CE1F90F6C22B72F05254A5E285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.SimpleBlockResult
struct  SimpleBlockResult_tC5C259D1E128A1CE1F90F6C22B72F05254A5E285  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.SimpleBlockResult::result
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(SimpleBlockResult_tC5C259D1E128A1CE1F90F6C22B72F05254A5E285, ___result_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_result_0() const { return ___result_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEBLOCKRESULT_TC5C259D1E128A1CE1F90F6C22B72F05254A5E285_H
#ifndef STREAMBLOCKCIPHER_TF61480B98D7C235D792A1E3EB513101801C1A2A5_H
#define STREAMBLOCKCIPHER_TF61480B98D7C235D792A1E3EB513101801C1A2A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.StreamBlockCipher
struct  StreamBlockCipher_tF61480B98D7C235D792A1E3EB513101801C1A2A5  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.StreamBlockCipher::cipher
	RuntimeObject* ___cipher_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.StreamBlockCipher::oneByte
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___oneByte_1;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(StreamBlockCipher_tF61480B98D7C235D792A1E3EB513101801C1A2A5, ___cipher_0)); }
	inline RuntimeObject* get_cipher_0() const { return ___cipher_0; }
	inline RuntimeObject** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(RuntimeObject* value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_oneByte_1() { return static_cast<int32_t>(offsetof(StreamBlockCipher_tF61480B98D7C235D792A1E3EB513101801C1A2A5, ___oneByte_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_oneByte_1() const { return ___oneByte_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_oneByte_1() { return &___oneByte_1; }
	inline void set_oneByte_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___oneByte_1 = value;
		Il2CppCodeGenWriteBarrier((&___oneByte_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMBLOCKCIPHER_TF61480B98D7C235D792A1E3EB513101801C1A2A5_H
#ifndef ABSTRACTTLSCIPHERFACTORY_TF28F1FD8C7DB102AC9A2C8841DD2664508DAAAC3_H
#define ABSTRACTTLSCIPHERFACTORY_TF28F1FD8C7DB102AC9A2C8841DD2664508DAAAC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsCipherFactory
struct  AbstractTlsCipherFactory_tF28F1FD8C7DB102AC9A2C8841DD2664508DAAAC3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCIPHERFACTORY_TF28F1FD8C7DB102AC9A2C8841DD2664508DAAAC3_H
#ifndef ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#define ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext
struct  AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mNonceRandom
	RuntimeObject* ___mNonceRandom_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecurityParameters
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * ___mSecurityParameters_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mClientVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mClientVersion_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mServerVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mServerVersion_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSession
	RuntimeObject* ___mSession_6;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mUserObject
	RuntimeObject * ___mUserObject_7;

public:
	inline static int32_t get_offset_of_mNonceRandom_1() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mNonceRandom_1)); }
	inline RuntimeObject* get_mNonceRandom_1() const { return ___mNonceRandom_1; }
	inline RuntimeObject** get_address_of_mNonceRandom_1() { return &___mNonceRandom_1; }
	inline void set_mNonceRandom_1(RuntimeObject* value)
	{
		___mNonceRandom_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonceRandom_1), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_2() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSecureRandom_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_2() const { return ___mSecureRandom_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_2() { return &___mSecureRandom_2; }
	inline void set_mSecureRandom_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_2), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_3() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSecurityParameters_3)); }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * get_mSecurityParameters_3() const { return ___mSecurityParameters_3; }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA ** get_address_of_mSecurityParameters_3() { return &___mSecurityParameters_3; }
	inline void set_mSecurityParameters_3(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * value)
	{
		___mSecurityParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_3), value);
	}

	inline static int32_t get_offset_of_mClientVersion_4() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mClientVersion_4)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mClientVersion_4() const { return ___mClientVersion_4; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mClientVersion_4() { return &___mClientVersion_4; }
	inline void set_mClientVersion_4(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mClientVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_4), value);
	}

	inline static int32_t get_offset_of_mServerVersion_5() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mServerVersion_5)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mServerVersion_5() const { return ___mServerVersion_5; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mServerVersion_5() { return &___mServerVersion_5; }
	inline void set_mServerVersion_5(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mServerVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_5), value);
	}

	inline static int32_t get_offset_of_mSession_6() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSession_6)); }
	inline RuntimeObject* get_mSession_6() const { return ___mSession_6; }
	inline RuntimeObject** get_address_of_mSession_6() { return &___mSession_6; }
	inline void set_mSession_6(RuntimeObject* value)
	{
		___mSession_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSession_6), value);
	}

	inline static int32_t get_offset_of_mUserObject_7() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mUserObject_7)); }
	inline RuntimeObject * get_mUserObject_7() const { return ___mUserObject_7; }
	inline RuntimeObject ** get_address_of_mUserObject_7() { return &___mUserObject_7; }
	inline void set_mUserObject_7(RuntimeObject * value)
	{
		___mUserObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___mUserObject_7), value);
	}
};

struct AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::counter
	int64_t ___counter_0;

public:
	inline static int32_t get_offset_of_counter_0() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields, ___counter_0)); }
	inline int64_t get_counter_0() const { return ___counter_0; }
	inline int64_t* get_address_of_counter_0() { return &___counter_0; }
	inline void set_counter_0(int64_t value)
	{
		___counter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#ifndef ABSTRACTTLSCREDENTIALS_TBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C_H
#define ABSTRACTTLSCREDENTIALS_TBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsCredentials
struct  AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCREDENTIALS_TBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C_H
#ifndef ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#define ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange
struct  AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mKeyExchange
	int32_t ___mKeyExchange_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mContext
	RuntimeObject* ___mContext_2;

public:
	inline static int32_t get_offset_of_mKeyExchange_0() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mKeyExchange_0)); }
	inline int32_t get_mKeyExchange_0() const { return ___mKeyExchange_0; }
	inline int32_t* get_address_of_mKeyExchange_0() { return &___mKeyExchange_0; }
	inline void set_mKeyExchange_0(int32_t value)
	{
		___mKeyExchange_0 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#ifndef ABSTRACTTLSPEER_T4CADCC4F7D80D3BA66C0422339E38604CBEC9638_H
#define ABSTRACTTLSPEER_T4CADCC4F7D80D3BA66C0422339E38604CBEC9638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer
struct  AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCloseable modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer::mCloseHandle
	RuntimeObject* ___mCloseHandle_0;

public:
	inline static int32_t get_offset_of_mCloseHandle_0() { return static_cast<int32_t>(offsetof(AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638, ___mCloseHandle_0)); }
	inline RuntimeObject* get_mCloseHandle_0() const { return ___mCloseHandle_0; }
	inline RuntimeObject** get_address_of_mCloseHandle_0() { return &___mCloseHandle_0; }
	inline void set_mCloseHandle_0(RuntimeObject* value)
	{
		___mCloseHandle_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCloseHandle_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSPEER_T4CADCC4F7D80D3BA66C0422339E38604CBEC9638_H
#ifndef ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#define ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner
struct  AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner::mContext
	RuntimeObject* ___mContext_0;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#ifndef ALERTDESCRIPTION_T7DDB92B1E429BADA2427235268D1081FAE555A78_H
#define ALERTDESCRIPTION_T7DDB92B1E429BADA2427235268D1081FAE555A78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AlertDescription
struct  AlertDescription_t7DDB92B1E429BADA2427235268D1081FAE555A78  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDESCRIPTION_T7DDB92B1E429BADA2427235268D1081FAE555A78_H
#ifndef ALERTLEVEL_T5F8F5FC0B6AA32540D189B555581E68F1D11282C_H
#define ALERTLEVEL_T5F8F5FC0B6AA32540D189B555581E68F1D11282C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AlertLevel
struct  AlertLevel_t5F8F5FC0B6AA32540D189B555581E68F1D11282C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTLEVEL_T5F8F5FC0B6AA32540D189B555581E68F1D11282C_H
#ifndef ALWAYSVALIDVERIFYER_T000A0A6E4C2EEF06CBE9AD1837FB3E49B4743D19_H
#define ALWAYSVALIDVERIFYER_T000A0A6E4C2EEF06CBE9AD1837FB3E49B4743D19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AlwaysValidVerifyer
struct  AlwaysValidVerifyer_t000A0A6E4C2EEF06CBE9AD1837FB3E49B4743D19  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALWAYSVALIDVERIFYER_T000A0A6E4C2EEF06CBE9AD1837FB3E49B4743D19_H
#ifndef BASICTLSPSKIDENTITY_T322D6EF9036FF976DAD780F9347FE36C10CBD74E_H
#define BASICTLSPSKIDENTITY_T322D6EF9036FF976DAD780F9347FE36C10CBD74E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.BasicTlsPskIdentity
struct  BasicTlsPskIdentity_t322D6EF9036FF976DAD780F9347FE36C10CBD74E  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.BasicTlsPskIdentity::mIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mIdentity_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.BasicTlsPskIdentity::mPsk
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPsk_1;

public:
	inline static int32_t get_offset_of_mIdentity_0() { return static_cast<int32_t>(offsetof(BasicTlsPskIdentity_t322D6EF9036FF976DAD780F9347FE36C10CBD74E, ___mIdentity_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mIdentity_0() const { return ___mIdentity_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mIdentity_0() { return &___mIdentity_0; }
	inline void set_mIdentity_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mIdentity_0 = value;
		Il2CppCodeGenWriteBarrier((&___mIdentity_0), value);
	}

	inline static int32_t get_offset_of_mPsk_1() { return static_cast<int32_t>(offsetof(BasicTlsPskIdentity_t322D6EF9036FF976DAD780F9347FE36C10CBD74E, ___mPsk_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPsk_1() const { return ___mPsk_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPsk_1() { return &___mPsk_1; }
	inline void set_mPsk_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPsk_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPsk_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTLSPSKIDENTITY_T322D6EF9036FF976DAD780F9347FE36C10CBD74E_H
#ifndef BULKCIPHERALGORITHM_T113F78FC73DBA7DCCAA8DDF3E32591D7F1DDFD55_H
#define BULKCIPHERALGORITHM_T113F78FC73DBA7DCCAA8DDF3E32591D7F1DDFD55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.BulkCipherAlgorithm
struct  BulkCipherAlgorithm_t113F78FC73DBA7DCCAA8DDF3E32591D7F1DDFD55  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULKCIPHERALGORITHM_T113F78FC73DBA7DCCAA8DDF3E32591D7F1DDFD55_H
#ifndef BYTEQUEUE_T6B86227D30D30F5B463C2A08778E38DE7F30FF65_H
#define BYTEQUEUE_T6B86227D30D30F5B463C2A08778E38DE7F30FF65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue
struct  ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue::databuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___databuf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue::skipped
	int32_t ___skipped_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue::available
	int32_t ___available_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue::readOnlyBuf
	bool ___readOnlyBuf_4;

public:
	inline static int32_t get_offset_of_databuf_1() { return static_cast<int32_t>(offsetof(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65, ___databuf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_databuf_1() const { return ___databuf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_databuf_1() { return &___databuf_1; }
	inline void set_databuf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___databuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___databuf_1), value);
	}

	inline static int32_t get_offset_of_skipped_2() { return static_cast<int32_t>(offsetof(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65, ___skipped_2)); }
	inline int32_t get_skipped_2() const { return ___skipped_2; }
	inline int32_t* get_address_of_skipped_2() { return &___skipped_2; }
	inline void set_skipped_2(int32_t value)
	{
		___skipped_2 = value;
	}

	inline static int32_t get_offset_of_available_3() { return static_cast<int32_t>(offsetof(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65, ___available_3)); }
	inline int32_t get_available_3() const { return ___available_3; }
	inline int32_t* get_address_of_available_3() { return &___available_3; }
	inline void set_available_3(int32_t value)
	{
		___available_3 = value;
	}

	inline static int32_t get_offset_of_readOnlyBuf_4() { return static_cast<int32_t>(offsetof(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65, ___readOnlyBuf_4)); }
	inline bool get_readOnlyBuf_4() const { return ___readOnlyBuf_4; }
	inline bool* get_address_of_readOnlyBuf_4() { return &___readOnlyBuf_4; }
	inline void set_readOnlyBuf_4(bool value)
	{
		___readOnlyBuf_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEQUEUE_T6B86227D30D30F5B463C2A08778E38DE7F30FF65_H
#ifndef CERTCHAINTYPE_T539793ADC109544565B558738F85F4D22DB9245E_H
#define CERTCHAINTYPE_T539793ADC109544565B558738F85F4D22DB9245E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertChainType
struct  CertChainType_t539793ADC109544565B558738F85F4D22DB9245E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTCHAINTYPE_T539793ADC109544565B558738F85F4D22DB9245E_H
#ifndef CERTIFICATE_TA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_H
#define CERTIFICATE_TA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate
struct  Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate::mCertificateList
	X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637* ___mCertificateList_1;

public:
	inline static int32_t get_offset_of_mCertificateList_1() { return static_cast<int32_t>(offsetof(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3, ___mCertificateList_1)); }
	inline X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637* get_mCertificateList_1() const { return ___mCertificateList_1; }
	inline X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637** get_address_of_mCertificateList_1() { return &___mCertificateList_1; }
	inline void set_mCertificateList_1(X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637* value)
	{
		___mCertificateList_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateList_1), value);
	}
};

struct Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate::EmptyChain
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___EmptyChain_0;

public:
	inline static int32_t get_offset_of_EmptyChain_0() { return static_cast<int32_t>(offsetof(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_StaticFields, ___EmptyChain_0)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_EmptyChain_0() const { return ___EmptyChain_0; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_EmptyChain_0() { return &___EmptyChain_0; }
	inline void set_EmptyChain_0(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___EmptyChain_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChain_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATE_TA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_H
#ifndef CERTIFICATEREQUEST_T5408DC90A7608121F4630E67FE3959536FACDDC9_H
#define CERTIFICATEREQUEST_T5408DC90A7608121F4630E67FE3959536FACDDC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest
struct  CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest::mCertificateTypes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mCertificateTypes_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest::mCertificateAuthorities
	RuntimeObject* ___mCertificateAuthorities_2;

public:
	inline static int32_t get_offset_of_mCertificateTypes_0() { return static_cast<int32_t>(offsetof(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9, ___mCertificateTypes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mCertificateTypes_0() const { return ___mCertificateTypes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mCertificateTypes_0() { return &___mCertificateTypes_0; }
	inline void set_mCertificateTypes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mCertificateTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateTypes_0), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mCertificateAuthorities_2() { return static_cast<int32_t>(offsetof(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9, ___mCertificateAuthorities_2)); }
	inline RuntimeObject* get_mCertificateAuthorities_2() const { return ___mCertificateAuthorities_2; }
	inline RuntimeObject** get_address_of_mCertificateAuthorities_2() { return &___mCertificateAuthorities_2; }
	inline void set_mCertificateAuthorities_2(RuntimeObject* value)
	{
		___mCertificateAuthorities_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateAuthorities_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEREQUEST_T5408DC90A7608121F4630E67FE3959536FACDDC9_H
#ifndef CERTIFICATESTATUS_T7BFBAE146415307C61BF67D12531E7118629F7EB_H
#define CERTIFICATESTATUS_T7BFBAE146415307C61BF67D12531E7118629F7EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus
struct  CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus::mStatusType
	uint8_t ___mStatusType_0;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus::mResponse
	RuntimeObject * ___mResponse_1;

public:
	inline static int32_t get_offset_of_mStatusType_0() { return static_cast<int32_t>(offsetof(CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB, ___mStatusType_0)); }
	inline uint8_t get_mStatusType_0() const { return ___mStatusType_0; }
	inline uint8_t* get_address_of_mStatusType_0() { return &___mStatusType_0; }
	inline void set_mStatusType_0(uint8_t value)
	{
		___mStatusType_0 = value;
	}

	inline static int32_t get_offset_of_mResponse_1() { return static_cast<int32_t>(offsetof(CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB, ___mResponse_1)); }
	inline RuntimeObject * get_mResponse_1() const { return ___mResponse_1; }
	inline RuntimeObject ** get_address_of_mResponse_1() { return &___mResponse_1; }
	inline void set_mResponse_1(RuntimeObject * value)
	{
		___mResponse_1 = value;
		Il2CppCodeGenWriteBarrier((&___mResponse_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESTATUS_T7BFBAE146415307C61BF67D12531E7118629F7EB_H
#ifndef CERTIFICATESTATUSREQUEST_TDBE5889501D5B67BC974B80C5C00DF33A4145A4E_H
#define CERTIFICATESTATUSREQUEST_TDBE5889501D5B67BC974B80C5C00DF33A4145A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatusRequest
struct  CertificateStatusRequest_tDBE5889501D5B67BC974B80C5C00DF33A4145A4E  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatusRequest::mStatusType
	uint8_t ___mStatusType_0;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatusRequest::mRequest
	RuntimeObject * ___mRequest_1;

public:
	inline static int32_t get_offset_of_mStatusType_0() { return static_cast<int32_t>(offsetof(CertificateStatusRequest_tDBE5889501D5B67BC974B80C5C00DF33A4145A4E, ___mStatusType_0)); }
	inline uint8_t get_mStatusType_0() const { return ___mStatusType_0; }
	inline uint8_t* get_address_of_mStatusType_0() { return &___mStatusType_0; }
	inline void set_mStatusType_0(uint8_t value)
	{
		___mStatusType_0 = value;
	}

	inline static int32_t get_offset_of_mRequest_1() { return static_cast<int32_t>(offsetof(CertificateStatusRequest_tDBE5889501D5B67BC974B80C5C00DF33A4145A4E, ___mRequest_1)); }
	inline RuntimeObject * get_mRequest_1() const { return ___mRequest_1; }
	inline RuntimeObject ** get_address_of_mRequest_1() { return &___mRequest_1; }
	inline void set_mRequest_1(RuntimeObject * value)
	{
		___mRequest_1 = value;
		Il2CppCodeGenWriteBarrier((&___mRequest_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESTATUSREQUEST_TDBE5889501D5B67BC974B80C5C00DF33A4145A4E_H
#ifndef CERTIFICATESTATUSTYPE_TC153F2BA0E9155F3CC8BD623AA108FFDFF607F37_H
#define CERTIFICATESTATUSTYPE_TC153F2BA0E9155F3CC8BD623AA108FFDFF607F37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatusType
struct  CertificateStatusType_tC153F2BA0E9155F3CC8BD623AA108FFDFF607F37  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESTATUSTYPE_TC153F2BA0E9155F3CC8BD623AA108FFDFF607F37_H
#ifndef CERTIFICATETYPE_T2A584B004ACDBC516F5BCD6637438A38F46EF3CD_H
#define CERTIFICATETYPE_T2A584B004ACDBC516F5BCD6637438A38F46EF3CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateType
struct  CertificateType_t2A584B004ACDBC516F5BCD6637438A38F46EF3CD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATETYPE_T2A584B004ACDBC516F5BCD6637438A38F46EF3CD_H
#ifndef CERTIFICATEURL_T85451E742770699FC38BEA63CFAC9D00C108D2F2_H
#define CERTIFICATEURL_T85451E742770699FC38BEA63CFAC9D00C108D2F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateUrl
struct  CertificateUrl_t85451E742770699FC38BEA63CFAC9D00C108D2F2  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateUrl::mType
	uint8_t ___mType_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateUrl::mUrlAndHashList
	RuntimeObject* ___mUrlAndHashList_1;

public:
	inline static int32_t get_offset_of_mType_0() { return static_cast<int32_t>(offsetof(CertificateUrl_t85451E742770699FC38BEA63CFAC9D00C108D2F2, ___mType_0)); }
	inline uint8_t get_mType_0() const { return ___mType_0; }
	inline uint8_t* get_address_of_mType_0() { return &___mType_0; }
	inline void set_mType_0(uint8_t value)
	{
		___mType_0 = value;
	}

	inline static int32_t get_offset_of_mUrlAndHashList_1() { return static_cast<int32_t>(offsetof(CertificateUrl_t85451E742770699FC38BEA63CFAC9D00C108D2F2, ___mUrlAndHashList_1)); }
	inline RuntimeObject* get_mUrlAndHashList_1() const { return ___mUrlAndHashList_1; }
	inline RuntimeObject** get_address_of_mUrlAndHashList_1() { return &___mUrlAndHashList_1; }
	inline void set_mUrlAndHashList_1(RuntimeObject* value)
	{
		___mUrlAndHashList_1 = value;
		Il2CppCodeGenWriteBarrier((&___mUrlAndHashList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEURL_T85451E742770699FC38BEA63CFAC9D00C108D2F2_H
#ifndef CHACHA20POLY1305_TC6E46B8346FA054310769035DDE62290C27CA443_H
#define CHACHA20POLY1305_TC6E46B8346FA054310769035DDE62290C27CA443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305
struct  Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::context
	RuntimeObject* ___context_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::encryptCipher
	ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * ___encryptCipher_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ChaCha7539Engine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::decryptCipher
	ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * ___decryptCipher_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::encryptIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encryptIV_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::decryptIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___decryptIV_5;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443, ___context_1)); }
	inline RuntimeObject* get_context_1() const { return ___context_1; }
	inline RuntimeObject** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(RuntimeObject* value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_encryptCipher_2() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443, ___encryptCipher_2)); }
	inline ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * get_encryptCipher_2() const { return ___encryptCipher_2; }
	inline ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD ** get_address_of_encryptCipher_2() { return &___encryptCipher_2; }
	inline void set_encryptCipher_2(ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * value)
	{
		___encryptCipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_2), value);
	}

	inline static int32_t get_offset_of_decryptCipher_3() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443, ___decryptCipher_3)); }
	inline ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * get_decryptCipher_3() const { return ___decryptCipher_3; }
	inline ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD ** get_address_of_decryptCipher_3() { return &___decryptCipher_3; }
	inline void set_decryptCipher_3(ChaCha7539Engine_t290F0F2D222FDB596A16FC60B0EFFCAFC78A90DD * value)
	{
		___decryptCipher_3 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_3), value);
	}

	inline static int32_t get_offset_of_encryptIV_4() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443, ___encryptIV_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encryptIV_4() const { return ___encryptIV_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encryptIV_4() { return &___encryptIV_4; }
	inline void set_encryptIV_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encryptIV_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptIV_4), value);
	}

	inline static int32_t get_offset_of_decryptIV_5() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443, ___decryptIV_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_decryptIV_5() const { return ___decryptIV_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_decryptIV_5() { return &___decryptIV_5; }
	inline void set_decryptIV_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___decryptIV_5 = value;
		Il2CppCodeGenWriteBarrier((&___decryptIV_5), value);
	}
};

struct Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::Zeroes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Zeroes_0;

public:
	inline static int32_t get_offset_of_Zeroes_0() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443_StaticFields, ___Zeroes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Zeroes_0() const { return ___Zeroes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Zeroes_0() { return &___Zeroes_0; }
	inline void set_Zeroes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Zeroes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Zeroes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHACHA20POLY1305_TC6E46B8346FA054310769035DDE62290C27CA443_H
#ifndef CHANGECIPHERSPEC_T7005E370B01454BC876664D0948FFCCE9EBF0196_H
#define CHANGECIPHERSPEC_T7005E370B01454BC876664D0948FFCCE9EBF0196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ChangeCipherSpec
struct  ChangeCipherSpec_t7005E370B01454BC876664D0948FFCCE9EBF0196  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGECIPHERSPEC_T7005E370B01454BC876664D0948FFCCE9EBF0196_H
#ifndef CIPHERSUITE_TCAE444CA29C75E2CCE69848D152EE4FC056E8C01_H
#define CIPHERSUITE_TCAE444CA29C75E2CCE69848D152EE4FC056E8C01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CipherSuite
struct  CipherSuite_tCAE444CA29C75E2CCE69848D152EE4FC056E8C01  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITE_TCAE444CA29C75E2CCE69848D152EE4FC056E8C01_H
#ifndef CIPHERTYPE_TBA8CD9570F8671211B881466B7C932838D4F3E91_H
#define CIPHERTYPE_TBA8CD9570F8671211B881466B7C932838D4F3E91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CipherType
struct  CipherType_tBA8CD9570F8671211B881466B7C932838D4F3E91  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERTYPE_TBA8CD9570F8671211B881466B7C932838D4F3E91_H
#ifndef CLIENTAUTHENTICATIONTYPE_T4F03A49120EF6169277D0112DCE477A457559C93_H
#define CLIENTAUTHENTICATIONTYPE_T4F03A49120EF6169277D0112DCE477A457559C93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ClientAuthenticationType
struct  ClientAuthenticationType_t4F03A49120EF6169277D0112DCE477A457559C93  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTAUTHENTICATIONTYPE_T4F03A49120EF6169277D0112DCE477A457559C93_H
#ifndef CLIENTCERTIFICATETYPE_TBF7004E3402ACC0429A5E716DA789545DB5A7D9D_H
#define CLIENTCERTIFICATETYPE_TBF7004E3402ACC0429A5E716DA789545DB5A7D9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ClientCertificateType
struct  ClientCertificateType_tBF7004E3402ACC0429A5E716DA789545DB5A7D9D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCERTIFICATETYPE_TBF7004E3402ACC0429A5E716DA789545DB5A7D9D_H
#ifndef COMBINEDHASH_T276E0DA7217257F403A0E7B41F4036872230ABB1_H
#define COMBINEDHASH_T276E0DA7217257F403A0E7B41F4036872230ABB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CombinedHash
struct  CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CombinedHash::mContext
	RuntimeObject* ___mContext_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CombinedHash::mMd5
	RuntimeObject* ___mMd5_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CombinedHash::mSha1
	RuntimeObject* ___mSha1_2;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}

	inline static int32_t get_offset_of_mMd5_1() { return static_cast<int32_t>(offsetof(CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1, ___mMd5_1)); }
	inline RuntimeObject* get_mMd5_1() const { return ___mMd5_1; }
	inline RuntimeObject** get_address_of_mMd5_1() { return &___mMd5_1; }
	inline void set_mMd5_1(RuntimeObject* value)
	{
		___mMd5_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMd5_1), value);
	}

	inline static int32_t get_offset_of_mSha1_2() { return static_cast<int32_t>(offsetof(CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1, ___mSha1_2)); }
	inline RuntimeObject* get_mSha1_2() const { return ___mSha1_2; }
	inline RuntimeObject** get_address_of_mSha1_2() { return &___mSha1_2; }
	inline void set_mSha1_2(RuntimeObject* value)
	{
		___mSha1_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSha1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBINEDHASH_T276E0DA7217257F403A0E7B41F4036872230ABB1_H
#ifndef COMPRESSIONMETHOD_TD3138972611839AEB88630219DE4CE301D811E97_H
#define COMPRESSIONMETHOD_TD3138972611839AEB88630219DE4CE301D811E97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CompressionMethod
struct  CompressionMethod_tD3138972611839AEB88630219DE4CE301D811E97  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_TD3138972611839AEB88630219DE4CE301D811E97_H
#ifndef CONNECTIONEND_T30A9692B3172BA0091016BAAABD518DE7429F720_H
#define CONNECTIONEND_T30A9692B3172BA0091016BAAABD518DE7429F720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ConnectionEnd
struct  ConnectionEnd_t30A9692B3172BA0091016BAAABD518DE7429F720  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONEND_T30A9692B3172BA0091016BAAABD518DE7429F720_H
#ifndef CONTENTTYPE_T811366A63D0D56FC9A3E37674D39AA4D5B75483B_H
#define CONTENTTYPE_T811366A63D0D56FC9A3E37674D39AA4D5B75483B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ContentType
struct  ContentType_t811366A63D0D56FC9A3E37674D39AA4D5B75483B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T811366A63D0D56FC9A3E37674D39AA4D5B75483B_H
#ifndef ALGORITHMIDENTIFIERFACTORY_TD2161B35AA02B3D23F291794E80CB346223AA325_H
#define ALGORITHMIDENTIFIERFACTORY_TD2161B35AA02B3D23F291794E80CB346223AA325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.AlgorithmIdentifierFactory
struct  AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325  : public RuntimeObject
{
public:

public:
};

struct AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.AlgorithmIdentifierFactory::IDEA_CBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IDEA_CBC_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.AlgorithmIdentifierFactory::CAST5_CBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CAST5_CBC_1;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.AlgorithmIdentifierFactory::rc2Table
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___rc2Table_2;

public:
	inline static int32_t get_offset_of_IDEA_CBC_0() { return static_cast<int32_t>(offsetof(AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields, ___IDEA_CBC_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IDEA_CBC_0() const { return ___IDEA_CBC_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IDEA_CBC_0() { return &___IDEA_CBC_0; }
	inline void set_IDEA_CBC_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IDEA_CBC_0 = value;
		Il2CppCodeGenWriteBarrier((&___IDEA_CBC_0), value);
	}

	inline static int32_t get_offset_of_CAST5_CBC_1() { return static_cast<int32_t>(offsetof(AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields, ___CAST5_CBC_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CAST5_CBC_1() const { return ___CAST5_CBC_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CAST5_CBC_1() { return &___CAST5_CBC_1; }
	inline void set_CAST5_CBC_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CAST5_CBC_1 = value;
		Il2CppCodeGenWriteBarrier((&___CAST5_CBC_1), value);
	}

	inline static int32_t get_offset_of_rc2Table_2() { return static_cast<int32_t>(offsetof(AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields, ___rc2Table_2)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_rc2Table_2() const { return ___rc2Table_2; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_rc2Table_2() { return &___rc2Table_2; }
	inline void set_rc2Table_2(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___rc2Table_2 = value;
		Il2CppCodeGenWriteBarrier((&___rc2Table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHMIDENTIFIERFACTORY_TD2161B35AA02B3D23F291794E80CB346223AA325_H
#ifndef CIPHERFACTORY_T191535822EA0BD50180660BABE0B4D293B7F0D5F_H
#define CIPHERFACTORY_T191535822EA0BD50180660BABE0B4D293B7F0D5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.CipherFactory
struct  CipherFactory_t191535822EA0BD50180660BABE0B4D293B7F0D5F  : public RuntimeObject
{
public:

public:
};

struct CipherFactory_t191535822EA0BD50180660BABE0B4D293B7F0D5F_StaticFields
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.CipherFactory::rc2Ekb
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___rc2Ekb_0;

public:
	inline static int32_t get_offset_of_rc2Ekb_0() { return static_cast<int32_t>(offsetof(CipherFactory_t191535822EA0BD50180660BABE0B4D293B7F0D5F_StaticFields, ___rc2Ekb_0)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_rc2Ekb_0() const { return ___rc2Ekb_0; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_rc2Ekb_0() { return &___rc2Ekb_0; }
	inline void set_rc2Ekb_0(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___rc2Ekb_0 = value;
		Il2CppCodeGenWriteBarrier((&___rc2Ekb_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERFACTORY_T191535822EA0BD50180660BABE0B4D293B7F0D5F_H
#ifndef CIPHERKEYGENERATORFACTORY_T02B86B440E718DC1A50970194ECE92DE1B3BD7A0_H
#define CIPHERKEYGENERATORFACTORY_T02B86B440E718DC1A50970194ECE92DE1B3BD7A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.CipherKeyGeneratorFactory
struct  CipherKeyGeneratorFactory_t02B86B440E718DC1A50970194ECE92DE1B3BD7A0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERKEYGENERATORFACTORY_T02B86B440E718DC1A50970194ECE92DE1B3BD7A0_H
#ifndef PACK_T297907094A867736FE6CEA66B35B75E5357FEF15_H
#define PACK_T297907094A867736FE6CEA66B35B75E5357FEF15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Utilities.Pack
struct  Pack_t297907094A867736FE6CEA66B35B75E5357FEF15  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PACK_T297907094A867736FE6CEA66B35B75E5357FEF15_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef BUFFEREDAEADBLOCKCIPHER_T1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E_H
#define BUFFEREDAEADBLOCKCIPHER_T1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAeadBlockCipher
struct  BufferedAeadBlockCipher_t1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAeadBlockCipher::cipher
	RuntimeObject* ___cipher_1;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(BufferedAeadBlockCipher_t1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDAEADBLOCKCIPHER_T1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E_H
#ifndef BUFFEREDAEADCIPHER_T58B7209F9A8B02B8D812987403A26C0755C97F01_H
#define BUFFEREDAEADCIPHER_T58B7209F9A8B02B8D812987403A26C0755C97F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAeadCipher
struct  BufferedAeadCipher_t58B7209F9A8B02B8D812987403A26C0755C97F01  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAeadCipher::cipher
	RuntimeObject* ___cipher_1;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(BufferedAeadCipher_t58B7209F9A8B02B8D812987403A26C0755C97F01, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDAEADCIPHER_T58B7209F9A8B02B8D812987403A26C0755C97F01_H
#ifndef BUFFEREDASYMMETRICBLOCKCIPHER_T2621F0EE1F6D3E822E9D5CF1998A779C949974B6_H
#define BUFFEREDASYMMETRICBLOCKCIPHER_T2621F0EE1F6D3E822E9D5CF1998A779C949974B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAsymmetricBlockCipher
struct  BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAsymmetricBlockCipher::cipher
	RuntimeObject* ___cipher_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAsymmetricBlockCipher::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedAsymmetricBlockCipher::bufOff
	int32_t ___bufOff_3;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6, ___buffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_2() const { return ___buffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_2), value);
	}

	inline static int32_t get_offset_of_bufOff_3() { return static_cast<int32_t>(offsetof(BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6, ___bufOff_3)); }
	inline int32_t get_bufOff_3() const { return ___bufOff_3; }
	inline int32_t* get_address_of_bufOff_3() { return &___bufOff_3; }
	inline void set_bufOff_3(int32_t value)
	{
		___bufOff_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDASYMMETRICBLOCKCIPHER_T2621F0EE1F6D3E822E9D5CF1998A779C949974B6_H
#ifndef BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#define BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher
struct  BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::bufOff
	int32_t ___bufOff_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::forEncryption
	bool ___forEncryption_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::cipher
	RuntimeObject* ___cipher_4;

public:
	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___buf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_1() const { return ___buf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_1 = value;
		Il2CppCodeGenWriteBarrier((&___buf_1), value);
	}

	inline static int32_t get_offset_of_bufOff_2() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___bufOff_2)); }
	inline int32_t get_bufOff_2() const { return ___bufOff_2; }
	inline int32_t* get_address_of_bufOff_2() { return &___bufOff_2; }
	inline void set_bufOff_2(int32_t value)
	{
		___bufOff_2 = value;
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#ifndef BUFFEREDIESCIPHER_T9A81D02FFA22654E36EAFF93A6E55345B1FDA62B_H
#define BUFFEREDIESCIPHER_T9A81D02FFA22654E36EAFF93A6E55345B1FDA62B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedIesCipher
struct  BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedIesCipher::engine
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA * ___engine_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedIesCipher::forEncryption
	bool ___forEncryption_2;
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedIesCipher::buffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___buffer_3;

public:
	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B, ___engine_1)); }
	inline IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA * get_engine_1() const { return ___engine_1; }
	inline IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA ** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA * value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}

	inline static int32_t get_offset_of_forEncryption_2() { return static_cast<int32_t>(offsetof(BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B, ___forEncryption_2)); }
	inline bool get_forEncryption_2() const { return ___forEncryption_2; }
	inline bool* get_address_of_forEncryption_2() { return &___forEncryption_2; }
	inline void set_forEncryption_2(bool value)
	{
		___forEncryption_2 = value;
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B, ___buffer_3)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_buffer_3() const { return ___buffer_3; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDIESCIPHER_T9A81D02FFA22654E36EAFF93A6E55345B1FDA62B_H
#ifndef BUFFEREDSTREAMCIPHER_TC4E3C1DC90536225E2383EECB82E10FCAC97F9C1_H
#define BUFFEREDSTREAMCIPHER_TC4E3C1DC90536225E2383EECB82E10FCAC97F9C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedStreamCipher
struct  BufferedStreamCipher_tC4E3C1DC90536225E2383EECB82E10FCAC97F9C1  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IStreamCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedStreamCipher::cipher
	RuntimeObject* ___cipher_1;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(BufferedStreamCipher_tC4E3C1DC90536225E2383EECB82E10FCAC97F9C1, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDSTREAMCIPHER_TC4E3C1DC90536225E2383EECB82E10FCAC97F9C1_H
#ifndef CRYPTOEXCEPTION_TD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77_H
#define CRYPTOEXCEPTION_TD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.CryptoException
struct  CryptoException_tD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOEXCEPTION_TD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77_H
#ifndef ABSTRACTTLSAGREEMENTCREDENTIALS_TD2D94F6B8ED88005D50E606A64FF44EDF924E069_H
#define ABSTRACTTLSAGREEMENTCREDENTIALS_TD2D94F6B8ED88005D50E606A64FF44EDF924E069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsAgreementCredentials
struct  AbstractTlsAgreementCredentials_tD2D94F6B8ED88005D50E606A64FF44EDF924E069  : public AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSAGREEMENTCREDENTIALS_TD2D94F6B8ED88005D50E606A64FF44EDF924E069_H
#ifndef ABSTRACTTLSCLIENT_TD2618A4C0AB20690FF055439D20B717ED70F8A14_H
#define ABSTRACTTLSCLIENT_TD2618A4C0AB20690FF055439D20B717ED70F8A14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient
struct  AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14  : public AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipherFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mCipherFactory
	RuntimeObject* ___mCipherFactory_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mContext
	RuntimeObject* ___mContext_2;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_7;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCompressionMethod
	int16_t ___mSelectedCompressionMethod_8;
	// System.Collections.Generic.List`1<System.String> BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::<HostNames>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CHostNamesU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<System.String> BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::<ClientSupportedProtocols>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CClientSupportedProtocolsU3Ek__BackingField_10;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::<ServerSupportedProtocol>k__BackingField
	String_t* ___U3CServerSupportedProtocolU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_mCipherFactory_1() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mCipherFactory_1)); }
	inline RuntimeObject* get_mCipherFactory_1() const { return ___mCipherFactory_1; }
	inline RuntimeObject** get_address_of_mCipherFactory_1() { return &___mCipherFactory_1; }
	inline void set_mCipherFactory_1(RuntimeObject* value)
	{
		___mCipherFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_3() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mSupportedSignatureAlgorithms_3)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_3() const { return ___mSupportedSignatureAlgorithms_3; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_3() { return &___mSupportedSignatureAlgorithms_3; }
	inline void set_mSupportedSignatureAlgorithms_3(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_3), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_4() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mNamedCurves_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_4() const { return ___mNamedCurves_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_4() { return &___mNamedCurves_4; }
	inline void set_mNamedCurves_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_4), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_5() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mClientECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_5() const { return ___mClientECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_5() { return &___mClientECPointFormats_5; }
	inline void set_mClientECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_6() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mServerECPointFormats_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_6() const { return ___mServerECPointFormats_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_6() { return &___mServerECPointFormats_6; }
	inline void set_mServerECPointFormats_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_6), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_7() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mSelectedCipherSuite_7)); }
	inline int32_t get_mSelectedCipherSuite_7() const { return ___mSelectedCipherSuite_7; }
	inline int32_t* get_address_of_mSelectedCipherSuite_7() { return &___mSelectedCipherSuite_7; }
	inline void set_mSelectedCipherSuite_7(int32_t value)
	{
		___mSelectedCipherSuite_7 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_8() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mSelectedCompressionMethod_8)); }
	inline int16_t get_mSelectedCompressionMethod_8() const { return ___mSelectedCompressionMethod_8; }
	inline int16_t* get_address_of_mSelectedCompressionMethod_8() { return &___mSelectedCompressionMethod_8; }
	inline void set_mSelectedCompressionMethod_8(int16_t value)
	{
		___mSelectedCompressionMethod_8 = value;
	}

	inline static int32_t get_offset_of_U3CHostNamesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___U3CHostNamesU3Ek__BackingField_9)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CHostNamesU3Ek__BackingField_9() const { return ___U3CHostNamesU3Ek__BackingField_9; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CHostNamesU3Ek__BackingField_9() { return &___U3CHostNamesU3Ek__BackingField_9; }
	inline void set_U3CHostNamesU3Ek__BackingField_9(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CHostNamesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostNamesU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CClientSupportedProtocolsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___U3CClientSupportedProtocolsU3Ek__BackingField_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CClientSupportedProtocolsU3Ek__BackingField_10() const { return ___U3CClientSupportedProtocolsU3Ek__BackingField_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CClientSupportedProtocolsU3Ek__BackingField_10() { return &___U3CClientSupportedProtocolsU3Ek__BackingField_10; }
	inline void set_U3CClientSupportedProtocolsU3Ek__BackingField_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CClientSupportedProtocolsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientSupportedProtocolsU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CServerSupportedProtocolU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___U3CServerSupportedProtocolU3Ek__BackingField_11)); }
	inline String_t* get_U3CServerSupportedProtocolU3Ek__BackingField_11() const { return ___U3CServerSupportedProtocolU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CServerSupportedProtocolU3Ek__BackingField_11() { return &___U3CServerSupportedProtocolU3Ek__BackingField_11; }
	inline void set_U3CServerSupportedProtocolU3Ek__BackingField_11(String_t* value)
	{
		___U3CServerSupportedProtocolU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerSupportedProtocolU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCLIENT_TD2618A4C0AB20690FF055439D20B717ED70F8A14_H
#ifndef ABSTRACTTLSENCRYPTIONCREDENTIALS_T28E0815D9DEB261CEC85F66E3C31B103A84D5908_H
#define ABSTRACTTLSENCRYPTIONCREDENTIALS_T28E0815D9DEB261CEC85F66E3C31B103A84D5908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsEncryptionCredentials
struct  AbstractTlsEncryptionCredentials_t28E0815D9DEB261CEC85F66E3C31B103A84D5908  : public AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSENCRYPTIONCREDENTIALS_T28E0815D9DEB261CEC85F66E3C31B103A84D5908_H
#ifndef ABSTRACTTLSSERVER_T5B6433B269A08B31A745579882002FE58C06EE25_H
#define ABSTRACTTLSSERVER_T5B6433B269A08B31A745579882002FE58C06EE25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer
struct  AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25  : public AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipherFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mCipherFactory
	RuntimeObject* ___mCipherFactory_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mContext
	RuntimeObject* ___mContext_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mClientVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mClientVersion_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mOfferedCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mOfferedCipherSuites_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mOfferedCompressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mOfferedCompressionMethods_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mClientExtensions
	RuntimeObject* ___mClientExtensions_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mEncryptThenMacOffered
	bool ___mEncryptThenMacOffered_7;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mMaxFragmentLengthOffered
	int16_t ___mMaxFragmentLengthOffered_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mTruncatedHMacOffered
	bool ___mTruncatedHMacOffered_9;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_10;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mEccCipherSuitesOffered
	bool ___mEccCipherSuitesOffered_11;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mServerVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mServerVersion_15;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_16;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mSelectedCompressionMethod
	uint8_t ___mSelectedCompressionMethod_17;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mServerExtensions
	RuntimeObject* ___mServerExtensions_18;

public:
	inline static int32_t get_offset_of_mCipherFactory_1() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mCipherFactory_1)); }
	inline RuntimeObject* get_mCipherFactory_1() const { return ___mCipherFactory_1; }
	inline RuntimeObject** get_address_of_mCipherFactory_1() { return &___mCipherFactory_1; }
	inline void set_mCipherFactory_1(RuntimeObject* value)
	{
		___mCipherFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}

	inline static int32_t get_offset_of_mClientVersion_3() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mClientVersion_3)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mClientVersion_3() const { return ___mClientVersion_3; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mClientVersion_3() { return &___mClientVersion_3; }
	inline void set_mClientVersion_3(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mClientVersion_3 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_3), value);
	}

	inline static int32_t get_offset_of_mOfferedCipherSuites_4() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mOfferedCipherSuites_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mOfferedCipherSuites_4() const { return ___mOfferedCipherSuites_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mOfferedCipherSuites_4() { return &___mOfferedCipherSuites_4; }
	inline void set_mOfferedCipherSuites_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mOfferedCipherSuites_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCipherSuites_4), value);
	}

	inline static int32_t get_offset_of_mOfferedCompressionMethods_5() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mOfferedCompressionMethods_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mOfferedCompressionMethods_5() const { return ___mOfferedCompressionMethods_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mOfferedCompressionMethods_5() { return &___mOfferedCompressionMethods_5; }
	inline void set_mOfferedCompressionMethods_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mOfferedCompressionMethods_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCompressionMethods_5), value);
	}

	inline static int32_t get_offset_of_mClientExtensions_6() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mClientExtensions_6)); }
	inline RuntimeObject* get_mClientExtensions_6() const { return ___mClientExtensions_6; }
	inline RuntimeObject** get_address_of_mClientExtensions_6() { return &___mClientExtensions_6; }
	inline void set_mClientExtensions_6(RuntimeObject* value)
	{
		___mClientExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___mClientExtensions_6), value);
	}

	inline static int32_t get_offset_of_mEncryptThenMacOffered_7() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mEncryptThenMacOffered_7)); }
	inline bool get_mEncryptThenMacOffered_7() const { return ___mEncryptThenMacOffered_7; }
	inline bool* get_address_of_mEncryptThenMacOffered_7() { return &___mEncryptThenMacOffered_7; }
	inline void set_mEncryptThenMacOffered_7(bool value)
	{
		___mEncryptThenMacOffered_7 = value;
	}

	inline static int32_t get_offset_of_mMaxFragmentLengthOffered_8() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mMaxFragmentLengthOffered_8)); }
	inline int16_t get_mMaxFragmentLengthOffered_8() const { return ___mMaxFragmentLengthOffered_8; }
	inline int16_t* get_address_of_mMaxFragmentLengthOffered_8() { return &___mMaxFragmentLengthOffered_8; }
	inline void set_mMaxFragmentLengthOffered_8(int16_t value)
	{
		___mMaxFragmentLengthOffered_8 = value;
	}

	inline static int32_t get_offset_of_mTruncatedHMacOffered_9() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mTruncatedHMacOffered_9)); }
	inline bool get_mTruncatedHMacOffered_9() const { return ___mTruncatedHMacOffered_9; }
	inline bool* get_address_of_mTruncatedHMacOffered_9() { return &___mTruncatedHMacOffered_9; }
	inline void set_mTruncatedHMacOffered_9(bool value)
	{
		___mTruncatedHMacOffered_9 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_10() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mSupportedSignatureAlgorithms_10)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_10() const { return ___mSupportedSignatureAlgorithms_10; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_10() { return &___mSupportedSignatureAlgorithms_10; }
	inline void set_mSupportedSignatureAlgorithms_10(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_10), value);
	}

	inline static int32_t get_offset_of_mEccCipherSuitesOffered_11() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mEccCipherSuitesOffered_11)); }
	inline bool get_mEccCipherSuitesOffered_11() const { return ___mEccCipherSuitesOffered_11; }
	inline bool* get_address_of_mEccCipherSuitesOffered_11() { return &___mEccCipherSuitesOffered_11; }
	inline void set_mEccCipherSuitesOffered_11(bool value)
	{
		___mEccCipherSuitesOffered_11 = value;
	}

	inline static int32_t get_offset_of_mNamedCurves_12() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mNamedCurves_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_12() const { return ___mNamedCurves_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_12() { return &___mNamedCurves_12; }
	inline void set_mNamedCurves_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_12 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_12), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_13() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mClientECPointFormats_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_13() const { return ___mClientECPointFormats_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_13() { return &___mClientECPointFormats_13; }
	inline void set_mClientECPointFormats_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_13 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_13), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_14() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mServerECPointFormats_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_14() const { return ___mServerECPointFormats_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_14() { return &___mServerECPointFormats_14; }
	inline void set_mServerECPointFormats_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_14), value);
	}

	inline static int32_t get_offset_of_mServerVersion_15() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mServerVersion_15)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mServerVersion_15() const { return ___mServerVersion_15; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mServerVersion_15() { return &___mServerVersion_15; }
	inline void set_mServerVersion_15(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mServerVersion_15 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_15), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_16() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mSelectedCipherSuite_16)); }
	inline int32_t get_mSelectedCipherSuite_16() const { return ___mSelectedCipherSuite_16; }
	inline int32_t* get_address_of_mSelectedCipherSuite_16() { return &___mSelectedCipherSuite_16; }
	inline void set_mSelectedCipherSuite_16(int32_t value)
	{
		___mSelectedCipherSuite_16 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_17() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mSelectedCompressionMethod_17)); }
	inline uint8_t get_mSelectedCompressionMethod_17() const { return ___mSelectedCompressionMethod_17; }
	inline uint8_t* get_address_of_mSelectedCompressionMethod_17() { return &___mSelectedCompressionMethod_17; }
	inline void set_mSelectedCompressionMethod_17(uint8_t value)
	{
		___mSelectedCompressionMethod_17 = value;
	}

	inline static int32_t get_offset_of_mServerExtensions_18() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mServerExtensions_18)); }
	inline RuntimeObject* get_mServerExtensions_18() const { return ___mServerExtensions_18; }
	inline RuntimeObject** get_address_of_mServerExtensions_18() { return &___mServerExtensions_18; }
	inline void set_mServerExtensions_18(RuntimeObject* value)
	{
		___mServerExtensions_18 = value;
		Il2CppCodeGenWriteBarrier((&___mServerExtensions_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSERVER_T5B6433B269A08B31A745579882002FE58C06EE25_H
#ifndef ABSTRACTTLSSIGNERCREDENTIALS_T68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD_H
#define ABSTRACTTLSSIGNERCREDENTIALS_T68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSignerCredentials
struct  AbstractTlsSignerCredentials_t68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD  : public AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNERCREDENTIALS_T68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD_H
#ifndef DEFAULTTLSCIPHERFACTORY_TAD29BB8B1F3A60AC17ED56905A5FCB8B1EEA663B_H
#define DEFAULTTLSCIPHERFACTORY_TAD29BB8B1F3A60AC17ED56905A5FCB8B1EEA663B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsCipherFactory
struct  DefaultTlsCipherFactory_tAD29BB8B1F3A60AC17ED56905A5FCB8B1EEA663B  : public AbstractTlsCipherFactory_tF28F1FD8C7DB102AC9A2C8841DD2664508DAAAC3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSCIPHERFACTORY_TAD29BB8B1F3A60AC17ED56905A5FCB8B1EEA663B_H
#ifndef CMSKEYTRANSRECIPIENTINFOGENERATOR_T60CAADD348521147BB5E0B4267016B26BF067730_H
#define CMSKEYTRANSRECIPIENTINFOGENERATOR_T60CAADD348521147BB5E0B4267016B26BF067730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsKeyTransRecipientInfoGenerator
struct  CmsKeyTransRecipientInfoGenerator_t60CAADD348521147BB5E0B4267016B26BF067730  : public KeyTransRecipientInfoGenerator_t2A9824564CB74998707741E748634E314C7CF453
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IKeyWrapper BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsKeyTransRecipientInfoGenerator::keyWrapper
	RuntimeObject* ___keyWrapper_7;

public:
	inline static int32_t get_offset_of_keyWrapper_7() { return static_cast<int32_t>(offsetof(CmsKeyTransRecipientInfoGenerator_t60CAADD348521147BB5E0B4267016B26BF067730, ___keyWrapper_7)); }
	inline RuntimeObject* get_keyWrapper_7() const { return ___keyWrapper_7; }
	inline RuntimeObject** get_address_of_keyWrapper_7() { return &___keyWrapper_7; }
	inline void set_keyWrapper_7(RuntimeObject* value)
	{
		___keyWrapper_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyWrapper_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSKEYTRANSRECIPIENTINFOGENERATOR_T60CAADD348521147BB5E0B4267016B26BF067730_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef DATALENGTHEXCEPTION_T59FBF63938797E40171F84922FC4BE5D4AED621C_H
#define DATALENGTHEXCEPTION_T59FBF63938797E40171F84922FC4BE5D4AED621C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.DataLengthException
struct  DataLengthException_t59FBF63938797E40171F84922FC4BE5D4AED621C  : public CryptoException_tD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATALENGTHEXCEPTION_T59FBF63938797E40171F84922FC4BE5D4AED621C_H
#ifndef INVALIDCIPHERTEXTEXCEPTION_TBC7EC8FB9CE399C0AC1EE866A51652D5C89240E5_H
#define INVALIDCIPHERTEXTEXCEPTION_TBC7EC8FB9CE399C0AC1EE866A51652D5C89240E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.InvalidCipherTextException
struct  InvalidCipherTextException_tBC7EC8FB9CE399C0AC1EE866A51652D5C89240E5  : public CryptoException_tD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCIPHERTEXTEXCEPTION_TBC7EC8FB9CE399C0AC1EE866A51652D5C89240E5_H
#ifndef MAXBYTESEXCEEDEDEXCEPTION_T073DDC8EB9AE1F3B7DDFB19D25F6C3AE39213180_H
#define MAXBYTESEXCEEDEDEXCEPTION_T073DDC8EB9AE1F3B7DDFB19D25F6C3AE39213180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.MaxBytesExceededException
struct  MaxBytesExceededException_t073DDC8EB9AE1F3B7DDFB19D25F6C3AE39213180  : public CryptoException_tD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXBYTESEXCEEDEDEXCEPTION_T073DDC8EB9AE1F3B7DDFB19D25F6C3AE39213180_H
#ifndef BYTEQUEUESTREAM_T4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A_H
#define BYTEQUEUESTREAM_T4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream
struct  ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream::buffer
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___buffer_5;

public:
	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A, ___buffer_5)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_buffer_5() const { return ___buffer_5; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEQUEUESTREAM_T4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A_H
#ifndef DEFAULTTLSAGREEMENTCREDENTIALS_T47990FAE902E6F22B262DBD9D3BF85715864FC7E_H
#define DEFAULTTLSAGREEMENTCREDENTIALS_T47990FAE902E6F22B262DBD9D3BF85715864FC7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsAgreementCredentials
struct  DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E  : public AbstractTlsAgreementCredentials_tD2D94F6B8ED88005D50E606A64FF44EDF924E069
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsAgreementCredentials::mCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mCertificate_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsAgreementCredentials::mPrivateKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mPrivateKey_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBasicAgreement BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsAgreementCredentials::mBasicAgreement
	RuntimeObject* ___mBasicAgreement_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsAgreementCredentials::mTruncateAgreement
	bool ___mTruncateAgreement_3;

public:
	inline static int32_t get_offset_of_mCertificate_0() { return static_cast<int32_t>(offsetof(DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E, ___mCertificate_0)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mCertificate_0() const { return ___mCertificate_0; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mCertificate_0() { return &___mCertificate_0; }
	inline void set_mCertificate_0(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mCertificate_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificate_0), value);
	}

	inline static int32_t get_offset_of_mPrivateKey_1() { return static_cast<int32_t>(offsetof(DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E, ___mPrivateKey_1)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mPrivateKey_1() const { return ___mPrivateKey_1; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mPrivateKey_1() { return &___mPrivateKey_1; }
	inline void set_mPrivateKey_1(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mPrivateKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPrivateKey_1), value);
	}

	inline static int32_t get_offset_of_mBasicAgreement_2() { return static_cast<int32_t>(offsetof(DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E, ___mBasicAgreement_2)); }
	inline RuntimeObject* get_mBasicAgreement_2() const { return ___mBasicAgreement_2; }
	inline RuntimeObject** get_address_of_mBasicAgreement_2() { return &___mBasicAgreement_2; }
	inline void set_mBasicAgreement_2(RuntimeObject* value)
	{
		___mBasicAgreement_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBasicAgreement_2), value);
	}

	inline static int32_t get_offset_of_mTruncateAgreement_3() { return static_cast<int32_t>(offsetof(DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E, ___mTruncateAgreement_3)); }
	inline bool get_mTruncateAgreement_3() const { return ___mTruncateAgreement_3; }
	inline bool* get_address_of_mTruncateAgreement_3() { return &___mTruncateAgreement_3; }
	inline void set_mTruncateAgreement_3(bool value)
	{
		___mTruncateAgreement_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSAGREEMENTCREDENTIALS_T47990FAE902E6F22B262DBD9D3BF85715864FC7E_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef OUTPUTLENGTHEXCEPTION_TD077A634E997234E86D4D8FF1A2B7412066CEE7E_H
#define OUTPUTLENGTHEXCEPTION_TD077A634E997234E86D4D8FF1A2B7412066CEE7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.OutputLengthException
struct  OutputLengthException_tD077A634E997234E86D4D8FF1A2B7412066CEE7E  : public DataLengthException_t59FBF63938797E40171F84922FC4BE5D4AED621C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTLENGTHEXCEPTION_TD077A634E997234E86D4D8FF1A2B7412066CEE7E_H
#ifndef LISTBUFFER16_TD8EC21AE09532A0678CC5F3FFF315B0F6031DFAD_H
#define LISTBUFFER16_TD8EC21AE09532A0678CC5F3FFF315B0F6031DFAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateUrl_ListBuffer16
struct  ListBuffer16_tD8EC21AE09532A0678CC5F3FFF315B0F6031DFAD  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTBUFFER16_TD8EC21AE09532A0678CC5F3FFF315B0F6031DFAD_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (CmsKeyTransRecipientInfoGenerator_t60CAADD348521147BB5E0B4267016B26BF067730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4400[1] = 
{
	CmsKeyTransRecipientInfoGenerator_t60CAADD348521147BB5E0B4267016B26BF067730::get_offset_of_keyWrapper_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4401[2] = 
{
	AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24::get_offset_of_publicParameter_0(),
	AsymmetricCipherKeyPair_t4630D28256FA535784C97BBD4D410AB0DC19FC24::get_offset_of_privateParameter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4402[1] = 
{
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316::get_offset_of_privateKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (BufferedAeadBlockCipher_t1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4403[1] = 
{
	BufferedAeadBlockCipher_t1D928F3F2A46F94C9A33C24A995E2E26F5FC3E6E::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (BufferedAeadCipher_t58B7209F9A8B02B8D812987403A26C0755C97F01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4404[1] = 
{
	BufferedAeadCipher_t58B7209F9A8B02B8D812987403A26C0755C97F01::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4405[3] = 
{
	BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6::get_offset_of_cipher_1(),
	BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6::get_offset_of_buffer_2(),
	BufferedAsymmetricBlockCipher_t2621F0EE1F6D3E822E9D5CF1998A779C949974B6::get_offset_of_bufOff_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[4] = 
{
	BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B::get_offset_of_buf_1(),
	BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B::get_offset_of_bufOff_2(),
	BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B::get_offset_of_forEncryption_3(),
	BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F), -1, sizeof(BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4407[1] = 
{
	BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields::get_offset_of_EmptyBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4408[3] = 
{
	BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B::get_offset_of_engine_1(),
	BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B::get_offset_of_forEncryption_2(),
	BufferedIesCipher_t9A81D02FFA22654E36EAFF93A6E55345B1FDA62B::get_offset_of_buffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (BufferedStreamCipher_tC4E3C1DC90536225E2383EECB82E10FCAC97F9C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[1] = 
{
	BufferedStreamCipher_tC4E3C1DC90536225E2383EECB82E10FCAC97F9C1::get_offset_of_cipher_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (Check_t288CF9AB9B725A839BDB9C23FBD55EA6E9712821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4411[4] = 
{
	CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB::get_offset_of_random_0(),
	CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB::get_offset_of_strength_1(),
	CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB::get_offset_of_uninitialised_2(),
	CipherKeyGenerator_t5159EB217F5FC7C4B39BB4EA981381BCC3689BBB::get_offset_of_defaultStrength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (CryptoException_tD74AA31DF2054FB99BDD051DF50A1C2C3D0EFB77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (DataLengthException_t59FBF63938797E40171F84922FC4BE5D4AED621C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (InvalidCipherTextException_tBC7EC8FB9CE399C0AC1EE866A51652D5C89240E5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4450[2] = 
{
	KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC::get_offset_of_random_0(),
	KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC::get_offset_of_strength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (MaxBytesExceededException_t073DDC8EB9AE1F3B7DDFB19D25F6C3AE39213180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (OutputLengthException_tD077A634E997234E86D4D8FF1A2B7412066CEE7E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4453[3] = 
{
	PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E::get_offset_of_mPassword_0(),
	PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E::get_offset_of_mSalt_1(),
	PbeParametersGenerator_t8393B43188C7630FB3E775E3120C5DE0A232437E::get_offset_of_mIterationCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (SimpleBlockResult_tC5C259D1E128A1CE1F90F6C22B72F05254A5E285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4454[1] = 
{
	SimpleBlockResult_tC5C259D1E128A1CE1F90F6C22B72F05254A5E285::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (StreamBlockCipher_tF61480B98D7C235D792A1E3EB513101801C1A2A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4455[2] = 
{
	StreamBlockCipher_tF61480B98D7C235D792A1E3EB513101801C1A2A5::get_offset_of_cipher_0(),
	StreamBlockCipher_tF61480B98D7C235D792A1E3EB513101801C1A2A5::get_offset_of_oneByte_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325), -1, sizeof(AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4456[3] = 
{
	AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields::get_offset_of_IDEA_CBC_0(),
	AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields::get_offset_of_CAST5_CBC_1(),
	AlgorithmIdentifierFactory_tD2161B35AA02B3D23F291794E80CB346223AA325_StaticFields::get_offset_of_rc2Table_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (CipherFactory_t191535822EA0BD50180660BABE0B4D293B7F0D5F), -1, sizeof(CipherFactory_t191535822EA0BD50180660BABE0B4D293B7F0D5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4457[1] = 
{
	CipherFactory_t191535822EA0BD50180660BABE0B4D293B7F0D5F_StaticFields::get_offset_of_rc2Ekb_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (CipherKeyGeneratorFactory_t02B86B440E718DC1A50970194ECE92DE1B3BD7A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (Pack_t297907094A867736FE6CEA66B35B75E5357FEF15), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (AbstractTlsAgreementCredentials_tD2D94F6B8ED88005D50E606A64FF44EDF924E069), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (AbstractTlsCipherFactory_tF28F1FD8C7DB102AC9A2C8841DD2664508DAAAC3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4462[11] = 
{
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mCipherFactory_1(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mContext_2(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mSupportedSignatureAlgorithms_3(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mNamedCurves_4(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mClientECPointFormats_5(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mServerECPointFormats_6(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mSelectedCipherSuite_7(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_mSelectedCompressionMethod_8(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_U3CHostNamesU3Ek__BackingField_9(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_U3CClientSupportedProtocolsU3Ek__BackingField_10(),
	AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14::get_offset_of_U3CServerSupportedProtocolU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424), -1, sizeof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4463[8] = 
{
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields::get_offset_of_counter_0(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mNonceRandom_1(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mSecureRandom_2(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mSecurityParameters_3(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mClientVersion_4(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mServerVersion_5(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mSession_6(),
	AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424::get_offset_of_mUserObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (AbstractTlsEncryptionCredentials_t28E0815D9DEB261CEC85F66E3C31B103A84D5908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4466[3] = 
{
	AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3::get_offset_of_mKeyExchange_0(),
	AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3::get_offset_of_mSupportedSignatureAlgorithms_1(),
	AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3::get_offset_of_mContext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { sizeof (AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4467[1] = 
{
	AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638::get_offset_of_mCloseHandle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4468[18] = 
{
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mCipherFactory_1(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mContext_2(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mClientVersion_3(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mOfferedCipherSuites_4(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mOfferedCompressionMethods_5(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mClientExtensions_6(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mEncryptThenMacOffered_7(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mMaxFragmentLengthOffered_8(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mTruncatedHMacOffered_9(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mSupportedSignatureAlgorithms_10(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mEccCipherSuitesOffered_11(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mNamedCurves_12(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mClientECPointFormats_13(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mServerECPointFormats_14(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mServerVersion_15(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mSelectedCipherSuite_16(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mSelectedCompressionMethod_17(),
	AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25::get_offset_of_mServerExtensions_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4469[1] = 
{
	AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F::get_offset_of_mContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (AbstractTlsSignerCredentials_t68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (AlertDescription_t7DDB92B1E429BADA2427235268D1081FAE555A78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4471[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (AlertLevel_t5F8F5FC0B6AA32540D189B555581E68F1D11282C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4472[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (AlwaysValidVerifyer_t000A0A6E4C2EEF06CBE9AD1837FB3E49B4743D19), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (BasicTlsPskIdentity_t322D6EF9036FF976DAD780F9347FE36C10CBD74E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4474[2] = 
{
	BasicTlsPskIdentity_t322D6EF9036FF976DAD780F9347FE36C10CBD74E::get_offset_of_mIdentity_0(),
	BasicTlsPskIdentity_t322D6EF9036FF976DAD780F9347FE36C10CBD74E::get_offset_of_mPsk_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { sizeof (BulkCipherAlgorithm_t113F78FC73DBA7DCCAA8DDF3E32591D7F1DDFD55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4475[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { sizeof (ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4476[5] = 
{
	0,
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65::get_offset_of_databuf_1(),
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65::get_offset_of_skipped_2(),
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65::get_offset_of_available_3(),
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65::get_offset_of_readOnlyBuf_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4477[1] = 
{
	ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A::get_offset_of_buffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (CertChainType_t539793ADC109544565B558738F85F4D22DB9245E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4478[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3), -1, sizeof(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4479[2] = 
{
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3_StaticFields::get_offset_of_EmptyChain_0(),
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3::get_offset_of_mCertificateList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4480[3] = 
{
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9::get_offset_of_mCertificateTypes_0(),
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9::get_offset_of_mSupportedSignatureAlgorithms_1(),
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9::get_offset_of_mCertificateAuthorities_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4481[2] = 
{
	CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB::get_offset_of_mStatusType_0(),
	CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB::get_offset_of_mResponse_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (CertificateStatusRequest_tDBE5889501D5B67BC974B80C5C00DF33A4145A4E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4482[2] = 
{
	CertificateStatusRequest_tDBE5889501D5B67BC974B80C5C00DF33A4145A4E::get_offset_of_mStatusType_0(),
	CertificateStatusRequest_tDBE5889501D5B67BC974B80C5C00DF33A4145A4E::get_offset_of_mRequest_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { sizeof (CertificateStatusType_tC153F2BA0E9155F3CC8BD623AA108FFDFF607F37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4483[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (CertificateType_t2A584B004ACDBC516F5BCD6637438A38F46EF3CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4484[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (CertificateUrl_t85451E742770699FC38BEA63CFAC9D00C108D2F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4485[2] = 
{
	CertificateUrl_t85451E742770699FC38BEA63CFAC9D00C108D2F2::get_offset_of_mType_0(),
	CertificateUrl_t85451E742770699FC38BEA63CFAC9D00C108D2F2::get_offset_of_mUrlAndHashList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (ListBuffer16_tD8EC21AE09532A0678CC5F3FFF315B0F6031DFAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443), -1, sizeof(Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4487[6] = 
{
	Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443_StaticFields::get_offset_of_Zeroes_0(),
	Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443::get_offset_of_context_1(),
	Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443::get_offset_of_encryptCipher_2(),
	Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443::get_offset_of_decryptCipher_3(),
	Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443::get_offset_of_encryptIV_4(),
	Chacha20Poly1305_tC6E46B8346FA054310769035DDE62290C27CA443::get_offset_of_decryptIV_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (ChangeCipherSpec_t7005E370B01454BC876664D0948FFCCE9EBF0196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4488[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (CipherSuite_tCAE444CA29C75E2CCE69848D152EE4FC056E8C01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4489[270] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { sizeof (CipherType_tBA8CD9570F8671211B881466B7C932838D4F3E91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4490[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (ClientAuthenticationType_t4F03A49120EF6169277D0112DCE477A457559C93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4491[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { sizeof (ClientCertificateType_tBF7004E3402ACC0429A5E716DA789545DB5A7D9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4492[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4493[3] = 
{
	CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1::get_offset_of_mContext_0(),
	CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1::get_offset_of_mMd5_1(),
	CombinedHash_t276E0DA7217257F403A0E7B41F4036872230ABB1::get_offset_of_mSha1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (CompressionMethod_tD3138972611839AEB88630219DE4CE301D811E97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4494[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (ConnectionEnd_t30A9692B3172BA0091016BAAABD518DE7429F720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4495[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (ContentType_t811366A63D0D56FC9A3E37674D39AA4D5B75483B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4496[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { sizeof (DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4498[4] = 
{
	DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E::get_offset_of_mCertificate_0(),
	DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E::get_offset_of_mPrivateKey_1(),
	DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E::get_offset_of_mBasicAgreement_2(),
	DefaultTlsAgreementCredentials_t47990FAE902E6F22B262DBD9D3BF85715864FC7E::get_offset_of_mTruncateAgreement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (DefaultTlsCipherFactory_tAD29BB8B1F3A60AC17ED56905A5FCB8B1EEA663B), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
