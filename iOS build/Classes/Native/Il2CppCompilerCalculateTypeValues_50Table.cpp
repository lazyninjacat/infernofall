﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null
struct Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator
struct BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter
struct PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable
struct AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser
struct AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo
struct ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser
struct ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser
struct EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo
struct OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData
struct SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMsg
struct CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest
struct CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder
struct CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.Controls
struct Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue
struct PKMacValue_t2354860B8647226235395857CE076879CE35713C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiArchiveOptions
struct PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoPrivKey
struct PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String
struct DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator
struct X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator
struct CmsAttributeTableGenerator_tA57B0778C2607AADC1C81E2F4F0B61F4C381650E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData
struct CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataGenerator
struct CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator
struct CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessable
struct CmsProcessable_tEBECBCD44AF58A497E35912A7F736AE7B740A3A5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableByteArray
struct CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsReadable
struct CmsReadable_t74C17B45A066C36E6B2280204BA27357DFD54AC5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper
struct CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore
struct RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore
struct SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.IEncryptedValuePadder
struct IEncryptedValuePadder_tF411CE6010DC98A6A588F50EEE0466CE8E431C1E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.IPKMacPrimitivesProvider
struct IPKMacPrimitivesProvider_t6467F8B2C62118D2CBD6521A8968219CF593AEB3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder
struct PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup
struct JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine
struct SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine/Parameter[]
struct ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine/UBI
struct UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine/UbiTweak
struct UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine
struct ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher
struct IBufferedCipher_tF573FBDFB2ABDB6CF7049013D91BED1EABEEAA3A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherBuilderWithKey
struct ICipherBuilderWithKey_t48C72BB27CAABF34AE3F7566E8BB619D05E7F299;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDerivationFunction
struct IDerivationFunction_t7156A3489DEA256AACADA1FE69E6C5A34CEE8493;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest
struct IDigest_t4779036D052ECF08BAC3DF71ED71EF04D7866C09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IKeyWrapper
struct IKeyWrapper_t04E00864E3E09ADE25D8EA669A75F58DC5C7CA50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac
struct IMac_tD02303ADA25C4494D9C2B119B486DF1D9302B33C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream
struct CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacSink
struct MacSink_tFAC174835C49820F15602AF337F88F6F496DC568;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISignatureFactory
struct ISignatureFactory_t1ABF092647E490BD98850BAF920E0041862DCB2A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPrivateParameters
struct MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters
struct Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PrivateKeyParameters
struct X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PrivateKeyParameters
struct X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[]
struct BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream
struct ZOutputStream_t504E5B441A273254366752284D503186AF93F77A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store
struct IX509Store_tAAF2E4C85341CF672C0779CD427B53EC4FFBED9C;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.FileInfo
struct FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt64[]
struct UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BASEDIGESTCALCULATOR_T7AE8BDE54E11200464609A898F02DB974ECCE5C5_H
#define BASEDIGESTCALCULATOR_T7AE8BDE54E11200464609A898F02DB974ECCE5C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.BaseDigestCalculator
struct  BaseDigestCalculator_t7AE8BDE54E11200464609A898F02DB974ECCE5C5  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.BaseDigestCalculator::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_0;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(BaseDigestCalculator_t7AE8BDE54E11200464609A898F02DB974ECCE5C5, ___digest_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_0() const { return ___digest_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEDIGESTCALCULATOR_T7AE8BDE54E11200464609A898F02DB974ECCE5C5_H
#ifndef CMSAUTHENVELOPEDDATA_T517E5C40E4E75E38F16766FC88C1C5F25F819978_H
#define CMSAUTHENVELOPEDDATA_T517E5C40E4E75E38F16766FC88C1C5F25F819978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData
struct  CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::recipientInfoStore
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * ___recipientInfoStore_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::contentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___contentInfo_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::originator
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * ___originator_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::authEncAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___authEncAlg_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::authAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___authAttrs_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData::unauthAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unauthAttrs_6;

public:
	inline static int32_t get_offset_of_recipientInfoStore_0() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___recipientInfoStore_0)); }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * get_recipientInfoStore_0() const { return ___recipientInfoStore_0; }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB ** get_address_of_recipientInfoStore_0() { return &___recipientInfoStore_0; }
	inline void set_recipientInfoStore_0(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * value)
	{
		___recipientInfoStore_0 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfoStore_0), value);
	}

	inline static int32_t get_offset_of_contentInfo_1() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___contentInfo_1)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_contentInfo_1() const { return ___contentInfo_1; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_contentInfo_1() { return &___contentInfo_1; }
	inline void set_contentInfo_1(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___contentInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_1), value);
	}

	inline static int32_t get_offset_of_originator_2() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___originator_2)); }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * get_originator_2() const { return ___originator_2; }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F ** get_address_of_originator_2() { return &___originator_2; }
	inline void set_originator_2(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * value)
	{
		___originator_2 = value;
		Il2CppCodeGenWriteBarrier((&___originator_2), value);
	}

	inline static int32_t get_offset_of_authEncAlg_3() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___authEncAlg_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_authEncAlg_3() const { return ___authEncAlg_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_authEncAlg_3() { return &___authEncAlg_3; }
	inline void set_authEncAlg_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___authEncAlg_3 = value;
		Il2CppCodeGenWriteBarrier((&___authEncAlg_3), value);
	}

	inline static int32_t get_offset_of_authAttrs_4() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___authAttrs_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_authAttrs_4() const { return ___authAttrs_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_authAttrs_4() { return &___authAttrs_4; }
	inline void set_authAttrs_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___authAttrs_4 = value;
		Il2CppCodeGenWriteBarrier((&___authAttrs_4), value);
	}

	inline static int32_t get_offset_of_mac_5() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___mac_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_5() const { return ___mac_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_5() { return &___mac_5; }
	inline void set_mac_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_5 = value;
		Il2CppCodeGenWriteBarrier((&___mac_5), value);
	}

	inline static int32_t get_offset_of_unauthAttrs_6() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978, ___unauthAttrs_6)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unauthAttrs_6() const { return ___unauthAttrs_6; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unauthAttrs_6() { return &___unauthAttrs_6; }
	inline void set_unauthAttrs_6(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unauthAttrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___unauthAttrs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENVELOPEDDATA_T517E5C40E4E75E38F16766FC88C1C5F25F819978_H
#ifndef AUTHENVELOPEDSECUREREADABLE_T8409506E05E8E91ABA88FB379084DBB07A081E47_H
#define AUTHENVELOPEDSECUREREADABLE_T8409506E05E8E91ABA88FB379084DBB07A081E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData_AuthEnvelopedSecureReadable
struct  AuthEnvelopedSecureReadable_t8409506E05E8E91ABA88FB379084DBB07A081E47  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedData_AuthEnvelopedSecureReadable::parent
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(AuthEnvelopedSecureReadable_t8409506E05E8E91ABA88FB379084DBB07A081E47, ___parent_0)); }
	inline CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978 * get_parent_0() const { return ___parent_0; }
	inline CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENVELOPEDSECUREREADABLE_T8409506E05E8E91ABA88FB379084DBB07A081E47_H
#ifndef CMSAUTHENVELOPEDGENERATOR_TC107F620EADFAA47D0047668DCA6C62788F23329_H
#define CMSAUTHENVELOPEDGENERATOR_TC107F620EADFAA47D0047668DCA6C62788F23329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator
struct  CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329  : public RuntimeObject
{
public:

public:
};

struct CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator::Aes128Ccm
	String_t* ___Aes128Ccm_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator::Aes192Ccm
	String_t* ___Aes192Ccm_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator::Aes256Ccm
	String_t* ___Aes256Ccm_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator::Aes128Gcm
	String_t* ___Aes128Gcm_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator::Aes192Gcm
	String_t* ___Aes192Gcm_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthEnvelopedGenerator::Aes256Gcm
	String_t* ___Aes256Gcm_5;

public:
	inline static int32_t get_offset_of_Aes128Ccm_0() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields, ___Aes128Ccm_0)); }
	inline String_t* get_Aes128Ccm_0() const { return ___Aes128Ccm_0; }
	inline String_t** get_address_of_Aes128Ccm_0() { return &___Aes128Ccm_0; }
	inline void set_Aes128Ccm_0(String_t* value)
	{
		___Aes128Ccm_0 = value;
		Il2CppCodeGenWriteBarrier((&___Aes128Ccm_0), value);
	}

	inline static int32_t get_offset_of_Aes192Ccm_1() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields, ___Aes192Ccm_1)); }
	inline String_t* get_Aes192Ccm_1() const { return ___Aes192Ccm_1; }
	inline String_t** get_address_of_Aes192Ccm_1() { return &___Aes192Ccm_1; }
	inline void set_Aes192Ccm_1(String_t* value)
	{
		___Aes192Ccm_1 = value;
		Il2CppCodeGenWriteBarrier((&___Aes192Ccm_1), value);
	}

	inline static int32_t get_offset_of_Aes256Ccm_2() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields, ___Aes256Ccm_2)); }
	inline String_t* get_Aes256Ccm_2() const { return ___Aes256Ccm_2; }
	inline String_t** get_address_of_Aes256Ccm_2() { return &___Aes256Ccm_2; }
	inline void set_Aes256Ccm_2(String_t* value)
	{
		___Aes256Ccm_2 = value;
		Il2CppCodeGenWriteBarrier((&___Aes256Ccm_2), value);
	}

	inline static int32_t get_offset_of_Aes128Gcm_3() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields, ___Aes128Gcm_3)); }
	inline String_t* get_Aes128Gcm_3() const { return ___Aes128Gcm_3; }
	inline String_t** get_address_of_Aes128Gcm_3() { return &___Aes128Gcm_3; }
	inline void set_Aes128Gcm_3(String_t* value)
	{
		___Aes128Gcm_3 = value;
		Il2CppCodeGenWriteBarrier((&___Aes128Gcm_3), value);
	}

	inline static int32_t get_offset_of_Aes192Gcm_4() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields, ___Aes192Gcm_4)); }
	inline String_t* get_Aes192Gcm_4() const { return ___Aes192Gcm_4; }
	inline String_t** get_address_of_Aes192Gcm_4() { return &___Aes192Gcm_4; }
	inline void set_Aes192Gcm_4(String_t* value)
	{
		___Aes192Gcm_4 = value;
		Il2CppCodeGenWriteBarrier((&___Aes192Gcm_4), value);
	}

	inline static int32_t get_offset_of_Aes256Gcm_5() { return static_cast<int32_t>(offsetof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields, ___Aes256Gcm_5)); }
	inline String_t* get_Aes256Gcm_5() const { return ___Aes256Gcm_5; }
	inline String_t** get_address_of_Aes256Gcm_5() { return &___Aes256Gcm_5; }
	inline void set_Aes256Gcm_5(String_t* value)
	{
		___Aes256Gcm_5 = value;
		Il2CppCodeGenWriteBarrier((&___Aes256Gcm_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENVELOPEDGENERATOR_TC107F620EADFAA47D0047668DCA6C62788F23329_H
#ifndef CMSAUTHENTICATEDDATA_T5181806CC3002A04DED8DFCDD546314EF678584F_H
#define CMSAUTHENTICATEDDATA_T5181806CC3002A04DED8DFCDD546314EF678584F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData
struct  CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData::recipientInfoStore
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * ___recipientInfoStore_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData::contentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___contentInfo_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData::macAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___macAlg_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData::authAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___authAttrs_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData::unauthAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unauthAttrs_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedData::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_5;

public:
	inline static int32_t get_offset_of_recipientInfoStore_0() { return static_cast<int32_t>(offsetof(CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F, ___recipientInfoStore_0)); }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * get_recipientInfoStore_0() const { return ___recipientInfoStore_0; }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB ** get_address_of_recipientInfoStore_0() { return &___recipientInfoStore_0; }
	inline void set_recipientInfoStore_0(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * value)
	{
		___recipientInfoStore_0 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfoStore_0), value);
	}

	inline static int32_t get_offset_of_contentInfo_1() { return static_cast<int32_t>(offsetof(CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F, ___contentInfo_1)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_contentInfo_1() const { return ___contentInfo_1; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_contentInfo_1() { return &___contentInfo_1; }
	inline void set_contentInfo_1(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___contentInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_1), value);
	}

	inline static int32_t get_offset_of_macAlg_2() { return static_cast<int32_t>(offsetof(CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F, ___macAlg_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_macAlg_2() const { return ___macAlg_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_macAlg_2() { return &___macAlg_2; }
	inline void set_macAlg_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___macAlg_2 = value;
		Il2CppCodeGenWriteBarrier((&___macAlg_2), value);
	}

	inline static int32_t get_offset_of_authAttrs_3() { return static_cast<int32_t>(offsetof(CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F, ___authAttrs_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_authAttrs_3() const { return ___authAttrs_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_authAttrs_3() { return &___authAttrs_3; }
	inline void set_authAttrs_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___authAttrs_3 = value;
		Il2CppCodeGenWriteBarrier((&___authAttrs_3), value);
	}

	inline static int32_t get_offset_of_unauthAttrs_4() { return static_cast<int32_t>(offsetof(CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F, ___unauthAttrs_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unauthAttrs_4() const { return ___unauthAttrs_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unauthAttrs_4() { return &___unauthAttrs_4; }
	inline void set_unauthAttrs_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unauthAttrs_4 = value;
		Il2CppCodeGenWriteBarrier((&___unauthAttrs_4), value);
	}

	inline static int32_t get_offset_of_mac_5() { return static_cast<int32_t>(offsetof(CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F, ___mac_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_5() const { return ___mac_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_5() { return &___mac_5; }
	inline void set_mac_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_5 = value;
		Il2CppCodeGenWriteBarrier((&___mac_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDDATA_T5181806CC3002A04DED8DFCDD546314EF678584F_H
#ifndef CMSCOMPRESSEDDATA_T2D4EB4FADD970507CD26086C016747712D781963_H
#define CMSCOMPRESSEDDATA_T2D4EB4FADD970507CD26086C016747712D781963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedData
struct  CmsCompressedData_t2D4EB4FADD970507CD26086C016747712D781963  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedData::contentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___contentInfo_0;

public:
	inline static int32_t get_offset_of_contentInfo_0() { return static_cast<int32_t>(offsetof(CmsCompressedData_t2D4EB4FADD970507CD26086C016747712D781963, ___contentInfo_0)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_contentInfo_0() const { return ___contentInfo_0; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_contentInfo_0() { return &___contentInfo_0; }
	inline void set_contentInfo_0(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___contentInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCOMPRESSEDDATA_T2D4EB4FADD970507CD26086C016747712D781963_H
#ifndef CMSCOMPRESSEDDATAGENERATOR_T6EECB1B6E90F463296C96F3F01EEF142D55DD63C_H
#define CMSCOMPRESSEDDATAGENERATOR_T6EECB1B6E90F463296C96F3F01EEF142D55DD63C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataGenerator
struct  CmsCompressedDataGenerator_t6EECB1B6E90F463296C96F3F01EEF142D55DD63C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCOMPRESSEDDATAGENERATOR_T6EECB1B6E90F463296C96F3F01EEF142D55DD63C_H
#ifndef CMSCOMPRESSEDDATASTREAMGENERATOR_TE37BBC81C03C4015F63BBB71844579A2A5FFC1FC_H
#define CMSCOMPRESSEDDATASTREAMGENERATOR_TE37BBC81C03C4015F63BBB71844579A2A5FFC1FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator
struct  CmsCompressedDataStreamGenerator_tE37BBC81C03C4015F63BBB71844579A2A5FFC1FC  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator::_bufferSize
	int32_t ____bufferSize_1;

public:
	inline static int32_t get_offset_of__bufferSize_1() { return static_cast<int32_t>(offsetof(CmsCompressedDataStreamGenerator_tE37BBC81C03C4015F63BBB71844579A2A5FFC1FC, ____bufferSize_1)); }
	inline int32_t get__bufferSize_1() const { return ____bufferSize_1; }
	inline int32_t* get_address_of__bufferSize_1() { return &____bufferSize_1; }
	inline void set__bufferSize_1(int32_t value)
	{
		____bufferSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCOMPRESSEDDATASTREAMGENERATOR_TE37BBC81C03C4015F63BBB71844579A2A5FFC1FC_H
#ifndef CMSCONTENTINFOPARSER_T1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7_H
#define CMSCONTENTINFOPARSER_T1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsContentInfoParser
struct  CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsContentInfoParser::contentInfo
	ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * ___contentInfo_0;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsContentInfoParser::data
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___data_1;

public:
	inline static int32_t get_offset_of_contentInfo_0() { return static_cast<int32_t>(offsetof(CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7, ___contentInfo_0)); }
	inline ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * get_contentInfo_0() const { return ___contentInfo_0; }
	inline ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC ** get_address_of_contentInfo_0() { return &___contentInfo_0; }
	inline void set_contentInfo_0(ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * value)
	{
		___contentInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7, ___data_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_data_1() const { return ___data_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCONTENTINFOPARSER_T1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7_H
#ifndef CMSENVELOPEDDATA_T6923656C48AADD9EF806148608738F33CD875E80_H
#define CMSENVELOPEDDATA_T6923656C48AADD9EF806148608738F33CD875E80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedData
struct  CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedData::recipientInfoStore
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * ___recipientInfoStore_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedData::contentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___contentInfo_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedData::encAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___encAlg_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedData::unprotectedAttributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unprotectedAttributes_3;

public:
	inline static int32_t get_offset_of_recipientInfoStore_0() { return static_cast<int32_t>(offsetof(CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80, ___recipientInfoStore_0)); }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * get_recipientInfoStore_0() const { return ___recipientInfoStore_0; }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB ** get_address_of_recipientInfoStore_0() { return &___recipientInfoStore_0; }
	inline void set_recipientInfoStore_0(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * value)
	{
		___recipientInfoStore_0 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfoStore_0), value);
	}

	inline static int32_t get_offset_of_contentInfo_1() { return static_cast<int32_t>(offsetof(CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80, ___contentInfo_1)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_contentInfo_1() const { return ___contentInfo_1; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_contentInfo_1() { return &___contentInfo_1; }
	inline void set_contentInfo_1(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___contentInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_1), value);
	}

	inline static int32_t get_offset_of_encAlg_2() { return static_cast<int32_t>(offsetof(CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80, ___encAlg_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_encAlg_2() const { return ___encAlg_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_encAlg_2() { return &___encAlg_2; }
	inline void set_encAlg_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___encAlg_2 = value;
		Il2CppCodeGenWriteBarrier((&___encAlg_2), value);
	}

	inline static int32_t get_offset_of_unprotectedAttributes_3() { return static_cast<int32_t>(offsetof(CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80, ___unprotectedAttributes_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unprotectedAttributes_3() const { return ___unprotectedAttributes_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unprotectedAttributes_3() { return &___unprotectedAttributes_3; }
	inline void set_unprotectedAttributes_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unprotectedAttributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___unprotectedAttributes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDDATA_T6923656C48AADD9EF806148608738F33CD875E80_H
#ifndef CMSENVELOPEDGENERATOR_TA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_H
#define CMSENVELOPEDGENERATOR_TA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator
struct  CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::recipientInfoGenerators
	RuntimeObject* ___recipientInfoGenerators_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::rand
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___rand_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::unprotectedAttributeGenerator
	RuntimeObject* ___unprotectedAttributeGenerator_24;

public:
	inline static int32_t get_offset_of_recipientInfoGenerators_22() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD, ___recipientInfoGenerators_22)); }
	inline RuntimeObject* get_recipientInfoGenerators_22() const { return ___recipientInfoGenerators_22; }
	inline RuntimeObject** get_address_of_recipientInfoGenerators_22() { return &___recipientInfoGenerators_22; }
	inline void set_recipientInfoGenerators_22(RuntimeObject* value)
	{
		___recipientInfoGenerators_22 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfoGenerators_22), value);
	}

	inline static int32_t get_offset_of_rand_23() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD, ___rand_23)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_rand_23() const { return ___rand_23; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_rand_23() { return &___rand_23; }
	inline void set_rand_23(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___rand_23 = value;
		Il2CppCodeGenWriteBarrier((&___rand_23), value);
	}

	inline static int32_t get_offset_of_unprotectedAttributeGenerator_24() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD, ___unprotectedAttributeGenerator_24)); }
	inline RuntimeObject* get_unprotectedAttributeGenerator_24() const { return ___unprotectedAttributeGenerator_24; }
	inline RuntimeObject** get_address_of_unprotectedAttributeGenerator_24() { return &___unprotectedAttributeGenerator_24; }
	inline void set_unprotectedAttributeGenerator_24(RuntimeObject* value)
	{
		___unprotectedAttributeGenerator_24 = value;
		Il2CppCodeGenWriteBarrier((&___unprotectedAttributeGenerator_24), value);
	}
};

struct CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::rc2Table
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___rc2Table_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::DesEde3Cbc
	String_t* ___DesEde3Cbc_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::RC2Cbc
	String_t* ___RC2Cbc_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Aes128Cbc
	String_t* ___Aes128Cbc_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Aes192Cbc
	String_t* ___Aes192Cbc_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Aes256Cbc
	String_t* ___Aes256Cbc_7;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Camellia128Cbc
	String_t* ___Camellia128Cbc_8;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Camellia192Cbc
	String_t* ___Camellia192Cbc_9;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Camellia256Cbc
	String_t* ___Camellia256Cbc_10;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::SeedCbc
	String_t* ___SeedCbc_11;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::DesEde3Wrap
	String_t* ___DesEde3Wrap_12;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Aes128Wrap
	String_t* ___Aes128Wrap_13;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Aes192Wrap
	String_t* ___Aes192Wrap_14;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Aes256Wrap
	String_t* ___Aes256Wrap_15;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Camellia128Wrap
	String_t* ___Camellia128Wrap_16;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Camellia192Wrap
	String_t* ___Camellia192Wrap_17;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::Camellia256Wrap
	String_t* ___Camellia256Wrap_18;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::SeedWrap
	String_t* ___SeedWrap_19;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::ECDHSha1Kdf
	String_t* ___ECDHSha1Kdf_20;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator::ECMqvSha1Kdf
	String_t* ___ECMqvSha1Kdf_21;

public:
	inline static int32_t get_offset_of_rc2Table_0() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___rc2Table_0)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_rc2Table_0() const { return ___rc2Table_0; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_rc2Table_0() { return &___rc2Table_0; }
	inline void set_rc2Table_0(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___rc2Table_0 = value;
		Il2CppCodeGenWriteBarrier((&___rc2Table_0), value);
	}

	inline static int32_t get_offset_of_DesEde3Cbc_1() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___DesEde3Cbc_1)); }
	inline String_t* get_DesEde3Cbc_1() const { return ___DesEde3Cbc_1; }
	inline String_t** get_address_of_DesEde3Cbc_1() { return &___DesEde3Cbc_1; }
	inline void set_DesEde3Cbc_1(String_t* value)
	{
		___DesEde3Cbc_1 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde3Cbc_1), value);
	}

	inline static int32_t get_offset_of_RC2Cbc_2() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___RC2Cbc_2)); }
	inline String_t* get_RC2Cbc_2() const { return ___RC2Cbc_2; }
	inline String_t** get_address_of_RC2Cbc_2() { return &___RC2Cbc_2; }
	inline void set_RC2Cbc_2(String_t* value)
	{
		___RC2Cbc_2 = value;
		Il2CppCodeGenWriteBarrier((&___RC2Cbc_2), value);
	}

	inline static int32_t get_offset_of_Aes128Cbc_5() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Aes128Cbc_5)); }
	inline String_t* get_Aes128Cbc_5() const { return ___Aes128Cbc_5; }
	inline String_t** get_address_of_Aes128Cbc_5() { return &___Aes128Cbc_5; }
	inline void set_Aes128Cbc_5(String_t* value)
	{
		___Aes128Cbc_5 = value;
		Il2CppCodeGenWriteBarrier((&___Aes128Cbc_5), value);
	}

	inline static int32_t get_offset_of_Aes192Cbc_6() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Aes192Cbc_6)); }
	inline String_t* get_Aes192Cbc_6() const { return ___Aes192Cbc_6; }
	inline String_t** get_address_of_Aes192Cbc_6() { return &___Aes192Cbc_6; }
	inline void set_Aes192Cbc_6(String_t* value)
	{
		___Aes192Cbc_6 = value;
		Il2CppCodeGenWriteBarrier((&___Aes192Cbc_6), value);
	}

	inline static int32_t get_offset_of_Aes256Cbc_7() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Aes256Cbc_7)); }
	inline String_t* get_Aes256Cbc_7() const { return ___Aes256Cbc_7; }
	inline String_t** get_address_of_Aes256Cbc_7() { return &___Aes256Cbc_7; }
	inline void set_Aes256Cbc_7(String_t* value)
	{
		___Aes256Cbc_7 = value;
		Il2CppCodeGenWriteBarrier((&___Aes256Cbc_7), value);
	}

	inline static int32_t get_offset_of_Camellia128Cbc_8() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Camellia128Cbc_8)); }
	inline String_t* get_Camellia128Cbc_8() const { return ___Camellia128Cbc_8; }
	inline String_t** get_address_of_Camellia128Cbc_8() { return &___Camellia128Cbc_8; }
	inline void set_Camellia128Cbc_8(String_t* value)
	{
		___Camellia128Cbc_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camellia128Cbc_8), value);
	}

	inline static int32_t get_offset_of_Camellia192Cbc_9() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Camellia192Cbc_9)); }
	inline String_t* get_Camellia192Cbc_9() const { return ___Camellia192Cbc_9; }
	inline String_t** get_address_of_Camellia192Cbc_9() { return &___Camellia192Cbc_9; }
	inline void set_Camellia192Cbc_9(String_t* value)
	{
		___Camellia192Cbc_9 = value;
		Il2CppCodeGenWriteBarrier((&___Camellia192Cbc_9), value);
	}

	inline static int32_t get_offset_of_Camellia256Cbc_10() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Camellia256Cbc_10)); }
	inline String_t* get_Camellia256Cbc_10() const { return ___Camellia256Cbc_10; }
	inline String_t** get_address_of_Camellia256Cbc_10() { return &___Camellia256Cbc_10; }
	inline void set_Camellia256Cbc_10(String_t* value)
	{
		___Camellia256Cbc_10 = value;
		Il2CppCodeGenWriteBarrier((&___Camellia256Cbc_10), value);
	}

	inline static int32_t get_offset_of_SeedCbc_11() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___SeedCbc_11)); }
	inline String_t* get_SeedCbc_11() const { return ___SeedCbc_11; }
	inline String_t** get_address_of_SeedCbc_11() { return &___SeedCbc_11; }
	inline void set_SeedCbc_11(String_t* value)
	{
		___SeedCbc_11 = value;
		Il2CppCodeGenWriteBarrier((&___SeedCbc_11), value);
	}

	inline static int32_t get_offset_of_DesEde3Wrap_12() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___DesEde3Wrap_12)); }
	inline String_t* get_DesEde3Wrap_12() const { return ___DesEde3Wrap_12; }
	inline String_t** get_address_of_DesEde3Wrap_12() { return &___DesEde3Wrap_12; }
	inline void set_DesEde3Wrap_12(String_t* value)
	{
		___DesEde3Wrap_12 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde3Wrap_12), value);
	}

	inline static int32_t get_offset_of_Aes128Wrap_13() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Aes128Wrap_13)); }
	inline String_t* get_Aes128Wrap_13() const { return ___Aes128Wrap_13; }
	inline String_t** get_address_of_Aes128Wrap_13() { return &___Aes128Wrap_13; }
	inline void set_Aes128Wrap_13(String_t* value)
	{
		___Aes128Wrap_13 = value;
		Il2CppCodeGenWriteBarrier((&___Aes128Wrap_13), value);
	}

	inline static int32_t get_offset_of_Aes192Wrap_14() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Aes192Wrap_14)); }
	inline String_t* get_Aes192Wrap_14() const { return ___Aes192Wrap_14; }
	inline String_t** get_address_of_Aes192Wrap_14() { return &___Aes192Wrap_14; }
	inline void set_Aes192Wrap_14(String_t* value)
	{
		___Aes192Wrap_14 = value;
		Il2CppCodeGenWriteBarrier((&___Aes192Wrap_14), value);
	}

	inline static int32_t get_offset_of_Aes256Wrap_15() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Aes256Wrap_15)); }
	inline String_t* get_Aes256Wrap_15() const { return ___Aes256Wrap_15; }
	inline String_t** get_address_of_Aes256Wrap_15() { return &___Aes256Wrap_15; }
	inline void set_Aes256Wrap_15(String_t* value)
	{
		___Aes256Wrap_15 = value;
		Il2CppCodeGenWriteBarrier((&___Aes256Wrap_15), value);
	}

	inline static int32_t get_offset_of_Camellia128Wrap_16() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Camellia128Wrap_16)); }
	inline String_t* get_Camellia128Wrap_16() const { return ___Camellia128Wrap_16; }
	inline String_t** get_address_of_Camellia128Wrap_16() { return &___Camellia128Wrap_16; }
	inline void set_Camellia128Wrap_16(String_t* value)
	{
		___Camellia128Wrap_16 = value;
		Il2CppCodeGenWriteBarrier((&___Camellia128Wrap_16), value);
	}

	inline static int32_t get_offset_of_Camellia192Wrap_17() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Camellia192Wrap_17)); }
	inline String_t* get_Camellia192Wrap_17() const { return ___Camellia192Wrap_17; }
	inline String_t** get_address_of_Camellia192Wrap_17() { return &___Camellia192Wrap_17; }
	inline void set_Camellia192Wrap_17(String_t* value)
	{
		___Camellia192Wrap_17 = value;
		Il2CppCodeGenWriteBarrier((&___Camellia192Wrap_17), value);
	}

	inline static int32_t get_offset_of_Camellia256Wrap_18() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___Camellia256Wrap_18)); }
	inline String_t* get_Camellia256Wrap_18() const { return ___Camellia256Wrap_18; }
	inline String_t** get_address_of_Camellia256Wrap_18() { return &___Camellia256Wrap_18; }
	inline void set_Camellia256Wrap_18(String_t* value)
	{
		___Camellia256Wrap_18 = value;
		Il2CppCodeGenWriteBarrier((&___Camellia256Wrap_18), value);
	}

	inline static int32_t get_offset_of_SeedWrap_19() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___SeedWrap_19)); }
	inline String_t* get_SeedWrap_19() const { return ___SeedWrap_19; }
	inline String_t** get_address_of_SeedWrap_19() { return &___SeedWrap_19; }
	inline void set_SeedWrap_19(String_t* value)
	{
		___SeedWrap_19 = value;
		Il2CppCodeGenWriteBarrier((&___SeedWrap_19), value);
	}

	inline static int32_t get_offset_of_ECDHSha1Kdf_20() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___ECDHSha1Kdf_20)); }
	inline String_t* get_ECDHSha1Kdf_20() const { return ___ECDHSha1Kdf_20; }
	inline String_t** get_address_of_ECDHSha1Kdf_20() { return &___ECDHSha1Kdf_20; }
	inline void set_ECDHSha1Kdf_20(String_t* value)
	{
		___ECDHSha1Kdf_20 = value;
		Il2CppCodeGenWriteBarrier((&___ECDHSha1Kdf_20), value);
	}

	inline static int32_t get_offset_of_ECMqvSha1Kdf_21() { return static_cast<int32_t>(offsetof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields, ___ECMqvSha1Kdf_21)); }
	inline String_t* get_ECMqvSha1Kdf_21() const { return ___ECMqvSha1Kdf_21; }
	inline String_t** get_address_of_ECMqvSha1Kdf_21() { return &___ECMqvSha1Kdf_21; }
	inline void set_ECMqvSha1Kdf_21(String_t* value)
	{
		___ECMqvSha1Kdf_21 = value;
		Il2CppCodeGenWriteBarrier((&___ECMqvSha1Kdf_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDGENERATOR_TA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_H
#ifndef CMSENVELOPEDHELPER_T76D73BA581D300AE36633AA416596DA31C391954_H
#define CMSENVELOPEDHELPER_T76D73BA581D300AE36633AA416596DA31C391954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper
struct  CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954  : public RuntimeObject
{
public:

public:
};

struct CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper::Instance
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * ___Instance_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper::KeySizes
	RuntimeObject* ___KeySizes_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper::BaseCipherNames
	RuntimeObject* ___BaseCipherNames_2;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields, ___Instance_0)); }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * get_Instance_0() const { return ___Instance_0; }
	inline CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}

	inline static int32_t get_offset_of_KeySizes_1() { return static_cast<int32_t>(offsetof(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields, ___KeySizes_1)); }
	inline RuntimeObject* get_KeySizes_1() const { return ___KeySizes_1; }
	inline RuntimeObject** get_address_of_KeySizes_1() { return &___KeySizes_1; }
	inline void set_KeySizes_1(RuntimeObject* value)
	{
		___KeySizes_1 = value;
		Il2CppCodeGenWriteBarrier((&___KeySizes_1), value);
	}

	inline static int32_t get_offset_of_BaseCipherNames_2() { return static_cast<int32_t>(offsetof(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields, ___BaseCipherNames_2)); }
	inline RuntimeObject* get_BaseCipherNames_2() const { return ___BaseCipherNames_2; }
	inline RuntimeObject** get_address_of_BaseCipherNames_2() { return &___BaseCipherNames_2; }
	inline void set_BaseCipherNames_2(RuntimeObject* value)
	{
		___BaseCipherNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___BaseCipherNames_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDHELPER_T76D73BA581D300AE36633AA416596DA31C391954_H
#ifndef CMSAUTHENTICATEDSECUREREADABLE_T64AD71AA236427A121C83DAD223B2EF05777A541_H
#define CMSAUTHENTICATEDSECUREREADABLE_T64AD71AA236427A121C83DAD223B2EF05777A541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsAuthenticatedSecureReadable
struct  CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsAuthenticatedSecureReadable::algorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsAuthenticatedSecureReadable::mac
	RuntimeObject* ___mac_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsReadable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsAuthenticatedSecureReadable::readable
	RuntimeObject* ___readable_2;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541, ___algorithm_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algorithm_0() const { return ___algorithm_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_mac_1() { return static_cast<int32_t>(offsetof(CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541, ___mac_1)); }
	inline RuntimeObject* get_mac_1() const { return ___mac_1; }
	inline RuntimeObject** get_address_of_mac_1() { return &___mac_1; }
	inline void set_mac_1(RuntimeObject* value)
	{
		___mac_1 = value;
		Il2CppCodeGenWriteBarrier((&___mac_1), value);
	}

	inline static int32_t get_offset_of_readable_2() { return static_cast<int32_t>(offsetof(CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541, ___readable_2)); }
	inline RuntimeObject* get_readable_2() const { return ___readable_2; }
	inline RuntimeObject** get_address_of_readable_2() { return &___readable_2; }
	inline void set_readable_2(RuntimeObject* value)
	{
		___readable_2 = value;
		Il2CppCodeGenWriteBarrier((&___readable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDSECUREREADABLE_T64AD71AA236427A121C83DAD223B2EF05777A541_H
#ifndef CMSENVELOPEDSECUREREADABLE_TA71BDD5AD9AA299BC8C62CFCDE15C18C01625782_H
#define CMSENVELOPEDSECUREREADABLE_TA71BDD5AD9AA299BC8C62CFCDE15C18C01625782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsEnvelopedSecureReadable
struct  CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsEnvelopedSecureReadable::algorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsEnvelopedSecureReadable::cipher
	RuntimeObject* ___cipher_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsReadable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedHelper_CmsEnvelopedSecureReadable::readable
	RuntimeObject* ___readable_2;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782, ___algorithm_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algorithm_0() const { return ___algorithm_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}

	inline static int32_t get_offset_of_readable_2() { return static_cast<int32_t>(offsetof(CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782, ___readable_2)); }
	inline RuntimeObject* get_readable_2() const { return ___readable_2; }
	inline RuntimeObject** get_address_of_readable_2() { return &___readable_2; }
	inline void set_readable_2(RuntimeObject* value)
	{
		___readable_2 = value;
		Il2CppCodeGenWriteBarrier((&___readable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDSECUREREADABLE_TA71BDD5AD9AA299BC8C62CFCDE15C18C01625782_H
#ifndef CMSPBEKEY_TADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1_H
#define CMSPBEKEY_TADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey
struct  CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1  : public RuntimeObject
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey::password
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___password_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsPbeKey::iterationCount
	int32_t ___iterationCount_2;

public:
	inline static int32_t get_offset_of_password_0() { return static_cast<int32_t>(offsetof(CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1, ___password_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_password_0() const { return ___password_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_password_0() { return &___password_0; }
	inline void set_password_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___password_0 = value;
		Il2CppCodeGenWriteBarrier((&___password_0), value);
	}

	inline static int32_t get_offset_of_salt_1() { return static_cast<int32_t>(offsetof(CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1, ___salt_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_1() const { return ___salt_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_1() { return &___salt_1; }
	inline void set_salt_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_1 = value;
		Il2CppCodeGenWriteBarrier((&___salt_1), value);
	}

	inline static int32_t get_offset_of_iterationCount_2() { return static_cast<int32_t>(offsetof(CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1, ___iterationCount_2)); }
	inline int32_t get_iterationCount_2() const { return ___iterationCount_2; }
	inline int32_t* get_address_of_iterationCount_2() { return &___iterationCount_2; }
	inline void set_iterationCount_2(int32_t value)
	{
		___iterationCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSPBEKEY_TADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1_H
#ifndef CMSPROCESSABLEBYTEARRAY_TF0A4887E18B016A79DF99F6EDA0D8598F91F58BF_H
#define CMSPROCESSABLEBYTEARRAY_TF0A4887E18B016A79DF99F6EDA0D8598F91F58BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableByteArray
struct  CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableByteArray::type
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___type_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableByteArray::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF, ___type_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_type_0() const { return ___type_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF, ___bytes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSPROCESSABLEBYTEARRAY_TF0A4887E18B016A79DF99F6EDA0D8598F91F58BF_H
#ifndef CMSPROCESSABLEFILE_TEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842_H
#define CMSPROCESSABLEFILE_TEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableFile
struct  CmsProcessableFile_tEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842  : public RuntimeObject
{
public:
	// System.IO.FileInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableFile::_file
	FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * ____file_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableFile::_bufSize
	int32_t ____bufSize_2;

public:
	inline static int32_t get_offset_of__file_1() { return static_cast<int32_t>(offsetof(CmsProcessableFile_tEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842, ____file_1)); }
	inline FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * get__file_1() const { return ____file_1; }
	inline FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C ** get_address_of__file_1() { return &____file_1; }
	inline void set__file_1(FileInfo_tF8C1D41E1ABDAC19BF4F76A491DD28DD8DBEE35C * value)
	{
		____file_1 = value;
		Il2CppCodeGenWriteBarrier((&____file_1), value);
	}

	inline static int32_t get_offset_of__bufSize_2() { return static_cast<int32_t>(offsetof(CmsProcessableFile_tEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842, ____bufSize_2)); }
	inline int32_t get__bufSize_2() const { return ____bufSize_2; }
	inline int32_t* get_address_of__bufSize_2() { return &____bufSize_2; }
	inline void set__bufSize_2(int32_t value)
	{
		____bufSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSPROCESSABLEFILE_TEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842_H
#ifndef CMSPROCESSABLEINPUTSTREAM_T1A01AA3EF483F067E9CEB332995A131B0D453FED_H
#define CMSPROCESSABLEINPUTSTREAM_T1A01AA3EF483F067E9CEB332995A131B0D453FED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableInputStream
struct  CmsProcessableInputStream_t1A01AA3EF483F067E9CEB332995A131B0D453FED  : public RuntimeObject
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableInputStream::input
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableInputStream::used
	bool ___used_1;

public:
	inline static int32_t get_offset_of_input_0() { return static_cast<int32_t>(offsetof(CmsProcessableInputStream_t1A01AA3EF483F067E9CEB332995A131B0D453FED, ___input_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_input_0() const { return ___input_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_input_0() { return &___input_0; }
	inline void set_input_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___input_0 = value;
		Il2CppCodeGenWriteBarrier((&___input_0), value);
	}

	inline static int32_t get_offset_of_used_1() { return static_cast<int32_t>(offsetof(CmsProcessableInputStream_t1A01AA3EF483F067E9CEB332995A131B0D453FED, ___used_1)); }
	inline bool get_used_1() const { return ___used_1; }
	inline bool* get_address_of_used_1() { return &___used_1; }
	inline void set_used_1(bool value)
	{
		___used_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSPROCESSABLEINPUTSTREAM_T1A01AA3EF483F067E9CEB332995A131B0D453FED_H
#ifndef CMSSIGNEDDATA_T37800D8662D7471F6AD429001166DD10EBBFEEC0_H
#define CMSSIGNEDDATA_T37800D8662D7471F6AD429001166DD10EBBFEEC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData
struct  CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::signedContent
	RuntimeObject* ___signedContent_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.SignedData BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::signedData
	SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F * ___signedData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::contentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___contentInfo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::signerInfoStore
	SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 * ___signerInfoStore_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::attrCertStore
	RuntimeObject* ___attrCertStore_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::certificateStore
	RuntimeObject* ___certificateStore_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::crlStore
	RuntimeObject* ___crlStore_7;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::hashes
	RuntimeObject* ___hashes_8;

public:
	inline static int32_t get_offset_of_signedContent_1() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___signedContent_1)); }
	inline RuntimeObject* get_signedContent_1() const { return ___signedContent_1; }
	inline RuntimeObject** get_address_of_signedContent_1() { return &___signedContent_1; }
	inline void set_signedContent_1(RuntimeObject* value)
	{
		___signedContent_1 = value;
		Il2CppCodeGenWriteBarrier((&___signedContent_1), value);
	}

	inline static int32_t get_offset_of_signedData_2() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___signedData_2)); }
	inline SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F * get_signedData_2() const { return ___signedData_2; }
	inline SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F ** get_address_of_signedData_2() { return &___signedData_2; }
	inline void set_signedData_2(SignedData_tCE4B70333E8EEED311B4A8DA5AB7ECF8C2F91A1F * value)
	{
		___signedData_2 = value;
		Il2CppCodeGenWriteBarrier((&___signedData_2), value);
	}

	inline static int32_t get_offset_of_contentInfo_3() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___contentInfo_3)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_contentInfo_3() const { return ___contentInfo_3; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_contentInfo_3() { return &___contentInfo_3; }
	inline void set_contentInfo_3(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___contentInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_3), value);
	}

	inline static int32_t get_offset_of_signerInfoStore_4() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___signerInfoStore_4)); }
	inline SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 * get_signerInfoStore_4() const { return ___signerInfoStore_4; }
	inline SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 ** get_address_of_signerInfoStore_4() { return &___signerInfoStore_4; }
	inline void set_signerInfoStore_4(SignerInformationStore_t048803C47E6447E8380F2410432F7371601A6575 * value)
	{
		___signerInfoStore_4 = value;
		Il2CppCodeGenWriteBarrier((&___signerInfoStore_4), value);
	}

	inline static int32_t get_offset_of_attrCertStore_5() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___attrCertStore_5)); }
	inline RuntimeObject* get_attrCertStore_5() const { return ___attrCertStore_5; }
	inline RuntimeObject** get_address_of_attrCertStore_5() { return &___attrCertStore_5; }
	inline void set_attrCertStore_5(RuntimeObject* value)
	{
		___attrCertStore_5 = value;
		Il2CppCodeGenWriteBarrier((&___attrCertStore_5), value);
	}

	inline static int32_t get_offset_of_certificateStore_6() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___certificateStore_6)); }
	inline RuntimeObject* get_certificateStore_6() const { return ___certificateStore_6; }
	inline RuntimeObject** get_address_of_certificateStore_6() { return &___certificateStore_6; }
	inline void set_certificateStore_6(RuntimeObject* value)
	{
		___certificateStore_6 = value;
		Il2CppCodeGenWriteBarrier((&___certificateStore_6), value);
	}

	inline static int32_t get_offset_of_crlStore_7() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___crlStore_7)); }
	inline RuntimeObject* get_crlStore_7() const { return ___crlStore_7; }
	inline RuntimeObject** get_address_of_crlStore_7() { return &___crlStore_7; }
	inline void set_crlStore_7(RuntimeObject* value)
	{
		___crlStore_7 = value;
		Il2CppCodeGenWriteBarrier((&___crlStore_7), value);
	}

	inline static int32_t get_offset_of_hashes_8() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0, ___hashes_8)); }
	inline RuntimeObject* get_hashes_8() const { return ___hashes_8; }
	inline RuntimeObject** get_address_of_hashes_8() { return &___hashes_8; }
	inline void set_hashes_8(RuntimeObject* value)
	{
		___hashes_8 = value;
		Il2CppCodeGenWriteBarrier((&___hashes_8), value);
	}
};

struct CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData::Helper
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * ___Helper_0;

public:
	inline static int32_t get_offset_of_Helper_0() { return static_cast<int32_t>(offsetof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0_StaticFields, ___Helper_0)); }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * get_Helper_0() const { return ___Helper_0; }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF ** get_address_of_Helper_0() { return &___Helper_0; }
	inline void set_Helper_0(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * value)
	{
		___Helper_0 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDDATA_T37800D8662D7471F6AD429001166DD10EBBFEEC0_H
#ifndef CMSSIGNEDGENERATOR_TC85671C8234B3E8E8176C2D0A5495A53265A2D44_H
#define CMSSIGNEDGENERATOR_TC85671C8234B3E8E8176C2D0A5495A53265A2D44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator
struct  CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_certs
	RuntimeObject* ____certs_17;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_crls
	RuntimeObject* ____crls_18;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_signers
	RuntimeObject* ____signers_19;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_digests
	RuntimeObject* ____digests_20;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_useDerForCerts
	bool ____useDerForCerts_21;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::_useDerForCrls
	bool ____useDerForCrls_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::rand
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___rand_23;

public:
	inline static int32_t get_offset_of__certs_17() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____certs_17)); }
	inline RuntimeObject* get__certs_17() const { return ____certs_17; }
	inline RuntimeObject** get_address_of__certs_17() { return &____certs_17; }
	inline void set__certs_17(RuntimeObject* value)
	{
		____certs_17 = value;
		Il2CppCodeGenWriteBarrier((&____certs_17), value);
	}

	inline static int32_t get_offset_of__crls_18() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____crls_18)); }
	inline RuntimeObject* get__crls_18() const { return ____crls_18; }
	inline RuntimeObject** get_address_of__crls_18() { return &____crls_18; }
	inline void set__crls_18(RuntimeObject* value)
	{
		____crls_18 = value;
		Il2CppCodeGenWriteBarrier((&____crls_18), value);
	}

	inline static int32_t get_offset_of__signers_19() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____signers_19)); }
	inline RuntimeObject* get__signers_19() const { return ____signers_19; }
	inline RuntimeObject** get_address_of__signers_19() { return &____signers_19; }
	inline void set__signers_19(RuntimeObject* value)
	{
		____signers_19 = value;
		Il2CppCodeGenWriteBarrier((&____signers_19), value);
	}

	inline static int32_t get_offset_of__digests_20() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____digests_20)); }
	inline RuntimeObject* get__digests_20() const { return ____digests_20; }
	inline RuntimeObject** get_address_of__digests_20() { return &____digests_20; }
	inline void set__digests_20(RuntimeObject* value)
	{
		____digests_20 = value;
		Il2CppCodeGenWriteBarrier((&____digests_20), value);
	}

	inline static int32_t get_offset_of__useDerForCerts_21() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____useDerForCerts_21)); }
	inline bool get__useDerForCerts_21() const { return ____useDerForCerts_21; }
	inline bool* get_address_of__useDerForCerts_21() { return &____useDerForCerts_21; }
	inline void set__useDerForCerts_21(bool value)
	{
		____useDerForCerts_21 = value;
	}

	inline static int32_t get_offset_of__useDerForCrls_22() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ____useDerForCrls_22)); }
	inline bool get__useDerForCrls_22() const { return ____useDerForCrls_22; }
	inline bool* get_address_of__useDerForCrls_22() { return &____useDerForCrls_22; }
	inline void set__useDerForCrls_22(bool value)
	{
		____useDerForCrls_22 = value;
	}

	inline static int32_t get_offset_of_rand_23() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44, ___rand_23)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_rand_23() const { return ___rand_23; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_rand_23() { return &___rand_23; }
	inline void set_rand_23(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___rand_23 = value;
		Il2CppCodeGenWriteBarrier((&___rand_23), value);
	}
};

struct CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::Data
	String_t* ___Data_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha1
	String_t* ___DigestSha1_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha224
	String_t* ___DigestSha224_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha256
	String_t* ___DigestSha256_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha384
	String_t* ___DigestSha384_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestSha512
	String_t* ___DigestSha512_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestMD5
	String_t* ___DigestMD5_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestGost3411
	String_t* ___DigestGost3411_7;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestRipeMD128
	String_t* ___DigestRipeMD128_8;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestRipeMD160
	String_t* ___DigestRipeMD160_9;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::DigestRipeMD256
	String_t* ___DigestRipeMD256_10;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionRsa
	String_t* ___EncryptionRsa_11;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionDsa
	String_t* ___EncryptionDsa_12;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionECDsa
	String_t* ___EncryptionECDsa_13;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionRsaPss
	String_t* ___EncryptionRsaPss_14;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionGost3410
	String_t* ___EncryptionGost3410_15;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedGenerator::EncryptionECGost3410
	String_t* ___EncryptionECGost3410_16;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___Data_0)); }
	inline String_t* get_Data_0() const { return ___Data_0; }
	inline String_t** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(String_t* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_DigestSha1_1() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha1_1)); }
	inline String_t* get_DigestSha1_1() const { return ___DigestSha1_1; }
	inline String_t** get_address_of_DigestSha1_1() { return &___DigestSha1_1; }
	inline void set_DigestSha1_1(String_t* value)
	{
		___DigestSha1_1 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha1_1), value);
	}

	inline static int32_t get_offset_of_DigestSha224_2() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha224_2)); }
	inline String_t* get_DigestSha224_2() const { return ___DigestSha224_2; }
	inline String_t** get_address_of_DigestSha224_2() { return &___DigestSha224_2; }
	inline void set_DigestSha224_2(String_t* value)
	{
		___DigestSha224_2 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha224_2), value);
	}

	inline static int32_t get_offset_of_DigestSha256_3() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha256_3)); }
	inline String_t* get_DigestSha256_3() const { return ___DigestSha256_3; }
	inline String_t** get_address_of_DigestSha256_3() { return &___DigestSha256_3; }
	inline void set_DigestSha256_3(String_t* value)
	{
		___DigestSha256_3 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha256_3), value);
	}

	inline static int32_t get_offset_of_DigestSha384_4() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha384_4)); }
	inline String_t* get_DigestSha384_4() const { return ___DigestSha384_4; }
	inline String_t** get_address_of_DigestSha384_4() { return &___DigestSha384_4; }
	inline void set_DigestSha384_4(String_t* value)
	{
		___DigestSha384_4 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha384_4), value);
	}

	inline static int32_t get_offset_of_DigestSha512_5() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestSha512_5)); }
	inline String_t* get_DigestSha512_5() const { return ___DigestSha512_5; }
	inline String_t** get_address_of_DigestSha512_5() { return &___DigestSha512_5; }
	inline void set_DigestSha512_5(String_t* value)
	{
		___DigestSha512_5 = value;
		Il2CppCodeGenWriteBarrier((&___DigestSha512_5), value);
	}

	inline static int32_t get_offset_of_DigestMD5_6() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestMD5_6)); }
	inline String_t* get_DigestMD5_6() const { return ___DigestMD5_6; }
	inline String_t** get_address_of_DigestMD5_6() { return &___DigestMD5_6; }
	inline void set_DigestMD5_6(String_t* value)
	{
		___DigestMD5_6 = value;
		Il2CppCodeGenWriteBarrier((&___DigestMD5_6), value);
	}

	inline static int32_t get_offset_of_DigestGost3411_7() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestGost3411_7)); }
	inline String_t* get_DigestGost3411_7() const { return ___DigestGost3411_7; }
	inline String_t** get_address_of_DigestGost3411_7() { return &___DigestGost3411_7; }
	inline void set_DigestGost3411_7(String_t* value)
	{
		___DigestGost3411_7 = value;
		Il2CppCodeGenWriteBarrier((&___DigestGost3411_7), value);
	}

	inline static int32_t get_offset_of_DigestRipeMD128_8() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestRipeMD128_8)); }
	inline String_t* get_DigestRipeMD128_8() const { return ___DigestRipeMD128_8; }
	inline String_t** get_address_of_DigestRipeMD128_8() { return &___DigestRipeMD128_8; }
	inline void set_DigestRipeMD128_8(String_t* value)
	{
		___DigestRipeMD128_8 = value;
		Il2CppCodeGenWriteBarrier((&___DigestRipeMD128_8), value);
	}

	inline static int32_t get_offset_of_DigestRipeMD160_9() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestRipeMD160_9)); }
	inline String_t* get_DigestRipeMD160_9() const { return ___DigestRipeMD160_9; }
	inline String_t** get_address_of_DigestRipeMD160_9() { return &___DigestRipeMD160_9; }
	inline void set_DigestRipeMD160_9(String_t* value)
	{
		___DigestRipeMD160_9 = value;
		Il2CppCodeGenWriteBarrier((&___DigestRipeMD160_9), value);
	}

	inline static int32_t get_offset_of_DigestRipeMD256_10() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___DigestRipeMD256_10)); }
	inline String_t* get_DigestRipeMD256_10() const { return ___DigestRipeMD256_10; }
	inline String_t** get_address_of_DigestRipeMD256_10() { return &___DigestRipeMD256_10; }
	inline void set_DigestRipeMD256_10(String_t* value)
	{
		___DigestRipeMD256_10 = value;
		Il2CppCodeGenWriteBarrier((&___DigestRipeMD256_10), value);
	}

	inline static int32_t get_offset_of_EncryptionRsa_11() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionRsa_11)); }
	inline String_t* get_EncryptionRsa_11() const { return ___EncryptionRsa_11; }
	inline String_t** get_address_of_EncryptionRsa_11() { return &___EncryptionRsa_11; }
	inline void set_EncryptionRsa_11(String_t* value)
	{
		___EncryptionRsa_11 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionRsa_11), value);
	}

	inline static int32_t get_offset_of_EncryptionDsa_12() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionDsa_12)); }
	inline String_t* get_EncryptionDsa_12() const { return ___EncryptionDsa_12; }
	inline String_t** get_address_of_EncryptionDsa_12() { return &___EncryptionDsa_12; }
	inline void set_EncryptionDsa_12(String_t* value)
	{
		___EncryptionDsa_12 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionDsa_12), value);
	}

	inline static int32_t get_offset_of_EncryptionECDsa_13() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionECDsa_13)); }
	inline String_t* get_EncryptionECDsa_13() const { return ___EncryptionECDsa_13; }
	inline String_t** get_address_of_EncryptionECDsa_13() { return &___EncryptionECDsa_13; }
	inline void set_EncryptionECDsa_13(String_t* value)
	{
		___EncryptionECDsa_13 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECDsa_13), value);
	}

	inline static int32_t get_offset_of_EncryptionRsaPss_14() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionRsaPss_14)); }
	inline String_t* get_EncryptionRsaPss_14() const { return ___EncryptionRsaPss_14; }
	inline String_t** get_address_of_EncryptionRsaPss_14() { return &___EncryptionRsaPss_14; }
	inline void set_EncryptionRsaPss_14(String_t* value)
	{
		___EncryptionRsaPss_14 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionRsaPss_14), value);
	}

	inline static int32_t get_offset_of_EncryptionGost3410_15() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionGost3410_15)); }
	inline String_t* get_EncryptionGost3410_15() const { return ___EncryptionGost3410_15; }
	inline String_t** get_address_of_EncryptionGost3410_15() { return &___EncryptionGost3410_15; }
	inline void set_EncryptionGost3410_15(String_t* value)
	{
		___EncryptionGost3410_15 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionGost3410_15), value);
	}

	inline static int32_t get_offset_of_EncryptionECGost3410_16() { return static_cast<int32_t>(offsetof(CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44_StaticFields, ___EncryptionECGost3410_16)); }
	inline String_t* get_EncryptionECGost3410_16() const { return ___EncryptionECGost3410_16; }
	inline String_t** get_address_of_EncryptionECGost3410_16() { return &___EncryptionECGost3410_16; }
	inline void set_EncryptionECGost3410_16(String_t* value)
	{
		___EncryptionECGost3410_16 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionECGost3410_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDGENERATOR_TC85671C8234B3E8E8176C2D0A5495A53265A2D44_H
#ifndef AUTHENTICATORCONTROL_T1EBBEB4478301069DE7ABF43A9A6EEF15824584B_H
#define AUTHENTICATORCONTROL_T1EBBEB4478301069DE7ABF43A9A6EEF15824584B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.AuthenticatorControl
struct  AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.AuthenticatorControl::token
	DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * ___token_1;

public:
	inline static int32_t get_offset_of_token_1() { return static_cast<int32_t>(offsetof(AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B, ___token_1)); }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * get_token_1() const { return ___token_1; }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 ** get_address_of_token_1() { return &___token_1; }
	inline void set_token_1(DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * value)
	{
		___token_1 = value;
		Il2CppCodeGenWriteBarrier((&___token_1), value);
	}
};

struct AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.AuthenticatorControl::type
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B_StaticFields, ___type_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_type_0() const { return ___type_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATORCONTROL_T1EBBEB4478301069DE7ABF43A9A6EEF15824584B_H
#ifndef CERTIFICATEREQUESTMESSAGE_T6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_H
#define CERTIFICATEREQUESTMESSAGE_T6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage
struct  CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMsg BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage::certReqMsg
	CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E * ___certReqMsg_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.Controls BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage::controls
	Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B * ___controls_5;

public:
	inline static int32_t get_offset_of_certReqMsg_4() { return static_cast<int32_t>(offsetof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F, ___certReqMsg_4)); }
	inline CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E * get_certReqMsg_4() const { return ___certReqMsg_4; }
	inline CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E ** get_address_of_certReqMsg_4() { return &___certReqMsg_4; }
	inline void set_certReqMsg_4(CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E * value)
	{
		___certReqMsg_4 = value;
		Il2CppCodeGenWriteBarrier((&___certReqMsg_4), value);
	}

	inline static int32_t get_offset_of_controls_5() { return static_cast<int32_t>(offsetof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F, ___controls_5)); }
	inline Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B * get_controls_5() const { return ___controls_5; }
	inline Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B ** get_address_of_controls_5() { return &___controls_5; }
	inline void set_controls_5(Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B * value)
	{
		___controls_5 = value;
		Il2CppCodeGenWriteBarrier((&___controls_5), value);
	}
};

struct CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage::popRaVerified
	int32_t ___popRaVerified_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage::popSigningKey
	int32_t ___popSigningKey_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage::popKeyEncipherment
	int32_t ___popKeyEncipherment_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessage::popKeyAgreement
	int32_t ___popKeyAgreement_3;

public:
	inline static int32_t get_offset_of_popRaVerified_0() { return static_cast<int32_t>(offsetof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields, ___popRaVerified_0)); }
	inline int32_t get_popRaVerified_0() const { return ___popRaVerified_0; }
	inline int32_t* get_address_of_popRaVerified_0() { return &___popRaVerified_0; }
	inline void set_popRaVerified_0(int32_t value)
	{
		___popRaVerified_0 = value;
	}

	inline static int32_t get_offset_of_popSigningKey_1() { return static_cast<int32_t>(offsetof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields, ___popSigningKey_1)); }
	inline int32_t get_popSigningKey_1() const { return ___popSigningKey_1; }
	inline int32_t* get_address_of_popSigningKey_1() { return &___popSigningKey_1; }
	inline void set_popSigningKey_1(int32_t value)
	{
		___popSigningKey_1 = value;
	}

	inline static int32_t get_offset_of_popKeyEncipherment_2() { return static_cast<int32_t>(offsetof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields, ___popKeyEncipherment_2)); }
	inline int32_t get_popKeyEncipherment_2() const { return ___popKeyEncipherment_2; }
	inline int32_t* get_address_of_popKeyEncipherment_2() { return &___popKeyEncipherment_2; }
	inline void set_popKeyEncipherment_2(int32_t value)
	{
		___popKeyEncipherment_2 = value;
	}

	inline static int32_t get_offset_of_popKeyAgreement_3() { return static_cast<int32_t>(offsetof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields, ___popKeyAgreement_3)); }
	inline int32_t get_popKeyAgreement_3() const { return ___popKeyAgreement_3; }
	inline int32_t* get_address_of_popKeyAgreement_3() { return &___popKeyAgreement_3; }
	inline void set_popKeyAgreement_3(int32_t value)
	{
		___popKeyAgreement_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEREQUESTMESSAGE_T6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_H
#ifndef CERTIFICATEREQUESTMESSAGEBUILDER_TA52FA4B71621C2A988476C07CD13CBE246466016_H
#define CERTIFICATEREQUESTMESSAGEBUILDER_TA52FA4B71621C2A988476C07CD13CBE246466016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder
struct  CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_certReqId
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ____certReqId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509ExtensionsGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_extGenerator
	X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * ____extGenerator_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_templateBuilder
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363 * ____templateBuilder_2;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_controls
	RuntimeObject* ____controls_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISignatureFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_popSigner
	RuntimeObject* ____popSigner_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_pkMacBuilder
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492 * ____pkMacBuilder_5;
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_password
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____password_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_sender
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ____sender_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_popoType
	int32_t ____popoType_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoPrivKey BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_popoPrivKey
	PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95 * ____popoPrivKey_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_popRaVerified
	Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * ____popRaVerified_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CertificateRequestMessageBuilder::_agreeMac
	PKMacValue_t2354860B8647226235395857CE076879CE35713C * ____agreeMac_11;

public:
	inline static int32_t get_offset_of__certReqId_0() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____certReqId_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get__certReqId_0() const { return ____certReqId_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of__certReqId_0() { return &____certReqId_0; }
	inline void set__certReqId_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		____certReqId_0 = value;
		Il2CppCodeGenWriteBarrier((&____certReqId_0), value);
	}

	inline static int32_t get_offset_of__extGenerator_1() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____extGenerator_1)); }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * get__extGenerator_1() const { return ____extGenerator_1; }
	inline X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 ** get_address_of__extGenerator_1() { return &____extGenerator_1; }
	inline void set__extGenerator_1(X509ExtensionsGenerator_t788E3FE4DF38CC565DDEE0B6D23F2A611C604E33 * value)
	{
		____extGenerator_1 = value;
		Il2CppCodeGenWriteBarrier((&____extGenerator_1), value);
	}

	inline static int32_t get_offset_of__templateBuilder_2() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____templateBuilder_2)); }
	inline CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363 * get__templateBuilder_2() const { return ____templateBuilder_2; }
	inline CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363 ** get_address_of__templateBuilder_2() { return &____templateBuilder_2; }
	inline void set__templateBuilder_2(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363 * value)
	{
		____templateBuilder_2 = value;
		Il2CppCodeGenWriteBarrier((&____templateBuilder_2), value);
	}

	inline static int32_t get_offset_of__controls_3() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____controls_3)); }
	inline RuntimeObject* get__controls_3() const { return ____controls_3; }
	inline RuntimeObject** get_address_of__controls_3() { return &____controls_3; }
	inline void set__controls_3(RuntimeObject* value)
	{
		____controls_3 = value;
		Il2CppCodeGenWriteBarrier((&____controls_3), value);
	}

	inline static int32_t get_offset_of__popSigner_4() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____popSigner_4)); }
	inline RuntimeObject* get__popSigner_4() const { return ____popSigner_4; }
	inline RuntimeObject** get_address_of__popSigner_4() { return &____popSigner_4; }
	inline void set__popSigner_4(RuntimeObject* value)
	{
		____popSigner_4 = value;
		Il2CppCodeGenWriteBarrier((&____popSigner_4), value);
	}

	inline static int32_t get_offset_of__pkMacBuilder_5() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____pkMacBuilder_5)); }
	inline PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492 * get__pkMacBuilder_5() const { return ____pkMacBuilder_5; }
	inline PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492 ** get_address_of__pkMacBuilder_5() { return &____pkMacBuilder_5; }
	inline void set__pkMacBuilder_5(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492 * value)
	{
		____pkMacBuilder_5 = value;
		Il2CppCodeGenWriteBarrier((&____pkMacBuilder_5), value);
	}

	inline static int32_t get_offset_of__password_6() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____password_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__password_6() const { return ____password_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__password_6() { return &____password_6; }
	inline void set__password_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____password_6 = value;
		Il2CppCodeGenWriteBarrier((&____password_6), value);
	}

	inline static int32_t get_offset_of__sender_7() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____sender_7)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get__sender_7() const { return ____sender_7; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of__sender_7() { return &____sender_7; }
	inline void set__sender_7(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		____sender_7 = value;
		Il2CppCodeGenWriteBarrier((&____sender_7), value);
	}

	inline static int32_t get_offset_of__popoType_8() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____popoType_8)); }
	inline int32_t get__popoType_8() const { return ____popoType_8; }
	inline int32_t* get_address_of__popoType_8() { return &____popoType_8; }
	inline void set__popoType_8(int32_t value)
	{
		____popoType_8 = value;
	}

	inline static int32_t get_offset_of__popoPrivKey_9() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____popoPrivKey_9)); }
	inline PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95 * get__popoPrivKey_9() const { return ____popoPrivKey_9; }
	inline PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95 ** get_address_of__popoPrivKey_9() { return &____popoPrivKey_9; }
	inline void set__popoPrivKey_9(PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95 * value)
	{
		____popoPrivKey_9 = value;
		Il2CppCodeGenWriteBarrier((&____popoPrivKey_9), value);
	}

	inline static int32_t get_offset_of__popRaVerified_10() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____popRaVerified_10)); }
	inline Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * get__popRaVerified_10() const { return ____popRaVerified_10; }
	inline Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 ** get_address_of__popRaVerified_10() { return &____popRaVerified_10; }
	inline void set__popRaVerified_10(Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * value)
	{
		____popRaVerified_10 = value;
		Il2CppCodeGenWriteBarrier((&____popRaVerified_10), value);
	}

	inline static int32_t get_offset_of__agreeMac_11() { return static_cast<int32_t>(offsetof(CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016, ____agreeMac_11)); }
	inline PKMacValue_t2354860B8647226235395857CE076879CE35713C * get__agreeMac_11() const { return ____agreeMac_11; }
	inline PKMacValue_t2354860B8647226235395857CE076879CE35713C ** get_address_of__agreeMac_11() { return &____agreeMac_11; }
	inline void set__agreeMac_11(PKMacValue_t2354860B8647226235395857CE076879CE35713C * value)
	{
		____agreeMac_11 = value;
		Il2CppCodeGenWriteBarrier((&____agreeMac_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEREQUESTMESSAGEBUILDER_TA52FA4B71621C2A988476C07CD13CBE246466016_H
#ifndef DEFAULTPKMACPRIMITIVESPROVIDER_TE3893E01783C636D581E2E89A77F2C99DA7D9CEF_H
#define DEFAULTPKMACPRIMITIVESPROVIDER_TE3893E01783C636D581E2E89A77F2C99DA7D9CEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.DefaultPKMacPrimitivesProvider
struct  DefaultPKMacPrimitivesProvider_tE3893E01783C636D581E2E89A77F2C99DA7D9CEF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPKMACPRIMITIVESPROVIDER_TE3893E01783C636D581E2E89A77F2C99DA7D9CEF_H
#ifndef DEFAULTPKMACRESULT_TDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840_H
#define DEFAULTPKMACRESULT_TDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.DefaultPKMacResult
struct  DefaultPKMacResult_tDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.DefaultPKMacResult::mac
	RuntimeObject* ___mac_0;

public:
	inline static int32_t get_offset_of_mac_0() { return static_cast<int32_t>(offsetof(DefaultPKMacResult_tDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840, ___mac_0)); }
	inline RuntimeObject* get_mac_0() const { return ___mac_0; }
	inline RuntimeObject** get_address_of_mac_0() { return &___mac_0; }
	inline void set_mac_0(RuntimeObject* value)
	{
		___mac_0 = value;
		Il2CppCodeGenWriteBarrier((&___mac_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPKMACRESULT_TDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840_H
#ifndef ENCRYPTEDVALUEBUILDER_TE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0_H
#define ENCRYPTEDVALUEBUILDER_TE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.EncryptedValueBuilder
struct  EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IKeyWrapper BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.EncryptedValueBuilder::wrapper
	RuntimeObject* ___wrapper_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherBuilderWithKey BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.EncryptedValueBuilder::encryptor
	RuntimeObject* ___encryptor_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.IEncryptedValuePadder BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.EncryptedValueBuilder::padder
	RuntimeObject* ___padder_2;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0, ___wrapper_0)); }
	inline RuntimeObject* get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject* value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_encryptor_1() { return static_cast<int32_t>(offsetof(EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0, ___encryptor_1)); }
	inline RuntimeObject* get_encryptor_1() const { return ___encryptor_1; }
	inline RuntimeObject** get_address_of_encryptor_1() { return &___encryptor_1; }
	inline void set_encryptor_1(RuntimeObject* value)
	{
		___encryptor_1 = value;
		Il2CppCodeGenWriteBarrier((&___encryptor_1), value);
	}

	inline static int32_t get_offset_of_padder_2() { return static_cast<int32_t>(offsetof(EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0, ___padder_2)); }
	inline RuntimeObject* get_padder_2() const { return ___padder_2; }
	inline RuntimeObject** get_address_of_padder_2() { return &___padder_2; }
	inline void set_padder_2(RuntimeObject* value)
	{
		___padder_2 = value;
		Il2CppCodeGenWriteBarrier((&___padder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDVALUEBUILDER_TE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0_H
#ifndef PKMACBUILDER_T4683635A2D7B64D1E00C680D714712BCE77E4492_H
#define PKMACBUILDER_T4683635A2D7B64D1E00C680D714712BCE77E4492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder
struct  PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::owf
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___owf_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::mac
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___mac_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.IPKMacPrimitivesProvider BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::provider
	RuntimeObject* ___provider_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::parameters
	PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 * ___parameters_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::iterationCount
	int32_t ___iterationCount_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::saltLength
	int32_t ___saltLength_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacBuilder::maxIterations
	int32_t ___maxIterations_7;

public:
	inline static int32_t get_offset_of_owf_0() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___owf_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_owf_0() const { return ___owf_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_owf_0() { return &___owf_0; }
	inline void set_owf_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___owf_0 = value;
		Il2CppCodeGenWriteBarrier((&___owf_0), value);
	}

	inline static int32_t get_offset_of_mac_1() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___mac_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_mac_1() const { return ___mac_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_mac_1() { return &___mac_1; }
	inline void set_mac_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___mac_1 = value;
		Il2CppCodeGenWriteBarrier((&___mac_1), value);
	}

	inline static int32_t get_offset_of_provider_2() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___provider_2)); }
	inline RuntimeObject* get_provider_2() const { return ___provider_2; }
	inline RuntimeObject** get_address_of_provider_2() { return &___provider_2; }
	inline void set_provider_2(RuntimeObject* value)
	{
		___provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___provider_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}

	inline static int32_t get_offset_of_parameters_4() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___parameters_4)); }
	inline PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 * get_parameters_4() const { return ___parameters_4; }
	inline PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 ** get_address_of_parameters_4() { return &___parameters_4; }
	inline void set_parameters_4(PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 * value)
	{
		___parameters_4 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_4), value);
	}

	inline static int32_t get_offset_of_iterationCount_5() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___iterationCount_5)); }
	inline int32_t get_iterationCount_5() const { return ___iterationCount_5; }
	inline int32_t* get_address_of_iterationCount_5() { return &___iterationCount_5; }
	inline void set_iterationCount_5(int32_t value)
	{
		___iterationCount_5 = value;
	}

	inline static int32_t get_offset_of_saltLength_6() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___saltLength_6)); }
	inline int32_t get_saltLength_6() const { return ___saltLength_6; }
	inline int32_t* get_address_of_saltLength_6() { return &___saltLength_6; }
	inline void set_saltLength_6(int32_t value)
	{
		___saltLength_6 = value;
	}

	inline static int32_t get_offset_of_maxIterations_7() { return static_cast<int32_t>(offsetof(PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492, ___maxIterations_7)); }
	inline int32_t get_maxIterations_7() const { return ___maxIterations_7; }
	inline int32_t* get_address_of_maxIterations_7() { return &___maxIterations_7; }
	inline void set_maxIterations_7(int32_t value)
	{
		___maxIterations_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKMACBUILDER_T4683635A2D7B64D1E00C680D714712BCE77E4492_H
#ifndef PKMACFACTORY_TA09740F8AE4D4EB21AC478DA0980722011767367_H
#define PKMACFACTORY_TA09740F8AE4D4EB21AC478DA0980722011767367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacFactory
struct  PKMacFactory_tA09740F8AE4D4EB21AC478DA0980722011767367  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PbmParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacFactory::parameters
	PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 * ___parameters_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacFactory::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(PKMacFactory_tA09740F8AE4D4EB21AC478DA0980722011767367, ___parameters_0)); }
	inline PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 * get_parameters_0() const { return ___parameters_0; }
	inline PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(PbmParameter_t9BBB8795E15A93183BF2DC11B992309A7383C504 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(PKMacFactory_tA09740F8AE4D4EB21AC478DA0980722011767367, ___key_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_1() const { return ___key_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKMACFACTORY_TA09740F8AE4D4EB21AC478DA0980722011767367_H
#ifndef PKMACSTREAMCALCULATOR_TB5D66385DCB6D50B930F5181F3B680AD24335E62_H
#define PKMACSTREAMCALCULATOR_TB5D66385DCB6D50B930F5181F3B680AD24335E62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacStreamCalculator
struct  PKMacStreamCalculator_tB5D66385DCB6D50B930F5181F3B680AD24335E62  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.MacSink BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PKMacStreamCalculator::_stream
	MacSink_tFAC174835C49820F15602AF337F88F6F496DC568 * ____stream_0;

public:
	inline static int32_t get_offset_of__stream_0() { return static_cast<int32_t>(offsetof(PKMacStreamCalculator_tB5D66385DCB6D50B930F5181F3B680AD24335E62, ____stream_0)); }
	inline MacSink_tFAC174835C49820F15602AF337F88F6F496DC568 * get__stream_0() const { return ____stream_0; }
	inline MacSink_tFAC174835C49820F15602AF337F88F6F496DC568 ** get_address_of__stream_0() { return &____stream_0; }
	inline void set__stream_0(MacSink_tFAC174835C49820F15602AF337F88F6F496DC568 * value)
	{
		____stream_0 = value;
		Il2CppCodeGenWriteBarrier((&____stream_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKMACSTREAMCALCULATOR_TB5D66385DCB6D50B930F5181F3B680AD24335E62_H
#ifndef PKIARCHIVECONTROL_TFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_H
#define PKIARCHIVECONTROL_TFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControl
struct  PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiArchiveOptions BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControl::pkiArchiveOptions
	PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482 * ___pkiArchiveOptions_4;

public:
	inline static int32_t get_offset_of_pkiArchiveOptions_4() { return static_cast<int32_t>(offsetof(PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C, ___pkiArchiveOptions_4)); }
	inline PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482 * get_pkiArchiveOptions_4() const { return ___pkiArchiveOptions_4; }
	inline PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482 ** get_address_of_pkiArchiveOptions_4() { return &___pkiArchiveOptions_4; }
	inline void set_pkiArchiveOptions_4(PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482 * value)
	{
		___pkiArchiveOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___pkiArchiveOptions_4), value);
	}
};

struct PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControl::encryptedPrivKey
	int32_t ___encryptedPrivKey_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControl::keyGenParameters
	int32_t ___keyGenParameters_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControl::archiveRemGenPrivKey
	int32_t ___archiveRemGenPrivKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControl::type
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___type_3;

public:
	inline static int32_t get_offset_of_encryptedPrivKey_0() { return static_cast<int32_t>(offsetof(PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields, ___encryptedPrivKey_0)); }
	inline int32_t get_encryptedPrivKey_0() const { return ___encryptedPrivKey_0; }
	inline int32_t* get_address_of_encryptedPrivKey_0() { return &___encryptedPrivKey_0; }
	inline void set_encryptedPrivKey_0(int32_t value)
	{
		___encryptedPrivKey_0 = value;
	}

	inline static int32_t get_offset_of_keyGenParameters_1() { return static_cast<int32_t>(offsetof(PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields, ___keyGenParameters_1)); }
	inline int32_t get_keyGenParameters_1() const { return ___keyGenParameters_1; }
	inline int32_t* get_address_of_keyGenParameters_1() { return &___keyGenParameters_1; }
	inline void set_keyGenParameters_1(int32_t value)
	{
		___keyGenParameters_1 = value;
	}

	inline static int32_t get_offset_of_archiveRemGenPrivKey_2() { return static_cast<int32_t>(offsetof(PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields, ___archiveRemGenPrivKey_2)); }
	inline int32_t get_archiveRemGenPrivKey_2() const { return ___archiveRemGenPrivKey_2; }
	inline int32_t* get_address_of_archiveRemGenPrivKey_2() { return &___archiveRemGenPrivKey_2; }
	inline void set_archiveRemGenPrivKey_2(int32_t value)
	{
		___archiveRemGenPrivKey_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields, ___type_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_type_3() const { return ___type_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIARCHIVECONTROL_TFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_H
#ifndef PKIARCHIVECONTROLBUILDER_TC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C_H
#define PKIARCHIVECONTROLBUILDER_TC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControlBuilder
struct  PkiArchiveControlBuilder_tC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControlBuilder::envGen
	CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC * ___envGen_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsProcessableByteArray BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.PkiArchiveControlBuilder::keyContent
	CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF * ___keyContent_1;

public:
	inline static int32_t get_offset_of_envGen_0() { return static_cast<int32_t>(offsetof(PkiArchiveControlBuilder_tC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C, ___envGen_0)); }
	inline CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC * get_envGen_0() const { return ___envGen_0; }
	inline CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC ** get_address_of_envGen_0() { return &___envGen_0; }
	inline void set_envGen_0(CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC * value)
	{
		___envGen_0 = value;
		Il2CppCodeGenWriteBarrier((&___envGen_0), value);
	}

	inline static int32_t get_offset_of_keyContent_1() { return static_cast<int32_t>(offsetof(PkiArchiveControlBuilder_tC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C, ___keyContent_1)); }
	inline CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF * get_keyContent_1() const { return ___keyContent_1; }
	inline CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF ** get_address_of_keyContent_1() { return &___keyContent_1; }
	inline void set_keyContent_1(CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF * value)
	{
		___keyContent_1 = value;
		Il2CppCodeGenWriteBarrier((&___keyContent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIARCHIVECONTROLBUILDER_TC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C_H
#ifndef PROOFOFPOSSESSIONSIGNINGKEYBUILDER_T812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D_H
#define PROOFOFPOSSESSIONSIGNINGKEYBUILDER_T812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.ProofOfPossessionSigningKeyBuilder
struct  ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.ProofOfPossessionSigningKeyBuilder::_certRequest
	CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 * ____certRequest_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.ProofOfPossessionSigningKeyBuilder::_pubKeyInfo
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ____pubKeyInfo_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.ProofOfPossessionSigningKeyBuilder::_name
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ____name_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.ProofOfPossessionSigningKeyBuilder::_publicKeyMAC
	PKMacValue_t2354860B8647226235395857CE076879CE35713C * ____publicKeyMAC_3;

public:
	inline static int32_t get_offset_of__certRequest_0() { return static_cast<int32_t>(offsetof(ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D, ____certRequest_0)); }
	inline CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 * get__certRequest_0() const { return ____certRequest_0; }
	inline CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 ** get_address_of__certRequest_0() { return &____certRequest_0; }
	inline void set__certRequest_0(CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 * value)
	{
		____certRequest_0 = value;
		Il2CppCodeGenWriteBarrier((&____certRequest_0), value);
	}

	inline static int32_t get_offset_of__pubKeyInfo_1() { return static_cast<int32_t>(offsetof(ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D, ____pubKeyInfo_1)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get__pubKeyInfo_1() const { return ____pubKeyInfo_1; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of__pubKeyInfo_1() { return &____pubKeyInfo_1; }
	inline void set__pubKeyInfo_1(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		____pubKeyInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____pubKeyInfo_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D, ____name_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get__name_2() const { return ____name_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__publicKeyMAC_3() { return static_cast<int32_t>(offsetof(ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D, ____publicKeyMAC_3)); }
	inline PKMacValue_t2354860B8647226235395857CE076879CE35713C * get__publicKeyMAC_3() const { return ____publicKeyMAC_3; }
	inline PKMacValue_t2354860B8647226235395857CE076879CE35713C ** get_address_of__publicKeyMAC_3() { return &____publicKeyMAC_3; }
	inline void set__publicKeyMAC_3(PKMacValue_t2354860B8647226235395857CE076879CE35713C * value)
	{
		____publicKeyMAC_3 = value;
		Il2CppCodeGenWriteBarrier((&____publicKeyMAC_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROOFOFPOSSESSIONSIGNINGKEYBUILDER_T812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D_H
#ifndef REGTOKENCONTROL_T72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_H
#define REGTOKENCONTROL_T72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.RegTokenControl
struct  RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.RegTokenControl::token
	DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * ___token_1;

public:
	inline static int32_t get_offset_of_token_1() { return static_cast<int32_t>(offsetof(RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308, ___token_1)); }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * get_token_1() const { return ___token_1; }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 ** get_address_of_token_1() { return &___token_1; }
	inline void set_token_1(DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * value)
	{
		___token_1 = value;
		Il2CppCodeGenWriteBarrier((&___token_1), value);
	}
};

struct RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.RegTokenControl::type
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_StaticFields, ___type_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_type_0() const { return ___type_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGTOKENCONTROL_T72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_H
#ifndef DHAGREEMENT_T69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E_H
#define DHAGREEMENT_T69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHAgreement
struct  DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHAgreement::key
	DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * ___key_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHAgreement::dhParams
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___dhParams_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHAgreement::privateValue
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___privateValue_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHAgreement::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E, ___key_0)); }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * get_key_0() const { return ___key_0; }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_dhParams_1() { return static_cast<int32_t>(offsetof(DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E, ___dhParams_1)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_dhParams_1() const { return ___dhParams_1; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_dhParams_1() { return &___dhParams_1; }
	inline void set_dhParams_1(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___dhParams_1 = value;
		Il2CppCodeGenWriteBarrier((&___dhParams_1), value);
	}

	inline static int32_t get_offset_of_privateValue_2() { return static_cast<int32_t>(offsetof(DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E, ___privateValue_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_privateValue_2() const { return ___privateValue_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_privateValue_2() { return &___privateValue_2; }
	inline void set_privateValue_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___privateValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___privateValue_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHAGREEMENT_T69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E_H
#ifndef DHBASICAGREEMENT_T6CCFB0CF3FFF6417C2409A457972547A6892A78F_H
#define DHBASICAGREEMENT_T6CCFB0CF3FFF6417C2409A457972547A6892A78F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHBasicAgreement
struct  DHBasicAgreement_t6CCFB0CF3FFF6417C2409A457972547A6892A78F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHBasicAgreement::key
	DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * ___key_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHBasicAgreement::dhParams
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___dhParams_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(DHBasicAgreement_t6CCFB0CF3FFF6417C2409A457972547A6892A78F, ___key_0)); }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * get_key_0() const { return ___key_0; }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_dhParams_1() { return static_cast<int32_t>(offsetof(DHBasicAgreement_t6CCFB0CF3FFF6417C2409A457972547A6892A78F, ___dhParams_1)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_dhParams_1() const { return ___dhParams_1; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_dhParams_1() { return &___dhParams_1; }
	inline void set_dhParams_1(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___dhParams_1 = value;
		Il2CppCodeGenWriteBarrier((&___dhParams_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHBASICAGREEMENT_T6CCFB0CF3FFF6417C2409A457972547A6892A78F_H
#ifndef DHSTANDARDGROUPS_T2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_H
#define DHSTANDARDGROUPS_T2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups
struct  DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34  : public RuntimeObject
{
public:

public:
};

struct DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc2409_768_p
	String_t* ___rfc2409_768_p_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc2409_768_g
	String_t* ___rfc2409_768_g_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc2409_768
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc2409_768_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc2409_1024_p
	String_t* ___rfc2409_1024_p_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc2409_1024_g
	String_t* ___rfc2409_1024_g_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc2409_1024
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc2409_1024_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_1536_p
	String_t* ___rfc3526_1536_p_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_1536_g
	String_t* ___rfc3526_1536_g_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_1536
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc3526_1536_8;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_2048_p
	String_t* ___rfc3526_2048_p_9;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_2048_g
	String_t* ___rfc3526_2048_g_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_2048
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc3526_2048_11;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_3072_p
	String_t* ___rfc3526_3072_p_12;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_3072_g
	String_t* ___rfc3526_3072_g_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_3072
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc3526_3072_14;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_4096_p
	String_t* ___rfc3526_4096_p_15;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_4096_g
	String_t* ___rfc3526_4096_g_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_4096
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc3526_4096_17;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_6144_p
	String_t* ___rfc3526_6144_p_18;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_6144_g
	String_t* ___rfc3526_6144_g_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_6144
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc3526_6144_20;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_8192_p
	String_t* ___rfc3526_8192_p_21;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_8192_g
	String_t* ___rfc3526_8192_g_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc3526_8192
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc3526_8192_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc4306_768
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc4306_768_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc4306_1024
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc4306_1024_25;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_1024_160_p
	String_t* ___rfc5114_1024_160_p_26;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_1024_160_g
	String_t* ___rfc5114_1024_160_g_27;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_1024_160_q
	String_t* ___rfc5114_1024_160_q_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_1024_160
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc5114_1024_160_29;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_224_p
	String_t* ___rfc5114_2048_224_p_30;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_224_g
	String_t* ___rfc5114_2048_224_g_31;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_224_q
	String_t* ___rfc5114_2048_224_q_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_224
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc5114_2048_224_33;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_256_p
	String_t* ___rfc5114_2048_256_p_34;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_256_g
	String_t* ___rfc5114_2048_256_g_35;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_256_q
	String_t* ___rfc5114_2048_256_q_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5114_2048_256
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc5114_2048_256_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5996_768
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc5996_768_38;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc5996_1024
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc5996_1024_39;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe2048_p
	String_t* ___rfc7919_ffdhe2048_p_40;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe2048
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc7919_ffdhe2048_41;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe3072_p
	String_t* ___rfc7919_ffdhe3072_p_42;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe3072
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc7919_ffdhe3072_43;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe4096_p
	String_t* ___rfc7919_ffdhe4096_p_44;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe4096
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc7919_ffdhe4096_45;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe6144_p
	String_t* ___rfc7919_ffdhe6144_p_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe6144
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc7919_ffdhe6144_47;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe8192_p
	String_t* ___rfc7919_ffdhe8192_p_48;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.DHStandardGroups::rfc7919_ffdhe8192
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___rfc7919_ffdhe8192_49;

public:
	inline static int32_t get_offset_of_rfc2409_768_p_0() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc2409_768_p_0)); }
	inline String_t* get_rfc2409_768_p_0() const { return ___rfc2409_768_p_0; }
	inline String_t** get_address_of_rfc2409_768_p_0() { return &___rfc2409_768_p_0; }
	inline void set_rfc2409_768_p_0(String_t* value)
	{
		___rfc2409_768_p_0 = value;
		Il2CppCodeGenWriteBarrier((&___rfc2409_768_p_0), value);
	}

	inline static int32_t get_offset_of_rfc2409_768_g_1() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc2409_768_g_1)); }
	inline String_t* get_rfc2409_768_g_1() const { return ___rfc2409_768_g_1; }
	inline String_t** get_address_of_rfc2409_768_g_1() { return &___rfc2409_768_g_1; }
	inline void set_rfc2409_768_g_1(String_t* value)
	{
		___rfc2409_768_g_1 = value;
		Il2CppCodeGenWriteBarrier((&___rfc2409_768_g_1), value);
	}

	inline static int32_t get_offset_of_rfc2409_768_2() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc2409_768_2)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc2409_768_2() const { return ___rfc2409_768_2; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc2409_768_2() { return &___rfc2409_768_2; }
	inline void set_rfc2409_768_2(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc2409_768_2 = value;
		Il2CppCodeGenWriteBarrier((&___rfc2409_768_2), value);
	}

	inline static int32_t get_offset_of_rfc2409_1024_p_3() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc2409_1024_p_3)); }
	inline String_t* get_rfc2409_1024_p_3() const { return ___rfc2409_1024_p_3; }
	inline String_t** get_address_of_rfc2409_1024_p_3() { return &___rfc2409_1024_p_3; }
	inline void set_rfc2409_1024_p_3(String_t* value)
	{
		___rfc2409_1024_p_3 = value;
		Il2CppCodeGenWriteBarrier((&___rfc2409_1024_p_3), value);
	}

	inline static int32_t get_offset_of_rfc2409_1024_g_4() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc2409_1024_g_4)); }
	inline String_t* get_rfc2409_1024_g_4() const { return ___rfc2409_1024_g_4; }
	inline String_t** get_address_of_rfc2409_1024_g_4() { return &___rfc2409_1024_g_4; }
	inline void set_rfc2409_1024_g_4(String_t* value)
	{
		___rfc2409_1024_g_4 = value;
		Il2CppCodeGenWriteBarrier((&___rfc2409_1024_g_4), value);
	}

	inline static int32_t get_offset_of_rfc2409_1024_5() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc2409_1024_5)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc2409_1024_5() const { return ___rfc2409_1024_5; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc2409_1024_5() { return &___rfc2409_1024_5; }
	inline void set_rfc2409_1024_5(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc2409_1024_5 = value;
		Il2CppCodeGenWriteBarrier((&___rfc2409_1024_5), value);
	}

	inline static int32_t get_offset_of_rfc3526_1536_p_6() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_1536_p_6)); }
	inline String_t* get_rfc3526_1536_p_6() const { return ___rfc3526_1536_p_6; }
	inline String_t** get_address_of_rfc3526_1536_p_6() { return &___rfc3526_1536_p_6; }
	inline void set_rfc3526_1536_p_6(String_t* value)
	{
		___rfc3526_1536_p_6 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_1536_p_6), value);
	}

	inline static int32_t get_offset_of_rfc3526_1536_g_7() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_1536_g_7)); }
	inline String_t* get_rfc3526_1536_g_7() const { return ___rfc3526_1536_g_7; }
	inline String_t** get_address_of_rfc3526_1536_g_7() { return &___rfc3526_1536_g_7; }
	inline void set_rfc3526_1536_g_7(String_t* value)
	{
		___rfc3526_1536_g_7 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_1536_g_7), value);
	}

	inline static int32_t get_offset_of_rfc3526_1536_8() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_1536_8)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc3526_1536_8() const { return ___rfc3526_1536_8; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc3526_1536_8() { return &___rfc3526_1536_8; }
	inline void set_rfc3526_1536_8(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc3526_1536_8 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_1536_8), value);
	}

	inline static int32_t get_offset_of_rfc3526_2048_p_9() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_2048_p_9)); }
	inline String_t* get_rfc3526_2048_p_9() const { return ___rfc3526_2048_p_9; }
	inline String_t** get_address_of_rfc3526_2048_p_9() { return &___rfc3526_2048_p_9; }
	inline void set_rfc3526_2048_p_9(String_t* value)
	{
		___rfc3526_2048_p_9 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_2048_p_9), value);
	}

	inline static int32_t get_offset_of_rfc3526_2048_g_10() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_2048_g_10)); }
	inline String_t* get_rfc3526_2048_g_10() const { return ___rfc3526_2048_g_10; }
	inline String_t** get_address_of_rfc3526_2048_g_10() { return &___rfc3526_2048_g_10; }
	inline void set_rfc3526_2048_g_10(String_t* value)
	{
		___rfc3526_2048_g_10 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_2048_g_10), value);
	}

	inline static int32_t get_offset_of_rfc3526_2048_11() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_2048_11)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc3526_2048_11() const { return ___rfc3526_2048_11; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc3526_2048_11() { return &___rfc3526_2048_11; }
	inline void set_rfc3526_2048_11(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc3526_2048_11 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_2048_11), value);
	}

	inline static int32_t get_offset_of_rfc3526_3072_p_12() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_3072_p_12)); }
	inline String_t* get_rfc3526_3072_p_12() const { return ___rfc3526_3072_p_12; }
	inline String_t** get_address_of_rfc3526_3072_p_12() { return &___rfc3526_3072_p_12; }
	inline void set_rfc3526_3072_p_12(String_t* value)
	{
		___rfc3526_3072_p_12 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_3072_p_12), value);
	}

	inline static int32_t get_offset_of_rfc3526_3072_g_13() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_3072_g_13)); }
	inline String_t* get_rfc3526_3072_g_13() const { return ___rfc3526_3072_g_13; }
	inline String_t** get_address_of_rfc3526_3072_g_13() { return &___rfc3526_3072_g_13; }
	inline void set_rfc3526_3072_g_13(String_t* value)
	{
		___rfc3526_3072_g_13 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_3072_g_13), value);
	}

	inline static int32_t get_offset_of_rfc3526_3072_14() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_3072_14)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc3526_3072_14() const { return ___rfc3526_3072_14; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc3526_3072_14() { return &___rfc3526_3072_14; }
	inline void set_rfc3526_3072_14(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc3526_3072_14 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_3072_14), value);
	}

	inline static int32_t get_offset_of_rfc3526_4096_p_15() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_4096_p_15)); }
	inline String_t* get_rfc3526_4096_p_15() const { return ___rfc3526_4096_p_15; }
	inline String_t** get_address_of_rfc3526_4096_p_15() { return &___rfc3526_4096_p_15; }
	inline void set_rfc3526_4096_p_15(String_t* value)
	{
		___rfc3526_4096_p_15 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_4096_p_15), value);
	}

	inline static int32_t get_offset_of_rfc3526_4096_g_16() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_4096_g_16)); }
	inline String_t* get_rfc3526_4096_g_16() const { return ___rfc3526_4096_g_16; }
	inline String_t** get_address_of_rfc3526_4096_g_16() { return &___rfc3526_4096_g_16; }
	inline void set_rfc3526_4096_g_16(String_t* value)
	{
		___rfc3526_4096_g_16 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_4096_g_16), value);
	}

	inline static int32_t get_offset_of_rfc3526_4096_17() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_4096_17)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc3526_4096_17() const { return ___rfc3526_4096_17; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc3526_4096_17() { return &___rfc3526_4096_17; }
	inline void set_rfc3526_4096_17(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc3526_4096_17 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_4096_17), value);
	}

	inline static int32_t get_offset_of_rfc3526_6144_p_18() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_6144_p_18)); }
	inline String_t* get_rfc3526_6144_p_18() const { return ___rfc3526_6144_p_18; }
	inline String_t** get_address_of_rfc3526_6144_p_18() { return &___rfc3526_6144_p_18; }
	inline void set_rfc3526_6144_p_18(String_t* value)
	{
		___rfc3526_6144_p_18 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_6144_p_18), value);
	}

	inline static int32_t get_offset_of_rfc3526_6144_g_19() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_6144_g_19)); }
	inline String_t* get_rfc3526_6144_g_19() const { return ___rfc3526_6144_g_19; }
	inline String_t** get_address_of_rfc3526_6144_g_19() { return &___rfc3526_6144_g_19; }
	inline void set_rfc3526_6144_g_19(String_t* value)
	{
		___rfc3526_6144_g_19 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_6144_g_19), value);
	}

	inline static int32_t get_offset_of_rfc3526_6144_20() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_6144_20)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc3526_6144_20() const { return ___rfc3526_6144_20; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc3526_6144_20() { return &___rfc3526_6144_20; }
	inline void set_rfc3526_6144_20(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc3526_6144_20 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_6144_20), value);
	}

	inline static int32_t get_offset_of_rfc3526_8192_p_21() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_8192_p_21)); }
	inline String_t* get_rfc3526_8192_p_21() const { return ___rfc3526_8192_p_21; }
	inline String_t** get_address_of_rfc3526_8192_p_21() { return &___rfc3526_8192_p_21; }
	inline void set_rfc3526_8192_p_21(String_t* value)
	{
		___rfc3526_8192_p_21 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_8192_p_21), value);
	}

	inline static int32_t get_offset_of_rfc3526_8192_g_22() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_8192_g_22)); }
	inline String_t* get_rfc3526_8192_g_22() const { return ___rfc3526_8192_g_22; }
	inline String_t** get_address_of_rfc3526_8192_g_22() { return &___rfc3526_8192_g_22; }
	inline void set_rfc3526_8192_g_22(String_t* value)
	{
		___rfc3526_8192_g_22 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_8192_g_22), value);
	}

	inline static int32_t get_offset_of_rfc3526_8192_23() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc3526_8192_23)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc3526_8192_23() const { return ___rfc3526_8192_23; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc3526_8192_23() { return &___rfc3526_8192_23; }
	inline void set_rfc3526_8192_23(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc3526_8192_23 = value;
		Il2CppCodeGenWriteBarrier((&___rfc3526_8192_23), value);
	}

	inline static int32_t get_offset_of_rfc4306_768_24() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc4306_768_24)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc4306_768_24() const { return ___rfc4306_768_24; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc4306_768_24() { return &___rfc4306_768_24; }
	inline void set_rfc4306_768_24(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc4306_768_24 = value;
		Il2CppCodeGenWriteBarrier((&___rfc4306_768_24), value);
	}

	inline static int32_t get_offset_of_rfc4306_1024_25() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc4306_1024_25)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc4306_1024_25() const { return ___rfc4306_1024_25; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc4306_1024_25() { return &___rfc4306_1024_25; }
	inline void set_rfc4306_1024_25(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc4306_1024_25 = value;
		Il2CppCodeGenWriteBarrier((&___rfc4306_1024_25), value);
	}

	inline static int32_t get_offset_of_rfc5114_1024_160_p_26() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_1024_160_p_26)); }
	inline String_t* get_rfc5114_1024_160_p_26() const { return ___rfc5114_1024_160_p_26; }
	inline String_t** get_address_of_rfc5114_1024_160_p_26() { return &___rfc5114_1024_160_p_26; }
	inline void set_rfc5114_1024_160_p_26(String_t* value)
	{
		___rfc5114_1024_160_p_26 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_1024_160_p_26), value);
	}

	inline static int32_t get_offset_of_rfc5114_1024_160_g_27() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_1024_160_g_27)); }
	inline String_t* get_rfc5114_1024_160_g_27() const { return ___rfc5114_1024_160_g_27; }
	inline String_t** get_address_of_rfc5114_1024_160_g_27() { return &___rfc5114_1024_160_g_27; }
	inline void set_rfc5114_1024_160_g_27(String_t* value)
	{
		___rfc5114_1024_160_g_27 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_1024_160_g_27), value);
	}

	inline static int32_t get_offset_of_rfc5114_1024_160_q_28() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_1024_160_q_28)); }
	inline String_t* get_rfc5114_1024_160_q_28() const { return ___rfc5114_1024_160_q_28; }
	inline String_t** get_address_of_rfc5114_1024_160_q_28() { return &___rfc5114_1024_160_q_28; }
	inline void set_rfc5114_1024_160_q_28(String_t* value)
	{
		___rfc5114_1024_160_q_28 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_1024_160_q_28), value);
	}

	inline static int32_t get_offset_of_rfc5114_1024_160_29() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_1024_160_29)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc5114_1024_160_29() const { return ___rfc5114_1024_160_29; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc5114_1024_160_29() { return &___rfc5114_1024_160_29; }
	inline void set_rfc5114_1024_160_29(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc5114_1024_160_29 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_1024_160_29), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_224_p_30() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_224_p_30)); }
	inline String_t* get_rfc5114_2048_224_p_30() const { return ___rfc5114_2048_224_p_30; }
	inline String_t** get_address_of_rfc5114_2048_224_p_30() { return &___rfc5114_2048_224_p_30; }
	inline void set_rfc5114_2048_224_p_30(String_t* value)
	{
		___rfc5114_2048_224_p_30 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_224_p_30), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_224_g_31() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_224_g_31)); }
	inline String_t* get_rfc5114_2048_224_g_31() const { return ___rfc5114_2048_224_g_31; }
	inline String_t** get_address_of_rfc5114_2048_224_g_31() { return &___rfc5114_2048_224_g_31; }
	inline void set_rfc5114_2048_224_g_31(String_t* value)
	{
		___rfc5114_2048_224_g_31 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_224_g_31), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_224_q_32() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_224_q_32)); }
	inline String_t* get_rfc5114_2048_224_q_32() const { return ___rfc5114_2048_224_q_32; }
	inline String_t** get_address_of_rfc5114_2048_224_q_32() { return &___rfc5114_2048_224_q_32; }
	inline void set_rfc5114_2048_224_q_32(String_t* value)
	{
		___rfc5114_2048_224_q_32 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_224_q_32), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_224_33() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_224_33)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc5114_2048_224_33() const { return ___rfc5114_2048_224_33; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc5114_2048_224_33() { return &___rfc5114_2048_224_33; }
	inline void set_rfc5114_2048_224_33(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc5114_2048_224_33 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_224_33), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_256_p_34() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_256_p_34)); }
	inline String_t* get_rfc5114_2048_256_p_34() const { return ___rfc5114_2048_256_p_34; }
	inline String_t** get_address_of_rfc5114_2048_256_p_34() { return &___rfc5114_2048_256_p_34; }
	inline void set_rfc5114_2048_256_p_34(String_t* value)
	{
		___rfc5114_2048_256_p_34 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_256_p_34), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_256_g_35() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_256_g_35)); }
	inline String_t* get_rfc5114_2048_256_g_35() const { return ___rfc5114_2048_256_g_35; }
	inline String_t** get_address_of_rfc5114_2048_256_g_35() { return &___rfc5114_2048_256_g_35; }
	inline void set_rfc5114_2048_256_g_35(String_t* value)
	{
		___rfc5114_2048_256_g_35 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_256_g_35), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_256_q_36() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_256_q_36)); }
	inline String_t* get_rfc5114_2048_256_q_36() const { return ___rfc5114_2048_256_q_36; }
	inline String_t** get_address_of_rfc5114_2048_256_q_36() { return &___rfc5114_2048_256_q_36; }
	inline void set_rfc5114_2048_256_q_36(String_t* value)
	{
		___rfc5114_2048_256_q_36 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_256_q_36), value);
	}

	inline static int32_t get_offset_of_rfc5114_2048_256_37() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5114_2048_256_37)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc5114_2048_256_37() const { return ___rfc5114_2048_256_37; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc5114_2048_256_37() { return &___rfc5114_2048_256_37; }
	inline void set_rfc5114_2048_256_37(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc5114_2048_256_37 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5114_2048_256_37), value);
	}

	inline static int32_t get_offset_of_rfc5996_768_38() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5996_768_38)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc5996_768_38() const { return ___rfc5996_768_38; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc5996_768_38() { return &___rfc5996_768_38; }
	inline void set_rfc5996_768_38(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc5996_768_38 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5996_768_38), value);
	}

	inline static int32_t get_offset_of_rfc5996_1024_39() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc5996_1024_39)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc5996_1024_39() const { return ___rfc5996_1024_39; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc5996_1024_39() { return &___rfc5996_1024_39; }
	inline void set_rfc5996_1024_39(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc5996_1024_39 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5996_1024_39), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe2048_p_40() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe2048_p_40)); }
	inline String_t* get_rfc7919_ffdhe2048_p_40() const { return ___rfc7919_ffdhe2048_p_40; }
	inline String_t** get_address_of_rfc7919_ffdhe2048_p_40() { return &___rfc7919_ffdhe2048_p_40; }
	inline void set_rfc7919_ffdhe2048_p_40(String_t* value)
	{
		___rfc7919_ffdhe2048_p_40 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe2048_p_40), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe2048_41() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe2048_41)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc7919_ffdhe2048_41() const { return ___rfc7919_ffdhe2048_41; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc7919_ffdhe2048_41() { return &___rfc7919_ffdhe2048_41; }
	inline void set_rfc7919_ffdhe2048_41(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc7919_ffdhe2048_41 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe2048_41), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe3072_p_42() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe3072_p_42)); }
	inline String_t* get_rfc7919_ffdhe3072_p_42() const { return ___rfc7919_ffdhe3072_p_42; }
	inline String_t** get_address_of_rfc7919_ffdhe3072_p_42() { return &___rfc7919_ffdhe3072_p_42; }
	inline void set_rfc7919_ffdhe3072_p_42(String_t* value)
	{
		___rfc7919_ffdhe3072_p_42 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe3072_p_42), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe3072_43() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe3072_43)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc7919_ffdhe3072_43() const { return ___rfc7919_ffdhe3072_43; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc7919_ffdhe3072_43() { return &___rfc7919_ffdhe3072_43; }
	inline void set_rfc7919_ffdhe3072_43(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc7919_ffdhe3072_43 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe3072_43), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe4096_p_44() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe4096_p_44)); }
	inline String_t* get_rfc7919_ffdhe4096_p_44() const { return ___rfc7919_ffdhe4096_p_44; }
	inline String_t** get_address_of_rfc7919_ffdhe4096_p_44() { return &___rfc7919_ffdhe4096_p_44; }
	inline void set_rfc7919_ffdhe4096_p_44(String_t* value)
	{
		___rfc7919_ffdhe4096_p_44 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe4096_p_44), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe4096_45() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe4096_45)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc7919_ffdhe4096_45() const { return ___rfc7919_ffdhe4096_45; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc7919_ffdhe4096_45() { return &___rfc7919_ffdhe4096_45; }
	inline void set_rfc7919_ffdhe4096_45(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc7919_ffdhe4096_45 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe4096_45), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe6144_p_46() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe6144_p_46)); }
	inline String_t* get_rfc7919_ffdhe6144_p_46() const { return ___rfc7919_ffdhe6144_p_46; }
	inline String_t** get_address_of_rfc7919_ffdhe6144_p_46() { return &___rfc7919_ffdhe6144_p_46; }
	inline void set_rfc7919_ffdhe6144_p_46(String_t* value)
	{
		___rfc7919_ffdhe6144_p_46 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe6144_p_46), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe6144_47() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe6144_47)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc7919_ffdhe6144_47() const { return ___rfc7919_ffdhe6144_47; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc7919_ffdhe6144_47() { return &___rfc7919_ffdhe6144_47; }
	inline void set_rfc7919_ffdhe6144_47(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc7919_ffdhe6144_47 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe6144_47), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe8192_p_48() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe8192_p_48)); }
	inline String_t* get_rfc7919_ffdhe8192_p_48() const { return ___rfc7919_ffdhe8192_p_48; }
	inline String_t** get_address_of_rfc7919_ffdhe8192_p_48() { return &___rfc7919_ffdhe8192_p_48; }
	inline void set_rfc7919_ffdhe8192_p_48(String_t* value)
	{
		___rfc7919_ffdhe8192_p_48 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe8192_p_48), value);
	}

	inline static int32_t get_offset_of_rfc7919_ffdhe8192_49() { return static_cast<int32_t>(offsetof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields, ___rfc7919_ffdhe8192_49)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_rfc7919_ffdhe8192_49() const { return ___rfc7919_ffdhe8192_49; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_rfc7919_ffdhe8192_49() { return &___rfc7919_ffdhe8192_49; }
	inline void set_rfc7919_ffdhe8192_49(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___rfc7919_ffdhe8192_49 = value;
		Il2CppCodeGenWriteBarrier((&___rfc7919_ffdhe8192_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHSTANDARDGROUPS_T2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_H
#ifndef ECDHBASICAGREEMENT_TC891F91B06C6B18A64AA947C700C9B322C0E0F2F_H
#define ECDHBASICAGREEMENT_TC891F91B06C6B18A64AA947C700C9B322C0E0F2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHBasicAgreement
struct  ECDHBasicAgreement_tC891F91B06C6B18A64AA947C700C9B322C0E0F2F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHBasicAgreement::privKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___privKey_0;

public:
	inline static int32_t get_offset_of_privKey_0() { return static_cast<int32_t>(offsetof(ECDHBasicAgreement_tC891F91B06C6B18A64AA947C700C9B322C0E0F2F, ___privKey_0)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_privKey_0() const { return ___privKey_0; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_privKey_0() { return &___privKey_0; }
	inline void set_privKey_0(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___privKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___privKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDHBASICAGREEMENT_TC891F91B06C6B18A64AA947C700C9B322C0E0F2F_H
#ifndef ECDHCBASICAGREEMENT_T2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B_H
#define ECDHCBASICAGREEMENT_T2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHCBasicAgreement
struct  ECDHCBasicAgreement_t2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHCBasicAgreement::privKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___privKey_0;

public:
	inline static int32_t get_offset_of_privKey_0() { return static_cast<int32_t>(offsetof(ECDHCBasicAgreement_t2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B, ___privKey_0)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_privKey_0() const { return ___privKey_0; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_privKey_0() { return &___privKey_0; }
	inline void set_privKey_0(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___privKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___privKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDHCBASICAGREEMENT_T2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B_H
#ifndef ECMQVBASICAGREEMENT_T16B063D215C9F45EECCBDFC5EDCBF22C852049C4_H
#define ECMQVBASICAGREEMENT_T16B063D215C9F45EECCBDFC5EDCBF22C852049C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECMqvBasicAgreement
struct  ECMqvBasicAgreement_t16B063D215C9F45EECCBDFC5EDCBF22C852049C4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPrivateParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECMqvBasicAgreement::privParams
	MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7 * ___privParams_0;

public:
	inline static int32_t get_offset_of_privParams_0() { return static_cast<int32_t>(offsetof(ECMqvBasicAgreement_t16B063D215C9F45EECCBDFC5EDCBF22C852049C4, ___privParams_0)); }
	inline MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7 * get_privParams_0() const { return ___privParams_0; }
	inline MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7 ** get_address_of_privParams_0() { return &___privParams_0; }
	inline void set_privParams_0(MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7 * value)
	{
		___privParams_0 = value;
		Il2CppCodeGenWriteBarrier((&___privParams_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECMQVBASICAGREEMENT_T16B063D215C9F45EECCBDFC5EDCBF22C852049C4_H
#ifndef JPAKEPARTICIPANT_TE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_H
#define JPAKEPARTICIPANT_TE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant
struct  JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::participantId
	String_t* ___participantId_8;
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::password
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___password_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::digest
	RuntimeObject* ___digest_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_14;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::partnerParticipantId
	String_t* ___partnerParticipantId_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::x1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x1_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::x2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x2_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::gx1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___gx1_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::gx2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___gx2_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::gx3
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___gx3_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::gx4
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___gx4_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::b
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___b_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::state
	int32_t ___state_23;

public:
	inline static int32_t get_offset_of_participantId_8() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___participantId_8)); }
	inline String_t* get_participantId_8() const { return ___participantId_8; }
	inline String_t** get_address_of_participantId_8() { return &___participantId_8; }
	inline void set_participantId_8(String_t* value)
	{
		___participantId_8 = value;
		Il2CppCodeGenWriteBarrier((&___participantId_8), value);
	}

	inline static int32_t get_offset_of_password_9() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___password_9)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_password_9() const { return ___password_9; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_password_9() { return &___password_9; }
	inline void set_password_9(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___password_9 = value;
		Il2CppCodeGenWriteBarrier((&___password_9), value);
	}

	inline static int32_t get_offset_of_digest_10() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___digest_10)); }
	inline RuntimeObject* get_digest_10() const { return ___digest_10; }
	inline RuntimeObject** get_address_of_digest_10() { return &___digest_10; }
	inline void set_digest_10(RuntimeObject* value)
	{
		___digest_10 = value;
		Il2CppCodeGenWriteBarrier((&___digest_10), value);
	}

	inline static int32_t get_offset_of_random_11() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___random_11)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_11() const { return ___random_11; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_11() { return &___random_11; }
	inline void set_random_11(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_11 = value;
		Il2CppCodeGenWriteBarrier((&___random_11), value);
	}

	inline static int32_t get_offset_of_p_12() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___p_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_12() const { return ___p_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_12() { return &___p_12; }
	inline void set_p_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_12 = value;
		Il2CppCodeGenWriteBarrier((&___p_12), value);
	}

	inline static int32_t get_offset_of_q_13() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___q_13)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_13() const { return ___q_13; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_13() { return &___q_13; }
	inline void set_q_13(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_13 = value;
		Il2CppCodeGenWriteBarrier((&___q_13), value);
	}

	inline static int32_t get_offset_of_g_14() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___g_14)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_14() const { return ___g_14; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_14() { return &___g_14; }
	inline void set_g_14(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_14 = value;
		Il2CppCodeGenWriteBarrier((&___g_14), value);
	}

	inline static int32_t get_offset_of_partnerParticipantId_15() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___partnerParticipantId_15)); }
	inline String_t* get_partnerParticipantId_15() const { return ___partnerParticipantId_15; }
	inline String_t** get_address_of_partnerParticipantId_15() { return &___partnerParticipantId_15; }
	inline void set_partnerParticipantId_15(String_t* value)
	{
		___partnerParticipantId_15 = value;
		Il2CppCodeGenWriteBarrier((&___partnerParticipantId_15), value);
	}

	inline static int32_t get_offset_of_x1_16() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___x1_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x1_16() const { return ___x1_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x1_16() { return &___x1_16; }
	inline void set_x1_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x1_16 = value;
		Il2CppCodeGenWriteBarrier((&___x1_16), value);
	}

	inline static int32_t get_offset_of_x2_17() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___x2_17)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x2_17() const { return ___x2_17; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x2_17() { return &___x2_17; }
	inline void set_x2_17(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x2_17 = value;
		Il2CppCodeGenWriteBarrier((&___x2_17), value);
	}

	inline static int32_t get_offset_of_gx1_18() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___gx1_18)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_gx1_18() const { return ___gx1_18; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_gx1_18() { return &___gx1_18; }
	inline void set_gx1_18(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___gx1_18 = value;
		Il2CppCodeGenWriteBarrier((&___gx1_18), value);
	}

	inline static int32_t get_offset_of_gx2_19() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___gx2_19)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_gx2_19() const { return ___gx2_19; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_gx2_19() { return &___gx2_19; }
	inline void set_gx2_19(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___gx2_19 = value;
		Il2CppCodeGenWriteBarrier((&___gx2_19), value);
	}

	inline static int32_t get_offset_of_gx3_20() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___gx3_20)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_gx3_20() const { return ___gx3_20; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_gx3_20() { return &___gx3_20; }
	inline void set_gx3_20(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___gx3_20 = value;
		Il2CppCodeGenWriteBarrier((&___gx3_20), value);
	}

	inline static int32_t get_offset_of_gx4_21() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___gx4_21)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_gx4_21() const { return ___gx4_21; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_gx4_21() { return &___gx4_21; }
	inline void set_gx4_21(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___gx4_21 = value;
		Il2CppCodeGenWriteBarrier((&___gx4_21), value);
	}

	inline static int32_t get_offset_of_b_22() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___b_22)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_b_22() const { return ___b_22; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_b_22() { return &___b_22; }
	inline void set_b_22(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___b_22 = value;
		Il2CppCodeGenWriteBarrier((&___b_22), value);
	}

	inline static int32_t get_offset_of_state_23() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0, ___state_23)); }
	inline int32_t get_state_23() const { return ___state_23; }
	inline int32_t* get_address_of_state_23() { return &___state_23; }
	inline void set_state_23(int32_t value)
	{
		___state_23 = value;
	}
};

struct JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_INITIALIZED
	int32_t ___STATE_INITIALIZED_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_ROUND_1_CREATED
	int32_t ___STATE_ROUND_1_CREATED_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_ROUND_1_VALIDATED
	int32_t ___STATE_ROUND_1_VALIDATED_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_ROUND_2_CREATED
	int32_t ___STATE_ROUND_2_CREATED_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_ROUND_2_VALIDATED
	int32_t ___STATE_ROUND_2_VALIDATED_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_KEY_CALCULATED
	int32_t ___STATE_KEY_CALCULATED_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_ROUND_3_CREATED
	int32_t ___STATE_ROUND_3_CREATED_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeParticipant::STATE_ROUND_3_VALIDATED
	int32_t ___STATE_ROUND_3_VALIDATED_7;

public:
	inline static int32_t get_offset_of_STATE_INITIALIZED_0() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_INITIALIZED_0)); }
	inline int32_t get_STATE_INITIALIZED_0() const { return ___STATE_INITIALIZED_0; }
	inline int32_t* get_address_of_STATE_INITIALIZED_0() { return &___STATE_INITIALIZED_0; }
	inline void set_STATE_INITIALIZED_0(int32_t value)
	{
		___STATE_INITIALIZED_0 = value;
	}

	inline static int32_t get_offset_of_STATE_ROUND_1_CREATED_1() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_ROUND_1_CREATED_1)); }
	inline int32_t get_STATE_ROUND_1_CREATED_1() const { return ___STATE_ROUND_1_CREATED_1; }
	inline int32_t* get_address_of_STATE_ROUND_1_CREATED_1() { return &___STATE_ROUND_1_CREATED_1; }
	inline void set_STATE_ROUND_1_CREATED_1(int32_t value)
	{
		___STATE_ROUND_1_CREATED_1 = value;
	}

	inline static int32_t get_offset_of_STATE_ROUND_1_VALIDATED_2() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_ROUND_1_VALIDATED_2)); }
	inline int32_t get_STATE_ROUND_1_VALIDATED_2() const { return ___STATE_ROUND_1_VALIDATED_2; }
	inline int32_t* get_address_of_STATE_ROUND_1_VALIDATED_2() { return &___STATE_ROUND_1_VALIDATED_2; }
	inline void set_STATE_ROUND_1_VALIDATED_2(int32_t value)
	{
		___STATE_ROUND_1_VALIDATED_2 = value;
	}

	inline static int32_t get_offset_of_STATE_ROUND_2_CREATED_3() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_ROUND_2_CREATED_3)); }
	inline int32_t get_STATE_ROUND_2_CREATED_3() const { return ___STATE_ROUND_2_CREATED_3; }
	inline int32_t* get_address_of_STATE_ROUND_2_CREATED_3() { return &___STATE_ROUND_2_CREATED_3; }
	inline void set_STATE_ROUND_2_CREATED_3(int32_t value)
	{
		___STATE_ROUND_2_CREATED_3 = value;
	}

	inline static int32_t get_offset_of_STATE_ROUND_2_VALIDATED_4() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_ROUND_2_VALIDATED_4)); }
	inline int32_t get_STATE_ROUND_2_VALIDATED_4() const { return ___STATE_ROUND_2_VALIDATED_4; }
	inline int32_t* get_address_of_STATE_ROUND_2_VALIDATED_4() { return &___STATE_ROUND_2_VALIDATED_4; }
	inline void set_STATE_ROUND_2_VALIDATED_4(int32_t value)
	{
		___STATE_ROUND_2_VALIDATED_4 = value;
	}

	inline static int32_t get_offset_of_STATE_KEY_CALCULATED_5() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_KEY_CALCULATED_5)); }
	inline int32_t get_STATE_KEY_CALCULATED_5() const { return ___STATE_KEY_CALCULATED_5; }
	inline int32_t* get_address_of_STATE_KEY_CALCULATED_5() { return &___STATE_KEY_CALCULATED_5; }
	inline void set_STATE_KEY_CALCULATED_5(int32_t value)
	{
		___STATE_KEY_CALCULATED_5 = value;
	}

	inline static int32_t get_offset_of_STATE_ROUND_3_CREATED_6() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_ROUND_3_CREATED_6)); }
	inline int32_t get_STATE_ROUND_3_CREATED_6() const { return ___STATE_ROUND_3_CREATED_6; }
	inline int32_t* get_address_of_STATE_ROUND_3_CREATED_6() { return &___STATE_ROUND_3_CREATED_6; }
	inline void set_STATE_ROUND_3_CREATED_6(int32_t value)
	{
		___STATE_ROUND_3_CREATED_6 = value;
	}

	inline static int32_t get_offset_of_STATE_ROUND_3_VALIDATED_7() { return static_cast<int32_t>(offsetof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields, ___STATE_ROUND_3_VALIDATED_7)); }
	inline int32_t get_STATE_ROUND_3_VALIDATED_7() const { return ___STATE_ROUND_3_VALIDATED_7; }
	inline int32_t* get_address_of_STATE_ROUND_3_VALIDATED_7() { return &___STATE_ROUND_3_VALIDATED_7; }
	inline void set_STATE_ROUND_3_VALIDATED_7(int32_t value)
	{
		___STATE_ROUND_3_VALIDATED_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEPARTICIPANT_TE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_H
#ifndef JPAKEPRIMEORDERGROUP_T0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902_H
#define JPAKEPRIMEORDERGROUP_T0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup
struct  JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_2;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902, ___p_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_0() const { return ___p_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902, ___q_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_1() const { return ___q_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902, ___g_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_2() const { return ___g_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEPRIMEORDERGROUP_T0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902_H
#ifndef JPAKEPRIMEORDERGROUPS_T2176906146D63FDAF25EA99EA207EA5E17923A7D_H
#define JPAKEPRIMEORDERGROUPS_T2176906146D63FDAF25EA99EA207EA5E17923A7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroups
struct  JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D  : public RuntimeObject
{
public:

public:
};

struct JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroups::SUN_JCE_1024
	JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * ___SUN_JCE_1024_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroups::NIST_2048
	JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * ___NIST_2048_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroup BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakePrimeOrderGroups::NIST_3072
	JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * ___NIST_3072_2;

public:
	inline static int32_t get_offset_of_SUN_JCE_1024_0() { return static_cast<int32_t>(offsetof(JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields, ___SUN_JCE_1024_0)); }
	inline JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * get_SUN_JCE_1024_0() const { return ___SUN_JCE_1024_0; }
	inline JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 ** get_address_of_SUN_JCE_1024_0() { return &___SUN_JCE_1024_0; }
	inline void set_SUN_JCE_1024_0(JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * value)
	{
		___SUN_JCE_1024_0 = value;
		Il2CppCodeGenWriteBarrier((&___SUN_JCE_1024_0), value);
	}

	inline static int32_t get_offset_of_NIST_2048_1() { return static_cast<int32_t>(offsetof(JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields, ___NIST_2048_1)); }
	inline JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * get_NIST_2048_1() const { return ___NIST_2048_1; }
	inline JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 ** get_address_of_NIST_2048_1() { return &___NIST_2048_1; }
	inline void set_NIST_2048_1(JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * value)
	{
		___NIST_2048_1 = value;
		Il2CppCodeGenWriteBarrier((&___NIST_2048_1), value);
	}

	inline static int32_t get_offset_of_NIST_3072_2() { return static_cast<int32_t>(offsetof(JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields, ___NIST_3072_2)); }
	inline JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * get_NIST_3072_2() const { return ___NIST_3072_2; }
	inline JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 ** get_address_of_NIST_3072_2() { return &___NIST_3072_2; }
	inline void set_NIST_3072_2(JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902 * value)
	{
		___NIST_3072_2 = value;
		Il2CppCodeGenWriteBarrier((&___NIST_3072_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEPRIMEORDERGROUPS_T2176906146D63FDAF25EA99EA207EA5E17923A7D_H
#ifndef JPAKEROUND1PAYLOAD_TD6CC10EDCC22855F12D84C2691E1BA9C50444660_H
#define JPAKEROUND1PAYLOAD_TD6CC10EDCC22855F12D84C2691E1BA9C50444660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound1Payload
struct  JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound1Payload::participantId
	String_t* ___participantId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound1Payload::gx1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___gx1_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound1Payload::gx2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___gx2_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound1Payload::knowledgeProofForX1
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___knowledgeProofForX1_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound1Payload::knowledgeProofForX2
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___knowledgeProofForX2_4;

public:
	inline static int32_t get_offset_of_participantId_0() { return static_cast<int32_t>(offsetof(JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660, ___participantId_0)); }
	inline String_t* get_participantId_0() const { return ___participantId_0; }
	inline String_t** get_address_of_participantId_0() { return &___participantId_0; }
	inline void set_participantId_0(String_t* value)
	{
		___participantId_0 = value;
		Il2CppCodeGenWriteBarrier((&___participantId_0), value);
	}

	inline static int32_t get_offset_of_gx1_1() { return static_cast<int32_t>(offsetof(JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660, ___gx1_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_gx1_1() const { return ___gx1_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_gx1_1() { return &___gx1_1; }
	inline void set_gx1_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___gx1_1 = value;
		Il2CppCodeGenWriteBarrier((&___gx1_1), value);
	}

	inline static int32_t get_offset_of_gx2_2() { return static_cast<int32_t>(offsetof(JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660, ___gx2_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_gx2_2() const { return ___gx2_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_gx2_2() { return &___gx2_2; }
	inline void set_gx2_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___gx2_2 = value;
		Il2CppCodeGenWriteBarrier((&___gx2_2), value);
	}

	inline static int32_t get_offset_of_knowledgeProofForX1_3() { return static_cast<int32_t>(offsetof(JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660, ___knowledgeProofForX1_3)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_knowledgeProofForX1_3() const { return ___knowledgeProofForX1_3; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_knowledgeProofForX1_3() { return &___knowledgeProofForX1_3; }
	inline void set_knowledgeProofForX1_3(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___knowledgeProofForX1_3 = value;
		Il2CppCodeGenWriteBarrier((&___knowledgeProofForX1_3), value);
	}

	inline static int32_t get_offset_of_knowledgeProofForX2_4() { return static_cast<int32_t>(offsetof(JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660, ___knowledgeProofForX2_4)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_knowledgeProofForX2_4() const { return ___knowledgeProofForX2_4; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_knowledgeProofForX2_4() { return &___knowledgeProofForX2_4; }
	inline void set_knowledgeProofForX2_4(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___knowledgeProofForX2_4 = value;
		Il2CppCodeGenWriteBarrier((&___knowledgeProofForX2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEROUND1PAYLOAD_TD6CC10EDCC22855F12D84C2691E1BA9C50444660_H
#ifndef JPAKEROUND2PAYLOAD_T671906100BBEF16C804C7DDCC6FDBD28D243F3CE_H
#define JPAKEROUND2PAYLOAD_T671906100BBEF16C804C7DDCC6FDBD28D243F3CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound2Payload
struct  JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound2Payload::participantId
	String_t* ___participantId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound2Payload::a
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___a_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound2Payload::knowledgeProofForX2s
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___knowledgeProofForX2s_2;

public:
	inline static int32_t get_offset_of_participantId_0() { return static_cast<int32_t>(offsetof(JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE, ___participantId_0)); }
	inline String_t* get_participantId_0() const { return ___participantId_0; }
	inline String_t** get_address_of_participantId_0() { return &___participantId_0; }
	inline void set_participantId_0(String_t* value)
	{
		___participantId_0 = value;
		Il2CppCodeGenWriteBarrier((&___participantId_0), value);
	}

	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE, ___a_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_a_1() const { return ___a_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___a_1 = value;
		Il2CppCodeGenWriteBarrier((&___a_1), value);
	}

	inline static int32_t get_offset_of_knowledgeProofForX2s_2() { return static_cast<int32_t>(offsetof(JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE, ___knowledgeProofForX2s_2)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_knowledgeProofForX2s_2() const { return ___knowledgeProofForX2s_2; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_knowledgeProofForX2s_2() { return &___knowledgeProofForX2s_2; }
	inline void set_knowledgeProofForX2s_2(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___knowledgeProofForX2s_2 = value;
		Il2CppCodeGenWriteBarrier((&___knowledgeProofForX2s_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEROUND2PAYLOAD_T671906100BBEF16C804C7DDCC6FDBD28D243F3CE_H
#ifndef JPAKEROUND3PAYLOAD_TC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4_H
#define JPAKEROUND3PAYLOAD_TC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound3Payload
struct  JPakeRound3Payload_tC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound3Payload::participantId
	String_t* ___participantId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeRound3Payload::macTag
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___macTag_1;

public:
	inline static int32_t get_offset_of_participantId_0() { return static_cast<int32_t>(offsetof(JPakeRound3Payload_tC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4, ___participantId_0)); }
	inline String_t* get_participantId_0() const { return ___participantId_0; }
	inline String_t** get_address_of_participantId_0() { return &___participantId_0; }
	inline void set_participantId_0(String_t* value)
	{
		___participantId_0 = value;
		Il2CppCodeGenWriteBarrier((&___participantId_0), value);
	}

	inline static int32_t get_offset_of_macTag_1() { return static_cast<int32_t>(offsetof(JPakeRound3Payload_tC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4, ___macTag_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_macTag_1() const { return ___macTag_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_macTag_1() { return &___macTag_1; }
	inline void set_macTag_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___macTag_1 = value;
		Il2CppCodeGenWriteBarrier((&___macTag_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEROUND3PAYLOAD_TC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4_H
#ifndef JPAKEUTILITIES_T6F6B550F494694D32111CF5A13192B01DA2E2796_H
#define JPAKEUTILITIES_T6F6B550F494694D32111CF5A13192B01DA2E2796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeUtilities
struct  JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796  : public RuntimeObject
{
public:

public:
};

struct JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeUtilities::Zero
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Zero_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.JPake.JPakeUtilities::One
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796_StaticFields, ___Zero_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Zero_0() const { return ___Zero_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Zero_0 = value;
		Il2CppCodeGenWriteBarrier((&___Zero_0), value);
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796_StaticFields, ___One_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_One_1() const { return ___One_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___One_1 = value;
		Il2CppCodeGenWriteBarrier((&___One_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPAKEUTILITIES_T6F6B550F494694D32111CF5A13192B01DA2E2796_H
#ifndef CONCATENATIONKDFGENERATOR_T1037F79692EB4C491DACC55635D501259EF09619_H
#define CONCATENATIONKDFGENERATOR_T1037F79692EB4C491DACC55635D501259EF09619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ConcatenationKdfGenerator
struct  ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ConcatenationKdfGenerator::mDigest
	RuntimeObject* ___mDigest_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ConcatenationKdfGenerator::mShared
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mShared_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ConcatenationKdfGenerator::mOtherInfo
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mOtherInfo_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ConcatenationKdfGenerator::mHLen
	int32_t ___mHLen_3;

public:
	inline static int32_t get_offset_of_mDigest_0() { return static_cast<int32_t>(offsetof(ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619, ___mDigest_0)); }
	inline RuntimeObject* get_mDigest_0() const { return ___mDigest_0; }
	inline RuntimeObject** get_address_of_mDigest_0() { return &___mDigest_0; }
	inline void set_mDigest_0(RuntimeObject* value)
	{
		___mDigest_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_0), value);
	}

	inline static int32_t get_offset_of_mShared_1() { return static_cast<int32_t>(offsetof(ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619, ___mShared_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mShared_1() const { return ___mShared_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mShared_1() { return &___mShared_1; }
	inline void set_mShared_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mShared_1 = value;
		Il2CppCodeGenWriteBarrier((&___mShared_1), value);
	}

	inline static int32_t get_offset_of_mOtherInfo_2() { return static_cast<int32_t>(offsetof(ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619, ___mOtherInfo_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mOtherInfo_2() const { return ___mOtherInfo_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mOtherInfo_2() { return &___mOtherInfo_2; }
	inline void set_mOtherInfo_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mOtherInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___mOtherInfo_2), value);
	}

	inline static int32_t get_offset_of_mHLen_3() { return static_cast<int32_t>(offsetof(ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619, ___mHLen_3)); }
	inline int32_t get_mHLen_3() const { return ___mHLen_3; }
	inline int32_t* get_address_of_mHLen_3() { return &___mHLen_3; }
	inline void set_mHLen_3(int32_t value)
	{
		___mHLen_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCATENATIONKDFGENERATOR_T1037F79692EB4C491DACC55635D501259EF09619_H
#ifndef DHKDFPARAMETERS_T511C519EE1A0B62760F6E3D49D1B92F6264A60B6_H
#define DHKDFPARAMETERS_T511C519EE1A0B62760F6E3D49D1B92F6264A60B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKdfParameters
struct  DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKdfParameters::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKdfParameters::keySize
	int32_t ___keySize_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKdfParameters::z
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___z_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKdfParameters::extraInfo
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___extraInfo_3;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6, ___algorithm_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_0() const { return ___algorithm_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_keySize_1() { return static_cast<int32_t>(offsetof(DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6, ___keySize_1)); }
	inline int32_t get_keySize_1() const { return ___keySize_1; }
	inline int32_t* get_address_of_keySize_1() { return &___keySize_1; }
	inline void set_keySize_1(int32_t value)
	{
		___keySize_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6, ___z_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_z_2() const { return ___z_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___z_2 = value;
		Il2CppCodeGenWriteBarrier((&___z_2), value);
	}

	inline static int32_t get_offset_of_extraInfo_3() { return static_cast<int32_t>(offsetof(DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6, ___extraInfo_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_extraInfo_3() const { return ___extraInfo_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_extraInfo_3() { return &___extraInfo_3; }
	inline void set_extraInfo_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___extraInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___extraInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKDFPARAMETERS_T511C519EE1A0B62760F6E3D49D1B92F6264A60B6_H
#ifndef DHKEKGENERATOR_T25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B_H
#define DHKEKGENERATOR_T25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKekGenerator
struct  DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKekGenerator::digest
	RuntimeObject* ___digest_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKekGenerator::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKekGenerator::keySize
	int32_t ___keySize_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKekGenerator::z
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___z_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.DHKekGenerator::partyAInfo
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___partyAInfo_4;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B, ___digest_0)); }
	inline RuntimeObject* get_digest_0() const { return ___digest_0; }
	inline RuntimeObject** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(RuntimeObject* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B, ___algorithm_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_1() const { return ___algorithm_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_1), value);
	}

	inline static int32_t get_offset_of_keySize_2() { return static_cast<int32_t>(offsetof(DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B, ___keySize_2)); }
	inline int32_t get_keySize_2() const { return ___keySize_2; }
	inline int32_t* get_address_of_keySize_2() { return &___keySize_2; }
	inline void set_keySize_2(int32_t value)
	{
		___keySize_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B, ___z_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_z_3() const { return ___z_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___z_3 = value;
		Il2CppCodeGenWriteBarrier((&___z_3), value);
	}

	inline static int32_t get_offset_of_partyAInfo_4() { return static_cast<int32_t>(offsetof(DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B, ___partyAInfo_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_partyAInfo_4() const { return ___partyAInfo_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_partyAInfo_4() { return &___partyAInfo_4; }
	inline void set_partyAInfo_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___partyAInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___partyAInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEKGENERATOR_T25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B_H
#ifndef ECDHKEKGENERATOR_TD2756B47C0A037451C8B7E2D2E32A144D9D75338_H
#define ECDHKEKGENERATOR_TD2756B47C0A037451C8B7E2D2E32A144D9D75338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ECDHKekGenerator
struct  ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDerivationFunction BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ECDHKekGenerator::kdf
	RuntimeObject* ___kdf_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ECDHKekGenerator::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ECDHKekGenerator::keySize
	int32_t ___keySize_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Kdf.ECDHKekGenerator::z
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___z_3;

public:
	inline static int32_t get_offset_of_kdf_0() { return static_cast<int32_t>(offsetof(ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338, ___kdf_0)); }
	inline RuntimeObject* get_kdf_0() const { return ___kdf_0; }
	inline RuntimeObject** get_address_of_kdf_0() { return &___kdf_0; }
	inline void set_kdf_0(RuntimeObject* value)
	{
		___kdf_0 = value;
		Il2CppCodeGenWriteBarrier((&___kdf_0), value);
	}

	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338, ___algorithm_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_1() const { return ___algorithm_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_1), value);
	}

	inline static int32_t get_offset_of_keySize_2() { return static_cast<int32_t>(offsetof(ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338, ___keySize_2)); }
	inline int32_t get_keySize_2() const { return ___keySize_2; }
	inline int32_t* get_address_of_keySize_2() { return &___keySize_2; }
	inline void set_keySize_2(int32_t value)
	{
		___keySize_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338, ___z_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_z_3() const { return ___z_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___z_3 = value;
		Il2CppCodeGenWriteBarrier((&___z_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDHKEKGENERATOR_TD2756B47C0A037451C8B7E2D2E32A144D9D75338_H
#ifndef SM2KEYEXCHANGE_T75F4E762053841B1461A992AC03CA6B26259A2CA_H
#define SM2KEYEXCHANGE_T75F4E762053841B1461A992AC03CA6B26259A2CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange
struct  SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mDigest
	RuntimeObject* ___mDigest_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mUserID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mUserID_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mStaticKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mStaticKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mStaticPubPoint
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___mStaticPubPoint_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mEphemeralPubPoint
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___mEphemeralPubPoint_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mECParams
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * ___mECParams_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mW
	int32_t ___mW_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mEphemeralKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mEphemeralKey_7;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.SM2KeyExchange::mInitiator
	bool ___mInitiator_8;

public:
	inline static int32_t get_offset_of_mDigest_0() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mDigest_0)); }
	inline RuntimeObject* get_mDigest_0() const { return ___mDigest_0; }
	inline RuntimeObject** get_address_of_mDigest_0() { return &___mDigest_0; }
	inline void set_mDigest_0(RuntimeObject* value)
	{
		___mDigest_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_0), value);
	}

	inline static int32_t get_offset_of_mUserID_1() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mUserID_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mUserID_1() const { return ___mUserID_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mUserID_1() { return &___mUserID_1; }
	inline void set_mUserID_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mUserID_1 = value;
		Il2CppCodeGenWriteBarrier((&___mUserID_1), value);
	}

	inline static int32_t get_offset_of_mStaticKey_2() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mStaticKey_2)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mStaticKey_2() const { return ___mStaticKey_2; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mStaticKey_2() { return &___mStaticKey_2; }
	inline void set_mStaticKey_2(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mStaticKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___mStaticKey_2), value);
	}

	inline static int32_t get_offset_of_mStaticPubPoint_3() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mStaticPubPoint_3)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_mStaticPubPoint_3() const { return ___mStaticPubPoint_3; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_mStaticPubPoint_3() { return &___mStaticPubPoint_3; }
	inline void set_mStaticPubPoint_3(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___mStaticPubPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___mStaticPubPoint_3), value);
	}

	inline static int32_t get_offset_of_mEphemeralPubPoint_4() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mEphemeralPubPoint_4)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_mEphemeralPubPoint_4() const { return ___mEphemeralPubPoint_4; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_mEphemeralPubPoint_4() { return &___mEphemeralPubPoint_4; }
	inline void set_mEphemeralPubPoint_4(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___mEphemeralPubPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___mEphemeralPubPoint_4), value);
	}

	inline static int32_t get_offset_of_mECParams_5() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mECParams_5)); }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * get_mECParams_5() const { return ___mECParams_5; }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C ** get_address_of_mECParams_5() { return &___mECParams_5; }
	inline void set_mECParams_5(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * value)
	{
		___mECParams_5 = value;
		Il2CppCodeGenWriteBarrier((&___mECParams_5), value);
	}

	inline static int32_t get_offset_of_mW_6() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mW_6)); }
	inline int32_t get_mW_6() const { return ___mW_6; }
	inline int32_t* get_address_of_mW_6() { return &___mW_6; }
	inline void set_mW_6(int32_t value)
	{
		___mW_6 = value;
	}

	inline static int32_t get_offset_of_mEphemeralKey_7() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mEphemeralKey_7)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mEphemeralKey_7() const { return ___mEphemeralKey_7; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mEphemeralKey_7() { return &___mEphemeralKey_7; }
	inline void set_mEphemeralKey_7(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mEphemeralKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___mEphemeralKey_7), value);
	}

	inline static int32_t get_offset_of_mInitiator_8() { return static_cast<int32_t>(offsetof(SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA, ___mInitiator_8)); }
	inline bool get_mInitiator_8() const { return ___mInitiator_8; }
	inline bool* get_address_of_mInitiator_8() { return &___mInitiator_8; }
	inline void set_mInitiator_8(bool value)
	{
		___mInitiator_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2KEYEXCHANGE_T75F4E762053841B1461A992AC03CA6B26259A2CA_H
#ifndef SRP6CLIENT_TFEB596C997E97A90130A00A17BD7BEB61F7085DE_H
#define SRP6CLIENT_TFEB596C997E97A90130A00A17BD7BEB61F7085DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client
struct  Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::N
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___N_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::privA
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___privA_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::pubA
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___pubA_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::B
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___B_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::x
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::u
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___u_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::S
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___S_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::M1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___M1_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::M2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___M2_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::Key
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Key_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::digest
	RuntimeObject* ___digest_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_12;

public:
	inline static int32_t get_offset_of_N_0() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___N_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_N_0() const { return ___N_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_N_0() { return &___N_0; }
	inline void set_N_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___N_0 = value;
		Il2CppCodeGenWriteBarrier((&___N_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_1() const { return ___g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_privA_2() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___privA_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_privA_2() const { return ___privA_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_privA_2() { return &___privA_2; }
	inline void set_privA_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___privA_2 = value;
		Il2CppCodeGenWriteBarrier((&___privA_2), value);
	}

	inline static int32_t get_offset_of_pubA_3() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___pubA_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_pubA_3() const { return ___pubA_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_pubA_3() { return &___pubA_3; }
	inline void set_pubA_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___pubA_3 = value;
		Il2CppCodeGenWriteBarrier((&___pubA_3), value);
	}

	inline static int32_t get_offset_of_B_4() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___B_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_B_4() const { return ___B_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_B_4() { return &___B_4; }
	inline void set_B_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___B_4 = value;
		Il2CppCodeGenWriteBarrier((&___B_4), value);
	}

	inline static int32_t get_offset_of_x_5() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___x_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x_5() const { return ___x_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x_5() { return &___x_5; }
	inline void set_x_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x_5 = value;
		Il2CppCodeGenWriteBarrier((&___x_5), value);
	}

	inline static int32_t get_offset_of_u_6() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___u_6)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_u_6() const { return ___u_6; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_u_6() { return &___u_6; }
	inline void set_u_6(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___u_6 = value;
		Il2CppCodeGenWriteBarrier((&___u_6), value);
	}

	inline static int32_t get_offset_of_S_7() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___S_7)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_S_7() const { return ___S_7; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_S_7() { return &___S_7; }
	inline void set_S_7(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___S_7 = value;
		Il2CppCodeGenWriteBarrier((&___S_7), value);
	}

	inline static int32_t get_offset_of_M1_8() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___M1_8)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_M1_8() const { return ___M1_8; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_M1_8() { return &___M1_8; }
	inline void set_M1_8(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___M1_8 = value;
		Il2CppCodeGenWriteBarrier((&___M1_8), value);
	}

	inline static int32_t get_offset_of_M2_9() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___M2_9)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_M2_9() const { return ___M2_9; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_M2_9() { return &___M2_9; }
	inline void set_M2_9(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___M2_9 = value;
		Il2CppCodeGenWriteBarrier((&___M2_9), value);
	}

	inline static int32_t get_offset_of_Key_10() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___Key_10)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Key_10() const { return ___Key_10; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Key_10() { return &___Key_10; }
	inline void set_Key_10(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Key_10 = value;
		Il2CppCodeGenWriteBarrier((&___Key_10), value);
	}

	inline static int32_t get_offset_of_digest_11() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___digest_11)); }
	inline RuntimeObject* get_digest_11() const { return ___digest_11; }
	inline RuntimeObject** get_address_of_digest_11() { return &___digest_11; }
	inline void set_digest_11(RuntimeObject* value)
	{
		___digest_11 = value;
		Il2CppCodeGenWriteBarrier((&___digest_11), value);
	}

	inline static int32_t get_offset_of_random_12() { return static_cast<int32_t>(offsetof(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE, ___random_12)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_12() const { return ___random_12; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_12() { return &___random_12; }
	inline void set_random_12(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_12 = value;
		Il2CppCodeGenWriteBarrier((&___random_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRP6CLIENT_TFEB596C997E97A90130A00A17BD7BEB61F7085DE_H
#ifndef SRP6SERVER_T7362C120EE9E4CFDA2A17F5EF699551F86EC1344_H
#define SRP6SERVER_T7362C120EE9E4CFDA2A17F5EF699551F86EC1344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server
struct  Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::N
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___N_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::v
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___v_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::digest
	RuntimeObject* ___digest_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::A
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___A_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::privB
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___privB_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::pubB
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___pubB_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::u
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___u_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::S
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___S_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::M1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___M1_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::M2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___M2_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server::Key
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Key_12;

public:
	inline static int32_t get_offset_of_N_0() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___N_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_N_0() const { return ___N_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_N_0() { return &___N_0; }
	inline void set_N_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___N_0 = value;
		Il2CppCodeGenWriteBarrier((&___N_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_1() const { return ___g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_v_2() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___v_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_v_2() const { return ___v_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_v_2() { return &___v_2; }
	inline void set_v_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___v_2 = value;
		Il2CppCodeGenWriteBarrier((&___v_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}

	inline static int32_t get_offset_of_digest_4() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___digest_4)); }
	inline RuntimeObject* get_digest_4() const { return ___digest_4; }
	inline RuntimeObject** get_address_of_digest_4() { return &___digest_4; }
	inline void set_digest_4(RuntimeObject* value)
	{
		___digest_4 = value;
		Il2CppCodeGenWriteBarrier((&___digest_4), value);
	}

	inline static int32_t get_offset_of_A_5() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___A_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_A_5() const { return ___A_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_A_5() { return &___A_5; }
	inline void set_A_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___A_5 = value;
		Il2CppCodeGenWriteBarrier((&___A_5), value);
	}

	inline static int32_t get_offset_of_privB_6() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___privB_6)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_privB_6() const { return ___privB_6; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_privB_6() { return &___privB_6; }
	inline void set_privB_6(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___privB_6 = value;
		Il2CppCodeGenWriteBarrier((&___privB_6), value);
	}

	inline static int32_t get_offset_of_pubB_7() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___pubB_7)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_pubB_7() const { return ___pubB_7; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_pubB_7() { return &___pubB_7; }
	inline void set_pubB_7(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___pubB_7 = value;
		Il2CppCodeGenWriteBarrier((&___pubB_7), value);
	}

	inline static int32_t get_offset_of_u_8() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___u_8)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_u_8() const { return ___u_8; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_u_8() { return &___u_8; }
	inline void set_u_8(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___u_8 = value;
		Il2CppCodeGenWriteBarrier((&___u_8), value);
	}

	inline static int32_t get_offset_of_S_9() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___S_9)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_S_9() const { return ___S_9; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_S_9() { return &___S_9; }
	inline void set_S_9(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___S_9 = value;
		Il2CppCodeGenWriteBarrier((&___S_9), value);
	}

	inline static int32_t get_offset_of_M1_10() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___M1_10)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_M1_10() const { return ___M1_10; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_M1_10() { return &___M1_10; }
	inline void set_M1_10(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___M1_10 = value;
		Il2CppCodeGenWriteBarrier((&___M1_10), value);
	}

	inline static int32_t get_offset_of_M2_11() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___M2_11)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_M2_11() const { return ___M2_11; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_M2_11() { return &___M2_11; }
	inline void set_M2_11(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___M2_11 = value;
		Il2CppCodeGenWriteBarrier((&___M2_11), value);
	}

	inline static int32_t get_offset_of_Key_12() { return static_cast<int32_t>(offsetof(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344, ___Key_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Key_12() const { return ___Key_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Key_12() { return &___Key_12; }
	inline void set_Key_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Key_12 = value;
		Il2CppCodeGenWriteBarrier((&___Key_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRP6SERVER_T7362C120EE9E4CFDA2A17F5EF699551F86EC1344_H
#ifndef SRP6STANDARDGROUPS_T4720717C06BC099DE95095BDE06A98B0A0F82A9B_H
#define SRP6STANDARDGROUPS_T4720717C06BC099DE95095BDE06A98B0A0F82A9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups
struct  Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B  : public RuntimeObject
{
public:

public:
};

struct Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_1024
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_1024_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_1536
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_1536_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_2048
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_2048_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_3072
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_3072_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_4096
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_4096_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_6144
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_6144_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6StandardGroups::rfc5054_8192
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___rfc5054_8192_20;

public:
	inline static int32_t get_offset_of_rfc5054_1024_2() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_1024_2)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_1024_2() const { return ___rfc5054_1024_2; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_1024_2() { return &___rfc5054_1024_2; }
	inline void set_rfc5054_1024_2(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_1024_2 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_1024_2), value);
	}

	inline static int32_t get_offset_of_rfc5054_1536_5() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_1536_5)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_1536_5() const { return ___rfc5054_1536_5; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_1536_5() { return &___rfc5054_1536_5; }
	inline void set_rfc5054_1536_5(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_1536_5 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_1536_5), value);
	}

	inline static int32_t get_offset_of_rfc5054_2048_8() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_2048_8)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_2048_8() const { return ___rfc5054_2048_8; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_2048_8() { return &___rfc5054_2048_8; }
	inline void set_rfc5054_2048_8(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_2048_8 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_2048_8), value);
	}

	inline static int32_t get_offset_of_rfc5054_3072_11() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_3072_11)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_3072_11() const { return ___rfc5054_3072_11; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_3072_11() { return &___rfc5054_3072_11; }
	inline void set_rfc5054_3072_11(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_3072_11 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_3072_11), value);
	}

	inline static int32_t get_offset_of_rfc5054_4096_14() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_4096_14)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_4096_14() const { return ___rfc5054_4096_14; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_4096_14() { return &___rfc5054_4096_14; }
	inline void set_rfc5054_4096_14(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_4096_14 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_4096_14), value);
	}

	inline static int32_t get_offset_of_rfc5054_6144_17() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_6144_17)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_6144_17() const { return ___rfc5054_6144_17; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_6144_17() { return &___rfc5054_6144_17; }
	inline void set_rfc5054_6144_17(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_6144_17 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_6144_17), value);
	}

	inline static int32_t get_offset_of_rfc5054_8192_20() { return static_cast<int32_t>(offsetof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields, ___rfc5054_8192_20)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_rfc5054_8192_20() const { return ___rfc5054_8192_20; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_rfc5054_8192_20() { return &___rfc5054_8192_20; }
	inline void set_rfc5054_8192_20(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___rfc5054_8192_20 = value;
		Il2CppCodeGenWriteBarrier((&___rfc5054_8192_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRP6STANDARDGROUPS_T4720717C06BC099DE95095BDE06A98B0A0F82A9B_H
#ifndef SRP6UTILITIES_TA5F33A63ED6B30EE471CCB7E6F272BF753E5E47A_H
#define SRP6UTILITIES_TA5F33A63ED6B30EE471CCB7E6F272BF753E5E47A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Utilities
struct  Srp6Utilities_tA5F33A63ED6B30EE471CCB7E6F272BF753E5E47A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRP6UTILITIES_TA5F33A63ED6B30EE471CCB7E6F272BF753E5E47A_H
#ifndef SRP6VERIFIERGENERATOR_TCE1BA3373611D9CC6FC0427941958E183056766B_H
#define SRP6VERIFIERGENERATOR_TCE1BA3373611D9CC6FC0427941958E183056766B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6VerifierGenerator
struct  Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6VerifierGenerator::N
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___N_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6VerifierGenerator::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6VerifierGenerator::digest
	RuntimeObject* ___digest_2;

public:
	inline static int32_t get_offset_of_N_0() { return static_cast<int32_t>(offsetof(Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B, ___N_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_N_0() const { return ___N_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_N_0() { return &___N_0; }
	inline void set_N_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___N_0 = value;
		Il2CppCodeGenWriteBarrier((&___N_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B, ___g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_1() const { return ___g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B, ___digest_2)); }
	inline RuntimeObject* get_digest_2() const { return ___digest_2; }
	inline RuntimeObject** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(RuntimeObject* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRP6VERIFIERGENERATOR_TCE1BA3373611D9CC6FC0427941958E183056766B_H
#ifndef X25519AGREEMENT_T6B021DC295082FBACCC38468E1EB16D9935C0909_H
#define X25519AGREEMENT_T6B021DC295082FBACCC38468E1EB16D9935C0909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.X25519Agreement
struct  X25519Agreement_t6B021DC295082FBACCC38468E1EB16D9935C0909  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.X25519Agreement::privateKey
	X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0 * ___privateKey_0;

public:
	inline static int32_t get_offset_of_privateKey_0() { return static_cast<int32_t>(offsetof(X25519Agreement_t6B021DC295082FBACCC38468E1EB16D9935C0909, ___privateKey_0)); }
	inline X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0 * get_privateKey_0() const { return ___privateKey_0; }
	inline X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0 ** get_address_of_privateKey_0() { return &___privateKey_0; }
	inline void set_privateKey_0(X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0 * value)
	{
		___privateKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519AGREEMENT_T6B021DC295082FBACCC38468E1EB16D9935C0909_H
#ifndef X448AGREEMENT_T9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427_H
#define X448AGREEMENT_T9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.X448Agreement
struct  X448Agreement_t9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.X448Agreement::privateKey
	X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165 * ___privateKey_0;

public:
	inline static int32_t get_offset_of_privateKey_0() { return static_cast<int32_t>(offsetof(X448Agreement_t9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427, ___privateKey_0)); }
	inline X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165 * get_privateKey_0() const { return ___privateKey_0; }
	inline X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165 ** get_address_of_privateKey_0() { return &___privateKey_0; }
	inline void set_privateKey_0(X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165 * value)
	{
		___privateKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448AGREEMENT_T9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427_H
#ifndef GENERALDIGEST_T30737BEEA96249128E61B761F593A2C44E86F2EA_H
#define GENERALDIGEST_T30737BEEA96249128E61B761F593A2C44E86F2EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest
struct  GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest::xBufOff
	int32_t ___xBufOff_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest::byteCount
	int64_t ___byteCount_3;

public:
	inline static int32_t get_offset_of_xBuf_1() { return static_cast<int32_t>(offsetof(GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA, ___xBuf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_1() const { return ___xBuf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_1() { return &___xBuf_1; }
	inline void set_xBuf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_1), value);
	}

	inline static int32_t get_offset_of_xBufOff_2() { return static_cast<int32_t>(offsetof(GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA, ___xBufOff_2)); }
	inline int32_t get_xBufOff_2() const { return ___xBufOff_2; }
	inline int32_t* get_address_of_xBufOff_2() { return &___xBufOff_2; }
	inline void set_xBufOff_2(int32_t value)
	{
		___xBufOff_2 = value;
	}

	inline static int32_t get_offset_of_byteCount_3() { return static_cast<int32_t>(offsetof(GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA, ___byteCount_3)); }
	inline int64_t get_byteCount_3() const { return ___byteCount_3; }
	inline int64_t* get_address_of_byteCount_3() { return &___byteCount_3; }
	inline void set_byteCount_3(int64_t value)
	{
		___byteCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALDIGEST_T30737BEEA96249128E61B761F593A2C44E86F2EA_H
#ifndef KECCAKDIGEST_TC0937665833293C5C98902C767A0DD60E0277A32_H
#define KECCAKDIGEST_TC0937665833293C5C98902C767A0DD60E0277A32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest
struct  KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32  : public RuntimeObject
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::state
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___state_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::dataQueue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___dataQueue_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::rate
	int32_t ___rate_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::bitsInQueue
	int32_t ___bitsInQueue_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::fixedOutputLength
	int32_t ___fixedOutputLength_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::squeezing
	bool ___squeezing_6;

public:
	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___state_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_state_1() const { return ___state_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___state_1 = value;
		Il2CppCodeGenWriteBarrier((&___state_1), value);
	}

	inline static int32_t get_offset_of_dataQueue_2() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___dataQueue_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_dataQueue_2() const { return ___dataQueue_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_dataQueue_2() { return &___dataQueue_2; }
	inline void set_dataQueue_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___dataQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataQueue_2), value);
	}

	inline static int32_t get_offset_of_rate_3() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___rate_3)); }
	inline int32_t get_rate_3() const { return ___rate_3; }
	inline int32_t* get_address_of_rate_3() { return &___rate_3; }
	inline void set_rate_3(int32_t value)
	{
		___rate_3 = value;
	}

	inline static int32_t get_offset_of_bitsInQueue_4() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___bitsInQueue_4)); }
	inline int32_t get_bitsInQueue_4() const { return ___bitsInQueue_4; }
	inline int32_t* get_address_of_bitsInQueue_4() { return &___bitsInQueue_4; }
	inline void set_bitsInQueue_4(int32_t value)
	{
		___bitsInQueue_4 = value;
	}

	inline static int32_t get_offset_of_fixedOutputLength_5() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___fixedOutputLength_5)); }
	inline int32_t get_fixedOutputLength_5() const { return ___fixedOutputLength_5; }
	inline int32_t* get_address_of_fixedOutputLength_5() { return &___fixedOutputLength_5; }
	inline void set_fixedOutputLength_5(int32_t value)
	{
		___fixedOutputLength_5 = value;
	}

	inline static int32_t get_offset_of_squeezing_6() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___squeezing_6)); }
	inline bool get_squeezing_6() const { return ___squeezing_6; }
	inline bool* get_address_of_squeezing_6() { return &___squeezing_6; }
	inline void set_squeezing_6(bool value)
	{
		___squeezing_6 = value;
	}
};

struct KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::KeccakRoundConstants
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___KeccakRoundConstants_0;

public:
	inline static int32_t get_offset_of_KeccakRoundConstants_0() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32_StaticFields, ___KeccakRoundConstants_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_KeccakRoundConstants_0() const { return ___KeccakRoundConstants_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_KeccakRoundConstants_0() { return &___KeccakRoundConstants_0; }
	inline void set_KeccakRoundConstants_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___KeccakRoundConstants_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeccakRoundConstants_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KECCAKDIGEST_TC0937665833293C5C98902C767A0DD60E0277A32_H
#ifndef LONGDIGEST_T23D3C83A6B4E5D9B9B507888AD068F22886B5203_H
#define LONGDIGEST_T23D3C83A6B4E5D9B9B507888AD068F22886B5203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest
struct  LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::MyByteLength
	int32_t ___MyByteLength_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::xBufOff
	int32_t ___xBufOff_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::byteCount1
	int64_t ___byteCount1_3;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::byteCount2
	int64_t ___byteCount2_4;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H1
	uint64_t ___H1_5;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H2
	uint64_t ___H2_6;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H3
	uint64_t ___H3_7;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H4
	uint64_t ___H4_8;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H5
	uint64_t ___H5_9;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H6
	uint64_t ___H6_10;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H7
	uint64_t ___H7_11;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H8
	uint64_t ___H8_12;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::W
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___W_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::wOff
	int32_t ___wOff_14;

public:
	inline static int32_t get_offset_of_MyByteLength_0() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___MyByteLength_0)); }
	inline int32_t get_MyByteLength_0() const { return ___MyByteLength_0; }
	inline int32_t* get_address_of_MyByteLength_0() { return &___MyByteLength_0; }
	inline void set_MyByteLength_0(int32_t value)
	{
		___MyByteLength_0 = value;
	}

	inline static int32_t get_offset_of_xBuf_1() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___xBuf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_1() const { return ___xBuf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_1() { return &___xBuf_1; }
	inline void set_xBuf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_1), value);
	}

	inline static int32_t get_offset_of_xBufOff_2() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___xBufOff_2)); }
	inline int32_t get_xBufOff_2() const { return ___xBufOff_2; }
	inline int32_t* get_address_of_xBufOff_2() { return &___xBufOff_2; }
	inline void set_xBufOff_2(int32_t value)
	{
		___xBufOff_2 = value;
	}

	inline static int32_t get_offset_of_byteCount1_3() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___byteCount1_3)); }
	inline int64_t get_byteCount1_3() const { return ___byteCount1_3; }
	inline int64_t* get_address_of_byteCount1_3() { return &___byteCount1_3; }
	inline void set_byteCount1_3(int64_t value)
	{
		___byteCount1_3 = value;
	}

	inline static int32_t get_offset_of_byteCount2_4() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___byteCount2_4)); }
	inline int64_t get_byteCount2_4() const { return ___byteCount2_4; }
	inline int64_t* get_address_of_byteCount2_4() { return &___byteCount2_4; }
	inline void set_byteCount2_4(int64_t value)
	{
		___byteCount2_4 = value;
	}

	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H1_5)); }
	inline uint64_t get_H1_5() const { return ___H1_5; }
	inline uint64_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint64_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H2_6)); }
	inline uint64_t get_H2_6() const { return ___H2_6; }
	inline uint64_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint64_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H3_7)); }
	inline uint64_t get_H3_7() const { return ___H3_7; }
	inline uint64_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint64_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H4_8)); }
	inline uint64_t get_H4_8() const { return ___H4_8; }
	inline uint64_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint64_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_H5_9() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H5_9)); }
	inline uint64_t get_H5_9() const { return ___H5_9; }
	inline uint64_t* get_address_of_H5_9() { return &___H5_9; }
	inline void set_H5_9(uint64_t value)
	{
		___H5_9 = value;
	}

	inline static int32_t get_offset_of_H6_10() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H6_10)); }
	inline uint64_t get_H6_10() const { return ___H6_10; }
	inline uint64_t* get_address_of_H6_10() { return &___H6_10; }
	inline void set_H6_10(uint64_t value)
	{
		___H6_10 = value;
	}

	inline static int32_t get_offset_of_H7_11() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H7_11)); }
	inline uint64_t get_H7_11() const { return ___H7_11; }
	inline uint64_t* get_address_of_H7_11() { return &___H7_11; }
	inline void set_H7_11(uint64_t value)
	{
		___H7_11 = value;
	}

	inline static int32_t get_offset_of_H8_12() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H8_12)); }
	inline uint64_t get_H8_12() const { return ___H8_12; }
	inline uint64_t* get_address_of_H8_12() { return &___H8_12; }
	inline void set_H8_12(uint64_t value)
	{
		___H8_12 = value;
	}

	inline static int32_t get_offset_of_W_13() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___W_13)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_W_13() const { return ___W_13; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_W_13() { return &___W_13; }
	inline void set_W_13(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___W_13 = value;
		Il2CppCodeGenWriteBarrier((&___W_13), value);
	}

	inline static int32_t get_offset_of_wOff_14() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___wOff_14)); }
	inline int32_t get_wOff_14() const { return ___wOff_14; }
	inline int32_t* get_address_of_wOff_14() { return &___wOff_14; }
	inline void set_wOff_14(int32_t value)
	{
		___wOff_14 = value;
	}
};

struct LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::K
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___K_15;

public:
	inline static int32_t get_offset_of_K_15() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203_StaticFields, ___K_15)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_K_15() const { return ___K_15; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_K_15() { return &___K_15; }
	inline void set_K_15(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___K_15 = value;
		Il2CppCodeGenWriteBarrier((&___K_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGDIGEST_T23D3C83A6B4E5D9B9B507888AD068F22886B5203_H
#ifndef SHORTENEDDIGEST_T9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1_H
#define SHORTENEDDIGEST_T9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.ShortenedDigest
struct  ShortenedDigest_t9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.ShortenedDigest::baseDigest
	RuntimeObject* ___baseDigest_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.ShortenedDigest::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_baseDigest_0() { return static_cast<int32_t>(offsetof(ShortenedDigest_t9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1, ___baseDigest_0)); }
	inline RuntimeObject* get_baseDigest_0() const { return ___baseDigest_0; }
	inline RuntimeObject** get_address_of_baseDigest_0() { return &___baseDigest_0; }
	inline void set_baseDigest_0(RuntimeObject* value)
	{
		___baseDigest_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseDigest_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(ShortenedDigest_t9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTENEDDIGEST_T9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1_H
#ifndef SKEINDIGEST_TB39A68555114FEF2E5204DBBC29CD4B91A7080E9_H
#define SKEINDIGEST_TB39A68555114FEF2E5204DBBC29CD4B91A7080E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinDigest
struct  SkeinDigest_tB39A68555114FEF2E5204DBBC29CD4B91A7080E9  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinDigest::engine
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * ___engine_3;

public:
	inline static int32_t get_offset_of_engine_3() { return static_cast<int32_t>(offsetof(SkeinDigest_tB39A68555114FEF2E5204DBBC29CD4B91A7080E9, ___engine_3)); }
	inline SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * get_engine_3() const { return ___engine_3; }
	inline SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 ** get_address_of_engine_3() { return &___engine_3; }
	inline void set_engine_3(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * value)
	{
		___engine_3 = value;
		Il2CppCodeGenWriteBarrier((&___engine_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEINDIGEST_TB39A68555114FEF2E5204DBBC29CD4B91A7080E9_H
#ifndef SKEINENGINE_T7D2EE75541E20F35A410CAF628C8A99F8B676B36_H
#define SKEINENGINE_T7D2EE75541E20F35A410CAF628C8A99F8B676B36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine
struct  SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::threefish
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE * ___threefish_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::outputSizeBytes
	int32_t ___outputSizeBytes_9;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::chain
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___chain_10;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::initialState
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___initialState_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Parameter[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::preMessageParameters
	ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55* ___preMessageParameters_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Parameter[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::postMessageParameters
	ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55* ___postMessageParameters_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::ubi
	UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382 * ___ubi_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::singleByte
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___singleByte_16;

public:
	inline static int32_t get_offset_of_threefish_8() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___threefish_8)); }
	inline ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE * get_threefish_8() const { return ___threefish_8; }
	inline ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE ** get_address_of_threefish_8() { return &___threefish_8; }
	inline void set_threefish_8(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE * value)
	{
		___threefish_8 = value;
		Il2CppCodeGenWriteBarrier((&___threefish_8), value);
	}

	inline static int32_t get_offset_of_outputSizeBytes_9() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___outputSizeBytes_9)); }
	inline int32_t get_outputSizeBytes_9() const { return ___outputSizeBytes_9; }
	inline int32_t* get_address_of_outputSizeBytes_9() { return &___outputSizeBytes_9; }
	inline void set_outputSizeBytes_9(int32_t value)
	{
		___outputSizeBytes_9 = value;
	}

	inline static int32_t get_offset_of_chain_10() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___chain_10)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_chain_10() const { return ___chain_10; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_chain_10() { return &___chain_10; }
	inline void set_chain_10(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___chain_10 = value;
		Il2CppCodeGenWriteBarrier((&___chain_10), value);
	}

	inline static int32_t get_offset_of_initialState_11() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___initialState_11)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_initialState_11() const { return ___initialState_11; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_initialState_11() { return &___initialState_11; }
	inline void set_initialState_11(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___initialState_11 = value;
		Il2CppCodeGenWriteBarrier((&___initialState_11), value);
	}

	inline static int32_t get_offset_of_key_12() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___key_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_12() const { return ___key_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_12() { return &___key_12; }
	inline void set_key_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_12 = value;
		Il2CppCodeGenWriteBarrier((&___key_12), value);
	}

	inline static int32_t get_offset_of_preMessageParameters_13() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___preMessageParameters_13)); }
	inline ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55* get_preMessageParameters_13() const { return ___preMessageParameters_13; }
	inline ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55** get_address_of_preMessageParameters_13() { return &___preMessageParameters_13; }
	inline void set_preMessageParameters_13(ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55* value)
	{
		___preMessageParameters_13 = value;
		Il2CppCodeGenWriteBarrier((&___preMessageParameters_13), value);
	}

	inline static int32_t get_offset_of_postMessageParameters_14() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___postMessageParameters_14)); }
	inline ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55* get_postMessageParameters_14() const { return ___postMessageParameters_14; }
	inline ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55** get_address_of_postMessageParameters_14() { return &___postMessageParameters_14; }
	inline void set_postMessageParameters_14(ParameterU5BU5D_tDBA7FD32545CC4B9C2B9C113565581A4319C5D55* value)
	{
		___postMessageParameters_14 = value;
		Il2CppCodeGenWriteBarrier((&___postMessageParameters_14), value);
	}

	inline static int32_t get_offset_of_ubi_15() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___ubi_15)); }
	inline UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382 * get_ubi_15() const { return ___ubi_15; }
	inline UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382 ** get_address_of_ubi_15() { return &___ubi_15; }
	inline void set_ubi_15(UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382 * value)
	{
		___ubi_15 = value;
		Il2CppCodeGenWriteBarrier((&___ubi_15), value);
	}

	inline static int32_t get_offset_of_singleByte_16() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36, ___singleByte_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_singleByte_16() const { return ___singleByte_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_singleByte_16() { return &___singleByte_16; }
	inline void set_singleByte_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___singleByte_16 = value;
		Il2CppCodeGenWriteBarrier((&___singleByte_16), value);
	}
};

struct SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine::INITIAL_STATES
	RuntimeObject* ___INITIAL_STATES_7;

public:
	inline static int32_t get_offset_of_INITIAL_STATES_7() { return static_cast<int32_t>(offsetof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36_StaticFields, ___INITIAL_STATES_7)); }
	inline RuntimeObject* get_INITIAL_STATES_7() const { return ___INITIAL_STATES_7; }
	inline RuntimeObject** get_address_of_INITIAL_STATES_7() { return &___INITIAL_STATES_7; }
	inline void set_INITIAL_STATES_7(RuntimeObject* value)
	{
		___INITIAL_STATES_7 = value;
		Il2CppCodeGenWriteBarrier((&___INITIAL_STATES_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEINENGINE_T7D2EE75541E20F35A410CAF628C8A99F8B676B36_H
#ifndef CONFIGURATION_T909112074B3AB1033D38060F7605F1987710E27B_H
#define CONFIGURATION_T909112074B3AB1033D38060F7605F1987710E27B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Configuration
struct  Configuration_t909112074B3AB1033D38060F7605F1987710E27B  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Configuration::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_0;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(Configuration_t909112074B3AB1033D38060F7605F1987710E27B, ___bytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATION_T909112074B3AB1033D38060F7605F1987710E27B_H
#ifndef PARAMETER_T349B5E4529D512858A79C534325B5E73C9417C30_H
#define PARAMETER_T349B5E4529D512858A79C534325B5E73C9417C30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Parameter
struct  Parameter_t349B5E4529D512858A79C534325B5E73C9417C30  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Parameter::type
	int32_t ___type_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_Parameter::value
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___value_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Parameter_t349B5E4529D512858A79C534325B5E73C9417C30, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(Parameter_t349B5E4529D512858A79C534325B5E73C9417C30, ___value_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_value_1() const { return ___value_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETER_T349B5E4529D512858A79C534325B5E73C9417C30_H
#ifndef UBI_TC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382_H
#define UBI_TC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI
struct  UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UbiTweak BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI::tweak
	UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D * ___tweak_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI::engine
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * ___engine_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI::currentBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___currentBlock_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI::currentOffset
	int32_t ___currentOffset_3;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UBI::message
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___message_4;

public:
	inline static int32_t get_offset_of_tweak_0() { return static_cast<int32_t>(offsetof(UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382, ___tweak_0)); }
	inline UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D * get_tweak_0() const { return ___tweak_0; }
	inline UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D ** get_address_of_tweak_0() { return &___tweak_0; }
	inline void set_tweak_0(UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D * value)
	{
		___tweak_0 = value;
		Il2CppCodeGenWriteBarrier((&___tweak_0), value);
	}

	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382, ___engine_1)); }
	inline SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * get_engine_1() const { return ___engine_1; }
	inline SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 ** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36 * value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}

	inline static int32_t get_offset_of_currentBlock_2() { return static_cast<int32_t>(offsetof(UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382, ___currentBlock_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_currentBlock_2() const { return ___currentBlock_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_currentBlock_2() { return &___currentBlock_2; }
	inline void set_currentBlock_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___currentBlock_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlock_2), value);
	}

	inline static int32_t get_offset_of_currentOffset_3() { return static_cast<int32_t>(offsetof(UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382, ___currentOffset_3)); }
	inline int32_t get_currentOffset_3() const { return ___currentOffset_3; }
	inline int32_t* get_address_of_currentOffset_3() { return &___currentOffset_3; }
	inline void set_currentOffset_3(int32_t value)
	{
		___currentOffset_3 = value;
	}

	inline static int32_t get_offset_of_message_4() { return static_cast<int32_t>(offsetof(UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382, ___message_4)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_message_4() const { return ___message_4; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_message_4() { return &___message_4; }
	inline void set_message_4(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___message_4 = value;
		Il2CppCodeGenWriteBarrier((&___message_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UBI_TC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382_H
#ifndef UBITWEAK_TAA61B75AF5F6DEB81B83826022BB8DCC091E286D_H
#define UBITWEAK_TAA61B75AF5F6DEB81B83826022BB8DCC091E286D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UbiTweak
struct  UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D  : public RuntimeObject
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UbiTweak::tweak
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___tweak_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SkeinEngine_UbiTweak::extendedPosition
	bool ___extendedPosition_4;

public:
	inline static int32_t get_offset_of_tweak_3() { return static_cast<int32_t>(offsetof(UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D, ___tweak_3)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_tweak_3() const { return ___tweak_3; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_tweak_3() { return &___tweak_3; }
	inline void set_tweak_3(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___tweak_3 = value;
		Il2CppCodeGenWriteBarrier((&___tweak_3), value);
	}

	inline static int32_t get_offset_of_extendedPosition_4() { return static_cast<int32_t>(offsetof(UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D, ___extendedPosition_4)); }
	inline bool get_extendedPosition_4() const { return ___extendedPosition_4; }
	inline bool* get_address_of_extendedPosition_4() { return &___extendedPosition_4; }
	inline void set_extendedPosition_4(bool value)
	{
		___extendedPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UBITWEAK_TAA61B75AF5F6DEB81B83826022BB8DCC091E286D_H
#ifndef TIGERDIGEST_TC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_H
#define TIGERDIGEST_TC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest
struct  TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::a
	int64_t ___a_6;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::b
	int64_t ___b_7;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::c
	int64_t ___c_8;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::byteCount
	int64_t ___byteCount_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::bOff
	int32_t ___bOff_11;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::x
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___x_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::xOff
	int32_t ___xOff_13;

public:
	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___a_6)); }
	inline int64_t get_a_6() const { return ___a_6; }
	inline int64_t* get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(int64_t value)
	{
		___a_6 = value;
	}

	inline static int32_t get_offset_of_b_7() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___b_7)); }
	inline int64_t get_b_7() const { return ___b_7; }
	inline int64_t* get_address_of_b_7() { return &___b_7; }
	inline void set_b_7(int64_t value)
	{
		___b_7 = value;
	}

	inline static int32_t get_offset_of_c_8() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___c_8)); }
	inline int64_t get_c_8() const { return ___c_8; }
	inline int64_t* get_address_of_c_8() { return &___c_8; }
	inline void set_c_8(int64_t value)
	{
		___c_8 = value;
	}

	inline static int32_t get_offset_of_byteCount_9() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___byteCount_9)); }
	inline int64_t get_byteCount_9() const { return ___byteCount_9; }
	inline int64_t* get_address_of_byteCount_9() { return &___byteCount_9; }
	inline void set_byteCount_9(int64_t value)
	{
		___byteCount_9 = value;
	}

	inline static int32_t get_offset_of_Buffer_10() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___Buffer_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_10() const { return ___Buffer_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_10() { return &___Buffer_10; }
	inline void set_Buffer_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_10), value);
	}

	inline static int32_t get_offset_of_bOff_11() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___bOff_11)); }
	inline int32_t get_bOff_11() const { return ___bOff_11; }
	inline int32_t* get_address_of_bOff_11() { return &___bOff_11; }
	inline void set_bOff_11(int32_t value)
	{
		___bOff_11 = value;
	}

	inline static int32_t get_offset_of_x_12() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___x_12)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_x_12() const { return ___x_12; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_x_12() { return &___x_12; }
	inline void set_x_12(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___x_12 = value;
		Il2CppCodeGenWriteBarrier((&___x_12), value);
	}

	inline static int32_t get_offset_of_xOff_13() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518, ___xOff_13)); }
	inline int32_t get_xOff_13() const { return ___xOff_13; }
	inline int32_t* get_address_of_xOff_13() { return &___xOff_13; }
	inline void set_xOff_13(int32_t value)
	{
		___xOff_13 = value;
	}
};

struct TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields
{
public:
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::t1
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t1_1;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::t2
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t2_2;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::t3
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t3_3;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.TigerDigest::t4
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___t4_4;

public:
	inline static int32_t get_offset_of_t1_1() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields, ___t1_1)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t1_1() const { return ___t1_1; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t1_1() { return &___t1_1; }
	inline void set_t1_1(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t1_1 = value;
		Il2CppCodeGenWriteBarrier((&___t1_1), value);
	}

	inline static int32_t get_offset_of_t2_2() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields, ___t2_2)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t2_2() const { return ___t2_2; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t2_2() { return &___t2_2; }
	inline void set_t2_2(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t2_2 = value;
		Il2CppCodeGenWriteBarrier((&___t2_2), value);
	}

	inline static int32_t get_offset_of_t3_3() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields, ___t3_3)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t3_3() const { return ___t3_3; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t3_3() { return &___t3_3; }
	inline void set_t3_3(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t3_3 = value;
		Il2CppCodeGenWriteBarrier((&___t3_3), value);
	}

	inline static int32_t get_offset_of_t4_4() { return static_cast<int32_t>(offsetof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields, ___t4_4)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_t4_4() const { return ___t4_4; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_t4_4() { return &___t4_4; }
	inline void set_t4_4(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___t4_4 = value;
		Il2CppCodeGenWriteBarrier((&___t4_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIGERDIGEST_TC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_H
#ifndef WHIRLPOOLDIGEST_TDED32B5555310790B28B191EE9786B7E61CCD0CB_H
#define WHIRLPOOLDIGEST_TDED32B5555310790B28B191EE9786B7E61CCD0CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest
struct  WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB  : public RuntimeObject
{
public:
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_rc
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____rc_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_16;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_bufferPos
	int32_t ____bufferPos_17;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_bitCount
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ____bitCount_18;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_hash
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____hash_19;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_K
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____K_20;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_L
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____L_21;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_block
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____block_22;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::_state
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____state_23;

public:
	inline static int32_t get_offset_of__rc_13() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____rc_13)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__rc_13() const { return ____rc_13; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__rc_13() { return &____rc_13; }
	inline void set__rc_13(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____rc_13 = value;
		Il2CppCodeGenWriteBarrier((&____rc_13), value);
	}

	inline static int32_t get_offset_of__buffer_16() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____buffer_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_16() const { return ____buffer_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_16() { return &____buffer_16; }
	inline void set__buffer_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_16 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_16), value);
	}

	inline static int32_t get_offset_of__bufferPos_17() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____bufferPos_17)); }
	inline int32_t get__bufferPos_17() const { return ____bufferPos_17; }
	inline int32_t* get_address_of__bufferPos_17() { return &____bufferPos_17; }
	inline void set__bufferPos_17(int32_t value)
	{
		____bufferPos_17 = value;
	}

	inline static int32_t get_offset_of__bitCount_18() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____bitCount_18)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get__bitCount_18() const { return ____bitCount_18; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of__bitCount_18() { return &____bitCount_18; }
	inline void set__bitCount_18(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		____bitCount_18 = value;
		Il2CppCodeGenWriteBarrier((&____bitCount_18), value);
	}

	inline static int32_t get_offset_of__hash_19() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____hash_19)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__hash_19() const { return ____hash_19; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__hash_19() { return &____hash_19; }
	inline void set__hash_19(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____hash_19 = value;
		Il2CppCodeGenWriteBarrier((&____hash_19), value);
	}

	inline static int32_t get_offset_of__K_20() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____K_20)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__K_20() const { return ____K_20; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__K_20() { return &____K_20; }
	inline void set__K_20(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____K_20 = value;
		Il2CppCodeGenWriteBarrier((&____K_20), value);
	}

	inline static int32_t get_offset_of__L_21() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____L_21)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__L_21() const { return ____L_21; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__L_21() { return &____L_21; }
	inline void set__L_21(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____L_21 = value;
		Il2CppCodeGenWriteBarrier((&____L_21), value);
	}

	inline static int32_t get_offset_of__block_22() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____block_22)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__block_22() const { return ____block_22; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__block_22() { return &____block_22; }
	inline void set__block_22(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____block_22 = value;
		Il2CppCodeGenWriteBarrier((&____block_22), value);
	}

	inline static int32_t get_offset_of__state_23() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB, ____state_23)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__state_23() const { return ____state_23; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__state_23() { return &____state_23; }
	inline void set__state_23(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____state_23 = value;
		Il2CppCodeGenWriteBarrier((&____state_23), value);
	}
};

struct WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::SBOX
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SBOX_4;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C0
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C0_5;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C1
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C1_6;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C2
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C2_7;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C3
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C3_8;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C4
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C4_9;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C5
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C5_10;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C6
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C6_11;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::C7
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___C7_12;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.WhirlpoolDigest::EIGHT
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___EIGHT_14;

public:
	inline static int32_t get_offset_of_SBOX_4() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___SBOX_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SBOX_4() const { return ___SBOX_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SBOX_4() { return &___SBOX_4; }
	inline void set_SBOX_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SBOX_4 = value;
		Il2CppCodeGenWriteBarrier((&___SBOX_4), value);
	}

	inline static int32_t get_offset_of_C0_5() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C0_5)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C0_5() const { return ___C0_5; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C0_5() { return &___C0_5; }
	inline void set_C0_5(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C0_5 = value;
		Il2CppCodeGenWriteBarrier((&___C0_5), value);
	}

	inline static int32_t get_offset_of_C1_6() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C1_6)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C1_6() const { return ___C1_6; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C1_6() { return &___C1_6; }
	inline void set_C1_6(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C1_6 = value;
		Il2CppCodeGenWriteBarrier((&___C1_6), value);
	}

	inline static int32_t get_offset_of_C2_7() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C2_7)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C2_7() const { return ___C2_7; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C2_7() { return &___C2_7; }
	inline void set_C2_7(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C2_7 = value;
		Il2CppCodeGenWriteBarrier((&___C2_7), value);
	}

	inline static int32_t get_offset_of_C3_8() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C3_8)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C3_8() const { return ___C3_8; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C3_8() { return &___C3_8; }
	inline void set_C3_8(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C3_8 = value;
		Il2CppCodeGenWriteBarrier((&___C3_8), value);
	}

	inline static int32_t get_offset_of_C4_9() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C4_9)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C4_9() const { return ___C4_9; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C4_9() { return &___C4_9; }
	inline void set_C4_9(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C4_9 = value;
		Il2CppCodeGenWriteBarrier((&___C4_9), value);
	}

	inline static int32_t get_offset_of_C5_10() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C5_10)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C5_10() const { return ___C5_10; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C5_10() { return &___C5_10; }
	inline void set_C5_10(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C5_10 = value;
		Il2CppCodeGenWriteBarrier((&___C5_10), value);
	}

	inline static int32_t get_offset_of_C6_11() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C6_11)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C6_11() const { return ___C6_11; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C6_11() { return &___C6_11; }
	inline void set_C6_11(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C6_11 = value;
		Il2CppCodeGenWriteBarrier((&___C6_11), value);
	}

	inline static int32_t get_offset_of_C7_12() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___C7_12)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_C7_12() const { return ___C7_12; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_C7_12() { return &___C7_12; }
	inline void set_C7_12(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___C7_12 = value;
		Il2CppCodeGenWriteBarrier((&___C7_12), value);
	}

	inline static int32_t get_offset_of_EIGHT_14() { return static_cast<int32_t>(offsetof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields, ___EIGHT_14)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_EIGHT_14() const { return ___EIGHT_14; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_EIGHT_14() { return &___EIGHT_14; }
	inline void set_EIGHT_14(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___EIGHT_14 = value;
		Il2CppCodeGenWriteBarrier((&___EIGHT_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHIRLPOOLDIGEST_TDED32B5555310790B28B191EE9786B7E61CCD0CB_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CMSAUTHENTICATEDDATAPARSER_TC07375BACD16E00BFCDD0A3570C1727A87FCD2BA_H
#define CMSAUTHENTICATEDDATAPARSER_TC07375BACD16E00BFCDD0A3570C1727A87FCD2BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser
struct  CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA  : public CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::_recipientInfoStore
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * ____recipientInfoStore_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::authData
	AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A * ___authData_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::macAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___macAlg_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::mac
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mac_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::authAttrs
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___authAttrs_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::unauthAttrs
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___unauthAttrs_7;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::authAttrNotRead
	bool ___authAttrNotRead_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataParser::unauthAttrNotRead
	bool ___unauthAttrNotRead_9;

public:
	inline static int32_t get_offset_of__recipientInfoStore_2() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ____recipientInfoStore_2)); }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * get__recipientInfoStore_2() const { return ____recipientInfoStore_2; }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB ** get_address_of__recipientInfoStore_2() { return &____recipientInfoStore_2; }
	inline void set__recipientInfoStore_2(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * value)
	{
		____recipientInfoStore_2 = value;
		Il2CppCodeGenWriteBarrier((&____recipientInfoStore_2), value);
	}

	inline static int32_t get_offset_of_authData_3() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___authData_3)); }
	inline AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A * get_authData_3() const { return ___authData_3; }
	inline AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A ** get_address_of_authData_3() { return &___authData_3; }
	inline void set_authData_3(AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A * value)
	{
		___authData_3 = value;
		Il2CppCodeGenWriteBarrier((&___authData_3), value);
	}

	inline static int32_t get_offset_of_macAlg_4() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___macAlg_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_macAlg_4() const { return ___macAlg_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_macAlg_4() { return &___macAlg_4; }
	inline void set_macAlg_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___macAlg_4 = value;
		Il2CppCodeGenWriteBarrier((&___macAlg_4), value);
	}

	inline static int32_t get_offset_of_mac_5() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___mac_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mac_5() const { return ___mac_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mac_5() { return &___mac_5; }
	inline void set_mac_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mac_5 = value;
		Il2CppCodeGenWriteBarrier((&___mac_5), value);
	}

	inline static int32_t get_offset_of_authAttrs_6() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___authAttrs_6)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_authAttrs_6() const { return ___authAttrs_6; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_authAttrs_6() { return &___authAttrs_6; }
	inline void set_authAttrs_6(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___authAttrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___authAttrs_6), value);
	}

	inline static int32_t get_offset_of_unauthAttrs_7() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___unauthAttrs_7)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_unauthAttrs_7() const { return ___unauthAttrs_7; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_unauthAttrs_7() { return &___unauthAttrs_7; }
	inline void set_unauthAttrs_7(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___unauthAttrs_7 = value;
		Il2CppCodeGenWriteBarrier((&___unauthAttrs_7), value);
	}

	inline static int32_t get_offset_of_authAttrNotRead_8() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___authAttrNotRead_8)); }
	inline bool get_authAttrNotRead_8() const { return ___authAttrNotRead_8; }
	inline bool* get_address_of_authAttrNotRead_8() { return &___authAttrNotRead_8; }
	inline void set_authAttrNotRead_8(bool value)
	{
		___authAttrNotRead_8 = value;
	}

	inline static int32_t get_offset_of_unauthAttrNotRead_9() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA, ___unauthAttrNotRead_9)); }
	inline bool get_unauthAttrNotRead_9() const { return ___unauthAttrNotRead_9; }
	inline bool* get_address_of_unauthAttrNotRead_9() { return &___unauthAttrNotRead_9; }
	inline void set_unauthAttrNotRead_9(bool value)
	{
		___unauthAttrNotRead_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDDATAPARSER_TC07375BACD16E00BFCDD0A3570C1727A87FCD2BA_H
#ifndef CMSAUTHENTICATEDGENERATOR_TA48E87AF97A3418F9195CF2B8B252E54DEC7C247_H
#define CMSAUTHENTICATEDGENERATOR_TA48E87AF97A3418F9195CF2B8B252E54DEC7C247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedGenerator
struct  CmsAuthenticatedGenerator_tA48E87AF97A3418F9195CF2B8B252E54DEC7C247  : public CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDGENERATOR_TA48E87AF97A3418F9195CF2B8B252E54DEC7C247_H
#ifndef CMSCOMPRESSEDDATAPARSER_T48EAB91FA1B6508F1BCEEB8448CF015F15848C52_H
#define CMSCOMPRESSEDDATAPARSER_T48EAB91FA1B6508F1BCEEB8448CF015F15848C52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataParser
struct  CmsCompressedDataParser_t48EAB91FA1B6508F1BCEEB8448CF015F15848C52  : public CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCOMPRESSEDDATAPARSER_T48EAB91FA1B6508F1BCEEB8448CF015F15848C52_H
#ifndef CMSENVELOPEDDATAGENERATOR_T0281FB12E7237928F0F4C729B4D42A35CAB9E3DC_H
#define CMSENVELOPEDDATAGENERATOR_T0281FB12E7237928F0F4C729B4D42A35CAB9E3DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataGenerator
struct  CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC  : public CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDDATAGENERATOR_T0281FB12E7237928F0F4C729B4D42A35CAB9E3DC_H
#ifndef CMSENVELOPEDDATAPARSER_TCF26A03219DCBEC90C071D64F863A65690B9A630_H
#define CMSENVELOPEDDATAPARSER_TCF26A03219DCBEC90C071D64F863A65690B9A630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataParser
struct  CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630  : public CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.RecipientInformationStore BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataParser::recipientInfoStore
	RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * ___recipientInfoStore_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataParser::envelopedData
	EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F * ___envelopedData_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataParser::_encAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ____encAlg_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataParser::_unprotectedAttributes
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ____unprotectedAttributes_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataParser::_attrNotRead
	bool ____attrNotRead_6;

public:
	inline static int32_t get_offset_of_recipientInfoStore_2() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630, ___recipientInfoStore_2)); }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * get_recipientInfoStore_2() const { return ___recipientInfoStore_2; }
	inline RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB ** get_address_of_recipientInfoStore_2() { return &___recipientInfoStore_2; }
	inline void set_recipientInfoStore_2(RecipientInformationStore_t10C56AA2333E7D948FC6BE5326CCF5B463B614DB * value)
	{
		___recipientInfoStore_2 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfoStore_2), value);
	}

	inline static int32_t get_offset_of_envelopedData_3() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630, ___envelopedData_3)); }
	inline EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F * get_envelopedData_3() const { return ___envelopedData_3; }
	inline EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F ** get_address_of_envelopedData_3() { return &___envelopedData_3; }
	inline void set_envelopedData_3(EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F * value)
	{
		___envelopedData_3 = value;
		Il2CppCodeGenWriteBarrier((&___envelopedData_3), value);
	}

	inline static int32_t get_offset_of__encAlg_4() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630, ____encAlg_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get__encAlg_4() const { return ____encAlg_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of__encAlg_4() { return &____encAlg_4; }
	inline void set__encAlg_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		____encAlg_4 = value;
		Il2CppCodeGenWriteBarrier((&____encAlg_4), value);
	}

	inline static int32_t get_offset_of__unprotectedAttributes_5() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630, ____unprotectedAttributes_5)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get__unprotectedAttributes_5() const { return ____unprotectedAttributes_5; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of__unprotectedAttributes_5() { return &____unprotectedAttributes_5; }
	inline void set__unprotectedAttributes_5(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		____unprotectedAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&____unprotectedAttributes_5), value);
	}

	inline static int32_t get_offset_of__attrNotRead_6() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630, ____attrNotRead_6)); }
	inline bool get__attrNotRead_6() const { return ____attrNotRead_6; }
	inline bool* get_address_of__attrNotRead_6() { return &____attrNotRead_6; }
	inline void set__attrNotRead_6(bool value)
	{
		____attrNotRead_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDDATAPARSER_TCF26A03219DCBEC90C071D64F863A65690B9A630_H
#ifndef CMSENVELOPEDDATASTREAMGENERATOR_T997BC9B3F5D50B35AF0EB20733C472415FFB382D_H
#define CMSENVELOPEDDATASTREAMGENERATOR_T997BC9B3F5D50B35AF0EB20733C472415FFB382D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator
struct  CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D  : public CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD
{
public:
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator::_originatorInfo
	RuntimeObject * ____originatorInfo_25;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator::_unprotectedAttributes
	RuntimeObject * ____unprotectedAttributes_26;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator::_bufferSize
	int32_t ____bufferSize_27;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator::_berEncodeRecipientSet
	bool ____berEncodeRecipientSet_28;

public:
	inline static int32_t get_offset_of__originatorInfo_25() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D, ____originatorInfo_25)); }
	inline RuntimeObject * get__originatorInfo_25() const { return ____originatorInfo_25; }
	inline RuntimeObject ** get_address_of__originatorInfo_25() { return &____originatorInfo_25; }
	inline void set__originatorInfo_25(RuntimeObject * value)
	{
		____originatorInfo_25 = value;
		Il2CppCodeGenWriteBarrier((&____originatorInfo_25), value);
	}

	inline static int32_t get_offset_of__unprotectedAttributes_26() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D, ____unprotectedAttributes_26)); }
	inline RuntimeObject * get__unprotectedAttributes_26() const { return ____unprotectedAttributes_26; }
	inline RuntimeObject ** get_address_of__unprotectedAttributes_26() { return &____unprotectedAttributes_26; }
	inline void set__unprotectedAttributes_26(RuntimeObject * value)
	{
		____unprotectedAttributes_26 = value;
		Il2CppCodeGenWriteBarrier((&____unprotectedAttributes_26), value);
	}

	inline static int32_t get_offset_of__bufferSize_27() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D, ____bufferSize_27)); }
	inline int32_t get__bufferSize_27() const { return ____bufferSize_27; }
	inline int32_t* get_address_of__bufferSize_27() { return &____bufferSize_27; }
	inline void set__bufferSize_27(int32_t value)
	{
		____bufferSize_27 = value;
	}

	inline static int32_t get_offset_of__berEncodeRecipientSet_28() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D, ____berEncodeRecipientSet_28)); }
	inline bool get__berEncodeRecipientSet_28() const { return ____berEncodeRecipientSet_28; }
	inline bool* get_address_of__berEncodeRecipientSet_28() { return &____berEncodeRecipientSet_28; }
	inline void set__berEncodeRecipientSet_28(bool value)
	{
		____berEncodeRecipientSet_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDDATASTREAMGENERATOR_T997BC9B3F5D50B35AF0EB20733C472415FFB382D_H
#ifndef CMSEXCEPTION_TD2E69995C20A2A3B6DCBA454EADA94CD3C4CD3F3_H
#define CMSEXCEPTION_TD2E69995C20A2A3B6DCBA454EADA94CD3C4CD3F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsException
struct  CmsException_tD2E69995C20A2A3B6DCBA454EADA94CD3C4CD3F3  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSEXCEPTION_TD2E69995C20A2A3B6DCBA454EADA94CD3C4CD3F3_H
#ifndef CMSSIGNEDDATAGENERATOR_TE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_H
#define CMSSIGNEDDATAGENERATOR_TE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator
struct  CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8  : public CmsSignedGenerator_tC85671C8234B3E8E8176C2D0A5495A53265A2D44
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator::signerInfs
	RuntimeObject* ___signerInfs_25;

public:
	inline static int32_t get_offset_of_signerInfs_25() { return static_cast<int32_t>(offsetof(CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8, ___signerInfs_25)); }
	inline RuntimeObject* get_signerInfs_25() const { return ___signerInfs_25; }
	inline RuntimeObject** get_address_of_signerInfs_25() { return &___signerInfs_25; }
	inline void set_signerInfs_25(RuntimeObject* value)
	{
		___signerInfs_25 = value;
		Il2CppCodeGenWriteBarrier((&___signerInfs_25), value);
	}
};

struct CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedDataGenerator::Helper
	CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * ___Helper_24;

public:
	inline static int32_t get_offset_of_Helper_24() { return static_cast<int32_t>(offsetof(CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_StaticFields, ___Helper_24)); }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * get_Helper_24() const { return ___Helper_24; }
	inline CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF ** get_address_of_Helper_24() { return &___Helper_24; }
	inline void set_Helper_24(CmsSignedHelper_t20DE4DC580086367E603B7C85A06BAD57ACA9EDF * value)
	{
		___Helper_24 = value;
		Il2CppCodeGenWriteBarrier((&___Helper_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSSIGNEDDATAGENERATOR_TE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_H
#ifndef CRMFEXCEPTION_T3785EC33CAB691AD6A472738E901DAF5F5C29915_H
#define CRMFEXCEPTION_T3785EC33CAB691AD6A472738E901DAF5F5C29915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crmf.CrmfException
struct  CrmfException_t3785EC33CAB691AD6A472738E901DAF5F5C29915  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRMFEXCEPTION_T3785EC33CAB691AD6A472738E901DAF5F5C29915_H
#ifndef ECDHWITHKDFBASICAGREEMENT_T571844060DBCFA1F91FC81F9489666A402F55785_H
#define ECDHWITHKDFBASICAGREEMENT_T571844060DBCFA1F91FC81F9489666A402F55785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHWithKdfBasicAgreement
struct  ECDHWithKdfBasicAgreement_t571844060DBCFA1F91FC81F9489666A402F55785  : public ECDHBasicAgreement_tC891F91B06C6B18A64AA947C700C9B322C0E0F2F
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHWithKdfBasicAgreement::algorithm
	String_t* ___algorithm_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDerivationFunction BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECDHWithKdfBasicAgreement::kdf
	RuntimeObject* ___kdf_2;

public:
	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(ECDHWithKdfBasicAgreement_t571844060DBCFA1F91FC81F9489666A402F55785, ___algorithm_1)); }
	inline String_t* get_algorithm_1() const { return ___algorithm_1; }
	inline String_t** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(String_t* value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_1), value);
	}

	inline static int32_t get_offset_of_kdf_2() { return static_cast<int32_t>(offsetof(ECDHWithKdfBasicAgreement_t571844060DBCFA1F91FC81F9489666A402F55785, ___kdf_2)); }
	inline RuntimeObject* get_kdf_2() const { return ___kdf_2; }
	inline RuntimeObject** get_address_of_kdf_2() { return &___kdf_2; }
	inline void set_kdf_2(RuntimeObject* value)
	{
		___kdf_2 = value;
		Il2CppCodeGenWriteBarrier((&___kdf_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDHWITHKDFBASICAGREEMENT_T571844060DBCFA1F91FC81F9489666A402F55785_H
#ifndef ECMQVWITHKDFBASICAGREEMENT_T60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049_H
#define ECMQVWITHKDFBASICAGREEMENT_T60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECMqvWithKdfBasicAgreement
struct  ECMqvWithKdfBasicAgreement_t60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049  : public ECMqvBasicAgreement_t16B063D215C9F45EECCBDFC5EDCBF22C852049C4
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECMqvWithKdfBasicAgreement::algorithm
	String_t* ___algorithm_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDerivationFunction BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.ECMqvWithKdfBasicAgreement::kdf
	RuntimeObject* ___kdf_2;

public:
	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(ECMqvWithKdfBasicAgreement_t60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049, ___algorithm_1)); }
	inline String_t* get_algorithm_1() const { return ___algorithm_1; }
	inline String_t** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(String_t* value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_1), value);
	}

	inline static int32_t get_offset_of_kdf_2() { return static_cast<int32_t>(offsetof(ECMqvWithKdfBasicAgreement_t60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049, ___kdf_2)); }
	inline RuntimeObject* get_kdf_2() const { return ___kdf_2; }
	inline RuntimeObject** get_address_of_kdf_2() { return &___kdf_2; }
	inline void set_kdf_2(RuntimeObject* value)
	{
		___kdf_2 = value;
		Il2CppCodeGenWriteBarrier((&___kdf_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECMQVWITHKDFBASICAGREEMENT_T60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049_H
#ifndef SM3DIGEST_T947D2183DBE3713D33C44D803EABF50473E457B5_H
#define SM3DIGEST_T947D2183DBE3713D33C44D803EABF50473E457B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SM3Digest
struct  SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SM3Digest::V
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___V_6;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SM3Digest::inwords
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___inwords_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SM3Digest::xOff
	int32_t ___xOff_8;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SM3Digest::W
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___W_9;

public:
	inline static int32_t get_offset_of_V_6() { return static_cast<int32_t>(offsetof(SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5, ___V_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_V_6() const { return ___V_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_V_6() { return &___V_6; }
	inline void set_V_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___V_6 = value;
		Il2CppCodeGenWriteBarrier((&___V_6), value);
	}

	inline static int32_t get_offset_of_inwords_7() { return static_cast<int32_t>(offsetof(SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5, ___inwords_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_inwords_7() const { return ___inwords_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_inwords_7() { return &___inwords_7; }
	inline void set_inwords_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___inwords_7 = value;
		Il2CppCodeGenWriteBarrier((&___inwords_7), value);
	}

	inline static int32_t get_offset_of_xOff_8() { return static_cast<int32_t>(offsetof(SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5, ___xOff_8)); }
	inline int32_t get_xOff_8() const { return ___xOff_8; }
	inline int32_t* get_address_of_xOff_8() { return &___xOff_8; }
	inline void set_xOff_8(int32_t value)
	{
		___xOff_8 = value;
	}

	inline static int32_t get_offset_of_W_9() { return static_cast<int32_t>(offsetof(SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5, ___W_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_W_9() const { return ___W_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_W_9() { return &___W_9; }
	inline void set_W_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___W_9 = value;
		Il2CppCodeGenWriteBarrier((&___W_9), value);
	}
};

struct SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.SM3Digest::T
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___T_10;

public:
	inline static int32_t get_offset_of_T_10() { return static_cast<int32_t>(offsetof(SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5_StaticFields, ___T_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_T_10() const { return ___T_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_T_10() { return &___T_10; }
	inline void set_T_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___T_10 = value;
		Il2CppCodeGenWriteBarrier((&___T_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM3DIGEST_T947D2183DBE3713D33C44D803EABF50473E457B5_H
#ifndef SHA1DIGEST_TF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC_H
#define SHA1DIGEST_TF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest
struct  Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::H1
	uint32_t ___H1_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::H2
	uint32_t ___H2_6;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::H3
	uint32_t ___H3_7;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::H4
	uint32_t ___H4_8;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::H5
	uint32_t ___H5_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha1Digest::xOff
	int32_t ___xOff_11;

public:
	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___H1_5)); }
	inline uint32_t get_H1_5() const { return ___H1_5; }
	inline uint32_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint32_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___H2_6)); }
	inline uint32_t get_H2_6() const { return ___H2_6; }
	inline uint32_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint32_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___H3_7)); }
	inline uint32_t get_H3_7() const { return ___H3_7; }
	inline uint32_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint32_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___H4_8)); }
	inline uint32_t get_H4_8() const { return ___H4_8; }
	inline uint32_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint32_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_H5_9() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___H5_9)); }
	inline uint32_t get_H5_9() const { return ___H5_9; }
	inline uint32_t* get_address_of_H5_9() { return &___H5_9; }
	inline void set_H5_9(uint32_t value)
	{
		___H5_9 = value;
	}

	inline static int32_t get_offset_of_X_10() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___X_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_10() const { return ___X_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_10() { return &___X_10; }
	inline void set_X_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_10 = value;
		Il2CppCodeGenWriteBarrier((&___X_10), value);
	}

	inline static int32_t get_offset_of_xOff_11() { return static_cast<int32_t>(offsetof(Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC, ___xOff_11)); }
	inline int32_t get_xOff_11() const { return ___xOff_11; }
	inline int32_t* get_address_of_xOff_11() { return &___xOff_11; }
	inline void set_xOff_11(int32_t value)
	{
		___xOff_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA1DIGEST_TF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC_H
#ifndef SHA224DIGEST_TA221B2B5844786D1ABA240D37373902286059F66_H
#define SHA224DIGEST_TA221B2B5844786D1ABA240D37373902286059F66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest
struct  Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H1
	uint32_t ___H1_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H2
	uint32_t ___H2_6;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H3
	uint32_t ___H3_7;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H4
	uint32_t ___H4_8;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H5
	uint32_t ___H5_9;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H6
	uint32_t ___H6_10;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H7
	uint32_t ___H7_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::H8
	uint32_t ___H8_12;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::xOff
	int32_t ___xOff_14;

public:
	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H1_5)); }
	inline uint32_t get_H1_5() const { return ___H1_5; }
	inline uint32_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint32_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H2_6)); }
	inline uint32_t get_H2_6() const { return ___H2_6; }
	inline uint32_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint32_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H3_7)); }
	inline uint32_t get_H3_7() const { return ___H3_7; }
	inline uint32_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint32_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H4_8)); }
	inline uint32_t get_H4_8() const { return ___H4_8; }
	inline uint32_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint32_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_H5_9() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H5_9)); }
	inline uint32_t get_H5_9() const { return ___H5_9; }
	inline uint32_t* get_address_of_H5_9() { return &___H5_9; }
	inline void set_H5_9(uint32_t value)
	{
		___H5_9 = value;
	}

	inline static int32_t get_offset_of_H6_10() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H6_10)); }
	inline uint32_t get_H6_10() const { return ___H6_10; }
	inline uint32_t* get_address_of_H6_10() { return &___H6_10; }
	inline void set_H6_10(uint32_t value)
	{
		___H6_10 = value;
	}

	inline static int32_t get_offset_of_H7_11() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H7_11)); }
	inline uint32_t get_H7_11() const { return ___H7_11; }
	inline uint32_t* get_address_of_H7_11() { return &___H7_11; }
	inline void set_H7_11(uint32_t value)
	{
		___H7_11 = value;
	}

	inline static int32_t get_offset_of_H8_12() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___H8_12)); }
	inline uint32_t get_H8_12() const { return ___H8_12; }
	inline uint32_t* get_address_of_H8_12() { return &___H8_12; }
	inline void set_H8_12(uint32_t value)
	{
		___H8_12 = value;
	}

	inline static int32_t get_offset_of_X_13() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___X_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_13() const { return ___X_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_13() { return &___X_13; }
	inline void set_X_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_13 = value;
		Il2CppCodeGenWriteBarrier((&___X_13), value);
	}

	inline static int32_t get_offset_of_xOff_14() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66, ___xOff_14)); }
	inline int32_t get_xOff_14() const { return ___xOff_14; }
	inline int32_t* get_address_of_xOff_14() { return &___xOff_14; }
	inline void set_xOff_14(int32_t value)
	{
		___xOff_14 = value;
	}
};

struct Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha224Digest::K
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___K_15;

public:
	inline static int32_t get_offset_of_K_15() { return static_cast<int32_t>(offsetof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66_StaticFields, ___K_15)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_K_15() const { return ___K_15; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_K_15() { return &___K_15; }
	inline void set_K_15(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___K_15 = value;
		Il2CppCodeGenWriteBarrier((&___K_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA224DIGEST_TA221B2B5844786D1ABA240D37373902286059F66_H
#ifndef SHA256DIGEST_T97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_H
#define SHA256DIGEST_T97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest
struct  Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H1
	uint32_t ___H1_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H2
	uint32_t ___H2_6;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H3
	uint32_t ___H3_7;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H4
	uint32_t ___H4_8;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H5
	uint32_t ___H5_9;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H6
	uint32_t ___H6_10;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H7
	uint32_t ___H7_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::H8
	uint32_t ___H8_12;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::xOff
	int32_t ___xOff_14;

public:
	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H1_5)); }
	inline uint32_t get_H1_5() const { return ___H1_5; }
	inline uint32_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint32_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H2_6)); }
	inline uint32_t get_H2_6() const { return ___H2_6; }
	inline uint32_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint32_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H3_7)); }
	inline uint32_t get_H3_7() const { return ___H3_7; }
	inline uint32_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint32_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H4_8)); }
	inline uint32_t get_H4_8() const { return ___H4_8; }
	inline uint32_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint32_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_H5_9() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H5_9)); }
	inline uint32_t get_H5_9() const { return ___H5_9; }
	inline uint32_t* get_address_of_H5_9() { return &___H5_9; }
	inline void set_H5_9(uint32_t value)
	{
		___H5_9 = value;
	}

	inline static int32_t get_offset_of_H6_10() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H6_10)); }
	inline uint32_t get_H6_10() const { return ___H6_10; }
	inline uint32_t* get_address_of_H6_10() { return &___H6_10; }
	inline void set_H6_10(uint32_t value)
	{
		___H6_10 = value;
	}

	inline static int32_t get_offset_of_H7_11() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H7_11)); }
	inline uint32_t get_H7_11() const { return ___H7_11; }
	inline uint32_t* get_address_of_H7_11() { return &___H7_11; }
	inline void set_H7_11(uint32_t value)
	{
		___H7_11 = value;
	}

	inline static int32_t get_offset_of_H8_12() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___H8_12)); }
	inline uint32_t get_H8_12() const { return ___H8_12; }
	inline uint32_t* get_address_of_H8_12() { return &___H8_12; }
	inline void set_H8_12(uint32_t value)
	{
		___H8_12 = value;
	}

	inline static int32_t get_offset_of_X_13() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___X_13)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_13() const { return ___X_13; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_13() { return &___X_13; }
	inline void set_X_13(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_13 = value;
		Il2CppCodeGenWriteBarrier((&___X_13), value);
	}

	inline static int32_t get_offset_of_xOff_14() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8, ___xOff_14)); }
	inline int32_t get_xOff_14() const { return ___xOff_14; }
	inline int32_t* get_address_of_xOff_14() { return &___xOff_14; }
	inline void set_xOff_14(int32_t value)
	{
		___xOff_14 = value;
	}
};

struct Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha256Digest::K
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___K_15;

public:
	inline static int32_t get_offset_of_K_15() { return static_cast<int32_t>(offsetof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_StaticFields, ___K_15)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_K_15() const { return ___K_15; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_K_15() { return &___K_15; }
	inline void set_K_15(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___K_15 = value;
		Il2CppCodeGenWriteBarrier((&___K_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA256DIGEST_T97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_H
#ifndef SHA384DIGEST_TB96C3F558807D7082BB4308C8B26931B75C57AC5_H
#define SHA384DIGEST_TB96C3F558807D7082BB4308C8B26931B75C57AC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha384Digest
struct  Sha384Digest_tB96C3F558807D7082BB4308C8B26931B75C57AC5  : public LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA384DIGEST_TB96C3F558807D7082BB4308C8B26931B75C57AC5_H
#ifndef SHA3DIGEST_TDF1048D4C4C4D4E1C7D321CF853F2A5B1070CA36_H
#define SHA3DIGEST_TDF1048D4C4C4D4E1C7D321CF853F2A5B1070CA36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha3Digest
struct  Sha3Digest_tDF1048D4C4C4D4E1C7D321CF853F2A5B1070CA36  : public KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA3DIGEST_TDF1048D4C4C4D4E1C7D321CF853F2A5B1070CA36_H
#ifndef SHA512DIGEST_TB6EBA4F3BDD9117165EEA066650779851AAF310A_H
#define SHA512DIGEST_TB6EBA4F3BDD9117165EEA066650779851AAF310A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512Digest
struct  Sha512Digest_tB6EBA4F3BDD9117165EEA066650779851AAF310A  : public LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA512DIGEST_TB6EBA4F3BDD9117165EEA066650779851AAF310A_H
#ifndef SHA512TDIGEST_T799DB0F6185DE0B0FDF399FDB6757B11442D8590_H
#define SHA512TDIGEST_T799DB0F6185DE0B0FDF399FDB6757B11442D8590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest
struct  Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590  : public LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::digestLength
	int32_t ___digestLength_17;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H1t
	uint64_t ___H1t_18;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H2t
	uint64_t ___H2t_19;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H3t
	uint64_t ___H3t_20;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H4t
	uint64_t ___H4t_21;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H5t
	uint64_t ___H5t_22;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H6t
	uint64_t ___H6t_23;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H7t
	uint64_t ___H7t_24;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Sha512tDigest::H8t
	uint64_t ___H8t_25;

public:
	inline static int32_t get_offset_of_digestLength_17() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___digestLength_17)); }
	inline int32_t get_digestLength_17() const { return ___digestLength_17; }
	inline int32_t* get_address_of_digestLength_17() { return &___digestLength_17; }
	inline void set_digestLength_17(int32_t value)
	{
		___digestLength_17 = value;
	}

	inline static int32_t get_offset_of_H1t_18() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H1t_18)); }
	inline uint64_t get_H1t_18() const { return ___H1t_18; }
	inline uint64_t* get_address_of_H1t_18() { return &___H1t_18; }
	inline void set_H1t_18(uint64_t value)
	{
		___H1t_18 = value;
	}

	inline static int32_t get_offset_of_H2t_19() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H2t_19)); }
	inline uint64_t get_H2t_19() const { return ___H2t_19; }
	inline uint64_t* get_address_of_H2t_19() { return &___H2t_19; }
	inline void set_H2t_19(uint64_t value)
	{
		___H2t_19 = value;
	}

	inline static int32_t get_offset_of_H3t_20() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H3t_20)); }
	inline uint64_t get_H3t_20() const { return ___H3t_20; }
	inline uint64_t* get_address_of_H3t_20() { return &___H3t_20; }
	inline void set_H3t_20(uint64_t value)
	{
		___H3t_20 = value;
	}

	inline static int32_t get_offset_of_H4t_21() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H4t_21)); }
	inline uint64_t get_H4t_21() const { return ___H4t_21; }
	inline uint64_t* get_address_of_H4t_21() { return &___H4t_21; }
	inline void set_H4t_21(uint64_t value)
	{
		___H4t_21 = value;
	}

	inline static int32_t get_offset_of_H5t_22() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H5t_22)); }
	inline uint64_t get_H5t_22() const { return ___H5t_22; }
	inline uint64_t* get_address_of_H5t_22() { return &___H5t_22; }
	inline void set_H5t_22(uint64_t value)
	{
		___H5t_22 = value;
	}

	inline static int32_t get_offset_of_H6t_23() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H6t_23)); }
	inline uint64_t get_H6t_23() const { return ___H6t_23; }
	inline uint64_t* get_address_of_H6t_23() { return &___H6t_23; }
	inline void set_H6t_23(uint64_t value)
	{
		___H6t_23 = value;
	}

	inline static int32_t get_offset_of_H7t_24() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H7t_24)); }
	inline uint64_t get_H7t_24() const { return ___H7t_24; }
	inline uint64_t* get_address_of_H7t_24() { return &___H7t_24; }
	inline void set_H7t_24(uint64_t value)
	{
		___H7t_24 = value;
	}

	inline static int32_t get_offset_of_H8t_25() { return static_cast<int32_t>(offsetof(Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590, ___H8t_25)); }
	inline uint64_t get_H8t_25() const { return ___H8t_25; }
	inline uint64_t* get_address_of_H8t_25() { return &___H8t_25; }
	inline void set_H8t_25(uint64_t value)
	{
		___H8t_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHA512TDIGEST_T799DB0F6185DE0B0FDF399FDB6757B11442D8590_H
#ifndef SHAKEDIGEST_T72AB41240CC33900FD697CB6E060E0DA509003A7_H
#define SHAKEDIGEST_T72AB41240CC33900FD697CB6E060E0DA509003A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.ShakeDigest
struct  ShakeDigest_t72AB41240CC33900FD697CB6E060E0DA509003A7  : public KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAKEDIGEST_T72AB41240CC33900FD697CB6E060E0DA509003A7_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef CMSATTRIBUTETABLEGENERATIONEXCEPTION_TDA49B2DD2452DC16AABD1F0F720FE3CA1B46AC8A_H
#define CMSATTRIBUTETABLEGENERATIONEXCEPTION_TDA49B2DD2452DC16AABD1F0F720FE3CA1B46AC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableGenerationException
struct  CmsAttributeTableGenerationException_tDA49B2DD2452DC16AABD1F0F720FE3CA1B46AC8A  : public CmsException_tD2E69995C20A2A3B6DCBA454EADA94CD3C4CD3F3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSATTRIBUTETABLEGENERATIONEXCEPTION_TDA49B2DD2452DC16AABD1F0F720FE3CA1B46AC8A_H
#ifndef CMSATTRIBUTETABLEPARAMETER_T3D362590ED58BEA61746116AF4DF1FFF65BDE6F4_H
#define CMSATTRIBUTETABLEPARAMETER_T3D362590ED58BEA61746116AF4DF1FFF65BDE6F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableParameter
struct  CmsAttributeTableParameter_t3D362590ED58BEA61746116AF4DF1FFF65BDE6F4 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAttributeTableParameter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CmsAttributeTableParameter_t3D362590ED58BEA61746116AF4DF1FFF65BDE6F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSATTRIBUTETABLEPARAMETER_T3D362590ED58BEA61746116AF4DF1FFF65BDE6F4_H
#ifndef CMSAUTHENTICATEDDATAGENERATOR_T512FB9CE3F98E8BA6E7F65D70B4F3A581F421ACE_H
#define CMSAUTHENTICATEDDATAGENERATOR_T512FB9CE3F98E8BA6E7F65D70B4F3A581F421ACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataGenerator
struct  CmsAuthenticatedDataGenerator_t512FB9CE3F98E8BA6E7F65D70B4F3A581F421ACE  : public CmsAuthenticatedGenerator_tA48E87AF97A3418F9195CF2B8B252E54DEC7C247
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDDATAGENERATOR_T512FB9CE3F98E8BA6E7F65D70B4F3A581F421ACE_H
#ifndef CMSAUTHENTICATEDDATASTREAMGENERATOR_T3DFED1A81C465E4727D52754E808E2201928C28F_H
#define CMSAUTHENTICATEDDATASTREAMGENERATOR_T3DFED1A81C465E4727D52754E808E2201928C28F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator
struct  CmsAuthenticatedDataStreamGenerator_t3DFED1A81C465E4727D52754E808E2201928C28F  : public CmsAuthenticatedGenerator_tA48E87AF97A3418F9195CF2B8B252E54DEC7C247
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator::_bufferSize
	int32_t ____bufferSize_25;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator::_berEncodeRecipientSet
	bool ____berEncodeRecipientSet_26;

public:
	inline static int32_t get_offset_of__bufferSize_25() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataStreamGenerator_t3DFED1A81C465E4727D52754E808E2201928C28F, ____bufferSize_25)); }
	inline int32_t get__bufferSize_25() const { return ____bufferSize_25; }
	inline int32_t* get_address_of__bufferSize_25() { return &____bufferSize_25; }
	inline void set__bufferSize_25(int32_t value)
	{
		____bufferSize_25 = value;
	}

	inline static int32_t get_offset_of__berEncodeRecipientSet_26() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataStreamGenerator_t3DFED1A81C465E4727D52754E808E2201928C28F, ____berEncodeRecipientSet_26)); }
	inline bool get__berEncodeRecipientSet_26() const { return ____berEncodeRecipientSet_26; }
	inline bool* get_address_of__berEncodeRecipientSet_26() { return &____berEncodeRecipientSet_26; }
	inline void set__berEncodeRecipientSet_26(bool value)
	{
		____berEncodeRecipientSet_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDDATASTREAMGENERATOR_T3DFED1A81C465E4727D52754E808E2201928C28F_H
#ifndef BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#define BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifndef CMSAUTHENTICATEDDATAOUTPUTSTREAM_T3829F3217EF177BBF298CAEFFA54C05A674B2181_H
#define CMSAUTHENTICATEDDATAOUTPUTSTREAM_T3829F3217EF177BBF298CAEFFA54C05A674B2181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator_CmsAuthenticatedDataOutputStream
struct  CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator_CmsAuthenticatedDataOutputStream::macStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___macStream_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator_CmsAuthenticatedDataOutputStream::mac
	RuntimeObject* ___mac_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator_CmsAuthenticatedDataOutputStream::cGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ___cGen_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator_CmsAuthenticatedDataOutputStream::authGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ___authGen_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsAuthenticatedDataStreamGenerator_CmsAuthenticatedDataOutputStream::eiGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ___eiGen_10;

public:
	inline static int32_t get_offset_of_macStream_6() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181, ___macStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_macStream_6() const { return ___macStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_macStream_6() { return &___macStream_6; }
	inline void set_macStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___macStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___macStream_6), value);
	}

	inline static int32_t get_offset_of_mac_7() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181, ___mac_7)); }
	inline RuntimeObject* get_mac_7() const { return ___mac_7; }
	inline RuntimeObject** get_address_of_mac_7() { return &___mac_7; }
	inline void set_mac_7(RuntimeObject* value)
	{
		___mac_7 = value;
		Il2CppCodeGenWriteBarrier((&___mac_7), value);
	}

	inline static int32_t get_offset_of_cGen_8() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181, ___cGen_8)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get_cGen_8() const { return ___cGen_8; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of_cGen_8() { return &___cGen_8; }
	inline void set_cGen_8(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		___cGen_8 = value;
		Il2CppCodeGenWriteBarrier((&___cGen_8), value);
	}

	inline static int32_t get_offset_of_authGen_9() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181, ___authGen_9)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get_authGen_9() const { return ___authGen_9; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of_authGen_9() { return &___authGen_9; }
	inline void set_authGen_9(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		___authGen_9 = value;
		Il2CppCodeGenWriteBarrier((&___authGen_9), value);
	}

	inline static int32_t get_offset_of_eiGen_10() { return static_cast<int32_t>(offsetof(CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181, ___eiGen_10)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get_eiGen_10() const { return ___eiGen_10; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of_eiGen_10() { return &___eiGen_10; }
	inline void set_eiGen_10(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		___eiGen_10 = value;
		Il2CppCodeGenWriteBarrier((&___eiGen_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSAUTHENTICATEDDATAOUTPUTSTREAM_T3829F3217EF177BBF298CAEFFA54C05A674B2181_H
#ifndef CMSCOMPRESSEDOUTPUTSTREAM_T475308B7C248BE866ECD83D851DAE71971F78335_H
#define CMSCOMPRESSEDOUTPUTSTREAM_T475308B7C248BE866ECD83D851DAE71971F78335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator_CmsCompressedOutputStream
struct  CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator_CmsCompressedOutputStream::_out
	ZOutputStream_t504E5B441A273254366752284D503186AF93F77A * ____out_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator_CmsCompressedOutputStream::_sGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____sGen_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator_CmsCompressedOutputStream::_cGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____cGen_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsCompressedDataStreamGenerator_CmsCompressedOutputStream::_eiGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____eiGen_9;

public:
	inline static int32_t get_offset_of__out_6() { return static_cast<int32_t>(offsetof(CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335, ____out_6)); }
	inline ZOutputStream_t504E5B441A273254366752284D503186AF93F77A * get__out_6() const { return ____out_6; }
	inline ZOutputStream_t504E5B441A273254366752284D503186AF93F77A ** get_address_of__out_6() { return &____out_6; }
	inline void set__out_6(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A * value)
	{
		____out_6 = value;
		Il2CppCodeGenWriteBarrier((&____out_6), value);
	}

	inline static int32_t get_offset_of__sGen_7() { return static_cast<int32_t>(offsetof(CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335, ____sGen_7)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__sGen_7() const { return ____sGen_7; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__sGen_7() { return &____sGen_7; }
	inline void set__sGen_7(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____sGen_7 = value;
		Il2CppCodeGenWriteBarrier((&____sGen_7), value);
	}

	inline static int32_t get_offset_of__cGen_8() { return static_cast<int32_t>(offsetof(CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335, ____cGen_8)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__cGen_8() const { return ____cGen_8; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__cGen_8() { return &____cGen_8; }
	inline void set__cGen_8(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____cGen_8 = value;
		Il2CppCodeGenWriteBarrier((&____cGen_8), value);
	}

	inline static int32_t get_offset_of__eiGen_9() { return static_cast<int32_t>(offsetof(CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335, ____eiGen_9)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__eiGen_9() const { return ____eiGen_9; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__eiGen_9() { return &____eiGen_9; }
	inline void set__eiGen_9(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____eiGen_9 = value;
		Il2CppCodeGenWriteBarrier((&____eiGen_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCOMPRESSEDOUTPUTSTREAM_T475308B7C248BE866ECD83D851DAE71971F78335_H
#ifndef CMSENVELOPEDDATAOUTPUTSTREAM_T40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D_H
#define CMSENVELOPEDDATAOUTPUTSTREAM_T40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator_CmsEnvelopedDataOutputStream
struct  CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator_CmsEnvelopedDataOutputStream::_outer
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD * ____outer_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator_CmsEnvelopedDataOutputStream::_out
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 * ____out_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator_CmsEnvelopedDataOutputStream::_cGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____cGen_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator_CmsEnvelopedDataOutputStream::_envGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____envGen_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.BerSequenceGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsEnvelopedDataStreamGenerator_CmsEnvelopedDataOutputStream::_eiGen
	BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * ____eiGen_10;

public:
	inline static int32_t get_offset_of__outer_6() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D, ____outer_6)); }
	inline CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD * get__outer_6() const { return ____outer_6; }
	inline CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD ** get_address_of__outer_6() { return &____outer_6; }
	inline void set__outer_6(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD * value)
	{
		____outer_6 = value;
		Il2CppCodeGenWriteBarrier((&____outer_6), value);
	}

	inline static int32_t get_offset_of__out_7() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D, ____out_7)); }
	inline CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 * get__out_7() const { return ____out_7; }
	inline CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 ** get_address_of__out_7() { return &____out_7; }
	inline void set__out_7(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 * value)
	{
		____out_7 = value;
		Il2CppCodeGenWriteBarrier((&____out_7), value);
	}

	inline static int32_t get_offset_of__cGen_8() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D, ____cGen_8)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__cGen_8() const { return ____cGen_8; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__cGen_8() { return &____cGen_8; }
	inline void set__cGen_8(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____cGen_8 = value;
		Il2CppCodeGenWriteBarrier((&____cGen_8), value);
	}

	inline static int32_t get_offset_of__envGen_9() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D, ____envGen_9)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__envGen_9() const { return ____envGen_9; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__envGen_9() { return &____envGen_9; }
	inline void set__envGen_9(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____envGen_9 = value;
		Il2CppCodeGenWriteBarrier((&____envGen_9), value);
	}

	inline static int32_t get_offset_of__eiGen_10() { return static_cast<int32_t>(offsetof(CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D, ____eiGen_10)); }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * get__eiGen_10() const { return ____eiGen_10; }
	inline BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE ** get_address_of__eiGen_10() { return &____eiGen_10; }
	inline void set__eiGen_10(BerSequenceGenerator_tB08F3E7FAB8B098C647670310FD7DD5021D8B1CE * value)
	{
		____eiGen_10 = value;
		Il2CppCodeGenWriteBarrier((&____eiGen_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSENVELOPEDDATAOUTPUTSTREAM_T40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5000 = { sizeof (Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5000[12] = 
{
	0,
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_H1_5(),
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_H2_6(),
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_H3_7(),
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_H4_8(),
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_H5_9(),
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_X_10(),
	Sha1Digest_tF0E4819A2F90461E6D592F1F3B78A9BCDB6503EC::get_offset_of_xOff_11(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5001 = { sizeof (Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66), -1, sizeof(Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5001[12] = 
{
	0,
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H1_5(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H2_6(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H3_7(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H4_8(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H5_9(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H6_10(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H7_11(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_H8_12(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_X_13(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66::get_offset_of_xOff_14(),
	Sha224Digest_tA221B2B5844786D1ABA240D37373902286059F66_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5002 = { sizeof (Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8), -1, sizeof(Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5002[12] = 
{
	0,
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H1_5(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H2_6(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H3_7(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H4_8(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H5_9(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H6_10(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H7_11(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_H8_12(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_X_13(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8::get_offset_of_xOff_14(),
	Sha256Digest_t97BCF2BCCCC771ADCFEDC8AADAF871A174385BD8_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5003 = { sizeof (Sha384Digest_tB96C3F558807D7082BB4308C8B26931B75C57AC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5003[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5004 = { sizeof (Sha3Digest_tDF1048D4C4C4D4E1C7D321CF853F2A5B1070CA36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5005 = { sizeof (Sha512Digest_tB6EBA4F3BDD9117165EEA066650779851AAF310A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5005[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5006 = { sizeof (Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5006[10] = 
{
	0,
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_digestLength_17(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H1t_18(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H2t_19(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H3t_20(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H4t_21(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H5t_22(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H6t_23(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H7t_24(),
	Sha512tDigest_t799DB0F6185DE0B0FDF399FDB6757B11442D8590::get_offset_of_H8t_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5007 = { sizeof (ShakeDigest_t72AB41240CC33900FD697CB6E060E0DA509003A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5008 = { sizeof (ShortenedDigest_t9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5008[2] = 
{
	ShortenedDigest_t9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1::get_offset_of_baseDigest_0(),
	ShortenedDigest_t9A2D65AE5EE7EBB31F572E91980E2D3F8D890EB1::get_offset_of_length_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5009 = { sizeof (SkeinDigest_tB39A68555114FEF2E5204DBBC29CD4B91A7080E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5009[4] = 
{
	0,
	0,
	0,
	SkeinDigest_tB39A68555114FEF2E5204DBBC29CD4B91A7080E9::get_offset_of_engine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5010 = { sizeof (SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36), -1, sizeof(SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5010[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36_StaticFields::get_offset_of_INITIAL_STATES_7(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_threefish_8(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_outputSizeBytes_9(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_chain_10(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_initialState_11(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_key_12(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_preMessageParameters_13(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_postMessageParameters_14(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_ubi_15(),
	SkeinEngine_t7D2EE75541E20F35A410CAF628C8A99F8B676B36::get_offset_of_singleByte_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5011 = { sizeof (Configuration_t909112074B3AB1033D38060F7605F1987710E27B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5011[1] = 
{
	Configuration_t909112074B3AB1033D38060F7605F1987710E27B::get_offset_of_bytes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5012 = { sizeof (Parameter_t349B5E4529D512858A79C534325B5E73C9417C30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5012[2] = 
{
	Parameter_t349B5E4529D512858A79C534325B5E73C9417C30::get_offset_of_type_0(),
	Parameter_t349B5E4529D512858A79C534325B5E73C9417C30::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5013 = { sizeof (UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5013[5] = 
{
	0,
	0,
	0,
	UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D::get_offset_of_tweak_3(),
	UbiTweak_tAA61B75AF5F6DEB81B83826022BB8DCC091E286D::get_offset_of_extendedPosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5014 = { sizeof (UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5014[5] = 
{
	UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382::get_offset_of_tweak_0(),
	UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382::get_offset_of_engine_1(),
	UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382::get_offset_of_currentBlock_2(),
	UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382::get_offset_of_currentOffset_3(),
	UBI_tC7CFBFC9446D2FFCADBFFBACBEA3CB4BB6244382::get_offset_of_message_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5015 = { sizeof (SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5), -1, sizeof(SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5015[7] = 
{
	0,
	0,
	SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5::get_offset_of_V_6(),
	SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5::get_offset_of_inwords_7(),
	SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5::get_offset_of_xOff_8(),
	SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5::get_offset_of_W_9(),
	SM3Digest_t947D2183DBE3713D33C44D803EABF50473E457B5_StaticFields::get_offset_of_T_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5016 = { sizeof (TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518), -1, sizeof(TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5016[14] = 
{
	0,
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields::get_offset_of_t1_1(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields::get_offset_of_t2_2(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields::get_offset_of_t3_3(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518_StaticFields::get_offset_of_t4_4(),
	0,
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_a_6(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_b_7(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_c_8(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_byteCount_9(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_Buffer_10(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_bOff_11(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_x_12(),
	TigerDigest_tC8D84B3FA59BE23F784D838D6FDB21A7C1B45518::get_offset_of_xOff_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5017 = { sizeof (WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB), -1, sizeof(WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5017[24] = 
{
	0,
	0,
	0,
	0,
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_SBOX_4(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C0_5(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C1_6(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C2_7(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C3_8(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C4_9(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C5_10(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C6_11(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_C7_12(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__rc_13(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB_StaticFields::get_offset_of_EIGHT_14(),
	0,
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__buffer_16(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__bufferPos_17(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__bitCount_18(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__hash_19(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__K_20(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__L_21(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__block_22(),
	WhirlpoolDigest_tDED32B5555310790B28B191EE9786B7E61CCD0CB::get_offset_of__state_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5018 = { sizeof (DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5018[4] = 
{
	DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E::get_offset_of_key_0(),
	DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E::get_offset_of_dhParams_1(),
	DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E::get_offset_of_privateValue_2(),
	DHAgreement_t69438E270EDCAEAACE24B6D0D4A894A2B0DFC28E::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5019 = { sizeof (DHBasicAgreement_t6CCFB0CF3FFF6417C2409A457972547A6892A78F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5019[2] = 
{
	DHBasicAgreement_t6CCFB0CF3FFF6417C2409A457972547A6892A78F::get_offset_of_key_0(),
	DHBasicAgreement_t6CCFB0CF3FFF6417C2409A457972547A6892A78F::get_offset_of_dhParams_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5020 = { sizeof (DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34), -1, sizeof(DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5020[50] = 
{
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc2409_768_p_0(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc2409_768_g_1(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc2409_768_2(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc2409_1024_p_3(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc2409_1024_g_4(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc2409_1024_5(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_1536_p_6(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_1536_g_7(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_1536_8(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_2048_p_9(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_2048_g_10(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_2048_11(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_3072_p_12(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_3072_g_13(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_3072_14(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_4096_p_15(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_4096_g_16(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_4096_17(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_6144_p_18(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_6144_g_19(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_6144_20(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_8192_p_21(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_8192_g_22(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc3526_8192_23(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc4306_768_24(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc4306_1024_25(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_1024_160_p_26(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_1024_160_g_27(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_1024_160_q_28(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_1024_160_29(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_224_p_30(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_224_g_31(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_224_q_32(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_224_33(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_256_p_34(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_256_g_35(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_256_q_36(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5114_2048_256_37(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5996_768_38(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc5996_1024_39(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe2048_p_40(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe2048_41(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe3072_p_42(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe3072_43(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe4096_p_44(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe4096_45(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe6144_p_46(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe6144_47(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe8192_p_48(),
	DHStandardGroups_t2D0EC5970A2CB1B9FAC3C79C1D0B39AA9262DA34_StaticFields::get_offset_of_rfc7919_ffdhe8192_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5021 = { sizeof (ECDHBasicAgreement_tC891F91B06C6B18A64AA947C700C9B322C0E0F2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5021[1] = 
{
	ECDHBasicAgreement_tC891F91B06C6B18A64AA947C700C9B322C0E0F2F::get_offset_of_privKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5022 = { sizeof (ECDHCBasicAgreement_t2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5022[1] = 
{
	ECDHCBasicAgreement_t2C3C2A014BF8C41EC5E82F9E46FBF419F18D521B::get_offset_of_privKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5023 = { sizeof (ECDHWithKdfBasicAgreement_t571844060DBCFA1F91FC81F9489666A402F55785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5023[2] = 
{
	ECDHWithKdfBasicAgreement_t571844060DBCFA1F91FC81F9489666A402F55785::get_offset_of_algorithm_1(),
	ECDHWithKdfBasicAgreement_t571844060DBCFA1F91FC81F9489666A402F55785::get_offset_of_kdf_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5024 = { sizeof (ECMqvBasicAgreement_t16B063D215C9F45EECCBDFC5EDCBF22C852049C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5024[1] = 
{
	ECMqvBasicAgreement_t16B063D215C9F45EECCBDFC5EDCBF22C852049C4::get_offset_of_privParams_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5025 = { sizeof (ECMqvWithKdfBasicAgreement_t60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5025[2] = 
{
	ECMqvWithKdfBasicAgreement_t60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049::get_offset_of_algorithm_1(),
	ECMqvWithKdfBasicAgreement_t60E86FDB6DC5F09717A331C7CCD40DE5DC4E6049::get_offset_of_kdf_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5026 = { sizeof (SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5026[9] = 
{
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mDigest_0(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mUserID_1(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mStaticKey_2(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mStaticPubPoint_3(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mEphemeralPubPoint_4(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mECParams_5(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mW_6(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mEphemeralKey_7(),
	SM2KeyExchange_t75F4E762053841B1461A992AC03CA6B26259A2CA::get_offset_of_mInitiator_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5027 = { sizeof (X25519Agreement_t6B021DC295082FBACCC38468E1EB16D9935C0909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5027[1] = 
{
	X25519Agreement_t6B021DC295082FBACCC38468E1EB16D9935C0909::get_offset_of_privateKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5028 = { sizeof (X448Agreement_t9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5028[1] = 
{
	X448Agreement_t9B1D7560ACE61FFE9FC88A1C99A4090E80B3C427::get_offset_of_privateKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5029 = { sizeof (Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5029[13] = 
{
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_N_0(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_g_1(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_privA_2(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_pubA_3(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_B_4(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_x_5(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_u_6(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_S_7(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_M1_8(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_M2_9(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_Key_10(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_digest_11(),
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE::get_offset_of_random_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5030 = { sizeof (Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5030[13] = 
{
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_N_0(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_g_1(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_v_2(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_random_3(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_digest_4(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_A_5(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_privB_6(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_pubB_7(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_u_8(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_S_9(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_M1_10(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_M2_11(),
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344::get_offset_of_Key_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5031 = { sizeof (Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B), -1, sizeof(Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5031[21] = 
{
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_1024_2(),
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_1536_5(),
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_2048_8(),
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_3072_11(),
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_4096_14(),
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_6144_17(),
	0,
	0,
	Srp6StandardGroups_t4720717C06BC099DE95095BDE06A98B0A0F82A9B_StaticFields::get_offset_of_rfc5054_8192_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5032 = { sizeof (Srp6Utilities_tA5F33A63ED6B30EE471CCB7E6F272BF753E5E47A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5033 = { sizeof (Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5033[3] = 
{
	Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B::get_offset_of_N_0(),
	Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B::get_offset_of_g_1(),
	Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B::get_offset_of_digest_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5034 = { sizeof (ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5034[4] = 
{
	ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619::get_offset_of_mDigest_0(),
	ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619::get_offset_of_mShared_1(),
	ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619::get_offset_of_mOtherInfo_2(),
	ConcatenationKdfGenerator_t1037F79692EB4C491DACC55635D501259EF09619::get_offset_of_mHLen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5035 = { sizeof (DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5035[4] = 
{
	DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6::get_offset_of_algorithm_0(),
	DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6::get_offset_of_keySize_1(),
	DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6::get_offset_of_z_2(),
	DHKdfParameters_t511C519EE1A0B62760F6E3D49D1B92F6264A60B6::get_offset_of_extraInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5036 = { sizeof (DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5036[5] = 
{
	DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B::get_offset_of_digest_0(),
	DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B::get_offset_of_algorithm_1(),
	DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B::get_offset_of_keySize_2(),
	DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B::get_offset_of_z_3(),
	DHKekGenerator_t25F9FD92579F7EBACDBB95D305FAD4B1EA9B675B::get_offset_of_partyAInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5037 = { sizeof (ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5037[4] = 
{
	ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338::get_offset_of_kdf_0(),
	ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338::get_offset_of_algorithm_1(),
	ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338::get_offset_of_keySize_2(),
	ECDHKekGenerator_tD2756B47C0A037451C8B7E2D2E32A144D9D75338::get_offset_of_z_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5038 = { sizeof (JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0), -1, sizeof(JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5038[24] = 
{
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_INITIALIZED_0(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_ROUND_1_CREATED_1(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_ROUND_1_VALIDATED_2(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_ROUND_2_CREATED_3(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_ROUND_2_VALIDATED_4(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_KEY_CALCULATED_5(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_ROUND_3_CREATED_6(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0_StaticFields::get_offset_of_STATE_ROUND_3_VALIDATED_7(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_participantId_8(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_password_9(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_digest_10(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_random_11(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_p_12(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_q_13(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_g_14(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_partnerParticipantId_15(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_x1_16(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_x2_17(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_gx1_18(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_gx2_19(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_gx3_20(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_gx4_21(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_b_22(),
	JPakeParticipant_tE5F2F33CCF64B102CB84BA5D6136A05FEEC359B0::get_offset_of_state_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5039 = { sizeof (JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5039[3] = 
{
	JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902::get_offset_of_p_0(),
	JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902::get_offset_of_q_1(),
	JPakePrimeOrderGroup_t0600ED1E8BDB62D4CC0291A44A27E82AEC8C1902::get_offset_of_g_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5040 = { sizeof (JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D), -1, sizeof(JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5040[3] = 
{
	JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields::get_offset_of_SUN_JCE_1024_0(),
	JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields::get_offset_of_NIST_2048_1(),
	JPakePrimeOrderGroups_t2176906146D63FDAF25EA99EA207EA5E17923A7D_StaticFields::get_offset_of_NIST_3072_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5041 = { sizeof (JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5041[5] = 
{
	JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660::get_offset_of_participantId_0(),
	JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660::get_offset_of_gx1_1(),
	JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660::get_offset_of_gx2_2(),
	JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660::get_offset_of_knowledgeProofForX1_3(),
	JPakeRound1Payload_tD6CC10EDCC22855F12D84C2691E1BA9C50444660::get_offset_of_knowledgeProofForX2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5042 = { sizeof (JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5042[3] = 
{
	JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE::get_offset_of_participantId_0(),
	JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE::get_offset_of_a_1(),
	JPakeRound2Payload_t671906100BBEF16C804C7DDCC6FDBD28D243F3CE::get_offset_of_knowledgeProofForX2s_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5043 = { sizeof (JPakeRound3Payload_tC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5043[2] = 
{
	JPakeRound3Payload_tC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4::get_offset_of_participantId_0(),
	JPakeRound3Payload_tC860B38C3FD64E81434EBF48C6A0A7B0CD6CB8F4::get_offset_of_macTag_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5044 = { sizeof (JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796), -1, sizeof(JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5044[2] = 
{
	JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796_StaticFields::get_offset_of_Zero_0(),
	JPakeUtilities_t6F6B550F494694D32111CF5A13192B01DA2E2796_StaticFields::get_offset_of_One_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5045 = { sizeof (AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B), -1, sizeof(AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5045[2] = 
{
	AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B_StaticFields::get_offset_of_type_0(),
	AuthenticatorControl_t1EBBEB4478301069DE7ABF43A9A6EEF15824584B::get_offset_of_token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5046 = { sizeof (CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F), -1, sizeof(CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5046[6] = 
{
	CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields::get_offset_of_popRaVerified_0(),
	CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields::get_offset_of_popSigningKey_1(),
	CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields::get_offset_of_popKeyEncipherment_2(),
	CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F_StaticFields::get_offset_of_popKeyAgreement_3(),
	CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F::get_offset_of_certReqMsg_4(),
	CertificateRequestMessage_t6639D2B8546FECCAE7D09AC3541F38541B9EBB3F::get_offset_of_controls_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5047 = { sizeof (CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5047[12] = 
{
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__certReqId_0(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__extGenerator_1(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__templateBuilder_2(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__controls_3(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__popSigner_4(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__pkMacBuilder_5(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__password_6(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__sender_7(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__popoType_8(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__popoPrivKey_9(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__popRaVerified_10(),
	CertificateRequestMessageBuilder_tA52FA4B71621C2A988476C07CD13CBE246466016::get_offset_of__agreeMac_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5048 = { sizeof (CrmfException_t3785EC33CAB691AD6A472738E901DAF5F5C29915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5049 = { sizeof (DefaultPKMacPrimitivesProvider_tE3893E01783C636D581E2E89A77F2C99DA7D9CEF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5050 = { sizeof (EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5050[3] = 
{
	EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0::get_offset_of_wrapper_0(),
	EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0::get_offset_of_encryptor_1(),
	EncryptedValueBuilder_tE115869DB57B2CBEC68F60A169AB9E91FFE5BBA0::get_offset_of_padder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5051 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5052 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5054 = { sizeof (PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C), -1, sizeof(PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5054[5] = 
{
	PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields::get_offset_of_encryptedPrivKey_0(),
	PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields::get_offset_of_keyGenParameters_1(),
	PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields::get_offset_of_archiveRemGenPrivKey_2(),
	PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C_StaticFields::get_offset_of_type_3(),
	PkiArchiveControl_tFFAEE3EDEEBAA133749A3EE916E4FFF74DA73E1C::get_offset_of_pkiArchiveOptions_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5055 = { sizeof (PkiArchiveControlBuilder_tC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5055[2] = 
{
	PkiArchiveControlBuilder_tC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C::get_offset_of_envGen_0(),
	PkiArchiveControlBuilder_tC48935B4C4C8BB0981E9ACB3B24C7766AD2CF89C::get_offset_of_keyContent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5056 = { sizeof (PKMacStreamCalculator_tB5D66385DCB6D50B930F5181F3B680AD24335E62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5056[1] = 
{
	PKMacStreamCalculator_tB5D66385DCB6D50B930F5181F3B680AD24335E62::get_offset_of__stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5057 = { sizeof (PKMacFactory_tA09740F8AE4D4EB21AC478DA0980722011767367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5057[2] = 
{
	PKMacFactory_tA09740F8AE4D4EB21AC478DA0980722011767367::get_offset_of_parameters_0(),
	PKMacFactory_tA09740F8AE4D4EB21AC478DA0980722011767367::get_offset_of_key_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5058 = { sizeof (DefaultPKMacResult_tDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5058[1] = 
{
	DefaultPKMacResult_tDF8A3EC5621E79DDAB4BF8E9EE71A6CA65321840::get_offset_of_mac_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5059 = { sizeof (PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5059[8] = 
{
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_owf_0(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_mac_1(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_provider_2(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_random_3(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_parameters_4(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_iterationCount_5(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_saltLength_6(),
	PKMacBuilder_t4683635A2D7B64D1E00C680D714712BCE77E4492::get_offset_of_maxIterations_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5060 = { sizeof (ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5060[4] = 
{
	ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D::get_offset_of__certRequest_0(),
	ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D::get_offset_of__pubKeyInfo_1(),
	ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D::get_offset_of__name_2(),
	ProofOfPossessionSigningKeyBuilder_t812401F5D2B492D8EF1D83F945D04DC5AD5B7E2D::get_offset_of__publicKeyMAC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5061 = { sizeof (RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308), -1, sizeof(RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5061[2] = 
{
	RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308_StaticFields::get_offset_of_type_0(),
	RegTokenControl_t72EBBB5FD0A1B7AA5C908AD262FEBA01F5843308::get_offset_of_token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5062 = { sizeof (BaseDigestCalculator_t7AE8BDE54E11200464609A898F02DB974ECCE5C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5062[1] = 
{
	BaseDigestCalculator_t7AE8BDE54E11200464609A898F02DB974ECCE5C5::get_offset_of_digest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5063 = { sizeof (CmsAttributeTableGenerationException_tDA49B2DD2452DC16AABD1F0F720FE3CA1B46AC8A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5064 = { sizeof (CmsAttributeTableParameter_t3D362590ED58BEA61746116AF4DF1FFF65BDE6F4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5064[5] = 
{
	CmsAttributeTableParameter_t3D362590ED58BEA61746116AF4DF1FFF65BDE6F4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5065 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5066 = { sizeof (CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5066[6] = 
{
	CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F::get_offset_of_recipientInfoStore_0(),
	CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F::get_offset_of_contentInfo_1(),
	CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F::get_offset_of_macAlg_2(),
	CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F::get_offset_of_authAttrs_3(),
	CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F::get_offset_of_unauthAttrs_4(),
	CmsAuthenticatedData_t5181806CC3002A04DED8DFCDD546314EF678584F::get_offset_of_mac_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5067 = { sizeof (CmsAuthenticatedDataGenerator_t512FB9CE3F98E8BA6E7F65D70B4F3A581F421ACE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5068 = { sizeof (CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5068[8] = 
{
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of__recipientInfoStore_2(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_authData_3(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_macAlg_4(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_mac_5(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_authAttrs_6(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_unauthAttrs_7(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_authAttrNotRead_8(),
	CmsAuthenticatedDataParser_tC07375BACD16E00BFCDD0A3570C1727A87FCD2BA::get_offset_of_unauthAttrNotRead_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5069 = { sizeof (CmsAuthenticatedDataStreamGenerator_t3DFED1A81C465E4727D52754E808E2201928C28F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5069[2] = 
{
	CmsAuthenticatedDataStreamGenerator_t3DFED1A81C465E4727D52754E808E2201928C28F::get_offset_of__bufferSize_25(),
	CmsAuthenticatedDataStreamGenerator_t3DFED1A81C465E4727D52754E808E2201928C28F::get_offset_of__berEncodeRecipientSet_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5070 = { sizeof (CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5070[5] = 
{
	CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181::get_offset_of_macStream_6(),
	CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181::get_offset_of_mac_7(),
	CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181::get_offset_of_cGen_8(),
	CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181::get_offset_of_authGen_9(),
	CmsAuthenticatedDataOutputStream_t3829F3217EF177BBF298CAEFFA54C05A674B2181::get_offset_of_eiGen_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5071 = { sizeof (CmsAuthenticatedGenerator_tA48E87AF97A3418F9195CF2B8B252E54DEC7C247), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5072 = { sizeof (CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5072[7] = 
{
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_recipientInfoStore_0(),
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_contentInfo_1(),
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_originator_2(),
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_authEncAlg_3(),
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_authAttrs_4(),
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_mac_5(),
	CmsAuthEnvelopedData_t517E5C40E4E75E38F16766FC88C1C5F25F819978::get_offset_of_unauthAttrs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5073 = { sizeof (AuthEnvelopedSecureReadable_t8409506E05E8E91ABA88FB379084DBB07A081E47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5073[1] = 
{
	AuthEnvelopedSecureReadable_t8409506E05E8E91ABA88FB379084DBB07A081E47::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5074 = { sizeof (CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329), -1, sizeof(CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5074[6] = 
{
	CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields::get_offset_of_Aes128Ccm_0(),
	CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields::get_offset_of_Aes192Ccm_1(),
	CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields::get_offset_of_Aes256Ccm_2(),
	CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields::get_offset_of_Aes128Gcm_3(),
	CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields::get_offset_of_Aes192Gcm_4(),
	CmsAuthEnvelopedGenerator_tC107F620EADFAA47D0047668DCA6C62788F23329_StaticFields::get_offset_of_Aes256Gcm_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5075 = { sizeof (CmsCompressedData_t2D4EB4FADD970507CD26086C016747712D781963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5075[1] = 
{
	CmsCompressedData_t2D4EB4FADD970507CD26086C016747712D781963::get_offset_of_contentInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5076 = { sizeof (CmsCompressedDataGenerator_t6EECB1B6E90F463296C96F3F01EEF142D55DD63C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5076[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5077 = { sizeof (CmsCompressedDataParser_t48EAB91FA1B6508F1BCEEB8448CF015F15848C52), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5078 = { sizeof (CmsCompressedDataStreamGenerator_tE37BBC81C03C4015F63BBB71844579A2A5FFC1FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5078[2] = 
{
	0,
	CmsCompressedDataStreamGenerator_tE37BBC81C03C4015F63BBB71844579A2A5FFC1FC::get_offset_of__bufferSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5079 = { sizeof (CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5079[4] = 
{
	CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335::get_offset_of__out_6(),
	CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335::get_offset_of__sGen_7(),
	CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335::get_offset_of__cGen_8(),
	CmsCompressedOutputStream_t475308B7C248BE866ECD83D851DAE71971F78335::get_offset_of__eiGen_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5080 = { sizeof (CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5080[2] = 
{
	CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7::get_offset_of_contentInfo_0(),
	CmsContentInfoParser_t1DF71D42FC1E91EFF2A91F8B0F27B777A80B54F7::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5081 = { sizeof (CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5081[4] = 
{
	CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80::get_offset_of_recipientInfoStore_0(),
	CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80::get_offset_of_contentInfo_1(),
	CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80::get_offset_of_encAlg_2(),
	CmsEnvelopedData_t6923656C48AADD9EF806148608738F33CD875E80::get_offset_of_unprotectedAttributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5082 = { sizeof (CmsEnvelopedDataGenerator_t0281FB12E7237928F0F4C729B4D42A35CAB9E3DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5083 = { sizeof (CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5083[5] = 
{
	CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630::get_offset_of_recipientInfoStore_2(),
	CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630::get_offset_of_envelopedData_3(),
	CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630::get_offset_of__encAlg_4(),
	CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630::get_offset_of__unprotectedAttributes_5(),
	CmsEnvelopedDataParser_tCF26A03219DCBEC90C071D64F863A65690B9A630::get_offset_of__attrNotRead_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5084 = { sizeof (CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5084[4] = 
{
	CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D::get_offset_of__originatorInfo_25(),
	CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D::get_offset_of__unprotectedAttributes_26(),
	CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D::get_offset_of__bufferSize_27(),
	CmsEnvelopedDataStreamGenerator_t997BC9B3F5D50B35AF0EB20733C472415FFB382D::get_offset_of__berEncodeRecipientSet_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5085 = { sizeof (CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5085[5] = 
{
	CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D::get_offset_of__outer_6(),
	CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D::get_offset_of__out_7(),
	CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D::get_offset_of__cGen_8(),
	CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D::get_offset_of__envGen_9(),
	CmsEnvelopedDataOutputStream_t40E41F87ADE607AFA804EA56EDBFA9C0EFC1E62D::get_offset_of__eiGen_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5086 = { sizeof (CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD), -1, sizeof(CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5086[25] = 
{
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_rc2Table_0(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_DesEde3Cbc_1(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_RC2Cbc_2(),
	0,
	0,
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Aes128Cbc_5(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Aes192Cbc_6(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Aes256Cbc_7(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Camellia128Cbc_8(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Camellia192Cbc_9(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Camellia256Cbc_10(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_SeedCbc_11(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_DesEde3Wrap_12(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Aes128Wrap_13(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Aes192Wrap_14(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Aes256Wrap_15(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Camellia128Wrap_16(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Camellia192Wrap_17(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_Camellia256Wrap_18(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_SeedWrap_19(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_ECDHSha1Kdf_20(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD_StaticFields::get_offset_of_ECMqvSha1Kdf_21(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD::get_offset_of_recipientInfoGenerators_22(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD::get_offset_of_rand_23(),
	CmsEnvelopedGenerator_tA489671C4CD6F0E751A94A8ACCA446A9E8771ACD::get_offset_of_unprotectedAttributeGenerator_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5087 = { sizeof (CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954), -1, sizeof(CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5087[3] = 
{
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields::get_offset_of_Instance_0(),
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields::get_offset_of_KeySizes_1(),
	CmsEnvelopedHelper_t76D73BA581D300AE36633AA416596DA31C391954_StaticFields::get_offset_of_BaseCipherNames_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5088 = { sizeof (CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5088[3] = 
{
	CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541::get_offset_of_algorithm_0(),
	CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541::get_offset_of_mac_1(),
	CmsAuthenticatedSecureReadable_t64AD71AA236427A121C83DAD223B2EF05777A541::get_offset_of_readable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5089 = { sizeof (CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5089[3] = 
{
	CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782::get_offset_of_algorithm_0(),
	CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782::get_offset_of_cipher_1(),
	CmsEnvelopedSecureReadable_tA71BDD5AD9AA299BC8C62CFCDE15C18C01625782::get_offset_of_readable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5090 = { sizeof (CmsException_tD2E69995C20A2A3B6DCBA454EADA94CD3C4CD3F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5091 = { sizeof (CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5091[3] = 
{
	CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1::get_offset_of_password_0(),
	CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1::get_offset_of_salt_1(),
	CmsPbeKey_tADFFD1D80599F992DCFF6265B0F1FA1CA8E267D1::get_offset_of_iterationCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5093 = { sizeof (CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5093[2] = 
{
	CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF::get_offset_of_type_0(),
	CmsProcessableByteArray_tF0A4887E18B016A79DF99F6EDA0D8598F91F58BF::get_offset_of_bytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5094 = { sizeof (CmsProcessableFile_tEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5094[3] = 
{
	0,
	CmsProcessableFile_tEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842::get_offset_of__file_1(),
	CmsProcessableFile_tEB8A9F6EF370E18AF8E76CA3BDC77AFBE4118842::get_offset_of__bufSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5095 = { sizeof (CmsProcessableInputStream_t1A01AA3EF483F067E9CEB332995A131B0D453FED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5095[2] = 
{
	CmsProcessableInputStream_t1A01AA3EF483F067E9CEB332995A131B0D453FED::get_offset_of_input_0(),
	CmsProcessableInputStream_t1A01AA3EF483F067E9CEB332995A131B0D453FED::get_offset_of_used_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5096 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5098 = { sizeof (CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0), -1, sizeof(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5098[9] = 
{
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0_StaticFields::get_offset_of_Helper_0(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_signedContent_1(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_signedData_2(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_contentInfo_3(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_signerInfoStore_4(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_attrCertStore_5(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_certificateStore_6(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_crlStore_7(),
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0::get_offset_of_hashes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5099 = { sizeof (CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8), -1, sizeof(CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5099[2] = 
{
	CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8_StaticFields::get_offset_of_Helper_24(),
	CmsSignedDataGenerator_tE83D89AC0CA68D5D2EC1E86CA48496969B61D0D8::get_offset_of_signerInfs_25(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
