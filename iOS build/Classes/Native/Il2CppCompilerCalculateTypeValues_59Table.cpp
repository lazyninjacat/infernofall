﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.Cookies.Cookie
struct Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE;
// BestHTTP.Examples.HTTP.AssetBundleSample
struct AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA;
// BestHTTP.Examples.Helpers.SampleBase
struct SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF;
// BestHTTP.Examples.Helpers.TextListItem
struct TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD;
// BestHTTP.Examples.SampleRoot
struct SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2;
// BestHTTP.Examples.UploadHubSample
struct UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C;
// BestHTTP.HTTPRequest
struct HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837;
// BestHTTP.ServerSentEvents.EventSource
struct EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6;
// BestHTTP.ServerSentEvents.OnRetryDelegate
struct OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46;
// BestHTTP.SignalR.Authentication.IAuthenticationProvider
struct IAuthenticationProvider_t315F41E3719790A1AA175D6C730B8AD7348F161F;
// BestHTTP.SignalR.Authentication.OnAuthenticationFailedDelegate
struct OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4;
// BestHTTP.SignalR.Authentication.OnAuthenticationSuccededDelegate
struct OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55;
// BestHTTP.SignalR.Connection
struct Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1;
// BestHTTP.SignalR.Hubs.Hub
struct Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21;
// BestHTTP.SignalR.Hubs.Hub[]
struct HubU5BU5D_t2E3D30B254608EF91EC6A9E8713A544654E00E1B;
// BestHTTP.SignalR.Hubs.OnMethodCallDelegate
struct OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE;
// BestHTTP.SignalR.Hubs.OnMethodFailedDelegate
struct OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B;
// BestHTTP.SignalR.Hubs.OnMethodProgressDelegate
struct OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4;
// BestHTTP.SignalR.Hubs.OnMethodResultDelegate
struct OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F;
// BestHTTP.SignalR.IConnection
struct IConnection_t624BD039767B2B87A6BE453C342E9374FCF08BA7;
// BestHTTP.SignalR.JsonEncoders.IJsonEncoder
struct IJsonEncoder_t18CBE15B72D4209F701C3987B2495A2B7D490D14;
// BestHTTP.SignalR.Messages.FailureMessage
struct FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D;
// BestHTTP.SignalR.Messages.MethodCallMessage
struct MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A;
// BestHTTP.SignalR.Messages.MultiMessage
struct MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C;
// BestHTTP.SignalR.Messages.ProgressMessage
struct ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F;
// BestHTTP.SignalR.Messages.ResultMessage
struct ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA;
// BestHTTP.SignalR.NegotiationData
struct NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340;
// BestHTTP.SignalR.OnClosedDelegate
struct OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98;
// BestHTTP.SignalR.OnConnectedDelegate
struct OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C;
// BestHTTP.SignalR.OnErrorDelegate
struct OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4;
// BestHTTP.SignalR.OnNonHubMessageDelegate
struct OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4;
// BestHTTP.SignalR.OnPrepareRequestDelegate
struct OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81;
// BestHTTP.SignalR.OnStateChanged
struct OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249;
// BestHTTP.SignalR.Transports.OnTransportStateChangedDelegate
struct OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA;
// BestHTTP.SignalR.Transports.TransportBase
struct TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4;
// BestHTTP.SignalRCore.HubConnection
struct HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0;
// BestHTTP.SignalRCore.IEncoder
struct IEncoder_t9E478E3F98C5C187E74E4D43023109F86C5FED4E;
// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate
struct OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8;
// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate
struct OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333;
// BestHTTP.SignalRCore.UpStreamItemController`1<BestHTTP.Examples.Person>
struct UpStreamItemController_1_t934FC2E80240A3BB0074E91B276C5F0F7583E49C;
// BestHTTP.SignalRCore.UpStreamItemController`1<System.String>
struct UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7;
// BestHTTP.SignalRCore.UploadChannel`2<System.String,System.Int32>
struct UploadChannel_2_t8A303FC456BF99D5456A4612FB52558FA8566FE6;
// BestHTTP.SocketIO.Events.SocketIOCallback
struct SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81;
// BestHTTP.SocketIO.SocketManager
struct SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322;
// BestHTTP.WebSocket.WebSocket
struct WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717;
// LitJson.ImporterFunc`2<System.Double,System.Int32>
struct ImporterFunc_2_tB95D3E8C45F2F182CCF09C5626F72F4226F1BBB5;
// LitJson.ImporterFunc`2<System.Double,System.Single>
struct ImporterFunc_2_tFDCD0D067BEADC4D8348CEC0F6848C54B425FF93;
// LitJson.ImporterFunc`2<System.Int32,System.Int64>
struct ImporterFunc_2_tDAEEA219F637C6A0AFC2CA3A8FCB8C12B554EB2E;
// LitJson.ImporterFunc`2<System.Int64,System.Int32>
struct ImporterFunc_2_tAEB2964AD99C07458D1E12FC135FC6BB162BD571;
// LitJson.ImporterFunc`2<System.String,System.Byte[]>
struct ImporterFunc_2_tBBF14463CDBCB9A87A528512FC3B631590274B11;
// LitJson.ImporterFunc`2<System.String,System.DateTime>
struct ImporterFunc_2_tD380E76366033F6C81F25607F37693016040A394;
// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String>
struct ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE;
// System.Action`1<BestHTTP.SignalR.NegotiationData>
struct Action_1_t102A496083CF16B515632B8B5AF8186088B6E6B9;
// System.Action`2<BestHTTP.SignalR.NegotiationData,System.String>
struct Action_2_t66CE6B50694D4908323F1B592B784E18DDF508A7;
// System.Action`2<BestHTTP.SignalRCore.TransportStates,BestHTTP.SignalRCore.TransportStates>
struct Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SignalR.Hubs.OnMethodCallCallbackDelegate>
struct Dictionary_2_t41EA3D7980400B516B8F72E7A81AE6350474DBD4;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.UInt64,BestHTTP.SignalR.Messages.ClientMessage>
struct Dictionary_2_t99E4DFB056C2898BBDF19B3069AE04E4460723CA;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_tF7A51CC4084F4A990C7D9BF815DF842BDAEA0D13;
// System.Collections.Generic.List`1<BestHTTP.Examples.Helpers.SampleBase>
struct List_1_tCA227DE701D55D71B8DB42FDB56C2C68C9BC50AC;
// System.Collections.Generic.List`1<BestHTTP.HTTPRequest>
struct List_1_t0E64F6F010761063FA9757A1456A883274512FE8;
// System.Collections.Generic.List`1<BestHTTP.SignalR.Messages.IServerMessage>
struct List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.Message>
struct List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.SupportedTransport>
struct List_1_t21B4829E916812BBC60EB794AB541A5113820BCB;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<BestHTTP.Cookies.Cookie>
struct Predicate_1_t1D10832677445F77B97D6841EAB8D16D9F09E5FE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AssetBundle
struct AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t8D8FCFC0424680D7B5E04346D932442D5886CD2E;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_t67C3F4F02A9005DD5A2992187D47E8C5B8031C36;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.RawImage[]
struct RawImageU5BU5D_tA6FB181A0441558D4F14E4BB982E772FE437525D;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GUIHELPER_T2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_H
#define GUIHELPER_T2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.GUIHelper
struct  GUIHelper_t2545CAEC80C2D121E0B0C5A4FA68E03478032AE0  : public RuntimeObject
{
public:

public:
};

struct GUIHelper_t2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_StaticFields
{
public:
	// System.String[] BestHTTP.Examples.GUIHelper::prefixes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___prefixes_0;

public:
	inline static int32_t get_offset_of_prefixes_0() { return static_cast<int32_t>(offsetof(GUIHelper_t2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_StaticFields, ___prefixes_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_prefixes_0() const { return ___prefixes_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_prefixes_0() { return &___prefixes_0; }
	inline void set_prefixes_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___prefixes_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIHELPER_T2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_H
#ifndef U3CSCROLLTOBOTTOMU3ED__4_TECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3_H
#define U3CSCROLLTOBOTTOMU3ED__4_TECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.GUIHelper_<ScrollToBottom>d__4
struct  U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.GUIHelper_<ScrollToBottom>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.GUIHelper_<ScrollToBottom>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.GUIHelper_<ScrollToBottom>d__4::scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ___scrollRect_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_scrollRect_2() { return static_cast<int32_t>(offsetof(U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3, ___scrollRect_2)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get_scrollRect_2() const { return ___scrollRect_2; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of_scrollRect_2() { return &___scrollRect_2; }
	inline void set_scrollRect_2(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		___scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCROLLTOBOTTOMU3ED__4_TECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3_H
#ifndef U3CDOWNLOADASSETBUNDLEU3ED__10_T2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4_H
#define U3CDOWNLOADASSETBUNDLEU3ED__10_T2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HTTP.AssetBundleSample_<DownloadAssetBundle>d__10
struct  U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.HTTP.AssetBundleSample_<DownloadAssetBundle>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.HTTP.AssetBundleSample_<DownloadAssetBundle>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BestHTTP.Examples.HTTP.AssetBundleSample BestHTTP.Examples.HTTP.AssetBundleSample_<DownloadAssetBundle>d__10::<>4__this
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA * ___U3CU3E4__this_2;
	// UnityEngine.AssetBundleCreateRequest BestHTTP.Examples.HTTP.AssetBundleSample_<DownloadAssetBundle>d__10::<async>5__2
	AssetBundleCreateRequest_t8D8FCFC0424680D7B5E04346D932442D5886CD2E * ___U3CasyncU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4, ___U3CU3E4__this_2)); }
	inline AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4, ___U3CasyncU3E5__2_3)); }
	inline AssetBundleCreateRequest_t8D8FCFC0424680D7B5E04346D932442D5886CD2E * get_U3CasyncU3E5__2_3() const { return ___U3CasyncU3E5__2_3; }
	inline AssetBundleCreateRequest_t8D8FCFC0424680D7B5E04346D932442D5886CD2E ** get_address_of_U3CasyncU3E5__2_3() { return &___U3CasyncU3E5__2_3; }
	inline void set_U3CasyncU3E5__2_3(AssetBundleCreateRequest_t8D8FCFC0424680D7B5E04346D932442D5886CD2E * value)
	{
		___U3CasyncU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADASSETBUNDLEU3ED__10_T2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4_H
#ifndef U3CPROCESSASSETBUNDLEU3ED__11_T57504E1491C6FDB9E1AF2B1CFB200370C40D758E_H
#define U3CPROCESSASSETBUNDLEU3ED__11_T57504E1491C6FDB9E1AF2B1CFB200370C40D758E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HTTP.AssetBundleSample_<ProcessAssetBundle>d__11
struct  U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.HTTP.AssetBundleSample_<ProcessAssetBundle>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.HTTP.AssetBundleSample_<ProcessAssetBundle>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.AssetBundle BestHTTP.Examples.HTTP.AssetBundleSample_<ProcessAssetBundle>d__11::bundle
	AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * ___bundle_2;
	// BestHTTP.Examples.HTTP.AssetBundleSample BestHTTP.Examples.HTTP.AssetBundleSample_<ProcessAssetBundle>d__11::<>4__this
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA * ___U3CU3E4__this_3;
	// UnityEngine.AssetBundleRequest BestHTTP.Examples.HTTP.AssetBundleSample_<ProcessAssetBundle>d__11::<asyncAsset>5__2
	AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * ___U3CasyncAssetU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_bundle_2() { return static_cast<int32_t>(offsetof(U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E, ___bundle_2)); }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * get_bundle_2() const { return ___bundle_2; }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 ** get_address_of_bundle_2() { return &___bundle_2; }
	inline void set_bundle_2(AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * value)
	{
		___bundle_2 = value;
		Il2CppCodeGenWriteBarrier((&___bundle_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E, ___U3CU3E4__this_3)); }
	inline AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CasyncAssetU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E, ___U3CasyncAssetU3E5__2_4)); }
	inline AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * get_U3CasyncAssetU3E5__2_4() const { return ___U3CasyncAssetU3E5__2_4; }
	inline AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 ** get_address_of_U3CasyncAssetU3E5__2_4() { return &___U3CasyncAssetU3E5__2_4; }
	inline void set_U3CasyncAssetU3E5__2_4(AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * value)
	{
		___U3CasyncAssetU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncAssetU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSASSETBUNDLEU3ED__11_T57504E1491C6FDB9E1AF2B1CFB200370C40D758E_H
#ifndef PERSON_TE39ECC4E37B08FB209284AF23FC90E31BD156210_H
#define PERSON_TE39ECC4E37B08FB209284AF23FC90E31BD156210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Person
struct  Person_tE39ECC4E37B08FB209284AF23FC90E31BD156210  : public RuntimeObject
{
public:
	// System.String BestHTTP.Examples.Person::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int64 BestHTTP.Examples.Person::<Age>k__BackingField
	int64_t ___U3CAgeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Person_tE39ECC4E37B08FB209284AF23FC90E31BD156210, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAgeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Person_tE39ECC4E37B08FB209284AF23FC90E31BD156210, ___U3CAgeU3Ek__BackingField_1)); }
	inline int64_t get_U3CAgeU3Ek__BackingField_1() const { return ___U3CAgeU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CAgeU3Ek__BackingField_1() { return &___U3CAgeU3Ek__BackingField_1; }
	inline void set_U3CAgeU3Ek__BackingField_1(int64_t value)
	{
		___U3CAgeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSON_TE39ECC4E37B08FB209284AF23FC90E31BD156210_H
#ifndef PREAUTHACCESSTOKENAUTHENTICATOR_T2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55_H
#define PREAUTHACCESSTOKENAUTHENTICATOR_T2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.PreAuthAccessTokenAuthenticator
struct  PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate BestHTTP.Examples.PreAuthAccessTokenAuthenticator::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * ___OnAuthenticationSucceded_0;
	// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate BestHTTP.Examples.PreAuthAccessTokenAuthenticator::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * ___OnAuthenticationFailed_1;
	// System.Uri BestHTTP.Examples.PreAuthAccessTokenAuthenticator::authenticationUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___authenticationUri_2;
	// System.String BestHTTP.Examples.PreAuthAccessTokenAuthenticator::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_OnAuthenticationSucceded_0() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55, ___OnAuthenticationSucceded_0)); }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * get_OnAuthenticationSucceded_0() const { return ___OnAuthenticationSucceded_0; }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 ** get_address_of_OnAuthenticationSucceded_0() { return &___OnAuthenticationSucceded_0; }
	inline void set_OnAuthenticationSucceded_0(OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * value)
	{
		___OnAuthenticationSucceded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationSucceded_0), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_1() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55, ___OnAuthenticationFailed_1)); }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * get_OnAuthenticationFailed_1() const { return ___OnAuthenticationFailed_1; }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 ** get_address_of_OnAuthenticationFailed_1() { return &___OnAuthenticationFailed_1; }
	inline void set_OnAuthenticationFailed_1(OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * value)
	{
		___OnAuthenticationFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationFailed_1), value);
	}

	inline static int32_t get_offset_of_authenticationUri_2() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55, ___authenticationUri_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_authenticationUri_2() const { return ___authenticationUri_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_authenticationUri_2() { return &___authenticationUri_2; }
	inline void set_authenticationUri_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___authenticationUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___authenticationUri_2), value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55, ___U3CTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_3() const { return ___U3CTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_3() { return &___U3CTokenU3Ek__BackingField_3; }
	inline void set_U3CTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREAUTHACCESSTOKENAUTHENTICATOR_T2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55_H
#ifndef REDIRECTLOGGERACCESSTOKENAUTHENTICATOR_TDFCFD4E9CB1D51E20A53F73701F6AB761E064A40_H
#define REDIRECTLOGGERACCESSTOKENAUTHENTICATOR_TDFCFD4E9CB1D51E20A53F73701F6AB761E064A40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.RedirectLoggerAccessTokenAuthenticator
struct  RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate BestHTTP.Examples.RedirectLoggerAccessTokenAuthenticator::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * ___OnAuthenticationSucceded_0;
	// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate BestHTTP.Examples.RedirectLoggerAccessTokenAuthenticator::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * ___OnAuthenticationFailed_1;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.RedirectLoggerAccessTokenAuthenticator::_connection
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ____connection_2;

public:
	inline static int32_t get_offset_of_OnAuthenticationSucceded_0() { return static_cast<int32_t>(offsetof(RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40, ___OnAuthenticationSucceded_0)); }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * get_OnAuthenticationSucceded_0() const { return ___OnAuthenticationSucceded_0; }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 ** get_address_of_OnAuthenticationSucceded_0() { return &___OnAuthenticationSucceded_0; }
	inline void set_OnAuthenticationSucceded_0(OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * value)
	{
		___OnAuthenticationSucceded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationSucceded_0), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_1() { return static_cast<int32_t>(offsetof(RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40, ___OnAuthenticationFailed_1)); }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * get_OnAuthenticationFailed_1() const { return ___OnAuthenticationFailed_1; }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 ** get_address_of_OnAuthenticationFailed_1() { return &___OnAuthenticationFailed_1; }
	inline void set_OnAuthenticationFailed_1(OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * value)
	{
		___OnAuthenticationFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationFailed_1), value);
	}

	inline static int32_t get_offset_of__connection_2() { return static_cast<int32_t>(offsetof(RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40, ____connection_2)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get__connection_2() const { return ____connection_2; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of__connection_2() { return &____connection_2; }
	inline void set__connection_2(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		____connection_2 = value;
		Il2CppCodeGenWriteBarrier((&____connection_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REDIRECTLOGGERACCESSTOKENAUTHENTICATOR_TDFCFD4E9CB1D51E20A53F73701F6AB761E064A40_H
#ifndef DATETIMEDATA_TF2E6D00C6143E9DD7B559EBEECAC681C36978CB9_H
#define DATETIMEDATA_TF2E6D00C6143E9DD7B559EBEECAC681C36978CB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.ServerSentEvents.DateTimeData
struct  DateTimeData_tF2E6D00C6143E9DD7B559EBEECAC681C36978CB9  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.ServerSentEvents.DateTimeData::eventid
	int32_t ___eventid_0;
	// System.String BestHTTP.Examples.ServerSentEvents.DateTimeData::datetime
	String_t* ___datetime_1;

public:
	inline static int32_t get_offset_of_eventid_0() { return static_cast<int32_t>(offsetof(DateTimeData_tF2E6D00C6143E9DD7B559EBEECAC681C36978CB9, ___eventid_0)); }
	inline int32_t get_eventid_0() const { return ___eventid_0; }
	inline int32_t* get_address_of_eventid_0() { return &___eventid_0; }
	inline void set_eventid_0(int32_t value)
	{
		___eventid_0 = value;
	}

	inline static int32_t get_offset_of_datetime_1() { return static_cast<int32_t>(offsetof(DateTimeData_tF2E6D00C6143E9DD7B559EBEECAC681C36978CB9, ___datetime_1)); }
	inline String_t* get_datetime_1() const { return ___datetime_1; }
	inline String_t** get_address_of_datetime_1() { return &___datetime_1; }
	inline void set_datetime_1(String_t* value)
	{
		___datetime_1 = value;
		Il2CppCodeGenWriteBarrier((&___datetime_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEDATA_TF2E6D00C6143E9DD7B559EBEECAC681C36978CB9_H
#ifndef U3CU3EC_TBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_H
#define U3CU3EC_TBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.SocketIOChatSample_<>c
struct  U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_StaticFields
{
public:
	// BestHTTP.Examples.SocketIOChatSample_<>c BestHTTP.Examples.SocketIOChatSample_<>c::<>9
	U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3 * ___U3CU3E9_0;
	// BestHTTP.SocketIO.Events.SocketIOCallback BestHTTP.Examples.SocketIOChatSample_<>c::<>9__22_2
	SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81 * ___U3CU3E9__22_2_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_StaticFields, ___U3CU3E9__22_2_1)); }
	inline SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81 * get_U3CU3E9__22_2_1() const { return ___U3CU3E9__22_2_1; }
	inline SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81 ** get_address_of_U3CU3E9__22_2_1() { return &___U3CU3E9__22_2_1; }
	inline void set_U3CU3E9__22_2_1(SocketIOCallback_t90E2FD268FCCB480BBDACFCD4B73425A47818B81 * value)
	{
		___U3CU3E9__22_2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B_H
#define U3CU3EC__DISPLAYCLASS29_0_T16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.SocketIOChatSample_<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B  : public RuntimeObject
{
public:
	// System.String BestHTTP.Examples.SocketIOChatSample_<>c__DisplayClass29_0::username
	String_t* ___username_0;

public:
	inline static int32_t get_offset_of_username_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B, ___username_0)); }
	inline String_t* get_username_0() const { return ___username_0; }
	inline String_t** get_address_of_username_0() { return &___username_0; }
	inline void set_username_0(String_t* value)
	{
		___username_0 = value;
		Il2CppCodeGenWriteBarrier((&___username_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T0E12188FFEB973328C181F3F472B1C901FEA4C96_H
#define U3CU3EC__DISPLAYCLASS30_0_T0E12188FFEB973328C181F3F472B1C901FEA4C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.SocketIOChatSample_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t0E12188FFEB973328C181F3F472B1C901FEA4C96  : public RuntimeObject
{
public:
	// System.String BestHTTP.Examples.SocketIOChatSample_<>c__DisplayClass30_0::username
	String_t* ___username_0;

public:
	inline static int32_t get_offset_of_username_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t0E12188FFEB973328C181F3F472B1C901FEA4C96, ___username_0)); }
	inline String_t* get_username_0() const { return ___username_0; }
	inline String_t** get_address_of_username_0() { return &___username_0; }
	inline void set_username_0(String_t* value)
	{
		___username_0 = value;
		Il2CppCodeGenWriteBarrier((&___username_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T0E12188FFEB973328C181F3F472B1C901FEA4C96_H
#ifndef PERSON_T375BD9876B7095CBCB2ED9C7606D5839670AFC4B_H
#define PERSON_T375BD9876B7095CBCB2ED9C7606D5839670AFC4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.TestHubSample_Person
struct  Person_t375BD9876B7095CBCB2ED9C7606D5839670AFC4B  : public RuntimeObject
{
public:
	// System.String BestHTTP.Examples.TestHubSample_Person::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int64 BestHTTP.Examples.TestHubSample_Person::<Age>k__BackingField
	int64_t ___U3CAgeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Person_t375BD9876B7095CBCB2ED9C7606D5839670AFC4B, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAgeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Person_t375BD9876B7095CBCB2ED9C7606D5839670AFC4B, ___U3CAgeU3Ek__BackingField_1)); }
	inline int64_t get_U3CAgeU3Ek__BackingField_1() const { return ___U3CAgeU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CAgeU3Ek__BackingField_1() { return &___U3CAgeU3Ek__BackingField_1; }
	inline void set_U3CAgeU3Ek__BackingField_1(int64_t value)
	{
		___U3CAgeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSON_T375BD9876B7095CBCB2ED9C7606D5839670AFC4B_H
#ifndef U3CPERSONECHOU3ED__19_T3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE_H
#define U3CPERSONECHOU3ED__19_T3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadHubSample_<PersonEcho>d__19
struct  U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.UploadHubSample_<PersonEcho>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.UploadHubSample_<PersonEcho>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BestHTTP.Examples.UploadHubSample BestHTTP.Examples.UploadHubSample_<PersonEcho>d__19::<>4__this
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * ___U3CU3E4__this_2;
	// BestHTTP.SignalRCore.UpStreamItemController`1<BestHTTP.Examples.Person> BestHTTP.Examples.UploadHubSample_<PersonEcho>d__19::<controller>5__2
	UpStreamItemController_1_t934FC2E80240A3BB0074E91B276C5F0F7583E49C * ___U3CcontrollerU3E5__2_3;
	// System.Int32 BestHTTP.Examples.UploadHubSample_<PersonEcho>d__19::<i>5__3
	int32_t ___U3CiU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE, ___U3CU3E4__this_2)); }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE, ___U3CcontrollerU3E5__2_3)); }
	inline UpStreamItemController_1_t934FC2E80240A3BB0074E91B276C5F0F7583E49C * get_U3CcontrollerU3E5__2_3() const { return ___U3CcontrollerU3E5__2_3; }
	inline UpStreamItemController_1_t934FC2E80240A3BB0074E91B276C5F0F7583E49C ** get_address_of_U3CcontrollerU3E5__2_3() { return &___U3CcontrollerU3E5__2_3; }
	inline void set_U3CcontrollerU3E5__2_3(UpStreamItemController_1_t934FC2E80240A3BB0074E91B276C5F0F7583E49C * value)
	{
		___U3CcontrollerU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE, ___U3CiU3E5__3_4)); }
	inline int32_t get_U3CiU3E5__3_4() const { return ___U3CiU3E5__3_4; }
	inline int32_t* get_address_of_U3CiU3E5__3_4() { return &___U3CiU3E5__3_4; }
	inline void set_U3CiU3E5__3_4(int32_t value)
	{
		___U3CiU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPERSONECHOU3ED__19_T3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE_H
#ifndef U3CSCORETRACKERU3ED__16_T8FCC86CE102EA8D32DB03262B005B79010301481_H
#define U3CSCORETRACKERU3ED__16_T8FCC86CE102EA8D32DB03262B005B79010301481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadHubSample_<ScoreTracker>d__16
struct  U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.UploadHubSample_<ScoreTracker>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.UploadHubSample_<ScoreTracker>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BestHTTP.Examples.UploadHubSample BestHTTP.Examples.UploadHubSample_<ScoreTracker>d__16::<>4__this
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * ___U3CU3E4__this_2;
	// BestHTTP.SignalRCore.UpStreamItemController`1<System.String> BestHTTP.Examples.UploadHubSample_<ScoreTracker>d__16::<controller>5__2
	UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * ___U3CcontrollerU3E5__2_3;
	// System.Int32 BestHTTP.Examples.UploadHubSample_<ScoreTracker>d__16::<i>5__3
	int32_t ___U3CiU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481, ___U3CU3E4__this_2)); }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481, ___U3CcontrollerU3E5__2_3)); }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * get_U3CcontrollerU3E5__2_3() const { return ___U3CcontrollerU3E5__2_3; }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 ** get_address_of_U3CcontrollerU3E5__2_3() { return &___U3CcontrollerU3E5__2_3; }
	inline void set_U3CcontrollerU3E5__2_3(UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * value)
	{
		___U3CcontrollerU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481, ___U3CiU3E5__3_4)); }
	inline int32_t get_U3CiU3E5__3_4() const { return ___U3CiU3E5__3_4; }
	inline int32_t* get_address_of_U3CiU3E5__3_4() { return &___U3CiU3E5__3_4; }
	inline void set_U3CiU3E5__3_4(int32_t value)
	{
		___U3CiU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCORETRACKERU3ED__16_T8FCC86CE102EA8D32DB03262B005B79010301481_H
#ifndef U3CSCORETRACKERWITHPARAMETERCHANNELSU3ED__17_T4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF_H
#define U3CSCORETRACKERWITHPARAMETERCHANNELSU3ED__17_T4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17
struct  U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BestHTTP.Examples.UploadHubSample BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17::<>4__this
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * ___U3CU3E4__this_2;
	// BestHTTP.SignalRCore.UpStreamItemController`1<System.String> BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17::<controller>5__2
	UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * ___U3CcontrollerU3E5__2_3;
	// BestHTTP.SignalRCore.UploadChannel`2<System.String,System.Int32> BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17::<player1param>5__3
	UploadChannel_2_t8A303FC456BF99D5456A4612FB52558FA8566FE6 * ___U3Cplayer1paramU3E5__3_4;
	// System.Int32 BestHTTP.Examples.UploadHubSample_<ScoreTrackerWithParameterChannels>d__17::<i>5__4
	int32_t ___U3CiU3E5__4_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF, ___U3CU3E4__this_2)); }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF, ___U3CcontrollerU3E5__2_3)); }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * get_U3CcontrollerU3E5__2_3() const { return ___U3CcontrollerU3E5__2_3; }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 ** get_address_of_U3CcontrollerU3E5__2_3() { return &___U3CcontrollerU3E5__2_3; }
	inline void set_U3CcontrollerU3E5__2_3(UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * value)
	{
		___U3CcontrollerU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3Cplayer1paramU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF, ___U3Cplayer1paramU3E5__3_4)); }
	inline UploadChannel_2_t8A303FC456BF99D5456A4612FB52558FA8566FE6 * get_U3Cplayer1paramU3E5__3_4() const { return ___U3Cplayer1paramU3E5__3_4; }
	inline UploadChannel_2_t8A303FC456BF99D5456A4612FB52558FA8566FE6 ** get_address_of_U3Cplayer1paramU3E5__3_4() { return &___U3Cplayer1paramU3E5__3_4; }
	inline void set_U3Cplayer1paramU3E5__3_4(UploadChannel_2_t8A303FC456BF99D5456A4612FB52558FA8566FE6 * value)
	{
		___U3Cplayer1paramU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cplayer1paramU3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF, ___U3CiU3E5__4_5)); }
	inline int32_t get_U3CiU3E5__4_5() const { return ___U3CiU3E5__4_5; }
	inline int32_t* get_address_of_U3CiU3E5__4_5() { return &___U3CiU3E5__4_5; }
	inline void set_U3CiU3E5__4_5(int32_t value)
	{
		___U3CiU3E5__4_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCORETRACKERWITHPARAMETERCHANNELSU3ED__17_T4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF_H
#ifndef U3CSTREAMECHOU3ED__18_TD87052C49E086FF61F51A00F76239E5ED45B94D6_H
#define U3CSTREAMECHOU3ED__18_TD87052C49E086FF61F51A00F76239E5ED45B94D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadHubSample_<StreamEcho>d__18
struct  U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.UploadHubSample_<StreamEcho>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.UploadHubSample_<StreamEcho>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BestHTTP.Examples.UploadHubSample BestHTTP.Examples.UploadHubSample_<StreamEcho>d__18::<>4__this
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * ___U3CU3E4__this_2;
	// BestHTTP.SignalRCore.UpStreamItemController`1<System.String> BestHTTP.Examples.UploadHubSample_<StreamEcho>d__18::<controller>5__2
	UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * ___U3CcontrollerU3E5__2_3;
	// System.Int32 BestHTTP.Examples.UploadHubSample_<StreamEcho>d__18::<i>5__3
	int32_t ___U3CiU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6, ___U3CU3E4__this_2)); }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6, ___U3CcontrollerU3E5__2_3)); }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * get_U3CcontrollerU3E5__2_3() const { return ___U3CcontrollerU3E5__2_3; }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 ** get_address_of_U3CcontrollerU3E5__2_3() { return &___U3CcontrollerU3E5__2_3; }
	inline void set_U3CcontrollerU3E5__2_3(UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * value)
	{
		___U3CcontrollerU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6, ___U3CiU3E5__3_4)); }
	inline int32_t get_U3CiU3E5__3_4() const { return ___U3CiU3E5__3_4; }
	inline int32_t* get_address_of_U3CiU3E5__3_4() { return &___U3CiU3E5__3_4; }
	inline void set_U3CiU3E5__3_4(int32_t value)
	{
		___U3CiU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTREAMECHOU3ED__18_TD87052C49E086FF61F51A00F76239E5ED45B94D6_H
#ifndef U3CUPLOADWORDU3ED__15_TA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5_H
#define U3CUPLOADWORDU3ED__15_TA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadHubSample_<UploadWord>d__15
struct  U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Examples.UploadHubSample_<UploadWord>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BestHTTP.Examples.UploadHubSample_<UploadWord>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BestHTTP.Examples.UploadHubSample BestHTTP.Examples.UploadHubSample_<UploadWord>d__15::<>4__this
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * ___U3CU3E4__this_2;
	// BestHTTP.SignalRCore.UpStreamItemController`1<System.String> BestHTTP.Examples.UploadHubSample_<UploadWord>d__15::<controller>5__2
	UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * ___U3CcontrollerU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5, ___U3CU3E4__this_2)); }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcontrollerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5, ___U3CcontrollerU3E5__2_3)); }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * get_U3CcontrollerU3E5__2_3() const { return ___U3CcontrollerU3E5__2_3; }
	inline UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 ** get_address_of_U3CcontrollerU3E5__2_3() { return &___U3CcontrollerU3E5__2_3; }
	inline void set_U3CcontrollerU3E5__2_3(UpStreamItemController_1_t97420141F942478BA4BB80AFE069C206485332B7 * value)
	{
		___U3CcontrollerU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontrollerU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPLOADWORDU3ED__15_TA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5_H
#ifndef HEADERAUTHENTICATOR_TEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC_H
#define HEADERAUTHENTICATOR_TEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Authentication.HeaderAuthenticator
struct  HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalR.Authentication.HeaderAuthenticator::<User>k__BackingField
	String_t* ___U3CUserU3Ek__BackingField_0;
	// System.String BestHTTP.SignalR.Authentication.HeaderAuthenticator::<Roles>k__BackingField
	String_t* ___U3CRolesU3Ek__BackingField_1;
	// BestHTTP.SignalR.Authentication.OnAuthenticationSuccededDelegate BestHTTP.SignalR.Authentication.HeaderAuthenticator::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 * ___OnAuthenticationSucceded_2;
	// BestHTTP.SignalR.Authentication.OnAuthenticationFailedDelegate BestHTTP.SignalR.Authentication.HeaderAuthenticator::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 * ___OnAuthenticationFailed_3;

public:
	inline static int32_t get_offset_of_U3CUserU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC, ___U3CUserU3Ek__BackingField_0)); }
	inline String_t* get_U3CUserU3Ek__BackingField_0() const { return ___U3CUserU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUserU3Ek__BackingField_0() { return &___U3CUserU3Ek__BackingField_0; }
	inline void set_U3CUserU3Ek__BackingField_0(String_t* value)
	{
		___U3CUserU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRolesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC, ___U3CRolesU3Ek__BackingField_1)); }
	inline String_t* get_U3CRolesU3Ek__BackingField_1() const { return ___U3CRolesU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRolesU3Ek__BackingField_1() { return &___U3CRolesU3Ek__BackingField_1; }
	inline void set_U3CRolesU3Ek__BackingField_1(String_t* value)
	{
		___U3CRolesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRolesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationSucceded_2() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC, ___OnAuthenticationSucceded_2)); }
	inline OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 * get_OnAuthenticationSucceded_2() const { return ___OnAuthenticationSucceded_2; }
	inline OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 ** get_address_of_OnAuthenticationSucceded_2() { return &___OnAuthenticationSucceded_2; }
	inline void set_OnAuthenticationSucceded_2(OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 * value)
	{
		___OnAuthenticationSucceded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationSucceded_2), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_3() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC, ___OnAuthenticationFailed_3)); }
	inline OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 * get_OnAuthenticationFailed_3() const { return ___OnAuthenticationFailed_3; }
	inline OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 ** get_address_of_OnAuthenticationFailed_3() { return &___OnAuthenticationFailed_3; }
	inline void set_OnAuthenticationFailed_3(OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 * value)
	{
		___OnAuthenticationFailed_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationFailed_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERAUTHENTICATOR_TEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC_H
#ifndef SAMPLECOOKIEAUTHENTICATION_T468705BBD4859389B548F9111453669A1447A4E7_H
#define SAMPLECOOKIEAUTHENTICATION_T468705BBD4859389B548F9111453669A1447A4E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Authentication.SampleCookieAuthentication
struct  SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.SignalR.Authentication.SampleCookieAuthentication::<AuthUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CAuthUriU3Ek__BackingField_0;
	// System.String BestHTTP.SignalR.Authentication.SampleCookieAuthentication::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_1;
	// System.String BestHTTP.SignalR.Authentication.SampleCookieAuthentication::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_2;
	// System.String BestHTTP.SignalR.Authentication.SampleCookieAuthentication::<UserRoles>k__BackingField
	String_t* ___U3CUserRolesU3Ek__BackingField_3;
	// System.Boolean BestHTTP.SignalR.Authentication.SampleCookieAuthentication::<IsPreAuthRequired>k__BackingField
	bool ___U3CIsPreAuthRequiredU3Ek__BackingField_4;
	// BestHTTP.SignalR.Authentication.OnAuthenticationSuccededDelegate BestHTTP.SignalR.Authentication.SampleCookieAuthentication::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 * ___OnAuthenticationSucceded_5;
	// BestHTTP.SignalR.Authentication.OnAuthenticationFailedDelegate BestHTTP.SignalR.Authentication.SampleCookieAuthentication::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 * ___OnAuthenticationFailed_6;
	// BestHTTP.HTTPRequest BestHTTP.SignalR.Authentication.SampleCookieAuthentication::AuthRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___AuthRequest_7;
	// BestHTTP.Cookies.Cookie BestHTTP.SignalR.Authentication.SampleCookieAuthentication::Cookie
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE * ___Cookie_8;

public:
	inline static int32_t get_offset_of_U3CAuthUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___U3CAuthUriU3Ek__BackingField_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CAuthUriU3Ek__BackingField_0() const { return ___U3CAuthUriU3Ek__BackingField_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CAuthUriU3Ek__BackingField_0() { return &___U3CAuthUriU3Ek__BackingField_0; }
	inline void set_U3CAuthUriU3Ek__BackingField_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CAuthUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthUriU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUserNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___U3CUserNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CUserNameU3Ek__BackingField_1() const { return ___U3CUserNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUserNameU3Ek__BackingField_1() { return &___U3CUserNameU3Ek__BackingField_1; }
	inline void set_U3CUserNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CUserNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___U3CPasswordU3Ek__BackingField_2)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_2() const { return ___U3CPasswordU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_2() { return &___U3CPasswordU3Ek__BackingField_2; }
	inline void set_U3CPasswordU3Ek__BackingField_2(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPasswordU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CUserRolesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___U3CUserRolesU3Ek__BackingField_3)); }
	inline String_t* get_U3CUserRolesU3Ek__BackingField_3() const { return ___U3CUserRolesU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CUserRolesU3Ek__BackingField_3() { return &___U3CUserRolesU3Ek__BackingField_3; }
	inline void set_U3CUserRolesU3Ek__BackingField_3(String_t* value)
	{
		___U3CUserRolesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserRolesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CIsPreAuthRequiredU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___U3CIsPreAuthRequiredU3Ek__BackingField_4)); }
	inline bool get_U3CIsPreAuthRequiredU3Ek__BackingField_4() const { return ___U3CIsPreAuthRequiredU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsPreAuthRequiredU3Ek__BackingField_4() { return &___U3CIsPreAuthRequiredU3Ek__BackingField_4; }
	inline void set_U3CIsPreAuthRequiredU3Ek__BackingField_4(bool value)
	{
		___U3CIsPreAuthRequiredU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_OnAuthenticationSucceded_5() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___OnAuthenticationSucceded_5)); }
	inline OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 * get_OnAuthenticationSucceded_5() const { return ___OnAuthenticationSucceded_5; }
	inline OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 ** get_address_of_OnAuthenticationSucceded_5() { return &___OnAuthenticationSucceded_5; }
	inline void set_OnAuthenticationSucceded_5(OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55 * value)
	{
		___OnAuthenticationSucceded_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationSucceded_5), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_6() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___OnAuthenticationFailed_6)); }
	inline OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 * get_OnAuthenticationFailed_6() const { return ___OnAuthenticationFailed_6; }
	inline OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 ** get_address_of_OnAuthenticationFailed_6() { return &___OnAuthenticationFailed_6; }
	inline void set_OnAuthenticationFailed_6(OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4 * value)
	{
		___OnAuthenticationFailed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationFailed_6), value);
	}

	inline static int32_t get_offset_of_AuthRequest_7() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___AuthRequest_7)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_AuthRequest_7() const { return ___AuthRequest_7; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_AuthRequest_7() { return &___AuthRequest_7; }
	inline void set_AuthRequest_7(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___AuthRequest_7 = value;
		Il2CppCodeGenWriteBarrier((&___AuthRequest_7), value);
	}

	inline static int32_t get_offset_of_Cookie_8() { return static_cast<int32_t>(offsetof(SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7, ___Cookie_8)); }
	inline Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE * get_Cookie_8() const { return ___Cookie_8; }
	inline Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE ** get_address_of_Cookie_8() { return &___Cookie_8; }
	inline void set_Cookie_8(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE * value)
	{
		___Cookie_8 = value;
		Il2CppCodeGenWriteBarrier((&___Cookie_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECOOKIEAUTHENTICATION_T468705BBD4859389B548F9111453669A1447A4E7_H
#ifndef U3CU3EC_T15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_H
#define U3CU3EC_T15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Authentication.SampleCookieAuthentication_<>c
struct  U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_StaticFields
{
public:
	// BestHTTP.SignalR.Authentication.SampleCookieAuthentication_<>c BestHTTP.SignalR.Authentication.SampleCookieAuthentication_<>c::<>9
	U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22 * ___U3CU3E9_0;
	// System.Predicate`1<BestHTTP.Cookies.Cookie> BestHTTP.SignalR.Authentication.SampleCookieAuthentication_<>c::<>9__31_0
	Predicate_1_t1D10832677445F77B97D6841EAB8D16D9F09E5FE * ___U3CU3E9__31_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Predicate_1_t1D10832677445F77B97D6841EAB8D16D9F09E5FE * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Predicate_1_t1D10832677445F77B97D6841EAB8D16D9F09E5FE ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Predicate_1_t1D10832677445F77B97D6841EAB8D16D9F09E5FE * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_H
#ifndef HUB_TC3897DD4D14A474DCB052292CC6F27EC56E8DC21_H
#define HUB_TC3897DD4D14A474DCB052292CC6F27EC56E8DC21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Hubs.Hub
struct  Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalR.Hubs.Hub::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> BestHTTP.SignalR.Hubs.Hub::state
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___state_1;
	// BestHTTP.SignalR.Hubs.OnMethodCallDelegate BestHTTP.SignalR.Hubs.Hub::OnMethodCall
	OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE * ___OnMethodCall_2;
	// System.Collections.Generic.Dictionary`2<System.UInt64,BestHTTP.SignalR.Messages.ClientMessage> BestHTTP.SignalR.Hubs.Hub::SentMessages
	Dictionary_2_t99E4DFB056C2898BBDF19B3069AE04E4460723CA * ___SentMessages_3;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SignalR.Hubs.OnMethodCallCallbackDelegate> BestHTTP.SignalR.Hubs.Hub::MethodTable
	Dictionary_2_t41EA3D7980400B516B8F72E7A81AE6350474DBD4 * ___MethodTable_4;
	// System.Text.StringBuilder BestHTTP.SignalR.Hubs.Hub::builder
	StringBuilder_t * ___builder_5;
	// BestHTTP.SignalR.Connection BestHTTP.SignalR.Hubs.Hub::<BestHTTP.SignalR.Hubs.IHub.Connection>k__BackingField
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1 * ___U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___state_1)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_state_1() const { return ___state_1; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___state_1 = value;
		Il2CppCodeGenWriteBarrier((&___state_1), value);
	}

	inline static int32_t get_offset_of_OnMethodCall_2() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___OnMethodCall_2)); }
	inline OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE * get_OnMethodCall_2() const { return ___OnMethodCall_2; }
	inline OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE ** get_address_of_OnMethodCall_2() { return &___OnMethodCall_2; }
	inline void set_OnMethodCall_2(OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE * value)
	{
		___OnMethodCall_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnMethodCall_2), value);
	}

	inline static int32_t get_offset_of_SentMessages_3() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___SentMessages_3)); }
	inline Dictionary_2_t99E4DFB056C2898BBDF19B3069AE04E4460723CA * get_SentMessages_3() const { return ___SentMessages_3; }
	inline Dictionary_2_t99E4DFB056C2898BBDF19B3069AE04E4460723CA ** get_address_of_SentMessages_3() { return &___SentMessages_3; }
	inline void set_SentMessages_3(Dictionary_2_t99E4DFB056C2898BBDF19B3069AE04E4460723CA * value)
	{
		___SentMessages_3 = value;
		Il2CppCodeGenWriteBarrier((&___SentMessages_3), value);
	}

	inline static int32_t get_offset_of_MethodTable_4() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___MethodTable_4)); }
	inline Dictionary_2_t41EA3D7980400B516B8F72E7A81AE6350474DBD4 * get_MethodTable_4() const { return ___MethodTable_4; }
	inline Dictionary_2_t41EA3D7980400B516B8F72E7A81AE6350474DBD4 ** get_address_of_MethodTable_4() { return &___MethodTable_4; }
	inline void set_MethodTable_4(Dictionary_2_t41EA3D7980400B516B8F72E7A81AE6350474DBD4 * value)
	{
		___MethodTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___MethodTable_4), value);
	}

	inline static int32_t get_offset_of_builder_5() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___builder_5)); }
	inline StringBuilder_t * get_builder_5() const { return ___builder_5; }
	inline StringBuilder_t ** get_address_of_builder_5() { return &___builder_5; }
	inline void set_builder_5(StringBuilder_t * value)
	{
		___builder_5 = value;
		Il2CppCodeGenWriteBarrier((&___builder_5), value);
	}

	inline static int32_t get_offset_of_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21, ___U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6)); }
	inline Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1 * get_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6() const { return ___U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6; }
	inline Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1 ** get_address_of_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6() { return &___U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6; }
	inline void set_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1 * value)
	{
		___U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUB_TC3897DD4D14A474DCB052292CC6F27EC56E8DC21_H
#ifndef DEFAULTJSONENCODER_T051D42DE1EADACBF41DDE748424CF3D2BEAFCE14_H
#define DEFAULTJSONENCODER_T051D42DE1EADACBF41DDE748424CF3D2BEAFCE14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.JsonEncoders.DefaultJsonEncoder
struct  DefaultJsonEncoder_t051D42DE1EADACBF41DDE748424CF3D2BEAFCE14  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTJSONENCODER_T051D42DE1EADACBF41DDE748424CF3D2BEAFCE14_H
#ifndef LITJSONENCODER_T27A8B7A59F72316A495C7107A97372EBF6112C43_H
#define LITJSONENCODER_T27A8B7A59F72316A495C7107A97372EBF6112C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.JsonEncoders.LitJsonEncoder
struct  LitJsonEncoder_t27A8B7A59F72316A495C7107A97372EBF6112C43  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITJSONENCODER_T27A8B7A59F72316A495C7107A97372EBF6112C43_H
#ifndef DATAMESSAGE_T392FE78D8913A8748A44B8CCD7755A578A4D7689_H
#define DATAMESSAGE_T392FE78D8913A8748A44B8CCD7755A578A4D7689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.DataMessage
struct  DataMessage_t392FE78D8913A8748A44B8CCD7755A578A4D7689  : public RuntimeObject
{
public:
	// System.Object BestHTTP.SignalR.Messages.DataMessage::<Data>k__BackingField
	RuntimeObject * ___U3CDataU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataMessage_t392FE78D8913A8748A44B8CCD7755A578A4D7689, ___U3CDataU3Ek__BackingField_0)); }
	inline RuntimeObject * get_U3CDataU3Ek__BackingField_0() const { return ___U3CDataU3Ek__BackingField_0; }
	inline RuntimeObject ** get_address_of_U3CDataU3Ek__BackingField_0() { return &___U3CDataU3Ek__BackingField_0; }
	inline void set_U3CDataU3Ek__BackingField_0(RuntimeObject * value)
	{
		___U3CDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMESSAGE_T392FE78D8913A8748A44B8CCD7755A578A4D7689_H
#ifndef FAILUREMESSAGE_T4E01BB179E3081607AF8F6DB62FA445048B61F9D_H
#define FAILUREMESSAGE_T4E01BB179E3081607AF8F6DB62FA445048B61F9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.FailureMessage
struct  FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D  : public RuntimeObject
{
public:
	// System.UInt64 BestHTTP.SignalR.Messages.FailureMessage::<InvocationId>k__BackingField
	uint64_t ___U3CInvocationIdU3Ek__BackingField_0;
	// System.Boolean BestHTTP.SignalR.Messages.FailureMessage::<IsHubError>k__BackingField
	bool ___U3CIsHubErrorU3Ek__BackingField_1;
	// System.String BestHTTP.SignalR.Messages.FailureMessage::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> BestHTTP.SignalR.Messages.FailureMessage::<AdditionalData>k__BackingField
	RuntimeObject* ___U3CAdditionalDataU3Ek__BackingField_3;
	// System.String BestHTTP.SignalR.Messages.FailureMessage::<StackTrace>k__BackingField
	String_t* ___U3CStackTraceU3Ek__BackingField_4;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> BestHTTP.SignalR.Messages.FailureMessage::<State>k__BackingField
	RuntimeObject* ___U3CStateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInvocationIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D, ___U3CInvocationIdU3Ek__BackingField_0)); }
	inline uint64_t get_U3CInvocationIdU3Ek__BackingField_0() const { return ___U3CInvocationIdU3Ek__BackingField_0; }
	inline uint64_t* get_address_of_U3CInvocationIdU3Ek__BackingField_0() { return &___U3CInvocationIdU3Ek__BackingField_0; }
	inline void set_U3CInvocationIdU3Ek__BackingField_0(uint64_t value)
	{
		___U3CInvocationIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsHubErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D, ___U3CIsHubErrorU3Ek__BackingField_1)); }
	inline bool get_U3CIsHubErrorU3Ek__BackingField_1() const { return ___U3CIsHubErrorU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsHubErrorU3Ek__BackingField_1() { return &___U3CIsHubErrorU3Ek__BackingField_1; }
	inline void set_U3CIsHubErrorU3Ek__BackingField_1(bool value)
	{
		___U3CIsHubErrorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D, ___U3CErrorMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_2() const { return ___U3CErrorMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_2() { return &___U3CErrorMessageU3Ek__BackingField_2; }
	inline void set_U3CErrorMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorMessageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CAdditionalDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D, ___U3CAdditionalDataU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CAdditionalDataU3Ek__BackingField_3() const { return ___U3CAdditionalDataU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CAdditionalDataU3Ek__BackingField_3() { return &___U3CAdditionalDataU3Ek__BackingField_3; }
	inline void set_U3CAdditionalDataU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CAdditionalDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdditionalDataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CStackTraceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D, ___U3CStackTraceU3Ek__BackingField_4)); }
	inline String_t* get_U3CStackTraceU3Ek__BackingField_4() const { return ___U3CStackTraceU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CStackTraceU3Ek__BackingField_4() { return &___U3CStackTraceU3Ek__BackingField_4; }
	inline void set_U3CStackTraceU3Ek__BackingField_4(String_t* value)
	{
		___U3CStackTraceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStackTraceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D, ___U3CStateU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CStateU3Ek__BackingField_5() const { return ___U3CStateU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CStateU3Ek__BackingField_5() { return &___U3CStateU3Ek__BackingField_5; }
	inline void set_U3CStateU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CStateU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAILUREMESSAGE_T4E01BB179E3081607AF8F6DB62FA445048B61F9D_H
#ifndef KEEPALIVEMESSAGE_T1A1F666B9D06F78ABD01F423864BC12BDDEF4748_H
#define KEEPALIVEMESSAGE_T1A1F666B9D06F78ABD01F423864BC12BDDEF4748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.KeepAliveMessage
struct  KeepAliveMessage_t1A1F666B9D06F78ABD01F423864BC12BDDEF4748  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEMESSAGE_T1A1F666B9D06F78ABD01F423864BC12BDDEF4748_H
#ifndef METHODCALLMESSAGE_T94D2AD55C50144E14FCA7F3141EB43F872C76E4A_H
#define METHODCALLMESSAGE_T94D2AD55C50144E14FCA7F3141EB43F872C76E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.MethodCallMessage
struct  MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalR.Messages.MethodCallMessage::<Hub>k__BackingField
	String_t* ___U3CHubU3Ek__BackingField_0;
	// System.String BestHTTP.SignalR.Messages.MethodCallMessage::<Method>k__BackingField
	String_t* ___U3CMethodU3Ek__BackingField_1;
	// System.Object[] BestHTTP.SignalR.Messages.MethodCallMessage::<Arguments>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CArgumentsU3Ek__BackingField_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> BestHTTP.SignalR.Messages.MethodCallMessage::<State>k__BackingField
	RuntimeObject* ___U3CStateU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CHubU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A, ___U3CHubU3Ek__BackingField_0)); }
	inline String_t* get_U3CHubU3Ek__BackingField_0() const { return ___U3CHubU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CHubU3Ek__BackingField_0() { return &___U3CHubU3Ek__BackingField_0; }
	inline void set_U3CHubU3Ek__BackingField_0(String_t* value)
	{
		___U3CHubU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHubU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A, ___U3CMethodU3Ek__BackingField_1)); }
	inline String_t* get_U3CMethodU3Ek__BackingField_1() const { return ___U3CMethodU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMethodU3Ek__BackingField_1() { return &___U3CMethodU3Ek__BackingField_1; }
	inline void set_U3CMethodU3Ek__BackingField_1(String_t* value)
	{
		___U3CMethodU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A, ___U3CArgumentsU3Ek__BackingField_2)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CArgumentsU3Ek__BackingField_2() const { return ___U3CArgumentsU3Ek__BackingField_2; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CArgumentsU3Ek__BackingField_2() { return &___U3CArgumentsU3Ek__BackingField_2; }
	inline void set_U3CArgumentsU3Ek__BackingField_2(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CArgumentsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A, ___U3CStateU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CStateU3Ek__BackingField_3() const { return ___U3CStateU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CStateU3Ek__BackingField_3() { return &___U3CStateU3Ek__BackingField_3; }
	inline void set_U3CStateU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CStateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODCALLMESSAGE_T94D2AD55C50144E14FCA7F3141EB43F872C76E4A_H
#ifndef PROGRESSMESSAGE_T6ACA32FD2830024A394E40F050268250F675C72F_H
#define PROGRESSMESSAGE_T6ACA32FD2830024A394E40F050268250F675C72F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.ProgressMessage
struct  ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F  : public RuntimeObject
{
public:
	// System.UInt64 BestHTTP.SignalR.Messages.ProgressMessage::<InvocationId>k__BackingField
	uint64_t ___U3CInvocationIdU3Ek__BackingField_0;
	// System.Double BestHTTP.SignalR.Messages.ProgressMessage::<Progress>k__BackingField
	double ___U3CProgressU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CInvocationIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F, ___U3CInvocationIdU3Ek__BackingField_0)); }
	inline uint64_t get_U3CInvocationIdU3Ek__BackingField_0() const { return ___U3CInvocationIdU3Ek__BackingField_0; }
	inline uint64_t* get_address_of_U3CInvocationIdU3Ek__BackingField_0() { return &___U3CInvocationIdU3Ek__BackingField_0; }
	inline void set_U3CInvocationIdU3Ek__BackingField_0(uint64_t value)
	{
		___U3CInvocationIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CProgressU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F, ___U3CProgressU3Ek__BackingField_1)); }
	inline double get_U3CProgressU3Ek__BackingField_1() const { return ___U3CProgressU3Ek__BackingField_1; }
	inline double* get_address_of_U3CProgressU3Ek__BackingField_1() { return &___U3CProgressU3Ek__BackingField_1; }
	inline void set_U3CProgressU3Ek__BackingField_1(double value)
	{
		___U3CProgressU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSMESSAGE_T6ACA32FD2830024A394E40F050268250F675C72F_H
#ifndef RESULTMESSAGE_T67714DA10EE2949CB7248F7899D60E61FA96E0BA_H
#define RESULTMESSAGE_T67714DA10EE2949CB7248F7899D60E61FA96E0BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.ResultMessage
struct  ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA  : public RuntimeObject
{
public:
	// System.UInt64 BestHTTP.SignalR.Messages.ResultMessage::<InvocationId>k__BackingField
	uint64_t ___U3CInvocationIdU3Ek__BackingField_0;
	// System.Object BestHTTP.SignalR.Messages.ResultMessage::<ReturnValue>k__BackingField
	RuntimeObject * ___U3CReturnValueU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> BestHTTP.SignalR.Messages.ResultMessage::<State>k__BackingField
	RuntimeObject* ___U3CStateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CInvocationIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA, ___U3CInvocationIdU3Ek__BackingField_0)); }
	inline uint64_t get_U3CInvocationIdU3Ek__BackingField_0() const { return ___U3CInvocationIdU3Ek__BackingField_0; }
	inline uint64_t* get_address_of_U3CInvocationIdU3Ek__BackingField_0() { return &___U3CInvocationIdU3Ek__BackingField_0; }
	inline void set_U3CInvocationIdU3Ek__BackingField_0(uint64_t value)
	{
		___U3CInvocationIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReturnValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA, ___U3CReturnValueU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CReturnValueU3Ek__BackingField_1() const { return ___U3CReturnValueU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CReturnValueU3Ek__BackingField_1() { return &___U3CReturnValueU3Ek__BackingField_1; }
	inline void set_U3CReturnValueU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CReturnValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReturnValueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA, ___U3CStateU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTMESSAGE_T67714DA10EE2949CB7248F7899D60E61FA96E0BA_H
#ifndef U3CU3EC_T94C8C81AD3F616BE0265443099BB5F60CD36D55E_H
#define U3CU3EC_T94C8C81AD3F616BE0265443099BB5F60CD36D55E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.ServerSentEventsTransport_<>c
struct  U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E_StaticFields
{
public:
	// BestHTTP.SignalR.Transports.ServerSentEventsTransport_<>c BestHTTP.SignalR.Transports.ServerSentEventsTransport_<>c::<>9
	U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E * ___U3CU3E9_0;
	// BestHTTP.ServerSentEvents.OnRetryDelegate BestHTTP.SignalR.Transports.ServerSentEventsTransport_<>c::<>9__6_0
	OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E_StaticFields, ___U3CU3E9__6_0_1)); }
	inline OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(OnRetryDelegate_tE6F4EF61343A96A4CA48BDCB5943C8F989F91F46 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T94C8C81AD3F616BE0265443099BB5F60CD36D55E_H
#ifndef DEFAULTACCESSTOKENAUTHENTICATOR_T4ADA7B68C430939C0097FB84C203DA9BE5E45C92_H
#define DEFAULTACCESSTOKENAUTHENTICATOR_T4ADA7B68C430939C0097FB84C203DA9BE5E45C92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Authentication.DefaultAccessTokenAuthenticator
struct  DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate BestHTTP.SignalRCore.Authentication.DefaultAccessTokenAuthenticator::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * ___OnAuthenticationSucceded_0;
	// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate BestHTTP.SignalRCore.Authentication.DefaultAccessTokenAuthenticator::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * ___OnAuthenticationFailed_1;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.SignalRCore.Authentication.DefaultAccessTokenAuthenticator::_connection
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ____connection_2;

public:
	inline static int32_t get_offset_of_OnAuthenticationSucceded_0() { return static_cast<int32_t>(offsetof(DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92, ___OnAuthenticationSucceded_0)); }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * get_OnAuthenticationSucceded_0() const { return ___OnAuthenticationSucceded_0; }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 ** get_address_of_OnAuthenticationSucceded_0() { return &___OnAuthenticationSucceded_0; }
	inline void set_OnAuthenticationSucceded_0(OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * value)
	{
		___OnAuthenticationSucceded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationSucceded_0), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_1() { return static_cast<int32_t>(offsetof(DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92, ___OnAuthenticationFailed_1)); }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * get_OnAuthenticationFailed_1() const { return ___OnAuthenticationFailed_1; }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 ** get_address_of_OnAuthenticationFailed_1() { return &___OnAuthenticationFailed_1; }
	inline void set_OnAuthenticationFailed_1(OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * value)
	{
		___OnAuthenticationFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationFailed_1), value);
	}

	inline static int32_t get_offset_of__connection_2() { return static_cast<int32_t>(offsetof(DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92, ____connection_2)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get__connection_2() const { return ____connection_2; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of__connection_2() { return &____connection_2; }
	inline void set__connection_2(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		____connection_2 = value;
		Il2CppCodeGenWriteBarrier((&____connection_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTACCESSTOKENAUTHENTICATOR_T4ADA7B68C430939C0097FB84C203DA9BE5E45C92_H
#ifndef HEADERAUTHENTICATOR_TC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1_H
#define HEADERAUTHENTICATOR_TC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Authentication.HeaderAuthenticator
struct  HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate BestHTTP.SignalRCore.Authentication.HeaderAuthenticator::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * ___OnAuthenticationSucceded_0;
	// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate BestHTTP.SignalRCore.Authentication.HeaderAuthenticator::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * ___OnAuthenticationFailed_1;
	// System.String BestHTTP.SignalRCore.Authentication.HeaderAuthenticator::_credentials
	String_t* ____credentials_2;

public:
	inline static int32_t get_offset_of_OnAuthenticationSucceded_0() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1, ___OnAuthenticationSucceded_0)); }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * get_OnAuthenticationSucceded_0() const { return ___OnAuthenticationSucceded_0; }
	inline OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 ** get_address_of_OnAuthenticationSucceded_0() { return &___OnAuthenticationSucceded_0; }
	inline void set_OnAuthenticationSucceded_0(OnAuthenticationSuccededDelegate_tEA42FDE767C73D79D089586F7D35643CD23D5333 * value)
	{
		___OnAuthenticationSucceded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationSucceded_0), value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_1() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1, ___OnAuthenticationFailed_1)); }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * get_OnAuthenticationFailed_1() const { return ___OnAuthenticationFailed_1; }
	inline OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 ** get_address_of_OnAuthenticationFailed_1() { return &___OnAuthenticationFailed_1; }
	inline void set_OnAuthenticationFailed_1(OnAuthenticationFailedDelegate_t6B861C5E3BFE161A494E605848645655F51C9BF8 * value)
	{
		___OnAuthenticationFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAuthenticationFailed_1), value);
	}

	inline static int32_t get_offset_of__credentials_2() { return static_cast<int32_t>(offsetof(HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1, ____credentials_2)); }
	inline String_t* get__credentials_2() const { return ____credentials_2; }
	inline String_t** get_address_of__credentials_2() { return &____credentials_2; }
	inline void set__credentials_2(String_t* value)
	{
		____credentials_2 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERAUTHENTICATOR_TC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1_H
#ifndef LITJSONENCODER_TDF89F668CD476C780B9A83B4E17045037E6B2E9B_H
#define LITJSONENCODER_TDF89F668CD476C780B9A83B4E17045037E6B2E9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Encoders.LitJsonEncoder
struct  LitJsonEncoder_tDF89F668CD476C780B9A83B4E17045037E6B2E9B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITJSONENCODER_TDF89F668CD476C780B9A83B4E17045037E6B2E9B_H
#ifndef U3CU3EC_T1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_H
#define U3CU3EC_T1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c
struct  U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields
{
public:
	// BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF * ___U3CU3E9_0;
	// LitJson.ImporterFunc`2<System.Int32,System.Int64> BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9__2_0
	ImporterFunc_2_tDAEEA219F637C6A0AFC2CA3A8FCB8C12B554EB2E * ___U3CU3E9__2_0_1;
	// LitJson.ImporterFunc`2<System.Int64,System.Int32> BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9__2_1
	ImporterFunc_2_tAEB2964AD99C07458D1E12FC135FC6BB162BD571 * ___U3CU3E9__2_1_2;
	// LitJson.ImporterFunc`2<System.Double,System.Int32> BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9__2_2
	ImporterFunc_2_tB95D3E8C45F2F182CCF09C5626F72F4226F1BBB5 * ___U3CU3E9__2_2_3;
	// LitJson.ImporterFunc`2<System.String,System.DateTime> BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9__2_3
	ImporterFunc_2_tD380E76366033F6C81F25607F37693016040A394 * ___U3CU3E9__2_3_4;
	// LitJson.ImporterFunc`2<System.Double,System.Single> BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9__2_4
	ImporterFunc_2_tFDCD0D067BEADC4D8348CEC0F6848C54B425FF93 * ___U3CU3E9__2_4_5;
	// LitJson.ImporterFunc`2<System.String,System.Byte[]> BestHTTP.SignalRCore.Encoders.LitJsonEncoder_<>c::<>9__2_5
	ImporterFunc_2_tBBF14463CDBCB9A87A528512FC3B631590274B11 * ___U3CU3E9__2_5_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9__2_0_1)); }
	inline ImporterFunc_2_tDAEEA219F637C6A0AFC2CA3A8FCB8C12B554EB2E * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline ImporterFunc_2_tDAEEA219F637C6A0AFC2CA3A8FCB8C12B554EB2E ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(ImporterFunc_2_tDAEEA219F637C6A0AFC2CA3A8FCB8C12B554EB2E * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9__2_1_2)); }
	inline ImporterFunc_2_tAEB2964AD99C07458D1E12FC135FC6BB162BD571 * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline ImporterFunc_2_tAEB2964AD99C07458D1E12FC135FC6BB162BD571 ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(ImporterFunc_2_tAEB2964AD99C07458D1E12FC135FC6BB162BD571 * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9__2_2_3)); }
	inline ImporterFunc_2_tB95D3E8C45F2F182CCF09C5626F72F4226F1BBB5 * get_U3CU3E9__2_2_3() const { return ___U3CU3E9__2_2_3; }
	inline ImporterFunc_2_tB95D3E8C45F2F182CCF09C5626F72F4226F1BBB5 ** get_address_of_U3CU3E9__2_2_3() { return &___U3CU3E9__2_2_3; }
	inline void set_U3CU3E9__2_2_3(ImporterFunc_2_tB95D3E8C45F2F182CCF09C5626F72F4226F1BBB5 * value)
	{
		___U3CU3E9__2_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9__2_3_4)); }
	inline ImporterFunc_2_tD380E76366033F6C81F25607F37693016040A394 * get_U3CU3E9__2_3_4() const { return ___U3CU3E9__2_3_4; }
	inline ImporterFunc_2_tD380E76366033F6C81F25607F37693016040A394 ** get_address_of_U3CU3E9__2_3_4() { return &___U3CU3E9__2_3_4; }
	inline void set_U3CU3E9__2_3_4(ImporterFunc_2_tD380E76366033F6C81F25607F37693016040A394 * value)
	{
		___U3CU3E9__2_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9__2_4_5)); }
	inline ImporterFunc_2_tFDCD0D067BEADC4D8348CEC0F6848C54B425FF93 * get_U3CU3E9__2_4_5() const { return ___U3CU3E9__2_4_5; }
	inline ImporterFunc_2_tFDCD0D067BEADC4D8348CEC0F6848C54B425FF93 ** get_address_of_U3CU3E9__2_4_5() { return &___U3CU3E9__2_4_5; }
	inline void set_U3CU3E9__2_4_5(ImporterFunc_2_tFDCD0D067BEADC4D8348CEC0F6848C54B425FF93 * value)
	{
		___U3CU3E9__2_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields, ___U3CU3E9__2_5_6)); }
	inline ImporterFunc_2_tBBF14463CDBCB9A87A528512FC3B631590274B11 * get_U3CU3E9__2_5_6() const { return ___U3CU3E9__2_5_6; }
	inline ImporterFunc_2_tBBF14463CDBCB9A87A528512FC3B631590274B11 ** get_address_of_U3CU3E9__2_5_6() { return &___U3CU3E9__2_5_6; }
	inline void set_U3CU3E9__2_5_6(ImporterFunc_2_tBBF14463CDBCB9A87A528512FC3B631590274B11 * value)
	{
		___U3CU3E9__2_5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_5_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_H
#ifndef MESSAGEPACKENCODER_T9ABA3A82D126343FDBC41418BE980839B01646CB_H
#define MESSAGEPACKENCODER_T9ABA3A82D126343FDBC41418BE980839B01646CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Encoders.MessagePackEncoder
struct  MessagePackEncoder_t9ABA3A82D126343FDBC41418BE980839B01646CB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEPACKENCODER_T9ABA3A82D126343FDBC41418BE980839B01646CB_H
#ifndef MESSAGEPACKPROTOCOL_TFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C_H
#define MESSAGEPACKPROTOCOL_TFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Encoders.MessagePackProtocol
struct  MessagePackProtocol_tFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.IEncoder BestHTTP.SignalRCore.Encoders.MessagePackProtocol::<Encoder>k__BackingField
	RuntimeObject* ___U3CEncoderU3Ek__BackingField_0;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.SignalRCore.Encoders.MessagePackProtocol::<Connection>k__BackingField
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___U3CConnectionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CEncoderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MessagePackProtocol_tFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C, ___U3CEncoderU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CEncoderU3Ek__BackingField_0() const { return ___U3CEncoderU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CEncoderU3Ek__BackingField_0() { return &___U3CEncoderU3Ek__BackingField_0; }
	inline void set_U3CEncoderU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CEncoderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEncoderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessagePackProtocol_tFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C, ___U3CConnectionU3Ek__BackingField_1)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_U3CConnectionU3Ek__BackingField_1() const { return ___U3CConnectionU3Ek__BackingField_1; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_U3CConnectionU3Ek__BackingField_1() { return &___U3CConnectionU3Ek__BackingField_1; }
	inline void set_U3CConnectionU3Ek__BackingField_1(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___U3CConnectionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEPACKPROTOCOL_TFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C_H
#ifndef NEGOTIATIONRESULT_TB96B0D56165F097275D02A21F454E537B48E505B_H
#define NEGOTIATIONRESULT_TB96B0D56165F097275D02A21F454E537B48E505B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.NegotiationResult
struct  NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SignalRCore.Messages.NegotiationResult::<NegotiateVersion>k__BackingField
	int32_t ___U3CNegotiateVersionU3Ek__BackingField_0;
	// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::<ConnectionToken>k__BackingField
	String_t* ___U3CConnectionTokenU3Ek__BackingField_1;
	// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::<ConnectionId>k__BackingField
	String_t* ___U3CConnectionIdU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.SupportedTransport> BestHTTP.SignalRCore.Messages.NegotiationResult::<SupportedTransports>k__BackingField
	List_1_t21B4829E916812BBC60EB794AB541A5113820BCB * ___U3CSupportedTransportsU3Ek__BackingField_3;
	// System.Uri BestHTTP.SignalRCore.Messages.NegotiationResult::<Url>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUrlU3Ek__BackingField_4;
	// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::<AccessToken>k__BackingField
	String_t* ___U3CAccessTokenU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNegotiateVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B, ___U3CNegotiateVersionU3Ek__BackingField_0)); }
	inline int32_t get_U3CNegotiateVersionU3Ek__BackingField_0() const { return ___U3CNegotiateVersionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNegotiateVersionU3Ek__BackingField_0() { return &___U3CNegotiateVersionU3Ek__BackingField_0; }
	inline void set_U3CNegotiateVersionU3Ek__BackingField_0(int32_t value)
	{
		___U3CNegotiateVersionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CConnectionTokenU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B, ___U3CConnectionTokenU3Ek__BackingField_1)); }
	inline String_t* get_U3CConnectionTokenU3Ek__BackingField_1() const { return ___U3CConnectionTokenU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CConnectionTokenU3Ek__BackingField_1() { return &___U3CConnectionTokenU3Ek__BackingField_1; }
	inline void set_U3CConnectionTokenU3Ek__BackingField_1(String_t* value)
	{
		___U3CConnectionTokenU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionTokenU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CConnectionIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B, ___U3CConnectionIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CConnectionIdU3Ek__BackingField_2() const { return ___U3CConnectionIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CConnectionIdU3Ek__BackingField_2() { return &___U3CConnectionIdU3Ek__BackingField_2; }
	inline void set_U3CConnectionIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CConnectionIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CSupportedTransportsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B, ___U3CSupportedTransportsU3Ek__BackingField_3)); }
	inline List_1_t21B4829E916812BBC60EB794AB541A5113820BCB * get_U3CSupportedTransportsU3Ek__BackingField_3() const { return ___U3CSupportedTransportsU3Ek__BackingField_3; }
	inline List_1_t21B4829E916812BBC60EB794AB541A5113820BCB ** get_address_of_U3CSupportedTransportsU3Ek__BackingField_3() { return &___U3CSupportedTransportsU3Ek__BackingField_3; }
	inline void set_U3CSupportedTransportsU3Ek__BackingField_3(List_1_t21B4829E916812BBC60EB794AB541A5113820BCB * value)
	{
		___U3CSupportedTransportsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSupportedTransportsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B, ___U3CUrlU3Ek__BackingField_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUrlU3Ek__BackingField_4() const { return ___U3CUrlU3Ek__BackingField_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUrlU3Ek__BackingField_4() { return &___U3CUrlU3Ek__BackingField_4; }
	inline void set_U3CUrlU3Ek__BackingField_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUrlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B, ___U3CAccessTokenU3Ek__BackingField_5)); }
	inline String_t* get_U3CAccessTokenU3Ek__BackingField_5() const { return ___U3CAccessTokenU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CAccessTokenU3Ek__BackingField_5() { return &___U3CAccessTokenU3Ek__BackingField_5; }
	inline void set_U3CAccessTokenU3Ek__BackingField_5(String_t* value)
	{
		___U3CAccessTokenU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGOTIATIONRESULT_TB96B0D56165F097275D02A21F454E537B48E505B_H
#ifndef SUPPORTEDTRANSPORT_TF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2_H
#define SUPPORTEDTRANSPORT_TF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.SupportedTransport
struct  SupportedTransport_tF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalRCore.Messages.SupportedTransport::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> BestHTTP.SignalRCore.Messages.SupportedTransport::<SupportedFormats>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CSupportedFormatsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SupportedTransport_tF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSupportedFormatsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SupportedTransport_tF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2, ___U3CSupportedFormatsU3Ek__BackingField_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CSupportedFormatsU3Ek__BackingField_1() const { return ___U3CSupportedFormatsU3Ek__BackingField_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CSupportedFormatsU3Ek__BackingField_1() { return &___U3CSupportedFormatsU3Ek__BackingField_1; }
	inline void set_U3CSupportedFormatsU3Ek__BackingField_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CSupportedFormatsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSupportedFormatsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTEDTRANSPORT_TF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CLIENTMESSAGE_TE29E1EEF2A060152F0CA8F004BE96FC936AE894A_H
#define CLIENTMESSAGE_TE29E1EEF2A060152F0CA8F004BE96FC936AE894A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.ClientMessage
struct  ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A 
{
public:
	// BestHTTP.SignalR.Hubs.Hub BestHTTP.SignalR.Messages.ClientMessage::Hub
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21 * ___Hub_0;
	// System.String BestHTTP.SignalR.Messages.ClientMessage::Method
	String_t* ___Method_1;
	// System.Object[] BestHTTP.SignalR.Messages.ClientMessage::Args
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Args_2;
	// System.UInt64 BestHTTP.SignalR.Messages.ClientMessage::CallIdx
	uint64_t ___CallIdx_3;
	// BestHTTP.SignalR.Hubs.OnMethodResultDelegate BestHTTP.SignalR.Messages.ClientMessage::ResultCallback
	OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F * ___ResultCallback_4;
	// BestHTTP.SignalR.Hubs.OnMethodFailedDelegate BestHTTP.SignalR.Messages.ClientMessage::ResultErrorCallback
	OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B * ___ResultErrorCallback_5;
	// BestHTTP.SignalR.Hubs.OnMethodProgressDelegate BestHTTP.SignalR.Messages.ClientMessage::ProgressCallback
	OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4 * ___ProgressCallback_6;

public:
	inline static int32_t get_offset_of_Hub_0() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___Hub_0)); }
	inline Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21 * get_Hub_0() const { return ___Hub_0; }
	inline Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21 ** get_address_of_Hub_0() { return &___Hub_0; }
	inline void set_Hub_0(Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21 * value)
	{
		___Hub_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hub_0), value);
	}

	inline static int32_t get_offset_of_Method_1() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___Method_1)); }
	inline String_t* get_Method_1() const { return ___Method_1; }
	inline String_t** get_address_of_Method_1() { return &___Method_1; }
	inline void set_Method_1(String_t* value)
	{
		___Method_1 = value;
		Il2CppCodeGenWriteBarrier((&___Method_1), value);
	}

	inline static int32_t get_offset_of_Args_2() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___Args_2)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_Args_2() const { return ___Args_2; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_Args_2() { return &___Args_2; }
	inline void set_Args_2(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___Args_2 = value;
		Il2CppCodeGenWriteBarrier((&___Args_2), value);
	}

	inline static int32_t get_offset_of_CallIdx_3() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___CallIdx_3)); }
	inline uint64_t get_CallIdx_3() const { return ___CallIdx_3; }
	inline uint64_t* get_address_of_CallIdx_3() { return &___CallIdx_3; }
	inline void set_CallIdx_3(uint64_t value)
	{
		___CallIdx_3 = value;
	}

	inline static int32_t get_offset_of_ResultCallback_4() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___ResultCallback_4)); }
	inline OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F * get_ResultCallback_4() const { return ___ResultCallback_4; }
	inline OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F ** get_address_of_ResultCallback_4() { return &___ResultCallback_4; }
	inline void set_ResultCallback_4(OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F * value)
	{
		___ResultCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___ResultCallback_4), value);
	}

	inline static int32_t get_offset_of_ResultErrorCallback_5() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___ResultErrorCallback_5)); }
	inline OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B * get_ResultErrorCallback_5() const { return ___ResultErrorCallback_5; }
	inline OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B ** get_address_of_ResultErrorCallback_5() { return &___ResultErrorCallback_5; }
	inline void set_ResultErrorCallback_5(OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B * value)
	{
		___ResultErrorCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___ResultErrorCallback_5), value);
	}

	inline static int32_t get_offset_of_ProgressCallback_6() { return static_cast<int32_t>(offsetof(ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A, ___ProgressCallback_6)); }
	inline OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4 * get_ProgressCallback_6() const { return ___ProgressCallback_6; }
	inline OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4 ** get_address_of_ProgressCallback_6() { return &___ProgressCallback_6; }
	inline void set_ProgressCallback_6(OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4 * value)
	{
		___ProgressCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressCallback_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalR.Messages.ClientMessage
struct ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A_marshaled_pinvoke
{
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21 * ___Hub_0;
	char* ___Method_1;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Args_2;
	uint64_t ___CallIdx_3;
	Il2CppMethodPointer ___ResultCallback_4;
	Il2CppMethodPointer ___ResultErrorCallback_5;
	Il2CppMethodPointer ___ProgressCallback_6;
};
// Native definition for COM marshalling of BestHTTP.SignalR.Messages.ClientMessage
struct ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A_marshaled_com
{
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21 * ___Hub_0;
	Il2CppChar* ___Method_1;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Args_2;
	uint64_t ___CallIdx_3;
	Il2CppMethodPointer ___ResultCallback_4;
	Il2CppMethodPointer ___ResultErrorCallback_5;
	Il2CppMethodPointer ___ProgressCallback_6;
};
#endif // CLIENTMESSAGE_TE29E1EEF2A060152F0CA8F004BE96FC936AE894A_H
#ifndef CANCELINVOCATIONMESSAGE_TBC65C628A457DA1D0286F514A35E9876137BF776_H
#define CANCELINVOCATIONMESSAGE_TBC65C628A457DA1D0286F514A35E9876137BF776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.CancelInvocationMessage
struct  CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776 
{
public:
	// System.String BestHTTP.SignalRCore.Messages.CancelInvocationMessage::invocationId
	String_t* ___invocationId_0;

public:
	inline static int32_t get_offset_of_invocationId_0() { return static_cast<int32_t>(offsetof(CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776, ___invocationId_0)); }
	inline String_t* get_invocationId_0() const { return ___invocationId_0; }
	inline String_t** get_address_of_invocationId_0() { return &___invocationId_0; }
	inline void set_invocationId_0(String_t* value)
	{
		___invocationId_0 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.CancelInvocationMessage
struct CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776_marshaled_pinvoke
{
	char* ___invocationId_0;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.CancelInvocationMessage
struct CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776_marshaled_com
{
	Il2CppChar* ___invocationId_0;
};
#endif // CANCELINVOCATIONMESSAGE_TBC65C628A457DA1D0286F514A35E9876137BF776_H
#ifndef PINGMESSAGE_T3E4FEDA904F050FABD4A5AE5186BF4A655889B09_H
#define PINGMESSAGE_T3E4FEDA904F050FABD4A5AE5186BF4A655889B09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.PingMessage
struct  PingMessage_t3E4FEDA904F050FABD4A5AE5186BF4A655889B09 
{
public:
	union
	{
		struct
		{
		};
		uint8_t PingMessage_t3E4FEDA904F050FABD4A5AE5186BF4A655889B09__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGMESSAGE_T3E4FEDA904F050FABD4A5AE5186BF4A655889B09_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#define SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.SupportedProtocols
struct  SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2 
{
public:
	// System.Int32 BestHTTP.Connections.SupportedProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#ifndef UPLOADSTREAM_T0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2_H
#define UPLOADSTREAM_T0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadStream
struct  UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.MemoryStream BestHTTP.Examples.UploadStream::ReadBuffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___ReadBuffer_5;
	// System.IO.MemoryStream BestHTTP.Examples.UploadStream::WriteBuffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___WriteBuffer_6;
	// System.Boolean BestHTTP.Examples.UploadStream::noMoreData
	bool ___noMoreData_7;
	// System.Threading.AutoResetEvent BestHTTP.Examples.UploadStream::ARE
	AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * ___ARE_8;
	// System.Object BestHTTP.Examples.UploadStream::locker
	RuntimeObject * ___locker_9;
	// System.String BestHTTP.Examples.UploadStream::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_ReadBuffer_5() { return static_cast<int32_t>(offsetof(UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2, ___ReadBuffer_5)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_ReadBuffer_5() const { return ___ReadBuffer_5; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_ReadBuffer_5() { return &___ReadBuffer_5; }
	inline void set_ReadBuffer_5(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___ReadBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___ReadBuffer_5), value);
	}

	inline static int32_t get_offset_of_WriteBuffer_6() { return static_cast<int32_t>(offsetof(UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2, ___WriteBuffer_6)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_WriteBuffer_6() const { return ___WriteBuffer_6; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_WriteBuffer_6() { return &___WriteBuffer_6; }
	inline void set_WriteBuffer_6(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___WriteBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___WriteBuffer_6), value);
	}

	inline static int32_t get_offset_of_noMoreData_7() { return static_cast<int32_t>(offsetof(UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2, ___noMoreData_7)); }
	inline bool get_noMoreData_7() const { return ___noMoreData_7; }
	inline bool* get_address_of_noMoreData_7() { return &___noMoreData_7; }
	inline void set_noMoreData_7(bool value)
	{
		___noMoreData_7 = value;
	}

	inline static int32_t get_offset_of_ARE_8() { return static_cast<int32_t>(offsetof(UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2, ___ARE_8)); }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * get_ARE_8() const { return ___ARE_8; }
	inline AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 ** get_address_of_ARE_8() { return &___ARE_8; }
	inline void set_ARE_8(AutoResetEvent_t2A1182CEEE4E184587D4DEAA4F382B810B21D3B7 * value)
	{
		___ARE_8 = value;
		Il2CppCodeGenWriteBarrier((&___ARE_8), value);
	}

	inline static int32_t get_offset_of_locker_9() { return static_cast<int32_t>(offsetof(UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2, ___locker_9)); }
	inline RuntimeObject * get_locker_9() const { return ___locker_9; }
	inline RuntimeObject ** get_address_of_locker_9() { return &___locker_9; }
	inline void set_locker_9(RuntimeObject * value)
	{
		___locker_9 = value;
		Il2CppCodeGenWriteBarrier((&___locker_9), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2, ___U3CNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CNameU3Ek__BackingField_10() const { return ___U3CNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_10() { return &___U3CNameU3Ek__BackingField_10; }
	inline void set_U3CNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSTREAM_T0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2_H
#ifndef CONNECTIONSTATES_T7FF3637CF19735B70869B226892AC58373AB82AD_H
#define CONNECTIONSTATES_T7FF3637CF19735B70869B226892AC58373AB82AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.ConnectionStates
struct  ConnectionStates_t7FF3637CF19735B70869B226892AC58373AB82AD 
{
public:
	// System.Int32 BestHTTP.SignalR.ConnectionStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionStates_t7FF3637CF19735B70869B226892AC58373AB82AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATES_T7FF3637CF19735B70869B226892AC58373AB82AD_H
#ifndef MESSAGETYPES_TB6017D8CD9729577E871D724D2CCA41A8E12F8EC_H
#define MESSAGETYPES_TB6017D8CD9729577E871D724D2CCA41A8E12F8EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.MessageTypes
struct  MessageTypes_tB6017D8CD9729577E871D724D2CCA41A8E12F8EC 
{
public:
	// System.Int32 BestHTTP.SignalR.MessageTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageTypes_tB6017D8CD9729577E871D724D2CCA41A8E12F8EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPES_TB6017D8CD9729577E871D724D2CCA41A8E12F8EC_H
#ifndef PROTOCOLVERSIONS_TC5746CAEC41DE3223C725582BB0CF80B9B516A68_H
#define PROTOCOLVERSIONS_TC5746CAEC41DE3223C725582BB0CF80B9B516A68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.ProtocolVersions
struct  ProtocolVersions_tC5746CAEC41DE3223C725582BB0CF80B9B516A68 
{
public:
	// System.Byte BestHTTP.SignalR.ProtocolVersions::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProtocolVersions_tC5746CAEC41DE3223C725582BB0CF80B9B516A68, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVERSIONS_TC5746CAEC41DE3223C725582BB0CF80B9B516A68_H
#ifndef REQUESTTYPES_T28AE22DC99ACA21791F75F66DF18C41A2BD2842A_H
#define REQUESTTYPES_T28AE22DC99ACA21791F75F66DF18C41A2BD2842A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.RequestTypes
struct  RequestTypes_t28AE22DC99ACA21791F75F66DF18C41A2BD2842A 
{
public:
	// System.Int32 BestHTTP.SignalR.RequestTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestTypes_t28AE22DC99ACA21791F75F66DF18C41A2BD2842A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTTYPES_T28AE22DC99ACA21791F75F66DF18C41A2BD2842A_H
#ifndef TRANSPORTSTATES_T2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9_H
#define TRANSPORTSTATES_T2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.TransportStates
struct  TransportStates_t2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9 
{
public:
	// System.Int32 BestHTTP.SignalR.TransportStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportStates_t2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTSTATES_T2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9_H
#ifndef TRANSPORTTYPES_T7038C136C8533C07764165F2E136BA4986826108_H
#define TRANSPORTTYPES_T7038C136C8533C07764165F2E136BA4986826108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.TransportTypes
struct  TransportTypes_t7038C136C8533C07764165F2E136BA4986826108 
{
public:
	// System.Int32 BestHTTP.SignalR.TransportTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportTypes_t7038C136C8533C07764165F2E136BA4986826108, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTTYPES_T7038C136C8533C07764165F2E136BA4986826108_H
#ifndef MESSAGETYPES_T6BB69413A5A279185FA3DBB8F994391217907ADB_H
#define MESSAGETYPES_T6BB69413A5A279185FA3DBB8F994391217907ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.MessageTypes
struct  MessageTypes_t6BB69413A5A279185FA3DBB8F994391217907ADB 
{
public:
	// System.Int32 BestHTTP.SignalRCore.Messages.MessageTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MessageTypes_t6BB69413A5A279185FA3DBB8F994391217907ADB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPES_T6BB69413A5A279185FA3DBB8F994391217907ADB_H
#ifndef TRANSPORTSTATES_TC5546B62EF1DD9E9220FEC265E5D626E6856A06E_H
#define TRANSPORTSTATES_TC5546B62EF1DD9E9220FEC265E5D626E6856A06E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.TransportStates
struct  TransportStates_tC5546B62EF1DD9E9220FEC265E5D626E6856A06E 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransportStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportStates_tC5546B62EF1DD9E9220FEC265E5D626E6856A06E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTSTATES_TC5546B62EF1DD9E9220FEC265E5D626E6856A06E_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CONNECTION_TFFB451BA78ACF8596803F7D83F1CB331728B9BD1_H
#define CONNECTION_TFFB451BA78ACF8596803F7D83F1CB331728B9BD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Connection
struct  Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.SignalR.Connection::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_1;
	// BestHTTP.SignalR.ConnectionStates BestHTTP.SignalR.Connection::_state
	int32_t ____state_2;
	// BestHTTP.SignalR.NegotiationData BestHTTP.SignalR.Connection::<NegotiationResult>k__BackingField
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340 * ___U3CNegotiationResultU3Ek__BackingField_3;
	// BestHTTP.SignalR.Hubs.Hub[] BestHTTP.SignalR.Connection::<Hubs>k__BackingField
	HubU5BU5D_t2E3D30B254608EF91EC6A9E8713A544654E00E1B* ___U3CHubsU3Ek__BackingField_4;
	// BestHTTP.SignalR.Transports.TransportBase BestHTTP.SignalR.Connection::<Transport>k__BackingField
	TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4 * ___U3CTransportU3Ek__BackingField_5;
	// BestHTTP.SignalR.ProtocolVersions BestHTTP.SignalR.Connection::<Protocol>k__BackingField
	uint8_t ___U3CProtocolU3Ek__BackingField_6;
	// PlatformSupport.Collections.ObjectModel.ObservableDictionary`2<System.String,System.String> BestHTTP.SignalR.Connection::additionalQueryParams
	ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE * ___additionalQueryParams_7;
	// System.Boolean BestHTTP.SignalR.Connection::<QueryParamsOnlyForHandshake>k__BackingField
	bool ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8;
	// BestHTTP.SignalR.JsonEncoders.IJsonEncoder BestHTTP.SignalR.Connection::<JsonEncoder>k__BackingField
	RuntimeObject* ___U3CJsonEncoderU3Ek__BackingField_9;
	// BestHTTP.SignalR.Authentication.IAuthenticationProvider BestHTTP.SignalR.Connection::<AuthenticationProvider>k__BackingField
	RuntimeObject* ___U3CAuthenticationProviderU3Ek__BackingField_10;
	// System.TimeSpan BestHTTP.SignalR.Connection::<PingInterval>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CPingIntervalU3Ek__BackingField_11;
	// System.TimeSpan BestHTTP.SignalR.Connection::<ReconnectDelay>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CReconnectDelayU3Ek__BackingField_12;
	// BestHTTP.SignalR.OnConnectedDelegate BestHTTP.SignalR.Connection::OnConnected
	OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * ___OnConnected_13;
	// BestHTTP.SignalR.OnClosedDelegate BestHTTP.SignalR.Connection::OnClosed
	OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98 * ___OnClosed_14;
	// BestHTTP.SignalR.OnErrorDelegate BestHTTP.SignalR.Connection::OnError
	OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4 * ___OnError_15;
	// BestHTTP.SignalR.OnConnectedDelegate BestHTTP.SignalR.Connection::OnReconnecting
	OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * ___OnReconnecting_16;
	// BestHTTP.SignalR.OnConnectedDelegate BestHTTP.SignalR.Connection::OnReconnected
	OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * ___OnReconnected_17;
	// BestHTTP.SignalR.OnStateChanged BestHTTP.SignalR.Connection::OnStateChanged
	OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249 * ___OnStateChanged_18;
	// BestHTTP.SignalR.OnNonHubMessageDelegate BestHTTP.SignalR.Connection::OnNonHubMessage
	OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4 * ___OnNonHubMessage_19;
	// BestHTTP.SignalR.OnPrepareRequestDelegate BestHTTP.SignalR.Connection::<RequestPreparator>k__BackingField
	OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81 * ___U3CRequestPreparatorU3Ek__BackingField_20;
	// System.Int64 BestHTTP.SignalR.Connection::ClientMessageCounter
	int64_t ___ClientMessageCounter_21;
	// System.String[] BestHTTP.SignalR.Connection::ClientProtocols
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ClientProtocols_22;
	// System.Int64 BestHTTP.SignalR.Connection::RequestCounter
	int64_t ___RequestCounter_23;
	// BestHTTP.SignalR.Messages.MultiMessage BestHTTP.SignalR.Connection::LastReceivedMessage
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C * ___LastReceivedMessage_24;
	// System.String BestHTTP.SignalR.Connection::GroupsToken
	String_t* ___GroupsToken_25;
	// System.Collections.Generic.List`1<BestHTTP.SignalR.Messages.IServerMessage> BestHTTP.SignalR.Connection::BufferedMessages
	List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 * ___BufferedMessages_26;
	// System.DateTime BestHTTP.SignalR.Connection::LastMessageReceivedAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___LastMessageReceivedAt_27;
	// System.DateTime BestHTTP.SignalR.Connection::ReconnectStartedAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___ReconnectStartedAt_28;
	// System.DateTime BestHTTP.SignalR.Connection::ReconnectDelayStartedAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___ReconnectDelayStartedAt_29;
	// System.Boolean BestHTTP.SignalR.Connection::ReconnectStarted
	bool ___ReconnectStarted_30;
	// System.DateTime BestHTTP.SignalR.Connection::LastPingSentAt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___LastPingSentAt_31;
	// BestHTTP.HTTPRequest BestHTTP.SignalR.Connection::PingRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___PingRequest_32;
	// System.Nullable`1<System.DateTime> BestHTTP.SignalR.Connection::TransportConnectionStartedAt
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___TransportConnectionStartedAt_33;
	// System.Text.StringBuilder BestHTTP.SignalR.Connection::queryBuilder
	StringBuilder_t * ___queryBuilder_34;
	// System.String BestHTTP.SignalR.Connection::BuiltConnectionData
	String_t* ___BuiltConnectionData_35;
	// System.String BestHTTP.SignalR.Connection::BuiltQueryParams
	String_t* ___BuiltQueryParams_36;
	// BestHTTP.Connections.SupportedProtocols BestHTTP.SignalR.Connection::NextProtocolToTry
	int32_t ___NextProtocolToTry_37;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CUriU3Ek__BackingField_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_1() const { return ___U3CUriU3Ek__BackingField_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_1() { return &___U3CUriU3Ek__BackingField_1; }
	inline void set_U3CUriU3Ek__BackingField_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ____state_2)); }
	inline int32_t get__state_2() const { return ____state_2; }
	inline int32_t* get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(int32_t value)
	{
		____state_2 = value;
	}

	inline static int32_t get_offset_of_U3CNegotiationResultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CNegotiationResultU3Ek__BackingField_3)); }
	inline NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340 * get_U3CNegotiationResultU3Ek__BackingField_3() const { return ___U3CNegotiationResultU3Ek__BackingField_3; }
	inline NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340 ** get_address_of_U3CNegotiationResultU3Ek__BackingField_3() { return &___U3CNegotiationResultU3Ek__BackingField_3; }
	inline void set_U3CNegotiationResultU3Ek__BackingField_3(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340 * value)
	{
		___U3CNegotiationResultU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNegotiationResultU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CHubsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CHubsU3Ek__BackingField_4)); }
	inline HubU5BU5D_t2E3D30B254608EF91EC6A9E8713A544654E00E1B* get_U3CHubsU3Ek__BackingField_4() const { return ___U3CHubsU3Ek__BackingField_4; }
	inline HubU5BU5D_t2E3D30B254608EF91EC6A9E8713A544654E00E1B** get_address_of_U3CHubsU3Ek__BackingField_4() { return &___U3CHubsU3Ek__BackingField_4; }
	inline void set_U3CHubsU3Ek__BackingField_4(HubU5BU5D_t2E3D30B254608EF91EC6A9E8713A544654E00E1B* value)
	{
		___U3CHubsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHubsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CTransportU3Ek__BackingField_5)); }
	inline TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4 * get_U3CTransportU3Ek__BackingField_5() const { return ___U3CTransportU3Ek__BackingField_5; }
	inline TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4 ** get_address_of_U3CTransportU3Ek__BackingField_5() { return &___U3CTransportU3Ek__BackingField_5; }
	inline void set_U3CTransportU3Ek__BackingField_5(TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4 * value)
	{
		___U3CTransportU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTransportU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CProtocolU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CProtocolU3Ek__BackingField_6)); }
	inline uint8_t get_U3CProtocolU3Ek__BackingField_6() const { return ___U3CProtocolU3Ek__BackingField_6; }
	inline uint8_t* get_address_of_U3CProtocolU3Ek__BackingField_6() { return &___U3CProtocolU3Ek__BackingField_6; }
	inline void set_U3CProtocolU3Ek__BackingField_6(uint8_t value)
	{
		___U3CProtocolU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_additionalQueryParams_7() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___additionalQueryParams_7)); }
	inline ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE * get_additionalQueryParams_7() const { return ___additionalQueryParams_7; }
	inline ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE ** get_address_of_additionalQueryParams_7() { return &___additionalQueryParams_7; }
	inline void set_additionalQueryParams_7(ObservableDictionary_2_tFF3AFD868FFB4164205C3E2B86D94CE1E3570EEE * value)
	{
		___additionalQueryParams_7 = value;
		Il2CppCodeGenWriteBarrier((&___additionalQueryParams_7), value);
	}

	inline static int32_t get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8)); }
	inline bool get_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8() const { return ___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8() { return &___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8; }
	inline void set_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8(bool value)
	{
		___U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CJsonEncoderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CJsonEncoderU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CJsonEncoderU3Ek__BackingField_9() const { return ___U3CJsonEncoderU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CJsonEncoderU3Ek__BackingField_9() { return &___U3CJsonEncoderU3Ek__BackingField_9; }
	inline void set_U3CJsonEncoderU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CJsonEncoderU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJsonEncoderU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CAuthenticationProviderU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CAuthenticationProviderU3Ek__BackingField_10() const { return ___U3CAuthenticationProviderU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CAuthenticationProviderU3Ek__BackingField_10() { return &___U3CAuthenticationProviderU3Ek__BackingField_10; }
	inline void set_U3CAuthenticationProviderU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CAuthenticationProviderU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticationProviderU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CPingIntervalU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CPingIntervalU3Ek__BackingField_11)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CPingIntervalU3Ek__BackingField_11() const { return ___U3CPingIntervalU3Ek__BackingField_11; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CPingIntervalU3Ek__BackingField_11() { return &___U3CPingIntervalU3Ek__BackingField_11; }
	inline void set_U3CPingIntervalU3Ek__BackingField_11(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CPingIntervalU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectDelayU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CReconnectDelayU3Ek__BackingField_12)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CReconnectDelayU3Ek__BackingField_12() const { return ___U3CReconnectDelayU3Ek__BackingField_12; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CReconnectDelayU3Ek__BackingField_12() { return &___U3CReconnectDelayU3Ek__BackingField_12; }
	inline void set_U3CReconnectDelayU3Ek__BackingField_12(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CReconnectDelayU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_OnConnected_13() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnConnected_13)); }
	inline OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * get_OnConnected_13() const { return ___OnConnected_13; }
	inline OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C ** get_address_of_OnConnected_13() { return &___OnConnected_13; }
	inline void set_OnConnected_13(OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * value)
	{
		___OnConnected_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnConnected_13), value);
	}

	inline static int32_t get_offset_of_OnClosed_14() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnClosed_14)); }
	inline OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98 * get_OnClosed_14() const { return ___OnClosed_14; }
	inline OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98 ** get_address_of_OnClosed_14() { return &___OnClosed_14; }
	inline void set_OnClosed_14(OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98 * value)
	{
		___OnClosed_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_14), value);
	}

	inline static int32_t get_offset_of_OnError_15() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnError_15)); }
	inline OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4 * get_OnError_15() const { return ___OnError_15; }
	inline OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4 ** get_address_of_OnError_15() { return &___OnError_15; }
	inline void set_OnError_15(OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4 * value)
	{
		___OnError_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_15), value);
	}

	inline static int32_t get_offset_of_OnReconnecting_16() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnReconnecting_16)); }
	inline OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * get_OnReconnecting_16() const { return ___OnReconnecting_16; }
	inline OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C ** get_address_of_OnReconnecting_16() { return &___OnReconnecting_16; }
	inline void set_OnReconnecting_16(OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * value)
	{
		___OnReconnecting_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnReconnecting_16), value);
	}

	inline static int32_t get_offset_of_OnReconnected_17() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnReconnected_17)); }
	inline OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * get_OnReconnected_17() const { return ___OnReconnected_17; }
	inline OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C ** get_address_of_OnReconnected_17() { return &___OnReconnected_17; }
	inline void set_OnReconnected_17(OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C * value)
	{
		___OnReconnected_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnReconnected_17), value);
	}

	inline static int32_t get_offset_of_OnStateChanged_18() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnStateChanged_18)); }
	inline OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249 * get_OnStateChanged_18() const { return ___OnStateChanged_18; }
	inline OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249 ** get_address_of_OnStateChanged_18() { return &___OnStateChanged_18; }
	inline void set_OnStateChanged_18(OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249 * value)
	{
		___OnStateChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_18), value);
	}

	inline static int32_t get_offset_of_OnNonHubMessage_19() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___OnNonHubMessage_19)); }
	inline OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4 * get_OnNonHubMessage_19() const { return ___OnNonHubMessage_19; }
	inline OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4 ** get_address_of_OnNonHubMessage_19() { return &___OnNonHubMessage_19; }
	inline void set_OnNonHubMessage_19(OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4 * value)
	{
		___OnNonHubMessage_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnNonHubMessage_19), value);
	}

	inline static int32_t get_offset_of_U3CRequestPreparatorU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___U3CRequestPreparatorU3Ek__BackingField_20)); }
	inline OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81 * get_U3CRequestPreparatorU3Ek__BackingField_20() const { return ___U3CRequestPreparatorU3Ek__BackingField_20; }
	inline OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81 ** get_address_of_U3CRequestPreparatorU3Ek__BackingField_20() { return &___U3CRequestPreparatorU3Ek__BackingField_20; }
	inline void set_U3CRequestPreparatorU3Ek__BackingField_20(OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81 * value)
	{
		___U3CRequestPreparatorU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestPreparatorU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_ClientMessageCounter_21() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___ClientMessageCounter_21)); }
	inline int64_t get_ClientMessageCounter_21() const { return ___ClientMessageCounter_21; }
	inline int64_t* get_address_of_ClientMessageCounter_21() { return &___ClientMessageCounter_21; }
	inline void set_ClientMessageCounter_21(int64_t value)
	{
		___ClientMessageCounter_21 = value;
	}

	inline static int32_t get_offset_of_ClientProtocols_22() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___ClientProtocols_22)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ClientProtocols_22() const { return ___ClientProtocols_22; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ClientProtocols_22() { return &___ClientProtocols_22; }
	inline void set_ClientProtocols_22(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ClientProtocols_22 = value;
		Il2CppCodeGenWriteBarrier((&___ClientProtocols_22), value);
	}

	inline static int32_t get_offset_of_RequestCounter_23() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___RequestCounter_23)); }
	inline int64_t get_RequestCounter_23() const { return ___RequestCounter_23; }
	inline int64_t* get_address_of_RequestCounter_23() { return &___RequestCounter_23; }
	inline void set_RequestCounter_23(int64_t value)
	{
		___RequestCounter_23 = value;
	}

	inline static int32_t get_offset_of_LastReceivedMessage_24() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___LastReceivedMessage_24)); }
	inline MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C * get_LastReceivedMessage_24() const { return ___LastReceivedMessage_24; }
	inline MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C ** get_address_of_LastReceivedMessage_24() { return &___LastReceivedMessage_24; }
	inline void set_LastReceivedMessage_24(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C * value)
	{
		___LastReceivedMessage_24 = value;
		Il2CppCodeGenWriteBarrier((&___LastReceivedMessage_24), value);
	}

	inline static int32_t get_offset_of_GroupsToken_25() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___GroupsToken_25)); }
	inline String_t* get_GroupsToken_25() const { return ___GroupsToken_25; }
	inline String_t** get_address_of_GroupsToken_25() { return &___GroupsToken_25; }
	inline void set_GroupsToken_25(String_t* value)
	{
		___GroupsToken_25 = value;
		Il2CppCodeGenWriteBarrier((&___GroupsToken_25), value);
	}

	inline static int32_t get_offset_of_BufferedMessages_26() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___BufferedMessages_26)); }
	inline List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 * get_BufferedMessages_26() const { return ___BufferedMessages_26; }
	inline List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 ** get_address_of_BufferedMessages_26() { return &___BufferedMessages_26; }
	inline void set_BufferedMessages_26(List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 * value)
	{
		___BufferedMessages_26 = value;
		Il2CppCodeGenWriteBarrier((&___BufferedMessages_26), value);
	}

	inline static int32_t get_offset_of_LastMessageReceivedAt_27() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___LastMessageReceivedAt_27)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_LastMessageReceivedAt_27() const { return ___LastMessageReceivedAt_27; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_LastMessageReceivedAt_27() { return &___LastMessageReceivedAt_27; }
	inline void set_LastMessageReceivedAt_27(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___LastMessageReceivedAt_27 = value;
	}

	inline static int32_t get_offset_of_ReconnectStartedAt_28() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___ReconnectStartedAt_28)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_ReconnectStartedAt_28() const { return ___ReconnectStartedAt_28; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_ReconnectStartedAt_28() { return &___ReconnectStartedAt_28; }
	inline void set_ReconnectStartedAt_28(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___ReconnectStartedAt_28 = value;
	}

	inline static int32_t get_offset_of_ReconnectDelayStartedAt_29() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___ReconnectDelayStartedAt_29)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_ReconnectDelayStartedAt_29() const { return ___ReconnectDelayStartedAt_29; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_ReconnectDelayStartedAt_29() { return &___ReconnectDelayStartedAt_29; }
	inline void set_ReconnectDelayStartedAt_29(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___ReconnectDelayStartedAt_29 = value;
	}

	inline static int32_t get_offset_of_ReconnectStarted_30() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___ReconnectStarted_30)); }
	inline bool get_ReconnectStarted_30() const { return ___ReconnectStarted_30; }
	inline bool* get_address_of_ReconnectStarted_30() { return &___ReconnectStarted_30; }
	inline void set_ReconnectStarted_30(bool value)
	{
		___ReconnectStarted_30 = value;
	}

	inline static int32_t get_offset_of_LastPingSentAt_31() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___LastPingSentAt_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_LastPingSentAt_31() const { return ___LastPingSentAt_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_LastPingSentAt_31() { return &___LastPingSentAt_31; }
	inline void set_LastPingSentAt_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___LastPingSentAt_31 = value;
	}

	inline static int32_t get_offset_of_PingRequest_32() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___PingRequest_32)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_PingRequest_32() const { return ___PingRequest_32; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_PingRequest_32() { return &___PingRequest_32; }
	inline void set_PingRequest_32(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___PingRequest_32 = value;
		Il2CppCodeGenWriteBarrier((&___PingRequest_32), value);
	}

	inline static int32_t get_offset_of_TransportConnectionStartedAt_33() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___TransportConnectionStartedAt_33)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_TransportConnectionStartedAt_33() const { return ___TransportConnectionStartedAt_33; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_TransportConnectionStartedAt_33() { return &___TransportConnectionStartedAt_33; }
	inline void set_TransportConnectionStartedAt_33(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___TransportConnectionStartedAt_33 = value;
	}

	inline static int32_t get_offset_of_queryBuilder_34() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___queryBuilder_34)); }
	inline StringBuilder_t * get_queryBuilder_34() const { return ___queryBuilder_34; }
	inline StringBuilder_t ** get_address_of_queryBuilder_34() { return &___queryBuilder_34; }
	inline void set_queryBuilder_34(StringBuilder_t * value)
	{
		___queryBuilder_34 = value;
		Il2CppCodeGenWriteBarrier((&___queryBuilder_34), value);
	}

	inline static int32_t get_offset_of_BuiltConnectionData_35() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___BuiltConnectionData_35)); }
	inline String_t* get_BuiltConnectionData_35() const { return ___BuiltConnectionData_35; }
	inline String_t** get_address_of_BuiltConnectionData_35() { return &___BuiltConnectionData_35; }
	inline void set_BuiltConnectionData_35(String_t* value)
	{
		___BuiltConnectionData_35 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltConnectionData_35), value);
	}

	inline static int32_t get_offset_of_BuiltQueryParams_36() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___BuiltQueryParams_36)); }
	inline String_t* get_BuiltQueryParams_36() const { return ___BuiltQueryParams_36; }
	inline String_t** get_address_of_BuiltQueryParams_36() { return &___BuiltQueryParams_36; }
	inline void set_BuiltQueryParams_36(String_t* value)
	{
		___BuiltQueryParams_36 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltQueryParams_36), value);
	}

	inline static int32_t get_offset_of_NextProtocolToTry_37() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1, ___NextProtocolToTry_37)); }
	inline int32_t get_NextProtocolToTry_37() const { return ___NextProtocolToTry_37; }
	inline int32_t* get_address_of_NextProtocolToTry_37() { return &___NextProtocolToTry_37; }
	inline void set_NextProtocolToTry_37(int32_t value)
	{
		___NextProtocolToTry_37 = value;
	}
};

struct Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1_StaticFields
{
public:
	// BestHTTP.SignalR.JsonEncoders.IJsonEncoder BestHTTP.SignalR.Connection::DefaultEncoder
	RuntimeObject* ___DefaultEncoder_0;

public:
	inline static int32_t get_offset_of_DefaultEncoder_0() { return static_cast<int32_t>(offsetof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1_StaticFields, ___DefaultEncoder_0)); }
	inline RuntimeObject* get_DefaultEncoder_0() const { return ___DefaultEncoder_0; }
	inline RuntimeObject** get_address_of_DefaultEncoder_0() { return &___DefaultEncoder_0; }
	inline void set_DefaultEncoder_0(RuntimeObject* value)
	{
		___DefaultEncoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultEncoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTION_TFFB451BA78ACF8596803F7D83F1CB331728B9BD1_H
#ifndef TRANSPORTBASE_T2706E8F493C0620140FB786680E39A4897A5E8C4_H
#define TRANSPORTBASE_T2706E8F493C0620140FB786680E39A4897A5E8C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.TransportBase
struct  TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalR.Transports.TransportBase::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// BestHTTP.SignalR.IConnection BestHTTP.SignalR.Transports.TransportBase::<Connection>k__BackingField
	RuntimeObject* ___U3CConnectionU3Ek__BackingField_2;
	// BestHTTP.SignalR.TransportStates BestHTTP.SignalR.Transports.TransportBase::_state
	int32_t ____state_3;
	// BestHTTP.SignalR.Transports.OnTransportStateChangedDelegate BestHTTP.SignalR.Transports.TransportBase::OnStateChanged
	OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA * ___OnStateChanged_4;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4, ___U3CConnectionU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CConnectionU3Ek__BackingField_2() const { return ___U3CConnectionU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CConnectionU3Ek__BackingField_2() { return &___U3CConnectionU3Ek__BackingField_2; }
	inline void set_U3CConnectionU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CConnectionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of_OnStateChanged_4() { return static_cast<int32_t>(offsetof(TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4, ___OnStateChanged_4)); }
	inline OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA * get_OnStateChanged_4() const { return ___OnStateChanged_4; }
	inline OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA ** get_address_of_OnStateChanged_4() { return &___OnStateChanged_4; }
	inline void set_OnStateChanged_4(OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA * value)
	{
		___OnStateChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTBASE_T2706E8F493C0620140FB786680E39A4897A5E8C4_H
#ifndef COMPLETION_TB9CE55E740004BBEA514FBB007AAE7DD550217E0_H
#define COMPLETION_TB9CE55E740004BBEA514FBB007AAE7DD550217E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.Completion
struct  Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.Completion::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.Completion::invocationId
	String_t* ___invocationId_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.Completion
struct Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.Completion
struct Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
};
#endif // COMPLETION_TB9CE55E740004BBEA514FBB007AAE7DD550217E0_H
#ifndef COMPLETIONWITHERROR_TCE8721888BA1E2A55C3FD56A8D0B5E99A204B266_H
#define COMPLETIONWITHERROR_TCE8721888BA1E2A55C3FD56A8D0B5E99A204B266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.CompletionWithError
struct  CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.CompletionWithError::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.CompletionWithError::invocationId
	String_t* ___invocationId_1;
	// System.String BestHTTP.SignalRCore.Messages.CompletionWithError::error
	String_t* ___error_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266, ___error_2)); }
	inline String_t* get_error_2() const { return ___error_2; }
	inline String_t** get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(String_t* value)
	{
		___error_2 = value;
		Il2CppCodeGenWriteBarrier((&___error_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.CompletionWithError
struct CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
	char* ___error_2;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.CompletionWithError
struct CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
	Il2CppChar* ___error_2;
};
#endif // COMPLETIONWITHERROR_TCE8721888BA1E2A55C3FD56A8D0B5E99A204B266_H
#ifndef COMPLETIONWITHRESULT_T337E6970C4D993161FC8FC16744F0066A43A09D4_H
#define COMPLETIONWITHRESULT_T337E6970C4D993161FC8FC16744F0066A43A09D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.CompletionWithResult
struct  CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.CompletionWithResult::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.CompletionWithResult::invocationId
	String_t* ___invocationId_1;
	// System.Object BestHTTP.SignalRCore.Messages.CompletionWithResult::result
	RuntimeObject * ___result_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4, ___result_2)); }
	inline RuntimeObject * get_result_2() const { return ___result_2; }
	inline RuntimeObject ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(RuntimeObject * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.CompletionWithResult
struct CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
	Il2CppIUnknown* ___result_2;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.CompletionWithResult
struct CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
	Il2CppIUnknown* ___result_2;
};
#endif // COMPLETIONWITHRESULT_T337E6970C4D993161FC8FC16744F0066A43A09D4_H
#ifndef INVOCATIONMESSAGE_T3238473A024AC45EC87EE767ED1A99D18E0AC5FD_H
#define INVOCATIONMESSAGE_T3238473A024AC45EC87EE767ED1A99D18E0AC5FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.InvocationMessage
struct  InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.InvocationMessage::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.InvocationMessage::invocationId
	String_t* ___invocationId_1;
	// System.Boolean BestHTTP.SignalRCore.Messages.InvocationMessage::nonblocking
	bool ___nonblocking_2;
	// System.String BestHTTP.SignalRCore.Messages.InvocationMessage::target
	String_t* ___target_3;
	// System.Object[] BestHTTP.SignalRCore.Messages.InvocationMessage::arguments
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}

	inline static int32_t get_offset_of_nonblocking_2() { return static_cast<int32_t>(offsetof(InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD, ___nonblocking_2)); }
	inline bool get_nonblocking_2() const { return ___nonblocking_2; }
	inline bool* get_address_of_nonblocking_2() { return &___nonblocking_2; }
	inline void set_nonblocking_2(bool value)
	{
		___nonblocking_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD, ___target_3)); }
	inline String_t* get_target_3() const { return ___target_3; }
	inline String_t** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(String_t* value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_arguments_4() { return static_cast<int32_t>(offsetof(InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD, ___arguments_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_arguments_4() const { return ___arguments_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_arguments_4() { return &___arguments_4; }
	inline void set_arguments_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.InvocationMessage
struct InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
	int32_t ___nonblocking_2;
	char* ___target_3;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.InvocationMessage
struct InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
	int32_t ___nonblocking_2;
	Il2CppChar* ___target_3;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
};
#endif // INVOCATIONMESSAGE_T3238473A024AC45EC87EE767ED1A99D18E0AC5FD_H
#ifndef MESSAGE_T50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0_H
#define MESSAGE_T50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.Message
struct  Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.Message::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.Message::invocationId
	String_t* ___invocationId_1;
	// System.Boolean BestHTTP.SignalRCore.Messages.Message::nonblocking
	bool ___nonblocking_2;
	// System.String BestHTTP.SignalRCore.Messages.Message::target
	String_t* ___target_3;
	// System.Object[] BestHTTP.SignalRCore.Messages.Message::arguments
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
	// System.String[] BestHTTP.SignalRCore.Messages.Message::streamIds
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___streamIds_5;
	// System.Object BestHTTP.SignalRCore.Messages.Message::item
	RuntimeObject * ___item_6;
	// System.Object BestHTTP.SignalRCore.Messages.Message::result
	RuntimeObject * ___result_7;
	// System.String BestHTTP.SignalRCore.Messages.Message::error
	String_t* ___error_8;
	// System.Boolean BestHTTP.SignalRCore.Messages.Message::allowReconnect
	bool ___allowReconnect_9;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}

	inline static int32_t get_offset_of_nonblocking_2() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___nonblocking_2)); }
	inline bool get_nonblocking_2() const { return ___nonblocking_2; }
	inline bool* get_address_of_nonblocking_2() { return &___nonblocking_2; }
	inline void set_nonblocking_2(bool value)
	{
		___nonblocking_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___target_3)); }
	inline String_t* get_target_3() const { return ___target_3; }
	inline String_t** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(String_t* value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_arguments_4() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___arguments_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_arguments_4() const { return ___arguments_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_arguments_4() { return &___arguments_4; }
	inline void set_arguments_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_4), value);
	}

	inline static int32_t get_offset_of_streamIds_5() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___streamIds_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_streamIds_5() const { return ___streamIds_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_streamIds_5() { return &___streamIds_5; }
	inline void set_streamIds_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___streamIds_5 = value;
		Il2CppCodeGenWriteBarrier((&___streamIds_5), value);
	}

	inline static int32_t get_offset_of_item_6() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___item_6)); }
	inline RuntimeObject * get_item_6() const { return ___item_6; }
	inline RuntimeObject ** get_address_of_item_6() { return &___item_6; }
	inline void set_item_6(RuntimeObject * value)
	{
		___item_6 = value;
		Il2CppCodeGenWriteBarrier((&___item_6), value);
	}

	inline static int32_t get_offset_of_result_7() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___result_7)); }
	inline RuntimeObject * get_result_7() const { return ___result_7; }
	inline RuntimeObject ** get_address_of_result_7() { return &___result_7; }
	inline void set_result_7(RuntimeObject * value)
	{
		___result_7 = value;
		Il2CppCodeGenWriteBarrier((&___result_7), value);
	}

	inline static int32_t get_offset_of_error_8() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___error_8)); }
	inline String_t* get_error_8() const { return ___error_8; }
	inline String_t** get_address_of_error_8() { return &___error_8; }
	inline void set_error_8(String_t* value)
	{
		___error_8 = value;
		Il2CppCodeGenWriteBarrier((&___error_8), value);
	}

	inline static int32_t get_offset_of_allowReconnect_9() { return static_cast<int32_t>(offsetof(Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0, ___allowReconnect_9)); }
	inline bool get_allowReconnect_9() const { return ___allowReconnect_9; }
	inline bool* get_address_of_allowReconnect_9() { return &___allowReconnect_9; }
	inline void set_allowReconnect_9(bool value)
	{
		___allowReconnect_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.Message
struct Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
	int32_t ___nonblocking_2;
	char* ___target_3;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
	char** ___streamIds_5;
	Il2CppIUnknown* ___item_6;
	Il2CppIUnknown* ___result_7;
	char* ___error_8;
	int32_t ___allowReconnect_9;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.Message
struct Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
	int32_t ___nonblocking_2;
	Il2CppChar* ___target_3;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
	Il2CppChar** ___streamIds_5;
	Il2CppIUnknown* ___item_6;
	Il2CppIUnknown* ___result_7;
	Il2CppChar* ___error_8;
	int32_t ___allowReconnect_9;
};
#endif // MESSAGE_T50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0_H
#ifndef STREAMITEMMESSAGE_T282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6_H
#define STREAMITEMMESSAGE_T282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.StreamItemMessage
struct  StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.StreamItemMessage::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.StreamItemMessage::invocationId
	String_t* ___invocationId_1;
	// System.Object BestHTTP.SignalRCore.Messages.StreamItemMessage::item
	RuntimeObject * ___item_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}

	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6, ___item_2)); }
	inline RuntimeObject * get_item_2() const { return ___item_2; }
	inline RuntimeObject ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(RuntimeObject * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier((&___item_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.StreamItemMessage
struct StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
	Il2CppIUnknown* ___item_2;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.StreamItemMessage
struct StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
	Il2CppIUnknown* ___item_2;
};
#endif // STREAMITEMMESSAGE_T282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6_H
#ifndef UPLOADINVOCATIONMESSAGE_T1BFFF8530DFE61F5157CF86E10098DA1DDED96C0_H
#define UPLOADINVOCATIONMESSAGE_T1BFFF8530DFE61F5157CF86E10098DA1DDED96C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Messages.UploadInvocationMessage
struct  UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0 
{
public:
	// BestHTTP.SignalRCore.Messages.MessageTypes BestHTTP.SignalRCore.Messages.UploadInvocationMessage::type
	int32_t ___type_0;
	// System.String BestHTTP.SignalRCore.Messages.UploadInvocationMessage::invocationId
	String_t* ___invocationId_1;
	// System.Boolean BestHTTP.SignalRCore.Messages.UploadInvocationMessage::nonblocking
	bool ___nonblocking_2;
	// System.String BestHTTP.SignalRCore.Messages.UploadInvocationMessage::target
	String_t* ___target_3;
	// System.Object[] BestHTTP.SignalRCore.Messages.UploadInvocationMessage::arguments
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
	// System.String[] BestHTTP.SignalRCore.Messages.UploadInvocationMessage::streamIds
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___streamIds_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_invocationId_1() { return static_cast<int32_t>(offsetof(UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0, ___invocationId_1)); }
	inline String_t* get_invocationId_1() const { return ___invocationId_1; }
	inline String_t** get_address_of_invocationId_1() { return &___invocationId_1; }
	inline void set_invocationId_1(String_t* value)
	{
		___invocationId_1 = value;
		Il2CppCodeGenWriteBarrier((&___invocationId_1), value);
	}

	inline static int32_t get_offset_of_nonblocking_2() { return static_cast<int32_t>(offsetof(UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0, ___nonblocking_2)); }
	inline bool get_nonblocking_2() const { return ___nonblocking_2; }
	inline bool* get_address_of_nonblocking_2() { return &___nonblocking_2; }
	inline void set_nonblocking_2(bool value)
	{
		___nonblocking_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0, ___target_3)); }
	inline String_t* get_target_3() const { return ___target_3; }
	inline String_t** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(String_t* value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_arguments_4() { return static_cast<int32_t>(offsetof(UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0, ___arguments_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_arguments_4() const { return ___arguments_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_arguments_4() { return &___arguments_4; }
	inline void set_arguments_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_4), value);
	}

	inline static int32_t get_offset_of_streamIds_5() { return static_cast<int32_t>(offsetof(UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0, ___streamIds_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_streamIds_5() const { return ___streamIds_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_streamIds_5() { return &___streamIds_5; }
	inline void set_streamIds_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___streamIds_5 = value;
		Il2CppCodeGenWriteBarrier((&___streamIds_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.Messages.UploadInvocationMessage
struct UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0_marshaled_pinvoke
{
	int32_t ___type_0;
	char* ___invocationId_1;
	int32_t ___nonblocking_2;
	char* ___target_3;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
	char** ___streamIds_5;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.Messages.UploadInvocationMessage
struct UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0_marshaled_com
{
	int32_t ___type_0;
	Il2CppChar* ___invocationId_1;
	int32_t ___nonblocking_2;
	Il2CppChar* ___target_3;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___arguments_4;
	Il2CppChar** ___streamIds_5;
};
#endif // UPLOADINVOCATIONMESSAGE_T1BFFF8530DFE61F5157CF86E10098DA1DDED96C0_H
#ifndef TRANSPORTBASE_TD1E09622468E1FC60AC579AD943D006B0BCEC75E_H
#define TRANSPORTBASE_TD1E09622468E1FC60AC579AD943D006B0BCEC75E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Transports.TransportBase
struct  TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.TransportStates BestHTTP.SignalRCore.Transports.TransportBase::_state
	int32_t ____state_0;
	// System.String BestHTTP.SignalRCore.Transports.TransportBase::<ErrorReason>k__BackingField
	String_t* ___U3CErrorReasonU3Ek__BackingField_1;
	// System.Action`2<BestHTTP.SignalRCore.TransportStates,BestHTTP.SignalRCore.TransportStates> BestHTTP.SignalRCore.Transports.TransportBase::OnStateChanged
	Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB * ___OnStateChanged_2;
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.Message> BestHTTP.SignalRCore.Transports.TransportBase::messages
	List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 * ___messages_3;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.SignalRCore.Transports.TransportBase::connection
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___connection_4;
	// System.Text.StringBuilder BestHTTP.SignalRCore.Transports.TransportBase::queryBuilder
	StringBuilder_t * ___queryBuilder_5;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ____state_0)); }
	inline int32_t get__state_0() const { return ____state_0; }
	inline int32_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(int32_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorReasonU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___U3CErrorReasonU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorReasonU3Ek__BackingField_1() const { return ___U3CErrorReasonU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorReasonU3Ek__BackingField_1() { return &___U3CErrorReasonU3Ek__BackingField_1; }
	inline void set_U3CErrorReasonU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorReasonU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorReasonU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_OnStateChanged_2() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___OnStateChanged_2)); }
	inline Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB * get_OnStateChanged_2() const { return ___OnStateChanged_2; }
	inline Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB ** get_address_of_OnStateChanged_2() { return &___OnStateChanged_2; }
	inline void set_OnStateChanged_2(Action_2_tA15D9B79DB2F59FCA2CA39724004226D6A2897FB * value)
	{
		___OnStateChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_2), value);
	}

	inline static int32_t get_offset_of_messages_3() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___messages_3)); }
	inline List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 * get_messages_3() const { return ___messages_3; }
	inline List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 ** get_address_of_messages_3() { return &___messages_3; }
	inline void set_messages_3(List_1_t9368258399CDE9A0765C07268ED1A1EFCA3518C3 * value)
	{
		___messages_3 = value;
		Il2CppCodeGenWriteBarrier((&___messages_3), value);
	}

	inline static int32_t get_offset_of_connection_4() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___connection_4)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_connection_4() const { return ___connection_4; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_connection_4() { return &___connection_4; }
	inline void set_connection_4(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___connection_4 = value;
		Il2CppCodeGenWriteBarrier((&___connection_4), value);
	}

	inline static int32_t get_offset_of_queryBuilder_5() { return static_cast<int32_t>(offsetof(TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E, ___queryBuilder_5)); }
	inline StringBuilder_t * get_queryBuilder_5() const { return ___queryBuilder_5; }
	inline StringBuilder_t ** get_address_of_queryBuilder_5() { return &___queryBuilder_5; }
	inline void set_queryBuilder_5(StringBuilder_t * value)
	{
		___queryBuilder_5 = value;
		Il2CppCodeGenWriteBarrier((&___queryBuilder_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSPORTBASE_TD1E09622468E1FC60AC579AD943D006B0BCEC75E_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T357D5ACD970F7036B6540D4E511909E232856A80_H
#define NULLABLE_1_T357D5ACD970F7036B6540D4E511909E232856A80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 
{
public:
	// T System.Nullable`1::value
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80, ___value_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_value_0() const { return ___value_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T357D5ACD970F7036B6540D4E511909E232856A80_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ONAUTHENTICATIONFAILEDDELEGATE_T34655CBBF228A876BC4DED628B7744D2F79998E4_H
#define ONAUTHENTICATIONFAILEDDELEGATE_T34655CBBF228A876BC4DED628B7744D2F79998E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Authentication.OnAuthenticationFailedDelegate
struct  OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONAUTHENTICATIONFAILEDDELEGATE_T34655CBBF228A876BC4DED628B7744D2F79998E4_H
#ifndef ONAUTHENTICATIONSUCCEDEDDELEGATE_T94E342910B973BE0794F8E324EC09A5395354C55_H
#define ONAUTHENTICATIONSUCCEDEDDELEGATE_T94E342910B973BE0794F8E324EC09A5395354C55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Authentication.OnAuthenticationSuccededDelegate
struct  OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONAUTHENTICATIONSUCCEDEDDELEGATE_T94E342910B973BE0794F8E324EC09A5395354C55_H
#ifndef ONMETHODCALLCALLBACKDELEGATE_T2DD6359595F1EFEAE8303FACA30B8A6CF02632D7_H
#define ONMETHODCALLCALLBACKDELEGATE_T2DD6359595F1EFEAE8303FACA30B8A6CF02632D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Hubs.OnMethodCallCallbackDelegate
struct  OnMethodCallCallbackDelegate_t2DD6359595F1EFEAE8303FACA30B8A6CF02632D7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMETHODCALLCALLBACKDELEGATE_T2DD6359595F1EFEAE8303FACA30B8A6CF02632D7_H
#ifndef ONMETHODCALLDELEGATE_T663C97606404E128C8A7F5A61BF72C0DFDF6CFFE_H
#define ONMETHODCALLDELEGATE_T663C97606404E128C8A7F5A61BF72C0DFDF6CFFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Hubs.OnMethodCallDelegate
struct  OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMETHODCALLDELEGATE_T663C97606404E128C8A7F5A61BF72C0DFDF6CFFE_H
#ifndef ONMETHODFAILEDDELEGATE_TD624C74A42D50331E1162C71DE5A804580ACF75B_H
#define ONMETHODFAILEDDELEGATE_TD624C74A42D50331E1162C71DE5A804580ACF75B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Hubs.OnMethodFailedDelegate
struct  OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMETHODFAILEDDELEGATE_TD624C74A42D50331E1162C71DE5A804580ACF75B_H
#ifndef ONMETHODPROGRESSDELEGATE_TC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4_H
#define ONMETHODPROGRESSDELEGATE_TC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Hubs.OnMethodProgressDelegate
struct  OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMETHODPROGRESSDELEGATE_TC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4_H
#ifndef ONMETHODRESULTDELEGATE_T65AAE7A6ACF4EA958CA033903850C88B394D887F_H
#define ONMETHODRESULTDELEGATE_T65AAE7A6ACF4EA958CA033903850C88B394D887F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Hubs.OnMethodResultDelegate
struct  OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMETHODRESULTDELEGATE_T65AAE7A6ACF4EA958CA033903850C88B394D887F_H
#ifndef MULTIMESSAGE_T1735D64E1B76A0DD022E792B348149DF9745ED1C_H
#define MULTIMESSAGE_T1735D64E1B76A0DD022E792B348149DF9745ED1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Messages.MultiMessage
struct  MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalR.Messages.MultiMessage::<MessageId>k__BackingField
	String_t* ___U3CMessageIdU3Ek__BackingField_0;
	// System.Boolean BestHTTP.SignalR.Messages.MultiMessage::<IsInitialization>k__BackingField
	bool ___U3CIsInitializationU3Ek__BackingField_1;
	// System.String BestHTTP.SignalR.Messages.MultiMessage::<GroupsToken>k__BackingField
	String_t* ___U3CGroupsTokenU3Ek__BackingField_2;
	// System.Boolean BestHTTP.SignalR.Messages.MultiMessage::<ShouldReconnect>k__BackingField
	bool ___U3CShouldReconnectU3Ek__BackingField_3;
	// System.Nullable`1<System.TimeSpan> BestHTTP.SignalR.Messages.MultiMessage::<PollDelay>k__BackingField
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___U3CPollDelayU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<BestHTTP.SignalR.Messages.IServerMessage> BestHTTP.SignalR.Messages.MultiMessage::<Data>k__BackingField
	List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 * ___U3CDataU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CMessageIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C, ___U3CMessageIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CMessageIdU3Ek__BackingField_0() const { return ___U3CMessageIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMessageIdU3Ek__BackingField_0() { return &___U3CMessageIdU3Ek__BackingField_0; }
	inline void set_U3CMessageIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CMessageIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsInitializationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C, ___U3CIsInitializationU3Ek__BackingField_1)); }
	inline bool get_U3CIsInitializationU3Ek__BackingField_1() const { return ___U3CIsInitializationU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsInitializationU3Ek__BackingField_1() { return &___U3CIsInitializationU3Ek__BackingField_1; }
	inline void set_U3CIsInitializationU3Ek__BackingField_1(bool value)
	{
		___U3CIsInitializationU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGroupsTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C, ___U3CGroupsTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CGroupsTokenU3Ek__BackingField_2() const { return ___U3CGroupsTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CGroupsTokenU3Ek__BackingField_2() { return &___U3CGroupsTokenU3Ek__BackingField_2; }
	inline void set_U3CGroupsTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CGroupsTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupsTokenU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CShouldReconnectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C, ___U3CShouldReconnectU3Ek__BackingField_3)); }
	inline bool get_U3CShouldReconnectU3Ek__BackingField_3() const { return ___U3CShouldReconnectU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CShouldReconnectU3Ek__BackingField_3() { return &___U3CShouldReconnectU3Ek__BackingField_3; }
	inline void set_U3CShouldReconnectU3Ek__BackingField_3(bool value)
	{
		___U3CShouldReconnectU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CPollDelayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C, ___U3CPollDelayU3Ek__BackingField_4)); }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  get_U3CPollDelayU3Ek__BackingField_4() const { return ___U3CPollDelayU3Ek__BackingField_4; }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * get_address_of_U3CPollDelayU3Ek__BackingField_4() { return &___U3CPollDelayU3Ek__BackingField_4; }
	inline void set_U3CPollDelayU3Ek__BackingField_4(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  value)
	{
		___U3CPollDelayU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C, ___U3CDataU3Ek__BackingField_5)); }
	inline List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 * get_U3CDataU3Ek__BackingField_5() const { return ___U3CDataU3Ek__BackingField_5; }
	inline List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 ** get_address_of_U3CDataU3Ek__BackingField_5() { return &___U3CDataU3Ek__BackingField_5; }
	inline void set_U3CDataU3Ek__BackingField_5(List_1_t0C6400E78E6ADB8F75F649BBF6732EBB739FA742 * value)
	{
		___U3CDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIMESSAGE_T1735D64E1B76A0DD022E792B348149DF9745ED1C_H
#ifndef NEGOTIATIONDATA_T82AED31F8B3334FD322A6C36DE57DF648E79C340_H
#define NEGOTIATIONDATA_T82AED31F8B3334FD322A6C36DE57DF648E79C340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.NegotiationData
struct  NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340  : public RuntimeObject
{
public:
	// System.String BestHTTP.SignalR.NegotiationData::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_0;
	// System.String BestHTTP.SignalR.NegotiationData::<WebSocketServerUrl>k__BackingField
	String_t* ___U3CWebSocketServerUrlU3Ek__BackingField_1;
	// System.String BestHTTP.SignalR.NegotiationData::<ConnectionToken>k__BackingField
	String_t* ___U3CConnectionTokenU3Ek__BackingField_2;
	// System.String BestHTTP.SignalR.NegotiationData::<ConnectionId>k__BackingField
	String_t* ___U3CConnectionIdU3Ek__BackingField_3;
	// System.Nullable`1<System.TimeSpan> BestHTTP.SignalR.NegotiationData::<KeepAliveTimeout>k__BackingField
	Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  ___U3CKeepAliveTimeoutU3Ek__BackingField_4;
	// System.TimeSpan BestHTTP.SignalR.NegotiationData::<DisconnectTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CDisconnectTimeoutU3Ek__BackingField_5;
	// System.TimeSpan BestHTTP.SignalR.NegotiationData::<ConnectionTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CConnectionTimeoutU3Ek__BackingField_6;
	// System.Boolean BestHTTP.SignalR.NegotiationData::<TryWebSockets>k__BackingField
	bool ___U3CTryWebSocketsU3Ek__BackingField_7;
	// System.String BestHTTP.SignalR.NegotiationData::<ProtocolVersion>k__BackingField
	String_t* ___U3CProtocolVersionU3Ek__BackingField_8;
	// System.TimeSpan BestHTTP.SignalR.NegotiationData::<TransportConnectTimeout>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CTransportConnectTimeoutU3Ek__BackingField_9;
	// System.TimeSpan BestHTTP.SignalR.NegotiationData::<LongPollDelay>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CLongPollDelayU3Ek__BackingField_10;
	// System.Action`1<BestHTTP.SignalR.NegotiationData> BestHTTP.SignalR.NegotiationData::OnReceived
	Action_1_t102A496083CF16B515632B8B5AF8186088B6E6B9 * ___OnReceived_11;
	// System.Action`2<BestHTTP.SignalR.NegotiationData,System.String> BestHTTP.SignalR.NegotiationData::OnError
	Action_2_t66CE6B50694D4908323F1B592B784E18DDF508A7 * ___OnError_12;
	// BestHTTP.HTTPRequest BestHTTP.SignalR.NegotiationData::NegotiationRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___NegotiationRequest_13;
	// BestHTTP.SignalR.IConnection BestHTTP.SignalR.NegotiationData::Connection
	RuntimeObject* ___Connection_14;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CUrlU3Ek__BackingField_0)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_0() const { return ___U3CUrlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_0() { return &___U3CUrlU3Ek__BackingField_0; }
	inline void set_U3CUrlU3Ek__BackingField_0(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWebSocketServerUrlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CWebSocketServerUrlU3Ek__BackingField_1)); }
	inline String_t* get_U3CWebSocketServerUrlU3Ek__BackingField_1() const { return ___U3CWebSocketServerUrlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CWebSocketServerUrlU3Ek__BackingField_1() { return &___U3CWebSocketServerUrlU3Ek__BackingField_1; }
	inline void set_U3CWebSocketServerUrlU3Ek__BackingField_1(String_t* value)
	{
		___U3CWebSocketServerUrlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebSocketServerUrlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CConnectionTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CConnectionTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CConnectionTokenU3Ek__BackingField_2() const { return ___U3CConnectionTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CConnectionTokenU3Ek__BackingField_2() { return &___U3CConnectionTokenU3Ek__BackingField_2; }
	inline void set_U3CConnectionTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CConnectionTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionTokenU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CConnectionIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CConnectionIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CConnectionIdU3Ek__BackingField_3() const { return ___U3CConnectionIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CConnectionIdU3Ek__BackingField_3() { return &___U3CConnectionIdU3Ek__BackingField_3; }
	inline void set_U3CConnectionIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CConnectionIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectionIdU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CKeepAliveTimeoutU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CKeepAliveTimeoutU3Ek__BackingField_4)); }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  get_U3CKeepAliveTimeoutU3Ek__BackingField_4() const { return ___U3CKeepAliveTimeoutU3Ek__BackingField_4; }
	inline Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80 * get_address_of_U3CKeepAliveTimeoutU3Ek__BackingField_4() { return &___U3CKeepAliveTimeoutU3Ek__BackingField_4; }
	inline void set_U3CKeepAliveTimeoutU3Ek__BackingField_4(Nullable_1_t357D5ACD970F7036B6540D4E511909E232856A80  value)
	{
		___U3CKeepAliveTimeoutU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CDisconnectTimeoutU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CDisconnectTimeoutU3Ek__BackingField_5)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CDisconnectTimeoutU3Ek__BackingField_5() const { return ___U3CDisconnectTimeoutU3Ek__BackingField_5; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CDisconnectTimeoutU3Ek__BackingField_5() { return &___U3CDisconnectTimeoutU3Ek__BackingField_5; }
	inline void set_U3CDisconnectTimeoutU3Ek__BackingField_5(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CDisconnectTimeoutU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CConnectionTimeoutU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CConnectionTimeoutU3Ek__BackingField_6)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CConnectionTimeoutU3Ek__BackingField_6() const { return ___U3CConnectionTimeoutU3Ek__BackingField_6; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CConnectionTimeoutU3Ek__BackingField_6() { return &___U3CConnectionTimeoutU3Ek__BackingField_6; }
	inline void set_U3CConnectionTimeoutU3Ek__BackingField_6(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CConnectionTimeoutU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CTryWebSocketsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CTryWebSocketsU3Ek__BackingField_7)); }
	inline bool get_U3CTryWebSocketsU3Ek__BackingField_7() const { return ___U3CTryWebSocketsU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CTryWebSocketsU3Ek__BackingField_7() { return &___U3CTryWebSocketsU3Ek__BackingField_7; }
	inline void set_U3CTryWebSocketsU3Ek__BackingField_7(bool value)
	{
		___U3CTryWebSocketsU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CProtocolVersionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CProtocolVersionU3Ek__BackingField_8)); }
	inline String_t* get_U3CProtocolVersionU3Ek__BackingField_8() const { return ___U3CProtocolVersionU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CProtocolVersionU3Ek__BackingField_8() { return &___U3CProtocolVersionU3Ek__BackingField_8; }
	inline void set_U3CProtocolVersionU3Ek__BackingField_8(String_t* value)
	{
		___U3CProtocolVersionU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolVersionU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CTransportConnectTimeoutU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CTransportConnectTimeoutU3Ek__BackingField_9)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CTransportConnectTimeoutU3Ek__BackingField_9() const { return ___U3CTransportConnectTimeoutU3Ek__BackingField_9; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CTransportConnectTimeoutU3Ek__BackingField_9() { return &___U3CTransportConnectTimeoutU3Ek__BackingField_9; }
	inline void set_U3CTransportConnectTimeoutU3Ek__BackingField_9(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CTransportConnectTimeoutU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLongPollDelayU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___U3CLongPollDelayU3Ek__BackingField_10)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CLongPollDelayU3Ek__BackingField_10() const { return ___U3CLongPollDelayU3Ek__BackingField_10; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CLongPollDelayU3Ek__BackingField_10() { return &___U3CLongPollDelayU3Ek__BackingField_10; }
	inline void set_U3CLongPollDelayU3Ek__BackingField_10(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CLongPollDelayU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_OnReceived_11() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___OnReceived_11)); }
	inline Action_1_t102A496083CF16B515632B8B5AF8186088B6E6B9 * get_OnReceived_11() const { return ___OnReceived_11; }
	inline Action_1_t102A496083CF16B515632B8B5AF8186088B6E6B9 ** get_address_of_OnReceived_11() { return &___OnReceived_11; }
	inline void set_OnReceived_11(Action_1_t102A496083CF16B515632B8B5AF8186088B6E6B9 * value)
	{
		___OnReceived_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnReceived_11), value);
	}

	inline static int32_t get_offset_of_OnError_12() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___OnError_12)); }
	inline Action_2_t66CE6B50694D4908323F1B592B784E18DDF508A7 * get_OnError_12() const { return ___OnError_12; }
	inline Action_2_t66CE6B50694D4908323F1B592B784E18DDF508A7 ** get_address_of_OnError_12() { return &___OnError_12; }
	inline void set_OnError_12(Action_2_t66CE6B50694D4908323F1B592B784E18DDF508A7 * value)
	{
		___OnError_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_12), value);
	}

	inline static int32_t get_offset_of_NegotiationRequest_13() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___NegotiationRequest_13)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_NegotiationRequest_13() const { return ___NegotiationRequest_13; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_NegotiationRequest_13() { return &___NegotiationRequest_13; }
	inline void set_NegotiationRequest_13(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___NegotiationRequest_13 = value;
		Il2CppCodeGenWriteBarrier((&___NegotiationRequest_13), value);
	}

	inline static int32_t get_offset_of_Connection_14() { return static_cast<int32_t>(offsetof(NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340, ___Connection_14)); }
	inline RuntimeObject* get_Connection_14() const { return ___Connection_14; }
	inline RuntimeObject** get_address_of_Connection_14() { return &___Connection_14; }
	inline void set_Connection_14(RuntimeObject* value)
	{
		___Connection_14 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGOTIATIONDATA_T82AED31F8B3334FD322A6C36DE57DF648E79C340_H
#ifndef ONCLOSEDDELEGATE_TA2AAABD7251D76ABBDAF038C0D022571460F0F98_H
#define ONCLOSEDDELEGATE_TA2AAABD7251D76ABBDAF038C0D022571460F0F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.OnClosedDelegate
struct  OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLOSEDDELEGATE_TA2AAABD7251D76ABBDAF038C0D022571460F0F98_H
#ifndef ONCONNECTEDDELEGATE_T68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C_H
#define ONCONNECTEDDELEGATE_T68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.OnConnectedDelegate
struct  OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCONNECTEDDELEGATE_T68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C_H
#ifndef ONERRORDELEGATE_TE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4_H
#define ONERRORDELEGATE_TE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.OnErrorDelegate
struct  OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORDELEGATE_TE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4_H
#ifndef ONNONHUBMESSAGEDELEGATE_T250CBCD416E95E40350F05C96C2DA5FEEF6269D4_H
#define ONNONHUBMESSAGEDELEGATE_T250CBCD416E95E40350F05C96C2DA5FEEF6269D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.OnNonHubMessageDelegate
struct  OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNONHUBMESSAGEDELEGATE_T250CBCD416E95E40350F05C96C2DA5FEEF6269D4_H
#ifndef ONPREPAREREQUESTDELEGATE_TBFA22ECEC2EB530032B56BB66F75C79A448A5B81_H
#define ONPREPAREREQUESTDELEGATE_TBFA22ECEC2EB530032B56BB66F75C79A448A5B81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.OnPrepareRequestDelegate
struct  OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPREPAREREQUESTDELEGATE_TBFA22ECEC2EB530032B56BB66F75C79A448A5B81_H
#ifndef ONSTATECHANGED_TFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249_H
#define ONSTATECHANGED_TFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.OnStateChanged
struct  OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATECHANGED_TFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249_H
#ifndef ONTRANSPORTSTATECHANGEDDELEGATE_TC01E6CC1DC09835144F79F585EAFF829AA2A26AA_H
#define ONTRANSPORTSTATECHANGEDDELEGATE_TC01E6CC1DC09835144F79F585EAFF829AA2A26AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.OnTransportStateChangedDelegate
struct  OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRANSPORTSTATECHANGEDDELEGATE_TC01E6CC1DC09835144F79F585EAFF829AA2A26AA_H
#ifndef POSTSENDTRANSPORTBASE_T15233AF34294A66CDB5FC191FE191F678F528ADA_H
#define POSTSENDTRANSPORTBASE_T15233AF34294A66CDB5FC191FE191F678F528ADA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.PostSendTransportBase
struct  PostSendTransportBase_t15233AF34294A66CDB5FC191FE191F678F528ADA  : public TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4
{
public:
	// System.Collections.Generic.List`1<BestHTTP.HTTPRequest> BestHTTP.SignalR.Transports.PostSendTransportBase::sendRequestQueue
	List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * ___sendRequestQueue_5;

public:
	inline static int32_t get_offset_of_sendRequestQueue_5() { return static_cast<int32_t>(offsetof(PostSendTransportBase_t15233AF34294A66CDB5FC191FE191F678F528ADA, ___sendRequestQueue_5)); }
	inline List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * get_sendRequestQueue_5() const { return ___sendRequestQueue_5; }
	inline List_1_t0E64F6F010761063FA9757A1456A883274512FE8 ** get_address_of_sendRequestQueue_5() { return &___sendRequestQueue_5; }
	inline void set_sendRequestQueue_5(List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * value)
	{
		___sendRequestQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___sendRequestQueue_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTSENDTRANSPORTBASE_T15233AF34294A66CDB5FC191FE191F678F528ADA_H
#ifndef WEBSOCKETTRANSPORT_T37511257303A7E425243B022ACA4A1355A6BC9BF_H
#define WEBSOCKETTRANSPORT_T37511257303A7E425243B022ACA4A1355A6BC9BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.WebSocketTransport
struct  WebSocketTransport_t37511257303A7E425243B022ACA4A1355A6BC9BF  : public TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4
{
public:
	// BestHTTP.WebSocket.WebSocket BestHTTP.SignalR.Transports.WebSocketTransport::wSocket
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * ___wSocket_5;

public:
	inline static int32_t get_offset_of_wSocket_5() { return static_cast<int32_t>(offsetof(WebSocketTransport_t37511257303A7E425243B022ACA4A1355A6BC9BF, ___wSocket_5)); }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * get_wSocket_5() const { return ___wSocket_5; }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 ** get_address_of_wSocket_5() { return &___wSocket_5; }
	inline void set_wSocket_5(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * value)
	{
		___wSocket_5 = value;
		Il2CppCodeGenWriteBarrier((&___wSocket_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETTRANSPORT_T37511257303A7E425243B022ACA4A1355A6BC9BF_H
#ifndef WEBSOCKETTRANSPORT_TF310BC98528295E21CB31F983F6EAD1F509AF8AC_H
#define WEBSOCKETTRANSPORT_TF310BC98528295E21CB31F983F6EAD1F509AF8AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalRCore.Transports.WebSocketTransport
struct  WebSocketTransport_tF310BC98528295E21CB31F983F6EAD1F509AF8AC  : public TransportBase_tD1E09622468E1FC60AC579AD943D006B0BCEC75E
{
public:
	// BestHTTP.WebSocket.WebSocket BestHTTP.SignalRCore.Transports.WebSocketTransport::webSocket
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * ___webSocket_6;

public:
	inline static int32_t get_offset_of_webSocket_6() { return static_cast<int32_t>(offsetof(WebSocketTransport_tF310BC98528295E21CB31F983F6EAD1F509AF8AC, ___webSocket_6)); }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * get_webSocket_6() const { return ___webSocket_6; }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 ** get_address_of_webSocket_6() { return &___webSocket_6; }
	inline void set_webSocket_6(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * value)
	{
		___webSocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___webSocket_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETTRANSPORT_TF310BC98528295E21CB31F983F6EAD1F509AF8AC_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef POLLINGTRANSPORT_TE4872E4BE81FD6773440986731BC4B8D8A31F7B1_H
#define POLLINGTRANSPORT_TE4872E4BE81FD6773440986731BC4B8D8A31F7B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.PollingTransport
struct  PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1  : public PostSendTransportBase_t15233AF34294A66CDB5FC191FE191F678F528ADA
{
public:
	// System.DateTime BestHTTP.SignalR.Transports.PollingTransport::LastPoll
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___LastPoll_6;
	// System.TimeSpan BestHTTP.SignalR.Transports.PollingTransport::PollDelay
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___PollDelay_7;
	// System.TimeSpan BestHTTP.SignalR.Transports.PollingTransport::PollTimeout
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___PollTimeout_8;
	// BestHTTP.HTTPRequest BestHTTP.SignalR.Transports.PollingTransport::pollRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___pollRequest_9;

public:
	inline static int32_t get_offset_of_LastPoll_6() { return static_cast<int32_t>(offsetof(PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1, ___LastPoll_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_LastPoll_6() const { return ___LastPoll_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_LastPoll_6() { return &___LastPoll_6; }
	inline void set_LastPoll_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___LastPoll_6 = value;
	}

	inline static int32_t get_offset_of_PollDelay_7() { return static_cast<int32_t>(offsetof(PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1, ___PollDelay_7)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_PollDelay_7() const { return ___PollDelay_7; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_PollDelay_7() { return &___PollDelay_7; }
	inline void set_PollDelay_7(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___PollDelay_7 = value;
	}

	inline static int32_t get_offset_of_PollTimeout_8() { return static_cast<int32_t>(offsetof(PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1, ___PollTimeout_8)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_PollTimeout_8() const { return ___PollTimeout_8; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_PollTimeout_8() { return &___PollTimeout_8; }
	inline void set_PollTimeout_8(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___PollTimeout_8 = value;
	}

	inline static int32_t get_offset_of_pollRequest_9() { return static_cast<int32_t>(offsetof(PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1, ___pollRequest_9)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_pollRequest_9() const { return ___pollRequest_9; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_pollRequest_9() { return &___pollRequest_9; }
	inline void set_pollRequest_9(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___pollRequest_9 = value;
		Il2CppCodeGenWriteBarrier((&___pollRequest_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLLINGTRANSPORT_TE4872E4BE81FD6773440986731BC4B8D8A31F7B1_H
#ifndef SERVERSENTEVENTSTRANSPORT_TC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56_H
#define SERVERSENTEVENTSTRANSPORT_TC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SignalR.Transports.ServerSentEventsTransport
struct  ServerSentEventsTransport_tC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56  : public PostSendTransportBase_t15233AF34294A66CDB5FC191FE191F678F528ADA
{
public:
	// BestHTTP.ServerSentEvents.EventSource BestHTTP.SignalR.Transports.ServerSentEventsTransport::EventSource
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 * ___EventSource_6;

public:
	inline static int32_t get_offset_of_EventSource_6() { return static_cast<int32_t>(offsetof(ServerSentEventsTransport_tC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56, ___EventSource_6)); }
	inline EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 * get_EventSource_6() const { return ___EventSource_6; }
	inline EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 ** get_address_of_EventSource_6() { return &___EventSource_6; }
	inline void set_EventSource_6(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 * value)
	{
		___EventSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___EventSource_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSENTEVENTSTRANSPORT_TC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SAMPLEBASE_TF16A4F4FD56E3EF09F6559FD85B06AAB569347AF_H
#define SAMPLEBASE_TF16A4F4FD56E3EF09F6559FD85B06AAB569347AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.SampleBase
struct  SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String BestHTTP.Examples.Helpers.SampleBase::Category
	String_t* ___Category_4;
	// System.String BestHTTP.Examples.Helpers.SampleBase::DisplayName
	String_t* ___DisplayName_5;
	// System.String BestHTTP.Examples.Helpers.SampleBase::Description
	String_t* ___Description_6;
	// UnityEngine.RuntimePlatform[] BestHTTP.Examples.Helpers.SampleBase::BannedPlatforms
	RuntimePlatformU5BU5D_t67C3F4F02A9005DD5A2992187D47E8C5B8031C36* ___BannedPlatforms_7;
	// BestHTTP.Examples.SampleRoot BestHTTP.Examples.Helpers.SampleBase::sampleSelector
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 * ___sampleSelector_8;

public:
	inline static int32_t get_offset_of_Category_4() { return static_cast<int32_t>(offsetof(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF, ___Category_4)); }
	inline String_t* get_Category_4() const { return ___Category_4; }
	inline String_t** get_address_of_Category_4() { return &___Category_4; }
	inline void set_Category_4(String_t* value)
	{
		___Category_4 = value;
		Il2CppCodeGenWriteBarrier((&___Category_4), value);
	}

	inline static int32_t get_offset_of_DisplayName_5() { return static_cast<int32_t>(offsetof(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF, ___DisplayName_5)); }
	inline String_t* get_DisplayName_5() const { return ___DisplayName_5; }
	inline String_t** get_address_of_DisplayName_5() { return &___DisplayName_5; }
	inline void set_DisplayName_5(String_t* value)
	{
		___DisplayName_5 = value;
		Il2CppCodeGenWriteBarrier((&___DisplayName_5), value);
	}

	inline static int32_t get_offset_of_Description_6() { return static_cast<int32_t>(offsetof(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF, ___Description_6)); }
	inline String_t* get_Description_6() const { return ___Description_6; }
	inline String_t** get_address_of_Description_6() { return &___Description_6; }
	inline void set_Description_6(String_t* value)
	{
		___Description_6 = value;
		Il2CppCodeGenWriteBarrier((&___Description_6), value);
	}

	inline static int32_t get_offset_of_BannedPlatforms_7() { return static_cast<int32_t>(offsetof(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF, ___BannedPlatforms_7)); }
	inline RuntimePlatformU5BU5D_t67C3F4F02A9005DD5A2992187D47E8C5B8031C36* get_BannedPlatforms_7() const { return ___BannedPlatforms_7; }
	inline RuntimePlatformU5BU5D_t67C3F4F02A9005DD5A2992187D47E8C5B8031C36** get_address_of_BannedPlatforms_7() { return &___BannedPlatforms_7; }
	inline void set_BannedPlatforms_7(RuntimePlatformU5BU5D_t67C3F4F02A9005DD5A2992187D47E8C5B8031C36* value)
	{
		___BannedPlatforms_7 = value;
		Il2CppCodeGenWriteBarrier((&___BannedPlatforms_7), value);
	}

	inline static int32_t get_offset_of_sampleSelector_8() { return static_cast<int32_t>(offsetof(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF, ___sampleSelector_8)); }
	inline SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 * get_sampleSelector_8() const { return ___sampleSelector_8; }
	inline SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 ** get_address_of_sampleSelector_8() { return &___sampleSelector_8; }
	inline void set_sampleSelector_8(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 * value)
	{
		___sampleSelector_8 = value;
		Il2CppCodeGenWriteBarrier((&___sampleSelector_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEBASE_TF16A4F4FD56E3EF09F6559FD85B06AAB569347AF_H
#ifndef LINK_T43C0A9D549C4C362AC321A0F71CF833FBD311BEF_H
#define LINK_T43C0A9D549C4C362AC321A0F71CF833FBD311BEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Link
struct  Link_t43C0A9D549C4C362AC321A0F71CF833FBD311BEF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String BestHTTP.Examples.Link::url
	String_t* ___url_4;
	// UnityEngine.Texture2D BestHTTP.Examples.Link::linkSelectCursor
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___linkSelectCursor_5;

public:
	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(Link_t43C0A9D549C4C362AC321A0F71CF833FBD311BEF, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier((&___url_4), value);
	}

	inline static int32_t get_offset_of_linkSelectCursor_5() { return static_cast<int32_t>(offsetof(Link_t43C0A9D549C4C362AC321A0F71CF833FBD311BEF, ___linkSelectCursor_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_linkSelectCursor_5() const { return ___linkSelectCursor_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_linkSelectCursor_5() { return &___linkSelectCursor_5; }
	inline void set_linkSelectCursor_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___linkSelectCursor_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSelectCursor_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T43C0A9D549C4C362AC321A0F71CF833FBD311BEF_H
#ifndef SAMPLEROOT_TF4635187B5A3EE83AAE13A844865FF96D8CC0DA2_H
#define SAMPLEROOT_TF4635187B5A3EE83AAE13A844865FF96D8CC0DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.SampleRoot
struct  SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String BestHTTP.Examples.SampleRoot::BaseURL
	String_t* ___BaseURL_4;
	// System.String BestHTTP.Examples.SampleRoot::CDNUrl
	String_t* ___CDNUrl_5;
	// UnityEngine.UI.Text BestHTTP.Examples.SampleRoot::_pluginVersion
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____pluginVersion_6;
	// UnityEngine.UI.Dropdown BestHTTP.Examples.SampleRoot::_logLevelDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ____logLevelDropdown_7;
	// UnityEngine.UI.Text BestHTTP.Examples.SampleRoot::_proxyLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____proxyLabel_8;
	// UnityEngine.UI.InputField BestHTTP.Examples.SampleRoot::_proxyInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____proxyInputField_9;
	// System.Collections.Generic.List`1<BestHTTP.Examples.Helpers.SampleBase> BestHTTP.Examples.SampleRoot::samples
	List_1_tCA227DE701D55D71B8DB42FDB56C2C68C9BC50AC * ___samples_10;
	// BestHTTP.Examples.Helpers.SampleBase BestHTTP.Examples.SampleRoot::selectedExamplePrefab
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * ___selectedExamplePrefab_11;

public:
	inline static int32_t get_offset_of_BaseURL_4() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ___BaseURL_4)); }
	inline String_t* get_BaseURL_4() const { return ___BaseURL_4; }
	inline String_t** get_address_of_BaseURL_4() { return &___BaseURL_4; }
	inline void set_BaseURL_4(String_t* value)
	{
		___BaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURL_4), value);
	}

	inline static int32_t get_offset_of_CDNUrl_5() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ___CDNUrl_5)); }
	inline String_t* get_CDNUrl_5() const { return ___CDNUrl_5; }
	inline String_t** get_address_of_CDNUrl_5() { return &___CDNUrl_5; }
	inline void set_CDNUrl_5(String_t* value)
	{
		___CDNUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___CDNUrl_5), value);
	}

	inline static int32_t get_offset_of__pluginVersion_6() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ____pluginVersion_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__pluginVersion_6() const { return ____pluginVersion_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__pluginVersion_6() { return &____pluginVersion_6; }
	inline void set__pluginVersion_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____pluginVersion_6 = value;
		Il2CppCodeGenWriteBarrier((&____pluginVersion_6), value);
	}

	inline static int32_t get_offset_of__logLevelDropdown_7() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ____logLevelDropdown_7)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get__logLevelDropdown_7() const { return ____logLevelDropdown_7; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of__logLevelDropdown_7() { return &____logLevelDropdown_7; }
	inline void set__logLevelDropdown_7(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		____logLevelDropdown_7 = value;
		Il2CppCodeGenWriteBarrier((&____logLevelDropdown_7), value);
	}

	inline static int32_t get_offset_of__proxyLabel_8() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ____proxyLabel_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__proxyLabel_8() const { return ____proxyLabel_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__proxyLabel_8() { return &____proxyLabel_8; }
	inline void set__proxyLabel_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____proxyLabel_8 = value;
		Il2CppCodeGenWriteBarrier((&____proxyLabel_8), value);
	}

	inline static int32_t get_offset_of__proxyInputField_9() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ____proxyInputField_9)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__proxyInputField_9() const { return ____proxyInputField_9; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__proxyInputField_9() { return &____proxyInputField_9; }
	inline void set__proxyInputField_9(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____proxyInputField_9 = value;
		Il2CppCodeGenWriteBarrier((&____proxyInputField_9), value);
	}

	inline static int32_t get_offset_of_samples_10() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ___samples_10)); }
	inline List_1_tCA227DE701D55D71B8DB42FDB56C2C68C9BC50AC * get_samples_10() const { return ___samples_10; }
	inline List_1_tCA227DE701D55D71B8DB42FDB56C2C68C9BC50AC ** get_address_of_samples_10() { return &___samples_10; }
	inline void set_samples_10(List_1_tCA227DE701D55D71B8DB42FDB56C2C68C9BC50AC * value)
	{
		___samples_10 = value;
		Il2CppCodeGenWriteBarrier((&___samples_10), value);
	}

	inline static int32_t get_offset_of_selectedExamplePrefab_11() { return static_cast<int32_t>(offsetof(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2, ___selectedExamplePrefab_11)); }
	inline SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * get_selectedExamplePrefab_11() const { return ___selectedExamplePrefab_11; }
	inline SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF ** get_address_of_selectedExamplePrefab_11() { return &___selectedExamplePrefab_11; }
	inline void set_selectedExamplePrefab_11(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * value)
	{
		___selectedExamplePrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___selectedExamplePrefab_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEROOT_TF4635187B5A3EE83AAE13A844865FF96D8CC0DA2_H
#ifndef ASSETBUNDLESAMPLE_T10A940895C8597DDF67F9FE8902C6997DC0CF9CA_H
#define ASSETBUNDLESAMPLE_T10A940895C8597DDF67F9FE8902C6997DC0CF9CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HTTP.AssetBundleSample
struct  AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.HTTP.AssetBundleSample::_path
	String_t* ____path_9;
	// System.String BestHTTP.Examples.HTTP.AssetBundleSample::_assetnameInBundle
	String_t* ____assetnameInBundle_10;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.AssetBundleSample::_statusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____statusText_11;
	// UnityEngine.UI.RawImage BestHTTP.Examples.HTTP.AssetBundleSample::_rawImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ____rawImage_12;
	// UnityEngine.UI.Button BestHTTP.Examples.HTTP.AssetBundleSample::_downloadButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____downloadButton_13;
	// BestHTTP.HTTPRequest BestHTTP.Examples.HTTP.AssetBundleSample::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_14;
	// UnityEngine.AssetBundle BestHTTP.Examples.HTTP.AssetBundleSample::cachedBundle
	AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * ___cachedBundle_15;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__assetnameInBundle_10() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ____assetnameInBundle_10)); }
	inline String_t* get__assetnameInBundle_10() const { return ____assetnameInBundle_10; }
	inline String_t** get_address_of__assetnameInBundle_10() { return &____assetnameInBundle_10; }
	inline void set__assetnameInBundle_10(String_t* value)
	{
		____assetnameInBundle_10 = value;
		Il2CppCodeGenWriteBarrier((&____assetnameInBundle_10), value);
	}

	inline static int32_t get_offset_of__statusText_11() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ____statusText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__statusText_11() const { return ____statusText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__statusText_11() { return &____statusText_11; }
	inline void set__statusText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____statusText_11 = value;
		Il2CppCodeGenWriteBarrier((&____statusText_11), value);
	}

	inline static int32_t get_offset_of__rawImage_12() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ____rawImage_12)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get__rawImage_12() const { return ____rawImage_12; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of__rawImage_12() { return &____rawImage_12; }
	inline void set__rawImage_12(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		____rawImage_12 = value;
		Il2CppCodeGenWriteBarrier((&____rawImage_12), value);
	}

	inline static int32_t get_offset_of__downloadButton_13() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ____downloadButton_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__downloadButton_13() const { return ____downloadButton_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__downloadButton_13() { return &____downloadButton_13; }
	inline void set__downloadButton_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____downloadButton_13 = value;
		Il2CppCodeGenWriteBarrier((&____downloadButton_13), value);
	}

	inline static int32_t get_offset_of_request_14() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ___request_14)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_14() const { return ___request_14; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_14() { return &___request_14; }
	inline void set_request_14(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_14 = value;
		Il2CppCodeGenWriteBarrier((&___request_14), value);
	}

	inline static int32_t get_offset_of_cachedBundle_15() { return static_cast<int32_t>(offsetof(AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA, ___cachedBundle_15)); }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * get_cachedBundle_15() const { return ___cachedBundle_15; }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 ** get_address_of_cachedBundle_15() { return &___cachedBundle_15; }
	inline void set_cachedBundle_15(AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * value)
	{
		___cachedBundle_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedBundle_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLESAMPLE_T10A940895C8597DDF67F9FE8902C6997DC0CF9CA_H
#ifndef STREAMINGSAMPLE_TE158B3FC35669A742CA69C3CD119F9844ECE28DF_H
#define STREAMINGSAMPLE_TE158B3FC35669A742CA69C3CD119F9844ECE28DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HTTP.StreamingSample
struct  StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.HTTP.StreamingSample::_downloadPath
	String_t* ____downloadPath_9;
	// UnityEngine.RectTransform BestHTTP.Examples.HTTP.StreamingSample::_streamingSetupRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____streamingSetupRoot_10;
	// UnityEngine.UI.Slider BestHTTP.Examples.HTTP.StreamingSample::_fragmentSizeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____fragmentSizeSlider_11;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.StreamingSample::_fragmentSizeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____fragmentSizeText_12;
	// UnityEngine.UI.Toggle BestHTTP.Examples.HTTP.StreamingSample::_disableCacheToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____disableCacheToggle_13;
	// UnityEngine.RectTransform BestHTTP.Examples.HTTP.StreamingSample::_reportingRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____reportingRoot_14;
	// UnityEngine.UI.Slider BestHTTP.Examples.HTTP.StreamingSample::_downloadProgressSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____downloadProgressSlider_15;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.StreamingSample::_downloadProgressText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____downloadProgressText_16;
	// UnityEngine.UI.Slider BestHTTP.Examples.HTTP.StreamingSample::_processedDataSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____processedDataSlider_17;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.StreamingSample::_processedDataText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____processedDataText_18;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.StreamingSample::_statusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____statusText_19;
	// UnityEngine.UI.Button BestHTTP.Examples.HTTP.StreamingSample::_startDownload
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____startDownload_20;
	// UnityEngine.UI.Button BestHTTP.Examples.HTTP.StreamingSample::_cancelDownload
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____cancelDownload_21;
	// BestHTTP.HTTPRequest BestHTTP.Examples.HTTP.StreamingSample::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_22;
	// System.Single BestHTTP.Examples.HTTP.StreamingSample::progress
	float ___progress_23;
	// System.Int32 BestHTTP.Examples.HTTP.StreamingSample::fragmentSize
	int32_t ___fragmentSize_24;
	// System.Int64 BestHTTP.Examples.HTTP.StreamingSample::<DownloadLength>k__BackingField
	int64_t ___U3CDownloadLengthU3Ek__BackingField_25;
	// System.Int64 BestHTTP.Examples.HTTP.StreamingSample::<ProcessedBytes>k__BackingField
	int64_t ___U3CProcessedBytesU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of__downloadPath_9() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____downloadPath_9)); }
	inline String_t* get__downloadPath_9() const { return ____downloadPath_9; }
	inline String_t** get_address_of__downloadPath_9() { return &____downloadPath_9; }
	inline void set__downloadPath_9(String_t* value)
	{
		____downloadPath_9 = value;
		Il2CppCodeGenWriteBarrier((&____downloadPath_9), value);
	}

	inline static int32_t get_offset_of__streamingSetupRoot_10() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____streamingSetupRoot_10)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__streamingSetupRoot_10() const { return ____streamingSetupRoot_10; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__streamingSetupRoot_10() { return &____streamingSetupRoot_10; }
	inline void set__streamingSetupRoot_10(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____streamingSetupRoot_10 = value;
		Il2CppCodeGenWriteBarrier((&____streamingSetupRoot_10), value);
	}

	inline static int32_t get_offset_of__fragmentSizeSlider_11() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____fragmentSizeSlider_11)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__fragmentSizeSlider_11() const { return ____fragmentSizeSlider_11; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__fragmentSizeSlider_11() { return &____fragmentSizeSlider_11; }
	inline void set__fragmentSizeSlider_11(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____fragmentSizeSlider_11 = value;
		Il2CppCodeGenWriteBarrier((&____fragmentSizeSlider_11), value);
	}

	inline static int32_t get_offset_of__fragmentSizeText_12() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____fragmentSizeText_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__fragmentSizeText_12() const { return ____fragmentSizeText_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__fragmentSizeText_12() { return &____fragmentSizeText_12; }
	inline void set__fragmentSizeText_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____fragmentSizeText_12 = value;
		Il2CppCodeGenWriteBarrier((&____fragmentSizeText_12), value);
	}

	inline static int32_t get_offset_of__disableCacheToggle_13() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____disableCacheToggle_13)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__disableCacheToggle_13() const { return ____disableCacheToggle_13; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__disableCacheToggle_13() { return &____disableCacheToggle_13; }
	inline void set__disableCacheToggle_13(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____disableCacheToggle_13 = value;
		Il2CppCodeGenWriteBarrier((&____disableCacheToggle_13), value);
	}

	inline static int32_t get_offset_of__reportingRoot_14() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____reportingRoot_14)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__reportingRoot_14() const { return ____reportingRoot_14; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__reportingRoot_14() { return &____reportingRoot_14; }
	inline void set__reportingRoot_14(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____reportingRoot_14 = value;
		Il2CppCodeGenWriteBarrier((&____reportingRoot_14), value);
	}

	inline static int32_t get_offset_of__downloadProgressSlider_15() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____downloadProgressSlider_15)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__downloadProgressSlider_15() const { return ____downloadProgressSlider_15; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__downloadProgressSlider_15() { return &____downloadProgressSlider_15; }
	inline void set__downloadProgressSlider_15(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____downloadProgressSlider_15 = value;
		Il2CppCodeGenWriteBarrier((&____downloadProgressSlider_15), value);
	}

	inline static int32_t get_offset_of__downloadProgressText_16() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____downloadProgressText_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__downloadProgressText_16() const { return ____downloadProgressText_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__downloadProgressText_16() { return &____downloadProgressText_16; }
	inline void set__downloadProgressText_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____downloadProgressText_16 = value;
		Il2CppCodeGenWriteBarrier((&____downloadProgressText_16), value);
	}

	inline static int32_t get_offset_of__processedDataSlider_17() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____processedDataSlider_17)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__processedDataSlider_17() const { return ____processedDataSlider_17; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__processedDataSlider_17() { return &____processedDataSlider_17; }
	inline void set__processedDataSlider_17(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____processedDataSlider_17 = value;
		Il2CppCodeGenWriteBarrier((&____processedDataSlider_17), value);
	}

	inline static int32_t get_offset_of__processedDataText_18() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____processedDataText_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__processedDataText_18() const { return ____processedDataText_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__processedDataText_18() { return &____processedDataText_18; }
	inline void set__processedDataText_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____processedDataText_18 = value;
		Il2CppCodeGenWriteBarrier((&____processedDataText_18), value);
	}

	inline static int32_t get_offset_of__statusText_19() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____statusText_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__statusText_19() const { return ____statusText_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__statusText_19() { return &____statusText_19; }
	inline void set__statusText_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____statusText_19 = value;
		Il2CppCodeGenWriteBarrier((&____statusText_19), value);
	}

	inline static int32_t get_offset_of__startDownload_20() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____startDownload_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__startDownload_20() const { return ____startDownload_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__startDownload_20() { return &____startDownload_20; }
	inline void set__startDownload_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____startDownload_20 = value;
		Il2CppCodeGenWriteBarrier((&____startDownload_20), value);
	}

	inline static int32_t get_offset_of__cancelDownload_21() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ____cancelDownload_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__cancelDownload_21() const { return ____cancelDownload_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__cancelDownload_21() { return &____cancelDownload_21; }
	inline void set__cancelDownload_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____cancelDownload_21 = value;
		Il2CppCodeGenWriteBarrier((&____cancelDownload_21), value);
	}

	inline static int32_t get_offset_of_request_22() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ___request_22)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_22() const { return ___request_22; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_22() { return &___request_22; }
	inline void set_request_22(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_22 = value;
		Il2CppCodeGenWriteBarrier((&___request_22), value);
	}

	inline static int32_t get_offset_of_progress_23() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ___progress_23)); }
	inline float get_progress_23() const { return ___progress_23; }
	inline float* get_address_of_progress_23() { return &___progress_23; }
	inline void set_progress_23(float value)
	{
		___progress_23 = value;
	}

	inline static int32_t get_offset_of_fragmentSize_24() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ___fragmentSize_24)); }
	inline int32_t get_fragmentSize_24() const { return ___fragmentSize_24; }
	inline int32_t* get_address_of_fragmentSize_24() { return &___fragmentSize_24; }
	inline void set_fragmentSize_24(int32_t value)
	{
		___fragmentSize_24 = value;
	}

	inline static int32_t get_offset_of_U3CDownloadLengthU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ___U3CDownloadLengthU3Ek__BackingField_25)); }
	inline int64_t get_U3CDownloadLengthU3Ek__BackingField_25() const { return ___U3CDownloadLengthU3Ek__BackingField_25; }
	inline int64_t* get_address_of_U3CDownloadLengthU3Ek__BackingField_25() { return &___U3CDownloadLengthU3Ek__BackingField_25; }
	inline void set_U3CDownloadLengthU3Ek__BackingField_25(int64_t value)
	{
		___U3CDownloadLengthU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CProcessedBytesU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF, ___U3CProcessedBytesU3Ek__BackingField_26)); }
	inline int64_t get_U3CProcessedBytesU3Ek__BackingField_26() const { return ___U3CProcessedBytesU3Ek__BackingField_26; }
	inline int64_t* get_address_of_U3CProcessedBytesU3Ek__BackingField_26() { return &___U3CProcessedBytesU3Ek__BackingField_26; }
	inline void set_U3CProcessedBytesU3Ek__BackingField_26(int64_t value)
	{
		___U3CProcessedBytesU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGSAMPLE_TE158B3FC35669A742CA69C3CD119F9844ECE28DF_H
#ifndef TEXTUREDOWNLOADSAMPLE_T6DDEAE90B447FF0DE335F4955342668E8901F12D_H
#define TEXTUREDOWNLOADSAMPLE_T6DDEAE90B447FF0DE335F4955342668E8901F12D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HTTP.TextureDownloadSample
struct  TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.HTTP.TextureDownloadSample::_path
	String_t* ____path_9;
	// System.String[] BestHTTP.Examples.HTTP.TextureDownloadSample::_imageNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____imageNames_10;
	// UnityEngine.UI.RawImage[] BestHTTP.Examples.HTTP.TextureDownloadSample::_images
	RawImageU5BU5D_tA6FB181A0441558D4F14E4BB982E772FE437525D* ____images_11;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.TextureDownloadSample::_maxConnectionPerServerLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____maxConnectionPerServerLabel_12;
	// UnityEngine.UI.Text BestHTTP.Examples.HTTP.TextureDownloadSample::_cacheLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____cacheLabel_13;
	// System.Byte BestHTTP.Examples.HTTP.TextureDownloadSample::savedMaxConnectionPerServer
	uint8_t ___savedMaxConnectionPerServer_14;
	// System.Boolean BestHTTP.Examples.HTTP.TextureDownloadSample::allDownloadedFromLocalCache
	bool ___allDownloadedFromLocalCache_15;
	// System.Collections.Generic.List`1<BestHTTP.HTTPRequest> BestHTTP.Examples.HTTP.TextureDownloadSample::activeRequests
	List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * ___activeRequests_16;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__imageNames_10() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ____imageNames_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__imageNames_10() const { return ____imageNames_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__imageNames_10() { return &____imageNames_10; }
	inline void set__imageNames_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____imageNames_10 = value;
		Il2CppCodeGenWriteBarrier((&____imageNames_10), value);
	}

	inline static int32_t get_offset_of__images_11() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ____images_11)); }
	inline RawImageU5BU5D_tA6FB181A0441558D4F14E4BB982E772FE437525D* get__images_11() const { return ____images_11; }
	inline RawImageU5BU5D_tA6FB181A0441558D4F14E4BB982E772FE437525D** get_address_of__images_11() { return &____images_11; }
	inline void set__images_11(RawImageU5BU5D_tA6FB181A0441558D4F14E4BB982E772FE437525D* value)
	{
		____images_11 = value;
		Il2CppCodeGenWriteBarrier((&____images_11), value);
	}

	inline static int32_t get_offset_of__maxConnectionPerServerLabel_12() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ____maxConnectionPerServerLabel_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__maxConnectionPerServerLabel_12() const { return ____maxConnectionPerServerLabel_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__maxConnectionPerServerLabel_12() { return &____maxConnectionPerServerLabel_12; }
	inline void set__maxConnectionPerServerLabel_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____maxConnectionPerServerLabel_12 = value;
		Il2CppCodeGenWriteBarrier((&____maxConnectionPerServerLabel_12), value);
	}

	inline static int32_t get_offset_of__cacheLabel_13() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ____cacheLabel_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__cacheLabel_13() const { return ____cacheLabel_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__cacheLabel_13() { return &____cacheLabel_13; }
	inline void set__cacheLabel_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____cacheLabel_13 = value;
		Il2CppCodeGenWriteBarrier((&____cacheLabel_13), value);
	}

	inline static int32_t get_offset_of_savedMaxConnectionPerServer_14() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ___savedMaxConnectionPerServer_14)); }
	inline uint8_t get_savedMaxConnectionPerServer_14() const { return ___savedMaxConnectionPerServer_14; }
	inline uint8_t* get_address_of_savedMaxConnectionPerServer_14() { return &___savedMaxConnectionPerServer_14; }
	inline void set_savedMaxConnectionPerServer_14(uint8_t value)
	{
		___savedMaxConnectionPerServer_14 = value;
	}

	inline static int32_t get_offset_of_allDownloadedFromLocalCache_15() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ___allDownloadedFromLocalCache_15)); }
	inline bool get_allDownloadedFromLocalCache_15() const { return ___allDownloadedFromLocalCache_15; }
	inline bool* get_address_of_allDownloadedFromLocalCache_15() { return &___allDownloadedFromLocalCache_15; }
	inline void set_allDownloadedFromLocalCache_15(bool value)
	{
		___allDownloadedFromLocalCache_15 = value;
	}

	inline static int32_t get_offset_of_activeRequests_16() { return static_cast<int32_t>(offsetof(TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D, ___activeRequests_16)); }
	inline List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * get_activeRequests_16() const { return ___activeRequests_16; }
	inline List_1_t0E64F6F010761063FA9757A1456A883274512FE8 ** get_address_of_activeRequests_16() { return &___activeRequests_16; }
	inline void set_activeRequests_16(List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * value)
	{
		___activeRequests_16 = value;
		Il2CppCodeGenWriteBarrier((&___activeRequests_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREDOWNLOADSAMPLE_T6DDEAE90B447FF0DE335F4955342668E8901F12D_H
#ifndef HUBWITHAUTHORIZATIONSAMPLE_T3A3AD4A03C7FAD56D0E06F3D9875F777F8915888_H
#define HUBWITHAUTHORIZATIONSAMPLE_T3A3AD4A03C7FAD56D0E06F3D9875F777F8915888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HubWithAuthorizationSample
struct  HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.HubWithAuthorizationSample::_path
	String_t* ____path_9;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.HubWithAuthorizationSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_10;
	// UnityEngine.RectTransform BestHTTP.Examples.HubWithAuthorizationSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_11;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithAuthorizationSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_12;
	// System.Int32 BestHTTP.Examples.HubWithAuthorizationSample::_maxListItemEntries
	int32_t ____maxListItemEntries_13;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithAuthorizationSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_14;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithAuthorizationSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_15;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.HubWithAuthorizationSample::hub
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___hub_16;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__scrollRect_10() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____scrollRect_10)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_10() const { return ____scrollRect_10; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_10() { return &____scrollRect_10; }
	inline void set__scrollRect_10(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_10 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_10), value);
	}

	inline static int32_t get_offset_of__contentRoot_11() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____contentRoot_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_11() const { return ____contentRoot_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_11() { return &____contentRoot_11; }
	inline void set__contentRoot_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_11), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_12() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____listItemPrefab_12)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_12() const { return ____listItemPrefab_12; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_12() { return &____listItemPrefab_12; }
	inline void set__listItemPrefab_12(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_12), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_13() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____maxListItemEntries_13)); }
	inline int32_t get__maxListItemEntries_13() const { return ____maxListItemEntries_13; }
	inline int32_t* get_address_of__maxListItemEntries_13() { return &____maxListItemEntries_13; }
	inline void set__maxListItemEntries_13(int32_t value)
	{
		____maxListItemEntries_13 = value;
	}

	inline static int32_t get_offset_of__connectButton_14() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____connectButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_14() const { return ____connectButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_14() { return &____connectButton_14; }
	inline void set__connectButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_14 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_14), value);
	}

	inline static int32_t get_offset_of__closeButton_15() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ____closeButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_15() const { return ____closeButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_15() { return &____closeButton_15; }
	inline void set__closeButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_15), value);
	}

	inline static int32_t get_offset_of_hub_16() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888, ___hub_16)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_hub_16() const { return ___hub_16; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_hub_16() { return &___hub_16; }
	inline void set_hub_16(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___hub_16 = value;
		Il2CppCodeGenWriteBarrier((&___hub_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUBWITHAUTHORIZATIONSAMPLE_T3A3AD4A03C7FAD56D0E06F3D9875F777F8915888_H
#ifndef HUBWITHPREAUTHORIZATIONSAMPLE_T1D465AB690DEF7153A88B732CB139C4C7963C88B_H
#define HUBWITHPREAUTHORIZATIONSAMPLE_T1D465AB690DEF7153A88B732CB139C4C7963C88B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HubWithPreAuthorizationSample
struct  HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.HubWithPreAuthorizationSample::_hubPath
	String_t* ____hubPath_9;
	// System.String BestHTTP.Examples.HubWithPreAuthorizationSample::_jwtTokenPath
	String_t* ____jwtTokenPath_10;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.HubWithPreAuthorizationSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_11;
	// UnityEngine.RectTransform BestHTTP.Examples.HubWithPreAuthorizationSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_12;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithPreAuthorizationSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_13;
	// System.Int32 BestHTTP.Examples.HubWithPreAuthorizationSample::_maxListItemEntries
	int32_t ____maxListItemEntries_14;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithPreAuthorizationSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_15;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithPreAuthorizationSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_16;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.HubWithPreAuthorizationSample::hub
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___hub_17;

public:
	inline static int32_t get_offset_of__hubPath_9() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____hubPath_9)); }
	inline String_t* get__hubPath_9() const { return ____hubPath_9; }
	inline String_t** get_address_of__hubPath_9() { return &____hubPath_9; }
	inline void set__hubPath_9(String_t* value)
	{
		____hubPath_9 = value;
		Il2CppCodeGenWriteBarrier((&____hubPath_9), value);
	}

	inline static int32_t get_offset_of__jwtTokenPath_10() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____jwtTokenPath_10)); }
	inline String_t* get__jwtTokenPath_10() const { return ____jwtTokenPath_10; }
	inline String_t** get_address_of__jwtTokenPath_10() { return &____jwtTokenPath_10; }
	inline void set__jwtTokenPath_10(String_t* value)
	{
		____jwtTokenPath_10 = value;
		Il2CppCodeGenWriteBarrier((&____jwtTokenPath_10), value);
	}

	inline static int32_t get_offset_of__scrollRect_11() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____scrollRect_11)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_11() const { return ____scrollRect_11; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_11() { return &____scrollRect_11; }
	inline void set__scrollRect_11(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_11 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_11), value);
	}

	inline static int32_t get_offset_of__contentRoot_12() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____contentRoot_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_12() const { return ____contentRoot_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_12() { return &____contentRoot_12; }
	inline void set__contentRoot_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_12 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_12), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_13() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____listItemPrefab_13)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_13() const { return ____listItemPrefab_13; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_13() { return &____listItemPrefab_13; }
	inline void set__listItemPrefab_13(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_13), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_14() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____maxListItemEntries_14)); }
	inline int32_t get__maxListItemEntries_14() const { return ____maxListItemEntries_14; }
	inline int32_t* get_address_of__maxListItemEntries_14() { return &____maxListItemEntries_14; }
	inline void set__maxListItemEntries_14(int32_t value)
	{
		____maxListItemEntries_14 = value;
	}

	inline static int32_t get_offset_of__connectButton_15() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____connectButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_15() const { return ____connectButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_15() { return &____connectButton_15; }
	inline void set__connectButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_15), value);
	}

	inline static int32_t get_offset_of__closeButton_16() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ____closeButton_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_16() const { return ____closeButton_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_16() { return &____closeButton_16; }
	inline void set__closeButton_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_16 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_16), value);
	}

	inline static int32_t get_offset_of_hub_17() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B, ___hub_17)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_hub_17() const { return ___hub_17; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_hub_17() { return &___hub_17; }
	inline void set_hub_17(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___hub_17 = value;
		Il2CppCodeGenWriteBarrier((&___hub_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUBWITHPREAUTHORIZATIONSAMPLE_T1D465AB690DEF7153A88B732CB139C4C7963C88B_H
#ifndef REDIRECTSAMPLE_T47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9_H
#define REDIRECTSAMPLE_T47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.RedirectSample
struct  RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.RedirectSample::_path
	String_t* ____path_9;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.RedirectSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_10;
	// UnityEngine.RectTransform BestHTTP.Examples.RedirectSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_11;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.RedirectSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_12;
	// System.Int32 BestHTTP.Examples.RedirectSample::_maxListItemEntries
	int32_t ____maxListItemEntries_13;
	// UnityEngine.UI.Button BestHTTP.Examples.RedirectSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_14;
	// UnityEngine.UI.Button BestHTTP.Examples.RedirectSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_15;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.RedirectSample::hub
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___hub_16;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__scrollRect_10() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____scrollRect_10)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_10() const { return ____scrollRect_10; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_10() { return &____scrollRect_10; }
	inline void set__scrollRect_10(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_10 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_10), value);
	}

	inline static int32_t get_offset_of__contentRoot_11() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____contentRoot_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_11() const { return ____contentRoot_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_11() { return &____contentRoot_11; }
	inline void set__contentRoot_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_11), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_12() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____listItemPrefab_12)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_12() const { return ____listItemPrefab_12; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_12() { return &____listItemPrefab_12; }
	inline void set__listItemPrefab_12(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_12), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_13() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____maxListItemEntries_13)); }
	inline int32_t get__maxListItemEntries_13() const { return ____maxListItemEntries_13; }
	inline int32_t* get_address_of__maxListItemEntries_13() { return &____maxListItemEntries_13; }
	inline void set__maxListItemEntries_13(int32_t value)
	{
		____maxListItemEntries_13 = value;
	}

	inline static int32_t get_offset_of__connectButton_14() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____connectButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_14() const { return ____connectButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_14() { return &____connectButton_14; }
	inline void set__connectButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_14 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_14), value);
	}

	inline static int32_t get_offset_of__closeButton_15() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ____closeButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_15() const { return ____closeButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_15() { return &____closeButton_15; }
	inline void set__closeButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_15), value);
	}

	inline static int32_t get_offset_of_hub_16() { return static_cast<int32_t>(offsetof(RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9, ___hub_16)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_hub_16() const { return ___hub_16; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_hub_16() { return &___hub_16; }
	inline void set_hub_16(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___hub_16 = value;
		Il2CppCodeGenWriteBarrier((&___hub_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REDIRECTSAMPLE_T47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9_H
#ifndef SIMPLESAMPLE_TCB732229C8FC30289373B05F91698E4FCDC1233B_H
#define SIMPLESAMPLE_TCB732229C8FC30289373B05F91698E4FCDC1233B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.ServerSentEvents.SimpleSample
struct  SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.ServerSentEvents.SimpleSample::_path
	String_t* ____path_9;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.ServerSentEvents.SimpleSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_10;
	// UnityEngine.RectTransform BestHTTP.Examples.ServerSentEvents.SimpleSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_11;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.ServerSentEvents.SimpleSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_12;
	// System.Int32 BestHTTP.Examples.ServerSentEvents.SimpleSample::_maxListItemEntries
	int32_t ____maxListItemEntries_13;
	// UnityEngine.UI.Button BestHTTP.Examples.ServerSentEvents.SimpleSample::_startButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____startButton_14;
	// UnityEngine.UI.Button BestHTTP.Examples.ServerSentEvents.SimpleSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_15;
	// BestHTTP.ServerSentEvents.EventSource BestHTTP.Examples.ServerSentEvents.SimpleSample::eventSource
	EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 * ___eventSource_16;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__scrollRect_10() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____scrollRect_10)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_10() const { return ____scrollRect_10; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_10() { return &____scrollRect_10; }
	inline void set__scrollRect_10(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_10 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_10), value);
	}

	inline static int32_t get_offset_of__contentRoot_11() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____contentRoot_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_11() const { return ____contentRoot_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_11() { return &____contentRoot_11; }
	inline void set__contentRoot_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_11), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_12() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____listItemPrefab_12)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_12() const { return ____listItemPrefab_12; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_12() { return &____listItemPrefab_12; }
	inline void set__listItemPrefab_12(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_12), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_13() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____maxListItemEntries_13)); }
	inline int32_t get__maxListItemEntries_13() const { return ____maxListItemEntries_13; }
	inline int32_t* get_address_of__maxListItemEntries_13() { return &____maxListItemEntries_13; }
	inline void set__maxListItemEntries_13(int32_t value)
	{
		____maxListItemEntries_13 = value;
	}

	inline static int32_t get_offset_of__startButton_14() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____startButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__startButton_14() const { return ____startButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__startButton_14() { return &____startButton_14; }
	inline void set__startButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____startButton_14 = value;
		Il2CppCodeGenWriteBarrier((&____startButton_14), value);
	}

	inline static int32_t get_offset_of__closeButton_15() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ____closeButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_15() const { return ____closeButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_15() { return &____closeButton_15; }
	inline void set__closeButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_15), value);
	}

	inline static int32_t get_offset_of_eventSource_16() { return static_cast<int32_t>(offsetof(SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B, ___eventSource_16)); }
	inline EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 * get_eventSource_16() const { return ___eventSource_16; }
	inline EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 ** get_address_of_eventSource_16() { return &___eventSource_16; }
	inline void set_eventSource_16(EventSource_tC73FE0F185EFDA91DA07E406C7930FA3F8E30CB6 * value)
	{
		___eventSource_16 = value;
		Il2CppCodeGenWriteBarrier((&___eventSource_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESAMPLE_TCB732229C8FC30289373B05F91698E4FCDC1233B_H
#ifndef SOCKETIOCHATSAMPLE_T925E2A4299247749B5E9105B8F868D38C94C47B3_H
#define SOCKETIOCHATSAMPLE_T925E2A4299247749B5E9105B8F868D38C94C47B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.SocketIOChatSample
struct  SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.TimeSpan BestHTTP.Examples.SocketIOChatSample::TYPING_TIMER_LENGTH
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___TYPING_TIMER_LENGTH_9;
	// System.String BestHTTP.Examples.SocketIOChatSample::address
	String_t* ___address_10;
	// UnityEngine.RectTransform BestHTTP.Examples.SocketIOChatSample::_loginRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____loginRoot_11;
	// UnityEngine.UI.InputField BestHTTP.Examples.SocketIOChatSample::_userNameInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____userNameInput_12;
	// UnityEngine.RectTransform BestHTTP.Examples.SocketIOChatSample::_chatRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____chatRoot_13;
	// UnityEngine.UI.Text BestHTTP.Examples.SocketIOChatSample::_participantsText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____participantsText_14;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.SocketIOChatSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_15;
	// UnityEngine.RectTransform BestHTTP.Examples.SocketIOChatSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_16;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.SocketIOChatSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_17;
	// System.Int32 BestHTTP.Examples.SocketIOChatSample::_maxListItemEntries
	int32_t ____maxListItemEntries_18;
	// UnityEngine.UI.Text BestHTTP.Examples.SocketIOChatSample::_typingUsersText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____typingUsersText_19;
	// UnityEngine.UI.InputField BestHTTP.Examples.SocketIOChatSample::_input
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____input_20;
	// UnityEngine.UI.Button BestHTTP.Examples.SocketIOChatSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_21;
	// UnityEngine.UI.Button BestHTTP.Examples.SocketIOChatSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_22;
	// BestHTTP.SocketIO.SocketManager BestHTTP.Examples.SocketIOChatSample::Manager
	SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * ___Manager_23;
	// System.Boolean BestHTTP.Examples.SocketIOChatSample::typing
	bool ___typing_24;
	// System.DateTime BestHTTP.Examples.SocketIOChatSample::lastTypingTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastTypingTime_25;
	// System.Collections.Generic.List`1<System.String> BestHTTP.Examples.SocketIOChatSample::typingUsers
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___typingUsers_26;

public:
	inline static int32_t get_offset_of_TYPING_TIMER_LENGTH_9() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ___TYPING_TIMER_LENGTH_9)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_TYPING_TIMER_LENGTH_9() const { return ___TYPING_TIMER_LENGTH_9; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_TYPING_TIMER_LENGTH_9() { return &___TYPING_TIMER_LENGTH_9; }
	inline void set_TYPING_TIMER_LENGTH_9(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___TYPING_TIMER_LENGTH_9 = value;
	}

	inline static int32_t get_offset_of_address_10() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ___address_10)); }
	inline String_t* get_address_10() const { return ___address_10; }
	inline String_t** get_address_of_address_10() { return &___address_10; }
	inline void set_address_10(String_t* value)
	{
		___address_10 = value;
		Il2CppCodeGenWriteBarrier((&___address_10), value);
	}

	inline static int32_t get_offset_of__loginRoot_11() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____loginRoot_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__loginRoot_11() const { return ____loginRoot_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__loginRoot_11() { return &____loginRoot_11; }
	inline void set__loginRoot_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____loginRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&____loginRoot_11), value);
	}

	inline static int32_t get_offset_of__userNameInput_12() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____userNameInput_12)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__userNameInput_12() const { return ____userNameInput_12; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__userNameInput_12() { return &____userNameInput_12; }
	inline void set__userNameInput_12(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____userNameInput_12 = value;
		Il2CppCodeGenWriteBarrier((&____userNameInput_12), value);
	}

	inline static int32_t get_offset_of__chatRoot_13() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____chatRoot_13)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__chatRoot_13() const { return ____chatRoot_13; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__chatRoot_13() { return &____chatRoot_13; }
	inline void set__chatRoot_13(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____chatRoot_13 = value;
		Il2CppCodeGenWriteBarrier((&____chatRoot_13), value);
	}

	inline static int32_t get_offset_of__participantsText_14() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____participantsText_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__participantsText_14() const { return ____participantsText_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__participantsText_14() { return &____participantsText_14; }
	inline void set__participantsText_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____participantsText_14 = value;
		Il2CppCodeGenWriteBarrier((&____participantsText_14), value);
	}

	inline static int32_t get_offset_of__scrollRect_15() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____scrollRect_15)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_15() const { return ____scrollRect_15; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_15() { return &____scrollRect_15; }
	inline void set__scrollRect_15(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_15 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_15), value);
	}

	inline static int32_t get_offset_of__contentRoot_16() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____contentRoot_16)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_16() const { return ____contentRoot_16; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_16() { return &____contentRoot_16; }
	inline void set__contentRoot_16(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_16 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_16), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_17() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____listItemPrefab_17)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_17() const { return ____listItemPrefab_17; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_17() { return &____listItemPrefab_17; }
	inline void set__listItemPrefab_17(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_17 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_17), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_18() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____maxListItemEntries_18)); }
	inline int32_t get__maxListItemEntries_18() const { return ____maxListItemEntries_18; }
	inline int32_t* get_address_of__maxListItemEntries_18() { return &____maxListItemEntries_18; }
	inline void set__maxListItemEntries_18(int32_t value)
	{
		____maxListItemEntries_18 = value;
	}

	inline static int32_t get_offset_of__typingUsersText_19() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____typingUsersText_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__typingUsersText_19() const { return ____typingUsersText_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__typingUsersText_19() { return &____typingUsersText_19; }
	inline void set__typingUsersText_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____typingUsersText_19 = value;
		Il2CppCodeGenWriteBarrier((&____typingUsersText_19), value);
	}

	inline static int32_t get_offset_of__input_20() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____input_20)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__input_20() const { return ____input_20; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__input_20() { return &____input_20; }
	inline void set__input_20(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____input_20 = value;
		Il2CppCodeGenWriteBarrier((&____input_20), value);
	}

	inline static int32_t get_offset_of__connectButton_21() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____connectButton_21)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_21() const { return ____connectButton_21; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_21() { return &____connectButton_21; }
	inline void set__connectButton_21(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_21 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_21), value);
	}

	inline static int32_t get_offset_of__closeButton_22() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ____closeButton_22)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_22() const { return ____closeButton_22; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_22() { return &____closeButton_22; }
	inline void set__closeButton_22(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_22 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_22), value);
	}

	inline static int32_t get_offset_of_Manager_23() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ___Manager_23)); }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * get_Manager_23() const { return ___Manager_23; }
	inline SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 ** get_address_of_Manager_23() { return &___Manager_23; }
	inline void set_Manager_23(SocketManager_t45E03E3DB4B05E935AA29985865B063BEADC7322 * value)
	{
		___Manager_23 = value;
		Il2CppCodeGenWriteBarrier((&___Manager_23), value);
	}

	inline static int32_t get_offset_of_typing_24() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ___typing_24)); }
	inline bool get_typing_24() const { return ___typing_24; }
	inline bool* get_address_of_typing_24() { return &___typing_24; }
	inline void set_typing_24(bool value)
	{
		___typing_24 = value;
	}

	inline static int32_t get_offset_of_lastTypingTime_25() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ___lastTypingTime_25)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastTypingTime_25() const { return ___lastTypingTime_25; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastTypingTime_25() { return &___lastTypingTime_25; }
	inline void set_lastTypingTime_25(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastTypingTime_25 = value;
	}

	inline static int32_t get_offset_of_typingUsers_26() { return static_cast<int32_t>(offsetof(SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3, ___typingUsers_26)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_typingUsers_26() const { return ___typingUsers_26; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_typingUsers_26() { return &___typingUsers_26; }
	inline void set_typingUsers_26(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___typingUsers_26 = value;
		Il2CppCodeGenWriteBarrier((&___typingUsers_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETIOCHATSAMPLE_T925E2A4299247749B5E9105B8F868D38C94C47B3_H
#ifndef TESTHUBSAMPLE_T1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4_H
#define TESTHUBSAMPLE_T1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.TestHubSample
struct  TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.TestHubSample::_path
	String_t* ____path_9;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.TestHubSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_10;
	// UnityEngine.RectTransform BestHTTP.Examples.TestHubSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_11;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.TestHubSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_12;
	// System.Int32 BestHTTP.Examples.TestHubSample::_maxListItemEntries
	int32_t ____maxListItemEntries_13;
	// UnityEngine.UI.Button BestHTTP.Examples.TestHubSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_14;
	// UnityEngine.UI.Button BestHTTP.Examples.TestHubSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_15;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.TestHubSample::hub
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___hub_16;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__scrollRect_10() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____scrollRect_10)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_10() const { return ____scrollRect_10; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_10() { return &____scrollRect_10; }
	inline void set__scrollRect_10(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_10 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_10), value);
	}

	inline static int32_t get_offset_of__contentRoot_11() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____contentRoot_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_11() const { return ____contentRoot_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_11() { return &____contentRoot_11; }
	inline void set__contentRoot_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_11), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_12() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____listItemPrefab_12)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_12() const { return ____listItemPrefab_12; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_12() { return &____listItemPrefab_12; }
	inline void set__listItemPrefab_12(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_12), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_13() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____maxListItemEntries_13)); }
	inline int32_t get__maxListItemEntries_13() const { return ____maxListItemEntries_13; }
	inline int32_t* get_address_of__maxListItemEntries_13() { return &____maxListItemEntries_13; }
	inline void set__maxListItemEntries_13(int32_t value)
	{
		____maxListItemEntries_13 = value;
	}

	inline static int32_t get_offset_of__connectButton_14() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____connectButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_14() const { return ____connectButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_14() { return &____connectButton_14; }
	inline void set__connectButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_14 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_14), value);
	}

	inline static int32_t get_offset_of__closeButton_15() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ____closeButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_15() const { return ____closeButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_15() { return &____closeButton_15; }
	inline void set__closeButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_15), value);
	}

	inline static int32_t get_offset_of_hub_16() { return static_cast<int32_t>(offsetof(TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4, ___hub_16)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_hub_16() const { return ___hub_16; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_hub_16() { return &___hub_16; }
	inline void set_hub_16(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___hub_16 = value;
		Il2CppCodeGenWriteBarrier((&___hub_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTHUBSAMPLE_T1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4_H
#ifndef UPLOADHUBSAMPLE_TBB874D4366825333F87097C95A5829E3286CA64C_H
#define UPLOADHUBSAMPLE_TBB874D4366825333F87097C95A5829E3286CA64C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.UploadHubSample
struct  UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.UploadHubSample::_path
	String_t* ____path_9;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.UploadHubSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_10;
	// UnityEngine.RectTransform BestHTTP.Examples.UploadHubSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_11;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.UploadHubSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_12;
	// System.Int32 BestHTTP.Examples.UploadHubSample::_maxListItemEntries
	int32_t ____maxListItemEntries_13;
	// UnityEngine.UI.Button BestHTTP.Examples.UploadHubSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_14;
	// UnityEngine.UI.Button BestHTTP.Examples.UploadHubSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_15;
	// System.Single BestHTTP.Examples.UploadHubSample::_yieldWaitTime
	float ____yieldWaitTime_16;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.UploadHubSample::hub
	HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * ___hub_17;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((&____path_9), value);
	}

	inline static int32_t get_offset_of__scrollRect_10() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____scrollRect_10)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_10() const { return ____scrollRect_10; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_10() { return &____scrollRect_10; }
	inline void set__scrollRect_10(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_10 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_10), value);
	}

	inline static int32_t get_offset_of__contentRoot_11() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____contentRoot_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_11() const { return ____contentRoot_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_11() { return &____contentRoot_11; }
	inline void set__contentRoot_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_11 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_11), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_12() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____listItemPrefab_12)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_12() const { return ____listItemPrefab_12; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_12() { return &____listItemPrefab_12; }
	inline void set__listItemPrefab_12(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_12), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_13() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____maxListItemEntries_13)); }
	inline int32_t get__maxListItemEntries_13() const { return ____maxListItemEntries_13; }
	inline int32_t* get_address_of__maxListItemEntries_13() { return &____maxListItemEntries_13; }
	inline void set__maxListItemEntries_13(int32_t value)
	{
		____maxListItemEntries_13 = value;
	}

	inline static int32_t get_offset_of__connectButton_14() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____connectButton_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_14() const { return ____connectButton_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_14() { return &____connectButton_14; }
	inline void set__connectButton_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_14 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_14), value);
	}

	inline static int32_t get_offset_of__closeButton_15() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____closeButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_15() const { return ____closeButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_15() { return &____closeButton_15; }
	inline void set__closeButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_15), value);
	}

	inline static int32_t get_offset_of__yieldWaitTime_16() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ____yieldWaitTime_16)); }
	inline float get__yieldWaitTime_16() const { return ____yieldWaitTime_16; }
	inline float* get_address_of__yieldWaitTime_16() { return &____yieldWaitTime_16; }
	inline void set__yieldWaitTime_16(float value)
	{
		____yieldWaitTime_16 = value;
	}

	inline static int32_t get_offset_of_hub_17() { return static_cast<int32_t>(offsetof(UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C, ___hub_17)); }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * get_hub_17() const { return ___hub_17; }
	inline HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 ** get_address_of_hub_17() { return &___hub_17; }
	inline void set_hub_17(HubConnection_tBC0BBEF230E3B51F7D537916AB544958E3771AE0 * value)
	{
		___hub_17 = value;
		Il2CppCodeGenWriteBarrier((&___hub_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADHUBSAMPLE_TBB874D4366825333F87097C95A5829E3286CA64C_H
#ifndef WEBSOCKETSAMPLE_T71FE0C88220D48712AE265562BAC13EE1FB39819_H
#define WEBSOCKETSAMPLE_T71FE0C88220D48712AE265562BAC13EE1FB39819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Websockets.WebSocketSample
struct  WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819  : public SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF
{
public:
	// System.String BestHTTP.Examples.Websockets.WebSocketSample::address
	String_t* ___address_9;
	// UnityEngine.UI.InputField BestHTTP.Examples.Websockets.WebSocketSample::_input
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____input_10;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.Websockets.WebSocketSample::_scrollRect
	ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * ____scrollRect_11;
	// UnityEngine.RectTransform BestHTTP.Examples.Websockets.WebSocketSample::_contentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____contentRoot_12;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.Websockets.WebSocketSample::_listItemPrefab
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * ____listItemPrefab_13;
	// System.Int32 BestHTTP.Examples.Websockets.WebSocketSample::_maxListItemEntries
	int32_t ____maxListItemEntries_14;
	// UnityEngine.UI.Button BestHTTP.Examples.Websockets.WebSocketSample::_connectButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____connectButton_15;
	// UnityEngine.UI.Button BestHTTP.Examples.Websockets.WebSocketSample::_closeButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeButton_16;
	// BestHTTP.WebSocket.WebSocket BestHTTP.Examples.Websockets.WebSocketSample::webSocket
	WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * ___webSocket_17;

public:
	inline static int32_t get_offset_of_address_9() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ___address_9)); }
	inline String_t* get_address_9() const { return ___address_9; }
	inline String_t** get_address_of_address_9() { return &___address_9; }
	inline void set_address_9(String_t* value)
	{
		___address_9 = value;
		Il2CppCodeGenWriteBarrier((&___address_9), value);
	}

	inline static int32_t get_offset_of__input_10() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____input_10)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__input_10() const { return ____input_10; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__input_10() { return &____input_10; }
	inline void set__input_10(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____input_10 = value;
		Il2CppCodeGenWriteBarrier((&____input_10), value);
	}

	inline static int32_t get_offset_of__scrollRect_11() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____scrollRect_11)); }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * get__scrollRect_11() const { return ____scrollRect_11; }
	inline ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 ** get_address_of__scrollRect_11() { return &____scrollRect_11; }
	inline void set__scrollRect_11(ScrollRect_tAD21D8FB1D33789C39BF4E4CD5CD012D9BD7DD51 * value)
	{
		____scrollRect_11 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_11), value);
	}

	inline static int32_t get_offset_of__contentRoot_12() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____contentRoot_12)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__contentRoot_12() const { return ____contentRoot_12; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__contentRoot_12() { return &____contentRoot_12; }
	inline void set__contentRoot_12(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____contentRoot_12 = value;
		Il2CppCodeGenWriteBarrier((&____contentRoot_12), value);
	}

	inline static int32_t get_offset_of__listItemPrefab_13() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____listItemPrefab_13)); }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * get__listItemPrefab_13() const { return ____listItemPrefab_13; }
	inline TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD ** get_address_of__listItemPrefab_13() { return &____listItemPrefab_13; }
	inline void set__listItemPrefab_13(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD * value)
	{
		____listItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&____listItemPrefab_13), value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_14() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____maxListItemEntries_14)); }
	inline int32_t get__maxListItemEntries_14() const { return ____maxListItemEntries_14; }
	inline int32_t* get_address_of__maxListItemEntries_14() { return &____maxListItemEntries_14; }
	inline void set__maxListItemEntries_14(int32_t value)
	{
		____maxListItemEntries_14 = value;
	}

	inline static int32_t get_offset_of__connectButton_15() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____connectButton_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__connectButton_15() const { return ____connectButton_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__connectButton_15() { return &____connectButton_15; }
	inline void set__connectButton_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____connectButton_15 = value;
		Il2CppCodeGenWriteBarrier((&____connectButton_15), value);
	}

	inline static int32_t get_offset_of__closeButton_16() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ____closeButton_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeButton_16() const { return ____closeButton_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeButton_16() { return &____closeButton_16; }
	inline void set__closeButton_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeButton_16 = value;
		Il2CppCodeGenWriteBarrier((&____closeButton_16), value);
	}

	inline static int32_t get_offset_of_webSocket_17() { return static_cast<int32_t>(offsetof(WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819, ___webSocket_17)); }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * get_webSocket_17() const { return ___webSocket_17; }
	inline WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 ** get_address_of_webSocket_17() { return &___webSocket_17; }
	inline void set_webSocket_17(WebSocket_t9ED526C25EE94964B52B25B5A107A561B35FA717 * value)
	{
		___webSocket_17 = value;
		Il2CppCodeGenWriteBarrier((&___webSocket_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSAMPLE_T71FE0C88220D48712AE265562BAC13EE1FB39819_H
#ifndef RESUMABLESTREAMINGSAMPLE_TB2810A52BD9CEE83156F50E551C51B874CCD122C_H
#define RESUMABLESTREAMINGSAMPLE_TB2810A52BD9CEE83156F50E551C51B874CCD122C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.HTTP.ResumableStreamingSample
struct  ResumableStreamingSample_tB2810A52BD9CEE83156F50E551C51B874CCD122C  : public StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF
{
public:
	// System.Int64 BestHTTP.Examples.HTTP.ResumableStreamingSample::downloadStartedAt
	int64_t ___downloadStartedAt_29;

public:
	inline static int32_t get_offset_of_downloadStartedAt_29() { return static_cast<int32_t>(offsetof(ResumableStreamingSample_tB2810A52BD9CEE83156F50E551C51B874CCD122C, ___downloadStartedAt_29)); }
	inline int64_t get_downloadStartedAt_29() const { return ___downloadStartedAt_29; }
	inline int64_t* get_address_of_downloadStartedAt_29() { return &___downloadStartedAt_29; }
	inline void set_downloadStartedAt_29(int64_t value)
	{
		___downloadStartedAt_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESUMABLESTREAMINGSAMPLE_TB2810A52BD9CEE83156F50E551C51B874CCD122C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900 = { sizeof (WebSocketTransport_tF310BC98528295E21CB31F983F6EAD1F509AF8AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5900[1] = 
{
	WebSocketTransport_tF310BC98528295E21CB31F983F6EAD1F509AF8AC::get_offset_of_webSocket_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901 = { sizeof (Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0)+ sizeof (RuntimeObject), sizeof(Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5901[2] = 
{
	Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Completion_tB9CE55E740004BBEA514FBB007AAE7DD550217E0::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902 = { sizeof (CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4)+ sizeof (RuntimeObject), sizeof(CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5902[3] = 
{
	CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompletionWithResult_t337E6970C4D993161FC8FC16744F0066A43A09D4::get_offset_of_result_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903 = { sizeof (CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266)+ sizeof (RuntimeObject), sizeof(CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5903[3] = 
{
	CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CompletionWithError_tCE8721888BA1E2A55C3FD56A8D0B5E99A204B266::get_offset_of_error_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904 = { sizeof (StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6)+ sizeof (RuntimeObject), sizeof(StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5904[3] = 
{
	StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StreamItemMessage_t282B49E2A2B6BD9E2255E00797CA3F6E018A5AF6::get_offset_of_item_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905 = { sizeof (InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5905[5] = 
{
	InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD::get_offset_of_nonblocking_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD::get_offset_of_target_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InvocationMessage_t3238473A024AC45EC87EE767ED1A99D18E0AC5FD::get_offset_of_arguments_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906 = { sizeof (UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5906[6] = 
{
	UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0::get_offset_of_nonblocking_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0::get_offset_of_target_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0::get_offset_of_arguments_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UploadInvocationMessage_t1BFFF8530DFE61F5157CF86E10098DA1DDED96C0::get_offset_of_streamIds_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907 = { sizeof (CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776)+ sizeof (RuntimeObject), sizeof(CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5907[1] = 
{
	CancelInvocationMessage_tBC65C628A457DA1D0286F514A35E9876137BF776::get_offset_of_invocationId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908 = { sizeof (PingMessage_t3E4FEDA904F050FABD4A5AE5186BF4A655889B09)+ sizeof (RuntimeObject), sizeof(PingMessage_t3E4FEDA904F050FABD4A5AE5186BF4A655889B09 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909 = { sizeof (MessageTypes_t6BB69413A5A279185FA3DBB8F994391217907ADB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5909[9] = 
{
	MessageTypes_t6BB69413A5A279185FA3DBB8F994391217907ADB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910 = { sizeof (Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5910[10] = 
{
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_invocationId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_nonblocking_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_target_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_arguments_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_streamIds_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_item_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_result_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_error_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Message_t50654065D5B3DDD26EDE36BA4A5B2628A6D09BC0::get_offset_of_allowReconnect_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911 = { sizeof (SupportedTransport_tF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5911[2] = 
{
	SupportedTransport_tF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2::get_offset_of_U3CNameU3Ek__BackingField_0(),
	SupportedTransport_tF9EBF59CB96BDD013E91E506F0D0BE3A9409D4F2::get_offset_of_U3CSupportedFormatsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912 = { sizeof (NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5912[6] = 
{
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B::get_offset_of_U3CNegotiateVersionU3Ek__BackingField_0(),
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B::get_offset_of_U3CConnectionTokenU3Ek__BackingField_1(),
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B::get_offset_of_U3CConnectionIdU3Ek__BackingField_2(),
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B::get_offset_of_U3CSupportedTransportsU3Ek__BackingField_3(),
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B::get_offset_of_U3CUrlU3Ek__BackingField_4(),
	NegotiationResult_tB96B0D56165F097275D02A21F454E537B48E505B::get_offset_of_U3CAccessTokenU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913 = { sizeof (LitJsonEncoder_tDF89F668CD476C780B9A83B4E17045037E6B2E9B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914 = { sizeof (U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF), -1, sizeof(U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5914[7] = 
{
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9__2_1_2(),
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9__2_2_3(),
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9__2_3_4(),
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9__2_4_5(),
	U3CU3Ec_t1F5BFD749CFC426615BEF50A2278689AEDCDE5FF_StaticFields::get_offset_of_U3CU3E9__2_5_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915 = { sizeof (MessagePackEncoder_t9ABA3A82D126343FDBC41418BE980839B01646CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916 = { sizeof (MessagePackProtocol_tFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5916[2] = 
{
	MessagePackProtocol_tFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C::get_offset_of_U3CEncoderU3Ek__BackingField_0(),
	MessagePackProtocol_tFA6CADA1227EBE1F379847B6A049BA4AA8BA4C0C::get_offset_of_U3CConnectionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917 = { sizeof (HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5917[3] = 
{
	HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1::get_offset_of_OnAuthenticationSucceded_0(),
	HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1::get_offset_of_OnAuthenticationFailed_1(),
	HeaderAuthenticator_tC6191CCE2F5F990CE2E4C6AE1ED6B7363107DAA1::get_offset_of__credentials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918 = { sizeof (DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5918[3] = 
{
	DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92::get_offset_of_OnAuthenticationSucceded_0(),
	DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92::get_offset_of_OnAuthenticationFailed_1(),
	DefaultAccessTokenAuthenticator_t4ADA7B68C430939C0097FB84C203DA9BE5E45C92::get_offset_of__connection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919 = { sizeof (OnNonHubMessageDelegate_t250CBCD416E95E40350F05C96C2DA5FEEF6269D4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920 = { sizeof (OnConnectedDelegate_t68DFCC14F7BDB9D15D948291EB8FC8F87E6DD78C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921 = { sizeof (OnClosedDelegate_tA2AAABD7251D76ABBDAF038C0D022571460F0F98), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922 = { sizeof (OnErrorDelegate_tE2207C38A5516ADD9C8F2BA34A13BE04C0227AF4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923 = { sizeof (OnStateChanged_tFCA5B3C81CA77039B2F4F3224B489EBE3E5EE249), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924 = { sizeof (OnPrepareRequestDelegate_tBFA22ECEC2EB530032B56BB66F75C79A448A5B81), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926 = { sizeof (ProtocolVersions_tC5746CAEC41DE3223C725582BB0CF80B9B516A68)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5926[4] = 
{
	ProtocolVersions_tC5746CAEC41DE3223C725582BB0CF80B9B516A68::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927 = { sizeof (Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1), -1, sizeof(Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5927[38] = 
{
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1_StaticFields::get_offset_of_DefaultEncoder_0(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CUriU3Ek__BackingField_1(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of__state_2(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CNegotiationResultU3Ek__BackingField_3(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CHubsU3Ek__BackingField_4(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CTransportU3Ek__BackingField_5(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CProtocolU3Ek__BackingField_6(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_additionalQueryParams_7(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CQueryParamsOnlyForHandshakeU3Ek__BackingField_8(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CJsonEncoderU3Ek__BackingField_9(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_10(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CPingIntervalU3Ek__BackingField_11(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CReconnectDelayU3Ek__BackingField_12(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnConnected_13(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnClosed_14(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnError_15(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnReconnecting_16(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnReconnected_17(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnStateChanged_18(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_OnNonHubMessage_19(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_U3CRequestPreparatorU3Ek__BackingField_20(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_ClientMessageCounter_21(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_ClientProtocols_22(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_RequestCounter_23(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_LastReceivedMessage_24(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_GroupsToken_25(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_BufferedMessages_26(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_LastMessageReceivedAt_27(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_ReconnectStartedAt_28(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_ReconnectDelayStartedAt_29(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_ReconnectStarted_30(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_LastPingSentAt_31(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_PingRequest_32(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_TransportConnectionStartedAt_33(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_queryBuilder_34(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_BuiltConnectionData_35(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_BuiltQueryParams_36(),
	Connection_tFFB451BA78ACF8596803F7D83F1CB331728B9BD1::get_offset_of_NextProtocolToTry_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928 = { sizeof (TransportTypes_t7038C136C8533C07764165F2E136BA4986826108)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5928[4] = 
{
	TransportTypes_t7038C136C8533C07764165F2E136BA4986826108::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929 = { sizeof (MessageTypes_tB6017D8CD9729577E871D724D2CCA41A8E12F8EC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5929[8] = 
{
	MessageTypes_tB6017D8CD9729577E871D724D2CCA41A8E12F8EC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930 = { sizeof (ConnectionStates_t7FF3637CF19735B70869B226892AC58373AB82AD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5930[8] = 
{
	ConnectionStates_t7FF3637CF19735B70869B226892AC58373AB82AD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931 = { sizeof (RequestTypes_t28AE22DC99ACA21791F75F66DF18C41A2BD2842A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5931[9] = 
{
	RequestTypes_t28AE22DC99ACA21791F75F66DF18C41A2BD2842A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932 = { sizeof (TransportStates_t2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5932[8] = 
{
	TransportStates_t2E01DE92A68BD02CE0253720BA01EF9B6B59F0F9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933 = { sizeof (NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5933[15] = 
{
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CUrlU3Ek__BackingField_0(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CWebSocketServerUrlU3Ek__BackingField_1(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CConnectionTokenU3Ek__BackingField_2(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CConnectionIdU3Ek__BackingField_3(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CKeepAliveTimeoutU3Ek__BackingField_4(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CDisconnectTimeoutU3Ek__BackingField_5(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CConnectionTimeoutU3Ek__BackingField_6(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CTryWebSocketsU3Ek__BackingField_7(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CProtocolVersionU3Ek__BackingField_8(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CTransportConnectTimeoutU3Ek__BackingField_9(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_U3CLongPollDelayU3Ek__BackingField_10(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_OnReceived_11(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_OnError_12(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_NegotiationRequest_13(),
	NegotiationData_t82AED31F8B3334FD322A6C36DE57DF648E79C340::get_offset_of_Connection_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934 = { sizeof (PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5934[4] = 
{
	PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1::get_offset_of_LastPoll_6(),
	PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1::get_offset_of_PollDelay_7(),
	PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1::get_offset_of_PollTimeout_8(),
	PollingTransport_tE4872E4BE81FD6773440986731BC4B8D8A31F7B1::get_offset_of_pollRequest_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935 = { sizeof (PostSendTransportBase_t15233AF34294A66CDB5FC191FE191F678F528ADA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5935[1] = 
{
	PostSendTransportBase_t15233AF34294A66CDB5FC191FE191F678F528ADA::get_offset_of_sendRequestQueue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936 = { sizeof (ServerSentEventsTransport_tC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5936[1] = 
{
	ServerSentEventsTransport_tC48CFC8915ECB0B7C3CF9E9C7E8A0BFE096A4A56::get_offset_of_EventSource_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937 = { sizeof (U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E), -1, sizeof(U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5937[2] = 
{
	U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t94C8C81AD3F616BE0265443099BB5F60CD36D55E_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938 = { sizeof (OnTransportStateChangedDelegate_tC01E6CC1DC09835144F79F585EAFF829AA2A26AA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939 = { sizeof (TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5939[5] = 
{
	0,
	TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4::get_offset_of_U3CNameU3Ek__BackingField_1(),
	TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4::get_offset_of_U3CConnectionU3Ek__BackingField_2(),
	TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4::get_offset_of__state_3(),
	TransportBase_t2706E8F493C0620140FB786680E39A4897A5E8C4::get_offset_of_OnStateChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940 = { sizeof (WebSocketTransport_t37511257303A7E425243B022ACA4A1355A6BC9BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5940[1] = 
{
	WebSocketTransport_t37511257303A7E425243B022ACA4A1355A6BC9BF::get_offset_of_wSocket_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941 = { sizeof (ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5941[7] = 
{
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_Hub_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_Method_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_Args_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_CallIdx_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_ResultCallback_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_ResultErrorCallback_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ClientMessage_tE29E1EEF2A060152F0CA8F004BE96FC936AE894A::get_offset_of_ProgressCallback_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944 = { sizeof (KeepAliveMessage_t1A1F666B9D06F78ABD01F423864BC12BDDEF4748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945 = { sizeof (MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5945[6] = 
{
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C::get_offset_of_U3CMessageIdU3Ek__BackingField_0(),
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C::get_offset_of_U3CIsInitializationU3Ek__BackingField_1(),
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C::get_offset_of_U3CGroupsTokenU3Ek__BackingField_2(),
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C::get_offset_of_U3CShouldReconnectU3Ek__BackingField_3(),
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C::get_offset_of_U3CPollDelayU3Ek__BackingField_4(),
	MultiMessage_t1735D64E1B76A0DD022E792B348149DF9745ED1C::get_offset_of_U3CDataU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946 = { sizeof (DataMessage_t392FE78D8913A8748A44B8CCD7755A578A4D7689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5946[1] = 
{
	DataMessage_t392FE78D8913A8748A44B8CCD7755A578A4D7689::get_offset_of_U3CDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947 = { sizeof (MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5947[4] = 
{
	MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A::get_offset_of_U3CHubU3Ek__BackingField_0(),
	MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A::get_offset_of_U3CMethodU3Ek__BackingField_1(),
	MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A::get_offset_of_U3CArgumentsU3Ek__BackingField_2(),
	MethodCallMessage_t94D2AD55C50144E14FCA7F3141EB43F872C76E4A::get_offset_of_U3CStateU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948 = { sizeof (ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5948[3] = 
{
	ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA::get_offset_of_U3CReturnValueU3Ek__BackingField_1(),
	ResultMessage_t67714DA10EE2949CB7248F7899D60E61FA96E0BA::get_offset_of_U3CStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949 = { sizeof (FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5949[6] = 
{
	FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D::get_offset_of_U3CIsHubErrorU3Ek__BackingField_1(),
	FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D::get_offset_of_U3CErrorMessageU3Ek__BackingField_2(),
	FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D::get_offset_of_U3CAdditionalDataU3Ek__BackingField_3(),
	FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D::get_offset_of_U3CStackTraceU3Ek__BackingField_4(),
	FailureMessage_t4E01BB179E3081607AF8F6DB62FA445048B61F9D::get_offset_of_U3CStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950 = { sizeof (ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5950[2] = 
{
	ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F::get_offset_of_U3CInvocationIdU3Ek__BackingField_0(),
	ProgressMessage_t6ACA32FD2830024A394E40F050268250F675C72F::get_offset_of_U3CProgressU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951 = { sizeof (OnMethodCallDelegate_t663C97606404E128C8A7F5A61BF72C0DFDF6CFFE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952 = { sizeof (OnMethodCallCallbackDelegate_t2DD6359595F1EFEAE8303FACA30B8A6CF02632D7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953 = { sizeof (OnMethodResultDelegate_t65AAE7A6ACF4EA958CA033903850C88B394D887F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954 = { sizeof (OnMethodFailedDelegate_tD624C74A42D50331E1162C71DE5A804580ACF75B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955 = { sizeof (OnMethodProgressDelegate_tC7EA5FDD863F3D753B500BF1EC76D64BA91C88C4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956 = { sizeof (Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5956[7] = 
{
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_state_1(),
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_OnMethodCall_2(),
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_SentMessages_3(),
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_MethodTable_4(),
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_builder_5(),
	Hub_tC3897DD4D14A474DCB052292CC6F27EC56E8DC21::get_offset_of_U3CBestHTTP_SignalR_Hubs_IHub_ConnectionU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958 = { sizeof (LitJsonEncoder_t27A8B7A59F72316A495C7107A97372EBF6112C43), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959 = { sizeof (DefaultJsonEncoder_t051D42DE1EADACBF41DDE748424CF3D2BEAFCE14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961 = { sizeof (SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5961[9] = 
{
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_U3CAuthUriU3Ek__BackingField_0(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_U3CUserNameU3Ek__BackingField_1(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_U3CPasswordU3Ek__BackingField_2(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_U3CUserRolesU3Ek__BackingField_3(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_U3CIsPreAuthRequiredU3Ek__BackingField_4(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_OnAuthenticationSucceded_5(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_OnAuthenticationFailed_6(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_AuthRequest_7(),
	SampleCookieAuthentication_t468705BBD4859389B548F9111453669A1447A4E7::get_offset_of_Cookie_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962 = { sizeof (U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22), -1, sizeof(U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5962[2] = 
{
	U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t15BF8D3F09D712E9C189AE0B0FD20221F5A1DC22_StaticFields::get_offset_of_U3CU3E9__31_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963 = { sizeof (HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5963[4] = 
{
	HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC::get_offset_of_U3CUserU3Ek__BackingField_0(),
	HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC::get_offset_of_U3CRolesU3Ek__BackingField_1(),
	HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC::get_offset_of_OnAuthenticationSucceded_2(),
	HeaderAuthenticator_tEA81B127FF6A9FD0B188C17BABF11D6EED4F0EBC::get_offset_of_OnAuthenticationFailed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964 = { sizeof (OnAuthenticationSuccededDelegate_t94E342910B973BE0794F8E324EC09A5395354C55), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965 = { sizeof (OnAuthenticationFailedDelegate_t34655CBBF228A876BC4DED628B7744D2F79998E4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967 = { sizeof (GUIHelper_t2545CAEC80C2D121E0B0C5A4FA68E03478032AE0), -1, sizeof(GUIHelper_t2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5967[1] = 
{
	GUIHelper_t2545CAEC80C2D121E0B0C5A4FA68E03478032AE0_StaticFields::get_offset_of_prefixes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968 = { sizeof (U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5968[3] = 
{
	U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3::get_offset_of_U3CU3E1__state_0(),
	U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3::get_offset_of_U3CU3E2__current_1(),
	U3CScrollToBottomU3Ed__4_tECD20D148C2AC6AC7F3FE9F14184E5DB7C7A33F3::get_offset_of_scrollRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969 = { sizeof (Link_t43C0A9D549C4C362AC321A0F71CF833FBD311BEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5969[2] = 
{
	Link_t43C0A9D549C4C362AC321A0F71CF833FBD311BEF::get_offset_of_url_4(),
	Link_t43C0A9D549C4C362AC321A0F71CF833FBD311BEF::get_offset_of_linkSelectCursor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970 = { sizeof (UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5970[6] = 
{
	UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2::get_offset_of_ReadBuffer_5(),
	UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2::get_offset_of_WriteBuffer_6(),
	UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2::get_offset_of_noMoreData_7(),
	UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2::get_offset_of_ARE_8(),
	UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2::get_offset_of_locker_9(),
	UploadStream_t0BBC51A41DEE8384A5AD0ACACEA42866868D6BE2::get_offset_of_U3CNameU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971 = { sizeof (SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5971[8] = 
{
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of_BaseURL_4(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of_CDNUrl_5(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of__pluginVersion_6(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of__logLevelDropdown_7(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of__proxyLabel_8(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of__proxyInputField_9(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of_samples_10(),
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2::get_offset_of_selectedExamplePrefab_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972 = { sizeof (HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5972[8] = 
{
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__path_9(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__scrollRect_10(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__contentRoot_11(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__listItemPrefab_12(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__maxListItemEntries_13(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__connectButton_14(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of__closeButton_15(),
	HubWithAuthorizationSample_t3A3AD4A03C7FAD56D0E06F3D9875F777F8915888::get_offset_of_hub_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973 = { sizeof (HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5973[9] = 
{
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__hubPath_9(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__jwtTokenPath_10(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__scrollRect_11(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__contentRoot_12(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__listItemPrefab_13(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__maxListItemEntries_14(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__connectButton_15(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of__closeButton_16(),
	HubWithPreAuthorizationSample_t1D465AB690DEF7153A88B732CB139C4C7963C88B::get_offset_of_hub_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974 = { sizeof (PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5974[4] = 
{
	PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55::get_offset_of_OnAuthenticationSucceded_0(),
	PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55::get_offset_of_OnAuthenticationFailed_1(),
	PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55::get_offset_of_authenticationUri_2(),
	PreAuthAccessTokenAuthenticator_t2B6BED04F3D46A8AD4A66C45E12F28D32C75CA55::get_offset_of_U3CTokenU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975 = { sizeof (RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5975[8] = 
{
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__path_9(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__scrollRect_10(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__contentRoot_11(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__listItemPrefab_12(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__maxListItemEntries_13(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__connectButton_14(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of__closeButton_15(),
	RedirectSample_t47E9E7D403B5BEF42CFB6FF0BE326C8643824FB9::get_offset_of_hub_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976 = { sizeof (RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5976[3] = 
{
	RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40::get_offset_of_OnAuthenticationSucceded_0(),
	RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40::get_offset_of_OnAuthenticationFailed_1(),
	RedirectLoggerAccessTokenAuthenticator_tDFCFD4E9CB1D51E20A53F73701F6AB761E064A40::get_offset_of__connection_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977 = { sizeof (TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5977[8] = 
{
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__path_9(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__scrollRect_10(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__contentRoot_11(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__listItemPrefab_12(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__maxListItemEntries_13(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__connectButton_14(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of__closeButton_15(),
	TestHubSample_t1CBE32E1EC7E4BE83A92CF4E8F49E01E5166C1D4::get_offset_of_hub_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978 = { sizeof (Person_t375BD9876B7095CBCB2ED9C7606D5839670AFC4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5978[2] = 
{
	Person_t375BD9876B7095CBCB2ED9C7606D5839670AFC4B::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Person_t375BD9876B7095CBCB2ED9C7606D5839670AFC4B::get_offset_of_U3CAgeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979 = { sizeof (Person_tE39ECC4E37B08FB209284AF23FC90E31BD156210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5979[2] = 
{
	Person_tE39ECC4E37B08FB209284AF23FC90E31BD156210::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Person_tE39ECC4E37B08FB209284AF23FC90E31BD156210::get_offset_of_U3CAgeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980 = { sizeof (UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5980[9] = 
{
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__path_9(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__scrollRect_10(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__contentRoot_11(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__listItemPrefab_12(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__maxListItemEntries_13(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__connectButton_14(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__closeButton_15(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of__yieldWaitTime_16(),
	UploadHubSample_tBB874D4366825333F87097C95A5829E3286CA64C::get_offset_of_hub_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981 = { sizeof (U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5981[4] = 
{
	U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5::get_offset_of_U3CU3E1__state_0(),
	U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5::get_offset_of_U3CU3E2__current_1(),
	U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5::get_offset_of_U3CU3E4__this_2(),
	U3CUploadWordU3Ed__15_tA3BCF5E5D958892B470C2D42F8E9CAE2B31E6AF5::get_offset_of_U3CcontrollerU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982 = { sizeof (U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5982[5] = 
{
	U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481::get_offset_of_U3CU3E1__state_0(),
	U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481::get_offset_of_U3CU3E2__current_1(),
	U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481::get_offset_of_U3CU3E4__this_2(),
	U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481::get_offset_of_U3CcontrollerU3E5__2_3(),
	U3CScoreTrackerU3Ed__16_t8FCC86CE102EA8D32DB03262B005B79010301481::get_offset_of_U3CiU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983 = { sizeof (U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5983[6] = 
{
	U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF::get_offset_of_U3CU3E1__state_0(),
	U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF::get_offset_of_U3CU3E2__current_1(),
	U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF::get_offset_of_U3CU3E4__this_2(),
	U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF::get_offset_of_U3CcontrollerU3E5__2_3(),
	U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF::get_offset_of_U3Cplayer1paramU3E5__3_4(),
	U3CScoreTrackerWithParameterChannelsU3Ed__17_t4C2F1E2DF90C13D7BEC8F10C12E4890888841FFF::get_offset_of_U3CiU3E5__4_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984 = { sizeof (U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5984[5] = 
{
	U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6::get_offset_of_U3CU3E1__state_0(),
	U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6::get_offset_of_U3CU3E2__current_1(),
	U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6::get_offset_of_U3CU3E4__this_2(),
	U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6::get_offset_of_U3CcontrollerU3E5__2_3(),
	U3CStreamEchoU3Ed__18_tD87052C49E086FF61F51A00F76239E5ED45B94D6::get_offset_of_U3CiU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985 = { sizeof (U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5985[5] = 
{
	U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE::get_offset_of_U3CU3E1__state_0(),
	U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE::get_offset_of_U3CU3E2__current_1(),
	U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE::get_offset_of_U3CU3E4__this_2(),
	U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE::get_offset_of_U3CcontrollerU3E5__2_3(),
	U3CPersonEchoU3Ed__19_t3DACAD83F1FA2447F5CEB6A4408BD6DC26E52DBE::get_offset_of_U3CiU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986 = { sizeof (SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5986[18] = 
{
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of_TYPING_TIMER_LENGTH_9(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of_address_10(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__loginRoot_11(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__userNameInput_12(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__chatRoot_13(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__participantsText_14(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__scrollRect_15(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__contentRoot_16(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__listItemPrefab_17(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__maxListItemEntries_18(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__typingUsersText_19(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__input_20(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__connectButton_21(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of__closeButton_22(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of_Manager_23(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of_typing_24(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of_lastTypingTime_25(),
	SocketIOChatSample_t925E2A4299247749B5E9105B8F868D38C94C47B3::get_offset_of_typingUsers_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987 = { sizeof (U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3), -1, sizeof(U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5987[2] = 
{
	U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tBC0F31ED9FA52A51EAFFCF5AECA0FFAE0E6E5BD3_StaticFields::get_offset_of_U3CU3E9__22_2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988 = { sizeof (U3CU3Ec__DisplayClass29_0_t16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5988[1] = 
{
	U3CU3Ec__DisplayClass29_0_t16AF3BD60E6AEF3E17BBB5BFF3A99660D062B95B::get_offset_of_username_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989 = { sizeof (U3CU3Ec__DisplayClass30_0_t0E12188FFEB973328C181F3F472B1C901FEA4C96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5989[1] = 
{
	U3CU3Ec__DisplayClass30_0_t0E12188FFEB973328C181F3F472B1C901FEA4C96::get_offset_of_username_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990 = { sizeof (WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5990[9] = 
{
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of_address_9(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__input_10(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__scrollRect_11(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__contentRoot_12(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__listItemPrefab_13(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__maxListItemEntries_14(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__connectButton_15(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of__closeButton_16(),
	WebSocketSample_t71FE0C88220D48712AE265562BAC13EE1FB39819::get_offset_of_webSocket_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991 = { sizeof (SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5991[8] = 
{
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__path_9(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__scrollRect_10(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__contentRoot_11(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__listItemPrefab_12(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__maxListItemEntries_13(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__startButton_14(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of__closeButton_15(),
	SimpleSample_tCB732229C8FC30289373B05F91698E4FCDC1233B::get_offset_of_eventSource_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992 = { sizeof (DateTimeData_tF2E6D00C6143E9DD7B559EBEECAC681C36978CB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5992[2] = 
{
	DateTimeData_tF2E6D00C6143E9DD7B559EBEECAC681C36978CB9::get_offset_of_eventid_0(),
	DateTimeData_tF2E6D00C6143E9DD7B559EBEECAC681C36978CB9::get_offset_of_datetime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993 = { sizeof (AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5993[7] = 
{
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of__path_9(),
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of__assetnameInBundle_10(),
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of__statusText_11(),
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of__rawImage_12(),
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of__downloadButton_13(),
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of_request_14(),
	AssetBundleSample_t10A940895C8597DDF67F9FE8902C6997DC0CF9CA::get_offset_of_cachedBundle_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994 = { sizeof (U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5994[4] = 
{
	U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadAssetBundleU3Ed__10_t2BE920CD4D16269A35366EE4B4ECA1C929CAE3B4::get_offset_of_U3CasyncU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995 = { sizeof (U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5995[5] = 
{
	U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E::get_offset_of_U3CU3E1__state_0(),
	U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E::get_offset_of_U3CU3E2__current_1(),
	U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E::get_offset_of_bundle_2(),
	U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E::get_offset_of_U3CU3E4__this_3(),
	U3CProcessAssetBundleU3Ed__11_t57504E1491C6FDB9E1AF2B1CFB200370C40D758E::get_offset_of_U3CasyncAssetU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996 = { sizeof (ResumableStreamingSample_tB2810A52BD9CEE83156F50E551C51B874CCD122C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5996[3] = 
{
	0,
	0,
	ResumableStreamingSample_tB2810A52BD9CEE83156F50E551C51B874CCD122C::get_offset_of_downloadStartedAt_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997 = { sizeof (StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5997[18] = 
{
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__downloadPath_9(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__streamingSetupRoot_10(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__fragmentSizeSlider_11(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__fragmentSizeText_12(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__disableCacheToggle_13(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__reportingRoot_14(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__downloadProgressSlider_15(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__downloadProgressText_16(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__processedDataSlider_17(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__processedDataText_18(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__statusText_19(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__startDownload_20(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of__cancelDownload_21(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of_request_22(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of_progress_23(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of_fragmentSize_24(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of_U3CDownloadLengthU3Ek__BackingField_25(),
	StreamingSample_tE158B3FC35669A742CA69C3CD119F9844ECE28DF::get_offset_of_U3CProcessedBytesU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998 = { sizeof (TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5998[8] = 
{
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of__path_9(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of__imageNames_10(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of__images_11(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of__maxConnectionPerServerLabel_12(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of__cacheLabel_13(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of_savedMaxConnectionPerServer_14(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of_allDownloadedFromLocalCache_15(),
	TextureDownloadSample_t6DDEAE90B447FF0DE335F4955342668E8901F12D::get_offset_of_activeRequests_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999 = { sizeof (SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5999[5] = 
{
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF::get_offset_of_Category_4(),
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF::get_offset_of_DisplayName_5(),
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF::get_offset_of_Description_6(),
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF::get_offset_of_BannedPlatforms_7(),
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF::get_offset_of_sampleSelector_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
