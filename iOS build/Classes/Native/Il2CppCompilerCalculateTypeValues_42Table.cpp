﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mPoint
struct AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mPoint[]
struct AbstractF2mPointU5BU5D_t8AA783AB482DD67A8EF076E58C54F216D80ED5FB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve
struct SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Point
struct SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve
struct SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Point
struct SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve
struct SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Point
struct SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve
struct SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Point
struct SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve
struct SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Point
struct SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve
struct SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Point
struct SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve
struct SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Point
struct SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve
struct SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Point
struct SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve
struct SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Point
struct SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Point
struct SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t6071986B64066FBF97315592BE971355FA584A39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[]
struct ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECLookupTable
struct ECLookupTable_t0482B45AAFCCE2246FC7DE98A44E84A1B4471A4B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointMap
struct ECPointMap_tA729AFEA3C50E282A5C6F68A2673E94BD5019EE3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint[]
struct ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism
struct ECEndomorphism_t6EB799DC03F118DC4DC962E5E5B94F009F586948;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvEndomorphism
struct GlvEndomorphism_t07DCAEF99F11D895EFA1EDB49606E83AC0D01033;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAParameters
struct GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters
struct GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters
struct ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier
struct ECMultiplier_t3D3844C8A426F0454D623B3F3AA875FE6896AB11;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo
struct WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519/PointExt[]
struct PointExtU5BU5D_t2EBDE0F37FC62D8CC49C3FFAD6680342B019E0BF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448/PointExt[]
struct PointExtU5BU5D_tBC8364B67F5E41214A2C4793AA1E4270B0E0B8C5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField
struct IFiniteField_tFCD218E0A913FF4E2BB6D10F42F69C3979B748D5;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#define ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractECLookupTable
struct  AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#ifndef SECP128R1FIELD_T72C56051F197DDAAFF6840FCB6066C2A947D10AD_H
#define SECP128R1FIELD_T72C56051F197DDAAFF6840FCB6066C2A947D10AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field
struct  SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD  : public RuntimeObject
{
public:

public:
};

struct SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1FIELD_T72C56051F197DDAAFF6840FCB6066C2A947D10AD_H
#ifndef SECP160R1FIELD_T87442400516E736E9BBF16AA666645CA42850617_H
#define SECP160R1FIELD_T87442400516E736E9BBF16AA666645CA42850617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field
struct  SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617  : public RuntimeObject
{
public:

public:
};

struct SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1FIELD_T87442400516E736E9BBF16AA666645CA42850617_H
#ifndef SECP160R2FIELD_TD36C9024533FC4D8B719DCAC402F402497A28934_H
#define SECP160R2FIELD_TD36C9024533FC4D8B719DCAC402F402497A28934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field
struct  SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934  : public RuntimeObject
{
public:

public:
};

struct SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2FIELD_TD36C9024533FC4D8B719DCAC402F402497A28934_H
#ifndef SECP192K1FIELD_T903D799295FED6C4B56C253812CEEDAB4A6441BC_H
#define SECP192K1FIELD_T903D799295FED6C4B56C253812CEEDAB4A6441BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field
struct  SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC  : public RuntimeObject
{
public:

public:
};

struct SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1FIELD_T903D799295FED6C4B56C253812CEEDAB4A6441BC_H
#ifndef SECP192R1FIELD_T595F31D32D6F3772167E931A6BADE81745E5DB2E_H
#define SECP192R1FIELD_T595F31D32D6F3772167E931A6BADE81745E5DB2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field
struct  SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E  : public RuntimeObject
{
public:

public:
};

struct SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1FIELD_T595F31D32D6F3772167E931A6BADE81745E5DB2E_H
#ifndef SECP224K1FIELD_T0A3F8D5DACBF9E71C334853120F79445C9FC917F_H
#define SECP224K1FIELD_T0A3F8D5DACBF9E71C334853120F79445C9FC917F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field
struct  SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F  : public RuntimeObject
{
public:

public:
};

struct SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1FIELD_T0A3F8D5DACBF9E71C334853120F79445C9FC917F_H
#ifndef SECP224R1FIELD_TBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_H
#define SECP224R1FIELD_TBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field
struct  SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E  : public RuntimeObject
{
public:

public:
};

struct SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1FIELD_TBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_H
#ifndef SECP256K1FIELD_T60217AC582790D26D9FAC277CC7D09C365A2B360_H
#define SECP256K1FIELD_T60217AC582790D26D9FAC277CC7D09C365A2B360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field
struct  SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360  : public RuntimeObject
{
public:

public:
};

struct SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1FIELD_T60217AC582790D26D9FAC277CC7D09C365A2B360_H
#ifndef ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#define ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct  ECCurve_t6071986B64066FBF97315592BE971355FA584A39  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_field
	RuntimeObject* ___m_field_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_a
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_a_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_b
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_b_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_order
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_order_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_cofactor
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_cofactor_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_coord
	int32_t ___m_coord_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_endomorphism
	RuntimeObject* ___m_endomorphism_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_multiplier
	RuntimeObject* ___m_multiplier_15;

public:
	inline static int32_t get_offset_of_m_field_8() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_field_8)); }
	inline RuntimeObject* get_m_field_8() const { return ___m_field_8; }
	inline RuntimeObject** get_address_of_m_field_8() { return &___m_field_8; }
	inline void set_m_field_8(RuntimeObject* value)
	{
		___m_field_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_field_8), value);
	}

	inline static int32_t get_offset_of_m_a_9() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_a_9)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_a_9() const { return ___m_a_9; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_a_9() { return &___m_a_9; }
	inline void set_m_a_9(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_a_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_a_9), value);
	}

	inline static int32_t get_offset_of_m_b_10() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_b_10)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_b_10() const { return ___m_b_10; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_b_10() { return &___m_b_10; }
	inline void set_m_b_10(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_b_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_b_10), value);
	}

	inline static int32_t get_offset_of_m_order_11() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_order_11)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_order_11() const { return ___m_order_11; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_order_11() { return &___m_order_11; }
	inline void set_m_order_11(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_order_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_order_11), value);
	}

	inline static int32_t get_offset_of_m_cofactor_12() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_cofactor_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_cofactor_12() const { return ___m_cofactor_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_cofactor_12() { return &___m_cofactor_12; }
	inline void set_m_cofactor_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_cofactor_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_cofactor_12), value);
	}

	inline static int32_t get_offset_of_m_coord_13() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_coord_13)); }
	inline int32_t get_m_coord_13() const { return ___m_coord_13; }
	inline int32_t* get_address_of_m_coord_13() { return &___m_coord_13; }
	inline void set_m_coord_13(int32_t value)
	{
		___m_coord_13 = value;
	}

	inline static int32_t get_offset_of_m_endomorphism_14() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_endomorphism_14)); }
	inline RuntimeObject* get_m_endomorphism_14() const { return ___m_endomorphism_14; }
	inline RuntimeObject** get_address_of_m_endomorphism_14() { return &___m_endomorphism_14; }
	inline void set_m_endomorphism_14(RuntimeObject* value)
	{
		___m_endomorphism_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_14), value);
	}

	inline static int32_t get_offset_of_m_multiplier_15() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_multiplier_15)); }
	inline RuntimeObject* get_m_multiplier_15() const { return ___m_multiplier_15; }
	inline RuntimeObject** get_address_of_m_multiplier_15() { return &___m_multiplier_15; }
	inline void set_m_multiplier_15(RuntimeObject* value)
	{
		___m_multiplier_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_multiplier_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#ifndef ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#define ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct  ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#ifndef ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#define ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct  ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___m_curve_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_x
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_x_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_y
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_y_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_zs
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___m_zs_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_withCompression
	bool ___m_withCompression_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_preCompTable
	RuntimeObject* ___m_preCompTable_6;

public:
	inline static int32_t get_offset_of_m_curve_1() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_curve_1)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_m_curve_1() const { return ___m_curve_1; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_m_curve_1() { return &___m_curve_1; }
	inline void set_m_curve_1(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___m_curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_1), value);
	}

	inline static int32_t get_offset_of_m_x_2() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_x_2)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_x_2() const { return ___m_x_2; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_x_2() { return &___m_x_2; }
	inline void set_m_x_2(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_x_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_x_2), value);
	}

	inline static int32_t get_offset_of_m_y_3() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_y_3)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_y_3() const { return ___m_y_3; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_y_3() { return &___m_y_3; }
	inline void set_m_y_3(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_y_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_y_3), value);
	}

	inline static int32_t get_offset_of_m_zs_4() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_zs_4)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_m_zs_4() const { return ___m_zs_4; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_m_zs_4() { return &___m_zs_4; }
	inline void set_m_zs_4(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___m_zs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_zs_4), value);
	}

	inline static int32_t get_offset_of_m_withCompression_5() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_withCompression_5)); }
	inline bool get_m_withCompression_5() const { return ___m_withCompression_5; }
	inline bool* get_address_of_m_withCompression_5() { return &___m_withCompression_5; }
	inline void set_m_withCompression_5(bool value)
	{
		___m_withCompression_5 = value;
	}

	inline static int32_t get_offset_of_m_preCompTable_6() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_preCompTable_6)); }
	inline RuntimeObject* get_m_preCompTable_6() const { return ___m_preCompTable_6; }
	inline RuntimeObject** get_address_of_m_preCompTable_6() { return &___m_preCompTable_6; }
	inline void set_m_preCompTable_6(RuntimeObject* value)
	{
		___m_preCompTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompTable_6), value);
	}
};

struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::EMPTY_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___EMPTY_ZS_0;

public:
	inline static int32_t get_offset_of_EMPTY_ZS_0() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields, ___EMPTY_ZS_0)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_EMPTY_ZS_0() const { return ___EMPTY_ZS_0; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_EMPTY_ZS_0() { return &___EMPTY_ZS_0; }
	inline void set_EMPTY_ZS_0(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___EMPTY_ZS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_ZS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#ifndef ENDOPRECOMPINFO_TF529412E67F45F2B53F97892259F0D5767B4C927_H
#define ENDOPRECOMPINFO_TF529412E67F45F2B53F97892259F0D5767B4C927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoPreCompInfo
struct  EndoPreCompInfo_tF529412E67F45F2B53F97892259F0D5767B4C927  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoPreCompInfo::m_endomorphism
	RuntimeObject* ___m_endomorphism_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoPreCompInfo::m_mappedPoint
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_mappedPoint_1;

public:
	inline static int32_t get_offset_of_m_endomorphism_0() { return static_cast<int32_t>(offsetof(EndoPreCompInfo_tF529412E67F45F2B53F97892259F0D5767B4C927, ___m_endomorphism_0)); }
	inline RuntimeObject* get_m_endomorphism_0() const { return ___m_endomorphism_0; }
	inline RuntimeObject** get_address_of_m_endomorphism_0() { return &___m_endomorphism_0; }
	inline void set_m_endomorphism_0(RuntimeObject* value)
	{
		___m_endomorphism_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_0), value);
	}

	inline static int32_t get_offset_of_m_mappedPoint_1() { return static_cast<int32_t>(offsetof(EndoPreCompInfo_tF529412E67F45F2B53F97892259F0D5767B4C927, ___m_mappedPoint_1)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_mappedPoint_1() const { return ___m_mappedPoint_1; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_mappedPoint_1() { return &___m_mappedPoint_1; }
	inline void set_m_mappedPoint_1(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_mappedPoint_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_mappedPoint_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDOPRECOMPINFO_TF529412E67F45F2B53F97892259F0D5767B4C927_H
#ifndef ENDOUTILITIES_TE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_H
#define ENDOUTILITIES_TE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoUtilities
struct  EndoUtilities_tE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D  : public RuntimeObject
{
public:

public:
};

struct EndoUtilities_tE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoUtilities::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(EndoUtilities_tE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDOUTILITIES_TE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_H
#ifndef MAPPOINTCALLBACK_TF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041_H
#define MAPPOINTCALLBACK_TF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoUtilities_MapPointCallback
struct  MapPointCallback_tF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoUtilities_MapPointCallback::m_endomorphism
	RuntimeObject* ___m_endomorphism_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.EndoUtilities_MapPointCallback::m_point
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_point_1;

public:
	inline static int32_t get_offset_of_m_endomorphism_0() { return static_cast<int32_t>(offsetof(MapPointCallback_tF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041, ___m_endomorphism_0)); }
	inline RuntimeObject* get_m_endomorphism_0() const { return ___m_endomorphism_0; }
	inline RuntimeObject** get_address_of_m_endomorphism_0() { return &___m_endomorphism_0; }
	inline void set_m_endomorphism_0(RuntimeObject* value)
	{
		___m_endomorphism_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_0), value);
	}

	inline static int32_t get_offset_of_m_point_1() { return static_cast<int32_t>(offsetof(MapPointCallback_tF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041, ___m_point_1)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_point_1() const { return ___m_point_1; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_point_1() { return &___m_point_1; }
	inline void set_m_point_1(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_point_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_point_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPOINTCALLBACK_TF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041_H
#ifndef GLVTYPEAENDOMORPHISM_T5CBCE2BB72B965010603431FF2D55898ED56BFF2_H
#define GLVTYPEAENDOMORPHISM_T5CBCE2BB72B965010603431FF2D55898ED56BFF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAEndomorphism
struct  GlvTypeAEndomorphism_t5CBCE2BB72B965010603431FF2D55898ED56BFF2  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAEndomorphism::m_parameters
	GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A * ___m_parameters_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointMap BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAEndomorphism::m_pointMap
	RuntimeObject* ___m_pointMap_1;

public:
	inline static int32_t get_offset_of_m_parameters_0() { return static_cast<int32_t>(offsetof(GlvTypeAEndomorphism_t5CBCE2BB72B965010603431FF2D55898ED56BFF2, ___m_parameters_0)); }
	inline GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A * get_m_parameters_0() const { return ___m_parameters_0; }
	inline GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A ** get_address_of_m_parameters_0() { return &___m_parameters_0; }
	inline void set_m_parameters_0(GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A * value)
	{
		___m_parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_parameters_0), value);
	}

	inline static int32_t get_offset_of_m_pointMap_1() { return static_cast<int32_t>(offsetof(GlvTypeAEndomorphism_t5CBCE2BB72B965010603431FF2D55898ED56BFF2, ___m_pointMap_1)); }
	inline RuntimeObject* get_m_pointMap_1() const { return ___m_pointMap_1; }
	inline RuntimeObject** get_address_of_m_pointMap_1() { return &___m_pointMap_1; }
	inline void set_m_pointMap_1(RuntimeObject* value)
	{
		___m_pointMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_pointMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVTYPEAENDOMORPHISM_T5CBCE2BB72B965010603431FF2D55898ED56BFF2_H
#ifndef GLVTYPEAPARAMETERS_T583529F26DBA710100F5BBF006F57FC7E417182A_H
#define GLVTYPEAPARAMETERS_T583529F26DBA710100F5BBF006F57FC7E417182A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAParameters
struct  GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAParameters::m_i
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_i_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAParameters::m_lambda
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_lambda_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeAParameters::m_splitParams
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 * ___m_splitParams_2;

public:
	inline static int32_t get_offset_of_m_i_0() { return static_cast<int32_t>(offsetof(GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A, ___m_i_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_i_0() const { return ___m_i_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_i_0() { return &___m_i_0; }
	inline void set_m_i_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_i_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_i_0), value);
	}

	inline static int32_t get_offset_of_m_lambda_1() { return static_cast<int32_t>(offsetof(GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A, ___m_lambda_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_lambda_1() const { return ___m_lambda_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_lambda_1() { return &___m_lambda_1; }
	inline void set_m_lambda_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_lambda_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lambda_1), value);
	}

	inline static int32_t get_offset_of_m_splitParams_2() { return static_cast<int32_t>(offsetof(GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A, ___m_splitParams_2)); }
	inline ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 * get_m_splitParams_2() const { return ___m_splitParams_2; }
	inline ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 ** get_address_of_m_splitParams_2() { return &___m_splitParams_2; }
	inline void set_m_splitParams_2(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 * value)
	{
		___m_splitParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_splitParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVTYPEAPARAMETERS_T583529F26DBA710100F5BBF006F57FC7E417182A_H
#ifndef GLVTYPEBENDOMORPHISM_TCB74CC801D3F994295F2C2B85918F1D61DB8B27C_H
#define GLVTYPEBENDOMORPHISM_TCB74CC801D3F994295F2C2B85918F1D61DB8B27C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism
struct  GlvTypeBEndomorphism_tCB74CC801D3F994295F2C2B85918F1D61DB8B27C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism::m_parameters
	GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42 * ___m_parameters_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointMap BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBEndomorphism::m_pointMap
	RuntimeObject* ___m_pointMap_1;

public:
	inline static int32_t get_offset_of_m_parameters_0() { return static_cast<int32_t>(offsetof(GlvTypeBEndomorphism_tCB74CC801D3F994295F2C2B85918F1D61DB8B27C, ___m_parameters_0)); }
	inline GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42 * get_m_parameters_0() const { return ___m_parameters_0; }
	inline GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42 ** get_address_of_m_parameters_0() { return &___m_parameters_0; }
	inline void set_m_parameters_0(GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42 * value)
	{
		___m_parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_parameters_0), value);
	}

	inline static int32_t get_offset_of_m_pointMap_1() { return static_cast<int32_t>(offsetof(GlvTypeBEndomorphism_tCB74CC801D3F994295F2C2B85918F1D61DB8B27C, ___m_pointMap_1)); }
	inline RuntimeObject* get_m_pointMap_1() const { return ___m_pointMap_1; }
	inline RuntimeObject** get_address_of_m_pointMap_1() { return &___m_pointMap_1; }
	inline void set_m_pointMap_1(RuntimeObject* value)
	{
		___m_pointMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_pointMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVTYPEBENDOMORPHISM_TCB74CC801D3F994295F2C2B85918F1D61DB8B27C_H
#ifndef GLVTYPEBPARAMETERS_T586957DCF9357EEA3C441B8AFC53FC4425333C42_H
#define GLVTYPEBPARAMETERS_T586957DCF9357EEA3C441B8AFC53FC4425333C42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters
struct  GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_beta
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_beta_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_lambda
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_lambda_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvTypeBParameters::m_splitParams
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 * ___m_splitParams_2;

public:
	inline static int32_t get_offset_of_m_beta_0() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42, ___m_beta_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_beta_0() const { return ___m_beta_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_beta_0() { return &___m_beta_0; }
	inline void set_m_beta_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_beta_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_beta_0), value);
	}

	inline static int32_t get_offset_of_m_lambda_1() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42, ___m_lambda_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_lambda_1() const { return ___m_lambda_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_lambda_1() { return &___m_lambda_1; }
	inline void set_m_lambda_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_lambda_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lambda_1), value);
	}

	inline static int32_t get_offset_of_m_splitParams_2() { return static_cast<int32_t>(offsetof(GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42, ___m_splitParams_2)); }
	inline ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 * get_m_splitParams_2() const { return ___m_splitParams_2; }
	inline ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 ** get_address_of_m_splitParams_2() { return &___m_splitParams_2; }
	inline void set_m_splitParams_2(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0 * value)
	{
		___m_splitParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_splitParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVTYPEBPARAMETERS_T586957DCF9357EEA3C441B8AFC53FC4425333C42_H
#ifndef SCALARSPLITPARAMETERS_T4355E06028202D0D63851CBBD2FE50740FF969C0_H
#define SCALARSPLITPARAMETERS_T4355E06028202D0D63851CBBD2FE50740FF969C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters
struct  ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_v1A
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_v1A_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_v1B
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_v1B_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_v2A
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_v2A_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_v2B
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_v2B_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_g1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_g1_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_g2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_g2_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ScalarSplitParameters::m_bits
	int32_t ___m_bits_6;

public:
	inline static int32_t get_offset_of_m_v1A_0() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_v1A_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_v1A_0() const { return ___m_v1A_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_v1A_0() { return &___m_v1A_0; }
	inline void set_m_v1A_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_v1A_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_v1A_0), value);
	}

	inline static int32_t get_offset_of_m_v1B_1() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_v1B_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_v1B_1() const { return ___m_v1B_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_v1B_1() { return &___m_v1B_1; }
	inline void set_m_v1B_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_v1B_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_v1B_1), value);
	}

	inline static int32_t get_offset_of_m_v2A_2() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_v2A_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_v2A_2() const { return ___m_v2A_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_v2A_2() { return &___m_v2A_2; }
	inline void set_m_v2A_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_v2A_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_v2A_2), value);
	}

	inline static int32_t get_offset_of_m_v2B_3() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_v2B_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_v2B_3() const { return ___m_v2B_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_v2B_3() { return &___m_v2B_3; }
	inline void set_m_v2B_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_v2B_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_v2B_3), value);
	}

	inline static int32_t get_offset_of_m_g1_4() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_g1_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_g1_4() const { return ___m_g1_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_g1_4() { return &___m_g1_4; }
	inline void set_m_g1_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_g1_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_g1_4), value);
	}

	inline static int32_t get_offset_of_m_g2_5() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_g2_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_g2_5() const { return ___m_g2_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_g2_5() { return &___m_g2_5; }
	inline void set_m_g2_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_g2_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_g2_5), value);
	}

	inline static int32_t get_offset_of_m_bits_6() { return static_cast<int32_t>(offsetof(ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0, ___m_bits_6)); }
	inline int32_t get_m_bits_6() const { return ___m_bits_6; }
	inline int32_t* get_address_of_m_bits_6() { return &___m_bits_6; }
	inline void set_m_bits_6(int32_t value)
	{
		___m_bits_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALARSPLITPARAMETERS_T4355E06028202D0D63851CBBD2FE50740FF969C0_H
#ifndef ABSTRACTECMULTIPLIER_TBCE555017CAD3C283D05FA0F2BB83B6B396039CB_H
#define ABSTRACTECMULTIPLIER_TBCE555017CAD3C283D05FA0F2BB83B6B396039CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.AbstractECMultiplier
struct  AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTECMULTIPLIER_TBCE555017CAD3C283D05FA0F2BB83B6B396039CB_H
#ifndef FIXEDPOINTPRECOMPINFO_T7E609156248AE55F576F239D23A6D16E39EB5648_H
#define FIXEDPOINTPRECOMPINFO_T7E609156248AE55F576F239D23A6D16E39EB5648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo
struct  FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo::m_offset
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_offset_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECLookupTable BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo::m_lookupTable
	RuntimeObject* ___m_lookupTable_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointPreCompInfo::m_width
	int32_t ___m_width_2;

public:
	inline static int32_t get_offset_of_m_offset_0() { return static_cast<int32_t>(offsetof(FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648, ___m_offset_0)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_offset_0() const { return ___m_offset_0; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_offset_0() { return &___m_offset_0; }
	inline void set_m_offset_0(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_offset_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_offset_0), value);
	}

	inline static int32_t get_offset_of_m_lookupTable_1() { return static_cast<int32_t>(offsetof(FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648, ___m_lookupTable_1)); }
	inline RuntimeObject* get_m_lookupTable_1() const { return ___m_lookupTable_1; }
	inline RuntimeObject** get_address_of_m_lookupTable_1() { return &___m_lookupTable_1; }
	inline void set_m_lookupTable_1(RuntimeObject* value)
	{
		___m_lookupTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_lookupTable_1), value);
	}

	inline static int32_t get_offset_of_m_width_2() { return static_cast<int32_t>(offsetof(FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648, ___m_width_2)); }
	inline int32_t get_m_width_2() const { return ___m_width_2; }
	inline int32_t* get_address_of_m_width_2() { return &___m_width_2; }
	inline void set_m_width_2(int32_t value)
	{
		___m_width_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTPRECOMPINFO_T7E609156248AE55F576F239D23A6D16E39EB5648_H
#ifndef FIXEDPOINTUTILITIES_T127AF864D9B7B2C687EA593A9E2E8EF1478252B1_H
#define FIXEDPOINTUTILITIES_T127AF864D9B7B2C687EA593A9E2E8EF1478252B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointUtilities
struct  FixedPointUtilities_t127AF864D9B7B2C687EA593A9E2E8EF1478252B1  : public RuntimeObject
{
public:

public:
};

struct FixedPointUtilities_t127AF864D9B7B2C687EA593A9E2E8EF1478252B1_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointUtilities::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(FixedPointUtilities_t127AF864D9B7B2C687EA593A9E2E8EF1478252B1_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTUTILITIES_T127AF864D9B7B2C687EA593A9E2E8EF1478252B1_H
#ifndef FIXEDPOINTCALLBACK_T9A8E93848CFAD427DE2365DCFA20B20F88A6DA43_H
#define FIXEDPOINTCALLBACK_T9A8E93848CFAD427DE2365DCFA20B20F88A6DA43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointUtilities_FixedPointCallback
struct  FixedPointCallback_t9A8E93848CFAD427DE2365DCFA20B20F88A6DA43  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointUtilities_FixedPointCallback::m_p
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_p_0;

public:
	inline static int32_t get_offset_of_m_p_0() { return static_cast<int32_t>(offsetof(FixedPointCallback_t9A8E93848CFAD427DE2365DCFA20B20F88A6DA43, ___m_p_0)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_p_0() const { return ___m_p_0; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_p_0() { return &___m_p_0; }
	inline void set_m_p_0(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_p_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_p_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTCALLBACK_T9A8E93848CFAD427DE2365DCFA20B20F88A6DA43_H
#ifndef VALIDITYPRECOMPINFO_TED77416C986FEB8F504F98C74FA731B1AF431246_H
#define VALIDITYPRECOMPINFO_TED77416C986FEB8F504F98C74FA731B1AF431246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ValidityPreCompInfo
struct  ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ValidityPreCompInfo::failed
	bool ___failed_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ValidityPreCompInfo::curveEquationPassed
	bool ___curveEquationPassed_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ValidityPreCompInfo::orderPassed
	bool ___orderPassed_3;

public:
	inline static int32_t get_offset_of_failed_1() { return static_cast<int32_t>(offsetof(ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246, ___failed_1)); }
	inline bool get_failed_1() const { return ___failed_1; }
	inline bool* get_address_of_failed_1() { return &___failed_1; }
	inline void set_failed_1(bool value)
	{
		___failed_1 = value;
	}

	inline static int32_t get_offset_of_curveEquationPassed_2() { return static_cast<int32_t>(offsetof(ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246, ___curveEquationPassed_2)); }
	inline bool get_curveEquationPassed_2() const { return ___curveEquationPassed_2; }
	inline bool* get_address_of_curveEquationPassed_2() { return &___curveEquationPassed_2; }
	inline void set_curveEquationPassed_2(bool value)
	{
		___curveEquationPassed_2 = value;
	}

	inline static int32_t get_offset_of_orderPassed_3() { return static_cast<int32_t>(offsetof(ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246, ___orderPassed_3)); }
	inline bool get_orderPassed_3() const { return ___orderPassed_3; }
	inline bool* get_address_of_orderPassed_3() { return &___orderPassed_3; }
	inline void set_orderPassed_3(bool value)
	{
		___orderPassed_3 = value;
	}
};

struct ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ValidityPreCompInfo::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDITYPRECOMPINFO_TED77416C986FEB8F504F98C74FA731B1AF431246_H
#ifndef WNAFUTILITIES_TB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_H
#define WNAFUTILITIES_TB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities
struct  WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7  : public RuntimeObject
{
public:

public:
};

struct WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::DEFAULT_WINDOW_SIZE_CUTOFFS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DEFAULT_WINDOW_SIZE_CUTOFFS_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::MAX_WIDTH
	int32_t ___MAX_WIDTH_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities::EMPTY_POINTS
	ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* ___EMPTY_POINTS_3;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}

	inline static int32_t get_offset_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1() { return static_cast<int32_t>(offsetof(WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields, ___DEFAULT_WINDOW_SIZE_CUTOFFS_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DEFAULT_WINDOW_SIZE_CUTOFFS_1() const { return ___DEFAULT_WINDOW_SIZE_CUTOFFS_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1() { return &___DEFAULT_WINDOW_SIZE_CUTOFFS_1; }
	inline void set_DEFAULT_WINDOW_SIZE_CUTOFFS_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DEFAULT_WINDOW_SIZE_CUTOFFS_1 = value;
		Il2CppCodeGenWriteBarrier((&___DEFAULT_WINDOW_SIZE_CUTOFFS_1), value);
	}

	inline static int32_t get_offset_of_MAX_WIDTH_2() { return static_cast<int32_t>(offsetof(WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields, ___MAX_WIDTH_2)); }
	inline int32_t get_MAX_WIDTH_2() const { return ___MAX_WIDTH_2; }
	inline int32_t* get_address_of_MAX_WIDTH_2() { return &___MAX_WIDTH_2; }
	inline void set_MAX_WIDTH_2(int32_t value)
	{
		___MAX_WIDTH_2 = value;
	}

	inline static int32_t get_offset_of_EMPTY_POINTS_3() { return static_cast<int32_t>(offsetof(WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields, ___EMPTY_POINTS_3)); }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* get_EMPTY_POINTS_3() const { return ___EMPTY_POINTS_3; }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1** get_address_of_EMPTY_POINTS_3() { return &___EMPTY_POINTS_3; }
	inline void set_EMPTY_POINTS_3(ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* value)
	{
		___EMPTY_POINTS_3 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_POINTS_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WNAFUTILITIES_TB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_H
#ifndef CONFIGUREBASEPOINTCALLBACK_T0EC8B5065F7AF7AD1459086033EB018912F81529_H
#define CONFIGUREBASEPOINTCALLBACK_T0EC8B5065F7AF7AD1459086033EB018912F81529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_ConfigureBasepointCallback
struct  ConfigureBasepointCallback_t0EC8B5065F7AF7AD1459086033EB018912F81529  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_ConfigureBasepointCallback::m_curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___m_curve_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_ConfigureBasepointCallback::m_confWidth
	int32_t ___m_confWidth_1;

public:
	inline static int32_t get_offset_of_m_curve_0() { return static_cast<int32_t>(offsetof(ConfigureBasepointCallback_t0EC8B5065F7AF7AD1459086033EB018912F81529, ___m_curve_0)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_m_curve_0() const { return ___m_curve_0; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_m_curve_0() { return &___m_curve_0; }
	inline void set_m_curve_0(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___m_curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_0), value);
	}

	inline static int32_t get_offset_of_m_confWidth_1() { return static_cast<int32_t>(offsetof(ConfigureBasepointCallback_t0EC8B5065F7AF7AD1459086033EB018912F81529, ___m_confWidth_1)); }
	inline int32_t get_m_confWidth_1() const { return ___m_confWidth_1; }
	inline int32_t* get_address_of_m_confWidth_1() { return &___m_confWidth_1; }
	inline void set_m_confWidth_1(int32_t value)
	{
		___m_confWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREBASEPOINTCALLBACK_T0EC8B5065F7AF7AD1459086033EB018912F81529_H
#ifndef MAPPOINTCALLBACK_TB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436_H
#define MAPPOINTCALLBACK_TB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_MapPointCallback
struct  MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_MapPointCallback::m_infoP
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 * ___m_infoP_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_MapPointCallback::m_includeNegated
	bool ___m_includeNegated_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointMap BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_MapPointCallback::m_pointMap
	RuntimeObject* ___m_pointMap_2;

public:
	inline static int32_t get_offset_of_m_infoP_0() { return static_cast<int32_t>(offsetof(MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436, ___m_infoP_0)); }
	inline WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 * get_m_infoP_0() const { return ___m_infoP_0; }
	inline WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 ** get_address_of_m_infoP_0() { return &___m_infoP_0; }
	inline void set_m_infoP_0(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 * value)
	{
		___m_infoP_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_infoP_0), value);
	}

	inline static int32_t get_offset_of_m_includeNegated_1() { return static_cast<int32_t>(offsetof(MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436, ___m_includeNegated_1)); }
	inline bool get_m_includeNegated_1() const { return ___m_includeNegated_1; }
	inline bool* get_address_of_m_includeNegated_1() { return &___m_includeNegated_1; }
	inline void set_m_includeNegated_1(bool value)
	{
		___m_includeNegated_1 = value;
	}

	inline static int32_t get_offset_of_m_pointMap_2() { return static_cast<int32_t>(offsetof(MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436, ___m_pointMap_2)); }
	inline RuntimeObject* get_m_pointMap_2() const { return ___m_pointMap_2; }
	inline RuntimeObject** get_address_of_m_pointMap_2() { return &___m_pointMap_2; }
	inline void set_m_pointMap_2(RuntimeObject* value)
	{
		___m_pointMap_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_pointMap_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPOINTCALLBACK_TB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436_H
#ifndef PRECOMPUTECALLBACK_TC2DAB06B707A71AC302B4634638668CC051F5D96_H
#define PRECOMPUTECALLBACK_TC2DAB06B707A71AC302B4634638668CC051F5D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeCallback
struct  PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeCallback::m_p
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_p_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeCallback::m_minWidth
	int32_t ___m_minWidth_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeCallback::m_includeNegated
	bool ___m_includeNegated_2;

public:
	inline static int32_t get_offset_of_m_p_0() { return static_cast<int32_t>(offsetof(PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96, ___m_p_0)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_p_0() const { return ___m_p_0; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_p_0() { return &___m_p_0; }
	inline void set_m_p_0(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_p_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_p_0), value);
	}

	inline static int32_t get_offset_of_m_minWidth_1() { return static_cast<int32_t>(offsetof(PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96, ___m_minWidth_1)); }
	inline int32_t get_m_minWidth_1() const { return ___m_minWidth_1; }
	inline int32_t* get_address_of_m_minWidth_1() { return &___m_minWidth_1; }
	inline void set_m_minWidth_1(int32_t value)
	{
		___m_minWidth_1 = value;
	}

	inline static int32_t get_offset_of_m_includeNegated_2() { return static_cast<int32_t>(offsetof(PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96, ___m_includeNegated_2)); }
	inline bool get_m_includeNegated_2() const { return ___m_includeNegated_2; }
	inline bool* get_address_of_m_includeNegated_2() { return &___m_includeNegated_2; }
	inline void set_m_includeNegated_2(bool value)
	{
		___m_includeNegated_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRECOMPUTECALLBACK_TC2DAB06B707A71AC302B4634638668CC051F5D96_H
#ifndef PRECOMPUTEWITHPOINTMAPCALLBACK_T8F9C60DE78DDC59B3968AEB9A095462E92E2C82E_H
#define PRECOMPUTEWITHPOINTMAPCALLBACK_T8F9C60DE78DDC59B3968AEB9A095462E92E2C82E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeWithPointMapCallback
struct  PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeWithPointMapCallback::m_point
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_point_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointMap BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeWithPointMapCallback::m_pointMap
	RuntimeObject* ___m_pointMap_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeWithPointMapCallback::m_fromWNaf
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 * ___m_fromWNaf_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafUtilities_PrecomputeWithPointMapCallback::m_includeNegated
	bool ___m_includeNegated_3;

public:
	inline static int32_t get_offset_of_m_point_0() { return static_cast<int32_t>(offsetof(PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E, ___m_point_0)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_point_0() const { return ___m_point_0; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_point_0() { return &___m_point_0; }
	inline void set_m_point_0(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_point_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_point_0), value);
	}

	inline static int32_t get_offset_of_m_pointMap_1() { return static_cast<int32_t>(offsetof(PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E, ___m_pointMap_1)); }
	inline RuntimeObject* get_m_pointMap_1() const { return ___m_pointMap_1; }
	inline RuntimeObject** get_address_of_m_pointMap_1() { return &___m_pointMap_1; }
	inline void set_m_pointMap_1(RuntimeObject* value)
	{
		___m_pointMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_pointMap_1), value);
	}

	inline static int32_t get_offset_of_m_fromWNaf_2() { return static_cast<int32_t>(offsetof(PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E, ___m_fromWNaf_2)); }
	inline WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 * get_m_fromWNaf_2() const { return ___m_fromWNaf_2; }
	inline WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 ** get_address_of_m_fromWNaf_2() { return &___m_fromWNaf_2; }
	inline void set_m_fromWNaf_2(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914 * value)
	{
		___m_fromWNaf_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fromWNaf_2), value);
	}

	inline static int32_t get_offset_of_m_includeNegated_3() { return static_cast<int32_t>(offsetof(PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E, ___m_includeNegated_3)); }
	inline bool get_m_includeNegated_3() const { return ___m_includeNegated_3; }
	inline bool* get_address_of_m_includeNegated_3() { return &___m_includeNegated_3; }
	inline void set_m_includeNegated_3(bool value)
	{
		___m_includeNegated_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRECOMPUTEWITHPOINTMAPCALLBACK_T8F9C60DE78DDC59B3968AEB9A095462E92E2C82E_H
#ifndef WTAUNAFCALLBACK_TF14D451006BBA0BF493C218CD55F30C117EB690C_H
#define WTAUNAFCALLBACK_TF14D451006BBA0BF493C218CD55F30C117EB690C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier_WTauNafCallback
struct  WTauNafCallback_tF14D451006BBA0BF493C218CD55F30C117EB690C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier_WTauNafCallback::m_p
	AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF * ___m_p_0;
	// System.SByte BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier_WTauNafCallback::m_a
	int8_t ___m_a_1;

public:
	inline static int32_t get_offset_of_m_p_0() { return static_cast<int32_t>(offsetof(WTauNafCallback_tF14D451006BBA0BF493C218CD55F30C117EB690C, ___m_p_0)); }
	inline AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF * get_m_p_0() const { return ___m_p_0; }
	inline AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF ** get_address_of_m_p_0() { return &___m_p_0; }
	inline void set_m_p_0(AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF * value)
	{
		___m_p_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_p_0), value);
	}

	inline static int32_t get_offset_of_m_a_1() { return static_cast<int32_t>(offsetof(WTauNafCallback_tF14D451006BBA0BF493C218CD55F30C117EB690C, ___m_a_1)); }
	inline int8_t get_m_a_1() const { return ___m_a_1; }
	inline int8_t* get_address_of_m_a_1() { return &___m_a_1; }
	inline void set_m_a_1(int8_t value)
	{
		___m_a_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WTAUNAFCALLBACK_TF14D451006BBA0BF493C218CD55F30C117EB690C_H
#ifndef WTAUNAFPRECOMPINFO_T00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425_H
#define WTAUNAFPRECOMPINFO_T00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafPreCompInfo
struct  WTauNafPreCompInfo_t00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mPoint[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafPreCompInfo::m_preComp
	AbstractF2mPointU5BU5D_t8AA783AB482DD67A8EF076E58C54F216D80ED5FB* ___m_preComp_0;

public:
	inline static int32_t get_offset_of_m_preComp_0() { return static_cast<int32_t>(offsetof(WTauNafPreCompInfo_t00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425, ___m_preComp_0)); }
	inline AbstractF2mPointU5BU5D_t8AA783AB482DD67A8EF076E58C54F216D80ED5FB* get_m_preComp_0() const { return ___m_preComp_0; }
	inline AbstractF2mPointU5BU5D_t8AA783AB482DD67A8EF076E58C54F216D80ED5FB** get_address_of_m_preComp_0() { return &___m_preComp_0; }
	inline void set_m_preComp_0(AbstractF2mPointU5BU5D_t8AA783AB482DD67A8EF076E58C54F216D80ED5FB* value)
	{
		___m_preComp_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_preComp_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WTAUNAFPRECOMPINFO_T00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425_H
#ifndef X25519_T86CC670092AF75CAA36A1BC881B3A08E26375639_H
#define X25519_T86CC670092AF75CAA36A1BC881B3A08E26375639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc7748.X25519
struct  X25519_t86CC670092AF75CAA36A1BC881B3A08E26375639  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519_T86CC670092AF75CAA36A1BC881B3A08E26375639_H
#ifndef X25519FIELD_T4D0E8E0830B57A0DEE4469E5741A182E22A7D359_H
#define X25519FIELD_T4D0E8E0830B57A0DEE4469E5741A182E22A7D359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc7748.X25519Field
struct  X25519Field_t4D0E8E0830B57A0DEE4469E5741A182E22A7D359  : public RuntimeObject
{
public:

public:
};

struct X25519Field_t4D0E8E0830B57A0DEE4469E5741A182E22A7D359_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc7748.X25519Field::RootNegOne
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___RootNegOne_4;

public:
	inline static int32_t get_offset_of_RootNegOne_4() { return static_cast<int32_t>(offsetof(X25519Field_t4D0E8E0830B57A0DEE4469E5741A182E22A7D359_StaticFields, ___RootNegOne_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_RootNegOne_4() const { return ___RootNegOne_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_RootNegOne_4() { return &___RootNegOne_4; }
	inline void set_RootNegOne_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___RootNegOne_4 = value;
		Il2CppCodeGenWriteBarrier((&___RootNegOne_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519FIELD_T4D0E8E0830B57A0DEE4469E5741A182E22A7D359_H
#ifndef X448_T497460387E139010D296F87FD309BC8E5A611682_H
#define X448_T497460387E139010D296F87FD309BC8E5A611682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc7748.X448
struct  X448_t497460387E139010D296F87FD309BC8E5A611682  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448_T497460387E139010D296F87FD309BC8E5A611682_H
#ifndef X448FIELD_T3FB712418037C22724F8D1D790A4DF93FAE3D4A9_H
#define X448FIELD_T3FB712418037C22724F8D1D790A4DF93FAE3D4A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc7748.X448Field
struct  X448Field_t3FB712418037C22724F8D1D790A4DF93FAE3D4A9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448FIELD_T3FB712418037C22724F8D1D790A4DF93FAE3D4A9_H
#ifndef ED25519_T572650320F5BF60E98AB3ABD1C0386804FCF773A_H
#define ED25519_T572650320F5BF60E98AB3ABD1C0386804FCF773A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519
struct  Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A  : public RuntimeObject
{
public:

public:
};

struct Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::PrehashSize
	int32_t ___PrehashSize_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::PublicKeySize
	int32_t ___PublicKeySize_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::SecretKeySize
	int32_t ___SecretKeySize_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::SignatureSize
	int32_t ___SignatureSize_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::Dom2Prefix
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Dom2Prefix_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_10;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::L
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___L_11;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::B_x
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___B_x_17;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::B_y
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___B_y_18;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::C_d
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___C_d_19;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::C_d2
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___C_d2_20;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::C_d4
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___C_d4_21;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::precompLock
	RuntimeObject * ___precompLock_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointExt[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::precompBaseTable
	PointExtU5BU5D_t2EBDE0F37FC62D8CC49C3FFAD6680342B019E0BF* ___precompBaseTable_29;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519::precompBase
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___precompBase_30;

public:
	inline static int32_t get_offset_of_PrehashSize_5() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___PrehashSize_5)); }
	inline int32_t get_PrehashSize_5() const { return ___PrehashSize_5; }
	inline int32_t* get_address_of_PrehashSize_5() { return &___PrehashSize_5; }
	inline void set_PrehashSize_5(int32_t value)
	{
		___PrehashSize_5 = value;
	}

	inline static int32_t get_offset_of_PublicKeySize_6() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___PublicKeySize_6)); }
	inline int32_t get_PublicKeySize_6() const { return ___PublicKeySize_6; }
	inline int32_t* get_address_of_PublicKeySize_6() { return &___PublicKeySize_6; }
	inline void set_PublicKeySize_6(int32_t value)
	{
		___PublicKeySize_6 = value;
	}

	inline static int32_t get_offset_of_SecretKeySize_7() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___SecretKeySize_7)); }
	inline int32_t get_SecretKeySize_7() const { return ___SecretKeySize_7; }
	inline int32_t* get_address_of_SecretKeySize_7() { return &___SecretKeySize_7; }
	inline void set_SecretKeySize_7(int32_t value)
	{
		___SecretKeySize_7 = value;
	}

	inline static int32_t get_offset_of_SignatureSize_8() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___SignatureSize_8)); }
	inline int32_t get_SignatureSize_8() const { return ___SignatureSize_8; }
	inline int32_t* get_address_of_SignatureSize_8() { return &___SignatureSize_8; }
	inline void set_SignatureSize_8(int32_t value)
	{
		___SignatureSize_8 = value;
	}

	inline static int32_t get_offset_of_Dom2Prefix_9() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___Dom2Prefix_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Dom2Prefix_9() const { return ___Dom2Prefix_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Dom2Prefix_9() { return &___Dom2Prefix_9; }
	inline void set_Dom2Prefix_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Dom2Prefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___Dom2Prefix_9), value);
	}

	inline static int32_t get_offset_of_P_10() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___P_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_10() const { return ___P_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_10() { return &___P_10; }
	inline void set_P_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_10 = value;
		Il2CppCodeGenWriteBarrier((&___P_10), value);
	}

	inline static int32_t get_offset_of_L_11() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___L_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_L_11() const { return ___L_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_L_11() { return &___L_11; }
	inline void set_L_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___L_11 = value;
		Il2CppCodeGenWriteBarrier((&___L_11), value);
	}

	inline static int32_t get_offset_of_B_x_17() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___B_x_17)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_B_x_17() const { return ___B_x_17; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_B_x_17() { return &___B_x_17; }
	inline void set_B_x_17(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___B_x_17 = value;
		Il2CppCodeGenWriteBarrier((&___B_x_17), value);
	}

	inline static int32_t get_offset_of_B_y_18() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___B_y_18)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_B_y_18() const { return ___B_y_18; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_B_y_18() { return &___B_y_18; }
	inline void set_B_y_18(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___B_y_18 = value;
		Il2CppCodeGenWriteBarrier((&___B_y_18), value);
	}

	inline static int32_t get_offset_of_C_d_19() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___C_d_19)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_C_d_19() const { return ___C_d_19; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_C_d_19() { return &___C_d_19; }
	inline void set_C_d_19(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___C_d_19 = value;
		Il2CppCodeGenWriteBarrier((&___C_d_19), value);
	}

	inline static int32_t get_offset_of_C_d2_20() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___C_d2_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_C_d2_20() const { return ___C_d2_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_C_d2_20() { return &___C_d2_20; }
	inline void set_C_d2_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___C_d2_20 = value;
		Il2CppCodeGenWriteBarrier((&___C_d2_20), value);
	}

	inline static int32_t get_offset_of_C_d4_21() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___C_d4_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_C_d4_21() const { return ___C_d4_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_C_d4_21() { return &___C_d4_21; }
	inline void set_C_d4_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___C_d4_21 = value;
		Il2CppCodeGenWriteBarrier((&___C_d4_21), value);
	}

	inline static int32_t get_offset_of_precompLock_28() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___precompLock_28)); }
	inline RuntimeObject * get_precompLock_28() const { return ___precompLock_28; }
	inline RuntimeObject ** get_address_of_precompLock_28() { return &___precompLock_28; }
	inline void set_precompLock_28(RuntimeObject * value)
	{
		___precompLock_28 = value;
		Il2CppCodeGenWriteBarrier((&___precompLock_28), value);
	}

	inline static int32_t get_offset_of_precompBaseTable_29() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___precompBaseTable_29)); }
	inline PointExtU5BU5D_t2EBDE0F37FC62D8CC49C3FFAD6680342B019E0BF* get_precompBaseTable_29() const { return ___precompBaseTable_29; }
	inline PointExtU5BU5D_t2EBDE0F37FC62D8CC49C3FFAD6680342B019E0BF** get_address_of_precompBaseTable_29() { return &___precompBaseTable_29; }
	inline void set_precompBaseTable_29(PointExtU5BU5D_t2EBDE0F37FC62D8CC49C3FFAD6680342B019E0BF* value)
	{
		___precompBaseTable_29 = value;
		Il2CppCodeGenWriteBarrier((&___precompBaseTable_29), value);
	}

	inline static int32_t get_offset_of_precompBase_30() { return static_cast<int32_t>(offsetof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields, ___precompBase_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_precompBase_30() const { return ___precompBase_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_precompBase_30() { return &___precompBase_30; }
	inline void set_precompBase_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___precompBase_30 = value;
		Il2CppCodeGenWriteBarrier((&___precompBase_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519_T572650320F5BF60E98AB3ABD1C0386804FCF773A_H
#ifndef POINTACCUM_TCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91_H
#define POINTACCUM_TCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointAccum
struct  PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointAccum::x
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___x_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointAccum::y
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___y_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointAccum::z
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___z_2;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointAccum::u
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___u_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointAccum::v
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___v_4;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91, ___x_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_x_0() const { return ___x_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91, ___y_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_y_1() const { return ___y_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___y_1 = value;
		Il2CppCodeGenWriteBarrier((&___y_1), value);
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91, ___z_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_z_2() const { return ___z_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___z_2 = value;
		Il2CppCodeGenWriteBarrier((&___z_2), value);
	}

	inline static int32_t get_offset_of_u_3() { return static_cast<int32_t>(offsetof(PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91, ___u_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_u_3() const { return ___u_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_u_3() { return &___u_3; }
	inline void set_u_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___u_3 = value;
		Il2CppCodeGenWriteBarrier((&___u_3), value);
	}

	inline static int32_t get_offset_of_v_4() { return static_cast<int32_t>(offsetof(PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91, ___v_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_v_4() const { return ___v_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_v_4() { return &___v_4; }
	inline void set_v_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___v_4 = value;
		Il2CppCodeGenWriteBarrier((&___v_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTACCUM_TCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91_H
#ifndef POINTEXT_T1717FE5F6B65CC98C75C4996BEC310D000C3B0EC_H
#define POINTEXT_T1717FE5F6B65CC98C75C4996BEC310D000C3B0EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointExt
struct  PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointExt::x
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___x_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointExt::y
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___y_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointExt::z
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___z_2;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointExt::t
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___t_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC, ___x_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_x_0() const { return ___x_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC, ___y_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_y_1() const { return ___y_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___y_1 = value;
		Il2CppCodeGenWriteBarrier((&___y_1), value);
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC, ___z_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_z_2() const { return ___z_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___z_2 = value;
		Il2CppCodeGenWriteBarrier((&___z_2), value);
	}

	inline static int32_t get_offset_of_t_3() { return static_cast<int32_t>(offsetof(PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC, ___t_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_t_3() const { return ___t_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_t_3() { return &___t_3; }
	inline void set_t_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___t_3 = value;
		Il2CppCodeGenWriteBarrier((&___t_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEXT_T1717FE5F6B65CC98C75C4996BEC310D000C3B0EC_H
#ifndef POINTPRECOMP_T75BB8642311CC417751D91A92F8C30B6E7BFD315_H
#define POINTPRECOMP_T75BB8642311CC417751D91A92F8C30B6E7BFD315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointPrecomp
struct  PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointPrecomp::ypx_h
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ypx_h_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointPrecomp::ymx_h
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ymx_h_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_PointPrecomp::xyd
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___xyd_2;

public:
	inline static int32_t get_offset_of_ypx_h_0() { return static_cast<int32_t>(offsetof(PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315, ___ypx_h_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ypx_h_0() const { return ___ypx_h_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ypx_h_0() { return &___ypx_h_0; }
	inline void set_ypx_h_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ypx_h_0 = value;
		Il2CppCodeGenWriteBarrier((&___ypx_h_0), value);
	}

	inline static int32_t get_offset_of_ymx_h_1() { return static_cast<int32_t>(offsetof(PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315, ___ymx_h_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ymx_h_1() const { return ___ymx_h_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ymx_h_1() { return &___ymx_h_1; }
	inline void set_ymx_h_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ymx_h_1 = value;
		Il2CppCodeGenWriteBarrier((&___ymx_h_1), value);
	}

	inline static int32_t get_offset_of_xyd_2() { return static_cast<int32_t>(offsetof(PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315, ___xyd_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_xyd_2() const { return ___xyd_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_xyd_2() { return &___xyd_2; }
	inline void set_xyd_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___xyd_2 = value;
		Il2CppCodeGenWriteBarrier((&___xyd_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTPRECOMP_T75BB8642311CC417751D91A92F8C30B6E7BFD315_H
#ifndef ED448_TDF248BBC81831AAA9D8FE5B14D62572C8E663566_H
#define ED448_TDF248BBC81831AAA9D8FE5B14D62572C8E663566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448
struct  Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566  : public RuntimeObject
{
public:

public:
};

struct Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::PrehashSize
	int32_t ___PrehashSize_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::PublicKeySize
	int32_t ___PublicKeySize_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::SecretKeySize
	int32_t ___SecretKeySize_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::SignatureSize
	int32_t ___SignatureSize_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::Dom4Prefix
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Dom4Prefix_9;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_10;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::L
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___L_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::N
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___N_12;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::B_x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___B_x_29;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::B_y
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___B_y_30;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::precompLock
	RuntimeObject * ___precompLock_38;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointExt[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::precompBaseTable
	PointExtU5BU5D_tBC8364B67F5E41214A2C4793AA1E4270B0E0B8C5* ___precompBaseTable_39;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448::precompBase
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___precompBase_40;

public:
	inline static int32_t get_offset_of_PrehashSize_5() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___PrehashSize_5)); }
	inline int32_t get_PrehashSize_5() const { return ___PrehashSize_5; }
	inline int32_t* get_address_of_PrehashSize_5() { return &___PrehashSize_5; }
	inline void set_PrehashSize_5(int32_t value)
	{
		___PrehashSize_5 = value;
	}

	inline static int32_t get_offset_of_PublicKeySize_6() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___PublicKeySize_6)); }
	inline int32_t get_PublicKeySize_6() const { return ___PublicKeySize_6; }
	inline int32_t* get_address_of_PublicKeySize_6() { return &___PublicKeySize_6; }
	inline void set_PublicKeySize_6(int32_t value)
	{
		___PublicKeySize_6 = value;
	}

	inline static int32_t get_offset_of_SecretKeySize_7() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___SecretKeySize_7)); }
	inline int32_t get_SecretKeySize_7() const { return ___SecretKeySize_7; }
	inline int32_t* get_address_of_SecretKeySize_7() { return &___SecretKeySize_7; }
	inline void set_SecretKeySize_7(int32_t value)
	{
		___SecretKeySize_7 = value;
	}

	inline static int32_t get_offset_of_SignatureSize_8() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___SignatureSize_8)); }
	inline int32_t get_SignatureSize_8() const { return ___SignatureSize_8; }
	inline int32_t* get_address_of_SignatureSize_8() { return &___SignatureSize_8; }
	inline void set_SignatureSize_8(int32_t value)
	{
		___SignatureSize_8 = value;
	}

	inline static int32_t get_offset_of_Dom4Prefix_9() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___Dom4Prefix_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Dom4Prefix_9() const { return ___Dom4Prefix_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Dom4Prefix_9() { return &___Dom4Prefix_9; }
	inline void set_Dom4Prefix_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Dom4Prefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___Dom4Prefix_9), value);
	}

	inline static int32_t get_offset_of_P_10() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___P_10)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_10() const { return ___P_10; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_10() { return &___P_10; }
	inline void set_P_10(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_10 = value;
		Il2CppCodeGenWriteBarrier((&___P_10), value);
	}

	inline static int32_t get_offset_of_L_11() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___L_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_L_11() const { return ___L_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_L_11() { return &___L_11; }
	inline void set_L_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___L_11 = value;
		Il2CppCodeGenWriteBarrier((&___L_11), value);
	}

	inline static int32_t get_offset_of_N_12() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___N_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_N_12() const { return ___N_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_N_12() { return &___N_12; }
	inline void set_N_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___N_12 = value;
		Il2CppCodeGenWriteBarrier((&___N_12), value);
	}

	inline static int32_t get_offset_of_B_x_29() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___B_x_29)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_B_x_29() const { return ___B_x_29; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_B_x_29() { return &___B_x_29; }
	inline void set_B_x_29(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___B_x_29 = value;
		Il2CppCodeGenWriteBarrier((&___B_x_29), value);
	}

	inline static int32_t get_offset_of_B_y_30() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___B_y_30)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_B_y_30() const { return ___B_y_30; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_B_y_30() { return &___B_y_30; }
	inline void set_B_y_30(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___B_y_30 = value;
		Il2CppCodeGenWriteBarrier((&___B_y_30), value);
	}

	inline static int32_t get_offset_of_precompLock_38() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___precompLock_38)); }
	inline RuntimeObject * get_precompLock_38() const { return ___precompLock_38; }
	inline RuntimeObject ** get_address_of_precompLock_38() { return &___precompLock_38; }
	inline void set_precompLock_38(RuntimeObject * value)
	{
		___precompLock_38 = value;
		Il2CppCodeGenWriteBarrier((&___precompLock_38), value);
	}

	inline static int32_t get_offset_of_precompBaseTable_39() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___precompBaseTable_39)); }
	inline PointExtU5BU5D_tBC8364B67F5E41214A2C4793AA1E4270B0E0B8C5* get_precompBaseTable_39() const { return ___precompBaseTable_39; }
	inline PointExtU5BU5D_tBC8364B67F5E41214A2C4793AA1E4270B0E0B8C5** get_address_of_precompBaseTable_39() { return &___precompBaseTable_39; }
	inline void set_precompBaseTable_39(PointExtU5BU5D_tBC8364B67F5E41214A2C4793AA1E4270B0E0B8C5* value)
	{
		___precompBaseTable_39 = value;
		Il2CppCodeGenWriteBarrier((&___precompBaseTable_39), value);
	}

	inline static int32_t get_offset_of_precompBase_40() { return static_cast<int32_t>(offsetof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields, ___precompBase_40)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_precompBase_40() const { return ___precompBase_40; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_precompBase_40() { return &___precompBase_40; }
	inline void set_precompBase_40(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___precompBase_40 = value;
		Il2CppCodeGenWriteBarrier((&___precompBase_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448_TDF248BBC81831AAA9D8FE5B14D62572C8E663566_H
#ifndef POINTEXT_T4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440_H
#define POINTEXT_T4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointExt
struct  PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointExt::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointExt::y
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___y_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointExt::z
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440, ___x_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_0() const { return ___x_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440, ___y_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_y_1() const { return ___y_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___y_1 = value;
		Il2CppCodeGenWriteBarrier((&___y_1), value);
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440, ___z_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_z_2() const { return ___z_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___z_2 = value;
		Il2CppCodeGenWriteBarrier((&___z_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEXT_T4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440_H
#ifndef POINTPRECOMP_TB14338861C9537E5945A745A9A04E545D4A8B16C_H
#define POINTPRECOMP_TB14338861C9537E5945A745A9A04E545D4A8B16C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointPrecomp
struct  PointPrecomp_tB14338861C9537E5945A745A9A04E545D4A8B16C  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointPrecomp::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_PointPrecomp::y
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(PointPrecomp_tB14338861C9537E5945A745A9A04E545D4A8B16C, ___x_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_0() const { return ___x_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(PointPrecomp_tB14338861C9537E5945A745A9A04E545D4A8B16C, ___y_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_y_1() const { return ___y_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___y_1 = value;
		Il2CppCodeGenWriteBarrier((&___y_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTPRECOMP_TB14338861C9537E5945A745A9A04E545D4A8B16C_H
#ifndef SCALEXNEGATEYPOINTMAP_T9CCE75F4AA8414AB949D891A71A0A4438A2857E1_H
#define SCALEXNEGATEYPOINTMAP_T9CCE75F4AA8414AB949D891A71A0A4438A2857E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleXNegateYPointMap
struct  ScaleXNegateYPointMap_t9CCE75F4AA8414AB949D891A71A0A4438A2857E1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleXNegateYPointMap::scale
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(ScaleXNegateYPointMap_t9CCE75F4AA8414AB949D891A71A0A4438A2857E1, ___scale_0)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_scale_0() const { return ___scale_0; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___scale_0 = value;
		Il2CppCodeGenWriteBarrier((&___scale_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEXNEGATEYPOINTMAP_T9CCE75F4AA8414AB949D891A71A0A4438A2857E1_H
#ifndef SCALEXPOINTMAP_T42135DDC25167827C9DC507657AAB5EA81ED63C9_H
#define SCALEXPOINTMAP_T42135DDC25167827C9DC507657AAB5EA81ED63C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleXPointMap
struct  ScaleXPointMap_t42135DDC25167827C9DC507657AAB5EA81ED63C9  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleXPointMap::scale
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(ScaleXPointMap_t42135DDC25167827C9DC507657AAB5EA81ED63C9, ___scale_0)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_scale_0() const { return ___scale_0; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___scale_0 = value;
		Il2CppCodeGenWriteBarrier((&___scale_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEXPOINTMAP_T42135DDC25167827C9DC507657AAB5EA81ED63C9_H
#ifndef SCALEYNEGATEXPOINTMAP_T10726709A13C26FFDC58DBB11A6D565AA11A029D_H
#define SCALEYNEGATEXPOINTMAP_T10726709A13C26FFDC58DBB11A6D565AA11A029D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleYNegateXPointMap
struct  ScaleYNegateXPointMap_t10726709A13C26FFDC58DBB11A6D565AA11A029D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleYNegateXPointMap::scale
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(ScaleYNegateXPointMap_t10726709A13C26FFDC58DBB11A6D565AA11A029D, ___scale_0)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_scale_0() const { return ___scale_0; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___scale_0 = value;
		Il2CppCodeGenWriteBarrier((&___scale_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEYNEGATEXPOINTMAP_T10726709A13C26FFDC58DBB11A6D565AA11A029D_H
#ifndef SCALEYPOINTMAP_T337439CD3CF7BC0EBEB53228D9D8D819434E5E66_H
#define SCALEYPOINTMAP_T337439CD3CF7BC0EBEB53228D9D8D819434E5E66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleYPointMap
struct  ScaleYPointMap_t337439CD3CF7BC0EBEB53228D9D8D819434E5E66  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ScaleYPointMap::scale
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(ScaleYPointMap_t337439CD3CF7BC0EBEB53228D9D8D819434E5E66, ___scale_0)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_scale_0() const { return ___scale_0; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___scale_0 = value;
		Il2CppCodeGenWriteBarrier((&___scale_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEYPOINTMAP_T337439CD3CF7BC0EBEB53228D9D8D819434E5E66_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#define ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpCurve
struct  AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96  : public ECCurve_t6071986B64066FBF97315592BE971355FA584A39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#ifndef ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#define ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpFieldElement
struct  AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A  : public ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#ifndef SECP128R1LOOKUPTABLE_T951705AE16226A32E2E572367C376863D0C47802_H
#define SECP128R1LOOKUPTABLE_T951705AE16226A32E2E572367C376863D0C47802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve_SecP128R1LookupTable
struct  SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve_SecP128R1LookupTable::m_outer
	SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve_SecP128R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve_SecP128R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802, ___m_outer_0)); }
	inline SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1LOOKUPTABLE_T951705AE16226A32E2E572367C376863D0C47802_H
#ifndef SECP160K1LOOKUPTABLE_T493CD6F83067335E224B9ACABC0E678F28B4B943_H
#define SECP160K1LOOKUPTABLE_T493CD6F83067335E224B9ACABC0E678F28B4B943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve_SecP160K1LookupTable
struct  SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve_SecP160K1LookupTable::m_outer
	SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve_SecP160K1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve_SecP160K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943, ___m_outer_0)); }
	inline SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1LOOKUPTABLE_T493CD6F83067335E224B9ACABC0E678F28B4B943_H
#ifndef SECP160R1LOOKUPTABLE_TCC551F6E330896DCED113E4F47674DEA275AE333_H
#define SECP160R1LOOKUPTABLE_TCC551F6E330896DCED113E4F47674DEA275AE333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve_SecP160R1LookupTable
struct  SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve_SecP160R1LookupTable::m_outer
	SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve_SecP160R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve_SecP160R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333, ___m_outer_0)); }
	inline SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1LOOKUPTABLE_TCC551F6E330896DCED113E4F47674DEA275AE333_H
#ifndef SECP160R2LOOKUPTABLE_TF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6_H
#define SECP160R2LOOKUPTABLE_TF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve_SecP160R2LookupTable
struct  SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve_SecP160R2LookupTable::m_outer
	SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve_SecP160R2LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve_SecP160R2LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6, ___m_outer_0)); }
	inline SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2LOOKUPTABLE_TF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6_H
#ifndef SECP192K1LOOKUPTABLE_T0712EB1745F2ABB598D1447525A61AD5F0732DE9_H
#define SECP192K1LOOKUPTABLE_T0712EB1745F2ABB598D1447525A61AD5F0732DE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve_SecP192K1LookupTable
struct  SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve_SecP192K1LookupTable::m_outer
	SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve_SecP192K1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve_SecP192K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9, ___m_outer_0)); }
	inline SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1LOOKUPTABLE_T0712EB1745F2ABB598D1447525A61AD5F0732DE9_H
#ifndef SECP192R1LOOKUPTABLE_T0147F00C0D31EC56DA790DCE737AC9DCB63E583E_H
#define SECP192R1LOOKUPTABLE_T0147F00C0D31EC56DA790DCE737AC9DCB63E583E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve_SecP192R1LookupTable
struct  SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve_SecP192R1LookupTable::m_outer
	SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve_SecP192R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve_SecP192R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E, ___m_outer_0)); }
	inline SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1LOOKUPTABLE_T0147F00C0D31EC56DA790DCE737AC9DCB63E583E_H
#ifndef SECP224K1LOOKUPTABLE_T74A6EE32041D273FE91DC9D6FA81E1C521A2CADD_H
#define SECP224K1LOOKUPTABLE_T74A6EE32041D273FE91DC9D6FA81E1C521A2CADD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve_SecP224K1LookupTable
struct  SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve_SecP224K1LookupTable::m_outer
	SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve_SecP224K1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve_SecP224K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD, ___m_outer_0)); }
	inline SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1LOOKUPTABLE_T74A6EE32041D273FE91DC9D6FA81E1C521A2CADD_H
#ifndef SECP224R1LOOKUPTABLE_T1EF583DDB2347C2C07E8EB9647767F1F4BBC3048_H
#define SECP224R1LOOKUPTABLE_T1EF583DDB2347C2C07E8EB9647767F1F4BBC3048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve_SecP224R1LookupTable
struct  SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve_SecP224R1LookupTable::m_outer
	SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve_SecP224R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve_SecP224R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048, ___m_outer_0)); }
	inline SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1LOOKUPTABLE_T1EF583DDB2347C2C07E8EB9647767F1F4BBC3048_H
#ifndef SECP256K1LOOKUPTABLE_T27195488E6BC0B9ECB6C81CC061193C8C5BBB913_H
#define SECP256K1LOOKUPTABLE_T27195488E6BC0B9ECB6C81CC061193C8C5BBB913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve_SecP256K1LookupTable
struct  SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve_SecP256K1LookupTable::m_outer
	SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve_SecP256K1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve_SecP256K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913, ___m_outer_0)); }
	inline SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1LOOKUPTABLE_T27195488E6BC0B9ECB6C81CC061193C8C5BBB913_H
#ifndef ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#define ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointBase
struct  ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB  : public ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#ifndef DOUBLEADDMULTIPLIER_T5400FC431C3A7D082C20554906CCF63124955683_H
#define DOUBLEADDMULTIPLIER_T5400FC431C3A7D082C20554906CCF63124955683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.DoubleAddMultiplier
struct  DoubleAddMultiplier_t5400FC431C3A7D082C20554906CCF63124955683  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEADDMULTIPLIER_T5400FC431C3A7D082C20554906CCF63124955683_H
#ifndef FIXEDPOINTCOMBMULTIPLIER_TD1D11B055177A6EDFCABDC567FCCF31476EA85C3_H
#define FIXEDPOINTCOMBMULTIPLIER_TD1D11B055177A6EDFCABDC567FCCF31476EA85C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.FixedPointCombMultiplier
struct  FixedPointCombMultiplier_tD1D11B055177A6EDFCABDC567FCCF31476EA85C3  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDPOINTCOMBMULTIPLIER_TD1D11B055177A6EDFCABDC567FCCF31476EA85C3_H
#ifndef GLVMULTIPLIER_T526A59283036D67F3126CDA9AC6CA2D4831C148A_H
#define GLVMULTIPLIER_T526A59283036D67F3126CDA9AC6CA2D4831C148A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.GlvMultiplier
struct  GlvMultiplier_t526A59283036D67F3126CDA9AC6CA2D4831C148A  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.GlvMultiplier::curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___curve_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.GlvEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.GlvMultiplier::glvEndomorphism
	RuntimeObject* ___glvEndomorphism_1;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(GlvMultiplier_t526A59283036D67F3126CDA9AC6CA2D4831C148A, ___curve_0)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_curve_0() const { return ___curve_0; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_glvEndomorphism_1() { return static_cast<int32_t>(offsetof(GlvMultiplier_t526A59283036D67F3126CDA9AC6CA2D4831C148A, ___glvEndomorphism_1)); }
	inline RuntimeObject* get_glvEndomorphism_1() const { return ___glvEndomorphism_1; }
	inline RuntimeObject** get_address_of_glvEndomorphism_1() { return &___glvEndomorphism_1; }
	inline void set_glvEndomorphism_1(RuntimeObject* value)
	{
		___glvEndomorphism_1 = value;
		Il2CppCodeGenWriteBarrier((&___glvEndomorphism_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLVMULTIPLIER_T526A59283036D67F3126CDA9AC6CA2D4831C148A_H
#ifndef MIXEDNAFR2LMULTIPLIER_TB257CFACC0939D158100547C0CB2EC0772A4C990_H
#define MIXEDNAFR2LMULTIPLIER_TB257CFACC0939D158100547C0CB2EC0772A4C990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.MixedNafR2LMultiplier
struct  MixedNafR2LMultiplier_tB257CFACC0939D158100547C0CB2EC0772A4C990  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.MixedNafR2LMultiplier::additionCoord
	int32_t ___additionCoord_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.MixedNafR2LMultiplier::doublingCoord
	int32_t ___doublingCoord_1;

public:
	inline static int32_t get_offset_of_additionCoord_0() { return static_cast<int32_t>(offsetof(MixedNafR2LMultiplier_tB257CFACC0939D158100547C0CB2EC0772A4C990, ___additionCoord_0)); }
	inline int32_t get_additionCoord_0() const { return ___additionCoord_0; }
	inline int32_t* get_address_of_additionCoord_0() { return &___additionCoord_0; }
	inline void set_additionCoord_0(int32_t value)
	{
		___additionCoord_0 = value;
	}

	inline static int32_t get_offset_of_doublingCoord_1() { return static_cast<int32_t>(offsetof(MixedNafR2LMultiplier_tB257CFACC0939D158100547C0CB2EC0772A4C990, ___doublingCoord_1)); }
	inline int32_t get_doublingCoord_1() const { return ___doublingCoord_1; }
	inline int32_t* get_address_of_doublingCoord_1() { return &___doublingCoord_1; }
	inline void set_doublingCoord_1(int32_t value)
	{
		___doublingCoord_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXEDNAFR2LMULTIPLIER_TB257CFACC0939D158100547C0CB2EC0772A4C990_H
#ifndef MONTGOMERYLADDERMULTIPLIER_TF60BEF210D2B3D0F90EEE9E6654266BDF29C9E8E_H
#define MONTGOMERYLADDERMULTIPLIER_TF60BEF210D2B3D0F90EEE9E6654266BDF29C9E8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.MontgomeryLadderMultiplier
struct  MontgomeryLadderMultiplier_tF60BEF210D2B3D0F90EEE9E6654266BDF29C9E8E  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONTGOMERYLADDERMULTIPLIER_TF60BEF210D2B3D0F90EEE9E6654266BDF29C9E8E_H
#ifndef NAFL2RMULTIPLIER_TD3B7C67F4C2DC09B6FBA5A67C8C20272B2131A17_H
#define NAFL2RMULTIPLIER_TD3B7C67F4C2DC09B6FBA5A67C8C20272B2131A17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.NafL2RMultiplier
struct  NafL2RMultiplier_tD3B7C67F4C2DC09B6FBA5A67C8C20272B2131A17  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAFL2RMULTIPLIER_TD3B7C67F4C2DC09B6FBA5A67C8C20272B2131A17_H
#ifndef NAFR2LMULTIPLIER_TECF309E0A3EBCF954C2C80636546E15F14132F46_H
#define NAFR2LMULTIPLIER_TECF309E0A3EBCF954C2C80636546E15F14132F46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.NafR2LMultiplier
struct  NafR2LMultiplier_tECF309E0A3EBCF954C2C80636546E15F14132F46  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAFR2LMULTIPLIER_TECF309E0A3EBCF954C2C80636546E15F14132F46_H
#ifndef REFERENCEMULTIPLIER_T8955669AD9959C53BCBBB0E2FD2C2133AAEF87B9_H
#define REFERENCEMULTIPLIER_T8955669AD9959C53BCBBB0E2FD2C2133AAEF87B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ReferenceMultiplier
struct  ReferenceMultiplier_t8955669AD9959C53BCBBB0E2FD2C2133AAEF87B9  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEMULTIPLIER_T8955669AD9959C53BCBBB0E2FD2C2133AAEF87B9_H
#ifndef WNAFL2RMULTIPLIER_TACE2E1E08C998602F66270114ADB08E10FC0D9D7_H
#define WNAFL2RMULTIPLIER_TACE2E1E08C998602F66270114ADB08E10FC0D9D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafL2RMultiplier
struct  WNafL2RMultiplier_tACE2E1E08C998602F66270114ADB08E10FC0D9D7  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WNAFL2RMULTIPLIER_TACE2E1E08C998602F66270114ADB08E10FC0D9D7_H
#ifndef WTAUNAFMULTIPLIER_TAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_H
#define WTAUNAFMULTIPLIER_TAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier
struct  WTauNafMultiplier_tAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

struct WTauNafMultiplier_tAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WTauNafMultiplier::PRECOMP_NAME
	String_t* ___PRECOMP_NAME_0;

public:
	inline static int32_t get_offset_of_PRECOMP_NAME_0() { return static_cast<int32_t>(offsetof(WTauNafMultiplier_tAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_StaticFields, ___PRECOMP_NAME_0)); }
	inline String_t* get_PRECOMP_NAME_0() const { return ___PRECOMP_NAME_0; }
	inline String_t** get_address_of_PRECOMP_NAME_0() { return &___PRECOMP_NAME_0; }
	inline void set_PRECOMP_NAME_0(String_t* value)
	{
		___PRECOMP_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_NAME_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WTAUNAFMULTIPLIER_TAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_H
#ifndef ZSIGNEDDIGITL2RMULTIPLIER_T6C2841B4A25CC18314C5D2FD0E320D60F06369E7_H
#define ZSIGNEDDIGITL2RMULTIPLIER_T6C2841B4A25CC18314C5D2FD0E320D60F06369E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ZSignedDigitL2RMultiplier
struct  ZSignedDigitL2RMultiplier_t6C2841B4A25CC18314C5D2FD0E320D60F06369E7  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSIGNEDDIGITL2RMULTIPLIER_T6C2841B4A25CC18314C5D2FD0E320D60F06369E7_H
#ifndef ZSIGNEDDIGITR2LMULTIPLIER_T30FEFFF978A2E9953E2AA8796C2BC4A66D478D8C_H
#define ZSIGNEDDIGITR2LMULTIPLIER_T30FEFFF978A2E9953E2AA8796C2BC4A66D478D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ZSignedDigitR2LMultiplier
struct  ZSignedDigitR2LMultiplier_t30FEFFF978A2E9953E2AA8796C2BC4A66D478D8C  : public AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSIGNEDDIGITR2LMULTIPLIER_T30FEFFF978A2E9953E2AA8796C2BC4A66D478D8C_H
#ifndef SIMPLELOOKUPTABLE_T4F1E4FC8967BB192762027BD2C77052A84966E2F_H
#define SIMPLELOOKUPTABLE_T4F1E4FC8967BB192762027BD2C77052A84966E2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.SimpleLookupTable
struct  SimpleLookupTable_t4F1E4FC8967BB192762027BD2C77052A84966E2F  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.SimpleLookupTable::points
	ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* ___points_0;

public:
	inline static int32_t get_offset_of_points_0() { return static_cast<int32_t>(offsetof(SimpleLookupTable_t4F1E4FC8967BB192762027BD2C77052A84966E2F, ___points_0)); }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* get_points_0() const { return ___points_0; }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1** get_address_of_points_0() { return &___points_0; }
	inline void set_points_0(ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* value)
	{
		___points_0 = value;
		Il2CppCodeGenWriteBarrier((&___points_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLELOOKUPTABLE_T4F1E4FC8967BB192762027BD2C77052A84966E2F_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#define ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpPoint
struct  AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519  : public ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#ifndef SECP128R1CURVE_T60068AB54FA6C6421190D0457F7691AB8FE34E21_H
#define SECP128R1CURVE_T60068AB54FA6C6421190D0457F7691AB8FE34E21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve
struct  SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve::m_infinity
	SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21, ___m_infinity_20)); }
	inline SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve::SECP128R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP128R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP128R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21_StaticFields, ___SECP128R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP128R1_AFFINE_ZS_19() const { return ___SECP128R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP128R1_AFFINE_ZS_19() { return &___SECP128R1_AFFINE_ZS_19; }
	inline void set_SECP128R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP128R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP128R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1CURVE_T60068AB54FA6C6421190D0457F7691AB8FE34E21_H
#ifndef SECP128R1FIELDELEMENT_TFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_H
#define SECP128R1FIELDELEMENT_TFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1FieldElement
struct  SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1FIELDELEMENT_TFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_H
#ifndef SECP160K1CURVE_TF4F4037EBF56135F71A5D08C6440A70E71C999A6_H
#define SECP160K1CURVE_TF4F4037EBF56135F71A5D08C6440A70E71C999A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve
struct  SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve::m_infinity
	SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6, ___m_infinity_20)); }
	inline SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve::SECP160K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP160K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP160K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6_StaticFields, ___SECP160K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP160K1_AFFINE_ZS_19() const { return ___SECP160K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP160K1_AFFINE_ZS_19() { return &___SECP160K1_AFFINE_ZS_19; }
	inline void set_SECP160K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP160K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP160K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1CURVE_TF4F4037EBF56135F71A5D08C6440A70E71C999A6_H
#ifndef SECP160R1CURVE_TB9B4255B13A3074070989EE3173B4C768CC8060A_H
#define SECP160R1CURVE_TB9B4255B13A3074070989EE3173B4C768CC8060A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve
struct  SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve::m_infinity
	SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A, ___m_infinity_20)); }
	inline SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve::SECP160R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP160R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP160R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A_StaticFields, ___SECP160R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP160R1_AFFINE_ZS_19() const { return ___SECP160R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP160R1_AFFINE_ZS_19() { return &___SECP160R1_AFFINE_ZS_19; }
	inline void set_SECP160R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP160R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP160R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1CURVE_TB9B4255B13A3074070989EE3173B4C768CC8060A_H
#ifndef SECP160R1FIELDELEMENT_TB3E415AC5E3B8FADDA06960B80B08813978F6C39_H
#define SECP160R1FIELDELEMENT_TB3E415AC5E3B8FADDA06960B80B08813978F6C39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1FieldElement
struct  SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1FIELDELEMENT_TB3E415AC5E3B8FADDA06960B80B08813978F6C39_H
#ifndef SECP160R2CURVE_TECD88B4A6CD190846ED50530D4ECC68125193ABE_H
#define SECP160R2CURVE_TECD88B4A6CD190846ED50530D4ECC68125193ABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve
struct  SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve::m_infinity
	SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE, ___m_infinity_20)); }
	inline SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve::SECP160R2_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP160R2_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP160R2_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE_StaticFields, ___SECP160R2_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP160R2_AFFINE_ZS_19() const { return ___SECP160R2_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP160R2_AFFINE_ZS_19() { return &___SECP160R2_AFFINE_ZS_19; }
	inline void set_SECP160R2_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP160R2_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP160R2_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2CURVE_TECD88B4A6CD190846ED50530D4ECC68125193ABE_H
#ifndef SECP160R2FIELDELEMENT_T9FC305BCF3D4C8196D957EF14424EB15859D56BB_H
#define SECP160R2FIELDELEMENT_T9FC305BCF3D4C8196D957EF14424EB15859D56BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2FieldElement
struct  SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2FIELDELEMENT_T9FC305BCF3D4C8196D957EF14424EB15859D56BB_H
#ifndef SECP192K1CURVE_TB39C2214C5E041221EC2168D3C47DD02B2166ECA_H
#define SECP192K1CURVE_TB39C2214C5E041221EC2168D3C47DD02B2166ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve
struct  SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve::m_infinity
	SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA, ___m_infinity_20)); }
	inline SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve::SECP192K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP192K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP192K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA_StaticFields, ___SECP192K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP192K1_AFFINE_ZS_19() const { return ___SECP192K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP192K1_AFFINE_ZS_19() { return &___SECP192K1_AFFINE_ZS_19; }
	inline void set_SECP192K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP192K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP192K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1CURVE_TB39C2214C5E041221EC2168D3C47DD02B2166ECA_H
#ifndef SECP192K1FIELDELEMENT_TE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_H
#define SECP192K1FIELDELEMENT_TE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1FieldElement
struct  SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1FIELDELEMENT_TE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_H
#ifndef SECP192R1CURVE_TC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_H
#define SECP192R1CURVE_TC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve
struct  SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve::m_infinity
	SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C, ___m_infinity_20)); }
	inline SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve::SECP192R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP192R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP192R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_StaticFields, ___SECP192R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP192R1_AFFINE_ZS_19() const { return ___SECP192R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP192R1_AFFINE_ZS_19() { return &___SECP192R1_AFFINE_ZS_19; }
	inline void set_SECP192R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP192R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP192R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1CURVE_TC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_H
#ifndef SECP192R1FIELDELEMENT_TC4FDFCFD61D484D43E13376986740FDA6B694651_H
#define SECP192R1FIELDELEMENT_TC4FDFCFD61D484D43E13376986740FDA6B694651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1FieldElement
struct  SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1FIELDELEMENT_TC4FDFCFD61D484D43E13376986740FDA6B694651_H
#ifndef SECP224K1CURVE_T7453474F1A35909AD0855804D058853ADD9A7730_H
#define SECP224K1CURVE_T7453474F1A35909AD0855804D058853ADD9A7730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve
struct  SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve::m_infinity
	SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730, ___m_infinity_20)); }
	inline SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve::SECP224K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP224K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP224K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730_StaticFields, ___SECP224K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP224K1_AFFINE_ZS_19() const { return ___SECP224K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP224K1_AFFINE_ZS_19() { return &___SECP224K1_AFFINE_ZS_19; }
	inline void set_SECP224K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP224K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP224K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1CURVE_T7453474F1A35909AD0855804D058853ADD9A7730_H
#ifndef SECP224K1FIELDELEMENT_T9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_H
#define SECP224K1FIELDELEMENT_T9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement
struct  SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB, ___x_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_2() const { return ___x_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

struct SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement::PRECOMP_POW2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PRECOMP_POW2_1;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}

	inline static int32_t get_offset_of_PRECOMP_POW2_1() { return static_cast<int32_t>(offsetof(SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_StaticFields, ___PRECOMP_POW2_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PRECOMP_POW2_1() const { return ___PRECOMP_POW2_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PRECOMP_POW2_1() { return &___PRECOMP_POW2_1; }
	inline void set_PRECOMP_POW2_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PRECOMP_POW2_1 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_POW2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1FIELDELEMENT_T9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_H
#ifndef SECP224R1CURVE_TF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_H
#define SECP224R1CURVE_TF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve
struct  SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve::m_infinity
	SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368, ___m_infinity_20)); }
	inline SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve::SECP224R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP224R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP224R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_StaticFields, ___SECP224R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP224R1_AFFINE_ZS_19() const { return ___SECP224R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP224R1_AFFINE_ZS_19() { return &___SECP224R1_AFFINE_ZS_19; }
	inline void set_SECP224R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP224R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP224R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1CURVE_TF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_H
#ifndef SECP224R1FIELDELEMENT_T49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_H
#define SECP224R1FIELDELEMENT_T49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1FieldElement
struct  SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1FIELDELEMENT_T49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_H
#ifndef SECP256K1CURVE_T935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_H
#define SECP256K1CURVE_T935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve
struct  SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve::m_infinity
	SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9, ___m_infinity_20)); }
	inline SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Curve::SECP256K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP256K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP256K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_StaticFields, ___SECP256K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP256K1_AFFINE_ZS_19() const { return ___SECP256K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP256K1_AFFINE_ZS_19() { return &___SECP256K1_AFFINE_ZS_19; }
	inline void set_SECP256K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP256K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP256K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1CURVE_T935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_H
#ifndef SECP256K1FIELDELEMENT_T9048E49BD768652D7D9B30E8DBB3CD6064834243_H
#define SECP256K1FIELDELEMENT_T9048E49BD768652D7D9B30E8DBB3CD6064834243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1FieldElement
struct  SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1FIELDELEMENT_T9048E49BD768652D7D9B30E8DBB3CD6064834243_H
#ifndef SECP256R1CURVE_T3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_H
#define SECP256R1CURVE_T3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve
struct  SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve::m_infinity
	SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2, ___m_infinity_20)); }
	inline SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve::SECP256R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP256R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP256R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_StaticFields, ___SECP256R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP256R1_AFFINE_ZS_19() const { return ___SECP256R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP256R1_AFFINE_ZS_19() { return &___SECP256R1_AFFINE_ZS_19; }
	inline void set_SECP256R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP256R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP256R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1CURVE_T3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_H
#ifndef WNAFPRECOMPINFO_T4EC059972FD8B4E2348105ABECCCB0011383D914_H
#define WNAFPRECOMPINFO_T4EC059972FD8B4E2348105ABECCCB0011383D914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo
struct  WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_promotionCountdown
	int32_t ___m_promotionCountdown_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_confWidth
	int32_t ___m_confWidth_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_preComp
	ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* ___m_preComp_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_preCompNeg
	ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* ___m_preCompNeg_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_twice
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___m_twice_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.WNafPreCompInfo::m_width
	int32_t ___m_width_5;

public:
	inline static int32_t get_offset_of_m_promotionCountdown_0() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914, ___m_promotionCountdown_0)); }
	inline int32_t get_m_promotionCountdown_0() const { return ___m_promotionCountdown_0; }
	inline int32_t* get_address_of_m_promotionCountdown_0() { return &___m_promotionCountdown_0; }
	inline void set_m_promotionCountdown_0(int32_t value)
	{
		___m_promotionCountdown_0 = value;
	}

	inline static int32_t get_offset_of_m_confWidth_1() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914, ___m_confWidth_1)); }
	inline int32_t get_m_confWidth_1() const { return ___m_confWidth_1; }
	inline int32_t* get_address_of_m_confWidth_1() { return &___m_confWidth_1; }
	inline void set_m_confWidth_1(int32_t value)
	{
		___m_confWidth_1 = value;
	}

	inline static int32_t get_offset_of_m_preComp_2() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914, ___m_preComp_2)); }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* get_m_preComp_2() const { return ___m_preComp_2; }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1** get_address_of_m_preComp_2() { return &___m_preComp_2; }
	inline void set_m_preComp_2(ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* value)
	{
		___m_preComp_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_preComp_2), value);
	}

	inline static int32_t get_offset_of_m_preCompNeg_3() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914, ___m_preCompNeg_3)); }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* get_m_preCompNeg_3() const { return ___m_preCompNeg_3; }
	inline ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1** get_address_of_m_preCompNeg_3() { return &___m_preCompNeg_3; }
	inline void set_m_preCompNeg_3(ECPointU5BU5D_t827C5BA889AD5229C4346ADFF2C9179BAC3732B1* value)
	{
		___m_preCompNeg_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompNeg_3), value);
	}

	inline static int32_t get_offset_of_m_twice_4() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914, ___m_twice_4)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_m_twice_4() const { return ___m_twice_4; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_m_twice_4() { return &___m_twice_4; }
	inline void set_m_twice_4(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___m_twice_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_twice_4), value);
	}

	inline static int32_t get_offset_of_m_width_5() { return static_cast<int32_t>(offsetof(WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914, ___m_width_5)); }
	inline int32_t get_m_width_5() const { return ___m_width_5; }
	inline int32_t* get_address_of_m_width_5() { return &___m_width_5; }
	inline void set_m_width_5(int32_t value)
	{
		___m_width_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WNAFPRECOMPINFO_T4EC059972FD8B4E2348105ABECCCB0011383D914_H
#ifndef ALGORITHM_T7B91EC64B3A8F578607F08A659770A126C0C3BC2_H
#define ALGORITHM_T7B91EC64B3A8F578607F08A659770A126C0C3BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_Algorithm
struct  Algorithm_t7B91EC64B3A8F578607F08A659770A126C0C3BC2 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed25519_Algorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Algorithm_t7B91EC64B3A8F578607F08A659770A126C0C3BC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHM_T7B91EC64B3A8F578607F08A659770A126C0C3BC2_H
#ifndef ALGORITHM_T4E2540310C583DBF88185B0D2F51DD3582CCBFC0_H
#define ALGORITHM_T4E2540310C583DBF88185B0D2F51DD3582CCBFC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_Algorithm
struct  Algorithm_t4E2540310C583DBF88185B0D2F51DD3582CCBFC0 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Rfc8032.Ed448_Algorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Algorithm_t4E2540310C583DBF88185B0D2F51DD3582CCBFC0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHM_T4E2540310C583DBF88185B0D2F51DD3582CCBFC0_H
#ifndef SECP128R1POINT_TE502A7CCAA399112E85268147FA9B0077B5E5687_H
#define SECP128R1POINT_TE502A7CCAA399112E85268147FA9B0077B5E5687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Point
struct  SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1POINT_TE502A7CCAA399112E85268147FA9B0077B5E5687_H
#ifndef SECP160K1POINT_T68EDC31CED81A3B71E0CA21DA18433D09E8F44A7_H
#define SECP160K1POINT_T68EDC31CED81A3B71E0CA21DA18433D09E8F44A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Point
struct  SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1POINT_T68EDC31CED81A3B71E0CA21DA18433D09E8F44A7_H
#ifndef SECP160R1POINT_TA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D_H
#define SECP160R1POINT_TA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Point
struct  SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1POINT_TA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D_H
#ifndef SECP160R2POINT_T66F02792EB3A40EFC3321D731C06D4C5699396E3_H
#define SECP160R2POINT_T66F02792EB3A40EFC3321D731C06D4C5699396E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Point
struct  SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2POINT_T66F02792EB3A40EFC3321D731C06D4C5699396E3_H
#ifndef SECP192K1POINT_T7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B_H
#define SECP192K1POINT_T7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Point
struct  SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1POINT_T7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B_H
#ifndef SECP192R1POINT_T0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717_H
#define SECP192R1POINT_T0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Point
struct  SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1POINT_T0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717_H
#ifndef SECP224K1POINT_T840B7E4E2F617FB6072201BC06E35573CA03F644_H
#define SECP224K1POINT_T840B7E4E2F617FB6072201BC06E35573CA03F644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Point
struct  SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1POINT_T840B7E4E2F617FB6072201BC06E35573CA03F644_H
#ifndef SECP224R1POINT_TDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD_H
#define SECP224R1POINT_TDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Point
struct  SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1POINT_TDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD_H
#ifndef SECP256K1POINT_T981F759B49333F954C0916E97600B3411C436063_H
#define SECP256K1POINT_T981F759B49333F954C0916E97600B3411C436063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256K1Point
struct  SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1POINT_T981F759B49333F954C0916E97600B3411C436063_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (ScaleXNegateYPointMap_t9CCE75F4AA8414AB949D891A71A0A4438A2857E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4200[1] = 
{
	ScaleXNegateYPointMap_t9CCE75F4AA8414AB949D891A71A0A4438A2857E1::get_offset_of_scale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (ScaleXPointMap_t42135DDC25167827C9DC507657AAB5EA81ED63C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4201[1] = 
{
	ScaleXPointMap_t42135DDC25167827C9DC507657AAB5EA81ED63C9::get_offset_of_scale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (ScaleYNegateXPointMap_t10726709A13C26FFDC58DBB11A6D565AA11A029D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4202[1] = 
{
	ScaleYNegateXPointMap_t10726709A13C26FFDC58DBB11A6D565AA11A029D::get_offset_of_scale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (ScaleYPointMap_t337439CD3CF7BC0EBEB53228D9D8D819434E5E66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4203[1] = 
{
	ScaleYPointMap_t337439CD3CF7BC0EBEB53228D9D8D819434E5E66::get_offset_of_scale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (SimpleLookupTable_t4F1E4FC8967BB192762027BD2C77052A84966E2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4204[1] = 
{
	SimpleLookupTable_t4F1E4FC8967BB192762027BD2C77052A84966E2F::get_offset_of_points_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A), -1, sizeof(Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4205[31] = 
{
	0,
	0,
	0,
	0,
	0,
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_PrehashSize_5(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_PublicKeySize_6(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_SecretKeySize_7(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_SignatureSize_8(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_Dom2Prefix_9(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_P_10(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_L_11(),
	0,
	0,
	0,
	0,
	0,
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_B_x_17(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_B_y_18(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_C_d_19(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_C_d2_20(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_C_d4_21(),
	0,
	0,
	0,
	0,
	0,
	0,
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_precompLock_28(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_precompBaseTable_29(),
	Ed25519_t572650320F5BF60E98AB3ABD1C0386804FCF773A_StaticFields::get_offset_of_precompBase_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (Algorithm_t7B91EC64B3A8F578607F08A659770A126C0C3BC2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4206[4] = 
{
	Algorithm_t7B91EC64B3A8F578607F08A659770A126C0C3BC2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4207[5] = 
{
	PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91::get_offset_of_x_0(),
	PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91::get_offset_of_y_1(),
	PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91::get_offset_of_z_2(),
	PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91::get_offset_of_u_3(),
	PointAccum_tCB35FD1CFCF12E0FDB5FD783E3F01E6CA13B2C91::get_offset_of_v_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4208[4] = 
{
	PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC::get_offset_of_x_0(),
	PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC::get_offset_of_y_1(),
	PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC::get_offset_of_z_2(),
	PointExt_t1717FE5F6B65CC98C75C4996BEC310D000C3B0EC::get_offset_of_t_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4209[3] = 
{
	PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315::get_offset_of_ypx_h_0(),
	PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315::get_offset_of_ymx_h_1(),
	PointPrecomp_t75BB8642311CC417751D91A92F8C30B6E7BFD315::get_offset_of_xyd_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566), -1, sizeof(Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4210[41] = 
{
	0,
	0,
	0,
	0,
	0,
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_PrehashSize_5(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_PublicKeySize_6(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_SecretKeySize_7(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_SignatureSize_8(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_Dom4Prefix_9(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_P_10(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_L_11(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_N_12(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_B_x_29(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_B_y_30(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_precompLock_38(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_precompBaseTable_39(),
	Ed448_tDF248BBC81831AAA9D8FE5B14D62572C8E663566_StaticFields::get_offset_of_precompBase_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (Algorithm_t4E2540310C583DBF88185B0D2F51DD3582CCBFC0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4211[3] = 
{
	Algorithm_t4E2540310C583DBF88185B0D2F51DD3582CCBFC0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4212[3] = 
{
	PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440::get_offset_of_x_0(),
	PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440::get_offset_of_y_1(),
	PointExt_t4EC7AB2C94A7F153F5FF54E1BCF63C0C9A807440::get_offset_of_z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (PointPrecomp_tB14338861C9537E5945A745A9A04E545D4A8B16C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4213[2] = 
{
	PointPrecomp_tB14338861C9537E5945A745A9A04E545D4A8B16C::get_offset_of_x_0(),
	PointPrecomp_tB14338861C9537E5945A745A9A04E545D4A8B16C::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (X25519_t86CC670092AF75CAA36A1BC881B3A08E26375639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4214[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (X25519Field_t4D0E8E0830B57A0DEE4469E5741A182E22A7D359), -1, sizeof(X25519Field_t4D0E8E0830B57A0DEE4469E5741A182E22A7D359_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4215[5] = 
{
	0,
	0,
	0,
	0,
	X25519Field_t4D0E8E0830B57A0DEE4469E5741A182E22A7D359_StaticFields::get_offset_of_RootNegOne_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (X448_t497460387E139010D296F87FD309BC8E5A611682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4216[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (X448Field_t3FB712418037C22724F8D1D790A4DF93FAE3D4A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4217[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (AbstractECMultiplier_tBCE555017CAD3C283D05FA0F2BB83B6B396039CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (DoubleAddMultiplier_t5400FC431C3A7D082C20554906CCF63124955683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (FixedPointCombMultiplier_tD1D11B055177A6EDFCABDC567FCCF31476EA85C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4222[3] = 
{
	FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648::get_offset_of_m_offset_0(),
	FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648::get_offset_of_m_lookupTable_1(),
	FixedPointPreCompInfo_t7E609156248AE55F576F239D23A6D16E39EB5648::get_offset_of_m_width_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (FixedPointUtilities_t127AF864D9B7B2C687EA593A9E2E8EF1478252B1), -1, sizeof(FixedPointUtilities_t127AF864D9B7B2C687EA593A9E2E8EF1478252B1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4223[1] = 
{
	FixedPointUtilities_t127AF864D9B7B2C687EA593A9E2E8EF1478252B1_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (FixedPointCallback_t9A8E93848CFAD427DE2365DCFA20B20F88A6DA43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[1] = 
{
	FixedPointCallback_t9A8E93848CFAD427DE2365DCFA20B20F88A6DA43::get_offset_of_m_p_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (GlvMultiplier_t526A59283036D67F3126CDA9AC6CA2D4831C148A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4225[2] = 
{
	GlvMultiplier_t526A59283036D67F3126CDA9AC6CA2D4831C148A::get_offset_of_curve_0(),
	GlvMultiplier_t526A59283036D67F3126CDA9AC6CA2D4831C148A::get_offset_of_glvEndomorphism_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (MixedNafR2LMultiplier_tB257CFACC0939D158100547C0CB2EC0772A4C990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4227[2] = 
{
	MixedNafR2LMultiplier_tB257CFACC0939D158100547C0CB2EC0772A4C990::get_offset_of_additionCoord_0(),
	MixedNafR2LMultiplier_tB257CFACC0939D158100547C0CB2EC0772A4C990::get_offset_of_doublingCoord_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (MontgomeryLadderMultiplier_tF60BEF210D2B3D0F90EEE9E6654266BDF29C9E8E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (NafL2RMultiplier_tD3B7C67F4C2DC09B6FBA5A67C8C20272B2131A17), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (NafR2LMultiplier_tECF309E0A3EBCF954C2C80636546E15F14132F46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (ReferenceMultiplier_t8955669AD9959C53BCBBB0E2FD2C2133AAEF87B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246), -1, sizeof(ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4233[4] = 
{
	ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246_StaticFields::get_offset_of_PRECOMP_NAME_0(),
	ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246::get_offset_of_failed_1(),
	ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246::get_offset_of_curveEquationPassed_2(),
	ValidityPreCompInfo_tED77416C986FEB8F504F98C74FA731B1AF431246::get_offset_of_orderPassed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (WNafL2RMultiplier_tACE2E1E08C998602F66270114ADB08E10FC0D9D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4235[6] = 
{
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914::get_offset_of_m_promotionCountdown_0(),
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914::get_offset_of_m_confWidth_1(),
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914::get_offset_of_m_preComp_2(),
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914::get_offset_of_m_preCompNeg_3(),
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914::get_offset_of_m_twice_4(),
	WNafPreCompInfo_t4EC059972FD8B4E2348105ABECCCB0011383D914::get_offset_of_m_width_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7), -1, sizeof(WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4236[4] = 
{
	WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields::get_offset_of_PRECOMP_NAME_0(),
	WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields::get_offset_of_DEFAULT_WINDOW_SIZE_CUTOFFS_1(),
	WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields::get_offset_of_MAX_WIDTH_2(),
	WNafUtilities_tB17A9E6A58ED5B6D2561CFF1A5769848D02D79D7_StaticFields::get_offset_of_EMPTY_POINTS_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (ConfigureBasepointCallback_t0EC8B5065F7AF7AD1459086033EB018912F81529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4237[2] = 
{
	ConfigureBasepointCallback_t0EC8B5065F7AF7AD1459086033EB018912F81529::get_offset_of_m_curve_0(),
	ConfigureBasepointCallback_t0EC8B5065F7AF7AD1459086033EB018912F81529::get_offset_of_m_confWidth_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4238[3] = 
{
	MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436::get_offset_of_m_infoP_0(),
	MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436::get_offset_of_m_includeNegated_1(),
	MapPointCallback_tB9E592157D3ECB9A34A3F9F7B7AA4C99E3E83436::get_offset_of_m_pointMap_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4239[3] = 
{
	PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96::get_offset_of_m_p_0(),
	PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96::get_offset_of_m_minWidth_1(),
	PrecomputeCallback_tC2DAB06B707A71AC302B4634638668CC051F5D96::get_offset_of_m_includeNegated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4240[4] = 
{
	PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E::get_offset_of_m_point_0(),
	PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E::get_offset_of_m_pointMap_1(),
	PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E::get_offset_of_m_fromWNaf_2(),
	PrecomputeWithPointMapCallback_t8F9C60DE78DDC59B3968AEB9A095462E92E2C82E::get_offset_of_m_includeNegated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (WTauNafMultiplier_tAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03), -1, sizeof(WTauNafMultiplier_tAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4241[1] = 
{
	WTauNafMultiplier_tAB49ACA0DF9CB13A65C7A5A4480CAEDAD114EF03_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (WTauNafCallback_tF14D451006BBA0BF493C218CD55F30C117EB690C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4242[2] = 
{
	WTauNafCallback_tF14D451006BBA0BF493C218CD55F30C117EB690C::get_offset_of_m_p_0(),
	WTauNafCallback_tF14D451006BBA0BF493C218CD55F30C117EB690C::get_offset_of_m_a_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (WTauNafPreCompInfo_t00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4243[1] = 
{
	WTauNafPreCompInfo_t00D4D3A8AFE2CE2DA9946212A6CCB52A6004B425::get_offset_of_m_preComp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (ZSignedDigitL2RMultiplier_t6C2841B4A25CC18314C5D2FD0E320D60F06369E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (ZSignedDigitR2LMultiplier_t30FEFFF978A2E9953E2AA8796C2BC4A66D478D8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (EndoPreCompInfo_tF529412E67F45F2B53F97892259F0D5767B4C927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4247[2] = 
{
	EndoPreCompInfo_tF529412E67F45F2B53F97892259F0D5767B4C927::get_offset_of_m_endomorphism_0(),
	EndoPreCompInfo_tF529412E67F45F2B53F97892259F0D5767B4C927::get_offset_of_m_mappedPoint_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (EndoUtilities_tE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D), -1, sizeof(EndoUtilities_tE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4248[1] = 
{
	EndoUtilities_tE49B5D8E1EBF4F0783972F34CAB223C9E57E8C1D_StaticFields::get_offset_of_PRECOMP_NAME_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (MapPointCallback_tF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4249[2] = 
{
	MapPointCallback_tF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041::get_offset_of_m_endomorphism_0(),
	MapPointCallback_tF4FB02AEE6E67B7FEDFB2F8FADFDCF5AD0AA3041::get_offset_of_m_point_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (GlvTypeAEndomorphism_t5CBCE2BB72B965010603431FF2D55898ED56BFF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4251[2] = 
{
	GlvTypeAEndomorphism_t5CBCE2BB72B965010603431FF2D55898ED56BFF2::get_offset_of_m_parameters_0(),
	GlvTypeAEndomorphism_t5CBCE2BB72B965010603431FF2D55898ED56BFF2::get_offset_of_m_pointMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4252[3] = 
{
	GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A::get_offset_of_m_i_0(),
	GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A::get_offset_of_m_lambda_1(),
	GlvTypeAParameters_t583529F26DBA710100F5BBF006F57FC7E417182A::get_offset_of_m_splitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (GlvTypeBEndomorphism_tCB74CC801D3F994295F2C2B85918F1D61DB8B27C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4253[2] = 
{
	GlvTypeBEndomorphism_tCB74CC801D3F994295F2C2B85918F1D61DB8B27C::get_offset_of_m_parameters_0(),
	GlvTypeBEndomorphism_tCB74CC801D3F994295F2C2B85918F1D61DB8B27C::get_offset_of_m_pointMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4254[3] = 
{
	GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42::get_offset_of_m_beta_0(),
	GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42::get_offset_of_m_lambda_1(),
	GlvTypeBParameters_t586957DCF9357EEA3C441B8AFC53FC4425333C42::get_offset_of_m_splitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { sizeof (ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4255[7] = 
{
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_v1A_0(),
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_v1B_1(),
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_v2A_2(),
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_v2B_3(),
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_g1_4(),
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_g2_5(),
	ScalarSplitParameters_t4355E06028202D0D63851CBBD2FE50740FF969C0::get_offset_of_m_bits_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21), -1, sizeof(SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4256[5] = 
{
	SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21_StaticFields::get_offset_of_SECP128R1_AFFINE_ZS_19(),
	SecP128R1Curve_t60068AB54FA6C6421190D0457F7691AB8FE34E21::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[3] = 
{
	SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802::get_offset_of_m_outer_0(),
	SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802::get_offset_of_m_table_1(),
	SecP128R1LookupTable_t951705AE16226A32E2E572367C376863D0C47802::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD), -1, sizeof(SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4258[5] = 
{
	SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields::get_offset_of_P_0(),
	SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields::get_offset_of_PExt_1(),
	SecP128R1Field_t72C56051F197DDAAFF6840FCB6066C2A947D10AD_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5), -1, sizeof(SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4259[2] = 
{
	SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5_StaticFields::get_offset_of_Q_0(),
	SecP128R1FieldElement_tFE05D92EBAED9B9FF8ECDFD0D5C54483B3F936E5::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (SecP128R1Point_tE502A7CCAA399112E85268147FA9B0077B5E5687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6), -1, sizeof(SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4261[5] = 
{
	SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6_StaticFields::get_offset_of_SECP160K1_AFFINE_ZS_19(),
	SecP160K1Curve_tF4F4037EBF56135F71A5D08C6440A70E71C999A6::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4262[3] = 
{
	SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943::get_offset_of_m_outer_0(),
	SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943::get_offset_of_m_table_1(),
	SecP160K1LookupTable_t493CD6F83067335E224B9ACABC0E678F28B4B943::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { sizeof (SecP160K1Point_t68EDC31CED81A3B71E0CA21DA18433D09E8F44A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A), -1, sizeof(SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4264[5] = 
{
	SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A_StaticFields::get_offset_of_SECP160R1_AFFINE_ZS_19(),
	SecP160R1Curve_tB9B4255B13A3074070989EE3173B4C768CC8060A::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4265[3] = 
{
	SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333::get_offset_of_m_outer_0(),
	SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333::get_offset_of_m_table_1(),
	SecP160R1LookupTable_tCC551F6E330896DCED113E4F47674DEA275AE333::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617), -1, sizeof(SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4266[6] = 
{
	SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields::get_offset_of_P_0(),
	SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields::get_offset_of_PExt_1(),
	SecP160R1Field_t87442400516E736E9BBF16AA666645CA42850617_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39), -1, sizeof(SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4267[2] = 
{
	SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39_StaticFields::get_offset_of_Q_0(),
	SecP160R1FieldElement_tB3E415AC5E3B8FADDA06960B80B08813978F6C39::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (SecP160R1Point_tA2BEAA3115E9B3CD419B377DBA64AC7D2A4EFA4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE), -1, sizeof(SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4269[5] = 
{
	SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE_StaticFields::get_offset_of_SECP160R2_AFFINE_ZS_19(),
	SecP160R2Curve_tECD88B4A6CD190846ED50530D4ECC68125193ABE::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[3] = 
{
	SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6::get_offset_of_m_outer_0(),
	SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6::get_offset_of_m_table_1(),
	SecP160R2LookupTable_tF683D2A3196B1F3EDAF6EFF741229C5A289BE6D6::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934), -1, sizeof(SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4271[6] = 
{
	SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields::get_offset_of_P_0(),
	SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields::get_offset_of_PExt_1(),
	SecP160R2Field_tD36C9024533FC4D8B719DCAC402F402497A28934_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB), -1, sizeof(SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4272[2] = 
{
	SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB_StaticFields::get_offset_of_Q_0(),
	SecP160R2FieldElement_t9FC305BCF3D4C8196D957EF14424EB15859D56BB::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (SecP160R2Point_t66F02792EB3A40EFC3321D731C06D4C5699396E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA), -1, sizeof(SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4274[5] = 
{
	SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA_StaticFields::get_offset_of_SECP192K1_AFFINE_ZS_19(),
	SecP192K1Curve_tB39C2214C5E041221EC2168D3C47DD02B2166ECA::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4275[3] = 
{
	SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9::get_offset_of_m_outer_0(),
	SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9::get_offset_of_m_table_1(),
	SecP192K1LookupTable_t0712EB1745F2ABB598D1447525A61AD5F0732DE9::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC), -1, sizeof(SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4276[6] = 
{
	SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields::get_offset_of_P_0(),
	SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields::get_offset_of_PExt_1(),
	SecP192K1Field_t903D799295FED6C4B56C253812CEEDAB4A6441BC_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76), -1, sizeof(SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4277[2] = 
{
	SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76_StaticFields::get_offset_of_Q_0(),
	SecP192K1FieldElement_tE0651B52CD94FD31D88E0002D5C2FDC51C8F3E76::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (SecP192K1Point_t7DF47DA77FBF956DE2F2B1CD854346D42C08CA4B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C), -1, sizeof(SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4279[5] = 
{
	SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C_StaticFields::get_offset_of_SECP192R1_AFFINE_ZS_19(),
	SecP192R1Curve_tC88BE281CDCC0BF0AEF7AB5594A410B216037F2C::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4280[3] = 
{
	SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E::get_offset_of_m_outer_0(),
	SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E::get_offset_of_m_table_1(),
	SecP192R1LookupTable_t0147F00C0D31EC56DA790DCE737AC9DCB63E583E::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E), -1, sizeof(SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4281[5] = 
{
	SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields::get_offset_of_P_0(),
	SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields::get_offset_of_PExt_1(),
	SecP192R1Field_t595F31D32D6F3772167E931A6BADE81745E5DB2E_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651), -1, sizeof(SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4282[2] = 
{
	SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651_StaticFields::get_offset_of_Q_0(),
	SecP192R1FieldElement_tC4FDFCFD61D484D43E13376986740FDA6B694651::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (SecP192R1Point_t0FD2A2BD13651F3F7F50FAE0FA5F370D56CCA717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730), -1, sizeof(SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4284[5] = 
{
	SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730_StaticFields::get_offset_of_SECP224K1_AFFINE_ZS_19(),
	SecP224K1Curve_t7453474F1A35909AD0855804D058853ADD9A7730::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4285[3] = 
{
	SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD::get_offset_of_m_outer_0(),
	SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD::get_offset_of_m_table_1(),
	SecP224K1LookupTable_t74A6EE32041D273FE91DC9D6FA81E1C521A2CADD::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F), -1, sizeof(SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4286[6] = 
{
	SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields::get_offset_of_P_0(),
	SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields::get_offset_of_PExt_1(),
	SecP224K1Field_t0A3F8D5DACBF9E71C334853120F79445C9FC917F_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB), -1, sizeof(SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4287[3] = 
{
	SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_StaticFields::get_offset_of_Q_0(),
	SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	SecP224K1FieldElement_t9F3D1DA8083CE94F3474DF66F793DAD5E56D69CB::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (SecP224K1Point_t840B7E4E2F617FB6072201BC06E35573CA03F644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368), -1, sizeof(SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4289[5] = 
{
	SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368_StaticFields::get_offset_of_SECP224R1_AFFINE_ZS_19(),
	SecP224R1Curve_tF5183C0B43B8DF42D858BBC96DCB3E5217C5C368::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4290[3] = 
{
	SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048::get_offset_of_m_outer_0(),
	SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048::get_offset_of_m_table_1(),
	SecP224R1LookupTable_t1EF583DDB2347C2C07E8EB9647767F1F4BBC3048::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E), -1, sizeof(SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4291[5] = 
{
	SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields::get_offset_of_P_0(),
	SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields::get_offset_of_PExt_1(),
	SecP224R1Field_tBAD2D88E6B509CBA3824F01DB5D956899E0D8E8E_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF), -1, sizeof(SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4292[2] = 
{
	SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF_StaticFields::get_offset_of_Q_0(),
	SecP224R1FieldElement_t49FA6A9AF1C5F794E3F27F64ABEA9A50876602FF::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (SecP224R1Point_tDF195BDA5463725D7A1FFD305B59FF6DDF0B57FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9), -1, sizeof(SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4294[5] = 
{
	SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9_StaticFields::get_offset_of_SECP256K1_AFFINE_ZS_19(),
	SecP256K1Curve_t935C9FC03F4645100A7295EFFDB2D002C7E9CDD9::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { sizeof (SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4295[3] = 
{
	SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913::get_offset_of_m_outer_0(),
	SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913::get_offset_of_m_table_1(),
	SecP256K1LookupTable_t27195488E6BC0B9ECB6C81CC061193C8C5BBB913::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { sizeof (SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360), -1, sizeof(SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4296[6] = 
{
	SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields::get_offset_of_P_0(),
	SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields::get_offset_of_PExt_1(),
	SecP256K1Field_t60217AC582790D26D9FAC277CC7D09C365A2B360_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243), -1, sizeof(SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4297[2] = 
{
	SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243_StaticFields::get_offset_of_Q_0(),
	SecP256K1FieldElement_t9048E49BD768652D7D9B30E8DBB3CD6064834243::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (SecP256K1Point_t981F759B49333F954C0916E97600B3411C436063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2), -1, sizeof(SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4299[5] = 
{
	SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2_StaticFields::get_offset_of_SECP256R1_AFFINE_ZS_19(),
	SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2::get_offset_of_m_infinity_20(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
