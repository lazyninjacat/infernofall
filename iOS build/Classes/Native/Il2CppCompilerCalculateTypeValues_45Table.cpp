﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6VerifierGenerator
struct Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_t391586A9268E9DABF6C6158169C448C4243FA87C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest
struct IDigest_t4779036D052ECF08BAC3DF71ED71EF04D7866C09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac
struct IMac_tD02303ADA25C4494D9C2B119B486DF1D9302B33C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner
struct ISigner_tB728365C47647B1E736C0C715DACEBA9341F6CDD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher
struct IAeadBlockCipher_tB707B139ECFEF6D78BD117C85A84E2F2BD106B84;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters
struct DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters
struct ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters
struct Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_t3F0A2BB86CE5BD61188A9989250C19FDFF733F50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue
struct ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream
struct ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate
struct Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest
struct CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus
struct CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DatagramTransport
struct DatagramTransport_t331B8A7BD9D6BE4285058B77429EACB245868DD1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigestInputBuffer
struct DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch
struct DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsHandshakeRetransmit
struct DtlsHandshakeRetransmit_tC436A8F4AD2CEBA34B363A1ECBCA6E3EDF08D3BA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer
struct DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake
struct DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReplayWindow
struct DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ICertificateVerifyer
struct ICertificateVerifyer_t0BEEC3081E81604C8BE28A353578CCDF4657E202;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.IClientCredentialsProvider
struct IClientCredentialsProvider_t9EB3BF72C6EAC87294D093690BCC4B064FADC715;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream
struct RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream/SequenceNumber
struct SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters
struct SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters/Builder
struct Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm
struct SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials
struct TlsAgreementCredentials_t57C812A5666B726F477BEC8FD32D53619C251FE7;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAuthentication
struct TlsAuthentication_t3F79058469D179AE666FEAB27EF6C4A2C8C510F1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipher
struct TlsCipher_tBE58DC7FECBDF73DD0FC38FDCB1D0B366497CB0E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipherFactory
struct TlsCipherFactory_t0FC331767EE833BF93E6B80F483626F0819BFBB5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClient
struct TlsClient_tFF9228F9ECA37D116491670FB804DC9FC4286DBE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContext
struct TlsClientContext_tAE2C092193457C8B9EAEE44A2CCA3EAF262B251F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl
struct TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCloseable
struct TlsCloseable_tE72C78755E8271CE1D8F5ACA0F26F5C14D30D61C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCompression
struct TlsCompression_t64325B7826E69F34373F64D92DC1BC4299EE140F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_t31D810C946EFADA4C750C42CAD416485053A047D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCredentials
struct TlsCredentials_t4B23F404DEF4AFBFF83CD6944E4707850AD7B955;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHVerifier
struct TlsDHVerifier_tE6C6EA21D97C2F0DC88EA8814159E8DE25F35E79;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash
struct TlsHandshakeHash_t269142D360A0ACD923D2C83F53967EED0B8A7480;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange
struct TlsKeyExchange_t9A6C458E34724BE3E8CF27F0E4B078ED6E8AFAD4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac
struct TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPeer
struct TlsPeer_t15DD284CCDC7DC795281B30F7EF85ADECEFCD004;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentity
struct TlsPskIdentity_t75FBEF04811B54221A234971E8E3AD69BD07DF88;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentityManager
struct TlsPskIdentityManager_t79B5F93BAE2EB8BA0CA6111DA692D23C5D3A6559;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServer
struct TlsServer_tCCA6F54E1749B5C63EAE0E110719CA857B9EDE44;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContext
struct TlsServerContext_t40456A024D5F5C32225B02951995C4D8E01E1430;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContextImpl
struct TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession
struct TlsSession_t572BDC5B5EC5BC5CFD4950D91A4EB16A421845E1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner
struct TlsSigner_tEE9BB91F8DCC5DCBF2E82A0E0761BA1946BB9338;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSignerCredentials
struct TlsSignerCredentials_t59299E7EF0C06E47564C41703C1F6C4598BA3441;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpGroupVerifier
struct TlsSrpGroupVerifier_t1709E0D66B52B037A50FB462474BE2C3129CD8D9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpIdentityManager
struct TlsSrpIdentityManager_t700D1FC4F31771BB7E16C0DA863ACDB1FE81CAE9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpLoginParameters
struct TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStream
struct TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream
struct ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#define ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext
struct  AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mNonceRandom
	RuntimeObject* ___mNonceRandom_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecurityParameters
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * ___mSecurityParameters_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mClientVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mClientVersion_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mServerVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mServerVersion_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSession
	RuntimeObject* ___mSession_6;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mUserObject
	RuntimeObject * ___mUserObject_7;

public:
	inline static int32_t get_offset_of_mNonceRandom_1() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mNonceRandom_1)); }
	inline RuntimeObject* get_mNonceRandom_1() const { return ___mNonceRandom_1; }
	inline RuntimeObject** get_address_of_mNonceRandom_1() { return &___mNonceRandom_1; }
	inline void set_mNonceRandom_1(RuntimeObject* value)
	{
		___mNonceRandom_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonceRandom_1), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_2() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSecureRandom_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_2() const { return ___mSecureRandom_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_2() { return &___mSecureRandom_2; }
	inline void set_mSecureRandom_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_2), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_3() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSecurityParameters_3)); }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * get_mSecurityParameters_3() const { return ___mSecurityParameters_3; }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA ** get_address_of_mSecurityParameters_3() { return &___mSecurityParameters_3; }
	inline void set_mSecurityParameters_3(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * value)
	{
		___mSecurityParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_3), value);
	}

	inline static int32_t get_offset_of_mClientVersion_4() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mClientVersion_4)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mClientVersion_4() const { return ___mClientVersion_4; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mClientVersion_4() { return &___mClientVersion_4; }
	inline void set_mClientVersion_4(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mClientVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_4), value);
	}

	inline static int32_t get_offset_of_mServerVersion_5() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mServerVersion_5)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mServerVersion_5() const { return ___mServerVersion_5; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mServerVersion_5() { return &___mServerVersion_5; }
	inline void set_mServerVersion_5(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mServerVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_5), value);
	}

	inline static int32_t get_offset_of_mSession_6() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSession_6)); }
	inline RuntimeObject* get_mSession_6() const { return ___mSession_6; }
	inline RuntimeObject** get_address_of_mSession_6() { return &___mSession_6; }
	inline void set_mSession_6(RuntimeObject* value)
	{
		___mSession_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSession_6), value);
	}

	inline static int32_t get_offset_of_mUserObject_7() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mUserObject_7)); }
	inline RuntimeObject * get_mUserObject_7() const { return ___mUserObject_7; }
	inline RuntimeObject ** get_address_of_mUserObject_7() { return &___mUserObject_7; }
	inline void set_mUserObject_7(RuntimeObject * value)
	{
		___mUserObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___mUserObject_7), value);
	}
};

struct AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::counter
	int64_t ___counter_0;

public:
	inline static int32_t get_offset_of_counter_0() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields, ___counter_0)); }
	inline int64_t get_counter_0() const { return ___counter_0; }
	inline int64_t* get_address_of_counter_0() { return &___counter_0; }
	inline void set_counter_0(int64_t value)
	{
		___counter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#ifndef ABSTRACTTLSCREDENTIALS_TBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C_H
#define ABSTRACTTLSCREDENTIALS_TBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsCredentials
struct  AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCREDENTIALS_TBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C_H
#ifndef ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#define ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange
struct  AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mKeyExchange
	int32_t ___mKeyExchange_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mContext
	RuntimeObject* ___mContext_2;

public:
	inline static int32_t get_offset_of_mKeyExchange_0() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mKeyExchange_0)); }
	inline int32_t get_mKeyExchange_0() const { return ___mKeyExchange_0; }
	inline int32_t* get_address_of_mKeyExchange_0() { return &___mKeyExchange_0; }
	inline void set_mKeyExchange_0(int32_t value)
	{
		___mKeyExchange_0 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#ifndef ABSTRACTTLSPEER_T4CADCC4F7D80D3BA66C0422339E38604CBEC9638_H
#define ABSTRACTTLSPEER_T4CADCC4F7D80D3BA66C0422339E38604CBEC9638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer
struct  AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCloseable modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer::mCloseHandle
	RuntimeObject* ___mCloseHandle_0;

public:
	inline static int32_t get_offset_of_mCloseHandle_0() { return static_cast<int32_t>(offsetof(AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638, ___mCloseHandle_0)); }
	inline RuntimeObject* get_mCloseHandle_0() const { return ___mCloseHandle_0; }
	inline RuntimeObject** get_address_of_mCloseHandle_0() { return &___mCloseHandle_0; }
	inline void set_mCloseHandle_0(RuntimeObject* value)
	{
		___mCloseHandle_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCloseHandle_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSPEER_T4CADCC4F7D80D3BA66C0422339E38604CBEC9638_H
#ifndef ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#define ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner
struct  AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner::mContext
	RuntimeObject* ___mContext_0;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#ifndef DEFAULTTLSDHVERIFIER_T758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_H
#define DEFAULTTLSDHVERIFIER_T758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsDHVerifier
struct  DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsDHVerifier::mGroups
	RuntimeObject* ___mGroups_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsDHVerifier::mMinimumPrimeBits
	int32_t ___mMinimumPrimeBits_3;

public:
	inline static int32_t get_offset_of_mGroups_2() { return static_cast<int32_t>(offsetof(DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5, ___mGroups_2)); }
	inline RuntimeObject* get_mGroups_2() const { return ___mGroups_2; }
	inline RuntimeObject** get_address_of_mGroups_2() { return &___mGroups_2; }
	inline void set_mGroups_2(RuntimeObject* value)
	{
		___mGroups_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGroups_2), value);
	}

	inline static int32_t get_offset_of_mMinimumPrimeBits_3() { return static_cast<int32_t>(offsetof(DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5, ___mMinimumPrimeBits_3)); }
	inline int32_t get_mMinimumPrimeBits_3() const { return ___mMinimumPrimeBits_3; }
	inline int32_t* get_address_of_mMinimumPrimeBits_3() { return &___mMinimumPrimeBits_3; }
	inline void set_mMinimumPrimeBits_3(int32_t value)
	{
		___mMinimumPrimeBits_3 = value;
	}
};

struct DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsDHVerifier::DefaultMinimumPrimeBits
	int32_t ___DefaultMinimumPrimeBits_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsDHVerifier::DefaultGroups
	RuntimeObject* ___DefaultGroups_1;

public:
	inline static int32_t get_offset_of_DefaultMinimumPrimeBits_0() { return static_cast<int32_t>(offsetof(DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_StaticFields, ___DefaultMinimumPrimeBits_0)); }
	inline int32_t get_DefaultMinimumPrimeBits_0() const { return ___DefaultMinimumPrimeBits_0; }
	inline int32_t* get_address_of_DefaultMinimumPrimeBits_0() { return &___DefaultMinimumPrimeBits_0; }
	inline void set_DefaultMinimumPrimeBits_0(int32_t value)
	{
		___DefaultMinimumPrimeBits_0 = value;
	}

	inline static int32_t get_offset_of_DefaultGroups_1() { return static_cast<int32_t>(offsetof(DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_StaticFields, ___DefaultGroups_1)); }
	inline RuntimeObject* get_DefaultGroups_1() const { return ___DefaultGroups_1; }
	inline RuntimeObject** get_address_of_DefaultGroups_1() { return &___DefaultGroups_1; }
	inline void set_DefaultGroups_1(RuntimeObject* value)
	{
		___DefaultGroups_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultGroups_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSDHVERIFIER_T758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_H
#ifndef DEFAULTTLSSRPGROUPVERIFIER_TD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_H
#define DEFAULTTLSSRPGROUPVERIFIER_TD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSrpGroupVerifier
struct  DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSrpGroupVerifier::mGroups
	RuntimeObject* ___mGroups_1;

public:
	inline static int32_t get_offset_of_mGroups_1() { return static_cast<int32_t>(offsetof(DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51, ___mGroups_1)); }
	inline RuntimeObject* get_mGroups_1() const { return ___mGroups_1; }
	inline RuntimeObject** get_address_of_mGroups_1() { return &___mGroups_1; }
	inline void set_mGroups_1(RuntimeObject* value)
	{
		___mGroups_1 = value;
		Il2CppCodeGenWriteBarrier((&___mGroups_1), value);
	}
};

struct DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_StaticFields
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSrpGroupVerifier::DefaultGroups
	RuntimeObject* ___DefaultGroups_0;

public:
	inline static int32_t get_offset_of_DefaultGroups_0() { return static_cast<int32_t>(offsetof(DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_StaticFields, ___DefaultGroups_0)); }
	inline RuntimeObject* get_DefaultGroups_0() const { return ___DefaultGroups_0; }
	inline RuntimeObject** get_address_of_DefaultGroups_0() { return &___DefaultGroups_0; }
	inline void set_DefaultGroups_0(RuntimeObject* value)
	{
		___DefaultGroups_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultGroups_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSSRPGROUPVERIFIER_TD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_H
#ifndef DEFERREDHASH_T26F4928C27F9DCFB925F5FB13AE42335C518BC80_H
#define DEFERREDHASH_T26F4928C27F9DCFB925F5FB13AE42335C518BC80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DeferredHash
struct  DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DeferredHash::mContext
	RuntimeObject* ___mContext_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigestInputBuffer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DeferredHash::mBuf
	DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661 * ___mBuf_2;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DeferredHash::mHashes
	RuntimeObject* ___mHashes_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DeferredHash::mPrfHashAlgorithm
	int32_t ___mPrfHashAlgorithm_4;

public:
	inline static int32_t get_offset_of_mContext_1() { return static_cast<int32_t>(offsetof(DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80, ___mContext_1)); }
	inline RuntimeObject* get_mContext_1() const { return ___mContext_1; }
	inline RuntimeObject** get_address_of_mContext_1() { return &___mContext_1; }
	inline void set_mContext_1(RuntimeObject* value)
	{
		___mContext_1 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_1), value);
	}

	inline static int32_t get_offset_of_mBuf_2() { return static_cast<int32_t>(offsetof(DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80, ___mBuf_2)); }
	inline DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661 * get_mBuf_2() const { return ___mBuf_2; }
	inline DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661 ** get_address_of_mBuf_2() { return &___mBuf_2; }
	inline void set_mBuf_2(DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661 * value)
	{
		___mBuf_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBuf_2), value);
	}

	inline static int32_t get_offset_of_mHashes_3() { return static_cast<int32_t>(offsetof(DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80, ___mHashes_3)); }
	inline RuntimeObject* get_mHashes_3() const { return ___mHashes_3; }
	inline RuntimeObject** get_address_of_mHashes_3() { return &___mHashes_3; }
	inline void set_mHashes_3(RuntimeObject* value)
	{
		___mHashes_3 = value;
		Il2CppCodeGenWriteBarrier((&___mHashes_3), value);
	}

	inline static int32_t get_offset_of_mPrfHashAlgorithm_4() { return static_cast<int32_t>(offsetof(DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80, ___mPrfHashAlgorithm_4)); }
	inline int32_t get_mPrfHashAlgorithm_4() const { return ___mPrfHashAlgorithm_4; }
	inline int32_t* get_address_of_mPrfHashAlgorithm_4() { return &___mPrfHashAlgorithm_4; }
	inline void set_mPrfHashAlgorithm_4(int32_t value)
	{
		___mPrfHashAlgorithm_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFERREDHASH_T26F4928C27F9DCFB925F5FB13AE42335C518BC80_H
#ifndef DIGITALLYSIGNED_T85D6E9B59B93258F44E26C20EA73305E45407A8D_H
#define DIGITALLYSIGNED_T85D6E9B59B93258F44E26C20EA73305E45407A8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigitallySigned
struct  DigitallySigned_t85D6E9B59B93258F44E26C20EA73305E45407A8D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigitallySigned::mAlgorithm
	SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 * ___mAlgorithm_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigitallySigned::mSignature
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSignature_1;

public:
	inline static int32_t get_offset_of_mAlgorithm_0() { return static_cast<int32_t>(offsetof(DigitallySigned_t85D6E9B59B93258F44E26C20EA73305E45407A8D, ___mAlgorithm_0)); }
	inline SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 * get_mAlgorithm_0() const { return ___mAlgorithm_0; }
	inline SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 ** get_address_of_mAlgorithm_0() { return &___mAlgorithm_0; }
	inline void set_mAlgorithm_0(SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 * value)
	{
		___mAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___mAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_mSignature_1() { return static_cast<int32_t>(offsetof(DigitallySigned_t85D6E9B59B93258F44E26C20EA73305E45407A8D, ___mSignature_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSignature_1() const { return ___mSignature_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSignature_1() { return &___mSignature_1; }
	inline void set_mSignature_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSignature_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSignature_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALLYSIGNED_T85D6E9B59B93258F44E26C20EA73305E45407A8D_H
#ifndef CLIENTHANDSHAKESTATE_T2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996_H
#define CLIENTHANDSHAKESTATE_T2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState
struct  ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClient BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::client
	RuntimeObject* ___client_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::clientContext
	TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * ___clientContext_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::tlsSession
	RuntimeObject* ___tlsSession_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::sessionParameters
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * ___sessionParameters_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::sessionParametersBuilder
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 * ___sessionParametersBuilder_4;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::offeredCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___offeredCipherSuites_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::clientExtensions
	RuntimeObject* ___clientExtensions_6;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::serverExtensions
	RuntimeObject* ___serverExtensions_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::selectedSessionID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___selectedSessionID_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::resumedSession
	bool ___resumedSession_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::secure_renegotiation
	bool ___secure_renegotiation_10;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::allowCertificateStatus
	bool ___allowCertificateStatus_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::expectSessionTicket
	bool ___expectSessionTicket_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::keyExchange
	RuntimeObject* ___keyExchange_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAuthentication BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::authentication
	RuntimeObject* ___authentication_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::certificateStatus
	CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * ___certificateStatus_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::certificateRequest
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * ___certificateRequest_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol_ClientHandshakeState::clientCredentials
	RuntimeObject* ___clientCredentials_17;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_clientContext_1() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___clientContext_1)); }
	inline TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * get_clientContext_1() const { return ___clientContext_1; }
	inline TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 ** get_address_of_clientContext_1() { return &___clientContext_1; }
	inline void set_clientContext_1(TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * value)
	{
		___clientContext_1 = value;
		Il2CppCodeGenWriteBarrier((&___clientContext_1), value);
	}

	inline static int32_t get_offset_of_tlsSession_2() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___tlsSession_2)); }
	inline RuntimeObject* get_tlsSession_2() const { return ___tlsSession_2; }
	inline RuntimeObject** get_address_of_tlsSession_2() { return &___tlsSession_2; }
	inline void set_tlsSession_2(RuntimeObject* value)
	{
		___tlsSession_2 = value;
		Il2CppCodeGenWriteBarrier((&___tlsSession_2), value);
	}

	inline static int32_t get_offset_of_sessionParameters_3() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___sessionParameters_3)); }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * get_sessionParameters_3() const { return ___sessionParameters_3; }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC ** get_address_of_sessionParameters_3() { return &___sessionParameters_3; }
	inline void set_sessionParameters_3(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * value)
	{
		___sessionParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___sessionParameters_3), value);
	}

	inline static int32_t get_offset_of_sessionParametersBuilder_4() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___sessionParametersBuilder_4)); }
	inline Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 * get_sessionParametersBuilder_4() const { return ___sessionParametersBuilder_4; }
	inline Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 ** get_address_of_sessionParametersBuilder_4() { return &___sessionParametersBuilder_4; }
	inline void set_sessionParametersBuilder_4(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 * value)
	{
		___sessionParametersBuilder_4 = value;
		Il2CppCodeGenWriteBarrier((&___sessionParametersBuilder_4), value);
	}

	inline static int32_t get_offset_of_offeredCipherSuites_5() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___offeredCipherSuites_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_offeredCipherSuites_5() const { return ___offeredCipherSuites_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_offeredCipherSuites_5() { return &___offeredCipherSuites_5; }
	inline void set_offeredCipherSuites_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___offeredCipherSuites_5 = value;
		Il2CppCodeGenWriteBarrier((&___offeredCipherSuites_5), value);
	}

	inline static int32_t get_offset_of_clientExtensions_6() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___clientExtensions_6)); }
	inline RuntimeObject* get_clientExtensions_6() const { return ___clientExtensions_6; }
	inline RuntimeObject** get_address_of_clientExtensions_6() { return &___clientExtensions_6; }
	inline void set_clientExtensions_6(RuntimeObject* value)
	{
		___clientExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___clientExtensions_6), value);
	}

	inline static int32_t get_offset_of_serverExtensions_7() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___serverExtensions_7)); }
	inline RuntimeObject* get_serverExtensions_7() const { return ___serverExtensions_7; }
	inline RuntimeObject** get_address_of_serverExtensions_7() { return &___serverExtensions_7; }
	inline void set_serverExtensions_7(RuntimeObject* value)
	{
		___serverExtensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___serverExtensions_7), value);
	}

	inline static int32_t get_offset_of_selectedSessionID_8() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___selectedSessionID_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_selectedSessionID_8() const { return ___selectedSessionID_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_selectedSessionID_8() { return &___selectedSessionID_8; }
	inline void set_selectedSessionID_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___selectedSessionID_8 = value;
		Il2CppCodeGenWriteBarrier((&___selectedSessionID_8), value);
	}

	inline static int32_t get_offset_of_resumedSession_9() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___resumedSession_9)); }
	inline bool get_resumedSession_9() const { return ___resumedSession_9; }
	inline bool* get_address_of_resumedSession_9() { return &___resumedSession_9; }
	inline void set_resumedSession_9(bool value)
	{
		___resumedSession_9 = value;
	}

	inline static int32_t get_offset_of_secure_renegotiation_10() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___secure_renegotiation_10)); }
	inline bool get_secure_renegotiation_10() const { return ___secure_renegotiation_10; }
	inline bool* get_address_of_secure_renegotiation_10() { return &___secure_renegotiation_10; }
	inline void set_secure_renegotiation_10(bool value)
	{
		___secure_renegotiation_10 = value;
	}

	inline static int32_t get_offset_of_allowCertificateStatus_11() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___allowCertificateStatus_11)); }
	inline bool get_allowCertificateStatus_11() const { return ___allowCertificateStatus_11; }
	inline bool* get_address_of_allowCertificateStatus_11() { return &___allowCertificateStatus_11; }
	inline void set_allowCertificateStatus_11(bool value)
	{
		___allowCertificateStatus_11 = value;
	}

	inline static int32_t get_offset_of_expectSessionTicket_12() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___expectSessionTicket_12)); }
	inline bool get_expectSessionTicket_12() const { return ___expectSessionTicket_12; }
	inline bool* get_address_of_expectSessionTicket_12() { return &___expectSessionTicket_12; }
	inline void set_expectSessionTicket_12(bool value)
	{
		___expectSessionTicket_12 = value;
	}

	inline static int32_t get_offset_of_keyExchange_13() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___keyExchange_13)); }
	inline RuntimeObject* get_keyExchange_13() const { return ___keyExchange_13; }
	inline RuntimeObject** get_address_of_keyExchange_13() { return &___keyExchange_13; }
	inline void set_keyExchange_13(RuntimeObject* value)
	{
		___keyExchange_13 = value;
		Il2CppCodeGenWriteBarrier((&___keyExchange_13), value);
	}

	inline static int32_t get_offset_of_authentication_14() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___authentication_14)); }
	inline RuntimeObject* get_authentication_14() const { return ___authentication_14; }
	inline RuntimeObject** get_address_of_authentication_14() { return &___authentication_14; }
	inline void set_authentication_14(RuntimeObject* value)
	{
		___authentication_14 = value;
		Il2CppCodeGenWriteBarrier((&___authentication_14), value);
	}

	inline static int32_t get_offset_of_certificateStatus_15() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___certificateStatus_15)); }
	inline CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * get_certificateStatus_15() const { return ___certificateStatus_15; }
	inline CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB ** get_address_of_certificateStatus_15() { return &___certificateStatus_15; }
	inline void set_certificateStatus_15(CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * value)
	{
		___certificateStatus_15 = value;
		Il2CppCodeGenWriteBarrier((&___certificateStatus_15), value);
	}

	inline static int32_t get_offset_of_certificateRequest_16() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___certificateRequest_16)); }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * get_certificateRequest_16() const { return ___certificateRequest_16; }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 ** get_address_of_certificateRequest_16() { return &___certificateRequest_16; }
	inline void set_certificateRequest_16(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * value)
	{
		___certificateRequest_16 = value;
		Il2CppCodeGenWriteBarrier((&___certificateRequest_16), value);
	}

	inline static int32_t get_offset_of_clientCredentials_17() { return static_cast<int32_t>(offsetof(ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996, ___clientCredentials_17)); }
	inline RuntimeObject* get_clientCredentials_17() const { return ___clientCredentials_17; }
	inline RuntimeObject** get_address_of_clientCredentials_17() { return &___clientCredentials_17; }
	inline void set_clientCredentials_17(RuntimeObject* value)
	{
		___clientCredentials_17 = value;
		Il2CppCodeGenWriteBarrier((&___clientCredentials_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTHANDSHAKESTATE_T2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996_H
#ifndef DTLSEPOCH_T71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F_H
#define DTLSEPOCH_T71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch
struct  DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReplayWindow BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch::mReplayWindow
	DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C * ___mReplayWindow_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch::mEpoch
	int32_t ___mEpoch_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch::mCipher
	RuntimeObject* ___mCipher_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch::mSequenceNumber
	int64_t ___mSequenceNumber_3;

public:
	inline static int32_t get_offset_of_mReplayWindow_0() { return static_cast<int32_t>(offsetof(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F, ___mReplayWindow_0)); }
	inline DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C * get_mReplayWindow_0() const { return ___mReplayWindow_0; }
	inline DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C ** get_address_of_mReplayWindow_0() { return &___mReplayWindow_0; }
	inline void set_mReplayWindow_0(DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C * value)
	{
		___mReplayWindow_0 = value;
		Il2CppCodeGenWriteBarrier((&___mReplayWindow_0), value);
	}

	inline static int32_t get_offset_of_mEpoch_1() { return static_cast<int32_t>(offsetof(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F, ___mEpoch_1)); }
	inline int32_t get_mEpoch_1() const { return ___mEpoch_1; }
	inline int32_t* get_address_of_mEpoch_1() { return &___mEpoch_1; }
	inline void set_mEpoch_1(int32_t value)
	{
		___mEpoch_1 = value;
	}

	inline static int32_t get_offset_of_mCipher_2() { return static_cast<int32_t>(offsetof(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F, ___mCipher_2)); }
	inline RuntimeObject* get_mCipher_2() const { return ___mCipher_2; }
	inline RuntimeObject** get_address_of_mCipher_2() { return &___mCipher_2; }
	inline void set_mCipher_2(RuntimeObject* value)
	{
		___mCipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCipher_2), value);
	}

	inline static int32_t get_offset_of_mSequenceNumber_3() { return static_cast<int32_t>(offsetof(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F, ___mSequenceNumber_3)); }
	inline int64_t get_mSequenceNumber_3() const { return ___mSequenceNumber_3; }
	inline int64_t* get_address_of_mSequenceNumber_3() { return &___mSequenceNumber_3; }
	inline void set_mSequenceNumber_3(int64_t value)
	{
		___mSequenceNumber_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSEPOCH_T71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F_H
#ifndef DTLSPROTOCOL_T27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76_H
#define DTLSPROTOCOL_T27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsProtocol
struct  DtlsProtocol_t27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsProtocol::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_0;

public:
	inline static int32_t get_offset_of_mSecureRandom_0() { return static_cast<int32_t>(offsetof(DtlsProtocol_t27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76, ___mSecureRandom_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_0() const { return ___mSecureRandom_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_0() { return &___mSecureRandom_0; }
	inline void set_mSecureRandom_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSPROTOCOL_T27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76_H
#ifndef DTLSREASSEMBLER_T9AD275AAEA95FB96FDA350E61456423BF18CA0A1_H
#define DTLSREASSEMBLER_T9AD275AAEA95FB96FDA350E61456423BF18CA0A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler
struct  DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler::mMsgType
	uint8_t ___mMsgType_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler::mBody
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mBody_1;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler::mMissing
	RuntimeObject* ___mMissing_2;

public:
	inline static int32_t get_offset_of_mMsgType_0() { return static_cast<int32_t>(offsetof(DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1, ___mMsgType_0)); }
	inline uint8_t get_mMsgType_0() const { return ___mMsgType_0; }
	inline uint8_t* get_address_of_mMsgType_0() { return &___mMsgType_0; }
	inline void set_mMsgType_0(uint8_t value)
	{
		___mMsgType_0 = value;
	}

	inline static int32_t get_offset_of_mBody_1() { return static_cast<int32_t>(offsetof(DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1, ___mBody_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mBody_1() const { return ___mBody_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mBody_1() { return &___mBody_1; }
	inline void set_mBody_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mBody_1 = value;
		Il2CppCodeGenWriteBarrier((&___mBody_1), value);
	}

	inline static int32_t get_offset_of_mMissing_2() { return static_cast<int32_t>(offsetof(DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1, ___mMissing_2)); }
	inline RuntimeObject* get_mMissing_2() const { return ___mMissing_2; }
	inline RuntimeObject** get_address_of_mMissing_2() { return &___mMissing_2; }
	inline void set_mMissing_2(RuntimeObject* value)
	{
		___mMissing_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMissing_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSREASSEMBLER_T9AD275AAEA95FB96FDA350E61456423BF18CA0A1_H
#ifndef RANGE_T36ACB4181D3602DB43EAD01460CB7158C1DC8F83_H
#define RANGE_T36ACB4181D3602DB43EAD01460CB7158C1DC8F83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler_Range
struct  Range_t36ACB4181D3602DB43EAD01460CB7158C1DC8F83  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler_Range::mStart
	int32_t ___mStart_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReassembler_Range::mEnd
	int32_t ___mEnd_1;

public:
	inline static int32_t get_offset_of_mStart_0() { return static_cast<int32_t>(offsetof(Range_t36ACB4181D3602DB43EAD01460CB7158C1DC8F83, ___mStart_0)); }
	inline int32_t get_mStart_0() const { return ___mStart_0; }
	inline int32_t* get_address_of_mStart_0() { return &___mStart_0; }
	inline void set_mStart_0(int32_t value)
	{
		___mStart_0 = value;
	}

	inline static int32_t get_offset_of_mEnd_1() { return static_cast<int32_t>(offsetof(Range_t36ACB4181D3602DB43EAD01460CB7158C1DC8F83, ___mEnd_1)); }
	inline int32_t get_mEnd_1() const { return ___mEnd_1; }
	inline int32_t* get_address_of_mEnd_1() { return &___mEnd_1; }
	inline void set_mEnd_1(int32_t value)
	{
		___mEnd_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T36ACB4181D3602DB43EAD01460CB7158C1DC8F83_H
#ifndef DTLSRELIABLEHANDSHAKE_TFF56061C35E5A8905ED6FE6DA94D076B2A8B1082_H
#define DTLSRELIABLEHANDSHAKE_TFF56061C35E5A8905ED6FE6DA94D076B2A8B1082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake
struct  DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mRecordLayer
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC * ___mRecordLayer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mHandshakeHash
	RuntimeObject* ___mHandshakeHash_3;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mCurrentInboundFlight
	RuntimeObject* ___mCurrentInboundFlight_4;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mPreviousInboundFlight
	RuntimeObject* ___mPreviousInboundFlight_5;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mOutboundFlight
	RuntimeObject* ___mOutboundFlight_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mSending
	bool ___mSending_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mMessageSeq
	int32_t ___mMessageSeq_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake::mNextReceiveSeq
	int32_t ___mNextReceiveSeq_9;

public:
	inline static int32_t get_offset_of_mRecordLayer_2() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mRecordLayer_2)); }
	inline DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC * get_mRecordLayer_2() const { return ___mRecordLayer_2; }
	inline DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC ** get_address_of_mRecordLayer_2() { return &___mRecordLayer_2; }
	inline void set_mRecordLayer_2(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC * value)
	{
		___mRecordLayer_2 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordLayer_2), value);
	}

	inline static int32_t get_offset_of_mHandshakeHash_3() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mHandshakeHash_3)); }
	inline RuntimeObject* get_mHandshakeHash_3() const { return ___mHandshakeHash_3; }
	inline RuntimeObject** get_address_of_mHandshakeHash_3() { return &___mHandshakeHash_3; }
	inline void set_mHandshakeHash_3(RuntimeObject* value)
	{
		___mHandshakeHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeHash_3), value);
	}

	inline static int32_t get_offset_of_mCurrentInboundFlight_4() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mCurrentInboundFlight_4)); }
	inline RuntimeObject* get_mCurrentInboundFlight_4() const { return ___mCurrentInboundFlight_4; }
	inline RuntimeObject** get_address_of_mCurrentInboundFlight_4() { return &___mCurrentInboundFlight_4; }
	inline void set_mCurrentInboundFlight_4(RuntimeObject* value)
	{
		___mCurrentInboundFlight_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCurrentInboundFlight_4), value);
	}

	inline static int32_t get_offset_of_mPreviousInboundFlight_5() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mPreviousInboundFlight_5)); }
	inline RuntimeObject* get_mPreviousInboundFlight_5() const { return ___mPreviousInboundFlight_5; }
	inline RuntimeObject** get_address_of_mPreviousInboundFlight_5() { return &___mPreviousInboundFlight_5; }
	inline void set_mPreviousInboundFlight_5(RuntimeObject* value)
	{
		___mPreviousInboundFlight_5 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviousInboundFlight_5), value);
	}

	inline static int32_t get_offset_of_mOutboundFlight_6() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mOutboundFlight_6)); }
	inline RuntimeObject* get_mOutboundFlight_6() const { return ___mOutboundFlight_6; }
	inline RuntimeObject** get_address_of_mOutboundFlight_6() { return &___mOutboundFlight_6; }
	inline void set_mOutboundFlight_6(RuntimeObject* value)
	{
		___mOutboundFlight_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOutboundFlight_6), value);
	}

	inline static int32_t get_offset_of_mSending_7() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mSending_7)); }
	inline bool get_mSending_7() const { return ___mSending_7; }
	inline bool* get_address_of_mSending_7() { return &___mSending_7; }
	inline void set_mSending_7(bool value)
	{
		___mSending_7 = value;
	}

	inline static int32_t get_offset_of_mMessageSeq_8() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mMessageSeq_8)); }
	inline int32_t get_mMessageSeq_8() const { return ___mMessageSeq_8; }
	inline int32_t* get_address_of_mMessageSeq_8() { return &___mMessageSeq_8; }
	inline void set_mMessageSeq_8(int32_t value)
	{
		___mMessageSeq_8 = value;
	}

	inline static int32_t get_offset_of_mNextReceiveSeq_9() { return static_cast<int32_t>(offsetof(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082, ___mNextReceiveSeq_9)); }
	inline int32_t get_mNextReceiveSeq_9() const { return ___mNextReceiveSeq_9; }
	inline int32_t* get_address_of_mNextReceiveSeq_9() { return &___mNextReceiveSeq_9; }
	inline void set_mNextReceiveSeq_9(int32_t value)
	{
		___mNextReceiveSeq_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSRELIABLEHANDSHAKE_TFF56061C35E5A8905ED6FE6DA94D076B2A8B1082_H
#ifndef MESSAGE_T6144210DA80B8DC1FFCC1922C94000031A5C7EE3_H
#define MESSAGE_T6144210DA80B8DC1FFCC1922C94000031A5C7EE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_Message
struct  Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_Message::mMessageSeq
	int32_t ___mMessageSeq_0;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_Message::mMsgType
	uint8_t ___mMsgType_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_Message::mBody
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mBody_2;

public:
	inline static int32_t get_offset_of_mMessageSeq_0() { return static_cast<int32_t>(offsetof(Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3, ___mMessageSeq_0)); }
	inline int32_t get_mMessageSeq_0() const { return ___mMessageSeq_0; }
	inline int32_t* get_address_of_mMessageSeq_0() { return &___mMessageSeq_0; }
	inline void set_mMessageSeq_0(int32_t value)
	{
		___mMessageSeq_0 = value;
	}

	inline static int32_t get_offset_of_mMsgType_1() { return static_cast<int32_t>(offsetof(Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3, ___mMsgType_1)); }
	inline uint8_t get_mMsgType_1() const { return ___mMsgType_1; }
	inline uint8_t* get_address_of_mMsgType_1() { return &___mMsgType_1; }
	inline void set_mMsgType_1(uint8_t value)
	{
		___mMsgType_1 = value;
	}

	inline static int32_t get_offset_of_mBody_2() { return static_cast<int32_t>(offsetof(Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3, ___mBody_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mBody_2() const { return ___mBody_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mBody_2() { return &___mBody_2; }
	inline void set_mBody_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mBody_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBody_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_T6144210DA80B8DC1FFCC1922C94000031A5C7EE3_H
#ifndef RETRANSMIT_T6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC_H
#define RETRANSMIT_T6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_Retransmit
struct  Retransmit_t6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_Retransmit::mOuter
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082 * ___mOuter_0;

public:
	inline static int32_t get_offset_of_mOuter_0() { return static_cast<int32_t>(offsetof(Retransmit_t6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC, ___mOuter_0)); }
	inline DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082 * get_mOuter_0() const { return ___mOuter_0; }
	inline DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082 ** get_address_of_mOuter_0() { return &___mOuter_0; }
	inline void set_mOuter_0(DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082 * value)
	{
		___mOuter_0 = value;
		Il2CppCodeGenWriteBarrier((&___mOuter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRANSMIT_T6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC_H
#ifndef DTLSREPLAYWINDOW_T205154583BA5F7E128BDED7524E33C94AB273A9C_H
#define DTLSREPLAYWINDOW_T205154583BA5F7E128BDED7524E33C94AB273A9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReplayWindow
struct  DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReplayWindow::mLatestConfirmedSeq
	int64_t ___mLatestConfirmedSeq_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReplayWindow::mBitmap
	int64_t ___mBitmap_3;

public:
	inline static int32_t get_offset_of_mLatestConfirmedSeq_2() { return static_cast<int32_t>(offsetof(DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C, ___mLatestConfirmedSeq_2)); }
	inline int64_t get_mLatestConfirmedSeq_2() const { return ___mLatestConfirmedSeq_2; }
	inline int64_t* get_address_of_mLatestConfirmedSeq_2() { return &___mLatestConfirmedSeq_2; }
	inline void set_mLatestConfirmedSeq_2(int64_t value)
	{
		___mLatestConfirmedSeq_2 = value;
	}

	inline static int32_t get_offset_of_mBitmap_3() { return static_cast<int32_t>(offsetof(DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C, ___mBitmap_3)); }
	inline int64_t get_mBitmap_3() const { return ___mBitmap_3; }
	inline int64_t* get_address_of_mBitmap_3() { return &___mBitmap_3; }
	inline void set_mBitmap_3(int64_t value)
	{
		___mBitmap_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSREPLAYWINDOW_T205154583BA5F7E128BDED7524E33C94AB273A9C_H
#ifndef SERVERHANDSHAKESTATE_TAD66F4DA91911EF994F76CD700F8DE037996AA16_H
#define SERVERHANDSHAKESTATE_TAD66F4DA91911EF994F76CD700F8DE037996AA16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState
struct  ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::server
	RuntimeObject* ___server_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContextImpl BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::serverContext
	TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE * ___serverContext_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::tlsSession
	RuntimeObject* ___tlsSession_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::sessionParameters
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * ___sessionParameters_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::sessionParametersBuilder
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 * ___sessionParametersBuilder_4;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::offeredCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___offeredCipherSuites_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::offeredCompressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___offeredCompressionMethods_6;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::clientExtensions
	RuntimeObject* ___clientExtensions_7;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::serverExtensions
	RuntimeObject* ___serverExtensions_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::resumedSession
	bool ___resumedSession_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::secure_renegotiation
	bool ___secure_renegotiation_10;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::allowCertificateStatus
	bool ___allowCertificateStatus_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::expectSessionTicket
	bool ___expectSessionTicket_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::keyExchange
	RuntimeObject* ___keyExchange_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::serverCredentials
	RuntimeObject* ___serverCredentials_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::certificateRequest
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * ___certificateRequest_15;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::clientCertificateType
	int16_t ___clientCertificateType_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol_ServerHandshakeState::clientCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___clientCertificate_17;

public:
	inline static int32_t get_offset_of_server_0() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___server_0)); }
	inline RuntimeObject* get_server_0() const { return ___server_0; }
	inline RuntimeObject** get_address_of_server_0() { return &___server_0; }
	inline void set_server_0(RuntimeObject* value)
	{
		___server_0 = value;
		Il2CppCodeGenWriteBarrier((&___server_0), value);
	}

	inline static int32_t get_offset_of_serverContext_1() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___serverContext_1)); }
	inline TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE * get_serverContext_1() const { return ___serverContext_1; }
	inline TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE ** get_address_of_serverContext_1() { return &___serverContext_1; }
	inline void set_serverContext_1(TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE * value)
	{
		___serverContext_1 = value;
		Il2CppCodeGenWriteBarrier((&___serverContext_1), value);
	}

	inline static int32_t get_offset_of_tlsSession_2() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___tlsSession_2)); }
	inline RuntimeObject* get_tlsSession_2() const { return ___tlsSession_2; }
	inline RuntimeObject** get_address_of_tlsSession_2() { return &___tlsSession_2; }
	inline void set_tlsSession_2(RuntimeObject* value)
	{
		___tlsSession_2 = value;
		Il2CppCodeGenWriteBarrier((&___tlsSession_2), value);
	}

	inline static int32_t get_offset_of_sessionParameters_3() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___sessionParameters_3)); }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * get_sessionParameters_3() const { return ___sessionParameters_3; }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC ** get_address_of_sessionParameters_3() { return &___sessionParameters_3; }
	inline void set_sessionParameters_3(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * value)
	{
		___sessionParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___sessionParameters_3), value);
	}

	inline static int32_t get_offset_of_sessionParametersBuilder_4() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___sessionParametersBuilder_4)); }
	inline Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 * get_sessionParametersBuilder_4() const { return ___sessionParametersBuilder_4; }
	inline Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 ** get_address_of_sessionParametersBuilder_4() { return &___sessionParametersBuilder_4; }
	inline void set_sessionParametersBuilder_4(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524 * value)
	{
		___sessionParametersBuilder_4 = value;
		Il2CppCodeGenWriteBarrier((&___sessionParametersBuilder_4), value);
	}

	inline static int32_t get_offset_of_offeredCipherSuites_5() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___offeredCipherSuites_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_offeredCipherSuites_5() const { return ___offeredCipherSuites_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_offeredCipherSuites_5() { return &___offeredCipherSuites_5; }
	inline void set_offeredCipherSuites_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___offeredCipherSuites_5 = value;
		Il2CppCodeGenWriteBarrier((&___offeredCipherSuites_5), value);
	}

	inline static int32_t get_offset_of_offeredCompressionMethods_6() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___offeredCompressionMethods_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_offeredCompressionMethods_6() const { return ___offeredCompressionMethods_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_offeredCompressionMethods_6() { return &___offeredCompressionMethods_6; }
	inline void set_offeredCompressionMethods_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___offeredCompressionMethods_6 = value;
		Il2CppCodeGenWriteBarrier((&___offeredCompressionMethods_6), value);
	}

	inline static int32_t get_offset_of_clientExtensions_7() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___clientExtensions_7)); }
	inline RuntimeObject* get_clientExtensions_7() const { return ___clientExtensions_7; }
	inline RuntimeObject** get_address_of_clientExtensions_7() { return &___clientExtensions_7; }
	inline void set_clientExtensions_7(RuntimeObject* value)
	{
		___clientExtensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___clientExtensions_7), value);
	}

	inline static int32_t get_offset_of_serverExtensions_8() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___serverExtensions_8)); }
	inline RuntimeObject* get_serverExtensions_8() const { return ___serverExtensions_8; }
	inline RuntimeObject** get_address_of_serverExtensions_8() { return &___serverExtensions_8; }
	inline void set_serverExtensions_8(RuntimeObject* value)
	{
		___serverExtensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___serverExtensions_8), value);
	}

	inline static int32_t get_offset_of_resumedSession_9() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___resumedSession_9)); }
	inline bool get_resumedSession_9() const { return ___resumedSession_9; }
	inline bool* get_address_of_resumedSession_9() { return &___resumedSession_9; }
	inline void set_resumedSession_9(bool value)
	{
		___resumedSession_9 = value;
	}

	inline static int32_t get_offset_of_secure_renegotiation_10() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___secure_renegotiation_10)); }
	inline bool get_secure_renegotiation_10() const { return ___secure_renegotiation_10; }
	inline bool* get_address_of_secure_renegotiation_10() { return &___secure_renegotiation_10; }
	inline void set_secure_renegotiation_10(bool value)
	{
		___secure_renegotiation_10 = value;
	}

	inline static int32_t get_offset_of_allowCertificateStatus_11() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___allowCertificateStatus_11)); }
	inline bool get_allowCertificateStatus_11() const { return ___allowCertificateStatus_11; }
	inline bool* get_address_of_allowCertificateStatus_11() { return &___allowCertificateStatus_11; }
	inline void set_allowCertificateStatus_11(bool value)
	{
		___allowCertificateStatus_11 = value;
	}

	inline static int32_t get_offset_of_expectSessionTicket_12() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___expectSessionTicket_12)); }
	inline bool get_expectSessionTicket_12() const { return ___expectSessionTicket_12; }
	inline bool* get_address_of_expectSessionTicket_12() { return &___expectSessionTicket_12; }
	inline void set_expectSessionTicket_12(bool value)
	{
		___expectSessionTicket_12 = value;
	}

	inline static int32_t get_offset_of_keyExchange_13() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___keyExchange_13)); }
	inline RuntimeObject* get_keyExchange_13() const { return ___keyExchange_13; }
	inline RuntimeObject** get_address_of_keyExchange_13() { return &___keyExchange_13; }
	inline void set_keyExchange_13(RuntimeObject* value)
	{
		___keyExchange_13 = value;
		Il2CppCodeGenWriteBarrier((&___keyExchange_13), value);
	}

	inline static int32_t get_offset_of_serverCredentials_14() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___serverCredentials_14)); }
	inline RuntimeObject* get_serverCredentials_14() const { return ___serverCredentials_14; }
	inline RuntimeObject** get_address_of_serverCredentials_14() { return &___serverCredentials_14; }
	inline void set_serverCredentials_14(RuntimeObject* value)
	{
		___serverCredentials_14 = value;
		Il2CppCodeGenWriteBarrier((&___serverCredentials_14), value);
	}

	inline static int32_t get_offset_of_certificateRequest_15() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___certificateRequest_15)); }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * get_certificateRequest_15() const { return ___certificateRequest_15; }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 ** get_address_of_certificateRequest_15() { return &___certificateRequest_15; }
	inline void set_certificateRequest_15(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * value)
	{
		___certificateRequest_15 = value;
		Il2CppCodeGenWriteBarrier((&___certificateRequest_15), value);
	}

	inline static int32_t get_offset_of_clientCertificateType_16() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___clientCertificateType_16)); }
	inline int16_t get_clientCertificateType_16() const { return ___clientCertificateType_16; }
	inline int16_t* get_address_of_clientCertificateType_16() { return &___clientCertificateType_16; }
	inline void set_clientCertificateType_16(int16_t value)
	{
		___clientCertificateType_16 = value;
	}

	inline static int32_t get_offset_of_clientCertificate_17() { return static_cast<int32_t>(offsetof(ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16, ___clientCertificate_17)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_clientCertificate_17() const { return ___clientCertificate_17; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_clientCertificate_17() { return &___clientCertificate_17; }
	inline void set_clientCertificate_17(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___clientCertificate_17 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificate_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERHANDSHAKESTATE_TAD66F4DA91911EF994F76CD700F8DE037996AA16_H
#ifndef DTLSTRANSPORT_TA7709949573CF90A024B57894F78605F2591BCD4_H
#define DTLSTRANSPORT_TA7709949573CF90A024B57894F78605F2591BCD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsTransport
struct  DtlsTransport_tA7709949573CF90A024B57894F78605F2591BCD4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsTransport::mRecordLayer
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC * ___mRecordLayer_0;

public:
	inline static int32_t get_offset_of_mRecordLayer_0() { return static_cast<int32_t>(offsetof(DtlsTransport_tA7709949573CF90A024B57894F78605F2591BCD4, ___mRecordLayer_0)); }
	inline DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC * get_mRecordLayer_0() const { return ___mRecordLayer_0; }
	inline DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC ** get_address_of_mRecordLayer_0() { return &___mRecordLayer_0; }
	inline void set_mRecordLayer_0(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC * value)
	{
		___mRecordLayer_0 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordLayer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSTRANSPORT_TA7709949573CF90A024B57894F78605F2591BCD4_H
#ifndef ECBASISTYPE_T0985027DBDAFA73665EC060C99F4997BCED1C858_H
#define ECBASISTYPE_T0985027DBDAFA73665EC060C99F4997BCED1C858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ECBasisType
struct  ECBasisType_t0985027DBDAFA73665EC060C99F4997BCED1C858  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECBASISTYPE_T0985027DBDAFA73665EC060C99F4997BCED1C858_H
#ifndef ECCURVETYPE_T36C8F053E5A2CC8A07F3E35239A257BA89606FB2_H
#define ECCURVETYPE_T36C8F053E5A2CC8A07F3E35239A257BA89606FB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ECCurveType
struct  ECCurveType_t36C8F053E5A2CC8A07F3E35239A257BA89606FB2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECCURVETYPE_T36C8F053E5A2CC8A07F3E35239A257BA89606FB2_H
#ifndef ECPOINTFORMAT_T5D3581724A6C6EBADD4BFE3541C23A41EFE94DEF_H
#define ECPOINTFORMAT_T5D3581724A6C6EBADD4BFE3541C23A41EFE94DEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ECPointFormat
struct  ECPointFormat_t5D3581724A6C6EBADD4BFE3541C23A41EFE94DEF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINTFORMAT_T5D3581724A6C6EBADD4BFE3541C23A41EFE94DEF_H
#ifndef ENCRYPTIONALGORITHM_T7391CF2B6E14F38EF5EA84A91549C484B9641251_H
#define ENCRYPTIONALGORITHM_T7391CF2B6E14F38EF5EA84A91549C484B9641251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.EncryptionAlgorithm
struct  EncryptionAlgorithm_t7391CF2B6E14F38EF5EA84A91549C484B9641251  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONALGORITHM_T7391CF2B6E14F38EF5EA84A91549C484B9641251_H
#ifndef EXPORTERLABEL_TD8CC03A7E7188DD15827E288B61C943C83E4FF1E_H
#define EXPORTERLABEL_TD8CC03A7E7188DD15827E288B61C943C83E4FF1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ExporterLabel
struct  ExporterLabel_tD8CC03A7E7188DD15827E288B61C943C83E4FF1E  : public RuntimeObject
{
public:

public:
};

struct ExporterLabel_tD8CC03A7E7188DD15827E288B61C943C83E4FF1E_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ExporterLabel::extended_master_secret
	String_t* ___extended_master_secret_8;

public:
	inline static int32_t get_offset_of_extended_master_secret_8() { return static_cast<int32_t>(offsetof(ExporterLabel_tD8CC03A7E7188DD15827E288B61C943C83E4FF1E_StaticFields, ___extended_master_secret_8)); }
	inline String_t* get_extended_master_secret_8() const { return ___extended_master_secret_8; }
	inline String_t** get_address_of_extended_master_secret_8() { return &___extended_master_secret_8; }
	inline void set_extended_master_secret_8(String_t* value)
	{
		___extended_master_secret_8 = value;
		Il2CppCodeGenWriteBarrier((&___extended_master_secret_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPORTERLABEL_TD8CC03A7E7188DD15827E288B61C943C83E4FF1E_H
#ifndef EXTENSIONTYPE_T0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_H
#define EXTENSIONTYPE_T0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ExtensionType
struct  ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9  : public RuntimeObject
{
public:

public:
};

struct ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ExtensionType::DRAFT_token_binding
	int32_t ___DRAFT_token_binding_25;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ExtensionType::negotiated_ff_dhe_groups
	int32_t ___negotiated_ff_dhe_groups_28;

public:
	inline static int32_t get_offset_of_DRAFT_token_binding_25() { return static_cast<int32_t>(offsetof(ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_StaticFields, ___DRAFT_token_binding_25)); }
	inline int32_t get_DRAFT_token_binding_25() const { return ___DRAFT_token_binding_25; }
	inline int32_t* get_address_of_DRAFT_token_binding_25() { return &___DRAFT_token_binding_25; }
	inline void set_DRAFT_token_binding_25(int32_t value)
	{
		___DRAFT_token_binding_25 = value;
	}

	inline static int32_t get_offset_of_negotiated_ff_dhe_groups_28() { return static_cast<int32_t>(offsetof(ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_StaticFields, ___negotiated_ff_dhe_groups_28)); }
	inline int32_t get_negotiated_ff_dhe_groups_28() const { return ___negotiated_ff_dhe_groups_28; }
	inline int32_t* get_address_of_negotiated_ff_dhe_groups_28() { return &___negotiated_ff_dhe_groups_28; }
	inline void set_negotiated_ff_dhe_groups_28(int32_t value)
	{
		___negotiated_ff_dhe_groups_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONTYPE_T0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_H
#ifndef FINITEFIELDDHEGROUP_T4CE0827C487C0B29D2273D772F92FD4CB777FCC3_H
#define FINITEFIELDDHEGROUP_T4CE0827C487C0B29D2273D772F92FD4CB777FCC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.FiniteFieldDheGroup
struct  FiniteFieldDheGroup_t4CE0827C487C0B29D2273D772F92FD4CB777FCC3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINITEFIELDDHEGROUP_T4CE0827C487C0B29D2273D772F92FD4CB777FCC3_H
#ifndef HANDSHAKETYPE_T4BA8E7DA9536F26D80DC7393C6BA2CE4770F1939_H
#define HANDSHAKETYPE_T4BA8E7DA9536F26D80DC7393C6BA2CE4770F1939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HandshakeType
struct  HandshakeType_t4BA8E7DA9536F26D80DC7393C6BA2CE4770F1939  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKETYPE_T4BA8E7DA9536F26D80DC7393C6BA2CE4770F1939_H
#ifndef HASHALGORITHM_T23CA04B8F7513B6FB35BD9BC0E15EA8313E50235_H
#define HASHALGORITHM_T23CA04B8F7513B6FB35BD9BC0E15EA8313E50235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HashAlgorithm
struct  HashAlgorithm_t23CA04B8F7513B6FB35BD9BC0E15EA8313E50235  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T23CA04B8F7513B6FB35BD9BC0E15EA8313E50235_H
#ifndef HEARTBEATEXTENSION_T444903CD5FC86CDCCC836B7C5B66050667DA6A2C_H
#define HEARTBEATEXTENSION_T444903CD5FC86CDCCC836B7C5B66050667DA6A2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatExtension
struct  HeartbeatExtension_t444903CD5FC86CDCCC836B7C5B66050667DA6A2C  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatExtension::mMode
	uint8_t ___mMode_0;

public:
	inline static int32_t get_offset_of_mMode_0() { return static_cast<int32_t>(offsetof(HeartbeatExtension_t444903CD5FC86CDCCC836B7C5B66050667DA6A2C, ___mMode_0)); }
	inline uint8_t get_mMode_0() const { return ___mMode_0; }
	inline uint8_t* get_address_of_mMode_0() { return &___mMode_0; }
	inline void set_mMode_0(uint8_t value)
	{
		___mMode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTBEATEXTENSION_T444903CD5FC86CDCCC836B7C5B66050667DA6A2C_H
#ifndef HEARTBEATMESSAGE_T0B339A92B1A5FA046A24CCD83178D9EE048F0198_H
#define HEARTBEATMESSAGE_T0B339A92B1A5FA046A24CCD83178D9EE048F0198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMessage
struct  HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMessage::mType
	uint8_t ___mType_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMessage::mPayload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPayload_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMessage::mPaddingLength
	int32_t ___mPaddingLength_2;

public:
	inline static int32_t get_offset_of_mType_0() { return static_cast<int32_t>(offsetof(HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198, ___mType_0)); }
	inline uint8_t get_mType_0() const { return ___mType_0; }
	inline uint8_t* get_address_of_mType_0() { return &___mType_0; }
	inline void set_mType_0(uint8_t value)
	{
		___mType_0 = value;
	}

	inline static int32_t get_offset_of_mPayload_1() { return static_cast<int32_t>(offsetof(HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198, ___mPayload_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPayload_1() const { return ___mPayload_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPayload_1() { return &___mPayload_1; }
	inline void set_mPayload_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPayload_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPayload_1), value);
	}

	inline static int32_t get_offset_of_mPaddingLength_2() { return static_cast<int32_t>(offsetof(HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198, ___mPaddingLength_2)); }
	inline int32_t get_mPaddingLength_2() const { return ___mPaddingLength_2; }
	inline int32_t* get_address_of_mPaddingLength_2() { return &___mPaddingLength_2; }
	inline void set_mPaddingLength_2(int32_t value)
	{
		___mPaddingLength_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTBEATMESSAGE_T0B339A92B1A5FA046A24CCD83178D9EE048F0198_H
#ifndef HEARTBEATMESSAGETYPE_TD0DB71717B8AFB5345E62F87D13D364BEE4FA72C_H
#define HEARTBEATMESSAGETYPE_TD0DB71717B8AFB5345E62F87D13D364BEE4FA72C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMessageType
struct  HeartbeatMessageType_tD0DB71717B8AFB5345E62F87D13D364BEE4FA72C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTBEATMESSAGETYPE_TD0DB71717B8AFB5345E62F87D13D364BEE4FA72C_H
#ifndef HEARTBEATMODE_TB95F20C85C7C4786DF7F5F329A15B6757C8AB618_H
#define HEARTBEATMODE_TB95F20C85C7C4786DF7F5F329A15B6757C8AB618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMode
struct  HeartbeatMode_tB95F20C85C7C4786DF7F5F329A15B6757C8AB618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTBEATMODE_TB95F20C85C7C4786DF7F5F329A15B6757C8AB618_H
#ifndef KEYEXCHANGEALGORITHM_T4432C2D0FF6BD00FDD6C268733B0404E60672DDE_H
#define KEYEXCHANGEALGORITHM_T4432C2D0FF6BD00FDD6C268733B0404E60672DDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.KeyExchangeAlgorithm
struct  KeyExchangeAlgorithm_t4432C2D0FF6BD00FDD6C268733B0404E60672DDE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEXCHANGEALGORITHM_T4432C2D0FF6BD00FDD6C268733B0404E60672DDE_H
#ifndef LEGACYTLSAUTHENTICATION_T110E4197E5A41ABF40FAE4B50ED443DA14026282_H
#define LEGACYTLSAUTHENTICATION_T110E4197E5A41ABF40FAE4B50ED443DA14026282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsAuthentication
struct  LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ICertificateVerifyer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsAuthentication::verifyer
	RuntimeObject* ___verifyer_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.IClientCredentialsProvider BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsAuthentication::credProvider
	RuntimeObject* ___credProvider_1;
	// System.Uri BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsAuthentication::TargetUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___TargetUri_2;

public:
	inline static int32_t get_offset_of_verifyer_0() { return static_cast<int32_t>(offsetof(LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282, ___verifyer_0)); }
	inline RuntimeObject* get_verifyer_0() const { return ___verifyer_0; }
	inline RuntimeObject** get_address_of_verifyer_0() { return &___verifyer_0; }
	inline void set_verifyer_0(RuntimeObject* value)
	{
		___verifyer_0 = value;
		Il2CppCodeGenWriteBarrier((&___verifyer_0), value);
	}

	inline static int32_t get_offset_of_credProvider_1() { return static_cast<int32_t>(offsetof(LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282, ___credProvider_1)); }
	inline RuntimeObject* get_credProvider_1() const { return ___credProvider_1; }
	inline RuntimeObject** get_address_of_credProvider_1() { return &___credProvider_1; }
	inline void set_credProvider_1(RuntimeObject* value)
	{
		___credProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___credProvider_1), value);
	}

	inline static int32_t get_offset_of_TargetUri_2() { return static_cast<int32_t>(offsetof(LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282, ___TargetUri_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_TargetUri_2() const { return ___TargetUri_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_TargetUri_2() { return &___TargetUri_2; }
	inline void set_TargetUri_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___TargetUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetUri_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYTLSAUTHENTICATION_T110E4197E5A41ABF40FAE4B50ED443DA14026282_H
#ifndef MACALGORITHM_T0A3E04B3D7C1C2106C764E1DC2E0DD3E8C602E52_H
#define MACALGORITHM_T0A3E04B3D7C1C2106C764E1DC2E0DD3E8C602E52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.MacAlgorithm
struct  MacAlgorithm_t0A3E04B3D7C1C2106C764E1DC2E0DD3E8C602E52  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACALGORITHM_T0A3E04B3D7C1C2106C764E1DC2E0DD3E8C602E52_H
#ifndef MAXFRAGMENTLENGTH_TCA3E065AC0A1B4BAB6A9888040CD54EE1DC04D96_H
#define MAXFRAGMENTLENGTH_TCA3E065AC0A1B4BAB6A9888040CD54EE1DC04D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.MaxFragmentLength
struct  MaxFragmentLength_tCA3E065AC0A1B4BAB6A9888040CD54EE1DC04D96  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXFRAGMENTLENGTH_TCA3E065AC0A1B4BAB6A9888040CD54EE1DC04D96_H
#ifndef NAMETYPE_T7FBF728941842E4B9126CDEBF63A21AF02DCFC9F_H
#define NAMETYPE_T7FBF728941842E4B9126CDEBF63A21AF02DCFC9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.NameType
struct  NameType_t7FBF728941842E4B9126CDEBF63A21AF02DCFC9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETYPE_T7FBF728941842E4B9126CDEBF63A21AF02DCFC9F_H
#ifndef NAMEDCURVE_TF3AB803EA45E9F8964D59419678F9D140C8C2C2F_H
#define NAMEDCURVE_TF3AB803EA45E9F8964D59419678F9D140C8C2C2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.NamedCurve
struct  NamedCurve_tF3AB803EA45E9F8964D59419678F9D140C8C2C2F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDCURVE_TF3AB803EA45E9F8964D59419678F9D140C8C2C2F_H
#ifndef NEWSESSIONTICKET_TD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD_H
#define NEWSESSIONTICKET_TD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.NewSessionTicket
struct  NewSessionTicket_tD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.NewSessionTicket::mTicketLifetimeHint
	int64_t ___mTicketLifetimeHint_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.NewSessionTicket::mTicket
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mTicket_1;

public:
	inline static int32_t get_offset_of_mTicketLifetimeHint_0() { return static_cast<int32_t>(offsetof(NewSessionTicket_tD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD, ___mTicketLifetimeHint_0)); }
	inline int64_t get_mTicketLifetimeHint_0() const { return ___mTicketLifetimeHint_0; }
	inline int64_t* get_address_of_mTicketLifetimeHint_0() { return &___mTicketLifetimeHint_0; }
	inline void set_mTicketLifetimeHint_0(int64_t value)
	{
		___mTicketLifetimeHint_0 = value;
	}

	inline static int32_t get_offset_of_mTicket_1() { return static_cast<int32_t>(offsetof(NewSessionTicket_tD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD, ___mTicket_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mTicket_1() const { return ___mTicket_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mTicket_1() { return &___mTicket_1; }
	inline void set_mTicket_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mTicket_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTicket_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWSESSIONTICKET_TD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD_H
#ifndef OCSPSTATUSREQUEST_T564046612A9E998EF7E0BCBB300A4371A9A1DC05_H
#define OCSPSTATUSREQUEST_T564046612A9E998EF7E0BCBB300A4371A9A1DC05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.OcspStatusRequest
struct  OcspStatusRequest_t564046612A9E998EF7E0BCBB300A4371A9A1DC05  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.OcspStatusRequest::mResponderIDList
	RuntimeObject* ___mResponderIDList_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.OcspStatusRequest::mRequestExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___mRequestExtensions_1;

public:
	inline static int32_t get_offset_of_mResponderIDList_0() { return static_cast<int32_t>(offsetof(OcspStatusRequest_t564046612A9E998EF7E0BCBB300A4371A9A1DC05, ___mResponderIDList_0)); }
	inline RuntimeObject* get_mResponderIDList_0() const { return ___mResponderIDList_0; }
	inline RuntimeObject** get_address_of_mResponderIDList_0() { return &___mResponderIDList_0; }
	inline void set_mResponderIDList_0(RuntimeObject* value)
	{
		___mResponderIDList_0 = value;
		Il2CppCodeGenWriteBarrier((&___mResponderIDList_0), value);
	}

	inline static int32_t get_offset_of_mRequestExtensions_1() { return static_cast<int32_t>(offsetof(OcspStatusRequest_t564046612A9E998EF7E0BCBB300A4371A9A1DC05, ___mRequestExtensions_1)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_mRequestExtensions_1() const { return ___mRequestExtensions_1; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_mRequestExtensions_1() { return &___mRequestExtensions_1; }
	inline void set_mRequestExtensions_1(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___mRequestExtensions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mRequestExtensions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPSTATUSREQUEST_T564046612A9E998EF7E0BCBB300A4371A9A1DC05_H
#ifndef PRFALGORITHM_T3CB4C47FCE4BD902E34302D37267779548D0A1C3_H
#define PRFALGORITHM_T3CB4C47FCE4BD902E34302D37267779548D0A1C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.PrfAlgorithm
struct  PrfAlgorithm_t3CB4C47FCE4BD902E34302D37267779548D0A1C3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRFALGORITHM_T3CB4C47FCE4BD902E34302D37267779548D0A1C3_H
#ifndef PROTOCOLVERSION_T235E146D3D32F8C5C3CF74B3BAD785DA46714450_H
#define PROTOCOLVERSION_T235E146D3D32F8C5C3CF74B3BAD785DA46714450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct  ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::version
	int32_t ___version_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_version_6() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450, ___version_6)); }
	inline int32_t get_version_6() const { return ___version_6; }
	inline int32_t* get_address_of_version_6() { return &___version_6; }
	inline void set_version_6(int32_t value)
	{
		___version_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

struct ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::SSLv3
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___SSLv3_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::TLSv10
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___TLSv10_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::TLSv11
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___TLSv11_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::TLSv12
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___TLSv12_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::DTLSv10
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___DTLSv10_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion::DTLSv12
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___DTLSv12_5;

public:
	inline static int32_t get_offset_of_SSLv3_0() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields, ___SSLv3_0)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_SSLv3_0() const { return ___SSLv3_0; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_SSLv3_0() { return &___SSLv3_0; }
	inline void set_SSLv3_0(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___SSLv3_0 = value;
		Il2CppCodeGenWriteBarrier((&___SSLv3_0), value);
	}

	inline static int32_t get_offset_of_TLSv10_1() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields, ___TLSv10_1)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_TLSv10_1() const { return ___TLSv10_1; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_TLSv10_1() { return &___TLSv10_1; }
	inline void set_TLSv10_1(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___TLSv10_1 = value;
		Il2CppCodeGenWriteBarrier((&___TLSv10_1), value);
	}

	inline static int32_t get_offset_of_TLSv11_2() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields, ___TLSv11_2)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_TLSv11_2() const { return ___TLSv11_2; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_TLSv11_2() { return &___TLSv11_2; }
	inline void set_TLSv11_2(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___TLSv11_2 = value;
		Il2CppCodeGenWriteBarrier((&___TLSv11_2), value);
	}

	inline static int32_t get_offset_of_TLSv12_3() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields, ___TLSv12_3)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_TLSv12_3() const { return ___TLSv12_3; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_TLSv12_3() { return &___TLSv12_3; }
	inline void set_TLSv12_3(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___TLSv12_3 = value;
		Il2CppCodeGenWriteBarrier((&___TLSv12_3), value);
	}

	inline static int32_t get_offset_of_DTLSv10_4() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields, ___DTLSv10_4)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_DTLSv10_4() const { return ___DTLSv10_4; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_DTLSv10_4() { return &___DTLSv10_4; }
	inline void set_DTLSv10_4(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___DTLSv10_4 = value;
		Il2CppCodeGenWriteBarrier((&___DTLSv10_4), value);
	}

	inline static int32_t get_offset_of_DTLSv12_5() { return static_cast<int32_t>(offsetof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields, ___DTLSv12_5)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_DTLSv12_5() const { return ___DTLSv12_5; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_DTLSv12_5() { return &___DTLSv12_5; }
	inline void set_DTLSv12_5(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___DTLSv12_5 = value;
		Il2CppCodeGenWriteBarrier((&___DTLSv12_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVERSION_T235E146D3D32F8C5C3CF74B3BAD785DA46714450_H
#ifndef RECORDSTREAM_TDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088_H
#define RECORDSTREAM_TDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream
struct  RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mHandler
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 * ___mHandler_5;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mInput
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___mInput_6;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mOutput
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___mOutput_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCompression BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mPendingCompression
	RuntimeObject* ___mPendingCompression_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCompression BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mReadCompression
	RuntimeObject* ___mReadCompression_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCompression BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteCompression
	RuntimeObject* ___mWriteCompression_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mPendingCipher
	RuntimeObject* ___mPendingCipher_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mReadCipher
	RuntimeObject* ___mReadCipher_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteCipher
	RuntimeObject* ___mWriteCipher_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_SequenceNumber BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mReadSeqNo
	SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 * ___mReadSeqNo_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_SequenceNumber BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteSeqNo
	SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 * ___mWriteSeqNo_15;
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mBuffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___mBuffer_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mHandshakeHash
	RuntimeObject* ___mHandshakeHash_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mHandshakeHashUpdater
	BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9 * ___mHandshakeHashUpdater_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mReadVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mReadVersion_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mWriteVersion_20;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mRestrictReadVersion
	bool ___mRestrictReadVersion_21;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mPlaintextLimit
	int32_t ___mPlaintextLimit_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mCompressedLimit
	int32_t ___mCompressedLimit_23;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream::mCiphertextLimit
	int32_t ___mCiphertextLimit_24;

public:
	inline static int32_t get_offset_of_mHandler_5() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mHandler_5)); }
	inline TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 * get_mHandler_5() const { return ___mHandler_5; }
	inline TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 ** get_address_of_mHandler_5() { return &___mHandler_5; }
	inline void set_mHandler_5(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 * value)
	{
		___mHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___mHandler_5), value);
	}

	inline static int32_t get_offset_of_mInput_6() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mInput_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_mInput_6() const { return ___mInput_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_mInput_6() { return &___mInput_6; }
	inline void set_mInput_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___mInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___mInput_6), value);
	}

	inline static int32_t get_offset_of_mOutput_7() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mOutput_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_mOutput_7() const { return ___mOutput_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_mOutput_7() { return &___mOutput_7; }
	inline void set_mOutput_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___mOutput_7 = value;
		Il2CppCodeGenWriteBarrier((&___mOutput_7), value);
	}

	inline static int32_t get_offset_of_mPendingCompression_8() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mPendingCompression_8)); }
	inline RuntimeObject* get_mPendingCompression_8() const { return ___mPendingCompression_8; }
	inline RuntimeObject** get_address_of_mPendingCompression_8() { return &___mPendingCompression_8; }
	inline void set_mPendingCompression_8(RuntimeObject* value)
	{
		___mPendingCompression_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPendingCompression_8), value);
	}

	inline static int32_t get_offset_of_mReadCompression_9() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mReadCompression_9)); }
	inline RuntimeObject* get_mReadCompression_9() const { return ___mReadCompression_9; }
	inline RuntimeObject** get_address_of_mReadCompression_9() { return &___mReadCompression_9; }
	inline void set_mReadCompression_9(RuntimeObject* value)
	{
		___mReadCompression_9 = value;
		Il2CppCodeGenWriteBarrier((&___mReadCompression_9), value);
	}

	inline static int32_t get_offset_of_mWriteCompression_10() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mWriteCompression_10)); }
	inline RuntimeObject* get_mWriteCompression_10() const { return ___mWriteCompression_10; }
	inline RuntimeObject** get_address_of_mWriteCompression_10() { return &___mWriteCompression_10; }
	inline void set_mWriteCompression_10(RuntimeObject* value)
	{
		___mWriteCompression_10 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteCompression_10), value);
	}

	inline static int32_t get_offset_of_mPendingCipher_11() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mPendingCipher_11)); }
	inline RuntimeObject* get_mPendingCipher_11() const { return ___mPendingCipher_11; }
	inline RuntimeObject** get_address_of_mPendingCipher_11() { return &___mPendingCipher_11; }
	inline void set_mPendingCipher_11(RuntimeObject* value)
	{
		___mPendingCipher_11 = value;
		Il2CppCodeGenWriteBarrier((&___mPendingCipher_11), value);
	}

	inline static int32_t get_offset_of_mReadCipher_12() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mReadCipher_12)); }
	inline RuntimeObject* get_mReadCipher_12() const { return ___mReadCipher_12; }
	inline RuntimeObject** get_address_of_mReadCipher_12() { return &___mReadCipher_12; }
	inline void set_mReadCipher_12(RuntimeObject* value)
	{
		___mReadCipher_12 = value;
		Il2CppCodeGenWriteBarrier((&___mReadCipher_12), value);
	}

	inline static int32_t get_offset_of_mWriteCipher_13() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mWriteCipher_13)); }
	inline RuntimeObject* get_mWriteCipher_13() const { return ___mWriteCipher_13; }
	inline RuntimeObject** get_address_of_mWriteCipher_13() { return &___mWriteCipher_13; }
	inline void set_mWriteCipher_13(RuntimeObject* value)
	{
		___mWriteCipher_13 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteCipher_13), value);
	}

	inline static int32_t get_offset_of_mReadSeqNo_14() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mReadSeqNo_14)); }
	inline SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 * get_mReadSeqNo_14() const { return ___mReadSeqNo_14; }
	inline SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 ** get_address_of_mReadSeqNo_14() { return &___mReadSeqNo_14; }
	inline void set_mReadSeqNo_14(SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 * value)
	{
		___mReadSeqNo_14 = value;
		Il2CppCodeGenWriteBarrier((&___mReadSeqNo_14), value);
	}

	inline static int32_t get_offset_of_mWriteSeqNo_15() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mWriteSeqNo_15)); }
	inline SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 * get_mWriteSeqNo_15() const { return ___mWriteSeqNo_15; }
	inline SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 ** get_address_of_mWriteSeqNo_15() { return &___mWriteSeqNo_15; }
	inline void set_mWriteSeqNo_15(SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2 * value)
	{
		___mWriteSeqNo_15 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteSeqNo_15), value);
	}

	inline static int32_t get_offset_of_mBuffer_16() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mBuffer_16)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_mBuffer_16() const { return ___mBuffer_16; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_mBuffer_16() { return &___mBuffer_16; }
	inline void set_mBuffer_16(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___mBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_16), value);
	}

	inline static int32_t get_offset_of_mHandshakeHash_17() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mHandshakeHash_17)); }
	inline RuntimeObject* get_mHandshakeHash_17() const { return ___mHandshakeHash_17; }
	inline RuntimeObject** get_address_of_mHandshakeHash_17() { return &___mHandshakeHash_17; }
	inline void set_mHandshakeHash_17(RuntimeObject* value)
	{
		___mHandshakeHash_17 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeHash_17), value);
	}

	inline static int32_t get_offset_of_mHandshakeHashUpdater_18() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mHandshakeHashUpdater_18)); }
	inline BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9 * get_mHandshakeHashUpdater_18() const { return ___mHandshakeHashUpdater_18; }
	inline BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9 ** get_address_of_mHandshakeHashUpdater_18() { return &___mHandshakeHashUpdater_18; }
	inline void set_mHandshakeHashUpdater_18(BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9 * value)
	{
		___mHandshakeHashUpdater_18 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeHashUpdater_18), value);
	}

	inline static int32_t get_offset_of_mReadVersion_19() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mReadVersion_19)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mReadVersion_19() const { return ___mReadVersion_19; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mReadVersion_19() { return &___mReadVersion_19; }
	inline void set_mReadVersion_19(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mReadVersion_19 = value;
		Il2CppCodeGenWriteBarrier((&___mReadVersion_19), value);
	}

	inline static int32_t get_offset_of_mWriteVersion_20() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mWriteVersion_20)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mWriteVersion_20() const { return ___mWriteVersion_20; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mWriteVersion_20() { return &___mWriteVersion_20; }
	inline void set_mWriteVersion_20(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mWriteVersion_20 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteVersion_20), value);
	}

	inline static int32_t get_offset_of_mRestrictReadVersion_21() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mRestrictReadVersion_21)); }
	inline bool get_mRestrictReadVersion_21() const { return ___mRestrictReadVersion_21; }
	inline bool* get_address_of_mRestrictReadVersion_21() { return &___mRestrictReadVersion_21; }
	inline void set_mRestrictReadVersion_21(bool value)
	{
		___mRestrictReadVersion_21 = value;
	}

	inline static int32_t get_offset_of_mPlaintextLimit_22() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mPlaintextLimit_22)); }
	inline int32_t get_mPlaintextLimit_22() const { return ___mPlaintextLimit_22; }
	inline int32_t* get_address_of_mPlaintextLimit_22() { return &___mPlaintextLimit_22; }
	inline void set_mPlaintextLimit_22(int32_t value)
	{
		___mPlaintextLimit_22 = value;
	}

	inline static int32_t get_offset_of_mCompressedLimit_23() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mCompressedLimit_23)); }
	inline int32_t get_mCompressedLimit_23() const { return ___mCompressedLimit_23; }
	inline int32_t* get_address_of_mCompressedLimit_23() { return &___mCompressedLimit_23; }
	inline void set_mCompressedLimit_23(int32_t value)
	{
		___mCompressedLimit_23 = value;
	}

	inline static int32_t get_offset_of_mCiphertextLimit_24() { return static_cast<int32_t>(offsetof(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088, ___mCiphertextLimit_24)); }
	inline int32_t get_mCiphertextLimit_24() const { return ___mCiphertextLimit_24; }
	inline int32_t* get_address_of_mCiphertextLimit_24() { return &___mCiphertextLimit_24; }
	inline void set_mCiphertextLimit_24(int32_t value)
	{
		___mCiphertextLimit_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDSTREAM_TDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088_H
#ifndef SEQUENCENUMBER_T2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2_H
#define SEQUENCENUMBER_T2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_SequenceNumber
struct  SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_SequenceNumber::value
	int64_t ___value_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_SequenceNumber::exhausted
	bool ___exhausted_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_exhausted_1() { return static_cast<int32_t>(offsetof(SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2, ___exhausted_1)); }
	inline bool get_exhausted_1() const { return ___exhausted_1; }
	inline bool* get_address_of_exhausted_1() { return &___exhausted_1; }
	inline void set_exhausted_1(bool value)
	{
		___exhausted_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCENUMBER_T2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2_H
#ifndef SECURITYPARAMETERS_T44C6864C78661F178B2CAFDBB37D1029E9BE6AFA_H
#define SECURITYPARAMETERS_T44C6864C78661F178B2CAFDBB37D1029E9BE6AFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct  SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::entity
	int32_t ___entity_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::cipherSuite
	int32_t ___cipherSuite_1;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::compressionAlgorithm
	uint8_t ___compressionAlgorithm_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::prfAlgorithm
	int32_t ___prfAlgorithm_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::verifyDataLength
	int32_t ___verifyDataLength_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::masterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___masterSecret_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::clientRandom
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clientRandom_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::serverRandom
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___serverRandom_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::sessionHash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sessionHash_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::pskIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pskIdentity_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::srpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___srpIdentity_10;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::maxFragmentLength
	int16_t ___maxFragmentLength_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::truncatedHMac
	bool ___truncatedHMac_12;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::encryptThenMac
	bool ___encryptThenMac_13;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters::extendedMasterSecret
	bool ___extendedMasterSecret_14;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___entity_0)); }
	inline int32_t get_entity_0() const { return ___entity_0; }
	inline int32_t* get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(int32_t value)
	{
		___entity_0 = value;
	}

	inline static int32_t get_offset_of_cipherSuite_1() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___cipherSuite_1)); }
	inline int32_t get_cipherSuite_1() const { return ___cipherSuite_1; }
	inline int32_t* get_address_of_cipherSuite_1() { return &___cipherSuite_1; }
	inline void set_cipherSuite_1(int32_t value)
	{
		___cipherSuite_1 = value;
	}

	inline static int32_t get_offset_of_compressionAlgorithm_2() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___compressionAlgorithm_2)); }
	inline uint8_t get_compressionAlgorithm_2() const { return ___compressionAlgorithm_2; }
	inline uint8_t* get_address_of_compressionAlgorithm_2() { return &___compressionAlgorithm_2; }
	inline void set_compressionAlgorithm_2(uint8_t value)
	{
		___compressionAlgorithm_2 = value;
	}

	inline static int32_t get_offset_of_prfAlgorithm_3() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___prfAlgorithm_3)); }
	inline int32_t get_prfAlgorithm_3() const { return ___prfAlgorithm_3; }
	inline int32_t* get_address_of_prfAlgorithm_3() { return &___prfAlgorithm_3; }
	inline void set_prfAlgorithm_3(int32_t value)
	{
		___prfAlgorithm_3 = value;
	}

	inline static int32_t get_offset_of_verifyDataLength_4() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___verifyDataLength_4)); }
	inline int32_t get_verifyDataLength_4() const { return ___verifyDataLength_4; }
	inline int32_t* get_address_of_verifyDataLength_4() { return &___verifyDataLength_4; }
	inline void set_verifyDataLength_4(int32_t value)
	{
		___verifyDataLength_4 = value;
	}

	inline static int32_t get_offset_of_masterSecret_5() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___masterSecret_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_masterSecret_5() const { return ___masterSecret_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_masterSecret_5() { return &___masterSecret_5; }
	inline void set_masterSecret_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___masterSecret_5 = value;
		Il2CppCodeGenWriteBarrier((&___masterSecret_5), value);
	}

	inline static int32_t get_offset_of_clientRandom_6() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___clientRandom_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clientRandom_6() const { return ___clientRandom_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clientRandom_6() { return &___clientRandom_6; }
	inline void set_clientRandom_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clientRandom_6 = value;
		Il2CppCodeGenWriteBarrier((&___clientRandom_6), value);
	}

	inline static int32_t get_offset_of_serverRandom_7() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___serverRandom_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_serverRandom_7() const { return ___serverRandom_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_serverRandom_7() { return &___serverRandom_7; }
	inline void set_serverRandom_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___serverRandom_7 = value;
		Il2CppCodeGenWriteBarrier((&___serverRandom_7), value);
	}

	inline static int32_t get_offset_of_sessionHash_8() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___sessionHash_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sessionHash_8() const { return ___sessionHash_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sessionHash_8() { return &___sessionHash_8; }
	inline void set_sessionHash_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sessionHash_8 = value;
		Il2CppCodeGenWriteBarrier((&___sessionHash_8), value);
	}

	inline static int32_t get_offset_of_pskIdentity_9() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___pskIdentity_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pskIdentity_9() const { return ___pskIdentity_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pskIdentity_9() { return &___pskIdentity_9; }
	inline void set_pskIdentity_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pskIdentity_9 = value;
		Il2CppCodeGenWriteBarrier((&___pskIdentity_9), value);
	}

	inline static int32_t get_offset_of_srpIdentity_10() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___srpIdentity_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_srpIdentity_10() const { return ___srpIdentity_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_srpIdentity_10() { return &___srpIdentity_10; }
	inline void set_srpIdentity_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___srpIdentity_10 = value;
		Il2CppCodeGenWriteBarrier((&___srpIdentity_10), value);
	}

	inline static int32_t get_offset_of_maxFragmentLength_11() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___maxFragmentLength_11)); }
	inline int16_t get_maxFragmentLength_11() const { return ___maxFragmentLength_11; }
	inline int16_t* get_address_of_maxFragmentLength_11() { return &___maxFragmentLength_11; }
	inline void set_maxFragmentLength_11(int16_t value)
	{
		___maxFragmentLength_11 = value;
	}

	inline static int32_t get_offset_of_truncatedHMac_12() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___truncatedHMac_12)); }
	inline bool get_truncatedHMac_12() const { return ___truncatedHMac_12; }
	inline bool* get_address_of_truncatedHMac_12() { return &___truncatedHMac_12; }
	inline void set_truncatedHMac_12(bool value)
	{
		___truncatedHMac_12 = value;
	}

	inline static int32_t get_offset_of_encryptThenMac_13() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___encryptThenMac_13)); }
	inline bool get_encryptThenMac_13() const { return ___encryptThenMac_13; }
	inline bool* get_address_of_encryptThenMac_13() { return &___encryptThenMac_13; }
	inline void set_encryptThenMac_13(bool value)
	{
		___encryptThenMac_13 = value;
	}

	inline static int32_t get_offset_of_extendedMasterSecret_14() { return static_cast<int32_t>(offsetof(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA, ___extendedMasterSecret_14)); }
	inline bool get_extendedMasterSecret_14() const { return ___extendedMasterSecret_14; }
	inline bool* get_address_of_extendedMasterSecret_14() { return &___extendedMasterSecret_14; }
	inline void set_extendedMasterSecret_14(bool value)
	{
		___extendedMasterSecret_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPARAMETERS_T44C6864C78661F178B2CAFDBB37D1029E9BE6AFA_H
#ifndef SERVERNAME_T84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1_H
#define SERVERNAME_T84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerName
struct  ServerName_t84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerName::mNameType
	uint8_t ___mNameType_0;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerName::mName
	RuntimeObject * ___mName_1;

public:
	inline static int32_t get_offset_of_mNameType_0() { return static_cast<int32_t>(offsetof(ServerName_t84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1, ___mNameType_0)); }
	inline uint8_t get_mNameType_0() const { return ___mNameType_0; }
	inline uint8_t* get_address_of_mNameType_0() { return &___mNameType_0; }
	inline void set_mNameType_0(uint8_t value)
	{
		___mNameType_0 = value;
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(ServerName_t84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1, ___mName_1)); }
	inline RuntimeObject * get_mName_1() const { return ___mName_1; }
	inline RuntimeObject ** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(RuntimeObject * value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERNAME_T84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1_H
#ifndef SERVERNAMELIST_TCCD2FC99552BA4447B057CA4ADE54CD2732A937E_H
#define SERVERNAMELIST_TCCD2FC99552BA4447B057CA4ADE54CD2732A937E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerNameList
struct  ServerNameList_tCCD2FC99552BA4447B057CA4ADE54CD2732A937E  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerNameList::mServerNameList
	RuntimeObject* ___mServerNameList_0;

public:
	inline static int32_t get_offset_of_mServerNameList_0() { return static_cast<int32_t>(offsetof(ServerNameList_tCCD2FC99552BA4447B057CA4ADE54CD2732A937E, ___mServerNameList_0)); }
	inline RuntimeObject* get_mServerNameList_0() const { return ___mServerNameList_0; }
	inline RuntimeObject** get_address_of_mServerNameList_0() { return &___mServerNameList_0; }
	inline void set_mServerNameList_0(RuntimeObject* value)
	{
		___mServerNameList_0 = value;
		Il2CppCodeGenWriteBarrier((&___mServerNameList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERNAMELIST_TCCD2FC99552BA4447B057CA4ADE54CD2732A937E_H
#ifndef SERVERONLYTLSAUTHENTICATION_T163D9B5BD47E1C5E5589B7C7D6E2E29F46E52E03_H
#define SERVERONLYTLSAUTHENTICATION_T163D9B5BD47E1C5E5589B7C7D6E2E29F46E52E03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerOnlyTlsAuthentication
struct  ServerOnlyTlsAuthentication_t163D9B5BD47E1C5E5589B7C7D6E2E29F46E52E03  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERONLYTLSAUTHENTICATION_T163D9B5BD47E1C5E5589B7C7D6E2E29F46E52E03_H
#ifndef SERVERSRPPARAMS_T4292841705ED7CEB4AC5C1EA40E89A5120F44C22_H
#define SERVERSRPPARAMS_T4292841705ED7CEB4AC5C1EA40E89A5120F44C22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerSrpParams
struct  ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerSrpParams::m_N
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_N_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerSrpParams::m_g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_g_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerSrpParams::m_B
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_B_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ServerSrpParams::m_s
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_s_3;

public:
	inline static int32_t get_offset_of_m_N_0() { return static_cast<int32_t>(offsetof(ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22, ___m_N_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_N_0() const { return ___m_N_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_N_0() { return &___m_N_0; }
	inline void set_m_N_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_N_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_N_0), value);
	}

	inline static int32_t get_offset_of_m_g_1() { return static_cast<int32_t>(offsetof(ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22, ___m_g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_g_1() const { return ___m_g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_g_1() { return &___m_g_1; }
	inline void set_m_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_g_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_g_1), value);
	}

	inline static int32_t get_offset_of_m_B_2() { return static_cast<int32_t>(offsetof(ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22, ___m_B_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_B_2() const { return ___m_B_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_B_2() { return &___m_B_2; }
	inline void set_m_B_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_B_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_B_2), value);
	}

	inline static int32_t get_offset_of_m_s_3() { return static_cast<int32_t>(offsetof(ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22, ___m_s_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_s_3() const { return ___m_s_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_s_3() { return &___m_s_3; }
	inline void set_m_s_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_s_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_s_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSRPPARAMS_T4292841705ED7CEB4AC5C1EA40E89A5120F44C22_H
#ifndef SESSIONPARAMETERS_TCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC_H
#define SESSIONPARAMETERS_TCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters
struct  SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mCipherSuite
	int32_t ___mCipherSuite_0;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mCompressionAlgorithm
	uint8_t ___mCompressionAlgorithm_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mMasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mMasterSecret_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mPeerCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mPeerCertificate_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mPskIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPskIdentity_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mSrpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSrpIdentity_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mEncodedServerExtensions
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mEncodedServerExtensions_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters::mExtendedMasterSecret
	bool ___mExtendedMasterSecret_7;

public:
	inline static int32_t get_offset_of_mCipherSuite_0() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mCipherSuite_0)); }
	inline int32_t get_mCipherSuite_0() const { return ___mCipherSuite_0; }
	inline int32_t* get_address_of_mCipherSuite_0() { return &___mCipherSuite_0; }
	inline void set_mCipherSuite_0(int32_t value)
	{
		___mCipherSuite_0 = value;
	}

	inline static int32_t get_offset_of_mCompressionAlgorithm_1() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mCompressionAlgorithm_1)); }
	inline uint8_t get_mCompressionAlgorithm_1() const { return ___mCompressionAlgorithm_1; }
	inline uint8_t* get_address_of_mCompressionAlgorithm_1() { return &___mCompressionAlgorithm_1; }
	inline void set_mCompressionAlgorithm_1(uint8_t value)
	{
		___mCompressionAlgorithm_1 = value;
	}

	inline static int32_t get_offset_of_mMasterSecret_2() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mMasterSecret_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mMasterSecret_2() const { return ___mMasterSecret_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mMasterSecret_2() { return &___mMasterSecret_2; }
	inline void set_mMasterSecret_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mMasterSecret_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMasterSecret_2), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_3() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mPeerCertificate_3)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mPeerCertificate_3() const { return ___mPeerCertificate_3; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mPeerCertificate_3() { return &___mPeerCertificate_3; }
	inline void set_mPeerCertificate_3(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mPeerCertificate_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_3), value);
	}

	inline static int32_t get_offset_of_mPskIdentity_4() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mPskIdentity_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPskIdentity_4() const { return ___mPskIdentity_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPskIdentity_4() { return &___mPskIdentity_4; }
	inline void set_mPskIdentity_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPskIdentity_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentity_4), value);
	}

	inline static int32_t get_offset_of_mSrpIdentity_5() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mSrpIdentity_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSrpIdentity_5() const { return ___mSrpIdentity_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSrpIdentity_5() { return &___mSrpIdentity_5; }
	inline void set_mSrpIdentity_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSrpIdentity_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpIdentity_5), value);
	}

	inline static int32_t get_offset_of_mEncodedServerExtensions_6() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mEncodedServerExtensions_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mEncodedServerExtensions_6() const { return ___mEncodedServerExtensions_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mEncodedServerExtensions_6() { return &___mEncodedServerExtensions_6; }
	inline void set_mEncodedServerExtensions_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mEncodedServerExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___mEncodedServerExtensions_6), value);
	}

	inline static int32_t get_offset_of_mExtendedMasterSecret_7() { return static_cast<int32_t>(offsetof(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC, ___mExtendedMasterSecret_7)); }
	inline bool get_mExtendedMasterSecret_7() const { return ___mExtendedMasterSecret_7; }
	inline bool* get_address_of_mExtendedMasterSecret_7() { return &___mExtendedMasterSecret_7; }
	inline void set_mExtendedMasterSecret_7(bool value)
	{
		___mExtendedMasterSecret_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONPARAMETERS_TCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC_H
#ifndef BUILDER_T05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524_H
#define BUILDER_T05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder
struct  Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mCipherSuite
	int32_t ___mCipherSuite_0;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mCompressionAlgorithm
	int16_t ___mCompressionAlgorithm_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mMasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mMasterSecret_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mPeerCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mPeerCertificate_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mPskIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPskIdentity_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mSrpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSrpIdentity_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mEncodedServerExtensions
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mEncodedServerExtensions_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mExtendedMasterSecret
	bool ___mExtendedMasterSecret_7;

public:
	inline static int32_t get_offset_of_mCipherSuite_0() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mCipherSuite_0)); }
	inline int32_t get_mCipherSuite_0() const { return ___mCipherSuite_0; }
	inline int32_t* get_address_of_mCipherSuite_0() { return &___mCipherSuite_0; }
	inline void set_mCipherSuite_0(int32_t value)
	{
		___mCipherSuite_0 = value;
	}

	inline static int32_t get_offset_of_mCompressionAlgorithm_1() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mCompressionAlgorithm_1)); }
	inline int16_t get_mCompressionAlgorithm_1() const { return ___mCompressionAlgorithm_1; }
	inline int16_t* get_address_of_mCompressionAlgorithm_1() { return &___mCompressionAlgorithm_1; }
	inline void set_mCompressionAlgorithm_1(int16_t value)
	{
		___mCompressionAlgorithm_1 = value;
	}

	inline static int32_t get_offset_of_mMasterSecret_2() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mMasterSecret_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mMasterSecret_2() const { return ___mMasterSecret_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mMasterSecret_2() { return &___mMasterSecret_2; }
	inline void set_mMasterSecret_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mMasterSecret_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMasterSecret_2), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_3() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mPeerCertificate_3)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mPeerCertificate_3() const { return ___mPeerCertificate_3; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mPeerCertificate_3() { return &___mPeerCertificate_3; }
	inline void set_mPeerCertificate_3(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mPeerCertificate_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_3), value);
	}

	inline static int32_t get_offset_of_mPskIdentity_4() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mPskIdentity_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPskIdentity_4() const { return ___mPskIdentity_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPskIdentity_4() { return &___mPskIdentity_4; }
	inline void set_mPskIdentity_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPskIdentity_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentity_4), value);
	}

	inline static int32_t get_offset_of_mSrpIdentity_5() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mSrpIdentity_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSrpIdentity_5() const { return ___mSrpIdentity_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSrpIdentity_5() { return &___mSrpIdentity_5; }
	inline void set_mSrpIdentity_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSrpIdentity_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpIdentity_5), value);
	}

	inline static int32_t get_offset_of_mEncodedServerExtensions_6() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mEncodedServerExtensions_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mEncodedServerExtensions_6() const { return ___mEncodedServerExtensions_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mEncodedServerExtensions_6() { return &___mEncodedServerExtensions_6; }
	inline void set_mEncodedServerExtensions_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mEncodedServerExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___mEncodedServerExtensions_6), value);
	}

	inline static int32_t get_offset_of_mExtendedMasterSecret_7() { return static_cast<int32_t>(offsetof(Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524, ___mExtendedMasterSecret_7)); }
	inline bool get_mExtendedMasterSecret_7() const { return ___mExtendedMasterSecret_7; }
	inline bool* get_address_of_mExtendedMasterSecret_7() { return &___mExtendedMasterSecret_7; }
	inline void set_mExtendedMasterSecret_7(bool value)
	{
		___mExtendedMasterSecret_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524_H
#ifndef SIGNATUREALGORITHM_TB005F7945F47B8B1C441511F99658F76EB5D2586_H
#define SIGNATUREALGORITHM_TB005F7945F47B8B1C441511F99658F76EB5D2586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAlgorithm
struct  SignatureAlgorithm_tB005F7945F47B8B1C441511F99658F76EB5D2586  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREALGORITHM_TB005F7945F47B8B1C441511F99658F76EB5D2586_H
#ifndef SIGNATUREANDHASHALGORITHM_TE68BB671672A1EE87744F89E78B4D68B5CEEF148_H
#define SIGNATUREANDHASHALGORITHM_TE68BB671672A1EE87744F89E78B4D68B5CEEF148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm
struct  SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm::mHash
	uint8_t ___mHash_0;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm::mSignature
	uint8_t ___mSignature_1;

public:
	inline static int32_t get_offset_of_mHash_0() { return static_cast<int32_t>(offsetof(SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148, ___mHash_0)); }
	inline uint8_t get_mHash_0() const { return ___mHash_0; }
	inline uint8_t* get_address_of_mHash_0() { return &___mHash_0; }
	inline void set_mHash_0(uint8_t value)
	{
		___mHash_0 = value;
	}

	inline static int32_t get_offset_of_mSignature_1() { return static_cast<int32_t>(offsetof(SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148, ___mSignature_1)); }
	inline uint8_t get_mSignature_1() const { return ___mSignature_1; }
	inline uint8_t* get_address_of_mSignature_1() { return &___mSignature_1; }
	inline void set_mSignature_1(uint8_t value)
	{
		___mSignature_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREANDHASHALGORITHM_TE68BB671672A1EE87744F89E78B4D68B5CEEF148_H
#ifndef SIMULATEDTLSSRPIDENTITYMANAGER_TC174941697F591C10F99C966071C3F0B89AC3FC1_H
#define SIMULATEDTLSSRPIDENTITYMANAGER_TC174941697F591C10F99C966071C3F0B89AC3FC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SimulatedTlsSrpIdentityManager
struct  SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SimulatedTlsSrpIdentityManager::mGroup
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___mGroup_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6VerifierGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SimulatedTlsSrpIdentityManager::mVerifierGenerator
	Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B * ___mVerifierGenerator_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SimulatedTlsSrpIdentityManager::mMac
	RuntimeObject* ___mMac_4;

public:
	inline static int32_t get_offset_of_mGroup_2() { return static_cast<int32_t>(offsetof(SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1, ___mGroup_2)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_mGroup_2() const { return ___mGroup_2; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_mGroup_2() { return &___mGroup_2; }
	inline void set_mGroup_2(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___mGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&___mGroup_2), value);
	}

	inline static int32_t get_offset_of_mVerifierGenerator_3() { return static_cast<int32_t>(offsetof(SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1, ___mVerifierGenerator_3)); }
	inline Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B * get_mVerifierGenerator_3() const { return ___mVerifierGenerator_3; }
	inline Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B ** get_address_of_mVerifierGenerator_3() { return &___mVerifierGenerator_3; }
	inline void set_mVerifierGenerator_3(Srp6VerifierGenerator_tCE1BA3373611D9CC6FC0427941958E183056766B * value)
	{
		___mVerifierGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVerifierGenerator_3), value);
	}

	inline static int32_t get_offset_of_mMac_4() { return static_cast<int32_t>(offsetof(SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1, ___mMac_4)); }
	inline RuntimeObject* get_mMac_4() const { return ___mMac_4; }
	inline RuntimeObject** get_address_of_mMac_4() { return &___mMac_4; }
	inline void set_mMac_4(RuntimeObject* value)
	{
		___mMac_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMac_4), value);
	}
};

struct SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SimulatedTlsSrpIdentityManager::PREFIX_PASSWORD
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___PREFIX_PASSWORD_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SimulatedTlsSrpIdentityManager::PREFIX_SALT
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___PREFIX_SALT_1;

public:
	inline static int32_t get_offset_of_PREFIX_PASSWORD_0() { return static_cast<int32_t>(offsetof(SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1_StaticFields, ___PREFIX_PASSWORD_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_PREFIX_PASSWORD_0() const { return ___PREFIX_PASSWORD_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_PREFIX_PASSWORD_0() { return &___PREFIX_PASSWORD_0; }
	inline void set_PREFIX_PASSWORD_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___PREFIX_PASSWORD_0 = value;
		Il2CppCodeGenWriteBarrier((&___PREFIX_PASSWORD_0), value);
	}

	inline static int32_t get_offset_of_PREFIX_SALT_1() { return static_cast<int32_t>(offsetof(SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1_StaticFields, ___PREFIX_SALT_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_PREFIX_SALT_1() const { return ___PREFIX_SALT_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_PREFIX_SALT_1() { return &___PREFIX_SALT_1; }
	inline void set_PREFIX_SALT_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___PREFIX_SALT_1 = value;
		Il2CppCodeGenWriteBarrier((&___PREFIX_SALT_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMULATEDTLSSRPIDENTITYMANAGER_TC174941697F591C10F99C966071C3F0B89AC3FC1_H
#ifndef SRTPPROTECTIONPROFILE_T18DAA3CED2E38323AF053D29060B51ABEA45D31A_H
#define SRTPPROTECTIONPROFILE_T18DAA3CED2E38323AF053D29060B51ABEA45D31A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrtpProtectionProfile
struct  SrtpProtectionProfile_t18DAA3CED2E38323AF053D29060B51ABEA45D31A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRTPPROTECTIONPROFILE_T18DAA3CED2E38323AF053D29060B51ABEA45D31A_H
#ifndef SSL3MAC_T9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_H
#define SSL3MAC_T9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Ssl3Mac
struct  Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Ssl3Mac::digest
	RuntimeObject* ___digest_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Ssl3Mac::padLength
	int32_t ___padLength_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Ssl3Mac::secret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___secret_6;

public:
	inline static int32_t get_offset_of_digest_4() { return static_cast<int32_t>(offsetof(Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6, ___digest_4)); }
	inline RuntimeObject* get_digest_4() const { return ___digest_4; }
	inline RuntimeObject** get_address_of_digest_4() { return &___digest_4; }
	inline void set_digest_4(RuntimeObject* value)
	{
		___digest_4 = value;
		Il2CppCodeGenWriteBarrier((&___digest_4), value);
	}

	inline static int32_t get_offset_of_padLength_5() { return static_cast<int32_t>(offsetof(Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6, ___padLength_5)); }
	inline int32_t get_padLength_5() const { return ___padLength_5; }
	inline int32_t* get_address_of_padLength_5() { return &___padLength_5; }
	inline void set_padLength_5(int32_t value)
	{
		___padLength_5 = value;
	}

	inline static int32_t get_offset_of_secret_6() { return static_cast<int32_t>(offsetof(Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6, ___secret_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_secret_6() const { return ___secret_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_secret_6() { return &___secret_6; }
	inline void set_secret_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___secret_6 = value;
		Il2CppCodeGenWriteBarrier((&___secret_6), value);
	}
};

struct Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Ssl3Mac::IPAD
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IPAD_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Ssl3Mac::OPAD
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OPAD_3;

public:
	inline static int32_t get_offset_of_IPAD_2() { return static_cast<int32_t>(offsetof(Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_StaticFields, ___IPAD_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IPAD_2() const { return ___IPAD_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IPAD_2() { return &___IPAD_2; }
	inline void set_IPAD_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IPAD_2 = value;
		Il2CppCodeGenWriteBarrier((&___IPAD_2), value);
	}

	inline static int32_t get_offset_of_OPAD_3() { return static_cast<int32_t>(offsetof(Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_StaticFields, ___OPAD_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OPAD_3() const { return ___OPAD_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OPAD_3() { return &___OPAD_3; }
	inline void set_OPAD_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OPAD_3 = value;
		Il2CppCodeGenWriteBarrier((&___OPAD_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSL3MAC_T9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_H
#ifndef SUPPLEMENTALDATAENTRY_TC9D387C5F209B97733AD8889F8A8AF73532AC1E5_H
#define SUPPLEMENTALDATAENTRY_TC9D387C5F209B97733AD8889F8A8AF73532AC1E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SupplementalDataEntry
struct  SupplementalDataEntry_tC9D387C5F209B97733AD8889F8A8AF73532AC1E5  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SupplementalDataEntry::mDataType
	int32_t ___mDataType_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SupplementalDataEntry::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_1;

public:
	inline static int32_t get_offset_of_mDataType_0() { return static_cast<int32_t>(offsetof(SupplementalDataEntry_tC9D387C5F209B97733AD8889F8A8AF73532AC1E5, ___mDataType_0)); }
	inline int32_t get_mDataType_0() const { return ___mDataType_0; }
	inline int32_t* get_address_of_mDataType_0() { return &___mDataType_0; }
	inline void set_mDataType_0(int32_t value)
	{
		___mDataType_0 = value;
	}

	inline static int32_t get_offset_of_mData_1() { return static_cast<int32_t>(offsetof(SupplementalDataEntry_tC9D387C5F209B97733AD8889F8A8AF73532AC1E5, ___mData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_1() const { return ___mData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_1() { return &___mData_1; }
	inline void set_mData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_1 = value;
		Il2CppCodeGenWriteBarrier((&___mData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPLEMENTALDATAENTRY_TC9D387C5F209B97733AD8889F8A8AF73532AC1E5_H
#ifndef SUPPLEMENTALDATATYPE_T3E47A54A05974E08048D412A6C904E158891386F_H
#define SUPPLEMENTALDATATYPE_T3E47A54A05974E08048D412A6C904E158891386F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SupplementalDataType
struct  SupplementalDataType_t3E47A54A05974E08048D412A6C904E158891386F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPLEMENTALDATATYPE_T3E47A54A05974E08048D412A6C904E158891386F_H
#ifndef TLSAEADCIPHER_T2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45_H
#define TLSAEADCIPHER_T2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher
struct  TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::context
	RuntimeObject* ___context_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::macSize
	int32_t ___macSize_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::record_iv_length
	int32_t ___record_iv_length_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::encryptCipher
	RuntimeObject* ___encryptCipher_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::decryptCipher
	RuntimeObject* ___decryptCipher_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::encryptImplicitNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encryptImplicitNonce_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::decryptImplicitNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___decryptImplicitNonce_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::nonceMode
	int32_t ___nonceMode_9;

public:
	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___context_2)); }
	inline RuntimeObject* get_context_2() const { return ___context_2; }
	inline RuntimeObject** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(RuntimeObject* value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_macSize_3() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___macSize_3)); }
	inline int32_t get_macSize_3() const { return ___macSize_3; }
	inline int32_t* get_address_of_macSize_3() { return &___macSize_3; }
	inline void set_macSize_3(int32_t value)
	{
		___macSize_3 = value;
	}

	inline static int32_t get_offset_of_record_iv_length_4() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___record_iv_length_4)); }
	inline int32_t get_record_iv_length_4() const { return ___record_iv_length_4; }
	inline int32_t* get_address_of_record_iv_length_4() { return &___record_iv_length_4; }
	inline void set_record_iv_length_4(int32_t value)
	{
		___record_iv_length_4 = value;
	}

	inline static int32_t get_offset_of_encryptCipher_5() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___encryptCipher_5)); }
	inline RuntimeObject* get_encryptCipher_5() const { return ___encryptCipher_5; }
	inline RuntimeObject** get_address_of_encryptCipher_5() { return &___encryptCipher_5; }
	inline void set_encryptCipher_5(RuntimeObject* value)
	{
		___encryptCipher_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_5), value);
	}

	inline static int32_t get_offset_of_decryptCipher_6() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___decryptCipher_6)); }
	inline RuntimeObject* get_decryptCipher_6() const { return ___decryptCipher_6; }
	inline RuntimeObject** get_address_of_decryptCipher_6() { return &___decryptCipher_6; }
	inline void set_decryptCipher_6(RuntimeObject* value)
	{
		___decryptCipher_6 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_6), value);
	}

	inline static int32_t get_offset_of_encryptImplicitNonce_7() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___encryptImplicitNonce_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encryptImplicitNonce_7() const { return ___encryptImplicitNonce_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encryptImplicitNonce_7() { return &___encryptImplicitNonce_7; }
	inline void set_encryptImplicitNonce_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encryptImplicitNonce_7 = value;
		Il2CppCodeGenWriteBarrier((&___encryptImplicitNonce_7), value);
	}

	inline static int32_t get_offset_of_decryptImplicitNonce_8() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___decryptImplicitNonce_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_decryptImplicitNonce_8() const { return ___decryptImplicitNonce_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_decryptImplicitNonce_8() { return &___decryptImplicitNonce_8; }
	inline void set_decryptImplicitNonce_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___decryptImplicitNonce_8 = value;
		Il2CppCodeGenWriteBarrier((&___decryptImplicitNonce_8), value);
	}

	inline static int32_t get_offset_of_nonceMode_9() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45, ___nonceMode_9)); }
	inline int32_t get_nonceMode_9() const { return ___nonceMode_9; }
	inline int32_t* get_address_of_nonceMode_9() { return &___nonceMode_9; }
	inline void set_nonceMode_9(int32_t value)
	{
		___nonceMode_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSAEADCIPHER_T2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45_H
#ifndef TLSBLOCKCIPHER_T47C84E1200534056DC7942C00E7E68B884EA0E73_H
#define TLSBLOCKCIPHER_T47C84E1200534056DC7942C00E7E68B884EA0E73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher
struct  TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::context
	RuntimeObject* ___context_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::randomData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___randomData_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::useExplicitIV
	bool ___useExplicitIV_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::encryptThenMac
	bool ___encryptThenMac_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::encryptCipher
	RuntimeObject* ___encryptCipher_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::decryptCipher
	RuntimeObject* ___decryptCipher_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::mWriteMac
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * ___mWriteMac_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::mReadMac
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * ___mReadMac_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::explicitIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___explicitIV_8;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_randomData_1() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___randomData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_randomData_1() const { return ___randomData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_randomData_1() { return &___randomData_1; }
	inline void set_randomData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___randomData_1 = value;
		Il2CppCodeGenWriteBarrier((&___randomData_1), value);
	}

	inline static int32_t get_offset_of_useExplicitIV_2() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___useExplicitIV_2)); }
	inline bool get_useExplicitIV_2() const { return ___useExplicitIV_2; }
	inline bool* get_address_of_useExplicitIV_2() { return &___useExplicitIV_2; }
	inline void set_useExplicitIV_2(bool value)
	{
		___useExplicitIV_2 = value;
	}

	inline static int32_t get_offset_of_encryptThenMac_3() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___encryptThenMac_3)); }
	inline bool get_encryptThenMac_3() const { return ___encryptThenMac_3; }
	inline bool* get_address_of_encryptThenMac_3() { return &___encryptThenMac_3; }
	inline void set_encryptThenMac_3(bool value)
	{
		___encryptThenMac_3 = value;
	}

	inline static int32_t get_offset_of_encryptCipher_4() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___encryptCipher_4)); }
	inline RuntimeObject* get_encryptCipher_4() const { return ___encryptCipher_4; }
	inline RuntimeObject** get_address_of_encryptCipher_4() { return &___encryptCipher_4; }
	inline void set_encryptCipher_4(RuntimeObject* value)
	{
		___encryptCipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_4), value);
	}

	inline static int32_t get_offset_of_decryptCipher_5() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___decryptCipher_5)); }
	inline RuntimeObject* get_decryptCipher_5() const { return ___decryptCipher_5; }
	inline RuntimeObject** get_address_of_decryptCipher_5() { return &___decryptCipher_5; }
	inline void set_decryptCipher_5(RuntimeObject* value)
	{
		___decryptCipher_5 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_5), value);
	}

	inline static int32_t get_offset_of_mWriteMac_6() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___mWriteMac_6)); }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * get_mWriteMac_6() const { return ___mWriteMac_6; }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E ** get_address_of_mWriteMac_6() { return &___mWriteMac_6; }
	inline void set_mWriteMac_6(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * value)
	{
		___mWriteMac_6 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteMac_6), value);
	}

	inline static int32_t get_offset_of_mReadMac_7() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___mReadMac_7)); }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * get_mReadMac_7() const { return ___mReadMac_7; }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E ** get_address_of_mReadMac_7() { return &___mReadMac_7; }
	inline void set_mReadMac_7(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * value)
	{
		___mReadMac_7 = value;
		Il2CppCodeGenWriteBarrier((&___mReadMac_7), value);
	}

	inline static int32_t get_offset_of_explicitIV_8() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73, ___explicitIV_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_explicitIV_8() const { return ___explicitIV_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_explicitIV_8() { return &___explicitIV_8; }
	inline void set_explicitIV_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___explicitIV_8 = value;
		Il2CppCodeGenWriteBarrier((&___explicitIV_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSBLOCKCIPHER_T47C84E1200534056DC7942C00E7E68B884EA0E73_H
#ifndef TLSDHUTILITIES_T1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_H
#define TLSDHUTILITIES_T1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities
struct  TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC  : public RuntimeObject
{
public:

public:
};

struct TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::Two
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Two_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe2432_p
	String_t* ___draft_ffdhe2432_p_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe2432
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___draft_ffdhe2432_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe3072_p
	String_t* ___draft_ffdhe3072_p_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe3072
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___draft_ffdhe3072_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe4096_p
	String_t* ___draft_ffdhe4096_p_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe4096
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___draft_ffdhe4096_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe6144_p
	String_t* ___draft_ffdhe6144_p_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe6144
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___draft_ffdhe6144_8;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe8192_p
	String_t* ___draft_ffdhe8192_p_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe8192
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___draft_ffdhe8192_10;

public:
	inline static int32_t get_offset_of_Two_0() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___Two_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Two_0() const { return ___Two_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Two_0() { return &___Two_0; }
	inline void set_Two_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Two_0 = value;
		Il2CppCodeGenWriteBarrier((&___Two_0), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe2432_p_1() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe2432_p_1)); }
	inline String_t* get_draft_ffdhe2432_p_1() const { return ___draft_ffdhe2432_p_1; }
	inline String_t** get_address_of_draft_ffdhe2432_p_1() { return &___draft_ffdhe2432_p_1; }
	inline void set_draft_ffdhe2432_p_1(String_t* value)
	{
		___draft_ffdhe2432_p_1 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe2432_p_1), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe2432_2() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe2432_2)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_draft_ffdhe2432_2() const { return ___draft_ffdhe2432_2; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_draft_ffdhe2432_2() { return &___draft_ffdhe2432_2; }
	inline void set_draft_ffdhe2432_2(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___draft_ffdhe2432_2 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe2432_2), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe3072_p_3() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe3072_p_3)); }
	inline String_t* get_draft_ffdhe3072_p_3() const { return ___draft_ffdhe3072_p_3; }
	inline String_t** get_address_of_draft_ffdhe3072_p_3() { return &___draft_ffdhe3072_p_3; }
	inline void set_draft_ffdhe3072_p_3(String_t* value)
	{
		___draft_ffdhe3072_p_3 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe3072_p_3), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe3072_4() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe3072_4)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_draft_ffdhe3072_4() const { return ___draft_ffdhe3072_4; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_draft_ffdhe3072_4() { return &___draft_ffdhe3072_4; }
	inline void set_draft_ffdhe3072_4(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___draft_ffdhe3072_4 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe3072_4), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe4096_p_5() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe4096_p_5)); }
	inline String_t* get_draft_ffdhe4096_p_5() const { return ___draft_ffdhe4096_p_5; }
	inline String_t** get_address_of_draft_ffdhe4096_p_5() { return &___draft_ffdhe4096_p_5; }
	inline void set_draft_ffdhe4096_p_5(String_t* value)
	{
		___draft_ffdhe4096_p_5 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe4096_p_5), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe4096_6() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe4096_6)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_draft_ffdhe4096_6() const { return ___draft_ffdhe4096_6; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_draft_ffdhe4096_6() { return &___draft_ffdhe4096_6; }
	inline void set_draft_ffdhe4096_6(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___draft_ffdhe4096_6 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe4096_6), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe6144_p_7() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe6144_p_7)); }
	inline String_t* get_draft_ffdhe6144_p_7() const { return ___draft_ffdhe6144_p_7; }
	inline String_t** get_address_of_draft_ffdhe6144_p_7() { return &___draft_ffdhe6144_p_7; }
	inline void set_draft_ffdhe6144_p_7(String_t* value)
	{
		___draft_ffdhe6144_p_7 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe6144_p_7), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe6144_8() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe6144_8)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_draft_ffdhe6144_8() const { return ___draft_ffdhe6144_8; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_draft_ffdhe6144_8() { return &___draft_ffdhe6144_8; }
	inline void set_draft_ffdhe6144_8(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___draft_ffdhe6144_8 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe6144_8), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe8192_p_9() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe8192_p_9)); }
	inline String_t* get_draft_ffdhe8192_p_9() const { return ___draft_ffdhe8192_p_9; }
	inline String_t** get_address_of_draft_ffdhe8192_p_9() { return &___draft_ffdhe8192_p_9; }
	inline void set_draft_ffdhe8192_p_9(String_t* value)
	{
		___draft_ffdhe8192_p_9 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe8192_p_9), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe8192_10() { return static_cast<int32_t>(offsetof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields, ___draft_ffdhe8192_10)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_draft_ffdhe8192_10() const { return ___draft_ffdhe8192_10; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_draft_ffdhe8192_10() { return &___draft_ffdhe8192_10; }
	inline void set_draft_ffdhe8192_10(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___draft_ffdhe8192_10 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe8192_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDHUTILITIES_T1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_H
#ifndef TLSDEFLATECOMPRESSION_TFD96E04916946BE85A6ABEEC4E993AA1E9C6C164_H
#define TLSDEFLATECOMPRESSION_TFD96E04916946BE85A6ABEEC4E993AA1E9C6C164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression
struct  TlsDeflateCompression_tFD96E04916946BE85A6ABEEC4E993AA1E9C6C164  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression::zIn
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___zIn_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression::zOut
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___zOut_5;

public:
	inline static int32_t get_offset_of_zIn_4() { return static_cast<int32_t>(offsetof(TlsDeflateCompression_tFD96E04916946BE85A6ABEEC4E993AA1E9C6C164, ___zIn_4)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_zIn_4() const { return ___zIn_4; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_zIn_4() { return &___zIn_4; }
	inline void set_zIn_4(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___zIn_4 = value;
		Il2CppCodeGenWriteBarrier((&___zIn_4), value);
	}

	inline static int32_t get_offset_of_zOut_5() { return static_cast<int32_t>(offsetof(TlsDeflateCompression_tFD96E04916946BE85A6ABEEC4E993AA1E9C6C164, ___zOut_5)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_zOut_5() const { return ___zOut_5; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_zOut_5() { return &___zOut_5; }
	inline void set_zOut_5(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___zOut_5 = value;
		Il2CppCodeGenWriteBarrier((&___zOut_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDEFLATECOMPRESSION_TFD96E04916946BE85A6ABEEC4E993AA1E9C6C164_H
#ifndef TLSECCUTILITIES_T28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_H
#define TLSECCUTILITIES_T28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsEccUtilities
struct  TlsEccUtilities_t28CD66E8F848B08318274AA1F19FFD8AB0CA2D21  : public RuntimeObject
{
public:

public:
};

struct TlsEccUtilities_t28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_StaticFields
{
public:
	// System.String[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsEccUtilities::CurveNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___CurveNames_0;

public:
	inline static int32_t get_offset_of_CurveNames_0() { return static_cast<int32_t>(offsetof(TlsEccUtilities_t28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_StaticFields, ___CurveNames_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_CurveNames_0() const { return ___CurveNames_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_CurveNames_0() { return &___CurveNames_0; }
	inline void set_CurveNames_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___CurveNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___CurveNames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECCUTILITIES_T28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ABSTRACTTLSCLIENT_TD2618A4C0AB20690FF055439D20B717ED70F8A14_H
#define ABSTRACTTLSCLIENT_TD2618A4C0AB20690FF055439D20B717ED70F8A14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient
struct  AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14  : public AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipherFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mCipherFactory
	RuntimeObject* ___mCipherFactory_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mContext
	RuntimeObject* ___mContext_2;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_7;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCompressionMethod
	int16_t ___mSelectedCompressionMethod_8;
	// System.Collections.Generic.List`1<System.String> BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::<HostNames>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CHostNamesU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<System.String> BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::<ClientSupportedProtocols>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CClientSupportedProtocolsU3Ek__BackingField_10;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::<ServerSupportedProtocol>k__BackingField
	String_t* ___U3CServerSupportedProtocolU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_mCipherFactory_1() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mCipherFactory_1)); }
	inline RuntimeObject* get_mCipherFactory_1() const { return ___mCipherFactory_1; }
	inline RuntimeObject** get_address_of_mCipherFactory_1() { return &___mCipherFactory_1; }
	inline void set_mCipherFactory_1(RuntimeObject* value)
	{
		___mCipherFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_3() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mSupportedSignatureAlgorithms_3)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_3() const { return ___mSupportedSignatureAlgorithms_3; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_3() { return &___mSupportedSignatureAlgorithms_3; }
	inline void set_mSupportedSignatureAlgorithms_3(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_3), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_4() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mNamedCurves_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_4() const { return ___mNamedCurves_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_4() { return &___mNamedCurves_4; }
	inline void set_mNamedCurves_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_4), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_5() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mClientECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_5() const { return ___mClientECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_5() { return &___mClientECPointFormats_5; }
	inline void set_mClientECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_6() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mServerECPointFormats_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_6() const { return ___mServerECPointFormats_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_6() { return &___mServerECPointFormats_6; }
	inline void set_mServerECPointFormats_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_6), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_7() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mSelectedCipherSuite_7)); }
	inline int32_t get_mSelectedCipherSuite_7() const { return ___mSelectedCipherSuite_7; }
	inline int32_t* get_address_of_mSelectedCipherSuite_7() { return &___mSelectedCipherSuite_7; }
	inline void set_mSelectedCipherSuite_7(int32_t value)
	{
		___mSelectedCipherSuite_7 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_8() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___mSelectedCompressionMethod_8)); }
	inline int16_t get_mSelectedCompressionMethod_8() const { return ___mSelectedCompressionMethod_8; }
	inline int16_t* get_address_of_mSelectedCompressionMethod_8() { return &___mSelectedCompressionMethod_8; }
	inline void set_mSelectedCompressionMethod_8(int16_t value)
	{
		___mSelectedCompressionMethod_8 = value;
	}

	inline static int32_t get_offset_of_U3CHostNamesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___U3CHostNamesU3Ek__BackingField_9)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CHostNamesU3Ek__BackingField_9() const { return ___U3CHostNamesU3Ek__BackingField_9; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CHostNamesU3Ek__BackingField_9() { return &___U3CHostNamesU3Ek__BackingField_9; }
	inline void set_U3CHostNamesU3Ek__BackingField_9(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CHostNamesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostNamesU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CClientSupportedProtocolsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___U3CClientSupportedProtocolsU3Ek__BackingField_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CClientSupportedProtocolsU3Ek__BackingField_10() const { return ___U3CClientSupportedProtocolsU3Ek__BackingField_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CClientSupportedProtocolsU3Ek__BackingField_10() { return &___U3CClientSupportedProtocolsU3Ek__BackingField_10; }
	inline void set_U3CClientSupportedProtocolsU3Ek__BackingField_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CClientSupportedProtocolsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientSupportedProtocolsU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CServerSupportedProtocolU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14, ___U3CServerSupportedProtocolU3Ek__BackingField_11)); }
	inline String_t* get_U3CServerSupportedProtocolU3Ek__BackingField_11() const { return ___U3CServerSupportedProtocolU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CServerSupportedProtocolU3Ek__BackingField_11() { return &___U3CServerSupportedProtocolU3Ek__BackingField_11; }
	inline void set_U3CServerSupportedProtocolU3Ek__BackingField_11(String_t* value)
	{
		___U3CServerSupportedProtocolU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerSupportedProtocolU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCLIENT_TD2618A4C0AB20690FF055439D20B717ED70F8A14_H
#ifndef ABSTRACTTLSENCRYPTIONCREDENTIALS_T28E0815D9DEB261CEC85F66E3C31B103A84D5908_H
#define ABSTRACTTLSENCRYPTIONCREDENTIALS_T28E0815D9DEB261CEC85F66E3C31B103A84D5908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsEncryptionCredentials
struct  AbstractTlsEncryptionCredentials_t28E0815D9DEB261CEC85F66E3C31B103A84D5908  : public AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSENCRYPTIONCREDENTIALS_T28E0815D9DEB261CEC85F66E3C31B103A84D5908_H
#ifndef ABSTRACTTLSSERVER_T5B6433B269A08B31A745579882002FE58C06EE25_H
#define ABSTRACTTLSSERVER_T5B6433B269A08B31A745579882002FE58C06EE25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer
struct  AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25  : public AbstractTlsPeer_t4CADCC4F7D80D3BA66C0422339E38604CBEC9638
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCipherFactory BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mCipherFactory
	RuntimeObject* ___mCipherFactory_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mContext
	RuntimeObject* ___mContext_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mClientVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mClientVersion_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mOfferedCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mOfferedCipherSuites_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mOfferedCompressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mOfferedCompressionMethods_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mClientExtensions
	RuntimeObject* ___mClientExtensions_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mEncryptThenMacOffered
	bool ___mEncryptThenMacOffered_7;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mMaxFragmentLengthOffered
	int16_t ___mMaxFragmentLengthOffered_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mTruncatedHMacOffered
	bool ___mTruncatedHMacOffered_9;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_10;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mEccCipherSuitesOffered
	bool ___mEccCipherSuitesOffered_11;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mServerVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mServerVersion_15;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_16;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mSelectedCompressionMethod
	uint8_t ___mSelectedCompressionMethod_17;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsServer::mServerExtensions
	RuntimeObject* ___mServerExtensions_18;

public:
	inline static int32_t get_offset_of_mCipherFactory_1() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mCipherFactory_1)); }
	inline RuntimeObject* get_mCipherFactory_1() const { return ___mCipherFactory_1; }
	inline RuntimeObject** get_address_of_mCipherFactory_1() { return &___mCipherFactory_1; }
	inline void set_mCipherFactory_1(RuntimeObject* value)
	{
		___mCipherFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}

	inline static int32_t get_offset_of_mClientVersion_3() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mClientVersion_3)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mClientVersion_3() const { return ___mClientVersion_3; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mClientVersion_3() { return &___mClientVersion_3; }
	inline void set_mClientVersion_3(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mClientVersion_3 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_3), value);
	}

	inline static int32_t get_offset_of_mOfferedCipherSuites_4() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mOfferedCipherSuites_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mOfferedCipherSuites_4() const { return ___mOfferedCipherSuites_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mOfferedCipherSuites_4() { return &___mOfferedCipherSuites_4; }
	inline void set_mOfferedCipherSuites_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mOfferedCipherSuites_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCipherSuites_4), value);
	}

	inline static int32_t get_offset_of_mOfferedCompressionMethods_5() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mOfferedCompressionMethods_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mOfferedCompressionMethods_5() const { return ___mOfferedCompressionMethods_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mOfferedCompressionMethods_5() { return &___mOfferedCompressionMethods_5; }
	inline void set_mOfferedCompressionMethods_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mOfferedCompressionMethods_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCompressionMethods_5), value);
	}

	inline static int32_t get_offset_of_mClientExtensions_6() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mClientExtensions_6)); }
	inline RuntimeObject* get_mClientExtensions_6() const { return ___mClientExtensions_6; }
	inline RuntimeObject** get_address_of_mClientExtensions_6() { return &___mClientExtensions_6; }
	inline void set_mClientExtensions_6(RuntimeObject* value)
	{
		___mClientExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___mClientExtensions_6), value);
	}

	inline static int32_t get_offset_of_mEncryptThenMacOffered_7() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mEncryptThenMacOffered_7)); }
	inline bool get_mEncryptThenMacOffered_7() const { return ___mEncryptThenMacOffered_7; }
	inline bool* get_address_of_mEncryptThenMacOffered_7() { return &___mEncryptThenMacOffered_7; }
	inline void set_mEncryptThenMacOffered_7(bool value)
	{
		___mEncryptThenMacOffered_7 = value;
	}

	inline static int32_t get_offset_of_mMaxFragmentLengthOffered_8() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mMaxFragmentLengthOffered_8)); }
	inline int16_t get_mMaxFragmentLengthOffered_8() const { return ___mMaxFragmentLengthOffered_8; }
	inline int16_t* get_address_of_mMaxFragmentLengthOffered_8() { return &___mMaxFragmentLengthOffered_8; }
	inline void set_mMaxFragmentLengthOffered_8(int16_t value)
	{
		___mMaxFragmentLengthOffered_8 = value;
	}

	inline static int32_t get_offset_of_mTruncatedHMacOffered_9() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mTruncatedHMacOffered_9)); }
	inline bool get_mTruncatedHMacOffered_9() const { return ___mTruncatedHMacOffered_9; }
	inline bool* get_address_of_mTruncatedHMacOffered_9() { return &___mTruncatedHMacOffered_9; }
	inline void set_mTruncatedHMacOffered_9(bool value)
	{
		___mTruncatedHMacOffered_9 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_10() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mSupportedSignatureAlgorithms_10)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_10() const { return ___mSupportedSignatureAlgorithms_10; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_10() { return &___mSupportedSignatureAlgorithms_10; }
	inline void set_mSupportedSignatureAlgorithms_10(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_10), value);
	}

	inline static int32_t get_offset_of_mEccCipherSuitesOffered_11() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mEccCipherSuitesOffered_11)); }
	inline bool get_mEccCipherSuitesOffered_11() const { return ___mEccCipherSuitesOffered_11; }
	inline bool* get_address_of_mEccCipherSuitesOffered_11() { return &___mEccCipherSuitesOffered_11; }
	inline void set_mEccCipherSuitesOffered_11(bool value)
	{
		___mEccCipherSuitesOffered_11 = value;
	}

	inline static int32_t get_offset_of_mNamedCurves_12() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mNamedCurves_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_12() const { return ___mNamedCurves_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_12() { return &___mNamedCurves_12; }
	inline void set_mNamedCurves_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_12 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_12), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_13() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mClientECPointFormats_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_13() const { return ___mClientECPointFormats_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_13() { return &___mClientECPointFormats_13; }
	inline void set_mClientECPointFormats_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_13 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_13), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_14() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mServerECPointFormats_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_14() const { return ___mServerECPointFormats_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_14() { return &___mServerECPointFormats_14; }
	inline void set_mServerECPointFormats_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_14), value);
	}

	inline static int32_t get_offset_of_mServerVersion_15() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mServerVersion_15)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mServerVersion_15() const { return ___mServerVersion_15; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mServerVersion_15() { return &___mServerVersion_15; }
	inline void set_mServerVersion_15(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mServerVersion_15 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_15), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_16() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mSelectedCipherSuite_16)); }
	inline int32_t get_mSelectedCipherSuite_16() const { return ___mSelectedCipherSuite_16; }
	inline int32_t* get_address_of_mSelectedCipherSuite_16() { return &___mSelectedCipherSuite_16; }
	inline void set_mSelectedCipherSuite_16(int32_t value)
	{
		___mSelectedCipherSuite_16 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_17() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mSelectedCompressionMethod_17)); }
	inline uint8_t get_mSelectedCompressionMethod_17() const { return ___mSelectedCompressionMethod_17; }
	inline uint8_t* get_address_of_mSelectedCompressionMethod_17() { return &___mSelectedCompressionMethod_17; }
	inline void set_mSelectedCompressionMethod_17(uint8_t value)
	{
		___mSelectedCompressionMethod_17 = value;
	}

	inline static int32_t get_offset_of_mServerExtensions_18() { return static_cast<int32_t>(offsetof(AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25, ___mServerExtensions_18)); }
	inline RuntimeObject* get_mServerExtensions_18() const { return ___mServerExtensions_18; }
	inline RuntimeObject** get_address_of_mServerExtensions_18() { return &___mServerExtensions_18; }
	inline void set_mServerExtensions_18(RuntimeObject* value)
	{
		___mServerExtensions_18 = value;
		Il2CppCodeGenWriteBarrier((&___mServerExtensions_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSERVER_T5B6433B269A08B31A745579882002FE58C06EE25_H
#ifndef ABSTRACTTLSSIGNERCREDENTIALS_T68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD_H
#define ABSTRACTTLSSIGNERCREDENTIALS_T68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSignerCredentials
struct  AbstractTlsSignerCredentials_t68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD  : public AbstractTlsCredentials_tBF8B3B617FEBAAE79AB02F3AAF2B6A1FFE5B3E1C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNERCREDENTIALS_T68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD_H
#ifndef DTLSCLIENTPROTOCOL_T3C84E51886D54A0F94231FEB717E2ED8A9A618A0_H
#define DTLSCLIENTPROTOCOL_T3C84E51886D54A0F94231FEB717E2ED8A9A618A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsClientProtocol
struct  DtlsClientProtocol_t3C84E51886D54A0F94231FEB717E2ED8A9A618A0  : public DtlsProtocol_t27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSCLIENTPROTOCOL_T3C84E51886D54A0F94231FEB717E2ED8A9A618A0_H
#ifndef DTLSSERVERPROTOCOL_TEAB0A17839E6FE5575AE322DBAAC6A88B28B7019_H
#define DTLSSERVERPROTOCOL_TEAB0A17839E6FE5575AE322DBAAC6A88B28B7019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol
struct  DtlsServerProtocol_tEAB0A17839E6FE5575AE322DBAAC6A88B28B7019  : public DtlsProtocol_t27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsServerProtocol::mVerifyRequests
	bool ___mVerifyRequests_1;

public:
	inline static int32_t get_offset_of_mVerifyRequests_1() { return static_cast<int32_t>(offsetof(DtlsServerProtocol_tEAB0A17839E6FE5575AE322DBAAC6A88B28B7019, ___mVerifyRequests_1)); }
	inline bool get_mVerifyRequests_1() const { return ___mVerifyRequests_1; }
	inline bool* get_address_of_mVerifyRequests_1() { return &___mVerifyRequests_1; }
	inline void set_mVerifyRequests_1(bool value)
	{
		___mVerifyRequests_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSSERVERPROTOCOL_TEAB0A17839E6FE5575AE322DBAAC6A88B28B7019_H
#ifndef TLSCLIENTCONTEXTIMPL_TFCA01255E21FD4D7C25645D3EEE4BCA2030108C0_H
#define TLSCLIENTCONTEXTIMPL_TFCA01255E21FD4D7C25645D3EEE4BCA2030108C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl
struct  TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0  : public AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCONTEXTIMPL_TFCA01255E21FD4D7C25645D3EEE4BCA2030108C0_H
#ifndef TLSDHKEYEXCHANGE_TA2DF2A49703DDB34CDFE174FDB77949E91C7C644_H
#define TLSDHKEYEXCHANGE_TA2DF2A49703DDB34CDFE174FDB77949E91C7C644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange
struct  TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644  : public AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mTlsSigner
	RuntimeObject* ___mTlsSigner_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHVerifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHVerifier
	RuntimeObject* ___mDHVerifier_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHParameters
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___mDHParameters_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mServerPublicKey_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mAgreementCredentials
	RuntimeObject* ___mAgreementCredentials_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHAgreePrivateKey
	DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * ___mDHAgreePrivateKey_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHAgreePublicKey
	DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 * ___mDHAgreePublicKey_9;

public:
	inline static int32_t get_offset_of_mTlsSigner_3() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mTlsSigner_3)); }
	inline RuntimeObject* get_mTlsSigner_3() const { return ___mTlsSigner_3; }
	inline RuntimeObject** get_address_of_mTlsSigner_3() { return &___mTlsSigner_3; }
	inline void set_mTlsSigner_3(RuntimeObject* value)
	{
		___mTlsSigner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSigner_3), value);
	}

	inline static int32_t get_offset_of_mDHVerifier_4() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mDHVerifier_4)); }
	inline RuntimeObject* get_mDHVerifier_4() const { return ___mDHVerifier_4; }
	inline RuntimeObject** get_address_of_mDHVerifier_4() { return &___mDHVerifier_4; }
	inline void set_mDHVerifier_4(RuntimeObject* value)
	{
		___mDHVerifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDHVerifier_4), value);
	}

	inline static int32_t get_offset_of_mDHParameters_5() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mDHParameters_5)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_mDHParameters_5() const { return ___mDHParameters_5; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_mDHParameters_5() { return &___mDHParameters_5; }
	inline void set_mDHParameters_5(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___mDHParameters_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDHParameters_5), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_6() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mServerPublicKey_6)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mServerPublicKey_6() const { return ___mServerPublicKey_6; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mServerPublicKey_6() { return &___mServerPublicKey_6; }
	inline void set_mServerPublicKey_6(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mServerPublicKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_6), value);
	}

	inline static int32_t get_offset_of_mAgreementCredentials_7() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mAgreementCredentials_7)); }
	inline RuntimeObject* get_mAgreementCredentials_7() const { return ___mAgreementCredentials_7; }
	inline RuntimeObject** get_address_of_mAgreementCredentials_7() { return &___mAgreementCredentials_7; }
	inline void set_mAgreementCredentials_7(RuntimeObject* value)
	{
		___mAgreementCredentials_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAgreementCredentials_7), value);
	}

	inline static int32_t get_offset_of_mDHAgreePrivateKey_8() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mDHAgreePrivateKey_8)); }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * get_mDHAgreePrivateKey_8() const { return ___mDHAgreePrivateKey_8; }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 ** get_address_of_mDHAgreePrivateKey_8() { return &___mDHAgreePrivateKey_8; }
	inline void set_mDHAgreePrivateKey_8(DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * value)
	{
		___mDHAgreePrivateKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___mDHAgreePrivateKey_8), value);
	}

	inline static int32_t get_offset_of_mDHAgreePublicKey_9() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644, ___mDHAgreePublicKey_9)); }
	inline DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 * get_mDHAgreePublicKey_9() const { return ___mDHAgreePublicKey_9; }
	inline DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 ** get_address_of_mDHAgreePublicKey_9() { return &___mDHAgreePublicKey_9; }
	inline void set_mDHAgreePublicKey_9(DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 * value)
	{
		___mDHAgreePublicKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___mDHAgreePublicKey_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDHKEYEXCHANGE_TA2DF2A49703DDB34CDFE174FDB77949E91C7C644_H
#ifndef TLSDSASIGNER_TE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E_H
#define TLSDSASIGNER_TE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDsaSigner
struct  TlsDsaSigner_tE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E  : public AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDSASIGNER_TE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E_H
#ifndef TLSECDHKEYEXCHANGE_T0402E5233B545D8D5B8DC9738C90B952BCF2DB19_H
#define TLSECDHKEYEXCHANGE_T0402E5233B545D8D5B8DC9738C90B952BCF2DB19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange
struct  TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19  : public AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mTlsSigner
	RuntimeObject* ___mTlsSigner_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mServerPublicKey_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mAgreementCredentials
	RuntimeObject* ___mAgreementCredentials_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mECAgreePrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mECAgreePrivateKey_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mECAgreePublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___mECAgreePublicKey_10;

public:
	inline static int32_t get_offset_of_mTlsSigner_3() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mTlsSigner_3)); }
	inline RuntimeObject* get_mTlsSigner_3() const { return ___mTlsSigner_3; }
	inline RuntimeObject** get_address_of_mTlsSigner_3() { return &___mTlsSigner_3; }
	inline void set_mTlsSigner_3(RuntimeObject* value)
	{
		___mTlsSigner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSigner_3), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_4() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mNamedCurves_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_4() const { return ___mNamedCurves_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_4() { return &___mNamedCurves_4; }
	inline void set_mNamedCurves_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_4), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_5() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mClientECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_5() const { return ___mClientECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_5() { return &___mClientECPointFormats_5; }
	inline void set_mClientECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_6() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mServerECPointFormats_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_6() const { return ___mServerECPointFormats_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_6() { return &___mServerECPointFormats_6; }
	inline void set_mServerECPointFormats_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_6), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_7() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mServerPublicKey_7)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mServerPublicKey_7() const { return ___mServerPublicKey_7; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mServerPublicKey_7() { return &___mServerPublicKey_7; }
	inline void set_mServerPublicKey_7(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mServerPublicKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_7), value);
	}

	inline static int32_t get_offset_of_mAgreementCredentials_8() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mAgreementCredentials_8)); }
	inline RuntimeObject* get_mAgreementCredentials_8() const { return ___mAgreementCredentials_8; }
	inline RuntimeObject** get_address_of_mAgreementCredentials_8() { return &___mAgreementCredentials_8; }
	inline void set_mAgreementCredentials_8(RuntimeObject* value)
	{
		___mAgreementCredentials_8 = value;
		Il2CppCodeGenWriteBarrier((&___mAgreementCredentials_8), value);
	}

	inline static int32_t get_offset_of_mECAgreePrivateKey_9() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mECAgreePrivateKey_9)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mECAgreePrivateKey_9() const { return ___mECAgreePrivateKey_9; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mECAgreePrivateKey_9() { return &___mECAgreePrivateKey_9; }
	inline void set_mECAgreePrivateKey_9(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mECAgreePrivateKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePrivateKey_9), value);
	}

	inline static int32_t get_offset_of_mECAgreePublicKey_10() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mECAgreePublicKey_10)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_mECAgreePublicKey_10() const { return ___mECAgreePublicKey_10; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_mECAgreePublicKey_10() { return &___mECAgreePublicKey_10; }
	inline void set_mECAgreePublicKey_10(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___mECAgreePublicKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePublicKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDHKEYEXCHANGE_T0402E5233B545D8D5B8DC9738C90B952BCF2DB19_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef DEFAULTTLSCLIENT_TFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF_H
#define DEFAULTTLSCLIENT_TFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsClient
struct  DefaultTlsClient_tFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF  : public AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHVerifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsClient::mDHVerifier
	RuntimeObject* ___mDHVerifier_12;

public:
	inline static int32_t get_offset_of_mDHVerifier_12() { return static_cast<int32_t>(offsetof(DefaultTlsClient_tFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF, ___mDHVerifier_12)); }
	inline RuntimeObject* get_mDHVerifier_12() const { return ___mDHVerifier_12; }
	inline RuntimeObject** get_address_of_mDHVerifier_12() { return &___mDHVerifier_12; }
	inline void set_mDHVerifier_12(RuntimeObject* value)
	{
		___mDHVerifier_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDHVerifier_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSCLIENT_TFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF_H
#ifndef DEFAULTTLSENCRYPTIONCREDENTIALS_T840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0_H
#define DEFAULTTLSENCRYPTIONCREDENTIALS_T840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsEncryptionCredentials
struct  DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0  : public AbstractTlsEncryptionCredentials_t28E0815D9DEB261CEC85F66E3C31B103A84D5908
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsEncryptionCredentials::mContext
	RuntimeObject* ___mContext_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsEncryptionCredentials::mCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mCertificate_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsEncryptionCredentials::mPrivateKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mPrivateKey_2;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}

	inline static int32_t get_offset_of_mCertificate_1() { return static_cast<int32_t>(offsetof(DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0, ___mCertificate_1)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mCertificate_1() const { return ___mCertificate_1; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mCertificate_1() { return &___mCertificate_1; }
	inline void set_mCertificate_1(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mCertificate_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificate_1), value);
	}

	inline static int32_t get_offset_of_mPrivateKey_2() { return static_cast<int32_t>(offsetof(DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0, ___mPrivateKey_2)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mPrivateKey_2() const { return ___mPrivateKey_2; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mPrivateKey_2() { return &___mPrivateKey_2; }
	inline void set_mPrivateKey_2(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mPrivateKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___mPrivateKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSENCRYPTIONCREDENTIALS_T840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0_H
#ifndef DEFAULTTLSSERVER_T50C2F11D4D70DE3E7451DFB2A6DE92BDE6B8D4F8_H
#define DEFAULTTLSSERVER_T50C2F11D4D70DE3E7451DFB2A6DE92BDE6B8D4F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsServer
struct  DefaultTlsServer_t50C2F11D4D70DE3E7451DFB2A6DE92BDE6B8D4F8  : public AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSSERVER_T50C2F11D4D70DE3E7451DFB2A6DE92BDE6B8D4F8_H
#ifndef DEFAULTTLSSIGNERCREDENTIALS_T54B0001684AD474B9BD2EC2471A6DE5899F1B43D_H
#define DEFAULTTLSSIGNERCREDENTIALS_T54B0001684AD474B9BD2EC2471A6DE5899F1B43D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSignerCredentials
struct  DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D  : public AbstractTlsSignerCredentials_t68F4C0CA850A745CBBA7F7DA1E74A9577D864BCD
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSignerCredentials::mContext
	RuntimeObject* ___mContext_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSignerCredentials::mCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mCertificate_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSignerCredentials::mPrivateKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mPrivateKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSignerCredentials::mSignatureAndHashAlgorithm
	SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 * ___mSignatureAndHashAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DefaultTlsSignerCredentials::mSigner
	RuntimeObject* ___mSigner_4;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}

	inline static int32_t get_offset_of_mCertificate_1() { return static_cast<int32_t>(offsetof(DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D, ___mCertificate_1)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mCertificate_1() const { return ___mCertificate_1; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mCertificate_1() { return &___mCertificate_1; }
	inline void set_mCertificate_1(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mCertificate_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificate_1), value);
	}

	inline static int32_t get_offset_of_mPrivateKey_2() { return static_cast<int32_t>(offsetof(DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D, ___mPrivateKey_2)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mPrivateKey_2() const { return ___mPrivateKey_2; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mPrivateKey_2() { return &___mPrivateKey_2; }
	inline void set_mPrivateKey_2(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mPrivateKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___mPrivateKey_2), value);
	}

	inline static int32_t get_offset_of_mSignatureAndHashAlgorithm_3() { return static_cast<int32_t>(offsetof(DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D, ___mSignatureAndHashAlgorithm_3)); }
	inline SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 * get_mSignatureAndHashAlgorithm_3() const { return ___mSignatureAndHashAlgorithm_3; }
	inline SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 ** get_address_of_mSignatureAndHashAlgorithm_3() { return &___mSignatureAndHashAlgorithm_3; }
	inline void set_mSignatureAndHashAlgorithm_3(SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148 * value)
	{
		___mSignatureAndHashAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSignatureAndHashAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_mSigner_4() { return static_cast<int32_t>(offsetof(DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D, ___mSigner_4)); }
	inline RuntimeObject* get_mSigner_4() const { return ___mSigner_4; }
	inline RuntimeObject** get_address_of_mSigner_4() { return &___mSigner_4; }
	inline void set_mSigner_4(RuntimeObject* value)
	{
		___mSigner_4 = value;
		Il2CppCodeGenWriteBarrier((&___mSigner_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSSIGNERCREDENTIALS_T54B0001684AD474B9BD2EC2471A6DE5899F1B43D_H
#ifndef DTLSRECORDLAYER_T2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC_H
#define DTLSRECORDLAYER_T2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer
struct  DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DatagramTransport BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mTransport
	RuntimeObject* ___mTransport_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mContext
	RuntimeObject* ___mContext_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPeer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mPeer
	RuntimeObject* ___mPeer_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mRecordQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mRecordQueue_7;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mClosed
	bool ___mClosed_8;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mFailed
	bool ___mFailed_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mReadVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mReadVersion_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mWriteVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mWriteVersion_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mInHandshake
	bool ___mInHandshake_12;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mPlaintextLimit
	int32_t ___mPlaintextLimit_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mCurrentEpoch
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * ___mCurrentEpoch_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mPendingEpoch
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * ___mPendingEpoch_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mReadEpoch
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * ___mReadEpoch_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mWriteEpoch
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * ___mWriteEpoch_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsHandshakeRetransmit BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mRetransmit
	RuntimeObject* ___mRetransmit_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsEpoch BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mRetransmitEpoch
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * ___mRetransmitEpoch_19;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsRecordLayer::mRetransmitExpiry
	int64_t ___mRetransmitExpiry_20;

public:
	inline static int32_t get_offset_of_mTransport_4() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mTransport_4)); }
	inline RuntimeObject* get_mTransport_4() const { return ___mTransport_4; }
	inline RuntimeObject** get_address_of_mTransport_4() { return &___mTransport_4; }
	inline void set_mTransport_4(RuntimeObject* value)
	{
		___mTransport_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTransport_4), value);
	}

	inline static int32_t get_offset_of_mContext_5() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mContext_5)); }
	inline RuntimeObject* get_mContext_5() const { return ___mContext_5; }
	inline RuntimeObject** get_address_of_mContext_5() { return &___mContext_5; }
	inline void set_mContext_5(RuntimeObject* value)
	{
		___mContext_5 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_5), value);
	}

	inline static int32_t get_offset_of_mPeer_6() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mPeer_6)); }
	inline RuntimeObject* get_mPeer_6() const { return ___mPeer_6; }
	inline RuntimeObject** get_address_of_mPeer_6() { return &___mPeer_6; }
	inline void set_mPeer_6(RuntimeObject* value)
	{
		___mPeer_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPeer_6), value);
	}

	inline static int32_t get_offset_of_mRecordQueue_7() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mRecordQueue_7)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mRecordQueue_7() const { return ___mRecordQueue_7; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mRecordQueue_7() { return &___mRecordQueue_7; }
	inline void set_mRecordQueue_7(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mRecordQueue_7 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordQueue_7), value);
	}

	inline static int32_t get_offset_of_mClosed_8() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mClosed_8)); }
	inline bool get_mClosed_8() const { return ___mClosed_8; }
	inline bool* get_address_of_mClosed_8() { return &___mClosed_8; }
	inline void set_mClosed_8(bool value)
	{
		___mClosed_8 = value;
	}

	inline static int32_t get_offset_of_mFailed_9() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mFailed_9)); }
	inline bool get_mFailed_9() const { return ___mFailed_9; }
	inline bool* get_address_of_mFailed_9() { return &___mFailed_9; }
	inline void set_mFailed_9(bool value)
	{
		___mFailed_9 = value;
	}

	inline static int32_t get_offset_of_mReadVersion_10() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mReadVersion_10)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mReadVersion_10() const { return ___mReadVersion_10; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mReadVersion_10() { return &___mReadVersion_10; }
	inline void set_mReadVersion_10(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mReadVersion_10 = value;
		Il2CppCodeGenWriteBarrier((&___mReadVersion_10), value);
	}

	inline static int32_t get_offset_of_mWriteVersion_11() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mWriteVersion_11)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mWriteVersion_11() const { return ___mWriteVersion_11; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mWriteVersion_11() { return &___mWriteVersion_11; }
	inline void set_mWriteVersion_11(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mWriteVersion_11 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteVersion_11), value);
	}

	inline static int32_t get_offset_of_mInHandshake_12() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mInHandshake_12)); }
	inline bool get_mInHandshake_12() const { return ___mInHandshake_12; }
	inline bool* get_address_of_mInHandshake_12() { return &___mInHandshake_12; }
	inline void set_mInHandshake_12(bool value)
	{
		___mInHandshake_12 = value;
	}

	inline static int32_t get_offset_of_mPlaintextLimit_13() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mPlaintextLimit_13)); }
	inline int32_t get_mPlaintextLimit_13() const { return ___mPlaintextLimit_13; }
	inline int32_t* get_address_of_mPlaintextLimit_13() { return &___mPlaintextLimit_13; }
	inline void set_mPlaintextLimit_13(int32_t value)
	{
		___mPlaintextLimit_13 = value;
	}

	inline static int32_t get_offset_of_mCurrentEpoch_14() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mCurrentEpoch_14)); }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * get_mCurrentEpoch_14() const { return ___mCurrentEpoch_14; }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F ** get_address_of_mCurrentEpoch_14() { return &___mCurrentEpoch_14; }
	inline void set_mCurrentEpoch_14(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * value)
	{
		___mCurrentEpoch_14 = value;
		Il2CppCodeGenWriteBarrier((&___mCurrentEpoch_14), value);
	}

	inline static int32_t get_offset_of_mPendingEpoch_15() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mPendingEpoch_15)); }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * get_mPendingEpoch_15() const { return ___mPendingEpoch_15; }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F ** get_address_of_mPendingEpoch_15() { return &___mPendingEpoch_15; }
	inline void set_mPendingEpoch_15(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * value)
	{
		___mPendingEpoch_15 = value;
		Il2CppCodeGenWriteBarrier((&___mPendingEpoch_15), value);
	}

	inline static int32_t get_offset_of_mReadEpoch_16() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mReadEpoch_16)); }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * get_mReadEpoch_16() const { return ___mReadEpoch_16; }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F ** get_address_of_mReadEpoch_16() { return &___mReadEpoch_16; }
	inline void set_mReadEpoch_16(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * value)
	{
		___mReadEpoch_16 = value;
		Il2CppCodeGenWriteBarrier((&___mReadEpoch_16), value);
	}

	inline static int32_t get_offset_of_mWriteEpoch_17() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mWriteEpoch_17)); }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * get_mWriteEpoch_17() const { return ___mWriteEpoch_17; }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F ** get_address_of_mWriteEpoch_17() { return &___mWriteEpoch_17; }
	inline void set_mWriteEpoch_17(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * value)
	{
		___mWriteEpoch_17 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteEpoch_17), value);
	}

	inline static int32_t get_offset_of_mRetransmit_18() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mRetransmit_18)); }
	inline RuntimeObject* get_mRetransmit_18() const { return ___mRetransmit_18; }
	inline RuntimeObject** get_address_of_mRetransmit_18() { return &___mRetransmit_18; }
	inline void set_mRetransmit_18(RuntimeObject* value)
	{
		___mRetransmit_18 = value;
		Il2CppCodeGenWriteBarrier((&___mRetransmit_18), value);
	}

	inline static int32_t get_offset_of_mRetransmitEpoch_19() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mRetransmitEpoch_19)); }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * get_mRetransmitEpoch_19() const { return ___mRetransmitEpoch_19; }
	inline DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F ** get_address_of_mRetransmitEpoch_19() { return &___mRetransmitEpoch_19; }
	inline void set_mRetransmitEpoch_19(DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F * value)
	{
		___mRetransmitEpoch_19 = value;
		Il2CppCodeGenWriteBarrier((&___mRetransmitEpoch_19), value);
	}

	inline static int32_t get_offset_of_mRetransmitExpiry_20() { return static_cast<int32_t>(offsetof(DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC, ___mRetransmitExpiry_20)); }
	inline int64_t get_mRetransmitExpiry_20() const { return ___mRetransmitExpiry_20; }
	inline int64_t* get_address_of_mRetransmitExpiry_20() { return &___mRetransmitExpiry_20; }
	inline void set_mRetransmitExpiry_20(int64_t value)
	{
		___mRetransmitExpiry_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTLSRECORDLAYER_T2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC_H
#ifndef PSKTLSCLIENT_T665AC937EB3C6C9710F37F154B8F669D3C854F57_H
#define PSKTLSCLIENT_T665AC937EB3C6C9710F37F154B8F669D3C854F57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.PskTlsClient
struct  PskTlsClient_t665AC937EB3C6C9710F37F154B8F669D3C854F57  : public AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHVerifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.PskTlsClient::mDHVerifier
	RuntimeObject* ___mDHVerifier_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentity BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.PskTlsClient::mPskIdentity
	RuntimeObject* ___mPskIdentity_13;

public:
	inline static int32_t get_offset_of_mDHVerifier_12() { return static_cast<int32_t>(offsetof(PskTlsClient_t665AC937EB3C6C9710F37F154B8F669D3C854F57, ___mDHVerifier_12)); }
	inline RuntimeObject* get_mDHVerifier_12() const { return ___mDHVerifier_12; }
	inline RuntimeObject** get_address_of_mDHVerifier_12() { return &___mDHVerifier_12; }
	inline void set_mDHVerifier_12(RuntimeObject* value)
	{
		___mDHVerifier_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDHVerifier_12), value);
	}

	inline static int32_t get_offset_of_mPskIdentity_13() { return static_cast<int32_t>(offsetof(PskTlsClient_t665AC937EB3C6C9710F37F154B8F669D3C854F57, ___mPskIdentity_13)); }
	inline RuntimeObject* get_mPskIdentity_13() const { return ___mPskIdentity_13; }
	inline RuntimeObject** get_address_of_mPskIdentity_13() { return &___mPskIdentity_13; }
	inline void set_mPskIdentity_13(RuntimeObject* value)
	{
		___mPskIdentity_13 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentity_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PSKTLSCLIENT_T665AC937EB3C6C9710F37F154B8F669D3C854F57_H
#ifndef PSKTLSSERVER_T34AD59BCC96D43E80DB1D4FED765F3E25B69AC13_H
#define PSKTLSSERVER_T34AD59BCC96D43E80DB1D4FED765F3E25B69AC13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.PskTlsServer
struct  PskTlsServer_t34AD59BCC96D43E80DB1D4FED765F3E25B69AC13  : public AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentityManager BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.PskTlsServer::mPskIdentityManager
	RuntimeObject* ___mPskIdentityManager_19;

public:
	inline static int32_t get_offset_of_mPskIdentityManager_19() { return static_cast<int32_t>(offsetof(PskTlsServer_t34AD59BCC96D43E80DB1D4FED765F3E25B69AC13, ___mPskIdentityManager_19)); }
	inline RuntimeObject* get_mPskIdentityManager_19() const { return ___mPskIdentityManager_19; }
	inline RuntimeObject** get_address_of_mPskIdentityManager_19() { return &___mPskIdentityManager_19; }
	inline void set_mPskIdentityManager_19(RuntimeObject* value)
	{
		___mPskIdentityManager_19 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentityManager_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PSKTLSSERVER_T34AD59BCC96D43E80DB1D4FED765F3E25B69AC13_H
#ifndef SRPTLSCLIENT_T573B722623BD3EE9354EF392AFF9722ACDE60D0C_H
#define SRPTLSCLIENT_T573B722623BD3EE9354EF392AFF9722ACDE60D0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsClient
struct  SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C  : public AbstractTlsClient_tD2618A4C0AB20690FF055439D20B717ED70F8A14
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpGroupVerifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsClient::mGroupVerifier
	RuntimeObject* ___mGroupVerifier_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsClient::mIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mIdentity_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsClient::mPassword
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPassword_14;

public:
	inline static int32_t get_offset_of_mGroupVerifier_12() { return static_cast<int32_t>(offsetof(SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C, ___mGroupVerifier_12)); }
	inline RuntimeObject* get_mGroupVerifier_12() const { return ___mGroupVerifier_12; }
	inline RuntimeObject** get_address_of_mGroupVerifier_12() { return &___mGroupVerifier_12; }
	inline void set_mGroupVerifier_12(RuntimeObject* value)
	{
		___mGroupVerifier_12 = value;
		Il2CppCodeGenWriteBarrier((&___mGroupVerifier_12), value);
	}

	inline static int32_t get_offset_of_mIdentity_13() { return static_cast<int32_t>(offsetof(SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C, ___mIdentity_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mIdentity_13() const { return ___mIdentity_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mIdentity_13() { return &___mIdentity_13; }
	inline void set_mIdentity_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mIdentity_13 = value;
		Il2CppCodeGenWriteBarrier((&___mIdentity_13), value);
	}

	inline static int32_t get_offset_of_mPassword_14() { return static_cast<int32_t>(offsetof(SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C, ___mPassword_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPassword_14() const { return ___mPassword_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPassword_14() { return &___mPassword_14; }
	inline void set_mPassword_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPassword_14 = value;
		Il2CppCodeGenWriteBarrier((&___mPassword_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRPTLSCLIENT_T573B722623BD3EE9354EF392AFF9722ACDE60D0C_H
#ifndef SRPTLSSERVER_TF3BED52B39E640E966915C47D2A14E4BEF2CFE8B_H
#define SRPTLSSERVER_TF3BED52B39E640E966915C47D2A14E4BEF2CFE8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsServer
struct  SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B  : public AbstractTlsServer_t5B6433B269A08B31A745579882002FE58C06EE25
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpIdentityManager BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsServer::mSrpIdentityManager
	RuntimeObject* ___mSrpIdentityManager_19;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsServer::mSrpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSrpIdentity_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpLoginParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SrpTlsServer::mLoginParameters
	TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238 * ___mLoginParameters_21;

public:
	inline static int32_t get_offset_of_mSrpIdentityManager_19() { return static_cast<int32_t>(offsetof(SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B, ___mSrpIdentityManager_19)); }
	inline RuntimeObject* get_mSrpIdentityManager_19() const { return ___mSrpIdentityManager_19; }
	inline RuntimeObject** get_address_of_mSrpIdentityManager_19() { return &___mSrpIdentityManager_19; }
	inline void set_mSrpIdentityManager_19(RuntimeObject* value)
	{
		___mSrpIdentityManager_19 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpIdentityManager_19), value);
	}

	inline static int32_t get_offset_of_mSrpIdentity_20() { return static_cast<int32_t>(offsetof(SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B, ___mSrpIdentity_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSrpIdentity_20() const { return ___mSrpIdentity_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSrpIdentity_20() { return &___mSrpIdentity_20; }
	inline void set_mSrpIdentity_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSrpIdentity_20 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpIdentity_20), value);
	}

	inline static int32_t get_offset_of_mLoginParameters_21() { return static_cast<int32_t>(offsetof(SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B, ___mLoginParameters_21)); }
	inline TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238 * get_mLoginParameters_21() const { return ___mLoginParameters_21; }
	inline TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238 ** get_address_of_mLoginParameters_21() { return &___mLoginParameters_21; }
	inline void set_mLoginParameters_21(TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238 * value)
	{
		___mLoginParameters_21 = value;
		Il2CppCodeGenWriteBarrier((&___mLoginParameters_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRPTLSSERVER_TF3BED52B39E640E966915C47D2A14E4BEF2CFE8B_H
#ifndef TLSDHEKEYEXCHANGE_T11DC49BE408AEAF5377AEA269624705A469BE9CC_H
#define TLSDHEKEYEXCHANGE_T11DC49BE408AEAF5377AEA269624705A469BE9CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDheKeyExchange
struct  TlsDheKeyExchange_t11DC49BE408AEAF5377AEA269624705A469BE9CC  : public TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSignerCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDheKeyExchange::mServerCredentials
	RuntimeObject* ___mServerCredentials_10;

public:
	inline static int32_t get_offset_of_mServerCredentials_10() { return static_cast<int32_t>(offsetof(TlsDheKeyExchange_t11DC49BE408AEAF5377AEA269624705A469BE9CC, ___mServerCredentials_10)); }
	inline RuntimeObject* get_mServerCredentials_10() const { return ___mServerCredentials_10; }
	inline RuntimeObject** get_address_of_mServerCredentials_10() { return &___mServerCredentials_10; }
	inline void set_mServerCredentials_10(RuntimeObject* value)
	{
		___mServerCredentials_10 = value;
		Il2CppCodeGenWriteBarrier((&___mServerCredentials_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDHEKEYEXCHANGE_T11DC49BE408AEAF5377AEA269624705A469BE9CC_H
#ifndef TLSDSSSIGNER_T7B3C24FA8AAB97BDD69F015E016C8260239111F7_H
#define TLSDSSSIGNER_T7B3C24FA8AAB97BDD69F015E016C8260239111F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDssSigner
struct  TlsDssSigner_t7B3C24FA8AAB97BDD69F015E016C8260239111F7  : public TlsDsaSigner_tE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDSSSIGNER_T7B3C24FA8AAB97BDD69F015E016C8260239111F7_H
#ifndef TLSECDHEKEYEXCHANGE_T3950DD21BC83197C5A6DECB3E7FDB772433CE353_H
#define TLSECDHEKEYEXCHANGE_T3950DD21BC83197C5A6DECB3E7FDB772433CE353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDheKeyExchange
struct  TlsECDheKeyExchange_t3950DD21BC83197C5A6DECB3E7FDB772433CE353  : public TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSignerCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDheKeyExchange::mServerCredentials
	RuntimeObject* ___mServerCredentials_11;

public:
	inline static int32_t get_offset_of_mServerCredentials_11() { return static_cast<int32_t>(offsetof(TlsECDheKeyExchange_t3950DD21BC83197C5A6DECB3E7FDB772433CE353, ___mServerCredentials_11)); }
	inline RuntimeObject* get_mServerCredentials_11() const { return ___mServerCredentials_11; }
	inline RuntimeObject** get_address_of_mServerCredentials_11() { return &___mServerCredentials_11; }
	inline void set_mServerCredentials_11(RuntimeObject* value)
	{
		___mServerCredentials_11 = value;
		Il2CppCodeGenWriteBarrier((&___mServerCredentials_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDHEKEYEXCHANGE_T3950DD21BC83197C5A6DECB3E7FDB772433CE353_H
#ifndef TLSPROTOCOL_T115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1_H
#define TLSPROTOCOL_T115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct  TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mApplicationDataQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mApplicationDataQueue_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAlertQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mAlertQueue_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mHandshakeQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mHandshakeQueue_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mRecordStream
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * ___mRecordStream_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsStream
	TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 * ___mTlsStream_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClosed
	bool ___mClosed_26;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mFailedWithError
	bool ___mFailedWithError_27;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataReady
	bool ___mAppDataReady_28;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataSplitEnabled
	bool ___mAppDataSplitEnabled_29;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataSplitMode
	int32_t ___mAppDataSplitMode_30;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectedVerifyData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mExpectedVerifyData_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsSession
	RuntimeObject* ___mTlsSession_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSessionParameters
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * ___mSessionParameters_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecurityParameters
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * ___mSecurityParameters_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mPeerCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mPeerCertificate_35;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mOfferedCipherSuites_36;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCompressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mOfferedCompressionMethods_37;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClientExtensions
	RuntimeObject* ___mClientExtensions_38;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mServerExtensions
	RuntimeObject* ___mServerExtensions_39;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mConnectionState
	int16_t ___mConnectionState_40;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mResumedSession
	bool ___mResumedSession_41;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mReceivedChangeCipherSpec
	bool ___mReceivedChangeCipherSpec_42;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRenegotiation
	bool ___mSecureRenegotiation_43;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAllowCertificateStatus
	bool ___mAllowCertificateStatus_44;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectSessionTicket
	bool ___mExpectSessionTicket_45;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mBlocking
	bool ___mBlocking_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mInputBuffers
	ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * ___mInputBuffers_47;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOutputBuffer
	ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * ___mOutputBuffer_48;

public:
	inline static int32_t get_offset_of_mApplicationDataQueue_20() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mApplicationDataQueue_20)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mApplicationDataQueue_20() const { return ___mApplicationDataQueue_20; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mApplicationDataQueue_20() { return &___mApplicationDataQueue_20; }
	inline void set_mApplicationDataQueue_20(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mApplicationDataQueue_20 = value;
		Il2CppCodeGenWriteBarrier((&___mApplicationDataQueue_20), value);
	}

	inline static int32_t get_offset_of_mAlertQueue_21() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAlertQueue_21)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mAlertQueue_21() const { return ___mAlertQueue_21; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mAlertQueue_21() { return &___mAlertQueue_21; }
	inline void set_mAlertQueue_21(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mAlertQueue_21 = value;
		Il2CppCodeGenWriteBarrier((&___mAlertQueue_21), value);
	}

	inline static int32_t get_offset_of_mHandshakeQueue_22() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mHandshakeQueue_22)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mHandshakeQueue_22() const { return ___mHandshakeQueue_22; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mHandshakeQueue_22() { return &___mHandshakeQueue_22; }
	inline void set_mHandshakeQueue_22(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mHandshakeQueue_22 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeQueue_22), value);
	}

	inline static int32_t get_offset_of_mRecordStream_23() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mRecordStream_23)); }
	inline RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * get_mRecordStream_23() const { return ___mRecordStream_23; }
	inline RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 ** get_address_of_mRecordStream_23() { return &___mRecordStream_23; }
	inline void set_mRecordStream_23(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * value)
	{
		___mRecordStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordStream_23), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_24() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSecureRandom_24)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_24() const { return ___mSecureRandom_24; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_24() { return &___mSecureRandom_24; }
	inline void set_mSecureRandom_24(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_24 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_24), value);
	}

	inline static int32_t get_offset_of_mTlsStream_25() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mTlsStream_25)); }
	inline TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 * get_mTlsStream_25() const { return ___mTlsStream_25; }
	inline TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 ** get_address_of_mTlsStream_25() { return &___mTlsStream_25; }
	inline void set_mTlsStream_25(TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 * value)
	{
		___mTlsStream_25 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsStream_25), value);
	}

	inline static int32_t get_offset_of_mClosed_26() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mClosed_26)); }
	inline bool get_mClosed_26() const { return ___mClosed_26; }
	inline bool* get_address_of_mClosed_26() { return &___mClosed_26; }
	inline void set_mClosed_26(bool value)
	{
		___mClosed_26 = value;
	}

	inline static int32_t get_offset_of_mFailedWithError_27() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mFailedWithError_27)); }
	inline bool get_mFailedWithError_27() const { return ___mFailedWithError_27; }
	inline bool* get_address_of_mFailedWithError_27() { return &___mFailedWithError_27; }
	inline void set_mFailedWithError_27(bool value)
	{
		___mFailedWithError_27 = value;
	}

	inline static int32_t get_offset_of_mAppDataReady_28() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAppDataReady_28)); }
	inline bool get_mAppDataReady_28() const { return ___mAppDataReady_28; }
	inline bool* get_address_of_mAppDataReady_28() { return &___mAppDataReady_28; }
	inline void set_mAppDataReady_28(bool value)
	{
		___mAppDataReady_28 = value;
	}

	inline static int32_t get_offset_of_mAppDataSplitEnabled_29() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAppDataSplitEnabled_29)); }
	inline bool get_mAppDataSplitEnabled_29() const { return ___mAppDataSplitEnabled_29; }
	inline bool* get_address_of_mAppDataSplitEnabled_29() { return &___mAppDataSplitEnabled_29; }
	inline void set_mAppDataSplitEnabled_29(bool value)
	{
		___mAppDataSplitEnabled_29 = value;
	}

	inline static int32_t get_offset_of_mAppDataSplitMode_30() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAppDataSplitMode_30)); }
	inline int32_t get_mAppDataSplitMode_30() const { return ___mAppDataSplitMode_30; }
	inline int32_t* get_address_of_mAppDataSplitMode_30() { return &___mAppDataSplitMode_30; }
	inline void set_mAppDataSplitMode_30(int32_t value)
	{
		___mAppDataSplitMode_30 = value;
	}

	inline static int32_t get_offset_of_mExpectedVerifyData_31() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mExpectedVerifyData_31)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mExpectedVerifyData_31() const { return ___mExpectedVerifyData_31; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mExpectedVerifyData_31() { return &___mExpectedVerifyData_31; }
	inline void set_mExpectedVerifyData_31(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mExpectedVerifyData_31 = value;
		Il2CppCodeGenWriteBarrier((&___mExpectedVerifyData_31), value);
	}

	inline static int32_t get_offset_of_mTlsSession_32() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mTlsSession_32)); }
	inline RuntimeObject* get_mTlsSession_32() const { return ___mTlsSession_32; }
	inline RuntimeObject** get_address_of_mTlsSession_32() { return &___mTlsSession_32; }
	inline void set_mTlsSession_32(RuntimeObject* value)
	{
		___mTlsSession_32 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSession_32), value);
	}

	inline static int32_t get_offset_of_mSessionParameters_33() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSessionParameters_33)); }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * get_mSessionParameters_33() const { return ___mSessionParameters_33; }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC ** get_address_of_mSessionParameters_33() { return &___mSessionParameters_33; }
	inline void set_mSessionParameters_33(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * value)
	{
		___mSessionParameters_33 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionParameters_33), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_34() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSecurityParameters_34)); }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * get_mSecurityParameters_34() const { return ___mSecurityParameters_34; }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA ** get_address_of_mSecurityParameters_34() { return &___mSecurityParameters_34; }
	inline void set_mSecurityParameters_34(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * value)
	{
		___mSecurityParameters_34 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_34), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_35() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mPeerCertificate_35)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mPeerCertificate_35() const { return ___mPeerCertificate_35; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mPeerCertificate_35() { return &___mPeerCertificate_35; }
	inline void set_mPeerCertificate_35(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mPeerCertificate_35 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_35), value);
	}

	inline static int32_t get_offset_of_mOfferedCipherSuites_36() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mOfferedCipherSuites_36)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mOfferedCipherSuites_36() const { return ___mOfferedCipherSuites_36; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mOfferedCipherSuites_36() { return &___mOfferedCipherSuites_36; }
	inline void set_mOfferedCipherSuites_36(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mOfferedCipherSuites_36 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCipherSuites_36), value);
	}

	inline static int32_t get_offset_of_mOfferedCompressionMethods_37() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mOfferedCompressionMethods_37)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mOfferedCompressionMethods_37() const { return ___mOfferedCompressionMethods_37; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mOfferedCompressionMethods_37() { return &___mOfferedCompressionMethods_37; }
	inline void set_mOfferedCompressionMethods_37(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mOfferedCompressionMethods_37 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCompressionMethods_37), value);
	}

	inline static int32_t get_offset_of_mClientExtensions_38() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mClientExtensions_38)); }
	inline RuntimeObject* get_mClientExtensions_38() const { return ___mClientExtensions_38; }
	inline RuntimeObject** get_address_of_mClientExtensions_38() { return &___mClientExtensions_38; }
	inline void set_mClientExtensions_38(RuntimeObject* value)
	{
		___mClientExtensions_38 = value;
		Il2CppCodeGenWriteBarrier((&___mClientExtensions_38), value);
	}

	inline static int32_t get_offset_of_mServerExtensions_39() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mServerExtensions_39)); }
	inline RuntimeObject* get_mServerExtensions_39() const { return ___mServerExtensions_39; }
	inline RuntimeObject** get_address_of_mServerExtensions_39() { return &___mServerExtensions_39; }
	inline void set_mServerExtensions_39(RuntimeObject* value)
	{
		___mServerExtensions_39 = value;
		Il2CppCodeGenWriteBarrier((&___mServerExtensions_39), value);
	}

	inline static int32_t get_offset_of_mConnectionState_40() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mConnectionState_40)); }
	inline int16_t get_mConnectionState_40() const { return ___mConnectionState_40; }
	inline int16_t* get_address_of_mConnectionState_40() { return &___mConnectionState_40; }
	inline void set_mConnectionState_40(int16_t value)
	{
		___mConnectionState_40 = value;
	}

	inline static int32_t get_offset_of_mResumedSession_41() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mResumedSession_41)); }
	inline bool get_mResumedSession_41() const { return ___mResumedSession_41; }
	inline bool* get_address_of_mResumedSession_41() { return &___mResumedSession_41; }
	inline void set_mResumedSession_41(bool value)
	{
		___mResumedSession_41 = value;
	}

	inline static int32_t get_offset_of_mReceivedChangeCipherSpec_42() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mReceivedChangeCipherSpec_42)); }
	inline bool get_mReceivedChangeCipherSpec_42() const { return ___mReceivedChangeCipherSpec_42; }
	inline bool* get_address_of_mReceivedChangeCipherSpec_42() { return &___mReceivedChangeCipherSpec_42; }
	inline void set_mReceivedChangeCipherSpec_42(bool value)
	{
		___mReceivedChangeCipherSpec_42 = value;
	}

	inline static int32_t get_offset_of_mSecureRenegotiation_43() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSecureRenegotiation_43)); }
	inline bool get_mSecureRenegotiation_43() const { return ___mSecureRenegotiation_43; }
	inline bool* get_address_of_mSecureRenegotiation_43() { return &___mSecureRenegotiation_43; }
	inline void set_mSecureRenegotiation_43(bool value)
	{
		___mSecureRenegotiation_43 = value;
	}

	inline static int32_t get_offset_of_mAllowCertificateStatus_44() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAllowCertificateStatus_44)); }
	inline bool get_mAllowCertificateStatus_44() const { return ___mAllowCertificateStatus_44; }
	inline bool* get_address_of_mAllowCertificateStatus_44() { return &___mAllowCertificateStatus_44; }
	inline void set_mAllowCertificateStatus_44(bool value)
	{
		___mAllowCertificateStatus_44 = value;
	}

	inline static int32_t get_offset_of_mExpectSessionTicket_45() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mExpectSessionTicket_45)); }
	inline bool get_mExpectSessionTicket_45() const { return ___mExpectSessionTicket_45; }
	inline bool* get_address_of_mExpectSessionTicket_45() { return &___mExpectSessionTicket_45; }
	inline void set_mExpectSessionTicket_45(bool value)
	{
		___mExpectSessionTicket_45 = value;
	}

	inline static int32_t get_offset_of_mBlocking_46() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mBlocking_46)); }
	inline bool get_mBlocking_46() const { return ___mBlocking_46; }
	inline bool* get_address_of_mBlocking_46() { return &___mBlocking_46; }
	inline void set_mBlocking_46(bool value)
	{
		___mBlocking_46 = value;
	}

	inline static int32_t get_offset_of_mInputBuffers_47() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mInputBuffers_47)); }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * get_mInputBuffers_47() const { return ___mInputBuffers_47; }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A ** get_address_of_mInputBuffers_47() { return &___mInputBuffers_47; }
	inline void set_mInputBuffers_47(ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * value)
	{
		___mInputBuffers_47 = value;
		Il2CppCodeGenWriteBarrier((&___mInputBuffers_47), value);
	}

	inline static int32_t get_offset_of_mOutputBuffer_48() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mOutputBuffer_48)); }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * get_mOutputBuffer_48() const { return ___mOutputBuffer_48; }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A ** get_address_of_mOutputBuffer_48() { return &___mOutputBuffer_48; }
	inline void set_mOutputBuffer_48(ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * value)
	{
		___mOutputBuffer_48 = value;
		Il2CppCodeGenWriteBarrier((&___mOutputBuffer_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOL_T115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1_H
#ifndef BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#define BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_T17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9_H
#ifndef ZOUTPUTSTREAM_T504E5B441A273254366752284D503186AF93F77A_H
#define ZOUTPUTSTREAM_T504E5B441A273254366752284D503186AF93F77A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream
struct  ZOutputStream_t504E5B441A273254366752284D503186AF93F77A  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZStream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::z
	ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * ___z_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::flushLevel
	int32_t ___flushLevel_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::compress
	bool ___compress_10;
	// System.IO.Stream BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::output
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___output_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Zlib.ZOutputStream::closed
	bool ___closed_12;

public:
	inline static int32_t get_offset_of_z_6() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___z_6)); }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * get_z_6() const { return ___z_6; }
	inline ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F ** get_address_of_z_6() { return &___z_6; }
	inline void set_z_6(ZStream_tF8878DF452A2750584EBDA42245AB6BFADF4FB7F * value)
	{
		___z_6 = value;
		Il2CppCodeGenWriteBarrier((&___z_6), value);
	}

	inline static int32_t get_offset_of_flushLevel_7() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___flushLevel_7)); }
	inline int32_t get_flushLevel_7() const { return ___flushLevel_7; }
	inline int32_t* get_address_of_flushLevel_7() { return &___flushLevel_7; }
	inline void set_flushLevel_7(int32_t value)
	{
		___flushLevel_7 = value;
	}

	inline static int32_t get_offset_of_buf_8() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___buf_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_8() const { return ___buf_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_8() { return &___buf_8; }
	inline void set_buf_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf_8), value);
	}

	inline static int32_t get_offset_of_buf1_9() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___buf1_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_9() const { return ___buf1_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_9() { return &___buf1_9; }
	inline void set_buf1_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_9 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_9), value);
	}

	inline static int32_t get_offset_of_compress_10() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___compress_10)); }
	inline bool get_compress_10() const { return ___compress_10; }
	inline bool* get_address_of_compress_10() { return &___compress_10; }
	inline void set_compress_10(bool value)
	{
		___compress_10 = value;
	}

	inline static int32_t get_offset_of_output_11() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___output_11)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_output_11() const { return ___output_11; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_output_11() { return &___output_11; }
	inline void set_output_11(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___output_11 = value;
		Il2CppCodeGenWriteBarrier((&___output_11), value);
	}

	inline static int32_t get_offset_of_closed_12() { return static_cast<int32_t>(offsetof(ZOutputStream_t504E5B441A273254366752284D503186AF93F77A, ___closed_12)); }
	inline bool get_closed_12() const { return ___closed_12; }
	inline bool* get_address_of_closed_12() { return &___closed_12; }
	inline void set_closed_12(bool value)
	{
		___closed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOUTPUTSTREAM_T504E5B441A273254366752284D503186AF93F77A_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef DIGESTINPUTBUFFER_T865CBD8CE9D77326DB82C209343F489D2AE80661_H
#define DIGESTINPUTBUFFER_T865CBD8CE9D77326DB82C209343F489D2AE80661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigestInputBuffer
struct  DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTINPUTBUFFER_T865CBD8CE9D77326DB82C209343F489D2AE80661_H
#ifndef DIGSTREAM_T1FA6B57887FC69D74CA22E04E5AC46A634588B35_H
#define DIGSTREAM_T1FA6B57887FC69D74CA22E04E5AC46A634588B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigestInputBuffer_DigStream
struct  DigStream_t1FA6B57887FC69D74CA22E04E5AC46A634588B35  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DigestInputBuffer_DigStream::d
	RuntimeObject* ___d_6;

public:
	inline static int32_t get_offset_of_d_6() { return static_cast<int32_t>(offsetof(DigStream_t1FA6B57887FC69D74CA22E04E5AC46A634588B35, ___d_6)); }
	inline RuntimeObject* get_d_6() const { return ___d_6; }
	inline RuntimeObject** get_address_of_d_6() { return &___d_6; }
	inline void set_d_6(RuntimeObject* value)
	{
		___d_6 = value;
		Il2CppCodeGenWriteBarrier((&___d_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGSTREAM_T1FA6B57887FC69D74CA22E04E5AC46A634588B35_H
#ifndef RECORDLAYERBUFFER_TBA4221E6564A999025B3A641CF0B16E3423F03DD_H
#define RECORDLAYERBUFFER_TBA4221E6564A999025B3A641CF0B16E3423F03DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.DtlsReliableHandshake_RecordLayerBuffer
struct  RecordLayerBuffer_tBA4221E6564A999025B3A641CF0B16E3423F03DD  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDLAYERBUFFER_TBA4221E6564A999025B3A641CF0B16E3423F03DD_H
#ifndef PAYLOADBUFFER_T8FC23200354825C8EC7E310502F03EF4014D05F2_H
#define PAYLOADBUFFER_T8FC23200354825C8EC7E310502F03EF4014D05F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.HeartbeatMessage_PayloadBuffer
struct  PayloadBuffer_t8FC23200354825C8EC7E310502F03EF4014D05F2  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADBUFFER_T8FC23200354825C8EC7E310502F03EF4014D05F2_H
#ifndef LEGACYTLSCLIENT_T0D1DF0D2E8C25280610E9E06BAADD8940AD99041_H
#define LEGACYTLSCLIENT_T0D1DF0D2E8C25280610E9E06BAADD8940AD99041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsClient
struct  LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041  : public DefaultTlsClient_tFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF
{
public:
	// System.Uri BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsClient::TargetUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___TargetUri_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ICertificateVerifyer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsClient::verifyer
	RuntimeObject* ___verifyer_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.IClientCredentialsProvider BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.LegacyTlsClient::credProvider
	RuntimeObject* ___credProvider_15;

public:
	inline static int32_t get_offset_of_TargetUri_13() { return static_cast<int32_t>(offsetof(LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041, ___TargetUri_13)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_TargetUri_13() const { return ___TargetUri_13; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_TargetUri_13() { return &___TargetUri_13; }
	inline void set_TargetUri_13(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___TargetUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___TargetUri_13), value);
	}

	inline static int32_t get_offset_of_verifyer_14() { return static_cast<int32_t>(offsetof(LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041, ___verifyer_14)); }
	inline RuntimeObject* get_verifyer_14() const { return ___verifyer_14; }
	inline RuntimeObject** get_address_of_verifyer_14() { return &___verifyer_14; }
	inline void set_verifyer_14(RuntimeObject* value)
	{
		___verifyer_14 = value;
		Il2CppCodeGenWriteBarrier((&___verifyer_14), value);
	}

	inline static int32_t get_offset_of_credProvider_15() { return static_cast<int32_t>(offsetof(LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041, ___credProvider_15)); }
	inline RuntimeObject* get_credProvider_15() const { return ___credProvider_15; }
	inline RuntimeObject** get_address_of_credProvider_15() { return &___credProvider_15; }
	inline void set_credProvider_15(RuntimeObject* value)
	{
		___credProvider_15 = value;
		Il2CppCodeGenWriteBarrier((&___credProvider_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYTLSCLIENT_T0D1DF0D2E8C25280610E9E06BAADD8940AD99041_H
#ifndef HANDSHAKEHASHUPDATESTREAM_TF402BC6A0924513202127BFAC93D61F5600481F7_H
#define HANDSHAKEHASHUPDATESTREAM_TF402BC6A0924513202127BFAC93D61F5600481F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_HandshakeHashUpdateStream
struct  HandshakeHashUpdateStream_tF402BC6A0924513202127BFAC93D61F5600481F7  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream_HandshakeHashUpdateStream::mOuter
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * ___mOuter_6;

public:
	inline static int32_t get_offset_of_mOuter_6() { return static_cast<int32_t>(offsetof(HandshakeHashUpdateStream_tF402BC6A0924513202127BFAC93D61F5600481F7, ___mOuter_6)); }
	inline RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * get_mOuter_6() const { return ___mOuter_6; }
	inline RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 ** get_address_of_mOuter_6() { return &___mOuter_6; }
	inline void set_mOuter_6(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * value)
	{
		___mOuter_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOuter_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEHASHUPDATESTREAM_TF402BC6A0924513202127BFAC93D61F5600481F7_H
#ifndef SIGNERINPUTBUFFER_TD958DFB6E004BB3CE1CC1B5CE5012FC0D0BAC77E_H
#define SIGNERINPUTBUFFER_TD958DFB6E004BB3CE1CC1B5CE5012FC0D0BAC77E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignerInputBuffer
struct  SignerInputBuffer_tD958DFB6E004BB3CE1CC1B5CE5012FC0D0BAC77E  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINPUTBUFFER_TD958DFB6E004BB3CE1CC1B5CE5012FC0D0BAC77E_H
#ifndef SIGSTREAM_T6B05C0E5C559B28874C6E2211B1258B33EBD3643_H
#define SIGSTREAM_T6B05C0E5C559B28874C6E2211B1258B33EBD3643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignerInputBuffer_SigStream
struct  SigStream_t6B05C0E5C559B28874C6E2211B1258B33EBD3643  : public BaseOutputStream_t17DB07CA94065F6B7BA125D4C6DF5AE74702C8B9
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SignerInputBuffer_SigStream::s
	RuntimeObject* ___s_6;

public:
	inline static int32_t get_offset_of_s_6() { return static_cast<int32_t>(offsetof(SigStream_t6B05C0E5C559B28874C6E2211B1258B33EBD3643, ___s_6)); }
	inline RuntimeObject* get_s_6() const { return ___s_6; }
	inline RuntimeObject** get_address_of_s_6() { return &___s_6; }
	inline void set_s_6(RuntimeObject* value)
	{
		___s_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGSTREAM_T6B05C0E5C559B28874C6E2211B1258B33EBD3643_H
#ifndef TLSCLIENTPROTOCOL_TBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152_H
#define TLSCLIENTPROTOCOL_TBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol
struct  TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152  : public TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClient BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClient
	RuntimeObject* ___mTlsClient_49;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClientContext
	TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * ___mTlsClientContext_50;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mSelectedSessionID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSelectedSessionID_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mKeyExchange
	RuntimeObject* ___mKeyExchange_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAuthentication BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mAuthentication
	RuntimeObject* ___mAuthentication_53;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateStatus
	CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * ___mCertificateStatus_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateRequest
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * ___mCertificateRequest_55;

public:
	inline static int32_t get_offset_of_mTlsClient_49() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mTlsClient_49)); }
	inline RuntimeObject* get_mTlsClient_49() const { return ___mTlsClient_49; }
	inline RuntimeObject** get_address_of_mTlsClient_49() { return &___mTlsClient_49; }
	inline void set_mTlsClient_49(RuntimeObject* value)
	{
		___mTlsClient_49 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsClient_49), value);
	}

	inline static int32_t get_offset_of_mTlsClientContext_50() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mTlsClientContext_50)); }
	inline TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * get_mTlsClientContext_50() const { return ___mTlsClientContext_50; }
	inline TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 ** get_address_of_mTlsClientContext_50() { return &___mTlsClientContext_50; }
	inline void set_mTlsClientContext_50(TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * value)
	{
		___mTlsClientContext_50 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsClientContext_50), value);
	}

	inline static int32_t get_offset_of_mSelectedSessionID_51() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mSelectedSessionID_51)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSelectedSessionID_51() const { return ___mSelectedSessionID_51; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSelectedSessionID_51() { return &___mSelectedSessionID_51; }
	inline void set_mSelectedSessionID_51(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSelectedSessionID_51 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectedSessionID_51), value);
	}

	inline static int32_t get_offset_of_mKeyExchange_52() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mKeyExchange_52)); }
	inline RuntimeObject* get_mKeyExchange_52() const { return ___mKeyExchange_52; }
	inline RuntimeObject** get_address_of_mKeyExchange_52() { return &___mKeyExchange_52; }
	inline void set_mKeyExchange_52(RuntimeObject* value)
	{
		___mKeyExchange_52 = value;
		Il2CppCodeGenWriteBarrier((&___mKeyExchange_52), value);
	}

	inline static int32_t get_offset_of_mAuthentication_53() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mAuthentication_53)); }
	inline RuntimeObject* get_mAuthentication_53() const { return ___mAuthentication_53; }
	inline RuntimeObject** get_address_of_mAuthentication_53() { return &___mAuthentication_53; }
	inline void set_mAuthentication_53(RuntimeObject* value)
	{
		___mAuthentication_53 = value;
		Il2CppCodeGenWriteBarrier((&___mAuthentication_53), value);
	}

	inline static int32_t get_offset_of_mCertificateStatus_54() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mCertificateStatus_54)); }
	inline CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * get_mCertificateStatus_54() const { return ___mCertificateStatus_54; }
	inline CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB ** get_address_of_mCertificateStatus_54() { return &___mCertificateStatus_54; }
	inline void set_mCertificateStatus_54(CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * value)
	{
		___mCertificateStatus_54 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateStatus_54), value);
	}

	inline static int32_t get_offset_of_mCertificateRequest_55() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mCertificateRequest_55)); }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * get_mCertificateRequest_55() const { return ___mCertificateRequest_55; }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 ** get_address_of_mCertificateRequest_55() { return &___mCertificateRequest_55; }
	inline void set_mCertificateRequest_55(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * value)
	{
		___mCertificateRequest_55 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateRequest_55), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTPROTOCOL_TBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152_H
#ifndef DEFLATEOUTPUTSTREAM_T772404FEF39123AAB44AEB4B9B108FB457249345_H
#define DEFLATEOUTPUTSTREAM_T772404FEF39123AAB44AEB4B9B108FB457249345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression_DeflateOutputStream
struct  DeflateOutputStream_t772404FEF39123AAB44AEB4B9B108FB457249345  : public ZOutputStream_t504E5B441A273254366752284D503186AF93F77A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEOUTPUTSTREAM_T772404FEF39123AAB44AEB4B9B108FB457249345_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (DefaultTlsClient_tFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4500[1] = 
{
	DefaultTlsClient_tFC8CFB42C44E7E1F1916DB48364C34DFFA5C10BF::get_offset_of_mDHVerifier_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5), -1, sizeof(DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4501[4] = 
{
	DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_StaticFields::get_offset_of_DefaultMinimumPrimeBits_0(),
	DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5_StaticFields::get_offset_of_DefaultGroups_1(),
	DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5::get_offset_of_mGroups_2(),
	DefaultTlsDHVerifier_t758FEF6DAB13BF8EF0FEC2B2823632D01A8D61F5::get_offset_of_mMinimumPrimeBits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4502[3] = 
{
	DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0::get_offset_of_mContext_0(),
	DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0::get_offset_of_mCertificate_1(),
	DefaultTlsEncryptionCredentials_t840656DEDEB1CDDC50AFC317D1E17AA91D22D0A0::get_offset_of_mPrivateKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (DefaultTlsServer_t50C2F11D4D70DE3E7451DFB2A6DE92BDE6B8D4F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { sizeof (DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4504[5] = 
{
	DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D::get_offset_of_mContext_0(),
	DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D::get_offset_of_mCertificate_1(),
	DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D::get_offset_of_mPrivateKey_2(),
	DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D::get_offset_of_mSignatureAndHashAlgorithm_3(),
	DefaultTlsSignerCredentials_t54B0001684AD474B9BD2EC2471A6DE5899F1B43D::get_offset_of_mSigner_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { sizeof (DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51), -1, sizeof(DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4505[2] = 
{
	DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51_StaticFields::get_offset_of_DefaultGroups_0(),
	DefaultTlsSrpGroupVerifier_tD33F55BA26CEAD2B0EBE50050D88F2DE66393E51::get_offset_of_mGroups_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4506[5] = 
{
	0,
	DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80::get_offset_of_mContext_1(),
	DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80::get_offset_of_mBuf_2(),
	DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80::get_offset_of_mHashes_3(),
	DeferredHash_t26F4928C27F9DCFB925F5FB13AE42335C518BC80::get_offset_of_mPrfHashAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (DigestInputBuffer_t865CBD8CE9D77326DB82C209343F489D2AE80661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (DigStream_t1FA6B57887FC69D74CA22E04E5AC46A634588B35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4508[1] = 
{
	DigStream_t1FA6B57887FC69D74CA22E04E5AC46A634588B35::get_offset_of_d_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (DigitallySigned_t85D6E9B59B93258F44E26C20EA73305E45407A8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4509[2] = 
{
	DigitallySigned_t85D6E9B59B93258F44E26C20EA73305E45407A8D::get_offset_of_mAlgorithm_0(),
	DigitallySigned_t85D6E9B59B93258F44E26C20EA73305E45407A8D::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (DtlsClientProtocol_t3C84E51886D54A0F94231FEB717E2ED8A9A618A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4511[18] = 
{
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_client_0(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_clientContext_1(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_tlsSession_2(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_sessionParameters_3(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_sessionParametersBuilder_4(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_offeredCipherSuites_5(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_clientExtensions_6(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_serverExtensions_7(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_selectedSessionID_8(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_resumedSession_9(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_secure_renegotiation_10(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_allowCertificateStatus_11(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_expectSessionTicket_12(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_keyExchange_13(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_authentication_14(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_certificateStatus_15(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_certificateRequest_16(),
	ClientHandshakeState_t2C4EFBFA2C822E4342DBFE0F401BB811FEAEB996::get_offset_of_clientCredentials_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4512[4] = 
{
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F::get_offset_of_mReplayWindow_0(),
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F::get_offset_of_mEpoch_1(),
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F::get_offset_of_mCipher_2(),
	DtlsEpoch_t71553EB988CD4E1F8A8E6DFCB723DA21397A2A2F::get_offset_of_mSequenceNumber_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (DtlsProtocol_t27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4514[1] = 
{
	DtlsProtocol_t27D8075738D0330D2CE7D2F72D9B5B0DFD11DF76::get_offset_of_mSecureRandom_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4515[3] = 
{
	DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1::get_offset_of_mMsgType_0(),
	DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1::get_offset_of_mBody_1(),
	DtlsReassembler_t9AD275AAEA95FB96FDA350E61456423BF18CA0A1::get_offset_of_mMissing_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (Range_t36ACB4181D3602DB43EAD01460CB7158C1DC8F83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4516[2] = 
{
	Range_t36ACB4181D3602DB43EAD01460CB7158C1DC8F83::get_offset_of_mStart_0(),
	Range_t36ACB4181D3602DB43EAD01460CB7158C1DC8F83::get_offset_of_mEnd_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4517[21] = 
{
	0,
	0,
	0,
	0,
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mTransport_4(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mContext_5(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mPeer_6(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mRecordQueue_7(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mClosed_8(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mFailed_9(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mReadVersion_10(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mWriteVersion_11(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mInHandshake_12(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mPlaintextLimit_13(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mCurrentEpoch_14(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mPendingEpoch_15(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mReadEpoch_16(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mWriteEpoch_17(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mRetransmit_18(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mRetransmitEpoch_19(),
	DtlsRecordLayer_t2C5EFA00C70AF3F6D8D9BE5B462138D3D1C820BC::get_offset_of_mRetransmitExpiry_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { sizeof (DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4518[10] = 
{
	0,
	0,
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mRecordLayer_2(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mHandshakeHash_3(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mCurrentInboundFlight_4(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mPreviousInboundFlight_5(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mOutboundFlight_6(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mSending_7(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mMessageSeq_8(),
	DtlsReliableHandshake_tFF56061C35E5A8905ED6FE6DA94D076B2A8B1082::get_offset_of_mNextReceiveSeq_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4519[3] = 
{
	Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3::get_offset_of_mMessageSeq_0(),
	Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3::get_offset_of_mMsgType_1(),
	Message_t6144210DA80B8DC1FFCC1922C94000031A5C7EE3::get_offset_of_mBody_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { sizeof (RecordLayerBuffer_tBA4221E6564A999025B3A641CF0B16E3423F03DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (Retransmit_t6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4521[1] = 
{
	Retransmit_t6BFA641736F0E3E8692F7EA58D1490FD5FBBC1EC::get_offset_of_mOuter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { sizeof (DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4522[4] = 
{
	0,
	0,
	DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C::get_offset_of_mLatestConfirmedSeq_2(),
	DtlsReplayWindow_t205154583BA5F7E128BDED7524E33C94AB273A9C::get_offset_of_mBitmap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { sizeof (DtlsServerProtocol_tEAB0A17839E6FE5575AE322DBAAC6A88B28B7019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4523[1] = 
{
	DtlsServerProtocol_tEAB0A17839E6FE5575AE322DBAAC6A88B28B7019::get_offset_of_mVerifyRequests_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { sizeof (ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4524[18] = 
{
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_server_0(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_serverContext_1(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_tlsSession_2(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_sessionParameters_3(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_sessionParametersBuilder_4(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_offeredCipherSuites_5(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_offeredCompressionMethods_6(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_clientExtensions_7(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_serverExtensions_8(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_resumedSession_9(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_secure_renegotiation_10(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_allowCertificateStatus_11(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_expectSessionTicket_12(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_keyExchange_13(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_serverCredentials_14(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_certificateRequest_15(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_clientCertificateType_16(),
	ServerHandshakeState_tAD66F4DA91911EF994F76CD700F8DE037996AA16::get_offset_of_clientCertificate_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { sizeof (DtlsTransport_tA7709949573CF90A024B57894F78605F2591BCD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4525[1] = 
{
	DtlsTransport_tA7709949573CF90A024B57894F78605F2591BCD4::get_offset_of_mRecordLayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (ECBasisType_t0985027DBDAFA73665EC060C99F4997BCED1C858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4526[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (ECCurveType_t36C8F053E5A2CC8A07F3E35239A257BA89606FB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4527[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (ECPointFormat_t5D3581724A6C6EBADD4BFE3541C23A41EFE94DEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4528[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (EncryptionAlgorithm_t7391CF2B6E14F38EF5EA84A91549C484B9641251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4529[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (ExporterLabel_tD8CC03A7E7188DD15827E288B61C943C83E4FF1E), -1, sizeof(ExporterLabel_tD8CC03A7E7188DD15827E288B61C943C83E4FF1E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4530[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExporterLabel_tD8CC03A7E7188DD15827E288B61C943C83E4FF1E_StaticFields::get_offset_of_extended_master_secret_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9), -1, sizeof(ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4531[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_StaticFields::get_offset_of_DRAFT_token_binding_25(),
	0,
	0,
	ExtensionType_t0ED47F3F5DAF3E608C2640B6961F5863E73E23F9_StaticFields::get_offset_of_negotiated_ff_dhe_groups_28(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (FiniteFieldDheGroup_t4CE0827C487C0B29D2273D772F92FD4CB777FCC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4532[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (HandshakeType_t4BA8E7DA9536F26D80DC7393C6BA2CE4770F1939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (HashAlgorithm_t23CA04B8F7513B6FB35BD9BC0E15EA8313E50235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4534[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (HeartbeatExtension_t444903CD5FC86CDCCC836B7C5B66050667DA6A2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4535[1] = 
{
	HeartbeatExtension_t444903CD5FC86CDCCC836B7C5B66050667DA6A2C::get_offset_of_mMode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4536[3] = 
{
	HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198::get_offset_of_mType_0(),
	HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198::get_offset_of_mPayload_1(),
	HeartbeatMessage_t0B339A92B1A5FA046A24CCD83178D9EE048F0198::get_offset_of_mPaddingLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (PayloadBuffer_t8FC23200354825C8EC7E310502F03EF4014D05F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (HeartbeatMessageType_tD0DB71717B8AFB5345E62F87D13D364BEE4FA72C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4538[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (HeartbeatMode_tB95F20C85C7C4786DF7F5F329A15B6757C8AB618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4539[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (KeyExchangeAlgorithm_t4432C2D0FF6BD00FDD6C268733B0404E60672DDE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4541[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4543[3] = 
{
	LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282::get_offset_of_verifyer_0(),
	LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282::get_offset_of_credProvider_1(),
	LegacyTlsAuthentication_t110E4197E5A41ABF40FAE4B50ED443DA14026282::get_offset_of_TargetUri_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4544[3] = 
{
	LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041::get_offset_of_TargetUri_13(),
	LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041::get_offset_of_verifyer_14(),
	LegacyTlsClient_t0D1DF0D2E8C25280610E9E06BAADD8940AD99041::get_offset_of_credProvider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (MacAlgorithm_t0A3E04B3D7C1C2106C764E1DC2E0DD3E8C602E52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4545[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (MaxFragmentLength_tCA3E065AC0A1B4BAB6A9888040CD54EE1DC04D96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4546[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (NamedCurve_tF3AB803EA45E9F8964D59419678F9D140C8C2C2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4547[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (NameType_t7FBF728941842E4B9126CDEBF63A21AF02DCFC9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4548[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (NewSessionTicket_tD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4549[2] = 
{
	NewSessionTicket_tD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD::get_offset_of_mTicketLifetimeHint_0(),
	NewSessionTicket_tD33D2CA555A7A6ADC9F25CF8EA514C8652E8F6BD::get_offset_of_mTicket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (OcspStatusRequest_t564046612A9E998EF7E0BCBB300A4371A9A1DC05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4550[2] = 
{
	OcspStatusRequest_t564046612A9E998EF7E0BCBB300A4371A9A1DC05::get_offset_of_mResponderIDList_0(),
	OcspStatusRequest_t564046612A9E998EF7E0BCBB300A4371A9A1DC05::get_offset_of_mRequestExtensions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (PrfAlgorithm_t3CB4C47FCE4BD902E34302D37267779548D0A1C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4551[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450), -1, sizeof(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4552[8] = 
{
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields::get_offset_of_SSLv3_0(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields::get_offset_of_TLSv10_1(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields::get_offset_of_TLSv11_2(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields::get_offset_of_TLSv12_3(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields::get_offset_of_DTLSv10_4(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450_StaticFields::get_offset_of_DTLSv12_5(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450::get_offset_of_version_6(),
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (PskTlsClient_t665AC937EB3C6C9710F37F154B8F669D3C854F57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4553[2] = 
{
	PskTlsClient_t665AC937EB3C6C9710F37F154B8F669D3C854F57::get_offset_of_mDHVerifier_12(),
	PskTlsClient_t665AC937EB3C6C9710F37F154B8F669D3C854F57::get_offset_of_mPskIdentity_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (PskTlsServer_t34AD59BCC96D43E80DB1D4FED765F3E25B69AC13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4554[1] = 
{
	PskTlsServer_t34AD59BCC96D43E80DB1D4FED765F3E25B69AC13::get_offset_of_mPskIdentityManager_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4555[25] = 
{
	0,
	0,
	0,
	0,
	0,
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mHandler_5(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mInput_6(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mOutput_7(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mPendingCompression_8(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mReadCompression_9(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mWriteCompression_10(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mPendingCipher_11(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mReadCipher_12(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mWriteCipher_13(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mReadSeqNo_14(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mWriteSeqNo_15(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mBuffer_16(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mHandshakeHash_17(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mHandshakeHashUpdater_18(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mReadVersion_19(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mWriteVersion_20(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mRestrictReadVersion_21(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mPlaintextLimit_22(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mCompressedLimit_23(),
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088::get_offset_of_mCiphertextLimit_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (HandshakeHashUpdateStream_tF402BC6A0924513202127BFAC93D61F5600481F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4556[1] = 
{
	HandshakeHashUpdateStream_tF402BC6A0924513202127BFAC93D61F5600481F7::get_offset_of_mOuter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4557[2] = 
{
	SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2::get_offset_of_value_0(),
	SequenceNumber_t2948AFA48DBAB579E83FB3A96BC2E64E1B4761B2::get_offset_of_exhausted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4558[15] = 
{
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_entity_0(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_cipherSuite_1(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_compressionAlgorithm_2(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_prfAlgorithm_3(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_verifyDataLength_4(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_masterSecret_5(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_clientRandom_6(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_serverRandom_7(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_sessionHash_8(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_pskIdentity_9(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_srpIdentity_10(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_maxFragmentLength_11(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_truncatedHMac_12(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_encryptThenMac_13(),
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA::get_offset_of_extendedMasterSecret_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (ServerName_t84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4559[2] = 
{
	ServerName_t84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1::get_offset_of_mNameType_0(),
	ServerName_t84EB3B7E8044F9F9F66DEEB8E0858AFC3E5544E1::get_offset_of_mName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (ServerNameList_tCCD2FC99552BA4447B057CA4ADE54CD2732A937E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4560[1] = 
{
	ServerNameList_tCCD2FC99552BA4447B057CA4ADE54CD2732A937E::get_offset_of_mServerNameList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (ServerOnlyTlsAuthentication_t163D9B5BD47E1C5E5589B7C7D6E2E29F46E52E03), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4562[4] = 
{
	ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22::get_offset_of_m_N_0(),
	ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22::get_offset_of_m_g_1(),
	ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22::get_offset_of_m_B_2(),
	ServerSrpParams_t4292841705ED7CEB4AC5C1EA40E89A5120F44C22::get_offset_of_m_s_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4563[8] = 
{
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mCipherSuite_0(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mCompressionAlgorithm_1(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mMasterSecret_2(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mPeerCertificate_3(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mPskIdentity_4(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mSrpIdentity_5(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mEncodedServerExtensions_6(),
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC::get_offset_of_mExtendedMasterSecret_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4564[8] = 
{
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mCipherSuite_0(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mCompressionAlgorithm_1(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mMasterSecret_2(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mPeerCertificate_3(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mPskIdentity_4(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mSrpIdentity_5(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mEncodedServerExtensions_6(),
	Builder_t05AC3ADAB74BF0D556FD0D43E2FCE1F82BBBD524::get_offset_of_mExtendedMasterSecret_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (SignatureAlgorithm_tB005F7945F47B8B1C441511F99658F76EB5D2586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4565[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4566[2] = 
{
	SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148::get_offset_of_mHash_0(),
	SignatureAndHashAlgorithm_tE68BB671672A1EE87744F89E78B4D68B5CEEF148::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (SignerInputBuffer_tD958DFB6E004BB3CE1CC1B5CE5012FC0D0BAC77E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (SigStream_t6B05C0E5C559B28874C6E2211B1258B33EBD3643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4568[1] = 
{
	SigStream_t6B05C0E5C559B28874C6E2211B1258B33EBD3643::get_offset_of_s_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1), -1, sizeof(SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4569[5] = 
{
	SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1_StaticFields::get_offset_of_PREFIX_PASSWORD_0(),
	SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1_StaticFields::get_offset_of_PREFIX_SALT_1(),
	SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1::get_offset_of_mGroup_2(),
	SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1::get_offset_of_mVerifierGenerator_3(),
	SimulatedTlsSrpIdentityManager_tC174941697F591C10F99C966071C3F0B89AC3FC1::get_offset_of_mMac_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4570[3] = 
{
	SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C::get_offset_of_mGroupVerifier_12(),
	SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C::get_offset_of_mIdentity_13(),
	SrpTlsClient_t573B722623BD3EE9354EF392AFF9722ACDE60D0C::get_offset_of_mPassword_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4571[3] = 
{
	SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B::get_offset_of_mSrpIdentityManager_19(),
	SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B::get_offset_of_mSrpIdentity_20(),
	SrpTlsServer_tF3BED52B39E640E966915C47D2A14E4BEF2CFE8B::get_offset_of_mLoginParameters_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (SrtpProtectionProfile_t18DAA3CED2E38323AF053D29060B51ABEA45D31A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4572[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6), -1, sizeof(Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4573[7] = 
{
	0,
	0,
	Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_StaticFields::get_offset_of_IPAD_2(),
	Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6_StaticFields::get_offset_of_OPAD_3(),
	Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6::get_offset_of_digest_4(),
	Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6::get_offset_of_padLength_5(),
	Ssl3Mac_t9FD6C8BC14C7331768A8AD72ECAC8AC5158D13A6::get_offset_of_secret_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (SupplementalDataEntry_tC9D387C5F209B97733AD8889F8A8AF73532AC1E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4574[2] = 
{
	SupplementalDataEntry_tC9D387C5F209B97733AD8889F8A8AF73532AC1E5::get_offset_of_mDataType_0(),
	SupplementalDataEntry_tC9D387C5F209B97733AD8889F8A8AF73532AC1E5::get_offset_of_mData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (SupplementalDataType_t3E47A54A05974E08048D412A6C904E158891386F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4575[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4576[10] = 
{
	0,
	0,
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_context_2(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_macSize_3(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_record_iv_length_4(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_encryptCipher_5(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_decryptCipher_6(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_encryptImplicitNonce_7(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_decryptImplicitNonce_8(),
	TlsAeadCipher_t2028B232AE59BD6AD5541DCDE1FA5AF4AA783F45::get_offset_of_nonceMode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4579[9] = 
{
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_context_0(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_randomData_1(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_useExplicitIV_2(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_encryptThenMac_3(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_encryptCipher_4(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_decryptCipher_5(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_mWriteMac_6(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_mReadMac_7(),
	TlsBlockCipher_t47C84E1200534056DC7942C00E7E68B884EA0E73::get_offset_of_explicitIV_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4585[7] = 
{
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mTlsClient_49(),
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mTlsClientContext_50(),
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mSelectedSessionID_51(),
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mKeyExchange_52(),
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mAuthentication_53(),
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mCertificateStatus_54(),
	TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152::get_offset_of_mCertificateRequest_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (TlsDeflateCompression_tFD96E04916946BE85A6ABEEC4E993AA1E9C6C164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4590[6] = 
{
	0,
	0,
	0,
	0,
	TlsDeflateCompression_tFD96E04916946BE85A6ABEEC4E993AA1E9C6C164::get_offset_of_zIn_4(),
	TlsDeflateCompression_tFD96E04916946BE85A6ABEEC4E993AA1E9C6C164::get_offset_of_zOut_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (DeflateOutputStream_t772404FEF39123AAB44AEB4B9B108FB457249345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (TlsDheKeyExchange_t11DC49BE408AEAF5377AEA269624705A469BE9CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4592[1] = 
{
	TlsDheKeyExchange_t11DC49BE408AEAF5377AEA269624705A469BE9CC::get_offset_of_mServerCredentials_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4593[7] = 
{
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mTlsSigner_3(),
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mDHVerifier_4(),
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mDHParameters_5(),
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mServerPublicKey_6(),
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mAgreementCredentials_7(),
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mDHAgreePrivateKey_8(),
	TlsDHKeyExchange_tA2DF2A49703DDB34CDFE174FDB77949E91C7C644::get_offset_of_mDHAgreePublicKey_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC), -1, sizeof(TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4594[11] = 
{
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_Two_0(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe2432_p_1(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe2432_2(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe3072_p_3(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe3072_4(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe4096_p_5(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe4096_6(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe6144_p_7(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe6144_8(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe8192_p_9(),
	TlsDHUtilities_t1382E22AB3080BA3F3EFDCB0ED80F4A0485461FC_StaticFields::get_offset_of_draft_ffdhe8192_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (TlsDsaSigner_tE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (TlsDssSigner_t7B3C24FA8AAB97BDD69F015E016C8260239111F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (TlsEccUtilities_t28CD66E8F848B08318274AA1F19FFD8AB0CA2D21), -1, sizeof(TlsEccUtilities_t28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4598[1] = 
{
	TlsEccUtilities_t28CD66E8F848B08318274AA1F19FFD8AB0CA2D21_StaticFields::get_offset_of_CurveNames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (TlsECDheKeyExchange_t3950DD21BC83197C5A6DECB3E7FDB772433CE353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4599[1] = 
{
	TlsECDheKeyExchange_t3950DD21BC83197C5A6DECB3E7FDB772433CE353::get_offset_of_mServerCredentials_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
