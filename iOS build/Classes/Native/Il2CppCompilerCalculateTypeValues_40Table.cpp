﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector
struct Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable
struct AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean
struct DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertID
struct EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2
struct EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo
struct CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedPrivateKeyInfo
struct EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo
struct PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy
struct Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq
struct TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampResp
struct TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo
struct TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NameConstraints
struct NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData
struct CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation
struct SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.IPasswordFinder
struct IPasswordFinder_t931E8E21FCB9FCE838DAFA22D719B1A6816530E6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.AsymmetricKeyEntry
struct AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store/IgnoresCaseHashtable
struct IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPath
struct PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCrlUtilities
struct PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode
struct PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor
struct TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken
struct TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken/CertID
struct CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator
struct TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenInfo
struct TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t585F03B00DBE773170901D2ABF9576187FAF92B5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionary
struct LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject
struct DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexEncoder
struct HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.IEncoder
struct IEncoder_tAE7C352A6FA2084CACA9681099EAC121CC6BC931;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.ITranslator
struct ITranslator_t8684F731D28642F9FD7FAEBDC185637B3638102C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Selector
struct IX509Selector_tA6E469AE4DB2DF9788DB58C58BABA300BA2AD53B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store
struct IX509Store_tAAF2E4C85341CF672C0779CD427B53EC4FFBED9C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate
struct X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerable
struct IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef MISCPEMGENERATOR_T9758CA33CD68BFC382913C19A3C388245CB28D5D_H
#define MISCPEMGENERATOR_T9758CA33CD68BFC382913C19A3C388245CB28D5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.MiscPemGenerator
struct  MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D  : public RuntimeObject
{
public:
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.MiscPemGenerator::obj
	RuntimeObject * ___obj_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.MiscPemGenerator::algorithm
	String_t* ___algorithm_1;
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.MiscPemGenerator::password
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___password_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.MiscPemGenerator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D, ___obj_0)); }
	inline RuntimeObject * get_obj_0() const { return ___obj_0; }
	inline RuntimeObject ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(RuntimeObject * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D, ___algorithm_1)); }
	inline String_t* get_algorithm_1() const { return ___algorithm_1; }
	inline String_t** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(String_t* value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_1), value);
	}

	inline static int32_t get_offset_of_password_2() { return static_cast<int32_t>(offsetof(MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D, ___password_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_password_2() const { return ___password_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_password_2() { return &___password_2; }
	inline void set_password_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___password_2 = value;
		Il2CppCodeGenWriteBarrier((&___password_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCPEMGENERATOR_T9758CA33CD68BFC382913C19A3C388245CB28D5D_H
#ifndef PEMUTILITIES_TD7CB41D267A8A8AE2671AD196FA432DA671FA55F_H
#define PEMUTILITIES_TD7CB41D267A8A8AE2671AD196FA432DA671FA55F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemUtilities
struct  PemUtilities_tD7CB41D267A8A8AE2671AD196FA432DA671FA55F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMUTILITIES_TD7CB41D267A8A8AE2671AD196FA432DA671FA55F_H
#ifndef PKCS8GENERATOR_T513AEB3E26D1991496F48A52874DE2712D4C1648_H
#define PKCS8GENERATOR_T513AEB3E26D1991496F48A52874DE2712D4C1648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator
struct  Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648  : public RuntimeObject
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::password
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___password_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::algorithm
	String_t* ___algorithm_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::iterationCount
	int32_t ___iterationCount_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::privKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___privKey_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_10;

public:
	inline static int32_t get_offset_of_password_6() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648, ___password_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_password_6() const { return ___password_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_password_6() { return &___password_6; }
	inline void set_password_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___password_6 = value;
		Il2CppCodeGenWriteBarrier((&___password_6), value);
	}

	inline static int32_t get_offset_of_algorithm_7() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648, ___algorithm_7)); }
	inline String_t* get_algorithm_7() const { return ___algorithm_7; }
	inline String_t** get_address_of_algorithm_7() { return &___algorithm_7; }
	inline void set_algorithm_7(String_t* value)
	{
		___algorithm_7 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_7), value);
	}

	inline static int32_t get_offset_of_iterationCount_8() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648, ___iterationCount_8)); }
	inline int32_t get_iterationCount_8() const { return ___iterationCount_8; }
	inline int32_t* get_address_of_iterationCount_8() { return &___iterationCount_8; }
	inline void set_iterationCount_8(int32_t value)
	{
		___iterationCount_8 = value;
	}

	inline static int32_t get_offset_of_privKey_9() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648, ___privKey_9)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_privKey_9() const { return ___privKey_9; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_privKey_9() { return &___privKey_9; }
	inline void set_privKey_9(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___privKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___privKey_9), value);
	}

	inline static int32_t get_offset_of_random_10() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648, ___random_10)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_10() const { return ___random_10; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_10() { return &___random_10; }
	inline void set_random_10(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_10 = value;
		Il2CppCodeGenWriteBarrier((&___random_10), value);
	}
};

struct Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::PbeSha1_RC4_128
	String_t* ___PbeSha1_RC4_128_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::PbeSha1_RC4_40
	String_t* ___PbeSha1_RC4_40_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::PbeSha1_3DES
	String_t* ___PbeSha1_3DES_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::PbeSha1_2DES
	String_t* ___PbeSha1_2DES_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::PbeSha1_RC2_128
	String_t* ___PbeSha1_RC2_128_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.Pkcs8Generator::PbeSha1_RC2_40
	String_t* ___PbeSha1_RC2_40_5;

public:
	inline static int32_t get_offset_of_PbeSha1_RC4_128_0() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields, ___PbeSha1_RC4_128_0)); }
	inline String_t* get_PbeSha1_RC4_128_0() const { return ___PbeSha1_RC4_128_0; }
	inline String_t** get_address_of_PbeSha1_RC4_128_0() { return &___PbeSha1_RC4_128_0; }
	inline void set_PbeSha1_RC4_128_0(String_t* value)
	{
		___PbeSha1_RC4_128_0 = value;
		Il2CppCodeGenWriteBarrier((&___PbeSha1_RC4_128_0), value);
	}

	inline static int32_t get_offset_of_PbeSha1_RC4_40_1() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields, ___PbeSha1_RC4_40_1)); }
	inline String_t* get_PbeSha1_RC4_40_1() const { return ___PbeSha1_RC4_40_1; }
	inline String_t** get_address_of_PbeSha1_RC4_40_1() { return &___PbeSha1_RC4_40_1; }
	inline void set_PbeSha1_RC4_40_1(String_t* value)
	{
		___PbeSha1_RC4_40_1 = value;
		Il2CppCodeGenWriteBarrier((&___PbeSha1_RC4_40_1), value);
	}

	inline static int32_t get_offset_of_PbeSha1_3DES_2() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields, ___PbeSha1_3DES_2)); }
	inline String_t* get_PbeSha1_3DES_2() const { return ___PbeSha1_3DES_2; }
	inline String_t** get_address_of_PbeSha1_3DES_2() { return &___PbeSha1_3DES_2; }
	inline void set_PbeSha1_3DES_2(String_t* value)
	{
		___PbeSha1_3DES_2 = value;
		Il2CppCodeGenWriteBarrier((&___PbeSha1_3DES_2), value);
	}

	inline static int32_t get_offset_of_PbeSha1_2DES_3() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields, ___PbeSha1_2DES_3)); }
	inline String_t* get_PbeSha1_2DES_3() const { return ___PbeSha1_2DES_3; }
	inline String_t** get_address_of_PbeSha1_2DES_3() { return &___PbeSha1_2DES_3; }
	inline void set_PbeSha1_2DES_3(String_t* value)
	{
		___PbeSha1_2DES_3 = value;
		Il2CppCodeGenWriteBarrier((&___PbeSha1_2DES_3), value);
	}

	inline static int32_t get_offset_of_PbeSha1_RC2_128_4() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields, ___PbeSha1_RC2_128_4)); }
	inline String_t* get_PbeSha1_RC2_128_4() const { return ___PbeSha1_RC2_128_4; }
	inline String_t** get_address_of_PbeSha1_RC2_128_4() { return &___PbeSha1_RC2_128_4; }
	inline void set_PbeSha1_RC2_128_4(String_t* value)
	{
		___PbeSha1_RC2_128_4 = value;
		Il2CppCodeGenWriteBarrier((&___PbeSha1_RC2_128_4), value);
	}

	inline static int32_t get_offset_of_PbeSha1_RC2_40_5() { return static_cast<int32_t>(offsetof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields, ___PbeSha1_RC2_40_5)); }
	inline String_t* get_PbeSha1_RC2_40_5() const { return ___PbeSha1_RC2_40_5; }
	inline String_t** get_address_of_PbeSha1_RC2_40_5() { return &___PbeSha1_RC2_40_5; }
	inline void set_PbeSha1_RC2_40_5(String_t* value)
	{
		___PbeSha1_RC2_40_5 = value;
		Il2CppCodeGenWriteBarrier((&___PbeSha1_RC2_40_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8GENERATOR_T513AEB3E26D1991496F48A52874DE2712D4C1648_H
#ifndef ENCRYPTEDPRIVATEKEYINFOFACTORY_T7F9B583FBCE4ECF73E308594DD330192E905AEB0_H
#define ENCRYPTEDPRIVATEKEYINFOFACTORY_T7F9B583FBCE4ECF73E308594DD330192E905AEB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.EncryptedPrivateKeyInfoFactory
struct  EncryptedPrivateKeyInfoFactory_t7F9B583FBCE4ECF73E308594DD330192E905AEB0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDPRIVATEKEYINFOFACTORY_T7F9B583FBCE4ECF73E308594DD330192E905AEB0_H
#ifndef PKCS12ENTRY_T6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F_H
#define PKCS12ENTRY_T6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Entry
struct  Pkcs12Entry_t6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Entry::attributes
	RuntimeObject* ___attributes_0;

public:
	inline static int32_t get_offset_of_attributes_0() { return static_cast<int32_t>(offsetof(Pkcs12Entry_t6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F, ___attributes_0)); }
	inline RuntimeObject* get_attributes_0() const { return ___attributes_0; }
	inline RuntimeObject** get_address_of_attributes_0() { return &___attributes_0; }
	inline void set_attributes_0(RuntimeObject* value)
	{
		___attributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12ENTRY_T6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F_H
#ifndef PKCS12STORE_T8A33D3E64E35A36A422302AF1617877011D2DFCE_H
#define PKCS12STORE_T8A33D3E64E35A36A422302AF1617877011D2DFCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store
struct  Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_IgnoresCaseHashtable BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::keys
	IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 * ___keys_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::localIds
	RuntimeObject* ___localIds_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_IgnoresCaseHashtable BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::certs
	IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 * ___certs_2;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::chainCerts
	RuntimeObject* ___chainCerts_3;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::keyCerts
	RuntimeObject* ___keyCerts_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::keyAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___keyAlgorithm_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::certAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___certAlgorithm_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::useDerEncoding
	bool ___useDerEncoding_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.AsymmetricKeyEntry BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store::unmarkedKeyEntry
	AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8 * ___unmarkedKeyEntry_8;

public:
	inline static int32_t get_offset_of_keys_0() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___keys_0)); }
	inline IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 * get_keys_0() const { return ___keys_0; }
	inline IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 ** get_address_of_keys_0() { return &___keys_0; }
	inline void set_keys_0(IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 * value)
	{
		___keys_0 = value;
		Il2CppCodeGenWriteBarrier((&___keys_0), value);
	}

	inline static int32_t get_offset_of_localIds_1() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___localIds_1)); }
	inline RuntimeObject* get_localIds_1() const { return ___localIds_1; }
	inline RuntimeObject** get_address_of_localIds_1() { return &___localIds_1; }
	inline void set_localIds_1(RuntimeObject* value)
	{
		___localIds_1 = value;
		Il2CppCodeGenWriteBarrier((&___localIds_1), value);
	}

	inline static int32_t get_offset_of_certs_2() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___certs_2)); }
	inline IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 * get_certs_2() const { return ___certs_2; }
	inline IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 ** get_address_of_certs_2() { return &___certs_2; }
	inline void set_certs_2(IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5 * value)
	{
		___certs_2 = value;
		Il2CppCodeGenWriteBarrier((&___certs_2), value);
	}

	inline static int32_t get_offset_of_chainCerts_3() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___chainCerts_3)); }
	inline RuntimeObject* get_chainCerts_3() const { return ___chainCerts_3; }
	inline RuntimeObject** get_address_of_chainCerts_3() { return &___chainCerts_3; }
	inline void set_chainCerts_3(RuntimeObject* value)
	{
		___chainCerts_3 = value;
		Il2CppCodeGenWriteBarrier((&___chainCerts_3), value);
	}

	inline static int32_t get_offset_of_keyCerts_4() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___keyCerts_4)); }
	inline RuntimeObject* get_keyCerts_4() const { return ___keyCerts_4; }
	inline RuntimeObject** get_address_of_keyCerts_4() { return &___keyCerts_4; }
	inline void set_keyCerts_4(RuntimeObject* value)
	{
		___keyCerts_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyCerts_4), value);
	}

	inline static int32_t get_offset_of_keyAlgorithm_5() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___keyAlgorithm_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_keyAlgorithm_5() const { return ___keyAlgorithm_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_keyAlgorithm_5() { return &___keyAlgorithm_5; }
	inline void set_keyAlgorithm_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___keyAlgorithm_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyAlgorithm_5), value);
	}

	inline static int32_t get_offset_of_certAlgorithm_6() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___certAlgorithm_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_certAlgorithm_6() const { return ___certAlgorithm_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_certAlgorithm_6() { return &___certAlgorithm_6; }
	inline void set_certAlgorithm_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___certAlgorithm_6 = value;
		Il2CppCodeGenWriteBarrier((&___certAlgorithm_6), value);
	}

	inline static int32_t get_offset_of_useDerEncoding_7() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___useDerEncoding_7)); }
	inline bool get_useDerEncoding_7() const { return ___useDerEncoding_7; }
	inline bool* get_address_of_useDerEncoding_7() { return &___useDerEncoding_7; }
	inline void set_useDerEncoding_7(bool value)
	{
		___useDerEncoding_7 = value;
	}

	inline static int32_t get_offset_of_unmarkedKeyEntry_8() { return static_cast<int32_t>(offsetof(Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE, ___unmarkedKeyEntry_8)); }
	inline AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8 * get_unmarkedKeyEntry_8() const { return ___unmarkedKeyEntry_8; }
	inline AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8 ** get_address_of_unmarkedKeyEntry_8() { return &___unmarkedKeyEntry_8; }
	inline void set_unmarkedKeyEntry_8(AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8 * value)
	{
		___unmarkedKeyEntry_8 = value;
		Il2CppCodeGenWriteBarrier((&___unmarkedKeyEntry_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12STORE_T8A33D3E64E35A36A422302AF1617877011D2DFCE_H
#ifndef CERTID_T764BCE0DF49AE96A163A00055501DED21132C9F8_H
#define CERTID_T764BCE0DF49AE96A163A00055501DED21132C9F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_CertId
struct  CertId_t764BCE0DF49AE96A163A00055501DED21132C9F8  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_CertId::id
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CertId_t764BCE0DF49AE96A163A00055501DED21132C9F8, ___id_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_id_0() const { return ___id_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTID_T764BCE0DF49AE96A163A00055501DED21132C9F8_H
#ifndef IGNORESCASEHASHTABLE_TA7603CFAD6585F0F1E01F6B092129BE22AFECDC5_H
#define IGNORESCASEHASHTABLE_TA7603CFAD6585F0F1E01F6B092129BE22AFECDC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_IgnoresCaseHashtable
struct  IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_IgnoresCaseHashtable::orig
	RuntimeObject* ___orig_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Store_IgnoresCaseHashtable::keys
	RuntimeObject* ___keys_1;

public:
	inline static int32_t get_offset_of_orig_0() { return static_cast<int32_t>(offsetof(IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5, ___orig_0)); }
	inline RuntimeObject* get_orig_0() const { return ___orig_0; }
	inline RuntimeObject** get_address_of_orig_0() { return &___orig_0; }
	inline void set_orig_0(RuntimeObject* value)
	{
		___orig_0 = value;
		Il2CppCodeGenWriteBarrier((&___orig_0), value);
	}

	inline static int32_t get_offset_of_keys_1() { return static_cast<int32_t>(offsetof(IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5, ___keys_1)); }
	inline RuntimeObject* get_keys_1() const { return ___keys_1; }
	inline RuntimeObject** get_address_of_keys_1() { return &___keys_1; }
	inline void set_keys_1(RuntimeObject* value)
	{
		___keys_1 = value;
		Il2CppCodeGenWriteBarrier((&___keys_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORESCASEHASHTABLE_TA7603CFAD6585F0F1E01F6B092129BE22AFECDC5_H
#ifndef PKCS12STOREBUILDER_TB72273133B4A84B1833B8A1054743C437541244C_H
#define PKCS12STOREBUILDER_TB72273133B4A84B1833B8A1054743C437541244C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12StoreBuilder
struct  Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12StoreBuilder::keyAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___keyAlgorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12StoreBuilder::certAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___certAlgorithm_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12StoreBuilder::useDerEncoding
	bool ___useDerEncoding_2;

public:
	inline static int32_t get_offset_of_keyAlgorithm_0() { return static_cast<int32_t>(offsetof(Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C, ___keyAlgorithm_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_keyAlgorithm_0() const { return ___keyAlgorithm_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_keyAlgorithm_0() { return &___keyAlgorithm_0; }
	inline void set_keyAlgorithm_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___keyAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___keyAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_certAlgorithm_1() { return static_cast<int32_t>(offsetof(Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C, ___certAlgorithm_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_certAlgorithm_1() const { return ___certAlgorithm_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_certAlgorithm_1() { return &___certAlgorithm_1; }
	inline void set_certAlgorithm_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___certAlgorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___certAlgorithm_1), value);
	}

	inline static int32_t get_offset_of_useDerEncoding_2() { return static_cast<int32_t>(offsetof(Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C, ___useDerEncoding_2)); }
	inline bool get_useDerEncoding_2() const { return ___useDerEncoding_2; }
	inline bool* get_address_of_useDerEncoding_2() { return &___useDerEncoding_2; }
	inline void set_useDerEncoding_2(bool value)
	{
		___useDerEncoding_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12STOREBUILDER_TB72273133B4A84B1833B8A1054743C437541244C_H
#ifndef PKCS12UTILITIES_T8C484813DE95B655A2AF3F63A6E7FF9E76C0694A_H
#define PKCS12UTILITIES_T8C484813DE95B655A2AF3F63A6E7FF9E76C0694A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs12Utilities
struct  Pkcs12Utilities_t8C484813DE95B655A2AF3F63A6E7FF9E76C0694A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12UTILITIES_T8C484813DE95B655A2AF3F63A6E7FF9E76C0694A_H
#ifndef PKCS8ENCRYPTEDPRIVATEKEYINFO_T518C1E625E8D3BD4D114DA676E2C85834F14A837_H
#define PKCS8ENCRYPTEDPRIVATEKEYINFO_T518C1E625E8D3BD4D114DA676E2C85834F14A837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs8EncryptedPrivateKeyInfo
struct  Pkcs8EncryptedPrivateKeyInfo_t518C1E625E8D3BD4D114DA676E2C85834F14A837  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedPrivateKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs8EncryptedPrivateKeyInfo::encryptedPrivateKeyInfo
	EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873 * ___encryptedPrivateKeyInfo_0;

public:
	inline static int32_t get_offset_of_encryptedPrivateKeyInfo_0() { return static_cast<int32_t>(offsetof(Pkcs8EncryptedPrivateKeyInfo_t518C1E625E8D3BD4D114DA676E2C85834F14A837, ___encryptedPrivateKeyInfo_0)); }
	inline EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873 * get_encryptedPrivateKeyInfo_0() const { return ___encryptedPrivateKeyInfo_0; }
	inline EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873 ** get_address_of_encryptedPrivateKeyInfo_0() { return &___encryptedPrivateKeyInfo_0; }
	inline void set_encryptedPrivateKeyInfo_0(EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873 * value)
	{
		___encryptedPrivateKeyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedPrivateKeyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8ENCRYPTEDPRIVATEKEYINFO_T518C1E625E8D3BD4D114DA676E2C85834F14A837_H
#ifndef PKCS8ENCRYPTEDPRIVATEKEYINFOBUILDER_TA1D73D1FFE49F54E047CBB28289D349096BFB745_H
#define PKCS8ENCRYPTEDPRIVATEKEYINFOBUILDER_TA1D73D1FFE49F54E047CBB28289D349096BFB745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs8EncryptedPrivateKeyInfoBuilder
struct  Pkcs8EncryptedPrivateKeyInfoBuilder_tA1D73D1FFE49F54E047CBB28289D349096BFB745  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs8EncryptedPrivateKeyInfoBuilder::privateKeyInfo
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 * ___privateKeyInfo_0;

public:
	inline static int32_t get_offset_of_privateKeyInfo_0() { return static_cast<int32_t>(offsetof(Pkcs8EncryptedPrivateKeyInfoBuilder_tA1D73D1FFE49F54E047CBB28289D349096BFB745, ___privateKeyInfo_0)); }
	inline PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 * get_privateKeyInfo_0() const { return ___privateKeyInfo_0; }
	inline PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 ** get_address_of_privateKeyInfo_0() { return &___privateKeyInfo_0; }
	inline void set_privateKeyInfo_0(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 * value)
	{
		___privateKeyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___privateKeyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8ENCRYPTEDPRIVATEKEYINFOBUILDER_TA1D73D1FFE49F54E047CBB28289D349096BFB745_H
#ifndef PRIVATEKEYINFOFACTORY_T366589F2CC8DDC7F9E99AB513329ABAF977C4288_H
#define PRIVATEKEYINFOFACTORY_T366589F2CC8DDC7F9E99AB513329ABAF977C4288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.PrivateKeyInfoFactory
struct  PrivateKeyInfoFactory_t366589F2CC8DDC7F9E99AB513329ABAF977C4288  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYINFOFACTORY_T366589F2CC8DDC7F9E99AB513329ABAF977C4288_H
#ifndef CERTSTATUS_T4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1_H
#define CERTSTATUS_T4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.CertStatus
struct  CertStatus_t4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.CertStatus::status
	int32_t ___status_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.CertStatus::revocationDate
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___revocationDate_3;

public:
	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(CertStatus_t4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1, ___status_2)); }
	inline int32_t get_status_2() const { return ___status_2; }
	inline int32_t* get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(int32_t value)
	{
		___status_2 = value;
	}

	inline static int32_t get_offset_of_revocationDate_3() { return static_cast<int32_t>(offsetof(CertStatus_t4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1, ___revocationDate_3)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_revocationDate_3() const { return ___revocationDate_3; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_revocationDate_3() { return &___revocationDate_3; }
	inline void set_revocationDate_3(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___revocationDate_3 = value;
		Il2CppCodeGenWriteBarrier((&___revocationDate_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTSTATUS_T4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1_H
#ifndef PKIXATTRCERTCHECKER_T5533D54A8FFFBDC161C6531896BFFA98E8B0DCF3_H
#define PKIXATTRCERTCHECKER_T5533D54A8FFFBDC161C6531896BFFA98E8B0DCF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixAttrCertChecker
struct  PkixAttrCertChecker_t5533D54A8FFFBDC161C6531896BFFA98E8B0DCF3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXATTRCERTCHECKER_T5533D54A8FFFBDC161C6531896BFFA98E8B0DCF3_H
#ifndef PKIXATTRCERTPATHBUILDER_TD438B8690C91A7D45BD35A2C3E203B36714CA6A4_H
#define PKIXATTRCERTPATHBUILDER_TD438B8690C91A7D45BD35A2C3E203B36714CA6A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixAttrCertPathBuilder
struct  PkixAttrCertPathBuilder_tD438B8690C91A7D45BD35A2C3E203B36714CA6A4  : public RuntimeObject
{
public:
	// System.Exception BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixAttrCertPathBuilder::certPathException
	Exception_t * ___certPathException_0;

public:
	inline static int32_t get_offset_of_certPathException_0() { return static_cast<int32_t>(offsetof(PkixAttrCertPathBuilder_tD438B8690C91A7D45BD35A2C3E203B36714CA6A4, ___certPathException_0)); }
	inline Exception_t * get_certPathException_0() const { return ___certPathException_0; }
	inline Exception_t ** get_address_of_certPathException_0() { return &___certPathException_0; }
	inline void set_certPathException_0(Exception_t * value)
	{
		___certPathException_0 = value;
		Il2CppCodeGenWriteBarrier((&___certPathException_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXATTRCERTPATHBUILDER_TD438B8690C91A7D45BD35A2C3E203B36714CA6A4_H
#ifndef PKIXATTRCERTPATHVALIDATOR_T95C2FBE1603D722B81FBCF2253C4805098266EF9_H
#define PKIXATTRCERTPATHVALIDATOR_T95C2FBE1603D722B81FBCF2253C4805098266EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixAttrCertPathValidator
struct  PkixAttrCertPathValidator_t95C2FBE1603D722B81FBCF2253C4805098266EF9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXATTRCERTPATHVALIDATOR_T95C2FBE1603D722B81FBCF2253C4805098266EF9_H
#ifndef PKIXCERTPATH_TD91288CFD58363299DC672B45850DE2C4D40651C_H
#define PKIXCERTPATH_TD91288CFD58363299DC672B45850DE2C4D40651C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPath
struct  PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPath::certificates
	RuntimeObject* ___certificates_1;

public:
	inline static int32_t get_offset_of_certificates_1() { return static_cast<int32_t>(offsetof(PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C, ___certificates_1)); }
	inline RuntimeObject* get_certificates_1() const { return ___certificates_1; }
	inline RuntimeObject** get_address_of_certificates_1() { return &___certificates_1; }
	inline void set_certificates_1(RuntimeObject* value)
	{
		___certificates_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_1), value);
	}
};

struct PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C_StaticFields
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPath::certPathEncodings
	RuntimeObject* ___certPathEncodings_0;

public:
	inline static int32_t get_offset_of_certPathEncodings_0() { return static_cast<int32_t>(offsetof(PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C_StaticFields, ___certPathEncodings_0)); }
	inline RuntimeObject* get_certPathEncodings_0() const { return ___certPathEncodings_0; }
	inline RuntimeObject** get_address_of_certPathEncodings_0() { return &___certPathEncodings_0; }
	inline void set_certPathEncodings_0(RuntimeObject* value)
	{
		___certPathEncodings_0 = value;
		Il2CppCodeGenWriteBarrier((&___certPathEncodings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATH_TD91288CFD58363299DC672B45850DE2C4D40651C_H
#ifndef PKIXCERTPATHBUILDER_T8A953260B0C87E347F09640C76F2288B434F7596_H
#define PKIXCERTPATHBUILDER_T8A953260B0C87E347F09640C76F2288B434F7596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathBuilder
struct  PkixCertPathBuilder_t8A953260B0C87E347F09640C76F2288B434F7596  : public RuntimeObject
{
public:
	// System.Exception BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathBuilder::certPathException
	Exception_t * ___certPathException_0;

public:
	inline static int32_t get_offset_of_certPathException_0() { return static_cast<int32_t>(offsetof(PkixCertPathBuilder_t8A953260B0C87E347F09640C76F2288B434F7596, ___certPathException_0)); }
	inline Exception_t * get_certPathException_0() const { return ___certPathException_0; }
	inline Exception_t ** get_address_of_certPathException_0() { return &___certPathException_0; }
	inline void set_certPathException_0(Exception_t * value)
	{
		___certPathException_0 = value;
		Il2CppCodeGenWriteBarrier((&___certPathException_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHBUILDER_T8A953260B0C87E347F09640C76F2288B434F7596_H
#ifndef PKIXCERTPATHCHECKER_T0574E1D786358E0C81EEAAE3BAD55DF8ED7B8A5E_H
#define PKIXCERTPATHCHECKER_T0574E1D786358E0C81EEAAE3BAD55DF8ED7B8A5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathChecker
struct  PkixCertPathChecker_t0574E1D786358E0C81EEAAE3BAD55DF8ED7B8A5E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHCHECKER_T0574E1D786358E0C81EEAAE3BAD55DF8ED7B8A5E_H
#ifndef PKIXCERTPATHVALIDATOR_TDAB138ACA0D6FB6291E03C57457B7A7391FCC1AA_H
#define PKIXCERTPATHVALIDATOR_TDAB138ACA0D6FB6291E03C57457B7A7391FCC1AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidator
struct  PkixCertPathValidator_tDAB138ACA0D6FB6291E03C57457B7A7391FCC1AA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHVALIDATOR_TDAB138ACA0D6FB6291E03C57457B7A7391FCC1AA_H
#ifndef PKIXCERTPATHVALIDATORRESULT_TFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35_H
#define PKIXCERTPATHVALIDATORRESULT_TFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorResult
struct  PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorResult::trustAnchor
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF * ___trustAnchor_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorResult::policyTree
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 * ___policyTree_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorResult::subjectPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___subjectPublicKey_2;

public:
	inline static int32_t get_offset_of_trustAnchor_0() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35, ___trustAnchor_0)); }
	inline TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF * get_trustAnchor_0() const { return ___trustAnchor_0; }
	inline TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF ** get_address_of_trustAnchor_0() { return &___trustAnchor_0; }
	inline void set_trustAnchor_0(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF * value)
	{
		___trustAnchor_0 = value;
		Il2CppCodeGenWriteBarrier((&___trustAnchor_0), value);
	}

	inline static int32_t get_offset_of_policyTree_1() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35, ___policyTree_1)); }
	inline PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 * get_policyTree_1() const { return ___policyTree_1; }
	inline PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 ** get_address_of_policyTree_1() { return &___policyTree_1; }
	inline void set_policyTree_1(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 * value)
	{
		___policyTree_1 = value;
		Il2CppCodeGenWriteBarrier((&___policyTree_1), value);
	}

	inline static int32_t get_offset_of_subjectPublicKey_2() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35, ___subjectPublicKey_2)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_subjectPublicKey_2() const { return ___subjectPublicKey_2; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_subjectPublicKey_2() { return &___subjectPublicKey_2; }
	inline void set_subjectPublicKey_2(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___subjectPublicKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPublicKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHVALIDATORRESULT_TFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35_H
#ifndef PKIXCERTPATHVALIDATORUTILITIES_TE32E916DD8F0BF9A6286A84335027E56716717A9_H
#define PKIXCERTPATHVALIDATORUTILITIES_TE32E916DD8F0BF9A6286A84335027E56716717A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities
struct  PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9  : public RuntimeObject
{
public:

public:
};

struct PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCrlUtilities BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities::CrlUtilities
	PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE * ___CrlUtilities_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities::ANY_POLICY
	String_t* ___ANY_POLICY_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities::CRL_NUMBER
	String_t* ___CRL_NUMBER_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities::KEY_CERT_SIGN
	int32_t ___KEY_CERT_SIGN_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities::CRL_SIGN
	int32_t ___CRL_SIGN_4;
	// System.String[] BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorUtilities::crlReasons
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___crlReasons_5;

public:
	inline static int32_t get_offset_of_CrlUtilities_0() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields, ___CrlUtilities_0)); }
	inline PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE * get_CrlUtilities_0() const { return ___CrlUtilities_0; }
	inline PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE ** get_address_of_CrlUtilities_0() { return &___CrlUtilities_0; }
	inline void set_CrlUtilities_0(PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE * value)
	{
		___CrlUtilities_0 = value;
		Il2CppCodeGenWriteBarrier((&___CrlUtilities_0), value);
	}

	inline static int32_t get_offset_of_ANY_POLICY_1() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields, ___ANY_POLICY_1)); }
	inline String_t* get_ANY_POLICY_1() const { return ___ANY_POLICY_1; }
	inline String_t** get_address_of_ANY_POLICY_1() { return &___ANY_POLICY_1; }
	inline void set_ANY_POLICY_1(String_t* value)
	{
		___ANY_POLICY_1 = value;
		Il2CppCodeGenWriteBarrier((&___ANY_POLICY_1), value);
	}

	inline static int32_t get_offset_of_CRL_NUMBER_2() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields, ___CRL_NUMBER_2)); }
	inline String_t* get_CRL_NUMBER_2() const { return ___CRL_NUMBER_2; }
	inline String_t** get_address_of_CRL_NUMBER_2() { return &___CRL_NUMBER_2; }
	inline void set_CRL_NUMBER_2(String_t* value)
	{
		___CRL_NUMBER_2 = value;
		Il2CppCodeGenWriteBarrier((&___CRL_NUMBER_2), value);
	}

	inline static int32_t get_offset_of_KEY_CERT_SIGN_3() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields, ___KEY_CERT_SIGN_3)); }
	inline int32_t get_KEY_CERT_SIGN_3() const { return ___KEY_CERT_SIGN_3; }
	inline int32_t* get_address_of_KEY_CERT_SIGN_3() { return &___KEY_CERT_SIGN_3; }
	inline void set_KEY_CERT_SIGN_3(int32_t value)
	{
		___KEY_CERT_SIGN_3 = value;
	}

	inline static int32_t get_offset_of_CRL_SIGN_4() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields, ___CRL_SIGN_4)); }
	inline int32_t get_CRL_SIGN_4() const { return ___CRL_SIGN_4; }
	inline int32_t* get_address_of_CRL_SIGN_4() { return &___CRL_SIGN_4; }
	inline void set_CRL_SIGN_4(int32_t value)
	{
		___CRL_SIGN_4 = value;
	}

	inline static int32_t get_offset_of_crlReasons_5() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields, ___crlReasons_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_crlReasons_5() const { return ___crlReasons_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_crlReasons_5() { return &___crlReasons_5; }
	inline void set_crlReasons_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___crlReasons_5 = value;
		Il2CppCodeGenWriteBarrier((&___crlReasons_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHVALIDATORUTILITIES_TE32E916DD8F0BF9A6286A84335027E56716717A9_H
#ifndef PKIXCRLUTILITIES_T26FDDBA4997F123F575D96DC2600A3A3904817EE_H
#define PKIXCRLUTILITIES_T26FDDBA4997F123F575D96DC2600A3A3904817EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCrlUtilities
struct  PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCRLUTILITIES_T26FDDBA4997F123F575D96DC2600A3A3904817EE_H
#ifndef PKIXNAMECONSTRAINTVALIDATOR_T1E574AE9747DF0C3949CFA010D2361C544C3CE05_H
#define PKIXNAMECONSTRAINTVALIDATOR_T1E574AE9747DF0C3949CFA010D2361C544C3CE05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator
struct  PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::excludedSubtreesDN
	RuntimeObject* ___excludedSubtreesDN_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::excludedSubtreesDNS
	RuntimeObject* ___excludedSubtreesDNS_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::excludedSubtreesEmail
	RuntimeObject* ___excludedSubtreesEmail_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::excludedSubtreesURI
	RuntimeObject* ___excludedSubtreesURI_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::excludedSubtreesIP
	RuntimeObject* ___excludedSubtreesIP_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::permittedSubtreesDN
	RuntimeObject* ___permittedSubtreesDN_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::permittedSubtreesDNS
	RuntimeObject* ___permittedSubtreesDNS_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::permittedSubtreesEmail
	RuntimeObject* ___permittedSubtreesEmail_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::permittedSubtreesURI
	RuntimeObject* ___permittedSubtreesURI_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::permittedSubtreesIP
	RuntimeObject* ___permittedSubtreesIP_10;

public:
	inline static int32_t get_offset_of_excludedSubtreesDN_1() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___excludedSubtreesDN_1)); }
	inline RuntimeObject* get_excludedSubtreesDN_1() const { return ___excludedSubtreesDN_1; }
	inline RuntimeObject** get_address_of_excludedSubtreesDN_1() { return &___excludedSubtreesDN_1; }
	inline void set_excludedSubtreesDN_1(RuntimeObject* value)
	{
		___excludedSubtreesDN_1 = value;
		Il2CppCodeGenWriteBarrier((&___excludedSubtreesDN_1), value);
	}

	inline static int32_t get_offset_of_excludedSubtreesDNS_2() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___excludedSubtreesDNS_2)); }
	inline RuntimeObject* get_excludedSubtreesDNS_2() const { return ___excludedSubtreesDNS_2; }
	inline RuntimeObject** get_address_of_excludedSubtreesDNS_2() { return &___excludedSubtreesDNS_2; }
	inline void set_excludedSubtreesDNS_2(RuntimeObject* value)
	{
		___excludedSubtreesDNS_2 = value;
		Il2CppCodeGenWriteBarrier((&___excludedSubtreesDNS_2), value);
	}

	inline static int32_t get_offset_of_excludedSubtreesEmail_3() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___excludedSubtreesEmail_3)); }
	inline RuntimeObject* get_excludedSubtreesEmail_3() const { return ___excludedSubtreesEmail_3; }
	inline RuntimeObject** get_address_of_excludedSubtreesEmail_3() { return &___excludedSubtreesEmail_3; }
	inline void set_excludedSubtreesEmail_3(RuntimeObject* value)
	{
		___excludedSubtreesEmail_3 = value;
		Il2CppCodeGenWriteBarrier((&___excludedSubtreesEmail_3), value);
	}

	inline static int32_t get_offset_of_excludedSubtreesURI_4() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___excludedSubtreesURI_4)); }
	inline RuntimeObject* get_excludedSubtreesURI_4() const { return ___excludedSubtreesURI_4; }
	inline RuntimeObject** get_address_of_excludedSubtreesURI_4() { return &___excludedSubtreesURI_4; }
	inline void set_excludedSubtreesURI_4(RuntimeObject* value)
	{
		___excludedSubtreesURI_4 = value;
		Il2CppCodeGenWriteBarrier((&___excludedSubtreesURI_4), value);
	}

	inline static int32_t get_offset_of_excludedSubtreesIP_5() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___excludedSubtreesIP_5)); }
	inline RuntimeObject* get_excludedSubtreesIP_5() const { return ___excludedSubtreesIP_5; }
	inline RuntimeObject** get_address_of_excludedSubtreesIP_5() { return &___excludedSubtreesIP_5; }
	inline void set_excludedSubtreesIP_5(RuntimeObject* value)
	{
		___excludedSubtreesIP_5 = value;
		Il2CppCodeGenWriteBarrier((&___excludedSubtreesIP_5), value);
	}

	inline static int32_t get_offset_of_permittedSubtreesDN_6() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___permittedSubtreesDN_6)); }
	inline RuntimeObject* get_permittedSubtreesDN_6() const { return ___permittedSubtreesDN_6; }
	inline RuntimeObject** get_address_of_permittedSubtreesDN_6() { return &___permittedSubtreesDN_6; }
	inline void set_permittedSubtreesDN_6(RuntimeObject* value)
	{
		___permittedSubtreesDN_6 = value;
		Il2CppCodeGenWriteBarrier((&___permittedSubtreesDN_6), value);
	}

	inline static int32_t get_offset_of_permittedSubtreesDNS_7() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___permittedSubtreesDNS_7)); }
	inline RuntimeObject* get_permittedSubtreesDNS_7() const { return ___permittedSubtreesDNS_7; }
	inline RuntimeObject** get_address_of_permittedSubtreesDNS_7() { return &___permittedSubtreesDNS_7; }
	inline void set_permittedSubtreesDNS_7(RuntimeObject* value)
	{
		___permittedSubtreesDNS_7 = value;
		Il2CppCodeGenWriteBarrier((&___permittedSubtreesDNS_7), value);
	}

	inline static int32_t get_offset_of_permittedSubtreesEmail_8() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___permittedSubtreesEmail_8)); }
	inline RuntimeObject* get_permittedSubtreesEmail_8() const { return ___permittedSubtreesEmail_8; }
	inline RuntimeObject** get_address_of_permittedSubtreesEmail_8() { return &___permittedSubtreesEmail_8; }
	inline void set_permittedSubtreesEmail_8(RuntimeObject* value)
	{
		___permittedSubtreesEmail_8 = value;
		Il2CppCodeGenWriteBarrier((&___permittedSubtreesEmail_8), value);
	}

	inline static int32_t get_offset_of_permittedSubtreesURI_9() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___permittedSubtreesURI_9)); }
	inline RuntimeObject* get_permittedSubtreesURI_9() const { return ___permittedSubtreesURI_9; }
	inline RuntimeObject** get_address_of_permittedSubtreesURI_9() { return &___permittedSubtreesURI_9; }
	inline void set_permittedSubtreesURI_9(RuntimeObject* value)
	{
		___permittedSubtreesURI_9 = value;
		Il2CppCodeGenWriteBarrier((&___permittedSubtreesURI_9), value);
	}

	inline static int32_t get_offset_of_permittedSubtreesIP_10() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05, ___permittedSubtreesIP_10)); }
	inline RuntimeObject* get_permittedSubtreesIP_10() const { return ___permittedSubtreesIP_10; }
	inline RuntimeObject** get_address_of_permittedSubtreesIP_10() { return &___permittedSubtreesIP_10; }
	inline void set_permittedSubtreesIP_10(RuntimeObject* value)
	{
		___permittedSubtreesIP_10 = value;
		Il2CppCodeGenWriteBarrier((&___permittedSubtreesIP_10), value);
	}
};

struct PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidator::SerialNumberOid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SerialNumberOid_0;

public:
	inline static int32_t get_offset_of_SerialNumberOid_0() { return static_cast<int32_t>(offsetof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05_StaticFields, ___SerialNumberOid_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SerialNumberOid_0() const { return ___SerialNumberOid_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SerialNumberOid_0() { return &___SerialNumberOid_0; }
	inline void set_SerialNumberOid_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SerialNumberOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___SerialNumberOid_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXNAMECONSTRAINTVALIDATOR_T1E574AE9747DF0C3949CFA010D2361C544C3CE05_H
#ifndef PKIXPARAMETERS_TFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3_H
#define PKIXPARAMETERS_TFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters
struct  PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::trustAnchors
	RuntimeObject* ___trustAnchors_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::date
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * ___date_3;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::certPathCheckers
	RuntimeObject* ___certPathCheckers_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::revocationEnabled
	bool ___revocationEnabled_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::initialPolicies
	RuntimeObject* ___initialPolicies_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::explicitPolicyRequired
	bool ___explicitPolicyRequired_7;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::anyPolicyInhibited
	bool ___anyPolicyInhibited_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::policyMappingInhibited
	bool ___policyMappingInhibited_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::policyQualifiersRejected
	bool ___policyQualifiersRejected_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Selector BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::certSelector
	RuntimeObject* ___certSelector_11;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::stores
	RuntimeObject* ___stores_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Selector BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::selector
	RuntimeObject* ___selector_13;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::additionalLocationsEnabled
	bool ___additionalLocationsEnabled_14;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::additionalStores
	RuntimeObject* ___additionalStores_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::trustedACIssuers
	RuntimeObject* ___trustedACIssuers_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::necessaryACAttributes
	RuntimeObject* ___necessaryACAttributes_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::prohibitedACAttributes
	RuntimeObject* ___prohibitedACAttributes_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::attrCertCheckers
	RuntimeObject* ___attrCertCheckers_19;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::validityModel
	int32_t ___validityModel_20;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixParameters::useDeltas
	bool ___useDeltas_21;

public:
	inline static int32_t get_offset_of_trustAnchors_2() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___trustAnchors_2)); }
	inline RuntimeObject* get_trustAnchors_2() const { return ___trustAnchors_2; }
	inline RuntimeObject** get_address_of_trustAnchors_2() { return &___trustAnchors_2; }
	inline void set_trustAnchors_2(RuntimeObject* value)
	{
		___trustAnchors_2 = value;
		Il2CppCodeGenWriteBarrier((&___trustAnchors_2), value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___date_3)); }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * get_date_3() const { return ___date_3; }
	inline DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F ** get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F * value)
	{
		___date_3 = value;
		Il2CppCodeGenWriteBarrier((&___date_3), value);
	}

	inline static int32_t get_offset_of_certPathCheckers_4() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___certPathCheckers_4)); }
	inline RuntimeObject* get_certPathCheckers_4() const { return ___certPathCheckers_4; }
	inline RuntimeObject** get_address_of_certPathCheckers_4() { return &___certPathCheckers_4; }
	inline void set_certPathCheckers_4(RuntimeObject* value)
	{
		___certPathCheckers_4 = value;
		Il2CppCodeGenWriteBarrier((&___certPathCheckers_4), value);
	}

	inline static int32_t get_offset_of_revocationEnabled_5() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___revocationEnabled_5)); }
	inline bool get_revocationEnabled_5() const { return ___revocationEnabled_5; }
	inline bool* get_address_of_revocationEnabled_5() { return &___revocationEnabled_5; }
	inline void set_revocationEnabled_5(bool value)
	{
		___revocationEnabled_5 = value;
	}

	inline static int32_t get_offset_of_initialPolicies_6() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___initialPolicies_6)); }
	inline RuntimeObject* get_initialPolicies_6() const { return ___initialPolicies_6; }
	inline RuntimeObject** get_address_of_initialPolicies_6() { return &___initialPolicies_6; }
	inline void set_initialPolicies_6(RuntimeObject* value)
	{
		___initialPolicies_6 = value;
		Il2CppCodeGenWriteBarrier((&___initialPolicies_6), value);
	}

	inline static int32_t get_offset_of_explicitPolicyRequired_7() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___explicitPolicyRequired_7)); }
	inline bool get_explicitPolicyRequired_7() const { return ___explicitPolicyRequired_7; }
	inline bool* get_address_of_explicitPolicyRequired_7() { return &___explicitPolicyRequired_7; }
	inline void set_explicitPolicyRequired_7(bool value)
	{
		___explicitPolicyRequired_7 = value;
	}

	inline static int32_t get_offset_of_anyPolicyInhibited_8() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___anyPolicyInhibited_8)); }
	inline bool get_anyPolicyInhibited_8() const { return ___anyPolicyInhibited_8; }
	inline bool* get_address_of_anyPolicyInhibited_8() { return &___anyPolicyInhibited_8; }
	inline void set_anyPolicyInhibited_8(bool value)
	{
		___anyPolicyInhibited_8 = value;
	}

	inline static int32_t get_offset_of_policyMappingInhibited_9() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___policyMappingInhibited_9)); }
	inline bool get_policyMappingInhibited_9() const { return ___policyMappingInhibited_9; }
	inline bool* get_address_of_policyMappingInhibited_9() { return &___policyMappingInhibited_9; }
	inline void set_policyMappingInhibited_9(bool value)
	{
		___policyMappingInhibited_9 = value;
	}

	inline static int32_t get_offset_of_policyQualifiersRejected_10() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___policyQualifiersRejected_10)); }
	inline bool get_policyQualifiersRejected_10() const { return ___policyQualifiersRejected_10; }
	inline bool* get_address_of_policyQualifiersRejected_10() { return &___policyQualifiersRejected_10; }
	inline void set_policyQualifiersRejected_10(bool value)
	{
		___policyQualifiersRejected_10 = value;
	}

	inline static int32_t get_offset_of_certSelector_11() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___certSelector_11)); }
	inline RuntimeObject* get_certSelector_11() const { return ___certSelector_11; }
	inline RuntimeObject** get_address_of_certSelector_11() { return &___certSelector_11; }
	inline void set_certSelector_11(RuntimeObject* value)
	{
		___certSelector_11 = value;
		Il2CppCodeGenWriteBarrier((&___certSelector_11), value);
	}

	inline static int32_t get_offset_of_stores_12() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___stores_12)); }
	inline RuntimeObject* get_stores_12() const { return ___stores_12; }
	inline RuntimeObject** get_address_of_stores_12() { return &___stores_12; }
	inline void set_stores_12(RuntimeObject* value)
	{
		___stores_12 = value;
		Il2CppCodeGenWriteBarrier((&___stores_12), value);
	}

	inline static int32_t get_offset_of_selector_13() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___selector_13)); }
	inline RuntimeObject* get_selector_13() const { return ___selector_13; }
	inline RuntimeObject** get_address_of_selector_13() { return &___selector_13; }
	inline void set_selector_13(RuntimeObject* value)
	{
		___selector_13 = value;
		Il2CppCodeGenWriteBarrier((&___selector_13), value);
	}

	inline static int32_t get_offset_of_additionalLocationsEnabled_14() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___additionalLocationsEnabled_14)); }
	inline bool get_additionalLocationsEnabled_14() const { return ___additionalLocationsEnabled_14; }
	inline bool* get_address_of_additionalLocationsEnabled_14() { return &___additionalLocationsEnabled_14; }
	inline void set_additionalLocationsEnabled_14(bool value)
	{
		___additionalLocationsEnabled_14 = value;
	}

	inline static int32_t get_offset_of_additionalStores_15() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___additionalStores_15)); }
	inline RuntimeObject* get_additionalStores_15() const { return ___additionalStores_15; }
	inline RuntimeObject** get_address_of_additionalStores_15() { return &___additionalStores_15; }
	inline void set_additionalStores_15(RuntimeObject* value)
	{
		___additionalStores_15 = value;
		Il2CppCodeGenWriteBarrier((&___additionalStores_15), value);
	}

	inline static int32_t get_offset_of_trustedACIssuers_16() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___trustedACIssuers_16)); }
	inline RuntimeObject* get_trustedACIssuers_16() const { return ___trustedACIssuers_16; }
	inline RuntimeObject** get_address_of_trustedACIssuers_16() { return &___trustedACIssuers_16; }
	inline void set_trustedACIssuers_16(RuntimeObject* value)
	{
		___trustedACIssuers_16 = value;
		Il2CppCodeGenWriteBarrier((&___trustedACIssuers_16), value);
	}

	inline static int32_t get_offset_of_necessaryACAttributes_17() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___necessaryACAttributes_17)); }
	inline RuntimeObject* get_necessaryACAttributes_17() const { return ___necessaryACAttributes_17; }
	inline RuntimeObject** get_address_of_necessaryACAttributes_17() { return &___necessaryACAttributes_17; }
	inline void set_necessaryACAttributes_17(RuntimeObject* value)
	{
		___necessaryACAttributes_17 = value;
		Il2CppCodeGenWriteBarrier((&___necessaryACAttributes_17), value);
	}

	inline static int32_t get_offset_of_prohibitedACAttributes_18() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___prohibitedACAttributes_18)); }
	inline RuntimeObject* get_prohibitedACAttributes_18() const { return ___prohibitedACAttributes_18; }
	inline RuntimeObject** get_address_of_prohibitedACAttributes_18() { return &___prohibitedACAttributes_18; }
	inline void set_prohibitedACAttributes_18(RuntimeObject* value)
	{
		___prohibitedACAttributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___prohibitedACAttributes_18), value);
	}

	inline static int32_t get_offset_of_attrCertCheckers_19() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___attrCertCheckers_19)); }
	inline RuntimeObject* get_attrCertCheckers_19() const { return ___attrCertCheckers_19; }
	inline RuntimeObject** get_address_of_attrCertCheckers_19() { return &___attrCertCheckers_19; }
	inline void set_attrCertCheckers_19(RuntimeObject* value)
	{
		___attrCertCheckers_19 = value;
		Il2CppCodeGenWriteBarrier((&___attrCertCheckers_19), value);
	}

	inline static int32_t get_offset_of_validityModel_20() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___validityModel_20)); }
	inline int32_t get_validityModel_20() const { return ___validityModel_20; }
	inline int32_t* get_address_of_validityModel_20() { return &___validityModel_20; }
	inline void set_validityModel_20(int32_t value)
	{
		___validityModel_20 = value;
	}

	inline static int32_t get_offset_of_useDeltas_21() { return static_cast<int32_t>(offsetof(PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3, ___useDeltas_21)); }
	inline bool get_useDeltas_21() const { return ___useDeltas_21; }
	inline bool* get_address_of_useDeltas_21() { return &___useDeltas_21; }
	inline void set_useDeltas_21(bool value)
	{
		___useDeltas_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXPARAMETERS_TFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3_H
#ifndef PKIXPOLICYNODE_T62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8_H
#define PKIXPOLICYNODE_T62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode
struct  PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8  : public RuntimeObject
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mChildren
	RuntimeObject* ___mChildren_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mDepth
	int32_t ___mDepth_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mExpectedPolicies
	RuntimeObject* ___mExpectedPolicies_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mParent
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 * ___mParent_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mPolicyQualifiers
	RuntimeObject* ___mPolicyQualifiers_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mValidPolicy
	String_t* ___mValidPolicy_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixPolicyNode::mCritical
	bool ___mCritical_6;

public:
	inline static int32_t get_offset_of_mChildren_0() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mChildren_0)); }
	inline RuntimeObject* get_mChildren_0() const { return ___mChildren_0; }
	inline RuntimeObject** get_address_of_mChildren_0() { return &___mChildren_0; }
	inline void set_mChildren_0(RuntimeObject* value)
	{
		___mChildren_0 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_0), value);
	}

	inline static int32_t get_offset_of_mDepth_1() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mDepth_1)); }
	inline int32_t get_mDepth_1() const { return ___mDepth_1; }
	inline int32_t* get_address_of_mDepth_1() { return &___mDepth_1; }
	inline void set_mDepth_1(int32_t value)
	{
		___mDepth_1 = value;
	}

	inline static int32_t get_offset_of_mExpectedPolicies_2() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mExpectedPolicies_2)); }
	inline RuntimeObject* get_mExpectedPolicies_2() const { return ___mExpectedPolicies_2; }
	inline RuntimeObject** get_address_of_mExpectedPolicies_2() { return &___mExpectedPolicies_2; }
	inline void set_mExpectedPolicies_2(RuntimeObject* value)
	{
		___mExpectedPolicies_2 = value;
		Il2CppCodeGenWriteBarrier((&___mExpectedPolicies_2), value);
	}

	inline static int32_t get_offset_of_mParent_3() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mParent_3)); }
	inline PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 * get_mParent_3() const { return ___mParent_3; }
	inline PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 ** get_address_of_mParent_3() { return &___mParent_3; }
	inline void set_mParent_3(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8 * value)
	{
		___mParent_3 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_3), value);
	}

	inline static int32_t get_offset_of_mPolicyQualifiers_4() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mPolicyQualifiers_4)); }
	inline RuntimeObject* get_mPolicyQualifiers_4() const { return ___mPolicyQualifiers_4; }
	inline RuntimeObject** get_address_of_mPolicyQualifiers_4() { return &___mPolicyQualifiers_4; }
	inline void set_mPolicyQualifiers_4(RuntimeObject* value)
	{
		___mPolicyQualifiers_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPolicyQualifiers_4), value);
	}

	inline static int32_t get_offset_of_mValidPolicy_5() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mValidPolicy_5)); }
	inline String_t* get_mValidPolicy_5() const { return ___mValidPolicy_5; }
	inline String_t** get_address_of_mValidPolicy_5() { return &___mValidPolicy_5; }
	inline void set_mValidPolicy_5(String_t* value)
	{
		___mValidPolicy_5 = value;
		Il2CppCodeGenWriteBarrier((&___mValidPolicy_5), value);
	}

	inline static int32_t get_offset_of_mCritical_6() { return static_cast<int32_t>(offsetof(PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8, ___mCritical_6)); }
	inline bool get_mCritical_6() const { return ___mCritical_6; }
	inline bool* get_address_of_mCritical_6() { return &___mCritical_6; }
	inline void set_mCritical_6(bool value)
	{
		___mCritical_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXPOLICYNODE_T62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8_H
#ifndef REASONSMASK_T39D10F15C9FAE12EDBCEF89E38D42326C101B79D_H
#define REASONSMASK_T39D10F15C9FAE12EDBCEF89E38D42326C101B79D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.ReasonsMask
struct  ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.ReasonsMask::_reasons
	int32_t ____reasons_0;

public:
	inline static int32_t get_offset_of__reasons_0() { return static_cast<int32_t>(offsetof(ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D, ____reasons_0)); }
	inline int32_t get__reasons_0() const { return ____reasons_0; }
	inline int32_t* get_address_of__reasons_0() { return &____reasons_0; }
	inline void set__reasons_0(int32_t value)
	{
		____reasons_0 = value;
	}
};

struct ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.ReasonsMask BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.ReasonsMask::AllReasons
	ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D * ___AllReasons_1;

public:
	inline static int32_t get_offset_of_AllReasons_1() { return static_cast<int32_t>(offsetof(ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D_StaticFields, ___AllReasons_1)); }
	inline ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D * get_AllReasons_1() const { return ___AllReasons_1; }
	inline ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D ** get_address_of_AllReasons_1() { return &___AllReasons_1; }
	inline void set_AllReasons_1(ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D * value)
	{
		___AllReasons_1 = value;
		Il2CppCodeGenWriteBarrier((&___AllReasons_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REASONSMASK_T39D10F15C9FAE12EDBCEF89E38D42326C101B79D_H
#ifndef RFC3280CERTPATHUTILITIES_T414A5F12EE63016095D202FB8A5BE4194A9CB571_H
#define RFC3280CERTPATHUTILITIES_T414A5F12EE63016095D202FB8A5BE4194A9CB571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3280CertPathUtilities
struct  Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571  : public RuntimeObject
{
public:

public:
};

struct Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCrlUtilities BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3280CertPathUtilities::CrlUtilities
	PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE * ___CrlUtilities_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3280CertPathUtilities::ANY_POLICY
	String_t* ___ANY_POLICY_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3280CertPathUtilities::KEY_CERT_SIGN
	int32_t ___KEY_CERT_SIGN_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3280CertPathUtilities::CRL_SIGN
	int32_t ___CRL_SIGN_3;
	// System.String[] BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3280CertPathUtilities::CrlReasons
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___CrlReasons_4;

public:
	inline static int32_t get_offset_of_CrlUtilities_0() { return static_cast<int32_t>(offsetof(Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields, ___CrlUtilities_0)); }
	inline PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE * get_CrlUtilities_0() const { return ___CrlUtilities_0; }
	inline PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE ** get_address_of_CrlUtilities_0() { return &___CrlUtilities_0; }
	inline void set_CrlUtilities_0(PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE * value)
	{
		___CrlUtilities_0 = value;
		Il2CppCodeGenWriteBarrier((&___CrlUtilities_0), value);
	}

	inline static int32_t get_offset_of_ANY_POLICY_1() { return static_cast<int32_t>(offsetof(Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields, ___ANY_POLICY_1)); }
	inline String_t* get_ANY_POLICY_1() const { return ___ANY_POLICY_1; }
	inline String_t** get_address_of_ANY_POLICY_1() { return &___ANY_POLICY_1; }
	inline void set_ANY_POLICY_1(String_t* value)
	{
		___ANY_POLICY_1 = value;
		Il2CppCodeGenWriteBarrier((&___ANY_POLICY_1), value);
	}

	inline static int32_t get_offset_of_KEY_CERT_SIGN_2() { return static_cast<int32_t>(offsetof(Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields, ___KEY_CERT_SIGN_2)); }
	inline int32_t get_KEY_CERT_SIGN_2() const { return ___KEY_CERT_SIGN_2; }
	inline int32_t* get_address_of_KEY_CERT_SIGN_2() { return &___KEY_CERT_SIGN_2; }
	inline void set_KEY_CERT_SIGN_2(int32_t value)
	{
		___KEY_CERT_SIGN_2 = value;
	}

	inline static int32_t get_offset_of_CRL_SIGN_3() { return static_cast<int32_t>(offsetof(Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields, ___CRL_SIGN_3)); }
	inline int32_t get_CRL_SIGN_3() const { return ___CRL_SIGN_3; }
	inline int32_t* get_address_of_CRL_SIGN_3() { return &___CRL_SIGN_3; }
	inline void set_CRL_SIGN_3(int32_t value)
	{
		___CRL_SIGN_3 = value;
	}

	inline static int32_t get_offset_of_CrlReasons_4() { return static_cast<int32_t>(offsetof(Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields, ___CrlReasons_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_CrlReasons_4() const { return ___CrlReasons_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_CrlReasons_4() { return &___CrlReasons_4; }
	inline void set_CrlReasons_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___CrlReasons_4 = value;
		Il2CppCodeGenWriteBarrier((&___CrlReasons_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC3280CERTPATHUTILITIES_T414A5F12EE63016095D202FB8A5BE4194A9CB571_H
#ifndef RFC3281CERTPATHUTILITIES_T11CD5A3750AB619E5ADA3FAFD212756C4EF7B365_H
#define RFC3281CERTPATHUTILITIES_T11CD5A3750AB619E5ADA3FAFD212756C4EF7B365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.Rfc3281CertPathUtilities
struct  Rfc3281CertPathUtilities_t11CD5A3750AB619E5ADA3FAFD212756C4EF7B365  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC3281CERTPATHUTILITIES_T11CD5A3750AB619E5ADA3FAFD212756C4EF7B365_H
#ifndef TRUSTANCHOR_T67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF_H
#define TRUSTANCHOR_T67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor
struct  TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor::pubKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___pubKey_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor::caName
	String_t* ___caName_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor::caPrincipal
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___caPrincipal_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor::trustedCert
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___trustedCert_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor::ncBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ncBytes_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.NameConstraints BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.TrustAnchor::nc
	NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C * ___nc_5;

public:
	inline static int32_t get_offset_of_pubKey_0() { return static_cast<int32_t>(offsetof(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF, ___pubKey_0)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_pubKey_0() const { return ___pubKey_0; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_pubKey_0() { return &___pubKey_0; }
	inline void set_pubKey_0(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___pubKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___pubKey_0), value);
	}

	inline static int32_t get_offset_of_caName_1() { return static_cast<int32_t>(offsetof(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF, ___caName_1)); }
	inline String_t* get_caName_1() const { return ___caName_1; }
	inline String_t** get_address_of_caName_1() { return &___caName_1; }
	inline void set_caName_1(String_t* value)
	{
		___caName_1 = value;
		Il2CppCodeGenWriteBarrier((&___caName_1), value);
	}

	inline static int32_t get_offset_of_caPrincipal_2() { return static_cast<int32_t>(offsetof(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF, ___caPrincipal_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_caPrincipal_2() const { return ___caPrincipal_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_caPrincipal_2() { return &___caPrincipal_2; }
	inline void set_caPrincipal_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___caPrincipal_2 = value;
		Il2CppCodeGenWriteBarrier((&___caPrincipal_2), value);
	}

	inline static int32_t get_offset_of_trustedCert_3() { return static_cast<int32_t>(offsetof(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF, ___trustedCert_3)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_trustedCert_3() const { return ___trustedCert_3; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_trustedCert_3() { return &___trustedCert_3; }
	inline void set_trustedCert_3(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___trustedCert_3 = value;
		Il2CppCodeGenWriteBarrier((&___trustedCert_3), value);
	}

	inline static int32_t get_offset_of_ncBytes_4() { return static_cast<int32_t>(offsetof(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF, ___ncBytes_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ncBytes_4() const { return ___ncBytes_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ncBytes_4() { return &___ncBytes_4; }
	inline void set_ncBytes_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ncBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ncBytes_4), value);
	}

	inline static int32_t get_offset_of_nc_5() { return static_cast<int32_t>(offsetof(TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF, ___nc_5)); }
	inline NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C * get_nc_5() const { return ___nc_5; }
	inline NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C ** get_address_of_nc_5() { return &___nc_5; }
	inline void set_nc_5(NameConstraints_t32DADF4E7B769CB9582A14FA4A4FD9672A635A6C * value)
	{
		___nc_5 = value;
		Il2CppCodeGenWriteBarrier((&___nc_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRUSTANCHOR_T67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF_H
#ifndef AGREEMENTUTILITIES_TCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_H
#define AGREEMENTUTILITIES_TCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.AgreementUtilities
struct  AgreementUtilities_tCD01694EFC38C394EC411E5DC5A4CA40DCE7A163  : public RuntimeObject
{
public:

public:
};

struct AgreementUtilities_tCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.AgreementUtilities::algorithms
	RuntimeObject* ___algorithms_0;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(AgreementUtilities_tCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGREEMENTUTILITIES_TCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_H
#ifndef CIPHERUTILITIES_T10FDD7485848434F5D5688ED087206F6DED48D05_H
#define CIPHERUTILITIES_T10FDD7485848434F5D5688ED087206F6DED48D05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities
struct  CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05  : public RuntimeObject
{
public:

public:
};

struct CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Security.CipherUtilities::oids
	RuntimeObject* ___oids_1;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_oids_1() { return static_cast<int32_t>(offsetof(CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05_StaticFields, ___oids_1)); }
	inline RuntimeObject* get_oids_1() const { return ___oids_1; }
	inline RuntimeObject** get_address_of_oids_1() { return &___oids_1; }
	inline void set_oids_1(RuntimeObject* value)
	{
		___oids_1 = value;
		Il2CppCodeGenWriteBarrier((&___oids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERUTILITIES_T10FDD7485848434F5D5688ED087206F6DED48D05_H
#ifndef GENTIMEACCURACY_T4814A445A04EB2C38DB89DE0107EDB0B1722C36A_H
#define GENTIMEACCURACY_T4814A445A04EB2C38DB89DE0107EDB0B1722C36A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.GenTimeAccuracy
struct  GenTimeAccuracy_t4814A445A04EB2C38DB89DE0107EDB0B1722C36A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.Accuracy BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.GenTimeAccuracy::accuracy
	Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 * ___accuracy_0;

public:
	inline static int32_t get_offset_of_accuracy_0() { return static_cast<int32_t>(offsetof(GenTimeAccuracy_t4814A445A04EB2C38DB89DE0107EDB0B1722C36A, ___accuracy_0)); }
	inline Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 * get_accuracy_0() const { return ___accuracy_0; }
	inline Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 ** get_address_of_accuracy_0() { return &___accuracy_0; }
	inline void set_accuracy_0(Accuracy_t0F0184312CD5A87E1D7E8274229F3C694D758835 * value)
	{
		___accuracy_0 = value;
		Il2CppCodeGenWriteBarrier((&___accuracy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENTIMEACCURACY_T4814A445A04EB2C38DB89DE0107EDB0B1722C36A_H
#ifndef TIMESTAMPREQUESTGENERATOR_T736A0CEB3F1F370B5C5E68185E3D4799B2089490_H
#define TIMESTAMPREQUESTGENERATOR_T736A0CEB3F1F370B5C5E68185E3D4799B2089490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequestGenerator
struct  TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequestGenerator::reqPolicy
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___reqPolicy_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequestGenerator::certReq
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___certReq_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequestGenerator::extensions
	RuntimeObject* ___extensions_2;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequestGenerator::extOrdering
	RuntimeObject* ___extOrdering_3;

public:
	inline static int32_t get_offset_of_reqPolicy_0() { return static_cast<int32_t>(offsetof(TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490, ___reqPolicy_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_reqPolicy_0() const { return ___reqPolicy_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_reqPolicy_0() { return &___reqPolicy_0; }
	inline void set_reqPolicy_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___reqPolicy_0 = value;
		Il2CppCodeGenWriteBarrier((&___reqPolicy_0), value);
	}

	inline static int32_t get_offset_of_certReq_1() { return static_cast<int32_t>(offsetof(TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490, ___certReq_1)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_certReq_1() const { return ___certReq_1; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_certReq_1() { return &___certReq_1; }
	inline void set_certReq_1(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___certReq_1 = value;
		Il2CppCodeGenWriteBarrier((&___certReq_1), value);
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490, ___extensions_2)); }
	inline RuntimeObject* get_extensions_2() const { return ___extensions_2; }
	inline RuntimeObject** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(RuntimeObject* value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_2), value);
	}

	inline static int32_t get_offset_of_extOrdering_3() { return static_cast<int32_t>(offsetof(TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490, ___extOrdering_3)); }
	inline RuntimeObject* get_extOrdering_3() const { return ___extOrdering_3; }
	inline RuntimeObject** get_address_of_extOrdering_3() { return &___extOrdering_3; }
	inline void set_extOrdering_3(RuntimeObject* value)
	{
		___extOrdering_3 = value;
		Il2CppCodeGenWriteBarrier((&___extOrdering_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPREQUESTGENERATOR_T736A0CEB3F1F370B5C5E68185E3D4799B2089490_H
#ifndef TIMESTAMPRESPONSE_T2EAB270C0EC68391A40574090078D175AA3F0A8D_H
#define TIMESTAMPRESPONSE_T2EAB270C0EC68391A40574090078D175AA3F0A8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponse
struct  TimeStampResponse_t2EAB270C0EC68391A40574090078D175AA3F0A8D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampResp BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponse::resp
	TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090 * ___resp_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponse::timeStampToken
	TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0 * ___timeStampToken_1;

public:
	inline static int32_t get_offset_of_resp_0() { return static_cast<int32_t>(offsetof(TimeStampResponse_t2EAB270C0EC68391A40574090078D175AA3F0A8D, ___resp_0)); }
	inline TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090 * get_resp_0() const { return ___resp_0; }
	inline TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090 ** get_address_of_resp_0() { return &___resp_0; }
	inline void set_resp_0(TimeStampResp_tAC8AE758086B07D77018BB1BAB5E780677BD3090 * value)
	{
		___resp_0 = value;
		Il2CppCodeGenWriteBarrier((&___resp_0), value);
	}

	inline static int32_t get_offset_of_timeStampToken_1() { return static_cast<int32_t>(offsetof(TimeStampResponse_t2EAB270C0EC68391A40574090078D175AA3F0A8D, ___timeStampToken_1)); }
	inline TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0 * get_timeStampToken_1() const { return ___timeStampToken_1; }
	inline TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0 ** get_address_of_timeStampToken_1() { return &___timeStampToken_1; }
	inline void set_timeStampToken_1(TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0 * value)
	{
		___timeStampToken_1 = value;
		Il2CppCodeGenWriteBarrier((&___timeStampToken_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPRESPONSE_T2EAB270C0EC68391A40574090078D175AA3F0A8D_H
#ifndef TIMESTAMPTOKEN_T5E2E37B82299418E2D4E594DBA14DA022AB0DCB0_H
#define TIMESTAMPTOKEN_T5E2E37B82299418E2D4E594DBA14DA022AB0DCB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken
struct  TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.CmsSignedData BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken::tsToken
	CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0 * ___tsToken_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.SignerInformation BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken::tsaSignerInfo
	SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2 * ___tsaSignerInfo_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken::tstInfo
	TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D * ___tstInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken_CertID BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken::certID
	CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F * ___certID_3;

public:
	inline static int32_t get_offset_of_tsToken_0() { return static_cast<int32_t>(offsetof(TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0, ___tsToken_0)); }
	inline CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0 * get_tsToken_0() const { return ___tsToken_0; }
	inline CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0 ** get_address_of_tsToken_0() { return &___tsToken_0; }
	inline void set_tsToken_0(CmsSignedData_t37800D8662D7471F6AD429001166DD10EBBFEEC0 * value)
	{
		___tsToken_0 = value;
		Il2CppCodeGenWriteBarrier((&___tsToken_0), value);
	}

	inline static int32_t get_offset_of_tsaSignerInfo_1() { return static_cast<int32_t>(offsetof(TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0, ___tsaSignerInfo_1)); }
	inline SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2 * get_tsaSignerInfo_1() const { return ___tsaSignerInfo_1; }
	inline SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2 ** get_address_of_tsaSignerInfo_1() { return &___tsaSignerInfo_1; }
	inline void set_tsaSignerInfo_1(SignerInformation_t29C492A05BA5B4FAC711F9BB477C0A5C254B09A2 * value)
	{
		___tsaSignerInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___tsaSignerInfo_1), value);
	}

	inline static int32_t get_offset_of_tstInfo_2() { return static_cast<int32_t>(offsetof(TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0, ___tstInfo_2)); }
	inline TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D * get_tstInfo_2() const { return ___tstInfo_2; }
	inline TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D ** get_address_of_tstInfo_2() { return &___tstInfo_2; }
	inline void set_tstInfo_2(TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D * value)
	{
		___tstInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___tstInfo_2), value);
	}

	inline static int32_t get_offset_of_certID_3() { return static_cast<int32_t>(offsetof(TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0, ___certID_3)); }
	inline CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F * get_certID_3() const { return ___certID_3; }
	inline CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F ** get_address_of_certID_3() { return &___certID_3; }
	inline void set_certID_3(CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F * value)
	{
		___certID_3 = value;
		Il2CppCodeGenWriteBarrier((&___certID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPTOKEN_T5E2E37B82299418E2D4E594DBA14DA022AB0DCB0_H
#ifndef CERTID_T5872911F314FF4FC8549E846DF63699EBC723F2F_H
#define CERTID_T5872911F314FF4FC8549E846DF63699EBC723F2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken_CertID
struct  CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertID BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken_CertID::certID
	EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960 * ___certID_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2 BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampToken_CertID::certIDv2
	EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F * ___certIDv2_1;

public:
	inline static int32_t get_offset_of_certID_0() { return static_cast<int32_t>(offsetof(CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F, ___certID_0)); }
	inline EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960 * get_certID_0() const { return ___certID_0; }
	inline EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960 ** get_address_of_certID_0() { return &___certID_0; }
	inline void set_certID_0(EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960 * value)
	{
		___certID_0 = value;
		Il2CppCodeGenWriteBarrier((&___certID_0), value);
	}

	inline static int32_t get_offset_of_certIDv2_1() { return static_cast<int32_t>(offsetof(CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F, ___certIDv2_1)); }
	inline EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F * get_certIDv2_1() const { return ___certIDv2_1; }
	inline EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F ** get_address_of_certIDv2_1() { return &___certIDv2_1; }
	inline void set_certIDv2_1(EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F * value)
	{
		___certIDv2_1 = value;
		Il2CppCodeGenWriteBarrier((&___certIDv2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTID_T5872911F314FF4FC8549E846DF63699EBC723F2F_H
#ifndef TIMESTAMPTOKENGENERATOR_T5A24E958969DE449778BB1FB791884B45A9F0E80_H
#define TIMESTAMPTOKENGENERATOR_T5A24E958969DE449778BB1FB791884B45A9F0E80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator
struct  TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::accuracySeconds
	int32_t ___accuracySeconds_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::accuracyMillis
	int32_t ___accuracyMillis_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::accuracyMicros
	int32_t ___accuracyMicros_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::ordering
	bool ___ordering_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::tsa
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___tsa_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::tsaPolicyOID
	String_t* ___tsaPolicyOID_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::key
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___key_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::cert
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___cert_7;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::digestOID
	String_t* ___digestOID_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::signedAttr
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___signedAttr_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::unsignedAttr
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * ___unsignedAttr_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::x509Certs
	RuntimeObject* ___x509Certs_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.Store.IX509Store BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator::x509Crls
	RuntimeObject* ___x509Crls_12;

public:
	inline static int32_t get_offset_of_accuracySeconds_0() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___accuracySeconds_0)); }
	inline int32_t get_accuracySeconds_0() const { return ___accuracySeconds_0; }
	inline int32_t* get_address_of_accuracySeconds_0() { return &___accuracySeconds_0; }
	inline void set_accuracySeconds_0(int32_t value)
	{
		___accuracySeconds_0 = value;
	}

	inline static int32_t get_offset_of_accuracyMillis_1() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___accuracyMillis_1)); }
	inline int32_t get_accuracyMillis_1() const { return ___accuracyMillis_1; }
	inline int32_t* get_address_of_accuracyMillis_1() { return &___accuracyMillis_1; }
	inline void set_accuracyMillis_1(int32_t value)
	{
		___accuracyMillis_1 = value;
	}

	inline static int32_t get_offset_of_accuracyMicros_2() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___accuracyMicros_2)); }
	inline int32_t get_accuracyMicros_2() const { return ___accuracyMicros_2; }
	inline int32_t* get_address_of_accuracyMicros_2() { return &___accuracyMicros_2; }
	inline void set_accuracyMicros_2(int32_t value)
	{
		___accuracyMicros_2 = value;
	}

	inline static int32_t get_offset_of_ordering_3() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___ordering_3)); }
	inline bool get_ordering_3() const { return ___ordering_3; }
	inline bool* get_address_of_ordering_3() { return &___ordering_3; }
	inline void set_ordering_3(bool value)
	{
		___ordering_3 = value;
	}

	inline static int32_t get_offset_of_tsa_4() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___tsa_4)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_tsa_4() const { return ___tsa_4; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_tsa_4() { return &___tsa_4; }
	inline void set_tsa_4(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___tsa_4 = value;
		Il2CppCodeGenWriteBarrier((&___tsa_4), value);
	}

	inline static int32_t get_offset_of_tsaPolicyOID_5() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___tsaPolicyOID_5)); }
	inline String_t* get_tsaPolicyOID_5() const { return ___tsaPolicyOID_5; }
	inline String_t** get_address_of_tsaPolicyOID_5() { return &___tsaPolicyOID_5; }
	inline void set_tsaPolicyOID_5(String_t* value)
	{
		___tsaPolicyOID_5 = value;
		Il2CppCodeGenWriteBarrier((&___tsaPolicyOID_5), value);
	}

	inline static int32_t get_offset_of_key_6() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___key_6)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_key_6() const { return ___key_6; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_key_6() { return &___key_6; }
	inline void set_key_6(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___key_6 = value;
		Il2CppCodeGenWriteBarrier((&___key_6), value);
	}

	inline static int32_t get_offset_of_cert_7() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___cert_7)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_cert_7() const { return ___cert_7; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_cert_7() { return &___cert_7; }
	inline void set_cert_7(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___cert_7 = value;
		Il2CppCodeGenWriteBarrier((&___cert_7), value);
	}

	inline static int32_t get_offset_of_digestOID_8() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___digestOID_8)); }
	inline String_t* get_digestOID_8() const { return ___digestOID_8; }
	inline String_t** get_address_of_digestOID_8() { return &___digestOID_8; }
	inline void set_digestOID_8(String_t* value)
	{
		___digestOID_8 = value;
		Il2CppCodeGenWriteBarrier((&___digestOID_8), value);
	}

	inline static int32_t get_offset_of_signedAttr_9() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___signedAttr_9)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_signedAttr_9() const { return ___signedAttr_9; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_signedAttr_9() { return &___signedAttr_9; }
	inline void set_signedAttr_9(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___signedAttr_9 = value;
		Il2CppCodeGenWriteBarrier((&___signedAttr_9), value);
	}

	inline static int32_t get_offset_of_unsignedAttr_10() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___unsignedAttr_10)); }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * get_unsignedAttr_10() const { return ___unsignedAttr_10; }
	inline AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 ** get_address_of_unsignedAttr_10() { return &___unsignedAttr_10; }
	inline void set_unsignedAttr_10(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0 * value)
	{
		___unsignedAttr_10 = value;
		Il2CppCodeGenWriteBarrier((&___unsignedAttr_10), value);
	}

	inline static int32_t get_offset_of_x509Certs_11() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___x509Certs_11)); }
	inline RuntimeObject* get_x509Certs_11() const { return ___x509Certs_11; }
	inline RuntimeObject** get_address_of_x509Certs_11() { return &___x509Certs_11; }
	inline void set_x509Certs_11(RuntimeObject* value)
	{
		___x509Certs_11 = value;
		Il2CppCodeGenWriteBarrier((&___x509Certs_11), value);
	}

	inline static int32_t get_offset_of_x509Crls_12() { return static_cast<int32_t>(offsetof(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80, ___x509Crls_12)); }
	inline RuntimeObject* get_x509Crls_12() const { return ___x509Crls_12; }
	inline RuntimeObject** get_address_of_x509Crls_12() { return &___x509Crls_12; }
	inline void set_x509Crls_12(RuntimeObject* value)
	{
		___x509Crls_12 = value;
		Il2CppCodeGenWriteBarrier((&___x509Crls_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPTOKENGENERATOR_T5A24E958969DE449778BB1FB791884B45A9F0E80_H
#ifndef TSPALGORITHMS_T6B8FAA3EBA05655593C5870A3E02EB205A81420E_H
#define TSPALGORITHMS_T6B8FAA3EBA05655593C5870A3E02EB205A81420E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms
struct  TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E  : public RuntimeObject
{
public:

public:
};

struct TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::MD5
	String_t* ___MD5_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Sha1
	String_t* ___Sha1_1;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Sha224
	String_t* ___Sha224_2;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Sha256
	String_t* ___Sha256_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Sha384
	String_t* ___Sha384_4;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Sha512
	String_t* ___Sha512_5;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::RipeMD128
	String_t* ___RipeMD128_6;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::RipeMD160
	String_t* ___RipeMD160_7;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::RipeMD256
	String_t* ___RipeMD256_8;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Gost3411
	String_t* ___Gost3411_9;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Gost3411_2012_256
	String_t* ___Gost3411_2012_256_10;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Gost3411_2012_512
	String_t* ___Gost3411_2012_512_11;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::SM3
	String_t* ___SM3_12;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspAlgorithms::Allowed
	RuntimeObject* ___Allowed_13;

public:
	inline static int32_t get_offset_of_MD5_0() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___MD5_0)); }
	inline String_t* get_MD5_0() const { return ___MD5_0; }
	inline String_t** get_address_of_MD5_0() { return &___MD5_0; }
	inline void set_MD5_0(String_t* value)
	{
		___MD5_0 = value;
		Il2CppCodeGenWriteBarrier((&___MD5_0), value);
	}

	inline static int32_t get_offset_of_Sha1_1() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Sha1_1)); }
	inline String_t* get_Sha1_1() const { return ___Sha1_1; }
	inline String_t** get_address_of_Sha1_1() { return &___Sha1_1; }
	inline void set_Sha1_1(String_t* value)
	{
		___Sha1_1 = value;
		Il2CppCodeGenWriteBarrier((&___Sha1_1), value);
	}

	inline static int32_t get_offset_of_Sha224_2() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Sha224_2)); }
	inline String_t* get_Sha224_2() const { return ___Sha224_2; }
	inline String_t** get_address_of_Sha224_2() { return &___Sha224_2; }
	inline void set_Sha224_2(String_t* value)
	{
		___Sha224_2 = value;
		Il2CppCodeGenWriteBarrier((&___Sha224_2), value);
	}

	inline static int32_t get_offset_of_Sha256_3() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Sha256_3)); }
	inline String_t* get_Sha256_3() const { return ___Sha256_3; }
	inline String_t** get_address_of_Sha256_3() { return &___Sha256_3; }
	inline void set_Sha256_3(String_t* value)
	{
		___Sha256_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sha256_3), value);
	}

	inline static int32_t get_offset_of_Sha384_4() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Sha384_4)); }
	inline String_t* get_Sha384_4() const { return ___Sha384_4; }
	inline String_t** get_address_of_Sha384_4() { return &___Sha384_4; }
	inline void set_Sha384_4(String_t* value)
	{
		___Sha384_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sha384_4), value);
	}

	inline static int32_t get_offset_of_Sha512_5() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Sha512_5)); }
	inline String_t* get_Sha512_5() const { return ___Sha512_5; }
	inline String_t** get_address_of_Sha512_5() { return &___Sha512_5; }
	inline void set_Sha512_5(String_t* value)
	{
		___Sha512_5 = value;
		Il2CppCodeGenWriteBarrier((&___Sha512_5), value);
	}

	inline static int32_t get_offset_of_RipeMD128_6() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___RipeMD128_6)); }
	inline String_t* get_RipeMD128_6() const { return ___RipeMD128_6; }
	inline String_t** get_address_of_RipeMD128_6() { return &___RipeMD128_6; }
	inline void set_RipeMD128_6(String_t* value)
	{
		___RipeMD128_6 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD128_6), value);
	}

	inline static int32_t get_offset_of_RipeMD160_7() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___RipeMD160_7)); }
	inline String_t* get_RipeMD160_7() const { return ___RipeMD160_7; }
	inline String_t** get_address_of_RipeMD160_7() { return &___RipeMD160_7; }
	inline void set_RipeMD160_7(String_t* value)
	{
		___RipeMD160_7 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD160_7), value);
	}

	inline static int32_t get_offset_of_RipeMD256_8() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___RipeMD256_8)); }
	inline String_t* get_RipeMD256_8() const { return ___RipeMD256_8; }
	inline String_t** get_address_of_RipeMD256_8() { return &___RipeMD256_8; }
	inline void set_RipeMD256_8(String_t* value)
	{
		___RipeMD256_8 = value;
		Il2CppCodeGenWriteBarrier((&___RipeMD256_8), value);
	}

	inline static int32_t get_offset_of_Gost3411_9() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Gost3411_9)); }
	inline String_t* get_Gost3411_9() const { return ___Gost3411_9; }
	inline String_t** get_address_of_Gost3411_9() { return &___Gost3411_9; }
	inline void set_Gost3411_9(String_t* value)
	{
		___Gost3411_9 = value;
		Il2CppCodeGenWriteBarrier((&___Gost3411_9), value);
	}

	inline static int32_t get_offset_of_Gost3411_2012_256_10() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Gost3411_2012_256_10)); }
	inline String_t* get_Gost3411_2012_256_10() const { return ___Gost3411_2012_256_10; }
	inline String_t** get_address_of_Gost3411_2012_256_10() { return &___Gost3411_2012_256_10; }
	inline void set_Gost3411_2012_256_10(String_t* value)
	{
		___Gost3411_2012_256_10 = value;
		Il2CppCodeGenWriteBarrier((&___Gost3411_2012_256_10), value);
	}

	inline static int32_t get_offset_of_Gost3411_2012_512_11() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Gost3411_2012_512_11)); }
	inline String_t* get_Gost3411_2012_512_11() const { return ___Gost3411_2012_512_11; }
	inline String_t** get_address_of_Gost3411_2012_512_11() { return &___Gost3411_2012_512_11; }
	inline void set_Gost3411_2012_512_11(String_t* value)
	{
		___Gost3411_2012_512_11 = value;
		Il2CppCodeGenWriteBarrier((&___Gost3411_2012_512_11), value);
	}

	inline static int32_t get_offset_of_SM3_12() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___SM3_12)); }
	inline String_t* get_SM3_12() const { return ___SM3_12; }
	inline String_t** get_address_of_SM3_12() { return &___SM3_12; }
	inline void set_SM3_12(String_t* value)
	{
		___SM3_12 = value;
		Il2CppCodeGenWriteBarrier((&___SM3_12), value);
	}

	inline static int32_t get_offset_of_Allowed_13() { return static_cast<int32_t>(offsetof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields, ___Allowed_13)); }
	inline RuntimeObject* get_Allowed_13() const { return ___Allowed_13; }
	inline RuntimeObject** get_address_of_Allowed_13() { return &___Allowed_13; }
	inline void set_Allowed_13(RuntimeObject* value)
	{
		___Allowed_13 = value;
		Il2CppCodeGenWriteBarrier((&___Allowed_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSPALGORITHMS_T6B8FAA3EBA05655593C5870A3E02EB205A81420E_H
#ifndef TSPUTIL_T6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_H
#define TSPUTIL_T6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspUtil
struct  TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22  : public RuntimeObject
{
public:

public:
};

struct TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspUtil::EmptySet
	RuntimeObject* ___EmptySet_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspUtil::EmptyList
	RuntimeObject* ___EmptyList_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspUtil::digestLengths
	RuntimeObject* ___digestLengths_2;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspUtil::digestNames
	RuntimeObject* ___digestNames_3;

public:
	inline static int32_t get_offset_of_EmptySet_0() { return static_cast<int32_t>(offsetof(TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields, ___EmptySet_0)); }
	inline RuntimeObject* get_EmptySet_0() const { return ___EmptySet_0; }
	inline RuntimeObject** get_address_of_EmptySet_0() { return &___EmptySet_0; }
	inline void set_EmptySet_0(RuntimeObject* value)
	{
		___EmptySet_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptySet_0), value);
	}

	inline static int32_t get_offset_of_EmptyList_1() { return static_cast<int32_t>(offsetof(TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields, ___EmptyList_1)); }
	inline RuntimeObject* get_EmptyList_1() const { return ___EmptyList_1; }
	inline RuntimeObject** get_address_of_EmptyList_1() { return &___EmptyList_1; }
	inline void set_EmptyList_1(RuntimeObject* value)
	{
		___EmptyList_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyList_1), value);
	}

	inline static int32_t get_offset_of_digestLengths_2() { return static_cast<int32_t>(offsetof(TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields, ___digestLengths_2)); }
	inline RuntimeObject* get_digestLengths_2() const { return ___digestLengths_2; }
	inline RuntimeObject** get_address_of_digestLengths_2() { return &___digestLengths_2; }
	inline void set_digestLengths_2(RuntimeObject* value)
	{
		___digestLengths_2 = value;
		Il2CppCodeGenWriteBarrier((&___digestLengths_2), value);
	}

	inline static int32_t get_offset_of_digestNames_3() { return static_cast<int32_t>(offsetof(TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields, ___digestNames_3)); }
	inline RuntimeObject* get_digestNames_3() const { return ___digestNames_3; }
	inline RuntimeObject** get_address_of_digestNames_3() { return &___digestNames_3; }
	inline void set_digestNames_3(RuntimeObject* value)
	{
		___digestNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___digestNames_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSPUTIL_T6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_H
#ifndef COLLECTIONUTILITIES_T41C837F75630401B89A24EE73E60C693E0493EF5_H
#define COLLECTIONUTILITIES_T41C837F75630401B89A24EE73E60C693E0493EF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.CollectionUtilities
struct  CollectionUtilities_t41C837F75630401B89A24EE73E60C693E0493EF5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILITIES_T41C837F75630401B89A24EE73E60C693E0493EF5_H
#ifndef EMPTYENUMERABLE_T53D95839E98C4889CC7461D6E28EFBF395D685A8_H
#define EMPTYENUMERABLE_T53D95839E98C4889CC7461D6E28EFBF395D685A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.EmptyEnumerable
struct  EmptyEnumerable_t53D95839E98C4889CC7461D6E28EFBF395D685A8  : public RuntimeObject
{
public:

public:
};

struct EmptyEnumerable_t53D95839E98C4889CC7461D6E28EFBF395D685A8_StaticFields
{
public:
	// System.Collections.IEnumerable BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.EmptyEnumerable::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(EmptyEnumerable_t53D95839E98C4889CC7461D6E28EFBF395D685A8_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYENUMERABLE_T53D95839E98C4889CC7461D6E28EFBF395D685A8_H
#ifndef EMPTYENUMERATOR_TADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_H
#define EMPTYENUMERATOR_TADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.EmptyEnumerator
struct  EmptyEnumerator_tADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E  : public RuntimeObject
{
public:

public:
};

struct EmptyEnumerator_tADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_StaticFields
{
public:
	// System.Collections.IEnumerator BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.EmptyEnumerator::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(EmptyEnumerator_tADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYENUMERATOR_TADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_H
#ifndef ENUMERABLEPROXY_T9757811863ABD782F3956897CCC6DDD8EE27DAF6_H
#define ENUMERABLEPROXY_T9757811863ABD782F3956897CCC6DDD8EE27DAF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.EnumerableProxy
struct  EnumerableProxy_t9757811863ABD782F3956897CCC6DDD8EE27DAF6  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.EnumerableProxy::inner
	RuntimeObject* ___inner_0;

public:
	inline static int32_t get_offset_of_inner_0() { return static_cast<int32_t>(offsetof(EnumerableProxy_t9757811863ABD782F3956897CCC6DDD8EE27DAF6, ___inner_0)); }
	inline RuntimeObject* get_inner_0() const { return ___inner_0; }
	inline RuntimeObject** get_address_of_inner_0() { return &___inner_0; }
	inline void set_inner_0(RuntimeObject* value)
	{
		___inner_0 = value;
		Il2CppCodeGenWriteBarrier((&___inner_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEPROXY_T9757811863ABD782F3956897CCC6DDD8EE27DAF6_H
#ifndef HASHSET_T263C966E697A34930D16BAA5058B0FB4A3671DA2_H
#define HASHSET_T263C966E697A34930D16BAA5058B0FB4A3671DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.HashSet
struct  HashSet_t263C966E697A34930D16BAA5058B0FB4A3671DA2  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.HashSet::impl
	RuntimeObject* ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(HashSet_t263C966E697A34930D16BAA5058B0FB4A3671DA2, ___impl_0)); }
	inline RuntimeObject* get_impl_0() const { return ___impl_0; }
	inline RuntimeObject** get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(RuntimeObject* value)
	{
		___impl_0 = value;
		Il2CppCodeGenWriteBarrier((&___impl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_T263C966E697A34930D16BAA5058B0FB4A3671DA2_H
#ifndef LINKEDDICTIONARY_T0695B145B529C831D447321806CAC2F8CEF2ED26_H
#define LINKEDDICTIONARY_T0695B145B529C831D447321806CAC2F8CEF2ED26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionary
struct  LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionary::hash
	RuntimeObject* ___hash_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionary::keys
	RuntimeObject* ___keys_1;

public:
	inline static int32_t get_offset_of_hash_0() { return static_cast<int32_t>(offsetof(LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26, ___hash_0)); }
	inline RuntimeObject* get_hash_0() const { return ___hash_0; }
	inline RuntimeObject** get_address_of_hash_0() { return &___hash_0; }
	inline void set_hash_0(RuntimeObject* value)
	{
		___hash_0 = value;
		Il2CppCodeGenWriteBarrier((&___hash_0), value);
	}

	inline static int32_t get_offset_of_keys_1() { return static_cast<int32_t>(offsetof(LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26, ___keys_1)); }
	inline RuntimeObject* get_keys_1() const { return ___keys_1; }
	inline RuntimeObject** get_address_of_keys_1() { return &___keys_1; }
	inline void set_keys_1(RuntimeObject* value)
	{
		___keys_1 = value;
		Il2CppCodeGenWriteBarrier((&___keys_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDDICTIONARY_T0695B145B529C831D447321806CAC2F8CEF2ED26_H
#ifndef LINKEDDICTIONARYENUMERATOR_T7D51242A16BFAB453027645F7E55CC040CE2B7F6_H
#define LINKEDDICTIONARYENUMERATOR_T7D51242A16BFAB453027645F7E55CC040CE2B7F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionaryEnumerator
struct  LinkedDictionaryEnumerator_t7D51242A16BFAB453027645F7E55CC040CE2B7F6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionaryEnumerator::parent
	LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26 * ___parent_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.LinkedDictionaryEnumerator::pos
	int32_t ___pos_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LinkedDictionaryEnumerator_t7D51242A16BFAB453027645F7E55CC040CE2B7F6, ___parent_0)); }
	inline LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26 * get_parent_0() const { return ___parent_0; }
	inline LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(LinkedDictionaryEnumerator_t7D51242A16BFAB453027645F7E55CC040CE2B7F6, ___pos_1)); }
	inline int32_t get_pos_1() const { return ___pos_1; }
	inline int32_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(int32_t value)
	{
		___pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDDICTIONARYENUMERATOR_T7D51242A16BFAB453027645F7E55CC040CE2B7F6_H
#ifndef UNMODIFIABLEDICTIONARY_T8A00E0B5E7350E7B7756E903FF88B28ED3A8E42E_H
#define UNMODIFIABLEDICTIONARY_T8A00E0B5E7350E7B7756E903FF88B28ED3A8E42E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableDictionary
struct  UnmodifiableDictionary_t8A00E0B5E7350E7B7756E903FF88B28ED3A8E42E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMODIFIABLEDICTIONARY_T8A00E0B5E7350E7B7756E903FF88B28ED3A8E42E_H
#ifndef UNMODIFIABLELIST_TF25052FCEB243F378A508E67E2739253D63BEBB1_H
#define UNMODIFIABLELIST_TF25052FCEB243F378A508E67E2739253D63BEBB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableList
struct  UnmodifiableList_tF25052FCEB243F378A508E67E2739253D63BEBB1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMODIFIABLELIST_TF25052FCEB243F378A508E67E2739253D63BEBB1_H
#ifndef UNMODIFIABLESET_T5CEDB4CF6C484173475B127E95E2439F9D9A8780_H
#define UNMODIFIABLESET_T5CEDB4CF6C484173475B127E95E2439F9D9A8780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableSet
struct  UnmodifiableSet_t5CEDB4CF6C484173475B127E95E2439F9D9A8780  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMODIFIABLESET_T5CEDB4CF6C484173475B127E95E2439F9D9A8780_H
#ifndef BASE64_TF27AD5C6FBD9446CADDDF0F99A695FD8872A0447_H
#define BASE64_TF27AD5C6FBD9446CADDDF0F99A695FD8872A0447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Base64
struct  Base64_tF27AD5C6FBD9446CADDDF0F99A695FD8872A0447  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64_TF27AD5C6FBD9446CADDDF0F99A695FD8872A0447_H
#ifndef BASE64ENCODER_T057A618D1174C18843F0B08885DC958BAE9D0719_H
#define BASE64ENCODER_T057A618D1174C18843F0B08885DC958BAE9D0719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Base64Encoder
struct  Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Base64Encoder::encodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encodingTable_0;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Base64Encoder::padding
	uint8_t ___padding_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Base64Encoder::decodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___decodingTable_2;

public:
	inline static int32_t get_offset_of_encodingTable_0() { return static_cast<int32_t>(offsetof(Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719, ___encodingTable_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encodingTable_0() const { return ___encodingTable_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encodingTable_0() { return &___encodingTable_0; }
	inline void set_encodingTable_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encodingTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___encodingTable_0), value);
	}

	inline static int32_t get_offset_of_padding_1() { return static_cast<int32_t>(offsetof(Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719, ___padding_1)); }
	inline uint8_t get_padding_1() const { return ___padding_1; }
	inline uint8_t* get_address_of_padding_1() { return &___padding_1; }
	inline void set_padding_1(uint8_t value)
	{
		___padding_1 = value;
	}

	inline static int32_t get_offset_of_decodingTable_2() { return static_cast<int32_t>(offsetof(Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719, ___decodingTable_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_decodingTable_2() const { return ___decodingTable_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_decodingTable_2() { return &___decodingTable_2; }
	inline void set_decodingTable_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___decodingTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___decodingTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_T057A618D1174C18843F0B08885DC958BAE9D0719_H
#ifndef BUFFEREDDECODER_TDF76C236B886C73D04B0F8E9C26785ED89B553BE_H
#define BUFFEREDDECODER_TDF76C236B886C73D04B0F8E9C26785ED89B553BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedDecoder
struct  BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedDecoder::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedDecoder::bufOff
	int32_t ___bufOff_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.ITranslator BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedDecoder::translator
	RuntimeObject* ___translator_2;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE, ___buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_bufOff_1() { return static_cast<int32_t>(offsetof(BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE, ___bufOff_1)); }
	inline int32_t get_bufOff_1() const { return ___bufOff_1; }
	inline int32_t* get_address_of_bufOff_1() { return &___bufOff_1; }
	inline void set_bufOff_1(int32_t value)
	{
		___bufOff_1 = value;
	}

	inline static int32_t get_offset_of_translator_2() { return static_cast<int32_t>(offsetof(BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE, ___translator_2)); }
	inline RuntimeObject* get_translator_2() const { return ___translator_2; }
	inline RuntimeObject** get_address_of_translator_2() { return &___translator_2; }
	inline void set_translator_2(RuntimeObject* value)
	{
		___translator_2 = value;
		Il2CppCodeGenWriteBarrier((&___translator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDDECODER_TDF76C236B886C73D04B0F8E9C26785ED89B553BE_H
#ifndef BUFFEREDENCODER_T02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4_H
#define BUFFEREDENCODER_T02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedEncoder
struct  BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedEncoder::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedEncoder::bufOff
	int32_t ___bufOff_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.ITranslator BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.BufferedEncoder::translator
	RuntimeObject* ___translator_2;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_bufOff_1() { return static_cast<int32_t>(offsetof(BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4, ___bufOff_1)); }
	inline int32_t get_bufOff_1() const { return ___bufOff_1; }
	inline int32_t* get_address_of_bufOff_1() { return &___bufOff_1; }
	inline void set_bufOff_1(int32_t value)
	{
		___bufOff_1 = value;
	}

	inline static int32_t get_offset_of_translator_2() { return static_cast<int32_t>(offsetof(BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4, ___translator_2)); }
	inline RuntimeObject* get_translator_2() const { return ___translator_2; }
	inline RuntimeObject** get_address_of_translator_2() { return &___translator_2; }
	inline void set_translator_2(RuntimeObject* value)
	{
		___translator_2 = value;
		Il2CppCodeGenWriteBarrier((&___translator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDENCODER_T02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4_H
#ifndef HEX_T91584B760E8A04A084DC9738ABCBC78C6AF908F7_H
#define HEX_T91584B760E8A04A084DC9738ABCBC78C6AF908F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Hex
struct  Hex_t91584B760E8A04A084DC9738ABCBC78C6AF908F7  : public RuntimeObject
{
public:

public:
};

struct Hex_t91584B760E8A04A084DC9738ABCBC78C6AF908F7_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexEncoder BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.Hex::encoder
	HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5 * ___encoder_0;

public:
	inline static int32_t get_offset_of_encoder_0() { return static_cast<int32_t>(offsetof(Hex_t91584B760E8A04A084DC9738ABCBC78C6AF908F7_StaticFields, ___encoder_0)); }
	inline HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5 * get_encoder_0() const { return ___encoder_0; }
	inline HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5 ** get_address_of_encoder_0() { return &___encoder_0; }
	inline void set_encoder_0(HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5 * value)
	{
		___encoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEX_T91584B760E8A04A084DC9738ABCBC78C6AF908F7_H
#ifndef HEXENCODER_T33B81F2456384DBF324591290274042B5FFF28A5_H
#define HEXENCODER_T33B81F2456384DBF324591290274042B5FFF28A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexEncoder
struct  HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexEncoder::encodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encodingTable_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexEncoder::decodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___decodingTable_1;

public:
	inline static int32_t get_offset_of_encodingTable_0() { return static_cast<int32_t>(offsetof(HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5, ___encodingTable_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encodingTable_0() const { return ___encodingTable_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encodingTable_0() { return &___encodingTable_0; }
	inline void set_encodingTable_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encodingTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___encodingTable_0), value);
	}

	inline static int32_t get_offset_of_decodingTable_1() { return static_cast<int32_t>(offsetof(HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5, ___decodingTable_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_decodingTable_1() const { return ___decodingTable_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_decodingTable_1() { return &___decodingTable_1; }
	inline void set_decodingTable_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___decodingTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___decodingTable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXENCODER_T33B81F2456384DBF324591290274042B5FFF28A5_H
#ifndef HEXTRANSLATOR_T168D0C4DA539BDF534B1729171CFEF435BD5ACD2_H
#define HEXTRANSLATOR_T168D0C4DA539BDF534B1729171CFEF435BD5ACD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexTranslator
struct  HexTranslator_t168D0C4DA539BDF534B1729171CFEF435BD5ACD2  : public RuntimeObject
{
public:

public:
};

struct HexTranslator_t168D0C4DA539BDF534B1729171CFEF435BD5ACD2_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.HexTranslator::hexTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___hexTable_0;

public:
	inline static int32_t get_offset_of_hexTable_0() { return static_cast<int32_t>(offsetof(HexTranslator_t168D0C4DA539BDF534B1729171CFEF435BD5ACD2_StaticFields, ___hexTable_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_hexTable_0() const { return ___hexTable_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_hexTable_0() { return &___hexTable_0; }
	inline void set_hexTable_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___hexTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___hexTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXTRANSLATOR_T168D0C4DA539BDF534B1729171CFEF435BD5ACD2_H
#ifndef URLBASE64_T683C2C122C3D3FA34F5CBBF3546472C48FF747C4_H
#define URLBASE64_T683C2C122C3D3FA34F5CBBF3546472C48FF747C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.UrlBase64
struct  UrlBase64_t683C2C122C3D3FA34F5CBBF3546472C48FF747C4  : public RuntimeObject
{
public:

public:
};

struct UrlBase64_t683C2C122C3D3FA34F5CBBF3546472C48FF747C4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.IEncoder BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.UrlBase64::encoder
	RuntimeObject* ___encoder_0;

public:
	inline static int32_t get_offset_of_encoder_0() { return static_cast<int32_t>(offsetof(UrlBase64_t683C2C122C3D3FA34F5CBBF3546472C48FF747C4_StaticFields, ___encoder_0)); }
	inline RuntimeObject* get_encoder_0() const { return ___encoder_0; }
	inline RuntimeObject** get_address_of_encoder_0() { return &___encoder_0; }
	inline void set_encoder_0(RuntimeObject* value)
	{
		___encoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLBASE64_T683C2C122C3D3FA34F5CBBF3546472C48FF747C4_H
#ifndef PEMHEADER_T1B54475402315098E69F80E778DC0D02F26CAE43_H
#define PEMHEADER_T1B54475402315098E69F80E778DC0D02F26CAE43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemHeader
struct  PemHeader_t1B54475402315098E69F80E778DC0D02F26CAE43  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemHeader::name
	String_t* ___name_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemHeader::val
	String_t* ___val_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PemHeader_t1B54475402315098E69F80E778DC0D02F26CAE43, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(PemHeader_t1B54475402315098E69F80E778DC0D02F26CAE43, ___val_1)); }
	inline String_t* get_val_1() const { return ___val_1; }
	inline String_t** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(String_t* value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMHEADER_T1B54475402315098E69F80E778DC0D02F26CAE43_H
#ifndef PEMOBJECT_TF3C582428AB698A9DF247EEC6A023BADBD538ED7_H
#define PEMOBJECT_TF3C582428AB698A9DF247EEC6A023BADBD538ED7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemObject
struct  PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemObject::type
	String_t* ___type_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemObject::headers
	RuntimeObject* ___headers_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemObject::content
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___content_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7, ___type_0)); }
	inline String_t* get_type_0() const { return ___type_0; }
	inline String_t** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(String_t* value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7, ___headers_1)); }
	inline RuntimeObject* get_headers_1() const { return ___headers_1; }
	inline RuntimeObject** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(RuntimeObject* value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7, ___content_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_content_2() const { return ___content_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMOBJECT_TF3C582428AB698A9DF247EEC6A023BADBD538ED7_H
#ifndef PEMREADER_T21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496_H
#define PEMREADER_T21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemReader
struct  PemReader_t21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496  : public RuntimeObject
{
public:
	// System.IO.TextReader BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemReader::reader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___reader_2;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(PemReader_t21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496, ___reader_2)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_reader_2() const { return ___reader_2; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMREADER_T21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496_H
#ifndef PEMWRITER_T89ABD27685227AEA5BE9F5AE583A6B0CCB964E01_H
#define PEMWRITER_T89ABD27685227AEA5BE9F5AE583A6B0CCB964E01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemWriter
struct  PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01  : public RuntimeObject
{
public:
	// System.IO.TextWriter BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemWriter::writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___writer_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemWriter::nlLength
	int32_t ___nlLength_2;
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemWriter::buf
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___buf_3;

public:
	inline static int32_t get_offset_of_writer_1() { return static_cast<int32_t>(offsetof(PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01, ___writer_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_writer_1() const { return ___writer_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_writer_1() { return &___writer_1; }
	inline void set_writer_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___writer_1 = value;
		Il2CppCodeGenWriteBarrier((&___writer_1), value);
	}

	inline static int32_t get_offset_of_nlLength_2() { return static_cast<int32_t>(offsetof(PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01, ___nlLength_2)); }
	inline int32_t get_nlLength_2() const { return ___nlLength_2; }
	inline int32_t* get_address_of_nlLength_2() { return &___nlLength_2; }
	inline void set_nlLength_2(int32_t value)
	{
		___nlLength_2 = value;
	}

	inline static int32_t get_offset_of_buf_3() { return static_cast<int32_t>(offsetof(PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01, ___buf_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_buf_3() const { return ___buf_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_buf_3() { return &___buf_3; }
	inline void set_buf_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___buf_3 = value;
		Il2CppCodeGenWriteBarrier((&___buf_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMWRITER_T89ABD27685227AEA5BE9F5AE583A6B0CCB964E01_H
#ifndef X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#define X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509ExtensionBase
struct  X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONBASE_T7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef CERTIFICATIONREQUEST_T3FC3548FB0929D05CBF794C0534713167411C990_H
#define CERTIFICATIONREQUEST_T3FC3548FB0929D05CBF794C0534713167411C990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest
struct  CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest::reqInfo
	CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 * ___reqInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest::sigAlgId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgId_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest::sigBits
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___sigBits_4;

public:
	inline static int32_t get_offset_of_reqInfo_2() { return static_cast<int32_t>(offsetof(CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990, ___reqInfo_2)); }
	inline CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 * get_reqInfo_2() const { return ___reqInfo_2; }
	inline CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 ** get_address_of_reqInfo_2() { return &___reqInfo_2; }
	inline void set_reqInfo_2(CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 * value)
	{
		___reqInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___reqInfo_2), value);
	}

	inline static int32_t get_offset_of_sigAlgId_3() { return static_cast<int32_t>(offsetof(CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990, ___sigAlgId_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgId_3() const { return ___sigAlgId_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgId_3() { return &___sigAlgId_3; }
	inline void set_sigAlgId_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgId_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgId_3), value);
	}

	inline static int32_t get_offset_of_sigBits_4() { return static_cast<int32_t>(offsetof(CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990, ___sigBits_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_sigBits_4() const { return ___sigBits_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_sigBits_4() { return &___sigBits_4; }
	inline void set_sigBits_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___sigBits_4 = value;
		Il2CppCodeGenWriteBarrier((&___sigBits_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATIONREQUEST_T3FC3548FB0929D05CBF794C0534713167411C990_H
#ifndef PEMREADER_TD5E543412F746DB9728631D7954B6B982BFB6F89_H
#define PEMREADER_TD5E543412F746DB9728631D7954B6B982BFB6F89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemReader
struct  PemReader_tD5E543412F746DB9728631D7954B6B982BFB6F89  : public PemReader_t21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.IPasswordFinder BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemReader::pFinder
	RuntimeObject* ___pFinder_3;

public:
	inline static int32_t get_offset_of_pFinder_3() { return static_cast<int32_t>(offsetof(PemReader_tD5E543412F746DB9728631D7954B6B982BFB6F89, ___pFinder_3)); }
	inline RuntimeObject* get_pFinder_3() const { return ___pFinder_3; }
	inline RuntimeObject** get_address_of_pFinder_3() { return &___pFinder_3; }
	inline void set_pFinder_3(RuntimeObject* value)
	{
		___pFinder_3 = value;
		Il2CppCodeGenWriteBarrier((&___pFinder_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMREADER_TD5E543412F746DB9728631D7954B6B982BFB6F89_H
#ifndef PEMWRITER_T67EBB2D5518CED8A8C1BC9C3354EC8B84F80EABE_H
#define PEMWRITER_T67EBB2D5518CED8A8C1BC9C3354EC8B84F80EABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemWriter
struct  PemWriter_t67EBB2D5518CED8A8C1BC9C3354EC8B84F80EABE  : public PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMWRITER_T67EBB2D5518CED8A8C1BC9C3354EC8B84F80EABE_H
#ifndef ASYMMETRICKEYENTRY_T9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8_H
#define ASYMMETRICKEYENTRY_T9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.AsymmetricKeyEntry
struct  AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8  : public Pkcs12Entry_t6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.AsymmetricKeyEntry::key
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___key_1;

public:
	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8, ___key_1)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_key_1() const { return ___key_1; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICKEYENTRY_T9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8_H
#ifndef PKCSEXCEPTION_TA0C67D199C5FFA7A3D0544334A3ABC04214FD67F_H
#define PKCSEXCEPTION_TA0C67D199C5FFA7A3D0544334A3ABC04214FD67F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.PkcsException
struct  PkcsException_tA0C67D199C5FFA7A3D0544334A3ABC04214FD67F  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCSEXCEPTION_TA0C67D199C5FFA7A3D0544334A3ABC04214FD67F_H
#ifndef X509CERTIFICATEENTRY_T1D64153C69F3ECC2C9DC287479226E668C784A8A_H
#define X509CERTIFICATEENTRY_T1D64153C69F3ECC2C9DC287479226E668C784A8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.X509CertificateEntry
struct  X509CertificateEntry_t1D64153C69F3ECC2C9DC287479226E668C784A8A  : public Pkcs12Entry_t6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.X509.X509Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.X509CertificateEntry::cert
	X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * ___cert_1;

public:
	inline static int32_t get_offset_of_cert_1() { return static_cast<int32_t>(offsetof(X509CertificateEntry_t1D64153C69F3ECC2C9DC287479226E668C784A8A, ___cert_1)); }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * get_cert_1() const { return ___cert_1; }
	inline X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A ** get_address_of_cert_1() { return &___cert_1; }
	inline void set_cert_1(X509Certificate_t0F915BB244D50EA295819D8FBDEDD50B178C789A * value)
	{
		___cert_1 = value;
		Il2CppCodeGenWriteBarrier((&___cert_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENTRY_T1D64153C69F3ECC2C9DC287479226E668C784A8A_H
#ifndef PKIXBUILDERPARAMETERS_T3FD8259936256DEC3E3DFA11C325FFC99B04B662_H
#define PKIXBUILDERPARAMETERS_T3FD8259936256DEC3E3DFA11C325FFC99B04B662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixBuilderParameters
struct  PkixBuilderParameters_t3FD8259936256DEC3E3DFA11C325FFC99B04B662  : public PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixBuilderParameters::maxPathLength
	int32_t ___maxPathLength_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixBuilderParameters::excludedCerts
	RuntimeObject* ___excludedCerts_23;

public:
	inline static int32_t get_offset_of_maxPathLength_22() { return static_cast<int32_t>(offsetof(PkixBuilderParameters_t3FD8259936256DEC3E3DFA11C325FFC99B04B662, ___maxPathLength_22)); }
	inline int32_t get_maxPathLength_22() const { return ___maxPathLength_22; }
	inline int32_t* get_address_of_maxPathLength_22() { return &___maxPathLength_22; }
	inline void set_maxPathLength_22(int32_t value)
	{
		___maxPathLength_22 = value;
	}

	inline static int32_t get_offset_of_excludedCerts_23() { return static_cast<int32_t>(offsetof(PkixBuilderParameters_t3FD8259936256DEC3E3DFA11C325FFC99B04B662, ___excludedCerts_23)); }
	inline RuntimeObject* get_excludedCerts_23() const { return ___excludedCerts_23; }
	inline RuntimeObject** get_address_of_excludedCerts_23() { return &___excludedCerts_23; }
	inline void set_excludedCerts_23(RuntimeObject* value)
	{
		___excludedCerts_23 = value;
		Il2CppCodeGenWriteBarrier((&___excludedCerts_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXBUILDERPARAMETERS_T3FD8259936256DEC3E3DFA11C325FFC99B04B662_H
#ifndef PKIXCERTPATHBUILDERRESULT_T3F6C81087566F8C8F1FF4971E511E035019F6C69_H
#define PKIXCERTPATHBUILDERRESULT_T3F6C81087566F8C8F1FF4971E511E035019F6C69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathBuilderResult
struct  PkixCertPathBuilderResult_t3F6C81087566F8C8F1FF4971E511E035019F6C69  : public PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPath BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathBuilderResult::certPath
	PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C * ___certPath_3;

public:
	inline static int32_t get_offset_of_certPath_3() { return static_cast<int32_t>(offsetof(PkixCertPathBuilderResult_t3F6C81087566F8C8F1FF4971E511E035019F6C69, ___certPath_3)); }
	inline PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C * get_certPath_3() const { return ___certPath_3; }
	inline PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C ** get_address_of_certPath_3() { return &___certPath_3; }
	inline void set_certPath_3(PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C * value)
	{
		___certPath_3 = value;
		Il2CppCodeGenWriteBarrier((&___certPath_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHBUILDERRESULT_T3F6C81087566F8C8F1FF4971E511E035019F6C69_H
#ifndef PKIXNAMECONSTRAINTVALIDATOREXCEPTION_TE14C675AE8E98DEBC031F116BD3DE85B6CA5E46B_H
#define PKIXNAMECONSTRAINTVALIDATOREXCEPTION_TE14C675AE8E98DEBC031F116BD3DE85B6CA5E46B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixNameConstraintValidatorException
struct  PkixNameConstraintValidatorException_tE14C675AE8E98DEBC031F116BD3DE85B6CA5E46B  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXNAMECONSTRAINTVALIDATOREXCEPTION_TE14C675AE8E98DEBC031F116BD3DE85B6CA5E46B_H
#ifndef GENERALSECURITYEXCEPTION_T716664C0B62297FAAEE029B451BFB68534483ABA_H
#define GENERALSECURITYEXCEPTION_T716664C0B62297FAAEE029B451BFB68534483ABA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.GeneralSecurityException
struct  GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALSECURITYEXCEPTION_T716664C0B62297FAAEE029B451BFB68534483ABA_H
#ifndef TIMESTAMPREQUEST_T6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC_H
#define TIMESTAMPREQUEST_T6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequest
struct  TimeStampRequest_t6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC  : public X509ExtensionBase_t7B928BFCDE8619FED07D3731E0DEBA3496FCB1E0
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TimeStampReq BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequest::req
	TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3 * ___req_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampRequest::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_1;

public:
	inline static int32_t get_offset_of_req_0() { return static_cast<int32_t>(offsetof(TimeStampRequest_t6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC, ___req_0)); }
	inline TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3 * get_req_0() const { return ___req_0; }
	inline TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3 ** get_address_of_req_0() { return &___req_0; }
	inline void set_req_0(TimeStampReq_tAA5C6E201885890945014C861159673C44B7C1B3 * value)
	{
		___req_0 = value;
		Il2CppCodeGenWriteBarrier((&___req_0), value);
	}

	inline static int32_t get_offset_of_extensions_1() { return static_cast<int32_t>(offsetof(TimeStampRequest_t6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC, ___extensions_1)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_1() const { return ___extensions_1; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_1() { return &___extensions_1; }
	inline void set_extensions_1(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPREQUEST_T6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC_H
#ifndef TSPEXCEPTION_T3F04BFEB1E8A4F94868CFBF641FB6D4741C97ED0_H
#define TSPEXCEPTION_T3F04BFEB1E8A4F94868CFBF641FB6D4741C97ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspException
struct  TspException_t3F04BFEB1E8A4F94868CFBF641FB6D4741C97ED0  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSPEXCEPTION_T3F04BFEB1E8A4F94868CFBF641FB6D4741C97ED0_H
#ifndef UNMODIFIABLEDICTIONARYPROXY_TDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93_H
#define UNMODIFIABLEDICTIONARYPROXY_TDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableDictionaryProxy
struct  UnmodifiableDictionaryProxy_tDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93  : public UnmodifiableDictionary_t8A00E0B5E7350E7B7756E903FF88B28ED3A8E42E
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableDictionaryProxy::d
	RuntimeObject* ___d_0;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(UnmodifiableDictionaryProxy_tDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93, ___d_0)); }
	inline RuntimeObject* get_d_0() const { return ___d_0; }
	inline RuntimeObject** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(RuntimeObject* value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((&___d_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMODIFIABLEDICTIONARYPROXY_TDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93_H
#ifndef UNMODIFIABLELISTPROXY_T93633871FC8C5CA5AE600CE72D58387FD3F509C8_H
#define UNMODIFIABLELISTPROXY_T93633871FC8C5CA5AE600CE72D58387FD3F509C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableListProxy
struct  UnmodifiableListProxy_t93633871FC8C5CA5AE600CE72D58387FD3F509C8  : public UnmodifiableList_tF25052FCEB243F378A508E67E2739253D63BEBB1
{
public:
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableListProxy::l
	RuntimeObject* ___l_0;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(UnmodifiableListProxy_t93633871FC8C5CA5AE600CE72D58387FD3F509C8, ___l_0)); }
	inline RuntimeObject* get_l_0() const { return ___l_0; }
	inline RuntimeObject** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(RuntimeObject* value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMODIFIABLELISTPROXY_T93633871FC8C5CA5AE600CE72D58387FD3F509C8_H
#ifndef UNMODIFIABLESETPROXY_T347F240436501502E23587A3ACA2F25E0011E2AF_H
#define UNMODIFIABLESETPROXY_T347F240436501502E23587A3ACA2F25E0011E2AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableSetProxy
struct  UnmodifiableSetProxy_t347F240436501502E23587A3ACA2F25E0011E2AF  : public UnmodifiableSet_t5CEDB4CF6C484173475B127E95E2439F9D9A8780
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.UnmodifiableSetProxy::s
	RuntimeObject* ___s_0;

public:
	inline static int32_t get_offset_of_s_0() { return static_cast<int32_t>(offsetof(UnmodifiableSetProxy_t347F240436501502E23587A3ACA2F25E0011E2AF, ___s_0)); }
	inline RuntimeObject* get_s_0() const { return ___s_0; }
	inline RuntimeObject** get_address_of_s_0() { return &___s_0; }
	inline void set_s_0(RuntimeObject* value)
	{
		___s_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMODIFIABLESETPROXY_T347F240436501502E23587A3ACA2F25E0011E2AF_H
#ifndef URLBASE64ENCODER_T9C043FAA627A0C5E4BE8AF95F2D3AF949CA498A5_H
#define URLBASE64ENCODER_T9C043FAA627A0C5E4BE8AF95F2D3AF949CA498A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Encoders.UrlBase64Encoder
struct  UrlBase64Encoder_t9C043FAA627A0C5E4BE8AF95F2D3AF949CA498A5  : public Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLBASE64ENCODER_T9C043FAA627A0C5E4BE8AF95F2D3AF949CA498A5_H
#ifndef PEMGENERATIONEXCEPTION_TA6C32ECF20F15AE0AECEDD38DDDF1525A4DE80A6_H
#define PEMGENERATIONEXCEPTION_TA6C32ECF20F15AE0AECEDD38DDDF1525A4DE80A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.IO.Pem.PemGenerationException
struct  PemGenerationException_tA6C32ECF20F15AE0AECEDD38DDDF1525A4DE80A6  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMGENERATIONEXCEPTION_TA6C32ECF20F15AE0AECEDD38DDDF1525A4DE80A6_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef PKISTATUS_T6CE847C49FFAF660A821103EBB226325A5332276_H
#define PKISTATUS_T6CE847C49FFAF660A821103EBB226325A5332276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatus
struct  PkiStatus_t6CE847C49FFAF660A821103EBB226325A5332276 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PkiStatus_t6CE847C49FFAF660A821103EBB226325A5332276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKISTATUS_T6CE847C49FFAF660A821103EBB226325A5332276_H
#ifndef DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#define DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifndef PEMBASEALG_T2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8_H
#define PEMBASEALG_T2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemUtilities_PemBaseAlg
struct  PemBaseAlg_t2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemUtilities_PemBaseAlg::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PemBaseAlg_t2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMBASEALG_T2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8_H
#ifndef PEMMODE_TCC138E2D7AA0FD0665A84558E653DEF1C3A99381_H
#define PEMMODE_TCC138E2D7AA0FD0665A84558E653DEF1C3A99381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemUtilities_PemMode
struct  PemMode_tCC138E2D7AA0FD0665A84558E653DEF1C3A99381 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemUtilities_PemMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PemMode_tCC138E2D7AA0FD0665A84558E653DEF1C3A99381, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMMODE_TCC138E2D7AA0FD0665A84558E653DEF1C3A99381_H
#ifndef PKCS10CERTIFICATIONREQUEST_TC0341C512CB4185B420651CD2FE41227B8F98ED7_H
#define PKCS10CERTIFICATIONREQUEST_TC0341C512CB4185B420651CD2FE41227B8F98ED7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequest
struct  Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7  : public CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990
{
public:

public:
};

struct Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequest::algorithms
	RuntimeObject* ___algorithms_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequest::exParams
	RuntimeObject* ___exParams_6;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequest::keyAlgorithms
	RuntimeObject* ___keyAlgorithms_7;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequest::oids
	RuntimeObject* ___oids_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequest::noParams
	RuntimeObject* ___noParams_9;

public:
	inline static int32_t get_offset_of_algorithms_5() { return static_cast<int32_t>(offsetof(Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields, ___algorithms_5)); }
	inline RuntimeObject* get_algorithms_5() const { return ___algorithms_5; }
	inline RuntimeObject** get_address_of_algorithms_5() { return &___algorithms_5; }
	inline void set_algorithms_5(RuntimeObject* value)
	{
		___algorithms_5 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_5), value);
	}

	inline static int32_t get_offset_of_exParams_6() { return static_cast<int32_t>(offsetof(Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields, ___exParams_6)); }
	inline RuntimeObject* get_exParams_6() const { return ___exParams_6; }
	inline RuntimeObject** get_address_of_exParams_6() { return &___exParams_6; }
	inline void set_exParams_6(RuntimeObject* value)
	{
		___exParams_6 = value;
		Il2CppCodeGenWriteBarrier((&___exParams_6), value);
	}

	inline static int32_t get_offset_of_keyAlgorithms_7() { return static_cast<int32_t>(offsetof(Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields, ___keyAlgorithms_7)); }
	inline RuntimeObject* get_keyAlgorithms_7() const { return ___keyAlgorithms_7; }
	inline RuntimeObject** get_address_of_keyAlgorithms_7() { return &___keyAlgorithms_7; }
	inline void set_keyAlgorithms_7(RuntimeObject* value)
	{
		___keyAlgorithms_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyAlgorithms_7), value);
	}

	inline static int32_t get_offset_of_oids_8() { return static_cast<int32_t>(offsetof(Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields, ___oids_8)); }
	inline RuntimeObject* get_oids_8() const { return ___oids_8; }
	inline RuntimeObject** get_address_of_oids_8() { return &___oids_8; }
	inline void set_oids_8(RuntimeObject* value)
	{
		___oids_8 = value;
		Il2CppCodeGenWriteBarrier((&___oids_8), value);
	}

	inline static int32_t get_offset_of_noParams_9() { return static_cast<int32_t>(offsetof(Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields, ___noParams_9)); }
	inline RuntimeObject* get_noParams_9() const { return ___noParams_9; }
	inline RuntimeObject** get_address_of_noParams_9() { return &___noParams_9; }
	inline void set_noParams_9(RuntimeObject* value)
	{
		___noParams_9 = value;
		Il2CppCodeGenWriteBarrier((&___noParams_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS10CERTIFICATIONREQUEST_TC0341C512CB4185B420651CD2FE41227B8F98ED7_H
#ifndef PKIXCERTPATHBUILDEREXCEPTION_T7DADE7671A80C3A12FE481B385306F616EB0D237_H
#define PKIXCERTPATHBUILDEREXCEPTION_T7DADE7671A80C3A12FE481B385306F616EB0D237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathBuilderException
struct  PkixCertPathBuilderException_t7DADE7671A80C3A12FE481B385306F616EB0D237  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHBUILDEREXCEPTION_T7DADE7671A80C3A12FE481B385306F616EB0D237_H
#ifndef PKIXCERTPATHVALIDATOREXCEPTION_TB111C00EDB52C1C2E107E301EE3717DBD0B325B4_H
#define PKIXCERTPATHVALIDATOREXCEPTION_TB111C00EDB52C1C2E107E301EE3717DBD0B325B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorException
struct  PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4  : public GeneralSecurityException_t716664C0B62297FAAEE029B451BFB68534483ABA
{
public:
	// System.Exception BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorException::cause
	Exception_t * ___cause_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPath BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorException::certPath
	PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C * ___certPath_18;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Pkix.PkixCertPathValidatorException::index
	int32_t ___index_19;

public:
	inline static int32_t get_offset_of_cause_17() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4, ___cause_17)); }
	inline Exception_t * get_cause_17() const { return ___cause_17; }
	inline Exception_t ** get_address_of_cause_17() { return &___cause_17; }
	inline void set_cause_17(Exception_t * value)
	{
		___cause_17 = value;
		Il2CppCodeGenWriteBarrier((&___cause_17), value);
	}

	inline static int32_t get_offset_of_certPath_18() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4, ___certPath_18)); }
	inline PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C * get_certPath_18() const { return ___certPath_18; }
	inline PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C ** get_address_of_certPath_18() { return &___certPath_18; }
	inline void set_certPath_18(PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C * value)
	{
		___certPath_18 = value;
		Il2CppCodeGenWriteBarrier((&___certPath_18), value);
	}

	inline static int32_t get_offset_of_index_19() { return static_cast<int32_t>(offsetof(PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4, ___index_19)); }
	inline int32_t get_index_19() const { return ___index_19; }
	inline int32_t* get_address_of_index_19() { return &___index_19; }
	inline void set_index_19(int32_t value)
	{
		___index_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIXCERTPATHVALIDATOREXCEPTION_TB111C00EDB52C1C2E107E301EE3717DBD0B325B4_H
#ifndef TIMESTAMPTOKENINFO_TBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D_H
#define TIMESTAMPTOKENINFO_TBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenInfo
struct  TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp.TstInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenInfo::tstInfo
	TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47 * ___tstInfo_0;
	// System.DateTime BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenInfo::genTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___genTime_1;

public:
	inline static int32_t get_offset_of_tstInfo_0() { return static_cast<int32_t>(offsetof(TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D, ___tstInfo_0)); }
	inline TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47 * get_tstInfo_0() const { return ___tstInfo_0; }
	inline TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47 ** get_address_of_tstInfo_0() { return &___tstInfo_0; }
	inline void set_tstInfo_0(TstInfo_tD5FA0B62E85C480BAEAECD7C7C09806246A51E47 * value)
	{
		___tstInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___tstInfo_0), value);
	}

	inline static int32_t get_offset_of_genTime_1() { return static_cast<int32_t>(offsetof(TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D, ___genTime_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_genTime_1() const { return ___genTime_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_genTime_1() { return &___genTime_1; }
	inline void set_genTime_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___genTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPTOKENINFO_TBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D_H
#ifndef TSPVALIDATIONEXCEPTION_T1EFAA3681304E5FAEA2A40050575F36A6D90200D_H
#define TSPVALIDATIONEXCEPTION_T1EFAA3681304E5FAEA2A40050575F36A6D90200D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspValidationException
struct  TspValidationException_t1EFAA3681304E5FAEA2A40050575F36A6D90200D  : public TspException_t3F04BFEB1E8A4F94868CFBF641FB6D4741C97ED0
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TspValidationException::failureCode
	int32_t ___failureCode_17;

public:
	inline static int32_t get_offset_of_failureCode_17() { return static_cast<int32_t>(offsetof(TspValidationException_t1EFAA3681304E5FAEA2A40050575F36A6D90200D, ___failureCode_17)); }
	inline int32_t get_failureCode_17() const { return ___failureCode_17; }
	inline int32_t* get_address_of_failureCode_17() { return &___failureCode_17; }
	inline void set_failureCode_17(int32_t value)
	{
		___failureCode_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSPVALIDATIONEXCEPTION_T1EFAA3681304E5FAEA2A40050575F36A6D90200D_H
#ifndef DATETIMEOBJECT_TEA8CEAF98EBBEB28D0E9045A049A443908BB626F_H
#define DATETIMEOBJECT_TEA8CEAF98EBBEB28D0E9045A049A443908BB626F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject
struct  DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F  : public RuntimeObject
{
public:
	// System.DateTime BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeObject::dt
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___dt_0;

public:
	inline static int32_t get_offset_of_dt_0() { return static_cast<int32_t>(offsetof(DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F, ___dt_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_dt_0() const { return ___dt_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_dt_0() { return &___dt_0; }
	inline void set_dt_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___dt_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOBJECT_TEA8CEAF98EBBEB28D0E9045A049A443908BB626F_H
#ifndef DATETIMEUTILITIES_T467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_H
#define DATETIMEUTILITIES_T467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeUtilities
struct  DateTimeUtilities_t467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtilities_t467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_StaticFields
{
public:
	// System.DateTime BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Date.DateTimeUtilities::UnixEpoch
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___UnixEpoch_0;

public:
	inline static int32_t get_offset_of_UnixEpoch_0() { return static_cast<int32_t>(offsetof(DateTimeUtilities_t467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_StaticFields, ___UnixEpoch_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_UnixEpoch_0() const { return ___UnixEpoch_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_UnixEpoch_0() { return &___UnixEpoch_0; }
	inline void set_UnixEpoch_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___UnixEpoch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILITIES_T467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#define DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_4;

public:
	inline static int32_t get_offset_of_mData_3() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mData_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_3() const { return ___mData_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_3() { return &___mData_3; }
	inline void set_mData_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_3 = value;
		Il2CppCodeGenWriteBarrier((&___mData_3), value);
	}

	inline static int32_t get_offset_of_mPadBits_4() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mPadBits_4)); }
	inline int32_t get_mPadBits_4() const { return ___mPadBits_4; }
	inline int32_t* get_address_of_mPadBits_4() { return &___mPadBits_4; }
	inline void set_mPadBits_4(int32_t value)
	{
		___mPadBits_4 = value;
	}
};

struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields, ___table_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_2() const { return ___table_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier((&___table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifndef PEMEXCEPTION_T1B2DBEDD8FE5015AEEEEFEE6EF52782141766D69_H
#define PEMEXCEPTION_T1B2DBEDD8FE5015AEEEEFEE6EF52782141766D69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.OpenSsl.PemException
struct  PemException_t1B2DBEDD8FE5015AEEEEFEE6EF52782141766D69  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMEXCEPTION_T1B2DBEDD8FE5015AEEEEFEE6EF52782141766D69_H
#ifndef PKCS10CERTIFICATIONREQUESTDELAYSIGNED_T2D7BC97F310F25D29C26E2C0F7549422278F2166_H
#define PKCS10CERTIFICATIONREQUESTDELAYSIGNED_T2D7BC97F310F25D29C26E2C0F7549422278F2166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.Pkcs10CertificationRequestDelaySigned
struct  Pkcs10CertificationRequestDelaySigned_t2D7BC97F310F25D29C26E2C0F7549422278F2166  : public Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS10CERTIFICATIONREQUESTDELAYSIGNED_T2D7BC97F310F25D29C26E2C0F7549422278F2166_H
#ifndef PKCSIOEXCEPTION_T49D771DF1CCAF68687C31080C9066E6EA9F125F5_H
#define PKCSIOEXCEPTION_T49D771DF1CCAF68687C31080C9066E6EA9F125F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Pkcs.PkcsIOException
struct  PkcsIOException_t49D771DF1CCAF68687C31080C9066E6EA9F125F5  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCSIOEXCEPTION_T49D771DF1CCAF68687C31080C9066E6EA9F125F5_H
#ifndef ENCRYPTIONEXCEPTION_TFB626EB44561DA4BCBE40AC7714C6CE04D4FB976_H
#define ENCRYPTIONEXCEPTION_TFB626EB44561DA4BCBE40AC7714C6CE04D4FB976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.EncryptionException
struct  EncryptionException_tFB626EB44561DA4BCBE40AC7714C6CE04D4FB976  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONEXCEPTION_TFB626EB44561DA4BCBE40AC7714C6CE04D4FB976_H
#ifndef PASSWORDEXCEPTION_TD94D5DFE338F51032ED7FB62E201121DA98B9A36_H
#define PASSWORDEXCEPTION_TD94D5DFE338F51032ED7FB62E201121DA98B9A36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.PasswordException
struct  PasswordException_tD94D5DFE338F51032ED7FB62E201121DA98B9A36  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSWORDEXCEPTION_TD94D5DFE338F51032ED7FB62E201121DA98B9A36_H
#ifndef TIMESTAMPRESPONSEGENERATOR_TA7D652E9057D610F2475C50D5B818A4D39549A09_H
#define TIMESTAMPRESPONSEGENERATOR_TA7D652E9057D610F2475C50D5B818A4D39549A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator
struct  TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cmp.PkiStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::status
	int32_t ___status_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1EncodableVector BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::statusStrings
	Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * ___statusStrings_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::failInfo
	int32_t ___failInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampTokenGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::tokenGenerator
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80 * ___tokenGenerator_3;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::acceptedAlgorithms
	RuntimeObject* ___acceptedAlgorithms_4;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::acceptedPolicies
	RuntimeObject* ___acceptedPolicies_5;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator::acceptedExtensions
	RuntimeObject* ___acceptedExtensions_6;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_statusStrings_1() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___statusStrings_1)); }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * get_statusStrings_1() const { return ___statusStrings_1; }
	inline Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 ** get_address_of_statusStrings_1() { return &___statusStrings_1; }
	inline void set_statusStrings_1(Asn1EncodableVector_t6C604FA3421FAF2C8EA0E464C76DFB4773682509 * value)
	{
		___statusStrings_1 = value;
		Il2CppCodeGenWriteBarrier((&___statusStrings_1), value);
	}

	inline static int32_t get_offset_of_failInfo_2() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___failInfo_2)); }
	inline int32_t get_failInfo_2() const { return ___failInfo_2; }
	inline int32_t* get_address_of_failInfo_2() { return &___failInfo_2; }
	inline void set_failInfo_2(int32_t value)
	{
		___failInfo_2 = value;
	}

	inline static int32_t get_offset_of_tokenGenerator_3() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___tokenGenerator_3)); }
	inline TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80 * get_tokenGenerator_3() const { return ___tokenGenerator_3; }
	inline TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80 ** get_address_of_tokenGenerator_3() { return &___tokenGenerator_3; }
	inline void set_tokenGenerator_3(TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80 * value)
	{
		___tokenGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&___tokenGenerator_3), value);
	}

	inline static int32_t get_offset_of_acceptedAlgorithms_4() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___acceptedAlgorithms_4)); }
	inline RuntimeObject* get_acceptedAlgorithms_4() const { return ___acceptedAlgorithms_4; }
	inline RuntimeObject** get_address_of_acceptedAlgorithms_4() { return &___acceptedAlgorithms_4; }
	inline void set_acceptedAlgorithms_4(RuntimeObject* value)
	{
		___acceptedAlgorithms_4 = value;
		Il2CppCodeGenWriteBarrier((&___acceptedAlgorithms_4), value);
	}

	inline static int32_t get_offset_of_acceptedPolicies_5() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___acceptedPolicies_5)); }
	inline RuntimeObject* get_acceptedPolicies_5() const { return ___acceptedPolicies_5; }
	inline RuntimeObject** get_address_of_acceptedPolicies_5() { return &___acceptedPolicies_5; }
	inline void set_acceptedPolicies_5(RuntimeObject* value)
	{
		___acceptedPolicies_5 = value;
		Il2CppCodeGenWriteBarrier((&___acceptedPolicies_5), value);
	}

	inline static int32_t get_offset_of_acceptedExtensions_6() { return static_cast<int32_t>(offsetof(TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09, ___acceptedExtensions_6)); }
	inline RuntimeObject* get_acceptedExtensions_6() const { return ___acceptedExtensions_6; }
	inline RuntimeObject** get_address_of_acceptedExtensions_6() { return &___acceptedExtensions_6; }
	inline void set_acceptedExtensions_6(RuntimeObject* value)
	{
		___acceptedExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___acceptedExtensions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPRESPONSEGENERATOR_TA7D652E9057D610F2475C50D5B818A4D39549A09_H
#ifndef FAILINFO_T5EA9CD4DA5C767A039A7B8940095383150A25566_H
#define FAILINFO_T5EA9CD4DA5C767A039A7B8940095383150A25566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp.TimeStampResponseGenerator_FailInfo
struct  FailInfo_t5EA9CD4DA5C767A039A7B8940095383150A25566  : public DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAILINFO_T5EA9CD4DA5C767A039A7B8940095383150A25566_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (PemGenerationException_tA6C32ECF20F15AE0AECEDD38DDDF1525A4DE80A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (PemHeader_t1B54475402315098E69F80E778DC0D02F26CAE43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4001[2] = 
{
	PemHeader_t1B54475402315098E69F80E778DC0D02F26CAE43::get_offset_of_name_0(),
	PemHeader_t1B54475402315098E69F80E778DC0D02F26CAE43::get_offset_of_val_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4002[3] = 
{
	PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7::get_offset_of_type_0(),
	PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7::get_offset_of_headers_1(),
	PemObject_tF3C582428AB698A9DF247EEC6A023BADBD538ED7::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (PemReader_t21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4005[3] = 
{
	0,
	0,
	PemReader_t21DD1C1D16E0C7B58C64C84FA2BEABD5A890A496::get_offset_of_reader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4006[4] = 
{
	0,
	PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01::get_offset_of_writer_1(),
	PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01::get_offset_of_nlLength_2(),
	PemWriter_t89ABD27685227AEA5BE9F5AE583A6B0CCB964E01::get_offset_of_buf_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (Base64_tF27AD5C6FBD9446CADDDF0F99A695FD8872A0447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4008[3] = 
{
	Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719::get_offset_of_encodingTable_0(),
	Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719::get_offset_of_padding_1(),
	Base64Encoder_t057A618D1174C18843F0B08885DC958BAE9D0719::get_offset_of_decodingTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4009[3] = 
{
	BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE::get_offset_of_buffer_0(),
	BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE::get_offset_of_bufOff_1(),
	BufferedDecoder_tDF76C236B886C73D04B0F8E9C26785ED89B553BE::get_offset_of_translator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[3] = 
{
	BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4::get_offset_of_Buffer_0(),
	BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4::get_offset_of_bufOff_1(),
	BufferedEncoder_t02DDD5BB6F488D3A4C10143FCEB5EF0EC515CBF4::get_offset_of_translator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (Hex_t91584B760E8A04A084DC9738ABCBC78C6AF908F7), -1, sizeof(Hex_t91584B760E8A04A084DC9738ABCBC78C6AF908F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4011[1] = 
{
	Hex_t91584B760E8A04A084DC9738ABCBC78C6AF908F7_StaticFields::get_offset_of_encoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4012[2] = 
{
	HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5::get_offset_of_encodingTable_0(),
	HexEncoder_t33B81F2456384DBF324591290274042B5FFF28A5::get_offset_of_decodingTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (HexTranslator_t168D0C4DA539BDF534B1729171CFEF435BD5ACD2), -1, sizeof(HexTranslator_t168D0C4DA539BDF534B1729171CFEF435BD5ACD2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4013[1] = 
{
	HexTranslator_t168D0C4DA539BDF534B1729171CFEF435BD5ACD2_StaticFields::get_offset_of_hexTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (UrlBase64_t683C2C122C3D3FA34F5CBBF3546472C48FF747C4), -1, sizeof(UrlBase64_t683C2C122C3D3FA34F5CBBF3546472C48FF747C4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4016[1] = 
{
	UrlBase64_t683C2C122C3D3FA34F5CBBF3546472C48FF747C4_StaticFields::get_offset_of_encoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (UrlBase64Encoder_t9C043FAA627A0C5E4BE8AF95F2D3AF949CA498A5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4018[1] = 
{
	DateTimeObject_tEA8CEAF98EBBEB28D0E9045A049A443908BB626F::get_offset_of_dt_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (DateTimeUtilities_t467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B), -1, sizeof(DateTimeUtilities_t467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4019[1] = 
{
	DateTimeUtilities_t467B5684F7A8C22B0E60FEE0FF41449ED7D31F6B_StaticFields::get_offset_of_UnixEpoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (CollectionUtilities_t41C837F75630401B89A24EE73E60C693E0493EF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (EmptyEnumerable_t53D95839E98C4889CC7461D6E28EFBF395D685A8), -1, sizeof(EmptyEnumerable_t53D95839E98C4889CC7461D6E28EFBF395D685A8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4021[1] = 
{
	EmptyEnumerable_t53D95839E98C4889CC7461D6E28EFBF395D685A8_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (EmptyEnumerator_tADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E), -1, sizeof(EmptyEnumerator_tADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4022[1] = 
{
	EmptyEnumerator_tADCC8CF97CB7D934ADBD3BA939ADF896FAC9AB5E_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (EnumerableProxy_t9757811863ABD782F3956897CCC6DDD8EE27DAF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4023[1] = 
{
	EnumerableProxy_t9757811863ABD782F3956897CCC6DDD8EE27DAF6::get_offset_of_inner_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (HashSet_t263C966E697A34930D16BAA5058B0FB4A3671DA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4024[1] = 
{
	HashSet_t263C966E697A34930D16BAA5058B0FB4A3671DA2::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4026[2] = 
{
	LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26::get_offset_of_hash_0(),
	LinkedDictionary_t0695B145B529C831D447321806CAC2F8CEF2ED26::get_offset_of_keys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (LinkedDictionaryEnumerator_t7D51242A16BFAB453027645F7E55CC040CE2B7F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4027[2] = 
{
	LinkedDictionaryEnumerator_t7D51242A16BFAB453027645F7E55CC040CE2B7F6::get_offset_of_parent_0(),
	LinkedDictionaryEnumerator_t7D51242A16BFAB453027645F7E55CC040CE2B7F6::get_offset_of_pos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (UnmodifiableDictionary_t8A00E0B5E7350E7B7756E903FF88B28ED3A8E42E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (UnmodifiableDictionaryProxy_tDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4029[1] = 
{
	UnmodifiableDictionaryProxy_tDEC4D4855C96EA57CC97E80A23F7BA4BD184DE93::get_offset_of_d_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (UnmodifiableList_tF25052FCEB243F378A508E67E2739253D63BEBB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (UnmodifiableListProxy_t93633871FC8C5CA5AE600CE72D58387FD3F509C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[1] = 
{
	UnmodifiableListProxy_t93633871FC8C5CA5AE600CE72D58387FD3F509C8::get_offset_of_l_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (UnmodifiableSet_t5CEDB4CF6C484173475B127E95E2439F9D9A8780), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (UnmodifiableSetProxy_t347F240436501502E23587A3ACA2F25E0011E2AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4033[1] = 
{
	UnmodifiableSetProxy_t347F240436501502E23587A3ACA2F25E0011E2AF::get_offset_of_s_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (GenTimeAccuracy_t4814A445A04EB2C38DB89DE0107EDB0B1722C36A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[1] = 
{
	GenTimeAccuracy_t4814A445A04EB2C38DB89DE0107EDB0B1722C36A::get_offset_of_accuracy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (TimeStampRequest_t6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[2] = 
{
	TimeStampRequest_t6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC::get_offset_of_req_0(),
	TimeStampRequest_t6BD38C2DCDB2A37C9F587E246AAC783A0FF5ACDC::get_offset_of_extensions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4036[4] = 
{
	TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490::get_offset_of_reqPolicy_0(),
	TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490::get_offset_of_certReq_1(),
	TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490::get_offset_of_extensions_2(),
	TimeStampRequestGenerator_t736A0CEB3F1F370B5C5E68185E3D4799B2089490::get_offset_of_extOrdering_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (TimeStampResponse_t2EAB270C0EC68391A40574090078D175AA3F0A8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4037[2] = 
{
	TimeStampResponse_t2EAB270C0EC68391A40574090078D175AA3F0A8D::get_offset_of_resp_0(),
	TimeStampResponse_t2EAB270C0EC68391A40574090078D175AA3F0A8D::get_offset_of_timeStampToken_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4038[7] = 
{
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_status_0(),
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_statusStrings_1(),
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_failInfo_2(),
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_tokenGenerator_3(),
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_acceptedAlgorithms_4(),
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_acceptedPolicies_5(),
	TimeStampResponseGenerator_tA7D652E9057D610F2475C50D5B818A4D39549A09::get_offset_of_acceptedExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (FailInfo_t5EA9CD4DA5C767A039A7B8940095383150A25566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4040[4] = 
{
	TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0::get_offset_of_tsToken_0(),
	TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0::get_offset_of_tsaSignerInfo_1(),
	TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0::get_offset_of_tstInfo_2(),
	TimeStampToken_t5E2E37B82299418E2D4E594DBA14DA022AB0DCB0::get_offset_of_certID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4041[2] = 
{
	CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F::get_offset_of_certID_0(),
	CertID_t5872911F314FF4FC8549E846DF63699EBC723F2F::get_offset_of_certIDv2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[13] = 
{
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_accuracySeconds_0(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_accuracyMillis_1(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_accuracyMicros_2(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_ordering_3(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_tsa_4(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_tsaPolicyOID_5(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_key_6(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_cert_7(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_digestOID_8(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_signedAttr_9(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_unsignedAttr_10(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_x509Certs_11(),
	TimeStampTokenGenerator_t5A24E958969DE449778BB1FB791884B45A9F0E80::get_offset_of_x509Crls_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4043[2] = 
{
	TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D::get_offset_of_tstInfo_0(),
	TimeStampTokenInfo_tBF3D82E804CA1D096F0F36A3DFA589FC0F5FF25D::get_offset_of_genTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E), -1, sizeof(TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4044[14] = 
{
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_MD5_0(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Sha1_1(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Sha224_2(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Sha256_3(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Sha384_4(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Sha512_5(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_RipeMD128_6(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_RipeMD160_7(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_RipeMD256_8(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Gost3411_9(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Gost3411_2012_256_10(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Gost3411_2012_512_11(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_SM3_12(),
	TspAlgorithms_t6B8FAA3EBA05655593C5870A3E02EB205A81420E_StaticFields::get_offset_of_Allowed_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (TspException_t3F04BFEB1E8A4F94868CFBF641FB6D4741C97ED0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22), -1, sizeof(TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4046[4] = 
{
	TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields::get_offset_of_EmptySet_0(),
	TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields::get_offset_of_EmptyList_1(),
	TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields::get_offset_of_digestLengths_2(),
	TspUtil_t6F60797FAD11B1CD36F954A6BB44F0EE3A7AFD22_StaticFields::get_offset_of_digestNames_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (TspValidationException_t1EFAA3681304E5FAEA2A40050575F36A6D90200D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4047[1] = 
{
	TspValidationException_t1EFAA3681304E5FAEA2A40050575F36A6D90200D::get_offset_of_failureCode_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (CertStatus_t4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[4] = 
{
	0,
	0,
	CertStatus_t4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1::get_offset_of_status_2(),
	CertStatus_t4C08B6ADA66D28BFFD5E32F73957A86B31B43CE1::get_offset_of_revocationDate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (PkixAttrCertChecker_t5533D54A8FFFBDC161C6531896BFFA98E8B0DCF3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (PkixAttrCertPathBuilder_tD438B8690C91A7D45BD35A2C3E203B36714CA6A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4050[1] = 
{
	PkixAttrCertPathBuilder_tD438B8690C91A7D45BD35A2C3E203B36714CA6A4::get_offset_of_certPathException_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (PkixAttrCertPathValidator_t95C2FBE1603D722B81FBCF2253C4805098266EF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (PkixBuilderParameters_t3FD8259936256DEC3E3DFA11C325FFC99B04B662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4052[2] = 
{
	PkixBuilderParameters_t3FD8259936256DEC3E3DFA11C325FFC99B04B662::get_offset_of_maxPathLength_22(),
	PkixBuilderParameters_t3FD8259936256DEC3E3DFA11C325FFC99B04B662::get_offset_of_excludedCerts_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C), -1, sizeof(PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4053[2] = 
{
	PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C_StaticFields::get_offset_of_certPathEncodings_0(),
	PkixCertPath_tD91288CFD58363299DC672B45850DE2C4D40651C::get_offset_of_certificates_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (PkixCertPathBuilder_t8A953260B0C87E347F09640C76F2288B434F7596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4054[1] = 
{
	PkixCertPathBuilder_t8A953260B0C87E347F09640C76F2288B434F7596::get_offset_of_certPathException_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (PkixCertPathBuilderException_t7DADE7671A80C3A12FE481B385306F616EB0D237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (PkixCertPathBuilderResult_t3F6C81087566F8C8F1FF4971E511E035019F6C69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4056[1] = 
{
	PkixCertPathBuilderResult_t3F6C81087566F8C8F1FF4971E511E035019F6C69::get_offset_of_certPath_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (PkixCertPathChecker_t0574E1D786358E0C81EEAAE3BAD55DF8ED7B8A5E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (PkixCertPathValidator_tDAB138ACA0D6FB6291E03C57457B7A7391FCC1AA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4059[3] = 
{
	PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4::get_offset_of_cause_17(),
	PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4::get_offset_of_certPath_18(),
	PkixCertPathValidatorException_tB111C00EDB52C1C2E107E301EE3717DBD0B325B4::get_offset_of_index_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[3] = 
{
	PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35::get_offset_of_trustAnchor_0(),
	PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35::get_offset_of_policyTree_1(),
	PkixCertPathValidatorResult_tFB6F8A6881B4D7AFFC6D9ACB8D9B775795518F35::get_offset_of_subjectPublicKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9), -1, sizeof(PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4061[6] = 
{
	PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields::get_offset_of_CrlUtilities_0(),
	PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields::get_offset_of_ANY_POLICY_1(),
	PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields::get_offset_of_CRL_NUMBER_2(),
	PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields::get_offset_of_KEY_CERT_SIGN_3(),
	PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields::get_offset_of_CRL_SIGN_4(),
	PkixCertPathValidatorUtilities_tE32E916DD8F0BF9A6286A84335027E56716717A9_StaticFields::get_offset_of_crlReasons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (PkixCrlUtilities_t26FDDBA4997F123F575D96DC2600A3A3904817EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05), -1, sizeof(PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4063[11] = 
{
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05_StaticFields::get_offset_of_SerialNumberOid_0(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_excludedSubtreesDN_1(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_excludedSubtreesDNS_2(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_excludedSubtreesEmail_3(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_excludedSubtreesURI_4(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_excludedSubtreesIP_5(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_permittedSubtreesDN_6(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_permittedSubtreesDNS_7(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_permittedSubtreesEmail_8(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_permittedSubtreesURI_9(),
	PkixNameConstraintValidator_t1E574AE9747DF0C3949CFA010D2361C544C3CE05::get_offset_of_permittedSubtreesIP_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (PkixNameConstraintValidatorException_tE14C675AE8E98DEBC031F116BD3DE85B6CA5E46B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4065[22] = 
{
	0,
	0,
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_trustAnchors_2(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_date_3(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_certPathCheckers_4(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_revocationEnabled_5(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_initialPolicies_6(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_explicitPolicyRequired_7(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_anyPolicyInhibited_8(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_policyMappingInhibited_9(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_policyQualifiersRejected_10(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_certSelector_11(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_stores_12(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_selector_13(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_additionalLocationsEnabled_14(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_additionalStores_15(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_trustedACIssuers_16(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_necessaryACAttributes_17(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_prohibitedACAttributes_18(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_attrCertCheckers_19(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_validityModel_20(),
	PkixParameters_tFF7350D1D9C65A5C26ADE5AE143D3B6468C119B3::get_offset_of_useDeltas_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4066[7] = 
{
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mChildren_0(),
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mDepth_1(),
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mExpectedPolicies_2(),
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mParent_3(),
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mPolicyQualifiers_4(),
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mValidPolicy_5(),
	PkixPolicyNode_t62BAE9DD6D4021559ED0F6BC99F1197A7BF983E8::get_offset_of_mCritical_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D), -1, sizeof(ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4067[2] = 
{
	ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D::get_offset_of__reasons_0(),
	ReasonsMask_t39D10F15C9FAE12EDBCEF89E38D42326C101B79D_StaticFields::get_offset_of_AllReasons_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571), -1, sizeof(Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4068[5] = 
{
	Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields::get_offset_of_CrlUtilities_0(),
	Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields::get_offset_of_ANY_POLICY_1(),
	Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields::get_offset_of_KEY_CERT_SIGN_2(),
	Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields::get_offset_of_CRL_SIGN_3(),
	Rfc3280CertPathUtilities_t414A5F12EE63016095D202FB8A5BE4194A9CB571_StaticFields::get_offset_of_CrlReasons_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (Rfc3281CertPathUtilities_t11CD5A3750AB619E5ADA3FAFD212756C4EF7B365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4070[6] = 
{
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF::get_offset_of_pubKey_0(),
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF::get_offset_of_caName_1(),
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF::get_offset_of_caPrincipal_2(),
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF::get_offset_of_trustedCert_3(),
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF::get_offset_of_ncBytes_4(),
	TrustAnchor_t67A7DB6944B5EAE24EC2D6F7F802AF60BA3A5CAF::get_offset_of_nc_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4071[1] = 
{
	AsymmetricKeyEntry_t9AA0D89C04CD712F3228256CA582F7D7ACA6EAD8::get_offset_of_key_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (EncryptedPrivateKeyInfoFactory_t7F9B583FBCE4ECF73E308594DD330192E905AEB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7), -1, sizeof(Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4073[5] = 
{
	Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields::get_offset_of_algorithms_5(),
	Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields::get_offset_of_exParams_6(),
	Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields::get_offset_of_keyAlgorithms_7(),
	Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields::get_offset_of_oids_8(),
	Pkcs10CertificationRequest_tC0341C512CB4185B420651CD2FE41227B8F98ED7_StaticFields::get_offset_of_noParams_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (Pkcs10CertificationRequestDelaySigned_t2D7BC97F310F25D29C26E2C0F7549422278F2166), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (Pkcs12Entry_t6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4075[1] = 
{
	Pkcs12Entry_t6C1CDB3EA0AB4F0C93A5D21A1E4077D7122D9F6F::get_offset_of_attributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4076[11] = 
{
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_keys_0(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_localIds_1(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_certs_2(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_chainCerts_3(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_keyCerts_4(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_keyAlgorithm_5(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_certAlgorithm_6(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_useDerEncoding_7(),
	Pkcs12Store_t8A33D3E64E35A36A422302AF1617877011D2DFCE::get_offset_of_unmarkedKeyEntry_8(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (CertId_t764BCE0DF49AE96A163A00055501DED21132C9F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4077[1] = 
{
	CertId_t764BCE0DF49AE96A163A00055501DED21132C9F8::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4078[2] = 
{
	IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5::get_offset_of_orig_0(),
	IgnoresCaseHashtable_tA7603CFAD6585F0F1E01F6B092129BE22AFECDC5::get_offset_of_keys_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4079[3] = 
{
	Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C::get_offset_of_keyAlgorithm_0(),
	Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C::get_offset_of_certAlgorithm_1(),
	Pkcs12StoreBuilder_tB72273133B4A84B1833B8A1054743C437541244C::get_offset_of_useDerEncoding_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (Pkcs12Utilities_t8C484813DE95B655A2AF3F63A6E7FF9E76C0694A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (Pkcs8EncryptedPrivateKeyInfo_t518C1E625E8D3BD4D114DA676E2C85834F14A837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4081[1] = 
{
	Pkcs8EncryptedPrivateKeyInfo_t518C1E625E8D3BD4D114DA676E2C85834F14A837::get_offset_of_encryptedPrivateKeyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (Pkcs8EncryptedPrivateKeyInfoBuilder_tA1D73D1FFE49F54E047CBB28289D349096BFB745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4082[1] = 
{
	Pkcs8EncryptedPrivateKeyInfoBuilder_tA1D73D1FFE49F54E047CBB28289D349096BFB745::get_offset_of_privateKeyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (PkcsException_tA0C67D199C5FFA7A3D0544334A3ABC04214FD67F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (PkcsIOException_t49D771DF1CCAF68687C31080C9066E6EA9F125F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (PrivateKeyInfoFactory_t366589F2CC8DDC7F9E99AB513329ABAF977C4288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (X509CertificateEntry_t1D64153C69F3ECC2C9DC287479226E668C784A8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4086[1] = 
{
	X509CertificateEntry_t1D64153C69F3ECC2C9DC287479226E668C784A8A::get_offset_of_cert_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4088[4] = 
{
	MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D::get_offset_of_obj_0(),
	MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D::get_offset_of_algorithm_1(),
	MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D::get_offset_of_password_2(),
	MiscPemGenerator_t9758CA33CD68BFC382913C19A3C388245CB28D5D::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (PemException_t1B2DBEDD8FE5015AEEEEFEE6EF52782141766D69), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (PemReader_tD5E543412F746DB9728631D7954B6B982BFB6F89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4090[1] = 
{
	PemReader_tD5E543412F746DB9728631D7954B6B982BFB6F89::get_offset_of_pFinder_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (PemUtilities_tD7CB41D267A8A8AE2671AD196FA432DA671FA55F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (PemBaseAlg_t2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4092[11] = 
{
	PemBaseAlg_t2221B8C0ABCD6F782AC87B4B749C3B060D6A53E8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (PemMode_tCC138E2D7AA0FD0665A84558E653DEF1C3A99381)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4093[5] = 
{
	PemMode_tCC138E2D7AA0FD0665A84558E653DEF1C3A99381::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (PemWriter_t67EBB2D5518CED8A8C1BC9C3354EC8B84F80EABE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648), -1, sizeof(Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4095[11] = 
{
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields::get_offset_of_PbeSha1_RC4_128_0(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields::get_offset_of_PbeSha1_RC4_40_1(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields::get_offset_of_PbeSha1_3DES_2(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields::get_offset_of_PbeSha1_2DES_3(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields::get_offset_of_PbeSha1_RC2_128_4(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648_StaticFields::get_offset_of_PbeSha1_RC2_40_5(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648::get_offset_of_password_6(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648::get_offset_of_algorithm_7(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648::get_offset_of_iterationCount_8(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648::get_offset_of_privKey_9(),
	Pkcs8Generator_t513AEB3E26D1991496F48A52874DE2712D4C1648::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { sizeof (EncryptionException_tFB626EB44561DA4BCBE40AC7714C6CE04D4FB976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { sizeof (PasswordException_tD94D5DFE338F51032ED7FB62E201121DA98B9A36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (AgreementUtilities_tCD01694EFC38C394EC411E5DC5A4CA40DCE7A163), -1, sizeof(AgreementUtilities_tCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4098[1] = 
{
	AgreementUtilities_tCD01694EFC38C394EC411E5DC5A4CA40DCE7A163_StaticFields::get_offset_of_algorithms_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05), -1, sizeof(CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4099[2] = 
{
	CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05_StaticFields::get_offset_of_algorithms_0(),
	CipherUtilities_t10FDD7485848434F5D5688ED087206F6DED48D05_StaticFields::get_offset_of_oids_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
