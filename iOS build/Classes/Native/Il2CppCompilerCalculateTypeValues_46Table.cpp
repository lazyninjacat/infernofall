﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client
struct Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server
struct Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_t5E47CAAC0C7B909FEE19C9C4D4342AFC8B15F719;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_t391586A9268E9DABF6C6158169C448C4243FA87C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest
struct IDigest_t4779036D052ECF08BAC3DF71ED71EF04D7866C09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDsa
struct IDsa_tB90289ECED3D647DB496B2452C469119DD55D161;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySource
struct IEntropySource_tA8EC50F7B296757E8BBAAA9B18AF425E5C78D845;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySourceProvider
struct IEntropySourceProvider_t5DB483A5D9FE5DA1B25C70B9E5365C7BE27FE7B5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac
struct IMac_tD02303ADA25C4494D9C2B119B486DF1D9302B33C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IStreamCipher
struct IStreamCipher_t76B7033CC7AD20CF07EA49A55A57FFD726E22467;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IXof
struct IXof_tEDDF9C6CF81FFF63DD829E86972BABF20508F554;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac
struct HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters
struct DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters
struct DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters
struct ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters
struct ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters
struct Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters
struct Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters
struct Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PublicKeyParameters
struct Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters
struct Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters
struct Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.ISP80090Drbg
struct ISP80090Drbg_t7854C41DD412E10334F08366EFC192AE4E59D7F2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IDrbgProvider
struct IDrbgProvider_t376EE337A016F086B8949350B31F650E5EE6AD72;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_t3F0A2BB86CE5BD61188A9989250C19FDFF733F50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng
struct X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer/Buffer
struct Buffer_t443824CE041E422156B9D23C39538DBF4867327C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner/Buffer
struct Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer/Buffer
struct Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaEncoding
struct IDsaEncoding_tF99D6A547E8046506595EE6F33DD3C9B0D98F261;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaKCalculator
struct IDsaKCalculator_t56D98D2C7C07C4143A6D34661567B8945392F861;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue
struct ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream
struct ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate
struct Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest
struct CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus
struct CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream
struct RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters
struct SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials
struct TlsAgreementCredentials_t57C812A5666B726F477BEC8FD32D53619C251FE7;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAuthentication
struct TlsAuthentication_t3F79058469D179AE666FEAB27EF6C4A2C8C510F1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClient
struct TlsClient_tFF9228F9ECA37D116491670FB804DC9FC4286DBE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl
struct TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_t31D810C946EFADA4C750C42CAD416485053A047D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCredentials
struct TlsCredentials_t4B23F404DEF4AFBFF83CD6944E4707850AD7B955;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHVerifier
struct TlsDHVerifier_tE6C6EA21D97C2F0DC88EA8814159E8DE25F35E79;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsEncryptionCredentials
struct TlsEncryptionCredentials_t811D5CFF7AB1B8304FFCB9D039953E869781BC9D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash
struct TlsHandshakeHash_t269142D360A0ACD923D2C83F53967EED0B8A7480;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange
struct TlsKeyExchange_t9A6C458E34724BE3E8CF27F0E4B078ED6E8AFAD4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac
struct TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentity
struct TlsPskIdentity_t75FBEF04811B54221A234971E8E3AD69BD07DF88;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentityManager
struct TlsPskIdentityManager_t79B5F93BAE2EB8BA0CA6111DA692D23C5D3A6559;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServer
struct TlsServer_tCCA6F54E1749B5C63EAE0E110719CA857B9EDE44;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContextImpl
struct TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession
struct TlsSession_t572BDC5B5EC5BC5CFD4950D91A4EB16A421845E1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner
struct TlsSigner_tEE9BB91F8DCC5DCBF2E82A0E0761BA1946BB9338;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSignerCredentials
struct TlsSignerCredentials_t59299E7EF0C06E47564C41703C1F6C4598BA3441;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpGroupVerifier
struct TlsSrpGroupVerifier_t1709E0D66B52B037A50FB462474BE2C3129CD8D9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStream
struct TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef AEADPARAMETERS_TEE08E2135B4F08D4C070C3EE72771C97BCF49F93_H
#define AEADPARAMETERS_TEE08E2135B4F08D4C070C3EE72771C97BCF49F93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.AeadParameters
struct  AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.AeadParameters::associatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___associatedText_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.AeadParameters::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.AeadParameters::key
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___key_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.AeadParameters::macSize
	int32_t ___macSize_3;

public:
	inline static int32_t get_offset_of_associatedText_0() { return static_cast<int32_t>(offsetof(AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93, ___associatedText_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_associatedText_0() const { return ___associatedText_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_associatedText_0() { return &___associatedText_0; }
	inline void set_associatedText_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___associatedText_0 = value;
		Il2CppCodeGenWriteBarrier((&___associatedText_0), value);
	}

	inline static int32_t get_offset_of_nonce_1() { return static_cast<int32_t>(offsetof(AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93, ___nonce_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_1() const { return ___nonce_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_1() { return &___nonce_1; }
	inline void set_nonce_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_1 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93, ___key_2)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_key_2() const { return ___key_2; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_macSize_3() { return static_cast<int32_t>(offsetof(AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93, ___macSize_3)); }
	inline int32_t get_macSize_3() const { return ___macSize_3; }
	inline int32_t* get_address_of_macSize_3() { return &___macSize_3; }
	inline void set_macSize_3(int32_t value)
	{
		___macSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEADPARAMETERS_TEE08E2135B4F08D4C070C3EE72771C97BCF49F93_H
#ifndef BASICENTROPYSOURCEPROVIDER_T19007444B2357EBDD30FF785A08CAAEC6A75444A_H
#define BASICENTROPYSOURCEPROVIDER_T19007444B2357EBDD30FF785A08CAAEC6A75444A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider
struct  BasicEntropySourceProvider_t19007444B2357EBDD30FF785A08CAAEC6A75444A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider::mPredictionResistant
	bool ___mPredictionResistant_1;

public:
	inline static int32_t get_offset_of_mSecureRandom_0() { return static_cast<int32_t>(offsetof(BasicEntropySourceProvider_t19007444B2357EBDD30FF785A08CAAEC6A75444A, ___mSecureRandom_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_0() const { return ___mSecureRandom_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_0() { return &___mSecureRandom_0; }
	inline void set_mSecureRandom_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_0), value);
	}

	inline static int32_t get_offset_of_mPredictionResistant_1() { return static_cast<int32_t>(offsetof(BasicEntropySourceProvider_t19007444B2357EBDD30FF785A08CAAEC6A75444A, ___mPredictionResistant_1)); }
	inline bool get_mPredictionResistant_1() const { return ___mPredictionResistant_1; }
	inline bool* get_address_of_mPredictionResistant_1() { return &___mPredictionResistant_1; }
	inline void set_mPredictionResistant_1(bool value)
	{
		___mPredictionResistant_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICENTROPYSOURCEPROVIDER_T19007444B2357EBDD30FF785A08CAAEC6A75444A_H
#ifndef BASICENTROPYSOURCE_T828E01DF2667940BC1E5C7E906245C04E5B38351_H
#define BASICENTROPYSOURCE_T828E01DF2667940BC1E5C7E906245C04E5B38351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider_BasicEntropySource
struct  BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider_BasicEntropySource::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider_BasicEntropySource::mPredictionResistant
	bool ___mPredictionResistant_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.BasicEntropySourceProvider_BasicEntropySource::mEntropySize
	int32_t ___mEntropySize_2;

public:
	inline static int32_t get_offset_of_mSecureRandom_0() { return static_cast<int32_t>(offsetof(BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351, ___mSecureRandom_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_0() const { return ___mSecureRandom_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_0() { return &___mSecureRandom_0; }
	inline void set_mSecureRandom_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_0), value);
	}

	inline static int32_t get_offset_of_mPredictionResistant_1() { return static_cast<int32_t>(offsetof(BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351, ___mPredictionResistant_1)); }
	inline bool get_mPredictionResistant_1() const { return ___mPredictionResistant_1; }
	inline bool* get_address_of_mPredictionResistant_1() { return &___mPredictionResistant_1; }
	inline void set_mPredictionResistant_1(bool value)
	{
		___mPredictionResistant_1 = value;
	}

	inline static int32_t get_offset_of_mEntropySize_2() { return static_cast<int32_t>(offsetof(BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351, ___mEntropySize_2)); }
	inline int32_t get_mEntropySize_2() const { return ___mEntropySize_2; }
	inline int32_t* get_address_of_mEntropySize_2() { return &___mEntropySize_2; }
	inline void set_mEntropySize_2(int32_t value)
	{
		___mEntropySize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICENTROPYSOURCE_T828E01DF2667940BC1E5C7E906245C04E5B38351_H
#ifndef CRYPTOAPIENTROPYSOURCEPROVIDER_T408F886CA7FA701D993C6A7F78E4E2D32197E7B5_H
#define CRYPTOAPIENTROPYSOURCEPROVIDER_T408F886CA7FA701D993C6A7F78E4E2D32197E7B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider
struct  CryptoApiEntropySourceProvider_t408F886CA7FA701D993C6A7F78E4E2D32197E7B5  : public RuntimeObject
{
public:
	// System.Security.Cryptography.RandomNumberGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider::mRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___mRng_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider::mPredictionResistant
	bool ___mPredictionResistant_1;

public:
	inline static int32_t get_offset_of_mRng_0() { return static_cast<int32_t>(offsetof(CryptoApiEntropySourceProvider_t408F886CA7FA701D993C6A7F78E4E2D32197E7B5, ___mRng_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_mRng_0() const { return ___mRng_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_mRng_0() { return &___mRng_0; }
	inline void set_mRng_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___mRng_0 = value;
		Il2CppCodeGenWriteBarrier((&___mRng_0), value);
	}

	inline static int32_t get_offset_of_mPredictionResistant_1() { return static_cast<int32_t>(offsetof(CryptoApiEntropySourceProvider_t408F886CA7FA701D993C6A7F78E4E2D32197E7B5, ___mPredictionResistant_1)); }
	inline bool get_mPredictionResistant_1() const { return ___mPredictionResistant_1; }
	inline bool* get_address_of_mPredictionResistant_1() { return &___mPredictionResistant_1; }
	inline void set_mPredictionResistant_1(bool value)
	{
		___mPredictionResistant_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOAPIENTROPYSOURCEPROVIDER_T408F886CA7FA701D993C6A7F78E4E2D32197E7B5_H
#ifndef CRYPTOAPIENTROPYSOURCE_T96C41ACAD91992D559920020F6BB9C8247186BBC_H
#define CRYPTOAPIENTROPYSOURCE_T96C41ACAD91992D559920020F6BB9C8247186BBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider_CryptoApiEntropySource
struct  CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC  : public RuntimeObject
{
public:
	// System.Security.Cryptography.RandomNumberGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider_CryptoApiEntropySource::mRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___mRng_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider_CryptoApiEntropySource::mPredictionResistant
	bool ___mPredictionResistant_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiEntropySourceProvider_CryptoApiEntropySource::mEntropySize
	int32_t ___mEntropySize_2;

public:
	inline static int32_t get_offset_of_mRng_0() { return static_cast<int32_t>(offsetof(CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC, ___mRng_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_mRng_0() const { return ___mRng_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_mRng_0() { return &___mRng_0; }
	inline void set_mRng_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___mRng_0 = value;
		Il2CppCodeGenWriteBarrier((&___mRng_0), value);
	}

	inline static int32_t get_offset_of_mPredictionResistant_1() { return static_cast<int32_t>(offsetof(CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC, ___mPredictionResistant_1)); }
	inline bool get_mPredictionResistant_1() const { return ___mPredictionResistant_1; }
	inline bool* get_address_of_mPredictionResistant_1() { return &___mPredictionResistant_1; }
	inline void set_mPredictionResistant_1(bool value)
	{
		___mPredictionResistant_1 = value;
	}

	inline static int32_t get_offset_of_mEntropySize_2() { return static_cast<int32_t>(offsetof(CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC, ___mEntropySize_2)); }
	inline int32_t get_mEntropySize_2() const { return ___mEntropySize_2; }
	inline int32_t* get_address_of_mEntropySize_2() { return &___mEntropySize_2; }
	inline void set_mEntropySize_2(int32_t value)
	{
		___mEntropySize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOAPIENTROPYSOURCE_T96C41ACAD91992D559920020F6BB9C8247186BBC_H
#ifndef CRYPTOAPIRANDOMGENERATOR_T3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B_H
#define CRYPTOAPIRANDOMGENERATOR_T3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator
struct  CryptoApiRandomGenerator_t3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B  : public RuntimeObject
{
public:
	// System.Security.Cryptography.RandomNumberGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator::rndProv
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___rndProv_0;

public:
	inline static int32_t get_offset_of_rndProv_0() { return static_cast<int32_t>(offsetof(CryptoApiRandomGenerator_t3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B, ___rndProv_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_rndProv_0() const { return ___rndProv_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_rndProv_0() { return &___rndProv_0; }
	inline void set_rndProv_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___rndProv_0 = value;
		Il2CppCodeGenWriteBarrier((&___rndProv_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOAPIRANDOMGENERATOR_T3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B_H
#ifndef DIGESTRANDOMGENERATOR_T5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A_H
#define DIGESTRANDOMGENERATOR_T5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator
struct  DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::stateCounter
	int64_t ___stateCounter_1;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::seedCounter
	int64_t ___seedCounter_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::digest
	RuntimeObject* ___digest_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::state
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___state_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_5;

public:
	inline static int32_t get_offset_of_stateCounter_1() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A, ___stateCounter_1)); }
	inline int64_t get_stateCounter_1() const { return ___stateCounter_1; }
	inline int64_t* get_address_of_stateCounter_1() { return &___stateCounter_1; }
	inline void set_stateCounter_1(int64_t value)
	{
		___stateCounter_1 = value;
	}

	inline static int32_t get_offset_of_seedCounter_2() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A, ___seedCounter_2)); }
	inline int64_t get_seedCounter_2() const { return ___seedCounter_2; }
	inline int64_t* get_address_of_seedCounter_2() { return &___seedCounter_2; }
	inline void set_seedCounter_2(int64_t value)
	{
		___seedCounter_2 = value;
	}

	inline static int32_t get_offset_of_digest_3() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A, ___digest_3)); }
	inline RuntimeObject* get_digest_3() const { return ___digest_3; }
	inline RuntimeObject** get_address_of_digest_3() { return &___digest_3; }
	inline void set_digest_3(RuntimeObject* value)
	{
		___digest_3 = value;
		Il2CppCodeGenWriteBarrier((&___digest_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A, ___state_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_state_4() const { return ___state_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_seed_5() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A, ___seed_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_5() const { return ___seed_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_5() { return &___seed_5; }
	inline void set_seed_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_5 = value;
		Il2CppCodeGenWriteBarrier((&___seed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTRANDOMGENERATOR_T5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A_H
#ifndef CTRSP800DRBG_T8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_H
#define CTRSP800DRBG_T8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg
struct  CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySource BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mEntropySource
	RuntimeObject* ___mEntropySource_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mEngine
	RuntimeObject* ___mEngine_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mKeySizeInBits
	int32_t ___mKeySizeInBits_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mSeedLength
	int32_t ___mSeedLength_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mSecurityStrength
	int32_t ___mSecurityStrength_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mKey_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mV_10;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mReseedCounter
	int64_t ___mReseedCounter_11;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::mIsTdea
	bool ___mIsTdea_12;

public:
	inline static int32_t get_offset_of_mEntropySource_4() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mEntropySource_4)); }
	inline RuntimeObject* get_mEntropySource_4() const { return ___mEntropySource_4; }
	inline RuntimeObject** get_address_of_mEntropySource_4() { return &___mEntropySource_4; }
	inline void set_mEntropySource_4(RuntimeObject* value)
	{
		___mEntropySource_4 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySource_4), value);
	}

	inline static int32_t get_offset_of_mEngine_5() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mEngine_5)); }
	inline RuntimeObject* get_mEngine_5() const { return ___mEngine_5; }
	inline RuntimeObject** get_address_of_mEngine_5() { return &___mEngine_5; }
	inline void set_mEngine_5(RuntimeObject* value)
	{
		___mEngine_5 = value;
		Il2CppCodeGenWriteBarrier((&___mEngine_5), value);
	}

	inline static int32_t get_offset_of_mKeySizeInBits_6() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mKeySizeInBits_6)); }
	inline int32_t get_mKeySizeInBits_6() const { return ___mKeySizeInBits_6; }
	inline int32_t* get_address_of_mKeySizeInBits_6() { return &___mKeySizeInBits_6; }
	inline void set_mKeySizeInBits_6(int32_t value)
	{
		___mKeySizeInBits_6 = value;
	}

	inline static int32_t get_offset_of_mSeedLength_7() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mSeedLength_7)); }
	inline int32_t get_mSeedLength_7() const { return ___mSeedLength_7; }
	inline int32_t* get_address_of_mSeedLength_7() { return &___mSeedLength_7; }
	inline void set_mSeedLength_7(int32_t value)
	{
		___mSeedLength_7 = value;
	}

	inline static int32_t get_offset_of_mSecurityStrength_8() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mSecurityStrength_8)); }
	inline int32_t get_mSecurityStrength_8() const { return ___mSecurityStrength_8; }
	inline int32_t* get_address_of_mSecurityStrength_8() { return &___mSecurityStrength_8; }
	inline void set_mSecurityStrength_8(int32_t value)
	{
		___mSecurityStrength_8 = value;
	}

	inline static int32_t get_offset_of_mKey_9() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mKey_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mKey_9() const { return ___mKey_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mKey_9() { return &___mKey_9; }
	inline void set_mKey_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___mKey_9), value);
	}

	inline static int32_t get_offset_of_mV_10() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mV_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mV_10() const { return ___mV_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mV_10() { return &___mV_10; }
	inline void set_mV_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mV_10 = value;
		Il2CppCodeGenWriteBarrier((&___mV_10), value);
	}

	inline static int32_t get_offset_of_mReseedCounter_11() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mReseedCounter_11)); }
	inline int64_t get_mReseedCounter_11() const { return ___mReseedCounter_11; }
	inline int64_t* get_address_of_mReseedCounter_11() { return &___mReseedCounter_11; }
	inline void set_mReseedCounter_11(int64_t value)
	{
		___mReseedCounter_11 = value;
	}

	inline static int32_t get_offset_of_mIsTdea_12() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96, ___mIsTdea_12)); }
	inline bool get_mIsTdea_12() const { return ___mIsTdea_12; }
	inline bool* get_address_of_mIsTdea_12() { return &___mIsTdea_12; }
	inline void set_mIsTdea_12(bool value)
	{
		___mIsTdea_12 = value;
	}
};

struct CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::TDEA_RESEED_MAX
	int64_t ___TDEA_RESEED_MAX_0;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::AES_RESEED_MAX
	int64_t ___AES_RESEED_MAX_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::TDEA_MAX_BITS_REQUEST
	int32_t ___TDEA_MAX_BITS_REQUEST_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::AES_MAX_BITS_REQUEST
	int32_t ___AES_MAX_BITS_REQUEST_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.CtrSP800Drbg::K_BITS
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___K_BITS_13;

public:
	inline static int32_t get_offset_of_TDEA_RESEED_MAX_0() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields, ___TDEA_RESEED_MAX_0)); }
	inline int64_t get_TDEA_RESEED_MAX_0() const { return ___TDEA_RESEED_MAX_0; }
	inline int64_t* get_address_of_TDEA_RESEED_MAX_0() { return &___TDEA_RESEED_MAX_0; }
	inline void set_TDEA_RESEED_MAX_0(int64_t value)
	{
		___TDEA_RESEED_MAX_0 = value;
	}

	inline static int32_t get_offset_of_AES_RESEED_MAX_1() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields, ___AES_RESEED_MAX_1)); }
	inline int64_t get_AES_RESEED_MAX_1() const { return ___AES_RESEED_MAX_1; }
	inline int64_t* get_address_of_AES_RESEED_MAX_1() { return &___AES_RESEED_MAX_1; }
	inline void set_AES_RESEED_MAX_1(int64_t value)
	{
		___AES_RESEED_MAX_1 = value;
	}

	inline static int32_t get_offset_of_TDEA_MAX_BITS_REQUEST_2() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields, ___TDEA_MAX_BITS_REQUEST_2)); }
	inline int32_t get_TDEA_MAX_BITS_REQUEST_2() const { return ___TDEA_MAX_BITS_REQUEST_2; }
	inline int32_t* get_address_of_TDEA_MAX_BITS_REQUEST_2() { return &___TDEA_MAX_BITS_REQUEST_2; }
	inline void set_TDEA_MAX_BITS_REQUEST_2(int32_t value)
	{
		___TDEA_MAX_BITS_REQUEST_2 = value;
	}

	inline static int32_t get_offset_of_AES_MAX_BITS_REQUEST_3() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields, ___AES_MAX_BITS_REQUEST_3)); }
	inline int32_t get_AES_MAX_BITS_REQUEST_3() const { return ___AES_MAX_BITS_REQUEST_3; }
	inline int32_t* get_address_of_AES_MAX_BITS_REQUEST_3() { return &___AES_MAX_BITS_REQUEST_3; }
	inline void set_AES_MAX_BITS_REQUEST_3(int32_t value)
	{
		___AES_MAX_BITS_REQUEST_3 = value;
	}

	inline static int32_t get_offset_of_K_BITS_13() { return static_cast<int32_t>(offsetof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields, ___K_BITS_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_K_BITS_13() const { return ___K_BITS_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_K_BITS_13() { return &___K_BITS_13; }
	inline void set_K_BITS_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___K_BITS_13 = value;
		Il2CppCodeGenWriteBarrier((&___K_BITS_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTRSP800DRBG_T8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_H
#ifndef DRBGUTILITIES_TD53F1AC30016A4F2F9EC0238366A3064575D496A_H
#define DRBGUTILITIES_TD53F1AC30016A4F2F9EC0238366A3064575D496A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.DrbgUtilities
struct  DrbgUtilities_tD53F1AC30016A4F2F9EC0238366A3064575D496A  : public RuntimeObject
{
public:

public:
};

struct DrbgUtilities_tD53F1AC30016A4F2F9EC0238366A3064575D496A_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.DrbgUtilities::maxSecurityStrengths
	RuntimeObject* ___maxSecurityStrengths_0;

public:
	inline static int32_t get_offset_of_maxSecurityStrengths_0() { return static_cast<int32_t>(offsetof(DrbgUtilities_tD53F1AC30016A4F2F9EC0238366A3064575D496A_StaticFields, ___maxSecurityStrengths_0)); }
	inline RuntimeObject* get_maxSecurityStrengths_0() const { return ___maxSecurityStrengths_0; }
	inline RuntimeObject** get_address_of_maxSecurityStrengths_0() { return &___maxSecurityStrengths_0; }
	inline void set_maxSecurityStrengths_0(RuntimeObject* value)
	{
		___maxSecurityStrengths_0 = value;
		Il2CppCodeGenWriteBarrier((&___maxSecurityStrengths_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRBGUTILITIES_TD53F1AC30016A4F2F9EC0238366A3064575D496A_H
#ifndef HMACSP800DRBG_T6313607242AE0FF6C233E5BE89F07B448A7DA4F6_H
#define HMACSP800DRBG_T6313607242AE0FF6C233E5BE89F07B448A7DA4F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg
struct  HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::mK
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mK_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::mV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mV_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySource BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::mEntropySource
	RuntimeObject* ___mEntropySource_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::mHMac
	RuntimeObject* ___mHMac_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::mSecurityStrength
	int32_t ___mSecurityStrength_6;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::mReseedCounter
	int64_t ___mReseedCounter_7;

public:
	inline static int32_t get_offset_of_mK_2() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6, ___mK_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mK_2() const { return ___mK_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mK_2() { return &___mK_2; }
	inline void set_mK_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mK_2 = value;
		Il2CppCodeGenWriteBarrier((&___mK_2), value);
	}

	inline static int32_t get_offset_of_mV_3() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6, ___mV_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mV_3() const { return ___mV_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mV_3() { return &___mV_3; }
	inline void set_mV_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mV_3 = value;
		Il2CppCodeGenWriteBarrier((&___mV_3), value);
	}

	inline static int32_t get_offset_of_mEntropySource_4() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6, ___mEntropySource_4)); }
	inline RuntimeObject* get_mEntropySource_4() const { return ___mEntropySource_4; }
	inline RuntimeObject** get_address_of_mEntropySource_4() { return &___mEntropySource_4; }
	inline void set_mEntropySource_4(RuntimeObject* value)
	{
		___mEntropySource_4 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySource_4), value);
	}

	inline static int32_t get_offset_of_mHMac_5() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6, ___mHMac_5)); }
	inline RuntimeObject* get_mHMac_5() const { return ___mHMac_5; }
	inline RuntimeObject** get_address_of_mHMac_5() { return &___mHMac_5; }
	inline void set_mHMac_5(RuntimeObject* value)
	{
		___mHMac_5 = value;
		Il2CppCodeGenWriteBarrier((&___mHMac_5), value);
	}

	inline static int32_t get_offset_of_mSecurityStrength_6() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6, ___mSecurityStrength_6)); }
	inline int32_t get_mSecurityStrength_6() const { return ___mSecurityStrength_6; }
	inline int32_t* get_address_of_mSecurityStrength_6() { return &___mSecurityStrength_6; }
	inline void set_mSecurityStrength_6(int32_t value)
	{
		___mSecurityStrength_6 = value;
	}

	inline static int32_t get_offset_of_mReseedCounter_7() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6, ___mReseedCounter_7)); }
	inline int64_t get_mReseedCounter_7() const { return ___mReseedCounter_7; }
	inline int64_t* get_address_of_mReseedCounter_7() { return &___mReseedCounter_7; }
	inline void set_mReseedCounter_7(int64_t value)
	{
		___mReseedCounter_7 = value;
	}
};

struct HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::RESEED_MAX
	int64_t ___RESEED_MAX_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HMacSP800Drbg::MAX_BITS_REQUEST
	int32_t ___MAX_BITS_REQUEST_1;

public:
	inline static int32_t get_offset_of_RESEED_MAX_0() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6_StaticFields, ___RESEED_MAX_0)); }
	inline int64_t get_RESEED_MAX_0() const { return ___RESEED_MAX_0; }
	inline int64_t* get_address_of_RESEED_MAX_0() { return &___RESEED_MAX_0; }
	inline void set_RESEED_MAX_0(int64_t value)
	{
		___RESEED_MAX_0 = value;
	}

	inline static int32_t get_offset_of_MAX_BITS_REQUEST_1() { return static_cast<int32_t>(offsetof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6_StaticFields, ___MAX_BITS_REQUEST_1)); }
	inline int32_t get_MAX_BITS_REQUEST_1() const { return ___MAX_BITS_REQUEST_1; }
	inline int32_t* get_address_of_MAX_BITS_REQUEST_1() { return &___MAX_BITS_REQUEST_1; }
	inline void set_MAX_BITS_REQUEST_1(int32_t value)
	{
		___MAX_BITS_REQUEST_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMACSP800DRBG_T6313607242AE0FF6C233E5BE89F07B448A7DA4F6_H
#ifndef HASHSP800DRBG_TAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_H
#define HASHSP800DRBG_TAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg
struct  HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mDigest
	RuntimeObject* ___mDigest_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySource BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mEntropySource
	RuntimeObject* ___mEntropySource_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mSecurityStrength
	int32_t ___mSecurityStrength_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mSeedLength
	int32_t ___mSeedLength_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mV_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mC_9;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::mReseedCounter
	int64_t ___mReseedCounter_10;

public:
	inline static int32_t get_offset_of_mDigest_4() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mDigest_4)); }
	inline RuntimeObject* get_mDigest_4() const { return ___mDigest_4; }
	inline RuntimeObject** get_address_of_mDigest_4() { return &___mDigest_4; }
	inline void set_mDigest_4(RuntimeObject* value)
	{
		___mDigest_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_4), value);
	}

	inline static int32_t get_offset_of_mEntropySource_5() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mEntropySource_5)); }
	inline RuntimeObject* get_mEntropySource_5() const { return ___mEntropySource_5; }
	inline RuntimeObject** get_address_of_mEntropySource_5() { return &___mEntropySource_5; }
	inline void set_mEntropySource_5(RuntimeObject* value)
	{
		___mEntropySource_5 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySource_5), value);
	}

	inline static int32_t get_offset_of_mSecurityStrength_6() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mSecurityStrength_6)); }
	inline int32_t get_mSecurityStrength_6() const { return ___mSecurityStrength_6; }
	inline int32_t* get_address_of_mSecurityStrength_6() { return &___mSecurityStrength_6; }
	inline void set_mSecurityStrength_6(int32_t value)
	{
		___mSecurityStrength_6 = value;
	}

	inline static int32_t get_offset_of_mSeedLength_7() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mSeedLength_7)); }
	inline int32_t get_mSeedLength_7() const { return ___mSeedLength_7; }
	inline int32_t* get_address_of_mSeedLength_7() { return &___mSeedLength_7; }
	inline void set_mSeedLength_7(int32_t value)
	{
		___mSeedLength_7 = value;
	}

	inline static int32_t get_offset_of_mV_8() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mV_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mV_8() const { return ___mV_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mV_8() { return &___mV_8; }
	inline void set_mV_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mV_8 = value;
		Il2CppCodeGenWriteBarrier((&___mV_8), value);
	}

	inline static int32_t get_offset_of_mC_9() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mC_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mC_9() const { return ___mC_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mC_9() { return &___mC_9; }
	inline void set_mC_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mC_9 = value;
		Il2CppCodeGenWriteBarrier((&___mC_9), value);
	}

	inline static int32_t get_offset_of_mReseedCounter_10() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5, ___mReseedCounter_10)); }
	inline int64_t get_mReseedCounter_10() const { return ___mReseedCounter_10; }
	inline int64_t* get_address_of_mReseedCounter_10() { return &___mReseedCounter_10; }
	inline void set_mReseedCounter_10(int64_t value)
	{
		___mReseedCounter_10 = value;
	}
};

struct HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::ONE
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ONE_0;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::RESEED_MAX
	int64_t ___RESEED_MAX_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::MAX_BITS_REQUEST
	int32_t ___MAX_BITS_REQUEST_2;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.HashSP800Drbg::seedlens
	RuntimeObject* ___seedlens_3;

public:
	inline static int32_t get_offset_of_ONE_0() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields, ___ONE_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ONE_0() const { return ___ONE_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ONE_0() { return &___ONE_0; }
	inline void set_ONE_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ONE_0 = value;
		Il2CppCodeGenWriteBarrier((&___ONE_0), value);
	}

	inline static int32_t get_offset_of_RESEED_MAX_1() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields, ___RESEED_MAX_1)); }
	inline int64_t get_RESEED_MAX_1() const { return ___RESEED_MAX_1; }
	inline int64_t* get_address_of_RESEED_MAX_1() { return &___RESEED_MAX_1; }
	inline void set_RESEED_MAX_1(int64_t value)
	{
		___RESEED_MAX_1 = value;
	}

	inline static int32_t get_offset_of_MAX_BITS_REQUEST_2() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields, ___MAX_BITS_REQUEST_2)); }
	inline int32_t get_MAX_BITS_REQUEST_2() const { return ___MAX_BITS_REQUEST_2; }
	inline int32_t* get_address_of_MAX_BITS_REQUEST_2() { return &___MAX_BITS_REQUEST_2; }
	inline void set_MAX_BITS_REQUEST_2(int32_t value)
	{
		___MAX_BITS_REQUEST_2 = value;
	}

	inline static int32_t get_offset_of_seedlens_3() { return static_cast<int32_t>(offsetof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields, ___seedlens_3)); }
	inline RuntimeObject* get_seedlens_3() const { return ___seedlens_3; }
	inline RuntimeObject** get_address_of_seedlens_3() { return &___seedlens_3; }
	inline void set_seedlens_3(RuntimeObject* value)
	{
		___seedlens_3 = value;
		Il2CppCodeGenWriteBarrier((&___seedlens_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSP800DRBG_TAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_H
#ifndef ENTROPYUTILITIES_TD12197BB2D9598056959C4ACD7543BAFAB04A36C_H
#define ENTROPYUTILITIES_TD12197BB2D9598056959C4ACD7543BAFAB04A36C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.EntropyUtilities
struct  EntropyUtilities_tD12197BB2D9598056959C4ACD7543BAFAB04A36C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTROPYUTILITIES_TD12197BB2D9598056959C4ACD7543BAFAB04A36C_H
#ifndef REVERSEDWINDOWGENERATOR_T171F7229177F9C5899E1B955817CFD447956572A_H
#define REVERSEDWINDOWGENERATOR_T171F7229177F9C5899E1B955817CFD447956572A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ReversedWindowGenerator
struct  ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ReversedWindowGenerator::generator
	RuntimeObject* ___generator_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ReversedWindowGenerator::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ReversedWindowGenerator::windowCount
	int32_t ___windowCount_2;

public:
	inline static int32_t get_offset_of_generator_0() { return static_cast<int32_t>(offsetof(ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A, ___generator_0)); }
	inline RuntimeObject* get_generator_0() const { return ___generator_0; }
	inline RuntimeObject** get_address_of_generator_0() { return &___generator_0; }
	inline void set_generator_0(RuntimeObject* value)
	{
		___generator_0 = value;
		Il2CppCodeGenWriteBarrier((&___generator_0), value);
	}

	inline static int32_t get_offset_of_window_1() { return static_cast<int32_t>(offsetof(ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A, ___window_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_1() const { return ___window_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_1() { return &___window_1; }
	inline void set_window_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_1 = value;
		Il2CppCodeGenWriteBarrier((&___window_1), value);
	}

	inline static int32_t get_offset_of_windowCount_2() { return static_cast<int32_t>(offsetof(ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A, ___windowCount_2)); }
	inline int32_t get_windowCount_2() const { return ___windowCount_2; }
	inline int32_t* get_address_of_windowCount_2() { return &___windowCount_2; }
	inline void set_windowCount_2(int32_t value)
	{
		___windowCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVERSEDWINDOWGENERATOR_T171F7229177F9C5899E1B955817CFD447956572A_H
#ifndef SP800SECURERANDOMBUILDER_T82710C7D53A63D36DDD25EB172AE96BF0DCB594C_H
#define SP800SECURERANDOMBUILDER_T82710C7D53A63D36DDD25EB172AE96BF0DCB594C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder
struct  SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder::mRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mRandom_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySourceProvider BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder::mEntropySourceProvider
	RuntimeObject* ___mEntropySourceProvider_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder::mPersonalizationString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPersonalizationString_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder::mSecurityStrength
	int32_t ___mSecurityStrength_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder::mEntropyBitsRequired
	int32_t ___mEntropyBitsRequired_4;

public:
	inline static int32_t get_offset_of_mRandom_0() { return static_cast<int32_t>(offsetof(SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C, ___mRandom_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mRandom_0() const { return ___mRandom_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mRandom_0() { return &___mRandom_0; }
	inline void set_mRandom_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mRandom_0 = value;
		Il2CppCodeGenWriteBarrier((&___mRandom_0), value);
	}

	inline static int32_t get_offset_of_mEntropySourceProvider_1() { return static_cast<int32_t>(offsetof(SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C, ___mEntropySourceProvider_1)); }
	inline RuntimeObject* get_mEntropySourceProvider_1() const { return ___mEntropySourceProvider_1; }
	inline RuntimeObject** get_address_of_mEntropySourceProvider_1() { return &___mEntropySourceProvider_1; }
	inline void set_mEntropySourceProvider_1(RuntimeObject* value)
	{
		___mEntropySourceProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySourceProvider_1), value);
	}

	inline static int32_t get_offset_of_mPersonalizationString_2() { return static_cast<int32_t>(offsetof(SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C, ___mPersonalizationString_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPersonalizationString_2() const { return ___mPersonalizationString_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPersonalizationString_2() { return &___mPersonalizationString_2; }
	inline void set_mPersonalizationString_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPersonalizationString_2 = value;
		Il2CppCodeGenWriteBarrier((&___mPersonalizationString_2), value);
	}

	inline static int32_t get_offset_of_mSecurityStrength_3() { return static_cast<int32_t>(offsetof(SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C, ___mSecurityStrength_3)); }
	inline int32_t get_mSecurityStrength_3() const { return ___mSecurityStrength_3; }
	inline int32_t* get_address_of_mSecurityStrength_3() { return &___mSecurityStrength_3; }
	inline void set_mSecurityStrength_3(int32_t value)
	{
		___mSecurityStrength_3 = value;
	}

	inline static int32_t get_offset_of_mEntropyBitsRequired_4() { return static_cast<int32_t>(offsetof(SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C, ___mEntropyBitsRequired_4)); }
	inline int32_t get_mEntropyBitsRequired_4() const { return ___mEntropyBitsRequired_4; }
	inline int32_t* get_address_of_mEntropyBitsRequired_4() { return &___mEntropyBitsRequired_4; }
	inline void set_mEntropyBitsRequired_4(int32_t value)
	{
		___mEntropyBitsRequired_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SP800SECURERANDOMBUILDER_T82710C7D53A63D36DDD25EB172AE96BF0DCB594C_H
#ifndef CTRDRBGPROVIDER_TAE3C31F9AD934C9071870463769945B88201E80F_H
#define CTRDRBGPROVIDER_TAE3C31F9AD934C9071870463769945B88201E80F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_CtrDrbgProvider
struct  CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_CtrDrbgProvider::mBlockCipher
	RuntimeObject* ___mBlockCipher_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_CtrDrbgProvider::mKeySizeInBits
	int32_t ___mKeySizeInBits_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_CtrDrbgProvider::mNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mNonce_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_CtrDrbgProvider::mPersonalizationString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPersonalizationString_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_CtrDrbgProvider::mSecurityStrength
	int32_t ___mSecurityStrength_4;

public:
	inline static int32_t get_offset_of_mBlockCipher_0() { return static_cast<int32_t>(offsetof(CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F, ___mBlockCipher_0)); }
	inline RuntimeObject* get_mBlockCipher_0() const { return ___mBlockCipher_0; }
	inline RuntimeObject** get_address_of_mBlockCipher_0() { return &___mBlockCipher_0; }
	inline void set_mBlockCipher_0(RuntimeObject* value)
	{
		___mBlockCipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBlockCipher_0), value);
	}

	inline static int32_t get_offset_of_mKeySizeInBits_1() { return static_cast<int32_t>(offsetof(CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F, ___mKeySizeInBits_1)); }
	inline int32_t get_mKeySizeInBits_1() const { return ___mKeySizeInBits_1; }
	inline int32_t* get_address_of_mKeySizeInBits_1() { return &___mKeySizeInBits_1; }
	inline void set_mKeySizeInBits_1(int32_t value)
	{
		___mKeySizeInBits_1 = value;
	}

	inline static int32_t get_offset_of_mNonce_2() { return static_cast<int32_t>(offsetof(CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F, ___mNonce_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mNonce_2() const { return ___mNonce_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mNonce_2() { return &___mNonce_2; }
	inline void set_mNonce_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mNonce_2 = value;
		Il2CppCodeGenWriteBarrier((&___mNonce_2), value);
	}

	inline static int32_t get_offset_of_mPersonalizationString_3() { return static_cast<int32_t>(offsetof(CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F, ___mPersonalizationString_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPersonalizationString_3() const { return ___mPersonalizationString_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPersonalizationString_3() { return &___mPersonalizationString_3; }
	inline void set_mPersonalizationString_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPersonalizationString_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPersonalizationString_3), value);
	}

	inline static int32_t get_offset_of_mSecurityStrength_4() { return static_cast<int32_t>(offsetof(CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F, ___mSecurityStrength_4)); }
	inline int32_t get_mSecurityStrength_4() const { return ___mSecurityStrength_4; }
	inline int32_t* get_address_of_mSecurityStrength_4() { return &___mSecurityStrength_4; }
	inline void set_mSecurityStrength_4(int32_t value)
	{
		___mSecurityStrength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTRDRBGPROVIDER_TAE3C31F9AD934C9071870463769945B88201E80F_H
#ifndef HMACDRBGPROVIDER_T98868B7971D2965585C3CBBA63482D24005D7EC7_H
#define HMACDRBGPROVIDER_T98868B7971D2965585C3CBBA63482D24005D7EC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HMacDrbgProvider
struct  HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HMacDrbgProvider::mHMac
	RuntimeObject* ___mHMac_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HMacDrbgProvider::mNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mNonce_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HMacDrbgProvider::mPersonalizationString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPersonalizationString_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HMacDrbgProvider::mSecurityStrength
	int32_t ___mSecurityStrength_3;

public:
	inline static int32_t get_offset_of_mHMac_0() { return static_cast<int32_t>(offsetof(HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7, ___mHMac_0)); }
	inline RuntimeObject* get_mHMac_0() const { return ___mHMac_0; }
	inline RuntimeObject** get_address_of_mHMac_0() { return &___mHMac_0; }
	inline void set_mHMac_0(RuntimeObject* value)
	{
		___mHMac_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHMac_0), value);
	}

	inline static int32_t get_offset_of_mNonce_1() { return static_cast<int32_t>(offsetof(HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7, ___mNonce_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mNonce_1() const { return ___mNonce_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mNonce_1() { return &___mNonce_1; }
	inline void set_mNonce_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mNonce_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonce_1), value);
	}

	inline static int32_t get_offset_of_mPersonalizationString_2() { return static_cast<int32_t>(offsetof(HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7, ___mPersonalizationString_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPersonalizationString_2() const { return ___mPersonalizationString_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPersonalizationString_2() { return &___mPersonalizationString_2; }
	inline void set_mPersonalizationString_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPersonalizationString_2 = value;
		Il2CppCodeGenWriteBarrier((&___mPersonalizationString_2), value);
	}

	inline static int32_t get_offset_of_mSecurityStrength_3() { return static_cast<int32_t>(offsetof(HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7, ___mSecurityStrength_3)); }
	inline int32_t get_mSecurityStrength_3() const { return ___mSecurityStrength_3; }
	inline int32_t* get_address_of_mSecurityStrength_3() { return &___mSecurityStrength_3; }
	inline void set_mSecurityStrength_3(int32_t value)
	{
		___mSecurityStrength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMACDRBGPROVIDER_T98868B7971D2965585C3CBBA63482D24005D7EC7_H
#ifndef HASHDRBGPROVIDER_TA03C959D27F2639D6D1C7B37E7D700547D406095_H
#define HASHDRBGPROVIDER_TA03C959D27F2639D6D1C7B37E7D700547D406095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HashDrbgProvider
struct  HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HashDrbgProvider::mDigest
	RuntimeObject* ___mDigest_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HashDrbgProvider::mNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mNonce_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HashDrbgProvider::mPersonalizationString
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPersonalizationString_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandomBuilder_HashDrbgProvider::mSecurityStrength
	int32_t ___mSecurityStrength_3;

public:
	inline static int32_t get_offset_of_mDigest_0() { return static_cast<int32_t>(offsetof(HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095, ___mDigest_0)); }
	inline RuntimeObject* get_mDigest_0() const { return ___mDigest_0; }
	inline RuntimeObject** get_address_of_mDigest_0() { return &___mDigest_0; }
	inline void set_mDigest_0(RuntimeObject* value)
	{
		___mDigest_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_0), value);
	}

	inline static int32_t get_offset_of_mNonce_1() { return static_cast<int32_t>(offsetof(HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095, ___mNonce_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mNonce_1() const { return ___mNonce_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mNonce_1() { return &___mNonce_1; }
	inline void set_mNonce_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mNonce_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonce_1), value);
	}

	inline static int32_t get_offset_of_mPersonalizationString_2() { return static_cast<int32_t>(offsetof(HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095, ___mPersonalizationString_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPersonalizationString_2() const { return ___mPersonalizationString_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPersonalizationString_2() { return &___mPersonalizationString_2; }
	inline void set_mPersonalizationString_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPersonalizationString_2 = value;
		Il2CppCodeGenWriteBarrier((&___mPersonalizationString_2), value);
	}

	inline static int32_t get_offset_of_mSecurityStrength_3() { return static_cast<int32_t>(offsetof(HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095, ___mSecurityStrength_3)); }
	inline int32_t get_mSecurityStrength_3() const { return ___mSecurityStrength_3; }
	inline int32_t* get_address_of_mSecurityStrength_3() { return &___mSecurityStrength_3; }
	inline void set_mSecurityStrength_3(int32_t value)
	{
		___mSecurityStrength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHDRBGPROVIDER_TA03C959D27F2639D6D1C7B37E7D700547D406095_H
#ifndef THREADEDSEEDGENERATOR_TB3B4E6EB56967BAB9BCC1741D6CDC72444C4B23C_H
#define THREADEDSEEDGENERATOR_TB3B4E6EB56967BAB9BCC1741D6CDC72444C4B23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ThreadedSeedGenerator
struct  ThreadedSeedGenerator_tB3B4E6EB56967BAB9BCC1741D6CDC72444C4B23C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADEDSEEDGENERATOR_TB3B4E6EB56967BAB9BCC1741D6CDC72444C4B23C_H
#ifndef VMPCRANDOMGENERATOR_T056DE51A28372AD537C06D583BCC0022FB8EDD71_H
#define VMPCRANDOMGENERATOR_T056DE51A28372AD537C06D583BCC0022FB8EDD71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.VmpcRandomGenerator
struct  VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.VmpcRandomGenerator::n
	uint8_t ___n_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.VmpcRandomGenerator::P
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___P_1;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.VmpcRandomGenerator::s
	uint8_t ___s_2;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71, ___n_0)); }
	inline uint8_t get_n_0() const { return ___n_0; }
	inline uint8_t* get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(uint8_t value)
	{
		___n_0 = value;
	}

	inline static int32_t get_offset_of_P_1() { return static_cast<int32_t>(offsetof(VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71, ___P_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_P_1() const { return ___P_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_P_1() { return &___P_1; }
	inline void set_P_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___P_1 = value;
		Il2CppCodeGenWriteBarrier((&___P_1), value);
	}

	inline static int32_t get_offset_of_s_2() { return static_cast<int32_t>(offsetof(VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71, ___s_2)); }
	inline uint8_t get_s_2() const { return ___s_2; }
	inline uint8_t* get_address_of_s_2() { return &___s_2; }
	inline void set_s_2(uint8_t value)
	{
		___s_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VMPCRANDOMGENERATOR_T056DE51A28372AD537C06D583BCC0022FB8EDD71_H
#ifndef X931RNG_TED3929E965136732FC0739FA158783E3BED94A4B_H
#define X931RNG_TED3929E965136732FC0739FA158783E3BED94A4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng
struct  X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mEngine
	RuntimeObject* ___mEngine_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySource BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mEntropySource
	RuntimeObject* ___mEntropySource_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mDT
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mDT_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mI
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mI_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mR
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mR_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mV_9;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng::mReseedCounter
	int64_t ___mReseedCounter_10;

public:
	inline static int32_t get_offset_of_mEngine_4() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mEngine_4)); }
	inline RuntimeObject* get_mEngine_4() const { return ___mEngine_4; }
	inline RuntimeObject** get_address_of_mEngine_4() { return &___mEngine_4; }
	inline void set_mEngine_4(RuntimeObject* value)
	{
		___mEngine_4 = value;
		Il2CppCodeGenWriteBarrier((&___mEngine_4), value);
	}

	inline static int32_t get_offset_of_mEntropySource_5() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mEntropySource_5)); }
	inline RuntimeObject* get_mEntropySource_5() const { return ___mEntropySource_5; }
	inline RuntimeObject** get_address_of_mEntropySource_5() { return &___mEntropySource_5; }
	inline void set_mEntropySource_5(RuntimeObject* value)
	{
		___mEntropySource_5 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySource_5), value);
	}

	inline static int32_t get_offset_of_mDT_6() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mDT_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mDT_6() const { return ___mDT_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mDT_6() { return &___mDT_6; }
	inline void set_mDT_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mDT_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDT_6), value);
	}

	inline static int32_t get_offset_of_mI_7() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mI_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mI_7() const { return ___mI_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mI_7() { return &___mI_7; }
	inline void set_mI_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mI_7 = value;
		Il2CppCodeGenWriteBarrier((&___mI_7), value);
	}

	inline static int32_t get_offset_of_mR_8() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mR_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mR_8() const { return ___mR_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mR_8() { return &___mR_8; }
	inline void set_mR_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mR_8 = value;
		Il2CppCodeGenWriteBarrier((&___mR_8), value);
	}

	inline static int32_t get_offset_of_mV_9() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mV_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mV_9() const { return ___mV_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mV_9() { return &___mV_9; }
	inline void set_mV_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mV_9 = value;
		Il2CppCodeGenWriteBarrier((&___mV_9), value);
	}

	inline static int32_t get_offset_of_mReseedCounter_10() { return static_cast<int32_t>(offsetof(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B, ___mReseedCounter_10)); }
	inline int64_t get_mReseedCounter_10() const { return ___mReseedCounter_10; }
	inline int64_t* get_address_of_mReseedCounter_10() { return &___mReseedCounter_10; }
	inline void set_mReseedCounter_10(int64_t value)
	{
		___mReseedCounter_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X931RNG_TED3929E965136732FC0739FA158783E3BED94A4B_H
#ifndef X931SECURERANDOMBUILDER_T91A1A537E20D0E756B998B49C3A5F50264624E76_H
#define X931SECURERANDOMBUILDER_T91A1A537E20D0E756B998B49C3A5F50264624E76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandomBuilder
struct  X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandomBuilder::mRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mRandom_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySourceProvider BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandomBuilder::mEntropySourceProvider
	RuntimeObject* ___mEntropySourceProvider_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandomBuilder::mDateTimeVector
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mDateTimeVector_2;

public:
	inline static int32_t get_offset_of_mRandom_0() { return static_cast<int32_t>(offsetof(X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76, ___mRandom_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mRandom_0() const { return ___mRandom_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mRandom_0() { return &___mRandom_0; }
	inline void set_mRandom_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mRandom_0 = value;
		Il2CppCodeGenWriteBarrier((&___mRandom_0), value);
	}

	inline static int32_t get_offset_of_mEntropySourceProvider_1() { return static_cast<int32_t>(offsetof(X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76, ___mEntropySourceProvider_1)); }
	inline RuntimeObject* get_mEntropySourceProvider_1() const { return ___mEntropySourceProvider_1; }
	inline RuntimeObject** get_address_of_mEntropySourceProvider_1() { return &___mEntropySourceProvider_1; }
	inline void set_mEntropySourceProvider_1(RuntimeObject* value)
	{
		___mEntropySourceProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySourceProvider_1), value);
	}

	inline static int32_t get_offset_of_mDateTimeVector_2() { return static_cast<int32_t>(offsetof(X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76, ___mDateTimeVector_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mDateTimeVector_2() const { return ___mDateTimeVector_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mDateTimeVector_2() { return &___mDateTimeVector_2; }
	inline void set_mDateTimeVector_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mDateTimeVector_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDateTimeVector_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X931SECURERANDOMBUILDER_T91A1A537E20D0E756B998B49C3A5F50264624E76_H
#ifndef DSADIGESTSIGNER_T7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23_H
#define DSADIGESTSIGNER_T7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaDigestSigner
struct  DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDsa BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::dsa
	RuntimeObject* ___dsa_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::digest
	RuntimeObject* ___digest_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaEncoding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::encoding
	RuntimeObject* ___encoding_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::forSigning
	bool ___forSigning_3;

public:
	inline static int32_t get_offset_of_dsa_0() { return static_cast<int32_t>(offsetof(DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23, ___dsa_0)); }
	inline RuntimeObject* get_dsa_0() const { return ___dsa_0; }
	inline RuntimeObject** get_address_of_dsa_0() { return &___dsa_0; }
	inline void set_dsa_0(RuntimeObject* value)
	{
		___dsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___dsa_0), value);
	}

	inline static int32_t get_offset_of_digest_1() { return static_cast<int32_t>(offsetof(DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23, ___digest_1)); }
	inline RuntimeObject* get_digest_1() const { return ___digest_1; }
	inline RuntimeObject** get_address_of_digest_1() { return &___digest_1; }
	inline void set_digest_1(RuntimeObject* value)
	{
		___digest_1 = value;
		Il2CppCodeGenWriteBarrier((&___digest_1), value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23, ___encoding_2)); }
	inline RuntimeObject* get_encoding_2() const { return ___encoding_2; }
	inline RuntimeObject** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(RuntimeObject* value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_forSigning_3() { return static_cast<int32_t>(offsetof(DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23, ___forSigning_3)); }
	inline bool get_forSigning_3() const { return ___forSigning_3; }
	inline bool* get_address_of_forSigning_3() { return &___forSigning_3; }
	inline void set_forSigning_3(bool value)
	{
		___forSigning_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSADIGESTSIGNER_T7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23_H
#ifndef DSASIGNER_TFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C_H
#define DSASIGNER_TFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaSigner
struct  DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaKCalculator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaSigner::kCalculator
	RuntimeObject* ___kCalculator_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaSigner::key
	DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043 * ___key_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.DsaSigner::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_2;

public:
	inline static int32_t get_offset_of_kCalculator_0() { return static_cast<int32_t>(offsetof(DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C, ___kCalculator_0)); }
	inline RuntimeObject* get_kCalculator_0() const { return ___kCalculator_0; }
	inline RuntimeObject** get_address_of_kCalculator_0() { return &___kCalculator_0; }
	inline void set_kCalculator_0(RuntimeObject* value)
	{
		___kCalculator_0 = value;
		Il2CppCodeGenWriteBarrier((&___kCalculator_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C, ___key_1)); }
	inline DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043 * get_key_1() const { return ___key_1; }
	inline DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C, ___random_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSASIGNER_TFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C_H
#ifndef ECDSASIGNER_TD66C53659D6B01F2E457F569C0D1139A2E161EEA_H
#define ECDSASIGNER_TD66C53659D6B01F2E457F569C0D1139A2E161EEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECDsaSigner
struct  ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaKCalculator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECDsaSigner::kCalculator
	RuntimeObject* ___kCalculator_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECDsaSigner::key
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * ___key_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECDsaSigner::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;

public:
	inline static int32_t get_offset_of_kCalculator_1() { return static_cast<int32_t>(offsetof(ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA, ___kCalculator_1)); }
	inline RuntimeObject* get_kCalculator_1() const { return ___kCalculator_1; }
	inline RuntimeObject** get_address_of_kCalculator_1() { return &___kCalculator_1; }
	inline void set_kCalculator_1(RuntimeObject* value)
	{
		___kCalculator_1 = value;
		Il2CppCodeGenWriteBarrier((&___kCalculator_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA, ___key_2)); }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * get_key_2() const { return ___key_2; }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

struct ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECDsaSigner::Eight
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Eight_0;

public:
	inline static int32_t get_offset_of_Eight_0() { return static_cast<int32_t>(offsetof(ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA_StaticFields, ___Eight_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Eight_0() const { return ___Eight_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Eight_0() { return &___Eight_0; }
	inline void set_Eight_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Eight_0 = value;
		Il2CppCodeGenWriteBarrier((&___Eight_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDSASIGNER_TD66C53659D6B01F2E457F569C0D1139A2E161EEA_H
#ifndef ECGOST3410SIGNER_T69F05BC41165FE707A05D2C7FE09601F39E1B005_H
#define ECGOST3410SIGNER_T69F05BC41165FE707A05D2C7FE09601F39E1B005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECGost3410Signer
struct  ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECGost3410Signer::key
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * ___key_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECGost3410Signer::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECGost3410Signer::forSigning
	bool ___forSigning_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005, ___key_0)); }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * get_key_0() const { return ___key_0; }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005, ___random_1)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECGOST3410SIGNER_T69F05BC41165FE707A05D2C7FE09601F39E1B005_H
#ifndef ECNRSIGNER_T23A29B78C8AF0A868C1B306126BCBABF9AE11AA9_H
#define ECNRSIGNER_T23A29B78C8AF0A868C1B306126BCBABF9AE11AA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECNRSigner
struct  ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECNRSigner::forSigning
	bool ___forSigning_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECNRSigner::key
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * ___key_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.ECNRSigner::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_2;

public:
	inline static int32_t get_offset_of_forSigning_0() { return static_cast<int32_t>(offsetof(ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9, ___forSigning_0)); }
	inline bool get_forSigning_0() const { return ___forSigning_0; }
	inline bool* get_address_of_forSigning_0() { return &___forSigning_0; }
	inline void set_forSigning_0(bool value)
	{
		___forSigning_0 = value;
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9, ___key_1)); }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * get_key_1() const { return ___key_1; }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9, ___random_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECNRSIGNER_T23A29B78C8AF0A868C1B306126BCBABF9AE11AA9_H
#ifndef ED25519SIGNER_TB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640_H
#define ED25519SIGNER_TB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer
struct  Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer_Buffer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer::buffer
	Buffer_t443824CE041E422156B9D23C39538DBF4867327C * ___buffer_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer::forSigning
	bool ___forSigning_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer::privateKey
	Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * ___privateKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer::publicKey
	Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * ___publicKey_3;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640, ___buffer_0)); }
	inline Buffer_t443824CE041E422156B9D23C39538DBF4867327C * get_buffer_0() const { return ___buffer_0; }
	inline Buffer_t443824CE041E422156B9D23C39538DBF4867327C ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(Buffer_t443824CE041E422156B9D23C39538DBF4867327C * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_forSigning_1() { return static_cast<int32_t>(offsetof(Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640, ___forSigning_1)); }
	inline bool get_forSigning_1() const { return ___forSigning_1; }
	inline bool* get_address_of_forSigning_1() { return &___forSigning_1; }
	inline void set_forSigning_1(bool value)
	{
		___forSigning_1 = value;
	}

	inline static int32_t get_offset_of_privateKey_2() { return static_cast<int32_t>(offsetof(Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640, ___privateKey_2)); }
	inline Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * get_privateKey_2() const { return ___privateKey_2; }
	inline Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 ** get_address_of_privateKey_2() { return &___privateKey_2; }
	inline void set_privateKey_2(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * value)
	{
		___privateKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_2), value);
	}

	inline static int32_t get_offset_of_publicKey_3() { return static_cast<int32_t>(offsetof(Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640, ___publicKey_3)); }
	inline Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * get_publicKey_3() const { return ___publicKey_3; }
	inline Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 ** get_address_of_publicKey_3() { return &___publicKey_3; }
	inline void set_publicKey_3(Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * value)
	{
		___publicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519SIGNER_TB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640_H
#ifndef ED25519CTXSIGNER_T2CF0B6495F0BA6DB63D84CA556C7965966BC0359_H
#define ED25519CTXSIGNER_T2CF0B6495F0BA6DB63D84CA556C7965966BC0359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner
struct  Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner_Buffer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner::buffer
	Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86 * ___buffer_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner::context
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___context_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner::forSigning
	bool ___forSigning_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner::privateKey
	Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * ___privateKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner::publicKey
	Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * ___publicKey_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359, ___buffer_0)); }
	inline Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86 * get_buffer_0() const { return ___buffer_0; }
	inline Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86 ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86 * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359, ___context_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_context_1() const { return ___context_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}

	inline static int32_t get_offset_of_privateKey_3() { return static_cast<int32_t>(offsetof(Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359, ___privateKey_3)); }
	inline Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * get_privateKey_3() const { return ___privateKey_3; }
	inline Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 ** get_address_of_privateKey_3() { return &___privateKey_3; }
	inline void set_privateKey_3(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * value)
	{
		___privateKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_3), value);
	}

	inline static int32_t get_offset_of_publicKey_4() { return static_cast<int32_t>(offsetof(Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359, ___publicKey_4)); }
	inline Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * get_publicKey_4() const { return ___publicKey_4; }
	inline Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 ** get_address_of_publicKey_4() { return &___publicKey_4; }
	inline void set_publicKey_4(Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * value)
	{
		___publicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519CTXSIGNER_T2CF0B6495F0BA6DB63D84CA556C7965966BC0359_H
#ifndef ED25519PHSIGNER_TD20CA75D7CD956BA254DC2D965087A9B4F192B6D_H
#define ED25519PHSIGNER_TD20CA75D7CD956BA254DC2D965087A9B4F192B6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519phSigner
struct  Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519phSigner::prehash
	RuntimeObject* ___prehash_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519phSigner::context
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___context_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519phSigner::forSigning
	bool ___forSigning_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519phSigner::privateKey
	Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * ___privateKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519phSigner::publicKey
	Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * ___publicKey_4;

public:
	inline static int32_t get_offset_of_prehash_0() { return static_cast<int32_t>(offsetof(Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D, ___prehash_0)); }
	inline RuntimeObject* get_prehash_0() const { return ___prehash_0; }
	inline RuntimeObject** get_address_of_prehash_0() { return &___prehash_0; }
	inline void set_prehash_0(RuntimeObject* value)
	{
		___prehash_0 = value;
		Il2CppCodeGenWriteBarrier((&___prehash_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D, ___context_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_context_1() const { return ___context_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}

	inline static int32_t get_offset_of_privateKey_3() { return static_cast<int32_t>(offsetof(Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D, ___privateKey_3)); }
	inline Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * get_privateKey_3() const { return ___privateKey_3; }
	inline Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 ** get_address_of_privateKey_3() { return &___privateKey_3; }
	inline void set_privateKey_3(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2 * value)
	{
		___privateKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_3), value);
	}

	inline static int32_t get_offset_of_publicKey_4() { return static_cast<int32_t>(offsetof(Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D, ___publicKey_4)); }
	inline Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * get_publicKey_4() const { return ___publicKey_4; }
	inline Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 ** get_address_of_publicKey_4() { return &___publicKey_4; }
	inline void set_publicKey_4(Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3 * value)
	{
		___publicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519PHSIGNER_TD20CA75D7CD956BA254DC2D965087A9B4F192B6D_H
#ifndef ED448SIGNER_TBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055_H
#define ED448SIGNER_TBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer
struct  Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer_Buffer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer::buffer
	Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542 * ___buffer_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer::context
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___context_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer::forSigning
	bool ___forSigning_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer::privateKey
	Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D * ___privateKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer::publicKey
	Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B * ___publicKey_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055, ___buffer_0)); }
	inline Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542 * get_buffer_0() const { return ___buffer_0; }
	inline Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542 ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542 * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055, ___context_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_context_1() const { return ___context_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}

	inline static int32_t get_offset_of_privateKey_3() { return static_cast<int32_t>(offsetof(Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055, ___privateKey_3)); }
	inline Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D * get_privateKey_3() const { return ___privateKey_3; }
	inline Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D ** get_address_of_privateKey_3() { return &___privateKey_3; }
	inline void set_privateKey_3(Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D * value)
	{
		___privateKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_3), value);
	}

	inline static int32_t get_offset_of_publicKey_4() { return static_cast<int32_t>(offsetof(Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055, ___publicKey_4)); }
	inline Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B * get_publicKey_4() const { return ___publicKey_4; }
	inline Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B ** get_address_of_publicKey_4() { return &___publicKey_4; }
	inline void set_publicKey_4(Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B * value)
	{
		___publicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448SIGNER_TBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055_H
#ifndef ED448PHSIGNER_TCFFAE46AC32649A773D0488EF787B23D9BB88004_H
#define ED448PHSIGNER_TCFFAE46AC32649A773D0488EF787B23D9BB88004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448phSigner
struct  Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IXof BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448phSigner::prehash
	RuntimeObject* ___prehash_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448phSigner::context
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___context_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448phSigner::forSigning
	bool ___forSigning_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448phSigner::privateKey
	Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D * ___privateKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448phSigner::publicKey
	Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B * ___publicKey_4;

public:
	inline static int32_t get_offset_of_prehash_0() { return static_cast<int32_t>(offsetof(Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004, ___prehash_0)); }
	inline RuntimeObject* get_prehash_0() const { return ___prehash_0; }
	inline RuntimeObject** get_address_of_prehash_0() { return &___prehash_0; }
	inline void set_prehash_0(RuntimeObject* value)
	{
		___prehash_0 = value;
		Il2CppCodeGenWriteBarrier((&___prehash_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004, ___context_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_context_1() const { return ___context_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}

	inline static int32_t get_offset_of_privateKey_3() { return static_cast<int32_t>(offsetof(Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004, ___privateKey_3)); }
	inline Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D * get_privateKey_3() const { return ___privateKey_3; }
	inline Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D ** get_address_of_privateKey_3() { return &___privateKey_3; }
	inline void set_privateKey_3(Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D * value)
	{
		___privateKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_3), value);
	}

	inline static int32_t get_offset_of_publicKey_4() { return static_cast<int32_t>(offsetof(Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004, ___publicKey_4)); }
	inline Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B * get_publicKey_4() const { return ___publicKey_4; }
	inline Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B ** get_address_of_publicKey_4() { return &___publicKey_4; }
	inline void set_publicKey_4(Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B * value)
	{
		___publicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448PHSIGNER_TCFFAE46AC32649A773D0488EF787B23D9BB88004_H
#ifndef GENERICSIGNER_T68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557_H
#define GENERICSIGNER_T68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.GenericSigner
struct  GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.GenericSigner::engine
	RuntimeObject* ___engine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.GenericSigner::digest
	RuntimeObject* ___digest_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.GenericSigner::forSigning
	bool ___forSigning_2;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557, ___engine_0)); }
	inline RuntimeObject* get_engine_0() const { return ___engine_0; }
	inline RuntimeObject** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(RuntimeObject* value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_digest_1() { return static_cast<int32_t>(offsetof(GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557, ___digest_1)); }
	inline RuntimeObject* get_digest_1() const { return ___digest_1; }
	inline RuntimeObject** get_address_of_digest_1() { return &___digest_1; }
	inline void set_digest_1(RuntimeObject* value)
	{
		___digest_1 = value;
		Il2CppCodeGenWriteBarrier((&___digest_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICSIGNER_T68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557_H
#ifndef GOST3410DIGESTSIGNER_TBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5_H
#define GOST3410DIGESTSIGNER_TBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410DigestSigner
struct  Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410DigestSigner::digest
	RuntimeObject* ___digest_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDsa BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410DigestSigner::dsaSigner
	RuntimeObject* ___dsaSigner_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410DigestSigner::size
	int32_t ___size_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410DigestSigner::halfSize
	int32_t ___halfSize_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410DigestSigner::forSigning
	bool ___forSigning_4;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5, ___digest_0)); }
	inline RuntimeObject* get_digest_0() const { return ___digest_0; }
	inline RuntimeObject** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(RuntimeObject* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_dsaSigner_1() { return static_cast<int32_t>(offsetof(Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5, ___dsaSigner_1)); }
	inline RuntimeObject* get_dsaSigner_1() const { return ___dsaSigner_1; }
	inline RuntimeObject** get_address_of_dsaSigner_1() { return &___dsaSigner_1; }
	inline void set_dsaSigner_1(RuntimeObject* value)
	{
		___dsaSigner_1 = value;
		Il2CppCodeGenWriteBarrier((&___dsaSigner_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_halfSize_3() { return static_cast<int32_t>(offsetof(Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5, ___halfSize_3)); }
	inline int32_t get_halfSize_3() const { return ___halfSize_3; }
	inline int32_t* get_address_of_halfSize_3() { return &___halfSize_3; }
	inline void set_halfSize_3(int32_t value)
	{
		___halfSize_3 = value;
	}

	inline static int32_t get_offset_of_forSigning_4() { return static_cast<int32_t>(offsetof(Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5, ___forSigning_4)); }
	inline bool get_forSigning_4() const { return ___forSigning_4; }
	inline bool* get_address_of_forSigning_4() { return &___forSigning_4; }
	inline void set_forSigning_4(bool value)
	{
		___forSigning_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410DIGESTSIGNER_TBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5_H
#ifndef GOST3410SIGNER_T057B6F369C8D89E26A37EC552CA812D7DD2A7E85_H
#define GOST3410SIGNER_T057B6F369C8D89E26A37EC552CA812D7DD2A7E85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410Signer
struct  Gost3410Signer_t057B6F369C8D89E26A37EC552CA812D7DD2A7E85  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410Signer::key
	Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186 * ___key_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Gost3410Signer::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(Gost3410Signer_t057B6F369C8D89E26A37EC552CA812D7DD2A7E85, ___key_0)); }
	inline Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186 * get_key_0() const { return ___key_0; }
	inline Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(Gost3410Signer_t057B6F369C8D89E26A37EC552CA812D7DD2A7E85, ___random_1)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410SIGNER_T057B6F369C8D89E26A37EC552CA812D7DD2A7E85_H
#ifndef HMACDSAKCALCULATOR_TC8CBD55698CA77683BC867F9645F2D831EDA7425_H
#define HMACDSAKCALCULATOR_TC8CBD55698CA77683BC867F9645F2D831EDA7425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator
struct  HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Macs.HMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::hMac
	HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 * ___hMac_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::K
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___K_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::V
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___V_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::n
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___n_3;

public:
	inline static int32_t get_offset_of_hMac_0() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425, ___hMac_0)); }
	inline HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 * get_hMac_0() const { return ___hMac_0; }
	inline HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 ** get_address_of_hMac_0() { return &___hMac_0; }
	inline void set_hMac_0(HMac_t7ED75AC4D2CE80BFEF6AE133869205F57E378045 * value)
	{
		___hMac_0 = value;
		Il2CppCodeGenWriteBarrier((&___hMac_0), value);
	}

	inline static int32_t get_offset_of_K_1() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425, ___K_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_K_1() const { return ___K_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_K_1() { return &___K_1; }
	inline void set_K_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___K_1 = value;
		Il2CppCodeGenWriteBarrier((&___K_1), value);
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425, ___V_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_V_2() const { return ___V_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___V_2 = value;
		Il2CppCodeGenWriteBarrier((&___V_2), value);
	}

	inline static int32_t get_offset_of_n_3() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425, ___n_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_n_3() const { return ___n_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_n_3() { return &___n_3; }
	inline void set_n_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___n_3 = value;
		Il2CppCodeGenWriteBarrier((&___n_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMACDSAKCALCULATOR_TC8CBD55698CA77683BC867F9645F2D831EDA7425_H
#ifndef ISO9796D2PSSSIGNER_T5133D138584C8EAAB8C689A8A85609EAADEBC698_H
#define ISO9796D2PSSSIGNER_T5133D138584C8EAAB8C689A8A85609EAADEBC698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner
struct  Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::digest
	RuntimeObject* ___digest_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::cipher
	RuntimeObject* ___cipher_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::standardSalt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___standardSalt_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::hLen
	int32_t ___hLen_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::trailer
	int32_t ___trailer_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::keyBits
	int32_t ___keyBits_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::block
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::mBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mBuf_16;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::messageLength
	int32_t ___messageLength_17;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::saltLength
	int32_t ___saltLength_18;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::fullMessage
	bool ___fullMessage_19;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::recoveredMessage
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___recoveredMessage_20;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::preSig
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___preSig_21;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::preBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___preBlock_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::preMStart
	int32_t ___preMStart_23;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2PssSigner::preTLength
	int32_t ___preTLength_24;

public:
	inline static int32_t get_offset_of_digest_8() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___digest_8)); }
	inline RuntimeObject* get_digest_8() const { return ___digest_8; }
	inline RuntimeObject** get_address_of_digest_8() { return &___digest_8; }
	inline void set_digest_8(RuntimeObject* value)
	{
		___digest_8 = value;
		Il2CppCodeGenWriteBarrier((&___digest_8), value);
	}

	inline static int32_t get_offset_of_cipher_9() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___cipher_9)); }
	inline RuntimeObject* get_cipher_9() const { return ___cipher_9; }
	inline RuntimeObject** get_address_of_cipher_9() { return &___cipher_9; }
	inline void set_cipher_9(RuntimeObject* value)
	{
		___cipher_9 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_9), value);
	}

	inline static int32_t get_offset_of_random_10() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___random_10)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_10() const { return ___random_10; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_10() { return &___random_10; }
	inline void set_random_10(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_10 = value;
		Il2CppCodeGenWriteBarrier((&___random_10), value);
	}

	inline static int32_t get_offset_of_standardSalt_11() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___standardSalt_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_standardSalt_11() const { return ___standardSalt_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_standardSalt_11() { return &___standardSalt_11; }
	inline void set_standardSalt_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___standardSalt_11 = value;
		Il2CppCodeGenWriteBarrier((&___standardSalt_11), value);
	}

	inline static int32_t get_offset_of_hLen_12() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___hLen_12)); }
	inline int32_t get_hLen_12() const { return ___hLen_12; }
	inline int32_t* get_address_of_hLen_12() { return &___hLen_12; }
	inline void set_hLen_12(int32_t value)
	{
		___hLen_12 = value;
	}

	inline static int32_t get_offset_of_trailer_13() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___trailer_13)); }
	inline int32_t get_trailer_13() const { return ___trailer_13; }
	inline int32_t* get_address_of_trailer_13() { return &___trailer_13; }
	inline void set_trailer_13(int32_t value)
	{
		___trailer_13 = value;
	}

	inline static int32_t get_offset_of_keyBits_14() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___keyBits_14)); }
	inline int32_t get_keyBits_14() const { return ___keyBits_14; }
	inline int32_t* get_address_of_keyBits_14() { return &___keyBits_14; }
	inline void set_keyBits_14(int32_t value)
	{
		___keyBits_14 = value;
	}

	inline static int32_t get_offset_of_block_15() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___block_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_block_15() const { return ___block_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_block_15() { return &___block_15; }
	inline void set_block_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___block_15 = value;
		Il2CppCodeGenWriteBarrier((&___block_15), value);
	}

	inline static int32_t get_offset_of_mBuf_16() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___mBuf_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mBuf_16() const { return ___mBuf_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mBuf_16() { return &___mBuf_16; }
	inline void set_mBuf_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mBuf_16 = value;
		Il2CppCodeGenWriteBarrier((&___mBuf_16), value);
	}

	inline static int32_t get_offset_of_messageLength_17() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___messageLength_17)); }
	inline int32_t get_messageLength_17() const { return ___messageLength_17; }
	inline int32_t* get_address_of_messageLength_17() { return &___messageLength_17; }
	inline void set_messageLength_17(int32_t value)
	{
		___messageLength_17 = value;
	}

	inline static int32_t get_offset_of_saltLength_18() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___saltLength_18)); }
	inline int32_t get_saltLength_18() const { return ___saltLength_18; }
	inline int32_t* get_address_of_saltLength_18() { return &___saltLength_18; }
	inline void set_saltLength_18(int32_t value)
	{
		___saltLength_18 = value;
	}

	inline static int32_t get_offset_of_fullMessage_19() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___fullMessage_19)); }
	inline bool get_fullMessage_19() const { return ___fullMessage_19; }
	inline bool* get_address_of_fullMessage_19() { return &___fullMessage_19; }
	inline void set_fullMessage_19(bool value)
	{
		___fullMessage_19 = value;
	}

	inline static int32_t get_offset_of_recoveredMessage_20() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___recoveredMessage_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_recoveredMessage_20() const { return ___recoveredMessage_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_recoveredMessage_20() { return &___recoveredMessage_20; }
	inline void set_recoveredMessage_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___recoveredMessage_20 = value;
		Il2CppCodeGenWriteBarrier((&___recoveredMessage_20), value);
	}

	inline static int32_t get_offset_of_preSig_21() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___preSig_21)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_preSig_21() const { return ___preSig_21; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_preSig_21() { return &___preSig_21; }
	inline void set_preSig_21(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___preSig_21 = value;
		Il2CppCodeGenWriteBarrier((&___preSig_21), value);
	}

	inline static int32_t get_offset_of_preBlock_22() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___preBlock_22)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_preBlock_22() const { return ___preBlock_22; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_preBlock_22() { return &___preBlock_22; }
	inline void set_preBlock_22(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___preBlock_22 = value;
		Il2CppCodeGenWriteBarrier((&___preBlock_22), value);
	}

	inline static int32_t get_offset_of_preMStart_23() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___preMStart_23)); }
	inline int32_t get_preMStart_23() const { return ___preMStart_23; }
	inline int32_t* get_address_of_preMStart_23() { return &___preMStart_23; }
	inline void set_preMStart_23(int32_t value)
	{
		___preMStart_23 = value;
	}

	inline static int32_t get_offset_of_preTLength_24() { return static_cast<int32_t>(offsetof(Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698, ___preTLength_24)); }
	inline int32_t get_preTLength_24() const { return ___preTLength_24; }
	inline int32_t* get_address_of_preTLength_24() { return &___preTLength_24; }
	inline void set_preTLength_24(int32_t value)
	{
		___preTLength_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO9796D2PSSSIGNER_T5133D138584C8EAAB8C689A8A85609EAADEBC698_H
#ifndef ISO9796D2SIGNER_T086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4_H
#define ISO9796D2SIGNER_T086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer
struct  Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::digest
	RuntimeObject* ___digest_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::cipher
	RuntimeObject* ___cipher_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::trailer
	int32_t ___trailer_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::keyBits
	int32_t ___keyBits_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::block
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::mBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mBuf_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::messageLength
	int32_t ___messageLength_14;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::fullMessage
	bool ___fullMessage_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::recoveredMessage
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___recoveredMessage_16;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::preSig
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___preSig_17;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Iso9796d2Signer::preBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___preBlock_18;

public:
	inline static int32_t get_offset_of_digest_8() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___digest_8)); }
	inline RuntimeObject* get_digest_8() const { return ___digest_8; }
	inline RuntimeObject** get_address_of_digest_8() { return &___digest_8; }
	inline void set_digest_8(RuntimeObject* value)
	{
		___digest_8 = value;
		Il2CppCodeGenWriteBarrier((&___digest_8), value);
	}

	inline static int32_t get_offset_of_cipher_9() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___cipher_9)); }
	inline RuntimeObject* get_cipher_9() const { return ___cipher_9; }
	inline RuntimeObject** get_address_of_cipher_9() { return &___cipher_9; }
	inline void set_cipher_9(RuntimeObject* value)
	{
		___cipher_9 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_9), value);
	}

	inline static int32_t get_offset_of_trailer_10() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___trailer_10)); }
	inline int32_t get_trailer_10() const { return ___trailer_10; }
	inline int32_t* get_address_of_trailer_10() { return &___trailer_10; }
	inline void set_trailer_10(int32_t value)
	{
		___trailer_10 = value;
	}

	inline static int32_t get_offset_of_keyBits_11() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___keyBits_11)); }
	inline int32_t get_keyBits_11() const { return ___keyBits_11; }
	inline int32_t* get_address_of_keyBits_11() { return &___keyBits_11; }
	inline void set_keyBits_11(int32_t value)
	{
		___keyBits_11 = value;
	}

	inline static int32_t get_offset_of_block_12() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___block_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_block_12() const { return ___block_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_block_12() { return &___block_12; }
	inline void set_block_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___block_12 = value;
		Il2CppCodeGenWriteBarrier((&___block_12), value);
	}

	inline static int32_t get_offset_of_mBuf_13() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___mBuf_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mBuf_13() const { return ___mBuf_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mBuf_13() { return &___mBuf_13; }
	inline void set_mBuf_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mBuf_13 = value;
		Il2CppCodeGenWriteBarrier((&___mBuf_13), value);
	}

	inline static int32_t get_offset_of_messageLength_14() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___messageLength_14)); }
	inline int32_t get_messageLength_14() const { return ___messageLength_14; }
	inline int32_t* get_address_of_messageLength_14() { return &___messageLength_14; }
	inline void set_messageLength_14(int32_t value)
	{
		___messageLength_14 = value;
	}

	inline static int32_t get_offset_of_fullMessage_15() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___fullMessage_15)); }
	inline bool get_fullMessage_15() const { return ___fullMessage_15; }
	inline bool* get_address_of_fullMessage_15() { return &___fullMessage_15; }
	inline void set_fullMessage_15(bool value)
	{
		___fullMessage_15 = value;
	}

	inline static int32_t get_offset_of_recoveredMessage_16() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___recoveredMessage_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_recoveredMessage_16() const { return ___recoveredMessage_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_recoveredMessage_16() { return &___recoveredMessage_16; }
	inline void set_recoveredMessage_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___recoveredMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___recoveredMessage_16), value);
	}

	inline static int32_t get_offset_of_preSig_17() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___preSig_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_preSig_17() const { return ___preSig_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_preSig_17() { return &___preSig_17; }
	inline void set_preSig_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___preSig_17 = value;
		Il2CppCodeGenWriteBarrier((&___preSig_17), value);
	}

	inline static int32_t get_offset_of_preBlock_18() { return static_cast<int32_t>(offsetof(Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4, ___preBlock_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_preBlock_18() const { return ___preBlock_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_preBlock_18() { return &___preBlock_18; }
	inline void set_preBlock_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___preBlock_18 = value;
		Il2CppCodeGenWriteBarrier((&___preBlock_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO9796D2SIGNER_T086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4_H
#ifndef ISOTRAILERS_TC17D8548B797661FBC98A6528955F94A6DDD544E_H
#define ISOTRAILERS_TC17D8548B797661FBC98A6528955F94A6DDD544E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IsoTrailers
struct  IsoTrailers_tC17D8548B797661FBC98A6528955F94A6DDD544E  : public RuntimeObject
{
public:

public:
};

struct IsoTrailers_tC17D8548B797661FBC98A6528955F94A6DDD544E_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IsoTrailers::trailerMap
	RuntimeObject* ___trailerMap_11;

public:
	inline static int32_t get_offset_of_trailerMap_11() { return static_cast<int32_t>(offsetof(IsoTrailers_tC17D8548B797661FBC98A6528955F94A6DDD544E_StaticFields, ___trailerMap_11)); }
	inline RuntimeObject* get_trailerMap_11() const { return ___trailerMap_11; }
	inline RuntimeObject** get_address_of_trailerMap_11() { return &___trailerMap_11; }
	inline void set_trailerMap_11(RuntimeObject* value)
	{
		___trailerMap_11 = value;
		Il2CppCodeGenWriteBarrier((&___trailerMap_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISOTRAILERS_TC17D8548B797661FBC98A6528955F94A6DDD544E_H
#ifndef PLAINDSAENCODING_TDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_H
#define PLAINDSAENCODING_TDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PlainDsaEncoding
struct  PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2  : public RuntimeObject
{
public:

public:
};

struct PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PlainDsaEncoding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PlainDsaEncoding::Instance
	PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_StaticFields, ___Instance_0)); }
	inline PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2 * get_Instance_0() const { return ___Instance_0; }
	inline PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAINDSAENCODING_TDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_H
#ifndef PSSSIGNER_TB2F38DFD2527BEC74B78C301231619792915EF6F_H
#define PSSSIGNER_TB2F38DFD2527BEC74B78C301231619792915EF6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner
struct  PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::contentDigest1
	RuntimeObject* ___contentDigest1_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::contentDigest2
	RuntimeObject* ___contentDigest2_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::mgfDigest
	RuntimeObject* ___mgfDigest_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::cipher
	RuntimeObject* ___cipher_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::hLen
	int32_t ___hLen_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::mgfhLen
	int32_t ___mgfhLen_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::sLen
	int32_t ___sLen_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::sSet
	bool ___sSet_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::emBits
	int32_t ___emBits_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::mDash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mDash_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::block
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block_13;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.PssSigner::trailer
	uint8_t ___trailer_14;

public:
	inline static int32_t get_offset_of_contentDigest1_1() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___contentDigest1_1)); }
	inline RuntimeObject* get_contentDigest1_1() const { return ___contentDigest1_1; }
	inline RuntimeObject** get_address_of_contentDigest1_1() { return &___contentDigest1_1; }
	inline void set_contentDigest1_1(RuntimeObject* value)
	{
		___contentDigest1_1 = value;
		Il2CppCodeGenWriteBarrier((&___contentDigest1_1), value);
	}

	inline static int32_t get_offset_of_contentDigest2_2() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___contentDigest2_2)); }
	inline RuntimeObject* get_contentDigest2_2() const { return ___contentDigest2_2; }
	inline RuntimeObject** get_address_of_contentDigest2_2() { return &___contentDigest2_2; }
	inline void set_contentDigest2_2(RuntimeObject* value)
	{
		___contentDigest2_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentDigest2_2), value);
	}

	inline static int32_t get_offset_of_mgfDigest_3() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___mgfDigest_3)); }
	inline RuntimeObject* get_mgfDigest_3() const { return ___mgfDigest_3; }
	inline RuntimeObject** get_address_of_mgfDigest_3() { return &___mgfDigest_3; }
	inline void set_mgfDigest_3(RuntimeObject* value)
	{
		___mgfDigest_3 = value;
		Il2CppCodeGenWriteBarrier((&___mgfDigest_3), value);
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}

	inline static int32_t get_offset_of_random_5() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___random_5)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_5() const { return ___random_5; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_5() { return &___random_5; }
	inline void set_random_5(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_5 = value;
		Il2CppCodeGenWriteBarrier((&___random_5), value);
	}

	inline static int32_t get_offset_of_hLen_6() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___hLen_6)); }
	inline int32_t get_hLen_6() const { return ___hLen_6; }
	inline int32_t* get_address_of_hLen_6() { return &___hLen_6; }
	inline void set_hLen_6(int32_t value)
	{
		___hLen_6 = value;
	}

	inline static int32_t get_offset_of_mgfhLen_7() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___mgfhLen_7)); }
	inline int32_t get_mgfhLen_7() const { return ___mgfhLen_7; }
	inline int32_t* get_address_of_mgfhLen_7() { return &___mgfhLen_7; }
	inline void set_mgfhLen_7(int32_t value)
	{
		___mgfhLen_7 = value;
	}

	inline static int32_t get_offset_of_sLen_8() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___sLen_8)); }
	inline int32_t get_sLen_8() const { return ___sLen_8; }
	inline int32_t* get_address_of_sLen_8() { return &___sLen_8; }
	inline void set_sLen_8(int32_t value)
	{
		___sLen_8 = value;
	}

	inline static int32_t get_offset_of_sSet_9() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___sSet_9)); }
	inline bool get_sSet_9() const { return ___sSet_9; }
	inline bool* get_address_of_sSet_9() { return &___sSet_9; }
	inline void set_sSet_9(bool value)
	{
		___sSet_9 = value;
	}

	inline static int32_t get_offset_of_emBits_10() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___emBits_10)); }
	inline int32_t get_emBits_10() const { return ___emBits_10; }
	inline int32_t* get_address_of_emBits_10() { return &___emBits_10; }
	inline void set_emBits_10(int32_t value)
	{
		___emBits_10 = value;
	}

	inline static int32_t get_offset_of_salt_11() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___salt_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_11() const { return ___salt_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_11() { return &___salt_11; }
	inline void set_salt_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_11 = value;
		Il2CppCodeGenWriteBarrier((&___salt_11), value);
	}

	inline static int32_t get_offset_of_mDash_12() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___mDash_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mDash_12() const { return ___mDash_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mDash_12() { return &___mDash_12; }
	inline void set_mDash_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mDash_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDash_12), value);
	}

	inline static int32_t get_offset_of_block_13() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___block_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_block_13() const { return ___block_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_block_13() { return &___block_13; }
	inline void set_block_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___block_13 = value;
		Il2CppCodeGenWriteBarrier((&___block_13), value);
	}

	inline static int32_t get_offset_of_trailer_14() { return static_cast<int32_t>(offsetof(PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F, ___trailer_14)); }
	inline uint8_t get_trailer_14() const { return ___trailer_14; }
	inline uint8_t* get_address_of_trailer_14() { return &___trailer_14; }
	inline void set_trailer_14(uint8_t value)
	{
		___trailer_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PSSSIGNER_TB2F38DFD2527BEC74B78C301231619792915EF6F_H
#ifndef RANDOMDSAKCALCULATOR_T218C24F0B03E145447BD2E2FD92AA6208CE4E6CC_H
#define RANDOMDSAKCALCULATOR_T218C24F0B03E145447BD2E2FD92AA6208CE4E6CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RandomDsaKCalculator
struct  RandomDsaKCalculator_t218C24F0B03E145447BD2E2FD92AA6208CE4E6CC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RandomDsaKCalculator::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RandomDsaKCalculator::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_1;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(RandomDsaKCalculator_t218C24F0B03E145447BD2E2FD92AA6208CE4E6CC, ___q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_0() const { return ___q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(RandomDsaKCalculator_t218C24F0B03E145447BD2E2FD92AA6208CE4E6CC, ___random_1)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMDSAKCALCULATOR_T218C24F0B03E145447BD2E2FD92AA6208CE4E6CC_H
#ifndef RSADIGESTSIGNER_TB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_H
#define RSADIGESTSIGNER_TB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RsaDigestSigner
struct  RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::rsaEngine
	RuntimeObject* ___rsaEngine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::algId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algId_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::digest
	RuntimeObject* ___digest_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::forSigning
	bool ___forSigning_3;

public:
	inline static int32_t get_offset_of_rsaEngine_0() { return static_cast<int32_t>(offsetof(RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9, ___rsaEngine_0)); }
	inline RuntimeObject* get_rsaEngine_0() const { return ___rsaEngine_0; }
	inline RuntimeObject** get_address_of_rsaEngine_0() { return &___rsaEngine_0; }
	inline void set_rsaEngine_0(RuntimeObject* value)
	{
		___rsaEngine_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsaEngine_0), value);
	}

	inline static int32_t get_offset_of_algId_1() { return static_cast<int32_t>(offsetof(RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9, ___algId_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algId_1() const { return ___algId_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algId_1() { return &___algId_1; }
	inline void set_algId_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algId_1 = value;
		Il2CppCodeGenWriteBarrier((&___algId_1), value);
	}

	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9, ___digest_2)); }
	inline RuntimeObject* get_digest_2() const { return ___digest_2; }
	inline RuntimeObject** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(RuntimeObject* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}

	inline static int32_t get_offset_of_forSigning_3() { return static_cast<int32_t>(offsetof(RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9, ___forSigning_3)); }
	inline bool get_forSigning_3() const { return ___forSigning_3; }
	inline bool* get_address_of_forSigning_3() { return &___forSigning_3; }
	inline void set_forSigning_3(bool value)
	{
		___forSigning_3 = value;
	}
};

struct RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::oidMap
	RuntimeObject* ___oidMap_4;

public:
	inline static int32_t get_offset_of_oidMap_4() { return static_cast<int32_t>(offsetof(RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_StaticFields, ___oidMap_4)); }
	inline RuntimeObject* get_oidMap_4() const { return ___oidMap_4; }
	inline RuntimeObject** get_address_of_oidMap_4() { return &___oidMap_4; }
	inline void set_oidMap_4(RuntimeObject* value)
	{
		___oidMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___oidMap_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSADIGESTSIGNER_TB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_H
#ifndef SM2SIGNER_TCE33319F3B53C6BF3647B2DE40E8903FEC6110D6_H
#define SM2SIGNER_TCE33319F3B53C6BF3647B2DE40E8903FEC6110D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer
struct  SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaKCalculator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::kCalculator
	RuntimeObject* ___kCalculator_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::digest
	RuntimeObject* ___digest_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.IDsaEncoding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::encoding
	RuntimeObject* ___encoding_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::ecParams
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * ___ecParams_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::pubPoint
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___pubPoint_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::ecKey
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * ___ecKey_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.SM2Signer::z
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___z_6;

public:
	inline static int32_t get_offset_of_kCalculator_0() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___kCalculator_0)); }
	inline RuntimeObject* get_kCalculator_0() const { return ___kCalculator_0; }
	inline RuntimeObject** get_address_of_kCalculator_0() { return &___kCalculator_0; }
	inline void set_kCalculator_0(RuntimeObject* value)
	{
		___kCalculator_0 = value;
		Il2CppCodeGenWriteBarrier((&___kCalculator_0), value);
	}

	inline static int32_t get_offset_of_digest_1() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___digest_1)); }
	inline RuntimeObject* get_digest_1() const { return ___digest_1; }
	inline RuntimeObject** get_address_of_digest_1() { return &___digest_1; }
	inline void set_digest_1(RuntimeObject* value)
	{
		___digest_1 = value;
		Il2CppCodeGenWriteBarrier((&___digest_1), value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___encoding_2)); }
	inline RuntimeObject* get_encoding_2() const { return ___encoding_2; }
	inline RuntimeObject** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(RuntimeObject* value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_ecParams_3() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___ecParams_3)); }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * get_ecParams_3() const { return ___ecParams_3; }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C ** get_address_of_ecParams_3() { return &___ecParams_3; }
	inline void set_ecParams_3(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * value)
	{
		___ecParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___ecParams_3), value);
	}

	inline static int32_t get_offset_of_pubPoint_4() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___pubPoint_4)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_pubPoint_4() const { return ___pubPoint_4; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_pubPoint_4() { return &___pubPoint_4; }
	inline void set_pubPoint_4(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___pubPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___pubPoint_4), value);
	}

	inline static int32_t get_offset_of_ecKey_5() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___ecKey_5)); }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * get_ecKey_5() const { return ___ecKey_5; }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 ** get_address_of_ecKey_5() { return &___ecKey_5; }
	inline void set_ecKey_5(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * value)
	{
		___ecKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___ecKey_5), value);
	}

	inline static int32_t get_offset_of_z_6() { return static_cast<int32_t>(offsetof(SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6, ___z_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_z_6() const { return ___z_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_z_6() { return &___z_6; }
	inline void set_z_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___z_6 = value;
		Il2CppCodeGenWriteBarrier((&___z_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2SIGNER_TCE33319F3B53C6BF3647B2DE40E8903FEC6110D6_H
#ifndef STANDARDDSAENCODING_T82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_H
#define STANDARDDSAENCODING_T82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.StandardDsaEncoding
struct  StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954  : public RuntimeObject
{
public:

public:
};

struct StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.StandardDsaEncoding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.StandardDsaEncoding::Instance
	StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_StaticFields, ___Instance_0)); }
	inline StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954 * get_Instance_0() const { return ___Instance_0; }
	inline StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDDSAENCODING_T82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_H
#ifndef X931SIGNER_T193EDE12132D0C69F92C719BA888D002B838FA81_H
#define X931SIGNER_T193EDE12132D0C69F92C719BA888D002B838FA81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer
struct  X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer::digest
	RuntimeObject* ___digest_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer::cipher
	RuntimeObject* ___cipher_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer::kParam
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___kParam_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer::trailer
	int32_t ___trailer_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer::keyBits
	int32_t ___keyBits_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.X931Signer::block
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block_14;

public:
	inline static int32_t get_offset_of_digest_9() { return static_cast<int32_t>(offsetof(X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81, ___digest_9)); }
	inline RuntimeObject* get_digest_9() const { return ___digest_9; }
	inline RuntimeObject** get_address_of_digest_9() { return &___digest_9; }
	inline void set_digest_9(RuntimeObject* value)
	{
		___digest_9 = value;
		Il2CppCodeGenWriteBarrier((&___digest_9), value);
	}

	inline static int32_t get_offset_of_cipher_10() { return static_cast<int32_t>(offsetof(X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81, ___cipher_10)); }
	inline RuntimeObject* get_cipher_10() const { return ___cipher_10; }
	inline RuntimeObject** get_address_of_cipher_10() { return &___cipher_10; }
	inline void set_cipher_10(RuntimeObject* value)
	{
		___cipher_10 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_10), value);
	}

	inline static int32_t get_offset_of_kParam_11() { return static_cast<int32_t>(offsetof(X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81, ___kParam_11)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_kParam_11() const { return ___kParam_11; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_kParam_11() { return &___kParam_11; }
	inline void set_kParam_11(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___kParam_11 = value;
		Il2CppCodeGenWriteBarrier((&___kParam_11), value);
	}

	inline static int32_t get_offset_of_trailer_12() { return static_cast<int32_t>(offsetof(X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81, ___trailer_12)); }
	inline int32_t get_trailer_12() const { return ___trailer_12; }
	inline int32_t* get_address_of_trailer_12() { return &___trailer_12; }
	inline void set_trailer_12(int32_t value)
	{
		___trailer_12 = value;
	}

	inline static int32_t get_offset_of_keyBits_13() { return static_cast<int32_t>(offsetof(X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81, ___keyBits_13)); }
	inline int32_t get_keyBits_13() const { return ___keyBits_13; }
	inline int32_t* get_address_of_keyBits_13() { return &___keyBits_13; }
	inline void set_keyBits_13(int32_t value)
	{
		___keyBits_13 = value;
	}

	inline static int32_t get_offset_of_block_14() { return static_cast<int32_t>(offsetof(X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81, ___block_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_block_14() const { return ___block_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_block_14() { return &___block_14; }
	inline void set_block_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___block_14 = value;
		Il2CppCodeGenWriteBarrier((&___block_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X931SIGNER_T193EDE12132D0C69F92C719BA888D002B838FA81_H
#ifndef ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#define ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext
struct  AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mNonceRandom
	RuntimeObject* ___mNonceRandom_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecurityParameters
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * ___mSecurityParameters_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mClientVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mClientVersion_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ProtocolVersion BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mServerVersion
	ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * ___mServerVersion_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSession
	RuntimeObject* ___mSession_6;
	// System.Object BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mUserObject
	RuntimeObject * ___mUserObject_7;

public:
	inline static int32_t get_offset_of_mNonceRandom_1() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mNonceRandom_1)); }
	inline RuntimeObject* get_mNonceRandom_1() const { return ___mNonceRandom_1; }
	inline RuntimeObject** get_address_of_mNonceRandom_1() { return &___mNonceRandom_1; }
	inline void set_mNonceRandom_1(RuntimeObject* value)
	{
		___mNonceRandom_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonceRandom_1), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_2() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSecureRandom_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_2() const { return ___mSecureRandom_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_2() { return &___mSecureRandom_2; }
	inline void set_mSecureRandom_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_2), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_3() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSecurityParameters_3)); }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * get_mSecurityParameters_3() const { return ___mSecurityParameters_3; }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA ** get_address_of_mSecurityParameters_3() { return &___mSecurityParameters_3; }
	inline void set_mSecurityParameters_3(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * value)
	{
		___mSecurityParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_3), value);
	}

	inline static int32_t get_offset_of_mClientVersion_4() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mClientVersion_4)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mClientVersion_4() const { return ___mClientVersion_4; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mClientVersion_4() { return &___mClientVersion_4; }
	inline void set_mClientVersion_4(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mClientVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_4), value);
	}

	inline static int32_t get_offset_of_mServerVersion_5() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mServerVersion_5)); }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * get_mServerVersion_5() const { return ___mServerVersion_5; }
	inline ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 ** get_address_of_mServerVersion_5() { return &___mServerVersion_5; }
	inline void set_mServerVersion_5(ProtocolVersion_t235E146D3D32F8C5C3CF74B3BAD785DA46714450 * value)
	{
		___mServerVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_5), value);
	}

	inline static int32_t get_offset_of_mSession_6() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mSession_6)); }
	inline RuntimeObject* get_mSession_6() const { return ___mSession_6; }
	inline RuntimeObject** get_address_of_mSession_6() { return &___mSession_6; }
	inline void set_mSession_6(RuntimeObject* value)
	{
		___mSession_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSession_6), value);
	}

	inline static int32_t get_offset_of_mUserObject_7() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424, ___mUserObject_7)); }
	inline RuntimeObject * get_mUserObject_7() const { return ___mUserObject_7; }
	inline RuntimeObject ** get_address_of_mUserObject_7() { return &___mUserObject_7; }
	inline void set_mUserObject_7(RuntimeObject * value)
	{
		___mUserObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___mUserObject_7), value);
	}
};

struct AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::counter
	int64_t ___counter_0;

public:
	inline static int32_t get_offset_of_counter_0() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424_StaticFields, ___counter_0)); }
	inline int64_t get_counter_0() const { return ___counter_0; }
	inline int64_t* get_address_of_counter_0() { return &___counter_0; }
	inline void set_counter_0(int64_t value)
	{
		___counter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCONTEXT_T4E0B1278345A3770F9F9275F1848BA098EDB7424_H
#ifndef ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#define ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange
struct  AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mKeyExchange
	int32_t ___mKeyExchange_0;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mContext
	RuntimeObject* ___mContext_2;

public:
	inline static int32_t get_offset_of_mKeyExchange_0() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mKeyExchange_0)); }
	inline int32_t get_mKeyExchange_0() const { return ___mKeyExchange_0; }
	inline int32_t* get_address_of_mKeyExchange_0() { return &___mKeyExchange_0; }
	inline void set_mKeyExchange_0(int32_t value)
	{
		___mKeyExchange_0 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSKEYEXCHANGE_TA84FE25083DED3A2AF25782405FF6320853563D3_H
#ifndef ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#define ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner
struct  AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner::mContext
	RuntimeObject* ___mContext_0;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNER_T74CA827C33F8208C7C93E2946ABE8A19CEF2F97F_H
#ifndef TLSEXTENSIONSUTILITIES_T000CC9BA9D75D72554731E8F516DAB794703706C_H
#define TLSEXTENSIONSUTILITIES_T000CC9BA9D75D72554731E8F516DAB794703706C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsExtensionsUtilities
struct  TlsExtensionsUtilities_t000CC9BA9D75D72554731E8F516DAB794703706C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSEXTENSIONSUTILITIES_T000CC9BA9D75D72554731E8F516DAB794703706C_H
#ifndef TLSMAC_TE6856EA9C9A3883B45988264A7BB1F37B01C207E_H
#define TLSMAC_TE6856EA9C9A3883B45988264A7BB1F37B01C207E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac
struct  TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac::context
	RuntimeObject* ___context_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac::secret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___secret_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac::mac
	RuntimeObject* ___mac_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac::digestBlockSize
	int32_t ___digestBlockSize_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac::digestOverhead
	int32_t ___digestOverhead_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac::macLength
	int32_t ___macLength_5;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_secret_1() { return static_cast<int32_t>(offsetof(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E, ___secret_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_secret_1() const { return ___secret_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_secret_1() { return &___secret_1; }
	inline void set_secret_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___secret_1 = value;
		Il2CppCodeGenWriteBarrier((&___secret_1), value);
	}

	inline static int32_t get_offset_of_mac_2() { return static_cast<int32_t>(offsetof(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E, ___mac_2)); }
	inline RuntimeObject* get_mac_2() const { return ___mac_2; }
	inline RuntimeObject** get_address_of_mac_2() { return &___mac_2; }
	inline void set_mac_2(RuntimeObject* value)
	{
		___mac_2 = value;
		Il2CppCodeGenWriteBarrier((&___mac_2), value);
	}

	inline static int32_t get_offset_of_digestBlockSize_3() { return static_cast<int32_t>(offsetof(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E, ___digestBlockSize_3)); }
	inline int32_t get_digestBlockSize_3() const { return ___digestBlockSize_3; }
	inline int32_t* get_address_of_digestBlockSize_3() { return &___digestBlockSize_3; }
	inline void set_digestBlockSize_3(int32_t value)
	{
		___digestBlockSize_3 = value;
	}

	inline static int32_t get_offset_of_digestOverhead_4() { return static_cast<int32_t>(offsetof(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E, ___digestOverhead_4)); }
	inline int32_t get_digestOverhead_4() const { return ___digestOverhead_4; }
	inline int32_t* get_address_of_digestOverhead_4() { return &___digestOverhead_4; }
	inline void set_digestOverhead_4(int32_t value)
	{
		___digestOverhead_4 = value;
	}

	inline static int32_t get_offset_of_macLength_5() { return static_cast<int32_t>(offsetof(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E, ___macLength_5)); }
	inline int32_t get_macLength_5() const { return ___macLength_5; }
	inline int32_t* get_address_of_macLength_5() { return &___macLength_5; }
	inline void set_macLength_5(int32_t value)
	{
		___macLength_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSMAC_TE6856EA9C9A3883B45988264A7BB1F37B01C207E_H
#ifndef TLSNULLCIPHER_T85F07D1CD2054E503356A45C8B539C0D622F8102_H
#define TLSNULLCIPHER_T85F07D1CD2054E503356A45C8B539C0D622F8102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsNullCipher
struct  TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsNullCipher::context
	RuntimeObject* ___context_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsNullCipher::writeMac
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * ___writeMac_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsNullCipher::readMac
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * ___readMac_2;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_writeMac_1() { return static_cast<int32_t>(offsetof(TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102, ___writeMac_1)); }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * get_writeMac_1() const { return ___writeMac_1; }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E ** get_address_of_writeMac_1() { return &___writeMac_1; }
	inline void set_writeMac_1(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * value)
	{
		___writeMac_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeMac_1), value);
	}

	inline static int32_t get_offset_of_readMac_2() { return static_cast<int32_t>(offsetof(TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102, ___readMac_2)); }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * get_readMac_2() const { return ___readMac_2; }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E ** get_address_of_readMac_2() { return &___readMac_2; }
	inline void set_readMac_2(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * value)
	{
		___readMac_2 = value;
		Il2CppCodeGenWriteBarrier((&___readMac_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSNULLCIPHER_T85F07D1CD2054E503356A45C8B539C0D622F8102_H
#ifndef TLSNULLCOMPRESSION_TBD01500502A33DB81FBD50398DEDEFA9A559B4F0_H
#define TLSNULLCOMPRESSION_TBD01500502A33DB81FBD50398DEDEFA9A559B4F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsNullCompression
struct  TlsNullCompression_tBD01500502A33DB81FBD50398DEDEFA9A559B4F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSNULLCOMPRESSION_TBD01500502A33DB81FBD50398DEDEFA9A559B4F0_H
#ifndef TLSRSAUTILITIES_TCFFC08A3A1F68DA34C47F96C26D7CBEF3D94BA5A_H
#define TLSRSAUTILITIES_TCFFC08A3A1F68DA34C47F96C26D7CBEF3D94BA5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaUtilities
struct  TlsRsaUtilities_tCFFC08A3A1F68DA34C47F96C26D7CBEF3D94BA5A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSRSAUTILITIES_TCFFC08A3A1F68DA34C47F96C26D7CBEF3D94BA5A_H
#ifndef TLSSRTPUTILS_TA0BE37A29F4E32EB13E7CE71293BE616C4B48DF8_H
#define TLSSRTPUTILS_TA0BE37A29F4E32EB13E7CE71293BE616C4B48DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSRTPUtils
struct  TlsSRTPUtils_tA0BE37A29F4E32EB13E7CE71293BE616C4B48DF8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSRTPUTILS_TA0BE37A29F4E32EB13E7CE71293BE616C4B48DF8_H
#ifndef TLSSESSIONIMPL_T1E2257A7B3C081B0EE63A51D94F9F36462CBDD06_H
#define TLSSESSIONIMPL_T1E2257A7B3C081B0EE63A51D94F9F36462CBDD06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSessionImpl
struct  TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSessionImpl::mSessionID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSessionID_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSessionImpl::mSessionParameters
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * ___mSessionParameters_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSessionImpl::mResumable
	bool ___mResumable_2;

public:
	inline static int32_t get_offset_of_mSessionID_0() { return static_cast<int32_t>(offsetof(TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06, ___mSessionID_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSessionID_0() const { return ___mSessionID_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSessionID_0() { return &___mSessionID_0; }
	inline void set_mSessionID_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSessionID_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionID_0), value);
	}

	inline static int32_t get_offset_of_mSessionParameters_1() { return static_cast<int32_t>(offsetof(TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06, ___mSessionParameters_1)); }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * get_mSessionParameters_1() const { return ___mSessionParameters_1; }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC ** get_address_of_mSessionParameters_1() { return &___mSessionParameters_1; }
	inline void set_mSessionParameters_1(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * value)
	{
		___mSessionParameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionParameters_1), value);
	}

	inline static int32_t get_offset_of_mResumable_2() { return static_cast<int32_t>(offsetof(TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06, ___mResumable_2)); }
	inline bool get_mResumable_2() const { return ___mResumable_2; }
	inline bool* get_address_of_mResumable_2() { return &___mResumable_2; }
	inline void set_mResumable_2(bool value)
	{
		___mResumable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSESSIONIMPL_T1E2257A7B3C081B0EE63A51D94F9F36462CBDD06_H
#ifndef TLSSRPLOGINPARAMETERS_T1BFA31D45798FF49920039BC42EC594D6C11E238_H
#define TLSSRPLOGINPARAMETERS_T1BFA31D45798FF49920039BC42EC594D6C11E238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpLoginParameters
struct  TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpLoginParameters::mGroup
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___mGroup_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpLoginParameters::mVerifier
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___mVerifier_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpLoginParameters::mSalt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSalt_2;

public:
	inline static int32_t get_offset_of_mGroup_0() { return static_cast<int32_t>(offsetof(TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238, ___mGroup_0)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_mGroup_0() const { return ___mGroup_0; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_mGroup_0() { return &___mGroup_0; }
	inline void set_mGroup_0(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___mGroup_0 = value;
		Il2CppCodeGenWriteBarrier((&___mGroup_0), value);
	}

	inline static int32_t get_offset_of_mVerifier_1() { return static_cast<int32_t>(offsetof(TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238, ___mVerifier_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_mVerifier_1() const { return ___mVerifier_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_mVerifier_1() { return &___mVerifier_1; }
	inline void set_mVerifier_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___mVerifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___mVerifier_1), value);
	}

	inline static int32_t get_offset_of_mSalt_2() { return static_cast<int32_t>(offsetof(TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238, ___mSalt_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSalt_2() const { return ___mSalt_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSalt_2() { return &___mSalt_2; }
	inline void set_mSalt_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSalt_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSalt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSRPLOGINPARAMETERS_T1BFA31D45798FF49920039BC42EC594D6C11E238_H
#ifndef TLSSRPUTILITIES_T0B8FFACB092E5D9A7C5EF3A1FEF8D2D5DE588DE3_H
#define TLSSRPUTILITIES_T0B8FFACB092E5D9A7C5EF3A1FEF8D2D5DE588DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpUtilities
struct  TlsSrpUtilities_t0B8FFACB092E5D9A7C5EF3A1FEF8D2D5DE588DE3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSRPUTILITIES_T0B8FFACB092E5D9A7C5EF3A1FEF8D2D5DE588DE3_H
#ifndef TLSSTREAMCIPHER_T652FDC12B5566A0410A850E6AC09CD9332ECA744_H
#define TLSSTREAMCIPHER_T652FDC12B5566A0410A850E6AC09CD9332ECA744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher
struct  TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsContext BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::context
	RuntimeObject* ___context_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IStreamCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::encryptCipher
	RuntimeObject* ___encryptCipher_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IStreamCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::decryptCipher
	RuntimeObject* ___decryptCipher_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::writeMac
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * ___writeMac_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::readMac
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * ___readMac_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::usesNonce
	bool ___usesNonce_5;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_encryptCipher_1() { return static_cast<int32_t>(offsetof(TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744, ___encryptCipher_1)); }
	inline RuntimeObject* get_encryptCipher_1() const { return ___encryptCipher_1; }
	inline RuntimeObject** get_address_of_encryptCipher_1() { return &___encryptCipher_1; }
	inline void set_encryptCipher_1(RuntimeObject* value)
	{
		___encryptCipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_1), value);
	}

	inline static int32_t get_offset_of_decryptCipher_2() { return static_cast<int32_t>(offsetof(TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744, ___decryptCipher_2)); }
	inline RuntimeObject* get_decryptCipher_2() const { return ___decryptCipher_2; }
	inline RuntimeObject** get_address_of_decryptCipher_2() { return &___decryptCipher_2; }
	inline void set_decryptCipher_2(RuntimeObject* value)
	{
		___decryptCipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_2), value);
	}

	inline static int32_t get_offset_of_writeMac_3() { return static_cast<int32_t>(offsetof(TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744, ___writeMac_3)); }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * get_writeMac_3() const { return ___writeMac_3; }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E ** get_address_of_writeMac_3() { return &___writeMac_3; }
	inline void set_writeMac_3(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * value)
	{
		___writeMac_3 = value;
		Il2CppCodeGenWriteBarrier((&___writeMac_3), value);
	}

	inline static int32_t get_offset_of_readMac_4() { return static_cast<int32_t>(offsetof(TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744, ___readMac_4)); }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * get_readMac_4() const { return ___readMac_4; }
	inline TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E ** get_address_of_readMac_4() { return &___readMac_4; }
	inline void set_readMac_4(TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E * value)
	{
		___readMac_4 = value;
		Il2CppCodeGenWriteBarrier((&___readMac_4), value);
	}

	inline static int32_t get_offset_of_usesNonce_5() { return static_cast<int32_t>(offsetof(TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744, ___usesNonce_5)); }
	inline bool get_usesNonce_5() const { return ___usesNonce_5; }
	inline bool* get_address_of_usesNonce_5() { return &___usesNonce_5; }
	inline void set_usesNonce_5(bool value)
	{
		___usesNonce_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAMCIPHER_T652FDC12B5566A0410A850E6AC09CD9332ECA744_H
#ifndef TLSUTILITIES_T9576CA748C9EC0B26E5CD9254F703579ECF0777C_H
#define TLSUTILITIES_T9576CA748C9EC0B26E5CD9254F703579ECF0777C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities
struct  TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C  : public RuntimeObject
{
public:

public:
};

struct TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBytes_0;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyShorts
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___EmptyShorts_1;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyInts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EmptyInts_2;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyLongs
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___EmptyLongs_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::SSL_CLIENT
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SSL_CLIENT_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::SSL_SERVER
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SSL_SERVER_5;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsUtilities::SSL3_CONST
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___SSL3_CONST_6;

public:
	inline static int32_t get_offset_of_EmptyBytes_0() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___EmptyBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBytes_0() const { return ___EmptyBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBytes_0() { return &___EmptyBytes_0; }
	inline void set_EmptyBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_0), value);
	}

	inline static int32_t get_offset_of_EmptyShorts_1() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___EmptyShorts_1)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_EmptyShorts_1() const { return ___EmptyShorts_1; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_EmptyShorts_1() { return &___EmptyShorts_1; }
	inline void set_EmptyShorts_1(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___EmptyShorts_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyShorts_1), value);
	}

	inline static int32_t get_offset_of_EmptyInts_2() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___EmptyInts_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EmptyInts_2() const { return ___EmptyInts_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EmptyInts_2() { return &___EmptyInts_2; }
	inline void set_EmptyInts_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EmptyInts_2 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyInts_2), value);
	}

	inline static int32_t get_offset_of_EmptyLongs_3() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___EmptyLongs_3)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_EmptyLongs_3() const { return ___EmptyLongs_3; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_EmptyLongs_3() { return &___EmptyLongs_3; }
	inline void set_EmptyLongs_3(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___EmptyLongs_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyLongs_3), value);
	}

	inline static int32_t get_offset_of_SSL_CLIENT_4() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___SSL_CLIENT_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SSL_CLIENT_4() const { return ___SSL_CLIENT_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SSL_CLIENT_4() { return &___SSL_CLIENT_4; }
	inline void set_SSL_CLIENT_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SSL_CLIENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___SSL_CLIENT_4), value);
	}

	inline static int32_t get_offset_of_SSL_SERVER_5() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___SSL_SERVER_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SSL_SERVER_5() const { return ___SSL_SERVER_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SSL_SERVER_5() { return &___SSL_SERVER_5; }
	inline void set_SSL_SERVER_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SSL_SERVER_5 = value;
		Il2CppCodeGenWriteBarrier((&___SSL_SERVER_5), value);
	}

	inline static int32_t get_offset_of_SSL3_CONST_6() { return static_cast<int32_t>(offsetof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields, ___SSL3_CONST_6)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_SSL3_CONST_6() const { return ___SSL3_CONST_6; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_SSL3_CONST_6() { return &___SSL3_CONST_6; }
	inline void set_SSL3_CONST_6(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___SSL3_CONST_6 = value;
		Il2CppCodeGenWriteBarrier((&___SSL3_CONST_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSUTILITIES_T9576CA748C9EC0B26E5CD9254F703579ECF0777C_H
#ifndef URLANDHASH_T6EB3DDE69190DD0AB8CEE90C6BB2229934137302_H
#define URLANDHASH_T6EB3DDE69190DD0AB8CEE90C6BB2229934137302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UrlAndHash
struct  UrlAndHash_t6EB3DDE69190DD0AB8CEE90C6BB2229934137302  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UrlAndHash::mUrl
	String_t* ___mUrl_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UrlAndHash::mSha1Hash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSha1Hash_1;

public:
	inline static int32_t get_offset_of_mUrl_0() { return static_cast<int32_t>(offsetof(UrlAndHash_t6EB3DDE69190DD0AB8CEE90C6BB2229934137302, ___mUrl_0)); }
	inline String_t* get_mUrl_0() const { return ___mUrl_0; }
	inline String_t** get_address_of_mUrl_0() { return &___mUrl_0; }
	inline void set_mUrl_0(String_t* value)
	{
		___mUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___mUrl_0), value);
	}

	inline static int32_t get_offset_of_mSha1Hash_1() { return static_cast<int32_t>(offsetof(UrlAndHash_t6EB3DDE69190DD0AB8CEE90C6BB2229934137302, ___mSha1Hash_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSha1Hash_1() const { return ___mSha1Hash_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSha1Hash_1() { return &___mSha1Hash_1; }
	inline void set_mSha1Hash_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSha1Hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSha1Hash_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLANDHASH_T6EB3DDE69190DD0AB8CEE90C6BB2229934137302_H
#ifndef USESRTPDATA_TA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D_H
#define USESRTPDATA_TA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UseSrtpData
struct  UseSrtpData_tA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UseSrtpData::mProtectionProfiles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mProtectionProfiles_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UseSrtpData::mMki
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mMki_1;

public:
	inline static int32_t get_offset_of_mProtectionProfiles_0() { return static_cast<int32_t>(offsetof(UseSrtpData_tA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D, ___mProtectionProfiles_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mProtectionProfiles_0() const { return ___mProtectionProfiles_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mProtectionProfiles_0() { return &___mProtectionProfiles_0; }
	inline void set_mProtectionProfiles_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mProtectionProfiles_0 = value;
		Il2CppCodeGenWriteBarrier((&___mProtectionProfiles_0), value);
	}

	inline static int32_t get_offset_of_mMki_1() { return static_cast<int32_t>(offsetof(UseSrtpData_tA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D, ___mMki_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mMki_1() const { return ___mMki_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mMki_1() { return &___mMki_1; }
	inline void set_mMki_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mMki_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMki_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USESRTPDATA_TA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D_H
#ifndef USERMAPPINGTYPE_TF44BE62B49B6BBA545C7E5BEC0B772B9D5ECABEE_H
#define USERMAPPINGTYPE_TF44BE62B49B6BBA545C7E5BEC0B772B9D5ECABEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.UserMappingType
struct  UserMappingType_tF44BE62B49B6BBA545C7E5BEC0B772B9D5ECABEE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERMAPPINGTYPE_TF44BE62B49B6BBA545C7E5BEC0B772B9D5ECABEE_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#define RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Random
struct  Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_0;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_1;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SeedArray_2;

public:
	inline static int32_t get_offset_of_inext_0() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inext_0)); }
	inline int32_t get_inext_0() const { return ___inext_0; }
	inline int32_t* get_address_of_inext_0() { return &___inext_0; }
	inline void set_inext_0(int32_t value)
	{
		___inext_0 = value;
	}

	inline static int32_t get_offset_of_inextp_1() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inextp_1)); }
	inline int32_t get_inextp_1() const { return ___inextp_1; }
	inline int32_t* get_address_of_inextp_1() { return &___inextp_1; }
	inline void set_inextp_1(int32_t value)
	{
		___inextp_1 = value;
	}

	inline static int32_t get_offset_of_SeedArray_2() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___SeedArray_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SeedArray_2() const { return ___SeedArray_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SeedArray_2() { return &___SeedArray_2; }
	inline void set_SeedArray_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SeedArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___SeedArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CCMPARAMETERS_TDEA0AB6441DFA994C80F3D4D207A0D5C01CBD543_H
#define CCMPARAMETERS_TDEA0AB6441DFA994C80F3D4D207A0D5C01CBD543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.CcmParameters
struct  CcmParameters_tDEA0AB6441DFA994C80F3D4D207A0D5C01CBD543  : public AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCMPARAMETERS_TDEA0AB6441DFA994C80F3D4D207A0D5C01CBD543_H
#ifndef TLSDSASIGNER_TE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E_H
#define TLSDSASIGNER_TE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDsaSigner
struct  TlsDsaSigner_tE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E  : public AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDSASIGNER_TE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E_H
#ifndef TLSECDHKEYEXCHANGE_T0402E5233B545D8D5B8DC9738C90B952BCF2DB19_H
#define TLSECDHKEYEXCHANGE_T0402E5233B545D8D5B8DC9738C90B952BCF2DB19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange
struct  TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19  : public AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mTlsSigner
	RuntimeObject* ___mTlsSigner_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mServerPublicKey_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mAgreementCredentials
	RuntimeObject* ___mAgreementCredentials_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mECAgreePrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mECAgreePrivateKey_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mECAgreePublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___mECAgreePublicKey_10;

public:
	inline static int32_t get_offset_of_mTlsSigner_3() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mTlsSigner_3)); }
	inline RuntimeObject* get_mTlsSigner_3() const { return ___mTlsSigner_3; }
	inline RuntimeObject** get_address_of_mTlsSigner_3() { return &___mTlsSigner_3; }
	inline void set_mTlsSigner_3(RuntimeObject* value)
	{
		___mTlsSigner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSigner_3), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_4() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mNamedCurves_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_4() const { return ___mNamedCurves_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_4() { return &___mNamedCurves_4; }
	inline void set_mNamedCurves_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_4), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_5() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mClientECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_5() const { return ___mClientECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_5() { return &___mClientECPointFormats_5; }
	inline void set_mClientECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_6() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mServerECPointFormats_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_6() const { return ___mServerECPointFormats_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_6() { return &___mServerECPointFormats_6; }
	inline void set_mServerECPointFormats_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_6), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_7() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mServerPublicKey_7)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mServerPublicKey_7() const { return ___mServerPublicKey_7; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mServerPublicKey_7() { return &___mServerPublicKey_7; }
	inline void set_mServerPublicKey_7(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mServerPublicKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_7), value);
	}

	inline static int32_t get_offset_of_mAgreementCredentials_8() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mAgreementCredentials_8)); }
	inline RuntimeObject* get_mAgreementCredentials_8() const { return ___mAgreementCredentials_8; }
	inline RuntimeObject** get_address_of_mAgreementCredentials_8() { return &___mAgreementCredentials_8; }
	inline void set_mAgreementCredentials_8(RuntimeObject* value)
	{
		___mAgreementCredentials_8 = value;
		Il2CppCodeGenWriteBarrier((&___mAgreementCredentials_8), value);
	}

	inline static int32_t get_offset_of_mECAgreePrivateKey_9() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mECAgreePrivateKey_9)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mECAgreePrivateKey_9() const { return ___mECAgreePrivateKey_9; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mECAgreePrivateKey_9() { return &___mECAgreePrivateKey_9; }
	inline void set_mECAgreePrivateKey_9(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mECAgreePrivateKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePrivateKey_9), value);
	}

	inline static int32_t get_offset_of_mECAgreePublicKey_10() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19, ___mECAgreePublicKey_10)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_mECAgreePublicKey_10() const { return ___mECAgreePublicKey_10; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_mECAgreePublicKey_10() { return &___mECAgreePublicKey_10; }
	inline void set_mECAgreePublicKey_10(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___mECAgreePublicKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePublicKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDHKEYEXCHANGE_T0402E5233B545D8D5B8DC9738C90B952BCF2DB19_H
#ifndef TLSPSKKEYEXCHANGE_T2CD0C038811589FC9178D11A7185F7D1012621C9_H
#define TLSPSKKEYEXCHANGE_T2CD0C038811589FC9178D11A7185F7D1012621C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange
struct  TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9  : public AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentity BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mPskIdentity
	RuntimeObject* ___mPskIdentity_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskIdentityManager BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mPskIdentityManager
	RuntimeObject* ___mPskIdentityManager_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsDHVerifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mDHVerifier
	RuntimeObject* ___mDHVerifier_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mDHParameters
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___mDHParameters_6;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mPskIdentityHint
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPskIdentityHint_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mPsk
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPsk_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mDHAgreePrivateKey
	DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * ___mDHAgreePrivateKey_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mDHAgreePublicKey
	DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 * ___mDHAgreePublicKey_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mECAgreePrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mECAgreePrivateKey_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mECAgreePublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___mECAgreePublicKey_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mServerPublicKey_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mRsaServerPublicKey
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___mRsaServerPublicKey_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsEncryptionCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mServerCredentials
	RuntimeObject* ___mServerCredentials_18;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsPskKeyExchange::mPremasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPremasterSecret_19;

public:
	inline static int32_t get_offset_of_mPskIdentity_3() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mPskIdentity_3)); }
	inline RuntimeObject* get_mPskIdentity_3() const { return ___mPskIdentity_3; }
	inline RuntimeObject** get_address_of_mPskIdentity_3() { return &___mPskIdentity_3; }
	inline void set_mPskIdentity_3(RuntimeObject* value)
	{
		___mPskIdentity_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentity_3), value);
	}

	inline static int32_t get_offset_of_mPskIdentityManager_4() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mPskIdentityManager_4)); }
	inline RuntimeObject* get_mPskIdentityManager_4() const { return ___mPskIdentityManager_4; }
	inline RuntimeObject** get_address_of_mPskIdentityManager_4() { return &___mPskIdentityManager_4; }
	inline void set_mPskIdentityManager_4(RuntimeObject* value)
	{
		___mPskIdentityManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentityManager_4), value);
	}

	inline static int32_t get_offset_of_mDHVerifier_5() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mDHVerifier_5)); }
	inline RuntimeObject* get_mDHVerifier_5() const { return ___mDHVerifier_5; }
	inline RuntimeObject** get_address_of_mDHVerifier_5() { return &___mDHVerifier_5; }
	inline void set_mDHVerifier_5(RuntimeObject* value)
	{
		___mDHVerifier_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDHVerifier_5), value);
	}

	inline static int32_t get_offset_of_mDHParameters_6() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mDHParameters_6)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_mDHParameters_6() const { return ___mDHParameters_6; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_mDHParameters_6() { return &___mDHParameters_6; }
	inline void set_mDHParameters_6(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___mDHParameters_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDHParameters_6), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_7() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mNamedCurves_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_7() const { return ___mNamedCurves_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_7() { return &___mNamedCurves_7; }
	inline void set_mNamedCurves_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_7 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_7), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_8() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mClientECPointFormats_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_8() const { return ___mClientECPointFormats_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_8() { return &___mClientECPointFormats_8; }
	inline void set_mClientECPointFormats_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_8), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_9() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mServerECPointFormats_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_9() const { return ___mServerECPointFormats_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_9() { return &___mServerECPointFormats_9; }
	inline void set_mServerECPointFormats_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_9), value);
	}

	inline static int32_t get_offset_of_mPskIdentityHint_10() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mPskIdentityHint_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPskIdentityHint_10() const { return ___mPskIdentityHint_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPskIdentityHint_10() { return &___mPskIdentityHint_10; }
	inline void set_mPskIdentityHint_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPskIdentityHint_10 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentityHint_10), value);
	}

	inline static int32_t get_offset_of_mPsk_11() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mPsk_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPsk_11() const { return ___mPsk_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPsk_11() { return &___mPsk_11; }
	inline void set_mPsk_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPsk_11 = value;
		Il2CppCodeGenWriteBarrier((&___mPsk_11), value);
	}

	inline static int32_t get_offset_of_mDHAgreePrivateKey_12() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mDHAgreePrivateKey_12)); }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * get_mDHAgreePrivateKey_12() const { return ___mDHAgreePrivateKey_12; }
	inline DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 ** get_address_of_mDHAgreePrivateKey_12() { return &___mDHAgreePrivateKey_12; }
	inline void set_mDHAgreePrivateKey_12(DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33 * value)
	{
		___mDHAgreePrivateKey_12 = value;
		Il2CppCodeGenWriteBarrier((&___mDHAgreePrivateKey_12), value);
	}

	inline static int32_t get_offset_of_mDHAgreePublicKey_13() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mDHAgreePublicKey_13)); }
	inline DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 * get_mDHAgreePublicKey_13() const { return ___mDHAgreePublicKey_13; }
	inline DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 ** get_address_of_mDHAgreePublicKey_13() { return &___mDHAgreePublicKey_13; }
	inline void set_mDHAgreePublicKey_13(DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18 * value)
	{
		___mDHAgreePublicKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___mDHAgreePublicKey_13), value);
	}

	inline static int32_t get_offset_of_mECAgreePrivateKey_14() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mECAgreePrivateKey_14)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mECAgreePrivateKey_14() const { return ___mECAgreePrivateKey_14; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mECAgreePrivateKey_14() { return &___mECAgreePrivateKey_14; }
	inline void set_mECAgreePrivateKey_14(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mECAgreePrivateKey_14 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePrivateKey_14), value);
	}

	inline static int32_t get_offset_of_mECAgreePublicKey_15() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mECAgreePublicKey_15)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_mECAgreePublicKey_15() const { return ___mECAgreePublicKey_15; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_mECAgreePublicKey_15() { return &___mECAgreePublicKey_15; }
	inline void set_mECAgreePublicKey_15(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___mECAgreePublicKey_15 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePublicKey_15), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_16() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mServerPublicKey_16)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mServerPublicKey_16() const { return ___mServerPublicKey_16; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mServerPublicKey_16() { return &___mServerPublicKey_16; }
	inline void set_mServerPublicKey_16(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mServerPublicKey_16 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_16), value);
	}

	inline static int32_t get_offset_of_mRsaServerPublicKey_17() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mRsaServerPublicKey_17)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_mRsaServerPublicKey_17() const { return ___mRsaServerPublicKey_17; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_mRsaServerPublicKey_17() { return &___mRsaServerPublicKey_17; }
	inline void set_mRsaServerPublicKey_17(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___mRsaServerPublicKey_17 = value;
		Il2CppCodeGenWriteBarrier((&___mRsaServerPublicKey_17), value);
	}

	inline static int32_t get_offset_of_mServerCredentials_18() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mServerCredentials_18)); }
	inline RuntimeObject* get_mServerCredentials_18() const { return ___mServerCredentials_18; }
	inline RuntimeObject** get_address_of_mServerCredentials_18() { return &___mServerCredentials_18; }
	inline void set_mServerCredentials_18(RuntimeObject* value)
	{
		___mServerCredentials_18 = value;
		Il2CppCodeGenWriteBarrier((&___mServerCredentials_18), value);
	}

	inline static int32_t get_offset_of_mPremasterSecret_19() { return static_cast<int32_t>(offsetof(TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9, ___mPremasterSecret_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPremasterSecret_19() const { return ___mPremasterSecret_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPremasterSecret_19() { return &___mPremasterSecret_19; }
	inline void set_mPremasterSecret_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPremasterSecret_19 = value;
		Il2CppCodeGenWriteBarrier((&___mPremasterSecret_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPSKKEYEXCHANGE_T2CD0C038811589FC9178D11A7185F7D1012621C9_H
#ifndef TLSRSAKEYEXCHANGE_T822F8F97568733C0DD7663167BDC18197078B3B1_H
#define TLSRSAKEYEXCHANGE_T822F8F97568733C0DD7663167BDC18197078B3B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange
struct  TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1  : public AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mServerPublicKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mRsaServerPublicKey
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___mRsaServerPublicKey_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsEncryptionCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mServerCredentials
	RuntimeObject* ___mServerCredentials_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mPremasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPremasterSecret_6;

public:
	inline static int32_t get_offset_of_mServerPublicKey_3() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1, ___mServerPublicKey_3)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mServerPublicKey_3() const { return ___mServerPublicKey_3; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mServerPublicKey_3() { return &___mServerPublicKey_3; }
	inline void set_mServerPublicKey_3(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mServerPublicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_3), value);
	}

	inline static int32_t get_offset_of_mRsaServerPublicKey_4() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1, ___mRsaServerPublicKey_4)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_mRsaServerPublicKey_4() const { return ___mRsaServerPublicKey_4; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_mRsaServerPublicKey_4() { return &___mRsaServerPublicKey_4; }
	inline void set_mRsaServerPublicKey_4(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___mRsaServerPublicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRsaServerPublicKey_4), value);
	}

	inline static int32_t get_offset_of_mServerCredentials_5() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1, ___mServerCredentials_5)); }
	inline RuntimeObject* get_mServerCredentials_5() const { return ___mServerCredentials_5; }
	inline RuntimeObject** get_address_of_mServerCredentials_5() { return &___mServerCredentials_5; }
	inline void set_mServerCredentials_5(RuntimeObject* value)
	{
		___mServerCredentials_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerCredentials_5), value);
	}

	inline static int32_t get_offset_of_mPremasterSecret_6() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1, ___mPremasterSecret_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPremasterSecret_6() const { return ___mPremasterSecret_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPremasterSecret_6() { return &___mPremasterSecret_6; }
	inline void set_mPremasterSecret_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPremasterSecret_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPremasterSecret_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSRSAKEYEXCHANGE_T822F8F97568733C0DD7663167BDC18197078B3B1_H
#ifndef TLSRSASIGNER_T4D74FA5A99017314DA8C11D30129A75638624E0A_H
#define TLSRSASIGNER_T4D74FA5A99017314DA8C11D30129A75638624E0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsRsaSigner
struct  TlsRsaSigner_t4D74FA5A99017314DA8C11D30129A75638624E0A  : public AbstractTlsSigner_t74CA827C33F8208C7C93E2946ABE8A19CEF2F97F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSRSASIGNER_T4D74FA5A99017314DA8C11D30129A75638624E0A_H
#ifndef TLSSERVERCONTEXTIMPL_T483AE72706A494F8082A01D126A7E55DE318B3FE_H
#define TLSSERVERCONTEXTIMPL_T483AE72706A494F8082A01D126A7E55DE318B3FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContextImpl
struct  TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE  : public AbstractTlsContext_t4E0B1278345A3770F9F9275F1848BA098EDB7424
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCONTEXTIMPL_T483AE72706A494F8082A01D126A7E55DE318B3FE_H
#ifndef TLSSRPKEYEXCHANGE_T78066067B7953C9702A1850A66683F5AABDC22B3_H
#define TLSSRPKEYEXCHANGE_T78066067B7953C9702A1850A66683F5AABDC22B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange
struct  TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3  : public AbstractTlsKeyExchange_tA84FE25083DED3A2AF25782405FF6320853563D3
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mTlsSigner
	RuntimeObject* ___mTlsSigner_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpGroupVerifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mGroupVerifier
	RuntimeObject* ___mGroupVerifier_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mIdentity_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mPassword
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPassword_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___mServerPublicKey_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mSrpGroup
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * ___mSrpGroup_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Client BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mSrpClient
	Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE * ___mSrpClient_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Agreement.Srp.Srp6Server BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mSrpServer
	Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344 * ___mSrpServer_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mSrpPeerCredentials
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___mSrpPeerCredentials_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mSrpVerifier
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___mSrpVerifier_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mSrpSalt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSrpSalt_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSignerCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSrpKeyExchange::mServerCredentials
	RuntimeObject* ___mServerCredentials_14;

public:
	inline static int32_t get_offset_of_mTlsSigner_3() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mTlsSigner_3)); }
	inline RuntimeObject* get_mTlsSigner_3() const { return ___mTlsSigner_3; }
	inline RuntimeObject** get_address_of_mTlsSigner_3() { return &___mTlsSigner_3; }
	inline void set_mTlsSigner_3(RuntimeObject* value)
	{
		___mTlsSigner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSigner_3), value);
	}

	inline static int32_t get_offset_of_mGroupVerifier_4() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mGroupVerifier_4)); }
	inline RuntimeObject* get_mGroupVerifier_4() const { return ___mGroupVerifier_4; }
	inline RuntimeObject** get_address_of_mGroupVerifier_4() { return &___mGroupVerifier_4; }
	inline void set_mGroupVerifier_4(RuntimeObject* value)
	{
		___mGroupVerifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___mGroupVerifier_4), value);
	}

	inline static int32_t get_offset_of_mIdentity_5() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mIdentity_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mIdentity_5() const { return ___mIdentity_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mIdentity_5() { return &___mIdentity_5; }
	inline void set_mIdentity_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mIdentity_5 = value;
		Il2CppCodeGenWriteBarrier((&___mIdentity_5), value);
	}

	inline static int32_t get_offset_of_mPassword_6() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mPassword_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPassword_6() const { return ___mPassword_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPassword_6() { return &___mPassword_6; }
	inline void set_mPassword_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPassword_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPassword_6), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_7() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mServerPublicKey_7)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_mServerPublicKey_7() const { return ___mServerPublicKey_7; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_mServerPublicKey_7() { return &___mServerPublicKey_7; }
	inline void set_mServerPublicKey_7(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___mServerPublicKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_7), value);
	}

	inline static int32_t get_offset_of_mSrpGroup_8() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mSrpGroup_8)); }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * get_mSrpGroup_8() const { return ___mSrpGroup_8; }
	inline Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 ** get_address_of_mSrpGroup_8() { return &___mSrpGroup_8; }
	inline void set_mSrpGroup_8(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464 * value)
	{
		___mSrpGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpGroup_8), value);
	}

	inline static int32_t get_offset_of_mSrpClient_9() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mSrpClient_9)); }
	inline Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE * get_mSrpClient_9() const { return ___mSrpClient_9; }
	inline Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE ** get_address_of_mSrpClient_9() { return &___mSrpClient_9; }
	inline void set_mSrpClient_9(Srp6Client_tFEB596C997E97A90130A00A17BD7BEB61F7085DE * value)
	{
		___mSrpClient_9 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpClient_9), value);
	}

	inline static int32_t get_offset_of_mSrpServer_10() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mSrpServer_10)); }
	inline Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344 * get_mSrpServer_10() const { return ___mSrpServer_10; }
	inline Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344 ** get_address_of_mSrpServer_10() { return &___mSrpServer_10; }
	inline void set_mSrpServer_10(Srp6Server_t7362C120EE9E4CFDA2A17F5EF699551F86EC1344 * value)
	{
		___mSrpServer_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpServer_10), value);
	}

	inline static int32_t get_offset_of_mSrpPeerCredentials_11() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mSrpPeerCredentials_11)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_mSrpPeerCredentials_11() const { return ___mSrpPeerCredentials_11; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_mSrpPeerCredentials_11() { return &___mSrpPeerCredentials_11; }
	inline void set_mSrpPeerCredentials_11(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___mSrpPeerCredentials_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpPeerCredentials_11), value);
	}

	inline static int32_t get_offset_of_mSrpVerifier_12() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mSrpVerifier_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_mSrpVerifier_12() const { return ___mSrpVerifier_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_mSrpVerifier_12() { return &___mSrpVerifier_12; }
	inline void set_mSrpVerifier_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___mSrpVerifier_12 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpVerifier_12), value);
	}

	inline static int32_t get_offset_of_mSrpSalt_13() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mSrpSalt_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSrpSalt_13() const { return ___mSrpSalt_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSrpSalt_13() { return &___mSrpSalt_13; }
	inline void set_mSrpSalt_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSrpSalt_13 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpSalt_13), value);
	}

	inline static int32_t get_offset_of_mServerCredentials_14() { return static_cast<int32_t>(offsetof(TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3, ___mServerCredentials_14)); }
	inline RuntimeObject* get_mServerCredentials_14() const { return ___mServerCredentials_14; }
	inline RuntimeObject** get_address_of_mServerCredentials_14() { return &___mServerCredentials_14; }
	inline void set_mServerCredentials_14(RuntimeObject* value)
	{
		___mServerCredentials_14 = value;
		Il2CppCodeGenWriteBarrier((&___mServerCredentials_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSRPKEYEXCHANGE_T78066067B7953C9702A1850A66683F5AABDC22B3_H
#ifndef SECURERANDOM_T5520D5E8543D2000539BD07077884C7FB17A9570_H
#define SECURERANDOM_T5520D5E8543D2000539BD07077884C7FB17A9570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct  SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570  : public Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IRandomGenerator BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::generator
	RuntimeObject* ___generator_5;

public:
	inline static int32_t get_offset_of_generator_5() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570, ___generator_5)); }
	inline RuntimeObject* get_generator_5() const { return ___generator_5; }
	inline RuntimeObject** get_address_of_generator_5() { return &___generator_5; }
	inline void set_generator_5(RuntimeObject* value)
	{
		___generator_5 = value;
		Il2CppCodeGenWriteBarrier((&___generator_5), value);
	}
};

struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields
{
public:
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::counter
	int64_t ___counter_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::master
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___master_4;
	// System.Double BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom::DoubleScale
	double ___DoubleScale_6;

public:
	inline static int32_t get_offset_of_counter_3() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields, ___counter_3)); }
	inline int64_t get_counter_3() const { return ___counter_3; }
	inline int64_t* get_address_of_counter_3() { return &___counter_3; }
	inline void set_counter_3(int64_t value)
	{
		___counter_3 = value;
	}

	inline static int32_t get_offset_of_master_4() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields, ___master_4)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_master_4() const { return ___master_4; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_master_4() { return &___master_4; }
	inline void set_master_4(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___master_4 = value;
		Il2CppCodeGenWriteBarrier((&___master_4), value);
	}

	inline static int32_t get_offset_of_DoubleScale_6() { return static_cast<int32_t>(offsetof(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570_StaticFields, ___DoubleScale_6)); }
	inline double get_DoubleScale_6() const { return ___DoubleScale_6; }
	inline double* get_address_of_DoubleScale_6() { return &___DoubleScale_6; }
	inline void set_DoubleScale_6(double value)
	{
		___DoubleScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURERANDOM_T5520D5E8543D2000539BD07077884C7FB17A9570_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef SP800SECURERANDOM_T0E78F8732DAAC0FA417DB9028221827B62DB3374_H
#define SP800SECURERANDOM_T0E78F8732DAAC0FA417DB9028221827B62DB3374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandom
struct  SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374  : public SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.IDrbgProvider BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandom::mDrbgProvider
	RuntimeObject* ___mDrbgProvider_7;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandom::mPredictionResistant
	bool ___mPredictionResistant_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandom::mRandomSource
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mRandomSource_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IEntropySource BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandom::mEntropySource
	RuntimeObject* ___mEntropySource_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.Drbg.ISP80090Drbg BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.SP800SecureRandom::mDrbg
	RuntimeObject* ___mDrbg_11;

public:
	inline static int32_t get_offset_of_mDrbgProvider_7() { return static_cast<int32_t>(offsetof(SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374, ___mDrbgProvider_7)); }
	inline RuntimeObject* get_mDrbgProvider_7() const { return ___mDrbgProvider_7; }
	inline RuntimeObject** get_address_of_mDrbgProvider_7() { return &___mDrbgProvider_7; }
	inline void set_mDrbgProvider_7(RuntimeObject* value)
	{
		___mDrbgProvider_7 = value;
		Il2CppCodeGenWriteBarrier((&___mDrbgProvider_7), value);
	}

	inline static int32_t get_offset_of_mPredictionResistant_8() { return static_cast<int32_t>(offsetof(SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374, ___mPredictionResistant_8)); }
	inline bool get_mPredictionResistant_8() const { return ___mPredictionResistant_8; }
	inline bool* get_address_of_mPredictionResistant_8() { return &___mPredictionResistant_8; }
	inline void set_mPredictionResistant_8(bool value)
	{
		___mPredictionResistant_8 = value;
	}

	inline static int32_t get_offset_of_mRandomSource_9() { return static_cast<int32_t>(offsetof(SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374, ___mRandomSource_9)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mRandomSource_9() const { return ___mRandomSource_9; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mRandomSource_9() { return &___mRandomSource_9; }
	inline void set_mRandomSource_9(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mRandomSource_9 = value;
		Il2CppCodeGenWriteBarrier((&___mRandomSource_9), value);
	}

	inline static int32_t get_offset_of_mEntropySource_10() { return static_cast<int32_t>(offsetof(SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374, ___mEntropySource_10)); }
	inline RuntimeObject* get_mEntropySource_10() const { return ___mEntropySource_10; }
	inline RuntimeObject** get_address_of_mEntropySource_10() { return &___mEntropySource_10; }
	inline void set_mEntropySource_10(RuntimeObject* value)
	{
		___mEntropySource_10 = value;
		Il2CppCodeGenWriteBarrier((&___mEntropySource_10), value);
	}

	inline static int32_t get_offset_of_mDrbg_11() { return static_cast<int32_t>(offsetof(SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374, ___mDrbg_11)); }
	inline RuntimeObject* get_mDrbg_11() const { return ___mDrbg_11; }
	inline RuntimeObject** get_address_of_mDrbg_11() { return &___mDrbg_11; }
	inline void set_mDrbg_11(RuntimeObject* value)
	{
		___mDrbg_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDrbg_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SP800SECURERANDOM_T0E78F8732DAAC0FA417DB9028221827B62DB3374_H
#ifndef SEEDGENERATOR_T050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1_H
#define SEEDGENERATOR_T050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ThreadedSeedGenerator_SeedGenerator
struct  SeedGenerator_t050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ThreadedSeedGenerator_SeedGenerator::counter
	int32_t ___counter_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.ThreadedSeedGenerator_SeedGenerator::stop
	bool ___stop_1;

public:
	inline static int32_t get_offset_of_counter_0() { return static_cast<int32_t>(offsetof(SeedGenerator_t050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1, ___counter_0)); }
	inline int32_t get_counter_0() const { return ___counter_0; }
	inline int32_t* get_address_of_counter_0() { return &___counter_0; }
	inline void set_counter_0(int32_t value)
	{
		___counter_0 = value;
	}

	inline static int32_t get_offset_of_stop_1() { return static_cast<int32_t>(offsetof(SeedGenerator_t050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1, ___stop_1)); }
	inline bool get_stop_1() const { return ___stop_1; }
	inline bool* get_address_of_stop_1() { return &___stop_1; }
	inline void set_stop_1(bool value)
	{
		___stop_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEDGENERATOR_T050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1_H
#ifndef X931SECURERANDOM_T1429B64B45579463D36C2548C2A4104570E0AAF0_H
#define X931SECURERANDOM_T1429B64B45579463D36C2548C2A4104570E0AAF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandom
struct  X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0  : public SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandom::mPredictionResistant
	bool ___mPredictionResistant_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandom::mRandomSource
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mRandomSource_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931Rng BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Prng.X931SecureRandom::mDrbg
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B * ___mDrbg_9;

public:
	inline static int32_t get_offset_of_mPredictionResistant_7() { return static_cast<int32_t>(offsetof(X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0, ___mPredictionResistant_7)); }
	inline bool get_mPredictionResistant_7() const { return ___mPredictionResistant_7; }
	inline bool* get_address_of_mPredictionResistant_7() { return &___mPredictionResistant_7; }
	inline void set_mPredictionResistant_7(bool value)
	{
		___mPredictionResistant_7 = value;
	}

	inline static int32_t get_offset_of_mRandomSource_8() { return static_cast<int32_t>(offsetof(X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0, ___mRandomSource_8)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mRandomSource_8() const { return ___mRandomSource_8; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mRandomSource_8() { return &___mRandomSource_8; }
	inline void set_mRandomSource_8(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mRandomSource_8 = value;
		Il2CppCodeGenWriteBarrier((&___mRandomSource_8), value);
	}

	inline static int32_t get_offset_of_mDrbg_9() { return static_cast<int32_t>(offsetof(X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0, ___mDrbg_9)); }
	inline X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B * get_mDrbg_9() const { return ___mDrbg_9; }
	inline X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B ** get_address_of_mDrbg_9() { return &___mDrbg_9; }
	inline void set_mDrbg_9(X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B * value)
	{
		___mDrbg_9 = value;
		Il2CppCodeGenWriteBarrier((&___mDrbg_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X931SECURERANDOM_T1429B64B45579463D36C2548C2A4104570E0AAF0_H
#ifndef TLSECDSASIGNER_T78E29228CEAA8DDC93255B9814A79F312CBDE5B2_H
#define TLSECDSASIGNER_T78E29228CEAA8DDC93255B9814A79F312CBDE5B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsECDsaSigner
struct  TlsECDsaSigner_t78E29228CEAA8DDC93255B9814A79F312CBDE5B2  : public TlsDsaSigner_tE1F0A0D2E8E3C3344435CD48A8C6EEEFFB81994E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDSASIGNER_T78E29228CEAA8DDC93255B9814A79F312CBDE5B2_H
#ifndef TLSPROTOCOL_T115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1_H
#define TLSPROTOCOL_T115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct  TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mApplicationDataQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mApplicationDataQueue_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAlertQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mAlertQueue_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueue BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mHandshakeQueue
	ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * ___mHandshakeQueue_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.RecordStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mRecordStream
	RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * ___mRecordStream_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mSecureRandom_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsStream
	TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 * ___mTlsStream_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClosed
	bool ___mClosed_26;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mFailedWithError
	bool ___mFailedWithError_27;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataReady
	bool ___mAppDataReady_28;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataSplitEnabled
	bool ___mAppDataSplitEnabled_29;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataSplitMode
	int32_t ___mAppDataSplitMode_30;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectedVerifyData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mExpectedVerifyData_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsSession BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsSession
	RuntimeObject* ___mTlsSession_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SessionParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSessionParameters
	SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * ___mSessionParameters_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.SecurityParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecurityParameters
	SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * ___mSecurityParameters_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.Certificate BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mPeerCertificate
	Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * ___mPeerCertificate_35;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mOfferedCipherSuites_36;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCompressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mOfferedCompressionMethods_37;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClientExtensions
	RuntimeObject* ___mClientExtensions_38;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mServerExtensions
	RuntimeObject* ___mServerExtensions_39;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mConnectionState
	int16_t ___mConnectionState_40;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mResumedSession
	bool ___mResumedSession_41;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mReceivedChangeCipherSpec
	bool ___mReceivedChangeCipherSpec_42;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRenegotiation
	bool ___mSecureRenegotiation_43;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAllowCertificateStatus
	bool ___mAllowCertificateStatus_44;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectSessionTicket
	bool ___mExpectSessionTicket_45;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mBlocking
	bool ___mBlocking_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mInputBuffers
	ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * ___mInputBuffers_47;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.ByteQueueStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOutputBuffer
	ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * ___mOutputBuffer_48;

public:
	inline static int32_t get_offset_of_mApplicationDataQueue_20() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mApplicationDataQueue_20)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mApplicationDataQueue_20() const { return ___mApplicationDataQueue_20; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mApplicationDataQueue_20() { return &___mApplicationDataQueue_20; }
	inline void set_mApplicationDataQueue_20(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mApplicationDataQueue_20 = value;
		Il2CppCodeGenWriteBarrier((&___mApplicationDataQueue_20), value);
	}

	inline static int32_t get_offset_of_mAlertQueue_21() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAlertQueue_21)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mAlertQueue_21() const { return ___mAlertQueue_21; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mAlertQueue_21() { return &___mAlertQueue_21; }
	inline void set_mAlertQueue_21(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mAlertQueue_21 = value;
		Il2CppCodeGenWriteBarrier((&___mAlertQueue_21), value);
	}

	inline static int32_t get_offset_of_mHandshakeQueue_22() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mHandshakeQueue_22)); }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * get_mHandshakeQueue_22() const { return ___mHandshakeQueue_22; }
	inline ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 ** get_address_of_mHandshakeQueue_22() { return &___mHandshakeQueue_22; }
	inline void set_mHandshakeQueue_22(ByteQueue_t6B86227D30D30F5B463C2A08778E38DE7F30FF65 * value)
	{
		___mHandshakeQueue_22 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeQueue_22), value);
	}

	inline static int32_t get_offset_of_mRecordStream_23() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mRecordStream_23)); }
	inline RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * get_mRecordStream_23() const { return ___mRecordStream_23; }
	inline RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 ** get_address_of_mRecordStream_23() { return &___mRecordStream_23; }
	inline void set_mRecordStream_23(RecordStream_tDF3A2790B114D5AF6244BD2ECFD132E5B2AA0088 * value)
	{
		___mRecordStream_23 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordStream_23), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_24() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSecureRandom_24)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mSecureRandom_24() const { return ___mSecureRandom_24; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mSecureRandom_24() { return &___mSecureRandom_24; }
	inline void set_mSecureRandom_24(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mSecureRandom_24 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_24), value);
	}

	inline static int32_t get_offset_of_mTlsStream_25() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mTlsStream_25)); }
	inline TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 * get_mTlsStream_25() const { return ___mTlsStream_25; }
	inline TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 ** get_address_of_mTlsStream_25() { return &___mTlsStream_25; }
	inline void set_mTlsStream_25(TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49 * value)
	{
		___mTlsStream_25 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsStream_25), value);
	}

	inline static int32_t get_offset_of_mClosed_26() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mClosed_26)); }
	inline bool get_mClosed_26() const { return ___mClosed_26; }
	inline bool* get_address_of_mClosed_26() { return &___mClosed_26; }
	inline void set_mClosed_26(bool value)
	{
		___mClosed_26 = value;
	}

	inline static int32_t get_offset_of_mFailedWithError_27() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mFailedWithError_27)); }
	inline bool get_mFailedWithError_27() const { return ___mFailedWithError_27; }
	inline bool* get_address_of_mFailedWithError_27() { return &___mFailedWithError_27; }
	inline void set_mFailedWithError_27(bool value)
	{
		___mFailedWithError_27 = value;
	}

	inline static int32_t get_offset_of_mAppDataReady_28() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAppDataReady_28)); }
	inline bool get_mAppDataReady_28() const { return ___mAppDataReady_28; }
	inline bool* get_address_of_mAppDataReady_28() { return &___mAppDataReady_28; }
	inline void set_mAppDataReady_28(bool value)
	{
		___mAppDataReady_28 = value;
	}

	inline static int32_t get_offset_of_mAppDataSplitEnabled_29() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAppDataSplitEnabled_29)); }
	inline bool get_mAppDataSplitEnabled_29() const { return ___mAppDataSplitEnabled_29; }
	inline bool* get_address_of_mAppDataSplitEnabled_29() { return &___mAppDataSplitEnabled_29; }
	inline void set_mAppDataSplitEnabled_29(bool value)
	{
		___mAppDataSplitEnabled_29 = value;
	}

	inline static int32_t get_offset_of_mAppDataSplitMode_30() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAppDataSplitMode_30)); }
	inline int32_t get_mAppDataSplitMode_30() const { return ___mAppDataSplitMode_30; }
	inline int32_t* get_address_of_mAppDataSplitMode_30() { return &___mAppDataSplitMode_30; }
	inline void set_mAppDataSplitMode_30(int32_t value)
	{
		___mAppDataSplitMode_30 = value;
	}

	inline static int32_t get_offset_of_mExpectedVerifyData_31() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mExpectedVerifyData_31)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mExpectedVerifyData_31() const { return ___mExpectedVerifyData_31; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mExpectedVerifyData_31() { return &___mExpectedVerifyData_31; }
	inline void set_mExpectedVerifyData_31(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mExpectedVerifyData_31 = value;
		Il2CppCodeGenWriteBarrier((&___mExpectedVerifyData_31), value);
	}

	inline static int32_t get_offset_of_mTlsSession_32() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mTlsSession_32)); }
	inline RuntimeObject* get_mTlsSession_32() const { return ___mTlsSession_32; }
	inline RuntimeObject** get_address_of_mTlsSession_32() { return &___mTlsSession_32; }
	inline void set_mTlsSession_32(RuntimeObject* value)
	{
		___mTlsSession_32 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSession_32), value);
	}

	inline static int32_t get_offset_of_mSessionParameters_33() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSessionParameters_33)); }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * get_mSessionParameters_33() const { return ___mSessionParameters_33; }
	inline SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC ** get_address_of_mSessionParameters_33() { return &___mSessionParameters_33; }
	inline void set_mSessionParameters_33(SessionParameters_tCE98F9F6B2A86A48C9EFCFDCAD30A28FFAA993DC * value)
	{
		___mSessionParameters_33 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionParameters_33), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_34() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSecurityParameters_34)); }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * get_mSecurityParameters_34() const { return ___mSecurityParameters_34; }
	inline SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA ** get_address_of_mSecurityParameters_34() { return &___mSecurityParameters_34; }
	inline void set_mSecurityParameters_34(SecurityParameters_t44C6864C78661F178B2CAFDBB37D1029E9BE6AFA * value)
	{
		___mSecurityParameters_34 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_34), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_35() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mPeerCertificate_35)); }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * get_mPeerCertificate_35() const { return ___mPeerCertificate_35; }
	inline Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 ** get_address_of_mPeerCertificate_35() { return &___mPeerCertificate_35; }
	inline void set_mPeerCertificate_35(Certificate_tA446BDBAD6449500DD9E6401FCF3648F1F9CE5D3 * value)
	{
		___mPeerCertificate_35 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_35), value);
	}

	inline static int32_t get_offset_of_mOfferedCipherSuites_36() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mOfferedCipherSuites_36)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mOfferedCipherSuites_36() const { return ___mOfferedCipherSuites_36; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mOfferedCipherSuites_36() { return &___mOfferedCipherSuites_36; }
	inline void set_mOfferedCipherSuites_36(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mOfferedCipherSuites_36 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCipherSuites_36), value);
	}

	inline static int32_t get_offset_of_mOfferedCompressionMethods_37() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mOfferedCompressionMethods_37)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mOfferedCompressionMethods_37() const { return ___mOfferedCompressionMethods_37; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mOfferedCompressionMethods_37() { return &___mOfferedCompressionMethods_37; }
	inline void set_mOfferedCompressionMethods_37(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mOfferedCompressionMethods_37 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCompressionMethods_37), value);
	}

	inline static int32_t get_offset_of_mClientExtensions_38() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mClientExtensions_38)); }
	inline RuntimeObject* get_mClientExtensions_38() const { return ___mClientExtensions_38; }
	inline RuntimeObject** get_address_of_mClientExtensions_38() { return &___mClientExtensions_38; }
	inline void set_mClientExtensions_38(RuntimeObject* value)
	{
		___mClientExtensions_38 = value;
		Il2CppCodeGenWriteBarrier((&___mClientExtensions_38), value);
	}

	inline static int32_t get_offset_of_mServerExtensions_39() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mServerExtensions_39)); }
	inline RuntimeObject* get_mServerExtensions_39() const { return ___mServerExtensions_39; }
	inline RuntimeObject** get_address_of_mServerExtensions_39() { return &___mServerExtensions_39; }
	inline void set_mServerExtensions_39(RuntimeObject* value)
	{
		___mServerExtensions_39 = value;
		Il2CppCodeGenWriteBarrier((&___mServerExtensions_39), value);
	}

	inline static int32_t get_offset_of_mConnectionState_40() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mConnectionState_40)); }
	inline int16_t get_mConnectionState_40() const { return ___mConnectionState_40; }
	inline int16_t* get_address_of_mConnectionState_40() { return &___mConnectionState_40; }
	inline void set_mConnectionState_40(int16_t value)
	{
		___mConnectionState_40 = value;
	}

	inline static int32_t get_offset_of_mResumedSession_41() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mResumedSession_41)); }
	inline bool get_mResumedSession_41() const { return ___mResumedSession_41; }
	inline bool* get_address_of_mResumedSession_41() { return &___mResumedSession_41; }
	inline void set_mResumedSession_41(bool value)
	{
		___mResumedSession_41 = value;
	}

	inline static int32_t get_offset_of_mReceivedChangeCipherSpec_42() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mReceivedChangeCipherSpec_42)); }
	inline bool get_mReceivedChangeCipherSpec_42() const { return ___mReceivedChangeCipherSpec_42; }
	inline bool* get_address_of_mReceivedChangeCipherSpec_42() { return &___mReceivedChangeCipherSpec_42; }
	inline void set_mReceivedChangeCipherSpec_42(bool value)
	{
		___mReceivedChangeCipherSpec_42 = value;
	}

	inline static int32_t get_offset_of_mSecureRenegotiation_43() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mSecureRenegotiation_43)); }
	inline bool get_mSecureRenegotiation_43() const { return ___mSecureRenegotiation_43; }
	inline bool* get_address_of_mSecureRenegotiation_43() { return &___mSecureRenegotiation_43; }
	inline void set_mSecureRenegotiation_43(bool value)
	{
		___mSecureRenegotiation_43 = value;
	}

	inline static int32_t get_offset_of_mAllowCertificateStatus_44() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mAllowCertificateStatus_44)); }
	inline bool get_mAllowCertificateStatus_44() const { return ___mAllowCertificateStatus_44; }
	inline bool* get_address_of_mAllowCertificateStatus_44() { return &___mAllowCertificateStatus_44; }
	inline void set_mAllowCertificateStatus_44(bool value)
	{
		___mAllowCertificateStatus_44 = value;
	}

	inline static int32_t get_offset_of_mExpectSessionTicket_45() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mExpectSessionTicket_45)); }
	inline bool get_mExpectSessionTicket_45() const { return ___mExpectSessionTicket_45; }
	inline bool* get_address_of_mExpectSessionTicket_45() { return &___mExpectSessionTicket_45; }
	inline void set_mExpectSessionTicket_45(bool value)
	{
		___mExpectSessionTicket_45 = value;
	}

	inline static int32_t get_offset_of_mBlocking_46() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mBlocking_46)); }
	inline bool get_mBlocking_46() const { return ___mBlocking_46; }
	inline bool* get_address_of_mBlocking_46() { return &___mBlocking_46; }
	inline void set_mBlocking_46(bool value)
	{
		___mBlocking_46 = value;
	}

	inline static int32_t get_offset_of_mInputBuffers_47() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mInputBuffers_47)); }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * get_mInputBuffers_47() const { return ___mInputBuffers_47; }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A ** get_address_of_mInputBuffers_47() { return &___mInputBuffers_47; }
	inline void set_mInputBuffers_47(ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * value)
	{
		___mInputBuffers_47 = value;
		Il2CppCodeGenWriteBarrier((&___mInputBuffers_47), value);
	}

	inline static int32_t get_offset_of_mOutputBuffer_48() { return static_cast<int32_t>(offsetof(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1, ___mOutputBuffer_48)); }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * get_mOutputBuffer_48() const { return ___mOutputBuffer_48; }
	inline ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A ** get_address_of_mOutputBuffer_48() { return &___mOutputBuffer_48; }
	inline void set_mOutputBuffer_48(ByteQueueStream_t4B80A39B7C5F36BFCEEBE915AE6F825D98805B8A * value)
	{
		___mOutputBuffer_48 = value;
		Il2CppCodeGenWriteBarrier((&___mOutputBuffer_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOL_T115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1_H
#ifndef TLSSTREAM_TA02B58EBAEB61225628FB0E93AFF1A97B7EADF49_H
#define TLSSTREAM_TA02B58EBAEB61225628FB0E93AFF1A97B7EADF49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStream
struct  TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsStream::handler
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 * ___handler_5;

public:
	inline static int32_t get_offset_of_handler_5() { return static_cast<int32_t>(offsetof(TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49, ___handler_5)); }
	inline TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 * get_handler_5() const { return ___handler_5; }
	inline TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 ** get_address_of_handler_5() { return &___handler_5; }
	inline void set_handler_5(TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1 * value)
	{
		___handler_5 = value;
		Il2CppCodeGenWriteBarrier((&___handler_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAM_TA02B58EBAEB61225628FB0E93AFF1A97B7EADF49_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef BUFFER_T443824CE041E422156B9D23C39538DBF4867327C_H
#define BUFFER_T443824CE041E422156B9D23C39538DBF4867327C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519Signer_Buffer
struct  Buffer_t443824CE041E422156B9D23C39538DBF4867327C  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_T443824CE041E422156B9D23C39538DBF4867327C_H
#ifndef BUFFER_TB384106F0A7CBA8709D4DC00B34A3B46502D6A86_H
#define BUFFER_TB384106F0A7CBA8709D4DC00B34A3B46502D6A86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed25519ctxSigner_Buffer
struct  Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_TB384106F0A7CBA8709D4DC00B34A3B46502D6A86_H
#ifndef BUFFER_TDB8BB49A1F261DA0208890AC82E54D2C4DDB5542_H
#define BUFFER_TDB8BB49A1F261DA0208890AC82E54D2C4DDB5542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Signers.Ed448Signer_Buffer
struct  Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_TDB8BB49A1F261DA0208890AC82E54D2C4DDB5542_H
#ifndef TLSCLIENTPROTOCOL_TBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152_H
#define TLSCLIENTPROTOCOL_TBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol
struct  TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152  : public TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClient BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClient
	RuntimeObject* ___mTlsClient_49;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClientContext
	TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * ___mTlsClientContext_50;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mSelectedSessionID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSelectedSessionID_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mKeyExchange
	RuntimeObject* ___mKeyExchange_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsAuthentication BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mAuthentication
	RuntimeObject* ___mAuthentication_53;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateStatus
	CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * ___mCertificateStatus_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateRequest
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * ___mCertificateRequest_55;

public:
	inline static int32_t get_offset_of_mTlsClient_49() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mTlsClient_49)); }
	inline RuntimeObject* get_mTlsClient_49() const { return ___mTlsClient_49; }
	inline RuntimeObject** get_address_of_mTlsClient_49() { return &___mTlsClient_49; }
	inline void set_mTlsClient_49(RuntimeObject* value)
	{
		___mTlsClient_49 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsClient_49), value);
	}

	inline static int32_t get_offset_of_mTlsClientContext_50() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mTlsClientContext_50)); }
	inline TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * get_mTlsClientContext_50() const { return ___mTlsClientContext_50; }
	inline TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 ** get_address_of_mTlsClientContext_50() { return &___mTlsClientContext_50; }
	inline void set_mTlsClientContext_50(TlsClientContextImpl_tFCA01255E21FD4D7C25645D3EEE4BCA2030108C0 * value)
	{
		___mTlsClientContext_50 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsClientContext_50), value);
	}

	inline static int32_t get_offset_of_mSelectedSessionID_51() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mSelectedSessionID_51)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSelectedSessionID_51() const { return ___mSelectedSessionID_51; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSelectedSessionID_51() { return &___mSelectedSessionID_51; }
	inline void set_mSelectedSessionID_51(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSelectedSessionID_51 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectedSessionID_51), value);
	}

	inline static int32_t get_offset_of_mKeyExchange_52() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mKeyExchange_52)); }
	inline RuntimeObject* get_mKeyExchange_52() const { return ___mKeyExchange_52; }
	inline RuntimeObject** get_address_of_mKeyExchange_52() { return &___mKeyExchange_52; }
	inline void set_mKeyExchange_52(RuntimeObject* value)
	{
		___mKeyExchange_52 = value;
		Il2CppCodeGenWriteBarrier((&___mKeyExchange_52), value);
	}

	inline static int32_t get_offset_of_mAuthentication_53() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mAuthentication_53)); }
	inline RuntimeObject* get_mAuthentication_53() const { return ___mAuthentication_53; }
	inline RuntimeObject** get_address_of_mAuthentication_53() { return &___mAuthentication_53; }
	inline void set_mAuthentication_53(RuntimeObject* value)
	{
		___mAuthentication_53 = value;
		Il2CppCodeGenWriteBarrier((&___mAuthentication_53), value);
	}

	inline static int32_t get_offset_of_mCertificateStatus_54() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mCertificateStatus_54)); }
	inline CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * get_mCertificateStatus_54() const { return ___mCertificateStatus_54; }
	inline CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB ** get_address_of_mCertificateStatus_54() { return &___mCertificateStatus_54; }
	inline void set_mCertificateStatus_54(CertificateStatus_t7BFBAE146415307C61BF67D12531E7118629F7EB * value)
	{
		___mCertificateStatus_54 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateStatus_54), value);
	}

	inline static int32_t get_offset_of_mCertificateRequest_55() { return static_cast<int32_t>(offsetof(TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152, ___mCertificateRequest_55)); }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * get_mCertificateRequest_55() const { return ___mCertificateRequest_55; }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 ** get_address_of_mCertificateRequest_55() { return &___mCertificateRequest_55; }
	inline void set_mCertificateRequest_55(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * value)
	{
		___mCertificateRequest_55 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateRequest_55), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTPROTOCOL_TBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152_H
#ifndef TLSEXCEPTION_TF090C606ED59D3FE76D82B4B3A33C61A5686683C_H
#define TLSEXCEPTION_TF090C606ED59D3FE76D82B4B3A33C61A5686683C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsException
struct  TlsException_tF090C606ED59D3FE76D82B4B3A33C61A5686683C  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSEXCEPTION_TF090C606ED59D3FE76D82B4B3A33C61A5686683C_H
#ifndef HANDSHAKEMESSAGE_T07EC240AA0C603DB70238CA21016CFB7CB08A922_H
#define HANDSHAKEMESSAGE_T07EC240AA0C603DB70238CA21016CFB7CB08A922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocol_HandshakeMessage
struct  HandshakeMessage_t07EC240AA0C603DB70238CA21016CFB7CB08A922  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEMESSAGE_T07EC240AA0C603DB70238CA21016CFB7CB08A922_H
#ifndef TLSSERVERPROTOCOL_T9CF0353069621D2E44BB513371A991EA8B3259A4_H
#define TLSSERVERPROTOCOL_T9CF0353069621D2E44BB513371A991EA8B3259A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol
struct  TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4  : public TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServer BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mTlsServer
	RuntimeObject* ___mTlsServer_49;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerContextImpl BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mTlsServerContext
	TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE * ___mTlsServerContext_50;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsKeyExchange BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mKeyExchange
	RuntimeObject* ___mKeyExchange_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsCredentials BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mServerCredentials
	RuntimeObject* ___mServerCredentials_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.CertificateRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mCertificateRequest
	CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * ___mCertificateRequest_53;
	// System.Int16 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mClientCertificateType
	int16_t ___mClientCertificateType_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsServerProtocol::mPrepareFinishHash
	RuntimeObject* ___mPrepareFinishHash_55;

public:
	inline static int32_t get_offset_of_mTlsServer_49() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mTlsServer_49)); }
	inline RuntimeObject* get_mTlsServer_49() const { return ___mTlsServer_49; }
	inline RuntimeObject** get_address_of_mTlsServer_49() { return &___mTlsServer_49; }
	inline void set_mTlsServer_49(RuntimeObject* value)
	{
		___mTlsServer_49 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsServer_49), value);
	}

	inline static int32_t get_offset_of_mTlsServerContext_50() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mTlsServerContext_50)); }
	inline TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE * get_mTlsServerContext_50() const { return ___mTlsServerContext_50; }
	inline TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE ** get_address_of_mTlsServerContext_50() { return &___mTlsServerContext_50; }
	inline void set_mTlsServerContext_50(TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE * value)
	{
		___mTlsServerContext_50 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsServerContext_50), value);
	}

	inline static int32_t get_offset_of_mKeyExchange_51() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mKeyExchange_51)); }
	inline RuntimeObject* get_mKeyExchange_51() const { return ___mKeyExchange_51; }
	inline RuntimeObject** get_address_of_mKeyExchange_51() { return &___mKeyExchange_51; }
	inline void set_mKeyExchange_51(RuntimeObject* value)
	{
		___mKeyExchange_51 = value;
		Il2CppCodeGenWriteBarrier((&___mKeyExchange_51), value);
	}

	inline static int32_t get_offset_of_mServerCredentials_52() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mServerCredentials_52)); }
	inline RuntimeObject* get_mServerCredentials_52() const { return ___mServerCredentials_52; }
	inline RuntimeObject** get_address_of_mServerCredentials_52() { return &___mServerCredentials_52; }
	inline void set_mServerCredentials_52(RuntimeObject* value)
	{
		___mServerCredentials_52 = value;
		Il2CppCodeGenWriteBarrier((&___mServerCredentials_52), value);
	}

	inline static int32_t get_offset_of_mCertificateRequest_53() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mCertificateRequest_53)); }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * get_mCertificateRequest_53() const { return ___mCertificateRequest_53; }
	inline CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 ** get_address_of_mCertificateRequest_53() { return &___mCertificateRequest_53; }
	inline void set_mCertificateRequest_53(CertificateRequest_t5408DC90A7608121F4630E67FE3959536FACDDC9 * value)
	{
		___mCertificateRequest_53 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateRequest_53), value);
	}

	inline static int32_t get_offset_of_mClientCertificateType_54() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mClientCertificateType_54)); }
	inline int16_t get_mClientCertificateType_54() const { return ___mClientCertificateType_54; }
	inline int16_t* get_address_of_mClientCertificateType_54() { return &___mClientCertificateType_54; }
	inline void set_mClientCertificateType_54(int16_t value)
	{
		___mClientCertificateType_54 = value;
	}

	inline static int32_t get_offset_of_mPrepareFinishHash_55() { return static_cast<int32_t>(offsetof(TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4, ___mPrepareFinishHash_55)); }
	inline RuntimeObject* get_mPrepareFinishHash_55() const { return ___mPrepareFinishHash_55; }
	inline RuntimeObject** get_address_of_mPrepareFinishHash_55() { return &___mPrepareFinishHash_55; }
	inline void set_mPrepareFinishHash_55(RuntimeObject* value)
	{
		___mPrepareFinishHash_55 = value;
		Il2CppCodeGenWriteBarrier((&___mPrepareFinishHash_55), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERPROTOCOL_T9CF0353069621D2E44BB513371A991EA8B3259A4_H
#ifndef ENDOFSTREAMEXCEPTION_T1B47BA867EC337F83056C2833A59293754AAC01F_H
#define ENDOFSTREAMEXCEPTION_T1B47BA867EC337F83056C2833A59293754AAC01F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.EndOfStreamException
struct  EndOfStreamException_t1B47BA867EC337F83056C2833A59293754AAC01F  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDOFSTREAMEXCEPTION_T1B47BA867EC337F83056C2833A59293754AAC01F_H
#ifndef TLSFATALALERT_TA2003E737B4889BCDF57860D501BA94B5143DCF8_H
#define TLSFATALALERT_TA2003E737B4889BCDF57860D501BA94B5143DCF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsFatalAlert
struct  TlsFatalAlert_tA2003E737B4889BCDF57860D501BA94B5143DCF8  : public TlsException_tF090C606ED59D3FE76D82B4B3A33C61A5686683C
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsFatalAlert::alertDescription
	uint8_t ___alertDescription_18;

public:
	inline static int32_t get_offset_of_alertDescription_18() { return static_cast<int32_t>(offsetof(TlsFatalAlert_tA2003E737B4889BCDF57860D501BA94B5143DCF8, ___alertDescription_18)); }
	inline uint8_t get_alertDescription_18() const { return ___alertDescription_18; }
	inline uint8_t* get_address_of_alertDescription_18() { return &___alertDescription_18; }
	inline void set_alertDescription_18(uint8_t value)
	{
		___alertDescription_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSFATALALERT_TA2003E737B4889BCDF57860D501BA94B5143DCF8_H
#ifndef TLSFATALALERTRECEIVED_TA0947AF016A92DAE59954BD9B3538E3D9935E6E0_H
#define TLSFATALALERTRECEIVED_TA0947AF016A92DAE59954BD9B3538E3D9935E6E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsFatalAlertReceived
struct  TlsFatalAlertReceived_tA0947AF016A92DAE59954BD9B3538E3D9935E6E0  : public TlsException_tF090C606ED59D3FE76D82B4B3A33C61A5686683C
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsFatalAlertReceived::alertDescription
	uint8_t ___alertDescription_18;

public:
	inline static int32_t get_offset_of_alertDescription_18() { return static_cast<int32_t>(offsetof(TlsFatalAlertReceived_tA0947AF016A92DAE59954BD9B3538E3D9935E6E0, ___alertDescription_18)); }
	inline uint8_t get_alertDescription_18() const { return ___alertDescription_18; }
	inline uint8_t* get_address_of_alertDescription_18() { return &___alertDescription_18; }
	inline void set_alertDescription_18(uint8_t value)
	{
		___alertDescription_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSFATALALERTRECEIVED_TA0947AF016A92DAE59954BD9B3538E3D9935E6E0_H
#ifndef TLSNOCLOSENOTIFYEXCEPTION_TAAE259C5D9E98A63E37F977FD76C6D54BA72DA88_H
#define TLSNOCLOSENOTIFYEXCEPTION_TAAE259C5D9E98A63E37F977FD76C6D54BA72DA88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsNoCloseNotifyException
struct  TlsNoCloseNotifyException_tAAE259C5D9E98A63E37F977FD76C6D54BA72DA88  : public EndOfStreamException_t1B47BA867EC337F83056C2833A59293754AAC01F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSNOCLOSENOTIFYEXCEPTION_TAAE259C5D9E98A63E37F977FD76C6D54BA72DA88_H
#ifndef TLSPROTOCOLHANDLER_T80AF0A2DD137033F84A36CB5D615653AAF209963_H
#define TLSPROTOCOLHANDLER_T80AF0A2DD137033F84A36CB5D615653AAF209963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls.TlsProtocolHandler
struct  TlsProtocolHandler_t80AF0A2DD137033F84A36CB5D615653AAF209963  : public TlsClientProtocol_tBD3F26A7FC2DD8AE20262DA9CD7F46A1BD283152
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOLHANDLER_T80AF0A2DD137033F84A36CB5D615653AAF209963_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4600[8] = 
{
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mTlsSigner_3(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mNamedCurves_4(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mClientECPointFormats_5(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mServerECPointFormats_6(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mServerPublicKey_7(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mAgreementCredentials_8(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mECAgreePrivateKey_9(),
	TlsECDHKeyExchange_t0402E5233B545D8D5B8DC9738C90B952BCF2DB19::get_offset_of_mECAgreePublicKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (TlsECDsaSigner_t78E29228CEAA8DDC93255B9814A79F312CBDE5B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (TlsException_tF090C606ED59D3FE76D82B4B3A33C61A5686683C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (TlsExtensionsUtilities_t000CC9BA9D75D72554731E8F516DAB794703706C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (TlsFatalAlert_tA2003E737B4889BCDF57860D501BA94B5143DCF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[1] = 
{
	TlsFatalAlert_tA2003E737B4889BCDF57860D501BA94B5143DCF8::get_offset_of_alertDescription_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (TlsFatalAlertReceived_tA0947AF016A92DAE59954BD9B3538E3D9935E6E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4606[1] = 
{
	TlsFatalAlertReceived_tA0947AF016A92DAE59954BD9B3538E3D9935E6E0::get_offset_of_alertDescription_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4609[6] = 
{
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E::get_offset_of_context_0(),
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E::get_offset_of_secret_1(),
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E::get_offset_of_mac_2(),
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E::get_offset_of_digestBlockSize_3(),
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E::get_offset_of_digestOverhead_4(),
	TlsMac_tE6856EA9C9A3883B45988264A7BB1F37B01C207E::get_offset_of_macLength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (TlsNoCloseNotifyException_tAAE259C5D9E98A63E37F977FD76C6D54BA72DA88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4611[3] = 
{
	TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102::get_offset_of_context_0(),
	TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102::get_offset_of_writeMac_1(),
	TlsNullCipher_t85F07D1CD2054E503356A45C8B539C0D622F8102::get_offset_of_readMac_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (TlsNullCompression_tBD01500502A33DB81FBD50398DEDEFA9A559B4F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4614[49] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mApplicationDataQueue_20(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mAlertQueue_21(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mHandshakeQueue_22(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mRecordStream_23(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mSecureRandom_24(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mTlsStream_25(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mClosed_26(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mFailedWithError_27(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mAppDataReady_28(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mAppDataSplitEnabled_29(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mAppDataSplitMode_30(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mExpectedVerifyData_31(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mTlsSession_32(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mSessionParameters_33(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mSecurityParameters_34(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mPeerCertificate_35(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mOfferedCipherSuites_36(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mOfferedCompressionMethods_37(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mClientExtensions_38(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mServerExtensions_39(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mConnectionState_40(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mResumedSession_41(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mReceivedChangeCipherSpec_42(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mSecureRenegotiation_43(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mAllowCertificateStatus_44(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mExpectSessionTicket_45(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mBlocking_46(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mInputBuffers_47(),
	TlsProtocol_t115FE6C08F0EE4B7FDE863973ADE78EC04AE5CF1::get_offset_of_mOutputBuffer_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (HandshakeMessage_t07EC240AA0C603DB70238CA21016CFB7CB08A922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (TlsProtocolHandler_t80AF0A2DD137033F84A36CB5D615653AAF209963), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4619[17] = 
{
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mPskIdentity_3(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mPskIdentityManager_4(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mDHVerifier_5(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mDHParameters_6(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mNamedCurves_7(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mClientECPointFormats_8(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mServerECPointFormats_9(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mPskIdentityHint_10(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mPsk_11(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mDHAgreePrivateKey_12(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mDHAgreePublicKey_13(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mECAgreePrivateKey_14(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mECAgreePublicKey_15(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mServerPublicKey_16(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mRsaServerPublicKey_17(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mServerCredentials_18(),
	TlsPskKeyExchange_t2CD0C038811589FC9178D11A7185F7D1012621C9::get_offset_of_mPremasterSecret_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4620[4] = 
{
	TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1::get_offset_of_mServerPublicKey_3(),
	TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1::get_offset_of_mRsaServerPublicKey_4(),
	TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1::get_offset_of_mServerCredentials_5(),
	TlsRsaKeyExchange_t822F8F97568733C0DD7663167BDC18197078B3B1::get_offset_of_mPremasterSecret_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (TlsRsaSigner_t4D74FA5A99017314DA8C11D30129A75638624E0A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (TlsRsaUtilities_tCFFC08A3A1F68DA34C47F96C26D7CBEF3D94BA5A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (TlsServerContextImpl_t483AE72706A494F8082A01D126A7E55DE318B3FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[7] = 
{
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mTlsServer_49(),
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mTlsServerContext_50(),
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mKeyExchange_51(),
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mServerCredentials_52(),
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mCertificateRequest_53(),
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mClientCertificateType_54(),
	TlsServerProtocol_t9CF0353069621D2E44BB513371A991EA8B3259A4::get_offset_of_mPrepareFinishHash_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4628[3] = 
{
	TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06::get_offset_of_mSessionID_0(),
	TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06::get_offset_of_mSessionParameters_1(),
	TlsSessionImpl_t1E2257A7B3C081B0EE63A51D94F9F36462CBDD06::get_offset_of_mResumable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4633[12] = 
{
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mTlsSigner_3(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mGroupVerifier_4(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mIdentity_5(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mPassword_6(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mServerPublicKey_7(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mSrpGroup_8(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mSrpClient_9(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mSrpServer_10(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mSrpPeerCredentials_11(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mSrpVerifier_12(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mSrpSalt_13(),
	TlsSrpKeyExchange_t78066067B7953C9702A1850A66683F5AABDC22B3::get_offset_of_mServerCredentials_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4634[3] = 
{
	TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238::get_offset_of_mGroup_0(),
	TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238::get_offset_of_mVerifier_1(),
	TlsSrpLoginParameters_t1BFA31D45798FF49920039BC42EC594D6C11E238::get_offset_of_mSalt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (TlsSrpUtilities_t0B8FFACB092E5D9A7C5EF3A1FEF8D2D5DE588DE3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (TlsSRTPUtils_tA0BE37A29F4E32EB13E7CE71293BE616C4B48DF8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4637[1] = 
{
	TlsStream_tA02B58EBAEB61225628FB0E93AFF1A97B7EADF49::get_offset_of_handler_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4638[6] = 
{
	TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744::get_offset_of_context_0(),
	TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744::get_offset_of_encryptCipher_1(),
	TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744::get_offset_of_decryptCipher_2(),
	TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744::get_offset_of_writeMac_3(),
	TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744::get_offset_of_readMac_4(),
	TlsStreamCipher_t652FDC12B5566A0410A850E6AC09CD9332ECA744::get_offset_of_usesNonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C), -1, sizeof(TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4639[7] = 
{
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_EmptyBytes_0(),
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_EmptyShorts_1(),
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_EmptyInts_2(),
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_EmptyLongs_3(),
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_SSL_CLIENT_4(),
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_SSL_SERVER_5(),
	TlsUtilities_t9576CA748C9EC0B26E5CD9254F703579ECF0777C_StaticFields::get_offset_of_SSL3_CONST_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (UrlAndHash_t6EB3DDE69190DD0AB8CEE90C6BB2229934137302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4640[2] = 
{
	UrlAndHash_t6EB3DDE69190DD0AB8CEE90C6BB2229934137302::get_offset_of_mUrl_0(),
	UrlAndHash_t6EB3DDE69190DD0AB8CEE90C6BB2229934137302::get_offset_of_mSha1Hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (UserMappingType_tF44BE62B49B6BBA545C7E5BEC0B772B9D5ECABEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4641[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (UseSrtpData_tA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4642[2] = 
{
	UseSrtpData_tA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D::get_offset_of_mProtectionProfiles_0(),
	UseSrtpData_tA73AD553A0ECE5D6F0A2A83FF3DE297503E0D63D::get_offset_of_mMki_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4643[4] = 
{
	DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23::get_offset_of_dsa_0(),
	DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23::get_offset_of_digest_1(),
	DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23::get_offset_of_encoding_2(),
	DsaDigestSigner_t7A605E8FC5D9F0F29D6D3EF095C4D031C3276D23::get_offset_of_forSigning_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4644[3] = 
{
	DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C::get_offset_of_kCalculator_0(),
	DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C::get_offset_of_key_1(),
	DsaSigner_tFCB42C4316A4B9F4F79F8504DBFC3B0C1FD15C6C::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA), -1, sizeof(ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4645[4] = 
{
	ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA_StaticFields::get_offset_of_Eight_0(),
	ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA::get_offset_of_kCalculator_1(),
	ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA::get_offset_of_key_2(),
	ECDsaSigner_tD66C53659D6B01F2E457F569C0D1139A2E161EEA::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4646[3] = 
{
	ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005::get_offset_of_key_0(),
	ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005::get_offset_of_random_1(),
	ECGost3410Signer_t69F05BC41165FE707A05D2C7FE09601F39E1B005::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4647[3] = 
{
	ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9::get_offset_of_forSigning_0(),
	ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9::get_offset_of_key_1(),
	ECNRSigner_t23A29B78C8AF0A868C1B306126BCBABF9AE11AA9::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4648[5] = 
{
	Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359::get_offset_of_buffer_0(),
	Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359::get_offset_of_context_1(),
	Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359::get_offset_of_forSigning_2(),
	Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359::get_offset_of_privateKey_3(),
	Ed25519ctxSigner_t2CF0B6495F0BA6DB63D84CA556C7965966BC0359::get_offset_of_publicKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (Buffer_tB384106F0A7CBA8709D4DC00B34A3B46502D6A86), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4650[5] = 
{
	Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D::get_offset_of_prehash_0(),
	Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D::get_offset_of_context_1(),
	Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D::get_offset_of_forSigning_2(),
	Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D::get_offset_of_privateKey_3(),
	Ed25519phSigner_tD20CA75D7CD956BA254DC2D965087A9B4F192B6D::get_offset_of_publicKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[4] = 
{
	Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640::get_offset_of_buffer_0(),
	Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640::get_offset_of_forSigning_1(),
	Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640::get_offset_of_privateKey_2(),
	Ed25519Signer_tB6A37D7FB3C1519364BBEC1FEAA96F7578B8B640::get_offset_of_publicKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (Buffer_t443824CE041E422156B9D23C39538DBF4867327C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4653[5] = 
{
	Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004::get_offset_of_prehash_0(),
	Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004::get_offset_of_context_1(),
	Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004::get_offset_of_forSigning_2(),
	Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004::get_offset_of_privateKey_3(),
	Ed448phSigner_tCFFAE46AC32649A773D0488EF787B23D9BB88004::get_offset_of_publicKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4654[5] = 
{
	Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055::get_offset_of_buffer_0(),
	Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055::get_offset_of_context_1(),
	Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055::get_offset_of_forSigning_2(),
	Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055::get_offset_of_privateKey_3(),
	Ed448Signer_tBC5A73E10128EF23C9676F8AB0EB6CD95FC0F055::get_offset_of_publicKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (Buffer_tDB8BB49A1F261DA0208890AC82E54D2C4DDB5542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4656[3] = 
{
	GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557::get_offset_of_engine_0(),
	GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557::get_offset_of_digest_1(),
	GenericSigner_t68C6E33EDC7D508EF01ADCEF159B2CE15FA5B557::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4657[5] = 
{
	Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5::get_offset_of_digest_0(),
	Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5::get_offset_of_dsaSigner_1(),
	Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5::get_offset_of_size_2(),
	Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5::get_offset_of_halfSize_3(),
	Gost3410DigestSigner_tBB8889F8B95CC77337FD5FFF2DC0C28C2A7267F5::get_offset_of_forSigning_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (Gost3410Signer_t057B6F369C8D89E26A37EC552CA812D7DD2A7E85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4658[2] = 
{
	Gost3410Signer_t057B6F369C8D89E26A37EC552CA812D7DD2A7E85::get_offset_of_key_0(),
	Gost3410Signer_t057B6F369C8D89E26A37EC552CA812D7DD2A7E85::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4659[4] = 
{
	HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425::get_offset_of_hMac_0(),
	HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425::get_offset_of_K_1(),
	HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425::get_offset_of_V_2(),
	HMacDsaKCalculator_tC8CBD55698CA77683BC867F9645F2D831EDA7425::get_offset_of_n_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4662[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_digest_8(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_cipher_9(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_random_10(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_standardSalt_11(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_hLen_12(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_trailer_13(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_keyBits_14(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_block_15(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_mBuf_16(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_messageLength_17(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_saltLength_18(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_fullMessage_19(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_recoveredMessage_20(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_preSig_21(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_preBlock_22(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_preMStart_23(),
	Iso9796d2PssSigner_t5133D138584C8EAAB8C689A8A85609EAADEBC698::get_offset_of_preTLength_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4663[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_digest_8(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_cipher_9(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_trailer_10(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_keyBits_11(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_block_12(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_mBuf_13(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_messageLength_14(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_fullMessage_15(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_recoveredMessage_16(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_preSig_17(),
	Iso9796d2Signer_t086FE812AC52D1E59C6530EAB5E8A1CFACDDFCB4::get_offset_of_preBlock_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (IsoTrailers_tC17D8548B797661FBC98A6528955F94A6DDD544E), -1, sizeof(IsoTrailers_tC17D8548B797661FBC98A6528955F94A6DDD544E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4664[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	IsoTrailers_tC17D8548B797661FBC98A6528955F94A6DDD544E_StaticFields::get_offset_of_trailerMap_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2), -1, sizeof(PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4665[1] = 
{
	PlainDsaEncoding_tDD93DA1C1AA6A35D8E1E286AC2AE68FE4AB4FEE2_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4666[15] = 
{
	0,
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_contentDigest1_1(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_contentDigest2_2(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_mgfDigest_3(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_cipher_4(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_random_5(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_hLen_6(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_mgfhLen_7(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_sLen_8(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_sSet_9(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_emBits_10(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_salt_11(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_mDash_12(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_block_13(),
	PssSigner_tB2F38DFD2527BEC74B78C301231619792915EF6F::get_offset_of_trailer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (RandomDsaKCalculator_t218C24F0B03E145447BD2E2FD92AA6208CE4E6CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4667[2] = 
{
	RandomDsaKCalculator_t218C24F0B03E145447BD2E2FD92AA6208CE4E6CC::get_offset_of_q_0(),
	RandomDsaKCalculator_t218C24F0B03E145447BD2E2FD92AA6208CE4E6CC::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9), -1, sizeof(RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4668[5] = 
{
	RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9::get_offset_of_rsaEngine_0(),
	RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9::get_offset_of_algId_1(),
	RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9::get_offset_of_digest_2(),
	RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9::get_offset_of_forSigning_3(),
	RsaDigestSigner_tB4A9A7E3B26C0D0C46FED923700D318142E6AEA9_StaticFields::get_offset_of_oidMap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4669[7] = 
{
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_kCalculator_0(),
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_digest_1(),
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_encoding_2(),
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_ecParams_3(),
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_pubPoint_4(),
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_ecKey_5(),
	SM2Signer_tCE33319F3B53C6BF3647B2DE40E8903FEC6110D6::get_offset_of_z_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954), -1, sizeof(StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4670[1] = 
{
	StandardDsaEncoding_t82A5AA9DACE40E8E9B2AF4FDDA8C3CDD6B8BD954_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4671[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81::get_offset_of_digest_9(),
	X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81::get_offset_of_cipher_10(),
	X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81::get_offset_of_kParam_11(),
	X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81::get_offset_of_trailer_12(),
	X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81::get_offset_of_keyBits_13(),
	X931Signer_t193EDE12132D0C69F92C719BA888D002B838FA81::get_offset_of_block_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (BasicEntropySourceProvider_t19007444B2357EBDD30FF785A08CAAEC6A75444A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4672[2] = 
{
	BasicEntropySourceProvider_t19007444B2357EBDD30FF785A08CAAEC6A75444A::get_offset_of_mSecureRandom_0(),
	BasicEntropySourceProvider_t19007444B2357EBDD30FF785A08CAAEC6A75444A::get_offset_of_mPredictionResistant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4673[3] = 
{
	BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351::get_offset_of_mSecureRandom_0(),
	BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351::get_offset_of_mPredictionResistant_1(),
	BasicEntropySource_t828E01DF2667940BC1E5C7E906245C04E5B38351::get_offset_of_mEntropySize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (CryptoApiEntropySourceProvider_t408F886CA7FA701D993C6A7F78E4E2D32197E7B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4674[2] = 
{
	CryptoApiEntropySourceProvider_t408F886CA7FA701D993C6A7F78E4E2D32197E7B5::get_offset_of_mRng_0(),
	CryptoApiEntropySourceProvider_t408F886CA7FA701D993C6A7F78E4E2D32197E7B5::get_offset_of_mPredictionResistant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4675[3] = 
{
	CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC::get_offset_of_mRng_0(),
	CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC::get_offset_of_mPredictionResistant_1(),
	CryptoApiEntropySource_t96C41ACAD91992D559920020F6BB9C8247186BBC::get_offset_of_mEntropySize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (CryptoApiRandomGenerator_t3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4676[1] = 
{
	CryptoApiRandomGenerator_t3DF915CC767BAA367C3A0B3F3BB8A0649F0AEF6B::get_offset_of_rndProv_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4677[6] = 
{
	0,
	DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A::get_offset_of_stateCounter_1(),
	DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A::get_offset_of_seedCounter_2(),
	DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A::get_offset_of_digest_3(),
	DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A::get_offset_of_state_4(),
	DigestRandomGenerator_t5BDDECC03BC7DD7750FB1DA5E88C928A6BF4277A::get_offset_of_seed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (EntropyUtilities_tD12197BB2D9598056959C4ACD7543BAFAB04A36C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4681[3] = 
{
	ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A::get_offset_of_generator_0(),
	ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A::get_offset_of_window_1(),
	ReversedWindowGenerator_t171F7229177F9C5899E1B955817CFD447956572A::get_offset_of_windowCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4682[5] = 
{
	SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374::get_offset_of_mDrbgProvider_7(),
	SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374::get_offset_of_mPredictionResistant_8(),
	SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374::get_offset_of_mRandomSource_9(),
	SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374::get_offset_of_mEntropySource_10(),
	SP800SecureRandom_t0E78F8732DAAC0FA417DB9028221827B62DB3374::get_offset_of_mDrbg_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4683[5] = 
{
	SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C::get_offset_of_mRandom_0(),
	SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C::get_offset_of_mEntropySourceProvider_1(),
	SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C::get_offset_of_mPersonalizationString_2(),
	SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C::get_offset_of_mSecurityStrength_3(),
	SP800SecureRandomBuilder_t82710C7D53A63D36DDD25EB172AE96BF0DCB594C::get_offset_of_mEntropyBitsRequired_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4684[4] = 
{
	HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095::get_offset_of_mDigest_0(),
	HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095::get_offset_of_mNonce_1(),
	HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095::get_offset_of_mPersonalizationString_2(),
	HashDrbgProvider_tA03C959D27F2639D6D1C7B37E7D700547D406095::get_offset_of_mSecurityStrength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4685[4] = 
{
	HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7::get_offset_of_mHMac_0(),
	HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7::get_offset_of_mNonce_1(),
	HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7::get_offset_of_mPersonalizationString_2(),
	HMacDrbgProvider_t98868B7971D2965585C3CBBA63482D24005D7EC7::get_offset_of_mSecurityStrength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4686[5] = 
{
	CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F::get_offset_of_mBlockCipher_0(),
	CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F::get_offset_of_mKeySizeInBits_1(),
	CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F::get_offset_of_mNonce_2(),
	CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F::get_offset_of_mPersonalizationString_3(),
	CtrDrbgProvider_tAE3C31F9AD934C9071870463769945B88201E80F::get_offset_of_mSecurityStrength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (ThreadedSeedGenerator_tB3B4E6EB56967BAB9BCC1741D6CDC72444C4B23C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { sizeof (SeedGenerator_t050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4688[2] = 
{
	SeedGenerator_t050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1::get_offset_of_counter_0(),
	SeedGenerator_t050A4FA56F0E49C36A60D1040B6E3A3CACA2ABB1::get_offset_of_stop_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4689[3] = 
{
	VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71::get_offset_of_n_0(),
	VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71::get_offset_of_P_1(),
	VmpcRandomGenerator_t056DE51A28372AD537C06D583BCC0022FB8EDD71::get_offset_of_s_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4690[11] = 
{
	0,
	0,
	0,
	0,
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mEngine_4(),
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mEntropySource_5(),
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mDT_6(),
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mI_7(),
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mR_8(),
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mV_9(),
	X931Rng_tED3929E965136732FC0739FA158783E3BED94A4B::get_offset_of_mReseedCounter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4691[3] = 
{
	X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0::get_offset_of_mPredictionResistant_7(),
	X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0::get_offset_of_mRandomSource_8(),
	X931SecureRandom_t1429B64B45579463D36C2548C2A4104570E0AAF0::get_offset_of_mDrbg_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4692[3] = 
{
	X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76::get_offset_of_mRandom_0(),
	X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76::get_offset_of_mEntropySourceProvider_1(),
	X931SecureRandomBuilder_t91A1A537E20D0E756B998B49C3A5F50264624E76::get_offset_of_mDateTimeVector_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96), -1, sizeof(CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4693[14] = 
{
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields::get_offset_of_TDEA_RESEED_MAX_0(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields::get_offset_of_AES_RESEED_MAX_1(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields::get_offset_of_TDEA_MAX_BITS_REQUEST_2(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields::get_offset_of_AES_MAX_BITS_REQUEST_3(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mEntropySource_4(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mEngine_5(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mKeySizeInBits_6(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mSeedLength_7(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mSecurityStrength_8(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mKey_9(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mV_10(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mReseedCounter_11(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96::get_offset_of_mIsTdea_12(),
	CtrSP800Drbg_t8B55AD6B2EF25E3C671AE8EBC568419617FEAB96_StaticFields::get_offset_of_K_BITS_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (DrbgUtilities_tD53F1AC30016A4F2F9EC0238366A3064575D496A), -1, sizeof(DrbgUtilities_tD53F1AC30016A4F2F9EC0238366A3064575D496A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4694[1] = 
{
	DrbgUtilities_tD53F1AC30016A4F2F9EC0238366A3064575D496A_StaticFields::get_offset_of_maxSecurityStrengths_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { sizeof (HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5), -1, sizeof(HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4695[11] = 
{
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields::get_offset_of_ONE_0(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields::get_offset_of_RESEED_MAX_1(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields::get_offset_of_MAX_BITS_REQUEST_2(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5_StaticFields::get_offset_of_seedlens_3(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mDigest_4(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mEntropySource_5(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mSecurityStrength_6(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mSeedLength_7(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mV_8(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mC_9(),
	HashSP800Drbg_tAD380A6FCBA53B2E2A359F4BF7D8455455012CA5::get_offset_of_mReseedCounter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6), -1, sizeof(HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4696[8] = 
{
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6_StaticFields::get_offset_of_RESEED_MAX_0(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6_StaticFields::get_offset_of_MAX_BITS_REQUEST_1(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6::get_offset_of_mK_2(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6::get_offset_of_mV_3(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6::get_offset_of_mEntropySource_4(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6::get_offset_of_mHMac_5(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6::get_offset_of_mSecurityStrength_6(),
	HMacSP800Drbg_t6313607242AE0FF6C233E5BE89F07B448A7DA4F6::get_offset_of_mReseedCounter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { sizeof (AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4698[4] = 
{
	AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93::get_offset_of_associatedText_0(),
	AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93::get_offset_of_nonce_1(),
	AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93::get_offset_of_key_2(),
	AeadParameters_tEE08E2135B4F08D4C070C3EE72771C97BCF49F93::get_offset_of_macSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { sizeof (CcmParameters_tDEA0AB6441DFA994C80F3D4D207A0D5C01CBD543), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
