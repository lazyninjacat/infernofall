﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.Examples.Helpers.SampleBase
struct SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF;
// BestHTTP.Examples.Helpers.SelectorUI.Category
struct Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477;
// BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo
struct ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8;
// BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem
struct ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970;
// BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI
struct SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A;
// BestHTTP.Examples.SampleRoot
struct SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2;
// Newtonsoft.Json.IArrayPool`1<System.Char>
struct IArrayPool_1_t772D9C201F8403239411CF918637D5CE3238F981;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_tC743A5579C4FEE7AABBE8063B8A13C44C524E10C;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_tEAEB2EE1239E8F13092235DB634358C5BDE6EB8C;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t8CE53BA9C98C0CE46845BBEB4FEF453F676C0743;
// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB;
// Newtonsoft.Json.Utilities.PropertyNameTable
struct PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t2D9983352140740F4E7E45D45E1E03ED96E0D389;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.Comparison`1<BestHTTP.Examples.Helpers.SampleBase>
struct Comparison_1_t18E4F3AED3899EA03CEDA6EE5DD401DB5BC25CFB;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648;
// System.Func`1<Newtonsoft.Json.JsonSerializerSettings>
struct Func_1_t7B3A2AD45D8625DD47957C363E065BC07E02776B;
// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver>
struct Func_1_t861F98E547FCB9AB51D52E49717586C4ABB37685;
// System.Func`2<BestHTTP.Cookies.Cookie,System.Int64>
struct Func_2_tA58CF79FFDFC7DDCC4BF55DAE4DDDC99D47A33C7;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T57B0800FF641881416BC0907D3D2A59FE93BD873_H
#define U3CMODULEU3E_T57B0800FF641881416BC0907D3D2A59FE93BD873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t57B0800FF641881416BC0907D3D2A59FE93BD873 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T57B0800FF641881416BC0907D3D2A59FE93BD873_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_TE0B727F01216D8C06837B7BA42EA80E7CB9F1687_H
#define U3CU3EC_TE0B727F01216D8C06837B7BA42EA80E7CB9F1687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.Components.Cookies_<>c
struct  U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687_StaticFields
{
public:
	// BestHTTP.Examples.Helpers.Components.Cookies_<>c BestHTTP.Examples.Helpers.Components.Cookies_<>c::<>9
	U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687 * ___U3CU3E9_0;
	// System.Func`2<BestHTTP.Cookies.Cookie,System.Int64> BestHTTP.Examples.Helpers.Components.Cookies_<>c::<>9__6_0
	Func_2_tA58CF79FFDFC7DDCC4BF55DAE4DDDC99D47A33C7 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_tA58CF79FFDFC7DDCC4BF55DAE4DDDC99D47A33C7 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_tA58CF79FFDFC7DDCC4BF55DAE4DDDC99D47A33C7 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_tA58CF79FFDFC7DDCC4BF55DAE4DDDC99D47A33C7 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE0B727F01216D8C06837B7BA42EA80E7CB9F1687_H
#ifndef U3CU3EC_T6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_H
#define U3CU3EC_T6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI_<>c
struct  U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_StaticFields
{
public:
	// BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI_<>c BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI_<>c::<>9
	U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E * ___U3CU3E9_0;
	// System.Comparison`1<BestHTTP.Examples.Helpers.SampleBase> BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI_<>c::<>9__9_0
	Comparison_1_t18E4F3AED3899EA03CEDA6EE5DD401DB5BC25CFB * ___U3CU3E9__9_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_StaticFields, ___U3CU3E9__9_0_1)); }
	inline Comparison_1_t18E4F3AED3899EA03CEDA6EE5DD401DB5BC25CFB * get_U3CU3E9__9_0_1() const { return ___U3CU3E9__9_0_1; }
	inline Comparison_1_t18E4F3AED3899EA03CEDA6EE5DD401DB5BC25CFB ** get_address_of_U3CU3E9__9_0_1() { return &___U3CU3E9__9_0_1; }
	inline void set_U3CU3E9__9_0_1(Comparison_1_t18E4F3AED3899EA03CEDA6EE5DD401DB5BC25CFB * value)
	{
		___U3CU3E9__9_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_H
#ifndef JSONCONVERT_T41186D2C92A15E30FBEB8087981F668A711CE25F_H
#define JSONCONVERT_T41186D2C92A15E30FBEB8087981F668A711CE25F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConvert
struct  JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F  : public RuntimeObject
{
public:

public:
};

struct JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields
{
public:
	// System.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::<DefaultSettings>k__BackingField
	Func_1_t7B3A2AD45D8625DD47957C363E065BC07E02776B * ___U3CDefaultSettingsU3Ek__BackingField_0;
	// System.String Newtonsoft.Json.JsonConvert::True
	String_t* ___True_1;
	// System.String Newtonsoft.Json.JsonConvert::False
	String_t* ___False_2;
	// System.String Newtonsoft.Json.JsonConvert::Null
	String_t* ___Null_3;
	// System.String Newtonsoft.Json.JsonConvert::Undefined
	String_t* ___Undefined_4;
	// System.String Newtonsoft.Json.JsonConvert::PositiveInfinity
	String_t* ___PositiveInfinity_5;
	// System.String Newtonsoft.Json.JsonConvert::NegativeInfinity
	String_t* ___NegativeInfinity_6;
	// System.String Newtonsoft.Json.JsonConvert::NaN
	String_t* ___NaN_7;

public:
	inline static int32_t get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___U3CDefaultSettingsU3Ek__BackingField_0)); }
	inline Func_1_t7B3A2AD45D8625DD47957C363E065BC07E02776B * get_U3CDefaultSettingsU3Ek__BackingField_0() const { return ___U3CDefaultSettingsU3Ek__BackingField_0; }
	inline Func_1_t7B3A2AD45D8625DD47957C363E065BC07E02776B ** get_address_of_U3CDefaultSettingsU3Ek__BackingField_0() { return &___U3CDefaultSettingsU3Ek__BackingField_0; }
	inline void set_U3CDefaultSettingsU3Ek__BackingField_0(Func_1_t7B3A2AD45D8625DD47957C363E065BC07E02776B * value)
	{
		___U3CDefaultSettingsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultSettingsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___True_1)); }
	inline String_t* get_True_1() const { return ___True_1; }
	inline String_t** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(String_t* value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((&___True_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___False_2)); }
	inline String_t* get_False_2() const { return ___False_2; }
	inline String_t** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(String_t* value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___Null_3)); }
	inline String_t* get_Null_3() const { return ___Null_3; }
	inline String_t** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(String_t* value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((&___Null_3), value);
	}

	inline static int32_t get_offset_of_Undefined_4() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___Undefined_4)); }
	inline String_t* get_Undefined_4() const { return ___Undefined_4; }
	inline String_t** get_address_of_Undefined_4() { return &___Undefined_4; }
	inline void set_Undefined_4(String_t* value)
	{
		___Undefined_4 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_4), value);
	}

	inline static int32_t get_offset_of_PositiveInfinity_5() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___PositiveInfinity_5)); }
	inline String_t* get_PositiveInfinity_5() const { return ___PositiveInfinity_5; }
	inline String_t** get_address_of_PositiveInfinity_5() { return &___PositiveInfinity_5; }
	inline void set_PositiveInfinity_5(String_t* value)
	{
		___PositiveInfinity_5 = value;
		Il2CppCodeGenWriteBarrier((&___PositiveInfinity_5), value);
	}

	inline static int32_t get_offset_of_NegativeInfinity_6() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___NegativeInfinity_6)); }
	inline String_t* get_NegativeInfinity_6() const { return ___NegativeInfinity_6; }
	inline String_t** get_address_of_NegativeInfinity_6() { return &___NegativeInfinity_6; }
	inline void set_NegativeInfinity_6(String_t* value)
	{
		___NegativeInfinity_6 = value;
		Il2CppCodeGenWriteBarrier((&___NegativeInfinity_6), value);
	}

	inline static int32_t get_offset_of_NaN_7() { return static_cast<int32_t>(offsetof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields, ___NaN_7)); }
	inline String_t* get_NaN_7() const { return ___NaN_7; }
	inline String_t** get_address_of_NaN_7() { return &___NaN_7; }
	inline void set_NaN_7(String_t* value)
	{
		___NaN_7 = value;
		Il2CppCodeGenWriteBarrier((&___NaN_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERT_T41186D2C92A15E30FBEB8087981F668A711CE25F_H
#ifndef JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#define JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef COLLECTION_1_TD22A8BE81B42784580585C44AFED36B169AFA740_H
#define COLLECTION_1_TD22A8BE81B42784580585C44AFED36B169AFA740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonConverter>
struct  Collection_1_tD22A8BE81B42784580585C44AFED36B169AFA740  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_tD22A8BE81B42784580585C44AFED36B169AFA740, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_tD22A8BE81B42784580585C44AFED36B169AFA740, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_TD22A8BE81B42784580585C44AFED36B169AFA740_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D1024_T262580B26E1F8C3057C04E41B38E058D345E203E_H
#define __STATICARRAYINITTYPESIZEU3D1024_T262580B26E1F8C3057C04E41B38E058D345E203E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024
struct  __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1024_T262580B26E1F8C3057C04E41B38E058D345E203E_H
#ifndef __STATICARRAYINITTYPESIZEU3D112_TBA6FAEC1E474CBDEC00CA40BD22865A888112885_H
#define __STATICARRAYINITTYPESIZEU3D112_TBA6FAEC1E474CBDEC00CA40BD22865A888112885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D112
struct  __StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885__padding[112];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D112_TBA6FAEC1E474CBDEC00CA40BD22865A888112885_H
#ifndef __STATICARRAYINITTYPESIZEU3D1152_T63DDBCF6A3D9AED58517711035E84F428212FB44_H
#define __STATICARRAYINITTYPESIZEU3D1152_T63DDBCF6A3D9AED58517711035E84F428212FB44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1152
struct  __StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44__padding[1152];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1152_T63DDBCF6A3D9AED58517711035E84F428212FB44_H
#ifndef __STATICARRAYINITTYPESIZEU3D116_T3ECC97757AB98ED162DDA5594A22E596E7310B27_H
#define __STATICARRAYINITTYPESIZEU3D116_T3ECC97757AB98ED162DDA5594A22E596E7310B27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D116
struct  __StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27__padding[116];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D116_T3ECC97757AB98ED162DDA5594A22E596E7310B27_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T40369F2228DD607A80D17BB3D2D5BE6993E03742_H
#define __STATICARRAYINITTYPESIZEU3D12_T40369F2228DD607A80D17BB3D2D5BE6993E03742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T40369F2228DD607A80D17BB3D2D5BE6993E03742_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T12E95F9892C7F6F42D9704D5452F45ACD91DEF27_H
#define __STATICARRAYINITTYPESIZEU3D120_T12E95F9892C7F6F42D9704D5452F45ACD91DEF27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120
struct  __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T12E95F9892C7F6F42D9704D5452F45ACD91DEF27_H
#ifndef __STATICARRAYINITTYPESIZEU3D124_TB51B1284695CD5EF856709FD5375A613C97A8821_H
#define __STATICARRAYINITTYPESIZEU3D124_TB51B1284695CD5EF856709FD5375A613C97A8821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D124
struct  __StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821__padding[124];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D124_TB51B1284695CD5EF856709FD5375A613C97A8821_H
#ifndef __STATICARRAYINITTYPESIZEU3D128_T6A568434D1DD1DC652C8F7BAF60D1397939FB018_H
#define __STATICARRAYINITTYPESIZEU3D128_T6A568434D1DD1DC652C8F7BAF60D1397939FB018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128
struct  __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018__padding[128];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D128_T6A568434D1DD1DC652C8F7BAF60D1397939FB018_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_TE9246BC51AC87DC889AA3C427D51E2C95286DD12_H
#define __STATICARRAYINITTYPESIZEU3D16_TE9246BC51AC87DC889AA3C427D51E2C95286DD12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16
struct  __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_TE9246BC51AC87DC889AA3C427D51E2C95286DD12_H
#ifndef __STATICARRAYINITTYPESIZEU3D160_TDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926_H
#define __STATICARRAYINITTYPESIZEU3D160_TDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D160
struct  __StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926__padding[160];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D160_TDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926_H
#ifndef __STATICARRAYINITTYPESIZEU3D19_TB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85_H
#define __STATICARRAYINITTYPESIZEU3D19_TB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D19
struct  __StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85__padding[19];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D19_TB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85_H
#ifndef __STATICARRAYINITTYPESIZEU3D192_TD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64_H
#define __STATICARRAYINITTYPESIZEU3D192_TD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D192
struct  __StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64__padding[192];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D192_TD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_TAE3A6C868C85AE191616B68E230CF63EC626520A_H
#define __STATICARRAYINITTYPESIZEU3D20_TAE3A6C868C85AE191616B68E230CF63EC626520A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20
struct  __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_TAE3A6C868C85AE191616B68E230CF63EC626520A_H
#ifndef __STATICARRAYINITTYPESIZEU3D2048_T5DE79873FEB376EFFB0288E5F1586BB39A09E510_H
#define __STATICARRAYINITTYPESIZEU3D2048_T5DE79873FEB376EFFB0288E5F1586BB39A09E510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048
struct  __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510__padding[2048];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D2048_T5DE79873FEB376EFFB0288E5F1586BB39A09E510_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T5B47C1227ED09E328284961C56B9745E8CF857B2_H
#define __STATICARRAYINITTYPESIZEU3D24_T5B47C1227ED09E328284961C56B9745E8CF857B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24
struct  __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T5B47C1227ED09E328284961C56B9745E8CF857B2_H
#ifndef __STATICARRAYINITTYPESIZEU3D256_TCF2FC3DC530AA2A595156A0FE4D2B533781D388B_H
#define __STATICARRAYINITTYPESIZEU3D256_TCF2FC3DC530AA2A595156A0FE4D2B533781D388B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256
struct  __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D256_TCF2FC3DC530AA2A595156A0FE4D2B533781D388B_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T23354B8C06A0C08995538AA6989AD5A6BD1F736B_H
#define __STATICARRAYINITTYPESIZEU3D28_T23354B8C06A0C08995538AA6989AD5A6BD1F736B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28
struct  __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T23354B8C06A0C08995538AA6989AD5A6BD1F736B_H
#ifndef __STATICARRAYINITTYPESIZEU3D3_T2FF14B1CAF9002504904F263389DDF1B48A1C432_H
#define __STATICARRAYINITTYPESIZEU3D3_T2FF14B1CAF9002504904F263389DDF1B48A1C432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D3
struct  __StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432__padding[3];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D3_T2FF14B1CAF9002504904F263389DDF1B48A1C432_H
#ifndef __STATICARRAYINITTYPESIZEU3D30_T27982463335070BCC28CF69F236F03360875F9CD_H
#define __STATICARRAYINITTYPESIZEU3D30_T27982463335070BCC28CF69F236F03360875F9CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D30
struct  __StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD__padding[30];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D30_T27982463335070BCC28CF69F236F03360875F9CD_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T1FA6CEC782AA662C9B5D2500E01EF0DBD1892391_H
#define __STATICARRAYINITTYPESIZEU3D32_T1FA6CEC782AA662C9B5D2500E01EF0DBD1892391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32
struct  __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T1FA6CEC782AA662C9B5D2500E01EF0DBD1892391_H
#ifndef __STATICARRAYINITTYPESIZEU3D36_T4966948BB7C8115E94C03B23C8BF2F7311DF4981_H
#define __STATICARRAYINITTYPESIZEU3D36_T4966948BB7C8115E94C03B23C8BF2F7311DF4981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D36
struct  __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981__padding[36];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D36_T4966948BB7C8115E94C03B23C8BF2F7311DF4981_H
#ifndef __STATICARRAYINITTYPESIZEU3D384_T748C53461277A9DAA5390470D2236736909F3FAD_H
#define __STATICARRAYINITTYPESIZEU3D384_T748C53461277A9DAA5390470D2236736909F3FAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D384
struct  __StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD__padding[384];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D384_T748C53461277A9DAA5390470D2236736909F3FAD_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_T81C3686E05073D410811D7E20015B0D31FD02B51_H
#define __STATICARRAYINITTYPESIZEU3D40_T81C3686E05073D410811D7E20015B0D31FD02B51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_T81C3686E05073D410811D7E20015B0D31FD02B51_H
#ifndef __STATICARRAYINITTYPESIZEU3D404_TAB7DC965F421CBB7140773669EBC7FF4362EE38D_H
#define __STATICARRAYINITTYPESIZEU3D404_TAB7DC965F421CBB7140773669EBC7FF4362EE38D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D404
struct  __StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D__padding[404];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D404_TAB7DC965F421CBB7140773669EBC7FF4362EE38D_H
#ifndef __STATICARRAYINITTYPESIZEU3D4096_T9A971A259AA05D3B04C6F63382905A5E23316C44_H
#define __STATICARRAYINITTYPESIZEU3D4096_T9A971A259AA05D3B04C6F63382905A5E23316C44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D4096
struct  __StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44__padding[4096];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D4096_T9A971A259AA05D3B04C6F63382905A5E23316C44_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_T850D7D2483453A735E5BF43D3DF23B3E49383942_H
#define __STATICARRAYINITTYPESIZEU3D44_T850D7D2483453A735E5BF43D3DF23B3E49383942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D44
struct  __StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_T850D7D2483453A735E5BF43D3DF23B3E49383942_H
#ifndef __STATICARRAYINITTYPESIZEU3D48_TAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A_H
#define __STATICARRAYINITTYPESIZEU3D48_TAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48
struct  __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A__padding[48];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D48_TAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A_H
#ifndef __STATICARRAYINITTYPESIZEU3D5_TC3DEA7A176A0226116DCE0C50012795158E22923_H
#define __STATICARRAYINITTYPESIZEU3D5_TC3DEA7A176A0226116DCE0C50012795158E22923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D5
struct  __StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923__padding[5];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D5_TC3DEA7A176A0226116DCE0C50012795158E22923_H
#ifndef __STATICARRAYINITTYPESIZEU3D511_T0A5821374352C92EB288D21CD4341DBBBD5C6AD9_H
#define __STATICARRAYINITTYPESIZEU3D511_T0A5821374352C92EB288D21CD4341DBBBD5C6AD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D511
struct  __StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9__padding[511];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D511_T0A5821374352C92EB288D21CD4341DBBBD5C6AD9_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_TED2E2674FDE7D2F45A762091ABCD036953771373_H
#define __STATICARRAYINITTYPESIZEU3D512_TED2E2674FDE7D2F45A762091ABCD036953771373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512
struct  __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_TED2E2674FDE7D2F45A762091ABCD036953771373_H
#ifndef __STATICARRAYINITTYPESIZEU3D56_TCE789691A5319A29277651D0EDDFF891792A6256_H
#define __STATICARRAYINITTYPESIZEU3D56_TCE789691A5319A29277651D0EDDFF891792A6256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D56
struct  __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256__padding[56];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D56_TCE789691A5319A29277651D0EDDFF891792A6256_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T2A988DB17692352FB8EBC461F2D4A70546950009_H
#define __STATICARRAYINITTYPESIZEU3D6_T2A988DB17692352FB8EBC461F2D4A70546950009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6
struct  __StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T2A988DB17692352FB8EBC461F2D4A70546950009_H
#ifndef __STATICARRAYINITTYPESIZEU3D6144_T3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4_H
#define __STATICARRAYINITTYPESIZEU3D6144_T3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6144
struct  __StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4__padding[6144];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6144_T3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4_H
#ifndef __STATICARRAYINITTYPESIZEU3D64_TAC179E5A47816D03F547C9EC0B8374CCE5C05991_H
#define __STATICARRAYINITTYPESIZEU3D64_TAC179E5A47816D03F547C9EC0B8374CCE5C05991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64
struct  __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D64_TAC179E5A47816D03F547C9EC0B8374CCE5C05991_H
#ifndef __STATICARRAYINITTYPESIZEU3D640_T07C2BC1E9E8555348A07BC4FA16F4894853703C6_H
#define __STATICARRAYINITTYPESIZEU3D640_T07C2BC1E9E8555348A07BC4FA16F4894853703C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D640
struct  __StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6__padding[640];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D640_T07C2BC1E9E8555348A07BC4FA16F4894853703C6_H
#ifndef __STATICARRAYINITTYPESIZEU3D68_TBC0122564AB1A13F811AD692B6F90250F54F07C0_H
#define __STATICARRAYINITTYPESIZEU3D68_TBC0122564AB1A13F811AD692B6F90250F54F07C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D68
struct  __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0__padding[68];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D68_TBC0122564AB1A13F811AD692B6F90250F54F07C0_H
#ifndef __STATICARRAYINITTYPESIZEU3D72_T2372182CEF6FF10BBA734B1338E94DF2176D148C_H
#define __STATICARRAYINITTYPESIZEU3D72_T2372182CEF6FF10BBA734B1338E94DF2176D148C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D72
struct  __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C__padding[72];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D72_T2372182CEF6FF10BBA734B1338E94DF2176D148C_H
#ifndef __STATICARRAYINITTYPESIZEU3D76_T38115C5264B74F6026FD592AC3726967368CBCCE_H
#define __STATICARRAYINITTYPESIZEU3D76_T38115C5264B74F6026FD592AC3726967368CBCCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D76
struct  __StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE__padding[76];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D76_T38115C5264B74F6026FD592AC3726967368CBCCE_H
#ifndef __STATICARRAYINITTYPESIZEU3D96_T0EC581000EDFC257A98827E661B6AB5E51F36015_H
#define __STATICARRAYINITTYPESIZEU3D96_T0EC581000EDFC257A98827E661B6AB5E51F36015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D96
struct  __StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015__padding[96];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D96_T0EC581000EDFC257A98827E661B6AB5E51F36015_H
#ifndef JSONCONSTRUCTORATTRIBUTE_TF2DEC4EEF6D09EA4AD193356DF661C34DFECFC06_H
#define JSONCONSTRUCTORATTRIBUTE_TF2DEC4EEF6D09EA4AD193356DF661C34DFECFC06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConstructorAttribute
struct  JsonConstructorAttribute_tF2DEC4EEF6D09EA4AD193356DF661C34DFECFC06  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONSTRUCTORATTRIBUTE_TF2DEC4EEF6D09EA4AD193356DF661C34DFECFC06_H
#ifndef JSONCONVERTERATTRIBUTE_T971BE732936930F54D76941AAC672587C409404B_H
#define JSONCONVERTERATTRIBUTE_T971BE732936930F54D76941AAC672587C409404B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverterAttribute
struct  JsonConverterAttribute_t971BE732936930F54D76941AAC672587C409404B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Newtonsoft.Json.JsonConverterAttribute::_converterType
	Type_t * ____converterType_0;
	// System.Object[] Newtonsoft.Json.JsonConverterAttribute::<ConverterParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CConverterParametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__converterType_0() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t971BE732936930F54D76941AAC672587C409404B, ____converterType_0)); }
	inline Type_t * get__converterType_0() const { return ____converterType_0; }
	inline Type_t ** get_address_of__converterType_0() { return &____converterType_0; }
	inline void set__converterType_0(Type_t * value)
	{
		____converterType_0 = value;
		Il2CppCodeGenWriteBarrier((&____converterType_0), value);
	}

	inline static int32_t get_offset_of_U3CConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t971BE732936930F54D76941AAC672587C409404B, ___U3CConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CConverterParametersU3Ek__BackingField_1() const { return ___U3CConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CConverterParametersU3Ek__BackingField_1() { return &___U3CConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterParametersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERATTRIBUTE_T971BE732936930F54D76941AAC672587C409404B_H
#ifndef JSONCONVERTERCOLLECTION_T88900312A11AA971C2C6438DD622F3E8067BA4E3_H
#define JSONCONVERTERCOLLECTION_T88900312A11AA971C2C6438DD622F3E8067BA4E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverterCollection
struct  JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3  : public Collection_1_tD22A8BE81B42784580585C44AFED36B169AFA740
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERCOLLECTION_T88900312A11AA971C2C6438DD622F3E8067BA4E3_H
#ifndef JSONEXCEPTION_T3BE8D674432CDEDA0DA71F0934F103EB509C1E78_H
#define JSONEXCEPTION_T3BE8D674432CDEDA0DA71F0934F103EB509C1E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonException
struct  JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T3BE8D674432CDEDA0DA71F0934F103EB509C1E78_H
#ifndef JSONEXTENSIONDATAATTRIBUTE_T90D1D7C4F9D65A96740C0756C512C8BCFBC62B03_H
#define JSONEXTENSIONDATAATTRIBUTE_T90D1D7C4F9D65A96740C0756C512C8BCFBC62B03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonExtensionDataAttribute
struct  JsonExtensionDataAttribute_t90D1D7C4F9D65A96740C0756C512C8BCFBC62B03  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::<WriteData>k__BackingField
	bool ___U3CWriteDataU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::<ReadData>k__BackingField
	bool ___U3CReadDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWriteDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonExtensionDataAttribute_t90D1D7C4F9D65A96740C0756C512C8BCFBC62B03, ___U3CWriteDataU3Ek__BackingField_0)); }
	inline bool get_U3CWriteDataU3Ek__BackingField_0() const { return ___U3CWriteDataU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CWriteDataU3Ek__BackingField_0() { return &___U3CWriteDataU3Ek__BackingField_0; }
	inline void set_U3CWriteDataU3Ek__BackingField_0(bool value)
	{
		___U3CWriteDataU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReadDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonExtensionDataAttribute_t90D1D7C4F9D65A96740C0756C512C8BCFBC62B03, ___U3CReadDataU3Ek__BackingField_1)); }
	inline bool get_U3CReadDataU3Ek__BackingField_1() const { return ___U3CReadDataU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CReadDataU3Ek__BackingField_1() { return &___U3CReadDataU3Ek__BackingField_1; }
	inline void set_U3CReadDataU3Ek__BackingField_1(bool value)
	{
		___U3CReadDataU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXTENSIONDATAATTRIBUTE_T90D1D7C4F9D65A96740C0756C512C8BCFBC62B03_H
#ifndef JSONIGNOREATTRIBUTE_TA00F2FBCE767ECB606C2BBD26192EC4871A4B726_H
#define JSONIGNOREATTRIBUTE_TA00F2FBCE767ECB606C2BBD26192EC4871A4B726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonIgnoreAttribute
struct  JsonIgnoreAttribute_tA00F2FBCE767ECB606C2BBD26192EC4871A4B726  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONIGNOREATTRIBUTE_TA00F2FBCE767ECB606C2BBD26192EC4871A4B726_H
#ifndef JSONREQUIREDATTRIBUTE_TBAE089AA5D5D0E66B636EDA11A6E8D9AD1BE4286_H
#define JSONREQUIREDATTRIBUTE_TBAE089AA5D5D0E66B636EDA11A6E8D9AD1BE4286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonRequiredAttribute
struct  JsonRequiredAttribute_tBAE089AA5D5D0E66B636EDA11A6E8D9AD1BE4286  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREQUIREDATTRIBUTE_TBAE089AA5D5D0E66B636EDA11A6E8D9AD1BE4286_H
#ifndef STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#define STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringBuffer
struct  StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____buffer_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4, ____buffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#ifndef STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#define STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReference
struct  StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringReference::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T837AC9D13901280E875914E3F0905F8FC0DD012F_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T837AC9D13901280E875914E3F0905F8FC0DD012F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::0060620709E93572EB9D62564332C778F7AE32E9
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___0060620709E93572EB9D62564332C778F7AE32E9_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::007AFA4B68442DD6E0877DE7624D0A5F508C474F
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___007AFA4B68442DD6E0877DE7624D0A5F508C474F_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::00A4ED0900CBF010B3628AA473D0D1477DB67FFF
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___00A4ED0900CBF010B3628AA473D0D1477DB67FFF_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::013B76F7DD3E0346CB351F079CC062EFBD293457
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___013B76F7DD3E0346CB351F079CC062EFBD293457_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::0392525BCB01691D1F319D89F2C12BF93A478467
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___0392525BCB01691D1F319D89F2C12BF93A478467_4;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::03D9F1A66AED1E059B1609A09E435B708A88C8B8
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___03D9F1A66AED1E059B1609A09E435B708A88C8B8_5;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::04A4F8F1CDFE8681F711039522BDB360478ACD84
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___04A4F8F1CDFE8681F711039522BDB360478ACD84_6;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::04D2A79C8A779AFAA779125335E9334C245EBB46
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___04D2A79C8A779AFAA779125335E9334C245EBB46_8;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0
	__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  ___059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::068B2E17352B5B9FF693CAE83421B679E0342A5C
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___068B2E17352B5B9FF693CAE83421B679E0342A5C_10;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::083DE622A9A685DC50D8D5653CB388A41343C8EC
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___083DE622A9A685DC50D8D5653CB388A41343C8EC_11;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::083EF765A34AC9DEC41761B4DCEA48EC009115E9
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___083EF765A34AC9DEC41761B4DCEA48EC009115E9_12;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::0953DF544832295E4A5B19928F95C351F25DA86A
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___0953DF544832295E4A5B19928F95C351F25DA86A_13;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48 <PrivateImplementationDetails>::095B351FE2104237B032546280C98C9804D331C5
	__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  ___095B351FE2104237B032546280C98C9804D331C5_14;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D36 <PrivateImplementationDetails>::0982B1B45B764F2694ABC3DE57204AC898651429
	__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  ___0982B1B45B764F2694ABC3DE57204AC898651429_15;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::0B1E7265E67D2458D8EB536A4B36380A5BF6E731
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___0B1E7265E67D2458D8EB536A4B36380A5BF6E731_16;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::0BE6B8194AB353640CA66F0A661EA93A3132393A
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___0BE6B8194AB353640CA66F0A661EA93A3132393A_17;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::0C4110BC17D746F018F47B49E0EB0D6590F69939
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___0C4110BC17D746F018F47B49E0EB0D6590F69939_18;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D72 <PrivateImplementationDetails>::0D0825E62E82DBEBFAD598623694129548E24C9C
	__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  ___0D0825E62E82DBEBFAD598623694129548E24C9C_19;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::0D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___0D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::0E699DEC139E44E7D1DF125C2CCB3E25A1BDF892
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___0E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::1005BA20F99323E3F050E781BB81D1A4479AB037
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___1005BA20F99323E3F050E781BB81D1A4479AB037_23;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::102C522344FCAC1545BDA50C0FC675C502FFEC53
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___102C522344FCAC1545BDA50C0FC675C502FFEC53_24;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::103752E99F3718E46F1AA9EED70682BF3A9B8A65
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___103752E99F3718E46F1AA9EED70682BF3A9B8A65_25;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::118FE9754A9699004D478E4BF686021154D96EAD
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___118FE9754A9699004D478E4BF686021154D96EAD_26;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::124B1C35B19149213F8F7D40AA8E0ABA15DD70EC
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::126589410FF9CA1510B9950BF0E79E5BFD60000B
	__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  ___126589410FF9CA1510B9950BF0E79E5BFD60000B_28;
	// System.Int32 <PrivateImplementationDetails>::1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C
	int32_t ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D56 <PrivateImplementationDetails>::14353D63A97A25640437E7C0DAE8B1596F34AB2C
	__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  ___14353D63A97A25640437E7C0DAE8B1596F34AB2C_30;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::146E4D4A36742E37DEE528D23474B9675695E406
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___146E4D4A36742E37DEE528D23474B9675695E406_31;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::148E9E3E864CD628C70D3DC1D8309483BD8C0E89
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::1648F737A4CFFDA4E6C83A3D742109BF9DBC2446
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::17651A9FA4DEA6C24D1287324CF4A640D080FE8E
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___17651A9FA4DEA6C24D1287324CF4A640D080FE8E_35;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::17BCB27C371D710141A300DA5A9A3EADF78D7A35
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___17BCB27C371D710141A300DA5A9A3EADF78D7A35_36;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::17E54FCA28103DF892BBB946DDEED4B061D7F5C7
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___17E54FCA28103DF892BBB946DDEED4B061D7F5C7_37;
	// System.Int32 <PrivateImplementationDetails>::18F46E3C2ECF4BAAE65361632BA7C1B4B9028827
	int32_t ___18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::1A43D7FEEED03520E11C4A8996F149705D99C6BB
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___1A43D7FEEED03520E11C4A8996F149705D99C6BB_39;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::1AFB455399A50580CF1039188ABA6BE82F309543
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___1AFB455399A50580CF1039188ABA6BE82F309543_40;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D384 <PrivateImplementationDetails>::1B180C6E41F096D53222F5E8EF558B78182CA401
	__StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD  ___1B180C6E41F096D53222F5E8EF558B78182CA401_41;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::1C1237F52E2ED7B4D229AE3978DA144B9E653F5E
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::1D3E73DD251585C4908CBA58A179E1911834C891
	__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  ___1D3E73DD251585C4908CBA58A179E1911834C891_43;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::1E3842329C5294DBE1DF588A77C68B35C6AF83BF
	__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  ___1E3842329C5294DBE1DF588A77C68B35C6AF83BF_44;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D76 <PrivateImplementationDetails>::1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38
	__StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE  ___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::21305ABEF36592B9C636A4DDFFB419BBEB14AC71
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___21305ABEF36592B9C636A4DDFFB419BBEB14AC71_46;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::214F93D9222D60794CE1EA0A10389885C5CA9824
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___214F93D9222D60794CE1EA0A10389885C5CA9824_47;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::21A3E0CD2847E2F12EEE37749A9E206494A55100
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___21A3E0CD2847E2F12EEE37749A9E206494A55100_48;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::22F13A28C218AA9B43303043F2CF664790D12BD7
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___22F13A28C218AA9B43303043F2CF664790D12BD7_49;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::239B59488F1CE7EBE225785FDC22A8E3102A2E82
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___239B59488F1CE7EBE225785FDC22A8E3102A2E82_50;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::26853A2C322BBAD5BBD886C60A32BBBCFE847F00
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___26853A2C322BBAD5BBD886C60A32BBBCFE847F00_51;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::27C3AB82CCA6CE2F199F4F670BF19513A3825B87
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___27C3AB82CCA6CE2F199F4F670BF19513A3825B87_52;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::27FED0F92A97C41B08D3115553BBDC064F417B6E
	__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  ___27FED0F92A97C41B08D3115553BBDC064F417B6E_53;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::29F7A0217340B5682E7DDF98ADAD952E2A360E65
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___29F7A0217340B5682E7DDF98ADAD952E2A360E65_55;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::2BA9E4B370D477F8C7FE286262D7ADC69CAF290E
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::2CABEB86D5B3D362822AF2E5D136A10A17AD85DA
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::2CDACAA27B7865A56A57B156CFB369245964BD2B
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___2CDACAA27B7865A56A57B156CFB369245964BD2B_58;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D116 <PrivateImplementationDetails>::2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71
	__StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27  ___2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60;
	// System.Int32 <PrivateImplementationDetails>::3045D5B29BA900304981918A153806D371B02549
	int32_t ___3045D5B29BA900304981918A153806D371B02549_61;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::30A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___30A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::30F65A149AF7DE938A9287048498B966AEBE54D4
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___30F65A149AF7DE938A9287048498B966AEBE54D4_63;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::310EFB639F3C7677A2A82B54EEED1124ED69E9A3
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::310FB325A3EA3EA527B55C2F08544D1CB92C19F4
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::31CDD717843C5C2B207F235634E6726898D4858A
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___31CDD717843C5C2B207F235634E6726898D4858A_66;
	// System.Int32 <PrivateImplementationDetails>::31D8729F7377B44017C0A2395A582C9CA4163277
	int32_t ___31D8729F7377B44017C0A2395A582C9CA4163277_67;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::320B018758ECE3752FFEDBAEB1A6DB67C80B9359
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::321CB68F886E22F95877B3535C1B34A6A94A40B6
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___321CB68F886E22F95877B3535C1B34A6A94A40B6_69;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::32ECB35FF8400B4E56FF5E09588FB20DD60350E7
	__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  ___32ECB35FF8400B4E56FF5E09588FB20DD60350E7_70;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::3544182260B8A15D332367E48C7530FC0E901FD3
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___3544182260B8A15D332367E48C7530FC0E901FD3_72;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD
	__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  ___35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::35E6464339FFAE0D3777B12A371F82D2D1F668CA
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___35E6464339FFAE0D3777B12A371F82D2D1F668CA_74;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::372040F482ABADADF58EF0C31A6A8BE386AF8A50
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___372040F482ABADADF58EF0C31A6A8BE386AF8A50_75;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::37454D933508E238CFB980F1077B24ADA4A480F4
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___37454D933508E238CFB980F1077B24ADA4A480F4_76;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::3A38ADC6BCFB84DE23160C1E50212DACBCD25A11
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::3BA78FFB102A191597BAC4A2418C57CDF377FEE6
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___3BA78FFB102A191597BAC4A2418C57CDF377FEE6_78;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::3C3704DB6466D48DD40FE189070F1896D1D45C8B
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___3C3704DB6466D48DD40FE189070F1896D1D45C8B_79;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::3D6EB645BC212077C1B37A3A32CA2A62F7B39018
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___3D6EB645BC212077C1B37A3A32CA2A62F7B39018_80;
	// System.Int32 <PrivateImplementationDetails>::3E8E493888B1BFB763D7553A6CC1978C17C198A3
	int32_t ___3E8E493888B1BFB763D7553A6CC1978C17C198A3_81;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::428007959831954B0C2DCFAF9DD641D629B00DBF
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___428007959831954B0C2DCFAF9DD641D629B00DBF_84;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::42C3E89412F11AA94E57C09EFB4B2B415C1AAB58
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::433175D38B13FFE177FDD661A309F1B528B3F6E2
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___433175D38B13FFE177FDD661A309F1B528B3F6E2_86;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::4435D44E1091E6624ED6B6E4FA3C9A8C5C996098
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::460C77B94933562DE2E0E5B4FD72B431DFAEB5B6
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::467C6758F235D3193618192A64129CBB602C9067
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___467C6758F235D3193618192A64129CBB602C9067_89;
	// System.Int32 <PrivateImplementationDetails>::47A4F979AF156CDA62313B97411B4C6CE62B8B5A
	int32_t ___47A4F979AF156CDA62313B97411B4C6CE62B8B5A_90;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::498B9317C14CF9004FE6CB88782D926B3E379350
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___498B9317C14CF9004FE6CB88782D926B3E379350_91;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::4A080107805D8F7103F56DD3B0D8FD5D55735937
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___4A080107805D8F7103F56DD3B0D8FD5D55735937_93;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::4B411385A36907D25D8088AE39AB6AAFA46B0642
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___4B411385A36907D25D8088AE39AB6AAFA46B0642_94;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::4C44594E2C603D85EC6195B1A7A6C5876CBB58E2
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::4C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___4C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::4E16474F9D4C9E164E5F3C5FF50D0FB922D244DD
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___4E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::4FFC8339E09825A68B861995F9C660EB11DBF13D
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___4FFC8339E09825A68B861995F9C660EB11DBF13D_98;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::50AA269217736906D8469B9191F420DC6B13A36A
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___50AA269217736906D8469B9191F420DC6B13A36A_99;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D112 <PrivateImplementationDetails>::50B1635D1FB2907A171B71751E1A3FA79423CA17
	__StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885  ___50B1635D1FB2907A171B71751E1A3FA79423CA17_100;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::512F92F4041B190727A330E2A6CC39E5D9EA06E6
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___512F92F4041B190727A330E2A6CC39E5D9EA06E6_101;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::5318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___5318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::547AF924C0A210AD3D72D199002D3CDBC2DBD0CA
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>::5581A70566F03554D8048EDBFC6E6B399AF9BCB1
	__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  ___5581A70566F03554D8048EDBFC6E6B399AF9BCB1_105;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::58F8E05C1E95C042205E13B76D1BFC0F7ABA6F88
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___58F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::59F5BD34B6C013DEACC784F69C67E95150033A84
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___59F5BD34B6C013DEACC784F69C67E95150033A84_107;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::5AB421AC76CECB8E84025172585CB97DE8BECD65
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___5AB421AC76CECB8E84025172585CB97DE8BECD65_108;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::5AE32F75295B7AC2FB76118848B92F3987B949A0
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___5AE32F75295B7AC2FB76118848B92F3987B949A0_109;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::5BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___5BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::5CF7299F6558A8AC3F821B4F2F65F23798D319D3
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___5CF7299F6558A8AC3F821B4F2F65F23798D319D3_111;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::5CFF612F2058DE2C287ECA5C6407ECC471F65576
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___5CFF612F2058DE2C287ECA5C6407ECC471F65576_112;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::5D41C56232C500092E99AC044D3C5C442B1C834F
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___5D41C56232C500092E99AC044D3C5C442B1C834F_113;
	// System.Int64 <PrivateImplementationDetails>::5DBC1A420B61F594A834C83E3DDC229C8AB77FDC
	int64_t ___5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::5E7F55149EC07597C76E6E3CD9F62274214061E6
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___5E7F55149EC07597C76E6E3CD9F62274214061E6_115;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::5EF7F909EFC731E811E21521A43A80FB5AC0B229
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___5EF7F909EFC731E811E21521A43A80FB5AC0B229_116;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::60A08108A32C9D3F263B2F42095A2694B7C1C1EF
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___60A08108A32C9D3F263B2F42095A2694B7C1C1EF_117;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::613CFAEE025A3AF3C6D13DEB22E298C1925C31B5
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::6277CE8FE3A9156D3455749B453AC88191D3C6D6
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___6277CE8FE3A9156D3455749B453AC88191D3C6D6_119;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48 <PrivateImplementationDetails>::62BAB0F245E66C3EB982CF5A7015F0A7C3382283
	__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  ___62BAB0F245E66C3EB982CF5A7015F0A7C3382283_120;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::64354464C9074B5BB4369689AAA131961CD1EF19
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___64354464C9074B5BB4369689AAA131961CD1EF19_122;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D72 <PrivateImplementationDetails>::643A9D76937E94519B73BE072D65E79BAFF3C213
	__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  ___643A9D76937E94519B73BE072D65E79BAFF3C213_123;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::643F7C1D25ADAFA2F5ED135D8331618A14714ED2
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D36 <PrivateImplementationDetails>::66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47
	__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  ___66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::672385C1D6A6C84A1AC3588540B09C4AE3B87DDC
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D116 <PrivateImplementationDetails>::67C0E784F3654B008A81E2988588CF4956CCF3DA
	__StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27  ___67C0E784F3654B008A81E2988588CF4956CCF3DA_127;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::68178023585F1F782745740AA583CDC778DB31B3
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___68178023585F1F782745740AA583CDC778DB31B3_128;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::6A316789EED01119DE92841832701A40AB0CABD6
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___6A316789EED01119DE92841832701A40AB0CABD6_129;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::6AAC0DB543C50F09E879F5B9F757319773564CE1
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___6AAC0DB543C50F09E879F5B9F757319773564CE1_130;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D192 <PrivateImplementationDetails>::6BBA9E7ACC5D57E30438CC78FF7E308399E10FE1
	__StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64  ___6BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::6BBD3A22A185224EE0EBAB0784455E9E245376B7
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___6BBD3A22A185224EE0EBAB0784455E9E245376B7_133;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D72 <PrivateImplementationDetails>::6C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7
	__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  ___6C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134;
	// System.Int32 <PrivateImplementationDetails>::6D19ABD7FD0986E382242A9B88C8D8B5F52598DF
	int32_t ___6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::6DEB7F74818574642B0B824B9C08B366C962A360
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___6DEB7F74818574642B0B824B9C08B366C962A360_137;
	// System.Int64 <PrivateImplementationDetails>::6DFD60679846B0E2064404E9EA3E1DDCE2C5709D
	int64_t ___6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D56 <PrivateImplementationDetails>::6E92E42774698E4C089FE0FE4F7539D0D5F845F5
	__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  ___6E92E42774698E4C089FE0FE4F7539D0D5F845F5_139;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::6F39BC29A161CAE5394821B1FDE160EB5229AE71
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___6F39BC29A161CAE5394821B1FDE160EB5229AE71_140;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::6F576E8737EAAC9B6B12BDFC370048CD205E2CDD
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::6F5D2C37DFCDB1E72B86BC606C0E717C70E188A4
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___6F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D404 <PrivateImplementationDetails>::6FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC
	__StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D  ___6FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D3 <PrivateImplementationDetails>::7037807198C22A7D2B0807371D763779A84FDFCF
	__StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432  ___7037807198C22A7D2B0807371D763779A84FDFCF_144;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::714A94F3805E05CA6C00F9A46489427ABEB25D60
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___714A94F3805E05CA6C00F9A46489427ABEB25D60_145;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7878E9E7126B2BDF365429C31842AE1903CD2DFF
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___7878E9E7126B2BDF365429C31842AE1903CD2DFF_147;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::79039C5C22F863E8EAE0E08E1318F343680E50A8
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___79039C5C22F863E8EAE0E08E1318F343680E50A8_148;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::79A213B796D2AD7A89C2071B0732B78207F5CE01
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___79A213B796D2AD7A89C2071B0732B78207F5CE01_149;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D5 <PrivateImplementationDetails>::79D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31
	__StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923  ___79D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>::79D521E6E3E55103005E9CC3FA43B3174FAF090F
	__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  ___79D521E6E3E55103005E9CC3FA43B3174FAF090F_151;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::7B18F6A671FF25FBD5F195673DAF4902FB2DA4C5
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___7B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7B22115C45C6AD570AFBAB744FA1058DF97CDBC1
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::7C57CFE9FF25243824AF38485BBF41F9E57ECF21
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___7C57CFE9FF25243824AF38485BBF41F9E57ECF21_154;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::7CFF7A50C8F8981091791CDB210243E8F465BC80
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___7CFF7A50C8F8981091791CDB210243E8F465BC80_155;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::7E68A6B197A3AF443FDA77CD8B492D4C72B3CF03
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___7E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D160 <PrivateImplementationDetails>::7F817443C1F736721DD92BD67C7C8994C7B8DEBF
	__StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926  ___7F817443C1F736721DD92BD67C7C8994C7B8DEBF_157;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::7FF0A15672FF2807983AB77C0DA74928986427C0
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___7FF0A15672FF2807983AB77C0DA74928986427C0_158;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::81FDCC6319440C75C8E422A1FE4E3F1A80B89F5F
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___81FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::8330271815E046D369E0B1F7673D308739FDCC07
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___8330271815E046D369E0B1F7673D308739FDCC07_161;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::840B3A53AAF3595FDF3313D46FFD246A7EA6E89E
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D76 <PrivateImplementationDetails>::8457F44B035C9073EE2D1F132D0A8AF5631DCDC8
	__StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE  ___8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::84F6B4137736E2F6671FC8787A500AC5C6E1D6AC
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>::850D4DC092689E1F0D8A70B6281848B27DEC0014
	__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  ___850D4DC092689E1F0D8A70B6281848B27DEC0014_165;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::86B0F85AC13B58F88DEFFD8FD6EE095438B98F10
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::87BAD4D818823E27FF0CBB935FF88DD21DAD3CC4
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___87BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::882888781BC0DC17021FB4F11BA783038C83B313
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___882888781BC0DC17021FB4F11BA783038C83B313_168;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::8AC10B57091AFE182D9E4375C05E3FA037FFB3FA
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::8C7FEE53346CDB1B119FCAD8D605F476400A03CE
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___8C7FEE53346CDB1B119FCAD8D605F476400A03CE_170;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D56 <PrivateImplementationDetails>::8C9BE3C02B5604C5CBF6A03E8032549588A6ED54
	__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  ___8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171;
	// System.Int32 <PrivateImplementationDetails>::8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A
	int32_t ___8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D124 <PrivateImplementationDetails>::8ED8F61DAA454B49CD5059AE4486C59174324E9E
	__StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821  ___8ED8F61DAA454B49CD5059AE4486C59174324E9E_173;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::8F22C9ECE1331718CBD268A9BBFD2F5E451441E3
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D640 <PrivateImplementationDetails>::90A0542282A011472F94E97CEAE59F8B3B1A3291
	__StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6  ___90A0542282A011472F94E97CEAE59F8B3B1A3291_175;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::929747B3E91297D56A461EE1B9E054EA31C70EC1
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___929747B3E91297D56A461EE1B9E054EA31C70EC1_176;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::959C1F7BD85779AAF7AA136A20A9C399A5019AEC
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::95CA85749ADCFBF9A2B82C0381DBCF95D175524C
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___95CA85749ADCFBF9A2B82C0381DBCF95D175524C_179;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D68 <PrivateImplementationDetails>::96ADC3934F8492C827987DFEE3B4DD4EF1738E78
	__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  ___96ADC3934F8492C827987DFEE3B4DD4EF1738E78_180;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::98CF708E75DA60C2A04D06606E726EC975E6DBAB
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___98CF708E75DA60C2A04D06606E726EC975E6DBAB_181;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::9AEFA90D8E67EBAE069B4B6C071A8E867B108B31
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::9F198D04FF67738AE583484145F45103ECE550CC
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___9F198D04FF67738AE583484145F45103ECE550CC_184;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D19 <PrivateImplementationDetails>::9F8365E9D6C62D3B47026EC465B05A7B5526B5CD
	__StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85  ___9F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::A0FABB8173BA247898A9FA267D0CE05500B667A0
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___A0FABB8173BA247898A9FA267D0CE05500B667A0_188;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::A19414BB17AF83ECF276AD49117E7F2EE344D567
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___A19414BB17AF83ECF276AD49117E7F2EE344D567_189;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::A32D5F33452B98E86BE15ED849ED6DF543B5F243
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___A32D5F33452B98E86BE15ED849ED6DF543B5F243_190;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::A4313AAA146ACFCA88681B7BFC3D644005F3792B
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___A4313AAA146ACFCA88681B7BFC3D644005F3792B_191;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::A43F35160EEC5F8DD047667DD94358EDCD29BA27
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___A43F35160EEC5F8DD047667DD94358EDCD29BA27_192;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::A471AF3330805980C7041F978D3CFF8838054E14
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___A471AF3330805980C7041F978D3CFF8838054E14_193;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6144 <PrivateImplementationDetails>::A474A0BEC4E2CE8491839502AE85F6EA8504C6BD
	__StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4  ___A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::A47DBBBB6655DAA5E024374BB9C8AA44FF40D444
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48 <PrivateImplementationDetails>::A53306F44DF494A019EA1487807B59CA336BF024
	__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  ___A53306F44DF494A019EA1487807B59CA336BF024_196;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::A5981DCAA364B0DC9E0385D893A31C2022364075
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___A5981DCAA364B0DC9E0385D893A31C2022364075_197;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::A696E1EE8632C559732B81052E4D2993B8783877
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___A696E1EE8632C559732B81052E4D2993B8783877_198;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::A6BA6E61D8DC7245D265FDFACD05778F1F6FA238
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::A77B822E6880234B59FD1904BF79AF68F5CB623F
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___A77B822E6880234B59FD1904BF79AF68F5CB623F_201;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D511 <PrivateImplementationDetails>::A94DF043C88FB07ACCA327B6A0BB063F20637E71
	__StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9  ___A94DF043C88FB07ACCA327B6A0BB063F20637E71_202;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2
	__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  ___AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::AAF72C1002FDBCAE040DAE16A10D82184CE83679
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___AAF72C1002FDBCAE040DAE16A10D82184CE83679_204;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::AD19F20EECB80A2079F504CB928A26FDE10E8C47
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___AD19F20EECB80A2079F504CB928A26FDE10E8C47_205;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::AD4075598ACA56EC39C5E575771BBB0CFBCE24EE
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48 <PrivateImplementationDetails>::AE843E1C1136C908565A6D4E04E8564B69465B3B
	__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  ___AE843E1C1136C908565A6D4E04E8564B69465B3B_208;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D68 <PrivateImplementationDetails>::AF3E960D2F0572119C78CAF04B900985A299000E
	__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  ___AF3E960D2F0572119C78CAF04B900985A299000E_209;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::B01915493E44597A78D21AC245268E65DDA920ED
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___B01915493E44597A78D21AC245268E65DDA920ED_210;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::B31A9EA1A2718942996E7F94106F0FC798C174C8
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___B31A9EA1A2718942996E7F94106F0FC798C174C8_212;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::B3A60EC240A886DA5AFD600CC73AE12514A881E8
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___B3A60EC240A886DA5AFD600CC73AE12514A881E8_213;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::B5E8BA68953A5283AD953094F0F391FA4502A3FA
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___B5E8BA68953A5283AD953094F0F391FA4502A3FA_216;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::B67A7FB4648C62F6A1337CA473436D0E787E8633
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___B67A7FB4648C62F6A1337CA473436D0E787E8633_217;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::B68637EF60D499620B99E336C59E4865FFC4C5D7
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___B68637EF60D499620B99E336C59E4865FFC4C5D7_218;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D30 <PrivateImplementationDetails>::B8DB0CB599EDD82A386D1A154FB3EB9235513DAD
	__StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD  ___B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::B997A0149EBF3CDD050D72AE1784E375A413B128
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___B997A0149EBF3CDD050D72AE1784E375A413B128_221;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::BBBDF1388DDDDEEC01227AF1738F138452242E3A
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___BBBDF1388DDDDEEC01227AF1738F138452242E3A_222;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::BCE617693C33CE2C76FE00F449CA910E4C6E117E
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___BCE617693C33CE2C76FE00F449CA910E4C6E117E_223;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::BD0C733AC85AB335526865FA6269790347865705
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___BD0C733AC85AB335526865FA6269790347865705_224;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::BE8E2513259482B6F307AC07F23F5D9FB4841EAA
	__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  ___BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::C079C42AC966756C902EC38C4D7989F3C20D3625
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___C079C42AC966756C902EC38C4D7989F3C20D3625_227;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D96 <PrivateImplementationDetails>::C105B70BED997DB5D36E1D2E84C1EFCB445A428C
	__StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015  ___C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::C1122CB95F41A6F2E808A0B815C822BFDAB9F180
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::C132685022CE310ACFD3A883E0A57033A482A959
	__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  ___C132685022CE310ACFD3A883E0A57033A482A959_231;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::C2443D9DBCD3810F90475C6F404BEDCFEF58661E
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C2D514B39C8DFA25365195A0759A5AE28D9F2A87
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C2FEEB3C521ADDD49A534A0876BA97FF5894476E
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C4B266E68FA20D0D222D86ADAD31EBB55118CD21
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D48 <PrivateImplementationDetails>::C5515C87D04DC0D00C7984096F5E35B4944C1CB6
	__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  ___C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::C95356610D5583976B69017BED7048EB50121B90
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___C95356610D5583976B69017BED7048EB50121B90_240;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::C991C784E7697AD0F91A159F03727BF4621A5AB8
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___C991C784E7697AD0F91A159F03727BF4621A5AB8_241;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99
	__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  ___CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::CC53D7FE00E6AC1385AF09521629229467BCCC86
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___CC53D7FE00E6AC1385AF09521629229467BCCC86_244;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D96 <PrivateImplementationDetails>::CE39574ADC95015A9B5E0475EB65EE8F32353FD4
	__StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015  ___CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::CF47311491B9DA067CDF3F5F087D7EFAF2B64F97
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D120 <PrivateImplementationDetails>::D068832E6B13A623916709C1E0E25ADCBE7B455F
	__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  ___D068832E6B13A623916709C1E0E25ADCBE7B455F_249;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6 <PrivateImplementationDetails>::D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C
	__StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009  ___D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::D129FBC67222EA7D73E90E51E4DCFCA8C7497D67
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D36 <PrivateImplementationDetails>::D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608
	__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  ___D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::D2C5BAE967587C6F3D9F2C4551911E0575A1101F
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::D31171F7904EB3247DD4834E43B47B1E2DCB97AC
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255;
	// System.Int32 <PrivateImplementationDetails>::D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557
	int32_t ___D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::D6898715AE96BC2F82A7BBA76E2BFC7100E282C3
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::D7231C06B1D6276752359120E26EAE206A7F74F9
	__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  ___D7231C06B1D6276752359120E26EAE206A7F74F9_258;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D2048 <PrivateImplementationDetails>::D73609A97D6906CFFB02ADDA320A4809483C458D
	__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  ___D73609A97D6906CFFB02ADDA320A4809483C458D_259;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::D860D5BD327368D1D4174620FE2E4A91FE9AADEC
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::D9C221237B647EC215A7BCDED447349810E6BF9C
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___D9C221237B647EC215A7BCDED447349810E6BF9C_263;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D124 <PrivateImplementationDetails>::DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A
	__StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821  ___DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D192 <PrivateImplementationDetails>::DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289
	__StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64  ___DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D56 <PrivateImplementationDetails>::DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82
	__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  ___DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::E0219F11D9EECC43022AA94967780250AC270D4B
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___E0219F11D9EECC43022AA94967780250AC270D4B_268;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::E17D18DCD8392C99D47823F8CB9F43896D115FB8
	__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  ___E17D18DCD8392C99D47823F8CB9F43896D115FB8_270;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::E43F6BA634642FB90FEEE1A8F9905E957741960C
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___E43F6BA634642FB90FEEE1A8F9905E957741960C_272;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::E53E13AFB95C5C24DF50875117B7DDCE12937B2E
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D56 <PrivateImplementationDetails>::E647D32D165F3510693DF9787DC98E0A0B63C5C2
	__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  ___E647D32D165F3510693DF9787DC98E0A0B63C5C2_274;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1152 <PrivateImplementationDetails>::EB6F545AEF284339D25594F900E7A395212460EB
	__StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44  ___EB6F545AEF284339D25594F900E7A395212460EB_276;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D44 <PrivateImplementationDetails>::EBE167F7962841FA83451C9C1663416D69AA5294
	__StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942  ___EBE167F7962841FA83451C9C1663416D69AA5294_277;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::EEDBCB52C67688DE5F5FD9209E8A25BC786A2430
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D68 <PrivateImplementationDetails>::EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B
	__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  ___EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::EF813A47B13574822D335279EF445343654A4F04
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___EF813A47B13574822D335279EF445343654A4F04_281;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D64 <PrivateImplementationDetails>::F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9
	__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  ___F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::F128744756EEB38C3EAD4A7E8536EC5D3FA430FF
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285;
	// System.Int32 <PrivateImplementationDetails>::F3E701C38098B41D23FA88977423371B3C00C0D1
	int32_t ___F3E701C38098B41D23FA88977423371B3C00C0D1_286;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D68 <PrivateImplementationDetails>::F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF
	__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  ___F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::F5AEFD834ADB72DAA720930140E9ECC087FCF389
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___F5AEFD834ADB72DAA720930140E9ECC087FCF389_288;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::F63639527E877A2CBCB26FFD41D4A59470BFF8C8
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D1024 <PrivateImplementationDetails>::F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C
	__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  ___F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D4096 <PrivateImplementationDetails>::FA5B1C8B2F287078ED719C15595DB729BDB85911
	__StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44  ___FA5B1C8B2F287078ED719C15595DB729BDB85911_291;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::FAD52931F5B79811D31566BB18B6E0B5D2E2A164
	__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  ___FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::FB3C663794DD23F500825FF78450D198FE338938
	__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  ___FB3C663794DD23F500825FF78450D198FE338938_294;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D256 <PrivateImplementationDetails>::FC9EEBC457831129D4AF4FF84333B481F4BED60E
	__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  ___FC9EEBC457831129D4AF4FF84333B481F4BED60E_295;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F
	__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  ___FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::FE5567E8D769550852182CDF69D74BB16DFF8E29
	__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  ___FE5567E8D769550852182CDF69D74BB16DFF8E29_298;

public:
	inline static int32_t get_offset_of_U30060620709E93572EB9D62564332C778F7AE32E9_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0060620709E93572EB9D62564332C778F7AE32E9_0)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U30060620709E93572EB9D62564332C778F7AE32E9_0() const { return ___0060620709E93572EB9D62564332C778F7AE32E9_0; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U30060620709E93572EB9D62564332C778F7AE32E9_0() { return &___0060620709E93572EB9D62564332C778F7AE32E9_0; }
	inline void set_U30060620709E93572EB9D62564332C778F7AE32E9_0(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___0060620709E93572EB9D62564332C778F7AE32E9_0 = value;
	}

	inline static int32_t get_offset_of_U3007AFA4B68442DD6E0877DE7624D0A5F508C474F_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___007AFA4B68442DD6E0877DE7624D0A5F508C474F_1)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_U3007AFA4B68442DD6E0877DE7624D0A5F508C474F_1() const { return ___007AFA4B68442DD6E0877DE7624D0A5F508C474F_1; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_U3007AFA4B68442DD6E0877DE7624D0A5F508C474F_1() { return &___007AFA4B68442DD6E0877DE7624D0A5F508C474F_1; }
	inline void set_U3007AFA4B68442DD6E0877DE7624D0A5F508C474F_1(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___007AFA4B68442DD6E0877DE7624D0A5F508C474F_1 = value;
	}

	inline static int32_t get_offset_of_U300A4ED0900CBF010B3628AA473D0D1477DB67FFF_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___00A4ED0900CBF010B3628AA473D0D1477DB67FFF_2)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U300A4ED0900CBF010B3628AA473D0D1477DB67FFF_2() const { return ___00A4ED0900CBF010B3628AA473D0D1477DB67FFF_2; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U300A4ED0900CBF010B3628AA473D0D1477DB67FFF_2() { return &___00A4ED0900CBF010B3628AA473D0D1477DB67FFF_2; }
	inline void set_U300A4ED0900CBF010B3628AA473D0D1477DB67FFF_2(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___00A4ED0900CBF010B3628AA473D0D1477DB67FFF_2 = value;
	}

	inline static int32_t get_offset_of_U3013B76F7DD3E0346CB351F079CC062EFBD293457_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___013B76F7DD3E0346CB351F079CC062EFBD293457_3)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U3013B76F7DD3E0346CB351F079CC062EFBD293457_3() const { return ___013B76F7DD3E0346CB351F079CC062EFBD293457_3; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U3013B76F7DD3E0346CB351F079CC062EFBD293457_3() { return &___013B76F7DD3E0346CB351F079CC062EFBD293457_3; }
	inline void set_U3013B76F7DD3E0346CB351F079CC062EFBD293457_3(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___013B76F7DD3E0346CB351F079CC062EFBD293457_3 = value;
	}

	inline static int32_t get_offset_of_U30392525BCB01691D1F319D89F2C12BF93A478467_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0392525BCB01691D1F319D89F2C12BF93A478467_4)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U30392525BCB01691D1F319D89F2C12BF93A478467_4() const { return ___0392525BCB01691D1F319D89F2C12BF93A478467_4; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U30392525BCB01691D1F319D89F2C12BF93A478467_4() { return &___0392525BCB01691D1F319D89F2C12BF93A478467_4; }
	inline void set_U30392525BCB01691D1F319D89F2C12BF93A478467_4(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___0392525BCB01691D1F319D89F2C12BF93A478467_4 = value;
	}

	inline static int32_t get_offset_of_U303D9F1A66AED1E059B1609A09E435B708A88C8B8_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___03D9F1A66AED1E059B1609A09E435B708A88C8B8_5)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U303D9F1A66AED1E059B1609A09E435B708A88C8B8_5() const { return ___03D9F1A66AED1E059B1609A09E435B708A88C8B8_5; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U303D9F1A66AED1E059B1609A09E435B708A88C8B8_5() { return &___03D9F1A66AED1E059B1609A09E435B708A88C8B8_5; }
	inline void set_U303D9F1A66AED1E059B1609A09E435B708A88C8B8_5(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___03D9F1A66AED1E059B1609A09E435B708A88C8B8_5 = value;
	}

	inline static int32_t get_offset_of_U304A4F8F1CDFE8681F711039522BDB360478ACD84_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___04A4F8F1CDFE8681F711039522BDB360478ACD84_6)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U304A4F8F1CDFE8681F711039522BDB360478ACD84_6() const { return ___04A4F8F1CDFE8681F711039522BDB360478ACD84_6; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U304A4F8F1CDFE8681F711039522BDB360478ACD84_6() { return &___04A4F8F1CDFE8681F711039522BDB360478ACD84_6; }
	inline void set_U304A4F8F1CDFE8681F711039522BDB360478ACD84_6(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___04A4F8F1CDFE8681F711039522BDB360478ACD84_6 = value;
	}

	inline static int32_t get_offset_of_U304B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U304B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7() const { return ___04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U304B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7() { return &___04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7; }
	inline void set_U304B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7 = value;
	}

	inline static int32_t get_offset_of_U304D2A79C8A779AFAA779125335E9334C245EBB46_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___04D2A79C8A779AFAA779125335E9334C245EBB46_8)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U304D2A79C8A779AFAA779125335E9334C245EBB46_8() const { return ___04D2A79C8A779AFAA779125335E9334C245EBB46_8; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U304D2A79C8A779AFAA779125335E9334C245EBB46_8() { return &___04D2A79C8A779AFAA779125335E9334C245EBB46_8; }
	inline void set_U304D2A79C8A779AFAA779125335E9334C245EBB46_8(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___04D2A79C8A779AFAA779125335E9334C245EBB46_8 = value;
	}

	inline static int32_t get_offset_of_U3059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9)); }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  get_U3059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9() const { return ___059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9; }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B * get_address_of_U3059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9() { return &___059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9; }
	inline void set_U3059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9(__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  value)
	{
		___059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9 = value;
	}

	inline static int32_t get_offset_of_U3068B2E17352B5B9FF693CAE83421B679E0342A5C_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___068B2E17352B5B9FF693CAE83421B679E0342A5C_10)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U3068B2E17352B5B9FF693CAE83421B679E0342A5C_10() const { return ___068B2E17352B5B9FF693CAE83421B679E0342A5C_10; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U3068B2E17352B5B9FF693CAE83421B679E0342A5C_10() { return &___068B2E17352B5B9FF693CAE83421B679E0342A5C_10; }
	inline void set_U3068B2E17352B5B9FF693CAE83421B679E0342A5C_10(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___068B2E17352B5B9FF693CAE83421B679E0342A5C_10 = value;
	}

	inline static int32_t get_offset_of_U3083DE622A9A685DC50D8D5653CB388A41343C8EC_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___083DE622A9A685DC50D8D5653CB388A41343C8EC_11)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U3083DE622A9A685DC50D8D5653CB388A41343C8EC_11() const { return ___083DE622A9A685DC50D8D5653CB388A41343C8EC_11; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U3083DE622A9A685DC50D8D5653CB388A41343C8EC_11() { return &___083DE622A9A685DC50D8D5653CB388A41343C8EC_11; }
	inline void set_U3083DE622A9A685DC50D8D5653CB388A41343C8EC_11(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___083DE622A9A685DC50D8D5653CB388A41343C8EC_11 = value;
	}

	inline static int32_t get_offset_of_U3083EF765A34AC9DEC41761B4DCEA48EC009115E9_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___083EF765A34AC9DEC41761B4DCEA48EC009115E9_12)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3083EF765A34AC9DEC41761B4DCEA48EC009115E9_12() const { return ___083EF765A34AC9DEC41761B4DCEA48EC009115E9_12; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3083EF765A34AC9DEC41761B4DCEA48EC009115E9_12() { return &___083EF765A34AC9DEC41761B4DCEA48EC009115E9_12; }
	inline void set_U3083EF765A34AC9DEC41761B4DCEA48EC009115E9_12(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___083EF765A34AC9DEC41761B4DCEA48EC009115E9_12 = value;
	}

	inline static int32_t get_offset_of_U30953DF544832295E4A5B19928F95C351F25DA86A_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0953DF544832295E4A5B19928F95C351F25DA86A_13)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U30953DF544832295E4A5B19928F95C351F25DA86A_13() const { return ___0953DF544832295E4A5B19928F95C351F25DA86A_13; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U30953DF544832295E4A5B19928F95C351F25DA86A_13() { return &___0953DF544832295E4A5B19928F95C351F25DA86A_13; }
	inline void set_U30953DF544832295E4A5B19928F95C351F25DA86A_13(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___0953DF544832295E4A5B19928F95C351F25DA86A_13 = value;
	}

	inline static int32_t get_offset_of_U3095B351FE2104237B032546280C98C9804D331C5_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___095B351FE2104237B032546280C98C9804D331C5_14)); }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  get_U3095B351FE2104237B032546280C98C9804D331C5_14() const { return ___095B351FE2104237B032546280C98C9804D331C5_14; }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A * get_address_of_U3095B351FE2104237B032546280C98C9804D331C5_14() { return &___095B351FE2104237B032546280C98C9804D331C5_14; }
	inline void set_U3095B351FE2104237B032546280C98C9804D331C5_14(__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  value)
	{
		___095B351FE2104237B032546280C98C9804D331C5_14 = value;
	}

	inline static int32_t get_offset_of_U30982B1B45B764F2694ABC3DE57204AC898651429_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0982B1B45B764F2694ABC3DE57204AC898651429_15)); }
	inline __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  get_U30982B1B45B764F2694ABC3DE57204AC898651429_15() const { return ___0982B1B45B764F2694ABC3DE57204AC898651429_15; }
	inline __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981 * get_address_of_U30982B1B45B764F2694ABC3DE57204AC898651429_15() { return &___0982B1B45B764F2694ABC3DE57204AC898651429_15; }
	inline void set_U30982B1B45B764F2694ABC3DE57204AC898651429_15(__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  value)
	{
		___0982B1B45B764F2694ABC3DE57204AC898651429_15 = value;
	}

	inline static int32_t get_offset_of_U30B1E7265E67D2458D8EB536A4B36380A5BF6E731_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0B1E7265E67D2458D8EB536A4B36380A5BF6E731_16)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U30B1E7265E67D2458D8EB536A4B36380A5BF6E731_16() const { return ___0B1E7265E67D2458D8EB536A4B36380A5BF6E731_16; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U30B1E7265E67D2458D8EB536A4B36380A5BF6E731_16() { return &___0B1E7265E67D2458D8EB536A4B36380A5BF6E731_16; }
	inline void set_U30B1E7265E67D2458D8EB536A4B36380A5BF6E731_16(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___0B1E7265E67D2458D8EB536A4B36380A5BF6E731_16 = value;
	}

	inline static int32_t get_offset_of_U30BE6B8194AB353640CA66F0A661EA93A3132393A_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0BE6B8194AB353640CA66F0A661EA93A3132393A_17)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U30BE6B8194AB353640CA66F0A661EA93A3132393A_17() const { return ___0BE6B8194AB353640CA66F0A661EA93A3132393A_17; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U30BE6B8194AB353640CA66F0A661EA93A3132393A_17() { return &___0BE6B8194AB353640CA66F0A661EA93A3132393A_17; }
	inline void set_U30BE6B8194AB353640CA66F0A661EA93A3132393A_17(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___0BE6B8194AB353640CA66F0A661EA93A3132393A_17 = value;
	}

	inline static int32_t get_offset_of_U30C4110BC17D746F018F47B49E0EB0D6590F69939_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0C4110BC17D746F018F47B49E0EB0D6590F69939_18)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U30C4110BC17D746F018F47B49E0EB0D6590F69939_18() const { return ___0C4110BC17D746F018F47B49E0EB0D6590F69939_18; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U30C4110BC17D746F018F47B49E0EB0D6590F69939_18() { return &___0C4110BC17D746F018F47B49E0EB0D6590F69939_18; }
	inline void set_U30C4110BC17D746F018F47B49E0EB0D6590F69939_18(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___0C4110BC17D746F018F47B49E0EB0D6590F69939_18 = value;
	}

	inline static int32_t get_offset_of_U30D0825E62E82DBEBFAD598623694129548E24C9C_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0D0825E62E82DBEBFAD598623694129548E24C9C_19)); }
	inline __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  get_U30D0825E62E82DBEBFAD598623694129548E24C9C_19() const { return ___0D0825E62E82DBEBFAD598623694129548E24C9C_19; }
	inline __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C * get_address_of_U30D0825E62E82DBEBFAD598623694129548E24C9C_19() { return &___0D0825E62E82DBEBFAD598623694129548E24C9C_19; }
	inline void set_U30D0825E62E82DBEBFAD598623694129548E24C9C_19(__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  value)
	{
		___0D0825E62E82DBEBFAD598623694129548E24C9C_19 = value;
	}

	inline static int32_t get_offset_of_U30D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U30D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20() const { return ___0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U30D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20() { return &___0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20; }
	inline void set_U30D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20 = value;
	}

	inline static int32_t get_offset_of_U30D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U30D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21() const { return ___0D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U30D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21() { return &___0D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21; }
	inline void set_U30D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___0D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21 = value;
	}

	inline static int32_t get_offset_of_U30E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___0E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_U30E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22() const { return ___0E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_U30E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22() { return &___0E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22; }
	inline void set_U30E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___0E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22 = value;
	}

	inline static int32_t get_offset_of_U31005BA20F99323E3F050E781BB81D1A4479AB037_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1005BA20F99323E3F050E781BB81D1A4479AB037_23)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_U31005BA20F99323E3F050E781BB81D1A4479AB037_23() const { return ___1005BA20F99323E3F050E781BB81D1A4479AB037_23; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_U31005BA20F99323E3F050E781BB81D1A4479AB037_23() { return &___1005BA20F99323E3F050E781BB81D1A4479AB037_23; }
	inline void set_U31005BA20F99323E3F050E781BB81D1A4479AB037_23(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___1005BA20F99323E3F050E781BB81D1A4479AB037_23 = value;
	}

	inline static int32_t get_offset_of_U3102C522344FCAC1545BDA50C0FC675C502FFEC53_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___102C522344FCAC1545BDA50C0FC675C502FFEC53_24)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U3102C522344FCAC1545BDA50C0FC675C502FFEC53_24() const { return ___102C522344FCAC1545BDA50C0FC675C502FFEC53_24; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U3102C522344FCAC1545BDA50C0FC675C502FFEC53_24() { return &___102C522344FCAC1545BDA50C0FC675C502FFEC53_24; }
	inline void set_U3102C522344FCAC1545BDA50C0FC675C502FFEC53_24(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___102C522344FCAC1545BDA50C0FC675C502FFEC53_24 = value;
	}

	inline static int32_t get_offset_of_U3103752E99F3718E46F1AA9EED70682BF3A9B8A65_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___103752E99F3718E46F1AA9EED70682BF3A9B8A65_25)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3103752E99F3718E46F1AA9EED70682BF3A9B8A65_25() const { return ___103752E99F3718E46F1AA9EED70682BF3A9B8A65_25; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3103752E99F3718E46F1AA9EED70682BF3A9B8A65_25() { return &___103752E99F3718E46F1AA9EED70682BF3A9B8A65_25; }
	inline void set_U3103752E99F3718E46F1AA9EED70682BF3A9B8A65_25(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___103752E99F3718E46F1AA9EED70682BF3A9B8A65_25 = value;
	}

	inline static int32_t get_offset_of_U3118FE9754A9699004D478E4BF686021154D96EAD_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___118FE9754A9699004D478E4BF686021154D96EAD_26)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3118FE9754A9699004D478E4BF686021154D96EAD_26() const { return ___118FE9754A9699004D478E4BF686021154D96EAD_26; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3118FE9754A9699004D478E4BF686021154D96EAD_26() { return &___118FE9754A9699004D478E4BF686021154D96EAD_26; }
	inline void set_U3118FE9754A9699004D478E4BF686021154D96EAD_26(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___118FE9754A9699004D478E4BF686021154D96EAD_26 = value;
	}

	inline static int32_t get_offset_of_U3124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U3124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27() const { return ___124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U3124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27() { return &___124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27; }
	inline void set_U3124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27 = value;
	}

	inline static int32_t get_offset_of_U3126589410FF9CA1510B9950BF0E79E5BFD60000B_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___126589410FF9CA1510B9950BF0E79E5BFD60000B_28)); }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  get_U3126589410FF9CA1510B9950BF0E79E5BFD60000B_28() const { return ___126589410FF9CA1510B9950BF0E79E5BFD60000B_28; }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B * get_address_of_U3126589410FF9CA1510B9950BF0E79E5BFD60000B_28() { return &___126589410FF9CA1510B9950BF0E79E5BFD60000B_28; }
	inline void set_U3126589410FF9CA1510B9950BF0E79E5BFD60000B_28(__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  value)
	{
		___126589410FF9CA1510B9950BF0E79E5BFD60000B_28 = value;
	}

	inline static int32_t get_offset_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29)); }
	inline int32_t get_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29() const { return ___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29; }
	inline int32_t* get_address_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29() { return &___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29; }
	inline void set_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29(int32_t value)
	{
		___1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29 = value;
	}

	inline static int32_t get_offset_of_U314353D63A97A25640437E7C0DAE8B1596F34AB2C_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___14353D63A97A25640437E7C0DAE8B1596F34AB2C_30)); }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  get_U314353D63A97A25640437E7C0DAE8B1596F34AB2C_30() const { return ___14353D63A97A25640437E7C0DAE8B1596F34AB2C_30; }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 * get_address_of_U314353D63A97A25640437E7C0DAE8B1596F34AB2C_30() { return &___14353D63A97A25640437E7C0DAE8B1596F34AB2C_30; }
	inline void set_U314353D63A97A25640437E7C0DAE8B1596F34AB2C_30(__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  value)
	{
		___14353D63A97A25640437E7C0DAE8B1596F34AB2C_30 = value;
	}

	inline static int32_t get_offset_of_U3146E4D4A36742E37DEE528D23474B9675695E406_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___146E4D4A36742E37DEE528D23474B9675695E406_31)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U3146E4D4A36742E37DEE528D23474B9675695E406_31() const { return ___146E4D4A36742E37DEE528D23474B9675695E406_31; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U3146E4D4A36742E37DEE528D23474B9675695E406_31() { return &___146E4D4A36742E37DEE528D23474B9675695E406_31; }
	inline void set_U3146E4D4A36742E37DEE528D23474B9675695E406_31(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___146E4D4A36742E37DEE528D23474B9675695E406_31 = value;
	}

	inline static int32_t get_offset_of_U3148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32() const { return ___148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32() { return &___148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32; }
	inline void set_U3148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32 = value;
	}

	inline static int32_t get_offset_of_U314F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U314F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33() const { return ___14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U314F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33() { return &___14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33; }
	inline void set_U314F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33 = value;
	}

	inline static int32_t get_offset_of_U31648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U31648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34() const { return ___1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U31648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34() { return &___1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34; }
	inline void set_U31648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34 = value;
	}

	inline static int32_t get_offset_of_U317651A9FA4DEA6C24D1287324CF4A640D080FE8E_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___17651A9FA4DEA6C24D1287324CF4A640D080FE8E_35)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U317651A9FA4DEA6C24D1287324CF4A640D080FE8E_35() const { return ___17651A9FA4DEA6C24D1287324CF4A640D080FE8E_35; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U317651A9FA4DEA6C24D1287324CF4A640D080FE8E_35() { return &___17651A9FA4DEA6C24D1287324CF4A640D080FE8E_35; }
	inline void set_U317651A9FA4DEA6C24D1287324CF4A640D080FE8E_35(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___17651A9FA4DEA6C24D1287324CF4A640D080FE8E_35 = value;
	}

	inline static int32_t get_offset_of_U317BCB27C371D710141A300DA5A9A3EADF78D7A35_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___17BCB27C371D710141A300DA5A9A3EADF78D7A35_36)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U317BCB27C371D710141A300DA5A9A3EADF78D7A35_36() const { return ___17BCB27C371D710141A300DA5A9A3EADF78D7A35_36; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U317BCB27C371D710141A300DA5A9A3EADF78D7A35_36() { return &___17BCB27C371D710141A300DA5A9A3EADF78D7A35_36; }
	inline void set_U317BCB27C371D710141A300DA5A9A3EADF78D7A35_36(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___17BCB27C371D710141A300DA5A9A3EADF78D7A35_36 = value;
	}

	inline static int32_t get_offset_of_U317E54FCA28103DF892BBB946DDEED4B061D7F5C7_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___17E54FCA28103DF892BBB946DDEED4B061D7F5C7_37)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_U317E54FCA28103DF892BBB946DDEED4B061D7F5C7_37() const { return ___17E54FCA28103DF892BBB946DDEED4B061D7F5C7_37; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_U317E54FCA28103DF892BBB946DDEED4B061D7F5C7_37() { return &___17E54FCA28103DF892BBB946DDEED4B061D7F5C7_37; }
	inline void set_U317E54FCA28103DF892BBB946DDEED4B061D7F5C7_37(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___17E54FCA28103DF892BBB946DDEED4B061D7F5C7_37 = value;
	}

	inline static int32_t get_offset_of_U318F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38)); }
	inline int32_t get_U318F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38() const { return ___18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38; }
	inline int32_t* get_address_of_U318F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38() { return &___18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38; }
	inline void set_U318F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38(int32_t value)
	{
		___18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38 = value;
	}

	inline static int32_t get_offset_of_U31A43D7FEEED03520E11C4A8996F149705D99C6BB_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1A43D7FEEED03520E11C4A8996F149705D99C6BB_39)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U31A43D7FEEED03520E11C4A8996F149705D99C6BB_39() const { return ___1A43D7FEEED03520E11C4A8996F149705D99C6BB_39; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U31A43D7FEEED03520E11C4A8996F149705D99C6BB_39() { return &___1A43D7FEEED03520E11C4A8996F149705D99C6BB_39; }
	inline void set_U31A43D7FEEED03520E11C4A8996F149705D99C6BB_39(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___1A43D7FEEED03520E11C4A8996F149705D99C6BB_39 = value;
	}

	inline static int32_t get_offset_of_U31AFB455399A50580CF1039188ABA6BE82F309543_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1AFB455399A50580CF1039188ABA6BE82F309543_40)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U31AFB455399A50580CF1039188ABA6BE82F309543_40() const { return ___1AFB455399A50580CF1039188ABA6BE82F309543_40; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U31AFB455399A50580CF1039188ABA6BE82F309543_40() { return &___1AFB455399A50580CF1039188ABA6BE82F309543_40; }
	inline void set_U31AFB455399A50580CF1039188ABA6BE82F309543_40(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___1AFB455399A50580CF1039188ABA6BE82F309543_40 = value;
	}

	inline static int32_t get_offset_of_U31B180C6E41F096D53222F5E8EF558B78182CA401_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1B180C6E41F096D53222F5E8EF558B78182CA401_41)); }
	inline __StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD  get_U31B180C6E41F096D53222F5E8EF558B78182CA401_41() const { return ___1B180C6E41F096D53222F5E8EF558B78182CA401_41; }
	inline __StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD * get_address_of_U31B180C6E41F096D53222F5E8EF558B78182CA401_41() { return &___1B180C6E41F096D53222F5E8EF558B78182CA401_41; }
	inline void set_U31B180C6E41F096D53222F5E8EF558B78182CA401_41(__StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD  value)
	{
		___1B180C6E41F096D53222F5E8EF558B78182CA401_41 = value;
	}

	inline static int32_t get_offset_of_U31C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U31C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42() const { return ___1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U31C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42() { return &___1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42; }
	inline void set_U31C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42 = value;
	}

	inline static int32_t get_offset_of_U31D3E73DD251585C4908CBA58A179E1911834C891_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1D3E73DD251585C4908CBA58A179E1911834C891_43)); }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  get_U31D3E73DD251585C4908CBA58A179E1911834C891_43() const { return ___1D3E73DD251585C4908CBA58A179E1911834C891_43; }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A * get_address_of_U31D3E73DD251585C4908CBA58A179E1911834C891_43() { return &___1D3E73DD251585C4908CBA58A179E1911834C891_43; }
	inline void set_U31D3E73DD251585C4908CBA58A179E1911834C891_43(__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  value)
	{
		___1D3E73DD251585C4908CBA58A179E1911834C891_43 = value;
	}

	inline static int32_t get_offset_of_U31E3842329C5294DBE1DF588A77C68B35C6AF83BF_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1E3842329C5294DBE1DF588A77C68B35C6AF83BF_44)); }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  get_U31E3842329C5294DBE1DF588A77C68B35C6AF83BF_44() const { return ___1E3842329C5294DBE1DF588A77C68B35C6AF83BF_44; }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B * get_address_of_U31E3842329C5294DBE1DF588A77C68B35C6AF83BF_44() { return &___1E3842329C5294DBE1DF588A77C68B35C6AF83BF_44; }
	inline void set_U31E3842329C5294DBE1DF588A77C68B35C6AF83BF_44(__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  value)
	{
		___1E3842329C5294DBE1DF588A77C68B35C6AF83BF_44 = value;
	}

	inline static int32_t get_offset_of_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45)); }
	inline __StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE  get_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45() const { return ___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45; }
	inline __StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE * get_address_of_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45() { return &___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45; }
	inline void set_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45(__StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE  value)
	{
		___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45 = value;
	}

	inline static int32_t get_offset_of_U321305ABEF36592B9C636A4DDFFB419BBEB14AC71_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___21305ABEF36592B9C636A4DDFFB419BBEB14AC71_46)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U321305ABEF36592B9C636A4DDFFB419BBEB14AC71_46() const { return ___21305ABEF36592B9C636A4DDFFB419BBEB14AC71_46; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U321305ABEF36592B9C636A4DDFFB419BBEB14AC71_46() { return &___21305ABEF36592B9C636A4DDFFB419BBEB14AC71_46; }
	inline void set_U321305ABEF36592B9C636A4DDFFB419BBEB14AC71_46(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___21305ABEF36592B9C636A4DDFFB419BBEB14AC71_46 = value;
	}

	inline static int32_t get_offset_of_U3214F93D9222D60794CE1EA0A10389885C5CA9824_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___214F93D9222D60794CE1EA0A10389885C5CA9824_47)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3214F93D9222D60794CE1EA0A10389885C5CA9824_47() const { return ___214F93D9222D60794CE1EA0A10389885C5CA9824_47; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3214F93D9222D60794CE1EA0A10389885C5CA9824_47() { return &___214F93D9222D60794CE1EA0A10389885C5CA9824_47; }
	inline void set_U3214F93D9222D60794CE1EA0A10389885C5CA9824_47(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___214F93D9222D60794CE1EA0A10389885C5CA9824_47 = value;
	}

	inline static int32_t get_offset_of_U321A3E0CD2847E2F12EEE37749A9E206494A55100_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___21A3E0CD2847E2F12EEE37749A9E206494A55100_48)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U321A3E0CD2847E2F12EEE37749A9E206494A55100_48() const { return ___21A3E0CD2847E2F12EEE37749A9E206494A55100_48; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U321A3E0CD2847E2F12EEE37749A9E206494A55100_48() { return &___21A3E0CD2847E2F12EEE37749A9E206494A55100_48; }
	inline void set_U321A3E0CD2847E2F12EEE37749A9E206494A55100_48(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___21A3E0CD2847E2F12EEE37749A9E206494A55100_48 = value;
	}

	inline static int32_t get_offset_of_U322F13A28C218AA9B43303043F2CF664790D12BD7_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___22F13A28C218AA9B43303043F2CF664790D12BD7_49)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U322F13A28C218AA9B43303043F2CF664790D12BD7_49() const { return ___22F13A28C218AA9B43303043F2CF664790D12BD7_49; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U322F13A28C218AA9B43303043F2CF664790D12BD7_49() { return &___22F13A28C218AA9B43303043F2CF664790D12BD7_49; }
	inline void set_U322F13A28C218AA9B43303043F2CF664790D12BD7_49(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___22F13A28C218AA9B43303043F2CF664790D12BD7_49 = value;
	}

	inline static int32_t get_offset_of_U3239B59488F1CE7EBE225785FDC22A8E3102A2E82_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___239B59488F1CE7EBE225785FDC22A8E3102A2E82_50)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U3239B59488F1CE7EBE225785FDC22A8E3102A2E82_50() const { return ___239B59488F1CE7EBE225785FDC22A8E3102A2E82_50; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U3239B59488F1CE7EBE225785FDC22A8E3102A2E82_50() { return &___239B59488F1CE7EBE225785FDC22A8E3102A2E82_50; }
	inline void set_U3239B59488F1CE7EBE225785FDC22A8E3102A2E82_50(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___239B59488F1CE7EBE225785FDC22A8E3102A2E82_50 = value;
	}

	inline static int32_t get_offset_of_U326853A2C322BBAD5BBD886C60A32BBBCFE847F00_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___26853A2C322BBAD5BBD886C60A32BBBCFE847F00_51)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U326853A2C322BBAD5BBD886C60A32BBBCFE847F00_51() const { return ___26853A2C322BBAD5BBD886C60A32BBBCFE847F00_51; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U326853A2C322BBAD5BBD886C60A32BBBCFE847F00_51() { return &___26853A2C322BBAD5BBD886C60A32BBBCFE847F00_51; }
	inline void set_U326853A2C322BBAD5BBD886C60A32BBBCFE847F00_51(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___26853A2C322BBAD5BBD886C60A32BBBCFE847F00_51 = value;
	}

	inline static int32_t get_offset_of_U327C3AB82CCA6CE2F199F4F670BF19513A3825B87_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___27C3AB82CCA6CE2F199F4F670BF19513A3825B87_52)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U327C3AB82CCA6CE2F199F4F670BF19513A3825B87_52() const { return ___27C3AB82CCA6CE2F199F4F670BF19513A3825B87_52; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U327C3AB82CCA6CE2F199F4F670BF19513A3825B87_52() { return &___27C3AB82CCA6CE2F199F4F670BF19513A3825B87_52; }
	inline void set_U327C3AB82CCA6CE2F199F4F670BF19513A3825B87_52(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___27C3AB82CCA6CE2F199F4F670BF19513A3825B87_52 = value;
	}

	inline static int32_t get_offset_of_U327FED0F92A97C41B08D3115553BBDC064F417B6E_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___27FED0F92A97C41B08D3115553BBDC064F417B6E_53)); }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  get_U327FED0F92A97C41B08D3115553BBDC064F417B6E_53() const { return ___27FED0F92A97C41B08D3115553BBDC064F417B6E_53; }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A * get_address_of_U327FED0F92A97C41B08D3115553BBDC064F417B6E_53() { return &___27FED0F92A97C41B08D3115553BBDC064F417B6E_53; }
	inline void set_U327FED0F92A97C41B08D3115553BBDC064F417B6E_53(__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  value)
	{
		___27FED0F92A97C41B08D3115553BBDC064F417B6E_53 = value;
	}

	inline static int32_t get_offset_of_U329EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U329EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54() const { return ___29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U329EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54() { return &___29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54; }
	inline void set_U329EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54 = value;
	}

	inline static int32_t get_offset_of_U329F7A0217340B5682E7DDF98ADAD952E2A360E65_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___29F7A0217340B5682E7DDF98ADAD952E2A360E65_55)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U329F7A0217340B5682E7DDF98ADAD952E2A360E65_55() const { return ___29F7A0217340B5682E7DDF98ADAD952E2A360E65_55; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U329F7A0217340B5682E7DDF98ADAD952E2A360E65_55() { return &___29F7A0217340B5682E7DDF98ADAD952E2A360E65_55; }
	inline void set_U329F7A0217340B5682E7DDF98ADAD952E2A360E65_55(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___29F7A0217340B5682E7DDF98ADAD952E2A360E65_55 = value;
	}

	inline static int32_t get_offset_of_U32BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_U32BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56() const { return ___2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_U32BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56() { return &___2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56; }
	inline void set_U32BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56 = value;
	}

	inline static int32_t get_offset_of_U32CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U32CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57() const { return ___2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U32CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57() { return &___2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57; }
	inline void set_U32CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57 = value;
	}

	inline static int32_t get_offset_of_U32CDACAA27B7865A56A57B156CFB369245964BD2B_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___2CDACAA27B7865A56A57B156CFB369245964BD2B_58)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U32CDACAA27B7865A56A57B156CFB369245964BD2B_58() const { return ___2CDACAA27B7865A56A57B156CFB369245964BD2B_58; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U32CDACAA27B7865A56A57B156CFB369245964BD2B_58() { return &___2CDACAA27B7865A56A57B156CFB369245964BD2B_58; }
	inline void set_U32CDACAA27B7865A56A57B156CFB369245964BD2B_58(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___2CDACAA27B7865A56A57B156CFB369245964BD2B_58 = value;
	}

	inline static int32_t get_offset_of_U32E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59)); }
	inline __StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27  get_U32E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59() const { return ___2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59; }
	inline __StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27 * get_address_of_U32E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59() { return &___2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59; }
	inline void set_U32E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59(__StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27  value)
	{
		___2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59 = value;
	}

	inline static int32_t get_offset_of_U32FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U32FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60() const { return ___2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U32FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60() { return &___2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60; }
	inline void set_U32FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60 = value;
	}

	inline static int32_t get_offset_of_U33045D5B29BA900304981918A153806D371B02549_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3045D5B29BA900304981918A153806D371B02549_61)); }
	inline int32_t get_U33045D5B29BA900304981918A153806D371B02549_61() const { return ___3045D5B29BA900304981918A153806D371B02549_61; }
	inline int32_t* get_address_of_U33045D5B29BA900304981918A153806D371B02549_61() { return &___3045D5B29BA900304981918A153806D371B02549_61; }
	inline void set_U33045D5B29BA900304981918A153806D371B02549_61(int32_t value)
	{
		___3045D5B29BA900304981918A153806D371B02549_61 = value;
	}

	inline static int32_t get_offset_of_U330A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___30A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U330A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62() const { return ___30A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U330A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62() { return &___30A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62; }
	inline void set_U330A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___30A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62 = value;
	}

	inline static int32_t get_offset_of_U330F65A149AF7DE938A9287048498B966AEBE54D4_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___30F65A149AF7DE938A9287048498B966AEBE54D4_63)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U330F65A149AF7DE938A9287048498B966AEBE54D4_63() const { return ___30F65A149AF7DE938A9287048498B966AEBE54D4_63; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U330F65A149AF7DE938A9287048498B966AEBE54D4_63() { return &___30F65A149AF7DE938A9287048498B966AEBE54D4_63; }
	inline void set_U330F65A149AF7DE938A9287048498B966AEBE54D4_63(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___30F65A149AF7DE938A9287048498B966AEBE54D4_63 = value;
	}

	inline static int32_t get_offset_of_U3310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U3310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64() const { return ___310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U3310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64() { return &___310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64; }
	inline void set_U3310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64 = value;
	}

	inline static int32_t get_offset_of_U3310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U3310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65() const { return ___310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U3310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65() { return &___310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65; }
	inline void set_U3310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65 = value;
	}

	inline static int32_t get_offset_of_U331CDD717843C5C2B207F235634E6726898D4858A_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___31CDD717843C5C2B207F235634E6726898D4858A_66)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_U331CDD717843C5C2B207F235634E6726898D4858A_66() const { return ___31CDD717843C5C2B207F235634E6726898D4858A_66; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_U331CDD717843C5C2B207F235634E6726898D4858A_66() { return &___31CDD717843C5C2B207F235634E6726898D4858A_66; }
	inline void set_U331CDD717843C5C2B207F235634E6726898D4858A_66(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___31CDD717843C5C2B207F235634E6726898D4858A_66 = value;
	}

	inline static int32_t get_offset_of_U331D8729F7377B44017C0A2395A582C9CA4163277_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___31D8729F7377B44017C0A2395A582C9CA4163277_67)); }
	inline int32_t get_U331D8729F7377B44017C0A2395A582C9CA4163277_67() const { return ___31D8729F7377B44017C0A2395A582C9CA4163277_67; }
	inline int32_t* get_address_of_U331D8729F7377B44017C0A2395A582C9CA4163277_67() { return &___31D8729F7377B44017C0A2395A582C9CA4163277_67; }
	inline void set_U331D8729F7377B44017C0A2395A582C9CA4163277_67(int32_t value)
	{
		___31D8729F7377B44017C0A2395A582C9CA4163277_67 = value;
	}

	inline static int32_t get_offset_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68() const { return ___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68() { return &___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68; }
	inline void set_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68 = value;
	}

	inline static int32_t get_offset_of_U3321CB68F886E22F95877B3535C1B34A6A94A40B6_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___321CB68F886E22F95877B3535C1B34A6A94A40B6_69)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U3321CB68F886E22F95877B3535C1B34A6A94A40B6_69() const { return ___321CB68F886E22F95877B3535C1B34A6A94A40B6_69; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U3321CB68F886E22F95877B3535C1B34A6A94A40B6_69() { return &___321CB68F886E22F95877B3535C1B34A6A94A40B6_69; }
	inline void set_U3321CB68F886E22F95877B3535C1B34A6A94A40B6_69(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___321CB68F886E22F95877B3535C1B34A6A94A40B6_69 = value;
	}

	inline static int32_t get_offset_of_U332ECB35FF8400B4E56FF5E09588FB20DD60350E7_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___32ECB35FF8400B4E56FF5E09588FB20DD60350E7_70)); }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  get_U332ECB35FF8400B4E56FF5E09588FB20DD60350E7_70() const { return ___32ECB35FF8400B4E56FF5E09588FB20DD60350E7_70; }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B * get_address_of_U332ECB35FF8400B4E56FF5E09588FB20DD60350E7_70() { return &___32ECB35FF8400B4E56FF5E09588FB20DD60350E7_70; }
	inline void set_U332ECB35FF8400B4E56FF5E09588FB20DD60350E7_70(__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  value)
	{
		___32ECB35FF8400B4E56FF5E09588FB20DD60350E7_70 = value;
	}

	inline static int32_t get_offset_of_U3340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U3340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71() const { return ___340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U3340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71() { return &___340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71; }
	inline void set_U3340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71 = value;
	}

	inline static int32_t get_offset_of_U33544182260B8A15D332367E48C7530FC0E901FD3_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3544182260B8A15D332367E48C7530FC0E901FD3_72)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U33544182260B8A15D332367E48C7530FC0E901FD3_72() const { return ___3544182260B8A15D332367E48C7530FC0E901FD3_72; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U33544182260B8A15D332367E48C7530FC0E901FD3_72() { return &___3544182260B8A15D332367E48C7530FC0E901FD3_72; }
	inline void set_U33544182260B8A15D332367E48C7530FC0E901FD3_72(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___3544182260B8A15D332367E48C7530FC0E901FD3_72 = value;
	}

	inline static int32_t get_offset_of_U335D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73)); }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  get_U335D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73() const { return ___35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73; }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A * get_address_of_U335D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73() { return &___35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73; }
	inline void set_U335D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73(__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  value)
	{
		___35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73 = value;
	}

	inline static int32_t get_offset_of_U335E6464339FFAE0D3777B12A371F82D2D1F668CA_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___35E6464339FFAE0D3777B12A371F82D2D1F668CA_74)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U335E6464339FFAE0D3777B12A371F82D2D1F668CA_74() const { return ___35E6464339FFAE0D3777B12A371F82D2D1F668CA_74; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U335E6464339FFAE0D3777B12A371F82D2D1F668CA_74() { return &___35E6464339FFAE0D3777B12A371F82D2D1F668CA_74; }
	inline void set_U335E6464339FFAE0D3777B12A371F82D2D1F668CA_74(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___35E6464339FFAE0D3777B12A371F82D2D1F668CA_74 = value;
	}

	inline static int32_t get_offset_of_U3372040F482ABADADF58EF0C31A6A8BE386AF8A50_75() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___372040F482ABADADF58EF0C31A6A8BE386AF8A50_75)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3372040F482ABADADF58EF0C31A6A8BE386AF8A50_75() const { return ___372040F482ABADADF58EF0C31A6A8BE386AF8A50_75; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3372040F482ABADADF58EF0C31A6A8BE386AF8A50_75() { return &___372040F482ABADADF58EF0C31A6A8BE386AF8A50_75; }
	inline void set_U3372040F482ABADADF58EF0C31A6A8BE386AF8A50_75(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___372040F482ABADADF58EF0C31A6A8BE386AF8A50_75 = value;
	}

	inline static int32_t get_offset_of_U337454D933508E238CFB980F1077B24ADA4A480F4_76() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___37454D933508E238CFB980F1077B24ADA4A480F4_76)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U337454D933508E238CFB980F1077B24ADA4A480F4_76() const { return ___37454D933508E238CFB980F1077B24ADA4A480F4_76; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U337454D933508E238CFB980F1077B24ADA4A480F4_76() { return &___37454D933508E238CFB980F1077B24ADA4A480F4_76; }
	inline void set_U337454D933508E238CFB980F1077B24ADA4A480F4_76(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___37454D933508E238CFB980F1077B24ADA4A480F4_76 = value;
	}

	inline static int32_t get_offset_of_U33A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_U33A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77() const { return ___3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_U33A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77() { return &___3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77; }
	inline void set_U33A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77 = value;
	}

	inline static int32_t get_offset_of_U33BA78FFB102A191597BAC4A2418C57CDF377FEE6_78() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3BA78FFB102A191597BAC4A2418C57CDF377FEE6_78)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U33BA78FFB102A191597BAC4A2418C57CDF377FEE6_78() const { return ___3BA78FFB102A191597BAC4A2418C57CDF377FEE6_78; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U33BA78FFB102A191597BAC4A2418C57CDF377FEE6_78() { return &___3BA78FFB102A191597BAC4A2418C57CDF377FEE6_78; }
	inline void set_U33BA78FFB102A191597BAC4A2418C57CDF377FEE6_78(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___3BA78FFB102A191597BAC4A2418C57CDF377FEE6_78 = value;
	}

	inline static int32_t get_offset_of_U33C3704DB6466D48DD40FE189070F1896D1D45C8B_79() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3C3704DB6466D48DD40FE189070F1896D1D45C8B_79)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U33C3704DB6466D48DD40FE189070F1896D1D45C8B_79() const { return ___3C3704DB6466D48DD40FE189070F1896D1D45C8B_79; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U33C3704DB6466D48DD40FE189070F1896D1D45C8B_79() { return &___3C3704DB6466D48DD40FE189070F1896D1D45C8B_79; }
	inline void set_U33C3704DB6466D48DD40FE189070F1896D1D45C8B_79(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___3C3704DB6466D48DD40FE189070F1896D1D45C8B_79 = value;
	}

	inline static int32_t get_offset_of_U33D6EB645BC212077C1B37A3A32CA2A62F7B39018_80() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3D6EB645BC212077C1B37A3A32CA2A62F7B39018_80)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U33D6EB645BC212077C1B37A3A32CA2A62F7B39018_80() const { return ___3D6EB645BC212077C1B37A3A32CA2A62F7B39018_80; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U33D6EB645BC212077C1B37A3A32CA2A62F7B39018_80() { return &___3D6EB645BC212077C1B37A3A32CA2A62F7B39018_80; }
	inline void set_U33D6EB645BC212077C1B37A3A32CA2A62F7B39018_80(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___3D6EB645BC212077C1B37A3A32CA2A62F7B39018_80 = value;
	}

	inline static int32_t get_offset_of_U33E8E493888B1BFB763D7553A6CC1978C17C198A3_81() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3E8E493888B1BFB763D7553A6CC1978C17C198A3_81)); }
	inline int32_t get_U33E8E493888B1BFB763D7553A6CC1978C17C198A3_81() const { return ___3E8E493888B1BFB763D7553A6CC1978C17C198A3_81; }
	inline int32_t* get_address_of_U33E8E493888B1BFB763D7553A6CC1978C17C198A3_81() { return &___3E8E493888B1BFB763D7553A6CC1978C17C198A3_81; }
	inline void set_U33E8E493888B1BFB763D7553A6CC1978C17C198A3_81(int32_t value)
	{
		___3E8E493888B1BFB763D7553A6CC1978C17C198A3_81 = value;
	}

	inline static int32_t get_offset_of_U33F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U33F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82() const { return ___3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U33F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82() { return &___3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82; }
	inline void set_U33F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82 = value;
	}

	inline static int32_t get_offset_of_U3400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U3400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83() const { return ___400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U3400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83() { return &___400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83; }
	inline void set_U3400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83 = value;
	}

	inline static int32_t get_offset_of_U3428007959831954B0C2DCFAF9DD641D629B00DBF_84() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___428007959831954B0C2DCFAF9DD641D629B00DBF_84)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U3428007959831954B0C2DCFAF9DD641D629B00DBF_84() const { return ___428007959831954B0C2DCFAF9DD641D629B00DBF_84; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U3428007959831954B0C2DCFAF9DD641D629B00DBF_84() { return &___428007959831954B0C2DCFAF9DD641D629B00DBF_84; }
	inline void set_U3428007959831954B0C2DCFAF9DD641D629B00DBF_84(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___428007959831954B0C2DCFAF9DD641D629B00DBF_84 = value;
	}

	inline static int32_t get_offset_of_U342C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U342C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85() const { return ___42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U342C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85() { return &___42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85; }
	inline void set_U342C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85 = value;
	}

	inline static int32_t get_offset_of_U3433175D38B13FFE177FDD661A309F1B528B3F6E2_86() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___433175D38B13FFE177FDD661A309F1B528B3F6E2_86)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U3433175D38B13FFE177FDD661A309F1B528B3F6E2_86() const { return ___433175D38B13FFE177FDD661A309F1B528B3F6E2_86; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U3433175D38B13FFE177FDD661A309F1B528B3F6E2_86() { return &___433175D38B13FFE177FDD661A309F1B528B3F6E2_86; }
	inline void set_U3433175D38B13FFE177FDD661A309F1B528B3F6E2_86(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___433175D38B13FFE177FDD661A309F1B528B3F6E2_86 = value;
	}

	inline static int32_t get_offset_of_U34435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U34435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87() const { return ___4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U34435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87() { return &___4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87; }
	inline void set_U34435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87 = value;
	}

	inline static int32_t get_offset_of_U3460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88() const { return ___460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88() { return &___460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88; }
	inline void set_U3460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88 = value;
	}

	inline static int32_t get_offset_of_U3467C6758F235D3193618192A64129CBB602C9067_89() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___467C6758F235D3193618192A64129CBB602C9067_89)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U3467C6758F235D3193618192A64129CBB602C9067_89() const { return ___467C6758F235D3193618192A64129CBB602C9067_89; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U3467C6758F235D3193618192A64129CBB602C9067_89() { return &___467C6758F235D3193618192A64129CBB602C9067_89; }
	inline void set_U3467C6758F235D3193618192A64129CBB602C9067_89(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___467C6758F235D3193618192A64129CBB602C9067_89 = value;
	}

	inline static int32_t get_offset_of_U347A4F979AF156CDA62313B97411B4C6CE62B8B5A_90() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___47A4F979AF156CDA62313B97411B4C6CE62B8B5A_90)); }
	inline int32_t get_U347A4F979AF156CDA62313B97411B4C6CE62B8B5A_90() const { return ___47A4F979AF156CDA62313B97411B4C6CE62B8B5A_90; }
	inline int32_t* get_address_of_U347A4F979AF156CDA62313B97411B4C6CE62B8B5A_90() { return &___47A4F979AF156CDA62313B97411B4C6CE62B8B5A_90; }
	inline void set_U347A4F979AF156CDA62313B97411B4C6CE62B8B5A_90(int32_t value)
	{
		___47A4F979AF156CDA62313B97411B4C6CE62B8B5A_90 = value;
	}

	inline static int32_t get_offset_of_U3498B9317C14CF9004FE6CB88782D926B3E379350_91() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___498B9317C14CF9004FE6CB88782D926B3E379350_91)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3498B9317C14CF9004FE6CB88782D926B3E379350_91() const { return ___498B9317C14CF9004FE6CB88782D926B3E379350_91; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3498B9317C14CF9004FE6CB88782D926B3E379350_91() { return &___498B9317C14CF9004FE6CB88782D926B3E379350_91; }
	inline void set_U3498B9317C14CF9004FE6CB88782D926B3E379350_91(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___498B9317C14CF9004FE6CB88782D926B3E379350_91 = value;
	}

	inline static int32_t get_offset_of_U3499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92() const { return ___499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92() { return &___499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92; }
	inline void set_U3499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92 = value;
	}

	inline static int32_t get_offset_of_U34A080107805D8F7103F56DD3B0D8FD5D55735937_93() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4A080107805D8F7103F56DD3B0D8FD5D55735937_93)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U34A080107805D8F7103F56DD3B0D8FD5D55735937_93() const { return ___4A080107805D8F7103F56DD3B0D8FD5D55735937_93; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U34A080107805D8F7103F56DD3B0D8FD5D55735937_93() { return &___4A080107805D8F7103F56DD3B0D8FD5D55735937_93; }
	inline void set_U34A080107805D8F7103F56DD3B0D8FD5D55735937_93(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___4A080107805D8F7103F56DD3B0D8FD5D55735937_93 = value;
	}

	inline static int32_t get_offset_of_U34B411385A36907D25D8088AE39AB6AAFA46B0642_94() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4B411385A36907D25D8088AE39AB6AAFA46B0642_94)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U34B411385A36907D25D8088AE39AB6AAFA46B0642_94() const { return ___4B411385A36907D25D8088AE39AB6AAFA46B0642_94; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U34B411385A36907D25D8088AE39AB6AAFA46B0642_94() { return &___4B411385A36907D25D8088AE39AB6AAFA46B0642_94; }
	inline void set_U34B411385A36907D25D8088AE39AB6AAFA46B0642_94(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___4B411385A36907D25D8088AE39AB6AAFA46B0642_94 = value;
	}

	inline static int32_t get_offset_of_U34C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U34C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95() const { return ___4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U34C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95() { return &___4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95; }
	inline void set_U34C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95 = value;
	}

	inline static int32_t get_offset_of_U34C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U34C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96() const { return ___4C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U34C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96() { return &___4C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96; }
	inline void set_U34C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___4C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96 = value;
	}

	inline static int32_t get_offset_of_U34E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U34E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97() const { return ___4E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U34E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97() { return &___4E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97; }
	inline void set_U34E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___4E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97 = value;
	}

	inline static int32_t get_offset_of_U34FFC8339E09825A68B861995F9C660EB11DBF13D_98() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___4FFC8339E09825A68B861995F9C660EB11DBF13D_98)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U34FFC8339E09825A68B861995F9C660EB11DBF13D_98() const { return ___4FFC8339E09825A68B861995F9C660EB11DBF13D_98; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U34FFC8339E09825A68B861995F9C660EB11DBF13D_98() { return &___4FFC8339E09825A68B861995F9C660EB11DBF13D_98; }
	inline void set_U34FFC8339E09825A68B861995F9C660EB11DBF13D_98(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___4FFC8339E09825A68B861995F9C660EB11DBF13D_98 = value;
	}

	inline static int32_t get_offset_of_U350AA269217736906D8469B9191F420DC6B13A36A_99() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___50AA269217736906D8469B9191F420DC6B13A36A_99)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U350AA269217736906D8469B9191F420DC6B13A36A_99() const { return ___50AA269217736906D8469B9191F420DC6B13A36A_99; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U350AA269217736906D8469B9191F420DC6B13A36A_99() { return &___50AA269217736906D8469B9191F420DC6B13A36A_99; }
	inline void set_U350AA269217736906D8469B9191F420DC6B13A36A_99(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___50AA269217736906D8469B9191F420DC6B13A36A_99 = value;
	}

	inline static int32_t get_offset_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_100() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___50B1635D1FB2907A171B71751E1A3FA79423CA17_100)); }
	inline __StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885  get_U350B1635D1FB2907A171B71751E1A3FA79423CA17_100() const { return ___50B1635D1FB2907A171B71751E1A3FA79423CA17_100; }
	inline __StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885 * get_address_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_100() { return &___50B1635D1FB2907A171B71751E1A3FA79423CA17_100; }
	inline void set_U350B1635D1FB2907A171B71751E1A3FA79423CA17_100(__StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885  value)
	{
		___50B1635D1FB2907A171B71751E1A3FA79423CA17_100 = value;
	}

	inline static int32_t get_offset_of_U3512F92F4041B190727A330E2A6CC39E5D9EA06E6_101() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___512F92F4041B190727A330E2A6CC39E5D9EA06E6_101)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3512F92F4041B190727A330E2A6CC39E5D9EA06E6_101() const { return ___512F92F4041B190727A330E2A6CC39E5D9EA06E6_101; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3512F92F4041B190727A330E2A6CC39E5D9EA06E6_101() { return &___512F92F4041B190727A330E2A6CC39E5D9EA06E6_101; }
	inline void set_U3512F92F4041B190727A330E2A6CC39E5D9EA06E6_101(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___512F92F4041B190727A330E2A6CC39E5D9EA06E6_101 = value;
	}

	inline static int32_t get_offset_of_U35318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U35318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102() const { return ___5318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U35318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102() { return &___5318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102; }
	inline void set_U35318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___5318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102 = value;
	}

	inline static int32_t get_offset_of_U3547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U3547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103() const { return ___547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U3547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103() { return &___547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103; }
	inline void set_U3547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103 = value;
	}

	inline static int32_t get_offset_of_U3555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U3555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104() const { return ___555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U3555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104() { return &___555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104; }
	inline void set_U3555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104 = value;
	}

	inline static int32_t get_offset_of_U35581A70566F03554D8048EDBFC6E6B399AF9BCB1_105() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5581A70566F03554D8048EDBFC6E6B399AF9BCB1_105)); }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  get_U35581A70566F03554D8048EDBFC6E6B399AF9BCB1_105() const { return ___5581A70566F03554D8048EDBFC6E6B399AF9BCB1_105; }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27 * get_address_of_U35581A70566F03554D8048EDBFC6E6B399AF9BCB1_105() { return &___5581A70566F03554D8048EDBFC6E6B399AF9BCB1_105; }
	inline void set_U35581A70566F03554D8048EDBFC6E6B399AF9BCB1_105(__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  value)
	{
		___5581A70566F03554D8048EDBFC6E6B399AF9BCB1_105 = value;
	}

	inline static int32_t get_offset_of_U358F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___58F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U358F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106() const { return ___58F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U358F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106() { return &___58F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106; }
	inline void set_U358F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___58F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106 = value;
	}

	inline static int32_t get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_107() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___59F5BD34B6C013DEACC784F69C67E95150033A84_107)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U359F5BD34B6C013DEACC784F69C67E95150033A84_107() const { return ___59F5BD34B6C013DEACC784F69C67E95150033A84_107; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_107() { return &___59F5BD34B6C013DEACC784F69C67E95150033A84_107; }
	inline void set_U359F5BD34B6C013DEACC784F69C67E95150033A84_107(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___59F5BD34B6C013DEACC784F69C67E95150033A84_107 = value;
	}

	inline static int32_t get_offset_of_U35AB421AC76CECB8E84025172585CB97DE8BECD65_108() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5AB421AC76CECB8E84025172585CB97DE8BECD65_108)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U35AB421AC76CECB8E84025172585CB97DE8BECD65_108() const { return ___5AB421AC76CECB8E84025172585CB97DE8BECD65_108; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U35AB421AC76CECB8E84025172585CB97DE8BECD65_108() { return &___5AB421AC76CECB8E84025172585CB97DE8BECD65_108; }
	inline void set_U35AB421AC76CECB8E84025172585CB97DE8BECD65_108(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___5AB421AC76CECB8E84025172585CB97DE8BECD65_108 = value;
	}

	inline static int32_t get_offset_of_U35AE32F75295B7AC2FB76118848B92F3987B949A0_109() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5AE32F75295B7AC2FB76118848B92F3987B949A0_109)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U35AE32F75295B7AC2FB76118848B92F3987B949A0_109() const { return ___5AE32F75295B7AC2FB76118848B92F3987B949A0_109; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U35AE32F75295B7AC2FB76118848B92F3987B949A0_109() { return &___5AE32F75295B7AC2FB76118848B92F3987B949A0_109; }
	inline void set_U35AE32F75295B7AC2FB76118848B92F3987B949A0_109(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___5AE32F75295B7AC2FB76118848B92F3987B949A0_109 = value;
	}

	inline static int32_t get_offset_of_U35BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U35BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110() const { return ___5BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U35BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110() { return &___5BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110; }
	inline void set_U35BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___5BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110 = value;
	}

	inline static int32_t get_offset_of_U35CF7299F6558A8AC3F821B4F2F65F23798D319D3_111() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5CF7299F6558A8AC3F821B4F2F65F23798D319D3_111)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U35CF7299F6558A8AC3F821B4F2F65F23798D319D3_111() const { return ___5CF7299F6558A8AC3F821B4F2F65F23798D319D3_111; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U35CF7299F6558A8AC3F821B4F2F65F23798D319D3_111() { return &___5CF7299F6558A8AC3F821B4F2F65F23798D319D3_111; }
	inline void set_U35CF7299F6558A8AC3F821B4F2F65F23798D319D3_111(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___5CF7299F6558A8AC3F821B4F2F65F23798D319D3_111 = value;
	}

	inline static int32_t get_offset_of_U35CFF612F2058DE2C287ECA5C6407ECC471F65576_112() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5CFF612F2058DE2C287ECA5C6407ECC471F65576_112)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U35CFF612F2058DE2C287ECA5C6407ECC471F65576_112() const { return ___5CFF612F2058DE2C287ECA5C6407ECC471F65576_112; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U35CFF612F2058DE2C287ECA5C6407ECC471F65576_112() { return &___5CFF612F2058DE2C287ECA5C6407ECC471F65576_112; }
	inline void set_U35CFF612F2058DE2C287ECA5C6407ECC471F65576_112(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___5CFF612F2058DE2C287ECA5C6407ECC471F65576_112 = value;
	}

	inline static int32_t get_offset_of_U35D41C56232C500092E99AC044D3C5C442B1C834F_113() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5D41C56232C500092E99AC044D3C5C442B1C834F_113)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U35D41C56232C500092E99AC044D3C5C442B1C834F_113() const { return ___5D41C56232C500092E99AC044D3C5C442B1C834F_113; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U35D41C56232C500092E99AC044D3C5C442B1C834F_113() { return &___5D41C56232C500092E99AC044D3C5C442B1C834F_113; }
	inline void set_U35D41C56232C500092E99AC044D3C5C442B1C834F_113(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___5D41C56232C500092E99AC044D3C5C442B1C834F_113 = value;
	}

	inline static int32_t get_offset_of_U35DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114)); }
	inline int64_t get_U35DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114() const { return ___5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114; }
	inline int64_t* get_address_of_U35DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114() { return &___5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114; }
	inline void set_U35DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114(int64_t value)
	{
		___5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114 = value;
	}

	inline static int32_t get_offset_of_U35E7F55149EC07597C76E6E3CD9F62274214061E6_115() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5E7F55149EC07597C76E6E3CD9F62274214061E6_115)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U35E7F55149EC07597C76E6E3CD9F62274214061E6_115() const { return ___5E7F55149EC07597C76E6E3CD9F62274214061E6_115; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U35E7F55149EC07597C76E6E3CD9F62274214061E6_115() { return &___5E7F55149EC07597C76E6E3CD9F62274214061E6_115; }
	inline void set_U35E7F55149EC07597C76E6E3CD9F62274214061E6_115(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___5E7F55149EC07597C76E6E3CD9F62274214061E6_115 = value;
	}

	inline static int32_t get_offset_of_U35EF7F909EFC731E811E21521A43A80FB5AC0B229_116() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___5EF7F909EFC731E811E21521A43A80FB5AC0B229_116)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U35EF7F909EFC731E811E21521A43A80FB5AC0B229_116() const { return ___5EF7F909EFC731E811E21521A43A80FB5AC0B229_116; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U35EF7F909EFC731E811E21521A43A80FB5AC0B229_116() { return &___5EF7F909EFC731E811E21521A43A80FB5AC0B229_116; }
	inline void set_U35EF7F909EFC731E811E21521A43A80FB5AC0B229_116(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___5EF7F909EFC731E811E21521A43A80FB5AC0B229_116 = value;
	}

	inline static int32_t get_offset_of_U360A08108A32C9D3F263B2F42095A2694B7C1C1EF_117() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___60A08108A32C9D3F263B2F42095A2694B7C1C1EF_117)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U360A08108A32C9D3F263B2F42095A2694B7C1C1EF_117() const { return ___60A08108A32C9D3F263B2F42095A2694B7C1C1EF_117; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U360A08108A32C9D3F263B2F42095A2694B7C1C1EF_117() { return &___60A08108A32C9D3F263B2F42095A2694B7C1C1EF_117; }
	inline void set_U360A08108A32C9D3F263B2F42095A2694B7C1C1EF_117(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___60A08108A32C9D3F263B2F42095A2694B7C1C1EF_117 = value;
	}

	inline static int32_t get_offset_of_U3613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118() const { return ___613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118() { return &___613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118; }
	inline void set_U3613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118 = value;
	}

	inline static int32_t get_offset_of_U36277CE8FE3A9156D3455749B453AC88191D3C6D6_119() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6277CE8FE3A9156D3455749B453AC88191D3C6D6_119)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U36277CE8FE3A9156D3455749B453AC88191D3C6D6_119() const { return ___6277CE8FE3A9156D3455749B453AC88191D3C6D6_119; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U36277CE8FE3A9156D3455749B453AC88191D3C6D6_119() { return &___6277CE8FE3A9156D3455749B453AC88191D3C6D6_119; }
	inline void set_U36277CE8FE3A9156D3455749B453AC88191D3C6D6_119(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___6277CE8FE3A9156D3455749B453AC88191D3C6D6_119 = value;
	}

	inline static int32_t get_offset_of_U362BAB0F245E66C3EB982CF5A7015F0A7C3382283_120() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___62BAB0F245E66C3EB982CF5A7015F0A7C3382283_120)); }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  get_U362BAB0F245E66C3EB982CF5A7015F0A7C3382283_120() const { return ___62BAB0F245E66C3EB982CF5A7015F0A7C3382283_120; }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A * get_address_of_U362BAB0F245E66C3EB982CF5A7015F0A7C3382283_120() { return &___62BAB0F245E66C3EB982CF5A7015F0A7C3382283_120; }
	inline void set_U362BAB0F245E66C3EB982CF5A7015F0A7C3382283_120(__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  value)
	{
		___62BAB0F245E66C3EB982CF5A7015F0A7C3382283_120 = value;
	}

	inline static int32_t get_offset_of_U3641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121() const { return ___641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121() { return &___641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121; }
	inline void set_U3641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121 = value;
	}

	inline static int32_t get_offset_of_U364354464C9074B5BB4369689AAA131961CD1EF19_122() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___64354464C9074B5BB4369689AAA131961CD1EF19_122)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U364354464C9074B5BB4369689AAA131961CD1EF19_122() const { return ___64354464C9074B5BB4369689AAA131961CD1EF19_122; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U364354464C9074B5BB4369689AAA131961CD1EF19_122() { return &___64354464C9074B5BB4369689AAA131961CD1EF19_122; }
	inline void set_U364354464C9074B5BB4369689AAA131961CD1EF19_122(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___64354464C9074B5BB4369689AAA131961CD1EF19_122 = value;
	}

	inline static int32_t get_offset_of_U3643A9D76937E94519B73BE072D65E79BAFF3C213_123() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___643A9D76937E94519B73BE072D65E79BAFF3C213_123)); }
	inline __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  get_U3643A9D76937E94519B73BE072D65E79BAFF3C213_123() const { return ___643A9D76937E94519B73BE072D65E79BAFF3C213_123; }
	inline __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C * get_address_of_U3643A9D76937E94519B73BE072D65E79BAFF3C213_123() { return &___643A9D76937E94519B73BE072D65E79BAFF3C213_123; }
	inline void set_U3643A9D76937E94519B73BE072D65E79BAFF3C213_123(__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  value)
	{
		___643A9D76937E94519B73BE072D65E79BAFF3C213_123 = value;
	}

	inline static int32_t get_offset_of_U3643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U3643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124() const { return ___643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U3643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124() { return &___643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124; }
	inline void set_U3643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124 = value;
	}

	inline static int32_t get_offset_of_U366961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125)); }
	inline __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  get_U366961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125() const { return ___66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125; }
	inline __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981 * get_address_of_U366961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125() { return &___66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125; }
	inline void set_U366961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125(__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  value)
	{
		___66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125 = value;
	}

	inline static int32_t get_offset_of_U3672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126() const { return ___672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126() { return &___672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126; }
	inline void set_U3672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126 = value;
	}

	inline static int32_t get_offset_of_U367C0E784F3654B008A81E2988588CF4956CCF3DA_127() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___67C0E784F3654B008A81E2988588CF4956CCF3DA_127)); }
	inline __StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27  get_U367C0E784F3654B008A81E2988588CF4956CCF3DA_127() const { return ___67C0E784F3654B008A81E2988588CF4956CCF3DA_127; }
	inline __StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27 * get_address_of_U367C0E784F3654B008A81E2988588CF4956CCF3DA_127() { return &___67C0E784F3654B008A81E2988588CF4956CCF3DA_127; }
	inline void set_U367C0E784F3654B008A81E2988588CF4956CCF3DA_127(__StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27  value)
	{
		___67C0E784F3654B008A81E2988588CF4956CCF3DA_127 = value;
	}

	inline static int32_t get_offset_of_U368178023585F1F782745740AA583CDC778DB31B3_128() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___68178023585F1F782745740AA583CDC778DB31B3_128)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U368178023585F1F782745740AA583CDC778DB31B3_128() const { return ___68178023585F1F782745740AA583CDC778DB31B3_128; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U368178023585F1F782745740AA583CDC778DB31B3_128() { return &___68178023585F1F782745740AA583CDC778DB31B3_128; }
	inline void set_U368178023585F1F782745740AA583CDC778DB31B3_128(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___68178023585F1F782745740AA583CDC778DB31B3_128 = value;
	}

	inline static int32_t get_offset_of_U36A316789EED01119DE92841832701A40AB0CABD6_129() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6A316789EED01119DE92841832701A40AB0CABD6_129)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U36A316789EED01119DE92841832701A40AB0CABD6_129() const { return ___6A316789EED01119DE92841832701A40AB0CABD6_129; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U36A316789EED01119DE92841832701A40AB0CABD6_129() { return &___6A316789EED01119DE92841832701A40AB0CABD6_129; }
	inline void set_U36A316789EED01119DE92841832701A40AB0CABD6_129(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___6A316789EED01119DE92841832701A40AB0CABD6_129 = value;
	}

	inline static int32_t get_offset_of_U36AAC0DB543C50F09E879F5B9F757319773564CE1_130() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6AAC0DB543C50F09E879F5B9F757319773564CE1_130)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U36AAC0DB543C50F09E879F5B9F757319773564CE1_130() const { return ___6AAC0DB543C50F09E879F5B9F757319773564CE1_130; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U36AAC0DB543C50F09E879F5B9F757319773564CE1_130() { return &___6AAC0DB543C50F09E879F5B9F757319773564CE1_130; }
	inline void set_U36AAC0DB543C50F09E879F5B9F757319773564CE1_130(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___6AAC0DB543C50F09E879F5B9F757319773564CE1_130 = value;
	}

	inline static int32_t get_offset_of_U36AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U36AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131() const { return ___6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U36AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131() { return &___6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131; }
	inline void set_U36AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131 = value;
	}

	inline static int32_t get_offset_of_U36BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132)); }
	inline __StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64  get_U36BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132() const { return ___6BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132; }
	inline __StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64 * get_address_of_U36BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132() { return &___6BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132; }
	inline void set_U36BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132(__StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64  value)
	{
		___6BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132 = value;
	}

	inline static int32_t get_offset_of_U36BBD3A22A185224EE0EBAB0784455E9E245376B7_133() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6BBD3A22A185224EE0EBAB0784455E9E245376B7_133)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U36BBD3A22A185224EE0EBAB0784455E9E245376B7_133() const { return ___6BBD3A22A185224EE0EBAB0784455E9E245376B7_133; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U36BBD3A22A185224EE0EBAB0784455E9E245376B7_133() { return &___6BBD3A22A185224EE0EBAB0784455E9E245376B7_133; }
	inline void set_U36BBD3A22A185224EE0EBAB0784455E9E245376B7_133(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___6BBD3A22A185224EE0EBAB0784455E9E245376B7_133 = value;
	}

	inline static int32_t get_offset_of_U36C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134)); }
	inline __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  get_U36C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134() const { return ___6C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134; }
	inline __StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C * get_address_of_U36C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134() { return &___6C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134; }
	inline void set_U36C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134(__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C  value)
	{
		___6C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134 = value;
	}

	inline static int32_t get_offset_of_U36D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135)); }
	inline int32_t get_U36D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135() const { return ___6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135; }
	inline int32_t* get_address_of_U36D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135() { return &___6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135; }
	inline void set_U36D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135(int32_t value)
	{
		___6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135 = value;
	}

	inline static int32_t get_offset_of_U36D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U36D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136() const { return ___6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U36D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136() { return &___6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136; }
	inline void set_U36D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136 = value;
	}

	inline static int32_t get_offset_of_U36DEB7F74818574642B0B824B9C08B366C962A360_137() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6DEB7F74818574642B0B824B9C08B366C962A360_137)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U36DEB7F74818574642B0B824B9C08B366C962A360_137() const { return ___6DEB7F74818574642B0B824B9C08B366C962A360_137; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U36DEB7F74818574642B0B824B9C08B366C962A360_137() { return &___6DEB7F74818574642B0B824B9C08B366C962A360_137; }
	inline void set_U36DEB7F74818574642B0B824B9C08B366C962A360_137(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___6DEB7F74818574642B0B824B9C08B366C962A360_137 = value;
	}

	inline static int32_t get_offset_of_U36DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138)); }
	inline int64_t get_U36DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138() const { return ___6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138; }
	inline int64_t* get_address_of_U36DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138() { return &___6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138; }
	inline void set_U36DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138(int64_t value)
	{
		___6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138 = value;
	}

	inline static int32_t get_offset_of_U36E92E42774698E4C089FE0FE4F7539D0D5F845F5_139() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6E92E42774698E4C089FE0FE4F7539D0D5F845F5_139)); }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  get_U36E92E42774698E4C089FE0FE4F7539D0D5F845F5_139() const { return ___6E92E42774698E4C089FE0FE4F7539D0D5F845F5_139; }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 * get_address_of_U36E92E42774698E4C089FE0FE4F7539D0D5F845F5_139() { return &___6E92E42774698E4C089FE0FE4F7539D0D5F845F5_139; }
	inline void set_U36E92E42774698E4C089FE0FE4F7539D0D5F845F5_139(__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  value)
	{
		___6E92E42774698E4C089FE0FE4F7539D0D5F845F5_139 = value;
	}

	inline static int32_t get_offset_of_U36F39BC29A161CAE5394821B1FDE160EB5229AE71_140() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6F39BC29A161CAE5394821B1FDE160EB5229AE71_140)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U36F39BC29A161CAE5394821B1FDE160EB5229AE71_140() const { return ___6F39BC29A161CAE5394821B1FDE160EB5229AE71_140; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U36F39BC29A161CAE5394821B1FDE160EB5229AE71_140() { return &___6F39BC29A161CAE5394821B1FDE160EB5229AE71_140; }
	inline void set_U36F39BC29A161CAE5394821B1FDE160EB5229AE71_140(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___6F39BC29A161CAE5394821B1FDE160EB5229AE71_140 = value;
	}

	inline static int32_t get_offset_of_U36F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U36F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141() const { return ___6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U36F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141() { return &___6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141; }
	inline void set_U36F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141 = value;
	}

	inline static int32_t get_offset_of_U36F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U36F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142() const { return ___6F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U36F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142() { return &___6F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142; }
	inline void set_U36F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___6F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142 = value;
	}

	inline static int32_t get_offset_of_U36FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___6FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143)); }
	inline __StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D  get_U36FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143() const { return ___6FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143; }
	inline __StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D * get_address_of_U36FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143() { return &___6FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143; }
	inline void set_U36FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143(__StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D  value)
	{
		___6FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143 = value;
	}

	inline static int32_t get_offset_of_U37037807198C22A7D2B0807371D763779A84FDFCF_144() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7037807198C22A7D2B0807371D763779A84FDFCF_144)); }
	inline __StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432  get_U37037807198C22A7D2B0807371D763779A84FDFCF_144() const { return ___7037807198C22A7D2B0807371D763779A84FDFCF_144; }
	inline __StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432 * get_address_of_U37037807198C22A7D2B0807371D763779A84FDFCF_144() { return &___7037807198C22A7D2B0807371D763779A84FDFCF_144; }
	inline void set_U37037807198C22A7D2B0807371D763779A84FDFCF_144(__StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432  value)
	{
		___7037807198C22A7D2B0807371D763779A84FDFCF_144 = value;
	}

	inline static int32_t get_offset_of_U3714A94F3805E05CA6C00F9A46489427ABEB25D60_145() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___714A94F3805E05CA6C00F9A46489427ABEB25D60_145)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U3714A94F3805E05CA6C00F9A46489427ABEB25D60_145() const { return ___714A94F3805E05CA6C00F9A46489427ABEB25D60_145; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U3714A94F3805E05CA6C00F9A46489427ABEB25D60_145() { return &___714A94F3805E05CA6C00F9A46489427ABEB25D60_145; }
	inline void set_U3714A94F3805E05CA6C00F9A46489427ABEB25D60_145(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___714A94F3805E05CA6C00F9A46489427ABEB25D60_145 = value;
	}

	inline static int32_t get_offset_of_U371F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U371F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146() const { return ___71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U371F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146() { return &___71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146; }
	inline void set_U371F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146 = value;
	}

	inline static int32_t get_offset_of_U37878E9E7126B2BDF365429C31842AE1903CD2DFF_147() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7878E9E7126B2BDF365429C31842AE1903CD2DFF_147)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U37878E9E7126B2BDF365429C31842AE1903CD2DFF_147() const { return ___7878E9E7126B2BDF365429C31842AE1903CD2DFF_147; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U37878E9E7126B2BDF365429C31842AE1903CD2DFF_147() { return &___7878E9E7126B2BDF365429C31842AE1903CD2DFF_147; }
	inline void set_U37878E9E7126B2BDF365429C31842AE1903CD2DFF_147(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___7878E9E7126B2BDF365429C31842AE1903CD2DFF_147 = value;
	}

	inline static int32_t get_offset_of_U379039C5C22F863E8EAE0E08E1318F343680E50A8_148() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___79039C5C22F863E8EAE0E08E1318F343680E50A8_148)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U379039C5C22F863E8EAE0E08E1318F343680E50A8_148() const { return ___79039C5C22F863E8EAE0E08E1318F343680E50A8_148; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U379039C5C22F863E8EAE0E08E1318F343680E50A8_148() { return &___79039C5C22F863E8EAE0E08E1318F343680E50A8_148; }
	inline void set_U379039C5C22F863E8EAE0E08E1318F343680E50A8_148(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___79039C5C22F863E8EAE0E08E1318F343680E50A8_148 = value;
	}

	inline static int32_t get_offset_of_U379A213B796D2AD7A89C2071B0732B78207F5CE01_149() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___79A213B796D2AD7A89C2071B0732B78207F5CE01_149)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U379A213B796D2AD7A89C2071B0732B78207F5CE01_149() const { return ___79A213B796D2AD7A89C2071B0732B78207F5CE01_149; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U379A213B796D2AD7A89C2071B0732B78207F5CE01_149() { return &___79A213B796D2AD7A89C2071B0732B78207F5CE01_149; }
	inline void set_U379A213B796D2AD7A89C2071B0732B78207F5CE01_149(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___79A213B796D2AD7A89C2071B0732B78207F5CE01_149 = value;
	}

	inline static int32_t get_offset_of_U379D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___79D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150)); }
	inline __StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923  get_U379D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150() const { return ___79D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150; }
	inline __StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923 * get_address_of_U379D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150() { return &___79D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150; }
	inline void set_U379D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150(__StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923  value)
	{
		___79D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150 = value;
	}

	inline static int32_t get_offset_of_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_151() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___79D521E6E3E55103005E9CC3FA43B3174FAF090F_151)); }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  get_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_151() const { return ___79D521E6E3E55103005E9CC3FA43B3174FAF090F_151; }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27 * get_address_of_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_151() { return &___79D521E6E3E55103005E9CC3FA43B3174FAF090F_151; }
	inline void set_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_151(__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  value)
	{
		___79D521E6E3E55103005E9CC3FA43B3174FAF090F_151 = value;
	}

	inline static int32_t get_offset_of_U37B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U37B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152() const { return ___7B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U37B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152() { return &___7B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152; }
	inline void set_U37B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___7B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152 = value;
	}

	inline static int32_t get_offset_of_U37B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U37B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153() const { return ___7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U37B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153() { return &___7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153; }
	inline void set_U37B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153 = value;
	}

	inline static int32_t get_offset_of_U37C57CFE9FF25243824AF38485BBF41F9E57ECF21_154() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7C57CFE9FF25243824AF38485BBF41F9E57ECF21_154)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U37C57CFE9FF25243824AF38485BBF41F9E57ECF21_154() const { return ___7C57CFE9FF25243824AF38485BBF41F9E57ECF21_154; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U37C57CFE9FF25243824AF38485BBF41F9E57ECF21_154() { return &___7C57CFE9FF25243824AF38485BBF41F9E57ECF21_154; }
	inline void set_U37C57CFE9FF25243824AF38485BBF41F9E57ECF21_154(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___7C57CFE9FF25243824AF38485BBF41F9E57ECF21_154 = value;
	}

	inline static int32_t get_offset_of_U37CFF7A50C8F8981091791CDB210243E8F465BC80_155() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7CFF7A50C8F8981091791CDB210243E8F465BC80_155)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_U37CFF7A50C8F8981091791CDB210243E8F465BC80_155() const { return ___7CFF7A50C8F8981091791CDB210243E8F465BC80_155; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_U37CFF7A50C8F8981091791CDB210243E8F465BC80_155() { return &___7CFF7A50C8F8981091791CDB210243E8F465BC80_155; }
	inline void set_U37CFF7A50C8F8981091791CDB210243E8F465BC80_155(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___7CFF7A50C8F8981091791CDB210243E8F465BC80_155 = value;
	}

	inline static int32_t get_offset_of_U37E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U37E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156() const { return ___7E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U37E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156() { return &___7E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156; }
	inline void set_U37E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___7E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156 = value;
	}

	inline static int32_t get_offset_of_U37F817443C1F736721DD92BD67C7C8994C7B8DEBF_157() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7F817443C1F736721DD92BD67C7C8994C7B8DEBF_157)); }
	inline __StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926  get_U37F817443C1F736721DD92BD67C7C8994C7B8DEBF_157() const { return ___7F817443C1F736721DD92BD67C7C8994C7B8DEBF_157; }
	inline __StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926 * get_address_of_U37F817443C1F736721DD92BD67C7C8994C7B8DEBF_157() { return &___7F817443C1F736721DD92BD67C7C8994C7B8DEBF_157; }
	inline void set_U37F817443C1F736721DD92BD67C7C8994C7B8DEBF_157(__StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926  value)
	{
		___7F817443C1F736721DD92BD67C7C8994C7B8DEBF_157 = value;
	}

	inline static int32_t get_offset_of_U37FF0A15672FF2807983AB77C0DA74928986427C0_158() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___7FF0A15672FF2807983AB77C0DA74928986427C0_158)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U37FF0A15672FF2807983AB77C0DA74928986427C0_158() const { return ___7FF0A15672FF2807983AB77C0DA74928986427C0_158; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U37FF0A15672FF2807983AB77C0DA74928986427C0_158() { return &___7FF0A15672FF2807983AB77C0DA74928986427C0_158; }
	inline void set_U37FF0A15672FF2807983AB77C0DA74928986427C0_158(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___7FF0A15672FF2807983AB77C0DA74928986427C0_158 = value;
	}

	inline static int32_t get_offset_of_U381FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___81FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U381FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159() const { return ___81FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U381FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159() { return &___81FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159; }
	inline void set_U381FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___81FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159 = value;
	}

	inline static int32_t get_offset_of_U3821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U3821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160() const { return ___821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U3821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160() { return &___821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160; }
	inline void set_U3821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160 = value;
	}

	inline static int32_t get_offset_of_U38330271815E046D369E0B1F7673D308739FDCC07_161() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8330271815E046D369E0B1F7673D308739FDCC07_161)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U38330271815E046D369E0B1F7673D308739FDCC07_161() const { return ___8330271815E046D369E0B1F7673D308739FDCC07_161; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U38330271815E046D369E0B1F7673D308739FDCC07_161() { return &___8330271815E046D369E0B1F7673D308739FDCC07_161; }
	inline void set_U38330271815E046D369E0B1F7673D308739FDCC07_161(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___8330271815E046D369E0B1F7673D308739FDCC07_161 = value;
	}

	inline static int32_t get_offset_of_U3840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U3840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162() const { return ___840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U3840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162() { return &___840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162; }
	inline void set_U3840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162 = value;
	}

	inline static int32_t get_offset_of_U38457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163)); }
	inline __StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE  get_U38457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163() const { return ___8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163; }
	inline __StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE * get_address_of_U38457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163() { return &___8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163; }
	inline void set_U38457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163(__StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE  value)
	{
		___8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163 = value;
	}

	inline static int32_t get_offset_of_U384F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U384F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164() const { return ___84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U384F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164() { return &___84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164; }
	inline void set_U384F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164 = value;
	}

	inline static int32_t get_offset_of_U3850D4DC092689E1F0D8A70B6281848B27DEC0014_165() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___850D4DC092689E1F0D8A70B6281848B27DEC0014_165)); }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  get_U3850D4DC092689E1F0D8A70B6281848B27DEC0014_165() const { return ___850D4DC092689E1F0D8A70B6281848B27DEC0014_165; }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27 * get_address_of_U3850D4DC092689E1F0D8A70B6281848B27DEC0014_165() { return &___850D4DC092689E1F0D8A70B6281848B27DEC0014_165; }
	inline void set_U3850D4DC092689E1F0D8A70B6281848B27DEC0014_165(__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  value)
	{
		___850D4DC092689E1F0D8A70B6281848B27DEC0014_165 = value;
	}

	inline static int32_t get_offset_of_U386B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_U386B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166() const { return ___86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_U386B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166() { return &___86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166; }
	inline void set_U386B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166 = value;
	}

	inline static int32_t get_offset_of_U387BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___87BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U387BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167() const { return ___87BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U387BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167() { return &___87BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167; }
	inline void set_U387BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___87BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167 = value;
	}

	inline static int32_t get_offset_of_U3882888781BC0DC17021FB4F11BA783038C83B313_168() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___882888781BC0DC17021FB4F11BA783038C83B313_168)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U3882888781BC0DC17021FB4F11BA783038C83B313_168() const { return ___882888781BC0DC17021FB4F11BA783038C83B313_168; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U3882888781BC0DC17021FB4F11BA783038C83B313_168() { return &___882888781BC0DC17021FB4F11BA783038C83B313_168; }
	inline void set_U3882888781BC0DC17021FB4F11BA783038C83B313_168(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___882888781BC0DC17021FB4F11BA783038C83B313_168 = value;
	}

	inline static int32_t get_offset_of_U38AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U38AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169() const { return ___8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U38AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169() { return &___8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169; }
	inline void set_U38AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169 = value;
	}

	inline static int32_t get_offset_of_U38C7FEE53346CDB1B119FCAD8D605F476400A03CE_170() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8C7FEE53346CDB1B119FCAD8D605F476400A03CE_170)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U38C7FEE53346CDB1B119FCAD8D605F476400A03CE_170() const { return ___8C7FEE53346CDB1B119FCAD8D605F476400A03CE_170; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U38C7FEE53346CDB1B119FCAD8D605F476400A03CE_170() { return &___8C7FEE53346CDB1B119FCAD8D605F476400A03CE_170; }
	inline void set_U38C7FEE53346CDB1B119FCAD8D605F476400A03CE_170(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___8C7FEE53346CDB1B119FCAD8D605F476400A03CE_170 = value;
	}

	inline static int32_t get_offset_of_U38C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171)); }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  get_U38C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171() const { return ___8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171; }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 * get_address_of_U38C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171() { return &___8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171; }
	inline void set_U38C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171(__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  value)
	{
		___8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171 = value;
	}

	inline static int32_t get_offset_of_U38D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172)); }
	inline int32_t get_U38D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172() const { return ___8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172; }
	inline int32_t* get_address_of_U38D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172() { return &___8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172; }
	inline void set_U38D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172(int32_t value)
	{
		___8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172 = value;
	}

	inline static int32_t get_offset_of_U38ED8F61DAA454B49CD5059AE4486C59174324E9E_173() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8ED8F61DAA454B49CD5059AE4486C59174324E9E_173)); }
	inline __StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821  get_U38ED8F61DAA454B49CD5059AE4486C59174324E9E_173() const { return ___8ED8F61DAA454B49CD5059AE4486C59174324E9E_173; }
	inline __StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821 * get_address_of_U38ED8F61DAA454B49CD5059AE4486C59174324E9E_173() { return &___8ED8F61DAA454B49CD5059AE4486C59174324E9E_173; }
	inline void set_U38ED8F61DAA454B49CD5059AE4486C59174324E9E_173(__StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821  value)
	{
		___8ED8F61DAA454B49CD5059AE4486C59174324E9E_173 = value;
	}

	inline static int32_t get_offset_of_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174() const { return ___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174() { return &___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174; }
	inline void set_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174 = value;
	}

	inline static int32_t get_offset_of_U390A0542282A011472F94E97CEAE59F8B3B1A3291_175() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___90A0542282A011472F94E97CEAE59F8B3B1A3291_175)); }
	inline __StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6  get_U390A0542282A011472F94E97CEAE59F8B3B1A3291_175() const { return ___90A0542282A011472F94E97CEAE59F8B3B1A3291_175; }
	inline __StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6 * get_address_of_U390A0542282A011472F94E97CEAE59F8B3B1A3291_175() { return &___90A0542282A011472F94E97CEAE59F8B3B1A3291_175; }
	inline void set_U390A0542282A011472F94E97CEAE59F8B3B1A3291_175(__StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6  value)
	{
		___90A0542282A011472F94E97CEAE59F8B3B1A3291_175 = value;
	}

	inline static int32_t get_offset_of_U3929747B3E91297D56A461EE1B9E054EA31C70EC1_176() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___929747B3E91297D56A461EE1B9E054EA31C70EC1_176)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_U3929747B3E91297D56A461EE1B9E054EA31C70EC1_176() const { return ___929747B3E91297D56A461EE1B9E054EA31C70EC1_176; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_U3929747B3E91297D56A461EE1B9E054EA31C70EC1_176() { return &___929747B3E91297D56A461EE1B9E054EA31C70EC1_176; }
	inline void set_U3929747B3E91297D56A461EE1B9E054EA31C70EC1_176(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___929747B3E91297D56A461EE1B9E054EA31C70EC1_176 = value;
	}

	inline static int32_t get_offset_of_U39597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U39597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177() const { return ___9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U39597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177() { return &___9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177; }
	inline void set_U39597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177 = value;
	}

	inline static int32_t get_offset_of_U3959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_U3959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178() const { return ___959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_U3959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178() { return &___959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178; }
	inline void set_U3959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178 = value;
	}

	inline static int32_t get_offset_of_U395CA85749ADCFBF9A2B82C0381DBCF95D175524C_179() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___95CA85749ADCFBF9A2B82C0381DBCF95D175524C_179)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_U395CA85749ADCFBF9A2B82C0381DBCF95D175524C_179() const { return ___95CA85749ADCFBF9A2B82C0381DBCF95D175524C_179; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_U395CA85749ADCFBF9A2B82C0381DBCF95D175524C_179() { return &___95CA85749ADCFBF9A2B82C0381DBCF95D175524C_179; }
	inline void set_U395CA85749ADCFBF9A2B82C0381DBCF95D175524C_179(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___95CA85749ADCFBF9A2B82C0381DBCF95D175524C_179 = value;
	}

	inline static int32_t get_offset_of_U396ADC3934F8492C827987DFEE3B4DD4EF1738E78_180() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___96ADC3934F8492C827987DFEE3B4DD4EF1738E78_180)); }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  get_U396ADC3934F8492C827987DFEE3B4DD4EF1738E78_180() const { return ___96ADC3934F8492C827987DFEE3B4DD4EF1738E78_180; }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0 * get_address_of_U396ADC3934F8492C827987DFEE3B4DD4EF1738E78_180() { return &___96ADC3934F8492C827987DFEE3B4DD4EF1738E78_180; }
	inline void set_U396ADC3934F8492C827987DFEE3B4DD4EF1738E78_180(__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  value)
	{
		___96ADC3934F8492C827987DFEE3B4DD4EF1738E78_180 = value;
	}

	inline static int32_t get_offset_of_U398CF708E75DA60C2A04D06606E726EC975E6DBAB_181() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___98CF708E75DA60C2A04D06606E726EC975E6DBAB_181)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_U398CF708E75DA60C2A04D06606E726EC975E6DBAB_181() const { return ___98CF708E75DA60C2A04D06606E726EC975E6DBAB_181; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_U398CF708E75DA60C2A04D06606E726EC975E6DBAB_181() { return &___98CF708E75DA60C2A04D06606E726EC975E6DBAB_181; }
	inline void set_U398CF708E75DA60C2A04D06606E726EC975E6DBAB_181(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___98CF708E75DA60C2A04D06606E726EC975E6DBAB_181 = value;
	}

	inline static int32_t get_offset_of_U39AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U39AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182() const { return ___9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U39AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182() { return &___9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182; }
	inline void set_U39AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182 = value;
	}

	inline static int32_t get_offset_of_U39E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_U39E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183() const { return ___9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_U39E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183() { return &___9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183; }
	inline void set_U39E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183 = value;
	}

	inline static int32_t get_offset_of_U39F198D04FF67738AE583484145F45103ECE550CC_184() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9F198D04FF67738AE583484145F45103ECE550CC_184)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_U39F198D04FF67738AE583484145F45103ECE550CC_184() const { return ___9F198D04FF67738AE583484145F45103ECE550CC_184; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_U39F198D04FF67738AE583484145F45103ECE550CC_184() { return &___9F198D04FF67738AE583484145F45103ECE550CC_184; }
	inline void set_U39F198D04FF67738AE583484145F45103ECE550CC_184(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___9F198D04FF67738AE583484145F45103ECE550CC_184 = value;
	}

	inline static int32_t get_offset_of_U39F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185)); }
	inline __StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85  get_U39F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185() const { return ___9F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185; }
	inline __StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85 * get_address_of_U39F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185() { return &___9F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185; }
	inline void set_U39F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185(__StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85  value)
	{
		___9F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185 = value;
	}

	inline static int32_t get_offset_of_U39FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_U39FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186() const { return ___9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_U39FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186() { return &___9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186; }
	inline void set_U39FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186 = value;
	}

	inline static int32_t get_offset_of_U39FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_U39FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187() const { return ___9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_U39FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187() { return &___9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187; }
	inline void set_U39FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187 = value;
	}

	inline static int32_t get_offset_of_A0FABB8173BA247898A9FA267D0CE05500B667A0_188() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A0FABB8173BA247898A9FA267D0CE05500B667A0_188)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_A0FABB8173BA247898A9FA267D0CE05500B667A0_188() const { return ___A0FABB8173BA247898A9FA267D0CE05500B667A0_188; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_A0FABB8173BA247898A9FA267D0CE05500B667A0_188() { return &___A0FABB8173BA247898A9FA267D0CE05500B667A0_188; }
	inline void set_A0FABB8173BA247898A9FA267D0CE05500B667A0_188(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___A0FABB8173BA247898A9FA267D0CE05500B667A0_188 = value;
	}

	inline static int32_t get_offset_of_A19414BB17AF83ECF276AD49117E7F2EE344D567_189() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A19414BB17AF83ECF276AD49117E7F2EE344D567_189)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_A19414BB17AF83ECF276AD49117E7F2EE344D567_189() const { return ___A19414BB17AF83ECF276AD49117E7F2EE344D567_189; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_A19414BB17AF83ECF276AD49117E7F2EE344D567_189() { return &___A19414BB17AF83ECF276AD49117E7F2EE344D567_189; }
	inline void set_A19414BB17AF83ECF276AD49117E7F2EE344D567_189(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___A19414BB17AF83ECF276AD49117E7F2EE344D567_189 = value;
	}

	inline static int32_t get_offset_of_A32D5F33452B98E86BE15ED849ED6DF543B5F243_190() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A32D5F33452B98E86BE15ED849ED6DF543B5F243_190)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_A32D5F33452B98E86BE15ED849ED6DF543B5F243_190() const { return ___A32D5F33452B98E86BE15ED849ED6DF543B5F243_190; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_A32D5F33452B98E86BE15ED849ED6DF543B5F243_190() { return &___A32D5F33452B98E86BE15ED849ED6DF543B5F243_190; }
	inline void set_A32D5F33452B98E86BE15ED849ED6DF543B5F243_190(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___A32D5F33452B98E86BE15ED849ED6DF543B5F243_190 = value;
	}

	inline static int32_t get_offset_of_A4313AAA146ACFCA88681B7BFC3D644005F3792B_191() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A4313AAA146ACFCA88681B7BFC3D644005F3792B_191)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_A4313AAA146ACFCA88681B7BFC3D644005F3792B_191() const { return ___A4313AAA146ACFCA88681B7BFC3D644005F3792B_191; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_A4313AAA146ACFCA88681B7BFC3D644005F3792B_191() { return &___A4313AAA146ACFCA88681B7BFC3D644005F3792B_191; }
	inline void set_A4313AAA146ACFCA88681B7BFC3D644005F3792B_191(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___A4313AAA146ACFCA88681B7BFC3D644005F3792B_191 = value;
	}

	inline static int32_t get_offset_of_A43F35160EEC5F8DD047667DD94358EDCD29BA27_192() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A43F35160EEC5F8DD047667DD94358EDCD29BA27_192)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_A43F35160EEC5F8DD047667DD94358EDCD29BA27_192() const { return ___A43F35160EEC5F8DD047667DD94358EDCD29BA27_192; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_A43F35160EEC5F8DD047667DD94358EDCD29BA27_192() { return &___A43F35160EEC5F8DD047667DD94358EDCD29BA27_192; }
	inline void set_A43F35160EEC5F8DD047667DD94358EDCD29BA27_192(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___A43F35160EEC5F8DD047667DD94358EDCD29BA27_192 = value;
	}

	inline static int32_t get_offset_of_A471AF3330805980C7041F978D3CFF8838054E14_193() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A471AF3330805980C7041F978D3CFF8838054E14_193)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_A471AF3330805980C7041F978D3CFF8838054E14_193() const { return ___A471AF3330805980C7041F978D3CFF8838054E14_193; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_A471AF3330805980C7041F978D3CFF8838054E14_193() { return &___A471AF3330805980C7041F978D3CFF8838054E14_193; }
	inline void set_A471AF3330805980C7041F978D3CFF8838054E14_193(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___A471AF3330805980C7041F978D3CFF8838054E14_193 = value;
	}

	inline static int32_t get_offset_of_A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194)); }
	inline __StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4  get_A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194() const { return ___A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194; }
	inline __StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4 * get_address_of_A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194() { return &___A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194; }
	inline void set_A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194(__StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4  value)
	{
		___A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194 = value;
	}

	inline static int32_t get_offset_of_A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195() const { return ___A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195() { return &___A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195; }
	inline void set_A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195 = value;
	}

	inline static int32_t get_offset_of_A53306F44DF494A019EA1487807B59CA336BF024_196() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A53306F44DF494A019EA1487807B59CA336BF024_196)); }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  get_A53306F44DF494A019EA1487807B59CA336BF024_196() const { return ___A53306F44DF494A019EA1487807B59CA336BF024_196; }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A * get_address_of_A53306F44DF494A019EA1487807B59CA336BF024_196() { return &___A53306F44DF494A019EA1487807B59CA336BF024_196; }
	inline void set_A53306F44DF494A019EA1487807B59CA336BF024_196(__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  value)
	{
		___A53306F44DF494A019EA1487807B59CA336BF024_196 = value;
	}

	inline static int32_t get_offset_of_A5981DCAA364B0DC9E0385D893A31C2022364075_197() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A5981DCAA364B0DC9E0385D893A31C2022364075_197)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_A5981DCAA364B0DC9E0385D893A31C2022364075_197() const { return ___A5981DCAA364B0DC9E0385D893A31C2022364075_197; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_A5981DCAA364B0DC9E0385D893A31C2022364075_197() { return &___A5981DCAA364B0DC9E0385D893A31C2022364075_197; }
	inline void set_A5981DCAA364B0DC9E0385D893A31C2022364075_197(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___A5981DCAA364B0DC9E0385D893A31C2022364075_197 = value;
	}

	inline static int32_t get_offset_of_A696E1EE8632C559732B81052E4D2993B8783877_198() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A696E1EE8632C559732B81052E4D2993B8783877_198)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_A696E1EE8632C559732B81052E4D2993B8783877_198() const { return ___A696E1EE8632C559732B81052E4D2993B8783877_198; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_A696E1EE8632C559732B81052E4D2993B8783877_198() { return &___A696E1EE8632C559732B81052E4D2993B8783877_198; }
	inline void set_A696E1EE8632C559732B81052E4D2993B8783877_198(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___A696E1EE8632C559732B81052E4D2993B8783877_198 = value;
	}

	inline static int32_t get_offset_of_A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199() const { return ___A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199() { return &___A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199; }
	inline void set_A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199 = value;
	}

	inline static int32_t get_offset_of_A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200() const { return ___A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200() { return &___A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200; }
	inline void set_A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200 = value;
	}

	inline static int32_t get_offset_of_A77B822E6880234B59FD1904BF79AF68F5CB623F_201() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A77B822E6880234B59FD1904BF79AF68F5CB623F_201)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_A77B822E6880234B59FD1904BF79AF68F5CB623F_201() const { return ___A77B822E6880234B59FD1904BF79AF68F5CB623F_201; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_A77B822E6880234B59FD1904BF79AF68F5CB623F_201() { return &___A77B822E6880234B59FD1904BF79AF68F5CB623F_201; }
	inline void set_A77B822E6880234B59FD1904BF79AF68F5CB623F_201(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___A77B822E6880234B59FD1904BF79AF68F5CB623F_201 = value;
	}

	inline static int32_t get_offset_of_A94DF043C88FB07ACCA327B6A0BB063F20637E71_202() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___A94DF043C88FB07ACCA327B6A0BB063F20637E71_202)); }
	inline __StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9  get_A94DF043C88FB07ACCA327B6A0BB063F20637E71_202() const { return ___A94DF043C88FB07ACCA327B6A0BB063F20637E71_202; }
	inline __StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9 * get_address_of_A94DF043C88FB07ACCA327B6A0BB063F20637E71_202() { return &___A94DF043C88FB07ACCA327B6A0BB063F20637E71_202; }
	inline void set_A94DF043C88FB07ACCA327B6A0BB063F20637E71_202(__StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9  value)
	{
		___A94DF043C88FB07ACCA327B6A0BB063F20637E71_202 = value;
	}

	inline static int32_t get_offset_of_AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203)); }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  get_AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203() const { return ___AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203; }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A * get_address_of_AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203() { return &___AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203; }
	inline void set_AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203(__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  value)
	{
		___AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203 = value;
	}

	inline static int32_t get_offset_of_AAF72C1002FDBCAE040DAE16A10D82184CE83679_204() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AAF72C1002FDBCAE040DAE16A10D82184CE83679_204)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_AAF72C1002FDBCAE040DAE16A10D82184CE83679_204() const { return ___AAF72C1002FDBCAE040DAE16A10D82184CE83679_204; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_AAF72C1002FDBCAE040DAE16A10D82184CE83679_204() { return &___AAF72C1002FDBCAE040DAE16A10D82184CE83679_204; }
	inline void set_AAF72C1002FDBCAE040DAE16A10D82184CE83679_204(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___AAF72C1002FDBCAE040DAE16A10D82184CE83679_204 = value;
	}

	inline static int32_t get_offset_of_AD19F20EECB80A2079F504CB928A26FDE10E8C47_205() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AD19F20EECB80A2079F504CB928A26FDE10E8C47_205)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_AD19F20EECB80A2079F504CB928A26FDE10E8C47_205() const { return ___AD19F20EECB80A2079F504CB928A26FDE10E8C47_205; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_AD19F20EECB80A2079F504CB928A26FDE10E8C47_205() { return &___AD19F20EECB80A2079F504CB928A26FDE10E8C47_205; }
	inline void set_AD19F20EECB80A2079F504CB928A26FDE10E8C47_205(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___AD19F20EECB80A2079F504CB928A26FDE10E8C47_205 = value;
	}

	inline static int32_t get_offset_of_AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206() const { return ___AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206() { return &___AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206; }
	inline void set_AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206 = value;
	}

	inline static int32_t get_offset_of_AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207() const { return ___AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207() { return &___AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207; }
	inline void set_AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207 = value;
	}

	inline static int32_t get_offset_of_AE843E1C1136C908565A6D4E04E8564B69465B3B_208() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AE843E1C1136C908565A6D4E04E8564B69465B3B_208)); }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  get_AE843E1C1136C908565A6D4E04E8564B69465B3B_208() const { return ___AE843E1C1136C908565A6D4E04E8564B69465B3B_208; }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A * get_address_of_AE843E1C1136C908565A6D4E04E8564B69465B3B_208() { return &___AE843E1C1136C908565A6D4E04E8564B69465B3B_208; }
	inline void set_AE843E1C1136C908565A6D4E04E8564B69465B3B_208(__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  value)
	{
		___AE843E1C1136C908565A6D4E04E8564B69465B3B_208 = value;
	}

	inline static int32_t get_offset_of_AF3E960D2F0572119C78CAF04B900985A299000E_209() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___AF3E960D2F0572119C78CAF04B900985A299000E_209)); }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  get_AF3E960D2F0572119C78CAF04B900985A299000E_209() const { return ___AF3E960D2F0572119C78CAF04B900985A299000E_209; }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0 * get_address_of_AF3E960D2F0572119C78CAF04B900985A299000E_209() { return &___AF3E960D2F0572119C78CAF04B900985A299000E_209; }
	inline void set_AF3E960D2F0572119C78CAF04B900985A299000E_209(__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  value)
	{
		___AF3E960D2F0572119C78CAF04B900985A299000E_209 = value;
	}

	inline static int32_t get_offset_of_B01915493E44597A78D21AC245268E65DDA920ED_210() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B01915493E44597A78D21AC245268E65DDA920ED_210)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_B01915493E44597A78D21AC245268E65DDA920ED_210() const { return ___B01915493E44597A78D21AC245268E65DDA920ED_210; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_B01915493E44597A78D21AC245268E65DDA920ED_210() { return &___B01915493E44597A78D21AC245268E65DDA920ED_210; }
	inline void set_B01915493E44597A78D21AC245268E65DDA920ED_210(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___B01915493E44597A78D21AC245268E65DDA920ED_210 = value;
	}

	inline static int32_t get_offset_of_B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211() const { return ___B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211() { return &___B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211; }
	inline void set_B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211 = value;
	}

	inline static int32_t get_offset_of_B31A9EA1A2718942996E7F94106F0FC798C174C8_212() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B31A9EA1A2718942996E7F94106F0FC798C174C8_212)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_B31A9EA1A2718942996E7F94106F0FC798C174C8_212() const { return ___B31A9EA1A2718942996E7F94106F0FC798C174C8_212; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_B31A9EA1A2718942996E7F94106F0FC798C174C8_212() { return &___B31A9EA1A2718942996E7F94106F0FC798C174C8_212; }
	inline void set_B31A9EA1A2718942996E7F94106F0FC798C174C8_212(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___B31A9EA1A2718942996E7F94106F0FC798C174C8_212 = value;
	}

	inline static int32_t get_offset_of_B3A60EC240A886DA5AFD600CC73AE12514A881E8_213() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B3A60EC240A886DA5AFD600CC73AE12514A881E8_213)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_B3A60EC240A886DA5AFD600CC73AE12514A881E8_213() const { return ___B3A60EC240A886DA5AFD600CC73AE12514A881E8_213; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_B3A60EC240A886DA5AFD600CC73AE12514A881E8_213() { return &___B3A60EC240A886DA5AFD600CC73AE12514A881E8_213; }
	inline void set_B3A60EC240A886DA5AFD600CC73AE12514A881E8_213(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___B3A60EC240A886DA5AFD600CC73AE12514A881E8_213 = value;
	}

	inline static int32_t get_offset_of_B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214() const { return ___B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214() { return &___B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214; }
	inline void set_B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214 = value;
	}

	inline static int32_t get_offset_of_B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215() const { return ___B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215() { return &___B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215; }
	inline void set_B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215 = value;
	}

	inline static int32_t get_offset_of_B5E8BA68953A5283AD953094F0F391FA4502A3FA_216() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B5E8BA68953A5283AD953094F0F391FA4502A3FA_216)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_B5E8BA68953A5283AD953094F0F391FA4502A3FA_216() const { return ___B5E8BA68953A5283AD953094F0F391FA4502A3FA_216; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_B5E8BA68953A5283AD953094F0F391FA4502A3FA_216() { return &___B5E8BA68953A5283AD953094F0F391FA4502A3FA_216; }
	inline void set_B5E8BA68953A5283AD953094F0F391FA4502A3FA_216(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___B5E8BA68953A5283AD953094F0F391FA4502A3FA_216 = value;
	}

	inline static int32_t get_offset_of_B67A7FB4648C62F6A1337CA473436D0E787E8633_217() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B67A7FB4648C62F6A1337CA473436D0E787E8633_217)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_B67A7FB4648C62F6A1337CA473436D0E787E8633_217() const { return ___B67A7FB4648C62F6A1337CA473436D0E787E8633_217; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_B67A7FB4648C62F6A1337CA473436D0E787E8633_217() { return &___B67A7FB4648C62F6A1337CA473436D0E787E8633_217; }
	inline void set_B67A7FB4648C62F6A1337CA473436D0E787E8633_217(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___B67A7FB4648C62F6A1337CA473436D0E787E8633_217 = value;
	}

	inline static int32_t get_offset_of_B68637EF60D499620B99E336C59E4865FFC4C5D7_218() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B68637EF60D499620B99E336C59E4865FFC4C5D7_218)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_B68637EF60D499620B99E336C59E4865FFC4C5D7_218() const { return ___B68637EF60D499620B99E336C59E4865FFC4C5D7_218; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_B68637EF60D499620B99E336C59E4865FFC4C5D7_218() { return &___B68637EF60D499620B99E336C59E4865FFC4C5D7_218; }
	inline void set_B68637EF60D499620B99E336C59E4865FFC4C5D7_218(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___B68637EF60D499620B99E336C59E4865FFC4C5D7_218 = value;
	}

	inline static int32_t get_offset_of_B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219() const { return ___B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219() { return &___B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219; }
	inline void set_B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219 = value;
	}

	inline static int32_t get_offset_of_B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220)); }
	inline __StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD  get_B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220() const { return ___B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220; }
	inline __StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD * get_address_of_B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220() { return &___B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220; }
	inline void set_B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220(__StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD  value)
	{
		___B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220 = value;
	}

	inline static int32_t get_offset_of_B997A0149EBF3CDD050D72AE1784E375A413B128_221() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___B997A0149EBF3CDD050D72AE1784E375A413B128_221)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_B997A0149EBF3CDD050D72AE1784E375A413B128_221() const { return ___B997A0149EBF3CDD050D72AE1784E375A413B128_221; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_B997A0149EBF3CDD050D72AE1784E375A413B128_221() { return &___B997A0149EBF3CDD050D72AE1784E375A413B128_221; }
	inline void set_B997A0149EBF3CDD050D72AE1784E375A413B128_221(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___B997A0149EBF3CDD050D72AE1784E375A413B128_221 = value;
	}

	inline static int32_t get_offset_of_BBBDF1388DDDDEEC01227AF1738F138452242E3A_222() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___BBBDF1388DDDDEEC01227AF1738F138452242E3A_222)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_BBBDF1388DDDDEEC01227AF1738F138452242E3A_222() const { return ___BBBDF1388DDDDEEC01227AF1738F138452242E3A_222; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_BBBDF1388DDDDEEC01227AF1738F138452242E3A_222() { return &___BBBDF1388DDDDEEC01227AF1738F138452242E3A_222; }
	inline void set_BBBDF1388DDDDEEC01227AF1738F138452242E3A_222(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___BBBDF1388DDDDEEC01227AF1738F138452242E3A_222 = value;
	}

	inline static int32_t get_offset_of_BCE617693C33CE2C76FE00F449CA910E4C6E117E_223() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___BCE617693C33CE2C76FE00F449CA910E4C6E117E_223)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_BCE617693C33CE2C76FE00F449CA910E4C6E117E_223() const { return ___BCE617693C33CE2C76FE00F449CA910E4C6E117E_223; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_BCE617693C33CE2C76FE00F449CA910E4C6E117E_223() { return &___BCE617693C33CE2C76FE00F449CA910E4C6E117E_223; }
	inline void set_BCE617693C33CE2C76FE00F449CA910E4C6E117E_223(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___BCE617693C33CE2C76FE00F449CA910E4C6E117E_223 = value;
	}

	inline static int32_t get_offset_of_BD0C733AC85AB335526865FA6269790347865705_224() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___BD0C733AC85AB335526865FA6269790347865705_224)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_BD0C733AC85AB335526865FA6269790347865705_224() const { return ___BD0C733AC85AB335526865FA6269790347865705_224; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_BD0C733AC85AB335526865FA6269790347865705_224() { return &___BD0C733AC85AB335526865FA6269790347865705_224; }
	inline void set_BD0C733AC85AB335526865FA6269790347865705_224(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___BD0C733AC85AB335526865FA6269790347865705_224 = value;
	}

	inline static int32_t get_offset_of_BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225)); }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  get_BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225() const { return ___BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225; }
	inline __StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 * get_address_of_BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225() { return &___BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225; }
	inline void set_BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51  value)
	{
		___BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225 = value;
	}

	inline static int32_t get_offset_of_BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226() const { return ___BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226() { return &___BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226; }
	inline void set_BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226 = value;
	}

	inline static int32_t get_offset_of_C079C42AC966756C902EC38C4D7989F3C20D3625_227() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C079C42AC966756C902EC38C4D7989F3C20D3625_227)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_C079C42AC966756C902EC38C4D7989F3C20D3625_227() const { return ___C079C42AC966756C902EC38C4D7989F3C20D3625_227; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_C079C42AC966756C902EC38C4D7989F3C20D3625_227() { return &___C079C42AC966756C902EC38C4D7989F3C20D3625_227; }
	inline void set_C079C42AC966756C902EC38C4D7989F3C20D3625_227(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___C079C42AC966756C902EC38C4D7989F3C20D3625_227 = value;
	}

	inline static int32_t get_offset_of_C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228() const { return ___C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228() { return &___C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228; }
	inline void set_C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228 = value;
	}

	inline static int32_t get_offset_of_C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229)); }
	inline __StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015  get_C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229() const { return ___C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229; }
	inline __StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015 * get_address_of_C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229() { return &___C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229; }
	inline void set_C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229(__StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015  value)
	{
		___C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229 = value;
	}

	inline static int32_t get_offset_of_C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230() const { return ___C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230() { return &___C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230; }
	inline void set_C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230 = value;
	}

	inline static int32_t get_offset_of_C132685022CE310ACFD3A883E0A57033A482A959_231() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C132685022CE310ACFD3A883E0A57033A482A959_231)); }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  get_C132685022CE310ACFD3A883E0A57033A482A959_231() const { return ___C132685022CE310ACFD3A883E0A57033A482A959_231; }
	inline __StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B * get_address_of_C132685022CE310ACFD3A883E0A57033A482A959_231() { return &___C132685022CE310ACFD3A883E0A57033A482A959_231; }
	inline void set_C132685022CE310ACFD3A883E0A57033A482A959_231(__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B  value)
	{
		___C132685022CE310ACFD3A883E0A57033A482A959_231 = value;
	}

	inline static int32_t get_offset_of_C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232() const { return ___C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232() { return &___C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232; }
	inline void set_C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232 = value;
	}

	inline static int32_t get_offset_of_C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233() const { return ___C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233() { return &___C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233; }
	inline void set_C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233 = value;
	}

	inline static int32_t get_offset_of_C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234() const { return ___C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234() { return &___C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234; }
	inline void set_C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234 = value;
	}

	inline static int32_t get_offset_of_C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235() const { return ___C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235() { return &___C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235; }
	inline void set_C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235 = value;
	}

	inline static int32_t get_offset_of_C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236() const { return ___C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236() { return &___C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236; }
	inline void set_C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236 = value;
	}

	inline static int32_t get_offset_of_C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237() const { return ___C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237() { return &___C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237; }
	inline void set_C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237 = value;
	}

	inline static int32_t get_offset_of_C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238)); }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  get_C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238() const { return ___C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238; }
	inline __StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A * get_address_of_C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238() { return &___C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238; }
	inline void set_C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238(__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A  value)
	{
		___C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238 = value;
	}

	inline static int32_t get_offset_of_C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239() const { return ___C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239() { return &___C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239; }
	inline void set_C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239 = value;
	}

	inline static int32_t get_offset_of_C95356610D5583976B69017BED7048EB50121B90_240() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C95356610D5583976B69017BED7048EB50121B90_240)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_C95356610D5583976B69017BED7048EB50121B90_240() const { return ___C95356610D5583976B69017BED7048EB50121B90_240; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_C95356610D5583976B69017BED7048EB50121B90_240() { return &___C95356610D5583976B69017BED7048EB50121B90_240; }
	inline void set_C95356610D5583976B69017BED7048EB50121B90_240(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___C95356610D5583976B69017BED7048EB50121B90_240 = value;
	}

	inline static int32_t get_offset_of_C991C784E7697AD0F91A159F03727BF4621A5AB8_241() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___C991C784E7697AD0F91A159F03727BF4621A5AB8_241)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_C991C784E7697AD0F91A159F03727BF4621A5AB8_241() const { return ___C991C784E7697AD0F91A159F03727BF4621A5AB8_241; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_C991C784E7697AD0F91A159F03727BF4621A5AB8_241() { return &___C991C784E7697AD0F91A159F03727BF4621A5AB8_241; }
	inline void set_C991C784E7697AD0F91A159F03727BF4621A5AB8_241(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___C991C784E7697AD0F91A159F03727BF4621A5AB8_241 = value;
	}

	inline static int32_t get_offset_of_CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242)); }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  get_CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242() const { return ___CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242; }
	inline __StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 * get_address_of_CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242() { return &___CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242; }
	inline void set_CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373  value)
	{
		___CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242 = value;
	}

	inline static int32_t get_offset_of_CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243() const { return ___CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243() { return &___CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243; }
	inline void set_CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243 = value;
	}

	inline static int32_t get_offset_of_CC53D7FE00E6AC1385AF09521629229467BCCC86_244() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CC53D7FE00E6AC1385AF09521629229467BCCC86_244)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_CC53D7FE00E6AC1385AF09521629229467BCCC86_244() const { return ___CC53D7FE00E6AC1385AF09521629229467BCCC86_244; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_CC53D7FE00E6AC1385AF09521629229467BCCC86_244() { return &___CC53D7FE00E6AC1385AF09521629229467BCCC86_244; }
	inline void set_CC53D7FE00E6AC1385AF09521629229467BCCC86_244(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___CC53D7FE00E6AC1385AF09521629229467BCCC86_244 = value;
	}

	inline static int32_t get_offset_of_CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245() const { return ___CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245() { return &___CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245; }
	inline void set_CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245 = value;
	}

	inline static int32_t get_offset_of_CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246() const { return ___CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246() { return &___CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246; }
	inline void set_CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246 = value;
	}

	inline static int32_t get_offset_of_CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247)); }
	inline __StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015  get_CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247() const { return ___CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247; }
	inline __StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015 * get_address_of_CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247() { return &___CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247; }
	inline void set_CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247(__StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015  value)
	{
		___CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247 = value;
	}

	inline static int32_t get_offset_of_CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248() const { return ___CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248() { return &___CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248; }
	inline void set_CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248 = value;
	}

	inline static int32_t get_offset_of_D068832E6B13A623916709C1E0E25ADCBE7B455F_249() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D068832E6B13A623916709C1E0E25ADCBE7B455F_249)); }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  get_D068832E6B13A623916709C1E0E25ADCBE7B455F_249() const { return ___D068832E6B13A623916709C1E0E25ADCBE7B455F_249; }
	inline __StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27 * get_address_of_D068832E6B13A623916709C1E0E25ADCBE7B455F_249() { return &___D068832E6B13A623916709C1E0E25ADCBE7B455F_249; }
	inline void set_D068832E6B13A623916709C1E0E25ADCBE7B455F_249(__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27  value)
	{
		___D068832E6B13A623916709C1E0E25ADCBE7B455F_249 = value;
	}

	inline static int32_t get_offset_of_D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250)); }
	inline __StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009  get_D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250() const { return ___D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250; }
	inline __StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009 * get_address_of_D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250() { return &___D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250; }
	inline void set_D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250(__StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009  value)
	{
		___D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250 = value;
	}

	inline static int32_t get_offset_of_D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251() const { return ___D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251() { return &___D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251; }
	inline void set_D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251 = value;
	}

	inline static int32_t get_offset_of_D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252)); }
	inline __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  get_D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252() const { return ___D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252; }
	inline __StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981 * get_address_of_D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252() { return &___D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252; }
	inline void set_D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252(__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981  value)
	{
		___D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252 = value;
	}

	inline static int32_t get_offset_of_D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253() const { return ___D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253() { return &___D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253; }
	inline void set_D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253 = value;
	}

	inline static int32_t get_offset_of_D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254() const { return ___D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254() { return &___D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254; }
	inline void set_D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254 = value;
	}

	inline static int32_t get_offset_of_D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255() const { return ___D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255() { return &___D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255; }
	inline void set_D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255 = value;
	}

	inline static int32_t get_offset_of_D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256)); }
	inline int32_t get_D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256() const { return ___D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256; }
	inline int32_t* get_address_of_D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256() { return &___D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256; }
	inline void set_D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256(int32_t value)
	{
		___D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256 = value;
	}

	inline static int32_t get_offset_of_D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257() const { return ___D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257() { return &___D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257; }
	inline void set_D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257 = value;
	}

	inline static int32_t get_offset_of_D7231C06B1D6276752359120E26EAE206A7F74F9_258() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D7231C06B1D6276752359120E26EAE206A7F74F9_258)); }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  get_D7231C06B1D6276752359120E26EAE206A7F74F9_258() const { return ___D7231C06B1D6276752359120E26EAE206A7F74F9_258; }
	inline __StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A * get_address_of_D7231C06B1D6276752359120E26EAE206A7F74F9_258() { return &___D7231C06B1D6276752359120E26EAE206A7F74F9_258; }
	inline void set_D7231C06B1D6276752359120E26EAE206A7F74F9_258(__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A  value)
	{
		___D7231C06B1D6276752359120E26EAE206A7F74F9_258 = value;
	}

	inline static int32_t get_offset_of_D73609A97D6906CFFB02ADDA320A4809483C458D_259() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D73609A97D6906CFFB02ADDA320A4809483C458D_259)); }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  get_D73609A97D6906CFFB02ADDA320A4809483C458D_259() const { return ___D73609A97D6906CFFB02ADDA320A4809483C458D_259; }
	inline __StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 * get_address_of_D73609A97D6906CFFB02ADDA320A4809483C458D_259() { return &___D73609A97D6906CFFB02ADDA320A4809483C458D_259; }
	inline void set_D73609A97D6906CFFB02ADDA320A4809483C458D_259(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510  value)
	{
		___D73609A97D6906CFFB02ADDA320A4809483C458D_259 = value;
	}

	inline static int32_t get_offset_of_D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260() const { return ___D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260() { return &___D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260; }
	inline void set_D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260 = value;
	}

	inline static int32_t get_offset_of_D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261() const { return ___D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261() { return &___D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261; }
	inline void set_D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261 = value;
	}

	inline static int32_t get_offset_of_D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262() const { return ___D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262() { return &___D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262; }
	inline void set_D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262 = value;
	}

	inline static int32_t get_offset_of_D9C221237B647EC215A7BCDED447349810E6BF9C_263() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___D9C221237B647EC215A7BCDED447349810E6BF9C_263)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_D9C221237B647EC215A7BCDED447349810E6BF9C_263() const { return ___D9C221237B647EC215A7BCDED447349810E6BF9C_263; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_D9C221237B647EC215A7BCDED447349810E6BF9C_263() { return &___D9C221237B647EC215A7BCDED447349810E6BF9C_263; }
	inline void set_D9C221237B647EC215A7BCDED447349810E6BF9C_263(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___D9C221237B647EC215A7BCDED447349810E6BF9C_263 = value;
	}

	inline static int32_t get_offset_of_DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264() const { return ___DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264() { return &___DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264; }
	inline void set_DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264 = value;
	}

	inline static int32_t get_offset_of_DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265)); }
	inline __StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821  get_DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265() const { return ___DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265; }
	inline __StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821 * get_address_of_DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265() { return &___DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265; }
	inline void set_DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265(__StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821  value)
	{
		___DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265 = value;
	}

	inline static int32_t get_offset_of_DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266)); }
	inline __StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64  get_DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266() const { return ___DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266; }
	inline __StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64 * get_address_of_DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266() { return &___DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266; }
	inline void set_DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266(__StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64  value)
	{
		___DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266 = value;
	}

	inline static int32_t get_offset_of_DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267)); }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  get_DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267() const { return ___DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267; }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 * get_address_of_DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267() { return &___DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267; }
	inline void set_DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267(__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  value)
	{
		___DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267 = value;
	}

	inline static int32_t get_offset_of_E0219F11D9EECC43022AA94967780250AC270D4B_268() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E0219F11D9EECC43022AA94967780250AC270D4B_268)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_E0219F11D9EECC43022AA94967780250AC270D4B_268() const { return ___E0219F11D9EECC43022AA94967780250AC270D4B_268; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_E0219F11D9EECC43022AA94967780250AC270D4B_268() { return &___E0219F11D9EECC43022AA94967780250AC270D4B_268; }
	inline void set_E0219F11D9EECC43022AA94967780250AC270D4B_268(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___E0219F11D9EECC43022AA94967780250AC270D4B_268 = value;
	}

	inline static int32_t get_offset_of_E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269() const { return ___E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269() { return &___E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269; }
	inline void set_E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269 = value;
	}

	inline static int32_t get_offset_of_E17D18DCD8392C99D47823F8CB9F43896D115FB8_270() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E17D18DCD8392C99D47823F8CB9F43896D115FB8_270)); }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  get_E17D18DCD8392C99D47823F8CB9F43896D115FB8_270() const { return ___E17D18DCD8392C99D47823F8CB9F43896D115FB8_270; }
	inline __StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 * get_address_of_E17D18DCD8392C99D47823F8CB9F43896D115FB8_270() { return &___E17D18DCD8392C99D47823F8CB9F43896D115FB8_270; }
	inline void set_E17D18DCD8392C99D47823F8CB9F43896D115FB8_270(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018  value)
	{
		___E17D18DCD8392C99D47823F8CB9F43896D115FB8_270 = value;
	}

	inline static int32_t get_offset_of_E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271() const { return ___E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271() { return &___E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271; }
	inline void set_E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271 = value;
	}

	inline static int32_t get_offset_of_E43F6BA634642FB90FEEE1A8F9905E957741960C_272() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E43F6BA634642FB90FEEE1A8F9905E957741960C_272)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_E43F6BA634642FB90FEEE1A8F9905E957741960C_272() const { return ___E43F6BA634642FB90FEEE1A8F9905E957741960C_272; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_E43F6BA634642FB90FEEE1A8F9905E957741960C_272() { return &___E43F6BA634642FB90FEEE1A8F9905E957741960C_272; }
	inline void set_E43F6BA634642FB90FEEE1A8F9905E957741960C_272(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___E43F6BA634642FB90FEEE1A8F9905E957741960C_272 = value;
	}

	inline static int32_t get_offset_of_E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273() const { return ___E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273() { return &___E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273; }
	inline void set_E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273 = value;
	}

	inline static int32_t get_offset_of_E647D32D165F3510693DF9787DC98E0A0B63C5C2_274() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___E647D32D165F3510693DF9787DC98E0A0B63C5C2_274)); }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  get_E647D32D165F3510693DF9787DC98E0A0B63C5C2_274() const { return ___E647D32D165F3510693DF9787DC98E0A0B63C5C2_274; }
	inline __StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 * get_address_of_E647D32D165F3510693DF9787DC98E0A0B63C5C2_274() { return &___E647D32D165F3510693DF9787DC98E0A0B63C5C2_274; }
	inline void set_E647D32D165F3510693DF9787DC98E0A0B63C5C2_274(__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256  value)
	{
		___E647D32D165F3510693DF9787DC98E0A0B63C5C2_274 = value;
	}

	inline static int32_t get_offset_of_EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275() const { return ___EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275() { return &___EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275; }
	inline void set_EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275 = value;
	}

	inline static int32_t get_offset_of_EB6F545AEF284339D25594F900E7A395212460EB_276() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EB6F545AEF284339D25594F900E7A395212460EB_276)); }
	inline __StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44  get_EB6F545AEF284339D25594F900E7A395212460EB_276() const { return ___EB6F545AEF284339D25594F900E7A395212460EB_276; }
	inline __StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44 * get_address_of_EB6F545AEF284339D25594F900E7A395212460EB_276() { return &___EB6F545AEF284339D25594F900E7A395212460EB_276; }
	inline void set_EB6F545AEF284339D25594F900E7A395212460EB_276(__StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44  value)
	{
		___EB6F545AEF284339D25594F900E7A395212460EB_276 = value;
	}

	inline static int32_t get_offset_of_EBE167F7962841FA83451C9C1663416D69AA5294_277() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EBE167F7962841FA83451C9C1663416D69AA5294_277)); }
	inline __StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942  get_EBE167F7962841FA83451C9C1663416D69AA5294_277() const { return ___EBE167F7962841FA83451C9C1663416D69AA5294_277; }
	inline __StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942 * get_address_of_EBE167F7962841FA83451C9C1663416D69AA5294_277() { return &___EBE167F7962841FA83451C9C1663416D69AA5294_277; }
	inline void set_EBE167F7962841FA83451C9C1663416D69AA5294_277(__StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942  value)
	{
		___EBE167F7962841FA83451C9C1663416D69AA5294_277 = value;
	}

	inline static int32_t get_offset_of_EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278() const { return ___EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278() { return &___EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278; }
	inline void set_EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278 = value;
	}

	inline static int32_t get_offset_of_EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279() const { return ___EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279() { return &___EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279; }
	inline void set_EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279 = value;
	}

	inline static int32_t get_offset_of_EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280)); }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  get_EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280() const { return ___EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280; }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0 * get_address_of_EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280() { return &___EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280; }
	inline void set_EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280(__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  value)
	{
		___EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280 = value;
	}

	inline static int32_t get_offset_of_EF813A47B13574822D335279EF445343654A4F04_281() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___EF813A47B13574822D335279EF445343654A4F04_281)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_EF813A47B13574822D335279EF445343654A4F04_281() const { return ___EF813A47B13574822D335279EF445343654A4F04_281; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_EF813A47B13574822D335279EF445343654A4F04_281() { return &___EF813A47B13574822D335279EF445343654A4F04_281; }
	inline void set_EF813A47B13574822D335279EF445343654A4F04_281(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___EF813A47B13574822D335279EF445343654A4F04_281 = value;
	}

	inline static int32_t get_offset_of_F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282)); }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  get_F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282() const { return ___F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282; }
	inline __StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 * get_address_of_F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282() { return &___F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282; }
	inline void set_F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991  value)
	{
		___F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282 = value;
	}

	inline static int32_t get_offset_of_F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283() const { return ___F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283() { return &___F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283; }
	inline void set_F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283 = value;
	}

	inline static int32_t get_offset_of_F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284() const { return ___F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284() { return &___F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284; }
	inline void set_F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284 = value;
	}

	inline static int32_t get_offset_of_F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285() const { return ___F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285() { return &___F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285; }
	inline void set_F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285 = value;
	}

	inline static int32_t get_offset_of_F3E701C38098B41D23FA88977423371B3C00C0D1_286() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F3E701C38098B41D23FA88977423371B3C00C0D1_286)); }
	inline int32_t get_F3E701C38098B41D23FA88977423371B3C00C0D1_286() const { return ___F3E701C38098B41D23FA88977423371B3C00C0D1_286; }
	inline int32_t* get_address_of_F3E701C38098B41D23FA88977423371B3C00C0D1_286() { return &___F3E701C38098B41D23FA88977423371B3C00C0D1_286; }
	inline void set_F3E701C38098B41D23FA88977423371B3C00C0D1_286(int32_t value)
	{
		___F3E701C38098B41D23FA88977423371B3C00C0D1_286 = value;
	}

	inline static int32_t get_offset_of_F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287)); }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  get_F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287() const { return ___F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287; }
	inline __StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0 * get_address_of_F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287() { return &___F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287; }
	inline void set_F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287(__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0  value)
	{
		___F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287 = value;
	}

	inline static int32_t get_offset_of_F5AEFD834ADB72DAA720930140E9ECC087FCF389_288() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F5AEFD834ADB72DAA720930140E9ECC087FCF389_288)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_F5AEFD834ADB72DAA720930140E9ECC087FCF389_288() const { return ___F5AEFD834ADB72DAA720930140E9ECC087FCF389_288; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_F5AEFD834ADB72DAA720930140E9ECC087FCF389_288() { return &___F5AEFD834ADB72DAA720930140E9ECC087FCF389_288; }
	inline void set_F5AEFD834ADB72DAA720930140E9ECC087FCF389_288(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___F5AEFD834ADB72DAA720930140E9ECC087FCF389_288 = value;
	}

	inline static int32_t get_offset_of_F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289() const { return ___F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289() { return &___F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289; }
	inline void set_F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289 = value;
	}

	inline static int32_t get_offset_of_F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290)); }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  get_F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290() const { return ___F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290; }
	inline __StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E * get_address_of_F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290() { return &___F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290; }
	inline void set_F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E  value)
	{
		___F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290 = value;
	}

	inline static int32_t get_offset_of_FA5B1C8B2F287078ED719C15595DB729BDB85911_291() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FA5B1C8B2F287078ED719C15595DB729BDB85911_291)); }
	inline __StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44  get_FA5B1C8B2F287078ED719C15595DB729BDB85911_291() const { return ___FA5B1C8B2F287078ED719C15595DB729BDB85911_291; }
	inline __StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44 * get_address_of_FA5B1C8B2F287078ED719C15595DB729BDB85911_291() { return &___FA5B1C8B2F287078ED719C15595DB729BDB85911_291; }
	inline void set_FA5B1C8B2F287078ED719C15595DB729BDB85911_291(__StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44  value)
	{
		___FA5B1C8B2F287078ED719C15595DB729BDB85911_291 = value;
	}

	inline static int32_t get_offset_of_FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292)); }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  get_FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292() const { return ___FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292; }
	inline __StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 * get_address_of_FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292() { return &___FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292; }
	inline void set_FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742  value)
	{
		___FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292 = value;
	}

	inline static int32_t get_offset_of_FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293() const { return ___FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293() { return &___FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293; }
	inline void set_FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293 = value;
	}

	inline static int32_t get_offset_of_FB3C663794DD23F500825FF78450D198FE338938_294() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FB3C663794DD23F500825FF78450D198FE338938_294)); }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  get_FB3C663794DD23F500825FF78450D198FE338938_294() const { return ___FB3C663794DD23F500825FF78450D198FE338938_294; }
	inline __StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 * get_address_of_FB3C663794DD23F500825FF78450D198FE338938_294() { return &___FB3C663794DD23F500825FF78450D198FE338938_294; }
	inline void set_FB3C663794DD23F500825FF78450D198FE338938_294(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2  value)
	{
		___FB3C663794DD23F500825FF78450D198FE338938_294 = value;
	}

	inline static int32_t get_offset_of_FC9EEBC457831129D4AF4FF84333B481F4BED60E_295() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FC9EEBC457831129D4AF4FF84333B481F4BED60E_295)); }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  get_FC9EEBC457831129D4AF4FF84333B481F4BED60E_295() const { return ___FC9EEBC457831129D4AF4FF84333B481F4BED60E_295; }
	inline __StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B * get_address_of_FC9EEBC457831129D4AF4FF84333B481F4BED60E_295() { return &___FC9EEBC457831129D4AF4FF84333B481F4BED60E_295; }
	inline void set_FC9EEBC457831129D4AF4FF84333B481F4BED60E_295(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B  value)
	{
		___FC9EEBC457831129D4AF4FF84333B481F4BED60E_295 = value;
	}

	inline static int32_t get_offset_of_FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296() const { return ___FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296() { return &___FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296; }
	inline void set_FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296 = value;
	}

	inline static int32_t get_offset_of_FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297)); }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  get_FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297() const { return ___FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297; }
	inline __StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 * get_address_of_FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297() { return &___FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297; }
	inline void set_FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391  value)
	{
		___FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297 = value;
	}

	inline static int32_t get_offset_of_FE5567E8D769550852182CDF69D74BB16DFF8E29_298() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields, ___FE5567E8D769550852182CDF69D74BB16DFF8E29_298)); }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  get_FE5567E8D769550852182CDF69D74BB16DFF8E29_298() const { return ___FE5567E8D769550852182CDF69D74BB16DFF8E29_298; }
	inline __StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 * get_address_of_FE5567E8D769550852182CDF69D74BB16DFF8E29_298() { return &___FE5567E8D769550852182CDF69D74BB16DFF8E29_298; }
	inline void set_FE5567E8D769550852182CDF69D74BB16DFF8E29_298(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12  value)
	{
		___FE5567E8D769550852182CDF69D74BB16DFF8E29_298 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T837AC9D13901280E875914E3F0905F8FC0DD012F_H
#ifndef CONSTRUCTORHANDLING_TC9A0E967603307F5C573320F3D7CDDA57CFFEA88_H
#define CONSTRUCTORHANDLING_TC9A0E967603307F5C573320F3D7CDDA57CFFEA88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_tC9A0E967603307F5C573320F3D7CDDA57CFFEA88 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstructorHandling_tC9A0E967603307F5C573320F3D7CDDA57CFFEA88, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_TC9A0E967603307F5C573320F3D7CDDA57CFFEA88_H
#ifndef DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#define DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifndef DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#define DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#ifndef DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#define DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifndef DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#define DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#ifndef FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#define FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifndef FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#define FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#ifndef FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#define FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifndef JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#define JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifndef STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#define STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader_State
struct  State_tF808A376CD3F052C3C1BE586C60C34C534ADB010 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tF808A376CD3F052C3C1BE586C60C34C534ADB010, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#ifndef JSONREADEREXCEPTION_TABAD0FF737611157A789D571AC50787F1FF4FF1F_H
#define JSONREADEREXCEPTION_TABAD0FF737611157A789D571AC50787F1FF4FF1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReaderException
struct  JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F  : public JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78
{
public:
	// System.Int32 Newtonsoft.Json.JsonReaderException::<LineNumber>k__BackingField
	int32_t ___U3CLineNumberU3Ek__BackingField_17;
	// System.Int32 Newtonsoft.Json.JsonReaderException::<LinePosition>k__BackingField
	int32_t ___U3CLinePositionU3Ek__BackingField_18;
	// System.String Newtonsoft.Json.JsonReaderException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_U3CLineNumberU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F, ___U3CLineNumberU3Ek__BackingField_17)); }
	inline int32_t get_U3CLineNumberU3Ek__BackingField_17() const { return ___U3CLineNumberU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CLineNumberU3Ek__BackingField_17() { return &___U3CLineNumberU3Ek__BackingField_17; }
	inline void set_U3CLineNumberU3Ek__BackingField_17(int32_t value)
	{
		___U3CLineNumberU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CLinePositionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F, ___U3CLinePositionU3Ek__BackingField_18)); }
	inline int32_t get_U3CLinePositionU3Ek__BackingField_18() const { return ___U3CLinePositionU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CLinePositionU3Ek__BackingField_18() { return &___U3CLinePositionU3Ek__BackingField_18; }
	inline void set_U3CLinePositionU3Ek__BackingField_18(int32_t value)
	{
		___U3CLinePositionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F, ___U3CPathU3Ek__BackingField_19)); }
	inline String_t* get_U3CPathU3Ek__BackingField_19() const { return ___U3CPathU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_19() { return &___U3CPathU3Ek__BackingField_19; }
	inline void set_U3CPathU3Ek__BackingField_19(String_t* value)
	{
		___U3CPathU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADEREXCEPTION_TABAD0FF737611157A789D571AC50787F1FF4FF1F_H
#ifndef JSONSERIALIZATIONEXCEPTION_TB0B6266035536D0449B9F7D1A94E80890286AF16_H
#define JSONSERIALIZATIONEXCEPTION_TB0B6266035536D0449B9F7D1A94E80890286AF16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializationException
struct  JsonSerializationException_tB0B6266035536D0449B9F7D1A94E80890286AF16  : public JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZATIONEXCEPTION_TB0B6266035536D0449B9F7D1A94E80890286AF16_H
#ifndef JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#define JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#ifndef STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#define STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter_State
struct  State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifndef JSONWRITEREXCEPTION_T952E0C802333733F4852F399876F0301BE340A0B_H
#define JSONWRITEREXCEPTION_T952E0C802333733F4852F399876F0301BE340A0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriterException
struct  JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B  : public JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78
{
public:
	// System.String Newtonsoft.Json.JsonWriterException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B, ___U3CPathU3Ek__BackingField_17)); }
	inline String_t* get_U3CPathU3Ek__BackingField_17() const { return ___U3CPathU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_17() { return &___U3CPathU3Ek__BackingField_17; }
	inline void set_U3CPathU3Ek__BackingField_17(String_t* value)
	{
		___U3CPathU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITEREXCEPTION_T952E0C802333733F4852F399876F0301BE340A0B_H
#ifndef MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#define MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#ifndef METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#define METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#ifndef MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#define MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#ifndef NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#define NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifndef OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#define OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifndef PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#define PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#ifndef READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#define READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#ifndef REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#define REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifndef REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#define REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#ifndef STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#define STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifndef TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#define TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifndef WRITESTATE_T08626015A7BC5FAD9FA08295B9184D4402CAC272_H
#define WRITESTATE_T08626015A7BC5FAD9FA08295B9184D4402CAC272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.WriteState
struct  WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272 
{
public:
	// System.Int32 Newtonsoft.Json.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T08626015A7BC5FAD9FA08295B9184D4402CAC272_H
#ifndef FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#define FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#define JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifndef NULLABLE_1_T2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9_H
#define NULLABLE_1_T2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ConstructorHandling>
struct  Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9_H
#ifndef NULLABLE_1_T0AFF4EB4F9100E8542FAE5E9B748C6AB47878902_H
#define NULLABLE_1_T0AFF4EB4F9100E8542FAE5E9B748C6AB47878902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0AFF4EB4F9100E8542FAE5E9B748C6AB47878902_H
#ifndef NULLABLE_1_T828D7B2DA38B853504C533AE33E0F2948573994A_H
#define NULLABLE_1_T828D7B2DA38B853504C533AE33E0F2948573994A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T828D7B2DA38B853504C533AE33E0F2948573994A_H
#ifndef NULLABLE_1_T3612CCC2209E4540E3BECB46C69104D5FBBF03BB_H
#define NULLABLE_1_T3612CCC2209E4540E3BECB46C69104D5FBBF03BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3612CCC2209E4540E3BECB46C69104D5FBBF03BB_H
#ifndef NULLABLE_1_TF4FB621C8DBE38E3A39FED564E870F44A4D8E718_H
#define NULLABLE_1_TF4FB621C8DBE38E3A39FED564E870F44A4D8E718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct  Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TF4FB621C8DBE38E3A39FED564E870F44A4D8E718_H
#ifndef NULLABLE_1_T1AB0BBC00715984FC97B99A7BD21F61F1F102415_H
#define NULLABLE_1_T1AB0BBC00715984FC97B99A7BD21F61F1F102415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1AB0BBC00715984FC97B99A7BD21F61F1F102415_H
#ifndef NULLABLE_1_TE21E9586B51188362D4719A62CF768EF13E54990_H
#define NULLABLE_1_TE21E9586B51188362D4719A62CF768EF13E54990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE21E9586B51188362D4719A62CF768EF13E54990_H
#ifndef NULLABLE_1_T96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70_H
#define NULLABLE_1_T96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70_H
#ifndef NULLABLE_1_TE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4_H
#define NULLABLE_1_TE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>
struct  Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4_H
#ifndef NULLABLE_1_TE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1_H
#define NULLABLE_1_TE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>
struct  Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1_H
#ifndef NULLABLE_1_T03E506703700988207428BB69043E307A2C53E53_H
#define NULLABLE_1_T03E506703700988207428BB69043E307A2C53E53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct  Nullable_1_t03E506703700988207428BB69043E307A2C53E53 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t03E506703700988207428BB69043E307A2C53E53, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t03E506703700988207428BB69043E307A2C53E53, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T03E506703700988207428BB69043E307A2C53E53_H
#ifndef NULLABLE_1_T898BA8195CF65B48365CBABBCEB55A053E136531_H
#define NULLABLE_1_T898BA8195CF65B48365CBABBCEB55A053E136531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct  Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T898BA8195CF65B48365CBABBCEB55A053E136531_H
#ifndef NULLABLE_1_T097635631B84BD8BBAB37D69ED338D7A8B8A2C89_H
#define NULLABLE_1_T097635631B84BD8BBAB37D69ED338D7A8B8A2C89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>
struct  Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T097635631B84BD8BBAB37D69ED338D7A8B8A2C89_H
#ifndef NULLABLE_1_T1C5FD937327E0ADA20126B4AC9A9A84A94847955_H
#define NULLABLE_1_T1C5FD937327E0ADA20126B4AC9A9A84A94847955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1C5FD937327E0ADA20126B4AC9A9A84A94847955_H
#ifndef NULLABLE_1_T3CC1103ACDEB987D97A844292CD0AA18E91DC10E_H
#define NULLABLE_1_T3CC1103ACDEB987D97A844292CD0AA18E91DC10E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Required>
struct  Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3CC1103ACDEB987D97A844292CD0AA18E91DC10E_H
#ifndef NULLABLE_1_T4B852454D2D7DBCB31FC1154137ACFD46DA02BD8_H
#define NULLABLE_1_T4B852454D2D7DBCB31FC1154137ACFD46DA02BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4B852454D2D7DBCB31FC1154137ACFD46DA02BD8_H
#ifndef NULLABLE_1_TA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A_H
#define NULLABLE_1_TA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A_H
#ifndef NULLABLE_1_TCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74_H
#define NULLABLE_1_TCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
struct  Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef JSONCONTAINERATTRIBUTE_T905D5199F79FD8BA5ADCC5C7F738693165D982FA_H
#define JSONCONTAINERATTRIBUTE_T905D5199F79FD8BA5ADCC5C7F738693165D982FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerAttribute
struct  JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type Newtonsoft.Json.JsonContainerAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_0;
	// System.Object[] Newtonsoft.Json.JsonContainerAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CItemConverterParametersU3Ek__BackingField_1;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonContainerAttribute::_isReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____isReference_2;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonContainerAttribute::_itemIsReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____itemIsReference_3;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonContainerAttribute::_itemReferenceLoopHandling
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ____itemReferenceLoopHandling_4;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonContainerAttribute::_itemTypeNameHandling
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ____itemTypeNameHandling_5;

public:
	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA, ___U3CItemConverterTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_0() const { return ___U3CItemConverterTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_0() { return &___U3CItemConverterTypeU3Ek__BackingField_0; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA, ___U3CItemConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CItemConverterParametersU3Ek__BackingField_1() const { return ___U3CItemConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CItemConverterParametersU3Ek__BackingField_1() { return &___U3CItemConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterParametersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__isReference_2() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA, ____isReference_2)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__isReference_2() const { return ____isReference_2; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__isReference_2() { return &____isReference_2; }
	inline void set__isReference_2(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____isReference_2 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_3() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA, ____itemIsReference_3)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__itemIsReference_3() const { return ____itemIsReference_3; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__itemIsReference_3() { return &____itemIsReference_3; }
	inline void set__itemIsReference_3(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____itemIsReference_3 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_4() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA, ____itemReferenceLoopHandling_4)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get__itemReferenceLoopHandling_4() const { return ____itemReferenceLoopHandling_4; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of__itemReferenceLoopHandling_4() { return &____itemReferenceLoopHandling_4; }
	inline void set__itemReferenceLoopHandling_4(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		____itemReferenceLoopHandling_4 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_5() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA, ____itemTypeNameHandling_5)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get__itemTypeNameHandling_5() const { return ____itemTypeNameHandling_5; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of__itemTypeNameHandling_5() { return &____itemTypeNameHandling_5; }
	inline void set__itemTypeNameHandling_5(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		____itemTypeNameHandling_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERATTRIBUTE_T905D5199F79FD8BA5ADCC5C7F738693165D982FA_H
#ifndef JSONPROPERTYATTRIBUTE_TECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A_H
#define JSONPROPERTYATTRIBUTE_TECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPropertyAttribute
struct  JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_nullValueHandling
	Nullable_1_t03E506703700988207428BB69043E307A2C53E53  ____nullValueHandling_0;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_defaultValueHandling
	Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  ____defaultValueHandling_1;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_referenceLoopHandling
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ____referenceLoopHandling_2;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonPropertyAttribute::_objectCreationHandling
	Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  ____objectCreationHandling_3;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_typeNameHandling
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ____typeNameHandling_4;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_isReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____isReference_5;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonPropertyAttribute::_order
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____order_6;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonPropertyAttribute::_required
	Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  ____required_7;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_itemIsReference
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____itemIsReference_8;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemReferenceLoopHandling
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ____itemReferenceLoopHandling_9;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemTypeNameHandling
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ____itemTypeNameHandling_10;
	// System.Type Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_11;
	// System.Object[] Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___U3CItemConverterParametersU3Ek__BackingField_12;
	// System.String Newtonsoft.Json.JsonPropertyAttribute::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of__nullValueHandling_0() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____nullValueHandling_0)); }
	inline Nullable_1_t03E506703700988207428BB69043E307A2C53E53  get__nullValueHandling_0() const { return ____nullValueHandling_0; }
	inline Nullable_1_t03E506703700988207428BB69043E307A2C53E53 * get_address_of__nullValueHandling_0() { return &____nullValueHandling_0; }
	inline void set__nullValueHandling_0(Nullable_1_t03E506703700988207428BB69043E307A2C53E53  value)
	{
		____nullValueHandling_0 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_1() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____defaultValueHandling_1)); }
	inline Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  get__defaultValueHandling_1() const { return ____defaultValueHandling_1; }
	inline Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718 * get_address_of__defaultValueHandling_1() { return &____defaultValueHandling_1; }
	inline void set__defaultValueHandling_1(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  value)
	{
		____defaultValueHandling_1 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_2() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____referenceLoopHandling_2)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get__referenceLoopHandling_2() const { return ____referenceLoopHandling_2; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of__referenceLoopHandling_2() { return &____referenceLoopHandling_2; }
	inline void set__referenceLoopHandling_2(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		____referenceLoopHandling_2 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_3() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____objectCreationHandling_3)); }
	inline Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  get__objectCreationHandling_3() const { return ____objectCreationHandling_3; }
	inline Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531 * get_address_of__objectCreationHandling_3() { return &____objectCreationHandling_3; }
	inline void set__objectCreationHandling_3(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  value)
	{
		____objectCreationHandling_3 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_4() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____typeNameHandling_4)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get__typeNameHandling_4() const { return ____typeNameHandling_4; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of__typeNameHandling_4() { return &____typeNameHandling_4; }
	inline void set__typeNameHandling_4(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		____typeNameHandling_4 = value;
	}

	inline static int32_t get_offset_of__isReference_5() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____isReference_5)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__isReference_5() const { return ____isReference_5; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__isReference_5() { return &____isReference_5; }
	inline void set__isReference_5(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____isReference_5 = value;
	}

	inline static int32_t get_offset_of__order_6() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____order_6)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__order_6() const { return ____order_6; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__order_6() { return &____order_6; }
	inline void set__order_6(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____order_6 = value;
	}

	inline static int32_t get_offset_of__required_7() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____required_7)); }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  get__required_7() const { return ____required_7; }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E * get_address_of__required_7() { return &____required_7; }
	inline void set__required_7(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  value)
	{
		____required_7 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_8() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____itemIsReference_8)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__itemIsReference_8() const { return ____itemIsReference_8; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__itemIsReference_8() { return &____itemIsReference_8; }
	inline void set__itemIsReference_8(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____itemIsReference_8 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_9() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____itemReferenceLoopHandling_9)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get__itemReferenceLoopHandling_9() const { return ____itemReferenceLoopHandling_9; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of__itemReferenceLoopHandling_9() { return &____itemReferenceLoopHandling_9; }
	inline void set__itemReferenceLoopHandling_9(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		____itemReferenceLoopHandling_9 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_10() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ____itemTypeNameHandling_10)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get__itemTypeNameHandling_10() const { return ____itemTypeNameHandling_10; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of__itemTypeNameHandling_10() { return &____itemTypeNameHandling_10; }
	inline void set__itemTypeNameHandling_10(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		____itemTypeNameHandling_10 = value;
	}

	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ___U3CItemConverterTypeU3Ek__BackingField_11)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_11() const { return ___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_11() { return &___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_11(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterTypeU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ___U3CItemConverterParametersU3Ek__BackingField_12)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_U3CItemConverterParametersU3Ek__BackingField_12() const { return ___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_U3CItemConverterParametersU3Ek__BackingField_12() { return &___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_12(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterParametersU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A, ___U3CPropertyNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_13() const { return ___U3CPropertyNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_13() { return &___U3CPropertyNameU3Ek__BackingField_13; }
	inline void set_U3CPropertyNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyNameU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYATTRIBUTE_TECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A_H
#ifndef JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#define JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader_State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____currentPosition_4)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____culture_5)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____maxDepth_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____stack_12)); }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * get__stack_12() const { return ____stack_12; }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#ifndef JSONSERIALIZER_T22D7DE9C2207B3F59CE86171E843ADA94B898623_H
#define JSONSERIALIZER_T22D7DE9C2207B3F59CE86171E843ADA94B898623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormat
	int32_t ____typeNameAssemblyFormat_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::_binder
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ____binder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____typeNameAssemblyFormat_1)); }
	inline int32_t get__typeNameAssemblyFormat_1() const { return ____typeNameAssemblyFormat_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormat_1() { return &____typeNameAssemblyFormat_1; }
	inline void set__typeNameAssemblyFormat_1(int32_t value)
	{
		____typeNameAssemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____converters_10)); }
	inline JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__binder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____binder_14)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get__binder_14() const { return ____binder_14; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of__binder_14() { return &____binder_14; }
	inline void set__binder_14(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		____binder_14 = value;
		Il2CppCodeGenWriteBarrier((&____binder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____context_15)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get__context_15() const { return ____context_15; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____formatting_17)); }
	inline Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateFormatHandling_18)); }
	inline Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateParseHandling_20)); }
	inline Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____floatFormatHandling_21)); }
	inline Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____floatParseHandling_22)); }
	inline Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____stringEscapeHandling_23)); }
	inline Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8 * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____culture_24)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____maxDepth_25)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____checkAdditionalContent_27)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ___Error_30)); }
	inline EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T22D7DE9C2207B3F59CE86171E843ADA94B898623_H
#ifndef JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#define JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter_State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stack_2)); }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentPosition_3)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifndef NULLABLE_1_TCF22087D771FCA8102A51C976A8992F7F8AE27B8_H
#define NULLABLE_1_TCF22087D771FCA8102A51C976A8992F7F8AE27B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Runtime.Serialization.StreamingContext>
struct  Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8 
{
public:
	// T System.Nullable`1::value
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8, ___value_0)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get_value_0() const { return ___value_0; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCF22087D771FCA8102A51C976A8992F7F8AE27B8_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef JSONARRAYATTRIBUTE_TF8360DD00442DA4453BBAC4E758E4B53DD8799B3_H
#define JSONARRAYATTRIBUTE_TF8360DD00442DA4453BBAC4E758E4B53DD8799B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonArrayAttribute
struct  JsonArrayAttribute_tF8360DD00442DA4453BBAC4E758E4B53DD8799B3  : public JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYATTRIBUTE_TF8360DD00442DA4453BBAC4E758E4B53DD8799B3_H
#ifndef JSONDICTIONARYATTRIBUTE_T0E0A3CF6D5AE30ABF7BB2C29ABC450B7160C4AB5_H
#define JSONDICTIONARYATTRIBUTE_T0E0A3CF6D5AE30ABF7BB2C29ABC450B7160C4AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonDictionaryAttribute
struct  JsonDictionaryAttribute_t0E0A3CF6D5AE30ABF7BB2C29ABC450B7160C4AB5  : public JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYATTRIBUTE_T0E0A3CF6D5AE30ABF7BB2C29ABC450B7160C4AB5_H
#ifndef JSONOBJECTATTRIBUTE_T6C4FB3D7397FBADA233AD510E5235AD53B20E534_H
#define JSONOBJECTATTRIBUTE_T6C4FB3D7397FBADA233AD510E5235AD53B20E534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonObjectAttribute
struct  JsonObjectAttribute_t6C4FB3D7397FBADA233AD510E5235AD53B20E534  : public JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::_memberSerialization
	int32_t ____memberSerialization_6;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonObjectAttribute::_itemRequired
	Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  ____itemRequired_7;

public:
	inline static int32_t get_offset_of__memberSerialization_6() { return static_cast<int32_t>(offsetof(JsonObjectAttribute_t6C4FB3D7397FBADA233AD510E5235AD53B20E534, ____memberSerialization_6)); }
	inline int32_t get__memberSerialization_6() const { return ____memberSerialization_6; }
	inline int32_t* get_address_of__memberSerialization_6() { return &____memberSerialization_6; }
	inline void set__memberSerialization_6(int32_t value)
	{
		____memberSerialization_6 = value;
	}

	inline static int32_t get_offset_of__itemRequired_7() { return static_cast<int32_t>(offsetof(JsonObjectAttribute_t6C4FB3D7397FBADA233AD510E5235AD53B20E534, ____itemRequired_7)); }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  get__itemRequired_7() const { return ____itemRequired_7; }
	inline Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E * get_address_of__itemRequired_7() { return &____itemRequired_7; }
	inline void set__itemRequired_7(Nullable_1_t3CC1103ACDEB987D97A844292CD0AA18E91DC10E  value)
	{
		____itemRequired_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTATTRIBUTE_T6C4FB3D7397FBADA233AD510E5235AD53B20E534_H
#ifndef JSONSERIALIZERSETTINGS_T6F615B4300C850B2B1FEFEBAC288DAC7D714229C_H
#define JSONSERIALIZERSETTINGS_T6F615B4300C850B2B1FEFEBAC288DAC7D714229C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializerSettings
struct  JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C  : public RuntimeObject
{
public:
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializerSettings::_formatting
	Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  ____formatting_2;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializerSettings::_dateFormatHandling
	Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  ____dateFormatHandling_3;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializerSettings::_dateTimeZoneHandling
	Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  ____dateTimeZoneHandling_4;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializerSettings::_dateParseHandling
	Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  ____dateParseHandling_5;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializerSettings::_floatFormatHandling
	Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  ____floatFormatHandling_6;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializerSettings::_floatParseHandling
	Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  ____floatParseHandling_7;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializerSettings::_stringEscapeHandling
	Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  ____stringEscapeHandling_8;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializerSettings::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_9;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializerSettings::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_10;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializerSettings::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_11;
	// System.Boolean Newtonsoft.Json.JsonSerializerSettings::_maxDepthSet
	bool ____maxDepthSet_12;
	// System.String Newtonsoft.Json.JsonSerializerSettings::_dateFormatString
	String_t* ____dateFormatString_13;
	// System.Boolean Newtonsoft.Json.JsonSerializerSettings::_dateFormatStringSet
	bool ____dateFormatStringSet_14;
	// System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle> Newtonsoft.Json.JsonSerializerSettings::_typeNameAssemblyFormat
	Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74  ____typeNameAssemblyFormat_15;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonSerializerSettings::_defaultValueHandling
	Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  ____defaultValueHandling_16;
	// System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling> Newtonsoft.Json.JsonSerializerSettings::_preserveReferencesHandling
	Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89  ____preserveReferencesHandling_17;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonSerializerSettings::_nullValueHandling
	Nullable_1_t03E506703700988207428BB69043E307A2C53E53  ____nullValueHandling_18;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonSerializerSettings::_objectCreationHandling
	Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  ____objectCreationHandling_19;
	// System.Nullable`1<Newtonsoft.Json.MissingMemberHandling> Newtonsoft.Json.JsonSerializerSettings::_missingMemberHandling
	Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1  ____missingMemberHandling_20;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonSerializerSettings::_referenceLoopHandling
	Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  ____referenceLoopHandling_21;
	// System.Nullable`1<System.Runtime.Serialization.StreamingContext> Newtonsoft.Json.JsonSerializerSettings::_context
	Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8  ____context_22;
	// System.Nullable`1<Newtonsoft.Json.ConstructorHandling> Newtonsoft.Json.JsonSerializerSettings::_constructorHandling
	Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9  ____constructorHandling_23;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonSerializerSettings::_typeNameHandling
	Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  ____typeNameHandling_24;
	// System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling> Newtonsoft.Json.JsonSerializerSettings::_metadataPropertyHandling
	Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4  ____metadataPropertyHandling_25;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::<Converters>k__BackingField
	RuntimeObject* ___U3CConvertersU3Ek__BackingField_26;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::<ContractResolver>k__BackingField
	RuntimeObject* ___U3CContractResolverU3Ek__BackingField_27;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::<EqualityComparer>k__BackingField
	RuntimeObject* ___U3CEqualityComparerU3Ek__BackingField_28;
	// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::<ReferenceResolverProvider>k__BackingField
	Func_1_t861F98E547FCB9AB51D52E49717586C4ABB37685 * ___U3CReferenceResolverProviderU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::<TraceWriter>k__BackingField
	RuntimeObject* ___U3CTraceWriterU3Ek__BackingField_30;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::<Binder>k__BackingField
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ___U3CBinderU3Ek__BackingField_31;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::<Error>k__BackingField
	EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * ___U3CErrorU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__formatting_2() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____formatting_2)); }
	inline Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  get__formatting_2() const { return ____formatting_2; }
	inline Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70 * get_address_of__formatting_2() { return &____formatting_2; }
	inline void set__formatting_2(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  value)
	{
		____formatting_2 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____dateFormatHandling_3)); }
	inline Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  get__dateFormatHandling_3() const { return ____dateFormatHandling_3; }
	inline Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902 * get_address_of__dateFormatHandling_3() { return &____dateFormatHandling_3; }
	inline void set__dateFormatHandling_3(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  value)
	{
		____dateFormatHandling_3 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____dateTimeZoneHandling_4)); }
	inline Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  get__dateTimeZoneHandling_4() const { return ____dateTimeZoneHandling_4; }
	inline Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB * get_address_of__dateTimeZoneHandling_4() { return &____dateTimeZoneHandling_4; }
	inline void set__dateTimeZoneHandling_4(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  value)
	{
		____dateTimeZoneHandling_4 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____dateParseHandling_5)); }
	inline Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  get__dateParseHandling_5() const { return ____dateParseHandling_5; }
	inline Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A * get_address_of__dateParseHandling_5() { return &____dateParseHandling_5; }
	inline void set__dateParseHandling_5(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  value)
	{
		____dateParseHandling_5 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____floatFormatHandling_6)); }
	inline Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  get__floatFormatHandling_6() const { return ____floatFormatHandling_6; }
	inline Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415 * get_address_of__floatFormatHandling_6() { return &____floatFormatHandling_6; }
	inline void set__floatFormatHandling_6(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  value)
	{
		____floatFormatHandling_6 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____floatParseHandling_7)); }
	inline Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  get__floatParseHandling_7() const { return ____floatParseHandling_7; }
	inline Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990 * get_address_of__floatParseHandling_7() { return &____floatParseHandling_7; }
	inline void set__floatParseHandling_7(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  value)
	{
		____floatParseHandling_7 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____stringEscapeHandling_8)); }
	inline Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  get__stringEscapeHandling_8() const { return ____stringEscapeHandling_8; }
	inline Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8 * get_address_of__stringEscapeHandling_8() { return &____stringEscapeHandling_8; }
	inline void set__stringEscapeHandling_8(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  value)
	{
		____stringEscapeHandling_8 = value;
	}

	inline static int32_t get_offset_of__culture_9() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____culture_9)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_9() const { return ____culture_9; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_9() { return &____culture_9; }
	inline void set__culture_9(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_9 = value;
		Il2CppCodeGenWriteBarrier((&____culture_9), value);
	}

	inline static int32_t get_offset_of__checkAdditionalContent_10() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____checkAdditionalContent_10)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_10() const { return ____checkAdditionalContent_10; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_10() { return &____checkAdditionalContent_10; }
	inline void set__checkAdditionalContent_10(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_10 = value;
	}

	inline static int32_t get_offset_of__maxDepth_11() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____maxDepth_11)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_11() const { return ____maxDepth_11; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_11() { return &____maxDepth_11; }
	inline void set__maxDepth_11(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_11 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_12() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____maxDepthSet_12)); }
	inline bool get__maxDepthSet_12() const { return ____maxDepthSet_12; }
	inline bool* get_address_of__maxDepthSet_12() { return &____maxDepthSet_12; }
	inline void set__maxDepthSet_12(bool value)
	{
		____maxDepthSet_12 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_13() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____dateFormatString_13)); }
	inline String_t* get__dateFormatString_13() const { return ____dateFormatString_13; }
	inline String_t** get_address_of__dateFormatString_13() { return &____dateFormatString_13; }
	inline void set__dateFormatString_13(String_t* value)
	{
		____dateFormatString_13 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_13), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_14() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____dateFormatStringSet_14)); }
	inline bool get__dateFormatStringSet_14() const { return ____dateFormatStringSet_14; }
	inline bool* get_address_of__dateFormatStringSet_14() { return &____dateFormatStringSet_14; }
	inline void set__dateFormatStringSet_14(bool value)
	{
		____dateFormatStringSet_14 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_15() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____typeNameAssemblyFormat_15)); }
	inline Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74  get__typeNameAssemblyFormat_15() const { return ____typeNameAssemblyFormat_15; }
	inline Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74 * get_address_of__typeNameAssemblyFormat_15() { return &____typeNameAssemblyFormat_15; }
	inline void set__typeNameAssemblyFormat_15(Nullable_1_tCB46F26D22ECB6D3D6669A33E2A9871ACEA72F74  value)
	{
		____typeNameAssemblyFormat_15 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_16() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____defaultValueHandling_16)); }
	inline Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  get__defaultValueHandling_16() const { return ____defaultValueHandling_16; }
	inline Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718 * get_address_of__defaultValueHandling_16() { return &____defaultValueHandling_16; }
	inline void set__defaultValueHandling_16(Nullable_1_tF4FB621C8DBE38E3A39FED564E870F44A4D8E718  value)
	{
		____defaultValueHandling_16 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_17() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____preserveReferencesHandling_17)); }
	inline Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89  get__preserveReferencesHandling_17() const { return ____preserveReferencesHandling_17; }
	inline Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89 * get_address_of__preserveReferencesHandling_17() { return &____preserveReferencesHandling_17; }
	inline void set__preserveReferencesHandling_17(Nullable_1_t097635631B84BD8BBAB37D69ED338D7A8B8A2C89  value)
	{
		____preserveReferencesHandling_17 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____nullValueHandling_18)); }
	inline Nullable_1_t03E506703700988207428BB69043E307A2C53E53  get__nullValueHandling_18() const { return ____nullValueHandling_18; }
	inline Nullable_1_t03E506703700988207428BB69043E307A2C53E53 * get_address_of__nullValueHandling_18() { return &____nullValueHandling_18; }
	inline void set__nullValueHandling_18(Nullable_1_t03E506703700988207428BB69043E307A2C53E53  value)
	{
		____nullValueHandling_18 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____objectCreationHandling_19)); }
	inline Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  get__objectCreationHandling_19() const { return ____objectCreationHandling_19; }
	inline Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531 * get_address_of__objectCreationHandling_19() { return &____objectCreationHandling_19; }
	inline void set__objectCreationHandling_19(Nullable_1_t898BA8195CF65B48365CBABBCEB55A053E136531  value)
	{
		____objectCreationHandling_19 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____missingMemberHandling_20)); }
	inline Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1  get__missingMemberHandling_20() const { return ____missingMemberHandling_20; }
	inline Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1 * get_address_of__missingMemberHandling_20() { return &____missingMemberHandling_20; }
	inline void set__missingMemberHandling_20(Nullable_1_tE0CCCDEA37127B6A61A1FB79CD3DD3BF16066DF1  value)
	{
		____missingMemberHandling_20 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____referenceLoopHandling_21)); }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  get__referenceLoopHandling_21() const { return ____referenceLoopHandling_21; }
	inline Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955 * get_address_of__referenceLoopHandling_21() { return &____referenceLoopHandling_21; }
	inline void set__referenceLoopHandling_21(Nullable_1_t1C5FD937327E0ADA20126B4AC9A9A84A94847955  value)
	{
		____referenceLoopHandling_21 = value;
	}

	inline static int32_t get_offset_of__context_22() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____context_22)); }
	inline Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8  get__context_22() const { return ____context_22; }
	inline Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8 * get_address_of__context_22() { return &____context_22; }
	inline void set__context_22(Nullable_1_tCF22087D771FCA8102A51C976A8992F7F8AE27B8  value)
	{
		____context_22 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____constructorHandling_23)); }
	inline Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9  get__constructorHandling_23() const { return ____constructorHandling_23; }
	inline Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9 * get_address_of__constructorHandling_23() { return &____constructorHandling_23; }
	inline void set__constructorHandling_23(Nullable_1_t2FF3B6D017132FC892A03BD6D3452BC2ADF1AFB9  value)
	{
		____constructorHandling_23 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_24() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____typeNameHandling_24)); }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  get__typeNameHandling_24() const { return ____typeNameHandling_24; }
	inline Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A * get_address_of__typeNameHandling_24() { return &____typeNameHandling_24; }
	inline void set__typeNameHandling_24(Nullable_1_tA34EC33C1F20385824BB800FF9BF9EB7F7E79E9A  value)
	{
		____typeNameHandling_24 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_25() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ____metadataPropertyHandling_25)); }
	inline Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4  get__metadataPropertyHandling_25() const { return ____metadataPropertyHandling_25; }
	inline Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4 * get_address_of__metadataPropertyHandling_25() { return &____metadataPropertyHandling_25; }
	inline void set__metadataPropertyHandling_25(Nullable_1_tE21EA6E51DEB8AF808A0A7FA344E6AA93878EEA4  value)
	{
		____metadataPropertyHandling_25 = value;
	}

	inline static int32_t get_offset_of_U3CConvertersU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CConvertersU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CConvertersU3Ek__BackingField_26() const { return ___U3CConvertersU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CConvertersU3Ek__BackingField_26() { return &___U3CConvertersU3Ek__BackingField_26; }
	inline void set_U3CConvertersU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CConvertersU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConvertersU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CContractResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CContractResolverU3Ek__BackingField_27)); }
	inline RuntimeObject* get_U3CContractResolverU3Ek__BackingField_27() const { return ___U3CContractResolverU3Ek__BackingField_27; }
	inline RuntimeObject** get_address_of_U3CContractResolverU3Ek__BackingField_27() { return &___U3CContractResolverU3Ek__BackingField_27; }
	inline void set_U3CContractResolverU3Ek__BackingField_27(RuntimeObject* value)
	{
		___U3CContractResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CEqualityComparerU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CEqualityComparerU3Ek__BackingField_28)); }
	inline RuntimeObject* get_U3CEqualityComparerU3Ek__BackingField_28() const { return ___U3CEqualityComparerU3Ek__BackingField_28; }
	inline RuntimeObject** get_address_of_U3CEqualityComparerU3Ek__BackingField_28() { return &___U3CEqualityComparerU3Ek__BackingField_28; }
	inline void set_U3CEqualityComparerU3Ek__BackingField_28(RuntimeObject* value)
	{
		___U3CEqualityComparerU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEqualityComparerU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CReferenceResolverProviderU3Ek__BackingField_29)); }
	inline Func_1_t861F98E547FCB9AB51D52E49717586C4ABB37685 * get_U3CReferenceResolverProviderU3Ek__BackingField_29() const { return ___U3CReferenceResolverProviderU3Ek__BackingField_29; }
	inline Func_1_t861F98E547FCB9AB51D52E49717586C4ABB37685 ** get_address_of_U3CReferenceResolverProviderU3Ek__BackingField_29() { return &___U3CReferenceResolverProviderU3Ek__BackingField_29; }
	inline void set_U3CReferenceResolverProviderU3Ek__BackingField_29(Func_1_t861F98E547FCB9AB51D52E49717586C4ABB37685 * value)
	{
		___U3CReferenceResolverProviderU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReferenceResolverProviderU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CTraceWriterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CTraceWriterU3Ek__BackingField_30)); }
	inline RuntimeObject* get_U3CTraceWriterU3Ek__BackingField_30() const { return ___U3CTraceWriterU3Ek__BackingField_30; }
	inline RuntimeObject** get_address_of_U3CTraceWriterU3Ek__BackingField_30() { return &___U3CTraceWriterU3Ek__BackingField_30; }
	inline void set_U3CTraceWriterU3Ek__BackingField_30(RuntimeObject* value)
	{
		___U3CTraceWriterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTraceWriterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CBinderU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CBinderU3Ek__BackingField_31)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get_U3CBinderU3Ek__BackingField_31() const { return ___U3CBinderU3Ek__BackingField_31; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of_U3CBinderU3Ek__BackingField_31() { return &___U3CBinderU3Ek__BackingField_31; }
	inline void set_U3CBinderU3Ek__BackingField_31(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		___U3CBinderU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBinderU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C, ___U3CErrorU3Ek__BackingField_32)); }
	inline EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * get_U3CErrorU3Ek__BackingField_32() const { return ___U3CErrorU3Ek__BackingField_32; }
	inline EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 ** get_address_of_U3CErrorU3Ek__BackingField_32() { return &___U3CErrorU3Ek__BackingField_32; }
	inline void set_U3CErrorU3Ek__BackingField_32(EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * value)
	{
		___U3CErrorU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_32), value);
	}
};

struct JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C_StaticFields
{
public:
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::DefaultContext
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ___DefaultContext_0;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializerSettings::DefaultCulture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___DefaultCulture_1;

public:
	inline static int32_t get_offset_of_DefaultContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C_StaticFields, ___DefaultContext_0)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get_DefaultContext_0() const { return ___DefaultContext_0; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of_DefaultContext_0() { return &___DefaultContext_0; }
	inline void set_DefaultContext_0(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		___DefaultContext_0 = value;
	}

	inline static int32_t get_offset_of_DefaultCulture_1() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C_StaticFields, ___DefaultCulture_1)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_DefaultCulture_1() const { return ___DefaultCulture_1; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_DefaultCulture_1() { return &___DefaultCulture_1; }
	inline void set_DefaultCulture_1(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___DefaultCulture_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultCulture_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERSETTINGS_T6F615B4300C850B2B1FEFEBAC288DAC7D714229C_H
#ifndef JSONTEXTREADER_T6A7D574DD2106436ACFC19E372AB9138EF3CC513_H
#define JSONTEXTREADER_T6A7D574DD2106436ACFC19E372AB9138EF3CC513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextReader
struct  JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513  : public JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636
{
public:
	// System.IO.TextReader Newtonsoft.Json.JsonTextReader::_reader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ____reader_15;
	// System.Char[] Newtonsoft.Json.JsonTextReader::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_16;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_charsUsed
	int32_t ____charsUsed_17;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_charPos
	int32_t ____charPos_18;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_lineStartPos
	int32_t ____lineStartPos_19;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_lineNumber
	int32_t ____lineNumber_20;
	// System.Boolean Newtonsoft.Json.JsonTextReader::_isEndOfFile
	bool ____isEndOfFile_21;
	// Newtonsoft.Json.Utilities.StringBuffer Newtonsoft.Json.JsonTextReader::_stringBuffer
	StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4  ____stringBuffer_22;
	// Newtonsoft.Json.Utilities.StringReference Newtonsoft.Json.JsonTextReader::_stringReference
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9  ____stringReference_23;
	// Newtonsoft.Json.IArrayPool`1<System.Char> Newtonsoft.Json.JsonTextReader::_arrayPool
	RuntimeObject* ____arrayPool_24;
	// Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.JsonTextReader::NameTable
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C * ___NameTable_25;

public:
	inline static int32_t get_offset_of__reader_15() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____reader_15)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get__reader_15() const { return ____reader_15; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of__reader_15() { return &____reader_15; }
	inline void set__reader_15(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		____reader_15 = value;
		Il2CppCodeGenWriteBarrier((&____reader_15), value);
	}

	inline static int32_t get_offset_of__chars_16() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____chars_16)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_16() const { return ____chars_16; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_16() { return &____chars_16; }
	inline void set__chars_16(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_16 = value;
		Il2CppCodeGenWriteBarrier((&____chars_16), value);
	}

	inline static int32_t get_offset_of__charsUsed_17() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____charsUsed_17)); }
	inline int32_t get__charsUsed_17() const { return ____charsUsed_17; }
	inline int32_t* get_address_of__charsUsed_17() { return &____charsUsed_17; }
	inline void set__charsUsed_17(int32_t value)
	{
		____charsUsed_17 = value;
	}

	inline static int32_t get_offset_of__charPos_18() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____charPos_18)); }
	inline int32_t get__charPos_18() const { return ____charPos_18; }
	inline int32_t* get_address_of__charPos_18() { return &____charPos_18; }
	inline void set__charPos_18(int32_t value)
	{
		____charPos_18 = value;
	}

	inline static int32_t get_offset_of__lineStartPos_19() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____lineStartPos_19)); }
	inline int32_t get__lineStartPos_19() const { return ____lineStartPos_19; }
	inline int32_t* get_address_of__lineStartPos_19() { return &____lineStartPos_19; }
	inline void set__lineStartPos_19(int32_t value)
	{
		____lineStartPos_19 = value;
	}

	inline static int32_t get_offset_of__lineNumber_20() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____lineNumber_20)); }
	inline int32_t get__lineNumber_20() const { return ____lineNumber_20; }
	inline int32_t* get_address_of__lineNumber_20() { return &____lineNumber_20; }
	inline void set__lineNumber_20(int32_t value)
	{
		____lineNumber_20 = value;
	}

	inline static int32_t get_offset_of__isEndOfFile_21() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____isEndOfFile_21)); }
	inline bool get__isEndOfFile_21() const { return ____isEndOfFile_21; }
	inline bool* get_address_of__isEndOfFile_21() { return &____isEndOfFile_21; }
	inline void set__isEndOfFile_21(bool value)
	{
		____isEndOfFile_21 = value;
	}

	inline static int32_t get_offset_of__stringBuffer_22() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____stringBuffer_22)); }
	inline StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4  get__stringBuffer_22() const { return ____stringBuffer_22; }
	inline StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4 * get_address_of__stringBuffer_22() { return &____stringBuffer_22; }
	inline void set__stringBuffer_22(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4  value)
	{
		____stringBuffer_22 = value;
	}

	inline static int32_t get_offset_of__stringReference_23() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____stringReference_23)); }
	inline StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9  get__stringReference_23() const { return ____stringReference_23; }
	inline StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9 * get_address_of__stringReference_23() { return &____stringReference_23; }
	inline void set__stringReference_23(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9  value)
	{
		____stringReference_23 = value;
	}

	inline static int32_t get_offset_of__arrayPool_24() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ____arrayPool_24)); }
	inline RuntimeObject* get__arrayPool_24() const { return ____arrayPool_24; }
	inline RuntimeObject** get_address_of__arrayPool_24() { return &____arrayPool_24; }
	inline void set__arrayPool_24(RuntimeObject* value)
	{
		____arrayPool_24 = value;
		Il2CppCodeGenWriteBarrier((&____arrayPool_24), value);
	}

	inline static int32_t get_offset_of_NameTable_25() { return static_cast<int32_t>(offsetof(JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513, ___NameTable_25)); }
	inline PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C * get_NameTable_25() const { return ___NameTable_25; }
	inline PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C ** get_address_of_NameTable_25() { return &___NameTable_25; }
	inline void set_NameTable_25(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C * value)
	{
		___NameTable_25 = value;
		Il2CppCodeGenWriteBarrier((&___NameTable_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTREADER_T6A7D574DD2106436ACFC19E372AB9138EF3CC513_H
#ifndef JSONTEXTWRITER_T44179CD34465F52DCBDDA019383D527CD84CFF01_H
#define JSONTEXTWRITER_T44179CD34465F52DCBDDA019383D527CD84CFF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextWriter
struct  JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01  : public JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76
{
public:
	// System.IO.TextWriter Newtonsoft.Json.JsonTextWriter::_writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ____writer_13;
	// Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::_base64Encoder
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB * ____base64Encoder_14;
	// System.Char Newtonsoft.Json.JsonTextWriter::_indentChar
	Il2CppChar ____indentChar_15;
	// System.Int32 Newtonsoft.Json.JsonTextWriter::_indentation
	int32_t ____indentation_16;
	// System.Char Newtonsoft.Json.JsonTextWriter::_quoteChar
	Il2CppChar ____quoteChar_17;
	// System.Boolean Newtonsoft.Json.JsonTextWriter::_quoteName
	bool ____quoteName_18;
	// System.Boolean[] Newtonsoft.Json.JsonTextWriter::_charEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____charEscapeFlags_19;
	// System.Char[] Newtonsoft.Json.JsonTextWriter::_writeBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____writeBuffer_20;
	// Newtonsoft.Json.IArrayPool`1<System.Char> Newtonsoft.Json.JsonTextWriter::_arrayPool
	RuntimeObject* ____arrayPool_21;
	// System.Char[] Newtonsoft.Json.JsonTextWriter::_indentChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____indentChars_22;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____writer_13)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get__writer_13() const { return ____writer_13; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__base64Encoder_14() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____base64Encoder_14)); }
	inline Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB * get__base64Encoder_14() const { return ____base64Encoder_14; }
	inline Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB ** get_address_of__base64Encoder_14() { return &____base64Encoder_14; }
	inline void set__base64Encoder_14(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB * value)
	{
		____base64Encoder_14 = value;
		Il2CppCodeGenWriteBarrier((&____base64Encoder_14), value);
	}

	inline static int32_t get_offset_of__indentChar_15() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____indentChar_15)); }
	inline Il2CppChar get__indentChar_15() const { return ____indentChar_15; }
	inline Il2CppChar* get_address_of__indentChar_15() { return &____indentChar_15; }
	inline void set__indentChar_15(Il2CppChar value)
	{
		____indentChar_15 = value;
	}

	inline static int32_t get_offset_of__indentation_16() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____indentation_16)); }
	inline int32_t get__indentation_16() const { return ____indentation_16; }
	inline int32_t* get_address_of__indentation_16() { return &____indentation_16; }
	inline void set__indentation_16(int32_t value)
	{
		____indentation_16 = value;
	}

	inline static int32_t get_offset_of__quoteChar_17() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____quoteChar_17)); }
	inline Il2CppChar get__quoteChar_17() const { return ____quoteChar_17; }
	inline Il2CppChar* get_address_of__quoteChar_17() { return &____quoteChar_17; }
	inline void set__quoteChar_17(Il2CppChar value)
	{
		____quoteChar_17 = value;
	}

	inline static int32_t get_offset_of__quoteName_18() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____quoteName_18)); }
	inline bool get__quoteName_18() const { return ____quoteName_18; }
	inline bool* get_address_of__quoteName_18() { return &____quoteName_18; }
	inline void set__quoteName_18(bool value)
	{
		____quoteName_18 = value;
	}

	inline static int32_t get_offset_of__charEscapeFlags_19() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____charEscapeFlags_19)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__charEscapeFlags_19() const { return ____charEscapeFlags_19; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__charEscapeFlags_19() { return &____charEscapeFlags_19; }
	inline void set__charEscapeFlags_19(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____charEscapeFlags_19 = value;
		Il2CppCodeGenWriteBarrier((&____charEscapeFlags_19), value);
	}

	inline static int32_t get_offset_of__writeBuffer_20() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____writeBuffer_20)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__writeBuffer_20() const { return ____writeBuffer_20; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__writeBuffer_20() { return &____writeBuffer_20; }
	inline void set__writeBuffer_20(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____writeBuffer_20 = value;
		Il2CppCodeGenWriteBarrier((&____writeBuffer_20), value);
	}

	inline static int32_t get_offset_of__arrayPool_21() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____arrayPool_21)); }
	inline RuntimeObject* get__arrayPool_21() const { return ____arrayPool_21; }
	inline RuntimeObject** get_address_of__arrayPool_21() { return &____arrayPool_21; }
	inline void set__arrayPool_21(RuntimeObject* value)
	{
		____arrayPool_21 = value;
		Il2CppCodeGenWriteBarrier((&____arrayPool_21), value);
	}

	inline static int32_t get_offset_of__indentChars_22() { return static_cast<int32_t>(offsetof(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01, ____indentChars_22)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__indentChars_22() const { return ____indentChars_22; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__indentChars_22() { return &____indentChars_22; }
	inline void set__indentChars_22(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____indentChars_22 = value;
		Il2CppCodeGenWriteBarrier((&____indentChars_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTWRITER_T44179CD34465F52DCBDDA019383D527CD84CFF01_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CACHE_TF8C173B605923CFED15F5EDA80A6F57C09D819F6_H
#define CACHE_TF8C173B605923CFED15F5EDA80A6F57C09D819F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.Components.Cache
struct  Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.Components.Cache::_count
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____count_4;
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.Components.Cache::_size
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____size_5;
	// UnityEngine.UI.Button BestHTTP.Examples.Helpers.Components.Cache::_clear
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____clear_6;

public:
	inline static int32_t get_offset_of__count_4() { return static_cast<int32_t>(offsetof(Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6, ____count_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__count_4() const { return ____count_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__count_4() { return &____count_4; }
	inline void set__count_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____count_4 = value;
		Il2CppCodeGenWriteBarrier((&____count_4), value);
	}

	inline static int32_t get_offset_of__size_5() { return static_cast<int32_t>(offsetof(Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6, ____size_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__size_5() const { return ____size_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__size_5() { return &____size_5; }
	inline void set__size_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____size_5 = value;
		Il2CppCodeGenWriteBarrier((&____size_5), value);
	}

	inline static int32_t get_offset_of__clear_6() { return static_cast<int32_t>(offsetof(Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6, ____clear_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__clear_6() const { return ____clear_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__clear_6() { return &____clear_6; }
	inline void set__clear_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____clear_6 = value;
		Il2CppCodeGenWriteBarrier((&____clear_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHE_TF8C173B605923CFED15F5EDA80A6F57C09D819F6_H
#ifndef COOKIES_TAAB760BE054E11D332D31BAE518F8AA55360DA49_H
#define COOKIES_TAAB760BE054E11D332D31BAE518F8AA55360DA49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.Components.Cookies
struct  Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.Components.Cookies::_count
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____count_4;
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.Components.Cookies::_size
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____size_5;
	// UnityEngine.UI.Button BestHTTP.Examples.Helpers.Components.Cookies::_clear
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____clear_6;

public:
	inline static int32_t get_offset_of__count_4() { return static_cast<int32_t>(offsetof(Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49, ____count_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__count_4() const { return ____count_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__count_4() { return &____count_4; }
	inline void set__count_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____count_4 = value;
		Il2CppCodeGenWriteBarrier((&____count_4), value);
	}

	inline static int32_t get_offset_of__size_5() { return static_cast<int32_t>(offsetof(Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49, ____size_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__size_5() const { return ____size_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__size_5() { return &____size_5; }
	inline void set__size_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____size_5 = value;
		Il2CppCodeGenWriteBarrier((&____size_5), value);
	}

	inline static int32_t get_offset_of__clear_6() { return static_cast<int32_t>(offsetof(Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49, ____clear_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__clear_6() const { return ____clear_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__clear_6() { return &____clear_6; }
	inline void set__clear_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____clear_6 = value;
		Il2CppCodeGenWriteBarrier((&____clear_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIES_TAAB760BE054E11D332D31BAE518F8AA55360DA49_H
#ifndef CATEGORY_T8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477_H
#define CATEGORY_T8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.SelectorUI.Category
struct  Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.SelectorUI.Category::_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____text_4;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477, ____text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__text_4() const { return ____text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORY_T8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477_H
#ifndef EXAMPLEINFO_T092897DDF7419CA98579EA1A0A9451539BF301E8_H
#define EXAMPLEINFO_T092897DDF7419CA98579EA1A0A9451539BF301E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo
struct  ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo::_header
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____header_4;
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo::_description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____description_5;
	// BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo::_parentUI
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A * ____parentUI_6;
	// BestHTTP.Examples.Helpers.SampleBase BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo::_example
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * ____example_7;

public:
	inline static int32_t get_offset_of__header_4() { return static_cast<int32_t>(offsetof(ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8, ____header_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__header_4() const { return ____header_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__header_4() { return &____header_4; }
	inline void set__header_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____header_4 = value;
		Il2CppCodeGenWriteBarrier((&____header_4), value);
	}

	inline static int32_t get_offset_of__description_5() { return static_cast<int32_t>(offsetof(ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8, ____description_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__description_5() const { return ____description_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__description_5() { return &____description_5; }
	inline void set__description_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____description_5 = value;
		Il2CppCodeGenWriteBarrier((&____description_5), value);
	}

	inline static int32_t get_offset_of__parentUI_6() { return static_cast<int32_t>(offsetof(ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8, ____parentUI_6)); }
	inline SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A * get__parentUI_6() const { return ____parentUI_6; }
	inline SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A ** get_address_of__parentUI_6() { return &____parentUI_6; }
	inline void set__parentUI_6(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A * value)
	{
		____parentUI_6 = value;
		Il2CppCodeGenWriteBarrier((&____parentUI_6), value);
	}

	inline static int32_t get_offset_of__example_7() { return static_cast<int32_t>(offsetof(ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8, ____example_7)); }
	inline SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * get__example_7() const { return ____example_7; }
	inline SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF ** get_address_of__example_7() { return &____example_7; }
	inline void set__example_7(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * value)
	{
		____example_7 = value;
		Il2CppCodeGenWriteBarrier((&____example_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEINFO_T092897DDF7419CA98579EA1A0A9451539BF301E8_H
#ifndef EXAMPLELISTITEM_TECED571A28DEBBAC04D0C4498F0CA901C610D970_H
#define EXAMPLELISTITEM_TECED571A28DEBBAC04D0C4498F0CA901C610D970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem
struct  ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem::_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____text_4;
	// BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem::<ParentUI>k__BackingField
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A * ___U3CParentUIU3Ek__BackingField_5;
	// BestHTTP.Examples.Helpers.SampleBase BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem::<ExamplePrefab>k__BackingField
	SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * ___U3CExamplePrefabU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970, ____text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__text_4() const { return ____text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of_U3CParentUIU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970, ___U3CParentUIU3Ek__BackingField_5)); }
	inline SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A * get_U3CParentUIU3Ek__BackingField_5() const { return ___U3CParentUIU3Ek__BackingField_5; }
	inline SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A ** get_address_of_U3CParentUIU3Ek__BackingField_5() { return &___U3CParentUIU3Ek__BackingField_5; }
	inline void set_U3CParentUIU3Ek__BackingField_5(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A * value)
	{
		___U3CParentUIU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentUIU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CExamplePrefabU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970, ___U3CExamplePrefabU3Ek__BackingField_6)); }
	inline SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * get_U3CExamplePrefabU3Ek__BackingField_6() const { return ___U3CExamplePrefabU3Ek__BackingField_6; }
	inline SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF ** get_address_of_U3CExamplePrefabU3Ek__BackingField_6() { return &___U3CExamplePrefabU3Ek__BackingField_6; }
	inline void set_U3CExamplePrefabU3Ek__BackingField_6(SampleBase_tF16A4F4FD56E3EF09F6559FD85B06AAB569347AF * value)
	{
		___U3CExamplePrefabU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExamplePrefabU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLELISTITEM_TECED571A28DEBBAC04D0C4498F0CA901C610D970_H
#ifndef SAMPLESELECTORUI_T483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A_H
#define SAMPLESELECTORUI_T483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI
struct  SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// BestHTTP.Examples.Helpers.SelectorUI.Category BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::_categoryListItemPrefab
	Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477 * ____categoryListItemPrefab_4;
	// BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::_exampleListItemPrefab
	ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 * ____exampleListItemPrefab_5;
	// BestHTTP.Examples.Helpers.SelectorUI.ExampleInfo BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::_exampleInfoPrefab
	ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8 * ____exampleInfoPrefab_6;
	// UnityEngine.RectTransform BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::_listRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____listRoot_7;
	// UnityEngine.RectTransform BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::_dyncamicContentRoot
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____dyncamicContentRoot_8;
	// BestHTTP.Examples.SampleRoot BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::sampleSelector
	SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 * ___sampleSelector_9;
	// BestHTTP.Examples.Helpers.SelectorUI.ExampleListItem BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::selectedSample
	ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 * ___selectedSample_10;
	// UnityEngine.GameObject BestHTTP.Examples.Helpers.SelectorUI.SampleSelectorUI::dynamicContent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___dynamicContent_11;

public:
	inline static int32_t get_offset_of__categoryListItemPrefab_4() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ____categoryListItemPrefab_4)); }
	inline Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477 * get__categoryListItemPrefab_4() const { return ____categoryListItemPrefab_4; }
	inline Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477 ** get_address_of__categoryListItemPrefab_4() { return &____categoryListItemPrefab_4; }
	inline void set__categoryListItemPrefab_4(Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477 * value)
	{
		____categoryListItemPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&____categoryListItemPrefab_4), value);
	}

	inline static int32_t get_offset_of__exampleListItemPrefab_5() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ____exampleListItemPrefab_5)); }
	inline ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 * get__exampleListItemPrefab_5() const { return ____exampleListItemPrefab_5; }
	inline ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 ** get_address_of__exampleListItemPrefab_5() { return &____exampleListItemPrefab_5; }
	inline void set__exampleListItemPrefab_5(ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 * value)
	{
		____exampleListItemPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&____exampleListItemPrefab_5), value);
	}

	inline static int32_t get_offset_of__exampleInfoPrefab_6() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ____exampleInfoPrefab_6)); }
	inline ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8 * get__exampleInfoPrefab_6() const { return ____exampleInfoPrefab_6; }
	inline ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8 ** get_address_of__exampleInfoPrefab_6() { return &____exampleInfoPrefab_6; }
	inline void set__exampleInfoPrefab_6(ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8 * value)
	{
		____exampleInfoPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&____exampleInfoPrefab_6), value);
	}

	inline static int32_t get_offset_of__listRoot_7() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ____listRoot_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__listRoot_7() const { return ____listRoot_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__listRoot_7() { return &____listRoot_7; }
	inline void set__listRoot_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____listRoot_7 = value;
		Il2CppCodeGenWriteBarrier((&____listRoot_7), value);
	}

	inline static int32_t get_offset_of__dyncamicContentRoot_8() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ____dyncamicContentRoot_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__dyncamicContentRoot_8() const { return ____dyncamicContentRoot_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__dyncamicContentRoot_8() { return &____dyncamicContentRoot_8; }
	inline void set__dyncamicContentRoot_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____dyncamicContentRoot_8 = value;
		Il2CppCodeGenWriteBarrier((&____dyncamicContentRoot_8), value);
	}

	inline static int32_t get_offset_of_sampleSelector_9() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ___sampleSelector_9)); }
	inline SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 * get_sampleSelector_9() const { return ___sampleSelector_9; }
	inline SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 ** get_address_of_sampleSelector_9() { return &___sampleSelector_9; }
	inline void set_sampleSelector_9(SampleRoot_tF4635187B5A3EE83AAE13A844865FF96D8CC0DA2 * value)
	{
		___sampleSelector_9 = value;
		Il2CppCodeGenWriteBarrier((&___sampleSelector_9), value);
	}

	inline static int32_t get_offset_of_selectedSample_10() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ___selectedSample_10)); }
	inline ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 * get_selectedSample_10() const { return ___selectedSample_10; }
	inline ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 ** get_address_of_selectedSample_10() { return &___selectedSample_10; }
	inline void set_selectedSample_10(ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970 * value)
	{
		___selectedSample_10 = value;
		Il2CppCodeGenWriteBarrier((&___selectedSample_10), value);
	}

	inline static int32_t get_offset_of_dynamicContent_11() { return static_cast<int32_t>(offsetof(SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A, ___dynamicContent_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_dynamicContent_11() const { return ___dynamicContent_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_dynamicContent_11() { return &___dynamicContent_11; }
	inline void set_dynamicContent_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___dynamicContent_11 = value;
		Il2CppCodeGenWriteBarrier((&___dynamicContent_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLESELECTORUI_T483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A_H
#ifndef TEXTLISTITEM_T5D03628A3D1E991E64674A567C7E1638FDD1B6BD_H
#define TEXTLISTITEM_T5D03628A3D1E991E64674A567C7E1638FDD1B6BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Examples.Helpers.TextListItem
struct  TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.TextListItem::_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____text_4;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD, ____text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__text_4() const { return ____text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTLISTITEM_T5D03628A3D1E991E64674A567C7E1638FDD1B6BD_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6000 = { sizeof (TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6000[1] = 
{
	TextListItem_t5D03628A3D1E991E64674A567C7E1638FDD1B6BD::get_offset_of__text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6001 = { sizeof (Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6001[1] = 
{
	Category_t8C1576BDEFC2B4BA6F5C0CB8E1810B750807F477::get_offset_of__text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6002 = { sizeof (ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6002[4] = 
{
	ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8::get_offset_of__header_4(),
	ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8::get_offset_of__description_5(),
	ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8::get_offset_of__parentUI_6(),
	ExampleInfo_t092897DDF7419CA98579EA1A0A9451539BF301E8::get_offset_of__example_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6003 = { sizeof (ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6003[3] = 
{
	ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970::get_offset_of__text_4(),
	ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970::get_offset_of_U3CParentUIU3Ek__BackingField_5(),
	ExampleListItem_tECED571A28DEBBAC04D0C4498F0CA901C610D970::get_offset_of_U3CExamplePrefabU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6004 = { sizeof (SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6004[8] = 
{
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of__categoryListItemPrefab_4(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of__exampleListItemPrefab_5(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of__exampleInfoPrefab_6(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of__listRoot_7(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of__dyncamicContentRoot_8(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of_sampleSelector_9(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of_selectedSample_10(),
	SampleSelectorUI_t483D6FA0D920CFAB7D9A9DEB73058DBC95ECC87A::get_offset_of_dynamicContent_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6005 = { sizeof (U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E), -1, sizeof(U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6005[2] = 
{
	U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6B3448F67883A3EA4C2C246B0CC54FF555EDDA3E_StaticFields::get_offset_of_U3CU3E9__9_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6006 = { sizeof (Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6006[3] = 
{
	Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6::get_offset_of__count_4(),
	Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6::get_offset_of__size_5(),
	Cache_tF8C173B605923CFED15F5EDA80A6F57C09D819F6::get_offset_of__clear_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6007 = { sizeof (Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6007[3] = 
{
	Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49::get_offset_of__count_4(),
	Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49::get_offset_of__size_5(),
	Cookies_tAAB760BE054E11D332D31BAE518F8AA55360DA49::get_offset_of__clear_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6008 = { sizeof (U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687), -1, sizeof(U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6008[2] = 
{
	U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE0B727F01216D8C06837B7BA42EA80E7CB9F1687_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6009 = { sizeof (U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F), -1, sizeof(U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6009[299] = 
{
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30060620709E93572EB9D62564332C778F7AE32E9_0(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3007AFA4B68442DD6E0877DE7624D0A5F508C474F_1(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U300A4ED0900CBF010B3628AA473D0D1477DB67FFF_2(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3013B76F7DD3E0346CB351F079CC062EFBD293457_3(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30392525BCB01691D1F319D89F2C12BF93A478467_4(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U303D9F1A66AED1E059B1609A09E435B708A88C8B8_5(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U304A4F8F1CDFE8681F711039522BDB360478ACD84_6(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U304B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_7(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U304D2A79C8A779AFAA779125335E9334C245EBB46_8(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_9(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3068B2E17352B5B9FF693CAE83421B679E0342A5C_10(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3083DE622A9A685DC50D8D5653CB388A41343C8EC_11(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3083EF765A34AC9DEC41761B4DCEA48EC009115E9_12(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30953DF544832295E4A5B19928F95C351F25DA86A_13(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3095B351FE2104237B032546280C98C9804D331C5_14(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30982B1B45B764F2694ABC3DE57204AC898651429_15(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30B1E7265E67D2458D8EB536A4B36380A5BF6E731_16(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30BE6B8194AB353640CA66F0A661EA93A3132393A_17(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30C4110BC17D746F018F47B49E0EB0D6590F69939_18(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30D0825E62E82DBEBFAD598623694129548E24C9C_19(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_20(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30D762A448FEF55A8E0DA63AB8F3FFC8D3A3F1070_21(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U30E699DEC139E44E7D1DF125C2CCB3E25A1BDF892_22(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31005BA20F99323E3F050E781BB81D1A4479AB037_23(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3102C522344FCAC1545BDA50C0FC675C502FFEC53_24(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3103752E99F3718E46F1AA9EED70682BF3A9B8A65_25(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3118FE9754A9699004D478E4BF686021154D96EAD_26(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_27(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3126589410FF9CA1510B9950BF0E79E5BFD60000B_28(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_29(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U314353D63A97A25640437E7C0DAE8B1596F34AB2C_30(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3146E4D4A36742E37DEE528D23474B9675695E406_31(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3148E9E3E864CD628C70D3DC1D8309483BD8C0E89_32(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U314F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_33(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31648F737A4CFFDA4E6C83A3D742109BF9DBC2446_34(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U317651A9FA4DEA6C24D1287324CF4A640D080FE8E_35(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U317BCB27C371D710141A300DA5A9A3EADF78D7A35_36(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U317E54FCA28103DF892BBB946DDEED4B061D7F5C7_37(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U318F46E3C2ECF4BAAE65361632BA7C1B4B9028827_38(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31A43D7FEEED03520E11C4A8996F149705D99C6BB_39(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31AFB455399A50580CF1039188ABA6BE82F309543_40(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31B180C6E41F096D53222F5E8EF558B78182CA401_41(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31C1237F52E2ED7B4D229AE3978DA144B9E653F5E_42(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31D3E73DD251585C4908CBA58A179E1911834C891_43(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31E3842329C5294DBE1DF588A77C68B35C6AF83BF_44(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_45(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U321305ABEF36592B9C636A4DDFFB419BBEB14AC71_46(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3214F93D9222D60794CE1EA0A10389885C5CA9824_47(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U321A3E0CD2847E2F12EEE37749A9E206494A55100_48(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U322F13A28C218AA9B43303043F2CF664790D12BD7_49(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3239B59488F1CE7EBE225785FDC22A8E3102A2E82_50(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U326853A2C322BBAD5BBD886C60A32BBBCFE847F00_51(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U327C3AB82CCA6CE2F199F4F670BF19513A3825B87_52(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U327FED0F92A97C41B08D3115553BBDC064F417B6E_53(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U329EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_54(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U329F7A0217340B5682E7DDF98ADAD952E2A360E65_55(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U32BA9E4B370D477F8C7FE286262D7ADC69CAF290E_56(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U32CABEB86D5B3D362822AF2E5D136A10A17AD85DA_57(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U32CDACAA27B7865A56A57B156CFB369245964BD2B_58(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U32E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_59(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U32FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_60(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33045D5B29BA900304981918A153806D371B02549_61(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U330A6392FC8DA4E1BBE29B5EEA977D84BE673CAF7_62(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U330F65A149AF7DE938A9287048498B966AEBE54D4_63(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3310EFB639F3C7677A2A82B54EEED1124ED69E9A3_64(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3310FB325A3EA3EA527B55C2F08544D1CB92C19F4_65(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U331CDD717843C5C2B207F235634E6726898D4858A_66(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U331D8729F7377B44017C0A2395A582C9CA4163277_67(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3320B018758ECE3752FFEDBAEB1A6DB67C80B9359_68(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3321CB68F886E22F95877B3535C1B34A6A94A40B6_69(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U332ECB35FF8400B4E56FF5E09588FB20DD60350E7_70(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3340B4B55E1DFD4BADC69CA57007FC1A4CDBA7943_71(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33544182260B8A15D332367E48C7530FC0E901FD3_72(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U335D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_73(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U335E6464339FFAE0D3777B12A371F82D2D1F668CA_74(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3372040F482ABADADF58EF0C31A6A8BE386AF8A50_75(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U337454D933508E238CFB980F1077B24ADA4A480F4_76(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33A38ADC6BCFB84DE23160C1E50212DACBCD25A11_77(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33BA78FFB102A191597BAC4A2418C57CDF377FEE6_78(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33C3704DB6466D48DD40FE189070F1896D1D45C8B_79(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33D6EB645BC212077C1B37A3A32CA2A62F7B39018_80(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33E8E493888B1BFB763D7553A6CC1978C17C198A3_81(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U33F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_82(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3400E52B9E7F331D1ACC81B4EA40D228C7A6D60F8_83(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3428007959831954B0C2DCFAF9DD641D629B00DBF_84(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U342C3E89412F11AA94E57C09EFB4B2B415C1AAB58_85(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3433175D38B13FFE177FDD661A309F1B528B3F6E2_86(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34435D44E1091E6624ED6B6E4FA3C9A8C5C996098_87(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_88(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3467C6758F235D3193618192A64129CBB602C9067_89(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U347A4F979AF156CDA62313B97411B4C6CE62B8B5A_90(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3498B9317C14CF9004FE6CB88782D926B3E379350_91(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3499A21DC530CE4D5D71E97C5D8DB1B3C60F0FC2E_92(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34A080107805D8F7103F56DD3B0D8FD5D55735937_93(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34B411385A36907D25D8088AE39AB6AAFA46B0642_94(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34C44594E2C603D85EC6195B1A7A6C5876CBB58E2_95(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34C8FA1E9ACD72AED71F3923AEC5DF6AC8B6EFFE4_96(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34E16474F9D4C9E164E5F3C5FF50D0FB922D244DD_97(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U34FFC8339E09825A68B861995F9C660EB11DBF13D_98(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U350AA269217736906D8469B9191F420DC6B13A36A_99(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_100(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3512F92F4041B190727A330E2A6CC39E5D9EA06E6_101(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35318DA9ACB77B4E0953DA6BF6B04DF138D74EA1E_102(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3547AF924C0A210AD3D72D199002D3CDBC2DBD0CA_103(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_104(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35581A70566F03554D8048EDBFC6E6B399AF9BCB1_105(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U358F8E05C1E95C042205E13B76D1BFC0F7ABA6F88_106(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_107(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35AB421AC76CECB8E84025172585CB97DE8BECD65_108(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35AE32F75295B7AC2FB76118848B92F3987B949A0_109(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35BF56A4EEA44BBE4974BB61DA08B42B6EC49B72D_110(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35CF7299F6558A8AC3F821B4F2F65F23798D319D3_111(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35CFF612F2058DE2C287ECA5C6407ECC471F65576_112(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35D41C56232C500092E99AC044D3C5C442B1C834F_113(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35DBC1A420B61F594A834C83E3DDC229C8AB77FDC_114(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35E7F55149EC07597C76E6E3CD9F62274214061E6_115(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U35EF7F909EFC731E811E21521A43A80FB5AC0B229_116(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U360A08108A32C9D3F263B2F42095A2694B7C1C1EF_117(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_118(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36277CE8FE3A9156D3455749B453AC88191D3C6D6_119(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U362BAB0F245E66C3EB982CF5A7015F0A7C3382283_120(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_121(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U364354464C9074B5BB4369689AAA131961CD1EF19_122(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3643A9D76937E94519B73BE072D65E79BAFF3C213_123(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3643F7C1D25ADAFA2F5ED135D8331618A14714ED2_124(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U366961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_125(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_126(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U367C0E784F3654B008A81E2988588CF4956CCF3DA_127(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U368178023585F1F782745740AA583CDC778DB31B3_128(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36A316789EED01119DE92841832701A40AB0CABD6_129(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36AAC0DB543C50F09E879F5B9F757319773564CE1_130(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_131(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36BBA9E7ACC5D57E30438CC78FF7E308399E10FE1_132(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36BBD3A22A185224EE0EBAB0784455E9E245376B7_133(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36C59E372F66FEFCF8A0CEFB4163EF666FC7A8DF7_134(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36D19ABD7FD0986E382242A9B88C8D8B5F52598DF_135(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_136(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36DEB7F74818574642B0B824B9C08B366C962A360_137(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36DFD60679846B0E2064404E9EA3E1DDCE2C5709D_138(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36E92E42774698E4C089FE0FE4F7539D0D5F845F5_139(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36F39BC29A161CAE5394821B1FDE160EB5229AE71_140(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36F576E8737EAAC9B6B12BDFC370048CD205E2CDD_141(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36F5D2C37DFCDB1E72B86BC606C0E717C70E188A4_142(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U36FE282215AF15E3C00DDCAB5E6BD720D1F5BC0BC_143(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37037807198C22A7D2B0807371D763779A84FDFCF_144(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3714A94F3805E05CA6C00F9A46489427ABEB25D60_145(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U371F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_146(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37878E9E7126B2BDF365429C31842AE1903CD2DFF_147(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U379039C5C22F863E8EAE0E08E1318F343680E50A8_148(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U379A213B796D2AD7A89C2071B0732B78207F5CE01_149(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U379D1A3D3B708EBFF6FA27AAF280AC9B637B4FF31_150(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_151(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37B18F6A671FF25FBD5F195673DAF4902FB2DA4C5_152(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37B22115C45C6AD570AFBAB744FA1058DF97CDBC1_153(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37C57CFE9FF25243824AF38485BBF41F9E57ECF21_154(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37CFF7A50C8F8981091791CDB210243E8F465BC80_155(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37E68A6B197A3AF443FDA77CD8B492D4C72B3CF03_156(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37F817443C1F736721DD92BD67C7C8994C7B8DEBF_157(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U37FF0A15672FF2807983AB77C0DA74928986427C0_158(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U381FDCC6319440C75C8E422A1FE4E3F1A80B89F5F_159(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_160(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38330271815E046D369E0B1F7673D308739FDCC07_161(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_162(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38457F44B035C9073EE2D1F132D0A8AF5631DCDC8_163(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U384F6B4137736E2F6671FC8787A500AC5C6E1D6AC_164(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3850D4DC092689E1F0D8A70B6281848B27DEC0014_165(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U386B0F85AC13B58F88DEFFD8FD6EE095438B98F10_166(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U387BAD4D818823E27FF0CBB935FF88DD21DAD3CC4_167(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3882888781BC0DC17021FB4F11BA783038C83B313_168(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38AC10B57091AFE182D9E4375C05E3FA037FFB3FA_169(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38C7FEE53346CDB1B119FCAD8D605F476400A03CE_170(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38C9BE3C02B5604C5CBF6A03E8032549588A6ED54_171(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_172(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38ED8F61DAA454B49CD5059AE4486C59174324E9E_173(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U38F22C9ECE1331718CBD268A9BBFD2F5E451441E3_174(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U390A0542282A011472F94E97CEAE59F8B3B1A3291_175(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3929747B3E91297D56A461EE1B9E054EA31C70EC1_176(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_177(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U3959C1F7BD85779AAF7AA136A20A9C399A5019AEC_178(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U395CA85749ADCFBF9A2B82C0381DBCF95D175524C_179(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U396ADC3934F8492C827987DFEE3B4DD4EF1738E78_180(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U398CF708E75DA60C2A04D06606E726EC975E6DBAB_181(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39AEFA90D8E67EBAE069B4B6C071A8E867B108B31_182(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_183(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39F198D04FF67738AE583484145F45103ECE550CC_184(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39F8365E9D6C62D3B47026EC465B05A7B5526B5CD_185(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_186(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_U39FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_187(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A0FABB8173BA247898A9FA267D0CE05500B667A0_188(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A19414BB17AF83ECF276AD49117E7F2EE344D567_189(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A32D5F33452B98E86BE15ED849ED6DF543B5F243_190(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A4313AAA146ACFCA88681B7BFC3D644005F3792B_191(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A43F35160EEC5F8DD047667DD94358EDCD29BA27_192(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A471AF3330805980C7041F978D3CFF8838054E14_193(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A474A0BEC4E2CE8491839502AE85F6EA8504C6BD_194(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A47DBBBB6655DAA5E024374BB9C8AA44FF40D444_195(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A53306F44DF494A019EA1487807B59CA336BF024_196(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A5981DCAA364B0DC9E0385D893A31C2022364075_197(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A696E1EE8632C559732B81052E4D2993B8783877_198(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A6BA6E61D8DC7245D265FDFACD05778F1F6FA238_199(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_200(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A77B822E6880234B59FD1904BF79AF68F5CB623F_201(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_A94DF043C88FB07ACCA327B6A0BB063F20637E71_202(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_203(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AAF72C1002FDBCAE040DAE16A10D82184CE83679_204(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AD19F20EECB80A2079F504CB928A26FDE10E8C47_205(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_206(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AD4075598ACA56EC39C5E575771BBB0CFBCE24EE_207(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AE843E1C1136C908565A6D4E04E8564B69465B3B_208(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_AF3E960D2F0572119C78CAF04B900985A299000E_209(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B01915493E44597A78D21AC245268E65DDA920ED_210(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_211(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B31A9EA1A2718942996E7F94106F0FC798C174C8_212(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B3A60EC240A886DA5AFD600CC73AE12514A881E8_213(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_214(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_215(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B5E8BA68953A5283AD953094F0F391FA4502A3FA_216(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B67A7FB4648C62F6A1337CA473436D0E787E8633_217(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B68637EF60D499620B99E336C59E4865FFC4C5D7_218(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_219(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B8DB0CB599EDD82A386D1A154FB3EB9235513DAD_220(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_B997A0149EBF3CDD050D72AE1784E375A413B128_221(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_BBBDF1388DDDDEEC01227AF1738F138452242E3A_222(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_BCE617693C33CE2C76FE00F449CA910E4C6E117E_223(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_BD0C733AC85AB335526865FA6269790347865705_224(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_BE8E2513259482B6F307AC07F23F5D9FB4841EAA_225(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_BEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_226(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C079C42AC966756C902EC38C4D7989F3C20D3625_227(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_228(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C105B70BED997DB5D36E1D2E84C1EFCB445A428C_229(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C1122CB95F41A6F2E808A0B815C822BFDAB9F180_230(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C132685022CE310ACFD3A883E0A57033A482A959_231(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C1C8DE48D8C0FA876F4ED0086BB93D7EAF61E4B2_232(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C2443D9DBCD3810F90475C6F404BEDCFEF58661E_233(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C2D514B39C8DFA25365195A0759A5AE28D9F2A87_234(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C2FEEB3C521ADDD49A534A0876BA97FF5894476E_235(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_236(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C4B266E68FA20D0D222D86ADAD31EBB55118CD21_237(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C5515C87D04DC0D00C7984096F5E35B4944C1CB6_238(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_239(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C95356610D5583976B69017BED7048EB50121B90_240(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_C991C784E7697AD0F91A159F03727BF4621A5AB8_241(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_242(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_243(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CC53D7FE00E6AC1385AF09521629229467BCCC86_244(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_245(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_246(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CE39574ADC95015A9B5E0475EB65EE8F32353FD4_247(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_CF47311491B9DA067CDF3F5F087D7EFAF2B64F97_248(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D068832E6B13A623916709C1E0E25ADCBE7B455F_249(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D0F0A985F21DE4FE8F4EF4B161308661EEEEA73C_250(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D129FBC67222EA7D73E90E51E4DCFCA8C7497D67_251(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D1C2E1F681ED98C3A518F7C96A5D5C6FC0E3F608_252(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D2C5BAE967587C6F3D9F2C4551911E0575A1101F_253(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D31171F7904EB3247DD4834E43B47B1E2DCB97AC_254(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_255(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_256(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D6898715AE96BC2F82A7BBA76E2BFC7100E282C3_257(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D7231C06B1D6276752359120E26EAE206A7F74F9_258(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D73609A97D6906CFFB02ADDA320A4809483C458D_259(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D860D5BD327368D1D4174620FE2E4A91FE9AADEC_260(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_261(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_262(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_D9C221237B647EC215A7BCDED447349810E6BF9C_263(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_DA7CFA4F9698F5D02C5D90A5858F65EE43B4D564_264(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_265(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_DB8FE39060DF4E7BC9CC190D08EFF78F0C61F289_266(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_267(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E0219F11D9EECC43022AA94967780250AC270D4B_268(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_269(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E17D18DCD8392C99D47823F8CB9F43896D115FB8_270(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_271(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E43F6BA634642FB90FEEE1A8F9905E957741960C_272(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E53E13AFB95C5C24DF50875117B7DDCE12937B2E_273(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_E647D32D165F3510693DF9787DC98E0A0B63C5C2_274(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_275(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EB6F545AEF284339D25594F900E7A395212460EB_276(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EBE167F7962841FA83451C9C1663416D69AA5294_277(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_278(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EEDBCB52C67688DE5F5FD9209E8A25BC786A2430_279(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_280(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_EF813A47B13574822D335279EF445343654A4F04_281(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F0AEF81E2E3EC0843206EBA7D65BCD1AF83DE7C9_282(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F128744756EEB38C3EAD4A7E8536EC5D3FA430FF_283(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_284(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_285(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F3E701C38098B41D23FA88977423371B3C00C0D1_286(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_287(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F5AEFD834ADB72DAA720930140E9ECC087FCF389_288(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F63639527E877A2CBCB26FFD41D4A59470BFF8C8_289(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_F8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_290(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FA5B1C8B2F287078ED719C15595DB729BDB85911_291(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FAD52931F5B79811D31566BB18B6E0B5D2E2A164_292(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_293(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FB3C663794DD23F500825FF78450D198FE338938_294(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FC9EEBC457831129D4AF4FF84333B481F4BED60E_295(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FCDDAA37B8499E00EB7B606D8540AA64DE5AE4D4_296(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FD9088D5287E3E3A6699200CB6E5DE3E9FDEDD9F_297(),
	U3CPrivateImplementationDetailsU3E_t837AC9D13901280E875914E3F0905F8FC0DD012F_StaticFields::get_offset_of_FE5567E8D769550852182CDF69D74BB16DFF8E29_298(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6010 = { sizeof (__StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D3_t2FF14B1CAF9002504904F263389DDF1B48A1C432 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6011 = { sizeof (__StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D5_tC3DEA7A176A0226116DCE0C50012795158E22923 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6012 = { sizeof (__StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t2A988DB17692352FB8EBC461F2D4A70546950009 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6013 = { sizeof (__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t40369F2228DD607A80D17BB3D2D5BE6993E03742 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6014 = { sizeof (__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_tE9246BC51AC87DC889AA3C427D51E2C95286DD12 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6015 = { sizeof (__StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D19_tB0FF1D15BF4C0810292DCF1103D532EA3FD3AB85 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6016 = { sizeof (__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_tAE3A6C868C85AE191616B68E230CF63EC626520A ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6017 = { sizeof (__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t5B47C1227ED09E328284961C56B9745E8CF857B2 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6018 = { sizeof (__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t23354B8C06A0C08995538AA6989AD5A6BD1F736B ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6019 = { sizeof (__StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D30_t27982463335070BCC28CF69F236F03360875F9CD ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6020 = { sizeof (__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t1FA6CEC782AA662C9B5D2500E01EF0DBD1892391 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6021 = { sizeof (__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D36_t4966948BB7C8115E94C03B23C8BF2F7311DF4981 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6022 = { sizeof (__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_t81C3686E05073D410811D7E20015B0D31FD02B51 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6023 = { sizeof (__StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_t850D7D2483453A735E5BF43D3DF23B3E49383942 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6024 = { sizeof (__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D48_tAAFA2CBD4C6B1C4D3B931972D4F279EA69E5542A ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6025 = { sizeof (__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D56_tCE789691A5319A29277651D0EDDFF891792A6256 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6026 = { sizeof (__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D64_tAC179E5A47816D03F547C9EC0B8374CCE5C05991 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6027 = { sizeof (__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D68_tBC0122564AB1A13F811AD692B6F90250F54F07C0 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6028 = { sizeof (__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D72_t2372182CEF6FF10BBA734B1338E94DF2176D148C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6029 = { sizeof (__StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D76_t38115C5264B74F6026FD592AC3726967368CBCCE ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6030 = { sizeof (__StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D96_t0EC581000EDFC257A98827E661B6AB5E51F36015 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6031 = { sizeof (__StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D112_tBA6FAEC1E474CBDEC00CA40BD22865A888112885 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6032 = { sizeof (__StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D116_t3ECC97757AB98ED162DDA5594A22E596E7310B27 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6033 = { sizeof (__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t12E95F9892C7F6F42D9704D5452F45ACD91DEF27 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6034 = { sizeof (__StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D124_tB51B1284695CD5EF856709FD5375A613C97A8821 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6035 = { sizeof (__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D128_t6A568434D1DD1DC652C8F7BAF60D1397939FB018 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6036 = { sizeof (__StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D160_tDECE94EBDBDBD8BFFFC5C41BB56E63703BAA0926 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6037 = { sizeof (__StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D192_tD175F5D12CCD1BBBFC4A7E495AA2CD37FE9A7C64 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6038 = { sizeof (__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D256_tCF2FC3DC530AA2A595156A0FE4D2B533781D388B ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6039 = { sizeof (__StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D384_t748C53461277A9DAA5390470D2236736909F3FAD ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6040 = { sizeof (__StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D404_tAB7DC965F421CBB7140773669EBC7FF4362EE38D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6041 = { sizeof (__StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D511_t0A5821374352C92EB288D21CD4341DBBBD5C6AD9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6042 = { sizeof (__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_tED2E2674FDE7D2F45A762091ABCD036953771373 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6043 = { sizeof (__StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D640_t07C2BC1E9E8555348A07BC4FA16F4894853703C6 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6044 = { sizeof (__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1024_t262580B26E1F8C3057C04E41B38E058D345E203E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6045 = { sizeof (__StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1152_t63DDBCF6A3D9AED58517711035E84F428212FB44 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6046 = { sizeof (__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D2048_t5DE79873FEB376EFFB0288E5F1586BB39A09E510 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6047 = { sizeof (__StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D4096_t9A971A259AA05D3B04C6F63382905A5E23316C44 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6048 = { sizeof (__StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6144_t3D3195A0EEF11E77307A04AD01FB23BEEADBAEA4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6049 = { sizeof (U3CModuleU3E_t57B0800FF641881416BC0907D3D2A59FE93BD873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6050 = { sizeof (ConstructorHandling_tC9A0E967603307F5C573320F3D7CDDA57CFFEA88)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6050[3] = 
{
	ConstructorHandling_tC9A0E967603307F5C573320F3D7CDDA57CFFEA88::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6051 = { sizeof (DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6051[3] = 
{
	DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6052 = { sizeof (DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6052[4] = 
{
	DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6053 = { sizeof (DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6053[5] = 
{
	DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6054 = { sizeof (DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6054[5] = 
{
	DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6055 = { sizeof (FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6055[4] = 
{
	FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6056 = { sizeof (FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6056[3] = 
{
	FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6057 = { sizeof (Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6057[3] = 
{
	Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6058 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6059 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6060 = { sizeof (JsonArrayAttribute_tF8360DD00442DA4453BBAC4E758E4B53DD8799B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6061 = { sizeof (JsonConstructorAttribute_tF2DEC4EEF6D09EA4AD193356DF661C34DFECFC06), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6062 = { sizeof (JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6062[6] = 
{
	JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0(),
	JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1(),
	JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA::get_offset_of__isReference_2(),
	JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA::get_offset_of__itemIsReference_3(),
	JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA::get_offset_of__itemReferenceLoopHandling_4(),
	JsonContainerAttribute_t905D5199F79FD8BA5ADCC5C7F738693165D982FA::get_offset_of__itemTypeNameHandling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6063 = { sizeof (JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F), -1, sizeof(JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6063[8] = 
{
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_True_1(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_False_2(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_Null_3(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_Undefined_4(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_PositiveInfinity_5(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_NegativeInfinity_6(),
	JsonConvert_t41186D2C92A15E30FBEB8087981F668A711CE25F_StaticFields::get_offset_of_NaN_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6064 = { sizeof (JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6065 = { sizeof (JsonConverterAttribute_t971BE732936930F54D76941AAC672587C409404B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6065[2] = 
{
	JsonConverterAttribute_t971BE732936930F54D76941AAC672587C409404B::get_offset_of__converterType_0(),
	JsonConverterAttribute_t971BE732936930F54D76941AAC672587C409404B::get_offset_of_U3CConverterParametersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6066 = { sizeof (JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6067 = { sizeof (JsonDictionaryAttribute_t0E0A3CF6D5AE30ABF7BB2C29ABC450B7160C4AB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6068 = { sizeof (JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6069 = { sizeof (JsonExtensionDataAttribute_t90D1D7C4F9D65A96740C0756C512C8BCFBC62B03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6069[2] = 
{
	JsonExtensionDataAttribute_t90D1D7C4F9D65A96740C0756C512C8BCFBC62B03::get_offset_of_U3CWriteDataU3Ek__BackingField_0(),
	JsonExtensionDataAttribute_t90D1D7C4F9D65A96740C0756C512C8BCFBC62B03::get_offset_of_U3CReadDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6070 = { sizeof (JsonIgnoreAttribute_tA00F2FBCE767ECB606C2BBD26192EC4871A4B726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6071 = { sizeof (JsonObjectAttribute_t6C4FB3D7397FBADA233AD510E5235AD53B20E534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6071[2] = 
{
	JsonObjectAttribute_t6C4FB3D7397FBADA233AD510E5235AD53B20E534::get_offset_of__memberSerialization_6(),
	JsonObjectAttribute_t6C4FB3D7397FBADA233AD510E5235AD53B20E534::get_offset_of__itemRequired_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6072 = { sizeof (JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6072[5] = 
{
	JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6073 = { sizeof (JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62)+ sizeof (RuntimeObject), sizeof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_pinvoke), sizeof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6073[5] = 
{
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields::get_offset_of_SpecialCharacters_0(),
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62::get_offset_of_Position_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62::get_offset_of_PropertyName_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62::get_offset_of_HasIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6074 = { sizeof (JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6074[14] = 
{
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__nullValueHandling_0(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__defaultValueHandling_1(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__referenceLoopHandling_2(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__objectCreationHandling_3(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__typeNameHandling_4(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__isReference_5(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__order_6(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__required_7(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__itemIsReference_8(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__itemReferenceLoopHandling_9(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of__itemTypeNameHandling_10(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12(),
	JsonPropertyAttribute_tECD4ED1B6F24969C3EC15AEFE6CA66B4F879153A::get_offset_of_U3CPropertyNameU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6075 = { sizeof (JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6075[15] = 
{
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__tokenType_0(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__value_1(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__quoteChar_2(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__currentState_3(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__currentPosition_4(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__culture_5(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__dateTimeZoneHandling_6(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__maxDepth_7(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__hasExceededMaxDepth_8(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__dateParseHandling_9(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__floatParseHandling_10(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__dateFormatString_11(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of__stack_12(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of_U3CCloseInputU3Ek__BackingField_13(),
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636::get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6076 = { sizeof (State_tF808A376CD3F052C3C1BE586C60C34C534ADB010)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6076[14] = 
{
	State_tF808A376CD3F052C3C1BE586C60C34C534ADB010::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6077 = { sizeof (JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6077[3] = 
{
	JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F::get_offset_of_U3CLineNumberU3Ek__BackingField_17(),
	JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F::get_offset_of_U3CLinePositionU3Ek__BackingField_18(),
	JsonReaderException_tABAD0FF737611157A789D571AC50787F1FF4FF1F::get_offset_of_U3CPathU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6078 = { sizeof (JsonRequiredAttribute_tBAE089AA5D5D0E66B636EDA11A6E8D9AD1BE4286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6079 = { sizeof (JsonSerializationException_tB0B6266035536D0449B9F7D1A94E80890286AF16), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6080 = { sizeof (JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6080[31] = 
{
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__typeNameHandling_0(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__typeNameAssemblyFormat_1(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__preserveReferencesHandling_2(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__referenceLoopHandling_3(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__missingMemberHandling_4(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__objectCreationHandling_5(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__nullValueHandling_6(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__defaultValueHandling_7(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__constructorHandling_8(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__metadataPropertyHandling_9(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__converters_10(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__contractResolver_11(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__traceWriter_12(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__equalityComparer_13(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__binder_14(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__context_15(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__referenceResolver_16(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__formatting_17(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__dateFormatHandling_18(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__dateTimeZoneHandling_19(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__dateParseHandling_20(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__floatFormatHandling_21(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__floatParseHandling_22(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__stringEscapeHandling_23(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__culture_24(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__maxDepth_25(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__maxDepthSet_26(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__checkAdditionalContent_27(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__dateFormatString_28(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of__dateFormatStringSet_29(),
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623::get_offset_of_Error_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6081 = { sizeof (JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C), -1, sizeof(JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6081[33] = 
{
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C_StaticFields::get_offset_of_DefaultContext_0(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C_StaticFields::get_offset_of_DefaultCulture_1(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__formatting_2(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__dateFormatHandling_3(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__dateTimeZoneHandling_4(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__dateParseHandling_5(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__floatFormatHandling_6(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__floatParseHandling_7(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__stringEscapeHandling_8(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__culture_9(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__checkAdditionalContent_10(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__maxDepth_11(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__maxDepthSet_12(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__dateFormatString_13(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__dateFormatStringSet_14(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__typeNameAssemblyFormat_15(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__defaultValueHandling_16(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__preserveReferencesHandling_17(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__nullValueHandling_18(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__objectCreationHandling_19(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__missingMemberHandling_20(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__referenceLoopHandling_21(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__context_22(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__constructorHandling_23(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__typeNameHandling_24(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of__metadataPropertyHandling_25(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CConvertersU3Ek__BackingField_26(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CContractResolverU3Ek__BackingField_27(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CEqualityComparerU3Ek__BackingField_28(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CTraceWriterU3Ek__BackingField_30(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CBinderU3Ek__BackingField_31(),
	JsonSerializerSettings_t6F615B4300C850B2B1FEFEBAC288DAC7D714229C::get_offset_of_U3CErrorU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6082 = { sizeof (ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6082[10] = 
{
	ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6083 = { sizeof (JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6083[11] = 
{
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__reader_15(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__chars_16(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__charsUsed_17(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__charPos_18(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__lineStartPos_19(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__lineNumber_20(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__isEndOfFile_21(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__stringBuffer_22(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__stringReference_23(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of__arrayPool_24(),
	JsonTextReader_t6A7D574DD2106436ACFC19E372AB9138EF3CC513::get_offset_of_NameTable_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6084 = { sizeof (JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6084[10] = 
{
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__writer_13(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__base64Encoder_14(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__indentChar_15(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__indentation_16(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__quoteChar_17(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__quoteName_18(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__charEscapeFlags_19(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__writeBuffer_20(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__arrayPool_21(),
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01::get_offset_of__indentChars_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6085 = { sizeof (JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6085[19] = 
{
	JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6086 = { sizeof (JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76), -1, sizeof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6086[13] = 
{
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__stack_2(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__currentPosition_3(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__currentState_4(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__formatting_5(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__dateFormatHandling_7(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__dateTimeZoneHandling_8(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__stringEscapeHandling_9(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__floatFormatHandling_10(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__dateFormatString_11(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__culture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6087 = { sizeof (State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6087[11] = 
{
	State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6088 = { sizeof (JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6088[1] = 
{
	JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B::get_offset_of_U3CPathU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6089 = { sizeof (MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6089[4] = 
{
	MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6090 = { sizeof (MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6090[4] = 
{
	MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6091 = { sizeof (MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6091[3] = 
{
	MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6092 = { sizeof (NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6092[3] = 
{
	NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6093 = { sizeof (ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6093[4] = 
{
	ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6094 = { sizeof (PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6094[5] = 
{
	PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6095 = { sizeof (ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6095[4] = 
{
	ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6096 = { sizeof (Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6096[5] = 
{
	Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6097 = { sizeof (StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6097[4] = 
{
	StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6098 = { sizeof (TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6098[6] = 
{
	TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6099 = { sizeof (WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6099[8] = 
{
	WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
