﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher
struct BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine
struct Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine/ThreefishCipher
struct ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_t5E47CAAC0C7B909FEE19C9C4D4342AFC8B15F719;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBasicAgreement
struct IBasicAgreement_tE49895CA0590A50A2F09F9B68F2927DCC10742B1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_t391586A9268E9DABF6C6158169C448C4243FA87C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters
struct ICipherParameters_t1E1B86752982ED2CFC2683B4C1A0BC0CDB77345B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDerivationFunction
struct IDerivationFunction_t7156A3489DEA256AACADA1FE69E6C5A34CEE8493;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest
struct IDigest_t4779036D052ECF08BAC3DF71ED71EF04D7866C09;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac
struct IMac_tD02303ADA25C4494D9C2B119B486DF1D9302B33C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IRsa
struct IRsa_t8EA293A52E8002BE021C72853DD68CCCB9B20820;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher
struct CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters
struct ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyParameters
struct ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesParameters
struct IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyParameters
struct NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV
struct ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[0...,0...]
struct ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Collections.IList[]
struct IListU5BU5D_t6779C6A7A4B7074C864EA7FE9328D68F0E4866DF;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.Int64[][]
struct Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt64[]
struct UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4;
// System.UInt64[][]
struct UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#define X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB, ___parameters_0)); }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifndef BLAKE2BDIGEST_T6671A8E0151C6C2E9A278439C58497B09B0D7C9A_H
#define BLAKE2BDIGEST_T6671A8E0151C6C2E9A278439C58497B09B0D7C9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest
struct  Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::digestLength
	int32_t ___digestLength_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::keyLength
	int32_t ___keyLength_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::personalization
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___personalization_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::bufferPos
	int32_t ___bufferPos_10;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::internalState
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___internalState_11;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::chainValue
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___chainValue_12;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::t0
	uint64_t ___t0_13;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::t1
	uint64_t ___t1_14;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::f0
	uint64_t ___f0_15;

public:
	inline static int32_t get_offset_of_digestLength_4() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___digestLength_4)); }
	inline int32_t get_digestLength_4() const { return ___digestLength_4; }
	inline int32_t* get_address_of_digestLength_4() { return &___digestLength_4; }
	inline void set_digestLength_4(int32_t value)
	{
		___digestLength_4 = value;
	}

	inline static int32_t get_offset_of_keyLength_5() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___keyLength_5)); }
	inline int32_t get_keyLength_5() const { return ___keyLength_5; }
	inline int32_t* get_address_of_keyLength_5() { return &___keyLength_5; }
	inline void set_keyLength_5(int32_t value)
	{
		___keyLength_5 = value;
	}

	inline static int32_t get_offset_of_salt_6() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___salt_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_6() const { return ___salt_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_6() { return &___salt_6; }
	inline void set_salt_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_6 = value;
		Il2CppCodeGenWriteBarrier((&___salt_6), value);
	}

	inline static int32_t get_offset_of_personalization_7() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___personalization_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_personalization_7() const { return ___personalization_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_personalization_7() { return &___personalization_7; }
	inline void set_personalization_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___personalization_7 = value;
		Il2CppCodeGenWriteBarrier((&___personalization_7), value);
	}

	inline static int32_t get_offset_of_key_8() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___key_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_8() const { return ___key_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_8() { return &___key_8; }
	inline void set_key_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_8 = value;
		Il2CppCodeGenWriteBarrier((&___key_8), value);
	}

	inline static int32_t get_offset_of_buffer_9() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___buffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_9() const { return ___buffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_9() { return &___buffer_9; }
	inline void set_buffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_9), value);
	}

	inline static int32_t get_offset_of_bufferPos_10() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___bufferPos_10)); }
	inline int32_t get_bufferPos_10() const { return ___bufferPos_10; }
	inline int32_t* get_address_of_bufferPos_10() { return &___bufferPos_10; }
	inline void set_bufferPos_10(int32_t value)
	{
		___bufferPos_10 = value;
	}

	inline static int32_t get_offset_of_internalState_11() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___internalState_11)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_internalState_11() const { return ___internalState_11; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_internalState_11() { return &___internalState_11; }
	inline void set_internalState_11(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___internalState_11 = value;
		Il2CppCodeGenWriteBarrier((&___internalState_11), value);
	}

	inline static int32_t get_offset_of_chainValue_12() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___chainValue_12)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_chainValue_12() const { return ___chainValue_12; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_chainValue_12() { return &___chainValue_12; }
	inline void set_chainValue_12(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___chainValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___chainValue_12), value);
	}

	inline static int32_t get_offset_of_t0_13() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___t0_13)); }
	inline uint64_t get_t0_13() const { return ___t0_13; }
	inline uint64_t* get_address_of_t0_13() { return &___t0_13; }
	inline void set_t0_13(uint64_t value)
	{
		___t0_13 = value;
	}

	inline static int32_t get_offset_of_t1_14() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___t1_14)); }
	inline uint64_t get_t1_14() const { return ___t1_14; }
	inline uint64_t* get_address_of_t1_14() { return &___t1_14; }
	inline void set_t1_14(uint64_t value)
	{
		___t1_14 = value;
	}

	inline static int32_t get_offset_of_f0_15() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A, ___f0_15)); }
	inline uint64_t get_f0_15() const { return ___f0_15; }
	inline uint64_t* get_address_of_f0_15() { return &___f0_15; }
	inline void set_f0_15(uint64_t value)
	{
		___f0_15 = value;
	}
};

struct Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::blake2b_IV
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___blake2b_IV_0;
	// System.Byte[0...,0...] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2bDigest::blake2b_sigma
	ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* ___blake2b_sigma_1;

public:
	inline static int32_t get_offset_of_blake2b_IV_0() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A_StaticFields, ___blake2b_IV_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_blake2b_IV_0() const { return ___blake2b_IV_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_blake2b_IV_0() { return &___blake2b_IV_0; }
	inline void set_blake2b_IV_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___blake2b_IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___blake2b_IV_0), value);
	}

	inline static int32_t get_offset_of_blake2b_sigma_1() { return static_cast<int32_t>(offsetof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A_StaticFields, ___blake2b_sigma_1)); }
	inline ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* get_blake2b_sigma_1() const { return ___blake2b_sigma_1; }
	inline ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D** get_address_of_blake2b_sigma_1() { return &___blake2b_sigma_1; }
	inline void set_blake2b_sigma_1(ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* value)
	{
		___blake2b_sigma_1 = value;
		Il2CppCodeGenWriteBarrier((&___blake2b_sigma_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLAKE2BDIGEST_T6671A8E0151C6C2E9A278439C58497B09B0D7C9A_H
#ifndef BLAKE2SDIGEST_T8ACCF2420E9801976C59143DEB8EE148FC7F7422_H
#define BLAKE2SDIGEST_T8ACCF2420E9801976C59143DEB8EE148FC7F7422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest
struct  Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::digestLength
	int32_t ___digestLength_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::keyLength
	int32_t ___keyLength_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::personalization
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___personalization_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::bufferPos
	int32_t ___bufferPos_10;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::internalState
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___internalState_11;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::chainValue
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___chainValue_12;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::t0
	uint32_t ___t0_13;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::t1
	uint32_t ___t1_14;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::f0
	uint32_t ___f0_15;

public:
	inline static int32_t get_offset_of_digestLength_4() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___digestLength_4)); }
	inline int32_t get_digestLength_4() const { return ___digestLength_4; }
	inline int32_t* get_address_of_digestLength_4() { return &___digestLength_4; }
	inline void set_digestLength_4(int32_t value)
	{
		___digestLength_4 = value;
	}

	inline static int32_t get_offset_of_keyLength_5() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___keyLength_5)); }
	inline int32_t get_keyLength_5() const { return ___keyLength_5; }
	inline int32_t* get_address_of_keyLength_5() { return &___keyLength_5; }
	inline void set_keyLength_5(int32_t value)
	{
		___keyLength_5 = value;
	}

	inline static int32_t get_offset_of_salt_6() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___salt_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_6() const { return ___salt_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_6() { return &___salt_6; }
	inline void set_salt_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_6 = value;
		Il2CppCodeGenWriteBarrier((&___salt_6), value);
	}

	inline static int32_t get_offset_of_personalization_7() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___personalization_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_personalization_7() const { return ___personalization_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_personalization_7() { return &___personalization_7; }
	inline void set_personalization_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___personalization_7 = value;
		Il2CppCodeGenWriteBarrier((&___personalization_7), value);
	}

	inline static int32_t get_offset_of_key_8() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___key_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_8() const { return ___key_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_8() { return &___key_8; }
	inline void set_key_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_8 = value;
		Il2CppCodeGenWriteBarrier((&___key_8), value);
	}

	inline static int32_t get_offset_of_buffer_9() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___buffer_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_9() const { return ___buffer_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_9() { return &___buffer_9; }
	inline void set_buffer_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_9 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_9), value);
	}

	inline static int32_t get_offset_of_bufferPos_10() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___bufferPos_10)); }
	inline int32_t get_bufferPos_10() const { return ___bufferPos_10; }
	inline int32_t* get_address_of_bufferPos_10() { return &___bufferPos_10; }
	inline void set_bufferPos_10(int32_t value)
	{
		___bufferPos_10 = value;
	}

	inline static int32_t get_offset_of_internalState_11() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___internalState_11)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_internalState_11() const { return ___internalState_11; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_internalState_11() { return &___internalState_11; }
	inline void set_internalState_11(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___internalState_11 = value;
		Il2CppCodeGenWriteBarrier((&___internalState_11), value);
	}

	inline static int32_t get_offset_of_chainValue_12() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___chainValue_12)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_chainValue_12() const { return ___chainValue_12; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_chainValue_12() { return &___chainValue_12; }
	inline void set_chainValue_12(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___chainValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___chainValue_12), value);
	}

	inline static int32_t get_offset_of_t0_13() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___t0_13)); }
	inline uint32_t get_t0_13() const { return ___t0_13; }
	inline uint32_t* get_address_of_t0_13() { return &___t0_13; }
	inline void set_t0_13(uint32_t value)
	{
		___t0_13 = value;
	}

	inline static int32_t get_offset_of_t1_14() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___t1_14)); }
	inline uint32_t get_t1_14() const { return ___t1_14; }
	inline uint32_t* get_address_of_t1_14() { return &___t1_14; }
	inline void set_t1_14(uint32_t value)
	{
		___t1_14 = value;
	}

	inline static int32_t get_offset_of_f0_15() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422, ___f0_15)); }
	inline uint32_t get_f0_15() const { return ___f0_15; }
	inline uint32_t* get_address_of_f0_15() { return &___f0_15; }
	inline void set_f0_15(uint32_t value)
	{
		___f0_15 = value;
	}
};

struct Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::blake2s_IV
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___blake2s_IV_0;
	// System.Byte[0...,0...] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Blake2sDigest::blake2s_sigma
	ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* ___blake2s_sigma_1;

public:
	inline static int32_t get_offset_of_blake2s_IV_0() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422_StaticFields, ___blake2s_IV_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_blake2s_IV_0() const { return ___blake2s_IV_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_blake2s_IV_0() { return &___blake2s_IV_0; }
	inline void set_blake2s_IV_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___blake2s_IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___blake2s_IV_0), value);
	}

	inline static int32_t get_offset_of_blake2s_sigma_1() { return static_cast<int32_t>(offsetof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422_StaticFields, ___blake2s_sigma_1)); }
	inline ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* get_blake2s_sigma_1() const { return ___blake2s_sigma_1; }
	inline ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D** get_address_of_blake2s_sigma_1() { return &___blake2s_sigma_1; }
	inline void set_blake2s_sigma_1(ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* value)
	{
		___blake2s_sigma_1 = value;
		Il2CppCodeGenWriteBarrier((&___blake2s_sigma_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLAKE2SDIGEST_T8ACCF2420E9801976C59143DEB8EE148FC7F7422_H
#ifndef DSTU7564DIGEST_T18C139AC1198FBDD591BC54693DA7390B66E785E_H
#define DSTU7564DIGEST_T18C139AC1198FBDD591BC54693DA7390B66E785E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest
struct  Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::hashSize
	int32_t ___hashSize_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::blockSize
	int32_t ___blockSize_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::columns
	int32_t ___columns_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::rounds
	int32_t ___rounds_7;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::state
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___state_8;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::tempState1
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___tempState1_9;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::tempState2
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___tempState2_10;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::inputBlocks
	uint64_t ___inputBlocks_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::bufOff
	int32_t ___bufOff_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_13;

public:
	inline static int32_t get_offset_of_hashSize_4() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___hashSize_4)); }
	inline int32_t get_hashSize_4() const { return ___hashSize_4; }
	inline int32_t* get_address_of_hashSize_4() { return &___hashSize_4; }
	inline void set_hashSize_4(int32_t value)
	{
		___hashSize_4 = value;
	}

	inline static int32_t get_offset_of_blockSize_5() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___blockSize_5)); }
	inline int32_t get_blockSize_5() const { return ___blockSize_5; }
	inline int32_t* get_address_of_blockSize_5() { return &___blockSize_5; }
	inline void set_blockSize_5(int32_t value)
	{
		___blockSize_5 = value;
	}

	inline static int32_t get_offset_of_columns_6() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___columns_6)); }
	inline int32_t get_columns_6() const { return ___columns_6; }
	inline int32_t* get_address_of_columns_6() { return &___columns_6; }
	inline void set_columns_6(int32_t value)
	{
		___columns_6 = value;
	}

	inline static int32_t get_offset_of_rounds_7() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___rounds_7)); }
	inline int32_t get_rounds_7() const { return ___rounds_7; }
	inline int32_t* get_address_of_rounds_7() { return &___rounds_7; }
	inline void set_rounds_7(int32_t value)
	{
		___rounds_7 = value;
	}

	inline static int32_t get_offset_of_state_8() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___state_8)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_state_8() const { return ___state_8; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_state_8() { return &___state_8; }
	inline void set_state_8(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___state_8 = value;
		Il2CppCodeGenWriteBarrier((&___state_8), value);
	}

	inline static int32_t get_offset_of_tempState1_9() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___tempState1_9)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_tempState1_9() const { return ___tempState1_9; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_tempState1_9() { return &___tempState1_9; }
	inline void set_tempState1_9(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___tempState1_9 = value;
		Il2CppCodeGenWriteBarrier((&___tempState1_9), value);
	}

	inline static int32_t get_offset_of_tempState2_10() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___tempState2_10)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_tempState2_10() const { return ___tempState2_10; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_tempState2_10() { return &___tempState2_10; }
	inline void set_tempState2_10(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___tempState2_10 = value;
		Il2CppCodeGenWriteBarrier((&___tempState2_10), value);
	}

	inline static int32_t get_offset_of_inputBlocks_11() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___inputBlocks_11)); }
	inline uint64_t get_inputBlocks_11() const { return ___inputBlocks_11; }
	inline uint64_t* get_address_of_inputBlocks_11() { return &___inputBlocks_11; }
	inline void set_inputBlocks_11(uint64_t value)
	{
		___inputBlocks_11 = value;
	}

	inline static int32_t get_offset_of_bufOff_12() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___bufOff_12)); }
	inline int32_t get_bufOff_12() const { return ___bufOff_12; }
	inline int32_t* get_address_of_bufOff_12() { return &___bufOff_12; }
	inline void set_bufOff_12(int32_t value)
	{
		___bufOff_12 = value;
	}

	inline static int32_t get_offset_of_buf_13() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E, ___buf_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_13() const { return ___buf_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_13() { return &___buf_13; }
	inline void set_buf_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_13 = value;
		Il2CppCodeGenWriteBarrier((&___buf_13), value);
	}
};

struct Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::S0
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S0_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::S1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S1_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::S2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S2_16;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Dstu7564Digest::S3
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S3_17;

public:
	inline static int32_t get_offset_of_S0_14() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields, ___S0_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S0_14() const { return ___S0_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S0_14() { return &___S0_14; }
	inline void set_S0_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S0_14 = value;
		Il2CppCodeGenWriteBarrier((&___S0_14), value);
	}

	inline static int32_t get_offset_of_S1_15() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields, ___S1_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S1_15() const { return ___S1_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S1_15() { return &___S1_15; }
	inline void set_S1_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S1_15 = value;
		Il2CppCodeGenWriteBarrier((&___S1_15), value);
	}

	inline static int32_t get_offset_of_S2_16() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields, ___S2_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S2_16() const { return ___S2_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S2_16() { return &___S2_16; }
	inline void set_S2_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S2_16 = value;
		Il2CppCodeGenWriteBarrier((&___S2_16), value);
	}

	inline static int32_t get_offset_of_S3_17() { return static_cast<int32_t>(offsetof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields, ___S3_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S3_17() const { return ___S3_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S3_17() { return &___S3_17; }
	inline void set_S3_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S3_17 = value;
		Il2CppCodeGenWriteBarrier((&___S3_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSTU7564DIGEST_T18C139AC1198FBDD591BC54693DA7390B66E785E_H
#ifndef GENERALDIGEST_T30737BEEA96249128E61B761F593A2C44E86F2EA_H
#define GENERALDIGEST_T30737BEEA96249128E61B761F593A2C44E86F2EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest
struct  GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest::xBufOff
	int32_t ___xBufOff_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.GeneralDigest::byteCount
	int64_t ___byteCount_3;

public:
	inline static int32_t get_offset_of_xBuf_1() { return static_cast<int32_t>(offsetof(GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA, ___xBuf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_1() const { return ___xBuf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_1() { return &___xBuf_1; }
	inline void set_xBuf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_1), value);
	}

	inline static int32_t get_offset_of_xBufOff_2() { return static_cast<int32_t>(offsetof(GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA, ___xBufOff_2)); }
	inline int32_t get_xBufOff_2() const { return ___xBufOff_2; }
	inline int32_t* get_address_of_xBufOff_2() { return &___xBufOff_2; }
	inline void set_xBufOff_2(int32_t value)
	{
		___xBufOff_2 = value;
	}

	inline static int32_t get_offset_of_byteCount_3() { return static_cast<int32_t>(offsetof(GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA, ___byteCount_3)); }
	inline int64_t get_byteCount_3() const { return ___byteCount_3; }
	inline int64_t* get_address_of_byteCount_3() { return &___byteCount_3; }
	inline void set_byteCount_3(int64_t value)
	{
		___byteCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALDIGEST_T30737BEEA96249128E61B761F593A2C44E86F2EA_H
#ifndef GOST3411DIGEST_TD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_H
#define GOST3411DIGEST_TD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest
struct  Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::L
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___L_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::M
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___M_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::Sum
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sum_4;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::C
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___C_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::xBufOff
	int32_t ___xBufOff_7;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::byteCount
	uint64_t ___byteCount_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::cipher
	RuntimeObject* ___cipher_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::sBox
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sBox_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::K
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___K_11;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::a
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___a_12;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::wS
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___wS_13;
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::w_S
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___w_S_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::U
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U_16;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::V
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___V_17;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::W
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___W_18;

public:
	inline static int32_t get_offset_of_H_1() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___H_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_1() const { return ___H_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_1() { return &___H_1; }
	inline void set_H_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_1 = value;
		Il2CppCodeGenWriteBarrier((&___H_1), value);
	}

	inline static int32_t get_offset_of_L_2() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___L_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_L_2() const { return ___L_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_L_2() { return &___L_2; }
	inline void set_L_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___L_2 = value;
		Il2CppCodeGenWriteBarrier((&___L_2), value);
	}

	inline static int32_t get_offset_of_M_3() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___M_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_M_3() const { return ___M_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_M_3() { return &___M_3; }
	inline void set_M_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___M_3 = value;
		Il2CppCodeGenWriteBarrier((&___M_3), value);
	}

	inline static int32_t get_offset_of_Sum_4() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___Sum_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sum_4() const { return ___Sum_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sum_4() { return &___Sum_4; }
	inline void set_Sum_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sum_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sum_4), value);
	}

	inline static int32_t get_offset_of_C_5() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___C_5)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_C_5() const { return ___C_5; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_C_5() { return &___C_5; }
	inline void set_C_5(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___C_5 = value;
		Il2CppCodeGenWriteBarrier((&___C_5), value);
	}

	inline static int32_t get_offset_of_xBuf_6() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___xBuf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_6() const { return ___xBuf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_6() { return &___xBuf_6; }
	inline void set_xBuf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_6 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_6), value);
	}

	inline static int32_t get_offset_of_xBufOff_7() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___xBufOff_7)); }
	inline int32_t get_xBufOff_7() const { return ___xBufOff_7; }
	inline int32_t* get_address_of_xBufOff_7() { return &___xBufOff_7; }
	inline void set_xBufOff_7(int32_t value)
	{
		___xBufOff_7 = value;
	}

	inline static int32_t get_offset_of_byteCount_8() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___byteCount_8)); }
	inline uint64_t get_byteCount_8() const { return ___byteCount_8; }
	inline uint64_t* get_address_of_byteCount_8() { return &___byteCount_8; }
	inline void set_byteCount_8(uint64_t value)
	{
		___byteCount_8 = value;
	}

	inline static int32_t get_offset_of_cipher_9() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___cipher_9)); }
	inline RuntimeObject* get_cipher_9() const { return ___cipher_9; }
	inline RuntimeObject** get_address_of_cipher_9() { return &___cipher_9; }
	inline void set_cipher_9(RuntimeObject* value)
	{
		___cipher_9 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_9), value);
	}

	inline static int32_t get_offset_of_sBox_10() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___sBox_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sBox_10() const { return ___sBox_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sBox_10() { return &___sBox_10; }
	inline void set_sBox_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sBox_10 = value;
		Il2CppCodeGenWriteBarrier((&___sBox_10), value);
	}

	inline static int32_t get_offset_of_K_11() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___K_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_K_11() const { return ___K_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_K_11() { return &___K_11; }
	inline void set_K_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___K_11 = value;
		Il2CppCodeGenWriteBarrier((&___K_11), value);
	}

	inline static int32_t get_offset_of_a_12() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___a_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_a_12() const { return ___a_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_a_12() { return &___a_12; }
	inline void set_a_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___a_12 = value;
		Il2CppCodeGenWriteBarrier((&___a_12), value);
	}

	inline static int32_t get_offset_of_wS_13() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___wS_13)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_wS_13() const { return ___wS_13; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_wS_13() { return &___wS_13; }
	inline void set_wS_13(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___wS_13 = value;
		Il2CppCodeGenWriteBarrier((&___wS_13), value);
	}

	inline static int32_t get_offset_of_w_S_14() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___w_S_14)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_w_S_14() const { return ___w_S_14; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_w_S_14() { return &___w_S_14; }
	inline void set_w_S_14(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___w_S_14 = value;
		Il2CppCodeGenWriteBarrier((&___w_S_14), value);
	}

	inline static int32_t get_offset_of_S_15() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___S_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_15() const { return ___S_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_15() { return &___S_15; }
	inline void set_S_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_15 = value;
		Il2CppCodeGenWriteBarrier((&___S_15), value);
	}

	inline static int32_t get_offset_of_U_16() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___U_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U_16() const { return ___U_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U_16() { return &___U_16; }
	inline void set_U_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U_16 = value;
		Il2CppCodeGenWriteBarrier((&___U_16), value);
	}

	inline static int32_t get_offset_of_V_17() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___V_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_V_17() const { return ___V_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_V_17() { return &___V_17; }
	inline void set_V_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___V_17 = value;
		Il2CppCodeGenWriteBarrier((&___V_17), value);
	}

	inline static int32_t get_offset_of_W_18() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865, ___W_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_W_18() const { return ___W_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_W_18() { return &___W_18; }
	inline void set_W_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___W_18 = value;
		Il2CppCodeGenWriteBarrier((&___W_18), value);
	}
};

struct Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411Digest::C2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___C2_19;

public:
	inline static int32_t get_offset_of_C2_19() { return static_cast<int32_t>(offsetof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_StaticFields, ___C2_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_C2_19() const { return ___C2_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_C2_19() { return &___C2_19; }
	inline void set_C2_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___C2_19 = value;
		Il2CppCodeGenWriteBarrier((&___C2_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3411DIGEST_TD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_H
#ifndef GOST3411_2012DIGEST_TC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_H
#define GOST3411_2012DIGEST_TC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest
struct  Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::N
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___N_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::Sigma
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sigma_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::Ki
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Ki_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::m
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::h
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___h_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::tmp
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tmp_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::block
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___block_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::bOff
	int32_t ___bOff_8;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_N_1() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___N_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_N_1() const { return ___N_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_N_1() { return &___N_1; }
	inline void set_N_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___N_1 = value;
		Il2CppCodeGenWriteBarrier((&___N_1), value);
	}

	inline static int32_t get_offset_of_Sigma_2() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___Sigma_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sigma_2() const { return ___Sigma_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sigma_2() { return &___Sigma_2; }
	inline void set_Sigma_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sigma_2 = value;
		Il2CppCodeGenWriteBarrier((&___Sigma_2), value);
	}

	inline static int32_t get_offset_of_Ki_3() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___Ki_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Ki_3() const { return ___Ki_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Ki_3() { return &___Ki_3; }
	inline void set_Ki_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Ki_3 = value;
		Il2CppCodeGenWriteBarrier((&___Ki_3), value);
	}

	inline static int32_t get_offset_of_m_4() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___m_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_4() const { return ___m_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_4() { return &___m_4; }
	inline void set_m_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_4), value);
	}

	inline static int32_t get_offset_of_h_5() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___h_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_h_5() const { return ___h_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_h_5() { return &___h_5; }
	inline void set_h_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___h_5 = value;
		Il2CppCodeGenWriteBarrier((&___h_5), value);
	}

	inline static int32_t get_offset_of_tmp_6() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___tmp_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tmp_6() const { return ___tmp_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tmp_6() { return &___tmp_6; }
	inline void set_tmp_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tmp_6 = value;
		Il2CppCodeGenWriteBarrier((&___tmp_6), value);
	}

	inline static int32_t get_offset_of_block_7() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___block_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_block_7() const { return ___block_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_block_7() { return &___block_7; }
	inline void set_block_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___block_7 = value;
		Il2CppCodeGenWriteBarrier((&___block_7), value);
	}

	inline static int32_t get_offset_of_bOff_8() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9, ___bOff_8)); }
	inline int32_t get_bOff_8() const { return ___bOff_8; }
	inline int32_t* get_address_of_bOff_8() { return &___bOff_8; }
	inline void set_bOff_8(int32_t value)
	{
		___bOff_8 = value;
	}
};

struct Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields
{
public:
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::C
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___C_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::Zero
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Zero_10;
	// System.UInt64[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012Digest::T
	UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2* ___T_11;

public:
	inline static int32_t get_offset_of_C_9() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields, ___C_9)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_C_9() const { return ___C_9; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_C_9() { return &___C_9; }
	inline void set_C_9(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___C_9 = value;
		Il2CppCodeGenWriteBarrier((&___C_9), value);
	}

	inline static int32_t get_offset_of_Zero_10() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields, ___Zero_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Zero_10() const { return ___Zero_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Zero_10() { return &___Zero_10; }
	inline void set_Zero_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Zero_10 = value;
		Il2CppCodeGenWriteBarrier((&___Zero_10), value);
	}

	inline static int32_t get_offset_of_T_11() { return static_cast<int32_t>(offsetof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields, ___T_11)); }
	inline UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2* get_T_11() const { return ___T_11; }
	inline UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2** get_address_of_T_11() { return &___T_11; }
	inline void set_T_11(UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2* value)
	{
		___T_11 = value;
		Il2CppCodeGenWriteBarrier((&___T_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3411_2012DIGEST_TC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_H
#ifndef KECCAKDIGEST_TC0937665833293C5C98902C767A0DD60E0277A32_H
#define KECCAKDIGEST_TC0937665833293C5C98902C767A0DD60E0277A32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest
struct  KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32  : public RuntimeObject
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::state
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___state_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::dataQueue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___dataQueue_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::rate
	int32_t ___rate_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::bitsInQueue
	int32_t ___bitsInQueue_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::fixedOutputLength
	int32_t ___fixedOutputLength_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::squeezing
	bool ___squeezing_6;

public:
	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___state_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_state_1() const { return ___state_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___state_1 = value;
		Il2CppCodeGenWriteBarrier((&___state_1), value);
	}

	inline static int32_t get_offset_of_dataQueue_2() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___dataQueue_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_dataQueue_2() const { return ___dataQueue_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_dataQueue_2() { return &___dataQueue_2; }
	inline void set_dataQueue_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___dataQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataQueue_2), value);
	}

	inline static int32_t get_offset_of_rate_3() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___rate_3)); }
	inline int32_t get_rate_3() const { return ___rate_3; }
	inline int32_t* get_address_of_rate_3() { return &___rate_3; }
	inline void set_rate_3(int32_t value)
	{
		___rate_3 = value;
	}

	inline static int32_t get_offset_of_bitsInQueue_4() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___bitsInQueue_4)); }
	inline int32_t get_bitsInQueue_4() const { return ___bitsInQueue_4; }
	inline int32_t* get_address_of_bitsInQueue_4() { return &___bitsInQueue_4; }
	inline void set_bitsInQueue_4(int32_t value)
	{
		___bitsInQueue_4 = value;
	}

	inline static int32_t get_offset_of_fixedOutputLength_5() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___fixedOutputLength_5)); }
	inline int32_t get_fixedOutputLength_5() const { return ___fixedOutputLength_5; }
	inline int32_t* get_address_of_fixedOutputLength_5() { return &___fixedOutputLength_5; }
	inline void set_fixedOutputLength_5(int32_t value)
	{
		___fixedOutputLength_5 = value;
	}

	inline static int32_t get_offset_of_squeezing_6() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32, ___squeezing_6)); }
	inline bool get_squeezing_6() const { return ___squeezing_6; }
	inline bool* get_address_of_squeezing_6() { return &___squeezing_6; }
	inline void set_squeezing_6(bool value)
	{
		___squeezing_6 = value;
	}
};

struct KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.KeccakDigest::KeccakRoundConstants
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___KeccakRoundConstants_0;

public:
	inline static int32_t get_offset_of_KeccakRoundConstants_0() { return static_cast<int32_t>(offsetof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32_StaticFields, ___KeccakRoundConstants_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_KeccakRoundConstants_0() const { return ___KeccakRoundConstants_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_KeccakRoundConstants_0() { return &___KeccakRoundConstants_0; }
	inline void set_KeccakRoundConstants_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___KeccakRoundConstants_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeccakRoundConstants_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KECCAKDIGEST_TC0937665833293C5C98902C767A0DD60E0277A32_H
#ifndef LONGDIGEST_T23D3C83A6B4E5D9B9B507888AD068F22886B5203_H
#define LONGDIGEST_T23D3C83A6B4E5D9B9B507888AD068F22886B5203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest
struct  LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::MyByteLength
	int32_t ___MyByteLength_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::xBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___xBuf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::xBufOff
	int32_t ___xBufOff_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::byteCount1
	int64_t ___byteCount1_3;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::byteCount2
	int64_t ___byteCount2_4;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H1
	uint64_t ___H1_5;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H2
	uint64_t ___H2_6;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H3
	uint64_t ___H3_7;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H4
	uint64_t ___H4_8;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H5
	uint64_t ___H5_9;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H6
	uint64_t ___H6_10;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H7
	uint64_t ___H7_11;
	// System.UInt64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::H8
	uint64_t ___H8_12;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::W
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___W_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::wOff
	int32_t ___wOff_14;

public:
	inline static int32_t get_offset_of_MyByteLength_0() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___MyByteLength_0)); }
	inline int32_t get_MyByteLength_0() const { return ___MyByteLength_0; }
	inline int32_t* get_address_of_MyByteLength_0() { return &___MyByteLength_0; }
	inline void set_MyByteLength_0(int32_t value)
	{
		___MyByteLength_0 = value;
	}

	inline static int32_t get_offset_of_xBuf_1() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___xBuf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_xBuf_1() const { return ___xBuf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_xBuf_1() { return &___xBuf_1; }
	inline void set_xBuf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___xBuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___xBuf_1), value);
	}

	inline static int32_t get_offset_of_xBufOff_2() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___xBufOff_2)); }
	inline int32_t get_xBufOff_2() const { return ___xBufOff_2; }
	inline int32_t* get_address_of_xBufOff_2() { return &___xBufOff_2; }
	inline void set_xBufOff_2(int32_t value)
	{
		___xBufOff_2 = value;
	}

	inline static int32_t get_offset_of_byteCount1_3() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___byteCount1_3)); }
	inline int64_t get_byteCount1_3() const { return ___byteCount1_3; }
	inline int64_t* get_address_of_byteCount1_3() { return &___byteCount1_3; }
	inline void set_byteCount1_3(int64_t value)
	{
		___byteCount1_3 = value;
	}

	inline static int32_t get_offset_of_byteCount2_4() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___byteCount2_4)); }
	inline int64_t get_byteCount2_4() const { return ___byteCount2_4; }
	inline int64_t* get_address_of_byteCount2_4() { return &___byteCount2_4; }
	inline void set_byteCount2_4(int64_t value)
	{
		___byteCount2_4 = value;
	}

	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H1_5)); }
	inline uint64_t get_H1_5() const { return ___H1_5; }
	inline uint64_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint64_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H2_6)); }
	inline uint64_t get_H2_6() const { return ___H2_6; }
	inline uint64_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint64_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H3_7)); }
	inline uint64_t get_H3_7() const { return ___H3_7; }
	inline uint64_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint64_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H4_8)); }
	inline uint64_t get_H4_8() const { return ___H4_8; }
	inline uint64_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint64_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_H5_9() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H5_9)); }
	inline uint64_t get_H5_9() const { return ___H5_9; }
	inline uint64_t* get_address_of_H5_9() { return &___H5_9; }
	inline void set_H5_9(uint64_t value)
	{
		___H5_9 = value;
	}

	inline static int32_t get_offset_of_H6_10() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H6_10)); }
	inline uint64_t get_H6_10() const { return ___H6_10; }
	inline uint64_t* get_address_of_H6_10() { return &___H6_10; }
	inline void set_H6_10(uint64_t value)
	{
		___H6_10 = value;
	}

	inline static int32_t get_offset_of_H7_11() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H7_11)); }
	inline uint64_t get_H7_11() const { return ___H7_11; }
	inline uint64_t* get_address_of_H7_11() { return &___H7_11; }
	inline void set_H7_11(uint64_t value)
	{
		___H7_11 = value;
	}

	inline static int32_t get_offset_of_H8_12() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___H8_12)); }
	inline uint64_t get_H8_12() const { return ___H8_12; }
	inline uint64_t* get_address_of_H8_12() { return &___H8_12; }
	inline void set_H8_12(uint64_t value)
	{
		___H8_12 = value;
	}

	inline static int32_t get_offset_of_W_13() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___W_13)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_W_13() const { return ___W_13; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_W_13() { return &___W_13; }
	inline void set_W_13(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___W_13 = value;
		Il2CppCodeGenWriteBarrier((&___W_13), value);
	}

	inline static int32_t get_offset_of_wOff_14() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203, ___wOff_14)); }
	inline int32_t get_wOff_14() const { return ___wOff_14; }
	inline int32_t* get_address_of_wOff_14() { return &___wOff_14; }
	inline void set_wOff_14(int32_t value)
	{
		___wOff_14 = value;
	}
};

struct LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.LongDigest::K
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___K_15;

public:
	inline static int32_t get_offset_of_K_15() { return static_cast<int32_t>(offsetof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203_StaticFields, ___K_15)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_K_15() const { return ___K_15; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_K_15() { return &___K_15; }
	inline void set_K_15(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___K_15 = value;
		Il2CppCodeGenWriteBarrier((&___K_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGDIGEST_T23D3C83A6B4E5D9B9B507888AD068F22886B5203_H
#ifndef MD2DIGEST_TB481F9F4007120CA649DCDEF9A3D7064F898208F_H
#define MD2DIGEST_TB481F9F4007120CA649DCDEF9A3D7064F898208F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest
struct  MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::X
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___X_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::xOff
	int32_t ___xOff_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::M
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___M_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::mOff
	int32_t ___mOff_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::C
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___C_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::COff
	int32_t ___COff_7;

public:
	inline static int32_t get_offset_of_X_2() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F, ___X_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_X_2() const { return ___X_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_X_2() { return &___X_2; }
	inline void set_X_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___X_2 = value;
		Il2CppCodeGenWriteBarrier((&___X_2), value);
	}

	inline static int32_t get_offset_of_xOff_3() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F, ___xOff_3)); }
	inline int32_t get_xOff_3() const { return ___xOff_3; }
	inline int32_t* get_address_of_xOff_3() { return &___xOff_3; }
	inline void set_xOff_3(int32_t value)
	{
		___xOff_3 = value;
	}

	inline static int32_t get_offset_of_M_4() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F, ___M_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_M_4() const { return ___M_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_M_4() { return &___M_4; }
	inline void set_M_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___M_4 = value;
		Il2CppCodeGenWriteBarrier((&___M_4), value);
	}

	inline static int32_t get_offset_of_mOff_5() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F, ___mOff_5)); }
	inline int32_t get_mOff_5() const { return ___mOff_5; }
	inline int32_t* get_address_of_mOff_5() { return &___mOff_5; }
	inline void set_mOff_5(int32_t value)
	{
		___mOff_5 = value;
	}

	inline static int32_t get_offset_of_C_6() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F, ___C_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_C_6() const { return ___C_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_C_6() { return &___C_6; }
	inline void set_C_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___C_6 = value;
		Il2CppCodeGenWriteBarrier((&___C_6), value);
	}

	inline static int32_t get_offset_of_COff_7() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F, ___COff_7)); }
	inline int32_t get_COff_7() const { return ___COff_7; }
	inline int32_t* get_address_of_COff_7() { return &___COff_7; }
	inline void set_COff_7(int32_t value)
	{
		___COff_7 = value;
	}
};

struct MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD2Digest::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_8;

public:
	inline static int32_t get_offset_of_S_8() { return static_cast<int32_t>(offsetof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F_StaticFields, ___S_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_8() const { return ___S_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_8() { return &___S_8; }
	inline void set_S_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_8 = value;
		Il2CppCodeGenWriteBarrier((&___S_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD2DIGEST_TB481F9F4007120CA649DCDEF9A3D7064F898208F_H
#ifndef NONMEMOABLEDIGEST_T492160D0F28E00EC004BAA82F2F31A9C1F6B43E6_H
#define NONMEMOABLEDIGEST_T492160D0F28E00EC004BAA82F2F31A9C1F6B43E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.NonMemoableDigest
struct  NonMemoableDigest_t492160D0F28E00EC004BAA82F2F31A9C1F6B43E6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.NonMemoableDigest::mBaseDigest
	RuntimeObject* ___mBaseDigest_0;

public:
	inline static int32_t get_offset_of_mBaseDigest_0() { return static_cast<int32_t>(offsetof(NonMemoableDigest_t492160D0F28E00EC004BAA82F2F31A9C1F6B43E6, ___mBaseDigest_0)); }
	inline RuntimeObject* get_mBaseDigest_0() const { return ___mBaseDigest_0; }
	inline RuntimeObject** get_address_of_mBaseDigest_0() { return &___mBaseDigest_0; }
	inline void set_mBaseDigest_0(RuntimeObject* value)
	{
		___mBaseDigest_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBaseDigest_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONMEMOABLEDIGEST_T492160D0F28E00EC004BAA82F2F31A9C1F6B43E6_H
#ifndef NULLDIGEST_T15A8C389FB3A4A06093B8092220246098A384DA6_H
#define NULLDIGEST_T15A8C389FB3A4A06093B8092220246098A384DA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.NullDigest
struct  NullDigest_t15A8C389FB3A4A06093B8092220246098A384DA6  : public RuntimeObject
{
public:
	// System.IO.MemoryStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.NullDigest::bOut
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___bOut_0;

public:
	inline static int32_t get_offset_of_bOut_0() { return static_cast<int32_t>(offsetof(NullDigest_t15A8C389FB3A4A06093B8092220246098A384DA6, ___bOut_0)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_bOut_0() const { return ___bOut_0; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_bOut_0() { return &___bOut_0; }
	inline void set_bOut_0(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___bOut_0 = value;
		Il2CppCodeGenWriteBarrier((&___bOut_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLDIGEST_T15A8C389FB3A4A06093B8092220246098A384DA6_H
#ifndef CUSTOMNAMEDCURVES_T715866532C927271C699D3A651AC8DEC2A9050F9_H
#define CUSTOMNAMEDCURVES_T715866532C927271C699D3A651AC8DEC2A9050F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves
struct  CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9  : public RuntimeObject
{
public:

public:
};

struct CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves::nameToCurve
	RuntimeObject* ___nameToCurve_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves::nameToOid
	RuntimeObject* ___nameToOid_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves::oidToCurve
	RuntimeObject* ___oidToCurve_2;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves::oidToName
	RuntimeObject* ___oidToName_3;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves::names
	RuntimeObject* ___names_4;

public:
	inline static int32_t get_offset_of_nameToCurve_0() { return static_cast<int32_t>(offsetof(CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields, ___nameToCurve_0)); }
	inline RuntimeObject* get_nameToCurve_0() const { return ___nameToCurve_0; }
	inline RuntimeObject** get_address_of_nameToCurve_0() { return &___nameToCurve_0; }
	inline void set_nameToCurve_0(RuntimeObject* value)
	{
		___nameToCurve_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameToCurve_0), value);
	}

	inline static int32_t get_offset_of_nameToOid_1() { return static_cast<int32_t>(offsetof(CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields, ___nameToOid_1)); }
	inline RuntimeObject* get_nameToOid_1() const { return ___nameToOid_1; }
	inline RuntimeObject** get_address_of_nameToOid_1() { return &___nameToOid_1; }
	inline void set_nameToOid_1(RuntimeObject* value)
	{
		___nameToOid_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameToOid_1), value);
	}

	inline static int32_t get_offset_of_oidToCurve_2() { return static_cast<int32_t>(offsetof(CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields, ___oidToCurve_2)); }
	inline RuntimeObject* get_oidToCurve_2() const { return ___oidToCurve_2; }
	inline RuntimeObject** get_address_of_oidToCurve_2() { return &___oidToCurve_2; }
	inline void set_oidToCurve_2(RuntimeObject* value)
	{
		___oidToCurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___oidToCurve_2), value);
	}

	inline static int32_t get_offset_of_oidToName_3() { return static_cast<int32_t>(offsetof(CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields, ___oidToName_3)); }
	inline RuntimeObject* get_oidToName_3() const { return ___oidToName_3; }
	inline RuntimeObject** get_address_of_oidToName_3() { return &___oidToName_3; }
	inline void set_oidToName_3(RuntimeObject* value)
	{
		___oidToName_3 = value;
		Il2CppCodeGenWriteBarrier((&___oidToName_3), value);
	}

	inline static int32_t get_offset_of_names_4() { return static_cast<int32_t>(offsetof(CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields, ___names_4)); }
	inline RuntimeObject* get_names_4() const { return ___names_4; }
	inline RuntimeObject** get_address_of_names_4() { return &___names_4; }
	inline void set_names_4(RuntimeObject* value)
	{
		___names_4 = value;
		Il2CppCodeGenWriteBarrier((&___names_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNAMEDCURVES_T715866532C927271C699D3A651AC8DEC2A9050F9_H
#ifndef ISO9796D1ENCODING_T0E174BA61E5F29155C8CAE521A1AAB07E35B1957_H
#define ISO9796D1ENCODING_T0E174BA61E5F29155C8CAE521A1AAB07E35B1957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding
struct  ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::engine
	RuntimeObject* ___engine_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::forEncryption
	bool ___forEncryption_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::bitSize
	int32_t ___bitSize_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::padBits
	int32_t ___padBits_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::modulus
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___modulus_8;

public:
	inline static int32_t get_offset_of_engine_4() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957, ___engine_4)); }
	inline RuntimeObject* get_engine_4() const { return ___engine_4; }
	inline RuntimeObject** get_address_of_engine_4() { return &___engine_4; }
	inline void set_engine_4(RuntimeObject* value)
	{
		___engine_4 = value;
		Il2CppCodeGenWriteBarrier((&___engine_4), value);
	}

	inline static int32_t get_offset_of_forEncryption_5() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957, ___forEncryption_5)); }
	inline bool get_forEncryption_5() const { return ___forEncryption_5; }
	inline bool* get_address_of_forEncryption_5() { return &___forEncryption_5; }
	inline void set_forEncryption_5(bool value)
	{
		___forEncryption_5 = value;
	}

	inline static int32_t get_offset_of_bitSize_6() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957, ___bitSize_6)); }
	inline int32_t get_bitSize_6() const { return ___bitSize_6; }
	inline int32_t* get_address_of_bitSize_6() { return &___bitSize_6; }
	inline void set_bitSize_6(int32_t value)
	{
		___bitSize_6 = value;
	}

	inline static int32_t get_offset_of_padBits_7() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957, ___padBits_7)); }
	inline int32_t get_padBits_7() const { return ___padBits_7; }
	inline int32_t* get_address_of_padBits_7() { return &___padBits_7; }
	inline void set_padBits_7(int32_t value)
	{
		___padBits_7 = value;
	}

	inline static int32_t get_offset_of_modulus_8() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957, ___modulus_8)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_modulus_8() const { return ___modulus_8; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_modulus_8() { return &___modulus_8; }
	inline void set_modulus_8(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___modulus_8 = value;
		Il2CppCodeGenWriteBarrier((&___modulus_8), value);
	}
};

struct ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::Sixteen
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Sixteen_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::Six
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Six_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::shadows
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___shadows_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.ISO9796d1Encoding::inverse
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___inverse_3;

public:
	inline static int32_t get_offset_of_Sixteen_0() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields, ___Sixteen_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Sixteen_0() const { return ___Sixteen_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Sixteen_0() { return &___Sixteen_0; }
	inline void set_Sixteen_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Sixteen_0 = value;
		Il2CppCodeGenWriteBarrier((&___Sixteen_0), value);
	}

	inline static int32_t get_offset_of_Six_1() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields, ___Six_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Six_1() const { return ___Six_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Six_1() { return &___Six_1; }
	inline void set_Six_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Six_1 = value;
		Il2CppCodeGenWriteBarrier((&___Six_1), value);
	}

	inline static int32_t get_offset_of_shadows_2() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields, ___shadows_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_shadows_2() const { return ___shadows_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_shadows_2() { return &___shadows_2; }
	inline void set_shadows_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___shadows_2 = value;
		Il2CppCodeGenWriteBarrier((&___shadows_2), value);
	}

	inline static int32_t get_offset_of_inverse_3() { return static_cast<int32_t>(offsetof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields, ___inverse_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_inverse_3() const { return ___inverse_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_inverse_3() { return &___inverse_3; }
	inline void set_inverse_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___inverse_3 = value;
		Il2CppCodeGenWriteBarrier((&___inverse_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO9796D1ENCODING_T0E174BA61E5F29155C8CAE521A1AAB07E35B1957_H
#ifndef OAEPENCODING_T7D9038FB221221A2AB3852750F220922D510A7DF_H
#define OAEPENCODING_T7D9038FB221221A2AB3852750F220922D510A7DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.OaepEncoding
struct  OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.OaepEncoding::defHash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___defHash_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.OaepEncoding::mgf1Hash
	RuntimeObject* ___mgf1Hash_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.OaepEncoding::engine
	RuntimeObject* ___engine_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.OaepEncoding::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.OaepEncoding::forEncryption
	bool ___forEncryption_4;

public:
	inline static int32_t get_offset_of_defHash_0() { return static_cast<int32_t>(offsetof(OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF, ___defHash_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_defHash_0() const { return ___defHash_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_defHash_0() { return &___defHash_0; }
	inline void set_defHash_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___defHash_0 = value;
		Il2CppCodeGenWriteBarrier((&___defHash_0), value);
	}

	inline static int32_t get_offset_of_mgf1Hash_1() { return static_cast<int32_t>(offsetof(OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF, ___mgf1Hash_1)); }
	inline RuntimeObject* get_mgf1Hash_1() const { return ___mgf1Hash_1; }
	inline RuntimeObject** get_address_of_mgf1Hash_1() { return &___mgf1Hash_1; }
	inline void set_mgf1Hash_1(RuntimeObject* value)
	{
		___mgf1Hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___mgf1Hash_1), value);
	}

	inline static int32_t get_offset_of_engine_2() { return static_cast<int32_t>(offsetof(OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF, ___engine_2)); }
	inline RuntimeObject* get_engine_2() const { return ___engine_2; }
	inline RuntimeObject** get_address_of_engine_2() { return &___engine_2; }
	inline void set_engine_2(RuntimeObject* value)
	{
		___engine_2 = value;
		Il2CppCodeGenWriteBarrier((&___engine_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}

	inline static int32_t get_offset_of_forEncryption_4() { return static_cast<int32_t>(offsetof(OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF, ___forEncryption_4)); }
	inline bool get_forEncryption_4() const { return ___forEncryption_4; }
	inline bool* get_address_of_forEncryption_4() { return &___forEncryption_4; }
	inline void set_forEncryption_4(bool value)
	{
		___forEncryption_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OAEPENCODING_T7D9038FB221221A2AB3852750F220922D510A7DF_H
#ifndef PKCS1ENCODING_TBA70E2A21F4F37A5819E46E1625F014D83F66080_H
#define PKCS1ENCODING_TBA70E2A21F4F37A5819E46E1625F014D83F66080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding
struct  Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::engine
	RuntimeObject* ___engine_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::forEncryption
	bool ___forEncryption_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::forPrivateKey
	bool ___forPrivateKey_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::useStrictLength
	bool ___useStrictLength_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::pLen
	int32_t ___pLen_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::fallback
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fallback_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::blockBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___blockBuffer_10;

public:
	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}

	inline static int32_t get_offset_of_engine_4() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___engine_4)); }
	inline RuntimeObject* get_engine_4() const { return ___engine_4; }
	inline RuntimeObject** get_address_of_engine_4() { return &___engine_4; }
	inline void set_engine_4(RuntimeObject* value)
	{
		___engine_4 = value;
		Il2CppCodeGenWriteBarrier((&___engine_4), value);
	}

	inline static int32_t get_offset_of_forEncryption_5() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___forEncryption_5)); }
	inline bool get_forEncryption_5() const { return ___forEncryption_5; }
	inline bool* get_address_of_forEncryption_5() { return &___forEncryption_5; }
	inline void set_forEncryption_5(bool value)
	{
		___forEncryption_5 = value;
	}

	inline static int32_t get_offset_of_forPrivateKey_6() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___forPrivateKey_6)); }
	inline bool get_forPrivateKey_6() const { return ___forPrivateKey_6; }
	inline bool* get_address_of_forPrivateKey_6() { return &___forPrivateKey_6; }
	inline void set_forPrivateKey_6(bool value)
	{
		___forPrivateKey_6 = value;
	}

	inline static int32_t get_offset_of_useStrictLength_7() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___useStrictLength_7)); }
	inline bool get_useStrictLength_7() const { return ___useStrictLength_7; }
	inline bool* get_address_of_useStrictLength_7() { return &___useStrictLength_7; }
	inline void set_useStrictLength_7(bool value)
	{
		___useStrictLength_7 = value;
	}

	inline static int32_t get_offset_of_pLen_8() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___pLen_8)); }
	inline int32_t get_pLen_8() const { return ___pLen_8; }
	inline int32_t* get_address_of_pLen_8() { return &___pLen_8; }
	inline void set_pLen_8(int32_t value)
	{
		___pLen_8 = value;
	}

	inline static int32_t get_offset_of_fallback_9() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___fallback_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fallback_9() const { return ___fallback_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fallback_9() { return &___fallback_9; }
	inline void set_fallback_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___fallback_9), value);
	}

	inline static int32_t get_offset_of_blockBuffer_10() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080, ___blockBuffer_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_blockBuffer_10() const { return ___blockBuffer_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_blockBuffer_10() { return &___blockBuffer_10; }
	inline void set_blockBuffer_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___blockBuffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___blockBuffer_10), value);
	}
};

struct Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080_StaticFields
{
public:
	// System.Boolean[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Encodings.Pkcs1Encoding::strictLengthEnabled
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___strictLengthEnabled_2;

public:
	inline static int32_t get_offset_of_strictLengthEnabled_2() { return static_cast<int32_t>(offsetof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080_StaticFields, ___strictLengthEnabled_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_strictLengthEnabled_2() const { return ___strictLengthEnabled_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_strictLengthEnabled_2() { return &___strictLengthEnabled_2; }
	inline void set_strictLengthEnabled_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___strictLengthEnabled_2 = value;
		Il2CppCodeGenWriteBarrier((&___strictLengthEnabled_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS1ENCODING_TBA70E2A21F4F37A5819E46E1625F014D83F66080_H
#ifndef DSTU7624ENGINE_T28C42C0A459A0C7B02A244520FC9BA75F6911457_H
#define DSTU7624ENGINE_T28C42C0A459A0C7B02A244520FC9BA75F6911457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine
struct  Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457  : public RuntimeObject
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::internalState
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___internalState_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::workingKey
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___workingKey_1;
	// System.UInt64[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::roundKeys
	UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2* ___roundKeys_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::wordsInBlock
	int32_t ___wordsInBlock_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::wordsInKey
	int32_t ___wordsInKey_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::roundsAmount
	int32_t ___roundsAmount_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::forEncryption
	bool ___forEncryption_9;

public:
	inline static int32_t get_offset_of_internalState_0() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___internalState_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_internalState_0() const { return ___internalState_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_internalState_0() { return &___internalState_0; }
	inline void set_internalState_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___internalState_0 = value;
		Il2CppCodeGenWriteBarrier((&___internalState_0), value);
	}

	inline static int32_t get_offset_of_workingKey_1() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___workingKey_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_workingKey_1() const { return ___workingKey_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_workingKey_1() { return &___workingKey_1; }
	inline void set_workingKey_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___workingKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_1), value);
	}

	inline static int32_t get_offset_of_roundKeys_2() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___roundKeys_2)); }
	inline UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2* get_roundKeys_2() const { return ___roundKeys_2; }
	inline UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2** get_address_of_roundKeys_2() { return &___roundKeys_2; }
	inline void set_roundKeys_2(UInt64U5BU5DU5BU5D_t58035BFE36EC34600DD3C7E06AB576C50267D2E2* value)
	{
		___roundKeys_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundKeys_2), value);
	}

	inline static int32_t get_offset_of_wordsInBlock_3() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___wordsInBlock_3)); }
	inline int32_t get_wordsInBlock_3() const { return ___wordsInBlock_3; }
	inline int32_t* get_address_of_wordsInBlock_3() { return &___wordsInBlock_3; }
	inline void set_wordsInBlock_3(int32_t value)
	{
		___wordsInBlock_3 = value;
	}

	inline static int32_t get_offset_of_wordsInKey_4() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___wordsInKey_4)); }
	inline int32_t get_wordsInKey_4() const { return ___wordsInKey_4; }
	inline int32_t* get_address_of_wordsInKey_4() { return &___wordsInKey_4; }
	inline void set_wordsInKey_4(int32_t value)
	{
		___wordsInKey_4 = value;
	}

	inline static int32_t get_offset_of_roundsAmount_8() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___roundsAmount_8)); }
	inline int32_t get_roundsAmount_8() const { return ___roundsAmount_8; }
	inline int32_t* get_address_of_roundsAmount_8() { return &___roundsAmount_8; }
	inline void set_roundsAmount_8(int32_t value)
	{
		___roundsAmount_8 = value;
	}

	inline static int32_t get_offset_of_forEncryption_9() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457, ___forEncryption_9)); }
	inline bool get_forEncryption_9() const { return ___forEncryption_9; }
	inline bool* get_address_of_forEncryption_9() { return &___forEncryption_9; }
	inline void set_forEncryption_9(bool value)
	{
		___forEncryption_9 = value;
	}
};

struct Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::S0
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S0_12;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::S1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S1_13;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::S2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S2_14;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::S3
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S3_15;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::T0
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___T0_16;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::T1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___T1_17;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::T2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___T2_18;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine::T3
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___T3_19;

public:
	inline static int32_t get_offset_of_S0_12() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___S0_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S0_12() const { return ___S0_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S0_12() { return &___S0_12; }
	inline void set_S0_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S0_12 = value;
		Il2CppCodeGenWriteBarrier((&___S0_12), value);
	}

	inline static int32_t get_offset_of_S1_13() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___S1_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S1_13() const { return ___S1_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S1_13() { return &___S1_13; }
	inline void set_S1_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S1_13 = value;
		Il2CppCodeGenWriteBarrier((&___S1_13), value);
	}

	inline static int32_t get_offset_of_S2_14() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___S2_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S2_14() const { return ___S2_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S2_14() { return &___S2_14; }
	inline void set_S2_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S2_14 = value;
		Il2CppCodeGenWriteBarrier((&___S2_14), value);
	}

	inline static int32_t get_offset_of_S3_15() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___S3_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S3_15() const { return ___S3_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S3_15() { return &___S3_15; }
	inline void set_S3_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S3_15 = value;
		Il2CppCodeGenWriteBarrier((&___S3_15), value);
	}

	inline static int32_t get_offset_of_T0_16() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___T0_16)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_T0_16() const { return ___T0_16; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_T0_16() { return &___T0_16; }
	inline void set_T0_16(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___T0_16 = value;
		Il2CppCodeGenWriteBarrier((&___T0_16), value);
	}

	inline static int32_t get_offset_of_T1_17() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___T1_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_T1_17() const { return ___T1_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_T1_17() { return &___T1_17; }
	inline void set_T1_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___T1_17 = value;
		Il2CppCodeGenWriteBarrier((&___T1_17), value);
	}

	inline static int32_t get_offset_of_T2_18() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___T2_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_T2_18() const { return ___T2_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_T2_18() { return &___T2_18; }
	inline void set_T2_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___T2_18 = value;
		Il2CppCodeGenWriteBarrier((&___T2_18), value);
	}

	inline static int32_t get_offset_of_T3_19() { return static_cast<int32_t>(offsetof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields, ___T3_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_T3_19() const { return ___T3_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_T3_19() { return &___T3_19; }
	inline void set_T3_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___T3_19 = value;
		Il2CppCodeGenWriteBarrier((&___T3_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSTU7624ENGINE_T28C42C0A459A0C7B02A244520FC9BA75F6911457_H
#ifndef DSTU7624WRAPENGINE_T529254156192E3594BF41EEBFF26C303C04DE29F_H
#define DSTU7624WRAPENGINE_T529254156192E3594BF41EEBFF26C303C04DE29F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624WrapEngine
struct  Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624WrapEngine::param
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___param_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624Engine BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624WrapEngine::engine
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 * ___engine_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624WrapEngine::forWrapping
	bool ___forWrapping_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Dstu7624WrapEngine::blockSize
	int32_t ___blockSize_3;

public:
	inline static int32_t get_offset_of_param_0() { return static_cast<int32_t>(offsetof(Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F, ___param_0)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_param_0() const { return ___param_0; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_param_0() { return &___param_0; }
	inline void set_param_0(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___param_0 = value;
		Il2CppCodeGenWriteBarrier((&___param_0), value);
	}

	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F, ___engine_1)); }
	inline Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 * get_engine_1() const { return ___engine_1; }
	inline Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 ** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457 * value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}

	inline static int32_t get_offset_of_forWrapping_2() { return static_cast<int32_t>(offsetof(Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F, ___forWrapping_2)); }
	inline bool get_forWrapping_2() const { return ___forWrapping_2; }
	inline bool* get_address_of_forWrapping_2() { return &___forWrapping_2; }
	inline void set_forWrapping_2(bool value)
	{
		___forWrapping_2 = value;
	}

	inline static int32_t get_offset_of_blockSize_3() { return static_cast<int32_t>(offsetof(Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F, ___blockSize_3)); }
	inline int32_t get_blockSize_3() const { return ___blockSize_3; }
	inline int32_t* get_address_of_blockSize_3() { return &___blockSize_3; }
	inline void set_blockSize_3(int32_t value)
	{
		___blockSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSTU7624WRAPENGINE_T529254156192E3594BF41EEBFF26C303C04DE29F_H
#ifndef ELGAMALENGINE_T1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18_H
#define ELGAMALENGINE_T1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ElGamalEngine
struct  ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ElGamalEngine::key
	ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB * ___key_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ElGamalEngine::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ElGamalEngine::forEncryption
	bool ___forEncryption_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ElGamalEngine::bitSize
	int32_t ___bitSize_3;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18, ___key_0)); }
	inline ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB * get_key_0() const { return ___key_0; }
	inline ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18, ___random_1)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}

	inline static int32_t get_offset_of_forEncryption_2() { return static_cast<int32_t>(offsetof(ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18, ___forEncryption_2)); }
	inline bool get_forEncryption_2() const { return ___forEncryption_2; }
	inline bool* get_address_of_forEncryption_2() { return &___forEncryption_2; }
	inline void set_forEncryption_2(bool value)
	{
		___forEncryption_2 = value;
	}

	inline static int32_t get_offset_of_bitSize_3() { return static_cast<int32_t>(offsetof(ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18, ___bitSize_3)); }
	inline int32_t get_bitSize_3() const { return ___bitSize_3; }
	inline int32_t* get_address_of_bitSize_3() { return &___bitSize_3; }
	inline void set_bitSize_3(int32_t value)
	{
		___bitSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALENGINE_T1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18_H
#ifndef GOST28147ENGINE_TBF602624EC9E1D448A33862F6E9469BF58A88C0F_H
#define GOST28147ENGINE_TBF602624EC9E1D448A33862F6E9469BF58A88C0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine
struct  Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::forEncryption
	bool ___forEncryption_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_3;

public:
	inline static int32_t get_offset_of_workingKey_1() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F, ___workingKey_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_1() const { return ___workingKey_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_1() { return &___workingKey_1; }
	inline void set_workingKey_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_1), value);
	}

	inline static int32_t get_offset_of_forEncryption_2() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F, ___forEncryption_2)); }
	inline bool get_forEncryption_2() const { return ___forEncryption_2; }
	inline bool* get_address_of_forEncryption_2() { return &___forEncryption_2; }
	inline void set_forEncryption_2(bool value)
	{
		___forEncryption_2 = value;
	}

	inline static int32_t get_offset_of_S_3() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F, ___S_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_3() const { return ___S_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_3() { return &___S_3; }
	inline void set_S_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_3 = value;
		Il2CppCodeGenWriteBarrier((&___S_3), value);
	}
};

struct Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::Sbox_Default
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sbox_Default_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_Test
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_Test_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_A
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_A_6;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_B
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_B_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_C
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_C_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::ESbox_D
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ESbox_D_9;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::DSbox_Test
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DSbox_Test_10;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::DSbox_A
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DSbox_A_11;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Gost28147Engine::sBoxes
	RuntimeObject* ___sBoxes_12;

public:
	inline static int32_t get_offset_of_Sbox_Default_4() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___Sbox_Default_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sbox_Default_4() const { return ___Sbox_Default_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sbox_Default_4() { return &___Sbox_Default_4; }
	inline void set_Sbox_Default_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sbox_Default_4 = value;
		Il2CppCodeGenWriteBarrier((&___Sbox_Default_4), value);
	}

	inline static int32_t get_offset_of_ESbox_Test_5() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___ESbox_Test_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_Test_5() const { return ___ESbox_Test_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_Test_5() { return &___ESbox_Test_5; }
	inline void set_ESbox_Test_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_Test_5 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_Test_5), value);
	}

	inline static int32_t get_offset_of_ESbox_A_6() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___ESbox_A_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_A_6() const { return ___ESbox_A_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_A_6() { return &___ESbox_A_6; }
	inline void set_ESbox_A_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_A_6 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_A_6), value);
	}

	inline static int32_t get_offset_of_ESbox_B_7() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___ESbox_B_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_B_7() const { return ___ESbox_B_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_B_7() { return &___ESbox_B_7; }
	inline void set_ESbox_B_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_B_7 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_B_7), value);
	}

	inline static int32_t get_offset_of_ESbox_C_8() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___ESbox_C_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_C_8() const { return ___ESbox_C_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_C_8() { return &___ESbox_C_8; }
	inline void set_ESbox_C_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_C_8 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_C_8), value);
	}

	inline static int32_t get_offset_of_ESbox_D_9() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___ESbox_D_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ESbox_D_9() const { return ___ESbox_D_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ESbox_D_9() { return &___ESbox_D_9; }
	inline void set_ESbox_D_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ESbox_D_9 = value;
		Il2CppCodeGenWriteBarrier((&___ESbox_D_9), value);
	}

	inline static int32_t get_offset_of_DSbox_Test_10() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___DSbox_Test_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DSbox_Test_10() const { return ___DSbox_Test_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DSbox_Test_10() { return &___DSbox_Test_10; }
	inline void set_DSbox_Test_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DSbox_Test_10 = value;
		Il2CppCodeGenWriteBarrier((&___DSbox_Test_10), value);
	}

	inline static int32_t get_offset_of_DSbox_A_11() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___DSbox_A_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DSbox_A_11() const { return ___DSbox_A_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DSbox_A_11() { return &___DSbox_A_11; }
	inline void set_DSbox_A_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DSbox_A_11 = value;
		Il2CppCodeGenWriteBarrier((&___DSbox_A_11), value);
	}

	inline static int32_t get_offset_of_sBoxes_12() { return static_cast<int32_t>(offsetof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields, ___sBoxes_12)); }
	inline RuntimeObject* get_sBoxes_12() const { return ___sBoxes_12; }
	inline RuntimeObject** get_address_of_sBoxes_12() { return &___sBoxes_12; }
	inline void set_sBoxes_12(RuntimeObject* value)
	{
		___sBoxes_12 = value;
		Il2CppCodeGenWriteBarrier((&___sBoxes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST28147ENGINE_TBF602624EC9E1D448A33862F6E9469BF58A88C0F_H
#ifndef HC128ENGINE_T844B45D142AF1591D3F15034CAE7EC2F65A8C906_H
#define HC128ENGINE_T844B45D142AF1591D3F15034CAE7EC2F65A8C906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine
struct  HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::p
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___p_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::q
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___q_1;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::cnt
	uint32_t ___cnt_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::initialised
	bool ___initialised_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC128Engine::idx
	int32_t ___idx_7;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___p_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_p_0() const { return ___p_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___q_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_q_1() const { return ___q_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_cnt_2() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___cnt_2)); }
	inline uint32_t get_cnt_2() const { return ___cnt_2; }
	inline uint32_t* get_address_of_cnt_2() { return &___cnt_2; }
	inline void set_cnt_2(uint32_t value)
	{
		___cnt_2 = value;
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___key_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_3() const { return ___key_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier((&___key_3), value);
	}

	inline static int32_t get_offset_of_iv_4() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___iv_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_4() const { return ___iv_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_4() { return &___iv_4; }
	inline void set_iv_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_4 = value;
		Il2CppCodeGenWriteBarrier((&___iv_4), value);
	}

	inline static int32_t get_offset_of_initialised_5() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___initialised_5)); }
	inline bool get_initialised_5() const { return ___initialised_5; }
	inline bool* get_address_of_initialised_5() { return &___initialised_5; }
	inline void set_initialised_5(bool value)
	{
		___initialised_5 = value;
	}

	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_6), value);
	}

	inline static int32_t get_offset_of_idx_7() { return static_cast<int32_t>(offsetof(HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906, ___idx_7)); }
	inline int32_t get_idx_7() const { return ___idx_7; }
	inline int32_t* get_address_of_idx_7() { return &___idx_7; }
	inline void set_idx_7(int32_t value)
	{
		___idx_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HC128ENGINE_T844B45D142AF1591D3F15034CAE7EC2F65A8C906_H
#ifndef HC256ENGINE_TCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944_H
#define HC256ENGINE_TCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine
struct  HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::p
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___p_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::q
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___q_1;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::cnt
	uint32_t ___cnt_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::initialised
	bool ___initialised_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.HC256Engine::idx
	int32_t ___idx_7;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___p_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_p_0() const { return ___p_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___q_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_q_1() const { return ___q_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_cnt_2() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___cnt_2)); }
	inline uint32_t get_cnt_2() const { return ___cnt_2; }
	inline uint32_t* get_address_of_cnt_2() { return &___cnt_2; }
	inline void set_cnt_2(uint32_t value)
	{
		___cnt_2 = value;
	}

	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___key_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_3() const { return ___key_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_3 = value;
		Il2CppCodeGenWriteBarrier((&___key_3), value);
	}

	inline static int32_t get_offset_of_iv_4() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___iv_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_4() const { return ___iv_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_4() { return &___iv_4; }
	inline void set_iv_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_4 = value;
		Il2CppCodeGenWriteBarrier((&___iv_4), value);
	}

	inline static int32_t get_offset_of_initialised_5() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___initialised_5)); }
	inline bool get_initialised_5() const { return ___initialised_5; }
	inline bool* get_address_of_initialised_5() { return &___initialised_5; }
	inline void set_initialised_5(bool value)
	{
		___initialised_5 = value;
	}

	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_6), value);
	}

	inline static int32_t get_offset_of_idx_7() { return static_cast<int32_t>(offsetof(HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944, ___idx_7)); }
	inline int32_t get_idx_7() const { return ___idx_7; }
	inline int32_t* get_address_of_idx_7() { return &___idx_7; }
	inline void set_idx_7(int32_t value)
	{
		___idx_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HC256ENGINE_TCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944_H
#ifndef IDEAENGINE_T0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_H
#define IDEAENGINE_T0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IdeaEngine
struct  IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IdeaEngine::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_1;

public:
	inline static int32_t get_offset_of_workingKey_1() { return static_cast<int32_t>(offsetof(IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F, ___workingKey_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_1() const { return ___workingKey_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_1() { return &___workingKey_1; }
	inline void set_workingKey_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_1), value);
	}
};

struct IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IdeaEngine::MASK
	int32_t ___MASK_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IdeaEngine::BASE
	int32_t ___BASE_3;

public:
	inline static int32_t get_offset_of_MASK_2() { return static_cast<int32_t>(offsetof(IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_StaticFields, ___MASK_2)); }
	inline int32_t get_MASK_2() const { return ___MASK_2; }
	inline int32_t* get_address_of_MASK_2() { return &___MASK_2; }
	inline void set_MASK_2(int32_t value)
	{
		___MASK_2 = value;
	}

	inline static int32_t get_offset_of_BASE_3() { return static_cast<int32_t>(offsetof(IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_StaticFields, ___BASE_3)); }
	inline int32_t get_BASE_3() const { return ___BASE_3; }
	inline int32_t* get_address_of_BASE_3() { return &___BASE_3; }
	inline void set_BASE_3(int32_t value)
	{
		___BASE_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDEAENGINE_T0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_H
#ifndef IESENGINE_T7F57FEE741231F14B6CF18E5CE1F648B47C27BBA_H
#define IESENGINE_T7F57FEE741231F14B6CF18E5CE1F648B47C27BBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine
struct  IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBasicAgreement BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::agree
	RuntimeObject* ___agree_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDerivationFunction BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::kdf
	RuntimeObject* ___kdf_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IMac BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::mac
	RuntimeObject* ___mac_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::cipher
	BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B * ___cipher_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::macBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBuf_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::forEncryption
	bool ___forEncryption_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::privParam
	RuntimeObject* ___privParam_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::pubParam
	RuntimeObject* ___pubParam_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IesEngine::param
	IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9 * ___param_8;

public:
	inline static int32_t get_offset_of_agree_0() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___agree_0)); }
	inline RuntimeObject* get_agree_0() const { return ___agree_0; }
	inline RuntimeObject** get_address_of_agree_0() { return &___agree_0; }
	inline void set_agree_0(RuntimeObject* value)
	{
		___agree_0 = value;
		Il2CppCodeGenWriteBarrier((&___agree_0), value);
	}

	inline static int32_t get_offset_of_kdf_1() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___kdf_1)); }
	inline RuntimeObject* get_kdf_1() const { return ___kdf_1; }
	inline RuntimeObject** get_address_of_kdf_1() { return &___kdf_1; }
	inline void set_kdf_1(RuntimeObject* value)
	{
		___kdf_1 = value;
		Il2CppCodeGenWriteBarrier((&___kdf_1), value);
	}

	inline static int32_t get_offset_of_mac_2() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___mac_2)); }
	inline RuntimeObject* get_mac_2() const { return ___mac_2; }
	inline RuntimeObject** get_address_of_mac_2() { return &___mac_2; }
	inline void set_mac_2(RuntimeObject* value)
	{
		___mac_2 = value;
		Il2CppCodeGenWriteBarrier((&___mac_2), value);
	}

	inline static int32_t get_offset_of_cipher_3() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___cipher_3)); }
	inline BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B * get_cipher_3() const { return ___cipher_3; }
	inline BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B ** get_address_of_cipher_3() { return &___cipher_3; }
	inline void set_cipher_3(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B * value)
	{
		___cipher_3 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_3), value);
	}

	inline static int32_t get_offset_of_macBuf_4() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___macBuf_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBuf_4() const { return ___macBuf_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBuf_4() { return &___macBuf_4; }
	inline void set_macBuf_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBuf_4 = value;
		Il2CppCodeGenWriteBarrier((&___macBuf_4), value);
	}

	inline static int32_t get_offset_of_forEncryption_5() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___forEncryption_5)); }
	inline bool get_forEncryption_5() const { return ___forEncryption_5; }
	inline bool* get_address_of_forEncryption_5() { return &___forEncryption_5; }
	inline void set_forEncryption_5(bool value)
	{
		___forEncryption_5 = value;
	}

	inline static int32_t get_offset_of_privParam_6() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___privParam_6)); }
	inline RuntimeObject* get_privParam_6() const { return ___privParam_6; }
	inline RuntimeObject** get_address_of_privParam_6() { return &___privParam_6; }
	inline void set_privParam_6(RuntimeObject* value)
	{
		___privParam_6 = value;
		Il2CppCodeGenWriteBarrier((&___privParam_6), value);
	}

	inline static int32_t get_offset_of_pubParam_7() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___pubParam_7)); }
	inline RuntimeObject* get_pubParam_7() const { return ___pubParam_7; }
	inline RuntimeObject** get_address_of_pubParam_7() { return &___pubParam_7; }
	inline void set_pubParam_7(RuntimeObject* value)
	{
		___pubParam_7 = value;
		Il2CppCodeGenWriteBarrier((&___pubParam_7), value);
	}

	inline static int32_t get_offset_of_param_8() { return static_cast<int32_t>(offsetof(IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA, ___param_8)); }
	inline IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9 * get_param_8() const { return ___param_8; }
	inline IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9 ** get_address_of_param_8() { return &___param_8; }
	inline void set_param_8(IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9 * value)
	{
		___param_8 = value;
		Il2CppCodeGenWriteBarrier((&___param_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IESENGINE_T7F57FEE741231F14B6CF18E5CE1F648B47C27BBA_H
#ifndef ISAACENGINE_TAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_H
#define ISAACENGINE_TAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine
struct  IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::engineState
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___engineState_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::results
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___results_3;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::a
	uint32_t ___a_4;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::b
	uint32_t ___b_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::c
	uint32_t ___c_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::index
	int32_t ___index_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::keyStream
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyStream_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::initialised
	bool ___initialised_10;

public:
	inline static int32_t get_offset_of_engineState_2() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___engineState_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_engineState_2() const { return ___engineState_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_engineState_2() { return &___engineState_2; }
	inline void set_engineState_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___engineState_2 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_2), value);
	}

	inline static int32_t get_offset_of_results_3() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___results_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_results_3() const { return ___results_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_results_3() { return &___results_3; }
	inline void set_results_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___results_3 = value;
		Il2CppCodeGenWriteBarrier((&___results_3), value);
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___a_4)); }
	inline uint32_t get_a_4() const { return ___a_4; }
	inline uint32_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint32_t value)
	{
		___a_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___b_5)); }
	inline uint32_t get_b_5() const { return ___b_5; }
	inline uint32_t* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(uint32_t value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_c_6() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___c_6)); }
	inline uint32_t get_c_6() const { return ___c_6; }
	inline uint32_t* get_address_of_c_6() { return &___c_6; }
	inline void set_c_6(uint32_t value)
	{
		___c_6 = value;
	}

	inline static int32_t get_offset_of_index_7() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___index_7)); }
	inline int32_t get_index_7() const { return ___index_7; }
	inline int32_t* get_address_of_index_7() { return &___index_7; }
	inline void set_index_7(int32_t value)
	{
		___index_7 = value;
	}

	inline static int32_t get_offset_of_keyStream_8() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___keyStream_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyStream_8() const { return ___keyStream_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyStream_8() { return &___keyStream_8; }
	inline void set_keyStream_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyStream_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyStream_8), value);
	}

	inline static int32_t get_offset_of_workingKey_9() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___workingKey_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_9() const { return ___workingKey_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_9() { return &___workingKey_9; }
	inline void set_workingKey_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_9), value);
	}

	inline static int32_t get_offset_of_initialised_10() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD, ___initialised_10)); }
	inline bool get_initialised_10() const { return ___initialised_10; }
	inline bool* get_address_of_initialised_10() { return &___initialised_10; }
	inline void set_initialised_10(bool value)
	{
		___initialised_10 = value;
	}
};

struct IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::sizeL
	int32_t ___sizeL_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.IsaacEngine::stateArraySize
	int32_t ___stateArraySize_1;

public:
	inline static int32_t get_offset_of_sizeL_0() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_StaticFields, ___sizeL_0)); }
	inline int32_t get_sizeL_0() const { return ___sizeL_0; }
	inline int32_t* get_address_of_sizeL_0() { return &___sizeL_0; }
	inline void set_sizeL_0(int32_t value)
	{
		___sizeL_0 = value;
	}

	inline static int32_t get_offset_of_stateArraySize_1() { return static_cast<int32_t>(offsetof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_StaticFields, ___stateArraySize_1)); }
	inline int32_t get_stateArraySize_1() const { return ___stateArraySize_1; }
	inline int32_t* get_address_of_stateArraySize_1() { return &___stateArraySize_1; }
	inline void set_stateArraySize_1(int32_t value)
	{
		___stateArraySize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISAACENGINE_TAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_H
#ifndef NACCACHESTERNENGINE_T6664D855A35AB2432647A7982A9B7A01FCA4B321_H
#define NACCACHESTERNENGINE_T6664D855A35AB2432647A7982A9B7A01FCA4B321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NaccacheSternEngine
struct  NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NaccacheSternEngine::forEncryption
	bool ___forEncryption_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NaccacheSternEngine::key
	NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7 * ___key_1;
	// System.Collections.IList[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NaccacheSternEngine::lookup
	IListU5BU5D_t6779C6A7A4B7074C864EA7FE9328D68F0E4866DF* ___lookup_2;

public:
	inline static int32_t get_offset_of_forEncryption_0() { return static_cast<int32_t>(offsetof(NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321, ___forEncryption_0)); }
	inline bool get_forEncryption_0() const { return ___forEncryption_0; }
	inline bool* get_address_of_forEncryption_0() { return &___forEncryption_0; }
	inline void set_forEncryption_0(bool value)
	{
		___forEncryption_0 = value;
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321, ___key_1)); }
	inline NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7 * get_key_1() const { return ___key_1; }
	inline NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_lookup_2() { return static_cast<int32_t>(offsetof(NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321, ___lookup_2)); }
	inline IListU5BU5D_t6779C6A7A4B7074C864EA7FE9328D68F0E4866DF* get_lookup_2() const { return ___lookup_2; }
	inline IListU5BU5D_t6779C6A7A4B7074C864EA7FE9328D68F0E4866DF** get_address_of_lookup_2() { return &___lookup_2; }
	inline void set_lookup_2(IListU5BU5D_t6779C6A7A4B7074C864EA7FE9328D68F0E4866DF* value)
	{
		___lookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NACCACHESTERNENGINE_T6664D855A35AB2432647A7982A9B7A01FCA4B321_H
#ifndef NOEKEONENGINE_TC3D1BF60777146C2468CE66A3995A8B5FEE24D80_H
#define NOEKEONENGINE_TC3D1BF60777146C2468CE66A3995A8B5FEE24D80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine
struct  NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::state
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___state_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::subKeys
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___subKeys_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::decryptKeys
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___decryptKeys_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::_initialised
	bool ____initialised_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::_forEncryption
	bool ____forEncryption_7;

public:
	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80, ___state_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_state_3() const { return ___state_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___state_3 = value;
		Il2CppCodeGenWriteBarrier((&___state_3), value);
	}

	inline static int32_t get_offset_of_subKeys_4() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80, ___subKeys_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_subKeys_4() const { return ___subKeys_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_subKeys_4() { return &___subKeys_4; }
	inline void set_subKeys_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___subKeys_4 = value;
		Il2CppCodeGenWriteBarrier((&___subKeys_4), value);
	}

	inline static int32_t get_offset_of_decryptKeys_5() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80, ___decryptKeys_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_decryptKeys_5() const { return ___decryptKeys_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_decryptKeys_5() { return &___decryptKeys_5; }
	inline void set_decryptKeys_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___decryptKeys_5 = value;
		Il2CppCodeGenWriteBarrier((&___decryptKeys_5), value);
	}

	inline static int32_t get_offset_of__initialised_6() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80, ____initialised_6)); }
	inline bool get__initialised_6() const { return ____initialised_6; }
	inline bool* get_address_of__initialised_6() { return &____initialised_6; }
	inline void set__initialised_6(bool value)
	{
		____initialised_6 = value;
	}

	inline static int32_t get_offset_of__forEncryption_7() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80, ____forEncryption_7)); }
	inline bool get__forEncryption_7() const { return ____forEncryption_7; }
	inline bool* get_address_of__forEncryption_7() { return &____forEncryption_7; }
	inline void set__forEncryption_7(bool value)
	{
		____forEncryption_7 = value;
	}
};

struct NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::nullVector
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___nullVector_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NoekeonEngine::roundConstants
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___roundConstants_2;

public:
	inline static int32_t get_offset_of_nullVector_1() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80_StaticFields, ___nullVector_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_nullVector_1() const { return ___nullVector_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_nullVector_1() { return &___nullVector_1; }
	inline void set_nullVector_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___nullVector_1 = value;
		Il2CppCodeGenWriteBarrier((&___nullVector_1), value);
	}

	inline static int32_t get_offset_of_roundConstants_2() { return static_cast<int32_t>(offsetof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80_StaticFields, ___roundConstants_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_roundConstants_2() const { return ___roundConstants_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_roundConstants_2() { return &___roundConstants_2; }
	inline void set_roundConstants_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___roundConstants_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundConstants_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOEKEONENGINE_TC3D1BF60777146C2468CE66A3995A8B5FEE24D80_H
#ifndef NULLENGINE_TB7750E6A21F2EA4946195177A9B5A88F680B3E29_H
#define NULLENGINE_TB7750E6A21F2EA4946195177A9B5A88F680B3E29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NullEngine
struct  NullEngine_tB7750E6A21F2EA4946195177A9B5A88F680B3E29  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.NullEngine::initialised
	bool ___initialised_0;

public:
	inline static int32_t get_offset_of_initialised_0() { return static_cast<int32_t>(offsetof(NullEngine_tB7750E6A21F2EA4946195177A9B5A88F680B3E29, ___initialised_0)); }
	inline bool get_initialised_0() const { return ___initialised_0; }
	inline bool* get_address_of_initialised_0() { return &___initialised_0; }
	inline void set_initialised_0(bool value)
	{
		___initialised_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLENGINE_TB7750E6A21F2EA4946195177A9B5A88F680B3E29_H
#ifndef RC2ENGINE_T2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_H
#define RC2ENGINE_T2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2Engine
struct  RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2Engine::workingKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___workingKey_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2Engine::encrypting
	bool ___encrypting_3;

public:
	inline static int32_t get_offset_of_workingKey_2() { return static_cast<int32_t>(offsetof(RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB, ___workingKey_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_workingKey_2() const { return ___workingKey_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_workingKey_2() { return &___workingKey_2; }
	inline void set_workingKey_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___workingKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_2), value);
	}

	inline static int32_t get_offset_of_encrypting_3() { return static_cast<int32_t>(offsetof(RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB, ___encrypting_3)); }
	inline bool get_encrypting_3() const { return ___encrypting_3; }
	inline bool* get_address_of_encrypting_3() { return &___encrypting_3; }
	inline void set_encrypting_3(bool value)
	{
		___encrypting_3 = value;
	}
};

struct RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2Engine::piTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___piTable_0;

public:
	inline static int32_t get_offset_of_piTable_0() { return static_cast<int32_t>(offsetof(RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_StaticFields, ___piTable_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_piTable_0() const { return ___piTable_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_piTable_0() { return &___piTable_0; }
	inline void set_piTable_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___piTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___piTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC2ENGINE_T2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_H
#ifndef RC2WRAPENGINE_TE33282629BF68C10E8E16AE1623F52FD7CB3348B_H
#define RC2WRAPENGINE_TE33282629BF68C10E8E16AE1623F52FD7CB3348B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine
struct  RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::engine
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * ___engine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::parameters
	RuntimeObject* ___parameters_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::paramPlusIV
	ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * ___paramPlusIV_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::forWrapping
	bool ___forWrapping_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::sr
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___sr_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::sha1
	RuntimeObject* ___sha1_7;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::digest
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___digest_8;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___engine_0)); }
	inline CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * get_engine_0() const { return ___engine_0; }
	inline CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F ** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___parameters_1)); }
	inline RuntimeObject* get_parameters_1() const { return ___parameters_1; }
	inline RuntimeObject** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(RuntimeObject* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_paramPlusIV_2() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___paramPlusIV_2)); }
	inline ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * get_paramPlusIV_2() const { return ___paramPlusIV_2; }
	inline ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF ** get_address_of_paramPlusIV_2() { return &___paramPlusIV_2; }
	inline void set_paramPlusIV_2(ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * value)
	{
		___paramPlusIV_2 = value;
		Il2CppCodeGenWriteBarrier((&___paramPlusIV_2), value);
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___iv_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_3() const { return ___iv_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}

	inline static int32_t get_offset_of_forWrapping_4() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___forWrapping_4)); }
	inline bool get_forWrapping_4() const { return ___forWrapping_4; }
	inline bool* get_address_of_forWrapping_4() { return &___forWrapping_4; }
	inline void set_forWrapping_4(bool value)
	{
		___forWrapping_4 = value;
	}

	inline static int32_t get_offset_of_sr_5() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___sr_5)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_sr_5() const { return ___sr_5; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_sr_5() { return &___sr_5; }
	inline void set_sr_5(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___sr_5 = value;
		Il2CppCodeGenWriteBarrier((&___sr_5), value);
	}

	inline static int32_t get_offset_of_sha1_7() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___sha1_7)); }
	inline RuntimeObject* get_sha1_7() const { return ___sha1_7; }
	inline RuntimeObject** get_address_of_sha1_7() { return &___sha1_7; }
	inline void set_sha1_7(RuntimeObject* value)
	{
		___sha1_7 = value;
		Il2CppCodeGenWriteBarrier((&___sha1_7), value);
	}

	inline static int32_t get_offset_of_digest_8() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B, ___digest_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_digest_8() const { return ___digest_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_digest_8() { return &___digest_8; }
	inline void set_digest_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___digest_8 = value;
		Il2CppCodeGenWriteBarrier((&___digest_8), value);
	}
};

struct RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC2WrapEngine::IV2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV2_6;

public:
	inline static int32_t get_offset_of_IV2_6() { return static_cast<int32_t>(offsetof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B_StaticFields, ___IV2_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV2_6() const { return ___IV2_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV2_6() { return &___IV2_6; }
	inline void set_IV2_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV2_6 = value;
		Il2CppCodeGenWriteBarrier((&___IV2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC2WRAPENGINE_TE33282629BF68C10E8E16AE1623F52FD7CB3348B_H
#ifndef RC4ENGINE_TB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_H
#define RC4ENGINE_TB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC4Engine
struct  RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC4Engine::engineState
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___engineState_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC4Engine::x
	int32_t ___x_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC4Engine::y
	int32_t ___y_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC4Engine::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_4;

public:
	inline static int32_t get_offset_of_engineState_1() { return static_cast<int32_t>(offsetof(RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F, ___engineState_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_engineState_1() const { return ___engineState_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_engineState_1() { return &___engineState_1; }
	inline void set_engineState_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___engineState_1 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_workingKey_4() { return static_cast<int32_t>(offsetof(RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F, ___workingKey_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_4() const { return ___workingKey_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_4() { return &___workingKey_4; }
	inline void set_workingKey_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_4), value);
	}
};

struct RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC4Engine::STATE_LENGTH
	int32_t ___STATE_LENGTH_0;

public:
	inline static int32_t get_offset_of_STATE_LENGTH_0() { return static_cast<int32_t>(offsetof(RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_StaticFields, ___STATE_LENGTH_0)); }
	inline int32_t get_STATE_LENGTH_0() const { return ___STATE_LENGTH_0; }
	inline int32_t* get_address_of_STATE_LENGTH_0() { return &___STATE_LENGTH_0; }
	inline void set_STATE_LENGTH_0(int32_t value)
	{
		___STATE_LENGTH_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC4ENGINE_TB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_H
#ifndef RC532ENGINE_TC458BF83C040E6C2E5600484E362C4A3972B1956_H
#define RC532ENGINE_TC458BF83C040E6C2E5600484E362C4A3972B1956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC532Engine
struct  RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC532Engine::_noRounds
	int32_t ____noRounds_0;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC532Engine::_S
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____S_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC532Engine::forEncryption
	bool ___forEncryption_4;

public:
	inline static int32_t get_offset_of__noRounds_0() { return static_cast<int32_t>(offsetof(RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956, ____noRounds_0)); }
	inline int32_t get__noRounds_0() const { return ____noRounds_0; }
	inline int32_t* get_address_of__noRounds_0() { return &____noRounds_0; }
	inline void set__noRounds_0(int32_t value)
	{
		____noRounds_0 = value;
	}

	inline static int32_t get_offset_of__S_1() { return static_cast<int32_t>(offsetof(RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956, ____S_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__S_1() const { return ____S_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__S_1() { return &____S_1; }
	inline void set__S_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____S_1 = value;
		Il2CppCodeGenWriteBarrier((&____S_1), value);
	}

	inline static int32_t get_offset_of_forEncryption_4() { return static_cast<int32_t>(offsetof(RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956, ___forEncryption_4)); }
	inline bool get_forEncryption_4() const { return ___forEncryption_4; }
	inline bool* get_address_of_forEncryption_4() { return &___forEncryption_4; }
	inline void set_forEncryption_4(bool value)
	{
		___forEncryption_4 = value;
	}
};

struct RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC532Engine::P32
	int32_t ___P32_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC532Engine::Q32
	int32_t ___Q32_3;

public:
	inline static int32_t get_offset_of_P32_2() { return static_cast<int32_t>(offsetof(RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956_StaticFields, ___P32_2)); }
	inline int32_t get_P32_2() const { return ___P32_2; }
	inline int32_t* get_address_of_P32_2() { return &___P32_2; }
	inline void set_P32_2(int32_t value)
	{
		___P32_2 = value;
	}

	inline static int32_t get_offset_of_Q32_3() { return static_cast<int32_t>(offsetof(RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956_StaticFields, ___Q32_3)); }
	inline int32_t get_Q32_3() const { return ___Q32_3; }
	inline int32_t* get_address_of_Q32_3() { return &___Q32_3; }
	inline void set_Q32_3(int32_t value)
	{
		___Q32_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC532ENGINE_TC458BF83C040E6C2E5600484E362C4A3972B1956_H
#ifndef RC564ENGINE_T10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_H
#define RC564ENGINE_T10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine
struct  RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::_noRounds
	int32_t ____noRounds_2;
	// System.Int64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::_S
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ____S_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::forEncryption
	bool ___forEncryption_6;

public:
	inline static int32_t get_offset_of__noRounds_2() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD, ____noRounds_2)); }
	inline int32_t get__noRounds_2() const { return ____noRounds_2; }
	inline int32_t* get_address_of__noRounds_2() { return &____noRounds_2; }
	inline void set__noRounds_2(int32_t value)
	{
		____noRounds_2 = value;
	}

	inline static int32_t get_offset_of__S_3() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD, ____S_3)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get__S_3() const { return ____S_3; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of__S_3() { return &____S_3; }
	inline void set__S_3(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		____S_3 = value;
		Il2CppCodeGenWriteBarrier((&____S_3), value);
	}

	inline static int32_t get_offset_of_forEncryption_6() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD, ___forEncryption_6)); }
	inline bool get_forEncryption_6() const { return ___forEncryption_6; }
	inline bool* get_address_of_forEncryption_6() { return &___forEncryption_6; }
	inline void set_forEncryption_6(bool value)
	{
		___forEncryption_6 = value;
	}
};

struct RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::wordSize
	int32_t ___wordSize_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::bytesPerWord
	int32_t ___bytesPerWord_1;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::P64
	int64_t ___P64_4;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC564Engine::Q64
	int64_t ___Q64_5;

public:
	inline static int32_t get_offset_of_wordSize_0() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields, ___wordSize_0)); }
	inline int32_t get_wordSize_0() const { return ___wordSize_0; }
	inline int32_t* get_address_of_wordSize_0() { return &___wordSize_0; }
	inline void set_wordSize_0(int32_t value)
	{
		___wordSize_0 = value;
	}

	inline static int32_t get_offset_of_bytesPerWord_1() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields, ___bytesPerWord_1)); }
	inline int32_t get_bytesPerWord_1() const { return ___bytesPerWord_1; }
	inline int32_t* get_address_of_bytesPerWord_1() { return &___bytesPerWord_1; }
	inline void set_bytesPerWord_1(int32_t value)
	{
		___bytesPerWord_1 = value;
	}

	inline static int32_t get_offset_of_P64_4() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields, ___P64_4)); }
	inline int64_t get_P64_4() const { return ___P64_4; }
	inline int64_t* get_address_of_P64_4() { return &___P64_4; }
	inline void set_P64_4(int64_t value)
	{
		___P64_4 = value;
	}

	inline static int32_t get_offset_of_Q64_5() { return static_cast<int32_t>(offsetof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields, ___Q64_5)); }
	inline int64_t get_Q64_5() const { return ___Q64_5; }
	inline int64_t* get_address_of_Q64_5() { return &___Q64_5; }
	inline void set_Q64_5(int64_t value)
	{
		___Q64_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC564ENGINE_T10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_H
#ifndef RC6ENGINE_TA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_H
#define RC6ENGINE_TA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine
struct  RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::_S
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____S_3;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::forEncryption
	bool ___forEncryption_7;

public:
	inline static int32_t get_offset_of__S_3() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A, ____S_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__S_3() const { return ____S_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__S_3() { return &____S_3; }
	inline void set__S_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____S_3 = value;
		Il2CppCodeGenWriteBarrier((&____S_3), value);
	}

	inline static int32_t get_offset_of_forEncryption_7() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A, ___forEncryption_7)); }
	inline bool get_forEncryption_7() const { return ___forEncryption_7; }
	inline bool* get_address_of_forEncryption_7() { return &___forEncryption_7; }
	inline void set_forEncryption_7(bool value)
	{
		___forEncryption_7 = value;
	}
};

struct RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::wordSize
	int32_t ___wordSize_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::bytesPerWord
	int32_t ___bytesPerWord_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::_noRounds
	int32_t ____noRounds_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::P32
	int32_t ___P32_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::Q32
	int32_t ___Q32_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RC6Engine::LGW
	int32_t ___LGW_6;

public:
	inline static int32_t get_offset_of_wordSize_0() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields, ___wordSize_0)); }
	inline int32_t get_wordSize_0() const { return ___wordSize_0; }
	inline int32_t* get_address_of_wordSize_0() { return &___wordSize_0; }
	inline void set_wordSize_0(int32_t value)
	{
		___wordSize_0 = value;
	}

	inline static int32_t get_offset_of_bytesPerWord_1() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields, ___bytesPerWord_1)); }
	inline int32_t get_bytesPerWord_1() const { return ___bytesPerWord_1; }
	inline int32_t* get_address_of_bytesPerWord_1() { return &___bytesPerWord_1; }
	inline void set_bytesPerWord_1(int32_t value)
	{
		___bytesPerWord_1 = value;
	}

	inline static int32_t get_offset_of__noRounds_2() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields, ____noRounds_2)); }
	inline int32_t get__noRounds_2() const { return ____noRounds_2; }
	inline int32_t* get_address_of__noRounds_2() { return &____noRounds_2; }
	inline void set__noRounds_2(int32_t value)
	{
		____noRounds_2 = value;
	}

	inline static int32_t get_offset_of_P32_4() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields, ___P32_4)); }
	inline int32_t get_P32_4() const { return ___P32_4; }
	inline int32_t* get_address_of_P32_4() { return &___P32_4; }
	inline void set_P32_4(int32_t value)
	{
		___P32_4 = value;
	}

	inline static int32_t get_offset_of_Q32_5() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields, ___Q32_5)); }
	inline int32_t get_Q32_5() const { return ___Q32_5; }
	inline int32_t* get_address_of_Q32_5() { return &___Q32_5; }
	inline void set_Q32_5(int32_t value)
	{
		___Q32_5 = value;
	}

	inline static int32_t get_offset_of_LGW_6() { return static_cast<int32_t>(offsetof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields, ___LGW_6)); }
	inline int32_t get_LGW_6() const { return ___LGW_6; }
	inline int32_t* get_address_of_LGW_6() { return &___LGW_6; }
	inline void set_LGW_6(int32_t value)
	{
		___LGW_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC6ENGINE_TA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_H
#ifndef RFC3211WRAPENGINE_T42D4C5775B401F61990FA67D5C4E70C03DE9AC35_H
#define RFC3211WRAPENGINE_T42D4C5775B401F61990FA67D5C4E70C03DE9AC35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3211WrapEngine
struct  Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Modes.CbcBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3211WrapEngine::engine
	CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * ___engine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3211WrapEngine::param
	ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * ___param_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3211WrapEngine::forWrapping
	bool ___forWrapping_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3211WrapEngine::rand
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___rand_3;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35, ___engine_0)); }
	inline CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * get_engine_0() const { return ___engine_0; }
	inline CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F ** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(CbcBlockCipher_t7565E6778810D01349A96CDFEDDE39A56F47907F * value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_param_1() { return static_cast<int32_t>(offsetof(Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35, ___param_1)); }
	inline ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * get_param_1() const { return ___param_1; }
	inline ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF ** get_address_of_param_1() { return &___param_1; }
	inline void set_param_1(ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF * value)
	{
		___param_1 = value;
		Il2CppCodeGenWriteBarrier((&___param_1), value);
	}

	inline static int32_t get_offset_of_forWrapping_2() { return static_cast<int32_t>(offsetof(Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35, ___forWrapping_2)); }
	inline bool get_forWrapping_2() const { return ___forWrapping_2; }
	inline bool* get_address_of_forWrapping_2() { return &___forWrapping_2; }
	inline void set_forWrapping_2(bool value)
	{
		___forWrapping_2 = value;
	}

	inline static int32_t get_offset_of_rand_3() { return static_cast<int32_t>(offsetof(Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35, ___rand_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_rand_3() const { return ___rand_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_rand_3() { return &___rand_3; }
	inline void set_rand_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___rand_3 = value;
		Il2CppCodeGenWriteBarrier((&___rand_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC3211WRAPENGINE_T42D4C5775B401F61990FA67D5C4E70C03DE9AC35_H
#ifndef RFC3394WRAPENGINE_T8255799FEF932C0736E276CD77596F43027A454B_H
#define RFC3394WRAPENGINE_T8255799FEF932C0736E276CD77596F43027A454B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine
struct  Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::engine
	RuntimeObject* ___engine_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::param
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___param_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::forWrapping
	bool ___forWrapping_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Rfc3394WrapEngine::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_3;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___engine_0)); }
	inline RuntimeObject* get_engine_0() const { return ___engine_0; }
	inline RuntimeObject** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(RuntimeObject* value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_param_1() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___param_1)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_param_1() const { return ___param_1; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_param_1() { return &___param_1; }
	inline void set_param_1(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___param_1 = value;
		Il2CppCodeGenWriteBarrier((&___param_1), value);
	}

	inline static int32_t get_offset_of_forWrapping_2() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___forWrapping_2)); }
	inline bool get_forWrapping_2() const { return ___forWrapping_2; }
	inline bool* get_address_of_forWrapping_2() { return &___forWrapping_2; }
	inline void set_forWrapping_2(bool value)
	{
		___forWrapping_2 = value;
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B, ___iv_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_3() const { return ___iv_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RFC3394WRAPENGINE_T8255799FEF932C0736E276CD77596F43027A454B_H
#ifndef RIJNDAELENGINE_TDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_H
#define RIJNDAELENGINE_TDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine
struct  RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::BC
	int32_t ___BC_9;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::BC_MASK
	int64_t ___BC_MASK_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::ROUNDS
	int32_t ___ROUNDS_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::blockBits
	int32_t ___blockBits_12;
	// System.Int64[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::workingKey
	Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* ___workingKey_13;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::A0
	int64_t ___A0_14;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::A1
	int64_t ___A1_15;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::A2
	int64_t ___A2_16;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::A3
	int64_t ___A3_17;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::forEncryption
	bool ___forEncryption_18;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::shifts0SC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___shifts0SC_19;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::shifts1SC
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___shifts1SC_20;

public:
	inline static int32_t get_offset_of_BC_9() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___BC_9)); }
	inline int32_t get_BC_9() const { return ___BC_9; }
	inline int32_t* get_address_of_BC_9() { return &___BC_9; }
	inline void set_BC_9(int32_t value)
	{
		___BC_9 = value;
	}

	inline static int32_t get_offset_of_BC_MASK_10() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___BC_MASK_10)); }
	inline int64_t get_BC_MASK_10() const { return ___BC_MASK_10; }
	inline int64_t* get_address_of_BC_MASK_10() { return &___BC_MASK_10; }
	inline void set_BC_MASK_10(int64_t value)
	{
		___BC_MASK_10 = value;
	}

	inline static int32_t get_offset_of_ROUNDS_11() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___ROUNDS_11)); }
	inline int32_t get_ROUNDS_11() const { return ___ROUNDS_11; }
	inline int32_t* get_address_of_ROUNDS_11() { return &___ROUNDS_11; }
	inline void set_ROUNDS_11(int32_t value)
	{
		___ROUNDS_11 = value;
	}

	inline static int32_t get_offset_of_blockBits_12() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___blockBits_12)); }
	inline int32_t get_blockBits_12() const { return ___blockBits_12; }
	inline int32_t* get_address_of_blockBits_12() { return &___blockBits_12; }
	inline void set_blockBits_12(int32_t value)
	{
		___blockBits_12 = value;
	}

	inline static int32_t get_offset_of_workingKey_13() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___workingKey_13)); }
	inline Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* get_workingKey_13() const { return ___workingKey_13; }
	inline Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030** get_address_of_workingKey_13() { return &___workingKey_13; }
	inline void set_workingKey_13(Int64U5BU5DU5BU5D_t8BCE5761DE83FA729EF5F6BF7997DBF0349BC030* value)
	{
		___workingKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_13), value);
	}

	inline static int32_t get_offset_of_A0_14() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___A0_14)); }
	inline int64_t get_A0_14() const { return ___A0_14; }
	inline int64_t* get_address_of_A0_14() { return &___A0_14; }
	inline void set_A0_14(int64_t value)
	{
		___A0_14 = value;
	}

	inline static int32_t get_offset_of_A1_15() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___A1_15)); }
	inline int64_t get_A1_15() const { return ___A1_15; }
	inline int64_t* get_address_of_A1_15() { return &___A1_15; }
	inline void set_A1_15(int64_t value)
	{
		___A1_15 = value;
	}

	inline static int32_t get_offset_of_A2_16() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___A2_16)); }
	inline int64_t get_A2_16() const { return ___A2_16; }
	inline int64_t* get_address_of_A2_16() { return &___A2_16; }
	inline void set_A2_16(int64_t value)
	{
		___A2_16 = value;
	}

	inline static int32_t get_offset_of_A3_17() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___A3_17)); }
	inline int64_t get_A3_17() const { return ___A3_17; }
	inline int64_t* get_address_of_A3_17() { return &___A3_17; }
	inline void set_A3_17(int64_t value)
	{
		___A3_17 = value;
	}

	inline static int32_t get_offset_of_forEncryption_18() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___forEncryption_18)); }
	inline bool get_forEncryption_18() const { return ___forEncryption_18; }
	inline bool* get_address_of_forEncryption_18() { return &___forEncryption_18; }
	inline void set_forEncryption_18(bool value)
	{
		___forEncryption_18 = value;
	}

	inline static int32_t get_offset_of_shifts0SC_19() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___shifts0SC_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_shifts0SC_19() const { return ___shifts0SC_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_shifts0SC_19() { return &___shifts0SC_19; }
	inline void set_shifts0SC_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___shifts0SC_19 = value;
		Il2CppCodeGenWriteBarrier((&___shifts0SC_19), value);
	}

	inline static int32_t get_offset_of_shifts1SC_20() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69, ___shifts1SC_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_shifts1SC_20() const { return ___shifts1SC_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_shifts1SC_20() { return &___shifts1SC_20; }
	inline void set_shifts1SC_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___shifts1SC_20 = value;
		Il2CppCodeGenWriteBarrier((&___shifts1SC_20), value);
	}
};

struct RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::MAXROUNDS
	int32_t ___MAXROUNDS_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::MAXKC
	int32_t ___MAXKC_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::Logtable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Logtable_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::Alogtable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Alogtable_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::Si
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Si_5;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::rcon
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rcon_6;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::shifts0
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___shifts0_7;
	// System.Byte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RijndaelEngine::shifts1
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___shifts1_8;

public:
	inline static int32_t get_offset_of_MAXROUNDS_0() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___MAXROUNDS_0)); }
	inline int32_t get_MAXROUNDS_0() const { return ___MAXROUNDS_0; }
	inline int32_t* get_address_of_MAXROUNDS_0() { return &___MAXROUNDS_0; }
	inline void set_MAXROUNDS_0(int32_t value)
	{
		___MAXROUNDS_0 = value;
	}

	inline static int32_t get_offset_of_MAXKC_1() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___MAXKC_1)); }
	inline int32_t get_MAXKC_1() const { return ___MAXKC_1; }
	inline int32_t* get_address_of_MAXKC_1() { return &___MAXKC_1; }
	inline void set_MAXKC_1(int32_t value)
	{
		___MAXKC_1 = value;
	}

	inline static int32_t get_offset_of_Logtable_2() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___Logtable_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Logtable_2() const { return ___Logtable_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Logtable_2() { return &___Logtable_2; }
	inline void set_Logtable_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Logtable_2 = value;
		Il2CppCodeGenWriteBarrier((&___Logtable_2), value);
	}

	inline static int32_t get_offset_of_Alogtable_3() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___Alogtable_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Alogtable_3() const { return ___Alogtable_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Alogtable_3() { return &___Alogtable_3; }
	inline void set_Alogtable_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Alogtable_3 = value;
		Il2CppCodeGenWriteBarrier((&___Alogtable_3), value);
	}

	inline static int32_t get_offset_of_S_4() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___S_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_4() const { return ___S_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_4() { return &___S_4; }
	inline void set_S_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_4 = value;
		Il2CppCodeGenWriteBarrier((&___S_4), value);
	}

	inline static int32_t get_offset_of_Si_5() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___Si_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Si_5() const { return ___Si_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Si_5() { return &___Si_5; }
	inline void set_Si_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Si_5 = value;
		Il2CppCodeGenWriteBarrier((&___Si_5), value);
	}

	inline static int32_t get_offset_of_rcon_6() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___rcon_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rcon_6() const { return ___rcon_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rcon_6() { return &___rcon_6; }
	inline void set_rcon_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rcon_6 = value;
		Il2CppCodeGenWriteBarrier((&___rcon_6), value);
	}

	inline static int32_t get_offset_of_shifts0_7() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___shifts0_7)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_shifts0_7() const { return ___shifts0_7; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_shifts0_7() { return &___shifts0_7; }
	inline void set_shifts0_7(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___shifts0_7 = value;
		Il2CppCodeGenWriteBarrier((&___shifts0_7), value);
	}

	inline static int32_t get_offset_of_shifts1_8() { return static_cast<int32_t>(offsetof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields, ___shifts1_8)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_shifts1_8() const { return ___shifts1_8; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_shifts1_8() { return &___shifts1_8; }
	inline void set_shifts1_8(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___shifts1_8 = value;
		Il2CppCodeGenWriteBarrier((&___shifts1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIJNDAELENGINE_TDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_H
#ifndef RSABLINDEDENGINE_T19C28C25C083528DA6463A951357512B4D140275_H
#define RSABLINDEDENGINE_T19C28C25C083528DA6463A951357512B4D140275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine
struct  RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IRsa BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine::core
	RuntimeObject* ___core_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine::key
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___key_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindedEngine::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_2;

public:
	inline static int32_t get_offset_of_core_0() { return static_cast<int32_t>(offsetof(RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275, ___core_0)); }
	inline RuntimeObject* get_core_0() const { return ___core_0; }
	inline RuntimeObject** get_address_of_core_0() { return &___core_0; }
	inline void set_core_0(RuntimeObject* value)
	{
		___core_0 = value;
		Il2CppCodeGenWriteBarrier((&___core_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275, ___key_1)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_key_1() const { return ___key_1; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275, ___random_2)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSABLINDEDENGINE_T19C28C25C083528DA6463A951357512B4D140275_H
#ifndef RSABLINDINGENGINE_T755813533965F407DC564072721F62A04C61E751_H
#define RSABLINDINGENGINE_T755813533965F407DC564072721F62A04C61E751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindingEngine
struct  RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IRsa BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindingEngine::core
	RuntimeObject* ___core_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindingEngine::key
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___key_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindingEngine::blindingFactor
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___blindingFactor_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaBlindingEngine::forEncryption
	bool ___forEncryption_3;

public:
	inline static int32_t get_offset_of_core_0() { return static_cast<int32_t>(offsetof(RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751, ___core_0)); }
	inline RuntimeObject* get_core_0() const { return ___core_0; }
	inline RuntimeObject** get_address_of_core_0() { return &___core_0; }
	inline void set_core_0(RuntimeObject* value)
	{
		___core_0 = value;
		Il2CppCodeGenWriteBarrier((&___core_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751, ___key_1)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_key_1() const { return ___key_1; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_blindingFactor_2() { return static_cast<int32_t>(offsetof(RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751, ___blindingFactor_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_blindingFactor_2() const { return ___blindingFactor_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_blindingFactor_2() { return &___blindingFactor_2; }
	inline void set_blindingFactor_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___blindingFactor_2 = value;
		Il2CppCodeGenWriteBarrier((&___blindingFactor_2), value);
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSABLINDINGENGINE_T755813533965F407DC564072721F62A04C61E751_H
#ifndef RSACOREENGINE_T673217EF908CCCE69C24D0D7D9CC35F6C6B839C6_H
#define RSACOREENGINE_T673217EF908CCCE69C24D0D7D9CC35F6C6B839C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaCoreEngine
struct  RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaCoreEngine::key
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___key_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaCoreEngine::forEncryption
	bool ___forEncryption_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaCoreEngine::bitSize
	int32_t ___bitSize_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6, ___key_0)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_key_0() const { return ___key_0; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_forEncryption_1() { return static_cast<int32_t>(offsetof(RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6, ___forEncryption_1)); }
	inline bool get_forEncryption_1() const { return ___forEncryption_1; }
	inline bool* get_address_of_forEncryption_1() { return &___forEncryption_1; }
	inline void set_forEncryption_1(bool value)
	{
		___forEncryption_1 = value;
	}

	inline static int32_t get_offset_of_bitSize_2() { return static_cast<int32_t>(offsetof(RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6, ___bitSize_2)); }
	inline int32_t get_bitSize_2() const { return ___bitSize_2; }
	inline int32_t* get_address_of_bitSize_2() { return &___bitSize_2; }
	inline void set_bitSize_2(int32_t value)
	{
		___bitSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSACOREENGINE_T673217EF908CCCE69C24D0D7D9CC35F6C6B839C6_H
#ifndef RSAENGINE_T84355B05A10A2D620A309F7E436B5A901AF720F0_H
#define RSAENGINE_T84355B05A10A2D620A309F7E436B5A901AF720F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaEngine
struct  RsaEngine_t84355B05A10A2D620A309F7E436B5A901AF720F0  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IRsa BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.RsaEngine::core
	RuntimeObject* ___core_0;

public:
	inline static int32_t get_offset_of_core_0() { return static_cast<int32_t>(offsetof(RsaEngine_t84355B05A10A2D620A309F7E436B5A901AF720F0, ___core_0)); }
	inline RuntimeObject* get_core_0() const { return ___core_0; }
	inline RuntimeObject** get_address_of_core_0() { return &___core_0; }
	inline void set_core_0(RuntimeObject* value)
	{
		___core_0 = value;
		Il2CppCodeGenWriteBarrier((&___core_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAENGINE_T84355B05A10A2D620A309F7E436B5A901AF720F0_H
#ifndef SM2ENGINE_TB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D_H
#define SM2ENGINE_TB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine
struct  SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IDigest BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine::mDigest
	RuntimeObject* ___mDigest_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine::mForEncryption
	bool ___mForEncryption_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine::mECKey
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * ___mECKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine::mECParams
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * ___mECParams_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine::mCurveLength
	int32_t ___mCurveLength_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM2Engine::mRandom
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___mRandom_5;

public:
	inline static int32_t get_offset_of_mDigest_0() { return static_cast<int32_t>(offsetof(SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D, ___mDigest_0)); }
	inline RuntimeObject* get_mDigest_0() const { return ___mDigest_0; }
	inline RuntimeObject** get_address_of_mDigest_0() { return &___mDigest_0; }
	inline void set_mDigest_0(RuntimeObject* value)
	{
		___mDigest_0 = value;
		Il2CppCodeGenWriteBarrier((&___mDigest_0), value);
	}

	inline static int32_t get_offset_of_mForEncryption_1() { return static_cast<int32_t>(offsetof(SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D, ___mForEncryption_1)); }
	inline bool get_mForEncryption_1() const { return ___mForEncryption_1; }
	inline bool* get_address_of_mForEncryption_1() { return &___mForEncryption_1; }
	inline void set_mForEncryption_1(bool value)
	{
		___mForEncryption_1 = value;
	}

	inline static int32_t get_offset_of_mECKey_2() { return static_cast<int32_t>(offsetof(SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D, ___mECKey_2)); }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * get_mECKey_2() const { return ___mECKey_2; }
	inline ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 ** get_address_of_mECKey_2() { return &___mECKey_2; }
	inline void set_mECKey_2(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4 * value)
	{
		___mECKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___mECKey_2), value);
	}

	inline static int32_t get_offset_of_mECParams_3() { return static_cast<int32_t>(offsetof(SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D, ___mECParams_3)); }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * get_mECParams_3() const { return ___mECParams_3; }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C ** get_address_of_mECParams_3() { return &___mECParams_3; }
	inline void set_mECParams_3(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * value)
	{
		___mECParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___mECParams_3), value);
	}

	inline static int32_t get_offset_of_mCurveLength_4() { return static_cast<int32_t>(offsetof(SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D, ___mCurveLength_4)); }
	inline int32_t get_mCurveLength_4() const { return ___mCurveLength_4; }
	inline int32_t* get_address_of_mCurveLength_4() { return &___mCurveLength_4; }
	inline void set_mCurveLength_4(int32_t value)
	{
		___mCurveLength_4 = value;
	}

	inline static int32_t get_offset_of_mRandom_5() { return static_cast<int32_t>(offsetof(SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D, ___mRandom_5)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_mRandom_5() const { return ___mRandom_5; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_mRandom_5() { return &___mRandom_5; }
	inline void set_mRandom_5(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___mRandom_5 = value;
		Il2CppCodeGenWriteBarrier((&___mRandom_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2ENGINE_TB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D_H
#ifndef SM4ENGINE_T05F4D2F0D9D28729873BACADBD6CDFB527225559_H
#define SM4ENGINE_T05F4D2F0D9D28729873BACADBD6CDFB527225559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM4Engine
struct  SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM4Engine::rk
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___rk_4;

public:
	inline static int32_t get_offset_of_rk_4() { return static_cast<int32_t>(offsetof(SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559, ___rk_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_rk_4() const { return ___rk_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_rk_4() { return &___rk_4; }
	inline void set_rk_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___rk_4 = value;
		Il2CppCodeGenWriteBarrier((&___rk_4), value);
	}
};

struct SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM4Engine::Sbox
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Sbox_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM4Engine::CK
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___CK_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SM4Engine::FK
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___FK_3;

public:
	inline static int32_t get_offset_of_Sbox_1() { return static_cast<int32_t>(offsetof(SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields, ___Sbox_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Sbox_1() const { return ___Sbox_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Sbox_1() { return &___Sbox_1; }
	inline void set_Sbox_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Sbox_1 = value;
		Il2CppCodeGenWriteBarrier((&___Sbox_1), value);
	}

	inline static int32_t get_offset_of_CK_2() { return static_cast<int32_t>(offsetof(SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields, ___CK_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_CK_2() const { return ___CK_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_CK_2() { return &___CK_2; }
	inline void set_CK_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___CK_2 = value;
		Il2CppCodeGenWriteBarrier((&___CK_2), value);
	}

	inline static int32_t get_offset_of_FK_3() { return static_cast<int32_t>(offsetof(SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields, ___FK_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_FK_3() const { return ___FK_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_FK_3() { return &___FK_3; }
	inline void set_FK_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___FK_3 = value;
		Il2CppCodeGenWriteBarrier((&___FK_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM4ENGINE_T05F4D2F0D9D28729873BACADBD6CDFB527225559_H
#ifndef SALSA20ENGINE_T521B446CFC278026CC81B6017B3797C847F42C69_H
#define SALSA20ENGINE_T521B446CFC278026CC81B6017B3797C847F42C69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine
struct  Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::rounds
	int32_t ___rounds_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::index
	int32_t ___index_6;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::engineState
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___engineState_7;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_8;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::keyStream
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyStream_9;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::initialised
	bool ___initialised_10;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW0
	uint32_t ___cW0_11;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW1
	uint32_t ___cW1_12;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW2
	uint32_t ___cW2_13;

public:
	inline static int32_t get_offset_of_rounds_5() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___rounds_5)); }
	inline int32_t get_rounds_5() const { return ___rounds_5; }
	inline int32_t* get_address_of_rounds_5() { return &___rounds_5; }
	inline void set_rounds_5(int32_t value)
	{
		___rounds_5 = value;
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}

	inline static int32_t get_offset_of_engineState_7() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___engineState_7)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_engineState_7() const { return ___engineState_7; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_engineState_7() { return &___engineState_7; }
	inline void set_engineState_7(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___engineState_7 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_7), value);
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___x_8)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_8() const { return ___x_8; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_8 = value;
		Il2CppCodeGenWriteBarrier((&___x_8), value);
	}

	inline static int32_t get_offset_of_keyStream_9() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___keyStream_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyStream_9() const { return ___keyStream_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyStream_9() { return &___keyStream_9; }
	inline void set_keyStream_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyStream_9 = value;
		Il2CppCodeGenWriteBarrier((&___keyStream_9), value);
	}

	inline static int32_t get_offset_of_initialised_10() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___initialised_10)); }
	inline bool get_initialised_10() const { return ___initialised_10; }
	inline bool* get_address_of_initialised_10() { return &___initialised_10; }
	inline void set_initialised_10(bool value)
	{
		___initialised_10 = value;
	}

	inline static int32_t get_offset_of_cW0_11() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___cW0_11)); }
	inline uint32_t get_cW0_11() const { return ___cW0_11; }
	inline uint32_t* get_address_of_cW0_11() { return &___cW0_11; }
	inline void set_cW0_11(uint32_t value)
	{
		___cW0_11 = value;
	}

	inline static int32_t get_offset_of_cW1_12() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___cW1_12)); }
	inline uint32_t get_cW1_12() const { return ___cW1_12; }
	inline uint32_t* get_address_of_cW1_12() { return &___cW1_12; }
	inline void set_cW1_12(uint32_t value)
	{
		___cW1_12 = value;
	}

	inline static int32_t get_offset_of_cW2_13() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69, ___cW2_13)); }
	inline uint32_t get_cW2_13() const { return ___cW2_13; }
	inline uint32_t* get_address_of_cW2_13() { return &___cW2_13; }
	inline void set_cW2_13(uint32_t value)
	{
		___cW2_13 = value;
	}
};

struct Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::DEFAULT_ROUNDS
	int32_t ___DEFAULT_ROUNDS_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::TAU_SIGMA
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___TAU_SIGMA_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::sigma
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sigma_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.Salsa20Engine::tau
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tau_4;

public:
	inline static int32_t get_offset_of_DEFAULT_ROUNDS_0() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___DEFAULT_ROUNDS_0)); }
	inline int32_t get_DEFAULT_ROUNDS_0() const { return ___DEFAULT_ROUNDS_0; }
	inline int32_t* get_address_of_DEFAULT_ROUNDS_0() { return &___DEFAULT_ROUNDS_0; }
	inline void set_DEFAULT_ROUNDS_0(int32_t value)
	{
		___DEFAULT_ROUNDS_0 = value;
	}

	inline static int32_t get_offset_of_TAU_SIGMA_2() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___TAU_SIGMA_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_TAU_SIGMA_2() const { return ___TAU_SIGMA_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_TAU_SIGMA_2() { return &___TAU_SIGMA_2; }
	inline void set_TAU_SIGMA_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___TAU_SIGMA_2 = value;
		Il2CppCodeGenWriteBarrier((&___TAU_SIGMA_2), value);
	}

	inline static int32_t get_offset_of_sigma_3() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___sigma_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sigma_3() const { return ___sigma_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sigma_3() { return &___sigma_3; }
	inline void set_sigma_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sigma_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigma_3), value);
	}

	inline static int32_t get_offset_of_tau_4() { return static_cast<int32_t>(offsetof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields, ___tau_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tau_4() const { return ___tau_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tau_4() { return &___tau_4; }
	inline void set_tau_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tau_4 = value;
		Il2CppCodeGenWriteBarrier((&___tau_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALSA20ENGINE_T521B446CFC278026CC81B6017B3797C847F42C69_H
#ifndef SEEDENGINE_TD49DE21249C0717632E3A37CBCCCE62F44DBA18A_H
#define SEEDENGINE_TD49DE21249C0717632E3A37CBCCCE62F44DBA18A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine
struct  SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::wKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___wKey_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::forEncryption
	bool ___forEncryption_7;

public:
	inline static int32_t get_offset_of_wKey_6() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A, ___wKey_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_wKey_6() const { return ___wKey_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_wKey_6() { return &___wKey_6; }
	inline void set_wKey_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___wKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___wKey_6), value);
	}

	inline static int32_t get_offset_of_forEncryption_7() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A, ___forEncryption_7)); }
	inline bool get_forEncryption_7() const { return ___forEncryption_7; }
	inline bool* get_address_of_forEncryption_7() { return &___forEncryption_7; }
	inline void set_forEncryption_7(bool value)
	{
		___forEncryption_7 = value;
	}
};

struct SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::SS0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS0_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::SS1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS1_2;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::SS2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS2_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::SS3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS3_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedEngine::KC
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KC_5;

public:
	inline static int32_t get_offset_of_SS0_1() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields, ___SS0_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS0_1() const { return ___SS0_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS0_1() { return &___SS0_1; }
	inline void set_SS0_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS0_1 = value;
		Il2CppCodeGenWriteBarrier((&___SS0_1), value);
	}

	inline static int32_t get_offset_of_SS1_2() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields, ___SS1_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS1_2() const { return ___SS1_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS1_2() { return &___SS1_2; }
	inline void set_SS1_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS1_2 = value;
		Il2CppCodeGenWriteBarrier((&___SS1_2), value);
	}

	inline static int32_t get_offset_of_SS2_3() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields, ___SS2_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS2_3() const { return ___SS2_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS2_3() { return &___SS2_3; }
	inline void set_SS2_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS2_3 = value;
		Il2CppCodeGenWriteBarrier((&___SS2_3), value);
	}

	inline static int32_t get_offset_of_SS3_4() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields, ___SS3_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS3_4() const { return ___SS3_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS3_4() { return &___SS3_4; }
	inline void set_SS3_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS3_4 = value;
		Il2CppCodeGenWriteBarrier((&___SS3_4), value);
	}

	inline static int32_t get_offset_of_KC_5() { return static_cast<int32_t>(offsetof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields, ___KC_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KC_5() const { return ___KC_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KC_5() { return &___KC_5; }
	inline void set_KC_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KC_5 = value;
		Il2CppCodeGenWriteBarrier((&___KC_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEDENGINE_TD49DE21249C0717632E3A37CBCCCE62F44DBA18A_H
#ifndef SERPENTENGINEBASE_TD001D8D52816040D5378C0AF71E577D684BEE71A_H
#define SERPENTENGINEBASE_TD001D8D52816040D5378C0AF71E577D684BEE71A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase
struct  SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::encrypting
	bool ___encrypting_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::wKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___wKey_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X0
	int32_t ___X0_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X1
	int32_t ___X1_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X2
	int32_t ___X2_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::X3
	int32_t ___X3_8;

public:
	inline static int32_t get_offset_of_encrypting_3() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A, ___encrypting_3)); }
	inline bool get_encrypting_3() const { return ___encrypting_3; }
	inline bool* get_address_of_encrypting_3() { return &___encrypting_3; }
	inline void set_encrypting_3(bool value)
	{
		___encrypting_3 = value;
	}

	inline static int32_t get_offset_of_wKey_4() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A, ___wKey_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_wKey_4() const { return ___wKey_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_wKey_4() { return &___wKey_4; }
	inline void set_wKey_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___wKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___wKey_4), value);
	}

	inline static int32_t get_offset_of_X0_5() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A, ___X0_5)); }
	inline int32_t get_X0_5() const { return ___X0_5; }
	inline int32_t* get_address_of_X0_5() { return &___X0_5; }
	inline void set_X0_5(int32_t value)
	{
		___X0_5 = value;
	}

	inline static int32_t get_offset_of_X1_6() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A, ___X1_6)); }
	inline int32_t get_X1_6() const { return ___X1_6; }
	inline int32_t* get_address_of_X1_6() { return &___X1_6; }
	inline void set_X1_6(int32_t value)
	{
		___X1_6 = value;
	}

	inline static int32_t get_offset_of_X2_7() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A, ___X2_7)); }
	inline int32_t get_X2_7() const { return ___X2_7; }
	inline int32_t* get_address_of_X2_7() { return &___X2_7; }
	inline void set_X2_7(int32_t value)
	{
		___X2_7 = value;
	}

	inline static int32_t get_offset_of_X3_8() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A, ___X3_8)); }
	inline int32_t get_X3_8() const { return ___X3_8; }
	inline int32_t* get_address_of_X3_8() { return &___X3_8; }
	inline void set_X3_8(int32_t value)
	{
		___X3_8 = value;
	}
};

struct SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngineBase::BlockSize
	int32_t ___BlockSize_0;

public:
	inline static int32_t get_offset_of_BlockSize_0() { return static_cast<int32_t>(offsetof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A_StaticFields, ___BlockSize_0)); }
	inline int32_t get_BlockSize_0() const { return ___BlockSize_0; }
	inline int32_t* get_address_of_BlockSize_0() { return &___BlockSize_0; }
	inline void set_BlockSize_0(int32_t value)
	{
		___BlockSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERPENTENGINEBASE_TD001D8D52816040D5378C0AF71E577D684BEE71A_H
#ifndef SKIPJACKENGINE_TE953AF794BEDF6C9BBB30C738717A53BADCCDB38_H
#define SKIPJACKENGINE_TE953AF794BEDF6C9BBB30C738717A53BADCCDB38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine
struct  SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine::key0
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___key0_2;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine::key1
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___key1_3;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine::key2
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___key2_4;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine::key3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___key3_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine::encrypting
	bool ___encrypting_6;

public:
	inline static int32_t get_offset_of_key0_2() { return static_cast<int32_t>(offsetof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38, ___key0_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_key0_2() const { return ___key0_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_key0_2() { return &___key0_2; }
	inline void set_key0_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___key0_2 = value;
		Il2CppCodeGenWriteBarrier((&___key0_2), value);
	}

	inline static int32_t get_offset_of_key1_3() { return static_cast<int32_t>(offsetof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38, ___key1_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_key1_3() const { return ___key1_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_key1_3() { return &___key1_3; }
	inline void set_key1_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___key1_3 = value;
		Il2CppCodeGenWriteBarrier((&___key1_3), value);
	}

	inline static int32_t get_offset_of_key2_4() { return static_cast<int32_t>(offsetof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38, ___key2_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_key2_4() const { return ___key2_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_key2_4() { return &___key2_4; }
	inline void set_key2_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___key2_4 = value;
		Il2CppCodeGenWriteBarrier((&___key2_4), value);
	}

	inline static int32_t get_offset_of_key3_5() { return static_cast<int32_t>(offsetof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38, ___key3_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_key3_5() const { return ___key3_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_key3_5() { return &___key3_5; }
	inline void set_key3_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___key3_5 = value;
		Il2CppCodeGenWriteBarrier((&___key3_5), value);
	}

	inline static int32_t get_offset_of_encrypting_6() { return static_cast<int32_t>(offsetof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38, ___encrypting_6)); }
	inline bool get_encrypting_6() const { return ___encrypting_6; }
	inline bool* get_address_of_encrypting_6() { return &___encrypting_6; }
	inline void set_encrypting_6(bool value)
	{
		___encrypting_6 = value;
	}
};

struct SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38_StaticFields
{
public:
	// System.Int16[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SkipjackEngine::ftable
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___ftable_1;

public:
	inline static int32_t get_offset_of_ftable_1() { return static_cast<int32_t>(offsetof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38_StaticFields, ___ftable_1)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_ftable_1() const { return ___ftable_1; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_ftable_1() { return &___ftable_1; }
	inline void set_ftable_1(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___ftable_1 = value;
		Il2CppCodeGenWriteBarrier((&___ftable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIPJACKENGINE_TE953AF794BEDF6C9BBB30C738717A53BADCCDB38_H
#ifndef TEAENGINE_TC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393_H
#define TEAENGINE_TC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine
struct  TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393  : public RuntimeObject
{
public:
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine::_a
	uint32_t ____a_4;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine::_b
	uint32_t ____b_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine::_c
	uint32_t ____c_6;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine::_d
	uint32_t ____d_7;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine::_initialised
	bool ____initialised_8;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TeaEngine::_forEncryption
	bool ____forEncryption_9;

public:
	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393, ____a_4)); }
	inline uint32_t get__a_4() const { return ____a_4; }
	inline uint32_t* get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(uint32_t value)
	{
		____a_4 = value;
	}

	inline static int32_t get_offset_of__b_5() { return static_cast<int32_t>(offsetof(TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393, ____b_5)); }
	inline uint32_t get__b_5() const { return ____b_5; }
	inline uint32_t* get_address_of__b_5() { return &____b_5; }
	inline void set__b_5(uint32_t value)
	{
		____b_5 = value;
	}

	inline static int32_t get_offset_of__c_6() { return static_cast<int32_t>(offsetof(TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393, ____c_6)); }
	inline uint32_t get__c_6() const { return ____c_6; }
	inline uint32_t* get_address_of__c_6() { return &____c_6; }
	inline void set__c_6(uint32_t value)
	{
		____c_6 = value;
	}

	inline static int32_t get_offset_of__d_7() { return static_cast<int32_t>(offsetof(TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393, ____d_7)); }
	inline uint32_t get__d_7() const { return ____d_7; }
	inline uint32_t* get_address_of__d_7() { return &____d_7; }
	inline void set__d_7(uint32_t value)
	{
		____d_7 = value;
	}

	inline static int32_t get_offset_of__initialised_8() { return static_cast<int32_t>(offsetof(TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393, ____initialised_8)); }
	inline bool get__initialised_8() const { return ____initialised_8; }
	inline bool* get_address_of__initialised_8() { return &____initialised_8; }
	inline void set__initialised_8(bool value)
	{
		____initialised_8 = value;
	}

	inline static int32_t get_offset_of__forEncryption_9() { return static_cast<int32_t>(offsetof(TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393, ____forEncryption_9)); }
	inline bool get__forEncryption_9() const { return ____forEncryption_9; }
	inline bool* get_address_of__forEncryption_9() { return &____forEncryption_9; }
	inline void set__forEncryption_9(bool value)
	{
		____forEncryption_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAENGINE_TC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393_H
#ifndef THREEFISHENGINE_T87218E8DD191868F6F79AC24C90C5772990699DE_H
#define THREEFISHENGINE_T87218E8DD191868F6F79AC24C90C5772990699DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine
struct  ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::blocksizeBytes
	int32_t ___blocksizeBytes_14;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::blocksizeWords
	int32_t ___blocksizeWords_15;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::currentBlock
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___currentBlock_16;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::t
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___t_17;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::kw
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___kw_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_ThreefishCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::cipher
	ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3 * ___cipher_19;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::forEncryption
	bool ___forEncryption_20;

public:
	inline static int32_t get_offset_of_blocksizeBytes_14() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___blocksizeBytes_14)); }
	inline int32_t get_blocksizeBytes_14() const { return ___blocksizeBytes_14; }
	inline int32_t* get_address_of_blocksizeBytes_14() { return &___blocksizeBytes_14; }
	inline void set_blocksizeBytes_14(int32_t value)
	{
		___blocksizeBytes_14 = value;
	}

	inline static int32_t get_offset_of_blocksizeWords_15() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___blocksizeWords_15)); }
	inline int32_t get_blocksizeWords_15() const { return ___blocksizeWords_15; }
	inline int32_t* get_address_of_blocksizeWords_15() { return &___blocksizeWords_15; }
	inline void set_blocksizeWords_15(int32_t value)
	{
		___blocksizeWords_15 = value;
	}

	inline static int32_t get_offset_of_currentBlock_16() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___currentBlock_16)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_currentBlock_16() const { return ___currentBlock_16; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_currentBlock_16() { return &___currentBlock_16; }
	inline void set_currentBlock_16(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___currentBlock_16 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlock_16), value);
	}

	inline static int32_t get_offset_of_t_17() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___t_17)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_t_17() const { return ___t_17; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_t_17() { return &___t_17; }
	inline void set_t_17(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___t_17 = value;
		Il2CppCodeGenWriteBarrier((&___t_17), value);
	}

	inline static int32_t get_offset_of_kw_18() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___kw_18)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_kw_18() const { return ___kw_18; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_kw_18() { return &___kw_18; }
	inline void set_kw_18(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___kw_18 = value;
		Il2CppCodeGenWriteBarrier((&___kw_18), value);
	}

	inline static int32_t get_offset_of_cipher_19() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___cipher_19)); }
	inline ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3 * get_cipher_19() const { return ___cipher_19; }
	inline ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3 ** get_address_of_cipher_19() { return &___cipher_19; }
	inline void set_cipher_19(ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3 * value)
	{
		___cipher_19 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_19), value);
	}

	inline static int32_t get_offset_of_forEncryption_20() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE, ___forEncryption_20)); }
	inline bool get_forEncryption_20() const { return ___forEncryption_20; }
	inline bool* get_address_of_forEncryption_20() { return &___forEncryption_20; }
	inline void set_forEncryption_20(bool value)
	{
		___forEncryption_20 = value;
	}
};

struct ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields
{
public:
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::MOD9
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___MOD9_10;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::MOD17
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___MOD17_11;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::MOD5
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___MOD5_12;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine::MOD3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___MOD3_13;

public:
	inline static int32_t get_offset_of_MOD9_10() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields, ___MOD9_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_MOD9_10() const { return ___MOD9_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_MOD9_10() { return &___MOD9_10; }
	inline void set_MOD9_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___MOD9_10 = value;
		Il2CppCodeGenWriteBarrier((&___MOD9_10), value);
	}

	inline static int32_t get_offset_of_MOD17_11() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields, ___MOD17_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_MOD17_11() const { return ___MOD17_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_MOD17_11() { return &___MOD17_11; }
	inline void set_MOD17_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___MOD17_11 = value;
		Il2CppCodeGenWriteBarrier((&___MOD17_11), value);
	}

	inline static int32_t get_offset_of_MOD5_12() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields, ___MOD5_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_MOD5_12() const { return ___MOD5_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_MOD5_12() { return &___MOD5_12; }
	inline void set_MOD5_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___MOD5_12 = value;
		Il2CppCodeGenWriteBarrier((&___MOD5_12), value);
	}

	inline static int32_t get_offset_of_MOD3_13() { return static_cast<int32_t>(offsetof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields, ___MOD3_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_MOD3_13() const { return ___MOD3_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_MOD3_13() { return &___MOD3_13; }
	inline void set_MOD3_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___MOD3_13 = value;
		Il2CppCodeGenWriteBarrier((&___MOD3_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREEFISHENGINE_T87218E8DD191868F6F79AC24C90C5772990699DE_H
#ifndef THREEFISHCIPHER_T721B1B330C79C107D4822CA3444094FA9811F5A3_H
#define THREEFISHCIPHER_T721B1B330C79C107D4822CA3444094FA9811F5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_ThreefishCipher
struct  ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3  : public RuntimeObject
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_ThreefishCipher::t
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___t_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_ThreefishCipher::kw
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___kw_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3, ___t_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_t_0() const { return ___t_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_kw_1() { return static_cast<int32_t>(offsetof(ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3, ___kw_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_kw_1() const { return ___kw_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_kw_1() { return &___kw_1; }
	inline void set_kw_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___kw_1 = value;
		Il2CppCodeGenWriteBarrier((&___kw_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREEFISHCIPHER_T721B1B330C79C107D4822CA3444094FA9811F5A3_H
#ifndef TWOFISHENGINE_T06508E128BE900AC8493C3404508CB018BBA9094_H
#define TWOFISHENGINE_T06508E128BE900AC8493C3404508CB018BBA9094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine
struct  TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::encrypting
	bool ___encrypting_36;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::gMDS0
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gMDS0_37;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::gMDS1
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gMDS1_38;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::gMDS2
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gMDS2_39;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::gMDS3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gMDS3_40;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::gSubKeys
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gSubKeys_41;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::gSBox
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___gSBox_42;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::k64Cnt
	int32_t ___k64Cnt_43;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_44;

public:
	inline static int32_t get_offset_of_encrypting_36() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___encrypting_36)); }
	inline bool get_encrypting_36() const { return ___encrypting_36; }
	inline bool* get_address_of_encrypting_36() { return &___encrypting_36; }
	inline void set_encrypting_36(bool value)
	{
		___encrypting_36 = value;
	}

	inline static int32_t get_offset_of_gMDS0_37() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___gMDS0_37)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gMDS0_37() const { return ___gMDS0_37; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gMDS0_37() { return &___gMDS0_37; }
	inline void set_gMDS0_37(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gMDS0_37 = value;
		Il2CppCodeGenWriteBarrier((&___gMDS0_37), value);
	}

	inline static int32_t get_offset_of_gMDS1_38() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___gMDS1_38)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gMDS1_38() const { return ___gMDS1_38; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gMDS1_38() { return &___gMDS1_38; }
	inline void set_gMDS1_38(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gMDS1_38 = value;
		Il2CppCodeGenWriteBarrier((&___gMDS1_38), value);
	}

	inline static int32_t get_offset_of_gMDS2_39() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___gMDS2_39)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gMDS2_39() const { return ___gMDS2_39; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gMDS2_39() { return &___gMDS2_39; }
	inline void set_gMDS2_39(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gMDS2_39 = value;
		Il2CppCodeGenWriteBarrier((&___gMDS2_39), value);
	}

	inline static int32_t get_offset_of_gMDS3_40() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___gMDS3_40)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gMDS3_40() const { return ___gMDS3_40; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gMDS3_40() { return &___gMDS3_40; }
	inline void set_gMDS3_40(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gMDS3_40 = value;
		Il2CppCodeGenWriteBarrier((&___gMDS3_40), value);
	}

	inline static int32_t get_offset_of_gSubKeys_41() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___gSubKeys_41)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gSubKeys_41() const { return ___gSubKeys_41; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gSubKeys_41() { return &___gSubKeys_41; }
	inline void set_gSubKeys_41(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gSubKeys_41 = value;
		Il2CppCodeGenWriteBarrier((&___gSubKeys_41), value);
	}

	inline static int32_t get_offset_of_gSBox_42() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___gSBox_42)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_gSBox_42() const { return ___gSBox_42; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_gSBox_42() { return &___gSBox_42; }
	inline void set_gSBox_42(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___gSBox_42 = value;
		Il2CppCodeGenWriteBarrier((&___gSBox_42), value);
	}

	inline static int32_t get_offset_of_k64Cnt_43() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___k64Cnt_43)); }
	inline int32_t get_k64Cnt_43() const { return ___k64Cnt_43; }
	inline int32_t* get_address_of_k64Cnt_43() { return &___k64Cnt_43; }
	inline void set_k64Cnt_43(int32_t value)
	{
		___k64Cnt_43 = value;
	}

	inline static int32_t get_offset_of_workingKey_44() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094, ___workingKey_44)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_44() const { return ___workingKey_44; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_44() { return &___workingKey_44; }
	inline void set_workingKey_44(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_44 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_44), value);
	}
};

struct TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094_StaticFields
{
public:
	// System.Byte[0...,0...] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TwofishEngine::P
	ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* ___P_0;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094_StaticFields, ___P_0)); }
	inline ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* get_P_0() const { return ___P_0; }
	inline ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(ByteU5BU2CU5D_t389906995615195DEDAFBA7E7E1C5E51115C345D* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOFISHENGINE_T06508E128BE900AC8493C3404508CB018BBA9094_H
#ifndef VMPCENGINE_T6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60_H
#define VMPCENGINE_T6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcEngine
struct  VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60  : public RuntimeObject
{
public:
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcEngine::n
	uint8_t ___n_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcEngine::P
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___P_1;
	// System.Byte BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcEngine::s
	uint8_t ___s_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcEngine::workingIV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingIV_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcEngine::workingKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___workingKey_4;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60, ___n_0)); }
	inline uint8_t get_n_0() const { return ___n_0; }
	inline uint8_t* get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(uint8_t value)
	{
		___n_0 = value;
	}

	inline static int32_t get_offset_of_P_1() { return static_cast<int32_t>(offsetof(VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60, ___P_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_P_1() const { return ___P_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_P_1() { return &___P_1; }
	inline void set_P_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___P_1 = value;
		Il2CppCodeGenWriteBarrier((&___P_1), value);
	}

	inline static int32_t get_offset_of_s_2() { return static_cast<int32_t>(offsetof(VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60, ___s_2)); }
	inline uint8_t get_s_2() const { return ___s_2; }
	inline uint8_t* get_address_of_s_2() { return &___s_2; }
	inline void set_s_2(uint8_t value)
	{
		___s_2 = value;
	}

	inline static int32_t get_offset_of_workingIV_3() { return static_cast<int32_t>(offsetof(VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60, ___workingIV_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingIV_3() const { return ___workingIV_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingIV_3() { return &___workingIV_3; }
	inline void set_workingIV_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingIV_3 = value;
		Il2CppCodeGenWriteBarrier((&___workingIV_3), value);
	}

	inline static int32_t get_offset_of_workingKey_4() { return static_cast<int32_t>(offsetof(VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60, ___workingKey_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_workingKey_4() const { return ___workingKey_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_workingKey_4() { return &___workingKey_4; }
	inline void set_workingKey_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___workingKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___workingKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VMPCENGINE_T6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60_H
#ifndef XTEAENGINE_T8BEE866A19A0A760B7585D9725AB12791F42DE1E_H
#define XTEAENGINE_T8BEE866A19A0A760B7585D9725AB12791F42DE1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XteaEngine
struct  XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E  : public RuntimeObject
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XteaEngine::_S
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____S_3;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XteaEngine::_sum0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____sum0_4;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XteaEngine::_sum1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____sum1_5;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XteaEngine::_initialised
	bool ____initialised_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XteaEngine::_forEncryption
	bool ____forEncryption_7;

public:
	inline static int32_t get_offset_of__S_3() { return static_cast<int32_t>(offsetof(XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E, ____S_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__S_3() const { return ____S_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__S_3() { return &____S_3; }
	inline void set__S_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____S_3 = value;
		Il2CppCodeGenWriteBarrier((&____S_3), value);
	}

	inline static int32_t get_offset_of__sum0_4() { return static_cast<int32_t>(offsetof(XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E, ____sum0_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__sum0_4() const { return ____sum0_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__sum0_4() { return &____sum0_4; }
	inline void set__sum0_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____sum0_4 = value;
		Il2CppCodeGenWriteBarrier((&____sum0_4), value);
	}

	inline static int32_t get_offset_of__sum1_5() { return static_cast<int32_t>(offsetof(XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E, ____sum1_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__sum1_5() const { return ____sum1_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__sum1_5() { return &____sum1_5; }
	inline void set__sum1_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____sum1_5 = value;
		Il2CppCodeGenWriteBarrier((&____sum1_5), value);
	}

	inline static int32_t get_offset_of__initialised_6() { return static_cast<int32_t>(offsetof(XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E, ____initialised_6)); }
	inline bool get__initialised_6() const { return ____initialised_6; }
	inline bool* get_address_of__initialised_6() { return &____initialised_6; }
	inline void set__initialised_6(bool value)
	{
		____initialised_6 = value;
	}

	inline static int32_t get_offset_of__forEncryption_7() { return static_cast<int32_t>(offsetof(XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E, ____forEncryption_7)); }
	inline bool get__forEncryption_7() const { return ____forEncryption_7; }
	inline bool* get_address_of__forEncryption_7() { return &____forEncryption_7; }
	inline void set__forEncryption_7(bool value)
	{
		____forEncryption_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEAENGINE_T8BEE866A19A0A760B7585D9725AB12791F42DE1E_H
#ifndef GOST3411_2012_256DIGEST_T1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_H
#define GOST3411_2012_256DIGEST_T1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012_256Digest
struct  Gost3411_2012_256Digest_t1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9  : public Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9
{
public:

public:
};

struct Gost3411_2012_256Digest_t1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012_256Digest::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_12;

public:
	inline static int32_t get_offset_of_IV_12() { return static_cast<int32_t>(offsetof(Gost3411_2012_256Digest_t1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_StaticFields, ___IV_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_12() const { return ___IV_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_12() { return &___IV_12; }
	inline void set_IV_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_12 = value;
		Il2CppCodeGenWriteBarrier((&___IV_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3411_2012_256DIGEST_T1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_H
#ifndef GOST3411_2012_512DIGEST_TA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_H
#define GOST3411_2012_512DIGEST_TA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012_512Digest
struct  Gost3411_2012_512Digest_tA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7  : public Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9
{
public:

public:
};

struct Gost3411_2012_512Digest_tA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.Gost3411_2012_512Digest::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_12;

public:
	inline static int32_t get_offset_of_IV_12() { return static_cast<int32_t>(offsetof(Gost3411_2012_512Digest_tA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_StaticFields, ___IV_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_12() const { return ___IV_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_12() { return &___IV_12; }
	inline void set_IV_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_12 = value;
		Il2CppCodeGenWriteBarrier((&___IV_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3411_2012_512DIGEST_TA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_H
#ifndef MD4DIGEST_TFD8F38C0E884472CCAB18012B94F3C90CE3867A1_H
#define MD4DIGEST_TFD8F38C0E884472CCAB18012B94F3C90CE3867A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest
struct  MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest::H1
	int32_t ___H1_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest::H2
	int32_t ___H2_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest::H3
	int32_t ___H3_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest::H4
	int32_t ___H4_8;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD4Digest::xOff
	int32_t ___xOff_10;

public:
	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1, ___H1_5)); }
	inline int32_t get_H1_5() const { return ___H1_5; }
	inline int32_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(int32_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1, ___H2_6)); }
	inline int32_t get_H2_6() const { return ___H2_6; }
	inline int32_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(int32_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1, ___H3_7)); }
	inline int32_t get_H3_7() const { return ___H3_7; }
	inline int32_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(int32_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1, ___H4_8)); }
	inline int32_t get_H4_8() const { return ___H4_8; }
	inline int32_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(int32_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_X_9() { return static_cast<int32_t>(offsetof(MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1, ___X_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_9() const { return ___X_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_9() { return &___X_9; }
	inline void set_X_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_9 = value;
		Il2CppCodeGenWriteBarrier((&___X_9), value);
	}

	inline static int32_t get_offset_of_xOff_10() { return static_cast<int32_t>(offsetof(MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1, ___xOff_10)); }
	inline int32_t get_xOff_10() const { return ___xOff_10; }
	inline int32_t* get_address_of_xOff_10() { return &___xOff_10; }
	inline void set_xOff_10(int32_t value)
	{
		___xOff_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4DIGEST_TFD8F38C0E884472CCAB18012B94F3C90CE3867A1_H
#ifndef MD5DIGEST_T0BA406F0C3C2A9A325E7885E06F41CD609469A5F_H
#define MD5DIGEST_T0BA406F0C3C2A9A325E7885E06F41CD609469A5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest
struct  MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::H1
	uint32_t ___H1_5;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::H2
	uint32_t ___H2_6;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::H3
	uint32_t ___H3_7;
	// System.UInt32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::H4
	uint32_t ___H4_8;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::X
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___X_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::xOff
	int32_t ___xOff_10;

public:
	inline static int32_t get_offset_of_H1_5() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F, ___H1_5)); }
	inline uint32_t get_H1_5() const { return ___H1_5; }
	inline uint32_t* get_address_of_H1_5() { return &___H1_5; }
	inline void set_H1_5(uint32_t value)
	{
		___H1_5 = value;
	}

	inline static int32_t get_offset_of_H2_6() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F, ___H2_6)); }
	inline uint32_t get_H2_6() const { return ___H2_6; }
	inline uint32_t* get_address_of_H2_6() { return &___H2_6; }
	inline void set_H2_6(uint32_t value)
	{
		___H2_6 = value;
	}

	inline static int32_t get_offset_of_H3_7() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F, ___H3_7)); }
	inline uint32_t get_H3_7() const { return ___H3_7; }
	inline uint32_t* get_address_of_H3_7() { return &___H3_7; }
	inline void set_H3_7(uint32_t value)
	{
		___H3_7 = value;
	}

	inline static int32_t get_offset_of_H4_8() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F, ___H4_8)); }
	inline uint32_t get_H4_8() const { return ___H4_8; }
	inline uint32_t* get_address_of_H4_8() { return &___H4_8; }
	inline void set_H4_8(uint32_t value)
	{
		___H4_8 = value;
	}

	inline static int32_t get_offset_of_X_9() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F, ___X_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_X_9() const { return ___X_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_X_9() { return &___X_9; }
	inline void set_X_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___X_9 = value;
		Il2CppCodeGenWriteBarrier((&___X_9), value);
	}

	inline static int32_t get_offset_of_xOff_10() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F, ___xOff_10)); }
	inline int32_t get_xOff_10() const { return ___xOff_10; }
	inline int32_t* get_address_of_xOff_10() { return &___xOff_10; }
	inline void set_xOff_10(int32_t value)
	{
		___xOff_10 = value;
	}
};

struct MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S11
	int32_t ___S11_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S12
	int32_t ___S12_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S13
	int32_t ___S13_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S14
	int32_t ___S14_14;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S21
	int32_t ___S21_15;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S22
	int32_t ___S22_16;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S23
	int32_t ___S23_17;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S24
	int32_t ___S24_18;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S31
	int32_t ___S31_19;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S32
	int32_t ___S32_20;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S33
	int32_t ___S33_21;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S34
	int32_t ___S34_22;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S41
	int32_t ___S41_23;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S42
	int32_t ___S42_24;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S43
	int32_t ___S43_25;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.MD5Digest::S44
	int32_t ___S44_26;

public:
	inline static int32_t get_offset_of_S11_11() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S11_11)); }
	inline int32_t get_S11_11() const { return ___S11_11; }
	inline int32_t* get_address_of_S11_11() { return &___S11_11; }
	inline void set_S11_11(int32_t value)
	{
		___S11_11 = value;
	}

	inline static int32_t get_offset_of_S12_12() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S12_12)); }
	inline int32_t get_S12_12() const { return ___S12_12; }
	inline int32_t* get_address_of_S12_12() { return &___S12_12; }
	inline void set_S12_12(int32_t value)
	{
		___S12_12 = value;
	}

	inline static int32_t get_offset_of_S13_13() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S13_13)); }
	inline int32_t get_S13_13() const { return ___S13_13; }
	inline int32_t* get_address_of_S13_13() { return &___S13_13; }
	inline void set_S13_13(int32_t value)
	{
		___S13_13 = value;
	}

	inline static int32_t get_offset_of_S14_14() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S14_14)); }
	inline int32_t get_S14_14() const { return ___S14_14; }
	inline int32_t* get_address_of_S14_14() { return &___S14_14; }
	inline void set_S14_14(int32_t value)
	{
		___S14_14 = value;
	}

	inline static int32_t get_offset_of_S21_15() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S21_15)); }
	inline int32_t get_S21_15() const { return ___S21_15; }
	inline int32_t* get_address_of_S21_15() { return &___S21_15; }
	inline void set_S21_15(int32_t value)
	{
		___S21_15 = value;
	}

	inline static int32_t get_offset_of_S22_16() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S22_16)); }
	inline int32_t get_S22_16() const { return ___S22_16; }
	inline int32_t* get_address_of_S22_16() { return &___S22_16; }
	inline void set_S22_16(int32_t value)
	{
		___S22_16 = value;
	}

	inline static int32_t get_offset_of_S23_17() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S23_17)); }
	inline int32_t get_S23_17() const { return ___S23_17; }
	inline int32_t* get_address_of_S23_17() { return &___S23_17; }
	inline void set_S23_17(int32_t value)
	{
		___S23_17 = value;
	}

	inline static int32_t get_offset_of_S24_18() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S24_18)); }
	inline int32_t get_S24_18() const { return ___S24_18; }
	inline int32_t* get_address_of_S24_18() { return &___S24_18; }
	inline void set_S24_18(int32_t value)
	{
		___S24_18 = value;
	}

	inline static int32_t get_offset_of_S31_19() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S31_19)); }
	inline int32_t get_S31_19() const { return ___S31_19; }
	inline int32_t* get_address_of_S31_19() { return &___S31_19; }
	inline void set_S31_19(int32_t value)
	{
		___S31_19 = value;
	}

	inline static int32_t get_offset_of_S32_20() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S32_20)); }
	inline int32_t get_S32_20() const { return ___S32_20; }
	inline int32_t* get_address_of_S32_20() { return &___S32_20; }
	inline void set_S32_20(int32_t value)
	{
		___S32_20 = value;
	}

	inline static int32_t get_offset_of_S33_21() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S33_21)); }
	inline int32_t get_S33_21() const { return ___S33_21; }
	inline int32_t* get_address_of_S33_21() { return &___S33_21; }
	inline void set_S33_21(int32_t value)
	{
		___S33_21 = value;
	}

	inline static int32_t get_offset_of_S34_22() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S34_22)); }
	inline int32_t get_S34_22() const { return ___S34_22; }
	inline int32_t* get_address_of_S34_22() { return &___S34_22; }
	inline void set_S34_22(int32_t value)
	{
		___S34_22 = value;
	}

	inline static int32_t get_offset_of_S41_23() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S41_23)); }
	inline int32_t get_S41_23() const { return ___S41_23; }
	inline int32_t* get_address_of_S41_23() { return &___S41_23; }
	inline void set_S41_23(int32_t value)
	{
		___S41_23 = value;
	}

	inline static int32_t get_offset_of_S42_24() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S42_24)); }
	inline int32_t get_S42_24() const { return ___S42_24; }
	inline int32_t* get_address_of_S42_24() { return &___S42_24; }
	inline void set_S42_24(int32_t value)
	{
		___S42_24 = value;
	}

	inline static int32_t get_offset_of_S43_25() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S43_25)); }
	inline int32_t get_S43_25() const { return ___S43_25; }
	inline int32_t* get_address_of_S43_25() { return &___S43_25; }
	inline void set_S43_25(int32_t value)
	{
		___S43_25 = value;
	}

	inline static int32_t get_offset_of_S44_26() { return static_cast<int32_t>(offsetof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields, ___S44_26)); }
	inline int32_t get_S44_26() const { return ___S44_26; }
	inline int32_t* get_address_of_S44_26() { return &___S44_26; }
	inline void set_S44_26(int32_t value)
	{
		___S44_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5DIGEST_T0BA406F0C3C2A9A325E7885E06F41CD609469A5F_H
#ifndef RIPEMD128DIGEST_T315ADF1D5103AED85039E0512C7E34EF9CA2C3D9_H
#define RIPEMD128DIGEST_T315ADF1D5103AED85039E0512C7E34EF9CA2C3D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest
struct  RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H0
	int32_t ___H0_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H1
	int32_t ___H1_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H2
	int32_t ___H2_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::H3
	int32_t ___H3_8;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD128Digest::xOff
	int32_t ___xOff_10;

public:
	inline static int32_t get_offset_of_H0_5() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9, ___H0_5)); }
	inline int32_t get_H0_5() const { return ___H0_5; }
	inline int32_t* get_address_of_H0_5() { return &___H0_5; }
	inline void set_H0_5(int32_t value)
	{
		___H0_5 = value;
	}

	inline static int32_t get_offset_of_H1_6() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9, ___H1_6)); }
	inline int32_t get_H1_6() const { return ___H1_6; }
	inline int32_t* get_address_of_H1_6() { return &___H1_6; }
	inline void set_H1_6(int32_t value)
	{
		___H1_6 = value;
	}

	inline static int32_t get_offset_of_H2_7() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9, ___H2_7)); }
	inline int32_t get_H2_7() const { return ___H2_7; }
	inline int32_t* get_address_of_H2_7() { return &___H2_7; }
	inline void set_H2_7(int32_t value)
	{
		___H2_7 = value;
	}

	inline static int32_t get_offset_of_H3_8() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9, ___H3_8)); }
	inline int32_t get_H3_8() const { return ___H3_8; }
	inline int32_t* get_address_of_H3_8() { return &___H3_8; }
	inline void set_H3_8(int32_t value)
	{
		___H3_8 = value;
	}

	inline static int32_t get_offset_of_X_9() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9, ___X_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_9() const { return ___X_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_9() { return &___X_9; }
	inline void set_X_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_9 = value;
		Il2CppCodeGenWriteBarrier((&___X_9), value);
	}

	inline static int32_t get_offset_of_xOff_10() { return static_cast<int32_t>(offsetof(RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9, ___xOff_10)); }
	inline int32_t get_xOff_10() const { return ___xOff_10; }
	inline int32_t* get_address_of_xOff_10() { return &___xOff_10; }
	inline void set_xOff_10(int32_t value)
	{
		___xOff_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD128DIGEST_T315ADF1D5103AED85039E0512C7E34EF9CA2C3D9_H
#ifndef RIPEMD160DIGEST_TB79BA430F1A53D5A342070008294BDE56AA350D4_H
#define RIPEMD160DIGEST_TB79BA430F1A53D5A342070008294BDE56AA350D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest
struct  RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H0
	int32_t ___H0_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H1
	int32_t ___H1_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H2
	int32_t ___H2_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H3
	int32_t ___H3_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::H4
	int32_t ___H4_9;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD160Digest::xOff
	int32_t ___xOff_11;

public:
	inline static int32_t get_offset_of_H0_5() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___H0_5)); }
	inline int32_t get_H0_5() const { return ___H0_5; }
	inline int32_t* get_address_of_H0_5() { return &___H0_5; }
	inline void set_H0_5(int32_t value)
	{
		___H0_5 = value;
	}

	inline static int32_t get_offset_of_H1_6() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___H1_6)); }
	inline int32_t get_H1_6() const { return ___H1_6; }
	inline int32_t* get_address_of_H1_6() { return &___H1_6; }
	inline void set_H1_6(int32_t value)
	{
		___H1_6 = value;
	}

	inline static int32_t get_offset_of_H2_7() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___H2_7)); }
	inline int32_t get_H2_7() const { return ___H2_7; }
	inline int32_t* get_address_of_H2_7() { return &___H2_7; }
	inline void set_H2_7(int32_t value)
	{
		___H2_7 = value;
	}

	inline static int32_t get_offset_of_H3_8() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___H3_8)); }
	inline int32_t get_H3_8() const { return ___H3_8; }
	inline int32_t* get_address_of_H3_8() { return &___H3_8; }
	inline void set_H3_8(int32_t value)
	{
		___H3_8 = value;
	}

	inline static int32_t get_offset_of_H4_9() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___H4_9)); }
	inline int32_t get_H4_9() const { return ___H4_9; }
	inline int32_t* get_address_of_H4_9() { return &___H4_9; }
	inline void set_H4_9(int32_t value)
	{
		___H4_9 = value;
	}

	inline static int32_t get_offset_of_X_10() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___X_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_10() const { return ___X_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_10() { return &___X_10; }
	inline void set_X_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_10 = value;
		Il2CppCodeGenWriteBarrier((&___X_10), value);
	}

	inline static int32_t get_offset_of_xOff_11() { return static_cast<int32_t>(offsetof(RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4, ___xOff_11)); }
	inline int32_t get_xOff_11() const { return ___xOff_11; }
	inline int32_t* get_address_of_xOff_11() { return &___xOff_11; }
	inline void set_xOff_11(int32_t value)
	{
		___xOff_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD160DIGEST_TB79BA430F1A53D5A342070008294BDE56AA350D4_H
#ifndef RIPEMD256DIGEST_T329D11C118AE7087208CA6A3706D68C602AFA670_H
#define RIPEMD256DIGEST_T329D11C118AE7087208CA6A3706D68C602AFA670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest
struct  RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H0
	int32_t ___H0_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H1
	int32_t ___H1_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H2
	int32_t ___H2_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H3
	int32_t ___H3_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H4
	int32_t ___H4_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H5
	int32_t ___H5_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H6
	int32_t ___H6_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::H7
	int32_t ___H7_12;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD256Digest::xOff
	int32_t ___xOff_14;

public:
	inline static int32_t get_offset_of_H0_5() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H0_5)); }
	inline int32_t get_H0_5() const { return ___H0_5; }
	inline int32_t* get_address_of_H0_5() { return &___H0_5; }
	inline void set_H0_5(int32_t value)
	{
		___H0_5 = value;
	}

	inline static int32_t get_offset_of_H1_6() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H1_6)); }
	inline int32_t get_H1_6() const { return ___H1_6; }
	inline int32_t* get_address_of_H1_6() { return &___H1_6; }
	inline void set_H1_6(int32_t value)
	{
		___H1_6 = value;
	}

	inline static int32_t get_offset_of_H2_7() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H2_7)); }
	inline int32_t get_H2_7() const { return ___H2_7; }
	inline int32_t* get_address_of_H2_7() { return &___H2_7; }
	inline void set_H2_7(int32_t value)
	{
		___H2_7 = value;
	}

	inline static int32_t get_offset_of_H3_8() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H3_8)); }
	inline int32_t get_H3_8() const { return ___H3_8; }
	inline int32_t* get_address_of_H3_8() { return &___H3_8; }
	inline void set_H3_8(int32_t value)
	{
		___H3_8 = value;
	}

	inline static int32_t get_offset_of_H4_9() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H4_9)); }
	inline int32_t get_H4_9() const { return ___H4_9; }
	inline int32_t* get_address_of_H4_9() { return &___H4_9; }
	inline void set_H4_9(int32_t value)
	{
		___H4_9 = value;
	}

	inline static int32_t get_offset_of_H5_10() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H5_10)); }
	inline int32_t get_H5_10() const { return ___H5_10; }
	inline int32_t* get_address_of_H5_10() { return &___H5_10; }
	inline void set_H5_10(int32_t value)
	{
		___H5_10 = value;
	}

	inline static int32_t get_offset_of_H6_11() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H6_11)); }
	inline int32_t get_H6_11() const { return ___H6_11; }
	inline int32_t* get_address_of_H6_11() { return &___H6_11; }
	inline void set_H6_11(int32_t value)
	{
		___H6_11 = value;
	}

	inline static int32_t get_offset_of_H7_12() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___H7_12)); }
	inline int32_t get_H7_12() const { return ___H7_12; }
	inline int32_t* get_address_of_H7_12() { return &___H7_12; }
	inline void set_H7_12(int32_t value)
	{
		___H7_12 = value;
	}

	inline static int32_t get_offset_of_X_13() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___X_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_13() const { return ___X_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_13() { return &___X_13; }
	inline void set_X_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_13 = value;
		Il2CppCodeGenWriteBarrier((&___X_13), value);
	}

	inline static int32_t get_offset_of_xOff_14() { return static_cast<int32_t>(offsetof(RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670, ___xOff_14)); }
	inline int32_t get_xOff_14() const { return ___xOff_14; }
	inline int32_t* get_address_of_xOff_14() { return &___xOff_14; }
	inline void set_xOff_14(int32_t value)
	{
		___xOff_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD256DIGEST_T329D11C118AE7087208CA6A3706D68C602AFA670_H
#ifndef RIPEMD320DIGEST_T3288B3543CC8BA8FE1456C32A866DC0DC70A9B27_H
#define RIPEMD320DIGEST_T3288B3543CC8BA8FE1456C32A866DC0DC70A9B27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest
struct  RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27  : public GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H0
	int32_t ___H0_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H1
	int32_t ___H1_6;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H2
	int32_t ___H2_7;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H3
	int32_t ___H3_8;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H4
	int32_t ___H4_9;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H5
	int32_t ___H5_10;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H6
	int32_t ___H6_11;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H7
	int32_t ___H7_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H8
	int32_t ___H8_13;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::H9
	int32_t ___H9_14;
	// System.Int32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::X
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___X_15;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Digests.RipeMD320Digest::xOff
	int32_t ___xOff_16;

public:
	inline static int32_t get_offset_of_H0_5() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H0_5)); }
	inline int32_t get_H0_5() const { return ___H0_5; }
	inline int32_t* get_address_of_H0_5() { return &___H0_5; }
	inline void set_H0_5(int32_t value)
	{
		___H0_5 = value;
	}

	inline static int32_t get_offset_of_H1_6() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H1_6)); }
	inline int32_t get_H1_6() const { return ___H1_6; }
	inline int32_t* get_address_of_H1_6() { return &___H1_6; }
	inline void set_H1_6(int32_t value)
	{
		___H1_6 = value;
	}

	inline static int32_t get_offset_of_H2_7() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H2_7)); }
	inline int32_t get_H2_7() const { return ___H2_7; }
	inline int32_t* get_address_of_H2_7() { return &___H2_7; }
	inline void set_H2_7(int32_t value)
	{
		___H2_7 = value;
	}

	inline static int32_t get_offset_of_H3_8() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H3_8)); }
	inline int32_t get_H3_8() const { return ___H3_8; }
	inline int32_t* get_address_of_H3_8() { return &___H3_8; }
	inline void set_H3_8(int32_t value)
	{
		___H3_8 = value;
	}

	inline static int32_t get_offset_of_H4_9() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H4_9)); }
	inline int32_t get_H4_9() const { return ___H4_9; }
	inline int32_t* get_address_of_H4_9() { return &___H4_9; }
	inline void set_H4_9(int32_t value)
	{
		___H4_9 = value;
	}

	inline static int32_t get_offset_of_H5_10() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H5_10)); }
	inline int32_t get_H5_10() const { return ___H5_10; }
	inline int32_t* get_address_of_H5_10() { return &___H5_10; }
	inline void set_H5_10(int32_t value)
	{
		___H5_10 = value;
	}

	inline static int32_t get_offset_of_H6_11() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H6_11)); }
	inline int32_t get_H6_11() const { return ___H6_11; }
	inline int32_t* get_address_of_H6_11() { return &___H6_11; }
	inline void set_H6_11(int32_t value)
	{
		___H6_11 = value;
	}

	inline static int32_t get_offset_of_H7_12() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H7_12)); }
	inline int32_t get_H7_12() const { return ___H7_12; }
	inline int32_t* get_address_of_H7_12() { return &___H7_12; }
	inline void set_H7_12(int32_t value)
	{
		___H7_12 = value;
	}

	inline static int32_t get_offset_of_H8_13() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H8_13)); }
	inline int32_t get_H8_13() const { return ___H8_13; }
	inline int32_t* get_address_of_H8_13() { return &___H8_13; }
	inline void set_H8_13(int32_t value)
	{
		___H8_13 = value;
	}

	inline static int32_t get_offset_of_H9_14() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___H9_14)); }
	inline int32_t get_H9_14() const { return ___H9_14; }
	inline int32_t* get_address_of_H9_14() { return &___H9_14; }
	inline void set_H9_14(int32_t value)
	{
		___H9_14 = value;
	}

	inline static int32_t get_offset_of_X_15() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___X_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_X_15() const { return ___X_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_X_15() { return &___X_15; }
	inline void set_X_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___X_15 = value;
		Il2CppCodeGenWriteBarrier((&___X_15), value);
	}

	inline static int32_t get_offset_of_xOff_16() { return static_cast<int32_t>(offsetof(RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27, ___xOff_16)); }
	inline int32_t get_xOff_16() const { return ___xOff_16; }
	inline int32_t* get_address_of_xOff_16() { return &___xOff_16; }
	inline void set_xOff_16(int32_t value)
	{
		___xOff_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIPEMD320DIGEST_T3288B3543CC8BA8FE1456C32A866DC0DC70A9B27_H
#ifndef CURVE25519HOLDER_TF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_H
#define CURVE25519HOLDER_TF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_Curve25519Holder
struct  Curve25519Holder_tF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Curve25519Holder_tF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_Curve25519Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Curve25519Holder_tF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519HOLDER_TF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_H
#ifndef SM2P256V1HOLDER_TCC06EE13F83A3A5147845DB07B203046BD09B584_H
#define SM2P256V1HOLDER_TCC06EE13F83A3A5147845DB07B203046BD09B584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SM2P256V1Holder
struct  SM2P256V1Holder_tCC06EE13F83A3A5147845DB07B203046BD09B584  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SM2P256V1Holder_tCC06EE13F83A3A5147845DB07B203046BD09B584_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SM2P256V1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SM2P256V1Holder_tCC06EE13F83A3A5147845DB07B203046BD09B584_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1HOLDER_TCC06EE13F83A3A5147845DB07B203046BD09B584_H
#ifndef SECP128R1HOLDER_T0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_H
#define SECP128R1HOLDER_T0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP128R1Holder
struct  SecP128R1Holder_t0BB3169BA0FF61C47E96A3B33CDA413E8081A44D  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP128R1Holder_t0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP128R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP128R1Holder_t0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1HOLDER_T0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_H
#ifndef SECP160K1HOLDER_T9458088EFDAB3C900EC1086D8AC4F2931195A8B6_H
#define SECP160K1HOLDER_T9458088EFDAB3C900EC1086D8AC4F2931195A8B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160K1Holder
struct  SecP160K1Holder_t9458088EFDAB3C900EC1086D8AC4F2931195A8B6  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP160K1Holder_t9458088EFDAB3C900EC1086D8AC4F2931195A8B6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP160K1Holder_t9458088EFDAB3C900EC1086D8AC4F2931195A8B6_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1HOLDER_T9458088EFDAB3C900EC1086D8AC4F2931195A8B6_H
#ifndef SECP160R1HOLDER_TC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_H
#define SECP160R1HOLDER_TC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R1Holder
struct  SecP160R1Holder_tC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP160R1Holder_tC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP160R1Holder_tC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1HOLDER_TC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_H
#ifndef SECP160R2HOLDER_T463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_H
#define SECP160R2HOLDER_T463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R2Holder
struct  SecP160R2Holder_t463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP160R2Holder_t463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP160R2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP160R2Holder_t463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2HOLDER_T463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_H
#ifndef SECP192K1HOLDER_T7EED6ECD76918AED2DA57ADA620E37463643C5D2_H
#define SECP192K1HOLDER_T7EED6ECD76918AED2DA57ADA620E37463643C5D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192K1Holder
struct  SecP192K1Holder_t7EED6ECD76918AED2DA57ADA620E37463643C5D2  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP192K1Holder_t7EED6ECD76918AED2DA57ADA620E37463643C5D2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP192K1Holder_t7EED6ECD76918AED2DA57ADA620E37463643C5D2_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1HOLDER_T7EED6ECD76918AED2DA57ADA620E37463643C5D2_H
#ifndef SECP192R1HOLDER_T023900B55B98B3A52D044E8ED0D5F541261B7946_H
#define SECP192R1HOLDER_T023900B55B98B3A52D044E8ED0D5F541261B7946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192R1Holder
struct  SecP192R1Holder_t023900B55B98B3A52D044E8ED0D5F541261B7946  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP192R1Holder_t023900B55B98B3A52D044E8ED0D5F541261B7946_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP192R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP192R1Holder_t023900B55B98B3A52D044E8ED0D5F541261B7946_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1HOLDER_T023900B55B98B3A52D044E8ED0D5F541261B7946_H
#ifndef SECP224K1HOLDER_T779CC302BA4620CCF594566FD5FCDA7040EA932D_H
#define SECP224K1HOLDER_T779CC302BA4620CCF594566FD5FCDA7040EA932D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224K1Holder
struct  SecP224K1Holder_t779CC302BA4620CCF594566FD5FCDA7040EA932D  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP224K1Holder_t779CC302BA4620CCF594566FD5FCDA7040EA932D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP224K1Holder_t779CC302BA4620CCF594566FD5FCDA7040EA932D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1HOLDER_T779CC302BA4620CCF594566FD5FCDA7040EA932D_H
#ifndef SECP224R1HOLDER_TF9C1664C23C67A37D5E2D40FF3836D073605BAE9_H
#define SECP224R1HOLDER_TF9C1664C23C67A37D5E2D40FF3836D073605BAE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224R1Holder
struct  SecP224R1Holder_tF9C1664C23C67A37D5E2D40FF3836D073605BAE9  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP224R1Holder_tF9C1664C23C67A37D5E2D40FF3836D073605BAE9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP224R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP224R1Holder_tF9C1664C23C67A37D5E2D40FF3836D073605BAE9_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1HOLDER_TF9C1664C23C67A37D5E2D40FF3836D073605BAE9_H
#ifndef SECP256K1HOLDER_T2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_H
#define SECP256K1HOLDER_T2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256K1Holder
struct  SecP256K1Holder_t2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP256K1Holder_t2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP256K1Holder_t2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256K1HOLDER_T2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_H
#ifndef SECP256R1HOLDER_T5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_H
#define SECP256R1HOLDER_T5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256R1Holder
struct  SecP256R1Holder_t5858477D99B7C4C5FE9B15DE8A2F151573C2D59C  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP256R1Holder_t5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP256R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP256R1Holder_t5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1HOLDER_T5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_H
#ifndef SECP384R1HOLDER_T7865CB5164942FDF6429A216D2D27B5F59D07AEB_H
#define SECP384R1HOLDER_T7865CB5164942FDF6429A216D2D27B5F59D07AEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP384R1Holder
struct  SecP384R1Holder_t7865CB5164942FDF6429A216D2D27B5F59D07AEB  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP384R1Holder_t7865CB5164942FDF6429A216D2D27B5F59D07AEB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP384R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP384R1Holder_t7865CB5164942FDF6429A216D2D27B5F59D07AEB_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1HOLDER_T7865CB5164942FDF6429A216D2D27B5F59D07AEB_H
#ifndef SECP521R1HOLDER_TFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_H
#define SECP521R1HOLDER_TFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP521R1Holder
struct  SecP521R1Holder_tFF2EA9196AE6B78E390444D14DA6B9AD5718A18A  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecP521R1Holder_tFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecP521R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecP521R1Holder_tFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1HOLDER_TFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_H
#ifndef SECT113R1HOLDER_TFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_H
#define SECT113R1HOLDER_TFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R1Holder
struct  SecT113R1Holder_tFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT113R1Holder_tFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT113R1Holder_tFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1HOLDER_TFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_H
#ifndef SECT113R2HOLDER_TFA266A166BE342812B2A42BC811B75BB94B5BEA4_H
#define SECT113R2HOLDER_TFA266A166BE342812B2A42BC811B75BB94B5BEA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R2Holder
struct  SecT113R2Holder_tFA266A166BE342812B2A42BC811B75BB94B5BEA4  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT113R2Holder_tFA266A166BE342812B2A42BC811B75BB94B5BEA4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT113R2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT113R2Holder_tFA266A166BE342812B2A42BC811B75BB94B5BEA4_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2HOLDER_TFA266A166BE342812B2A42BC811B75BB94B5BEA4_H
#ifndef SECT131R1HOLDER_T85D86EF87759A6381BB30D4F662ED351F9EF4F13_H
#define SECT131R1HOLDER_T85D86EF87759A6381BB30D4F662ED351F9EF4F13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R1Holder
struct  SecT131R1Holder_t85D86EF87759A6381BB30D4F662ED351F9EF4F13  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT131R1Holder_t85D86EF87759A6381BB30D4F662ED351F9EF4F13_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT131R1Holder_t85D86EF87759A6381BB30D4F662ED351F9EF4F13_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1HOLDER_T85D86EF87759A6381BB30D4F662ED351F9EF4F13_H
#ifndef SECT131R2HOLDER_TA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_H
#define SECT131R2HOLDER_TA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R2Holder
struct  SecT131R2Holder_tA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT131R2Holder_tA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT131R2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT131R2Holder_tA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2HOLDER_TA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_H
#ifndef SECT163K1HOLDER_TF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_H
#define SECT163K1HOLDER_TF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163K1Holder
struct  SecT163K1Holder_tF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT163K1Holder_tF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT163K1Holder_tF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1HOLDER_TF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_H
#ifndef SECT163R1HOLDER_TE5CAEBBBB937C97F3B09640329FDFEA53A23027B_H
#define SECT163R1HOLDER_TE5CAEBBBB937C97F3B09640329FDFEA53A23027B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R1Holder
struct  SecT163R1Holder_tE5CAEBBBB937C97F3B09640329FDFEA53A23027B  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT163R1Holder_tE5CAEBBBB937C97F3B09640329FDFEA53A23027B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT163R1Holder_tE5CAEBBBB937C97F3B09640329FDFEA53A23027B_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1HOLDER_TE5CAEBBBB937C97F3B09640329FDFEA53A23027B_H
#ifndef SECT163R2HOLDER_T950CB4672EE123F3B5B245CF3CAF61857C4C0076_H
#define SECT163R2HOLDER_T950CB4672EE123F3B5B245CF3CAF61857C4C0076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R2Holder
struct  SecT163R2Holder_t950CB4672EE123F3B5B245CF3CAF61857C4C0076  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT163R2Holder_t950CB4672EE123F3B5B245CF3CAF61857C4C0076_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT163R2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT163R2Holder_t950CB4672EE123F3B5B245CF3CAF61857C4C0076_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2HOLDER_T950CB4672EE123F3B5B245CF3CAF61857C4C0076_H
#ifndef SECT193R1HOLDER_T55D039F8C4A76689A9EDD4B2A3DC5E5301923112_H
#define SECT193R1HOLDER_T55D039F8C4A76689A9EDD4B2A3DC5E5301923112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R1Holder
struct  SecT193R1Holder_t55D039F8C4A76689A9EDD4B2A3DC5E5301923112  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT193R1Holder_t55D039F8C4A76689A9EDD4B2A3DC5E5301923112_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT193R1Holder_t55D039F8C4A76689A9EDD4B2A3DC5E5301923112_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1HOLDER_T55D039F8C4A76689A9EDD4B2A3DC5E5301923112_H
#ifndef SECT193R2HOLDER_T4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_H
#define SECT193R2HOLDER_T4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R2Holder
struct  SecT193R2Holder_t4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT193R2Holder_t4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT193R2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT193R2Holder_t4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2HOLDER_T4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_H
#ifndef SECT233K1HOLDER_T31937EA90A908952E95300BCF66E9304B62587B0_H
#define SECT233K1HOLDER_T31937EA90A908952E95300BCF66E9304B62587B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233K1Holder
struct  SecT233K1Holder_t31937EA90A908952E95300BCF66E9304B62587B0  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT233K1Holder_t31937EA90A908952E95300BCF66E9304B62587B0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT233K1Holder_t31937EA90A908952E95300BCF66E9304B62587B0_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1HOLDER_T31937EA90A908952E95300BCF66E9304B62587B0_H
#ifndef SECT233R1HOLDER_TAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_H
#define SECT233R1HOLDER_TAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233R1Holder
struct  SecT233R1Holder_tAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT233R1Holder_tAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT233R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT233R1Holder_tAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1HOLDER_TAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_H
#ifndef SECT239K1HOLDER_TAC620F403C96F183E3B3E4E4F55E29379729548A_H
#define SECT239K1HOLDER_TAC620F403C96F183E3B3E4E4F55E29379729548A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT239K1Holder
struct  SecT239K1Holder_tAC620F403C96F183E3B3E4E4F55E29379729548A  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT239K1Holder_tAC620F403C96F183E3B3E4E4F55E29379729548A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT239K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT239K1Holder_tAC620F403C96F183E3B3E4E4F55E29379729548A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1HOLDER_TAC620F403C96F183E3B3E4E4F55E29379729548A_H
#ifndef SECT283K1HOLDER_TD3B89D1F3BC241BC006C1681276417324F969D9E_H
#define SECT283K1HOLDER_TD3B89D1F3BC241BC006C1681276417324F969D9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283K1Holder
struct  SecT283K1Holder_tD3B89D1F3BC241BC006C1681276417324F969D9E  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT283K1Holder_tD3B89D1F3BC241BC006C1681276417324F969D9E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT283K1Holder_tD3B89D1F3BC241BC006C1681276417324F969D9E_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1HOLDER_TD3B89D1F3BC241BC006C1681276417324F969D9E_H
#ifndef SECT283R1HOLDER_T97556E29260135ED6718CB14927BC696030E3E1B_H
#define SECT283R1HOLDER_T97556E29260135ED6718CB14927BC696030E3E1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283R1Holder
struct  SecT283R1Holder_t97556E29260135ED6718CB14927BC696030E3E1B  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT283R1Holder_t97556E29260135ED6718CB14927BC696030E3E1B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT283R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT283R1Holder_t97556E29260135ED6718CB14927BC696030E3E1B_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1HOLDER_T97556E29260135ED6718CB14927BC696030E3E1B_H
#ifndef SECT409K1HOLDER_TD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_H
#define SECT409K1HOLDER_TD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409K1Holder
struct  SecT409K1Holder_tD44530F7E8F5E7EF0D447F4F774CCD51AC064C45  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT409K1Holder_tD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT409K1Holder_tD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1HOLDER_TD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_H
#ifndef SECT409R1HOLDER_T39FE3675D6225AB3E56C996FB91A7360F3889F9D_H
#define SECT409R1HOLDER_T39FE3675D6225AB3E56C996FB91A7360F3889F9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409R1Holder
struct  SecT409R1Holder_t39FE3675D6225AB3E56C996FB91A7360F3889F9D  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT409R1Holder_t39FE3675D6225AB3E56C996FB91A7360F3889F9D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT409R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT409R1Holder_t39FE3675D6225AB3E56C996FB91A7360F3889F9D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1HOLDER_T39FE3675D6225AB3E56C996FB91A7360F3889F9D_H
#ifndef SECT571K1HOLDER_T40F26417E156CCAD4352713B84FCDFA6622E4B02_H
#define SECT571K1HOLDER_T40F26417E156CCAD4352713B84FCDFA6622E4B02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571K1Holder
struct  SecT571K1Holder_t40F26417E156CCAD4352713B84FCDFA6622E4B02  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT571K1Holder_t40F26417E156CCAD4352713B84FCDFA6622E4B02_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571K1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT571K1Holder_t40F26417E156CCAD4352713B84FCDFA6622E4B02_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1HOLDER_T40F26417E156CCAD4352713B84FCDFA6622E4B02_H
#ifndef SECT571R1HOLDER_T07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_H
#define SECT571R1HOLDER_T07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571R1Holder
struct  SecT571R1Holder_t07F0C8E79699A1E1275ECE11C85EC41FA1F78C61  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SecT571R1Holder_t07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.EC.CustomNamedCurves_SecT571R1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SecT571R1Holder_t07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1HOLDER_T07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_H
#ifndef SEEDWRAPENGINE_TFAB755E6892018F1239A0F114B54651DAEC18214_H
#define SEEDWRAPENGINE_TFAB755E6892018F1239A0F114B54651DAEC18214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SeedWrapEngine
struct  SeedWrapEngine_tFAB755E6892018F1239A0F114B54651DAEC18214  : public Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEDWRAPENGINE_TFAB755E6892018F1239A0F114B54651DAEC18214_H
#ifndef SERPENTENGINE_T93CEAAA8FF79BC82445556D0622B792B2F7B0AB8_H
#define SERPENTENGINE_T93CEAAA8FF79BC82445556D0622B792B2F7B0AB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.SerpentEngine
struct  SerpentEngine_t93CEAAA8FF79BC82445556D0622B792B2F7B0AB8  : public SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERPENTENGINE_T93CEAAA8FF79BC82445556D0622B792B2F7B0AB8_H
#ifndef THREEFISH1024CIPHER_TD0F2BCDB231026D86142B0D5172F7BB8112E1B23_H
#define THREEFISH1024CIPHER_TD0F2BCDB231026D86142B0D5172F7BB8112E1B23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_Threefish1024Cipher
struct  Threefish1024Cipher_tD0F2BCDB231026D86142B0D5172F7BB8112E1B23  : public ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREEFISH1024CIPHER_TD0F2BCDB231026D86142B0D5172F7BB8112E1B23_H
#ifndef THREEFISH256CIPHER_TBDAFCC7AE5D015BAA2B6A026CC87AEEF850FE046_H
#define THREEFISH256CIPHER_TBDAFCC7AE5D015BAA2B6A026CC87AEEF850FE046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_Threefish256Cipher
struct  Threefish256Cipher_tBDAFCC7AE5D015BAA2B6A026CC87AEEF850FE046  : public ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREEFISH256CIPHER_TBDAFCC7AE5D015BAA2B6A026CC87AEEF850FE046_H
#ifndef THREEFISH512CIPHER_TBA924FDE3C9A5A958F666F8244EBA80F24AD6FA9_H
#define THREEFISH512CIPHER_TBA924FDE3C9A5A958F666F8244EBA80F24AD6FA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.ThreefishEngine_Threefish512Cipher
struct  Threefish512Cipher_tBA924FDE3C9A5A958F666F8244EBA80F24AD6FA9  : public ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREEFISH512CIPHER_TBA924FDE3C9A5A958F666F8244EBA80F24AD6FA9_H
#ifndef TNEPRESENGINE_T27896A53BFA857980032319916D6095FBCA40E8B_H
#define TNEPRESENGINE_T27896A53BFA857980032319916D6095FBCA40E8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.TnepresEngine
struct  TnepresEngine_t27896A53BFA857980032319916D6095FBCA40E8B  : public SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TNEPRESENGINE_T27896A53BFA857980032319916D6095FBCA40E8B_H
#ifndef VMPCKSA3ENGINE_TF189A44CC35B63FE81F44B0E91A5D4869052A011_H
#define VMPCKSA3ENGINE_TF189A44CC35B63FE81F44B0E91A5D4869052A011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.VmpcKsa3Engine
struct  VmpcKsa3Engine_tF189A44CC35B63FE81F44B0E91A5D4869052A011  : public VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VMPCKSA3ENGINE_TF189A44CC35B63FE81F44B0E91A5D4869052A011_H
#ifndef XSALSA20ENGINE_T742342714C445DD7E62961D24FE0E47F7E893646_H
#define XSALSA20ENGINE_T742342714C445DD7E62961D24FE0E47F7E893646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Engines.XSalsa20Engine
struct  XSalsa20Engine_t742342714C445DD7E62961D24FE0E47F7E893646  : public Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSALSA20ENGINE_T742342714C445DD7E62961D24FE0E47F7E893646_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4900 = { sizeof (Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457), -1, sizeof(Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4900[20] = 
{
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_internalState_0(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_workingKey_1(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_roundKeys_2(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_wordsInBlock_3(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_wordsInKey_4(),
	0,
	0,
	0,
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_roundsAmount_8(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457::get_offset_of_forEncryption_9(),
	0,
	0,
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_S0_12(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_S1_13(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_S2_14(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_S3_15(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_T0_16(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_T1_17(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_T2_18(),
	Dstu7624Engine_t28C42C0A459A0C7B02A244520FC9BA75F6911457_StaticFields::get_offset_of_T3_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4901 = { sizeof (Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4901[4] = 
{
	Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F::get_offset_of_param_0(),
	Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F::get_offset_of_engine_1(),
	Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F::get_offset_of_forWrapping_2(),
	Dstu7624WrapEngine_t529254156192E3594BF41EEBFF26C303C04DE29F::get_offset_of_blockSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4902 = { sizeof (ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4902[4] = 
{
	ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18::get_offset_of_key_0(),
	ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18::get_offset_of_random_1(),
	ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18::get_offset_of_forEncryption_2(),
	ElGamalEngine_t1DBC884B8BA0FFA151A9BAB4576FC8C15E902C18::get_offset_of_bitSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4903 = { sizeof (Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F), -1, sizeof(Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4903[13] = 
{
	0,
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F::get_offset_of_workingKey_1(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F::get_offset_of_forEncryption_2(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F::get_offset_of_S_3(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_Sbox_Default_4(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_ESbox_Test_5(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_ESbox_A_6(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_ESbox_B_7(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_ESbox_C_8(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_ESbox_D_9(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_DSbox_Test_10(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_DSbox_A_11(),
	Gost28147Engine_tBF602624EC9E1D448A33862F6E9469BF58A88C0F_StaticFields::get_offset_of_sBoxes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4904 = { sizeof (HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4904[8] = 
{
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_p_0(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_q_1(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_cnt_2(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_key_3(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_iv_4(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_initialised_5(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_buf_6(),
	HC128Engine_t844B45D142AF1591D3F15034CAE7EC2F65A8C906::get_offset_of_idx_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4905 = { sizeof (HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4905[8] = 
{
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_p_0(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_q_1(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_cnt_2(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_key_3(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_iv_4(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_initialised_5(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_buf_6(),
	HC256Engine_tCBEFB05E801128FDFBACF4BCCDE5DB19A0D02944::get_offset_of_idx_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4906 = { sizeof (IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F), -1, sizeof(IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4906[4] = 
{
	0,
	IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F::get_offset_of_workingKey_1(),
	IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_StaticFields::get_offset_of_MASK_2(),
	IdeaEngine_t0C11E41D5F92B1342B74B6010ABFA6287CD1CB5F_StaticFields::get_offset_of_BASE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4907 = { sizeof (IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4907[9] = 
{
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_agree_0(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_kdf_1(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_mac_2(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_cipher_3(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_macBuf_4(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_forEncryption_5(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_privParam_6(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_pubParam_7(),
	IesEngine_t7F57FEE741231F14B6CF18E5CE1F648B47C27BBA::get_offset_of_param_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4908 = { sizeof (IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD), -1, sizeof(IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4908[11] = 
{
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_StaticFields::get_offset_of_sizeL_0(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD_StaticFields::get_offset_of_stateArraySize_1(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_engineState_2(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_results_3(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_a_4(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_b_5(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_c_6(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_index_7(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_keyStream_8(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_workingKey_9(),
	IsaacEngine_tAB909B4A57A7E5A8F93E222D5F5F7136FAA17BAD::get_offset_of_initialised_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4909 = { sizeof (NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4909[3] = 
{
	NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321::get_offset_of_forEncryption_0(),
	NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321::get_offset_of_key_1(),
	NaccacheSternEngine_t6664D855A35AB2432647A7982A9B7A01FCA4B321::get_offset_of_lookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4910 = { sizeof (NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80), -1, sizeof(NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4910[8] = 
{
	0,
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80_StaticFields::get_offset_of_nullVector_1(),
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80_StaticFields::get_offset_of_roundConstants_2(),
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80::get_offset_of_state_3(),
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80::get_offset_of_subKeys_4(),
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80::get_offset_of_decryptKeys_5(),
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80::get_offset_of__initialised_6(),
	NoekeonEngine_tC3D1BF60777146C2468CE66A3995A8B5FEE24D80::get_offset_of__forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4911 = { sizeof (NullEngine_tB7750E6A21F2EA4946195177A9B5A88F680B3E29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4911[2] = 
{
	NullEngine_tB7750E6A21F2EA4946195177A9B5A88F680B3E29::get_offset_of_initialised_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4912 = { sizeof (RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB), -1, sizeof(RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4912[4] = 
{
	RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB_StaticFields::get_offset_of_piTable_0(),
	0,
	RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB::get_offset_of_workingKey_2(),
	RC2Engine_t2FB8DF958DDE0FDEFA79A4A11C2F09E4E44BD2FB::get_offset_of_encrypting_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4913 = { sizeof (RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B), -1, sizeof(RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4913[9] = 
{
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_engine_0(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_parameters_1(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_paramPlusIV_2(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_iv_3(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_forWrapping_4(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_sr_5(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B_StaticFields::get_offset_of_IV2_6(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_sha1_7(),
	RC2WrapEngine_tE33282629BF68C10E8E16AE1623F52FD7CB3348B::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4914 = { sizeof (RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F), -1, sizeof(RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4914[5] = 
{
	RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F_StaticFields::get_offset_of_STATE_LENGTH_0(),
	RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F::get_offset_of_engineState_1(),
	RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F::get_offset_of_x_2(),
	RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F::get_offset_of_y_3(),
	RC4Engine_tB783EEEBB626358CB6CCF4A6B3B3D1474FD6AC5F::get_offset_of_workingKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4915 = { sizeof (RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956), -1, sizeof(RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4915[5] = 
{
	RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956::get_offset_of__noRounds_0(),
	RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956::get_offset_of__S_1(),
	RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956_StaticFields::get_offset_of_P32_2(),
	RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956_StaticFields::get_offset_of_Q32_3(),
	RC532Engine_tC458BF83C040E6C2E5600484E362C4A3972B1956::get_offset_of_forEncryption_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4916 = { sizeof (RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD), -1, sizeof(RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4916[7] = 
{
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields::get_offset_of_wordSize_0(),
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields::get_offset_of_bytesPerWord_1(),
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD::get_offset_of__noRounds_2(),
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD::get_offset_of__S_3(),
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields::get_offset_of_P64_4(),
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD_StaticFields::get_offset_of_Q64_5(),
	RC564Engine_t10F46FA30B1388A5078B5FB0DDFC8D1CBAA42DBD::get_offset_of_forEncryption_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4917 = { sizeof (RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A), -1, sizeof(RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4917[8] = 
{
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields::get_offset_of_wordSize_0(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields::get_offset_of_bytesPerWord_1(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields::get_offset_of__noRounds_2(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A::get_offset_of__S_3(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields::get_offset_of_P32_4(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields::get_offset_of_Q32_5(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A_StaticFields::get_offset_of_LGW_6(),
	RC6Engine_tA0B70F0830A22F811ED32CF37DCE858D0B8EFA9A::get_offset_of_forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4918 = { sizeof (Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4918[4] = 
{
	Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35::get_offset_of_engine_0(),
	Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35::get_offset_of_param_1(),
	Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35::get_offset_of_forWrapping_2(),
	Rfc3211WrapEngine_t42D4C5775B401F61990FA67D5C4E70C03DE9AC35::get_offset_of_rand_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4919 = { sizeof (Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4919[4] = 
{
	Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B::get_offset_of_engine_0(),
	Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B::get_offset_of_param_1(),
	Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B::get_offset_of_forWrapping_2(),
	Rfc3394WrapEngine_t8255799FEF932C0736E276CD77596F43027A454B::get_offset_of_iv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4920 = { sizeof (RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69), -1, sizeof(RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4920[21] = 
{
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_MAXROUNDS_0(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_MAXKC_1(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_Logtable_2(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_Alogtable_3(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_S_4(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_Si_5(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_rcon_6(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_shifts0_7(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69_StaticFields::get_offset_of_shifts1_8(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_BC_9(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_BC_MASK_10(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_ROUNDS_11(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_blockBits_12(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_workingKey_13(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_A0_14(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_A1_15(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_A2_16(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_A3_17(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_forEncryption_18(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_shifts0SC_19(),
	RijndaelEngine_tDE3DF82CFDF5BCFAEF6A6DE6920D6253D6F0DE69::get_offset_of_shifts1SC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4921 = { sizeof (RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4921[3] = 
{
	RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275::get_offset_of_core_0(),
	RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275::get_offset_of_key_1(),
	RsaBlindedEngine_t19C28C25C083528DA6463A951357512B4D140275::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4922 = { sizeof (RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4922[4] = 
{
	RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751::get_offset_of_core_0(),
	RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751::get_offset_of_key_1(),
	RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751::get_offset_of_blindingFactor_2(),
	RsaBlindingEngine_t755813533965F407DC564072721F62A04C61E751::get_offset_of_forEncryption_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4923 = { sizeof (RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4923[3] = 
{
	RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6::get_offset_of_key_0(),
	RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6::get_offset_of_forEncryption_1(),
	RsaCoreEngine_t673217EF908CCCE69C24D0D7D9CC35F6C6B839C6::get_offset_of_bitSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4924 = { sizeof (RsaEngine_t84355B05A10A2D620A309F7E436B5A901AF720F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4924[1] = 
{
	RsaEngine_t84355B05A10A2D620A309F7E436B5A901AF720F0::get_offset_of_core_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4925 = { sizeof (Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69), -1, sizeof(Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4925[14] = 
{
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields::get_offset_of_DEFAULT_ROUNDS_0(),
	0,
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields::get_offset_of_TAU_SIGMA_2(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields::get_offset_of_sigma_3(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69_StaticFields::get_offset_of_tau_4(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_rounds_5(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_index_6(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_engineState_7(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_x_8(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_keyStream_9(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_initialised_10(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_cW0_11(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_cW1_12(),
	Salsa20Engine_t521B446CFC278026CC81B6017B3797C847F42C69::get_offset_of_cW2_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4926 = { sizeof (SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A), -1, sizeof(SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4926[8] = 
{
	0,
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields::get_offset_of_SS0_1(),
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields::get_offset_of_SS1_2(),
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields::get_offset_of_SS2_3(),
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields::get_offset_of_SS3_4(),
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A_StaticFields::get_offset_of_KC_5(),
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A::get_offset_of_wKey_6(),
	SeedEngine_tD49DE21249C0717632E3A37CBCCCE62F44DBA18A::get_offset_of_forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4927 = { sizeof (SeedWrapEngine_tFAB755E6892018F1239A0F114B54651DAEC18214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4928 = { sizeof (SerpentEngine_t93CEAAA8FF79BC82445556D0622B792B2F7B0AB8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4929 = { sizeof (SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A), -1, sizeof(SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4929[9] = 
{
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A_StaticFields::get_offset_of_BlockSize_0(),
	0,
	0,
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A::get_offset_of_encrypting_3(),
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A::get_offset_of_wKey_4(),
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A::get_offset_of_X0_5(),
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A::get_offset_of_X1_6(),
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A::get_offset_of_X2_7(),
	SerpentEngineBase_tD001D8D52816040D5378C0AF71E577D684BEE71A::get_offset_of_X3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4930 = { sizeof (SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38), -1, sizeof(SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4930[7] = 
{
	0,
	SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38_StaticFields::get_offset_of_ftable_1(),
	SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38::get_offset_of_key0_2(),
	SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38::get_offset_of_key1_3(),
	SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38::get_offset_of_key2_4(),
	SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38::get_offset_of_key3_5(),
	SkipjackEngine_tE953AF794BEDF6C9BBB30C738717A53BADCCDB38::get_offset_of_encrypting_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4931 = { sizeof (SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4931[6] = 
{
	SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D::get_offset_of_mDigest_0(),
	SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D::get_offset_of_mForEncryption_1(),
	SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D::get_offset_of_mECKey_2(),
	SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D::get_offset_of_mECParams_3(),
	SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D::get_offset_of_mCurveLength_4(),
	SM2Engine_tB2A8167B7E2AF4977ADD84BEAB8E78E86CB02A1D::get_offset_of_mRandom_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4932 = { sizeof (SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559), -1, sizeof(SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4932[5] = 
{
	0,
	SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields::get_offset_of_Sbox_1(),
	SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields::get_offset_of_CK_2(),
	SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559_StaticFields::get_offset_of_FK_3(),
	SM4Engine_t05F4D2F0D9D28729873BACADBD6CDFB527225559::get_offset_of_rk_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4933 = { sizeof (TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4933[10] = 
{
	0,
	0,
	0,
	0,
	TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393::get_offset_of__a_4(),
	TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393::get_offset_of__b_5(),
	TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393::get_offset_of__c_6(),
	TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393::get_offset_of__d_7(),
	TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393::get_offset_of__initialised_8(),
	TeaEngine_tC0BAC0C74CB674BDACEB0A72CBFFA72CE67F2393::get_offset_of__forEncryption_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4934 = { sizeof (ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE), -1, sizeof(ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4934[21] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields::get_offset_of_MOD9_10(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields::get_offset_of_MOD17_11(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields::get_offset_of_MOD5_12(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE_StaticFields::get_offset_of_MOD3_13(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_blocksizeBytes_14(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_blocksizeWords_15(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_currentBlock_16(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_t_17(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_kw_18(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_cipher_19(),
	ThreefishEngine_t87218E8DD191868F6F79AC24C90C5772990699DE::get_offset_of_forEncryption_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4935 = { sizeof (ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4935[2] = 
{
	ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3::get_offset_of_t_0(),
	ThreefishCipher_t721B1B330C79C107D4822CA3444094FA9811F5A3::get_offset_of_kw_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4936 = { sizeof (Threefish256Cipher_tBDAFCC7AE5D015BAA2B6A026CC87AEEF850FE046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4936[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4937 = { sizeof (Threefish512Cipher_tBA924FDE3C9A5A958F666F8244EBA80F24AD6FA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4937[32] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4938 = { sizeof (Threefish1024Cipher_tD0F2BCDB231026D86142B0D5172F7BB8112E1B23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4938[64] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4939 = { sizeof (TnepresEngine_t27896A53BFA857980032319916D6095FBCA40E8B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4940 = { sizeof (TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094), -1, sizeof(TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4940[45] = 
{
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094_StaticFields::get_offset_of_P_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_encrypting_36(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_gMDS0_37(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_gMDS1_38(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_gMDS2_39(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_gMDS3_40(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_gSubKeys_41(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_gSBox_42(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_k64Cnt_43(),
	TwofishEngine_t06508E128BE900AC8493C3404508CB018BBA9094::get_offset_of_workingKey_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4941 = { sizeof (VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4941[5] = 
{
	VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60::get_offset_of_n_0(),
	VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60::get_offset_of_P_1(),
	VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60::get_offset_of_s_2(),
	VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60::get_offset_of_workingIV_3(),
	VmpcEngine_t6FF2EA4CD3E79BE8BEF94B655B03283D9BE5CE60::get_offset_of_workingKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4942 = { sizeof (VmpcKsa3Engine_tF189A44CC35B63FE81F44B0E91A5D4869052A011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4943 = { sizeof (XSalsa20Engine_t742342714C445DD7E62961D24FE0E47F7E893646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4944 = { sizeof (XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4944[8] = 
{
	0,
	0,
	0,
	XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E::get_offset_of__S_3(),
	XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E::get_offset_of__sum0_4(),
	XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E::get_offset_of__sum1_5(),
	XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E::get_offset_of__initialised_6(),
	XteaEngine_t8BEE866A19A0A760B7585D9725AB12791F42DE1E::get_offset_of__forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4945 = { sizeof (ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957), -1, sizeof(ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4945[9] = 
{
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields::get_offset_of_Sixteen_0(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields::get_offset_of_Six_1(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields::get_offset_of_shadows_2(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957_StaticFields::get_offset_of_inverse_3(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957::get_offset_of_engine_4(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957::get_offset_of_forEncryption_5(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957::get_offset_of_bitSize_6(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957::get_offset_of_padBits_7(),
	ISO9796d1Encoding_t0E174BA61E5F29155C8CAE521A1AAB07E35B1957::get_offset_of_modulus_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4946 = { sizeof (OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4946[5] = 
{
	OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF::get_offset_of_defHash_0(),
	OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF::get_offset_of_mgf1Hash_1(),
	OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF::get_offset_of_engine_2(),
	OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF::get_offset_of_random_3(),
	OaepEncoding_t7D9038FB221221A2AB3852750F220922D510A7DF::get_offset_of_forEncryption_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4947 = { sizeof (Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080), -1, sizeof(Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4947[11] = 
{
	0,
	0,
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080_StaticFields::get_offset_of_strictLengthEnabled_2(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_random_3(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_engine_4(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_forEncryption_5(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_forPrivateKey_6(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_useStrictLength_7(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_pLen_8(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_fallback_9(),
	Pkcs1Encoding_tBA70E2A21F4F37A5819E46E1625F014D83F66080::get_offset_of_blockBuffer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4948 = { sizeof (CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9), -1, sizeof(CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4948[5] = 
{
	CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields::get_offset_of_nameToCurve_0(),
	CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields::get_offset_of_nameToOid_1(),
	CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields::get_offset_of_oidToCurve_2(),
	CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields::get_offset_of_oidToName_3(),
	CustomNamedCurves_t715866532C927271C699D3A651AC8DEC2A9050F9_StaticFields::get_offset_of_names_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4949 = { sizeof (Curve25519Holder_tF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0), -1, sizeof(Curve25519Holder_tF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4949[1] = 
{
	Curve25519Holder_tF256A7F057B6ADADA5333E5E317CCBF5B3FAF3A0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4950 = { sizeof (SecP128R1Holder_t0BB3169BA0FF61C47E96A3B33CDA413E8081A44D), -1, sizeof(SecP128R1Holder_t0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4950[1] = 
{
	SecP128R1Holder_t0BB3169BA0FF61C47E96A3B33CDA413E8081A44D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4951 = { sizeof (SecP160K1Holder_t9458088EFDAB3C900EC1086D8AC4F2931195A8B6), -1, sizeof(SecP160K1Holder_t9458088EFDAB3C900EC1086D8AC4F2931195A8B6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4951[1] = 
{
	SecP160K1Holder_t9458088EFDAB3C900EC1086D8AC4F2931195A8B6_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4952 = { sizeof (SecP160R1Holder_tC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F), -1, sizeof(SecP160R1Holder_tC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4952[1] = 
{
	SecP160R1Holder_tC84D3C09BA779A5E2D6C1C3E4E6030A04B49CB5F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4953 = { sizeof (SecP160R2Holder_t463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F), -1, sizeof(SecP160R2Holder_t463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4953[1] = 
{
	SecP160R2Holder_t463102E7C4BC631B8CBF5A4C93E9D6FC2031AE2F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4954 = { sizeof (SecP192K1Holder_t7EED6ECD76918AED2DA57ADA620E37463643C5D2), -1, sizeof(SecP192K1Holder_t7EED6ECD76918AED2DA57ADA620E37463643C5D2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4954[1] = 
{
	SecP192K1Holder_t7EED6ECD76918AED2DA57ADA620E37463643C5D2_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4955 = { sizeof (SecP192R1Holder_t023900B55B98B3A52D044E8ED0D5F541261B7946), -1, sizeof(SecP192R1Holder_t023900B55B98B3A52D044E8ED0D5F541261B7946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4955[1] = 
{
	SecP192R1Holder_t023900B55B98B3A52D044E8ED0D5F541261B7946_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4956 = { sizeof (SecP224K1Holder_t779CC302BA4620CCF594566FD5FCDA7040EA932D), -1, sizeof(SecP224K1Holder_t779CC302BA4620CCF594566FD5FCDA7040EA932D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4956[1] = 
{
	SecP224K1Holder_t779CC302BA4620CCF594566FD5FCDA7040EA932D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4957 = { sizeof (SecP224R1Holder_tF9C1664C23C67A37D5E2D40FF3836D073605BAE9), -1, sizeof(SecP224R1Holder_tF9C1664C23C67A37D5E2D40FF3836D073605BAE9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4957[1] = 
{
	SecP224R1Holder_tF9C1664C23C67A37D5E2D40FF3836D073605BAE9_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4958 = { sizeof (SecP256K1Holder_t2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD), -1, sizeof(SecP256K1Holder_t2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4958[1] = 
{
	SecP256K1Holder_t2E24B5017DDCCFA02A8C8C06D1533D57C4D2EECD_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4959 = { sizeof (SecP256R1Holder_t5858477D99B7C4C5FE9B15DE8A2F151573C2D59C), -1, sizeof(SecP256R1Holder_t5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4959[1] = 
{
	SecP256R1Holder_t5858477D99B7C4C5FE9B15DE8A2F151573C2D59C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4960 = { sizeof (SecP384R1Holder_t7865CB5164942FDF6429A216D2D27B5F59D07AEB), -1, sizeof(SecP384R1Holder_t7865CB5164942FDF6429A216D2D27B5F59D07AEB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4960[1] = 
{
	SecP384R1Holder_t7865CB5164942FDF6429A216D2D27B5F59D07AEB_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4961 = { sizeof (SecP521R1Holder_tFF2EA9196AE6B78E390444D14DA6B9AD5718A18A), -1, sizeof(SecP521R1Holder_tFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4961[1] = 
{
	SecP521R1Holder_tFF2EA9196AE6B78E390444D14DA6B9AD5718A18A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4962 = { sizeof (SecT113R1Holder_tFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F), -1, sizeof(SecT113R1Holder_tFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4962[1] = 
{
	SecT113R1Holder_tFF9A1B3B0F9D63034DC9AB2E2E6F60E58B6AFC1F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4963 = { sizeof (SecT113R2Holder_tFA266A166BE342812B2A42BC811B75BB94B5BEA4), -1, sizeof(SecT113R2Holder_tFA266A166BE342812B2A42BC811B75BB94B5BEA4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4963[1] = 
{
	SecT113R2Holder_tFA266A166BE342812B2A42BC811B75BB94B5BEA4_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4964 = { sizeof (SecT131R1Holder_t85D86EF87759A6381BB30D4F662ED351F9EF4F13), -1, sizeof(SecT131R1Holder_t85D86EF87759A6381BB30D4F662ED351F9EF4F13_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4964[1] = 
{
	SecT131R1Holder_t85D86EF87759A6381BB30D4F662ED351F9EF4F13_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4965 = { sizeof (SecT131R2Holder_tA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48), -1, sizeof(SecT131R2Holder_tA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4965[1] = 
{
	SecT131R2Holder_tA7F25BBFAB7DB510EBA9D76464E47D8CF987EB48_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4966 = { sizeof (SecT163K1Holder_tF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F), -1, sizeof(SecT163K1Holder_tF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4966[1] = 
{
	SecT163K1Holder_tF48E7C9D0FA1F444CB6E28C3CDEA2807CC601C8F_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4967 = { sizeof (SecT163R1Holder_tE5CAEBBBB937C97F3B09640329FDFEA53A23027B), -1, sizeof(SecT163R1Holder_tE5CAEBBBB937C97F3B09640329FDFEA53A23027B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4967[1] = 
{
	SecT163R1Holder_tE5CAEBBBB937C97F3B09640329FDFEA53A23027B_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4968 = { sizeof (SecT163R2Holder_t950CB4672EE123F3B5B245CF3CAF61857C4C0076), -1, sizeof(SecT163R2Holder_t950CB4672EE123F3B5B245CF3CAF61857C4C0076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4968[1] = 
{
	SecT163R2Holder_t950CB4672EE123F3B5B245CF3CAF61857C4C0076_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4969 = { sizeof (SecT193R1Holder_t55D039F8C4A76689A9EDD4B2A3DC5E5301923112), -1, sizeof(SecT193R1Holder_t55D039F8C4A76689A9EDD4B2A3DC5E5301923112_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4969[1] = 
{
	SecT193R1Holder_t55D039F8C4A76689A9EDD4B2A3DC5E5301923112_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4970 = { sizeof (SecT193R2Holder_t4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330), -1, sizeof(SecT193R2Holder_t4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4970[1] = 
{
	SecT193R2Holder_t4BF1CD9DC7C574B73C60F6AC91DF2332C36ED330_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4971 = { sizeof (SecT233K1Holder_t31937EA90A908952E95300BCF66E9304B62587B0), -1, sizeof(SecT233K1Holder_t31937EA90A908952E95300BCF66E9304B62587B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4971[1] = 
{
	SecT233K1Holder_t31937EA90A908952E95300BCF66E9304B62587B0_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4972 = { sizeof (SecT233R1Holder_tAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C), -1, sizeof(SecT233R1Holder_tAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4972[1] = 
{
	SecT233R1Holder_tAFC9078E1BC52B1E23AB6109A04BF69119EB6E0C_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4973 = { sizeof (SecT239K1Holder_tAC620F403C96F183E3B3E4E4F55E29379729548A), -1, sizeof(SecT239K1Holder_tAC620F403C96F183E3B3E4E4F55E29379729548A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4973[1] = 
{
	SecT239K1Holder_tAC620F403C96F183E3B3E4E4F55E29379729548A_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4974 = { sizeof (SecT283K1Holder_tD3B89D1F3BC241BC006C1681276417324F969D9E), -1, sizeof(SecT283K1Holder_tD3B89D1F3BC241BC006C1681276417324F969D9E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4974[1] = 
{
	SecT283K1Holder_tD3B89D1F3BC241BC006C1681276417324F969D9E_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4975 = { sizeof (SecT283R1Holder_t97556E29260135ED6718CB14927BC696030E3E1B), -1, sizeof(SecT283R1Holder_t97556E29260135ED6718CB14927BC696030E3E1B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4975[1] = 
{
	SecT283R1Holder_t97556E29260135ED6718CB14927BC696030E3E1B_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4976 = { sizeof (SecT409K1Holder_tD44530F7E8F5E7EF0D447F4F774CCD51AC064C45), -1, sizeof(SecT409K1Holder_tD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4976[1] = 
{
	SecT409K1Holder_tD44530F7E8F5E7EF0D447F4F774CCD51AC064C45_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4977 = { sizeof (SecT409R1Holder_t39FE3675D6225AB3E56C996FB91A7360F3889F9D), -1, sizeof(SecT409R1Holder_t39FE3675D6225AB3E56C996FB91A7360F3889F9D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4977[1] = 
{
	SecT409R1Holder_t39FE3675D6225AB3E56C996FB91A7360F3889F9D_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4978 = { sizeof (SecT571K1Holder_t40F26417E156CCAD4352713B84FCDFA6622E4B02), -1, sizeof(SecT571K1Holder_t40F26417E156CCAD4352713B84FCDFA6622E4B02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4978[1] = 
{
	SecT571K1Holder_t40F26417E156CCAD4352713B84FCDFA6622E4B02_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4979 = { sizeof (SecT571R1Holder_t07F0C8E79699A1E1275ECE11C85EC41FA1F78C61), -1, sizeof(SecT571R1Holder_t07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4979[1] = 
{
	SecT571R1Holder_t07F0C8E79699A1E1275ECE11C85EC41FA1F78C61_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4980 = { sizeof (SM2P256V1Holder_tCC06EE13F83A3A5147845DB07B203046BD09B584), -1, sizeof(SM2P256V1Holder_tCC06EE13F83A3A5147845DB07B203046BD09B584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4980[1] = 
{
	SM2P256V1Holder_tCC06EE13F83A3A5147845DB07B203046BD09B584_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4981 = { sizeof (Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A), -1, sizeof(Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4981[16] = 
{
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A_StaticFields::get_offset_of_blake2b_IV_0(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A_StaticFields::get_offset_of_blake2b_sigma_1(),
	0,
	0,
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_digestLength_4(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_keyLength_5(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_salt_6(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_personalization_7(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_key_8(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_buffer_9(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_bufferPos_10(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_internalState_11(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_chainValue_12(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_t0_13(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_t1_14(),
	Blake2bDigest_t6671A8E0151C6C2E9A278439C58497B09B0D7C9A::get_offset_of_f0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4982 = { sizeof (Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422), -1, sizeof(Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4982[16] = 
{
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422_StaticFields::get_offset_of_blake2s_IV_0(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422_StaticFields::get_offset_of_blake2s_sigma_1(),
	0,
	0,
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_digestLength_4(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_keyLength_5(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_salt_6(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_personalization_7(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_key_8(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_buffer_9(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_bufferPos_10(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_internalState_11(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_chainValue_12(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_t0_13(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_t1_14(),
	Blake2sDigest_t8ACCF2420E9801976C59143DEB8EE148FC7F7422::get_offset_of_f0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4983 = { sizeof (Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E), -1, sizeof(Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4983[18] = 
{
	0,
	0,
	0,
	0,
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_hashSize_4(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_blockSize_5(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_columns_6(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_rounds_7(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_state_8(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_tempState1_9(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_tempState2_10(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_inputBlocks_11(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_bufOff_12(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E::get_offset_of_buf_13(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields::get_offset_of_S0_14(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields::get_offset_of_S1_15(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields::get_offset_of_S2_16(),
	Dstu7564Digest_t18C139AC1198FBDD591BC54693DA7390B66E785E_StaticFields::get_offset_of_S3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4984 = { sizeof (GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4984[4] = 
{
	0,
	GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA::get_offset_of_xBuf_1(),
	GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA::get_offset_of_xBufOff_2(),
	GeneralDigest_t30737BEEA96249128E61B761F593A2C44E86F2EA::get_offset_of_byteCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4985 = { sizeof (Gost3411_2012_256Digest_t1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9), -1, sizeof(Gost3411_2012_256Digest_t1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4985[1] = 
{
	Gost3411_2012_256Digest_t1BB6088AFD2BDA80F0B1F5A9A38A3804BC7505E9_StaticFields::get_offset_of_IV_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4986 = { sizeof (Gost3411_2012_512Digest_tA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7), -1, sizeof(Gost3411_2012_512Digest_tA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4986[1] = 
{
	Gost3411_2012_512Digest_tA2E8170D11E85C68A65B7F7A4F26E1D8AFA437D7_StaticFields::get_offset_of_IV_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4987 = { sizeof (Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9), -1, sizeof(Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4987[12] = 
{
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_IV_0(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_N_1(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_Sigma_2(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_Ki_3(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_m_4(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_h_5(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_tmp_6(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_block_7(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9::get_offset_of_bOff_8(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields::get_offset_of_C_9(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields::get_offset_of_Zero_10(),
	Gost3411_2012Digest_tC22308BA8BA8FC1DFD368E2B71EF6D22FABE03F9_StaticFields::get_offset_of_T_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4988 = { sizeof (Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865), -1, sizeof(Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4988[20] = 
{
	0,
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_H_1(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_L_2(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_M_3(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_Sum_4(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_C_5(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_xBuf_6(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_xBufOff_7(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_byteCount_8(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_cipher_9(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_sBox_10(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_K_11(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_a_12(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_wS_13(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_w_S_14(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_S_15(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_U_16(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_V_17(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865::get_offset_of_W_18(),
	Gost3411Digest_tD1C8FBA9251F5B00F37CEF7F39C89527E00D3865_StaticFields::get_offset_of_C2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4989 = { sizeof (KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32), -1, sizeof(KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4989[7] = 
{
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32_StaticFields::get_offset_of_KeccakRoundConstants_0(),
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32::get_offset_of_state_1(),
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32::get_offset_of_dataQueue_2(),
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32::get_offset_of_rate_3(),
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32::get_offset_of_bitsInQueue_4(),
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32::get_offset_of_fixedOutputLength_5(),
	KeccakDigest_tC0937665833293C5C98902C767A0DD60E0277A32::get_offset_of_squeezing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4990 = { sizeof (LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203), -1, sizeof(LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4990[16] = 
{
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_MyByteLength_0(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_xBuf_1(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_xBufOff_2(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_byteCount1_3(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_byteCount2_4(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H1_5(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H2_6(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H3_7(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H4_8(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H5_9(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H6_10(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H7_11(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_H8_12(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_W_13(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203::get_offset_of_wOff_14(),
	LongDigest_t23D3C83A6B4E5D9B9B507888AD068F22886B5203_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4991 = { sizeof (MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F), -1, sizeof(MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4991[9] = 
{
	0,
	0,
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F::get_offset_of_X_2(),
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F::get_offset_of_xOff_3(),
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F::get_offset_of_M_4(),
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F::get_offset_of_mOff_5(),
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F::get_offset_of_C_6(),
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F::get_offset_of_COff_7(),
	MD2Digest_tB481F9F4007120CA649DCDEF9A3D7064F898208F_StaticFields::get_offset_of_S_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4992 = { sizeof (MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4992[19] = 
{
	0,
	MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1::get_offset_of_H1_5(),
	MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1::get_offset_of_H2_6(),
	MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1::get_offset_of_H3_7(),
	MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1::get_offset_of_H4_8(),
	MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1::get_offset_of_X_9(),
	MD4Digest_tFD8F38C0E884472CCAB18012B94F3C90CE3867A1::get_offset_of_xOff_10(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4993 = { sizeof (MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F), -1, sizeof(MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4993[23] = 
{
	0,
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F::get_offset_of_H1_5(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F::get_offset_of_H2_6(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F::get_offset_of_H3_7(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F::get_offset_of_H4_8(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F::get_offset_of_X_9(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F::get_offset_of_xOff_10(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S11_11(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S12_12(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S13_13(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S14_14(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S21_15(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S22_16(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S23_17(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S24_18(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S31_19(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S32_20(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S33_21(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S34_22(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S41_23(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S42_24(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S43_25(),
	MD5Digest_t0BA406F0C3C2A9A325E7885E06F41CD609469A5F_StaticFields::get_offset_of_S44_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4994 = { sizeof (NonMemoableDigest_t492160D0F28E00EC004BAA82F2F31A9C1F6B43E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4994[1] = 
{
	NonMemoableDigest_t492160D0F28E00EC004BAA82F2F31A9C1F6B43E6::get_offset_of_mBaseDigest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4995 = { sizeof (NullDigest_t15A8C389FB3A4A06093B8092220246098A384DA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4995[1] = 
{
	NullDigest_t15A8C389FB3A4A06093B8092220246098A384DA6::get_offset_of_bOut_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4996 = { sizeof (RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4996[7] = 
{
	0,
	RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9::get_offset_of_H0_5(),
	RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9::get_offset_of_H1_6(),
	RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9::get_offset_of_H2_7(),
	RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9::get_offset_of_H3_8(),
	RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9::get_offset_of_X_9(),
	RipeMD128Digest_t315ADF1D5103AED85039E0512C7E34EF9CA2C3D9::get_offset_of_xOff_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4997 = { sizeof (RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4997[8] = 
{
	0,
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_H0_5(),
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_H1_6(),
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_H2_7(),
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_H3_8(),
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_H4_9(),
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_X_10(),
	RipeMD160Digest_tB79BA430F1A53D5A342070008294BDE56AA350D4::get_offset_of_xOff_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4998 = { sizeof (RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4998[11] = 
{
	0,
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H0_5(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H1_6(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H2_7(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H3_8(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H4_9(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H5_10(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H6_11(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_H7_12(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_X_13(),
	RipeMD256Digest_t329D11C118AE7087208CA6A3706D68C602AFA670::get_offset_of_xOff_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4999 = { sizeof (RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4999[13] = 
{
	0,
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H0_5(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H1_6(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H2_7(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H3_8(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H4_9(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H5_10(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H6_11(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H7_12(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H8_13(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_H9_14(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_X_15(),
	RipeMD320Digest_t3288B3543CC8BA8FE1456C32A866DC0DC70A9B27::get_offset_of_xOff_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
