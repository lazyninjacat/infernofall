﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.EnvelopedDataHelper
struct EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[]
struct BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.ZTauElement[]
struct ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519
struct Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Point
struct Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve
struct SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Point
struct SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve
struct SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve
struct SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Point
struct SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve
struct SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Point
struct SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve
struct SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Point
struct SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve
struct SecT113R2Curve_t991A5352277966895F4693704773720780531C59;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Point
struct SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve
struct SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Point
struct SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve
struct SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Point
struct SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve
struct SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Point
struct SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve
struct SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Point
struct SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve
struct SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Point
struct SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve
struct SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Point
struct SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve
struct SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Point
struct SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve
struct SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Point
struct SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve
struct SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Point
struct SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve
struct SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Point
struct SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve
struct SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Point
struct SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve
struct SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Point
struct SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve
struct SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Point
struct SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve
struct SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Point
struct SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement
struct SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve
struct SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Point
struct SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve
struct SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Point
struct SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t6071986B64066FBF97315592BE971355FA584A39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[]
struct ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism
struct ECEndomorphism_t6EB799DC03F118DC4DC962E5E5B94F009F586948;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier
struct ECMultiplier_t3D3844C8A426F0454D623B3F3AA875FE6896AB11;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField
struct IFiniteField_tFCD218E0A913FF4E2BB6D10F42F69C3979B748D5;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.SByte[][]
struct SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt64[]
struct UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SIMPLEBIGDECIMAL_TC6B179F4E0B6415BF526C871B2A278842974D167_H
#define SIMPLEBIGDECIMAL_TC6B179F4E0B6415BF526C871B2A278842974D167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.SimpleBigDecimal
struct  SimpleBigDecimal_tC6B179F4E0B6415BF526C871B2A278842974D167  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.SimpleBigDecimal::bigInt
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___bigInt_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.SimpleBigDecimal::scale
	int32_t ___scale_1;

public:
	inline static int32_t get_offset_of_bigInt_0() { return static_cast<int32_t>(offsetof(SimpleBigDecimal_tC6B179F4E0B6415BF526C871B2A278842974D167, ___bigInt_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_bigInt_0() const { return ___bigInt_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_bigInt_0() { return &___bigInt_0; }
	inline void set_bigInt_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___bigInt_0 = value;
		Il2CppCodeGenWriteBarrier((&___bigInt_0), value);
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(SimpleBigDecimal_tC6B179F4E0B6415BF526C871B2A278842974D167, ___scale_1)); }
	inline int32_t get_scale_1() const { return ___scale_1; }
	inline int32_t* get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(int32_t value)
	{
		___scale_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEBIGDECIMAL_TC6B179F4E0B6415BF526C871B2A278842974D167_H
#ifndef TNAF_T205143603BEF6691CF6DCE645EF5CE4EE8110580_H
#define TNAF_T205143603BEF6691CF6DCE645EF5CE4EE8110580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf
struct  Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580  : public RuntimeObject
{
public:

public:
};

struct Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::MinusOne
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___MinusOne_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::MinusTwo
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___MinusTwo_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::MinusThree
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___MinusThree_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::Four
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Four_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.ZTauElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha0
	ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A* ___Alpha0_6;
	// System.SByte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha0Tnaf
	SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B* ___Alpha0Tnaf_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.ZTauElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha1
	ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A* ___Alpha1_8;
	// System.SByte[][] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha1Tnaf
	SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B* ___Alpha1Tnaf_9;

public:
	inline static int32_t get_offset_of_MinusOne_0() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___MinusOne_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_MinusOne_0() const { return ___MinusOne_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_MinusOne_0() { return &___MinusOne_0; }
	inline void set_MinusOne_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___MinusOne_0 = value;
		Il2CppCodeGenWriteBarrier((&___MinusOne_0), value);
	}

	inline static int32_t get_offset_of_MinusTwo_1() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___MinusTwo_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_MinusTwo_1() const { return ___MinusTwo_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_MinusTwo_1() { return &___MinusTwo_1; }
	inline void set_MinusTwo_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___MinusTwo_1 = value;
		Il2CppCodeGenWriteBarrier((&___MinusTwo_1), value);
	}

	inline static int32_t get_offset_of_MinusThree_2() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___MinusThree_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_MinusThree_2() const { return ___MinusThree_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_MinusThree_2() { return &___MinusThree_2; }
	inline void set_MinusThree_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___MinusThree_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinusThree_2), value);
	}

	inline static int32_t get_offset_of_Four_3() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___Four_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Four_3() const { return ___Four_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Four_3() { return &___Four_3; }
	inline void set_Four_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Four_3 = value;
		Il2CppCodeGenWriteBarrier((&___Four_3), value);
	}

	inline static int32_t get_offset_of_Alpha0_6() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___Alpha0_6)); }
	inline ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A* get_Alpha0_6() const { return ___Alpha0_6; }
	inline ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A** get_address_of_Alpha0_6() { return &___Alpha0_6; }
	inline void set_Alpha0_6(ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A* value)
	{
		___Alpha0_6 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha0_6), value);
	}

	inline static int32_t get_offset_of_Alpha0Tnaf_7() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___Alpha0Tnaf_7)); }
	inline SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B* get_Alpha0Tnaf_7() const { return ___Alpha0Tnaf_7; }
	inline SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B** get_address_of_Alpha0Tnaf_7() { return &___Alpha0Tnaf_7; }
	inline void set_Alpha0Tnaf_7(SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B* value)
	{
		___Alpha0Tnaf_7 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha0Tnaf_7), value);
	}

	inline static int32_t get_offset_of_Alpha1_8() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___Alpha1_8)); }
	inline ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A* get_Alpha1_8() const { return ___Alpha1_8; }
	inline ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A** get_address_of_Alpha1_8() { return &___Alpha1_8; }
	inline void set_Alpha1_8(ZTauElementU5BU5D_t582D7D74B3D43656DCB34738B768CEA5A21BC85A* value)
	{
		___Alpha1_8 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha1_8), value);
	}

	inline static int32_t get_offset_of_Alpha1Tnaf_9() { return static_cast<int32_t>(offsetof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields, ___Alpha1Tnaf_9)); }
	inline SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B* get_Alpha1Tnaf_9() const { return ___Alpha1Tnaf_9; }
	inline SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B** get_address_of_Alpha1Tnaf_9() { return &___Alpha1Tnaf_9; }
	inline void set_Alpha1Tnaf_9(SByteU5BU5DU5BU5D_t19CBFB8629EDA96B024E08CC2B9645E36310E28B* value)
	{
		___Alpha1Tnaf_9 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha1Tnaf_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TNAF_T205143603BEF6691CF6DCE645EF5CE4EE8110580_H
#ifndef ZTAUELEMENT_TFBE15C434250DCCDD6B61013AFD2D78309FFB613_H
#define ZTAUELEMENT_TFBE15C434250DCCDD6B61013AFD2D78309FFB613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.ZTauElement
struct  ZTauElement_tFBE15C434250DCCDD6B61013AFD2D78309FFB613  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.ZTauElement::u
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___u_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Abc.ZTauElement::v
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___v_1;

public:
	inline static int32_t get_offset_of_u_0() { return static_cast<int32_t>(offsetof(ZTauElement_tFBE15C434250DCCDD6B61013AFD2D78309FFB613, ___u_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_u_0() const { return ___u_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_u_0() { return &___u_0; }
	inline void set_u_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___u_0 = value;
		Il2CppCodeGenWriteBarrier((&___u_0), value);
	}

	inline static int32_t get_offset_of_v_1() { return static_cast<int32_t>(offsetof(ZTauElement_tFBE15C434250DCCDD6B61013AFD2D78309FFB613, ___v_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_v_1() const { return ___v_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_v_1() { return &___v_1; }
	inline void set_v_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___v_1 = value;
		Il2CppCodeGenWriteBarrier((&___v_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZTAUELEMENT_TFBE15C434250DCCDD6B61013AFD2D78309FFB613_H
#ifndef ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#define ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractECLookupTable
struct  AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTECLOOKUPTABLE_TC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08_H
#ifndef CURVE25519FIELD_TDD3476A938537689C252CA4FB0FCE26857D180B6_H
#define CURVE25519FIELD_TDD3476A938537689C252CA4FB0FCE26857D180B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Field
struct  Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6  : public RuntimeObject
{
public:

public:
};

struct Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_2() { return static_cast<int32_t>(offsetof(Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6_StaticFields, ___PExt_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_2() const { return ___PExt_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_2() { return &___PExt_2; }
	inline void set_PExt_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519FIELD_TDD3476A938537689C252CA4FB0FCE26857D180B6_H
#ifndef SM2P256V1FIELD_TE98827DCBE882E004724C89109F209F061D62697_H
#define SM2P256V1FIELD_TE98827DCBE882E004724C89109F209F061D62697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Field
struct  SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697  : public RuntimeObject
{
public:

public:
};

struct SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1FIELD_TE98827DCBE882E004724C89109F209F061D62697_H
#ifndef SECP256R1FIELD_T777987681746F7EFB70D90D52B5D16605D9C5404_H
#define SECP256R1FIELD_T777987681746F7EFB70D90D52B5D16605D9C5404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Field
struct  SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404  : public RuntimeObject
{
public:

public:
};

struct SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1FIELD_T777987681746F7EFB70D90D52B5D16605D9C5404_H
#ifndef SECP384R1FIELD_TD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_H
#define SECP384R1FIELD_TD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field
struct  SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D  : public RuntimeObject
{
public:

public:
};

struct SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1FIELD_TD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_H
#ifndef SECP521R1FIELD_TD47DE3BBE85069282736BC7A527BE128D1E665EE_H
#define SECP521R1FIELD_TD47DE3BBE85069282736BC7A527BE128D1E665EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Field
struct  SecP521R1Field_tD47DE3BBE85069282736BC7A527BE128D1E665EE  : public RuntimeObject
{
public:

public:
};

struct SecP521R1Field_tD47DE3BBE85069282736BC7A527BE128D1E665EE_StaticFields
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP521R1Field_tD47DE3BBE85069282736BC7A527BE128D1E665EE_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1FIELD_TD47DE3BBE85069282736BC7A527BE128D1E665EE_H
#ifndef SECT113FIELD_T39CB2E686A85D53F7AC46B6B77992F9B82D60074_H
#define SECT113FIELD_T39CB2E686A85D53F7AC46B6B77992F9B82D60074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113Field
struct  SecT113Field_t39CB2E686A85D53F7AC46B6B77992F9B82D60074  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113FIELD_T39CB2E686A85D53F7AC46B6B77992F9B82D60074_H
#ifndef SECT131FIELD_T73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_H
#define SECT131FIELD_T73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131Field
struct  SecT131Field_t73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50  : public RuntimeObject
{
public:

public:
};

struct SecT131Field_t73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_2;

public:
	inline static int32_t get_offset_of_ROOT_Z_2() { return static_cast<int32_t>(offsetof(SecT131Field_t73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_StaticFields, ___ROOT_Z_2)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_2() const { return ___ROOT_Z_2; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_2() { return &___ROOT_Z_2; }
	inline void set_ROOT_Z_2(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_2 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131FIELD_T73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_H
#ifndef SECT163FIELD_T5717E4757416F04979AFD82DF103E2A716196F08_H
#define SECT163FIELD_T5717E4757416F04979AFD82DF103E2A716196F08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163Field
struct  SecT163Field_t5717E4757416F04979AFD82DF103E2A716196F08  : public RuntimeObject
{
public:

public:
};

struct SecT163Field_t5717E4757416F04979AFD82DF103E2A716196F08_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_2;

public:
	inline static int32_t get_offset_of_ROOT_Z_2() { return static_cast<int32_t>(offsetof(SecT163Field_t5717E4757416F04979AFD82DF103E2A716196F08_StaticFields, ___ROOT_Z_2)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_2() const { return ___ROOT_Z_2; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_2() { return &___ROOT_Z_2; }
	inline void set_ROOT_Z_2(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_2 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163FIELD_T5717E4757416F04979AFD82DF103E2A716196F08_H
#ifndef SECT193FIELD_TB08F01C274AEECE436C03D349B7792351880A5C9_H
#define SECT193FIELD_TB08F01C274AEECE436C03D349B7792351880A5C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193Field
struct  SecT193Field_tB08F01C274AEECE436C03D349B7792351880A5C9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193FIELD_TB08F01C274AEECE436C03D349B7792351880A5C9_H
#ifndef SECT233FIELD_T7D38FB325C7393A090A2ADD73BC07A1BB86564AA_H
#define SECT233FIELD_T7D38FB325C7393A090A2ADD73BC07A1BB86564AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233Field
struct  SecT233Field_t7D38FB325C7393A090A2ADD73BC07A1BB86564AA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233FIELD_T7D38FB325C7393A090A2ADD73BC07A1BB86564AA_H
#ifndef SECT239FIELD_T0D0545024FD6AE4E3C5327B8B8ADD672A04B8042_H
#define SECT239FIELD_T0D0545024FD6AE4E3C5327B8B8ADD672A04B8042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239Field
struct  SecT239Field_t0D0545024FD6AE4E3C5327B8B8ADD672A04B8042  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239FIELD_T0D0545024FD6AE4E3C5327B8B8ADD672A04B8042_H
#ifndef SECT283FIELD_T72503D5F62573E4D32BF8E3A69905BEC8718BF1A_H
#define SECT283FIELD_T72503D5F62573E4D32BF8E3A69905BEC8718BF1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283Field
struct  SecT283Field_t72503D5F62573E4D32BF8E3A69905BEC8718BF1A  : public RuntimeObject
{
public:

public:
};

struct SecT283Field_t72503D5F62573E4D32BF8E3A69905BEC8718BF1A_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_2;

public:
	inline static int32_t get_offset_of_ROOT_Z_2() { return static_cast<int32_t>(offsetof(SecT283Field_t72503D5F62573E4D32BF8E3A69905BEC8718BF1A_StaticFields, ___ROOT_Z_2)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_2() const { return ___ROOT_Z_2; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_2() { return &___ROOT_Z_2; }
	inline void set_ROOT_Z_2(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_2 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283FIELD_T72503D5F62573E4D32BF8E3A69905BEC8718BF1A_H
#ifndef SECT409FIELD_T639E3B51B3736615E9DFC03F3A147BD5968E32E8_H
#define SECT409FIELD_T639E3B51B3736615E9DFC03F3A147BD5968E32E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409Field
struct  SecT409Field_t639E3B51B3736615E9DFC03F3A147BD5968E32E8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409FIELD_T639E3B51B3736615E9DFC03F3A147BD5968E32E8_H
#ifndef SECT571FIELD_T4192C1BFC09947758F3428476BDE2232ABB24341_H
#define SECT571FIELD_T4192C1BFC09947758F3428476BDE2232ABB24341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571Field
struct  SecT571Field_t4192C1BFC09947758F3428476BDE2232ABB24341  : public RuntimeObject
{
public:

public:
};

struct SecT571Field_t4192C1BFC09947758F3428476BDE2232ABB24341_StaticFields
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571Field::ROOT_Z
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___ROOT_Z_2;

public:
	inline static int32_t get_offset_of_ROOT_Z_2() { return static_cast<int32_t>(offsetof(SecT571Field_t4192C1BFC09947758F3428476BDE2232ABB24341_StaticFields, ___ROOT_Z_2)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_ROOT_Z_2() const { return ___ROOT_Z_2; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_ROOT_Z_2() { return &___ROOT_Z_2; }
	inline void set_ROOT_Z_2(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___ROOT_Z_2 = value;
		Il2CppCodeGenWriteBarrier((&___ROOT_Z_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571FIELD_T4192C1BFC09947758F3428476BDE2232ABB24341_H
#ifndef ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#define ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct  ECCurve_t6071986B64066FBF97315592BE971355FA584A39  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.Field.IFiniteField BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_field
	RuntimeObject* ___m_field_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_a
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_a_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_b
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_b_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_order
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_order_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_cofactor
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___m_cofactor_12;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_coord
	int32_t ___m_coord_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Endo.ECEndomorphism BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_endomorphism
	RuntimeObject* ___m_endomorphism_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve::m_multiplier
	RuntimeObject* ___m_multiplier_15;

public:
	inline static int32_t get_offset_of_m_field_8() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_field_8)); }
	inline RuntimeObject* get_m_field_8() const { return ___m_field_8; }
	inline RuntimeObject** get_address_of_m_field_8() { return &___m_field_8; }
	inline void set_m_field_8(RuntimeObject* value)
	{
		___m_field_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_field_8), value);
	}

	inline static int32_t get_offset_of_m_a_9() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_a_9)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_a_9() const { return ___m_a_9; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_a_9() { return &___m_a_9; }
	inline void set_m_a_9(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_a_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_a_9), value);
	}

	inline static int32_t get_offset_of_m_b_10() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_b_10)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_b_10() const { return ___m_b_10; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_b_10() { return &___m_b_10; }
	inline void set_m_b_10(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_b_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_b_10), value);
	}

	inline static int32_t get_offset_of_m_order_11() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_order_11)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_order_11() const { return ___m_order_11; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_order_11() { return &___m_order_11; }
	inline void set_m_order_11(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_order_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_order_11), value);
	}

	inline static int32_t get_offset_of_m_cofactor_12() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_cofactor_12)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_m_cofactor_12() const { return ___m_cofactor_12; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_m_cofactor_12() { return &___m_cofactor_12; }
	inline void set_m_cofactor_12(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___m_cofactor_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_cofactor_12), value);
	}

	inline static int32_t get_offset_of_m_coord_13() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_coord_13)); }
	inline int32_t get_m_coord_13() const { return ___m_coord_13; }
	inline int32_t* get_address_of_m_coord_13() { return &___m_coord_13; }
	inline void set_m_coord_13(int32_t value)
	{
		___m_coord_13 = value;
	}

	inline static int32_t get_offset_of_m_endomorphism_14() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_endomorphism_14)); }
	inline RuntimeObject* get_m_endomorphism_14() const { return ___m_endomorphism_14; }
	inline RuntimeObject** get_address_of_m_endomorphism_14() { return &___m_endomorphism_14; }
	inline void set_m_endomorphism_14(RuntimeObject* value)
	{
		___m_endomorphism_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_14), value);
	}

	inline static int32_t get_offset_of_m_multiplier_15() { return static_cast<int32_t>(offsetof(ECCurve_t6071986B64066FBF97315592BE971355FA584A39, ___m_multiplier_15)); }
	inline RuntimeObject* get_m_multiplier_15() const { return ___m_multiplier_15; }
	inline RuntimeObject** get_address_of_m_multiplier_15() { return &___m_multiplier_15; }
	inline void set_m_multiplier_15(RuntimeObject* value)
	{
		___m_multiplier_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_multiplier_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECCURVE_T6071986B64066FBF97315592BE971355FA584A39_H
#ifndef ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#define ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement
struct  ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECFIELDELEMENT_T057485A48DA7E395BBA71829B6B78D160EC5F427_H
#ifndef ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#define ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct  ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___m_curve_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_x
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_x_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_y
	ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * ___m_y_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_zs
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___m_zs_4;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_withCompression
	bool ___m_withCompression_5;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::m_preCompTable
	RuntimeObject* ___m_preCompTable_6;

public:
	inline static int32_t get_offset_of_m_curve_1() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_curve_1)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_m_curve_1() const { return ___m_curve_1; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_m_curve_1() { return &___m_curve_1; }
	inline void set_m_curve_1(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___m_curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_1), value);
	}

	inline static int32_t get_offset_of_m_x_2() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_x_2)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_x_2() const { return ___m_x_2; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_x_2() { return &___m_x_2; }
	inline void set_m_x_2(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_x_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_x_2), value);
	}

	inline static int32_t get_offset_of_m_y_3() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_y_3)); }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * get_m_y_3() const { return ___m_y_3; }
	inline ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 ** get_address_of_m_y_3() { return &___m_y_3; }
	inline void set_m_y_3(ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427 * value)
	{
		___m_y_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_y_3), value);
	}

	inline static int32_t get_offset_of_m_zs_4() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_zs_4)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_m_zs_4() const { return ___m_zs_4; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_m_zs_4() { return &___m_zs_4; }
	inline void set_m_zs_4(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___m_zs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_zs_4), value);
	}

	inline static int32_t get_offset_of_m_withCompression_5() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_withCompression_5)); }
	inline bool get_m_withCompression_5() const { return ___m_withCompression_5; }
	inline bool* get_address_of_m_withCompression_5() { return &___m_withCompression_5; }
	inline void set_m_withCompression_5(bool value)
	{
		___m_withCompression_5 = value;
	}

	inline static int32_t get_offset_of_m_preCompTable_6() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E, ___m_preCompTable_6)); }
	inline RuntimeObject* get_m_preCompTable_6() const { return ___m_preCompTable_6; }
	inline RuntimeObject** get_address_of_m_preCompTable_6() { return &___m_preCompTable_6; }
	inline void set_m_preCompTable_6(RuntimeObject* value)
	{
		___m_preCompTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompTable_6), value);
	}
};

struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint::EMPTY_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___EMPTY_ZS_0;

public:
	inline static int32_t get_offset_of_EMPTY_ZS_0() { return static_cast<int32_t>(offsetof(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E_StaticFields, ___EMPTY_ZS_0)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_EMPTY_ZS_0() const { return ___EMPTY_ZS_0; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_EMPTY_ZS_0() { return &___EMPTY_ZS_0; }
	inline void set_EMPTY_ZS_0(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___EMPTY_ZS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_ZS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINT_T76C9C9A58D681A59C0795B257095B198DDC5518E_H
#ifndef CMSCONTENTENCRYPTORBUILDER_TF4ECE9654844D752C05C7F359C87CCB037861A23_H
#define CMSCONTENTENCRYPTORBUILDER_TF4ECE9654844D752C05C7F359C87CCB037861A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsContentEncryptorBuilder
struct  CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsContentEncryptorBuilder::encryptionOID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___encryptionOID_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsContentEncryptorBuilder::keySize
	int32_t ___keySize_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Cms.EnvelopedDataHelper BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsContentEncryptorBuilder::helper
	EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5 * ___helper_3;

public:
	inline static int32_t get_offset_of_encryptionOID_1() { return static_cast<int32_t>(offsetof(CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23, ___encryptionOID_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_encryptionOID_1() const { return ___encryptionOID_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_encryptionOID_1() { return &___encryptionOID_1; }
	inline void set_encryptionOID_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___encryptionOID_1 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionOID_1), value);
	}

	inline static int32_t get_offset_of_keySize_2() { return static_cast<int32_t>(offsetof(CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23, ___keySize_2)); }
	inline int32_t get_keySize_2() const { return ___keySize_2; }
	inline int32_t* get_address_of_keySize_2() { return &___keySize_2; }
	inline void set_keySize_2(int32_t value)
	{
		___keySize_2 = value;
	}

	inline static int32_t get_offset_of_helper_3() { return static_cast<int32_t>(offsetof(CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23, ___helper_3)); }
	inline EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5 * get_helper_3() const { return ___helper_3; }
	inline EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5 ** get_address_of_helper_3() { return &___helper_3; }
	inline void set_helper_3(EnvelopedDataHelper_t927BADD2921320FC22E97784092748945C3385B5 * value)
	{
		___helper_3 = value;
		Il2CppCodeGenWriteBarrier((&___helper_3), value);
	}
};

struct CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Operators.CmsContentEncryptorBuilder::KeySizes
	RuntimeObject* ___KeySizes_0;

public:
	inline static int32_t get_offset_of_KeySizes_0() { return static_cast<int32_t>(offsetof(CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23_StaticFields, ___KeySizes_0)); }
	inline RuntimeObject* get_KeySizes_0() const { return ___KeySizes_0; }
	inline RuntimeObject** get_address_of_KeySizes_0() { return &___KeySizes_0; }
	inline void set_KeySizes_0(RuntimeObject* value)
	{
		___KeySizes_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeySizes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSCONTENTENCRYPTORBUILDER_TF4ECE9654844D752C05C7F359C87CCB037861A23_H
#ifndef ABSTRACTF2MCURVE_T6B0E18B7670A83808724BFAB503DA1B72EE06187_H
#define ABSTRACTF2MCURVE_T6B0E18B7670A83808724BFAB503DA1B72EE06187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mCurve
struct  AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187  : public ECCurve_t6071986B64066FBF97315592BE971355FA584A39
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mCurve::si
	BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* ___si_16;

public:
	inline static int32_t get_offset_of_si_16() { return static_cast<int32_t>(offsetof(AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187, ___si_16)); }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* get_si_16() const { return ___si_16; }
	inline BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539** get_address_of_si_16() { return &___si_16; }
	inline void set_si_16(BigIntegerU5BU5D_t341F263D8A0392D9001EF2D781E0E4B982785539* value)
	{
		___si_16 = value;
		Il2CppCodeGenWriteBarrier((&___si_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MCURVE_T6B0E18B7670A83808724BFAB503DA1B72EE06187_H
#ifndef ABSTRACTF2MFIELDELEMENT_T7781A419D272BDEF84C6D55FD05F4A7413CD2C50_H
#define ABSTRACTF2MFIELDELEMENT_T7781A419D272BDEF84C6D55FD05F4A7413CD2C50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mFieldElement
struct  AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50  : public ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MFIELDELEMENT_T7781A419D272BDEF84C6D55FD05F4A7413CD2C50_H
#ifndef ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#define ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpCurve
struct  AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96  : public ECCurve_t6071986B64066FBF97315592BE971355FA584A39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPCURVE_T56FCFCC45B55783648A8495F6EEDE080A84B4E96_H
#ifndef ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#define ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpFieldElement
struct  AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A  : public ECFieldElement_t057485A48DA7E395BBA71829B6B78D160EC5F427
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPFIELDELEMENT_T6DC375B70C69B659A2C1E1951098A0979867555A_H
#ifndef CURVE25519LOOKUPTABLE_T04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035_H
#define CURVE25519LOOKUPTABLE_T04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519_Curve25519LookupTable
struct  Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519_Curve25519LookupTable::m_outer
	Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519_Curve25519LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519_Curve25519LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035, ___m_outer_0)); }
	inline Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9 * get_m_outer_0() const { return ___m_outer_0; }
	inline Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519LOOKUPTABLE_T04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035_H
#ifndef SM2P256V1LOOKUPTABLE_T310BF99D124BEF07A682980392310EBA2098A7CB_H
#define SM2P256V1LOOKUPTABLE_T310BF99D124BEF07A682980392310EBA2098A7CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve_SM2P256V1LookupTable
struct  SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve_SM2P256V1LookupTable::m_outer
	SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve_SM2P256V1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve_SM2P256V1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB, ___m_outer_0)); }
	inline SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6 * get_m_outer_0() const { return ___m_outer_0; }
	inline SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1LOOKUPTABLE_T310BF99D124BEF07A682980392310EBA2098A7CB_H
#ifndef SECP256R1LOOKUPTABLE_TF1ADEBA814647310ED660A5B518A1026B752D499_H
#define SECP256R1LOOKUPTABLE_TF1ADEBA814647310ED660A5B518A1026B752D499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve_SecP256R1LookupTable
struct  SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve_SecP256R1LookupTable::m_outer
	SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve_SecP256R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Curve_SecP256R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499, ___m_outer_0)); }
	inline SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP256R1Curve_t3A597DCB2F7C2359E8AD7136309A7FC05A1039E2 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1LOOKUPTABLE_TF1ADEBA814647310ED660A5B518A1026B752D499_H
#ifndef SECP384R1LOOKUPTABLE_TBA54A565A48B2F6C72BEF484A700B5A4E90FA205_H
#define SECP384R1LOOKUPTABLE_TBA54A565A48B2F6C72BEF484A700B5A4E90FA205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve_SecP384R1LookupTable
struct  SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve_SecP384R1LookupTable::m_outer
	SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98 * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve_SecP384R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve_SecP384R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205, ___m_outer_0)); }
	inline SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1LOOKUPTABLE_TBA54A565A48B2F6C72BEF484A700B5A4E90FA205_H
#ifndef SECP521R1LOOKUPTABLE_T84D3C30F40D42BA8AD7EA064A1346C019D569B56_H
#define SECP521R1LOOKUPTABLE_T84D3C30F40D42BA8AD7EA064A1346C019D569B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve_SecP521R1LookupTable
struct  SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve_SecP521R1LookupTable::m_outer
	SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D * ___m_outer_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve_SecP521R1LookupTable::m_table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve_SecP521R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56, ___m_outer_0)); }
	inline SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D * get_m_outer_0() const { return ___m_outer_0; }
	inline SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56, ___m_table_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_m_table_1() const { return ___m_table_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1LOOKUPTABLE_T84D3C30F40D42BA8AD7EA064A1346C019D569B56_H
#ifndef SECT113R1LOOKUPTABLE_T34EC181B90CBB854C81C733BDA013FEF458C670E_H
#define SECT113R1LOOKUPTABLE_T34EC181B90CBB854C81C733BDA013FEF458C670E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve_SecT113R1LookupTable
struct  SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve_SecT113R1LookupTable::m_outer
	SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve_SecT113R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve_SecT113R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E, ___m_outer_0)); }
	inline SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1LOOKUPTABLE_T34EC181B90CBB854C81C733BDA013FEF458C670E_H
#ifndef SECT113R2LOOKUPTABLE_TA445175F15A788135D9B5078D8A7603F78D3466F_H
#define SECT113R2LOOKUPTABLE_TA445175F15A788135D9B5078D8A7603F78D3466F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve_SecT113R2LookupTable
struct  SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve_SecT113R2LookupTable::m_outer
	SecT113R2Curve_t991A5352277966895F4693704773720780531C59 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve_SecT113R2LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve_SecT113R2LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F, ___m_outer_0)); }
	inline SecT113R2Curve_t991A5352277966895F4693704773720780531C59 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT113R2Curve_t991A5352277966895F4693704773720780531C59 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT113R2Curve_t991A5352277966895F4693704773720780531C59 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2LOOKUPTABLE_TA445175F15A788135D9B5078D8A7603F78D3466F_H
#ifndef SECT131R1LOOKUPTABLE_TF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD_H
#define SECT131R1LOOKUPTABLE_TF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve_SecT131R1LookupTable
struct  SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve_SecT131R1LookupTable::m_outer
	SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve_SecT131R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve_SecT131R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD, ___m_outer_0)); }
	inline SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1LOOKUPTABLE_TF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD_H
#ifndef SECT131R2LOOKUPTABLE_TF65C4067DA4BAF31A772DB03828DE3A1AF22207A_H
#define SECT131R2LOOKUPTABLE_TF65C4067DA4BAF31A772DB03828DE3A1AF22207A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve_SecT131R2LookupTable
struct  SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve_SecT131R2LookupTable::m_outer
	SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve_SecT131R2LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve_SecT131R2LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A, ___m_outer_0)); }
	inline SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2LOOKUPTABLE_TF65C4067DA4BAF31A772DB03828DE3A1AF22207A_H
#ifndef SECT163K1LOOKUPTABLE_TED9153D29E0A01658A4D613265447878294D64F9_H
#define SECT163K1LOOKUPTABLE_TED9153D29E0A01658A4D613265447878294D64F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve_SecT163K1LookupTable
struct  SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve_SecT163K1LookupTable::m_outer
	SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve_SecT163K1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve_SecT163K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9, ___m_outer_0)); }
	inline SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1LOOKUPTABLE_TED9153D29E0A01658A4D613265447878294D64F9_H
#ifndef SECT163R1LOOKUPTABLE_TEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1_H
#define SECT163R1LOOKUPTABLE_TEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve_SecT163R1LookupTable
struct  SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve_SecT163R1LookupTable::m_outer
	SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve_SecT163R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve_SecT163R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1, ___m_outer_0)); }
	inline SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1LOOKUPTABLE_TEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1_H
#ifndef SECT163R2LOOKUPTABLE_TC55E12024A381DABE0A93C04B098D4D61D897B5D_H
#define SECT163R2LOOKUPTABLE_TC55E12024A381DABE0A93C04B098D4D61D897B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve_SecT163R2LookupTable
struct  SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve_SecT163R2LookupTable::m_outer
	SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve_SecT163R2LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve_SecT163R2LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D, ___m_outer_0)); }
	inline SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2LOOKUPTABLE_TC55E12024A381DABE0A93C04B098D4D61D897B5D_H
#ifndef SECT193R1LOOKUPTABLE_TFC0475A328ED88603E50DCFB7BAE6D4F4195C762_H
#define SECT193R1LOOKUPTABLE_TFC0475A328ED88603E50DCFB7BAE6D4F4195C762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve_SecT193R1LookupTable
struct  SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve_SecT193R1LookupTable::m_outer
	SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve_SecT193R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve_SecT193R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762, ___m_outer_0)); }
	inline SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1LOOKUPTABLE_TFC0475A328ED88603E50DCFB7BAE6D4F4195C762_H
#ifndef SECT193R2LOOKUPTABLE_T7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508_H
#define SECT193R2LOOKUPTABLE_T7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve_SecT193R2LookupTable
struct  SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve_SecT193R2LookupTable::m_outer
	SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve_SecT193R2LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve_SecT193R2LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508, ___m_outer_0)); }
	inline SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2LOOKUPTABLE_T7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508_H
#ifndef SECT233K1LOOKUPTABLE_T74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E_H
#define SECT233K1LOOKUPTABLE_T74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve_SecT233K1LookupTable
struct  SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve_SecT233K1LookupTable::m_outer
	SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve_SecT233K1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve_SecT233K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E, ___m_outer_0)); }
	inline SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1LOOKUPTABLE_T74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E_H
#ifndef SECT233R1LOOKUPTABLE_TC91F4A1BE3758E17803F7C299D3E9909F7C687C4_H
#define SECT233R1LOOKUPTABLE_TC91F4A1BE3758E17803F7C299D3E9909F7C687C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve_SecT233R1LookupTable
struct  SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve_SecT233R1LookupTable::m_outer
	SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve_SecT233R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve_SecT233R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4, ___m_outer_0)); }
	inline SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1LOOKUPTABLE_TC91F4A1BE3758E17803F7C299D3E9909F7C687C4_H
#ifndef SECT239K1LOOKUPTABLE_TB3FDDBBED4B1CFF26C31EF7569B45AD822383D91_H
#define SECT239K1LOOKUPTABLE_TB3FDDBBED4B1CFF26C31EF7569B45AD822383D91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve_SecT239K1LookupTable
struct  SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve_SecT239K1LookupTable::m_outer
	SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve_SecT239K1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve_SecT239K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91, ___m_outer_0)); }
	inline SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1LOOKUPTABLE_TB3FDDBBED4B1CFF26C31EF7569B45AD822383D91_H
#ifndef SECT283K1LOOKUPTABLE_TC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8_H
#define SECT283K1LOOKUPTABLE_TC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve_SecT283K1LookupTable
struct  SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve_SecT283K1LookupTable::m_outer
	SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve_SecT283K1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve_SecT283K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8, ___m_outer_0)); }
	inline SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1LOOKUPTABLE_TC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8_H
#ifndef SECT283R1LOOKUPTABLE_T6DB54E45178FABE871C7B957776BC82FEB05A649_H
#define SECT283R1LOOKUPTABLE_T6DB54E45178FABE871C7B957776BC82FEB05A649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve_SecT283R1LookupTable
struct  SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve_SecT283R1LookupTable::m_outer
	SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve_SecT283R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve_SecT283R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649, ___m_outer_0)); }
	inline SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1LOOKUPTABLE_T6DB54E45178FABE871C7B957776BC82FEB05A649_H
#ifndef SECT409K1LOOKUPTABLE_TC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B_H
#define SECT409K1LOOKUPTABLE_TC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve_SecT409K1LookupTable
struct  SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve_SecT409K1LookupTable::m_outer
	SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve_SecT409K1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve_SecT409K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B, ___m_outer_0)); }
	inline SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1LOOKUPTABLE_TC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B_H
#ifndef SECT409R1LOOKUPTABLE_T66C7A47E74C94BBE74CFA4E278E153020BB8E8E6_H
#define SECT409R1LOOKUPTABLE_T66C7A47E74C94BBE74CFA4E278E153020BB8E8E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve_SecT409R1LookupTable
struct  SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve_SecT409R1LookupTable::m_outer
	SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve_SecT409R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve_SecT409R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6, ___m_outer_0)); }
	inline SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1LOOKUPTABLE_T66C7A47E74C94BBE74CFA4E278E153020BB8E8E6_H
#ifndef SECT571K1LOOKUPTABLE_TF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D_H
#define SECT571K1LOOKUPTABLE_TF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve_SecT571K1LookupTable
struct  SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve_SecT571K1LookupTable::m_outer
	SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve_SecT571K1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve_SecT571K1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D, ___m_outer_0)); }
	inline SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1LOOKUPTABLE_TF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D_H
#ifndef SECT571R1LOOKUPTABLE_TDCF20699FF29EB94F30A489E6E823785D9C2138C_H
#define SECT571R1LOOKUPTABLE_TDCF20699FF29EB94F30A489E6E823785D9C2138C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve_SecT571R1LookupTable
struct  SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C  : public AbstractECLookupTable_tC78D0B134E6D0D7189471818AF3DA5CCB2BC5E08
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve_SecT571R1LookupTable::m_outer
	SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544 * ___m_outer_0;
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve_SecT571R1LookupTable::m_table
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___m_table_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve_SecT571R1LookupTable::m_size
	int32_t ___m_size_2;

public:
	inline static int32_t get_offset_of_m_outer_0() { return static_cast<int32_t>(offsetof(SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C, ___m_outer_0)); }
	inline SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544 * get_m_outer_0() const { return ___m_outer_0; }
	inline SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544 ** get_address_of_m_outer_0() { return &___m_outer_0; }
	inline void set_m_outer_0(SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544 * value)
	{
		___m_outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_outer_0), value);
	}

	inline static int32_t get_offset_of_m_table_1() { return static_cast<int32_t>(offsetof(SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C, ___m_table_1)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_m_table_1() const { return ___m_table_1; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_m_table_1() { return &___m_table_1; }
	inline void set_m_table_1(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___m_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_table_1), value);
	}

	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C, ___m_size_2)); }
	inline int32_t get_m_size_2() const { return ___m_size_2; }
	inline int32_t* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(int32_t value)
	{
		___m_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1LOOKUPTABLE_TDCF20699FF29EB94F30A489E6E823785D9C2138C_H
#ifndef ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#define ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPointBase
struct  ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB  : public ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINTBASE_T27F91F242B063C822C0F100D6B7E27214F72A9CB_H
#ifndef ABSTRACTF2MPOINT_T16788EF8771AEC999392932B2C0237BC015F7FAF_H
#define ABSTRACTF2MPOINT_T16788EF8771AEC999392932B2C0237BC015F7FAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractF2mPoint
struct  AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF  : public ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MPOINT_T16788EF8771AEC999392932B2C0237BC015F7FAF_H
#ifndef ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#define ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.AbstractFpPoint
struct  AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519  : public ECPointBase_t27F91F242B063C822C0F100D6B7E27214F72A9CB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPPOINT_TBAA02ED31D2B0FDE93B0D804264977CE359BD519_H
#ifndef CURVE25519_TE6BE7BA7458E061F3F69652F5DB610DD965173F9_H
#define CURVE25519_TE6BE7BA7458E061F3F69652F5DB610DD965173F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519
struct  Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::m_infinity
	Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957 * ___m_infinity_22;

public:
	inline static int32_t get_offset_of_m_infinity_22() { return static_cast<int32_t>(offsetof(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9, ___m_infinity_22)); }
	inline Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957 * get_m_infinity_22() const { return ___m_infinity_22; }
	inline Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957 ** get_address_of_m_infinity_22() { return &___m_infinity_22; }
	inline void set_m_infinity_22(Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957 * value)
	{
		___m_infinity_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_22), value);
	}
};

struct Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::C_a
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___C_a_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::C_b
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___C_b_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::CURVE25519_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___CURVE25519_AFFINE_ZS_21;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_C_a_17() { return static_cast<int32_t>(offsetof(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields, ___C_a_17)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_C_a_17() const { return ___C_a_17; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_C_a_17() { return &___C_a_17; }
	inline void set_C_a_17(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___C_a_17 = value;
		Il2CppCodeGenWriteBarrier((&___C_a_17), value);
	}

	inline static int32_t get_offset_of_C_b_18() { return static_cast<int32_t>(offsetof(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields, ___C_b_18)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_C_b_18() const { return ___C_b_18; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_C_b_18() { return &___C_b_18; }
	inline void set_C_b_18(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___C_b_18 = value;
		Il2CppCodeGenWriteBarrier((&___C_b_18), value);
	}

	inline static int32_t get_offset_of_CURVE25519_AFFINE_ZS_21() { return static_cast<int32_t>(offsetof(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields, ___CURVE25519_AFFINE_ZS_21)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_CURVE25519_AFFINE_ZS_21() const { return ___CURVE25519_AFFINE_ZS_21; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_CURVE25519_AFFINE_ZS_21() { return &___CURVE25519_AFFINE_ZS_21; }
	inline void set_CURVE25519_AFFINE_ZS_21(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___CURVE25519_AFFINE_ZS_21 = value;
		Il2CppCodeGenWriteBarrier((&___CURVE25519_AFFINE_ZS_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519_TE6BE7BA7458E061F3F69652F5DB610DD965173F9_H
#ifndef CURVE25519FIELDELEMENT_T193171FE385A3E6FFC36E10873E438A1B42EC58B_H
#define CURVE25519FIELDELEMENT_T193171FE385A3E6FFC36E10873E438A1B42EC58B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement
struct  Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B, ___x_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_2() const { return ___x_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

struct Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement::PRECOMP_POW2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PRECOMP_POW2_1;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}

	inline static int32_t get_offset_of_PRECOMP_POW2_1() { return static_cast<int32_t>(offsetof(Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B_StaticFields, ___PRECOMP_POW2_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PRECOMP_POW2_1() const { return ___PRECOMP_POW2_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PRECOMP_POW2_1() { return &___PRECOMP_POW2_1; }
	inline void set_PRECOMP_POW2_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PRECOMP_POW2_1 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_POW2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519FIELDELEMENT_T193171FE385A3E6FFC36E10873E438A1B42EC58B_H
#ifndef SM2P256V1CURVE_TE0E86456EECD2D5F53A9362120C73383F4760DB6_H
#define SM2P256V1CURVE_TE0E86456EECD2D5F53A9362120C73383F4760DB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve
struct  SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve::m_infinity
	SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6, ___m_infinity_20)); }
	inline SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Curve::SM2P256V1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SM2P256V1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SM2P256V1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6_StaticFields, ___SM2P256V1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SM2P256V1_AFFINE_ZS_19() const { return ___SM2P256V1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SM2P256V1_AFFINE_ZS_19() { return &___SM2P256V1_AFFINE_ZS_19; }
	inline void set_SM2P256V1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SM2P256V1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SM2P256V1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1CURVE_TE0E86456EECD2D5F53A9362120C73383F4760DB6_H
#ifndef SM2P256V1FIELDELEMENT_TCFE2889BC0189C9A2AB4423713702C63E752210E_H
#define SM2P256V1FIELDELEMENT_TCFE2889BC0189C9A2AB4423713702C63E752210E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1FieldElement
struct  SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1FIELDELEMENT_TCFE2889BC0189C9A2AB4423713702C63E752210E_H
#ifndef SECP256R1FIELDELEMENT_TE7F6784179B11DAD7316CFC49FF5899BBBF73C95_H
#define SECP256R1FIELDELEMENT_TE7F6784179B11DAD7316CFC49FF5899BBBF73C95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1FieldElement
struct  SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1FIELDELEMENT_TE7F6784179B11DAD7316CFC49FF5899BBBF73C95_H
#ifndef SECP384R1CURVE_T62D73FB67A590441DA962052796B90B050AD6D98_H
#define SECP384R1CURVE_T62D73FB67A590441DA962052796B90B050AD6D98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve
struct  SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve::m_infinity
	SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98, ___m_infinity_20)); }
	inline SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Curve::SECP384R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP384R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP384R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98_StaticFields, ___SECP384R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP384R1_AFFINE_ZS_19() const { return ___SECP384R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP384R1_AFFINE_ZS_19() { return &___SECP384R1_AFFINE_ZS_19; }
	inline void set_SECP384R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP384R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP384R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1CURVE_T62D73FB67A590441DA962052796B90B050AD6D98_H
#ifndef SECP384R1FIELDELEMENT_TA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_H
#define SECP384R1FIELDELEMENT_TA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1FieldElement
struct  SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1FIELDELEMENT_TA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_H
#ifndef SECP521R1CURVE_T2E791E526A31EEADCCC5272B2C843969253E1C8D_H
#define SECP521R1CURVE_T2E791E526A31EEADCCC5272B2C843969253E1C8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve
struct  SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D  : public AbstractFpCurve_t56FCFCC45B55783648A8495F6EEDE080A84B4E96
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve::m_infinity
	SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D, ___m_infinity_20)); }
	inline SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Curve::SECP521R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECP521R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_q_16() { return static_cast<int32_t>(offsetof(SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D_StaticFields, ___q_16)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_16() const { return ___q_16; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_16() { return &___q_16; }
	inline void set_q_16(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_16 = value;
		Il2CppCodeGenWriteBarrier((&___q_16), value);
	}

	inline static int32_t get_offset_of_SECP521R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D_StaticFields, ___SECP521R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECP521R1_AFFINE_ZS_19() const { return ___SECP521R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECP521R1_AFFINE_ZS_19() { return &___SECP521R1_AFFINE_ZS_19; }
	inline void set_SECP521R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECP521R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECP521R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1CURVE_T2E791E526A31EEADCCC5272B2C843969253E1C8D_H
#ifndef SECP521R1FIELDELEMENT_TB61FD67368C5AEE6775D29BE15EA17D1E1406A85_H
#define SECP521R1FIELDELEMENT_TB61FD67368C5AEE6775D29BE15EA17D1E1406A85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1FieldElement
struct  SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85  : public AbstractFpFieldElement_t6DC375B70C69B659A2C1E1951098A0979867555A
{
public:
	// System.UInt32[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1FieldElement::Q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85_StaticFields, ___Q_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1FIELDELEMENT_TB61FD67368C5AEE6775D29BE15EA17D1E1406A85_H
#ifndef SECT113FIELDELEMENT_T8E5FE908623CC2E303FB3A9630964A66FEEAC539_H
#define SECT113FIELDELEMENT_T8E5FE908623CC2E303FB3A9630964A66FEEAC539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113FieldElement
struct  SecT113FieldElement_t8E5FE908623CC2E303FB3A9630964A66FEEAC539  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT113FieldElement_t8E5FE908623CC2E303FB3A9630964A66FEEAC539, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113FIELDELEMENT_T8E5FE908623CC2E303FB3A9630964A66FEEAC539_H
#ifndef SECT113R1CURVE_T5D17AF7F2C812191214FD48EB62E6BCB384ED385_H
#define SECT113R1CURVE_T5D17AF7F2C812191214FD48EB62E6BCB384ED385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve
struct  SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve::m_infinity
	SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385, ___m_infinity_20)); }
	inline SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Curve::SECT113R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT113R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT113R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385_StaticFields, ___SECT113R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT113R1_AFFINE_ZS_19() const { return ___SECT113R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT113R1_AFFINE_ZS_19() { return &___SECT113R1_AFFINE_ZS_19; }
	inline void set_SECT113R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT113R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT113R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1CURVE_T5D17AF7F2C812191214FD48EB62E6BCB384ED385_H
#ifndef SECT113R2CURVE_T991A5352277966895F4693704773720780531C59_H
#define SECT113R2CURVE_T991A5352277966895F4693704773720780531C59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve
struct  SecT113R2Curve_t991A5352277966895F4693704773720780531C59  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve::m_infinity
	SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT113R2Curve_t991A5352277966895F4693704773720780531C59, ___m_infinity_20)); }
	inline SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT113R2Curve_t991A5352277966895F4693704773720780531C59_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Curve::SECT113R2_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT113R2_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT113R2_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT113R2Curve_t991A5352277966895F4693704773720780531C59_StaticFields, ___SECT113R2_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT113R2_AFFINE_ZS_19() const { return ___SECT113R2_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT113R2_AFFINE_ZS_19() { return &___SECT113R2_AFFINE_ZS_19; }
	inline void set_SECT113R2_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT113R2_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT113R2_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2CURVE_T991A5352277966895F4693704773720780531C59_H
#ifndef SECT131FIELDELEMENT_T37F614744FFC569E5EC69970454E62884897783C_H
#define SECT131FIELDELEMENT_T37F614744FFC569E5EC69970454E62884897783C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131FieldElement
struct  SecT131FieldElement_t37F614744FFC569E5EC69970454E62884897783C  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT131FieldElement_t37F614744FFC569E5EC69970454E62884897783C, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131FIELDELEMENT_T37F614744FFC569E5EC69970454E62884897783C_H
#ifndef SECT131R1CURVE_TBD175320BA67E87BA3DC3611E21C9D3A2D521071_H
#define SECT131R1CURVE_TBD175320BA67E87BA3DC3611E21C9D3A2D521071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve
struct  SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve::m_infinity
	SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071, ___m_infinity_20)); }
	inline SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Curve::SECT131R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT131R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT131R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071_StaticFields, ___SECT131R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT131R1_AFFINE_ZS_19() const { return ___SECT131R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT131R1_AFFINE_ZS_19() { return &___SECT131R1_AFFINE_ZS_19; }
	inline void set_SECT131R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT131R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT131R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1CURVE_TBD175320BA67E87BA3DC3611E21C9D3A2D521071_H
#ifndef SECT131R2CURVE_TB5D7F72E243F92146EAC655525BA3832BDAC586B_H
#define SECT131R2CURVE_TB5D7F72E243F92146EAC655525BA3832BDAC586B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve
struct  SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve::m_infinity
	SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B, ___m_infinity_20)); }
	inline SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Curve::SECT131R2_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT131R2_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT131R2_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B_StaticFields, ___SECT131R2_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT131R2_AFFINE_ZS_19() const { return ___SECT131R2_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT131R2_AFFINE_ZS_19() { return &___SECT131R2_AFFINE_ZS_19; }
	inline void set_SECT131R2_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT131R2_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT131R2_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2CURVE_TB5D7F72E243F92146EAC655525BA3832BDAC586B_H
#ifndef SECT163FIELDELEMENT_TDDE60F03E634E9D984ADF05B4854908146D3D337_H
#define SECT163FIELDELEMENT_TDDE60F03E634E9D984ADF05B4854908146D3D337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163FieldElement
struct  SecT163FieldElement_tDDE60F03E634E9D984ADF05B4854908146D3D337  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT163FieldElement_tDDE60F03E634E9D984ADF05B4854908146D3D337, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163FIELDELEMENT_TDDE60F03E634E9D984ADF05B4854908146D3D337_H
#ifndef SECT163K1CURVE_T39705CD50AF9A62FBBAC2137BD7E68DD823258E5_H
#define SECT163K1CURVE_T39705CD50AF9A62FBBAC2137BD7E68DD823258E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve
struct  SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve::m_infinity
	SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5, ___m_infinity_20)); }
	inline SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Curve::SECT163K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT163K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT163K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5_StaticFields, ___SECT163K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT163K1_AFFINE_ZS_19() const { return ___SECT163K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT163K1_AFFINE_ZS_19() { return &___SECT163K1_AFFINE_ZS_19; }
	inline void set_SECT163K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT163K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT163K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1CURVE_T39705CD50AF9A62FBBAC2137BD7E68DD823258E5_H
#ifndef SECT163R1CURVE_T99B8CCE49BB96C70621624833A61E7CD8FDF52AC_H
#define SECT163R1CURVE_T99B8CCE49BB96C70621624833A61E7CD8FDF52AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve
struct  SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve::m_infinity
	SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC, ___m_infinity_20)); }
	inline SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Curve::SECT163R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT163R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT163R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC_StaticFields, ___SECT163R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT163R1_AFFINE_ZS_19() const { return ___SECT163R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT163R1_AFFINE_ZS_19() { return &___SECT163R1_AFFINE_ZS_19; }
	inline void set_SECT163R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT163R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT163R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1CURVE_T99B8CCE49BB96C70621624833A61E7CD8FDF52AC_H
#ifndef SECT163R2CURVE_T5A9C954A36747DC843C5F50712B815F16B939024_H
#define SECT163R2CURVE_T5A9C954A36747DC843C5F50712B815F16B939024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve
struct  SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve::m_infinity
	SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024, ___m_infinity_20)); }
	inline SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Curve::SECT163R2_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT163R2_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT163R2_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024_StaticFields, ___SECT163R2_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT163R2_AFFINE_ZS_19() const { return ___SECT163R2_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT163R2_AFFINE_ZS_19() { return &___SECT163R2_AFFINE_ZS_19; }
	inline void set_SECT163R2_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT163R2_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT163R2_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2CURVE_T5A9C954A36747DC843C5F50712B815F16B939024_H
#ifndef SECT193FIELDELEMENT_TFB38FCAE5F9676251BFA7AA769AE82FCF90E0787_H
#define SECT193FIELDELEMENT_TFB38FCAE5F9676251BFA7AA769AE82FCF90E0787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193FieldElement
struct  SecT193FieldElement_tFB38FCAE5F9676251BFA7AA769AE82FCF90E0787  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT193FieldElement_tFB38FCAE5F9676251BFA7AA769AE82FCF90E0787, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193FIELDELEMENT_TFB38FCAE5F9676251BFA7AA769AE82FCF90E0787_H
#ifndef SECT193R1CURVE_T7D631AA5BDF0BCC51BA5649C80E5486835793930_H
#define SECT193R1CURVE_T7D631AA5BDF0BCC51BA5649C80E5486835793930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve
struct  SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve::m_infinity
	SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930, ___m_infinity_20)); }
	inline SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Curve::SECT193R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT193R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT193R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930_StaticFields, ___SECT193R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT193R1_AFFINE_ZS_19() const { return ___SECT193R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT193R1_AFFINE_ZS_19() { return &___SECT193R1_AFFINE_ZS_19; }
	inline void set_SECT193R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT193R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT193R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1CURVE_T7D631AA5BDF0BCC51BA5649C80E5486835793930_H
#ifndef SECT193R2CURVE_T13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_H
#define SECT193R2CURVE_T13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve
struct  SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve::m_infinity
	SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF, ___m_infinity_20)); }
	inline SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Curve::SECT193R2_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT193R2_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT193R2_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_StaticFields, ___SECT193R2_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT193R2_AFFINE_ZS_19() const { return ___SECT193R2_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT193R2_AFFINE_ZS_19() { return &___SECT193R2_AFFINE_ZS_19; }
	inline void set_SECT193R2_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT193R2_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT193R2_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2CURVE_T13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_H
#ifndef SECT233FIELDELEMENT_T5554F76263187F66FF8B0F356BDD91E839CC0AF8_H
#define SECT233FIELDELEMENT_T5554F76263187F66FF8B0F356BDD91E839CC0AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233FieldElement
struct  SecT233FieldElement_t5554F76263187F66FF8B0F356BDD91E839CC0AF8  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT233FieldElement_t5554F76263187F66FF8B0F356BDD91E839CC0AF8, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233FIELDELEMENT_T5554F76263187F66FF8B0F356BDD91E839CC0AF8_H
#ifndef SECT233K1CURVE_T845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_H
#define SECT233K1CURVE_T845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve
struct  SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve::m_infinity
	SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB, ___m_infinity_20)); }
	inline SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Curve::SECT233K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT233K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT233K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_StaticFields, ___SECT233K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT233K1_AFFINE_ZS_19() const { return ___SECT233K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT233K1_AFFINE_ZS_19() { return &___SECT233K1_AFFINE_ZS_19; }
	inline void set_SECT233K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT233K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT233K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1CURVE_T845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_H
#ifndef SECT233R1CURVE_T743FB00CE2B44DE0B303C200863CEC1BC68C64F2_H
#define SECT233R1CURVE_T743FB00CE2B44DE0B303C200863CEC1BC68C64F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve
struct  SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve::m_infinity
	SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2, ___m_infinity_20)); }
	inline SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Curve::SECT233R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT233R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT233R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2_StaticFields, ___SECT233R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT233R1_AFFINE_ZS_19() const { return ___SECT233R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT233R1_AFFINE_ZS_19() { return &___SECT233R1_AFFINE_ZS_19; }
	inline void set_SECT233R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT233R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT233R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1CURVE_T743FB00CE2B44DE0B303C200863CEC1BC68C64F2_H
#ifndef SECT239FIELDELEMENT_TB51945D7AEC3011AA8E16E5157FF9E31027D766D_H
#define SECT239FIELDELEMENT_TB51945D7AEC3011AA8E16E5157FF9E31027D766D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239FieldElement
struct  SecT239FieldElement_tB51945D7AEC3011AA8E16E5157FF9E31027D766D  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT239FieldElement_tB51945D7AEC3011AA8E16E5157FF9E31027D766D, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239FIELDELEMENT_TB51945D7AEC3011AA8E16E5157FF9E31027D766D_H
#ifndef SECT239K1CURVE_T27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_H
#define SECT239K1CURVE_T27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve
struct  SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve::m_infinity
	SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F, ___m_infinity_20)); }
	inline SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Curve::SECT239K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT239K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT239K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_StaticFields, ___SECT239K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT239K1_AFFINE_ZS_19() const { return ___SECT239K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT239K1_AFFINE_ZS_19() { return &___SECT239K1_AFFINE_ZS_19; }
	inline void set_SECT239K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT239K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT239K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1CURVE_T27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_H
#ifndef SECT283FIELDELEMENT_T07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42_H
#define SECT283FIELDELEMENT_T07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283FieldElement
struct  SecT283FieldElement_t07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT283FieldElement_t07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283FIELDELEMENT_T07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42_H
#ifndef SECT283K1CURVE_TACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_H
#define SECT283K1CURVE_TACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve
struct  SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve::m_infinity
	SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232, ___m_infinity_20)); }
	inline SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Curve::SECT283K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT283K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT283K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_StaticFields, ___SECT283K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT283K1_AFFINE_ZS_19() const { return ___SECT283K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT283K1_AFFINE_ZS_19() { return &___SECT283K1_AFFINE_ZS_19; }
	inline void set_SECT283K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT283K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT283K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1CURVE_TACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_H
#ifndef SECT283R1CURVE_T903572D0E728A53FC36A330F1832C04F534F1F31_H
#define SECT283R1CURVE_T903572D0E728A53FC36A330F1832C04F534F1F31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve
struct  SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve::m_infinity
	SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31, ___m_infinity_20)); }
	inline SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Curve::SECT283R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT283R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT283R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31_StaticFields, ___SECT283R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT283R1_AFFINE_ZS_19() const { return ___SECT283R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT283R1_AFFINE_ZS_19() { return &___SECT283R1_AFFINE_ZS_19; }
	inline void set_SECT283R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT283R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT283R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1CURVE_T903572D0E728A53FC36A330F1832C04F534F1F31_H
#ifndef SECT409FIELDELEMENT_T55D69B576761A80D7B5F806F9AED80F1CC3BA532_H
#define SECT409FIELDELEMENT_T55D69B576761A80D7B5F806F9AED80F1CC3BA532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409FieldElement
struct  SecT409FieldElement_t55D69B576761A80D7B5F806F9AED80F1CC3BA532  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT409FieldElement_t55D69B576761A80D7B5F806F9AED80F1CC3BA532, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409FIELDELEMENT_T55D69B576761A80D7B5F806F9AED80F1CC3BA532_H
#ifndef SECT409K1CURVE_T9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_H
#define SECT409K1CURVE_T9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve
struct  SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve::m_infinity
	SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE, ___m_infinity_20)); }
	inline SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Curve::SECT409K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT409K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT409K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_StaticFields, ___SECT409K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT409K1_AFFINE_ZS_19() const { return ___SECT409K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT409K1_AFFINE_ZS_19() { return &___SECT409K1_AFFINE_ZS_19; }
	inline void set_SECT409K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT409K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT409K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1CURVE_T9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_H
#ifndef SECT409R1CURVE_T42145CA43B460CF77C6A293F76CD6E589B60D444_H
#define SECT409R1CURVE_T42145CA43B460CF77C6A293F76CD6E589B60D444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve
struct  SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve::m_infinity
	SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444, ___m_infinity_20)); }
	inline SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Curve::SECT409R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT409R1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT409R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444_StaticFields, ___SECT409R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT409R1_AFFINE_ZS_19() const { return ___SECT409R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT409R1_AFFINE_ZS_19() { return &___SECT409R1_AFFINE_ZS_19; }
	inline void set_SECT409R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT409R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT409R1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1CURVE_T42145CA43B460CF77C6A293F76CD6E589B60D444_H
#ifndef SECT571FIELDELEMENT_T3F2B1330EEF0AAA4C6273FC95B330F7E2448F908_H
#define SECT571FIELDELEMENT_T3F2B1330EEF0AAA4C6273FC95B330F7E2448F908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement
struct  SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908  : public AbstractF2mFieldElement_t7781A419D272BDEF84C6D55FD05F4A7413CD2C50
{
public:
	// System.UInt64[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement::x
	UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908, ___x_0)); }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* get_x_0() const { return ___x_0; }
	inline UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(UInt64U5BU5D_tA808FE881491284FF25AFDF5C4BC92A826031EF4* value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((&___x_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571FIELDELEMENT_T3F2B1330EEF0AAA4C6273FC95B330F7E2448F908_H
#ifndef SECT571K1CURVE_TF62A85C881A5B49478B3F5E8EA465119DB3C9C02_H
#define SECT571K1CURVE_TF62A85C881A5B49478B3F5E8EA465119DB3C9C02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve
struct  SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve::m_infinity
	SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02, ___m_infinity_20)); }
	inline SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Curve::SECT571K1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT571K1_AFFINE_ZS_19;

public:
	inline static int32_t get_offset_of_SECT571K1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02_StaticFields, ___SECT571K1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT571K1_AFFINE_ZS_19() const { return ___SECT571K1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT571K1_AFFINE_ZS_19() { return &___SECT571K1_AFFINE_ZS_19; }
	inline void set_SECT571K1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT571K1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT571K1_AFFINE_ZS_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1CURVE_TF62A85C881A5B49478B3F5E8EA465119DB3C9C02_H
#ifndef SECT571R1CURVE_T0FDF48FA3BE020215779142613531578FD95B544_H
#define SECT571R1CURVE_T0FDF48FA3BE020215779142613531578FD95B544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve
struct  SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544  : public AbstractF2mCurve_t6B0E18B7670A83808724BFAB503DA1B72EE06187
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Point BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::m_infinity
	SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9 * ___m_infinity_20;

public:
	inline static int32_t get_offset_of_m_infinity_20() { return static_cast<int32_t>(offsetof(SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544, ___m_infinity_20)); }
	inline SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9 * get_m_infinity_20() const { return ___m_infinity_20; }
	inline SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9 ** get_address_of_m_infinity_20() { return &___m_infinity_20; }
	inline void set_m_infinity_20(SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9 * value)
	{
		___m_infinity_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_20), value);
	}
};

struct SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECFieldElement[] BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::SECT571R1_AFFINE_ZS
	ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* ___SECT571R1_AFFINE_ZS_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::SecT571R1_B
	SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 * ___SecT571R1_B_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571FieldElement BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Curve::SecT571R1_B_SQRT
	SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 * ___SecT571R1_B_SQRT_22;

public:
	inline static int32_t get_offset_of_SECT571R1_AFFINE_ZS_19() { return static_cast<int32_t>(offsetof(SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields, ___SECT571R1_AFFINE_ZS_19)); }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* get_SECT571R1_AFFINE_ZS_19() const { return ___SECT571R1_AFFINE_ZS_19; }
	inline ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA** get_address_of_SECT571R1_AFFINE_ZS_19() { return &___SECT571R1_AFFINE_ZS_19; }
	inline void set_SECT571R1_AFFINE_ZS_19(ECFieldElementU5BU5D_t6D519FC57B5902143A20C64E4A8062A748D356BA* value)
	{
		___SECT571R1_AFFINE_ZS_19 = value;
		Il2CppCodeGenWriteBarrier((&___SECT571R1_AFFINE_ZS_19), value);
	}

	inline static int32_t get_offset_of_SecT571R1_B_21() { return static_cast<int32_t>(offsetof(SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields, ___SecT571R1_B_21)); }
	inline SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 * get_SecT571R1_B_21() const { return ___SecT571R1_B_21; }
	inline SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 ** get_address_of_SecT571R1_B_21() { return &___SecT571R1_B_21; }
	inline void set_SecT571R1_B_21(SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 * value)
	{
		___SecT571R1_B_21 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571R1_B_21), value);
	}

	inline static int32_t get_offset_of_SecT571R1_B_SQRT_22() { return static_cast<int32_t>(offsetof(SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields, ___SecT571R1_B_SQRT_22)); }
	inline SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 * get_SecT571R1_B_SQRT_22() const { return ___SecT571R1_B_SQRT_22; }
	inline SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 ** get_address_of_SecT571R1_B_SQRT_22() { return &___SecT571R1_B_SQRT_22; }
	inline void set_SecT571R1_B_SQRT_22(SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908 * value)
	{
		___SecT571R1_B_SQRT_22 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571R1_B_SQRT_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1CURVE_T0FDF48FA3BE020215779142613531578FD95B544_H
#ifndef CURVE25519POINT_TE214CA050988B50C3EA62E8EDB3BEA0DBF88D957_H
#define CURVE25519POINT_TE214CA050988B50C3EA62E8EDB3BEA0DBF88D957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Point
struct  Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519POINT_TE214CA050988B50C3EA62E8EDB3BEA0DBF88D957_H
#ifndef SM2P256V1POINT_T618C60A49C1223B01F39D1EFC10EC26CA530AC04_H
#define SM2P256V1POINT_T618C60A49C1223B01F39D1EFC10EC26CA530AC04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.GM.SM2P256V1Point
struct  SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1POINT_T618C60A49C1223B01F39D1EFC10EC26CA530AC04_H
#ifndef SECP256R1POINT_T2873CC3090906CD40D0B5777D2995188BE0DB605_H
#define SECP256R1POINT_T2873CC3090906CD40D0B5777D2995188BE0DB605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP256R1Point
struct  SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1POINT_T2873CC3090906CD40D0B5777D2995188BE0DB605_H
#ifndef SECP384R1POINT_T5C8A576EE763465857F886C9733AEE183A477D46_H
#define SECP384R1POINT_T5C8A576EE763465857F886C9733AEE183A477D46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP384R1Point
struct  SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1POINT_T5C8A576EE763465857F886C9733AEE183A477D46_H
#ifndef SECP521R1POINT_T48E344F511893049F33BEB321366FD97036DB933_H
#define SECP521R1POINT_T48E344F511893049F33BEB321366FD97036DB933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecP521R1Point
struct  SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933  : public AbstractFpPoint_tBAA02ED31D2B0FDE93B0D804264977CE359BD519
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1POINT_T48E344F511893049F33BEB321366FD97036DB933_H
#ifndef SECT113R1POINT_T1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED_H
#define SECT113R1POINT_T1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R1Point
struct  SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1POINT_T1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED_H
#ifndef SECT113R2POINT_T9C51575488F6581A310E4D3FFEF7F10C0288D336_H
#define SECT113R2POINT_T9C51575488F6581A310E4D3FFEF7F10C0288D336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT113R2Point
struct  SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2POINT_T9C51575488F6581A310E4D3FFEF7F10C0288D336_H
#ifndef SECT131R1POINT_TACEEA5887AD487277586A2DC290CA9F001621C72_H
#define SECT131R1POINT_TACEEA5887AD487277586A2DC290CA9F001621C72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R1Point
struct  SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1POINT_TACEEA5887AD487277586A2DC290CA9F001621C72_H
#ifndef SECT131R2POINT_T919DF14637C366F25813B1AC7939FCCD2395311D_H
#define SECT131R2POINT_T919DF14637C366F25813B1AC7939FCCD2395311D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT131R2Point
struct  SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2POINT_T919DF14637C366F25813B1AC7939FCCD2395311D_H
#ifndef SECT163K1POINT_T46566468BA4F027210DCB6B83062579C24817D9A_H
#define SECT163K1POINT_T46566468BA4F027210DCB6B83062579C24817D9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163K1Point
struct  SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1POINT_T46566468BA4F027210DCB6B83062579C24817D9A_H
#ifndef SECT163R1POINT_TB2112675B476672ACF56BA6867159A6FB93A7CDF_H
#define SECT163R1POINT_TB2112675B476672ACF56BA6867159A6FB93A7CDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R1Point
struct  SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1POINT_TB2112675B476672ACF56BA6867159A6FB93A7CDF_H
#ifndef SECT163R2POINT_T90099D0139F716B807FB2F557A3E4A5F614CFA1E_H
#define SECT163R2POINT_T90099D0139F716B807FB2F557A3E4A5F614CFA1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT163R2Point
struct  SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2POINT_T90099D0139F716B807FB2F557A3E4A5F614CFA1E_H
#ifndef SECT193R1POINT_T443B689F430D984CA469668CAF18B9FBE998118C_H
#define SECT193R1POINT_T443B689F430D984CA469668CAF18B9FBE998118C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R1Point
struct  SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1POINT_T443B689F430D984CA469668CAF18B9FBE998118C_H
#ifndef SECT193R2POINT_T313D01B5C9BA843B6E5FE6241BE87F194EEEE085_H
#define SECT193R2POINT_T313D01B5C9BA843B6E5FE6241BE87F194EEEE085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT193R2Point
struct  SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2POINT_T313D01B5C9BA843B6E5FE6241BE87F194EEEE085_H
#ifndef SECT233K1POINT_T3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6_H
#define SECT233K1POINT_T3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233K1Point
struct  SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1POINT_T3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6_H
#ifndef SECT233R1POINT_T16C3831AADF78757F7FA4A041DBC046CDB3AE0AB_H
#define SECT233R1POINT_T16C3831AADF78757F7FA4A041DBC046CDB3AE0AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT233R1Point
struct  SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1POINT_T16C3831AADF78757F7FA4A041DBC046CDB3AE0AB_H
#ifndef SECT239K1POINT_TC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5_H
#define SECT239K1POINT_TC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT239K1Point
struct  SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1POINT_TC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5_H
#ifndef SECT283K1POINT_TAAAC96757370D50BF0A74D5F0DA3D9F765545E9F_H
#define SECT283K1POINT_TAAAC96757370D50BF0A74D5F0DA3D9F765545E9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283K1Point
struct  SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1POINT_TAAAC96757370D50BF0A74D5F0DA3D9F765545E9F_H
#ifndef SECT283R1POINT_T70ECFD1E9F7D17358A84FDE9827A809E7AF5340F_H
#define SECT283R1POINT_T70ECFD1E9F7D17358A84FDE9827A809E7AF5340F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT283R1Point
struct  SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1POINT_T70ECFD1E9F7D17358A84FDE9827A809E7AF5340F_H
#ifndef SECT409K1POINT_T228439846BA230C91129B6BC0DBE38CB4F7D5FCC_H
#define SECT409K1POINT_T228439846BA230C91129B6BC0DBE38CB4F7D5FCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409K1Point
struct  SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1POINT_T228439846BA230C91129B6BC0DBE38CB4F7D5FCC_H
#ifndef SECT409R1POINT_T0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF_H
#define SECT409R1POINT_T0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT409R1Point
struct  SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1POINT_T0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF_H
#ifndef SECT571K1POINT_T94328F1B178C011C656FCB8FA3862BD5DCDEF31E_H
#define SECT571K1POINT_T94328F1B178C011C656FCB8FA3862BD5DCDEF31E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571K1Point
struct  SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1POINT_T94328F1B178C011C656FCB8FA3862BD5DCDEF31E_H
#ifndef SECT571R1POINT_T56BC129973EE78E2F0717244AE0982004455DFB9_H
#define SECT571R1POINT_T56BC129973EE78E2F0717244AE0982004455DFB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.Custom.Sec.SecT571R1Point
struct  SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9  : public AbstractF2mPoint_t16788EF8771AEC999392932B2C0237BC015F7FAF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1POINT_T56BC129973EE78E2F0717244AE0982004455DFB9_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { sizeof (SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4300[3] = 
{
	SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499::get_offset_of_m_outer_0(),
	SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499::get_offset_of_m_table_1(),
	SecP256R1LookupTable_tF1ADEBA814647310ED660A5B518A1026B752D499::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404), -1, sizeof(SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4301[4] = 
{
	SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404_StaticFields::get_offset_of_P_0(),
	SecP256R1Field_t777987681746F7EFB70D90D52B5D16605D9C5404_StaticFields::get_offset_of_PExt_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95), -1, sizeof(SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4302[2] = 
{
	SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95_StaticFields::get_offset_of_Q_0(),
	SecP256R1FieldElement_tE7F6784179B11DAD7316CFC49FF5899BBBF73C95::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (SecP256R1Point_t2873CC3090906CD40D0B5777D2995188BE0DB605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98), -1, sizeof(SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4304[5] = 
{
	SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98_StaticFields::get_offset_of_SECP384R1_AFFINE_ZS_19(),
	SecP384R1Curve_t62D73FB67A590441DA962052796B90B050AD6D98::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4305[3] = 
{
	SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205::get_offset_of_m_outer_0(),
	SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205::get_offset_of_m_table_1(),
	SecP384R1LookupTable_tBA54A565A48B2F6C72BEF484A700B5A4E90FA205::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D), -1, sizeof(SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4306[5] = 
{
	SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields::get_offset_of_P_0(),
	SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields::get_offset_of_PExt_1(),
	SecP384R1Field_tD28D46C1EE0D5F78B056408B9DCC051A9410FA8D_StaticFields::get_offset_of_PExtInv_2(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { sizeof (SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674), -1, sizeof(SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4307[2] = 
{
	SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674_StaticFields::get_offset_of_Q_0(),
	SecP384R1FieldElement_tA5A3421EEC3E3B0B5C99EAB345FE9E84F3915674::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (SecP384R1Point_t5C8A576EE763465857F886C9733AEE183A477D46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D), -1, sizeof(SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4309[5] = 
{
	SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D_StaticFields::get_offset_of_SECP521R1_AFFINE_ZS_19(),
	SecP521R1Curve_t2E791E526A31EEADCCC5272B2C843969253E1C8D::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4310[3] = 
{
	SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56::get_offset_of_m_outer_0(),
	SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56::get_offset_of_m_table_1(),
	SecP521R1LookupTable_t84D3C30F40D42BA8AD7EA064A1346C019D569B56::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (SecP521R1Field_tD47DE3BBE85069282736BC7A527BE128D1E665EE), -1, sizeof(SecP521R1Field_tD47DE3BBE85069282736BC7A527BE128D1E665EE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4311[2] = 
{
	SecP521R1Field_tD47DE3BBE85069282736BC7A527BE128D1E665EE_StaticFields::get_offset_of_P_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85), -1, sizeof(SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4312[2] = 
{
	SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85_StaticFields::get_offset_of_Q_0(),
	SecP521R1FieldElement_tB61FD67368C5AEE6775D29BE15EA17D1E1406A85::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { sizeof (SecP521R1Point_t48E344F511893049F33BEB321366FD97036DB933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (SecT113Field_t39CB2E686A85D53F7AC46B6B77992F9B82D60074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4314[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (SecT113FieldElement_t8E5FE908623CC2E303FB3A9630964A66FEEAC539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4315[1] = 
{
	SecT113FieldElement_t8E5FE908623CC2E303FB3A9630964A66FEEAC539::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385), -1, sizeof(SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4316[4] = 
{
	0,
	0,
	SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385_StaticFields::get_offset_of_SECT113R1_AFFINE_ZS_19(),
	SecT113R1Curve_t5D17AF7F2C812191214FD48EB62E6BCB384ED385::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4317[3] = 
{
	SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E::get_offset_of_m_outer_0(),
	SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E::get_offset_of_m_table_1(),
	SecT113R1LookupTable_t34EC181B90CBB854C81C733BDA013FEF458C670E::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { sizeof (SecT113R1Point_t1D2950F1043BAC5E04A5C93A6B36289A2BE7BEED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { sizeof (SecT113R2Curve_t991A5352277966895F4693704773720780531C59), -1, sizeof(SecT113R2Curve_t991A5352277966895F4693704773720780531C59_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4319[4] = 
{
	0,
	0,
	SecT113R2Curve_t991A5352277966895F4693704773720780531C59_StaticFields::get_offset_of_SECT113R2_AFFINE_ZS_19(),
	SecT113R2Curve_t991A5352277966895F4693704773720780531C59::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4320[3] = 
{
	SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F::get_offset_of_m_outer_0(),
	SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F::get_offset_of_m_table_1(),
	SecT113R2LookupTable_tA445175F15A788135D9B5078D8A7603F78D3466F::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (SecT113R2Point_t9C51575488F6581A310E4D3FFEF7F10C0288D336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (SecT131Field_t73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50), -1, sizeof(SecT131Field_t73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4322[3] = 
{
	0,
	0,
	SecT131Field_t73FAE180DC308CB2D3F5504DBC0EF9D8F2C52C50_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (SecT131FieldElement_t37F614744FFC569E5EC69970454E62884897783C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4323[1] = 
{
	SecT131FieldElement_t37F614744FFC569E5EC69970454E62884897783C::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { sizeof (SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071), -1, sizeof(SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4324[4] = 
{
	0,
	0,
	SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071_StaticFields::get_offset_of_SECT131R1_AFFINE_ZS_19(),
	SecT131R1Curve_tBD175320BA67E87BA3DC3611E21C9D3A2D521071::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4325[3] = 
{
	SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD::get_offset_of_m_outer_0(),
	SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD::get_offset_of_m_table_1(),
	SecT131R1LookupTable_tF90DE76FD64EB9C86CDCC47E854C6E02A1093FDD::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (SecT131R1Point_tACEEA5887AD487277586A2DC290CA9F001621C72), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { sizeof (SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B), -1, sizeof(SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4327[4] = 
{
	0,
	0,
	SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B_StaticFields::get_offset_of_SECT131R2_AFFINE_ZS_19(),
	SecT131R2Curve_tB5D7F72E243F92146EAC655525BA3832BDAC586B::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4328[3] = 
{
	SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A::get_offset_of_m_outer_0(),
	SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A::get_offset_of_m_table_1(),
	SecT131R2LookupTable_tF65C4067DA4BAF31A772DB03828DE3A1AF22207A::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { sizeof (SecT131R2Point_t919DF14637C366F25813B1AC7939FCCD2395311D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (SecT163Field_t5717E4757416F04979AFD82DF103E2A716196F08), -1, sizeof(SecT163Field_t5717E4757416F04979AFD82DF103E2A716196F08_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4330[3] = 
{
	0,
	0,
	SecT163Field_t5717E4757416F04979AFD82DF103E2A716196F08_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (SecT163FieldElement_tDDE60F03E634E9D984ADF05B4854908146D3D337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4331[1] = 
{
	SecT163FieldElement_tDDE60F03E634E9D984ADF05B4854908146D3D337::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5), -1, sizeof(SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4332[4] = 
{
	0,
	0,
	SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5_StaticFields::get_offset_of_SECT163K1_AFFINE_ZS_19(),
	SecT163K1Curve_t39705CD50AF9A62FBBAC2137BD7E68DD823258E5::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4333[3] = 
{
	SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9::get_offset_of_m_outer_0(),
	SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9::get_offset_of_m_table_1(),
	SecT163K1LookupTable_tED9153D29E0A01658A4D613265447878294D64F9::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (SecT163K1Point_t46566468BA4F027210DCB6B83062579C24817D9A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC), -1, sizeof(SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4335[4] = 
{
	0,
	0,
	SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC_StaticFields::get_offset_of_SECT163R1_AFFINE_ZS_19(),
	SecT163R1Curve_t99B8CCE49BB96C70621624833A61E7CD8FDF52AC::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4336[3] = 
{
	SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1::get_offset_of_m_outer_0(),
	SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1::get_offset_of_m_table_1(),
	SecT163R1LookupTable_tEB1A6E16343DBA7C1C1B8F2574A424B4470DF2F1::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { sizeof (SecT163R1Point_tB2112675B476672ACF56BA6867159A6FB93A7CDF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024), -1, sizeof(SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4338[4] = 
{
	0,
	0,
	SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024_StaticFields::get_offset_of_SECT163R2_AFFINE_ZS_19(),
	SecT163R2Curve_t5A9C954A36747DC843C5F50712B815F16B939024::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4339[3] = 
{
	SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D::get_offset_of_m_outer_0(),
	SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D::get_offset_of_m_table_1(),
	SecT163R2LookupTable_tC55E12024A381DABE0A93C04B098D4D61D897B5D::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { sizeof (SecT163R2Point_t90099D0139F716B807FB2F557A3E4A5F614CFA1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { sizeof (SecT193Field_tB08F01C274AEECE436C03D349B7792351880A5C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4341[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (SecT193FieldElement_tFB38FCAE5F9676251BFA7AA769AE82FCF90E0787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4342[1] = 
{
	SecT193FieldElement_tFB38FCAE5F9676251BFA7AA769AE82FCF90E0787::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930), -1, sizeof(SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4343[4] = 
{
	0,
	0,
	SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930_StaticFields::get_offset_of_SECT193R1_AFFINE_ZS_19(),
	SecT193R1Curve_t7D631AA5BDF0BCC51BA5649C80E5486835793930::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4344[3] = 
{
	SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762::get_offset_of_m_outer_0(),
	SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762::get_offset_of_m_table_1(),
	SecT193R1LookupTable_tFC0475A328ED88603E50DCFB7BAE6D4F4195C762::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (SecT193R1Point_t443B689F430D984CA469668CAF18B9FBE998118C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF), -1, sizeof(SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4346[4] = 
{
	0,
	0,
	SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF_StaticFields::get_offset_of_SECT193R2_AFFINE_ZS_19(),
	SecT193R2Curve_t13A8E045FF37BBA8125502D57DC0E60A4FB97BBF::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4347[3] = 
{
	SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508::get_offset_of_m_outer_0(),
	SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508::get_offset_of_m_table_1(),
	SecT193R2LookupTable_t7C6BD2FF13E1D9AB539D54B8AAA18AD5F4264508::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { sizeof (SecT193R2Point_t313D01B5C9BA843B6E5FE6241BE87F194EEEE085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (SecT233Field_t7D38FB325C7393A090A2ADD73BC07A1BB86564AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4349[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (SecT233FieldElement_t5554F76263187F66FF8B0F356BDD91E839CC0AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4350[1] = 
{
	SecT233FieldElement_t5554F76263187F66FF8B0F356BDD91E839CC0AF8::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB), -1, sizeof(SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4351[4] = 
{
	0,
	0,
	SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB_StaticFields::get_offset_of_SECT233K1_AFFINE_ZS_19(),
	SecT233K1Curve_t845E13E1B9C57436AE7E4ED3A07B8A1039303DCB::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4352[3] = 
{
	SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E::get_offset_of_m_outer_0(),
	SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E::get_offset_of_m_table_1(),
	SecT233K1LookupTable_t74D5E3A8FEF2A86717D5CFC4B73C1BCB7CD8F59E::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (SecT233K1Point_t3AECCA08AD0CED68CEF98325FD82C9E3A904D3A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2), -1, sizeof(SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4354[4] = 
{
	0,
	0,
	SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2_StaticFields::get_offset_of_SECT233R1_AFFINE_ZS_19(),
	SecT233R1Curve_t743FB00CE2B44DE0B303C200863CEC1BC68C64F2::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4355[3] = 
{
	SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4::get_offset_of_m_outer_0(),
	SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4::get_offset_of_m_table_1(),
	SecT233R1LookupTable_tC91F4A1BE3758E17803F7C299D3E9909F7C687C4::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (SecT233R1Point_t16C3831AADF78757F7FA4A041DBC046CDB3AE0AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (SecT239Field_t0D0545024FD6AE4E3C5327B8B8ADD672A04B8042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4357[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (SecT239FieldElement_tB51945D7AEC3011AA8E16E5157FF9E31027D766D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4358[1] = 
{
	SecT239FieldElement_tB51945D7AEC3011AA8E16E5157FF9E31027D766D::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F), -1, sizeof(SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4359[4] = 
{
	0,
	0,
	SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F_StaticFields::get_offset_of_SECT239K1_AFFINE_ZS_19(),
	SecT239K1Curve_t27ED97CAD9B4083E4A8B110C39D7EFD435B2782F::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4360[3] = 
{
	SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91::get_offset_of_m_outer_0(),
	SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91::get_offset_of_m_table_1(),
	SecT239K1LookupTable_tB3FDDBBED4B1CFF26C31EF7569B45AD822383D91::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (SecT239K1Point_tC402D70B7C2838C347F7A2412CEA1B5AE00EBCD5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (SecT283Field_t72503D5F62573E4D32BF8E3A69905BEC8718BF1A), -1, sizeof(SecT283Field_t72503D5F62573E4D32BF8E3A69905BEC8718BF1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4362[3] = 
{
	0,
	0,
	SecT283Field_t72503D5F62573E4D32BF8E3A69905BEC8718BF1A_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (SecT283FieldElement_t07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4363[1] = 
{
	SecT283FieldElement_t07B0D9FCDC82C00DE269D1DE2E203AE3546AFC42::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232), -1, sizeof(SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4364[4] = 
{
	0,
	0,
	SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232_StaticFields::get_offset_of_SECT283K1_AFFINE_ZS_19(),
	SecT283K1Curve_tACD44683C7893A61F1B1B35D24FB5BA8F5B2E232::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4365[3] = 
{
	SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8::get_offset_of_m_outer_0(),
	SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8::get_offset_of_m_table_1(),
	SecT283K1LookupTable_tC9B341115AABB6A4D7DD4051E26E6B2CC0AD1BC8::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (SecT283K1Point_tAAAC96757370D50BF0A74D5F0DA3D9F765545E9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31), -1, sizeof(SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4367[4] = 
{
	0,
	0,
	SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31_StaticFields::get_offset_of_SECT283R1_AFFINE_ZS_19(),
	SecT283R1Curve_t903572D0E728A53FC36A330F1832C04F534F1F31::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4368[3] = 
{
	SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649::get_offset_of_m_outer_0(),
	SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649::get_offset_of_m_table_1(),
	SecT283R1LookupTable_t6DB54E45178FABE871C7B957776BC82FEB05A649::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { sizeof (SecT283R1Point_t70ECFD1E9F7D17358A84FDE9827A809E7AF5340F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { sizeof (SecT409Field_t639E3B51B3736615E9DFC03F3A147BD5968E32E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4370[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { sizeof (SecT409FieldElement_t55D69B576761A80D7B5F806F9AED80F1CC3BA532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4371[1] = 
{
	SecT409FieldElement_t55D69B576761A80D7B5F806F9AED80F1CC3BA532::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { sizeof (SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE), -1, sizeof(SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4372[4] = 
{
	0,
	0,
	SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE_StaticFields::get_offset_of_SECT409K1_AFFINE_ZS_19(),
	SecT409K1Curve_t9C49EC543DFA4FAA87092AF5CD328F28D05A57FE::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { sizeof (SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4373[3] = 
{
	SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B::get_offset_of_m_outer_0(),
	SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B::get_offset_of_m_table_1(),
	SecT409K1LookupTable_tC1E1FDF5A4FD56C13DC88D9594B9A4A87316833B::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { sizeof (SecT409K1Point_t228439846BA230C91129B6BC0DBE38CB4F7D5FCC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444), -1, sizeof(SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4375[4] = 
{
	0,
	0,
	SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444_StaticFields::get_offset_of_SECT409R1_AFFINE_ZS_19(),
	SecT409R1Curve_t42145CA43B460CF77C6A293F76CD6E589B60D444::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4376[3] = 
{
	SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6::get_offset_of_m_outer_0(),
	SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6::get_offset_of_m_table_1(),
	SecT409R1LookupTable_t66C7A47E74C94BBE74CFA4E278E153020BB8E8E6::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { sizeof (SecT409R1Point_t0F6B226B227C73C9F7D0A8E7A7BA8BE1B90B38BF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (SecT571Field_t4192C1BFC09947758F3428476BDE2232ABB24341), -1, sizeof(SecT571Field_t4192C1BFC09947758F3428476BDE2232ABB24341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4378[3] = 
{
	0,
	0,
	SecT571Field_t4192C1BFC09947758F3428476BDE2232ABB24341_StaticFields::get_offset_of_ROOT_Z_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4379[1] = 
{
	SecT571FieldElement_t3F2B1330EEF0AAA4C6273FC95B330F7E2448F908::get_offset_of_x_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02), -1, sizeof(SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4380[4] = 
{
	0,
	0,
	SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02_StaticFields::get_offset_of_SECT571K1_AFFINE_ZS_19(),
	SecT571K1Curve_tF62A85C881A5B49478B3F5E8EA465119DB3C9C02::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4381[3] = 
{
	SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D::get_offset_of_m_outer_0(),
	SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D::get_offset_of_m_table_1(),
	SecT571K1LookupTable_tF1BAF0A06B7D069EA6CD6CE6BA907A79918F247D::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { sizeof (SecT571K1Point_t94328F1B178C011C656FCB8FA3862BD5DCDEF31E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544), -1, sizeof(SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4383[6] = 
{
	0,
	0,
	SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields::get_offset_of_SECT571R1_AFFINE_ZS_19(),
	SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544::get_offset_of_m_infinity_20(),
	SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields::get_offset_of_SecT571R1_B_21(),
	SecT571R1Curve_t0FDF48FA3BE020215779142613531578FD95B544_StaticFields::get_offset_of_SecT571R1_B_SQRT_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4384[3] = 
{
	SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C::get_offset_of_m_outer_0(),
	SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C::get_offset_of_m_table_1(),
	SecT571R1LookupTable_tDCF20699FF29EB94F30A489E6E823785D9C2138C::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { sizeof (SecT571R1Point_t56BC129973EE78E2F0717244AE0982004455DFB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { sizeof (SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6), -1, sizeof(SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4386[5] = 
{
	SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6_StaticFields::get_offset_of_q_16(),
	0,
	0,
	SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6_StaticFields::get_offset_of_SM2P256V1_AFFINE_ZS_19(),
	SM2P256V1Curve_tE0E86456EECD2D5F53A9362120C73383F4760DB6::get_offset_of_m_infinity_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { sizeof (SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4387[3] = 
{
	SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB::get_offset_of_m_outer_0(),
	SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB::get_offset_of_m_table_1(),
	SM2P256V1LookupTable_t310BF99D124BEF07A682980392310EBA2098A7CB::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { sizeof (SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697), -1, sizeof(SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4388[4] = 
{
	SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697_StaticFields::get_offset_of_P_0(),
	SM2P256V1Field_tE98827DCBE882E004724C89109F209F061D62697_StaticFields::get_offset_of_PExt_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { sizeof (SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E), -1, sizeof(SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4389[2] = 
{
	SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E_StaticFields::get_offset_of_Q_0(),
	SM2P256V1FieldElement_tCFE2889BC0189C9A2AB4423713702C63E752210E::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { sizeof (SM2P256V1Point_t618C60A49C1223B01F39D1EFC10EC26CA530AC04), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { sizeof (Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9), -1, sizeof(Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4391[7] = 
{
	Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields::get_offset_of_q_16(),
	Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields::get_offset_of_C_a_17(),
	Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields::get_offset_of_C_b_18(),
	0,
	0,
	Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9_StaticFields::get_offset_of_CURVE25519_AFFINE_ZS_21(),
	Curve25519_tE6BE7BA7458E061F3F69652F5DB610DD965173F9::get_offset_of_m_infinity_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { sizeof (Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4392[3] = 
{
	Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035::get_offset_of_m_outer_0(),
	Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035::get_offset_of_m_table_1(),
	Curve25519LookupTable_t04F7E6ADA0034687C42B8CEDC2B7AFC9F2338035::get_offset_of_m_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { sizeof (Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6), -1, sizeof(Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4393[4] = 
{
	Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6_StaticFields::get_offset_of_P_0(),
	0,
	Curve25519Field_tDD3476A938537689C252CA4FB0FCE26857D180B6_StaticFields::get_offset_of_PExt_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { sizeof (Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B), -1, sizeof(Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4394[3] = 
{
	Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B_StaticFields::get_offset_of_Q_0(),
	Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	Curve25519FieldElement_t193171FE385A3E6FFC36E10873E438A1B42EC58B::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (Curve25519Point_tE214CA050988B50C3EA62E8EDB3BEA0DBF88D957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (SimpleBigDecimal_tC6B179F4E0B6415BF526C871B2A278842974D167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4396[2] = 
{
	SimpleBigDecimal_tC6B179F4E0B6415BF526C871B2A278842974D167::get_offset_of_bigInt_0(),
	SimpleBigDecimal_tC6B179F4E0B6415BF526C871B2A278842974D167::get_offset_of_scale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580), -1, sizeof(Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4397[10] = 
{
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_MinusOne_0(),
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_MinusTwo_1(),
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_MinusThree_2(),
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_Four_3(),
	0,
	0,
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_Alpha0_6(),
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_Alpha0Tnaf_7(),
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_Alpha1_8(),
	Tnaf_t205143603BEF6691CF6DCE645EF5CE4EE8110580_StaticFields::get_offset_of_Alpha1Tnaf_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (ZTauElement_tFBE15C434250DCCDD6B61013AFD2D78309FFB613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4398[2] = 
{
	ZTauElement_tFBE15C434250DCCDD6B61013AFD2D78309FFB613::get_offset_of_u_0(),
	ZTauElement_tFBE15C434250DCCDD6B61013AFD2D78309FFB613::get_offset_of_v_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23), -1, sizeof(CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4399[4] = 
{
	CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23_StaticFields::get_offset_of_KeySizes_0(),
	CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23::get_offset_of_encryptionOID_1(),
	CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23::get_offset_of_keySize_2(),
	CmsContentEncryptorBuilder_tF4ECE9654844D752C05C7F359C87CCB037861A23::get_offset_of_helper_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
