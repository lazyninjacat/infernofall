﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject
struct Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated[]
struct DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String
struct DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerPrintableString
struct DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.DataGroupHash[]
struct DataGroupHashU5BU5D_t7F4EC94BDD487B6A664D565DAD66ABC67137D30C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsVersionInfo
struct LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority
struct NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID
struct CertID_tD10877523D531F998848FBD48D854308134F2918;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus
struct CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus
struct OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID
struct ResponderID_t26B99AFE9E4E05948990F461880598D304592346;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseBytes
struct ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData
struct ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Signature
struct Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest
struct TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo
struct CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo
struct ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo[]
struct ContentInfoU5BU5D_tF44A316B0082B249A8FEDDC4CF3B69A48FBBABED;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptionScheme
struct EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.IssuerAndSerialNumber
struct IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.KeyDerivationFunc
struct KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.MacData
struct MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString
struct DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlReason
struct CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo
struct DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial
struct IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure[]
struct X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.String
struct String_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef ICAOOBJECTIDENTIFIERS_T0165C8775248E294D497F5964B7E5C8494C6093F_H
#define ICAOOBJECTIDENTIFIERS_T0165C8775248E294D497F5964B7E5C8494C6093F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers
struct  IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F  : public RuntimeObject
{
public:

public:
};

struct IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcao
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcao_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoMrtd
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoMrtd_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoMrtdSecurity
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoMrtdSecurity_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoLdsSecurityObject
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoLdsSecurityObject_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoCscaMasterList
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoCscaMasterList_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoCscaMasterListSigningKey
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoCscaMasterListSigningKey_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoDocumentTypeList
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoDocumentTypeList_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoAAProtocolObject
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoAAProtocolObject_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoExtensions
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoExtensions_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.IcaoObjectIdentifiers::IdIcaoExtensionsNamechangekeyrollover
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIcaoExtensionsNamechangekeyrollover_9;

public:
	inline static int32_t get_offset_of_IdIcao_0() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcao_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcao_0() const { return ___IdIcao_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcao_0() { return &___IdIcao_0; }
	inline void set_IdIcao_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcao_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcao_0), value);
	}

	inline static int32_t get_offset_of_IdIcaoMrtd_1() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoMrtd_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoMrtd_1() const { return ___IdIcaoMrtd_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoMrtd_1() { return &___IdIcaoMrtd_1; }
	inline void set_IdIcaoMrtd_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoMrtd_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoMrtd_1), value);
	}

	inline static int32_t get_offset_of_IdIcaoMrtdSecurity_2() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoMrtdSecurity_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoMrtdSecurity_2() const { return ___IdIcaoMrtdSecurity_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoMrtdSecurity_2() { return &___IdIcaoMrtdSecurity_2; }
	inline void set_IdIcaoMrtdSecurity_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoMrtdSecurity_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoMrtdSecurity_2), value);
	}

	inline static int32_t get_offset_of_IdIcaoLdsSecurityObject_3() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoLdsSecurityObject_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoLdsSecurityObject_3() const { return ___IdIcaoLdsSecurityObject_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoLdsSecurityObject_3() { return &___IdIcaoLdsSecurityObject_3; }
	inline void set_IdIcaoLdsSecurityObject_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoLdsSecurityObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoLdsSecurityObject_3), value);
	}

	inline static int32_t get_offset_of_IdIcaoCscaMasterList_4() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoCscaMasterList_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoCscaMasterList_4() const { return ___IdIcaoCscaMasterList_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoCscaMasterList_4() { return &___IdIcaoCscaMasterList_4; }
	inline void set_IdIcaoCscaMasterList_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoCscaMasterList_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoCscaMasterList_4), value);
	}

	inline static int32_t get_offset_of_IdIcaoCscaMasterListSigningKey_5() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoCscaMasterListSigningKey_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoCscaMasterListSigningKey_5() const { return ___IdIcaoCscaMasterListSigningKey_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoCscaMasterListSigningKey_5() { return &___IdIcaoCscaMasterListSigningKey_5; }
	inline void set_IdIcaoCscaMasterListSigningKey_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoCscaMasterListSigningKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoCscaMasterListSigningKey_5), value);
	}

	inline static int32_t get_offset_of_IdIcaoDocumentTypeList_6() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoDocumentTypeList_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoDocumentTypeList_6() const { return ___IdIcaoDocumentTypeList_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoDocumentTypeList_6() { return &___IdIcaoDocumentTypeList_6; }
	inline void set_IdIcaoDocumentTypeList_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoDocumentTypeList_6 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoDocumentTypeList_6), value);
	}

	inline static int32_t get_offset_of_IdIcaoAAProtocolObject_7() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoAAProtocolObject_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoAAProtocolObject_7() const { return ___IdIcaoAAProtocolObject_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoAAProtocolObject_7() { return &___IdIcaoAAProtocolObject_7; }
	inline void set_IdIcaoAAProtocolObject_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoAAProtocolObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoAAProtocolObject_7), value);
	}

	inline static int32_t get_offset_of_IdIcaoExtensions_8() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoExtensions_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoExtensions_8() const { return ___IdIcaoExtensions_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoExtensions_8() { return &___IdIcaoExtensions_8; }
	inline void set_IdIcaoExtensions_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoExtensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoExtensions_8), value);
	}

	inline static int32_t get_offset_of_IdIcaoExtensionsNamechangekeyrollover_9() { return static_cast<int32_t>(offsetof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields, ___IdIcaoExtensionsNamechangekeyrollover_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIcaoExtensionsNamechangekeyrollover_9() const { return ___IdIcaoExtensionsNamechangekeyrollover_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIcaoExtensionsNamechangekeyrollover_9() { return &___IdIcaoExtensionsNamechangekeyrollover_9; }
	inline void set_IdIcaoExtensionsNamechangekeyrollover_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIcaoExtensionsNamechangekeyrollover_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdIcaoExtensionsNamechangekeyrollover_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICAOOBJECTIDENTIFIERS_T0165C8775248E294D497F5964B7E5C8494C6093F_H
#ifndef ISISMTTOBJECTIDENTIFIERS_T15A1A1C61A20D72DE84B26DF8855DA7B8349B308_H
#define ISISMTTOBJECTIDENTIFIERS_T15A1A1C61A20D72DE84B26DF8855DA7B8349B308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers
struct  IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308  : public RuntimeObject
{
public:

public:
};

struct IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMtt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMtt_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttCP
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttCP_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttCPAccredited
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttCPAccredited_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttAT
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttAT_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATDateOfCertGen
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATDateOfCertGen_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATProcuration
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATProcuration_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATAdmission
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATAdmission_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATMonetaryLimit
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATMonetaryLimit_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATDeclarationOfMajority
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATDeclarationOfMajority_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATIccsn
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATIccsn_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATPKReference
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATPKReference_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATRestriction
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATRestriction_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATRetrieveIfAllowed
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATRetrieveIfAllowed_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATRequestedCertificate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATRequestedCertificate_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATNamingAuthorities
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATNamingAuthorities_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATCertInDirSince
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATCertInDirSince_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATCertHash
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATCertHash_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATNameAtBirth
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATNameAtBirth_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATAdditionalInformation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATAdditionalInformation_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.IsisMttObjectIdentifiers::IdIsisMttATLiabilityLimitationFlag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATLiabilityLimitationFlag_19;

public:
	inline static int32_t get_offset_of_IdIsisMtt_0() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMtt_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMtt_0() const { return ___IdIsisMtt_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMtt_0() { return &___IdIsisMtt_0; }
	inline void set_IdIsisMtt_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMtt_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMtt_0), value);
	}

	inline static int32_t get_offset_of_IdIsisMttCP_1() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttCP_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttCP_1() const { return ___IdIsisMttCP_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttCP_1() { return &___IdIsisMttCP_1; }
	inline void set_IdIsisMttCP_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttCP_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttCP_1), value);
	}

	inline static int32_t get_offset_of_IdIsisMttCPAccredited_2() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttCPAccredited_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttCPAccredited_2() const { return ___IdIsisMttCPAccredited_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttCPAccredited_2() { return &___IdIsisMttCPAccredited_2; }
	inline void set_IdIsisMttCPAccredited_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttCPAccredited_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttCPAccredited_2), value);
	}

	inline static int32_t get_offset_of_IdIsisMttAT_3() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttAT_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttAT_3() const { return ___IdIsisMttAT_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttAT_3() { return &___IdIsisMttAT_3; }
	inline void set_IdIsisMttAT_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttAT_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttAT_3), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATDateOfCertGen_4() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATDateOfCertGen_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATDateOfCertGen_4() const { return ___IdIsisMttATDateOfCertGen_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATDateOfCertGen_4() { return &___IdIsisMttATDateOfCertGen_4; }
	inline void set_IdIsisMttATDateOfCertGen_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATDateOfCertGen_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATDateOfCertGen_4), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATProcuration_5() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATProcuration_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATProcuration_5() const { return ___IdIsisMttATProcuration_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATProcuration_5() { return &___IdIsisMttATProcuration_5; }
	inline void set_IdIsisMttATProcuration_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATProcuration_5 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATProcuration_5), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATAdmission_6() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATAdmission_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATAdmission_6() const { return ___IdIsisMttATAdmission_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATAdmission_6() { return &___IdIsisMttATAdmission_6; }
	inline void set_IdIsisMttATAdmission_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATAdmission_6 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATAdmission_6), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATMonetaryLimit_7() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATMonetaryLimit_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATMonetaryLimit_7() const { return ___IdIsisMttATMonetaryLimit_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATMonetaryLimit_7() { return &___IdIsisMttATMonetaryLimit_7; }
	inline void set_IdIsisMttATMonetaryLimit_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATMonetaryLimit_7 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATMonetaryLimit_7), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATDeclarationOfMajority_8() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATDeclarationOfMajority_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATDeclarationOfMajority_8() const { return ___IdIsisMttATDeclarationOfMajority_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATDeclarationOfMajority_8() { return &___IdIsisMttATDeclarationOfMajority_8; }
	inline void set_IdIsisMttATDeclarationOfMajority_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATDeclarationOfMajority_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATDeclarationOfMajority_8), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATIccsn_9() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATIccsn_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATIccsn_9() const { return ___IdIsisMttATIccsn_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATIccsn_9() { return &___IdIsisMttATIccsn_9; }
	inline void set_IdIsisMttATIccsn_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATIccsn_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATIccsn_9), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATPKReference_10() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATPKReference_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATPKReference_10() const { return ___IdIsisMttATPKReference_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATPKReference_10() { return &___IdIsisMttATPKReference_10; }
	inline void set_IdIsisMttATPKReference_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATPKReference_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATPKReference_10), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATRestriction_11() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATRestriction_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATRestriction_11() const { return ___IdIsisMttATRestriction_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATRestriction_11() { return &___IdIsisMttATRestriction_11; }
	inline void set_IdIsisMttATRestriction_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATRestriction_11 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATRestriction_11), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATRetrieveIfAllowed_12() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATRetrieveIfAllowed_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATRetrieveIfAllowed_12() const { return ___IdIsisMttATRetrieveIfAllowed_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATRetrieveIfAllowed_12() { return &___IdIsisMttATRetrieveIfAllowed_12; }
	inline void set_IdIsisMttATRetrieveIfAllowed_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATRetrieveIfAllowed_12 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATRetrieveIfAllowed_12), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATRequestedCertificate_13() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATRequestedCertificate_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATRequestedCertificate_13() const { return ___IdIsisMttATRequestedCertificate_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATRequestedCertificate_13() { return &___IdIsisMttATRequestedCertificate_13; }
	inline void set_IdIsisMttATRequestedCertificate_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATRequestedCertificate_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATRequestedCertificate_13), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATNamingAuthorities_14() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATNamingAuthorities_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATNamingAuthorities_14() const { return ___IdIsisMttATNamingAuthorities_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATNamingAuthorities_14() { return &___IdIsisMttATNamingAuthorities_14; }
	inline void set_IdIsisMttATNamingAuthorities_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATNamingAuthorities_14 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATNamingAuthorities_14), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATCertInDirSince_15() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATCertInDirSince_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATCertInDirSince_15() const { return ___IdIsisMttATCertInDirSince_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATCertInDirSince_15() { return &___IdIsisMttATCertInDirSince_15; }
	inline void set_IdIsisMttATCertInDirSince_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATCertInDirSince_15 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATCertInDirSince_15), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATCertHash_16() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATCertHash_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATCertHash_16() const { return ___IdIsisMttATCertHash_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATCertHash_16() { return &___IdIsisMttATCertHash_16; }
	inline void set_IdIsisMttATCertHash_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATCertHash_16 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATCertHash_16), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATNameAtBirth_17() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATNameAtBirth_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATNameAtBirth_17() const { return ___IdIsisMttATNameAtBirth_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATNameAtBirth_17() { return &___IdIsisMttATNameAtBirth_17; }
	inline void set_IdIsisMttATNameAtBirth_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATNameAtBirth_17 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATNameAtBirth_17), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATAdditionalInformation_18() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATAdditionalInformation_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATAdditionalInformation_18() const { return ___IdIsisMttATAdditionalInformation_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATAdditionalInformation_18() { return &___IdIsisMttATAdditionalInformation_18; }
	inline void set_IdIsisMttATAdditionalInformation_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATAdditionalInformation_18 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATAdditionalInformation_18), value);
	}

	inline static int32_t get_offset_of_IdIsisMttATLiabilityLimitationFlag_19() { return static_cast<int32_t>(offsetof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields, ___IdIsisMttATLiabilityLimitationFlag_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATLiabilityLimitationFlag_19() const { return ___IdIsisMttATLiabilityLimitationFlag_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATLiabilityLimitationFlag_19() { return &___IdIsisMttATLiabilityLimitationFlag_19; }
	inline void set_IdIsisMttATLiabilityLimitationFlag_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATLiabilityLimitationFlag_19 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATLiabilityLimitationFlag_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISISMTTOBJECTIDENTIFIERS_T15A1A1C61A20D72DE84B26DF8855DA7B8349B308_H
#ifndef KISAOBJECTIDENTIFIERS_TD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_H
#define KISAOBJECTIDENTIFIERS_TD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Kisa.KisaObjectIdentifiers
struct  KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42  : public RuntimeObject
{
public:

public:
};

struct KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Kisa.KisaObjectIdentifiers::IdSeedCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSeedCbc_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Kisa.KisaObjectIdentifiers::IdNpkiAppCmsSeedWrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdNpkiAppCmsSeedWrap_1;

public:
	inline static int32_t get_offset_of_IdSeedCbc_0() { return static_cast<int32_t>(offsetof(KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_StaticFields, ___IdSeedCbc_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSeedCbc_0() const { return ___IdSeedCbc_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSeedCbc_0() { return &___IdSeedCbc_0; }
	inline void set_IdSeedCbc_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSeedCbc_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdSeedCbc_0), value);
	}

	inline static int32_t get_offset_of_IdNpkiAppCmsSeedWrap_1() { return static_cast<int32_t>(offsetof(KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_StaticFields, ___IdNpkiAppCmsSeedWrap_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdNpkiAppCmsSeedWrap_1() const { return ___IdNpkiAppCmsSeedWrap_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdNpkiAppCmsSeedWrap_1() { return &___IdNpkiAppCmsSeedWrap_1; }
	inline void set_IdNpkiAppCmsSeedWrap_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdNpkiAppCmsSeedWrap_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdNpkiAppCmsSeedWrap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KISAOBJECTIDENTIFIERS_TD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_H
#ifndef MICROSOFTOBJECTIDENTIFIERS_TFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_H
#define MICROSOFTOBJECTIDENTIFIERS_TFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers
struct  MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA  : public RuntimeObject
{
public:

public:
};

struct MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::Microsoft
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Microsoft_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::MicrosoftCertTemplateV1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MicrosoftCertTemplateV1_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::MicrosoftCAVersion
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MicrosoftCAVersion_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::MicrosoftPrevCACertHash
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MicrosoftPrevCACertHash_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::MicrosoftCrlNextPublish
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MicrosoftCrlNextPublish_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::MicrosoftCertTemplateV2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MicrosoftCertTemplateV2_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Microsoft.MicrosoftObjectIdentifiers::MicrosoftAppPolicies
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MicrosoftAppPolicies_6;

public:
	inline static int32_t get_offset_of_Microsoft_0() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___Microsoft_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Microsoft_0() const { return ___Microsoft_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Microsoft_0() { return &___Microsoft_0; }
	inline void set_Microsoft_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Microsoft_0 = value;
		Il2CppCodeGenWriteBarrier((&___Microsoft_0), value);
	}

	inline static int32_t get_offset_of_MicrosoftCertTemplateV1_1() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___MicrosoftCertTemplateV1_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MicrosoftCertTemplateV1_1() const { return ___MicrosoftCertTemplateV1_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MicrosoftCertTemplateV1_1() { return &___MicrosoftCertTemplateV1_1; }
	inline void set_MicrosoftCertTemplateV1_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MicrosoftCertTemplateV1_1 = value;
		Il2CppCodeGenWriteBarrier((&___MicrosoftCertTemplateV1_1), value);
	}

	inline static int32_t get_offset_of_MicrosoftCAVersion_2() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___MicrosoftCAVersion_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MicrosoftCAVersion_2() const { return ___MicrosoftCAVersion_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MicrosoftCAVersion_2() { return &___MicrosoftCAVersion_2; }
	inline void set_MicrosoftCAVersion_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MicrosoftCAVersion_2 = value;
		Il2CppCodeGenWriteBarrier((&___MicrosoftCAVersion_2), value);
	}

	inline static int32_t get_offset_of_MicrosoftPrevCACertHash_3() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___MicrosoftPrevCACertHash_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MicrosoftPrevCACertHash_3() const { return ___MicrosoftPrevCACertHash_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MicrosoftPrevCACertHash_3() { return &___MicrosoftPrevCACertHash_3; }
	inline void set_MicrosoftPrevCACertHash_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MicrosoftPrevCACertHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___MicrosoftPrevCACertHash_3), value);
	}

	inline static int32_t get_offset_of_MicrosoftCrlNextPublish_4() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___MicrosoftCrlNextPublish_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MicrosoftCrlNextPublish_4() const { return ___MicrosoftCrlNextPublish_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MicrosoftCrlNextPublish_4() { return &___MicrosoftCrlNextPublish_4; }
	inline void set_MicrosoftCrlNextPublish_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MicrosoftCrlNextPublish_4 = value;
		Il2CppCodeGenWriteBarrier((&___MicrosoftCrlNextPublish_4), value);
	}

	inline static int32_t get_offset_of_MicrosoftCertTemplateV2_5() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___MicrosoftCertTemplateV2_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MicrosoftCertTemplateV2_5() const { return ___MicrosoftCertTemplateV2_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MicrosoftCertTemplateV2_5() { return &___MicrosoftCertTemplateV2_5; }
	inline void set_MicrosoftCertTemplateV2_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MicrosoftCertTemplateV2_5 = value;
		Il2CppCodeGenWriteBarrier((&___MicrosoftCertTemplateV2_5), value);
	}

	inline static int32_t get_offset_of_MicrosoftAppPolicies_6() { return static_cast<int32_t>(offsetof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields, ___MicrosoftAppPolicies_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MicrosoftAppPolicies_6() const { return ___MicrosoftAppPolicies_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MicrosoftAppPolicies_6() { return &___MicrosoftAppPolicies_6; }
	inline void set_MicrosoftAppPolicies_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MicrosoftAppPolicies_6 = value;
		Il2CppCodeGenWriteBarrier((&___MicrosoftAppPolicies_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROSOFTOBJECTIDENTIFIERS_TFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_H
#ifndef MISCOBJECTIDENTIFIERS_T434A9E0801266F7EF5B8236EDB06B959A0C7896A_H
#define MISCOBJECTIDENTIFIERS_T434A9E0801266F7EF5B8236EDB06B959A0C7896A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers
struct  MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A  : public RuntimeObject
{
public:

public:
};

struct MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Netscape
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Netscape_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCertType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeCertType_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeBaseUrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeBaseUrl_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeRevocationUrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeRevocationUrl_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCARevocationUrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeCARevocationUrl_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeRenewalUrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeRenewalUrl_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCAPolicyUrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeCAPolicyUrl_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeSslServerName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeSslServerName_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NetscapeCertComment
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NetscapeCertComment_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Verisign
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Verisign_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignCzagExtension
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VerisignCzagExtension_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignPrivate_6_9
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VerisignPrivate_6_9_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignOnSiteJurisdictionHash
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VerisignOnSiteJurisdictionHash_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignBitString_6_13
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VerisignBitString_6_13_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignDnbDunsNumber
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VerisignDnbDunsNumber_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::VerisignIssStrongCrypto
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VerisignIssStrongCrypto_15;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Novell
	String_t* ___Novell_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::NovellSecurityAttribs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NovellSecurityAttribs_17;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::Entrust
	String_t* ___Entrust_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::EntrustVersionExtension
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EntrustVersionExtension_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cast5CBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cast5CBC_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::as_sys_sec_alg_ideaCBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___as_sys_sec_alg_ideaCBC_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cryptlib_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cryptlib_algorithm_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_ECB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cryptlib_algorithm_blowfish_ECB_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_CBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cryptlib_algorithm_blowfish_CBC_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_CFB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cryptlib_algorithm_blowfish_CFB_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::cryptlib_algorithm_blowfish_OFB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___cryptlib_algorithm_blowfish_OFB_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::blake2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___blake2_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2b160_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2b256_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2b384_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2b512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2b512_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2s128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2s128_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2s160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2s160_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2s224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2s224_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_blake2s256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_blake2s256_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.MiscObjectIdentifiers::id_scrypt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_scrypt_37;

public:
	inline static int32_t get_offset_of_Netscape_0() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___Netscape_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Netscape_0() const { return ___Netscape_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Netscape_0() { return &___Netscape_0; }
	inline void set_Netscape_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Netscape_0 = value;
		Il2CppCodeGenWriteBarrier((&___Netscape_0), value);
	}

	inline static int32_t get_offset_of_NetscapeCertType_1() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeCertType_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeCertType_1() const { return ___NetscapeCertType_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeCertType_1() { return &___NetscapeCertType_1; }
	inline void set_NetscapeCertType_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeCertType_1 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCertType_1), value);
	}

	inline static int32_t get_offset_of_NetscapeBaseUrl_2() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeBaseUrl_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeBaseUrl_2() const { return ___NetscapeBaseUrl_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeBaseUrl_2() { return &___NetscapeBaseUrl_2; }
	inline void set_NetscapeBaseUrl_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeBaseUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeBaseUrl_2), value);
	}

	inline static int32_t get_offset_of_NetscapeRevocationUrl_3() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeRevocationUrl_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeRevocationUrl_3() const { return ___NetscapeRevocationUrl_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeRevocationUrl_3() { return &___NetscapeRevocationUrl_3; }
	inline void set_NetscapeRevocationUrl_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeRevocationUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeRevocationUrl_3), value);
	}

	inline static int32_t get_offset_of_NetscapeCARevocationUrl_4() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeCARevocationUrl_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeCARevocationUrl_4() const { return ___NetscapeCARevocationUrl_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeCARevocationUrl_4() { return &___NetscapeCARevocationUrl_4; }
	inline void set_NetscapeCARevocationUrl_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeCARevocationUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCARevocationUrl_4), value);
	}

	inline static int32_t get_offset_of_NetscapeRenewalUrl_5() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeRenewalUrl_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeRenewalUrl_5() const { return ___NetscapeRenewalUrl_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeRenewalUrl_5() { return &___NetscapeRenewalUrl_5; }
	inline void set_NetscapeRenewalUrl_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeRenewalUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeRenewalUrl_5), value);
	}

	inline static int32_t get_offset_of_NetscapeCAPolicyUrl_6() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeCAPolicyUrl_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeCAPolicyUrl_6() const { return ___NetscapeCAPolicyUrl_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeCAPolicyUrl_6() { return &___NetscapeCAPolicyUrl_6; }
	inline void set_NetscapeCAPolicyUrl_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeCAPolicyUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCAPolicyUrl_6), value);
	}

	inline static int32_t get_offset_of_NetscapeSslServerName_7() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeSslServerName_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeSslServerName_7() const { return ___NetscapeSslServerName_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeSslServerName_7() { return &___NetscapeSslServerName_7; }
	inline void set_NetscapeSslServerName_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeSslServerName_7 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeSslServerName_7), value);
	}

	inline static int32_t get_offset_of_NetscapeCertComment_8() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NetscapeCertComment_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NetscapeCertComment_8() const { return ___NetscapeCertComment_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NetscapeCertComment_8() { return &___NetscapeCertComment_8; }
	inline void set_NetscapeCertComment_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NetscapeCertComment_8 = value;
		Il2CppCodeGenWriteBarrier((&___NetscapeCertComment_8), value);
	}

	inline static int32_t get_offset_of_Verisign_9() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___Verisign_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Verisign_9() const { return ___Verisign_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Verisign_9() { return &___Verisign_9; }
	inline void set_Verisign_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Verisign_9 = value;
		Il2CppCodeGenWriteBarrier((&___Verisign_9), value);
	}

	inline static int32_t get_offset_of_VerisignCzagExtension_10() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___VerisignCzagExtension_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VerisignCzagExtension_10() const { return ___VerisignCzagExtension_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VerisignCzagExtension_10() { return &___VerisignCzagExtension_10; }
	inline void set_VerisignCzagExtension_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VerisignCzagExtension_10 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignCzagExtension_10), value);
	}

	inline static int32_t get_offset_of_VerisignPrivate_6_9_11() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___VerisignPrivate_6_9_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VerisignPrivate_6_9_11() const { return ___VerisignPrivate_6_9_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VerisignPrivate_6_9_11() { return &___VerisignPrivate_6_9_11; }
	inline void set_VerisignPrivate_6_9_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VerisignPrivate_6_9_11 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignPrivate_6_9_11), value);
	}

	inline static int32_t get_offset_of_VerisignOnSiteJurisdictionHash_12() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___VerisignOnSiteJurisdictionHash_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VerisignOnSiteJurisdictionHash_12() const { return ___VerisignOnSiteJurisdictionHash_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VerisignOnSiteJurisdictionHash_12() { return &___VerisignOnSiteJurisdictionHash_12; }
	inline void set_VerisignOnSiteJurisdictionHash_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VerisignOnSiteJurisdictionHash_12 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignOnSiteJurisdictionHash_12), value);
	}

	inline static int32_t get_offset_of_VerisignBitString_6_13_13() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___VerisignBitString_6_13_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VerisignBitString_6_13_13() const { return ___VerisignBitString_6_13_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VerisignBitString_6_13_13() { return &___VerisignBitString_6_13_13; }
	inline void set_VerisignBitString_6_13_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VerisignBitString_6_13_13 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignBitString_6_13_13), value);
	}

	inline static int32_t get_offset_of_VerisignDnbDunsNumber_14() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___VerisignDnbDunsNumber_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VerisignDnbDunsNumber_14() const { return ___VerisignDnbDunsNumber_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VerisignDnbDunsNumber_14() { return &___VerisignDnbDunsNumber_14; }
	inline void set_VerisignDnbDunsNumber_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VerisignDnbDunsNumber_14 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignDnbDunsNumber_14), value);
	}

	inline static int32_t get_offset_of_VerisignIssStrongCrypto_15() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___VerisignIssStrongCrypto_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VerisignIssStrongCrypto_15() const { return ___VerisignIssStrongCrypto_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VerisignIssStrongCrypto_15() { return &___VerisignIssStrongCrypto_15; }
	inline void set_VerisignIssStrongCrypto_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VerisignIssStrongCrypto_15 = value;
		Il2CppCodeGenWriteBarrier((&___VerisignIssStrongCrypto_15), value);
	}

	inline static int32_t get_offset_of_Novell_16() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___Novell_16)); }
	inline String_t* get_Novell_16() const { return ___Novell_16; }
	inline String_t** get_address_of_Novell_16() { return &___Novell_16; }
	inline void set_Novell_16(String_t* value)
	{
		___Novell_16 = value;
		Il2CppCodeGenWriteBarrier((&___Novell_16), value);
	}

	inline static int32_t get_offset_of_NovellSecurityAttribs_17() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___NovellSecurityAttribs_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NovellSecurityAttribs_17() const { return ___NovellSecurityAttribs_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NovellSecurityAttribs_17() { return &___NovellSecurityAttribs_17; }
	inline void set_NovellSecurityAttribs_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NovellSecurityAttribs_17 = value;
		Il2CppCodeGenWriteBarrier((&___NovellSecurityAttribs_17), value);
	}

	inline static int32_t get_offset_of_Entrust_18() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___Entrust_18)); }
	inline String_t* get_Entrust_18() const { return ___Entrust_18; }
	inline String_t** get_address_of_Entrust_18() { return &___Entrust_18; }
	inline void set_Entrust_18(String_t* value)
	{
		___Entrust_18 = value;
		Il2CppCodeGenWriteBarrier((&___Entrust_18), value);
	}

	inline static int32_t get_offset_of_EntrustVersionExtension_19() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___EntrustVersionExtension_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EntrustVersionExtension_19() const { return ___EntrustVersionExtension_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EntrustVersionExtension_19() { return &___EntrustVersionExtension_19; }
	inline void set_EntrustVersionExtension_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EntrustVersionExtension_19 = value;
		Il2CppCodeGenWriteBarrier((&___EntrustVersionExtension_19), value);
	}

	inline static int32_t get_offset_of_cast5CBC_20() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cast5CBC_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cast5CBC_20() const { return ___cast5CBC_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cast5CBC_20() { return &___cast5CBC_20; }
	inline void set_cast5CBC_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cast5CBC_20 = value;
		Il2CppCodeGenWriteBarrier((&___cast5CBC_20), value);
	}

	inline static int32_t get_offset_of_as_sys_sec_alg_ideaCBC_21() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___as_sys_sec_alg_ideaCBC_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_as_sys_sec_alg_ideaCBC_21() const { return ___as_sys_sec_alg_ideaCBC_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_as_sys_sec_alg_ideaCBC_21() { return &___as_sys_sec_alg_ideaCBC_21; }
	inline void set_as_sys_sec_alg_ideaCBC_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___as_sys_sec_alg_ideaCBC_21 = value;
		Il2CppCodeGenWriteBarrier((&___as_sys_sec_alg_ideaCBC_21), value);
	}

	inline static int32_t get_offset_of_cryptlib_22() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cryptlib_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cryptlib_22() const { return ___cryptlib_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cryptlib_22() { return &___cryptlib_22; }
	inline void set_cryptlib_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cryptlib_22 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_22), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_23() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cryptlib_algorithm_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cryptlib_algorithm_23() const { return ___cryptlib_algorithm_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cryptlib_algorithm_23() { return &___cryptlib_algorithm_23; }
	inline void set_cryptlib_algorithm_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cryptlib_algorithm_23 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_23), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_ECB_24() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cryptlib_algorithm_blowfish_ECB_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cryptlib_algorithm_blowfish_ECB_24() const { return ___cryptlib_algorithm_blowfish_ECB_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cryptlib_algorithm_blowfish_ECB_24() { return &___cryptlib_algorithm_blowfish_ECB_24; }
	inline void set_cryptlib_algorithm_blowfish_ECB_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cryptlib_algorithm_blowfish_ECB_24 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_ECB_24), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_CBC_25() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cryptlib_algorithm_blowfish_CBC_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cryptlib_algorithm_blowfish_CBC_25() const { return ___cryptlib_algorithm_blowfish_CBC_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cryptlib_algorithm_blowfish_CBC_25() { return &___cryptlib_algorithm_blowfish_CBC_25; }
	inline void set_cryptlib_algorithm_blowfish_CBC_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cryptlib_algorithm_blowfish_CBC_25 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_CBC_25), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_CFB_26() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cryptlib_algorithm_blowfish_CFB_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cryptlib_algorithm_blowfish_CFB_26() const { return ___cryptlib_algorithm_blowfish_CFB_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cryptlib_algorithm_blowfish_CFB_26() { return &___cryptlib_algorithm_blowfish_CFB_26; }
	inline void set_cryptlib_algorithm_blowfish_CFB_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cryptlib_algorithm_blowfish_CFB_26 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_CFB_26), value);
	}

	inline static int32_t get_offset_of_cryptlib_algorithm_blowfish_OFB_27() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___cryptlib_algorithm_blowfish_OFB_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_cryptlib_algorithm_blowfish_OFB_27() const { return ___cryptlib_algorithm_blowfish_OFB_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_cryptlib_algorithm_blowfish_OFB_27() { return &___cryptlib_algorithm_blowfish_OFB_27; }
	inline void set_cryptlib_algorithm_blowfish_OFB_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___cryptlib_algorithm_blowfish_OFB_27 = value;
		Il2CppCodeGenWriteBarrier((&___cryptlib_algorithm_blowfish_OFB_27), value);
	}

	inline static int32_t get_offset_of_blake2_28() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___blake2_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_blake2_28() const { return ___blake2_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_blake2_28() { return &___blake2_28; }
	inline void set_blake2_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___blake2_28 = value;
		Il2CppCodeGenWriteBarrier((&___blake2_28), value);
	}

	inline static int32_t get_offset_of_id_blake2b160_29() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2b160_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2b160_29() const { return ___id_blake2b160_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2b160_29() { return &___id_blake2b160_29; }
	inline void set_id_blake2b160_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2b160_29 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b160_29), value);
	}

	inline static int32_t get_offset_of_id_blake2b256_30() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2b256_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2b256_30() const { return ___id_blake2b256_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2b256_30() { return &___id_blake2b256_30; }
	inline void set_id_blake2b256_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2b256_30 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b256_30), value);
	}

	inline static int32_t get_offset_of_id_blake2b384_31() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2b384_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2b384_31() const { return ___id_blake2b384_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2b384_31() { return &___id_blake2b384_31; }
	inline void set_id_blake2b384_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2b384_31 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b384_31), value);
	}

	inline static int32_t get_offset_of_id_blake2b512_32() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2b512_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2b512_32() const { return ___id_blake2b512_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2b512_32() { return &___id_blake2b512_32; }
	inline void set_id_blake2b512_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2b512_32 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2b512_32), value);
	}

	inline static int32_t get_offset_of_id_blake2s128_33() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2s128_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2s128_33() const { return ___id_blake2s128_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2s128_33() { return &___id_blake2s128_33; }
	inline void set_id_blake2s128_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2s128_33 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2s128_33), value);
	}

	inline static int32_t get_offset_of_id_blake2s160_34() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2s160_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2s160_34() const { return ___id_blake2s160_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2s160_34() { return &___id_blake2s160_34; }
	inline void set_id_blake2s160_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2s160_34 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2s160_34), value);
	}

	inline static int32_t get_offset_of_id_blake2s224_35() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2s224_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2s224_35() const { return ___id_blake2s224_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2s224_35() { return &___id_blake2s224_35; }
	inline void set_id_blake2s224_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2s224_35 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2s224_35), value);
	}

	inline static int32_t get_offset_of_id_blake2s256_36() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_blake2s256_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_blake2s256_36() const { return ___id_blake2s256_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_blake2s256_36() { return &___id_blake2s256_36; }
	inline void set_id_blake2s256_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_blake2s256_36 = value;
		Il2CppCodeGenWriteBarrier((&___id_blake2s256_36), value);
	}

	inline static int32_t get_offset_of_id_scrypt_37() { return static_cast<int32_t>(offsetof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields, ___id_scrypt_37)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_scrypt_37() const { return ___id_scrypt_37; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_scrypt_37() { return &___id_scrypt_37; }
	inline void set_id_scrypt_37(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_scrypt_37 = value;
		Il2CppCodeGenWriteBarrier((&___id_scrypt_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCOBJECTIDENTIFIERS_T434A9E0801266F7EF5B8236EDB06B959A0C7896A_H
#ifndef NISTNAMEDCURVES_TD0C397D70281A0EDD03CD5DAEF2CA33554317344_H
#define NISTNAMEDCURVES_TD0C397D70281A0EDD03CD5DAEF2CA33554317344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistNamedCurves
struct  NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344  : public RuntimeObject
{
public:

public:
};

struct NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistNamedCurves::names
	RuntimeObject* ___names_1;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_names_1() { return static_cast<int32_t>(offsetof(NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344_StaticFields, ___names_1)); }
	inline RuntimeObject* get_names_1() const { return ___names_1; }
	inline RuntimeObject** get_address_of_names_1() { return &___names_1; }
	inline void set_names_1(RuntimeObject* value)
	{
		___names_1 = value;
		Il2CppCodeGenWriteBarrier((&___names_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NISTNAMEDCURVES_TD0C397D70281A0EDD03CD5DAEF2CA33554317344_H
#ifndef NISTOBJECTIDENTIFIERS_T5946473F810F7AF197B5FEF06B8AD010DA965C2A_H
#define NISTOBJECTIDENTIFIERS_T5946473F810F7AF197B5FEF06B8AD010DA965C2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers
struct  NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A  : public RuntimeObject
{
public:

public:
};

struct NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::NistAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___NistAlgorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::HashAlgs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___HashAlgs_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha256_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha384_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha512_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha224_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha512_224_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha512_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha512_256_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha3_224_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha3_256_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha3_384_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdSha3_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha3_512_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdShake128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdShake128_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdShake256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdShake256_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdHMacWithSha3_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHMacWithSha3_224_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdHMacWithSha3_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHMacWithSha3_256_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdHMacWithSha3_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHMacWithSha3_384_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdHMacWithSha3_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHMacWithSha3_512_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::Aes
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Aes_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Ecb_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Cbc_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ofb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Ofb_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Cfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Cfb_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Wrap_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Gcm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Gcm_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes128Ccm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes128Ccm_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Ecb_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Cbc_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ofb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Ofb_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Cfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Cfb_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Wrap_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Gcm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Gcm_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes192Ccm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes192Ccm_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Ecb_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Cbc_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ofb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Ofb_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Cfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Cfb_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Wrap_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Gcm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Gcm_38;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdAes256Ccm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAes256Ccm_39;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsaWithSha2_40;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DsaWithSha224_41;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DsaWithSha256_42;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DsaWithSha384_43;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::DsaWithSha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DsaWithSha512_44;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha3_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsaWithSha3_224_45;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha3_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsaWithSha3_256_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha3_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsaWithSha3_384_47;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdDsaWithSha3_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdDsaWithSha3_512_48;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdEcdsaWithSha3_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEcdsaWithSha3_224_49;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdEcdsaWithSha3_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEcdsaWithSha3_256_50;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdEcdsaWithSha3_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEcdsaWithSha3_384_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdEcdsaWithSha3_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdEcdsaWithSha3_512_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdRsassaPkcs1V15WithSha3_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsassaPkcs1V15WithSha3_224_53;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdRsassaPkcs1V15WithSha3_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsassaPkcs1V15WithSha3_256_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdRsassaPkcs1V15WithSha3_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsassaPkcs1V15WithSha3_384_55;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers::IdRsassaPkcs1V15WithSha3_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsassaPkcs1V15WithSha3_512_56;

public:
	inline static int32_t get_offset_of_NistAlgorithm_0() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___NistAlgorithm_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_NistAlgorithm_0() const { return ___NistAlgorithm_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_NistAlgorithm_0() { return &___NistAlgorithm_0; }
	inline void set_NistAlgorithm_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___NistAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___NistAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_HashAlgs_1() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___HashAlgs_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_HashAlgs_1() const { return ___HashAlgs_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_HashAlgs_1() { return &___HashAlgs_1; }
	inline void set_HashAlgs_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___HashAlgs_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashAlgs_1), value);
	}

	inline static int32_t get_offset_of_IdSha256_2() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha256_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha256_2() const { return ___IdSha256_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha256_2() { return &___IdSha256_2; }
	inline void set_IdSha256_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha256_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha256_2), value);
	}

	inline static int32_t get_offset_of_IdSha384_3() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha384_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha384_3() const { return ___IdSha384_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha384_3() { return &___IdSha384_3; }
	inline void set_IdSha384_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha384_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha384_3), value);
	}

	inline static int32_t get_offset_of_IdSha512_4() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha512_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha512_4() const { return ___IdSha512_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha512_4() { return &___IdSha512_4; }
	inline void set_IdSha512_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha512_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha512_4), value);
	}

	inline static int32_t get_offset_of_IdSha224_5() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha224_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha224_5() const { return ___IdSha224_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha224_5() { return &___IdSha224_5; }
	inline void set_IdSha224_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha224_5 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha224_5), value);
	}

	inline static int32_t get_offset_of_IdSha512_224_6() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha512_224_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha512_224_6() const { return ___IdSha512_224_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha512_224_6() { return &___IdSha512_224_6; }
	inline void set_IdSha512_224_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha512_224_6 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha512_224_6), value);
	}

	inline static int32_t get_offset_of_IdSha512_256_7() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha512_256_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha512_256_7() const { return ___IdSha512_256_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha512_256_7() { return &___IdSha512_256_7; }
	inline void set_IdSha512_256_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha512_256_7 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha512_256_7), value);
	}

	inline static int32_t get_offset_of_IdSha3_224_8() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha3_224_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha3_224_8() const { return ___IdSha3_224_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha3_224_8() { return &___IdSha3_224_8; }
	inline void set_IdSha3_224_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha3_224_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_224_8), value);
	}

	inline static int32_t get_offset_of_IdSha3_256_9() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha3_256_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha3_256_9() const { return ___IdSha3_256_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha3_256_9() { return &___IdSha3_256_9; }
	inline void set_IdSha3_256_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha3_256_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_256_9), value);
	}

	inline static int32_t get_offset_of_IdSha3_384_10() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha3_384_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha3_384_10() const { return ___IdSha3_384_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha3_384_10() { return &___IdSha3_384_10; }
	inline void set_IdSha3_384_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha3_384_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_384_10), value);
	}

	inline static int32_t get_offset_of_IdSha3_512_11() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdSha3_512_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha3_512_11() const { return ___IdSha3_512_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha3_512_11() { return &___IdSha3_512_11; }
	inline void set_IdSha3_512_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha3_512_11 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha3_512_11), value);
	}

	inline static int32_t get_offset_of_IdShake128_12() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdShake128_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdShake128_12() const { return ___IdShake128_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdShake128_12() { return &___IdShake128_12; }
	inline void set_IdShake128_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdShake128_12 = value;
		Il2CppCodeGenWriteBarrier((&___IdShake128_12), value);
	}

	inline static int32_t get_offset_of_IdShake256_13() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdShake256_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdShake256_13() const { return ___IdShake256_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdShake256_13() { return &___IdShake256_13; }
	inline void set_IdShake256_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdShake256_13 = value;
		Il2CppCodeGenWriteBarrier((&___IdShake256_13), value);
	}

	inline static int32_t get_offset_of_IdHMacWithSha3_224_14() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdHMacWithSha3_224_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHMacWithSha3_224_14() const { return ___IdHMacWithSha3_224_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHMacWithSha3_224_14() { return &___IdHMacWithSha3_224_14; }
	inline void set_IdHMacWithSha3_224_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHMacWithSha3_224_14 = value;
		Il2CppCodeGenWriteBarrier((&___IdHMacWithSha3_224_14), value);
	}

	inline static int32_t get_offset_of_IdHMacWithSha3_256_15() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdHMacWithSha3_256_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHMacWithSha3_256_15() const { return ___IdHMacWithSha3_256_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHMacWithSha3_256_15() { return &___IdHMacWithSha3_256_15; }
	inline void set_IdHMacWithSha3_256_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHMacWithSha3_256_15 = value;
		Il2CppCodeGenWriteBarrier((&___IdHMacWithSha3_256_15), value);
	}

	inline static int32_t get_offset_of_IdHMacWithSha3_384_16() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdHMacWithSha3_384_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHMacWithSha3_384_16() const { return ___IdHMacWithSha3_384_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHMacWithSha3_384_16() { return &___IdHMacWithSha3_384_16; }
	inline void set_IdHMacWithSha3_384_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHMacWithSha3_384_16 = value;
		Il2CppCodeGenWriteBarrier((&___IdHMacWithSha3_384_16), value);
	}

	inline static int32_t get_offset_of_IdHMacWithSha3_512_17() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdHMacWithSha3_512_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHMacWithSha3_512_17() const { return ___IdHMacWithSha3_512_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHMacWithSha3_512_17() { return &___IdHMacWithSha3_512_17; }
	inline void set_IdHMacWithSha3_512_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHMacWithSha3_512_17 = value;
		Il2CppCodeGenWriteBarrier((&___IdHMacWithSha3_512_17), value);
	}

	inline static int32_t get_offset_of_Aes_18() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___Aes_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Aes_18() const { return ___Aes_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Aes_18() { return &___Aes_18; }
	inline void set_Aes_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Aes_18 = value;
		Il2CppCodeGenWriteBarrier((&___Aes_18), value);
	}

	inline static int32_t get_offset_of_IdAes128Ecb_19() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Ecb_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Ecb_19() const { return ___IdAes128Ecb_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Ecb_19() { return &___IdAes128Ecb_19; }
	inline void set_IdAes128Ecb_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Ecb_19 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Ecb_19), value);
	}

	inline static int32_t get_offset_of_IdAes128Cbc_20() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Cbc_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Cbc_20() const { return ___IdAes128Cbc_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Cbc_20() { return &___IdAes128Cbc_20; }
	inline void set_IdAes128Cbc_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Cbc_20 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Cbc_20), value);
	}

	inline static int32_t get_offset_of_IdAes128Ofb_21() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Ofb_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Ofb_21() const { return ___IdAes128Ofb_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Ofb_21() { return &___IdAes128Ofb_21; }
	inline void set_IdAes128Ofb_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Ofb_21 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Ofb_21), value);
	}

	inline static int32_t get_offset_of_IdAes128Cfb_22() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Cfb_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Cfb_22() const { return ___IdAes128Cfb_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Cfb_22() { return &___IdAes128Cfb_22; }
	inline void set_IdAes128Cfb_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Cfb_22 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Cfb_22), value);
	}

	inline static int32_t get_offset_of_IdAes128Wrap_23() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Wrap_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Wrap_23() const { return ___IdAes128Wrap_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Wrap_23() { return &___IdAes128Wrap_23; }
	inline void set_IdAes128Wrap_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Wrap_23 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Wrap_23), value);
	}

	inline static int32_t get_offset_of_IdAes128Gcm_24() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Gcm_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Gcm_24() const { return ___IdAes128Gcm_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Gcm_24() { return &___IdAes128Gcm_24; }
	inline void set_IdAes128Gcm_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Gcm_24 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Gcm_24), value);
	}

	inline static int32_t get_offset_of_IdAes128Ccm_25() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes128Ccm_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes128Ccm_25() const { return ___IdAes128Ccm_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes128Ccm_25() { return &___IdAes128Ccm_25; }
	inline void set_IdAes128Ccm_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes128Ccm_25 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes128Ccm_25), value);
	}

	inline static int32_t get_offset_of_IdAes192Ecb_26() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Ecb_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Ecb_26() const { return ___IdAes192Ecb_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Ecb_26() { return &___IdAes192Ecb_26; }
	inline void set_IdAes192Ecb_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Ecb_26 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Ecb_26), value);
	}

	inline static int32_t get_offset_of_IdAes192Cbc_27() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Cbc_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Cbc_27() const { return ___IdAes192Cbc_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Cbc_27() { return &___IdAes192Cbc_27; }
	inline void set_IdAes192Cbc_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Cbc_27 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Cbc_27), value);
	}

	inline static int32_t get_offset_of_IdAes192Ofb_28() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Ofb_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Ofb_28() const { return ___IdAes192Ofb_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Ofb_28() { return &___IdAes192Ofb_28; }
	inline void set_IdAes192Ofb_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Ofb_28 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Ofb_28), value);
	}

	inline static int32_t get_offset_of_IdAes192Cfb_29() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Cfb_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Cfb_29() const { return ___IdAes192Cfb_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Cfb_29() { return &___IdAes192Cfb_29; }
	inline void set_IdAes192Cfb_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Cfb_29 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Cfb_29), value);
	}

	inline static int32_t get_offset_of_IdAes192Wrap_30() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Wrap_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Wrap_30() const { return ___IdAes192Wrap_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Wrap_30() { return &___IdAes192Wrap_30; }
	inline void set_IdAes192Wrap_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Wrap_30 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Wrap_30), value);
	}

	inline static int32_t get_offset_of_IdAes192Gcm_31() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Gcm_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Gcm_31() const { return ___IdAes192Gcm_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Gcm_31() { return &___IdAes192Gcm_31; }
	inline void set_IdAes192Gcm_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Gcm_31 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Gcm_31), value);
	}

	inline static int32_t get_offset_of_IdAes192Ccm_32() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes192Ccm_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes192Ccm_32() const { return ___IdAes192Ccm_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes192Ccm_32() { return &___IdAes192Ccm_32; }
	inline void set_IdAes192Ccm_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes192Ccm_32 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes192Ccm_32), value);
	}

	inline static int32_t get_offset_of_IdAes256Ecb_33() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Ecb_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Ecb_33() const { return ___IdAes256Ecb_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Ecb_33() { return &___IdAes256Ecb_33; }
	inline void set_IdAes256Ecb_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Ecb_33 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Ecb_33), value);
	}

	inline static int32_t get_offset_of_IdAes256Cbc_34() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Cbc_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Cbc_34() const { return ___IdAes256Cbc_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Cbc_34() { return &___IdAes256Cbc_34; }
	inline void set_IdAes256Cbc_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Cbc_34 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Cbc_34), value);
	}

	inline static int32_t get_offset_of_IdAes256Ofb_35() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Ofb_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Ofb_35() const { return ___IdAes256Ofb_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Ofb_35() { return &___IdAes256Ofb_35; }
	inline void set_IdAes256Ofb_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Ofb_35 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Ofb_35), value);
	}

	inline static int32_t get_offset_of_IdAes256Cfb_36() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Cfb_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Cfb_36() const { return ___IdAes256Cfb_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Cfb_36() { return &___IdAes256Cfb_36; }
	inline void set_IdAes256Cfb_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Cfb_36 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Cfb_36), value);
	}

	inline static int32_t get_offset_of_IdAes256Wrap_37() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Wrap_37)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Wrap_37() const { return ___IdAes256Wrap_37; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Wrap_37() { return &___IdAes256Wrap_37; }
	inline void set_IdAes256Wrap_37(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Wrap_37 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Wrap_37), value);
	}

	inline static int32_t get_offset_of_IdAes256Gcm_38() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Gcm_38)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Gcm_38() const { return ___IdAes256Gcm_38; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Gcm_38() { return &___IdAes256Gcm_38; }
	inline void set_IdAes256Gcm_38(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Gcm_38 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Gcm_38), value);
	}

	inline static int32_t get_offset_of_IdAes256Ccm_39() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdAes256Ccm_39)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAes256Ccm_39() const { return ___IdAes256Ccm_39; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAes256Ccm_39() { return &___IdAes256Ccm_39; }
	inline void set_IdAes256Ccm_39(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAes256Ccm_39 = value;
		Il2CppCodeGenWriteBarrier((&___IdAes256Ccm_39), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha2_40() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdDsaWithSha2_40)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsaWithSha2_40() const { return ___IdDsaWithSha2_40; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsaWithSha2_40() { return &___IdDsaWithSha2_40; }
	inline void set_IdDsaWithSha2_40(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsaWithSha2_40 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha2_40), value);
	}

	inline static int32_t get_offset_of_DsaWithSha224_41() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___DsaWithSha224_41)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DsaWithSha224_41() const { return ___DsaWithSha224_41; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DsaWithSha224_41() { return &___DsaWithSha224_41; }
	inline void set_DsaWithSha224_41(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DsaWithSha224_41 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha224_41), value);
	}

	inline static int32_t get_offset_of_DsaWithSha256_42() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___DsaWithSha256_42)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DsaWithSha256_42() const { return ___DsaWithSha256_42; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DsaWithSha256_42() { return &___DsaWithSha256_42; }
	inline void set_DsaWithSha256_42(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DsaWithSha256_42 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha256_42), value);
	}

	inline static int32_t get_offset_of_DsaWithSha384_43() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___DsaWithSha384_43)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DsaWithSha384_43() const { return ___DsaWithSha384_43; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DsaWithSha384_43() { return &___DsaWithSha384_43; }
	inline void set_DsaWithSha384_43(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DsaWithSha384_43 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha384_43), value);
	}

	inline static int32_t get_offset_of_DsaWithSha512_44() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___DsaWithSha512_44)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DsaWithSha512_44() const { return ___DsaWithSha512_44; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DsaWithSha512_44() { return &___DsaWithSha512_44; }
	inline void set_DsaWithSha512_44(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DsaWithSha512_44 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha512_44), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha3_224_45() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdDsaWithSha3_224_45)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsaWithSha3_224_45() const { return ___IdDsaWithSha3_224_45; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsaWithSha3_224_45() { return &___IdDsaWithSha3_224_45; }
	inline void set_IdDsaWithSha3_224_45(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsaWithSha3_224_45 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha3_224_45), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha3_256_46() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdDsaWithSha3_256_46)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsaWithSha3_256_46() const { return ___IdDsaWithSha3_256_46; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsaWithSha3_256_46() { return &___IdDsaWithSha3_256_46; }
	inline void set_IdDsaWithSha3_256_46(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsaWithSha3_256_46 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha3_256_46), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha3_384_47() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdDsaWithSha3_384_47)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsaWithSha3_384_47() const { return ___IdDsaWithSha3_384_47; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsaWithSha3_384_47() { return &___IdDsaWithSha3_384_47; }
	inline void set_IdDsaWithSha3_384_47(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsaWithSha3_384_47 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha3_384_47), value);
	}

	inline static int32_t get_offset_of_IdDsaWithSha3_512_48() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdDsaWithSha3_512_48)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdDsaWithSha3_512_48() const { return ___IdDsaWithSha3_512_48; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdDsaWithSha3_512_48() { return &___IdDsaWithSha3_512_48; }
	inline void set_IdDsaWithSha3_512_48(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdDsaWithSha3_512_48 = value;
		Il2CppCodeGenWriteBarrier((&___IdDsaWithSha3_512_48), value);
	}

	inline static int32_t get_offset_of_IdEcdsaWithSha3_224_49() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdEcdsaWithSha3_224_49)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEcdsaWithSha3_224_49() const { return ___IdEcdsaWithSha3_224_49; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEcdsaWithSha3_224_49() { return &___IdEcdsaWithSha3_224_49; }
	inline void set_IdEcdsaWithSha3_224_49(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEcdsaWithSha3_224_49 = value;
		Il2CppCodeGenWriteBarrier((&___IdEcdsaWithSha3_224_49), value);
	}

	inline static int32_t get_offset_of_IdEcdsaWithSha3_256_50() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdEcdsaWithSha3_256_50)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEcdsaWithSha3_256_50() const { return ___IdEcdsaWithSha3_256_50; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEcdsaWithSha3_256_50() { return &___IdEcdsaWithSha3_256_50; }
	inline void set_IdEcdsaWithSha3_256_50(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEcdsaWithSha3_256_50 = value;
		Il2CppCodeGenWriteBarrier((&___IdEcdsaWithSha3_256_50), value);
	}

	inline static int32_t get_offset_of_IdEcdsaWithSha3_384_51() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdEcdsaWithSha3_384_51)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEcdsaWithSha3_384_51() const { return ___IdEcdsaWithSha3_384_51; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEcdsaWithSha3_384_51() { return &___IdEcdsaWithSha3_384_51; }
	inline void set_IdEcdsaWithSha3_384_51(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEcdsaWithSha3_384_51 = value;
		Il2CppCodeGenWriteBarrier((&___IdEcdsaWithSha3_384_51), value);
	}

	inline static int32_t get_offset_of_IdEcdsaWithSha3_512_52() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdEcdsaWithSha3_512_52)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdEcdsaWithSha3_512_52() const { return ___IdEcdsaWithSha3_512_52; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdEcdsaWithSha3_512_52() { return &___IdEcdsaWithSha3_512_52; }
	inline void set_IdEcdsaWithSha3_512_52(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdEcdsaWithSha3_512_52 = value;
		Il2CppCodeGenWriteBarrier((&___IdEcdsaWithSha3_512_52), value);
	}

	inline static int32_t get_offset_of_IdRsassaPkcs1V15WithSha3_224_53() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdRsassaPkcs1V15WithSha3_224_53)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsassaPkcs1V15WithSha3_224_53() const { return ___IdRsassaPkcs1V15WithSha3_224_53; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsassaPkcs1V15WithSha3_224_53() { return &___IdRsassaPkcs1V15WithSha3_224_53; }
	inline void set_IdRsassaPkcs1V15WithSha3_224_53(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsassaPkcs1V15WithSha3_224_53 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsassaPkcs1V15WithSha3_224_53), value);
	}

	inline static int32_t get_offset_of_IdRsassaPkcs1V15WithSha3_256_54() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdRsassaPkcs1V15WithSha3_256_54)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsassaPkcs1V15WithSha3_256_54() const { return ___IdRsassaPkcs1V15WithSha3_256_54; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsassaPkcs1V15WithSha3_256_54() { return &___IdRsassaPkcs1V15WithSha3_256_54; }
	inline void set_IdRsassaPkcs1V15WithSha3_256_54(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsassaPkcs1V15WithSha3_256_54 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsassaPkcs1V15WithSha3_256_54), value);
	}

	inline static int32_t get_offset_of_IdRsassaPkcs1V15WithSha3_384_55() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdRsassaPkcs1V15WithSha3_384_55)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsassaPkcs1V15WithSha3_384_55() const { return ___IdRsassaPkcs1V15WithSha3_384_55; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsassaPkcs1V15WithSha3_384_55() { return &___IdRsassaPkcs1V15WithSha3_384_55; }
	inline void set_IdRsassaPkcs1V15WithSha3_384_55(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsassaPkcs1V15WithSha3_384_55 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsassaPkcs1V15WithSha3_384_55), value);
	}

	inline static int32_t get_offset_of_IdRsassaPkcs1V15WithSha3_512_56() { return static_cast<int32_t>(offsetof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields, ___IdRsassaPkcs1V15WithSha3_512_56)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsassaPkcs1V15WithSha3_512_56() const { return ___IdRsassaPkcs1V15WithSha3_512_56; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsassaPkcs1V15WithSha3_512_56() { return &___IdRsassaPkcs1V15WithSha3_512_56; }
	inline void set_IdRsassaPkcs1V15WithSha3_512_56(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsassaPkcs1V15WithSha3_512_56 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsassaPkcs1V15WithSha3_512_56), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NISTOBJECTIDENTIFIERS_T5946473F810F7AF197B5FEF06B8AD010DA965C2A_H
#ifndef NTTOBJECTIDENTIFIERS_T83D07721D9CD5346AB08E436C66DF5C97854CFF9_H
#define NTTOBJECTIDENTIFIERS_T83D07721D9CD5346AB08E436C66DF5C97854CFF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers
struct  NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9  : public RuntimeObject
{
public:

public:
};

struct NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers::IdCamellia128Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCamellia128Cbc_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers::IdCamellia192Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCamellia192Cbc_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers::IdCamellia256Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCamellia256Cbc_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers::IdCamellia128Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCamellia128Wrap_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers::IdCamellia192Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCamellia192Wrap_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ntt.NttObjectIdentifiers::IdCamellia256Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCamellia256Wrap_5;

public:
	inline static int32_t get_offset_of_IdCamellia128Cbc_0() { return static_cast<int32_t>(offsetof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields, ___IdCamellia128Cbc_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCamellia128Cbc_0() const { return ___IdCamellia128Cbc_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCamellia128Cbc_0() { return &___IdCamellia128Cbc_0; }
	inline void set_IdCamellia128Cbc_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCamellia128Cbc_0 = value;
		Il2CppCodeGenWriteBarrier((&___IdCamellia128Cbc_0), value);
	}

	inline static int32_t get_offset_of_IdCamellia192Cbc_1() { return static_cast<int32_t>(offsetof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields, ___IdCamellia192Cbc_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCamellia192Cbc_1() const { return ___IdCamellia192Cbc_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCamellia192Cbc_1() { return &___IdCamellia192Cbc_1; }
	inline void set_IdCamellia192Cbc_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCamellia192Cbc_1 = value;
		Il2CppCodeGenWriteBarrier((&___IdCamellia192Cbc_1), value);
	}

	inline static int32_t get_offset_of_IdCamellia256Cbc_2() { return static_cast<int32_t>(offsetof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields, ___IdCamellia256Cbc_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCamellia256Cbc_2() const { return ___IdCamellia256Cbc_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCamellia256Cbc_2() { return &___IdCamellia256Cbc_2; }
	inline void set_IdCamellia256Cbc_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCamellia256Cbc_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdCamellia256Cbc_2), value);
	}

	inline static int32_t get_offset_of_IdCamellia128Wrap_3() { return static_cast<int32_t>(offsetof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields, ___IdCamellia128Wrap_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCamellia128Wrap_3() const { return ___IdCamellia128Wrap_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCamellia128Wrap_3() { return &___IdCamellia128Wrap_3; }
	inline void set_IdCamellia128Wrap_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCamellia128Wrap_3 = value;
		Il2CppCodeGenWriteBarrier((&___IdCamellia128Wrap_3), value);
	}

	inline static int32_t get_offset_of_IdCamellia192Wrap_4() { return static_cast<int32_t>(offsetof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields, ___IdCamellia192Wrap_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCamellia192Wrap_4() const { return ___IdCamellia192Wrap_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCamellia192Wrap_4() { return &___IdCamellia192Wrap_4; }
	inline void set_IdCamellia192Wrap_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCamellia192Wrap_4 = value;
		Il2CppCodeGenWriteBarrier((&___IdCamellia192Wrap_4), value);
	}

	inline static int32_t get_offset_of_IdCamellia256Wrap_5() { return static_cast<int32_t>(offsetof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields, ___IdCamellia256Wrap_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCamellia256Wrap_5() const { return ___IdCamellia256Wrap_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCamellia256Wrap_5() { return &___IdCamellia256Wrap_5; }
	inline void set_IdCamellia256Wrap_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCamellia256Wrap_5 = value;
		Il2CppCodeGenWriteBarrier((&___IdCamellia256Wrap_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NTTOBJECTIDENTIFIERS_T83D07721D9CD5346AB08E436C66DF5C97854CFF9_H
#ifndef OCSPOBJECTIDENTIFIERS_T6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_H
#define OCSPOBJECTIDENTIFIERS_T6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers
struct  OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2  : public RuntimeObject
{
public:

public:
};

struct OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcsp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcsp_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspBasic
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspBasic_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspNonce
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspNonce_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspCrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspCrl_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspResponse
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspResponse_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspNocheck
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspNocheck_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspArchiveCutoff
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspArchiveCutoff_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspObjectIdentifiers::PkixOcspServiceLocator
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkixOcspServiceLocator_8;

public:
	inline static int32_t get_offset_of_PkixOcsp_1() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcsp_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcsp_1() const { return ___PkixOcsp_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcsp_1() { return &___PkixOcsp_1; }
	inline void set_PkixOcsp_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcsp_1 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcsp_1), value);
	}

	inline static int32_t get_offset_of_PkixOcspBasic_2() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspBasic_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspBasic_2() const { return ___PkixOcspBasic_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspBasic_2() { return &___PkixOcspBasic_2; }
	inline void set_PkixOcspBasic_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspBasic_2 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspBasic_2), value);
	}

	inline static int32_t get_offset_of_PkixOcspNonce_3() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspNonce_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspNonce_3() const { return ___PkixOcspNonce_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspNonce_3() { return &___PkixOcspNonce_3; }
	inline void set_PkixOcspNonce_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspNonce_3 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspNonce_3), value);
	}

	inline static int32_t get_offset_of_PkixOcspCrl_4() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspCrl_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspCrl_4() const { return ___PkixOcspCrl_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspCrl_4() { return &___PkixOcspCrl_4; }
	inline void set_PkixOcspCrl_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspCrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspCrl_4), value);
	}

	inline static int32_t get_offset_of_PkixOcspResponse_5() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspResponse_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspResponse_5() const { return ___PkixOcspResponse_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspResponse_5() { return &___PkixOcspResponse_5; }
	inline void set_PkixOcspResponse_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspResponse_5 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspResponse_5), value);
	}

	inline static int32_t get_offset_of_PkixOcspNocheck_6() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspNocheck_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspNocheck_6() const { return ___PkixOcspNocheck_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspNocheck_6() { return &___PkixOcspNocheck_6; }
	inline void set_PkixOcspNocheck_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspNocheck_6 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspNocheck_6), value);
	}

	inline static int32_t get_offset_of_PkixOcspArchiveCutoff_7() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspArchiveCutoff_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspArchiveCutoff_7() const { return ___PkixOcspArchiveCutoff_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspArchiveCutoff_7() { return &___PkixOcspArchiveCutoff_7; }
	inline void set_PkixOcspArchiveCutoff_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspArchiveCutoff_7 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspArchiveCutoff_7), value);
	}

	inline static int32_t get_offset_of_PkixOcspServiceLocator_8() { return static_cast<int32_t>(offsetof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields, ___PkixOcspServiceLocator_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkixOcspServiceLocator_8() const { return ___PkixOcspServiceLocator_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkixOcspServiceLocator_8() { return &___PkixOcspServiceLocator_8; }
	inline void set_PkixOcspServiceLocator_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkixOcspServiceLocator_8 = value;
		Il2CppCodeGenWriteBarrier((&___PkixOcspServiceLocator_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPOBJECTIDENTIFIERS_T6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_H
#ifndef OIWOBJECTIDENTIFIERS_TC330CEB3A5408A5AE425AD7396D1A07FD019141D_H
#define OIWOBJECTIDENTIFIERS_TC330CEB3A5408A5AE425AD7396D1A07FD019141D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers
struct  OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D  : public RuntimeObject
{
public:

public:
};

struct OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::MD4WithRsa
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD4WithRsa_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::MD5WithRsa
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD5WithRsa_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::MD4WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD4WithRsaEncryption_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesEcb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesEcb_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesCbc_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesOfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesOfb_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesCfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesCfb_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DesEde
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesEde_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::IdSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSha1_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::DsaWithSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DsaWithSha1_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::Sha1WithRsa
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha1WithRsa_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.OiwObjectIdentifiers::ElGamalAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ElGamalAlgorithm_11;

public:
	inline static int32_t get_offset_of_MD4WithRsa_0() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___MD4WithRsa_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD4WithRsa_0() const { return ___MD4WithRsa_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD4WithRsa_0() { return &___MD4WithRsa_0; }
	inline void set_MD4WithRsa_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD4WithRsa_0 = value;
		Il2CppCodeGenWriteBarrier((&___MD4WithRsa_0), value);
	}

	inline static int32_t get_offset_of_MD5WithRsa_1() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___MD5WithRsa_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD5WithRsa_1() const { return ___MD5WithRsa_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD5WithRsa_1() { return &___MD5WithRsa_1; }
	inline void set_MD5WithRsa_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD5WithRsa_1 = value;
		Il2CppCodeGenWriteBarrier((&___MD5WithRsa_1), value);
	}

	inline static int32_t get_offset_of_MD4WithRsaEncryption_2() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___MD4WithRsaEncryption_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD4WithRsaEncryption_2() const { return ___MD4WithRsaEncryption_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD4WithRsaEncryption_2() { return &___MD4WithRsaEncryption_2; }
	inline void set_MD4WithRsaEncryption_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD4WithRsaEncryption_2 = value;
		Il2CppCodeGenWriteBarrier((&___MD4WithRsaEncryption_2), value);
	}

	inline static int32_t get_offset_of_DesEcb_3() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___DesEcb_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesEcb_3() const { return ___DesEcb_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesEcb_3() { return &___DesEcb_3; }
	inline void set_DesEcb_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesEcb_3 = value;
		Il2CppCodeGenWriteBarrier((&___DesEcb_3), value);
	}

	inline static int32_t get_offset_of_DesCbc_4() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___DesCbc_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesCbc_4() const { return ___DesCbc_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesCbc_4() { return &___DesCbc_4; }
	inline void set_DesCbc_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesCbc_4 = value;
		Il2CppCodeGenWriteBarrier((&___DesCbc_4), value);
	}

	inline static int32_t get_offset_of_DesOfb_5() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___DesOfb_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesOfb_5() const { return ___DesOfb_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesOfb_5() { return &___DesOfb_5; }
	inline void set_DesOfb_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesOfb_5 = value;
		Il2CppCodeGenWriteBarrier((&___DesOfb_5), value);
	}

	inline static int32_t get_offset_of_DesCfb_6() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___DesCfb_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesCfb_6() const { return ___DesCfb_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesCfb_6() { return &___DesCfb_6; }
	inline void set_DesCfb_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesCfb_6 = value;
		Il2CppCodeGenWriteBarrier((&___DesCfb_6), value);
	}

	inline static int32_t get_offset_of_DesEde_7() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___DesEde_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesEde_7() const { return ___DesEde_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesEde_7() { return &___DesEde_7; }
	inline void set_DesEde_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesEde_7 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde_7), value);
	}

	inline static int32_t get_offset_of_IdSha1_8() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___IdSha1_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSha1_8() const { return ___IdSha1_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSha1_8() { return &___IdSha1_8; }
	inline void set_IdSha1_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSha1_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdSha1_8), value);
	}

	inline static int32_t get_offset_of_DsaWithSha1_9() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___DsaWithSha1_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DsaWithSha1_9() const { return ___DsaWithSha1_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DsaWithSha1_9() { return &___DsaWithSha1_9; }
	inline void set_DsaWithSha1_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DsaWithSha1_9 = value;
		Il2CppCodeGenWriteBarrier((&___DsaWithSha1_9), value);
	}

	inline static int32_t get_offset_of_Sha1WithRsa_10() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___Sha1WithRsa_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha1WithRsa_10() const { return ___Sha1WithRsa_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha1WithRsa_10() { return &___Sha1WithRsa_10; }
	inline void set_Sha1WithRsa_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha1WithRsa_10 = value;
		Il2CppCodeGenWriteBarrier((&___Sha1WithRsa_10), value);
	}

	inline static int32_t get_offset_of_ElGamalAlgorithm_11() { return static_cast<int32_t>(offsetof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields, ___ElGamalAlgorithm_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ElGamalAlgorithm_11() const { return ___ElGamalAlgorithm_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ElGamalAlgorithm_11() { return &___ElGamalAlgorithm_11; }
	inline void set_ElGamalAlgorithm_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ElGamalAlgorithm_11 = value;
		Il2CppCodeGenWriteBarrier((&___ElGamalAlgorithm_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIWOBJECTIDENTIFIERS_TC330CEB3A5408A5AE425AD7396D1A07FD019141D_H
#ifndef PKCSOBJECTIDENTIFIERS_T3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_H
#define PKCSOBJECTIDENTIFIERS_T3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers
struct  PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924  : public RuntimeObject
{
public:

public:
};

struct PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs1Oid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs1Oid_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::RsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RsaEncryption_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD2WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD2WithRsaEncryption_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD4WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD4WithRsaEncryption_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD5WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD5WithRsaEncryption_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha1WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha1WithRsaEncryption_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SrsaOaepEncryptionSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SrsaOaepEncryptionSet_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdRsaesOaep
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsaesOaep_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdMgf1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdMgf1_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdPSpecified
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdPSpecified_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdRsassaPss
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsassaPss_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha256WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha256WithRsaEncryption_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha384WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha384WithRsaEncryption_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha512WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha512WithRsaEncryption_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha224WithRsaEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha224WithRsaEncryption_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha512_224WithRSAEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha512_224WithRSAEncryption_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Sha512_256WithRSAEncryption
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Sha512_256WithRSAEncryption_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::DhKeyAgreement
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DhKeyAgreement_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD2AndDesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithMD2AndDesCbc_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD2AndRC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithMD2AndRC2Cbc_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD5AndDesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithMD5AndDesCbc_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithMD5AndRC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithMD5AndRC2Cbc_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithSha1AndDesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithSha1AndDesCbc_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithSha1AndRC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithSha1AndRC2Cbc_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdPbeS2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdPbeS2_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdPbkdf2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdPbkdf2_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::DesEde3Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DesEde3Cbc_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::RC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RC2Cbc_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::rc4
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___rc4_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD2_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD4
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD4_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::MD5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MD5_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHmacWithSha1_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHmacWithSha224_38;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHmacWithSha256_39;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHmacWithSha384_40;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdHmacWithSha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdHmacWithSha512_41;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Data
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Data_43;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SignedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SignedData_44;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::EnvelopedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EnvelopedData_45;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SignedAndEnvelopedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SignedAndEnvelopedData_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::DigestedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DigestedData_47;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::EncryptedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EncryptedData_48;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtEmailAddress
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtEmailAddress_50;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtUnstructuredName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtUnstructuredName_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtContentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtContentType_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtMessageDigest
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtMessageDigest_53;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtSigningTime
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtSigningTime_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtCounterSignature
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtCounterSignature_55;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtChallengePassword
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtChallengePassword_56;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtUnstructuredAddress
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtUnstructuredAddress_57;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtExtendedCertificateAttributes
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtExtendedCertificateAttributes_58;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtSigningDescription
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtSigningDescription_59;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtExtensionRequest
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtExtensionRequest_60;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtSmimeCapabilities
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtSmimeCapabilities_61;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdSmime
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSmime_62;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtFriendlyName
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtFriendlyName_63;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs9AtLocalKeyID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs9AtLocalKeyID_64;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::X509CertType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___X509CertType_65;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::X509Certificate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___X509Certificate_67;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SdsiCertificate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SdsiCertificate_68;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::X509Crl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___X509Crl_70;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlg
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlg_71;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgEsdh
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlgEsdh_72;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgCms3DesWrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlgCms3DesWrap_73;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgCmsRC2Wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlgCmsRC2Wrap_74;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgPwriKek
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlgPwriKek_75;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgSsdh
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlgSsdh_76;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdRsaKem
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdRsaKem_77;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAlgAeadChaCha20Poly1305
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAlgAeadChaCha20Poly1305_78;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PreferSignedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PreferSignedData_79;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::CannotDecryptAny
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CannotDecryptAny_80;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SmimeCapabilitiesVersions
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SmimeCapabilitiesVersions_81;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAReceiptRequest
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAReceiptRequest_82;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTAuthData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCTAuthData_84;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTTstInfo
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCTTstInfo_85;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTCompressedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCTCompressedData_86;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTAuthEnvelopedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCTAuthEnvelopedData_87;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCTTimestampedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCTTimestampedData_88;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfOrigin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCtiEtsProofOfOrigin_90;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfReceipt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCtiEtsProofOfReceipt_91;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfDelivery
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCtiEtsProofOfDelivery_92;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfSender
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCtiEtsProofOfSender_93;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfApproval
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCtiEtsProofOfApproval_94;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdCtiEtsProofOfCreation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdCtiEtsProofOfCreation_95;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAOid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAOid_97;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAContentHint
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAContentHint_98;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAMsgSigDigest
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAMsgSigDigest_99;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAContentReference
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAContentReference_100;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEncrypKeyPref
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEncrypKeyPref_101;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASigningCertificate
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAASigningCertificate_102;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASigningCertificateV2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAASigningCertificateV2_103;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAContentIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAContentIdentifier_104;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASignatureTimeStampToken
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAASignatureTimeStampToken_105;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsSigPolicyID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsSigPolicyID_106;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCommitmentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsCommitmentType_107;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsSignerLocation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsSignerLocation_108;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsSignerAttr
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsSignerAttr_109;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsOtherSigCert
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsOtherSigCert_110;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsContentTimestamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsContentTimestamp_111;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCertificateRefs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsCertificateRefs_112;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsRevocationRefs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsRevocationRefs_113;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCertValues
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsCertValues_114;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsRevocationValues
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsRevocationValues_115;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsEscTimeStamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsEscTimeStamp_116;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsCertCrlTimestamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsCertCrlTimestamp_117;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAEtsArchiveTimestamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAEtsArchiveTimestamp_118;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAADecryptKeyID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAADecryptKeyID_119;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAImplCryptoAlgs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAImplCryptoAlgs_120;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAAsymmDecryptKeyID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAAsymmDecryptKeyID_121;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAImplCompressAlgs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAImplCompressAlgs_122;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAACommunityIdentifiers
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAACommunityIdentifiers_123;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASigPolicyID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAASigPolicyID_124;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAACommitmentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAACommitmentType_125;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAASignerLocation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAASignerLocation_126;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdAAOtherSigCert
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdAAOtherSigCert_127;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdSpqEtsUri
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSpqEtsUri_129;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::IdSpqEtsUNotice
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdSpqEtsUNotice_130;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::KeyBag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___KeyBag_133;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::Pkcs8ShroudedKeyBag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Pkcs8ShroudedKeyBag_134;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::CertBag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CertBag_135;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::CrlBag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CrlBag_136;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SecretBag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecretBag_137;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::SafeContentsBag
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SafeContentsBag_138;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd128BitRC4
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithShaAnd128BitRC4_140;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd40BitRC4
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithShaAnd40BitRC4_141;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd3KeyTripleDesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithShaAnd3KeyTripleDesCbc_142;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd2KeyTripleDesCbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithShaAnd2KeyTripleDesCbc_143;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbeWithShaAnd128BitRC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbeWithShaAnd128BitRC2Cbc_144;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers::PbewithShaAnd40BitRC2Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PbewithShaAnd40BitRC2Cbc_145;

public:
	inline static int32_t get_offset_of_Pkcs1Oid_1() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs1Oid_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs1Oid_1() const { return ___Pkcs1Oid_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs1Oid_1() { return &___Pkcs1Oid_1; }
	inline void set_Pkcs1Oid_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs1Oid_1 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs1Oid_1), value);
	}

	inline static int32_t get_offset_of_RsaEncryption_2() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___RsaEncryption_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RsaEncryption_2() const { return ___RsaEncryption_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RsaEncryption_2() { return &___RsaEncryption_2; }
	inline void set_RsaEncryption_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RsaEncryption_2 = value;
		Il2CppCodeGenWriteBarrier((&___RsaEncryption_2), value);
	}

	inline static int32_t get_offset_of_MD2WithRsaEncryption_3() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___MD2WithRsaEncryption_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD2WithRsaEncryption_3() const { return ___MD2WithRsaEncryption_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD2WithRsaEncryption_3() { return &___MD2WithRsaEncryption_3; }
	inline void set_MD2WithRsaEncryption_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD2WithRsaEncryption_3 = value;
		Il2CppCodeGenWriteBarrier((&___MD2WithRsaEncryption_3), value);
	}

	inline static int32_t get_offset_of_MD4WithRsaEncryption_4() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___MD4WithRsaEncryption_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD4WithRsaEncryption_4() const { return ___MD4WithRsaEncryption_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD4WithRsaEncryption_4() { return &___MD4WithRsaEncryption_4; }
	inline void set_MD4WithRsaEncryption_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD4WithRsaEncryption_4 = value;
		Il2CppCodeGenWriteBarrier((&___MD4WithRsaEncryption_4), value);
	}

	inline static int32_t get_offset_of_MD5WithRsaEncryption_5() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___MD5WithRsaEncryption_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD5WithRsaEncryption_5() const { return ___MD5WithRsaEncryption_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD5WithRsaEncryption_5() { return &___MD5WithRsaEncryption_5; }
	inline void set_MD5WithRsaEncryption_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD5WithRsaEncryption_5 = value;
		Il2CppCodeGenWriteBarrier((&___MD5WithRsaEncryption_5), value);
	}

	inline static int32_t get_offset_of_Sha1WithRsaEncryption_6() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha1WithRsaEncryption_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha1WithRsaEncryption_6() const { return ___Sha1WithRsaEncryption_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha1WithRsaEncryption_6() { return &___Sha1WithRsaEncryption_6; }
	inline void set_Sha1WithRsaEncryption_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha1WithRsaEncryption_6 = value;
		Il2CppCodeGenWriteBarrier((&___Sha1WithRsaEncryption_6), value);
	}

	inline static int32_t get_offset_of_SrsaOaepEncryptionSet_7() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SrsaOaepEncryptionSet_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SrsaOaepEncryptionSet_7() const { return ___SrsaOaepEncryptionSet_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SrsaOaepEncryptionSet_7() { return &___SrsaOaepEncryptionSet_7; }
	inline void set_SrsaOaepEncryptionSet_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SrsaOaepEncryptionSet_7 = value;
		Il2CppCodeGenWriteBarrier((&___SrsaOaepEncryptionSet_7), value);
	}

	inline static int32_t get_offset_of_IdRsaesOaep_8() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdRsaesOaep_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsaesOaep_8() const { return ___IdRsaesOaep_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsaesOaep_8() { return &___IdRsaesOaep_8; }
	inline void set_IdRsaesOaep_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsaesOaep_8 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsaesOaep_8), value);
	}

	inline static int32_t get_offset_of_IdMgf1_9() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdMgf1_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdMgf1_9() const { return ___IdMgf1_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdMgf1_9() { return &___IdMgf1_9; }
	inline void set_IdMgf1_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdMgf1_9 = value;
		Il2CppCodeGenWriteBarrier((&___IdMgf1_9), value);
	}

	inline static int32_t get_offset_of_IdPSpecified_10() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdPSpecified_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdPSpecified_10() const { return ___IdPSpecified_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdPSpecified_10() { return &___IdPSpecified_10; }
	inline void set_IdPSpecified_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdPSpecified_10 = value;
		Il2CppCodeGenWriteBarrier((&___IdPSpecified_10), value);
	}

	inline static int32_t get_offset_of_IdRsassaPss_11() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdRsassaPss_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsassaPss_11() const { return ___IdRsassaPss_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsassaPss_11() { return &___IdRsassaPss_11; }
	inline void set_IdRsassaPss_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsassaPss_11 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsassaPss_11), value);
	}

	inline static int32_t get_offset_of_Sha256WithRsaEncryption_12() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha256WithRsaEncryption_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha256WithRsaEncryption_12() const { return ___Sha256WithRsaEncryption_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha256WithRsaEncryption_12() { return &___Sha256WithRsaEncryption_12; }
	inline void set_Sha256WithRsaEncryption_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha256WithRsaEncryption_12 = value;
		Il2CppCodeGenWriteBarrier((&___Sha256WithRsaEncryption_12), value);
	}

	inline static int32_t get_offset_of_Sha384WithRsaEncryption_13() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha384WithRsaEncryption_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha384WithRsaEncryption_13() const { return ___Sha384WithRsaEncryption_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha384WithRsaEncryption_13() { return &___Sha384WithRsaEncryption_13; }
	inline void set_Sha384WithRsaEncryption_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha384WithRsaEncryption_13 = value;
		Il2CppCodeGenWriteBarrier((&___Sha384WithRsaEncryption_13), value);
	}

	inline static int32_t get_offset_of_Sha512WithRsaEncryption_14() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha512WithRsaEncryption_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha512WithRsaEncryption_14() const { return ___Sha512WithRsaEncryption_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha512WithRsaEncryption_14() { return &___Sha512WithRsaEncryption_14; }
	inline void set_Sha512WithRsaEncryption_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha512WithRsaEncryption_14 = value;
		Il2CppCodeGenWriteBarrier((&___Sha512WithRsaEncryption_14), value);
	}

	inline static int32_t get_offset_of_Sha224WithRsaEncryption_15() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha224WithRsaEncryption_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha224WithRsaEncryption_15() const { return ___Sha224WithRsaEncryption_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha224WithRsaEncryption_15() { return &___Sha224WithRsaEncryption_15; }
	inline void set_Sha224WithRsaEncryption_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha224WithRsaEncryption_15 = value;
		Il2CppCodeGenWriteBarrier((&___Sha224WithRsaEncryption_15), value);
	}

	inline static int32_t get_offset_of_Sha512_224WithRSAEncryption_16() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha512_224WithRSAEncryption_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha512_224WithRSAEncryption_16() const { return ___Sha512_224WithRSAEncryption_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha512_224WithRSAEncryption_16() { return &___Sha512_224WithRSAEncryption_16; }
	inline void set_Sha512_224WithRSAEncryption_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha512_224WithRSAEncryption_16 = value;
		Il2CppCodeGenWriteBarrier((&___Sha512_224WithRSAEncryption_16), value);
	}

	inline static int32_t get_offset_of_Sha512_256WithRSAEncryption_17() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Sha512_256WithRSAEncryption_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Sha512_256WithRSAEncryption_17() const { return ___Sha512_256WithRSAEncryption_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Sha512_256WithRSAEncryption_17() { return &___Sha512_256WithRSAEncryption_17; }
	inline void set_Sha512_256WithRSAEncryption_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Sha512_256WithRSAEncryption_17 = value;
		Il2CppCodeGenWriteBarrier((&___Sha512_256WithRSAEncryption_17), value);
	}

	inline static int32_t get_offset_of_DhKeyAgreement_19() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___DhKeyAgreement_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DhKeyAgreement_19() const { return ___DhKeyAgreement_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DhKeyAgreement_19() { return &___DhKeyAgreement_19; }
	inline void set_DhKeyAgreement_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DhKeyAgreement_19 = value;
		Il2CppCodeGenWriteBarrier((&___DhKeyAgreement_19), value);
	}

	inline static int32_t get_offset_of_PbeWithMD2AndDesCbc_21() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithMD2AndDesCbc_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithMD2AndDesCbc_21() const { return ___PbeWithMD2AndDesCbc_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithMD2AndDesCbc_21() { return &___PbeWithMD2AndDesCbc_21; }
	inline void set_PbeWithMD2AndDesCbc_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithMD2AndDesCbc_21 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD2AndDesCbc_21), value);
	}

	inline static int32_t get_offset_of_PbeWithMD2AndRC2Cbc_22() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithMD2AndRC2Cbc_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithMD2AndRC2Cbc_22() const { return ___PbeWithMD2AndRC2Cbc_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithMD2AndRC2Cbc_22() { return &___PbeWithMD2AndRC2Cbc_22; }
	inline void set_PbeWithMD2AndRC2Cbc_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithMD2AndRC2Cbc_22 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD2AndRC2Cbc_22), value);
	}

	inline static int32_t get_offset_of_PbeWithMD5AndDesCbc_23() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithMD5AndDesCbc_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithMD5AndDesCbc_23() const { return ___PbeWithMD5AndDesCbc_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithMD5AndDesCbc_23() { return &___PbeWithMD5AndDesCbc_23; }
	inline void set_PbeWithMD5AndDesCbc_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithMD5AndDesCbc_23 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD5AndDesCbc_23), value);
	}

	inline static int32_t get_offset_of_PbeWithMD5AndRC2Cbc_24() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithMD5AndRC2Cbc_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithMD5AndRC2Cbc_24() const { return ___PbeWithMD5AndRC2Cbc_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithMD5AndRC2Cbc_24() { return &___PbeWithMD5AndRC2Cbc_24; }
	inline void set_PbeWithMD5AndRC2Cbc_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithMD5AndRC2Cbc_24 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithMD5AndRC2Cbc_24), value);
	}

	inline static int32_t get_offset_of_PbeWithSha1AndDesCbc_25() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithSha1AndDesCbc_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithSha1AndDesCbc_25() const { return ___PbeWithSha1AndDesCbc_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithSha1AndDesCbc_25() { return &___PbeWithSha1AndDesCbc_25; }
	inline void set_PbeWithSha1AndDesCbc_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithSha1AndDesCbc_25 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithSha1AndDesCbc_25), value);
	}

	inline static int32_t get_offset_of_PbeWithSha1AndRC2Cbc_26() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithSha1AndRC2Cbc_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithSha1AndRC2Cbc_26() const { return ___PbeWithSha1AndRC2Cbc_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithSha1AndRC2Cbc_26() { return &___PbeWithSha1AndRC2Cbc_26; }
	inline void set_PbeWithSha1AndRC2Cbc_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithSha1AndRC2Cbc_26 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithSha1AndRC2Cbc_26), value);
	}

	inline static int32_t get_offset_of_IdPbeS2_27() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdPbeS2_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdPbeS2_27() const { return ___IdPbeS2_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdPbeS2_27() { return &___IdPbeS2_27; }
	inline void set_IdPbeS2_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdPbeS2_27 = value;
		Il2CppCodeGenWriteBarrier((&___IdPbeS2_27), value);
	}

	inline static int32_t get_offset_of_IdPbkdf2_28() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdPbkdf2_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdPbkdf2_28() const { return ___IdPbkdf2_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdPbkdf2_28() { return &___IdPbkdf2_28; }
	inline void set_IdPbkdf2_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdPbkdf2_28 = value;
		Il2CppCodeGenWriteBarrier((&___IdPbkdf2_28), value);
	}

	inline static int32_t get_offset_of_DesEde3Cbc_30() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___DesEde3Cbc_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DesEde3Cbc_30() const { return ___DesEde3Cbc_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DesEde3Cbc_30() { return &___DesEde3Cbc_30; }
	inline void set_DesEde3Cbc_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DesEde3Cbc_30 = value;
		Il2CppCodeGenWriteBarrier((&___DesEde3Cbc_30), value);
	}

	inline static int32_t get_offset_of_RC2Cbc_31() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___RC2Cbc_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RC2Cbc_31() const { return ___RC2Cbc_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RC2Cbc_31() { return &___RC2Cbc_31; }
	inline void set_RC2Cbc_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RC2Cbc_31 = value;
		Il2CppCodeGenWriteBarrier((&___RC2Cbc_31), value);
	}

	inline static int32_t get_offset_of_rc4_32() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___rc4_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_rc4_32() const { return ___rc4_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_rc4_32() { return &___rc4_32; }
	inline void set_rc4_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___rc4_32 = value;
		Il2CppCodeGenWriteBarrier((&___rc4_32), value);
	}

	inline static int32_t get_offset_of_MD2_34() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___MD2_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD2_34() const { return ___MD2_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD2_34() { return &___MD2_34; }
	inline void set_MD2_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD2_34 = value;
		Il2CppCodeGenWriteBarrier((&___MD2_34), value);
	}

	inline static int32_t get_offset_of_MD4_35() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___MD4_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD4_35() const { return ___MD4_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD4_35() { return &___MD4_35; }
	inline void set_MD4_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD4_35 = value;
		Il2CppCodeGenWriteBarrier((&___MD4_35), value);
	}

	inline static int32_t get_offset_of_MD5_36() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___MD5_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MD5_36() const { return ___MD5_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MD5_36() { return &___MD5_36; }
	inline void set_MD5_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MD5_36 = value;
		Il2CppCodeGenWriteBarrier((&___MD5_36), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha1_37() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdHmacWithSha1_37)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHmacWithSha1_37() const { return ___IdHmacWithSha1_37; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHmacWithSha1_37() { return &___IdHmacWithSha1_37; }
	inline void set_IdHmacWithSha1_37(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHmacWithSha1_37 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha1_37), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha224_38() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdHmacWithSha224_38)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHmacWithSha224_38() const { return ___IdHmacWithSha224_38; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHmacWithSha224_38() { return &___IdHmacWithSha224_38; }
	inline void set_IdHmacWithSha224_38(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHmacWithSha224_38 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha224_38), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha256_39() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdHmacWithSha256_39)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHmacWithSha256_39() const { return ___IdHmacWithSha256_39; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHmacWithSha256_39() { return &___IdHmacWithSha256_39; }
	inline void set_IdHmacWithSha256_39(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHmacWithSha256_39 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha256_39), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha384_40() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdHmacWithSha384_40)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHmacWithSha384_40() const { return ___IdHmacWithSha384_40; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHmacWithSha384_40() { return &___IdHmacWithSha384_40; }
	inline void set_IdHmacWithSha384_40(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHmacWithSha384_40 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha384_40), value);
	}

	inline static int32_t get_offset_of_IdHmacWithSha512_41() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdHmacWithSha512_41)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdHmacWithSha512_41() const { return ___IdHmacWithSha512_41; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdHmacWithSha512_41() { return &___IdHmacWithSha512_41; }
	inline void set_IdHmacWithSha512_41(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdHmacWithSha512_41 = value;
		Il2CppCodeGenWriteBarrier((&___IdHmacWithSha512_41), value);
	}

	inline static int32_t get_offset_of_Data_43() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Data_43)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Data_43() const { return ___Data_43; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Data_43() { return &___Data_43; }
	inline void set_Data_43(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Data_43 = value;
		Il2CppCodeGenWriteBarrier((&___Data_43), value);
	}

	inline static int32_t get_offset_of_SignedData_44() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SignedData_44)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SignedData_44() const { return ___SignedData_44; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SignedData_44() { return &___SignedData_44; }
	inline void set_SignedData_44(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SignedData_44 = value;
		Il2CppCodeGenWriteBarrier((&___SignedData_44), value);
	}

	inline static int32_t get_offset_of_EnvelopedData_45() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___EnvelopedData_45)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EnvelopedData_45() const { return ___EnvelopedData_45; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EnvelopedData_45() { return &___EnvelopedData_45; }
	inline void set_EnvelopedData_45(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EnvelopedData_45 = value;
		Il2CppCodeGenWriteBarrier((&___EnvelopedData_45), value);
	}

	inline static int32_t get_offset_of_SignedAndEnvelopedData_46() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SignedAndEnvelopedData_46)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SignedAndEnvelopedData_46() const { return ___SignedAndEnvelopedData_46; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SignedAndEnvelopedData_46() { return &___SignedAndEnvelopedData_46; }
	inline void set_SignedAndEnvelopedData_46(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SignedAndEnvelopedData_46 = value;
		Il2CppCodeGenWriteBarrier((&___SignedAndEnvelopedData_46), value);
	}

	inline static int32_t get_offset_of_DigestedData_47() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___DigestedData_47)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DigestedData_47() const { return ___DigestedData_47; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DigestedData_47() { return &___DigestedData_47; }
	inline void set_DigestedData_47(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DigestedData_47 = value;
		Il2CppCodeGenWriteBarrier((&___DigestedData_47), value);
	}

	inline static int32_t get_offset_of_EncryptedData_48() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___EncryptedData_48)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EncryptedData_48() const { return ___EncryptedData_48; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EncryptedData_48() { return &___EncryptedData_48; }
	inline void set_EncryptedData_48(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EncryptedData_48 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptedData_48), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtEmailAddress_50() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtEmailAddress_50)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtEmailAddress_50() const { return ___Pkcs9AtEmailAddress_50; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtEmailAddress_50() { return &___Pkcs9AtEmailAddress_50; }
	inline void set_Pkcs9AtEmailAddress_50(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtEmailAddress_50 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtEmailAddress_50), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtUnstructuredName_51() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtUnstructuredName_51)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtUnstructuredName_51() const { return ___Pkcs9AtUnstructuredName_51; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtUnstructuredName_51() { return &___Pkcs9AtUnstructuredName_51; }
	inline void set_Pkcs9AtUnstructuredName_51(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtUnstructuredName_51 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtUnstructuredName_51), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtContentType_52() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtContentType_52)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtContentType_52() const { return ___Pkcs9AtContentType_52; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtContentType_52() { return &___Pkcs9AtContentType_52; }
	inline void set_Pkcs9AtContentType_52(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtContentType_52 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtContentType_52), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtMessageDigest_53() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtMessageDigest_53)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtMessageDigest_53() const { return ___Pkcs9AtMessageDigest_53; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtMessageDigest_53() { return &___Pkcs9AtMessageDigest_53; }
	inline void set_Pkcs9AtMessageDigest_53(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtMessageDigest_53 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtMessageDigest_53), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtSigningTime_54() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtSigningTime_54)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtSigningTime_54() const { return ___Pkcs9AtSigningTime_54; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtSigningTime_54() { return &___Pkcs9AtSigningTime_54; }
	inline void set_Pkcs9AtSigningTime_54(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtSigningTime_54 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtSigningTime_54), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtCounterSignature_55() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtCounterSignature_55)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtCounterSignature_55() const { return ___Pkcs9AtCounterSignature_55; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtCounterSignature_55() { return &___Pkcs9AtCounterSignature_55; }
	inline void set_Pkcs9AtCounterSignature_55(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtCounterSignature_55 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtCounterSignature_55), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtChallengePassword_56() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtChallengePassword_56)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtChallengePassword_56() const { return ___Pkcs9AtChallengePassword_56; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtChallengePassword_56() { return &___Pkcs9AtChallengePassword_56; }
	inline void set_Pkcs9AtChallengePassword_56(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtChallengePassword_56 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtChallengePassword_56), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtUnstructuredAddress_57() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtUnstructuredAddress_57)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtUnstructuredAddress_57() const { return ___Pkcs9AtUnstructuredAddress_57; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtUnstructuredAddress_57() { return &___Pkcs9AtUnstructuredAddress_57; }
	inline void set_Pkcs9AtUnstructuredAddress_57(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtUnstructuredAddress_57 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtUnstructuredAddress_57), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtExtendedCertificateAttributes_58() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtExtendedCertificateAttributes_58)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtExtendedCertificateAttributes_58() const { return ___Pkcs9AtExtendedCertificateAttributes_58; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtExtendedCertificateAttributes_58() { return &___Pkcs9AtExtendedCertificateAttributes_58; }
	inline void set_Pkcs9AtExtendedCertificateAttributes_58(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtExtendedCertificateAttributes_58 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtExtendedCertificateAttributes_58), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtSigningDescription_59() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtSigningDescription_59)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtSigningDescription_59() const { return ___Pkcs9AtSigningDescription_59; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtSigningDescription_59() { return &___Pkcs9AtSigningDescription_59; }
	inline void set_Pkcs9AtSigningDescription_59(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtSigningDescription_59 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtSigningDescription_59), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtExtensionRequest_60() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtExtensionRequest_60)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtExtensionRequest_60() const { return ___Pkcs9AtExtensionRequest_60; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtExtensionRequest_60() { return &___Pkcs9AtExtensionRequest_60; }
	inline void set_Pkcs9AtExtensionRequest_60(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtExtensionRequest_60 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtExtensionRequest_60), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtSmimeCapabilities_61() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtSmimeCapabilities_61)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtSmimeCapabilities_61() const { return ___Pkcs9AtSmimeCapabilities_61; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtSmimeCapabilities_61() { return &___Pkcs9AtSmimeCapabilities_61; }
	inline void set_Pkcs9AtSmimeCapabilities_61(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtSmimeCapabilities_61 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtSmimeCapabilities_61), value);
	}

	inline static int32_t get_offset_of_IdSmime_62() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdSmime_62)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSmime_62() const { return ___IdSmime_62; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSmime_62() { return &___IdSmime_62; }
	inline void set_IdSmime_62(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSmime_62 = value;
		Il2CppCodeGenWriteBarrier((&___IdSmime_62), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtFriendlyName_63() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtFriendlyName_63)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtFriendlyName_63() const { return ___Pkcs9AtFriendlyName_63; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtFriendlyName_63() { return &___Pkcs9AtFriendlyName_63; }
	inline void set_Pkcs9AtFriendlyName_63(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtFriendlyName_63 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtFriendlyName_63), value);
	}

	inline static int32_t get_offset_of_Pkcs9AtLocalKeyID_64() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs9AtLocalKeyID_64)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs9AtLocalKeyID_64() const { return ___Pkcs9AtLocalKeyID_64; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs9AtLocalKeyID_64() { return &___Pkcs9AtLocalKeyID_64; }
	inline void set_Pkcs9AtLocalKeyID_64(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs9AtLocalKeyID_64 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs9AtLocalKeyID_64), value);
	}

	inline static int32_t get_offset_of_X509CertType_65() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___X509CertType_65)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_X509CertType_65() const { return ___X509CertType_65; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_X509CertType_65() { return &___X509CertType_65; }
	inline void set_X509CertType_65(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___X509CertType_65 = value;
		Il2CppCodeGenWriteBarrier((&___X509CertType_65), value);
	}

	inline static int32_t get_offset_of_X509Certificate_67() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___X509Certificate_67)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_X509Certificate_67() const { return ___X509Certificate_67; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_X509Certificate_67() { return &___X509Certificate_67; }
	inline void set_X509Certificate_67(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___X509Certificate_67 = value;
		Il2CppCodeGenWriteBarrier((&___X509Certificate_67), value);
	}

	inline static int32_t get_offset_of_SdsiCertificate_68() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SdsiCertificate_68)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SdsiCertificate_68() const { return ___SdsiCertificate_68; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SdsiCertificate_68() { return &___SdsiCertificate_68; }
	inline void set_SdsiCertificate_68(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SdsiCertificate_68 = value;
		Il2CppCodeGenWriteBarrier((&___SdsiCertificate_68), value);
	}

	inline static int32_t get_offset_of_X509Crl_70() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___X509Crl_70)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_X509Crl_70() const { return ___X509Crl_70; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_X509Crl_70() { return &___X509Crl_70; }
	inline void set_X509Crl_70(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___X509Crl_70 = value;
		Il2CppCodeGenWriteBarrier((&___X509Crl_70), value);
	}

	inline static int32_t get_offset_of_IdAlg_71() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlg_71)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlg_71() const { return ___IdAlg_71; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlg_71() { return &___IdAlg_71; }
	inline void set_IdAlg_71(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlg_71 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlg_71), value);
	}

	inline static int32_t get_offset_of_IdAlgEsdh_72() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlgEsdh_72)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlgEsdh_72() const { return ___IdAlgEsdh_72; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlgEsdh_72() { return &___IdAlgEsdh_72; }
	inline void set_IdAlgEsdh_72(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlgEsdh_72 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgEsdh_72), value);
	}

	inline static int32_t get_offset_of_IdAlgCms3DesWrap_73() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlgCms3DesWrap_73)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlgCms3DesWrap_73() const { return ___IdAlgCms3DesWrap_73; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlgCms3DesWrap_73() { return &___IdAlgCms3DesWrap_73; }
	inline void set_IdAlgCms3DesWrap_73(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlgCms3DesWrap_73 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgCms3DesWrap_73), value);
	}

	inline static int32_t get_offset_of_IdAlgCmsRC2Wrap_74() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlgCmsRC2Wrap_74)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlgCmsRC2Wrap_74() const { return ___IdAlgCmsRC2Wrap_74; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlgCmsRC2Wrap_74() { return &___IdAlgCmsRC2Wrap_74; }
	inline void set_IdAlgCmsRC2Wrap_74(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlgCmsRC2Wrap_74 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgCmsRC2Wrap_74), value);
	}

	inline static int32_t get_offset_of_IdAlgPwriKek_75() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlgPwriKek_75)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlgPwriKek_75() const { return ___IdAlgPwriKek_75; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlgPwriKek_75() { return &___IdAlgPwriKek_75; }
	inline void set_IdAlgPwriKek_75(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlgPwriKek_75 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgPwriKek_75), value);
	}

	inline static int32_t get_offset_of_IdAlgSsdh_76() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlgSsdh_76)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlgSsdh_76() const { return ___IdAlgSsdh_76; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlgSsdh_76() { return &___IdAlgSsdh_76; }
	inline void set_IdAlgSsdh_76(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlgSsdh_76 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgSsdh_76), value);
	}

	inline static int32_t get_offset_of_IdRsaKem_77() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdRsaKem_77)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdRsaKem_77() const { return ___IdRsaKem_77; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdRsaKem_77() { return &___IdRsaKem_77; }
	inline void set_IdRsaKem_77(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdRsaKem_77 = value;
		Il2CppCodeGenWriteBarrier((&___IdRsaKem_77), value);
	}

	inline static int32_t get_offset_of_IdAlgAeadChaCha20Poly1305_78() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAlgAeadChaCha20Poly1305_78)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAlgAeadChaCha20Poly1305_78() const { return ___IdAlgAeadChaCha20Poly1305_78; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAlgAeadChaCha20Poly1305_78() { return &___IdAlgAeadChaCha20Poly1305_78; }
	inline void set_IdAlgAeadChaCha20Poly1305_78(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAlgAeadChaCha20Poly1305_78 = value;
		Il2CppCodeGenWriteBarrier((&___IdAlgAeadChaCha20Poly1305_78), value);
	}

	inline static int32_t get_offset_of_PreferSignedData_79() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PreferSignedData_79)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PreferSignedData_79() const { return ___PreferSignedData_79; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PreferSignedData_79() { return &___PreferSignedData_79; }
	inline void set_PreferSignedData_79(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PreferSignedData_79 = value;
		Il2CppCodeGenWriteBarrier((&___PreferSignedData_79), value);
	}

	inline static int32_t get_offset_of_CannotDecryptAny_80() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___CannotDecryptAny_80)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CannotDecryptAny_80() const { return ___CannotDecryptAny_80; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CannotDecryptAny_80() { return &___CannotDecryptAny_80; }
	inline void set_CannotDecryptAny_80(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CannotDecryptAny_80 = value;
		Il2CppCodeGenWriteBarrier((&___CannotDecryptAny_80), value);
	}

	inline static int32_t get_offset_of_SmimeCapabilitiesVersions_81() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SmimeCapabilitiesVersions_81)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SmimeCapabilitiesVersions_81() const { return ___SmimeCapabilitiesVersions_81; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SmimeCapabilitiesVersions_81() { return &___SmimeCapabilitiesVersions_81; }
	inline void set_SmimeCapabilitiesVersions_81(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SmimeCapabilitiesVersions_81 = value;
		Il2CppCodeGenWriteBarrier((&___SmimeCapabilitiesVersions_81), value);
	}

	inline static int32_t get_offset_of_IdAAReceiptRequest_82() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAReceiptRequest_82)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAReceiptRequest_82() const { return ___IdAAReceiptRequest_82; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAReceiptRequest_82() { return &___IdAAReceiptRequest_82; }
	inline void set_IdAAReceiptRequest_82(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAReceiptRequest_82 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAReceiptRequest_82), value);
	}

	inline static int32_t get_offset_of_IdCTAuthData_84() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCTAuthData_84)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCTAuthData_84() const { return ___IdCTAuthData_84; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCTAuthData_84() { return &___IdCTAuthData_84; }
	inline void set_IdCTAuthData_84(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCTAuthData_84 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTAuthData_84), value);
	}

	inline static int32_t get_offset_of_IdCTTstInfo_85() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCTTstInfo_85)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCTTstInfo_85() const { return ___IdCTTstInfo_85; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCTTstInfo_85() { return &___IdCTTstInfo_85; }
	inline void set_IdCTTstInfo_85(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCTTstInfo_85 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTTstInfo_85), value);
	}

	inline static int32_t get_offset_of_IdCTCompressedData_86() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCTCompressedData_86)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCTCompressedData_86() const { return ___IdCTCompressedData_86; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCTCompressedData_86() { return &___IdCTCompressedData_86; }
	inline void set_IdCTCompressedData_86(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCTCompressedData_86 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTCompressedData_86), value);
	}

	inline static int32_t get_offset_of_IdCTAuthEnvelopedData_87() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCTAuthEnvelopedData_87)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCTAuthEnvelopedData_87() const { return ___IdCTAuthEnvelopedData_87; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCTAuthEnvelopedData_87() { return &___IdCTAuthEnvelopedData_87; }
	inline void set_IdCTAuthEnvelopedData_87(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCTAuthEnvelopedData_87 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTAuthEnvelopedData_87), value);
	}

	inline static int32_t get_offset_of_IdCTTimestampedData_88() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCTTimestampedData_88)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCTTimestampedData_88() const { return ___IdCTTimestampedData_88; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCTTimestampedData_88() { return &___IdCTTimestampedData_88; }
	inline void set_IdCTTimestampedData_88(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCTTimestampedData_88 = value;
		Il2CppCodeGenWriteBarrier((&___IdCTTimestampedData_88), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfOrigin_90() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCtiEtsProofOfOrigin_90)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCtiEtsProofOfOrigin_90() const { return ___IdCtiEtsProofOfOrigin_90; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCtiEtsProofOfOrigin_90() { return &___IdCtiEtsProofOfOrigin_90; }
	inline void set_IdCtiEtsProofOfOrigin_90(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCtiEtsProofOfOrigin_90 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfOrigin_90), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfReceipt_91() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCtiEtsProofOfReceipt_91)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCtiEtsProofOfReceipt_91() const { return ___IdCtiEtsProofOfReceipt_91; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCtiEtsProofOfReceipt_91() { return &___IdCtiEtsProofOfReceipt_91; }
	inline void set_IdCtiEtsProofOfReceipt_91(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCtiEtsProofOfReceipt_91 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfReceipt_91), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfDelivery_92() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCtiEtsProofOfDelivery_92)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCtiEtsProofOfDelivery_92() const { return ___IdCtiEtsProofOfDelivery_92; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCtiEtsProofOfDelivery_92() { return &___IdCtiEtsProofOfDelivery_92; }
	inline void set_IdCtiEtsProofOfDelivery_92(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCtiEtsProofOfDelivery_92 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfDelivery_92), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfSender_93() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCtiEtsProofOfSender_93)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCtiEtsProofOfSender_93() const { return ___IdCtiEtsProofOfSender_93; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCtiEtsProofOfSender_93() { return &___IdCtiEtsProofOfSender_93; }
	inline void set_IdCtiEtsProofOfSender_93(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCtiEtsProofOfSender_93 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfSender_93), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfApproval_94() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCtiEtsProofOfApproval_94)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCtiEtsProofOfApproval_94() const { return ___IdCtiEtsProofOfApproval_94; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCtiEtsProofOfApproval_94() { return &___IdCtiEtsProofOfApproval_94; }
	inline void set_IdCtiEtsProofOfApproval_94(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCtiEtsProofOfApproval_94 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfApproval_94), value);
	}

	inline static int32_t get_offset_of_IdCtiEtsProofOfCreation_95() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdCtiEtsProofOfCreation_95)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdCtiEtsProofOfCreation_95() const { return ___IdCtiEtsProofOfCreation_95; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdCtiEtsProofOfCreation_95() { return &___IdCtiEtsProofOfCreation_95; }
	inline void set_IdCtiEtsProofOfCreation_95(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdCtiEtsProofOfCreation_95 = value;
		Il2CppCodeGenWriteBarrier((&___IdCtiEtsProofOfCreation_95), value);
	}

	inline static int32_t get_offset_of_IdAAOid_97() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAOid_97)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAOid_97() const { return ___IdAAOid_97; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAOid_97() { return &___IdAAOid_97; }
	inline void set_IdAAOid_97(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAOid_97 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAOid_97), value);
	}

	inline static int32_t get_offset_of_IdAAContentHint_98() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAContentHint_98)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAContentHint_98() const { return ___IdAAContentHint_98; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAContentHint_98() { return &___IdAAContentHint_98; }
	inline void set_IdAAContentHint_98(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAContentHint_98 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAContentHint_98), value);
	}

	inline static int32_t get_offset_of_IdAAMsgSigDigest_99() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAMsgSigDigest_99)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAMsgSigDigest_99() const { return ___IdAAMsgSigDigest_99; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAMsgSigDigest_99() { return &___IdAAMsgSigDigest_99; }
	inline void set_IdAAMsgSigDigest_99(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAMsgSigDigest_99 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAMsgSigDigest_99), value);
	}

	inline static int32_t get_offset_of_IdAAContentReference_100() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAContentReference_100)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAContentReference_100() const { return ___IdAAContentReference_100; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAContentReference_100() { return &___IdAAContentReference_100; }
	inline void set_IdAAContentReference_100(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAContentReference_100 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAContentReference_100), value);
	}

	inline static int32_t get_offset_of_IdAAEncrypKeyPref_101() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEncrypKeyPref_101)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEncrypKeyPref_101() const { return ___IdAAEncrypKeyPref_101; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEncrypKeyPref_101() { return &___IdAAEncrypKeyPref_101; }
	inline void set_IdAAEncrypKeyPref_101(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEncrypKeyPref_101 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEncrypKeyPref_101), value);
	}

	inline static int32_t get_offset_of_IdAASigningCertificate_102() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAASigningCertificate_102)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAASigningCertificate_102() const { return ___IdAASigningCertificate_102; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAASigningCertificate_102() { return &___IdAASigningCertificate_102; }
	inline void set_IdAASigningCertificate_102(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAASigningCertificate_102 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASigningCertificate_102), value);
	}

	inline static int32_t get_offset_of_IdAASigningCertificateV2_103() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAASigningCertificateV2_103)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAASigningCertificateV2_103() const { return ___IdAASigningCertificateV2_103; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAASigningCertificateV2_103() { return &___IdAASigningCertificateV2_103; }
	inline void set_IdAASigningCertificateV2_103(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAASigningCertificateV2_103 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASigningCertificateV2_103), value);
	}

	inline static int32_t get_offset_of_IdAAContentIdentifier_104() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAContentIdentifier_104)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAContentIdentifier_104() const { return ___IdAAContentIdentifier_104; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAContentIdentifier_104() { return &___IdAAContentIdentifier_104; }
	inline void set_IdAAContentIdentifier_104(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAContentIdentifier_104 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAContentIdentifier_104), value);
	}

	inline static int32_t get_offset_of_IdAASignatureTimeStampToken_105() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAASignatureTimeStampToken_105)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAASignatureTimeStampToken_105() const { return ___IdAASignatureTimeStampToken_105; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAASignatureTimeStampToken_105() { return &___IdAASignatureTimeStampToken_105; }
	inline void set_IdAASignatureTimeStampToken_105(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAASignatureTimeStampToken_105 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASignatureTimeStampToken_105), value);
	}

	inline static int32_t get_offset_of_IdAAEtsSigPolicyID_106() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsSigPolicyID_106)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsSigPolicyID_106() const { return ___IdAAEtsSigPolicyID_106; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsSigPolicyID_106() { return &___IdAAEtsSigPolicyID_106; }
	inline void set_IdAAEtsSigPolicyID_106(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsSigPolicyID_106 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsSigPolicyID_106), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCommitmentType_107() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsCommitmentType_107)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsCommitmentType_107() const { return ___IdAAEtsCommitmentType_107; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsCommitmentType_107() { return &___IdAAEtsCommitmentType_107; }
	inline void set_IdAAEtsCommitmentType_107(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsCommitmentType_107 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCommitmentType_107), value);
	}

	inline static int32_t get_offset_of_IdAAEtsSignerLocation_108() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsSignerLocation_108)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsSignerLocation_108() const { return ___IdAAEtsSignerLocation_108; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsSignerLocation_108() { return &___IdAAEtsSignerLocation_108; }
	inline void set_IdAAEtsSignerLocation_108(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsSignerLocation_108 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsSignerLocation_108), value);
	}

	inline static int32_t get_offset_of_IdAAEtsSignerAttr_109() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsSignerAttr_109)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsSignerAttr_109() const { return ___IdAAEtsSignerAttr_109; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsSignerAttr_109() { return &___IdAAEtsSignerAttr_109; }
	inline void set_IdAAEtsSignerAttr_109(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsSignerAttr_109 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsSignerAttr_109), value);
	}

	inline static int32_t get_offset_of_IdAAEtsOtherSigCert_110() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsOtherSigCert_110)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsOtherSigCert_110() const { return ___IdAAEtsOtherSigCert_110; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsOtherSigCert_110() { return &___IdAAEtsOtherSigCert_110; }
	inline void set_IdAAEtsOtherSigCert_110(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsOtherSigCert_110 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsOtherSigCert_110), value);
	}

	inline static int32_t get_offset_of_IdAAEtsContentTimestamp_111() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsContentTimestamp_111)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsContentTimestamp_111() const { return ___IdAAEtsContentTimestamp_111; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsContentTimestamp_111() { return &___IdAAEtsContentTimestamp_111; }
	inline void set_IdAAEtsContentTimestamp_111(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsContentTimestamp_111 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsContentTimestamp_111), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCertificateRefs_112() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsCertificateRefs_112)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsCertificateRefs_112() const { return ___IdAAEtsCertificateRefs_112; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsCertificateRefs_112() { return &___IdAAEtsCertificateRefs_112; }
	inline void set_IdAAEtsCertificateRefs_112(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsCertificateRefs_112 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCertificateRefs_112), value);
	}

	inline static int32_t get_offset_of_IdAAEtsRevocationRefs_113() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsRevocationRefs_113)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsRevocationRefs_113() const { return ___IdAAEtsRevocationRefs_113; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsRevocationRefs_113() { return &___IdAAEtsRevocationRefs_113; }
	inline void set_IdAAEtsRevocationRefs_113(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsRevocationRefs_113 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsRevocationRefs_113), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCertValues_114() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsCertValues_114)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsCertValues_114() const { return ___IdAAEtsCertValues_114; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsCertValues_114() { return &___IdAAEtsCertValues_114; }
	inline void set_IdAAEtsCertValues_114(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsCertValues_114 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCertValues_114), value);
	}

	inline static int32_t get_offset_of_IdAAEtsRevocationValues_115() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsRevocationValues_115)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsRevocationValues_115() const { return ___IdAAEtsRevocationValues_115; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsRevocationValues_115() { return &___IdAAEtsRevocationValues_115; }
	inline void set_IdAAEtsRevocationValues_115(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsRevocationValues_115 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsRevocationValues_115), value);
	}

	inline static int32_t get_offset_of_IdAAEtsEscTimeStamp_116() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsEscTimeStamp_116)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsEscTimeStamp_116() const { return ___IdAAEtsEscTimeStamp_116; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsEscTimeStamp_116() { return &___IdAAEtsEscTimeStamp_116; }
	inline void set_IdAAEtsEscTimeStamp_116(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsEscTimeStamp_116 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsEscTimeStamp_116), value);
	}

	inline static int32_t get_offset_of_IdAAEtsCertCrlTimestamp_117() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsCertCrlTimestamp_117)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsCertCrlTimestamp_117() const { return ___IdAAEtsCertCrlTimestamp_117; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsCertCrlTimestamp_117() { return &___IdAAEtsCertCrlTimestamp_117; }
	inline void set_IdAAEtsCertCrlTimestamp_117(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsCertCrlTimestamp_117 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsCertCrlTimestamp_117), value);
	}

	inline static int32_t get_offset_of_IdAAEtsArchiveTimestamp_118() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAEtsArchiveTimestamp_118)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAEtsArchiveTimestamp_118() const { return ___IdAAEtsArchiveTimestamp_118; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAEtsArchiveTimestamp_118() { return &___IdAAEtsArchiveTimestamp_118; }
	inline void set_IdAAEtsArchiveTimestamp_118(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAEtsArchiveTimestamp_118 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAEtsArchiveTimestamp_118), value);
	}

	inline static int32_t get_offset_of_IdAADecryptKeyID_119() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAADecryptKeyID_119)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAADecryptKeyID_119() const { return ___IdAADecryptKeyID_119; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAADecryptKeyID_119() { return &___IdAADecryptKeyID_119; }
	inline void set_IdAADecryptKeyID_119(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAADecryptKeyID_119 = value;
		Il2CppCodeGenWriteBarrier((&___IdAADecryptKeyID_119), value);
	}

	inline static int32_t get_offset_of_IdAAImplCryptoAlgs_120() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAImplCryptoAlgs_120)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAImplCryptoAlgs_120() const { return ___IdAAImplCryptoAlgs_120; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAImplCryptoAlgs_120() { return &___IdAAImplCryptoAlgs_120; }
	inline void set_IdAAImplCryptoAlgs_120(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAImplCryptoAlgs_120 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAImplCryptoAlgs_120), value);
	}

	inline static int32_t get_offset_of_IdAAAsymmDecryptKeyID_121() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAAsymmDecryptKeyID_121)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAAsymmDecryptKeyID_121() const { return ___IdAAAsymmDecryptKeyID_121; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAAsymmDecryptKeyID_121() { return &___IdAAAsymmDecryptKeyID_121; }
	inline void set_IdAAAsymmDecryptKeyID_121(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAAsymmDecryptKeyID_121 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAAsymmDecryptKeyID_121), value);
	}

	inline static int32_t get_offset_of_IdAAImplCompressAlgs_122() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAImplCompressAlgs_122)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAImplCompressAlgs_122() const { return ___IdAAImplCompressAlgs_122; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAImplCompressAlgs_122() { return &___IdAAImplCompressAlgs_122; }
	inline void set_IdAAImplCompressAlgs_122(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAImplCompressAlgs_122 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAImplCompressAlgs_122), value);
	}

	inline static int32_t get_offset_of_IdAACommunityIdentifiers_123() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAACommunityIdentifiers_123)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAACommunityIdentifiers_123() const { return ___IdAACommunityIdentifiers_123; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAACommunityIdentifiers_123() { return &___IdAACommunityIdentifiers_123; }
	inline void set_IdAACommunityIdentifiers_123(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAACommunityIdentifiers_123 = value;
		Il2CppCodeGenWriteBarrier((&___IdAACommunityIdentifiers_123), value);
	}

	inline static int32_t get_offset_of_IdAASigPolicyID_124() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAASigPolicyID_124)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAASigPolicyID_124() const { return ___IdAASigPolicyID_124; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAASigPolicyID_124() { return &___IdAASigPolicyID_124; }
	inline void set_IdAASigPolicyID_124(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAASigPolicyID_124 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASigPolicyID_124), value);
	}

	inline static int32_t get_offset_of_IdAACommitmentType_125() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAACommitmentType_125)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAACommitmentType_125() const { return ___IdAACommitmentType_125; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAACommitmentType_125() { return &___IdAACommitmentType_125; }
	inline void set_IdAACommitmentType_125(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAACommitmentType_125 = value;
		Il2CppCodeGenWriteBarrier((&___IdAACommitmentType_125), value);
	}

	inline static int32_t get_offset_of_IdAASignerLocation_126() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAASignerLocation_126)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAASignerLocation_126() const { return ___IdAASignerLocation_126; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAASignerLocation_126() { return &___IdAASignerLocation_126; }
	inline void set_IdAASignerLocation_126(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAASignerLocation_126 = value;
		Il2CppCodeGenWriteBarrier((&___IdAASignerLocation_126), value);
	}

	inline static int32_t get_offset_of_IdAAOtherSigCert_127() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdAAOtherSigCert_127)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdAAOtherSigCert_127() const { return ___IdAAOtherSigCert_127; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdAAOtherSigCert_127() { return &___IdAAOtherSigCert_127; }
	inline void set_IdAAOtherSigCert_127(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdAAOtherSigCert_127 = value;
		Il2CppCodeGenWriteBarrier((&___IdAAOtherSigCert_127), value);
	}

	inline static int32_t get_offset_of_IdSpqEtsUri_129() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdSpqEtsUri_129)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSpqEtsUri_129() const { return ___IdSpqEtsUri_129; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSpqEtsUri_129() { return &___IdSpqEtsUri_129; }
	inline void set_IdSpqEtsUri_129(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSpqEtsUri_129 = value;
		Il2CppCodeGenWriteBarrier((&___IdSpqEtsUri_129), value);
	}

	inline static int32_t get_offset_of_IdSpqEtsUNotice_130() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___IdSpqEtsUNotice_130)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdSpqEtsUNotice_130() const { return ___IdSpqEtsUNotice_130; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdSpqEtsUNotice_130() { return &___IdSpqEtsUNotice_130; }
	inline void set_IdSpqEtsUNotice_130(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdSpqEtsUNotice_130 = value;
		Il2CppCodeGenWriteBarrier((&___IdSpqEtsUNotice_130), value);
	}

	inline static int32_t get_offset_of_KeyBag_133() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___KeyBag_133)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_KeyBag_133() const { return ___KeyBag_133; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_KeyBag_133() { return &___KeyBag_133; }
	inline void set_KeyBag_133(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___KeyBag_133 = value;
		Il2CppCodeGenWriteBarrier((&___KeyBag_133), value);
	}

	inline static int32_t get_offset_of_Pkcs8ShroudedKeyBag_134() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___Pkcs8ShroudedKeyBag_134)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Pkcs8ShroudedKeyBag_134() const { return ___Pkcs8ShroudedKeyBag_134; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Pkcs8ShroudedKeyBag_134() { return &___Pkcs8ShroudedKeyBag_134; }
	inline void set_Pkcs8ShroudedKeyBag_134(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Pkcs8ShroudedKeyBag_134 = value;
		Il2CppCodeGenWriteBarrier((&___Pkcs8ShroudedKeyBag_134), value);
	}

	inline static int32_t get_offset_of_CertBag_135() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___CertBag_135)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CertBag_135() const { return ___CertBag_135; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CertBag_135() { return &___CertBag_135; }
	inline void set_CertBag_135(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CertBag_135 = value;
		Il2CppCodeGenWriteBarrier((&___CertBag_135), value);
	}

	inline static int32_t get_offset_of_CrlBag_136() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___CrlBag_136)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CrlBag_136() const { return ___CrlBag_136; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CrlBag_136() { return &___CrlBag_136; }
	inline void set_CrlBag_136(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CrlBag_136 = value;
		Il2CppCodeGenWriteBarrier((&___CrlBag_136), value);
	}

	inline static int32_t get_offset_of_SecretBag_137() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SecretBag_137)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecretBag_137() const { return ___SecretBag_137; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecretBag_137() { return &___SecretBag_137; }
	inline void set_SecretBag_137(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecretBag_137 = value;
		Il2CppCodeGenWriteBarrier((&___SecretBag_137), value);
	}

	inline static int32_t get_offset_of_SafeContentsBag_138() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___SafeContentsBag_138)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SafeContentsBag_138() const { return ___SafeContentsBag_138; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SafeContentsBag_138() { return &___SafeContentsBag_138; }
	inline void set_SafeContentsBag_138(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SafeContentsBag_138 = value;
		Il2CppCodeGenWriteBarrier((&___SafeContentsBag_138), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd128BitRC4_140() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithShaAnd128BitRC4_140)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithShaAnd128BitRC4_140() const { return ___PbeWithShaAnd128BitRC4_140; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithShaAnd128BitRC4_140() { return &___PbeWithShaAnd128BitRC4_140; }
	inline void set_PbeWithShaAnd128BitRC4_140(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithShaAnd128BitRC4_140 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd128BitRC4_140), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd40BitRC4_141() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithShaAnd40BitRC4_141)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithShaAnd40BitRC4_141() const { return ___PbeWithShaAnd40BitRC4_141; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithShaAnd40BitRC4_141() { return &___PbeWithShaAnd40BitRC4_141; }
	inline void set_PbeWithShaAnd40BitRC4_141(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithShaAnd40BitRC4_141 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd40BitRC4_141), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd3KeyTripleDesCbc_142() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithShaAnd3KeyTripleDesCbc_142)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithShaAnd3KeyTripleDesCbc_142() const { return ___PbeWithShaAnd3KeyTripleDesCbc_142; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithShaAnd3KeyTripleDesCbc_142() { return &___PbeWithShaAnd3KeyTripleDesCbc_142; }
	inline void set_PbeWithShaAnd3KeyTripleDesCbc_142(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithShaAnd3KeyTripleDesCbc_142 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd3KeyTripleDesCbc_142), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd2KeyTripleDesCbc_143() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithShaAnd2KeyTripleDesCbc_143)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithShaAnd2KeyTripleDesCbc_143() const { return ___PbeWithShaAnd2KeyTripleDesCbc_143; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithShaAnd2KeyTripleDesCbc_143() { return &___PbeWithShaAnd2KeyTripleDesCbc_143; }
	inline void set_PbeWithShaAnd2KeyTripleDesCbc_143(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithShaAnd2KeyTripleDesCbc_143 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd2KeyTripleDesCbc_143), value);
	}

	inline static int32_t get_offset_of_PbeWithShaAnd128BitRC2Cbc_144() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbeWithShaAnd128BitRC2Cbc_144)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbeWithShaAnd128BitRC2Cbc_144() const { return ___PbeWithShaAnd128BitRC2Cbc_144; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbeWithShaAnd128BitRC2Cbc_144() { return &___PbeWithShaAnd128BitRC2Cbc_144; }
	inline void set_PbeWithShaAnd128BitRC2Cbc_144(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbeWithShaAnd128BitRC2Cbc_144 = value;
		Il2CppCodeGenWriteBarrier((&___PbeWithShaAnd128BitRC2Cbc_144), value);
	}

	inline static int32_t get_offset_of_PbewithShaAnd40BitRC2Cbc_145() { return static_cast<int32_t>(offsetof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields, ___PbewithShaAnd40BitRC2Cbc_145)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PbewithShaAnd40BitRC2Cbc_145() const { return ___PbewithShaAnd40BitRC2Cbc_145; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PbewithShaAnd40BitRC2Cbc_145() { return &___PbewithShaAnd40BitRC2Cbc_145; }
	inline void set_PbewithShaAnd40BitRC2Cbc_145(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PbewithShaAnd40BitRC2Cbc_145 = value;
		Il2CppCodeGenWriteBarrier((&___PbewithShaAnd40BitRC2Cbc_145), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCSOBJECTIDENTIFIERS_T3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_H
#ifndef ROSSTANDARTOBJECTIDENTIFIERS_T07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_H
#define ROSSTANDARTOBJECTIDENTIFIERS_T07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers
struct  RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C  : public RuntimeObject
{
public:

public:
};

struct RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::rosstandart
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___rosstandart_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3411_12_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3411_12_256_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3411_12_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3411_12_512_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_hmac_gost_3411_12_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_hmac_gost_3411_12_256_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_hmac_gost_3411_12_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_hmac_gost_3411_12_512_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_256_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_512_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_signwithdigest_gost_3410_12_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_signwithdigest_gost_3410_12_256_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_signwithdigest_gost_3410_12_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_signwithdigest_gost_3410_12_512_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_agreement
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_agreement_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_agreement_gost_3410_12_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_agreement_gost_3410_12_256_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_agreement_gost_3410_12_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_agreement_gost_3410_12_512_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_256_paramSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_256_paramSet_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_256_paramSetA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_256_paramSetA_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_512_paramSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_512_paramSet_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_512_paramSetA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_512_paramSetA_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_512_paramSetB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_512_paramSetB_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_3410_12_512_paramSetC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_3410_12_512_paramSetC_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Rosstandart.RosstandartObjectIdentifiers::id_tc26_gost_28147_param_Z
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_tc26_gost_28147_param_Z_19;

public:
	inline static int32_t get_offset_of_rosstandart_0() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___rosstandart_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_rosstandart_0() const { return ___rosstandart_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_rosstandart_0() { return &___rosstandart_0; }
	inline void set_rosstandart_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___rosstandart_0 = value;
		Il2CppCodeGenWriteBarrier((&___rosstandart_0), value);
	}

	inline static int32_t get_offset_of_id_tc26_1() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_1() const { return ___id_tc26_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_1() { return &___id_tc26_1; }
	inline void set_id_tc26_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_1), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3411_12_256_2() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3411_12_256_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3411_12_256_2() const { return ___id_tc26_gost_3411_12_256_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3411_12_256_2() { return &___id_tc26_gost_3411_12_256_2; }
	inline void set_id_tc26_gost_3411_12_256_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3411_12_256_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3411_12_256_2), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3411_12_512_3() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3411_12_512_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3411_12_512_3() const { return ___id_tc26_gost_3411_12_512_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3411_12_512_3() { return &___id_tc26_gost_3411_12_512_3; }
	inline void set_id_tc26_gost_3411_12_512_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3411_12_512_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3411_12_512_3), value);
	}

	inline static int32_t get_offset_of_id_tc26_hmac_gost_3411_12_256_4() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_hmac_gost_3411_12_256_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_hmac_gost_3411_12_256_4() const { return ___id_tc26_hmac_gost_3411_12_256_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_hmac_gost_3411_12_256_4() { return &___id_tc26_hmac_gost_3411_12_256_4; }
	inline void set_id_tc26_hmac_gost_3411_12_256_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_hmac_gost_3411_12_256_4 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_hmac_gost_3411_12_256_4), value);
	}

	inline static int32_t get_offset_of_id_tc26_hmac_gost_3411_12_512_5() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_hmac_gost_3411_12_512_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_hmac_gost_3411_12_512_5() const { return ___id_tc26_hmac_gost_3411_12_512_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_hmac_gost_3411_12_512_5() { return &___id_tc26_hmac_gost_3411_12_512_5; }
	inline void set_id_tc26_hmac_gost_3411_12_512_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_hmac_gost_3411_12_512_5 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_hmac_gost_3411_12_512_5), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_256_6() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_256_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_256_6() const { return ___id_tc26_gost_3410_12_256_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_256_6() { return &___id_tc26_gost_3410_12_256_6; }
	inline void set_id_tc26_gost_3410_12_256_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_256_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_256_6), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_512_7() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_512_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_512_7() const { return ___id_tc26_gost_3410_12_512_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_512_7() { return &___id_tc26_gost_3410_12_512_7; }
	inline void set_id_tc26_gost_3410_12_512_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_512_7 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_512_7), value);
	}

	inline static int32_t get_offset_of_id_tc26_signwithdigest_gost_3410_12_256_8() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_signwithdigest_gost_3410_12_256_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_signwithdigest_gost_3410_12_256_8() const { return ___id_tc26_signwithdigest_gost_3410_12_256_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_signwithdigest_gost_3410_12_256_8() { return &___id_tc26_signwithdigest_gost_3410_12_256_8; }
	inline void set_id_tc26_signwithdigest_gost_3410_12_256_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_signwithdigest_gost_3410_12_256_8 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_signwithdigest_gost_3410_12_256_8), value);
	}

	inline static int32_t get_offset_of_id_tc26_signwithdigest_gost_3410_12_512_9() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_signwithdigest_gost_3410_12_512_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_signwithdigest_gost_3410_12_512_9() const { return ___id_tc26_signwithdigest_gost_3410_12_512_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_signwithdigest_gost_3410_12_512_9() { return &___id_tc26_signwithdigest_gost_3410_12_512_9; }
	inline void set_id_tc26_signwithdigest_gost_3410_12_512_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_signwithdigest_gost_3410_12_512_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_signwithdigest_gost_3410_12_512_9), value);
	}

	inline static int32_t get_offset_of_id_tc26_agreement_10() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_agreement_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_agreement_10() const { return ___id_tc26_agreement_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_agreement_10() { return &___id_tc26_agreement_10; }
	inline void set_id_tc26_agreement_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_agreement_10 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_agreement_10), value);
	}

	inline static int32_t get_offset_of_id_tc26_agreement_gost_3410_12_256_11() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_agreement_gost_3410_12_256_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_agreement_gost_3410_12_256_11() const { return ___id_tc26_agreement_gost_3410_12_256_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_agreement_gost_3410_12_256_11() { return &___id_tc26_agreement_gost_3410_12_256_11; }
	inline void set_id_tc26_agreement_gost_3410_12_256_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_agreement_gost_3410_12_256_11 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_agreement_gost_3410_12_256_11), value);
	}

	inline static int32_t get_offset_of_id_tc26_agreement_gost_3410_12_512_12() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_agreement_gost_3410_12_512_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_agreement_gost_3410_12_512_12() const { return ___id_tc26_agreement_gost_3410_12_512_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_agreement_gost_3410_12_512_12() { return &___id_tc26_agreement_gost_3410_12_512_12; }
	inline void set_id_tc26_agreement_gost_3410_12_512_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_agreement_gost_3410_12_512_12 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_agreement_gost_3410_12_512_12), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_256_paramSet_13() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_256_paramSet_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_256_paramSet_13() const { return ___id_tc26_gost_3410_12_256_paramSet_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_256_paramSet_13() { return &___id_tc26_gost_3410_12_256_paramSet_13; }
	inline void set_id_tc26_gost_3410_12_256_paramSet_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_256_paramSet_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_256_paramSet_13), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_256_paramSetA_14() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_256_paramSetA_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_256_paramSetA_14() const { return ___id_tc26_gost_3410_12_256_paramSetA_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_256_paramSetA_14() { return &___id_tc26_gost_3410_12_256_paramSetA_14; }
	inline void set_id_tc26_gost_3410_12_256_paramSetA_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_256_paramSetA_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_256_paramSetA_14), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_512_paramSet_15() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_512_paramSet_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_512_paramSet_15() const { return ___id_tc26_gost_3410_12_512_paramSet_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_512_paramSet_15() { return &___id_tc26_gost_3410_12_512_paramSet_15; }
	inline void set_id_tc26_gost_3410_12_512_paramSet_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_512_paramSet_15 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_512_paramSet_15), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_512_paramSetA_16() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_512_paramSetA_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_512_paramSetA_16() const { return ___id_tc26_gost_3410_12_512_paramSetA_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_512_paramSetA_16() { return &___id_tc26_gost_3410_12_512_paramSetA_16; }
	inline void set_id_tc26_gost_3410_12_512_paramSetA_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_512_paramSetA_16 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_512_paramSetA_16), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_512_paramSetB_17() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_512_paramSetB_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_512_paramSetB_17() const { return ___id_tc26_gost_3410_12_512_paramSetB_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_512_paramSetB_17() { return &___id_tc26_gost_3410_12_512_paramSetB_17; }
	inline void set_id_tc26_gost_3410_12_512_paramSetB_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_512_paramSetB_17 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_512_paramSetB_17), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_3410_12_512_paramSetC_18() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_3410_12_512_paramSetC_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_3410_12_512_paramSetC_18() const { return ___id_tc26_gost_3410_12_512_paramSetC_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_3410_12_512_paramSetC_18() { return &___id_tc26_gost_3410_12_512_paramSetC_18; }
	inline void set_id_tc26_gost_3410_12_512_paramSetC_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_3410_12_512_paramSetC_18 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_3410_12_512_paramSetC_18), value);
	}

	inline static int32_t get_offset_of_id_tc26_gost_28147_param_Z_19() { return static_cast<int32_t>(offsetof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields, ___id_tc26_gost_28147_param_Z_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_tc26_gost_28147_param_Z_19() const { return ___id_tc26_gost_28147_param_Z_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_tc26_gost_28147_param_Z_19() { return &___id_tc26_gost_28147_param_Z_19; }
	inline void set_id_tc26_gost_28147_param_Z_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_tc26_gost_28147_param_Z_19 = value;
		Il2CppCodeGenWriteBarrier((&___id_tc26_gost_28147_param_Z_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROSSTANDARTOBJECTIDENTIFIERS_T07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_H
#ifndef SECOBJECTIDENTIFIERS_T2823853D647F566400C5ABCDBAF12213293859F8_H
#define SECOBJECTIDENTIFIERS_T2823853D647F566400C5ABCDBAF12213293859F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers
struct  SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8  : public RuntimeObject
{
public:

public:
};

struct SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EllipticCurve_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT163k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT163k1_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT163r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT163r1_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT239k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT239k1_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT113r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT113r1_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT113r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT113r2_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP112r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP112r1_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP112r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP112r2_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP160r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP160r1_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP160k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP160k1_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP256k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP256k1_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT163r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT163r2_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT283k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT283k1_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT283r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT283r1_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT131r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT131r1_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT131r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT131r2_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT193r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT193r1_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT193r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT193r2_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT233k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT233k1_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT233r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT233r1_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP128r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP128r1_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP128r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP128r2_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP160r2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP160r2_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP192k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP192k1_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP224k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP224k1_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP224r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP224r1_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP384r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP384r1_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP521r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP521r1_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT409k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT409k1_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT409r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT409r1_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT571k1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT571k1_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecT571r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecT571r1_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP192r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP192r1_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecObjectIdentifiers::SecP256r1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SecP256r1_33;

public:
	inline static int32_t get_offset_of_EllipticCurve_0() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___EllipticCurve_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EllipticCurve_0() const { return ___EllipticCurve_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EllipticCurve_0() { return &___EllipticCurve_0; }
	inline void set_EllipticCurve_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EllipticCurve_0 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_0), value);
	}

	inline static int32_t get_offset_of_SecT163k1_1() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT163k1_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT163k1_1() const { return ___SecT163k1_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT163k1_1() { return &___SecT163k1_1; }
	inline void set_SecT163k1_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT163k1_1 = value;
		Il2CppCodeGenWriteBarrier((&___SecT163k1_1), value);
	}

	inline static int32_t get_offset_of_SecT163r1_2() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT163r1_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT163r1_2() const { return ___SecT163r1_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT163r1_2() { return &___SecT163r1_2; }
	inline void set_SecT163r1_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT163r1_2 = value;
		Il2CppCodeGenWriteBarrier((&___SecT163r1_2), value);
	}

	inline static int32_t get_offset_of_SecT239k1_3() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT239k1_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT239k1_3() const { return ___SecT239k1_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT239k1_3() { return &___SecT239k1_3; }
	inline void set_SecT239k1_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT239k1_3 = value;
		Il2CppCodeGenWriteBarrier((&___SecT239k1_3), value);
	}

	inline static int32_t get_offset_of_SecT113r1_4() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT113r1_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT113r1_4() const { return ___SecT113r1_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT113r1_4() { return &___SecT113r1_4; }
	inline void set_SecT113r1_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT113r1_4 = value;
		Il2CppCodeGenWriteBarrier((&___SecT113r1_4), value);
	}

	inline static int32_t get_offset_of_SecT113r2_5() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT113r2_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT113r2_5() const { return ___SecT113r2_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT113r2_5() { return &___SecT113r2_5; }
	inline void set_SecT113r2_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT113r2_5 = value;
		Il2CppCodeGenWriteBarrier((&___SecT113r2_5), value);
	}

	inline static int32_t get_offset_of_SecP112r1_6() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP112r1_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP112r1_6() const { return ___SecP112r1_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP112r1_6() { return &___SecP112r1_6; }
	inline void set_SecP112r1_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP112r1_6 = value;
		Il2CppCodeGenWriteBarrier((&___SecP112r1_6), value);
	}

	inline static int32_t get_offset_of_SecP112r2_7() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP112r2_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP112r2_7() const { return ___SecP112r2_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP112r2_7() { return &___SecP112r2_7; }
	inline void set_SecP112r2_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP112r2_7 = value;
		Il2CppCodeGenWriteBarrier((&___SecP112r2_7), value);
	}

	inline static int32_t get_offset_of_SecP160r1_8() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP160r1_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP160r1_8() const { return ___SecP160r1_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP160r1_8() { return &___SecP160r1_8; }
	inline void set_SecP160r1_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP160r1_8 = value;
		Il2CppCodeGenWriteBarrier((&___SecP160r1_8), value);
	}

	inline static int32_t get_offset_of_SecP160k1_9() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP160k1_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP160k1_9() const { return ___SecP160k1_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP160k1_9() { return &___SecP160k1_9; }
	inline void set_SecP160k1_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP160k1_9 = value;
		Il2CppCodeGenWriteBarrier((&___SecP160k1_9), value);
	}

	inline static int32_t get_offset_of_SecP256k1_10() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP256k1_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP256k1_10() const { return ___SecP256k1_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP256k1_10() { return &___SecP256k1_10; }
	inline void set_SecP256k1_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP256k1_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecP256k1_10), value);
	}

	inline static int32_t get_offset_of_SecT163r2_11() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT163r2_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT163r2_11() const { return ___SecT163r2_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT163r2_11() { return &___SecT163r2_11; }
	inline void set_SecT163r2_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT163r2_11 = value;
		Il2CppCodeGenWriteBarrier((&___SecT163r2_11), value);
	}

	inline static int32_t get_offset_of_SecT283k1_12() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT283k1_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT283k1_12() const { return ___SecT283k1_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT283k1_12() { return &___SecT283k1_12; }
	inline void set_SecT283k1_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT283k1_12 = value;
		Il2CppCodeGenWriteBarrier((&___SecT283k1_12), value);
	}

	inline static int32_t get_offset_of_SecT283r1_13() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT283r1_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT283r1_13() const { return ___SecT283r1_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT283r1_13() { return &___SecT283r1_13; }
	inline void set_SecT283r1_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT283r1_13 = value;
		Il2CppCodeGenWriteBarrier((&___SecT283r1_13), value);
	}

	inline static int32_t get_offset_of_SecT131r1_14() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT131r1_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT131r1_14() const { return ___SecT131r1_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT131r1_14() { return &___SecT131r1_14; }
	inline void set_SecT131r1_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT131r1_14 = value;
		Il2CppCodeGenWriteBarrier((&___SecT131r1_14), value);
	}

	inline static int32_t get_offset_of_SecT131r2_15() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT131r2_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT131r2_15() const { return ___SecT131r2_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT131r2_15() { return &___SecT131r2_15; }
	inline void set_SecT131r2_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT131r2_15 = value;
		Il2CppCodeGenWriteBarrier((&___SecT131r2_15), value);
	}

	inline static int32_t get_offset_of_SecT193r1_16() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT193r1_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT193r1_16() const { return ___SecT193r1_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT193r1_16() { return &___SecT193r1_16; }
	inline void set_SecT193r1_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT193r1_16 = value;
		Il2CppCodeGenWriteBarrier((&___SecT193r1_16), value);
	}

	inline static int32_t get_offset_of_SecT193r2_17() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT193r2_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT193r2_17() const { return ___SecT193r2_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT193r2_17() { return &___SecT193r2_17; }
	inline void set_SecT193r2_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT193r2_17 = value;
		Il2CppCodeGenWriteBarrier((&___SecT193r2_17), value);
	}

	inline static int32_t get_offset_of_SecT233k1_18() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT233k1_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT233k1_18() const { return ___SecT233k1_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT233k1_18() { return &___SecT233k1_18; }
	inline void set_SecT233k1_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT233k1_18 = value;
		Il2CppCodeGenWriteBarrier((&___SecT233k1_18), value);
	}

	inline static int32_t get_offset_of_SecT233r1_19() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT233r1_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT233r1_19() const { return ___SecT233r1_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT233r1_19() { return &___SecT233r1_19; }
	inline void set_SecT233r1_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT233r1_19 = value;
		Il2CppCodeGenWriteBarrier((&___SecT233r1_19), value);
	}

	inline static int32_t get_offset_of_SecP128r1_20() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP128r1_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP128r1_20() const { return ___SecP128r1_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP128r1_20() { return &___SecP128r1_20; }
	inline void set_SecP128r1_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP128r1_20 = value;
		Il2CppCodeGenWriteBarrier((&___SecP128r1_20), value);
	}

	inline static int32_t get_offset_of_SecP128r2_21() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP128r2_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP128r2_21() const { return ___SecP128r2_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP128r2_21() { return &___SecP128r2_21; }
	inline void set_SecP128r2_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP128r2_21 = value;
		Il2CppCodeGenWriteBarrier((&___SecP128r2_21), value);
	}

	inline static int32_t get_offset_of_SecP160r2_22() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP160r2_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP160r2_22() const { return ___SecP160r2_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP160r2_22() { return &___SecP160r2_22; }
	inline void set_SecP160r2_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP160r2_22 = value;
		Il2CppCodeGenWriteBarrier((&___SecP160r2_22), value);
	}

	inline static int32_t get_offset_of_SecP192k1_23() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP192k1_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP192k1_23() const { return ___SecP192k1_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP192k1_23() { return &___SecP192k1_23; }
	inline void set_SecP192k1_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP192k1_23 = value;
		Il2CppCodeGenWriteBarrier((&___SecP192k1_23), value);
	}

	inline static int32_t get_offset_of_SecP224k1_24() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP224k1_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP224k1_24() const { return ___SecP224k1_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP224k1_24() { return &___SecP224k1_24; }
	inline void set_SecP224k1_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP224k1_24 = value;
		Il2CppCodeGenWriteBarrier((&___SecP224k1_24), value);
	}

	inline static int32_t get_offset_of_SecP224r1_25() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP224r1_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP224r1_25() const { return ___SecP224r1_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP224r1_25() { return &___SecP224r1_25; }
	inline void set_SecP224r1_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP224r1_25 = value;
		Il2CppCodeGenWriteBarrier((&___SecP224r1_25), value);
	}

	inline static int32_t get_offset_of_SecP384r1_26() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP384r1_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP384r1_26() const { return ___SecP384r1_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP384r1_26() { return &___SecP384r1_26; }
	inline void set_SecP384r1_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP384r1_26 = value;
		Il2CppCodeGenWriteBarrier((&___SecP384r1_26), value);
	}

	inline static int32_t get_offset_of_SecP521r1_27() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP521r1_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP521r1_27() const { return ___SecP521r1_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP521r1_27() { return &___SecP521r1_27; }
	inline void set_SecP521r1_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP521r1_27 = value;
		Il2CppCodeGenWriteBarrier((&___SecP521r1_27), value);
	}

	inline static int32_t get_offset_of_SecT409k1_28() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT409k1_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT409k1_28() const { return ___SecT409k1_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT409k1_28() { return &___SecT409k1_28; }
	inline void set_SecT409k1_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT409k1_28 = value;
		Il2CppCodeGenWriteBarrier((&___SecT409k1_28), value);
	}

	inline static int32_t get_offset_of_SecT409r1_29() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT409r1_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT409r1_29() const { return ___SecT409r1_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT409r1_29() { return &___SecT409r1_29; }
	inline void set_SecT409r1_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT409r1_29 = value;
		Il2CppCodeGenWriteBarrier((&___SecT409r1_29), value);
	}

	inline static int32_t get_offset_of_SecT571k1_30() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT571k1_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT571k1_30() const { return ___SecT571k1_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT571k1_30() { return &___SecT571k1_30; }
	inline void set_SecT571k1_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT571k1_30 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571k1_30), value);
	}

	inline static int32_t get_offset_of_SecT571r1_31() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecT571r1_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecT571r1_31() const { return ___SecT571r1_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecT571r1_31() { return &___SecT571r1_31; }
	inline void set_SecT571r1_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecT571r1_31 = value;
		Il2CppCodeGenWriteBarrier((&___SecT571r1_31), value);
	}

	inline static int32_t get_offset_of_SecP192r1_32() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP192r1_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP192r1_32() const { return ___SecP192r1_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP192r1_32() { return &___SecP192r1_32; }
	inline void set_SecP192r1_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP192r1_32 = value;
		Il2CppCodeGenWriteBarrier((&___SecP192r1_32), value);
	}

	inline static int32_t get_offset_of_SecP256r1_33() { return static_cast<int32_t>(offsetof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields, ___SecP256r1_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SecP256r1_33() const { return ___SecP256r1_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SecP256r1_33() { return &___SecP256r1_33; }
	inline void set_SecP256r1_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SecP256r1_33 = value;
		Il2CppCodeGenWriteBarrier((&___SecP256r1_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECOBJECTIDENTIFIERS_T2823853D647F566400C5ABCDBAF12213293859F8_H
#ifndef X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#define X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB, ___parameters_0)); }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef CSCAMASTERLIST_T96760FDDB5C81271096193F96B55A66115140878_H
#define CSCAMASTERLIST_T96760FDDB5C81271096193F96B55A66115140878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.CscaMasterList
struct  CscaMasterList_t96760FDDB5C81271096193F96B55A66115140878  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.CscaMasterList::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.CscaMasterList::certList
	X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637* ___certList_3;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(CscaMasterList_t96760FDDB5C81271096193F96B55A66115140878, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_certList_3() { return static_cast<int32_t>(offsetof(CscaMasterList_t96760FDDB5C81271096193F96B55A66115140878, ___certList_3)); }
	inline X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637* get_certList_3() const { return ___certList_3; }
	inline X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637** get_address_of_certList_3() { return &___certList_3; }
	inline void set_certList_3(X509CertificateStructureU5BU5D_tFA512DCED9AD4413B4F9DA3343A65860B0B91637* value)
	{
		___certList_3 = value;
		Il2CppCodeGenWriteBarrier((&___certList_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSCAMASTERLIST_T96760FDDB5C81271096193F96B55A66115140878_H
#ifndef DATAGROUPHASH_TD7B19EA56B858B032BCC6973F5ED2BA990035FAE_H
#define DATAGROUPHASH_TD7B19EA56B858B032BCC6973F5ED2BA990035FAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.DataGroupHash
struct  DataGroupHash_tD7B19EA56B858B032BCC6973F5ED2BA990035FAE  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.DataGroupHash::dataGroupNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___dataGroupNumber_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.DataGroupHash::dataGroupHashValue
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___dataGroupHashValue_3;

public:
	inline static int32_t get_offset_of_dataGroupNumber_2() { return static_cast<int32_t>(offsetof(DataGroupHash_tD7B19EA56B858B032BCC6973F5ED2BA990035FAE, ___dataGroupNumber_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_dataGroupNumber_2() const { return ___dataGroupNumber_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_dataGroupNumber_2() { return &___dataGroupNumber_2; }
	inline void set_dataGroupNumber_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___dataGroupNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___dataGroupNumber_2), value);
	}

	inline static int32_t get_offset_of_dataGroupHashValue_3() { return static_cast<int32_t>(offsetof(DataGroupHash_tD7B19EA56B858B032BCC6973F5ED2BA990035FAE, ___dataGroupHashValue_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_dataGroupHashValue_3() const { return ___dataGroupHashValue_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_dataGroupHashValue_3() { return &___dataGroupHashValue_3; }
	inline void set_dataGroupHashValue_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___dataGroupHashValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataGroupHashValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAGROUPHASH_TD7B19EA56B858B032BCC6973F5ED2BA990035FAE_H
#ifndef LDSSECURITYOBJECT_TD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838_H
#define LDSSECURITYOBJECT_TD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsSecurityObject
struct  LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsSecurityObject::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsSecurityObject::digestAlgorithmIdentifier
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digestAlgorithmIdentifier_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.DataGroupHash[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsSecurityObject::datagroupHash
	DataGroupHashU5BU5D_t7F4EC94BDD487B6A664D565DAD66ABC67137D30C* ___datagroupHash_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsVersionInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsSecurityObject::versionInfo
	LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63 * ___versionInfo_6;

public:
	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838, ___version_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_3() const { return ___version_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}

	inline static int32_t get_offset_of_digestAlgorithmIdentifier_4() { return static_cast<int32_t>(offsetof(LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838, ___digestAlgorithmIdentifier_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digestAlgorithmIdentifier_4() const { return ___digestAlgorithmIdentifier_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digestAlgorithmIdentifier_4() { return &___digestAlgorithmIdentifier_4; }
	inline void set_digestAlgorithmIdentifier_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digestAlgorithmIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithmIdentifier_4), value);
	}

	inline static int32_t get_offset_of_datagroupHash_5() { return static_cast<int32_t>(offsetof(LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838, ___datagroupHash_5)); }
	inline DataGroupHashU5BU5D_t7F4EC94BDD487B6A664D565DAD66ABC67137D30C* get_datagroupHash_5() const { return ___datagroupHash_5; }
	inline DataGroupHashU5BU5D_t7F4EC94BDD487B6A664D565DAD66ABC67137D30C** get_address_of_datagroupHash_5() { return &___datagroupHash_5; }
	inline void set_datagroupHash_5(DataGroupHashU5BU5D_t7F4EC94BDD487B6A664D565DAD66ABC67137D30C* value)
	{
		___datagroupHash_5 = value;
		Il2CppCodeGenWriteBarrier((&___datagroupHash_5), value);
	}

	inline static int32_t get_offset_of_versionInfo_6() { return static_cast<int32_t>(offsetof(LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838, ___versionInfo_6)); }
	inline LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63 * get_versionInfo_6() const { return ___versionInfo_6; }
	inline LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63 ** get_address_of_versionInfo_6() { return &___versionInfo_6; }
	inline void set_versionInfo_6(LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63 * value)
	{
		___versionInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___versionInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LDSSECURITYOBJECT_TD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838_H
#ifndef LDSVERSIONINFO_T2E232DD335C469BA0E583F9FEF848280EBED4F63_H
#define LDSVERSIONINFO_T2E232DD335C469BA0E583F9FEF848280EBED4F63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsVersionInfo
struct  LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerPrintableString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsVersionInfo::ldsVersion
	DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * ___ldsVersion_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerPrintableString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Icao.LdsVersionInfo::unicodeVersion
	DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * ___unicodeVersion_3;

public:
	inline static int32_t get_offset_of_ldsVersion_2() { return static_cast<int32_t>(offsetof(LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63, ___ldsVersion_2)); }
	inline DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * get_ldsVersion_2() const { return ___ldsVersion_2; }
	inline DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 ** get_address_of_ldsVersion_2() { return &___ldsVersion_2; }
	inline void set_ldsVersion_2(DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * value)
	{
		___ldsVersion_2 = value;
		Il2CppCodeGenWriteBarrier((&___ldsVersion_2), value);
	}

	inline static int32_t get_offset_of_unicodeVersion_3() { return static_cast<int32_t>(offsetof(LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63, ___unicodeVersion_3)); }
	inline DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * get_unicodeVersion_3() const { return ___unicodeVersion_3; }
	inline DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 ** get_address_of_unicodeVersion_3() { return &___unicodeVersion_3; }
	inline void set_unicodeVersion_3(DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * value)
	{
		___unicodeVersion_3 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeVersion_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LDSVERSIONINFO_T2E232DD335C469BA0E583F9FEF848280EBED4F63_H
#ifndef CERTHASH_T1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8_H
#define CERTHASH_T1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.CertHash
struct  CertHash_t1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.CertHash::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.CertHash::certificateHash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___certificateHash_3;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(CertHash_t1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_certificateHash_3() { return static_cast<int32_t>(offsetof(CertHash_t1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8, ___certificateHash_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_certificateHash_3() const { return ___certificateHash_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_certificateHash_3() { return &___certificateHash_3; }
	inline void set_certificateHash_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___certificateHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___certificateHash_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTHASH_T1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8_H
#ifndef REQUESTEDCERTIFICATE_T361384DE9AB65E541101A5605E023098790EEE7F_H
#define REQUESTEDCERTIFICATE_T361384DE9AB65E541101A5605E023098790EEE7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.RequestedCertificate
struct  RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509CertificateStructure BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.RequestedCertificate::cert
	X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * ___cert_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.RequestedCertificate::publicKeyCert
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___publicKeyCert_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.RequestedCertificate::attributeCert
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___attributeCert_4;

public:
	inline static int32_t get_offset_of_cert_2() { return static_cast<int32_t>(offsetof(RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F, ___cert_2)); }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * get_cert_2() const { return ___cert_2; }
	inline X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 ** get_address_of_cert_2() { return &___cert_2; }
	inline void set_cert_2(X509CertificateStructure_t76D9AE98B45AAF1A06FE9E795E97503122F17242 * value)
	{
		___cert_2 = value;
		Il2CppCodeGenWriteBarrier((&___cert_2), value);
	}

	inline static int32_t get_offset_of_publicKeyCert_3() { return static_cast<int32_t>(offsetof(RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F, ___publicKeyCert_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_publicKeyCert_3() const { return ___publicKeyCert_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_publicKeyCert_3() { return &___publicKeyCert_3; }
	inline void set_publicKeyCert_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___publicKeyCert_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyCert_3), value);
	}

	inline static int32_t get_offset_of_attributeCert_4() { return static_cast<int32_t>(offsetof(RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F, ___attributeCert_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_attributeCert_4() const { return ___attributeCert_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_attributeCert_4() { return &___attributeCert_4; }
	inline void set_attributeCert_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___attributeCert_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCert_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTEDCERTIFICATE_T361384DE9AB65E541101A5605E023098790EEE7F_H
#ifndef ADDITIONALINFORMATIONSYNTAX_T0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D_H
#define ADDITIONALINFORMATIONSYNTAX_T0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.AdditionalInformationSyntax
struct  AdditionalInformationSyntax_t0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.AdditionalInformationSyntax::information
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___information_2;

public:
	inline static int32_t get_offset_of_information_2() { return static_cast<int32_t>(offsetof(AdditionalInformationSyntax_t0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D, ___information_2)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_information_2() const { return ___information_2; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_information_2() { return &___information_2; }
	inline void set_information_2(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___information_2 = value;
		Il2CppCodeGenWriteBarrier((&___information_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDITIONALINFORMATIONSYNTAX_T0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D_H
#ifndef ADMISSIONSYNTAX_TBAB9E07D2191AEDADF4E868C165F187961AD5187_H
#define ADMISSIONSYNTAX_TBAB9E07D2191AEDADF4E868C165F187961AD5187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.AdmissionSyntax
struct  AdmissionSyntax_tBAB9E07D2191AEDADF4E868C165F187961AD5187  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.AdmissionSyntax::admissionAuthority
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___admissionAuthority_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.AdmissionSyntax::contentsOfAdmissions
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___contentsOfAdmissions_3;

public:
	inline static int32_t get_offset_of_admissionAuthority_2() { return static_cast<int32_t>(offsetof(AdmissionSyntax_tBAB9E07D2191AEDADF4E868C165F187961AD5187, ___admissionAuthority_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_admissionAuthority_2() const { return ___admissionAuthority_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_admissionAuthority_2() { return &___admissionAuthority_2; }
	inline void set_admissionAuthority_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___admissionAuthority_2 = value;
		Il2CppCodeGenWriteBarrier((&___admissionAuthority_2), value);
	}

	inline static int32_t get_offset_of_contentsOfAdmissions_3() { return static_cast<int32_t>(offsetof(AdmissionSyntax_tBAB9E07D2191AEDADF4E868C165F187961AD5187, ___contentsOfAdmissions_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_contentsOfAdmissions_3() const { return ___contentsOfAdmissions_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_contentsOfAdmissions_3() { return &___contentsOfAdmissions_3; }
	inline void set_contentsOfAdmissions_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___contentsOfAdmissions_3 = value;
		Il2CppCodeGenWriteBarrier((&___contentsOfAdmissions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMISSIONSYNTAX_TBAB9E07D2191AEDADF4E868C165F187961AD5187_H
#ifndef ADMISSIONS_TCCC4D9812D22941DA4A9842A40A21B376A22DA78_H
#define ADMISSIONS_TCCC4D9812D22941DA4A9842A40A21B376A22DA78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.Admissions
struct  Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.Admissions::admissionAuthority
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___admissionAuthority_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.Admissions::namingAuthority
	NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D * ___namingAuthority_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.Admissions::professionInfos
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___professionInfos_4;

public:
	inline static int32_t get_offset_of_admissionAuthority_2() { return static_cast<int32_t>(offsetof(Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78, ___admissionAuthority_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_admissionAuthority_2() const { return ___admissionAuthority_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_admissionAuthority_2() { return &___admissionAuthority_2; }
	inline void set_admissionAuthority_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___admissionAuthority_2 = value;
		Il2CppCodeGenWriteBarrier((&___admissionAuthority_2), value);
	}

	inline static int32_t get_offset_of_namingAuthority_3() { return static_cast<int32_t>(offsetof(Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78, ___namingAuthority_3)); }
	inline NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D * get_namingAuthority_3() const { return ___namingAuthority_3; }
	inline NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D ** get_address_of_namingAuthority_3() { return &___namingAuthority_3; }
	inline void set_namingAuthority_3(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D * value)
	{
		___namingAuthority_3 = value;
		Il2CppCodeGenWriteBarrier((&___namingAuthority_3), value);
	}

	inline static int32_t get_offset_of_professionInfos_4() { return static_cast<int32_t>(offsetof(Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78, ___professionInfos_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_professionInfos_4() const { return ___professionInfos_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_professionInfos_4() { return &___professionInfos_4; }
	inline void set_professionInfos_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___professionInfos_4 = value;
		Il2CppCodeGenWriteBarrier((&___professionInfos_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMISSIONS_TCCC4D9812D22941DA4A9842A40A21B376A22DA78_H
#ifndef DECLARATIONOFMAJORITY_T4E08A2FF28B11CE752A6B0E42EE23CB29E668190_H
#define DECLARATIONOFMAJORITY_T4E08A2FF28B11CE752A6B0E42EE23CB29E668190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.DeclarationOfMajority
struct  DeclarationOfMajority_t4E08A2FF28B11CE752A6B0E42EE23CB29E668190  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObject BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.DeclarationOfMajority::declaration
	Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD * ___declaration_2;

public:
	inline static int32_t get_offset_of_declaration_2() { return static_cast<int32_t>(offsetof(DeclarationOfMajority_t4E08A2FF28B11CE752A6B0E42EE23CB29E668190, ___declaration_2)); }
	inline Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD * get_declaration_2() const { return ___declaration_2; }
	inline Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD ** get_address_of_declaration_2() { return &___declaration_2; }
	inline void set_declaration_2(Asn1TaggedObject_t02C4D2D88510F17ED8F969894CA89B565BA3F0FD * value)
	{
		___declaration_2 = value;
		Il2CppCodeGenWriteBarrier((&___declaration_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECLARATIONOFMAJORITY_T4E08A2FF28B11CE752A6B0E42EE23CB29E668190_H
#ifndef MONETARYLIMIT_T613FB7A16442DB2DEFF21612E85A0A5DADE83524_H
#define MONETARYLIMIT_T613FB7A16442DB2DEFF21612E85A0A5DADE83524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.MonetaryLimit
struct  MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerPrintableString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.MonetaryLimit::currency
	DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * ___currency_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.MonetaryLimit::amount
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___amount_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.MonetaryLimit::exponent
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___exponent_4;

public:
	inline static int32_t get_offset_of_currency_2() { return static_cast<int32_t>(offsetof(MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524, ___currency_2)); }
	inline DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * get_currency_2() const { return ___currency_2; }
	inline DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 ** get_address_of_currency_2() { return &___currency_2; }
	inline void set_currency_2(DerPrintableString_tCA859465A18F31F2332B5409C7976B16738B74F0 * value)
	{
		___currency_2 = value;
		Il2CppCodeGenWriteBarrier((&___currency_2), value);
	}

	inline static int32_t get_offset_of_amount_3() { return static_cast<int32_t>(offsetof(MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524, ___amount_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_amount_3() const { return ___amount_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_amount_3() { return &___amount_3; }
	inline void set_amount_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___amount_3 = value;
		Il2CppCodeGenWriteBarrier((&___amount_3), value);
	}

	inline static int32_t get_offset_of_exponent_4() { return static_cast<int32_t>(offsetof(MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524, ___exponent_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_exponent_4() const { return ___exponent_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_exponent_4() { return &___exponent_4; }
	inline void set_exponent_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___exponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___exponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONETARYLIMIT_T613FB7A16442DB2DEFF21612E85A0A5DADE83524_H
#ifndef NAMINGAUTHORITY_TCBD021B53A00647699CB202F35ABAB0637BFA53D_H
#define NAMINGAUTHORITY_TCBD021B53A00647699CB202F35ABAB0637BFA53D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority
struct  NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority::namingAuthorityID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___namingAuthorityID_3;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority::namingAuthorityUrl
	String_t* ___namingAuthorityUrl_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority::namingAuthorityText
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___namingAuthorityText_5;

public:
	inline static int32_t get_offset_of_namingAuthorityID_3() { return static_cast<int32_t>(offsetof(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D, ___namingAuthorityID_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_namingAuthorityID_3() const { return ___namingAuthorityID_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_namingAuthorityID_3() { return &___namingAuthorityID_3; }
	inline void set_namingAuthorityID_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___namingAuthorityID_3 = value;
		Il2CppCodeGenWriteBarrier((&___namingAuthorityID_3), value);
	}

	inline static int32_t get_offset_of_namingAuthorityUrl_4() { return static_cast<int32_t>(offsetof(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D, ___namingAuthorityUrl_4)); }
	inline String_t* get_namingAuthorityUrl_4() const { return ___namingAuthorityUrl_4; }
	inline String_t** get_address_of_namingAuthorityUrl_4() { return &___namingAuthorityUrl_4; }
	inline void set_namingAuthorityUrl_4(String_t* value)
	{
		___namingAuthorityUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___namingAuthorityUrl_4), value);
	}

	inline static int32_t get_offset_of_namingAuthorityText_5() { return static_cast<int32_t>(offsetof(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D, ___namingAuthorityText_5)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_namingAuthorityText_5() const { return ___namingAuthorityText_5; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_namingAuthorityText_5() { return &___namingAuthorityText_5; }
	inline void set_namingAuthorityText_5(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___namingAuthorityText_5 = value;
		Il2CppCodeGenWriteBarrier((&___namingAuthorityText_5), value);
	}
};

struct NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority::IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2;

public:
	inline static int32_t get_offset_of_IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2() { return static_cast<int32_t>(offsetof(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D_StaticFields, ___IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2() const { return ___IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2() { return &___IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2; }
	inline void set_IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2 = value;
		Il2CppCodeGenWriteBarrier((&___IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMINGAUTHORITY_TCBD021B53A00647699CB202F35ABAB0637BFA53D_H
#ifndef PROCURATIONSYNTAX_T6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0_H
#define PROCURATIONSYNTAX_T6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProcurationSyntax
struct  ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProcurationSyntax::country
	String_t* ___country_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProcurationSyntax::typeOfSubstitution
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___typeOfSubstitution_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProcurationSyntax::thirdPerson
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___thirdPerson_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProcurationSyntax::certRef
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___certRef_5;

public:
	inline static int32_t get_offset_of_country_2() { return static_cast<int32_t>(offsetof(ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0, ___country_2)); }
	inline String_t* get_country_2() const { return ___country_2; }
	inline String_t** get_address_of_country_2() { return &___country_2; }
	inline void set_country_2(String_t* value)
	{
		___country_2 = value;
		Il2CppCodeGenWriteBarrier((&___country_2), value);
	}

	inline static int32_t get_offset_of_typeOfSubstitution_3() { return static_cast<int32_t>(offsetof(ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0, ___typeOfSubstitution_3)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_typeOfSubstitution_3() const { return ___typeOfSubstitution_3; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_typeOfSubstitution_3() { return &___typeOfSubstitution_3; }
	inline void set_typeOfSubstitution_3(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___typeOfSubstitution_3 = value;
		Il2CppCodeGenWriteBarrier((&___typeOfSubstitution_3), value);
	}

	inline static int32_t get_offset_of_thirdPerson_4() { return static_cast<int32_t>(offsetof(ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0, ___thirdPerson_4)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_thirdPerson_4() const { return ___thirdPerson_4; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_thirdPerson_4() { return &___thirdPerson_4; }
	inline void set_thirdPerson_4(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___thirdPerson_4 = value;
		Il2CppCodeGenWriteBarrier((&___thirdPerson_4), value);
	}

	inline static int32_t get_offset_of_certRef_5() { return static_cast<int32_t>(offsetof(ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0, ___certRef_5)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_certRef_5() const { return ___certRef_5; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_certRef_5() { return &___certRef_5; }
	inline void set_certRef_5(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___certRef_5 = value;
		Il2CppCodeGenWriteBarrier((&___certRef_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCURATIONSYNTAX_T6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0_H
#ifndef PROFESSIONINFO_T4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_H
#define PROFESSIONINFO_T4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo
struct  ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.NamingAuthority BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::namingAuthority
	NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D * ___namingAuthority_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::professionItems
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___professionItems_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::professionOids
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___professionOids_23;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::registrationNumber
	String_t* ___registrationNumber_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::addProfessionInfo
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___addProfessionInfo_25;

public:
	inline static int32_t get_offset_of_namingAuthority_21() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A, ___namingAuthority_21)); }
	inline NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D * get_namingAuthority_21() const { return ___namingAuthority_21; }
	inline NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D ** get_address_of_namingAuthority_21() { return &___namingAuthority_21; }
	inline void set_namingAuthority_21(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D * value)
	{
		___namingAuthority_21 = value;
		Il2CppCodeGenWriteBarrier((&___namingAuthority_21), value);
	}

	inline static int32_t get_offset_of_professionItems_22() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A, ___professionItems_22)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_professionItems_22() const { return ___professionItems_22; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_professionItems_22() { return &___professionItems_22; }
	inline void set_professionItems_22(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___professionItems_22 = value;
		Il2CppCodeGenWriteBarrier((&___professionItems_22), value);
	}

	inline static int32_t get_offset_of_professionOids_23() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A, ___professionOids_23)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_professionOids_23() const { return ___professionOids_23; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_professionOids_23() { return &___professionOids_23; }
	inline void set_professionOids_23(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___professionOids_23 = value;
		Il2CppCodeGenWriteBarrier((&___professionOids_23), value);
	}

	inline static int32_t get_offset_of_registrationNumber_24() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A, ___registrationNumber_24)); }
	inline String_t* get_registrationNumber_24() const { return ___registrationNumber_24; }
	inline String_t** get_address_of_registrationNumber_24() { return &___registrationNumber_24; }
	inline void set_registrationNumber_24(String_t* value)
	{
		___registrationNumber_24 = value;
		Il2CppCodeGenWriteBarrier((&___registrationNumber_24), value);
	}

	inline static int32_t get_offset_of_addProfessionInfo_25() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A, ___addProfessionInfo_25)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_addProfessionInfo_25() const { return ___addProfessionInfo_25; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_addProfessionInfo_25() { return &___addProfessionInfo_25; }
	inline void set_addProfessionInfo_25(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___addProfessionInfo_25 = value;
		Il2CppCodeGenWriteBarrier((&___addProfessionInfo_25), value);
	}
};

struct ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Rechtsanwltin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Rechtsanwltin_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Rechtsanwalt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Rechtsanwalt_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Rechtsbeistand
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Rechtsbeistand_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Steuerberaterin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Steuerberaterin_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Steuerberater
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Steuerberater_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Steuerbevollmchtigte
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Steuerbevollmchtigte_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Steuerbevollmchtigter
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Steuerbevollmchtigter_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Notarin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notarin_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Notar
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notar_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Notarvertreterin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notarvertreterin_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Notarvertreter
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notarvertreter_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Notariatsverwalterin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notariatsverwalterin_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Notariatsverwalter
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notariatsverwalter_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Wirtschaftsprferin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Wirtschaftsprferin_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Wirtschaftsprfer
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Wirtschaftsprfer_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::VereidigteBuchprferin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VereidigteBuchprferin_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::VereidigterBuchprfer
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___VereidigterBuchprfer_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Patentanwltin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Patentanwltin_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.ProfessionInfo::Patentanwalt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Patentanwalt_20;

public:
	inline static int32_t get_offset_of_Rechtsanwltin_2() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Rechtsanwltin_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Rechtsanwltin_2() const { return ___Rechtsanwltin_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Rechtsanwltin_2() { return &___Rechtsanwltin_2; }
	inline void set_Rechtsanwltin_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Rechtsanwltin_2 = value;
		Il2CppCodeGenWriteBarrier((&___Rechtsanwltin_2), value);
	}

	inline static int32_t get_offset_of_Rechtsanwalt_3() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Rechtsanwalt_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Rechtsanwalt_3() const { return ___Rechtsanwalt_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Rechtsanwalt_3() { return &___Rechtsanwalt_3; }
	inline void set_Rechtsanwalt_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Rechtsanwalt_3 = value;
		Il2CppCodeGenWriteBarrier((&___Rechtsanwalt_3), value);
	}

	inline static int32_t get_offset_of_Rechtsbeistand_4() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Rechtsbeistand_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Rechtsbeistand_4() const { return ___Rechtsbeistand_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Rechtsbeistand_4() { return &___Rechtsbeistand_4; }
	inline void set_Rechtsbeistand_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Rechtsbeistand_4 = value;
		Il2CppCodeGenWriteBarrier((&___Rechtsbeistand_4), value);
	}

	inline static int32_t get_offset_of_Steuerberaterin_5() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Steuerberaterin_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Steuerberaterin_5() const { return ___Steuerberaterin_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Steuerberaterin_5() { return &___Steuerberaterin_5; }
	inline void set_Steuerberaterin_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Steuerberaterin_5 = value;
		Il2CppCodeGenWriteBarrier((&___Steuerberaterin_5), value);
	}

	inline static int32_t get_offset_of_Steuerberater_6() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Steuerberater_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Steuerberater_6() const { return ___Steuerberater_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Steuerberater_6() { return &___Steuerberater_6; }
	inline void set_Steuerberater_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Steuerberater_6 = value;
		Il2CppCodeGenWriteBarrier((&___Steuerberater_6), value);
	}

	inline static int32_t get_offset_of_Steuerbevollmchtigte_7() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Steuerbevollmchtigte_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Steuerbevollmchtigte_7() const { return ___Steuerbevollmchtigte_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Steuerbevollmchtigte_7() { return &___Steuerbevollmchtigte_7; }
	inline void set_Steuerbevollmchtigte_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Steuerbevollmchtigte_7 = value;
		Il2CppCodeGenWriteBarrier((&___Steuerbevollmchtigte_7), value);
	}

	inline static int32_t get_offset_of_Steuerbevollmchtigter_8() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Steuerbevollmchtigter_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Steuerbevollmchtigter_8() const { return ___Steuerbevollmchtigter_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Steuerbevollmchtigter_8() { return &___Steuerbevollmchtigter_8; }
	inline void set_Steuerbevollmchtigter_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Steuerbevollmchtigter_8 = value;
		Il2CppCodeGenWriteBarrier((&___Steuerbevollmchtigter_8), value);
	}

	inline static int32_t get_offset_of_Notarin_9() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Notarin_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notarin_9() const { return ___Notarin_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notarin_9() { return &___Notarin_9; }
	inline void set_Notarin_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notarin_9 = value;
		Il2CppCodeGenWriteBarrier((&___Notarin_9), value);
	}

	inline static int32_t get_offset_of_Notar_10() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Notar_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notar_10() const { return ___Notar_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notar_10() { return &___Notar_10; }
	inline void set_Notar_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notar_10 = value;
		Il2CppCodeGenWriteBarrier((&___Notar_10), value);
	}

	inline static int32_t get_offset_of_Notarvertreterin_11() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Notarvertreterin_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notarvertreterin_11() const { return ___Notarvertreterin_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notarvertreterin_11() { return &___Notarvertreterin_11; }
	inline void set_Notarvertreterin_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notarvertreterin_11 = value;
		Il2CppCodeGenWriteBarrier((&___Notarvertreterin_11), value);
	}

	inline static int32_t get_offset_of_Notarvertreter_12() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Notarvertreter_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notarvertreter_12() const { return ___Notarvertreter_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notarvertreter_12() { return &___Notarvertreter_12; }
	inline void set_Notarvertreter_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notarvertreter_12 = value;
		Il2CppCodeGenWriteBarrier((&___Notarvertreter_12), value);
	}

	inline static int32_t get_offset_of_Notariatsverwalterin_13() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Notariatsverwalterin_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notariatsverwalterin_13() const { return ___Notariatsverwalterin_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notariatsverwalterin_13() { return &___Notariatsverwalterin_13; }
	inline void set_Notariatsverwalterin_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notariatsverwalterin_13 = value;
		Il2CppCodeGenWriteBarrier((&___Notariatsverwalterin_13), value);
	}

	inline static int32_t get_offset_of_Notariatsverwalter_14() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Notariatsverwalter_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notariatsverwalter_14() const { return ___Notariatsverwalter_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notariatsverwalter_14() { return &___Notariatsverwalter_14; }
	inline void set_Notariatsverwalter_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notariatsverwalter_14 = value;
		Il2CppCodeGenWriteBarrier((&___Notariatsverwalter_14), value);
	}

	inline static int32_t get_offset_of_Wirtschaftsprferin_15() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Wirtschaftsprferin_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Wirtschaftsprferin_15() const { return ___Wirtschaftsprferin_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Wirtschaftsprferin_15() { return &___Wirtschaftsprferin_15; }
	inline void set_Wirtschaftsprferin_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Wirtschaftsprferin_15 = value;
		Il2CppCodeGenWriteBarrier((&___Wirtschaftsprferin_15), value);
	}

	inline static int32_t get_offset_of_Wirtschaftsprfer_16() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Wirtschaftsprfer_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Wirtschaftsprfer_16() const { return ___Wirtschaftsprfer_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Wirtschaftsprfer_16() { return &___Wirtschaftsprfer_16; }
	inline void set_Wirtschaftsprfer_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Wirtschaftsprfer_16 = value;
		Il2CppCodeGenWriteBarrier((&___Wirtschaftsprfer_16), value);
	}

	inline static int32_t get_offset_of_VereidigteBuchprferin_17() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___VereidigteBuchprferin_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VereidigteBuchprferin_17() const { return ___VereidigteBuchprferin_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VereidigteBuchprferin_17() { return &___VereidigteBuchprferin_17; }
	inline void set_VereidigteBuchprferin_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VereidigteBuchprferin_17 = value;
		Il2CppCodeGenWriteBarrier((&___VereidigteBuchprferin_17), value);
	}

	inline static int32_t get_offset_of_VereidigterBuchprfer_18() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___VereidigterBuchprfer_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_VereidigterBuchprfer_18() const { return ___VereidigterBuchprfer_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_VereidigterBuchprfer_18() { return &___VereidigterBuchprfer_18; }
	inline void set_VereidigterBuchprfer_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___VereidigterBuchprfer_18 = value;
		Il2CppCodeGenWriteBarrier((&___VereidigterBuchprfer_18), value);
	}

	inline static int32_t get_offset_of_Patentanwltin_19() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Patentanwltin_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Patentanwltin_19() const { return ___Patentanwltin_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Patentanwltin_19() { return &___Patentanwltin_19; }
	inline void set_Patentanwltin_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Patentanwltin_19 = value;
		Il2CppCodeGenWriteBarrier((&___Patentanwltin_19), value);
	}

	inline static int32_t get_offset_of_Patentanwalt_20() { return static_cast<int32_t>(offsetof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields, ___Patentanwalt_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Patentanwalt_20() const { return ___Patentanwalt_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Patentanwalt_20() { return &___Patentanwalt_20; }
	inline void set_Patentanwalt_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Patentanwalt_20 = value;
		Il2CppCodeGenWriteBarrier((&___Patentanwalt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFESSIONINFO_T4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_H
#ifndef RESTRICTION_TDCC0C2CDB9AAC73C4ABE76673361922169EFD011_H
#define RESTRICTION_TDCC0C2CDB9AAC73C4ABE76673361922169EFD011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.Restriction
struct  Restriction_tDCC0C2CDB9AAC73C4ABE76673361922169EFD011  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.Restriction::restriction
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___restriction_2;

public:
	inline static int32_t get_offset_of_restriction_2() { return static_cast<int32_t>(offsetof(Restriction_tDCC0C2CDB9AAC73C4ABE76673361922169EFD011, ___restriction_2)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_restriction_2() const { return ___restriction_2; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_restriction_2() { return &___restriction_2; }
	inline void set_restriction_2(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___restriction_2 = value;
		Il2CppCodeGenWriteBarrier((&___restriction_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTION_TDCC0C2CDB9AAC73C4ABE76673361922169EFD011_H
#ifndef CAST5CBCPARAMETERS_TC0A8D78706D80E81D7E1A960D22474755DDC1991_H
#define CAST5CBCPARAMETERS_TC0A8D78706D80E81D7E1A960D22474755DDC1991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.Cast5CbcParameters
struct  Cast5CbcParameters_tC0A8D78706D80E81D7E1A960D22474755DDC1991  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.Cast5CbcParameters::keyLength
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___keyLength_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.Cast5CbcParameters::iv
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___iv_3;

public:
	inline static int32_t get_offset_of_keyLength_2() { return static_cast<int32_t>(offsetof(Cast5CbcParameters_tC0A8D78706D80E81D7E1A960D22474755DDC1991, ___keyLength_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_keyLength_2() const { return ___keyLength_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_keyLength_2() { return &___keyLength_2; }
	inline void set_keyLength_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___keyLength_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyLength_2), value);
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(Cast5CbcParameters_tC0A8D78706D80E81D7E1A960D22474755DDC1991, ___iv_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_iv_3() const { return ___iv_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAST5CBCPARAMETERS_TC0A8D78706D80E81D7E1A960D22474755DDC1991_H
#ifndef IDEACBCPAR_T8F7E384542D30D87569D73B634D5E758EB03237D_H
#define IDEACBCPAR_T8F7E384542D30D87569D73B634D5E758EB03237D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.IdeaCbcPar
struct  IdeaCbcPar_t8F7E384542D30D87569D73B634D5E758EB03237D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.IdeaCbcPar::iv
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___iv_2;

public:
	inline static int32_t get_offset_of_iv_2() { return static_cast<int32_t>(offsetof(IdeaCbcPar_t8F7E384542D30D87569D73B634D5E758EB03237D, ___iv_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_iv_2() const { return ___iv_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_iv_2() { return &___iv_2; }
	inline void set_iv_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___iv_2 = value;
		Il2CppCodeGenWriteBarrier((&___iv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDEACBCPAR_T8F7E384542D30D87569D73B634D5E758EB03237D_H
#ifndef PUBLICKEYANDCHALLENGE_TD76E4D0962738CFF58387D1BA44AF6069F93335E_H
#define PUBLICKEYANDCHALLENGE_TD76E4D0962738CFF58387D1BA44AF6069F93335E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Mozilla.PublicKeyAndChallenge
struct  PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Mozilla.PublicKeyAndChallenge::pkacSeq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___pkacSeq_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Mozilla.PublicKeyAndChallenge::spki
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___spki_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Mozilla.PublicKeyAndChallenge::challenge
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * ___challenge_4;

public:
	inline static int32_t get_offset_of_pkacSeq_2() { return static_cast<int32_t>(offsetof(PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E, ___pkacSeq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_pkacSeq_2() const { return ___pkacSeq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_pkacSeq_2() { return &___pkacSeq_2; }
	inline void set_pkacSeq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___pkacSeq_2 = value;
		Il2CppCodeGenWriteBarrier((&___pkacSeq_2), value);
	}

	inline static int32_t get_offset_of_spki_3() { return static_cast<int32_t>(offsetof(PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E, ___spki_3)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_spki_3() const { return ___spki_3; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_spki_3() { return &___spki_3; }
	inline void set_spki_3(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___spki_3 = value;
		Il2CppCodeGenWriteBarrier((&___spki_3), value);
	}

	inline static int32_t get_offset_of_challenge_4() { return static_cast<int32_t>(offsetof(PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E, ___challenge_4)); }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * get_challenge_4() const { return ___challenge_4; }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE ** get_address_of_challenge_4() { return &___challenge_4; }
	inline void set_challenge_4(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * value)
	{
		___challenge_4 = value;
		Il2CppCodeGenWriteBarrier((&___challenge_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEYANDCHALLENGE_TD76E4D0962738CFF58387D1BA44AF6069F93335E_H
#ifndef BASICOCSPRESPONSE_T7EFCFB5EB615E106A534DFCA49C6893CB232B9ED_H
#define BASICOCSPRESPONSE_T7EFCFB5EB615E106A534DFCA49C6893CB232B9ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse
struct  BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse::tbsResponseData
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * ___tbsResponseData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse::signatureAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signatureAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse::signature
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___signature_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.BasicOcspResponse::certs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certs_5;

public:
	inline static int32_t get_offset_of_tbsResponseData_2() { return static_cast<int32_t>(offsetof(BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED, ___tbsResponseData_2)); }
	inline ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * get_tbsResponseData_2() const { return ___tbsResponseData_2; }
	inline ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 ** get_address_of_tbsResponseData_2() { return &___tbsResponseData_2; }
	inline void set_tbsResponseData_2(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8 * value)
	{
		___tbsResponseData_2 = value;
		Il2CppCodeGenWriteBarrier((&___tbsResponseData_2), value);
	}

	inline static int32_t get_offset_of_signatureAlgorithm_3() { return static_cast<int32_t>(offsetof(BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED, ___signatureAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signatureAlgorithm_3() const { return ___signatureAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signatureAlgorithm_3() { return &___signatureAlgorithm_3; }
	inline void set_signatureAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signatureAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_signature_4() { return static_cast<int32_t>(offsetof(BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED, ___signature_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_signature_4() const { return ___signature_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_signature_4() { return &___signature_4; }
	inline void set_signature_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___signature_4 = value;
		Il2CppCodeGenWriteBarrier((&___signature_4), value);
	}

	inline static int32_t get_offset_of_certs_5() { return static_cast<int32_t>(offsetof(BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED, ___certs_5)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certs_5() const { return ___certs_5; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certs_5() { return &___certs_5; }
	inline void set_certs_5(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certs_5 = value;
		Il2CppCodeGenWriteBarrier((&___certs_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICOCSPRESPONSE_T7EFCFB5EB615E106A534DFCA49C6893CB232B9ED_H
#ifndef CERTID_TD10877523D531F998848FBD48D854308134F2918_H
#define CERTID_TD10877523D531F998848FBD48D854308134F2918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID
struct  CertID_tD10877523D531F998848FBD48D854308134F2918  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID::issuerNameHash
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___issuerNameHash_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID::issuerKeyHash
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___issuerKeyHash_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_5;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(CertID_tD10877523D531F998848FBD48D854308134F2918, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_issuerNameHash_3() { return static_cast<int32_t>(offsetof(CertID_tD10877523D531F998848FBD48D854308134F2918, ___issuerNameHash_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_issuerNameHash_3() const { return ___issuerNameHash_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_issuerNameHash_3() { return &___issuerNameHash_3; }
	inline void set_issuerNameHash_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___issuerNameHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuerNameHash_3), value);
	}

	inline static int32_t get_offset_of_issuerKeyHash_4() { return static_cast<int32_t>(offsetof(CertID_tD10877523D531F998848FBD48D854308134F2918, ___issuerKeyHash_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_issuerKeyHash_4() const { return ___issuerKeyHash_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_issuerKeyHash_4() { return &___issuerKeyHash_4; }
	inline void set_issuerKeyHash_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___issuerKeyHash_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuerKeyHash_4), value);
	}

	inline static int32_t get_offset_of_serialNumber_5() { return static_cast<int32_t>(offsetof(CertID_tD10877523D531F998848FBD48D854308134F2918, ___serialNumber_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_5() const { return ___serialNumber_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_5() { return &___serialNumber_5; }
	inline void set_serialNumber_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_5 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTID_TD10877523D531F998848FBD48D854308134F2918_H
#ifndef CERTSTATUS_T1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3_H
#define CERTSTATUS_T1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus
struct  CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus::tagNo
	int32_t ___tagNo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus::value
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___value_3;

public:
	inline static int32_t get_offset_of_tagNo_2() { return static_cast<int32_t>(offsetof(CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3, ___tagNo_2)); }
	inline int32_t get_tagNo_2() const { return ___tagNo_2; }
	inline int32_t* get_address_of_tagNo_2() { return &___tagNo_2; }
	inline void set_tagNo_2(int32_t value)
	{
		___tagNo_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3, ___value_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_value_3() const { return ___value_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTSTATUS_T1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3_H
#ifndef CRLID_T63CDF185EE71A10AFD728E56D0557FD39C0F81CE_H
#define CRLID_T63CDF185EE71A10AFD728E56D0557FD39C0F81CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CrlID
struct  CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CrlID::crlUrl
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * ___crlUrl_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CrlID::crlNum
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___crlNum_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CrlID::crlTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___crlTime_4;

public:
	inline static int32_t get_offset_of_crlUrl_2() { return static_cast<int32_t>(offsetof(CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE, ___crlUrl_2)); }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * get_crlUrl_2() const { return ___crlUrl_2; }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE ** get_address_of_crlUrl_2() { return &___crlUrl_2; }
	inline void set_crlUrl_2(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * value)
	{
		___crlUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___crlUrl_2), value);
	}

	inline static int32_t get_offset_of_crlNum_3() { return static_cast<int32_t>(offsetof(CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE, ___crlNum_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_crlNum_3() const { return ___crlNum_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_crlNum_3() { return &___crlNum_3; }
	inline void set_crlNum_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___crlNum_3 = value;
		Il2CppCodeGenWriteBarrier((&___crlNum_3), value);
	}

	inline static int32_t get_offset_of_crlTime_4() { return static_cast<int32_t>(offsetof(CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE, ___crlTime_4)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_crlTime_4() const { return ___crlTime_4; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_crlTime_4() { return &___crlTime_4; }
	inline void set_crlTime_4(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___crlTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___crlTime_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLID_T63CDF185EE71A10AFD728E56D0557FD39C0F81CE_H
#ifndef OCSPREQUEST_TD617A6CB72C32FA21B8D537CE63C574A01AB7B0C_H
#define OCSPREQUEST_TD617A6CB72C32FA21B8D537CE63C574A01AB7B0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspRequest
struct  OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspRequest::tbsRequest
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0 * ___tbsRequest_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Signature BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspRequest::optionalSignature
	Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278 * ___optionalSignature_3;

public:
	inline static int32_t get_offset_of_tbsRequest_2() { return static_cast<int32_t>(offsetof(OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C, ___tbsRequest_2)); }
	inline TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0 * get_tbsRequest_2() const { return ___tbsRequest_2; }
	inline TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0 ** get_address_of_tbsRequest_2() { return &___tbsRequest_2; }
	inline void set_tbsRequest_2(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0 * value)
	{
		___tbsRequest_2 = value;
		Il2CppCodeGenWriteBarrier((&___tbsRequest_2), value);
	}

	inline static int32_t get_offset_of_optionalSignature_3() { return static_cast<int32_t>(offsetof(OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C, ___optionalSignature_3)); }
	inline Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278 * get_optionalSignature_3() const { return ___optionalSignature_3; }
	inline Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278 ** get_address_of_optionalSignature_3() { return &___optionalSignature_3; }
	inline void set_optionalSignature_3(Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278 * value)
	{
		___optionalSignature_3 = value;
		Il2CppCodeGenWriteBarrier((&___optionalSignature_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPREQUEST_TD617A6CB72C32FA21B8D537CE63C574A01AB7B0C_H
#ifndef OCSPRESPONSE_T9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4_H
#define OCSPRESPONSE_T9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponse
struct  OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponse::responseStatus
	OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567 * ___responseStatus_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseBytes BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponse::responseBytes
	ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1 * ___responseBytes_3;

public:
	inline static int32_t get_offset_of_responseStatus_2() { return static_cast<int32_t>(offsetof(OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4, ___responseStatus_2)); }
	inline OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567 * get_responseStatus_2() const { return ___responseStatus_2; }
	inline OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567 ** get_address_of_responseStatus_2() { return &___responseStatus_2; }
	inline void set_responseStatus_2(OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567 * value)
	{
		___responseStatus_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseStatus_2), value);
	}

	inline static int32_t get_offset_of_responseBytes_3() { return static_cast<int32_t>(offsetof(OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4, ___responseBytes_3)); }
	inline ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1 * get_responseBytes_3() const { return ___responseBytes_3; }
	inline ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1 ** get_address_of_responseBytes_3() { return &___responseBytes_3; }
	inline void set_responseBytes_3(ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1 * value)
	{
		___responseBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___responseBytes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPONSE_T9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4_H
#ifndef REQUEST_T7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2_H
#define REQUEST_T7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Request
struct  Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Request::reqCert
	CertID_tD10877523D531F998848FBD48D854308134F2918 * ___reqCert_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Request::singleRequestExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___singleRequestExtensions_3;

public:
	inline static int32_t get_offset_of_reqCert_2() { return static_cast<int32_t>(offsetof(Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2, ___reqCert_2)); }
	inline CertID_tD10877523D531F998848FBD48D854308134F2918 * get_reqCert_2() const { return ___reqCert_2; }
	inline CertID_tD10877523D531F998848FBD48D854308134F2918 ** get_address_of_reqCert_2() { return &___reqCert_2; }
	inline void set_reqCert_2(CertID_tD10877523D531F998848FBD48D854308134F2918 * value)
	{
		___reqCert_2 = value;
		Il2CppCodeGenWriteBarrier((&___reqCert_2), value);
	}

	inline static int32_t get_offset_of_singleRequestExtensions_3() { return static_cast<int32_t>(offsetof(Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2, ___singleRequestExtensions_3)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_singleRequestExtensions_3() const { return ___singleRequestExtensions_3; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_singleRequestExtensions_3() { return &___singleRequestExtensions_3; }
	inline void set_singleRequestExtensions_3(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___singleRequestExtensions_3 = value;
		Il2CppCodeGenWriteBarrier((&___singleRequestExtensions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2_H
#ifndef RESPONDERID_T26B99AFE9E4E05948990F461880598D304592346_H
#define RESPONDERID_T26B99AFE9E4E05948990F461880598D304592346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID
struct  ResponderID_t26B99AFE9E4E05948990F461880598D304592346  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID::id
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___id_2;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(ResponderID_t26B99AFE9E4E05948990F461880598D304592346, ___id_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_id_2() const { return ___id_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONDERID_T26B99AFE9E4E05948990F461880598D304592346_H
#ifndef RESPONSEBYTES_TDE75206E26774444407DB0BF4858B95D8D2BE4E1_H
#define RESPONSEBYTES_TDE75206E26774444407DB0BF4858B95D8D2BE4E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseBytes
struct  ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseBytes::responseType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___responseType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseBytes::response
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___response_3;

public:
	inline static int32_t get_offset_of_responseType_2() { return static_cast<int32_t>(offsetof(ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1, ___responseType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_responseType_2() const { return ___responseType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_responseType_2() { return &___responseType_2; }
	inline void set_responseType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___responseType_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseType_2), value);
	}

	inline static int32_t get_offset_of_response_3() { return static_cast<int32_t>(offsetof(ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1, ___response_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_response_3() const { return ___response_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_response_3() { return &___response_3; }
	inline void set_response_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___response_3 = value;
		Il2CppCodeGenWriteBarrier((&___response_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEBYTES_TDE75206E26774444407DB0BF4858B95D8D2BE4E1_H
#ifndef RESPONSEDATA_T916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_H
#define RESPONSEDATA_T916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData
struct  ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::versionPresent
	bool ___versionPresent_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::responderID
	ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * ___responderID_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::producedAt
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___producedAt_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::responses
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___responses_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::responseExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___responseExtensions_8;

public:
	inline static int32_t get_offset_of_versionPresent_3() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8, ___versionPresent_3)); }
	inline bool get_versionPresent_3() const { return ___versionPresent_3; }
	inline bool* get_address_of_versionPresent_3() { return &___versionPresent_3; }
	inline void set_versionPresent_3(bool value)
	{
		___versionPresent_3 = value;
	}

	inline static int32_t get_offset_of_version_4() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8, ___version_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_4() const { return ___version_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_4() { return &___version_4; }
	inline void set_version_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_4 = value;
		Il2CppCodeGenWriteBarrier((&___version_4), value);
	}

	inline static int32_t get_offset_of_responderID_5() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8, ___responderID_5)); }
	inline ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * get_responderID_5() const { return ___responderID_5; }
	inline ResponderID_t26B99AFE9E4E05948990F461880598D304592346 ** get_address_of_responderID_5() { return &___responderID_5; }
	inline void set_responderID_5(ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * value)
	{
		___responderID_5 = value;
		Il2CppCodeGenWriteBarrier((&___responderID_5), value);
	}

	inline static int32_t get_offset_of_producedAt_6() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8, ___producedAt_6)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_producedAt_6() const { return ___producedAt_6; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_producedAt_6() { return &___producedAt_6; }
	inline void set_producedAt_6(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___producedAt_6 = value;
		Il2CppCodeGenWriteBarrier((&___producedAt_6), value);
	}

	inline static int32_t get_offset_of_responses_7() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8, ___responses_7)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_responses_7() const { return ___responses_7; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_responses_7() { return &___responses_7; }
	inline void set_responses_7(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___responses_7 = value;
		Il2CppCodeGenWriteBarrier((&___responses_7), value);
	}

	inline static int32_t get_offset_of_responseExtensions_8() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8, ___responseExtensions_8)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_responseExtensions_8() const { return ___responseExtensions_8; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_responseExtensions_8() { return &___responseExtensions_8; }
	inline void set_responseExtensions_8(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___responseExtensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___responseExtensions_8), value);
	}
};

struct ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponseData::V1
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___V1_2;

public:
	inline static int32_t get_offset_of_V1_2() { return static_cast<int32_t>(offsetof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_StaticFields, ___V1_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_V1_2() const { return ___V1_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_V1_2() { return &___V1_2; }
	inline void set_V1_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___V1_2 = value;
		Il2CppCodeGenWriteBarrier((&___V1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEDATA_T916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_H
#ifndef REVOKEDINFO_TA91D4DF893E7B2B3FF3203185A60F9BC799EA82B_H
#define REVOKEDINFO_TA91D4DF893E7B2B3FF3203185A60F9BC799EA82B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.RevokedInfo
struct  RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.RevokedInfo::revocationTime
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___revocationTime_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.CrlReason BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.RevokedInfo::revocationReason
	CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08 * ___revocationReason_3;

public:
	inline static int32_t get_offset_of_revocationTime_2() { return static_cast<int32_t>(offsetof(RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B, ___revocationTime_2)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_revocationTime_2() const { return ___revocationTime_2; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_revocationTime_2() { return &___revocationTime_2; }
	inline void set_revocationTime_2(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___revocationTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___revocationTime_2), value);
	}

	inline static int32_t get_offset_of_revocationReason_3() { return static_cast<int32_t>(offsetof(RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B, ___revocationReason_3)); }
	inline CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08 * get_revocationReason_3() const { return ___revocationReason_3; }
	inline CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08 ** get_address_of_revocationReason_3() { return &___revocationReason_3; }
	inline void set_revocationReason_3(CrlReason_t483113851B3620EA05D3EB258A0476FAF88BCD08 * value)
	{
		___revocationReason_3 = value;
		Il2CppCodeGenWriteBarrier((&___revocationReason_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVOKEDINFO_TA91D4DF893E7B2B3FF3203185A60F9BC799EA82B_H
#ifndef SERVICELOCATOR_T1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03_H
#define SERVICELOCATOR_T1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ServiceLocator
struct  ServiceLocator_t1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ServiceLocator::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ServiceLocator::locator
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___locator_3;

public:
	inline static int32_t get_offset_of_issuer_2() { return static_cast<int32_t>(offsetof(ServiceLocator_t1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03, ___issuer_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_2() const { return ___issuer_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_2() { return &___issuer_2; }
	inline void set_issuer_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_2), value);
	}

	inline static int32_t get_offset_of_locator_3() { return static_cast<int32_t>(offsetof(ServiceLocator_t1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03, ___locator_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_locator_3() const { return ___locator_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_locator_3() { return &___locator_3; }
	inline void set_locator_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___locator_3 = value;
		Il2CppCodeGenWriteBarrier((&___locator_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICELOCATOR_T1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03_H
#ifndef SIGNATURE_T229E902D74284E84E51BEDE6D24BB7D0D50F8278_H
#define SIGNATURE_T229E902D74284E84E51BEDE6D24BB7D0D50F8278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Signature
struct  Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Signature::signatureAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signatureAlgorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Signature::signatureValue
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___signatureValue_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.Signature::certs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certs_4;

public:
	inline static int32_t get_offset_of_signatureAlgorithm_2() { return static_cast<int32_t>(offsetof(Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278, ___signatureAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signatureAlgorithm_2() const { return ___signatureAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signatureAlgorithm_2() { return &___signatureAlgorithm_2; }
	inline void set_signatureAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signatureAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___signatureAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_signatureValue_3() { return static_cast<int32_t>(offsetof(Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278, ___signatureValue_3)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_signatureValue_3() const { return ___signatureValue_3; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_signatureValue_3() { return &___signatureValue_3; }
	inline void set_signatureValue_3(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___signatureValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___signatureValue_3), value);
	}

	inline static int32_t get_offset_of_certs_4() { return static_cast<int32_t>(offsetof(Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278, ___certs_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certs_4() const { return ___certs_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certs_4() { return &___certs_4; }
	inline void set_certs_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certs_4 = value;
		Il2CppCodeGenWriteBarrier((&___certs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATURE_T229E902D74284E84E51BEDE6D24BB7D0D50F8278_H
#ifndef SINGLERESPONSE_TCC53D74D61AFFD871093D41C706AD239EEBD36C5_H
#define SINGLERESPONSE_TCC53D74D61AFFD871093D41C706AD239EEBD36C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse
struct  SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse::certID
	CertID_tD10877523D531F998848FBD48D854308134F2918 * ___certID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.CertStatus BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse::certStatus
	CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 * ___certStatus_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse::thisUpdate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___thisUpdate_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse::nextUpdate
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___nextUpdate_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.SingleResponse::singleExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___singleExtensions_6;

public:
	inline static int32_t get_offset_of_certID_2() { return static_cast<int32_t>(offsetof(SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5, ___certID_2)); }
	inline CertID_tD10877523D531F998848FBD48D854308134F2918 * get_certID_2() const { return ___certID_2; }
	inline CertID_tD10877523D531F998848FBD48D854308134F2918 ** get_address_of_certID_2() { return &___certID_2; }
	inline void set_certID_2(CertID_tD10877523D531F998848FBD48D854308134F2918 * value)
	{
		___certID_2 = value;
		Il2CppCodeGenWriteBarrier((&___certID_2), value);
	}

	inline static int32_t get_offset_of_certStatus_3() { return static_cast<int32_t>(offsetof(SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5, ___certStatus_3)); }
	inline CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 * get_certStatus_3() const { return ___certStatus_3; }
	inline CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 ** get_address_of_certStatus_3() { return &___certStatus_3; }
	inline void set_certStatus_3(CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3 * value)
	{
		___certStatus_3 = value;
		Il2CppCodeGenWriteBarrier((&___certStatus_3), value);
	}

	inline static int32_t get_offset_of_thisUpdate_4() { return static_cast<int32_t>(offsetof(SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5, ___thisUpdate_4)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_thisUpdate_4() const { return ___thisUpdate_4; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_thisUpdate_4() { return &___thisUpdate_4; }
	inline void set_thisUpdate_4(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___thisUpdate_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisUpdate_4), value);
	}

	inline static int32_t get_offset_of_nextUpdate_5() { return static_cast<int32_t>(offsetof(SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5, ___nextUpdate_5)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_nextUpdate_5() const { return ___nextUpdate_5; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_nextUpdate_5() { return &___nextUpdate_5; }
	inline void set_nextUpdate_5(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___nextUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextUpdate_5), value);
	}

	inline static int32_t get_offset_of_singleExtensions_6() { return static_cast<int32_t>(offsetof(SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5, ___singleExtensions_6)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_singleExtensions_6() const { return ___singleExtensions_6; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_singleExtensions_6() { return &___singleExtensions_6; }
	inline void set_singleExtensions_6(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___singleExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___singleExtensions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLERESPONSE_TCC53D74D61AFFD871093D41C706AD239EEBD36C5_H
#ifndef TBSREQUEST_T3D10DD9716C0C92C615CC9C303CB4C07230900D0_H
#define TBSREQUEST_T3D10DD9716C0C92C615CC9C303CB4C07230900D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest
struct  TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest::requestorName
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___requestorName_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest::requestList
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___requestList_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest::requestExtensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___requestExtensions_6;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest::versionSet
	bool ___versionSet_7;

public:
	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0, ___version_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_3() const { return ___version_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}

	inline static int32_t get_offset_of_requestorName_4() { return static_cast<int32_t>(offsetof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0, ___requestorName_4)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_requestorName_4() const { return ___requestorName_4; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_requestorName_4() { return &___requestorName_4; }
	inline void set_requestorName_4(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___requestorName_4 = value;
		Il2CppCodeGenWriteBarrier((&___requestorName_4), value);
	}

	inline static int32_t get_offset_of_requestList_5() { return static_cast<int32_t>(offsetof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0, ___requestList_5)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_requestList_5() const { return ___requestList_5; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_requestList_5() { return &___requestList_5; }
	inline void set_requestList_5(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___requestList_5 = value;
		Il2CppCodeGenWriteBarrier((&___requestList_5), value);
	}

	inline static int32_t get_offset_of_requestExtensions_6() { return static_cast<int32_t>(offsetof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0, ___requestExtensions_6)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_requestExtensions_6() const { return ___requestExtensions_6; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_requestExtensions_6() { return &___requestExtensions_6; }
	inline void set_requestExtensions_6(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___requestExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestExtensions_6), value);
	}

	inline static int32_t get_offset_of_versionSet_7() { return static_cast<int32_t>(offsetof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0, ___versionSet_7)); }
	inline bool get_versionSet_7() const { return ___versionSet_7; }
	inline bool* get_address_of_versionSet_7() { return &___versionSet_7; }
	inline void set_versionSet_7(bool value)
	{
		___versionSet_7 = value;
	}
};

struct TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.TbsRequest::V1
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___V1_2;

public:
	inline static int32_t get_offset_of_V1_2() { return static_cast<int32_t>(offsetof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0_StaticFields, ___V1_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_V1_2() const { return ___V1_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_V1_2() { return &___V1_2; }
	inline void set_V1_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___V1_2 = value;
		Il2CppCodeGenWriteBarrier((&___V1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBSREQUEST_T3D10DD9716C0C92C615CC9C303CB4C07230900D0_H
#ifndef ELGAMALPARAMETER_TE0A6853A4B770C3C8FDF6D43751487B7DDA54158_H
#define ELGAMALPARAMETER_TE0A6853A4B770C3C8FDF6D43751487B7DDA54158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.ElGamalParameter
struct  ElGamalParameter_tE0A6853A4B770C3C8FDF6D43751487B7DDA54158  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.ElGamalParameter::p
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___p_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Oiw.ElGamalParameter::g
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___g_3;

public:
	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(ElGamalParameter_tE0A6853A4B770C3C8FDF6D43751487B7DDA54158, ___p_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_p_2() const { return ___p_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier((&___p_2), value);
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(ElGamalParameter_tE0A6853A4B770C3C8FDF6D43751487B7DDA54158, ___g_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_g_3() const { return ___g_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___g_3 = value;
		Il2CppCodeGenWriteBarrier((&___g_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPARAMETER_TE0A6853A4B770C3C8FDF6D43751487B7DDA54158_H
#ifndef ATTRIBUTEPKCS_TB0FC1E66550949351C65BC4B249730B879B5673D_H
#define ATTRIBUTEPKCS_TB0FC1E66550949351C65BC4B249730B879B5673D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.AttributePkcs
struct  AttributePkcs_tB0FC1E66550949351C65BC4B249730B879B5673D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.AttributePkcs::attrType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___attrType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.AttributePkcs::attrValues
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attrValues_3;

public:
	inline static int32_t get_offset_of_attrType_2() { return static_cast<int32_t>(offsetof(AttributePkcs_tB0FC1E66550949351C65BC4B249730B879B5673D, ___attrType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_attrType_2() const { return ___attrType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_attrType_2() { return &___attrType_2; }
	inline void set_attrType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___attrType_2 = value;
		Il2CppCodeGenWriteBarrier((&___attrType_2), value);
	}

	inline static int32_t get_offset_of_attrValues_3() { return static_cast<int32_t>(offsetof(AttributePkcs_tB0FC1E66550949351C65BC4B249730B879B5673D, ___attrValues_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attrValues_3() const { return ___attrValues_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attrValues_3() { return &___attrValues_3; }
	inline void set_attrValues_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attrValues_3 = value;
		Il2CppCodeGenWriteBarrier((&___attrValues_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPKCS_TB0FC1E66550949351C65BC4B249730B879B5673D_H
#ifndef AUTHENTICATEDSAFE_TDB866D3A83929A01FA9AD0E0186B92126BD6925A_H
#define AUTHENTICATEDSAFE_TDB866D3A83929A01FA9AD0E0186B92126BD6925A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.AuthenticatedSafe
struct  AuthenticatedSafe_tDB866D3A83929A01FA9AD0E0186B92126BD6925A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.AuthenticatedSafe::info
	ContentInfoU5BU5D_tF44A316B0082B249A8FEDDC4CF3B69A48FBBABED* ___info_2;

public:
	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(AuthenticatedSafe_tDB866D3A83929A01FA9AD0E0186B92126BD6925A, ___info_2)); }
	inline ContentInfoU5BU5D_tF44A316B0082B249A8FEDDC4CF3B69A48FBBABED* get_info_2() const { return ___info_2; }
	inline ContentInfoU5BU5D_tF44A316B0082B249A8FEDDC4CF3B69A48FBBABED** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(ContentInfoU5BU5D_tF44A316B0082B249A8FEDDC4CF3B69A48FBBABED* value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDSAFE_TDB866D3A83929A01FA9AD0E0186B92126BD6925A_H
#ifndef CERTBAG_TB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123_H
#define CERTBAG_TB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertBag
struct  CertBag_tB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertBag::certID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___certID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertBag::certValue
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___certValue_3;

public:
	inline static int32_t get_offset_of_certID_2() { return static_cast<int32_t>(offsetof(CertBag_tB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123, ___certID_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_certID_2() const { return ___certID_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_certID_2() { return &___certID_2; }
	inline void set_certID_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___certID_2 = value;
		Il2CppCodeGenWriteBarrier((&___certID_2), value);
	}

	inline static int32_t get_offset_of_certValue_3() { return static_cast<int32_t>(offsetof(CertBag_tB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123, ___certValue_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_certValue_3() const { return ___certValue_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_certValue_3() { return &___certValue_3; }
	inline void set_certValue_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___certValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___certValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTBAG_TB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123_H
#ifndef CERTIFICATIONREQUEST_T3FC3548FB0929D05CBF794C0534713167411C990_H
#define CERTIFICATIONREQUEST_T3FC3548FB0929D05CBF794C0534713167411C990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest
struct  CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest::reqInfo
	CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 * ___reqInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest::sigAlgId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___sigAlgId_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequest::sigBits
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___sigBits_4;

public:
	inline static int32_t get_offset_of_reqInfo_2() { return static_cast<int32_t>(offsetof(CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990, ___reqInfo_2)); }
	inline CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 * get_reqInfo_2() const { return ___reqInfo_2; }
	inline CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 ** get_address_of_reqInfo_2() { return &___reqInfo_2; }
	inline void set_reqInfo_2(CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667 * value)
	{
		___reqInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___reqInfo_2), value);
	}

	inline static int32_t get_offset_of_sigAlgId_3() { return static_cast<int32_t>(offsetof(CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990, ___sigAlgId_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_sigAlgId_3() const { return ___sigAlgId_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_sigAlgId_3() { return &___sigAlgId_3; }
	inline void set_sigAlgId_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___sigAlgId_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigAlgId_3), value);
	}

	inline static int32_t get_offset_of_sigBits_4() { return static_cast<int32_t>(offsetof(CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990, ___sigBits_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_sigBits_4() const { return ___sigBits_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_sigBits_4() { return &___sigBits_4; }
	inline void set_sigBits_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___sigBits_4 = value;
		Il2CppCodeGenWriteBarrier((&___sigBits_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATIONREQUEST_T3FC3548FB0929D05CBF794C0534713167411C990_H
#ifndef CERTIFICATIONREQUESTINFO_T759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667_H
#define CERTIFICATIONREQUESTINFO_T759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo
struct  CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo::subjectPKInfo
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___subjectPKInfo_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.CertificationRequestInfo::attributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attributes_5;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_subject_3() { return static_cast<int32_t>(offsetof(CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667, ___subject_3)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_3() const { return ___subject_3; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_3() { return &___subject_3; }
	inline void set_subject_3(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_3 = value;
		Il2CppCodeGenWriteBarrier((&___subject_3), value);
	}

	inline static int32_t get_offset_of_subjectPKInfo_4() { return static_cast<int32_t>(offsetof(CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667, ___subjectPKInfo_4)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_subjectPKInfo_4() const { return ___subjectPKInfo_4; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_subjectPKInfo_4() { return &___subjectPKInfo_4; }
	inline void set_subjectPKInfo_4(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___subjectPKInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___subjectPKInfo_4), value);
	}

	inline static int32_t get_offset_of_attributes_5() { return static_cast<int32_t>(offsetof(CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667, ___attributes_5)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attributes_5() const { return ___attributes_5; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attributes_5() { return &___attributes_5; }
	inline void set_attributes_5(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATIONREQUESTINFO_T759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667_H
#ifndef CONTENTINFO_T9964D2D1E644A18E982B32D4D2B255CA1247B333_H
#define CONTENTINFO_T9964D2D1E644A18E982B32D4D2B255CA1247B333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo
struct  ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo::contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___contentType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo::content
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___content_3;

public:
	inline static int32_t get_offset_of_contentType_2() { return static_cast<int32_t>(offsetof(ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333, ___contentType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_contentType_2() const { return ___contentType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_contentType_2() { return &___contentType_2; }
	inline void set_contentType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___contentType_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_2), value);
	}

	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333, ___content_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_content_3() const { return ___content_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier((&___content_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTINFO_T9964D2D1E644A18E982B32D4D2B255CA1247B333_H
#ifndef DHPARAMETER_T0E9A1766D60FE777DC842EA57E8B59E5E3D88F03_H
#define DHPARAMETER_T0E9A1766D60FE777DC842EA57E8B59E5E3D88F03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.DHParameter
struct  DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.DHParameter::p
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___p_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.DHParameter::g
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___g_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.DHParameter::l
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___l_4;

public:
	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03, ___p_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_p_2() const { return ___p_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier((&___p_2), value);
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03, ___g_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_g_3() const { return ___g_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___g_3 = value;
		Il2CppCodeGenWriteBarrier((&___g_3), value);
	}

	inline static int32_t get_offset_of_l_4() { return static_cast<int32_t>(offsetof(DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03, ___l_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_l_4() const { return ___l_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_l_4() { return &___l_4; }
	inline void set_l_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___l_4 = value;
		Il2CppCodeGenWriteBarrier((&___l_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPARAMETER_T0E9A1766D60FE777DC842EA57E8B59E5E3D88F03_H
#ifndef ENCRYPTEDDATA_TAB3D0E458B2974A7FDB4997162AF8AC419C099A3_H
#define ENCRYPTEDDATA_TAB3D0E458B2974A7FDB4997162AF8AC419C099A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedData
struct  EncryptedData_tAB3D0E458B2974A7FDB4997162AF8AC419C099A3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedData::data
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(EncryptedData_tAB3D0E458B2974A7FDB4997162AF8AC419C099A3, ___data_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_data_2() const { return ___data_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDDATA_TAB3D0E458B2974A7FDB4997162AF8AC419C099A3_H
#ifndef ENCRYPTEDPRIVATEKEYINFO_TF57C0CFDBCD2013FD26FB114662389F3A9B4D873_H
#define ENCRYPTEDPRIVATEKEYINFO_TF57C0CFDBCD2013FD26FB114662389F3A9B4D873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedPrivateKeyInfo
struct  EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedPrivateKeyInfo::algId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptedPrivateKeyInfo::data
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___data_3;

public:
	inline static int32_t get_offset_of_algId_2() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873, ___algId_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algId_2() const { return ___algId_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algId_2() { return &___algId_2; }
	inline void set_algId_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algId_2 = value;
		Il2CppCodeGenWriteBarrier((&___algId_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873, ___data_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_data_3() const { return ___data_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDPRIVATEKEYINFO_TF57C0CFDBCD2013FD26FB114662389F3A9B4D873_H
#ifndef ISSUERANDSERIALNUMBER_TA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9_H
#define ISSUERANDSERIALNUMBER_TA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.IssuerAndSerialNumber
struct  IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.IssuerAndSerialNumber::name
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___name_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.IssuerAndSerialNumber::certSerialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certSerialNumber_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9, ___name_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_name_2() const { return ___name_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_certSerialNumber_3() { return static_cast<int32_t>(offsetof(IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9, ___certSerialNumber_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certSerialNumber_3() const { return ___certSerialNumber_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certSerialNumber_3() { return &___certSerialNumber_3; }
	inline void set_certSerialNumber_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certSerialNumber_3 = value;
		Il2CppCodeGenWriteBarrier((&___certSerialNumber_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISSUERANDSERIALNUMBER_TA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9_H
#ifndef MACDATA_T5250DED1EA72B51F5EC9622D1376FD5191E48180_H
#define MACDATA_T5250DED1EA72B51F5EC9622D1376FD5191E48180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.MacData
struct  MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.DigestInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.MacData::digInfo
	DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 * ___digInfo_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.MacData::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.MacData::iterationCount
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___iterationCount_4;

public:
	inline static int32_t get_offset_of_digInfo_2() { return static_cast<int32_t>(offsetof(MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180, ___digInfo_2)); }
	inline DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 * get_digInfo_2() const { return ___digInfo_2; }
	inline DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 ** get_address_of_digInfo_2() { return &___digInfo_2; }
	inline void set_digInfo_2(DigestInfo_tB45D2AE982ABCE52832F57B2D89F996C42354D70 * value)
	{
		___digInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___digInfo_2), value);
	}

	inline static int32_t get_offset_of_salt_3() { return static_cast<int32_t>(offsetof(MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180, ___salt_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_3() const { return ___salt_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_3() { return &___salt_3; }
	inline void set_salt_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_3 = value;
		Il2CppCodeGenWriteBarrier((&___salt_3), value);
	}

	inline static int32_t get_offset_of_iterationCount_4() { return static_cast<int32_t>(offsetof(MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180, ___iterationCount_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_iterationCount_4() const { return ___iterationCount_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_iterationCount_4() { return &___iterationCount_4; }
	inline void set_iterationCount_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___iterationCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___iterationCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACDATA_T5250DED1EA72B51F5EC9622D1376FD5191E48180_H
#ifndef PBEPARAMETER_TD22BA15864F16F467555F0B0D8A1C501FAC3344B_H
#define PBEPARAMETER_TD22BA15864F16F467555F0B0D8A1C501FAC3344B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PbeParameter
struct  PbeParameter_tD22BA15864F16F467555F0B0D8A1C501FAC3344B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PbeParameter::salt
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___salt_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PbeParameter::iterationCount
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___iterationCount_3;

public:
	inline static int32_t get_offset_of_salt_2() { return static_cast<int32_t>(offsetof(PbeParameter_tD22BA15864F16F467555F0B0D8A1C501FAC3344B, ___salt_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_salt_2() const { return ___salt_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_salt_2() { return &___salt_2; }
	inline void set_salt_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___salt_2 = value;
		Il2CppCodeGenWriteBarrier((&___salt_2), value);
	}

	inline static int32_t get_offset_of_iterationCount_3() { return static_cast<int32_t>(offsetof(PbeParameter_tD22BA15864F16F467555F0B0D8A1C501FAC3344B, ___iterationCount_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_iterationCount_3() const { return ___iterationCount_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_iterationCount_3() { return &___iterationCount_3; }
	inline void set_iterationCount_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___iterationCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___iterationCount_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBEPARAMETER_TD22BA15864F16F467555F0B0D8A1C501FAC3344B_H
#ifndef PBES2PARAMETERS_T94FADE77C15FBF5307484B80E3232EAC7A8F6CBC_H
#define PBES2PARAMETERS_T94FADE77C15FBF5307484B80E3232EAC7A8F6CBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PbeS2Parameters
struct  PbeS2Parameters_t94FADE77C15FBF5307484B80E3232EAC7A8F6CBC  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.KeyDerivationFunc BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PbeS2Parameters::func
	KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F * ___func_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptionScheme BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PbeS2Parameters::scheme
	EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8 * ___scheme_3;

public:
	inline static int32_t get_offset_of_func_2() { return static_cast<int32_t>(offsetof(PbeS2Parameters_t94FADE77C15FBF5307484B80E3232EAC7A8F6CBC, ___func_2)); }
	inline KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F * get_func_2() const { return ___func_2; }
	inline KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F ** get_address_of_func_2() { return &___func_2; }
	inline void set_func_2(KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F * value)
	{
		___func_2 = value;
		Il2CppCodeGenWriteBarrier((&___func_2), value);
	}

	inline static int32_t get_offset_of_scheme_3() { return static_cast<int32_t>(offsetof(PbeS2Parameters_t94FADE77C15FBF5307484B80E3232EAC7A8F6CBC, ___scheme_3)); }
	inline EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8 * get_scheme_3() const { return ___scheme_3; }
	inline EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8 ** get_address_of_scheme_3() { return &___scheme_3; }
	inline void set_scheme_3(EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8 * value)
	{
		___scheme_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBES2PARAMETERS_T94FADE77C15FBF5307484B80E3232EAC7A8F6CBC_H
#ifndef PBKDF2PARAMS_T7B10C98E4886B7EB59613AB54F1901952FB94A8E_H
#define PBKDF2PARAMS_T7B10C98E4886B7EB59613AB54F1901952FB94A8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pbkdf2Params
struct  Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pbkdf2Params::octStr
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___octStr_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pbkdf2Params::iterationCount
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___iterationCount_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pbkdf2Params::keyLength
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___keyLength_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pbkdf2Params::prf
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___prf_6;

public:
	inline static int32_t get_offset_of_octStr_3() { return static_cast<int32_t>(offsetof(Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E, ___octStr_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_octStr_3() const { return ___octStr_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_octStr_3() { return &___octStr_3; }
	inline void set_octStr_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___octStr_3 = value;
		Il2CppCodeGenWriteBarrier((&___octStr_3), value);
	}

	inline static int32_t get_offset_of_iterationCount_4() { return static_cast<int32_t>(offsetof(Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E, ___iterationCount_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_iterationCount_4() const { return ___iterationCount_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_iterationCount_4() { return &___iterationCount_4; }
	inline void set_iterationCount_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___iterationCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___iterationCount_4), value);
	}

	inline static int32_t get_offset_of_keyLength_5() { return static_cast<int32_t>(offsetof(Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E, ___keyLength_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_keyLength_5() const { return ___keyLength_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_keyLength_5() { return &___keyLength_5; }
	inline void set_keyLength_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___keyLength_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyLength_5), value);
	}

	inline static int32_t get_offset_of_prf_6() { return static_cast<int32_t>(offsetof(Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E, ___prf_6)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_prf_6() const { return ___prf_6; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_prf_6() { return &___prf_6; }
	inline void set_prf_6(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___prf_6 = value;
		Il2CppCodeGenWriteBarrier((&___prf_6), value);
	}
};

struct Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pbkdf2Params::algid_hmacWithSHA1
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algid_hmacWithSHA1_2;

public:
	inline static int32_t get_offset_of_algid_hmacWithSHA1_2() { return static_cast<int32_t>(offsetof(Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E_StaticFields, ___algid_hmacWithSHA1_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algid_hmacWithSHA1_2() const { return ___algid_hmacWithSHA1_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algid_hmacWithSHA1_2() { return &___algid_hmacWithSHA1_2; }
	inline void set_algid_hmacWithSHA1_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algid_hmacWithSHA1_2 = value;
		Il2CppCodeGenWriteBarrier((&___algid_hmacWithSHA1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBKDF2PARAMS_T7B10C98E4886B7EB59613AB54F1901952FB94A8E_H
#ifndef PFX_T54484D6B5406C8B1391FC7907B97E2060AC29BF3_H
#define PFX_T54484D6B5406C8B1391FC7907B97E2060AC29BF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pfx
struct  Pfx_t54484D6B5406C8B1391FC7907B97E2060AC29BF3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pfx::contentInfo
	ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 * ___contentInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.MacData BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pfx::macData
	MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180 * ___macData_3;

public:
	inline static int32_t get_offset_of_contentInfo_2() { return static_cast<int32_t>(offsetof(Pfx_t54484D6B5406C8B1391FC7907B97E2060AC29BF3, ___contentInfo_2)); }
	inline ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 * get_contentInfo_2() const { return ___contentInfo_2; }
	inline ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 ** get_address_of_contentInfo_2() { return &___contentInfo_2; }
	inline void set_contentInfo_2(ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 * value)
	{
		___contentInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_2), value);
	}

	inline static int32_t get_offset_of_macData_3() { return static_cast<int32_t>(offsetof(Pfx_t54484D6B5406C8B1391FC7907B97E2060AC29BF3, ___macData_3)); }
	inline MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180 * get_macData_3() const { return ___macData_3; }
	inline MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180 ** get_address_of_macData_3() { return &___macData_3; }
	inline void set_macData_3(MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180 * value)
	{
		___macData_3 = value;
		Il2CppCodeGenWriteBarrier((&___macData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PFX_T54484D6B5406C8B1391FC7907B97E2060AC29BF3_H
#ifndef PKCS12PBEPARAMS_T05847F5A3EFC046E9D30EE859A8640DD1B40E59D_H
#define PKCS12PBEPARAMS_T05847F5A3EFC046E9D30EE859A8640DD1B40E59D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pkcs12PbeParams
struct  Pkcs12PbeParams_t05847F5A3EFC046E9D30EE859A8640DD1B40E59D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pkcs12PbeParams::iterations
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___iterations_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.Pkcs12PbeParams::iv
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___iv_3;

public:
	inline static int32_t get_offset_of_iterations_2() { return static_cast<int32_t>(offsetof(Pkcs12PbeParams_t05847F5A3EFC046E9D30EE859A8640DD1B40E59D, ___iterations_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_iterations_2() const { return ___iterations_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_iterations_2() { return &___iterations_2; }
	inline void set_iterations_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___iterations_2 = value;
		Il2CppCodeGenWriteBarrier((&___iterations_2), value);
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(Pkcs12PbeParams_t05847F5A3EFC046E9D30EE859A8640DD1B40E59D, ___iv_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_iv_3() const { return ___iv_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12PBEPARAMS_T05847F5A3EFC046E9D30EE859A8640DD1B40E59D_H
#ifndef PRIVATEKEYINFO_T294A934F04B4B3879E71E81DB82B28066A10D317_H
#define PRIVATEKEYINFO_T294A934F04B4B3879E71E81DB82B28066A10D317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo
struct  PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo::privateKeyAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___privateKeyAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo::privateKey
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___privateKey_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo::attributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attributes_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo::publicKey
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___publicKey_6;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_privateKeyAlgorithm_3() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317, ___privateKeyAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_privateKeyAlgorithm_3() const { return ___privateKeyAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_privateKeyAlgorithm_3() { return &___privateKeyAlgorithm_3; }
	inline void set_privateKeyAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___privateKeyAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___privateKeyAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_privateKey_4() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317, ___privateKey_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_privateKey_4() const { return ___privateKey_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_privateKey_4() { return &___privateKey_4; }
	inline void set_privateKey_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___privateKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_4), value);
	}

	inline static int32_t get_offset_of_attributes_5() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317, ___attributes_5)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attributes_5() const { return ___attributes_5; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attributes_5() { return &___attributes_5; }
	inline void set_attributes_5(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_5), value);
	}

	inline static int32_t get_offset_of_publicKey_6() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317, ___publicKey_6)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_publicKey_6() const { return ___publicKey_6; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_publicKey_6() { return &___publicKey_6; }
	inline void set_publicKey_6(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___publicKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYINFO_T294A934F04B4B3879E71E81DB82B28066A10D317_H
#ifndef RC2CBCPARAMETER_T1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8_H
#define RC2CBCPARAMETER_T1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RC2CbcParameter
struct  RC2CbcParameter_t1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RC2CbcParameter::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RC2CbcParameter::iv
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___iv_3;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(RC2CbcParameter_t1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_iv_3() { return static_cast<int32_t>(offsetof(RC2CbcParameter_t1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8, ___iv_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_iv_3() const { return ___iv_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_iv_3() { return &___iv_3; }
	inline void set_iv_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___iv_3 = value;
		Il2CppCodeGenWriteBarrier((&___iv_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC2CBCPARAMETER_T1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8_H
#ifndef RSAPRIVATEKEYSTRUCTURE_T91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D_H
#define RSAPRIVATEKEYSTRUCTURE_T91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure
struct  RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::modulus
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___modulus_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::publicExponent
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___publicExponent_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::privateExponent
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___privateExponent_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::prime1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___prime1_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::prime2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___prime2_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::exponent1
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___exponent1_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::exponent2
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___exponent2_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaPrivateKeyStructure::coefficient
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___coefficient_9;

public:
	inline static int32_t get_offset_of_modulus_2() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___modulus_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_modulus_2() const { return ___modulus_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_modulus_2() { return &___modulus_2; }
	inline void set_modulus_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___modulus_2 = value;
		Il2CppCodeGenWriteBarrier((&___modulus_2), value);
	}

	inline static int32_t get_offset_of_publicExponent_3() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___publicExponent_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_publicExponent_3() const { return ___publicExponent_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_publicExponent_3() { return &___publicExponent_3; }
	inline void set_publicExponent_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___publicExponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicExponent_3), value);
	}

	inline static int32_t get_offset_of_privateExponent_4() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___privateExponent_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_privateExponent_4() const { return ___privateExponent_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_privateExponent_4() { return &___privateExponent_4; }
	inline void set_privateExponent_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___privateExponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___privateExponent_4), value);
	}

	inline static int32_t get_offset_of_prime1_5() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___prime1_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_prime1_5() const { return ___prime1_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_prime1_5() { return &___prime1_5; }
	inline void set_prime1_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___prime1_5 = value;
		Il2CppCodeGenWriteBarrier((&___prime1_5), value);
	}

	inline static int32_t get_offset_of_prime2_6() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___prime2_6)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_prime2_6() const { return ___prime2_6; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_prime2_6() { return &___prime2_6; }
	inline void set_prime2_6(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___prime2_6 = value;
		Il2CppCodeGenWriteBarrier((&___prime2_6), value);
	}

	inline static int32_t get_offset_of_exponent1_7() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___exponent1_7)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_exponent1_7() const { return ___exponent1_7; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_exponent1_7() { return &___exponent1_7; }
	inline void set_exponent1_7(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___exponent1_7 = value;
		Il2CppCodeGenWriteBarrier((&___exponent1_7), value);
	}

	inline static int32_t get_offset_of_exponent2_8() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___exponent2_8)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_exponent2_8() const { return ___exponent2_8; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_exponent2_8() { return &___exponent2_8; }
	inline void set_exponent2_8(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___exponent2_8 = value;
		Il2CppCodeGenWriteBarrier((&___exponent2_8), value);
	}

	inline static int32_t get_offset_of_coefficient_9() { return static_cast<int32_t>(offsetof(RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D, ___coefficient_9)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_coefficient_9() const { return ___coefficient_9; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_coefficient_9() { return &___coefficient_9; }
	inline void set_coefficient_9(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___coefficient_9 = value;
		Il2CppCodeGenWriteBarrier((&___coefficient_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPRIVATEKEYSTRUCTURE_T91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D_H
#ifndef RSAESOAEPPARAMETERS_T961CFDFDA6F2ADF6040601D70E851771BBBB36E7_H
#define RSAESOAEPPARAMETERS_T961CFDFDA6F2ADF6040601D70E851771BBBB36E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters
struct  RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters::maskGenAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___maskGenAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters::pSourceAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___pSourceAlgorithm_4;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_maskGenAlgorithm_3() { return static_cast<int32_t>(offsetof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7, ___maskGenAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_maskGenAlgorithm_3() const { return ___maskGenAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_maskGenAlgorithm_3() { return &___maskGenAlgorithm_3; }
	inline void set_maskGenAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___maskGenAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___maskGenAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_pSourceAlgorithm_4() { return static_cast<int32_t>(offsetof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7, ___pSourceAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_pSourceAlgorithm_4() const { return ___pSourceAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_pSourceAlgorithm_4() { return &___pSourceAlgorithm_4; }
	inline void set_pSourceAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___pSourceAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___pSourceAlgorithm_4), value);
	}
};

struct RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters::DefaultHashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___DefaultHashAlgorithm_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters::DefaultMaskGenFunction
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___DefaultMaskGenFunction_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsaesOaepParameters::DefaultPSourceAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___DefaultPSourceAlgorithm_7;

public:
	inline static int32_t get_offset_of_DefaultHashAlgorithm_5() { return static_cast<int32_t>(offsetof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields, ___DefaultHashAlgorithm_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_DefaultHashAlgorithm_5() const { return ___DefaultHashAlgorithm_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_DefaultHashAlgorithm_5() { return &___DefaultHashAlgorithm_5; }
	inline void set_DefaultHashAlgorithm_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___DefaultHashAlgorithm_5 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultHashAlgorithm_5), value);
	}

	inline static int32_t get_offset_of_DefaultMaskGenFunction_6() { return static_cast<int32_t>(offsetof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields, ___DefaultMaskGenFunction_6)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_DefaultMaskGenFunction_6() const { return ___DefaultMaskGenFunction_6; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_DefaultMaskGenFunction_6() { return &___DefaultMaskGenFunction_6; }
	inline void set_DefaultMaskGenFunction_6(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___DefaultMaskGenFunction_6 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaskGenFunction_6), value);
	}

	inline static int32_t get_offset_of_DefaultPSourceAlgorithm_7() { return static_cast<int32_t>(offsetof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields, ___DefaultPSourceAlgorithm_7)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_DefaultPSourceAlgorithm_7() const { return ___DefaultPSourceAlgorithm_7; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_DefaultPSourceAlgorithm_7() { return &___DefaultPSourceAlgorithm_7; }
	inline void set_DefaultPSourceAlgorithm_7(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___DefaultPSourceAlgorithm_7 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultPSourceAlgorithm_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAESOAEPPARAMETERS_T961CFDFDA6F2ADF6040601D70E851771BBBB36E7_H
#ifndef RSASSAPSSPARAMETERS_TCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_H
#define RSASSAPSSPARAMETERS_TCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters
struct  RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::maskGenAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___maskGenAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::saltLength
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___saltLength_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::trailerField
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___trailerField_5;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_maskGenAlgorithm_3() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A, ___maskGenAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_maskGenAlgorithm_3() const { return ___maskGenAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_maskGenAlgorithm_3() { return &___maskGenAlgorithm_3; }
	inline void set_maskGenAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___maskGenAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___maskGenAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_saltLength_4() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A, ___saltLength_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_saltLength_4() const { return ___saltLength_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_saltLength_4() { return &___saltLength_4; }
	inline void set_saltLength_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___saltLength_4 = value;
		Il2CppCodeGenWriteBarrier((&___saltLength_4), value);
	}

	inline static int32_t get_offset_of_trailerField_5() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A, ___trailerField_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_trailerField_5() const { return ___trailerField_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_trailerField_5() { return &___trailerField_5; }
	inline void set_trailerField_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___trailerField_5 = value;
		Il2CppCodeGenWriteBarrier((&___trailerField_5), value);
	}
};

struct RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::DefaultHashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___DefaultHashAlgorithm_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::DefaultMaskGenFunction
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___DefaultMaskGenFunction_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::DefaultSaltLength
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___DefaultSaltLength_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.RsassaPssParameters::DefaultTrailerField
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___DefaultTrailerField_9;

public:
	inline static int32_t get_offset_of_DefaultHashAlgorithm_6() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields, ___DefaultHashAlgorithm_6)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_DefaultHashAlgorithm_6() const { return ___DefaultHashAlgorithm_6; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_DefaultHashAlgorithm_6() { return &___DefaultHashAlgorithm_6; }
	inline void set_DefaultHashAlgorithm_6(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___DefaultHashAlgorithm_6 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultHashAlgorithm_6), value);
	}

	inline static int32_t get_offset_of_DefaultMaskGenFunction_7() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields, ___DefaultMaskGenFunction_7)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_DefaultMaskGenFunction_7() const { return ___DefaultMaskGenFunction_7; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_DefaultMaskGenFunction_7() { return &___DefaultMaskGenFunction_7; }
	inline void set_DefaultMaskGenFunction_7(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___DefaultMaskGenFunction_7 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaskGenFunction_7), value);
	}

	inline static int32_t get_offset_of_DefaultSaltLength_8() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields, ___DefaultSaltLength_8)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_DefaultSaltLength_8() const { return ___DefaultSaltLength_8; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_DefaultSaltLength_8() { return &___DefaultSaltLength_8; }
	inline void set_DefaultSaltLength_8(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___DefaultSaltLength_8 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultSaltLength_8), value);
	}

	inline static int32_t get_offset_of_DefaultTrailerField_9() { return static_cast<int32_t>(offsetof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields, ___DefaultTrailerField_9)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_DefaultTrailerField_9() const { return ___DefaultTrailerField_9; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_DefaultTrailerField_9() { return &___DefaultTrailerField_9; }
	inline void set_DefaultTrailerField_9(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___DefaultTrailerField_9 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultTrailerField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSASSAPSSPARAMETERS_TCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_H
#ifndef SAFEBAG_T9DFBE82DAE47DB338435F0F2F2020E5E56E58757_H
#define SAFEBAG_T9DFBE82DAE47DB338435F0F2F2020E5E56E58757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SafeBag
struct  SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SafeBag::bagID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bagID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SafeBag::bagValue
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___bagValue_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SafeBag::bagAttributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___bagAttributes_4;

public:
	inline static int32_t get_offset_of_bagID_2() { return static_cast<int32_t>(offsetof(SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757, ___bagID_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bagID_2() const { return ___bagID_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bagID_2() { return &___bagID_2; }
	inline void set_bagID_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bagID_2 = value;
		Il2CppCodeGenWriteBarrier((&___bagID_2), value);
	}

	inline static int32_t get_offset_of_bagValue_3() { return static_cast<int32_t>(offsetof(SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757, ___bagValue_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_bagValue_3() const { return ___bagValue_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_bagValue_3() { return &___bagValue_3; }
	inline void set_bagValue_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___bagValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___bagValue_3), value);
	}

	inline static int32_t get_offset_of_bagAttributes_4() { return static_cast<int32_t>(offsetof(SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757, ___bagAttributes_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_bagAttributes_4() const { return ___bagAttributes_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_bagAttributes_4() { return &___bagAttributes_4; }
	inline void set_bagAttributes_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___bagAttributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bagAttributes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEBAG_T9DFBE82DAE47DB338435F0F2F2020E5E56E58757_H
#ifndef SIGNEDDATA_TE673D80201B53C3B123936B534830EDECE8A2A3B_H
#define SIGNEDDATA_TE673D80201B53C3B123936B534830EDECE8A2A3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData
struct  SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData::digestAlgorithms
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___digestAlgorithms_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData::contentInfo
	ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 * ___contentInfo_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData::certificates
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___certificates_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData::crls
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___crls_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignedData::signerInfos
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___signerInfos_7;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_digestAlgorithms_3() { return static_cast<int32_t>(offsetof(SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B, ___digestAlgorithms_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_digestAlgorithms_3() const { return ___digestAlgorithms_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_digestAlgorithms_3() { return &___digestAlgorithms_3; }
	inline void set_digestAlgorithms_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___digestAlgorithms_3 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithms_3), value);
	}

	inline static int32_t get_offset_of_contentInfo_4() { return static_cast<int32_t>(offsetof(SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B, ___contentInfo_4)); }
	inline ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 * get_contentInfo_4() const { return ___contentInfo_4; }
	inline ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 ** get_address_of_contentInfo_4() { return &___contentInfo_4; }
	inline void set_contentInfo_4(ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333 * value)
	{
		___contentInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___contentInfo_4), value);
	}

	inline static int32_t get_offset_of_certificates_5() { return static_cast<int32_t>(offsetof(SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B, ___certificates_5)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_certificates_5() const { return ___certificates_5; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_certificates_5() { return &___certificates_5; }
	inline void set_certificates_5(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___certificates_5 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_5), value);
	}

	inline static int32_t get_offset_of_crls_6() { return static_cast<int32_t>(offsetof(SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B, ___crls_6)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_crls_6() const { return ___crls_6; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_crls_6() { return &___crls_6; }
	inline void set_crls_6(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___crls_6 = value;
		Il2CppCodeGenWriteBarrier((&___crls_6), value);
	}

	inline static int32_t get_offset_of_signerInfos_7() { return static_cast<int32_t>(offsetof(SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B, ___signerInfos_7)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_signerInfos_7() const { return ___signerInfos_7; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_signerInfos_7() { return &___signerInfos_7; }
	inline void set_signerInfos_7(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___signerInfos_7 = value;
		Il2CppCodeGenWriteBarrier((&___signerInfos_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNEDDATA_TE673D80201B53C3B123936B534830EDECE8A2A3B_H
#ifndef SIGNERINFO_TB2BB1A7730898B3B3D998B182726BC04A6B382FB_H
#define SIGNERINFO_TB2BB1A7730898B3B3D998B182726BC04A6B382FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo
struct  SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.IssuerAndSerialNumber BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::issuerAndSerialNumber
	IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9 * ___issuerAndSerialNumber_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::digAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::authenticatedAttributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___authenticatedAttributes_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::digEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digEncryptionAlgorithm_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::encryptedDigest
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedDigest_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.SignerInfo::unauthenticatedAttributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unauthenticatedAttributes_8;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_issuerAndSerialNumber_3() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___issuerAndSerialNumber_3)); }
	inline IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9 * get_issuerAndSerialNumber_3() const { return ___issuerAndSerialNumber_3; }
	inline IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9 ** get_address_of_issuerAndSerialNumber_3() { return &___issuerAndSerialNumber_3; }
	inline void set_issuerAndSerialNumber_3(IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9 * value)
	{
		___issuerAndSerialNumber_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuerAndSerialNumber_3), value);
	}

	inline static int32_t get_offset_of_digAlgorithm_4() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___digAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digAlgorithm_4() const { return ___digAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digAlgorithm_4() { return &___digAlgorithm_4; }
	inline void set_digAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___digAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_authenticatedAttributes_5() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___authenticatedAttributes_5)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_authenticatedAttributes_5() const { return ___authenticatedAttributes_5; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_authenticatedAttributes_5() { return &___authenticatedAttributes_5; }
	inline void set_authenticatedAttributes_5(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___authenticatedAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___authenticatedAttributes_5), value);
	}

	inline static int32_t get_offset_of_digEncryptionAlgorithm_6() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___digEncryptionAlgorithm_6)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digEncryptionAlgorithm_6() const { return ___digEncryptionAlgorithm_6; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digEncryptionAlgorithm_6() { return &___digEncryptionAlgorithm_6; }
	inline void set_digEncryptionAlgorithm_6(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digEncryptionAlgorithm_6 = value;
		Il2CppCodeGenWriteBarrier((&___digEncryptionAlgorithm_6), value);
	}

	inline static int32_t get_offset_of_encryptedDigest_7() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___encryptedDigest_7)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedDigest_7() const { return ___encryptedDigest_7; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedDigest_7() { return &___encryptedDigest_7; }
	inline void set_encryptedDigest_7(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedDigest_7 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedDigest_7), value);
	}

	inline static int32_t get_offset_of_unauthenticatedAttributes_8() { return static_cast<int32_t>(offsetof(SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB, ___unauthenticatedAttributes_8)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unauthenticatedAttributes_8() const { return ___unauthenticatedAttributes_8; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unauthenticatedAttributes_8() { return &___unauthenticatedAttributes_8; }
	inline void set_unauthenticatedAttributes_8(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unauthenticatedAttributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___unauthenticatedAttributes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINFO_TB2BB1A7730898B3B3D998B182726BC04A6B382FB_H
#ifndef SECP256R1HOLDER_T96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_H
#define SECP256R1HOLDER_T96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256r1Holder
struct  Secp256r1Holder_t96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp256r1Holder_t96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp256r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp256r1Holder_t96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP256R1HOLDER_T96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_H
#ifndef SECP384R1HOLDER_TE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_H
#define SECP384R1HOLDER_TE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp384r1Holder
struct  Secp384r1Holder_tE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp384r1Holder_tE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp384r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp384r1Holder_tE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP384R1HOLDER_TE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_H
#ifndef SECP521R1HOLDER_TE15CEA4A4709788B9792D61C3E4D36B898C8AF87_H
#define SECP521R1HOLDER_TE15CEA4A4709788B9792D61C3E4D36B898C8AF87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp521r1Holder
struct  Secp521r1Holder_tE15CEA4A4709788B9792D61C3E4D36B898C8AF87  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Secp521r1Holder_tE15CEA4A4709788B9792D61C3E4D36B898C8AF87_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Secp521r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Secp521r1Holder_tE15CEA4A4709788B9792D61C3E4D36B898C8AF87_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP521R1HOLDER_TE15CEA4A4709788B9792D61C3E4D36B898C8AF87_H
#ifndef SECT113R1HOLDER_TFFB546E6027190FE185CA47AB26366B8BA0272B4_H
#define SECT113R1HOLDER_TFFB546E6027190FE185CA47AB26366B8BA0272B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r1Holder
struct  Sect113r1Holder_tFFB546E6027190FE185CA47AB26366B8BA0272B4  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect113r1Holder_tFFB546E6027190FE185CA47AB26366B8BA0272B4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect113r1Holder_tFFB546E6027190FE185CA47AB26366B8BA0272B4_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R1HOLDER_TFFB546E6027190FE185CA47AB26366B8BA0272B4_H
#ifndef SECT113R2HOLDER_T6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_H
#define SECT113R2HOLDER_T6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r2Holder
struct  Sect113r2Holder_t6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect113r2Holder_t6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect113r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect113r2Holder_t6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT113R2HOLDER_T6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_H
#ifndef SECT131R1HOLDER_T361CF2410CD1A14B3A4B581F3D29951BA00AD609_H
#define SECT131R1HOLDER_T361CF2410CD1A14B3A4B581F3D29951BA00AD609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r1Holder
struct  Sect131r1Holder_t361CF2410CD1A14B3A4B581F3D29951BA00AD609  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect131r1Holder_t361CF2410CD1A14B3A4B581F3D29951BA00AD609_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect131r1Holder_t361CF2410CD1A14B3A4B581F3D29951BA00AD609_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R1HOLDER_T361CF2410CD1A14B3A4B581F3D29951BA00AD609_H
#ifndef SECT131R2HOLDER_T65C733D6FA0743525A788993C3E543344B241A84_H
#define SECT131R2HOLDER_T65C733D6FA0743525A788993C3E543344B241A84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r2Holder
struct  Sect131r2Holder_t65C733D6FA0743525A788993C3E543344B241A84  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect131r2Holder_t65C733D6FA0743525A788993C3E543344B241A84_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect131r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect131r2Holder_t65C733D6FA0743525A788993C3E543344B241A84_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT131R2HOLDER_T65C733D6FA0743525A788993C3E543344B241A84_H
#ifndef SECT163K1HOLDER_TABE77A7C962E428B7BFF909021E1D41D07B535B9_H
#define SECT163K1HOLDER_TABE77A7C962E428B7BFF909021E1D41D07B535B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163k1Holder
struct  Sect163k1Holder_tABE77A7C962E428B7BFF909021E1D41D07B535B9  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect163k1Holder_tABE77A7C962E428B7BFF909021E1D41D07B535B9_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect163k1Holder_tABE77A7C962E428B7BFF909021E1D41D07B535B9_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163K1HOLDER_TABE77A7C962E428B7BFF909021E1D41D07B535B9_H
#ifndef SECT163R1HOLDER_T783FFDB7169309843E9D04AB46790114819F8FBD_H
#define SECT163R1HOLDER_T783FFDB7169309843E9D04AB46790114819F8FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r1Holder
struct  Sect163r1Holder_t783FFDB7169309843E9D04AB46790114819F8FBD  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect163r1Holder_t783FFDB7169309843E9D04AB46790114819F8FBD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect163r1Holder_t783FFDB7169309843E9D04AB46790114819F8FBD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R1HOLDER_T783FFDB7169309843E9D04AB46790114819F8FBD_H
#ifndef SECT163R2HOLDER_T68938809DE9CB858D450BEA8863C1093CA1E78E5_H
#define SECT163R2HOLDER_T68938809DE9CB858D450BEA8863C1093CA1E78E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r2Holder
struct  Sect163r2Holder_t68938809DE9CB858D450BEA8863C1093CA1E78E5  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect163r2Holder_t68938809DE9CB858D450BEA8863C1093CA1E78E5_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect163r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect163r2Holder_t68938809DE9CB858D450BEA8863C1093CA1E78E5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT163R2HOLDER_T68938809DE9CB858D450BEA8863C1093CA1E78E5_H
#ifndef SECT193R1HOLDER_T7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_H
#define SECT193R1HOLDER_T7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r1Holder
struct  Sect193r1Holder_t7A07E1FE873141EBEF494A5359ABBA9B4899ECAD  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect193r1Holder_t7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect193r1Holder_t7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R1HOLDER_T7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_H
#ifndef SECT193R2HOLDER_TC33A4F9C2A6E6652D72A1AFF5414D988769B510A_H
#define SECT193R2HOLDER_TC33A4F9C2A6E6652D72A1AFF5414D988769B510A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r2Holder
struct  Sect193r2Holder_tC33A4F9C2A6E6652D72A1AFF5414D988769B510A  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect193r2Holder_tC33A4F9C2A6E6652D72A1AFF5414D988769B510A_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect193r2Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect193r2Holder_tC33A4F9C2A6E6652D72A1AFF5414D988769B510A_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT193R2HOLDER_TC33A4F9C2A6E6652D72A1AFF5414D988769B510A_H
#ifndef SECT233K1HOLDER_T68108206AFD5421E7EF49208C7B38D45C570A02F_H
#define SECT233K1HOLDER_T68108206AFD5421E7EF49208C7B38D45C570A02F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233k1Holder
struct  Sect233k1Holder_t68108206AFD5421E7EF49208C7B38D45C570A02F  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect233k1Holder_t68108206AFD5421E7EF49208C7B38D45C570A02F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect233k1Holder_t68108206AFD5421E7EF49208C7B38D45C570A02F_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233K1HOLDER_T68108206AFD5421E7EF49208C7B38D45C570A02F_H
#ifndef SECT233R1HOLDER_TC172303D845BEA8F92E21461C2330213CCEA88D5_H
#define SECT233R1HOLDER_TC172303D845BEA8F92E21461C2330213CCEA88D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233r1Holder
struct  Sect233r1Holder_tC172303D845BEA8F92E21461C2330213CCEA88D5  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect233r1Holder_tC172303D845BEA8F92E21461C2330213CCEA88D5_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect233r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect233r1Holder_tC172303D845BEA8F92E21461C2330213CCEA88D5_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT233R1HOLDER_TC172303D845BEA8F92E21461C2330213CCEA88D5_H
#ifndef SECT239K1HOLDER_T72F4E23811A8B1A07989D015934739A813F80EAC_H
#define SECT239K1HOLDER_T72F4E23811A8B1A07989D015934739A813F80EAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect239k1Holder
struct  Sect239k1Holder_t72F4E23811A8B1A07989D015934739A813F80EAC  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect239k1Holder_t72F4E23811A8B1A07989D015934739A813F80EAC_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect239k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect239k1Holder_t72F4E23811A8B1A07989D015934739A813F80EAC_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT239K1HOLDER_T72F4E23811A8B1A07989D015934739A813F80EAC_H
#ifndef SECT283K1HOLDER_T7A676FE2581CA9D968CF81679A7007F5D6E201F1_H
#define SECT283K1HOLDER_T7A676FE2581CA9D968CF81679A7007F5D6E201F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283k1Holder
struct  Sect283k1Holder_t7A676FE2581CA9D968CF81679A7007F5D6E201F1  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect283k1Holder_t7A676FE2581CA9D968CF81679A7007F5D6E201F1_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect283k1Holder_t7A676FE2581CA9D968CF81679A7007F5D6E201F1_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283K1HOLDER_T7A676FE2581CA9D968CF81679A7007F5D6E201F1_H
#ifndef SECT283R1HOLDER_TFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_H
#define SECT283R1HOLDER_TFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283r1Holder
struct  Sect283r1Holder_tFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect283r1Holder_tFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect283r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect283r1Holder_tFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT283R1HOLDER_TFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_H
#ifndef SECT409K1HOLDER_T46679435D9C1FB7685677349152509A7C9E8EFCF_H
#define SECT409K1HOLDER_T46679435D9C1FB7685677349152509A7C9E8EFCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409k1Holder
struct  Sect409k1Holder_t46679435D9C1FB7685677349152509A7C9E8EFCF  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect409k1Holder_t46679435D9C1FB7685677349152509A7C9E8EFCF_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect409k1Holder_t46679435D9C1FB7685677349152509A7C9E8EFCF_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409K1HOLDER_T46679435D9C1FB7685677349152509A7C9E8EFCF_H
#ifndef SECT409R1HOLDER_T520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_H
#define SECT409R1HOLDER_T520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409r1Holder
struct  Sect409r1Holder_t520841E6D1A488CB4EA873B9EAA1C8D6DC57053D  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect409r1Holder_t520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect409r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect409r1Holder_t520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT409R1HOLDER_T520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_H
#ifndef SECT571K1HOLDER_T0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_H
#define SECT571K1HOLDER_T0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571k1Holder
struct  Sect571k1Holder_t0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect571k1Holder_t0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571k1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect571k1Holder_t0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571K1HOLDER_T0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_H
#ifndef SECT571R1HOLDER_TB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_H
#define SECT571R1HOLDER_TB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571r1Holder
struct  Sect571r1Holder_tB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct Sect571r1Holder_tB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Sec.SecNamedCurves_Sect571r1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(Sect571r1Holder_tB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECT571R1HOLDER_TB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_H
#ifndef ALGORITHMIDENTIFIER_TCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3_H
#define ALGORITHMIDENTIFIER_TCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct  AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier::algorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier::parameters
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___parameters_3;

public:
	inline static int32_t get_offset_of_algorithm_2() { return static_cast<int32_t>(offsetof(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3, ___algorithm_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithm_2() const { return ___algorithm_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithm_2() { return &___algorithm_2; }
	inline void set_algorithm_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_2), value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3, ___parameters_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_parameters_3() const { return ___parameters_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHMIDENTIFIER_TCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#define DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated
struct  DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::start
	int32_t ___start_3;

public:
	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F, ___bytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_2), value);
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F, ___start_3)); }
	inline int32_t get_start_3() const { return ___start_3; }
	inline int32_t* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(int32_t value)
	{
		___start_3 = value;
	}
};

struct DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerEnumerated::cache
	DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* ___cache_4;

public:
	inline static int32_t get_offset_of_cache_4() { return static_cast<int32_t>(offsetof(DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_StaticFields, ___cache_4)); }
	inline DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* get_cache_4() const { return ___cache_4; }
	inline DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD** get_address_of_cache_4() { return &___cache_4; }
	inline void set_cache_4(DerEnumeratedU5BU5D_tCE8F14A0DCD19581E127C6F12CAC36989A6173DD* value)
	{
		___cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERENUMERATED_TD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F_H
#ifndef DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#define DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerStringBase
struct  DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERSTRINGBASE_TBB10EC5BE1523B5985272F82694424B960436303_H
#ifndef CHOICE_T53EB0E1C59D68F556567F316F42293FC9C938F18_H
#define CHOICE_T53EB0E1C59D68F556567F316F42293FC9C938F18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.RequestedCertificate_Choice
struct  Choice_t53EB0E1C59D68F556567F316F42293FC9C938F18 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.Ocsp.RequestedCertificate_Choice::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Choice_t53EB0E1C59D68F556567F316F42293FC9C938F18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHOICE_T53EB0E1C59D68F556567F316F42293FC9C938F18_H
#ifndef CHOICE_TC743D91AEE82E283EEFECA4D68CF5F539ADDF177_H
#define CHOICE_TC743D91AEE82E283EEFECA4D68CF5F539ADDF177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.DeclarationOfMajority_Choice
struct  Choice_tC743D91AEE82E283EEFECA4D68CF5F539ADDF177 
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IsisMtt.X509.DeclarationOfMajority_Choice::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Choice_tC743D91AEE82E283EEFECA4D68CF5F539ADDF177, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHOICE_TC743D91AEE82E283EEFECA4D68CF5F539ADDF177_H
#ifndef ENCRYPTIONSCHEME_TB525AB51C24FAA672B8A2A5953FD373A651BE9B8_H
#define ENCRYPTIONSCHEME_TB525AB51C24FAA672B8A2A5953FD373A651BE9B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.EncryptionScheme
struct  EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8  : public AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTIONSCHEME_TB525AB51C24FAA672B8A2A5953FD373A651BE9B8_H
#ifndef KEYDERIVATIONFUNC_T2661E9BA4E148CAE30795033FE2607D190E9409F_H
#define KEYDERIVATIONFUNC_T2661E9BA4E148CAE30795033FE2607D190E9409F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.KeyDerivationFunc
struct  KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F  : public AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYDERIVATIONFUNC_T2661E9BA4E148CAE30795033FE2607D190E9409F_H
#ifndef DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#define DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct  DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::mPadBits
	int32_t ___mPadBits_4;

public:
	inline static int32_t get_offset_of_mData_3() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mData_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_3() const { return ___mData_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_3() { return &___mData_3; }
	inline void set_mData_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_3 = value;
		Il2CppCodeGenWriteBarrier((&___mData_3), value);
	}

	inline static int32_t get_offset_of_mPadBits_4() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374, ___mPadBits_4)); }
	inline int32_t get_mPadBits_4() const { return ___mPadBits_4; }
	inline int32_t* get_address_of_mPadBits_4() { return &___mPadBits_4; }
	inline void set_mPadBits_4(int32_t value)
	{
		___mPadBits_4 = value;
	}
};

struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields
{
public:
	// System.Char[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString::table
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___table_2;

public:
	inline static int32_t get_offset_of_table_2() { return static_cast<int32_t>(offsetof(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374_StaticFields, ___table_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_table_2() const { return ___table_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_table_2() { return &___table_2; }
	inline void set_table_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___table_2 = value;
		Il2CppCodeGenWriteBarrier((&___table_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERBITSTRING_T96C9BD19660FE417056A0EEB0187771D7EB10374_H
#ifndef DERIA5STRING_TB7145189E4E76E79FD67198F52692D7B119415DE_H
#define DERIA5STRING_TB7145189E4E76E79FD67198F52692D7B119415DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String
struct  DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE  : public DerStringBase_tBB10EC5BE1523B5985272F82694424B960436303
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String::str
	String_t* ___str_2;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERIA5STRING_TB7145189E4E76E79FD67198F52692D7B119415DE_H
#ifndef OCSPRESPONSESTATUS_TC6359981CAFEA382DA6D82A96DB875BCC72D4567_H
#define OCSPRESPONSESTATUS_TC6359981CAFEA382DA6D82A96DB875BCC72D4567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.OcspResponseStatus
struct  OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567  : public DerEnumerated_tD38A1BCC416DA6ABAFCAFA2F446D3AE1806CCE5F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPONSESTATUS_TC6359981CAFEA382DA6D82A96DB875BCC72D4567_H
#ifndef NETSCAPECERTTYPE_T4911BEF472F66879BE93F19FDFAA51F63B4BC5F2_H
#define NETSCAPECERTTYPE_T4911BEF472F66879BE93F19FDFAA51F63B4BC5F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.NetscapeCertType
struct  NetscapeCertType_t4911BEF472F66879BE93F19FDFAA51F63B4BC5F2  : public DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPECERTTYPE_T4911BEF472F66879BE93F19FDFAA51F63B4BC5F2_H
#ifndef NETSCAPEREVOCATIONURL_T2E69775386EEF895AADBF27CC9F12F1A54B9B7B8_H
#define NETSCAPEREVOCATIONURL_T2E69775386EEF895AADBF27CC9F12F1A54B9B7B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.NetscapeRevocationUrl
struct  NetscapeRevocationUrl_t2E69775386EEF895AADBF27CC9F12F1A54B9B7B8  : public DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPEREVOCATIONURL_T2E69775386EEF895AADBF27CC9F12F1A54B9B7B8_H
#ifndef VERISIGNCZAGEXTENSION_TECD1C5E797412DDD6F43C6F8E816EDE658C303CD_H
#define VERISIGNCZAGEXTENSION_TECD1C5E797412DDD6F43C6F8E816EDE658C303CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Misc.VerisignCzagExtension
struct  VerisignCzagExtension_tECD1C5E797412DDD6F43C6F8E816EDE658C303CD  : public DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERISIGNCZAGEXTENSION_TECD1C5E797412DDD6F43C6F8E816EDE658C303CD_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof (Secp256r1Holder_t96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51), -1, sizeof(Secp256r1Holder_t96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5400[1] = 
{
	Secp256r1Holder_t96FFDB8B914D9F9CE8B164F7CC6420D1AD60EC51_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof (Secp384r1Holder_tE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6), -1, sizeof(Secp384r1Holder_tE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5401[1] = 
{
	Secp384r1Holder_tE31DCA4AF73C7DF8A2969FC7FD0F22DA353BEBB6_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof (Secp521r1Holder_tE15CEA4A4709788B9792D61C3E4D36B898C8AF87), -1, sizeof(Secp521r1Holder_tE15CEA4A4709788B9792D61C3E4D36B898C8AF87_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5402[1] = 
{
	Secp521r1Holder_tE15CEA4A4709788B9792D61C3E4D36B898C8AF87_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof (Sect113r1Holder_tFFB546E6027190FE185CA47AB26366B8BA0272B4), -1, sizeof(Sect113r1Holder_tFFB546E6027190FE185CA47AB26366B8BA0272B4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5403[3] = 
{
	Sect113r1Holder_tFFB546E6027190FE185CA47AB26366B8BA0272B4_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof (Sect113r2Holder_t6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2), -1, sizeof(Sect113r2Holder_t6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5404[3] = 
{
	Sect113r2Holder_t6A11DF6AA4DF1D3F9892768B933ED4EE979F7AC2_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof (Sect131r1Holder_t361CF2410CD1A14B3A4B581F3D29951BA00AD609), -1, sizeof(Sect131r1Holder_t361CF2410CD1A14B3A4B581F3D29951BA00AD609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5405[5] = 
{
	Sect131r1Holder_t361CF2410CD1A14B3A4B581F3D29951BA00AD609_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof (Sect131r2Holder_t65C733D6FA0743525A788993C3E543344B241A84), -1, sizeof(Sect131r2Holder_t65C733D6FA0743525A788993C3E543344B241A84_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5406[5] = 
{
	Sect131r2Holder_t65C733D6FA0743525A788993C3E543344B241A84_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof (Sect163k1Holder_tABE77A7C962E428B7BFF909021E1D41D07B535B9), -1, sizeof(Sect163k1Holder_tABE77A7C962E428B7BFF909021E1D41D07B535B9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5407[5] = 
{
	Sect163k1Holder_tABE77A7C962E428B7BFF909021E1D41D07B535B9_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof (Sect163r1Holder_t783FFDB7169309843E9D04AB46790114819F8FBD), -1, sizeof(Sect163r1Holder_t783FFDB7169309843E9D04AB46790114819F8FBD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5408[5] = 
{
	Sect163r1Holder_t783FFDB7169309843E9D04AB46790114819F8FBD_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof (Sect163r2Holder_t68938809DE9CB858D450BEA8863C1093CA1E78E5), -1, sizeof(Sect163r2Holder_t68938809DE9CB858D450BEA8863C1093CA1E78E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5409[5] = 
{
	Sect163r2Holder_t68938809DE9CB858D450BEA8863C1093CA1E78E5_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { sizeof (Sect193r1Holder_t7A07E1FE873141EBEF494A5359ABBA9B4899ECAD), -1, sizeof(Sect193r1Holder_t7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5410[3] = 
{
	Sect193r1Holder_t7A07E1FE873141EBEF494A5359ABBA9B4899ECAD_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof (Sect193r2Holder_tC33A4F9C2A6E6652D72A1AFF5414D988769B510A), -1, sizeof(Sect193r2Holder_tC33A4F9C2A6E6652D72A1AFF5414D988769B510A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5411[3] = 
{
	Sect193r2Holder_tC33A4F9C2A6E6652D72A1AFF5414D988769B510A_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof (Sect233k1Holder_t68108206AFD5421E7EF49208C7B38D45C570A02F), -1, sizeof(Sect233k1Holder_t68108206AFD5421E7EF49208C7B38D45C570A02F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5412[3] = 
{
	Sect233k1Holder_t68108206AFD5421E7EF49208C7B38D45C570A02F_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof (Sect233r1Holder_tC172303D845BEA8F92E21461C2330213CCEA88D5), -1, sizeof(Sect233r1Holder_tC172303D845BEA8F92E21461C2330213CCEA88D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5413[3] = 
{
	Sect233r1Holder_tC172303D845BEA8F92E21461C2330213CCEA88D5_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof (Sect239k1Holder_t72F4E23811A8B1A07989D015934739A813F80EAC), -1, sizeof(Sect239k1Holder_t72F4E23811A8B1A07989D015934739A813F80EAC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5414[3] = 
{
	Sect239k1Holder_t72F4E23811A8B1A07989D015934739A813F80EAC_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof (Sect283k1Holder_t7A676FE2581CA9D968CF81679A7007F5D6E201F1), -1, sizeof(Sect283k1Holder_t7A676FE2581CA9D968CF81679A7007F5D6E201F1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5415[5] = 
{
	Sect283k1Holder_t7A676FE2581CA9D968CF81679A7007F5D6E201F1_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof (Sect283r1Holder_tFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB), -1, sizeof(Sect283r1Holder_tFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5416[5] = 
{
	Sect283r1Holder_tFE97FCBF3D63FF9E5087870E35E8EAA8A7F6D6EB_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof (Sect409k1Holder_t46679435D9C1FB7685677349152509A7C9E8EFCF), -1, sizeof(Sect409k1Holder_t46679435D9C1FB7685677349152509A7C9E8EFCF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5417[3] = 
{
	Sect409k1Holder_t46679435D9C1FB7685677349152509A7C9E8EFCF_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof (Sect409r1Holder_t520841E6D1A488CB4EA873B9EAA1C8D6DC57053D), -1, sizeof(Sect409r1Holder_t520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5418[3] = 
{
	Sect409r1Holder_t520841E6D1A488CB4EA873B9EAA1C8D6DC57053D_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof (Sect571k1Holder_t0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD), -1, sizeof(Sect571k1Holder_t0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5419[5] = 
{
	Sect571k1Holder_t0FA2E5231BB711FFF7C56E5C4CE5ADDD344892CD_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof (Sect571r1Holder_tB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02), -1, sizeof(Sect571r1Holder_tB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5420[5] = 
{
	Sect571r1Holder_tB6AAFAFCE01F0CCE7976C98B4A30BD6FB0FADF02_StaticFields::get_offset_of_Instance_1(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof (SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8), -1, sizeof(SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5421[34] = 
{
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_EllipticCurve_0(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT163k1_1(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT163r1_2(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT239k1_3(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT113r1_4(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT113r2_5(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP112r1_6(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP112r2_7(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP160r1_8(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP160k1_9(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP256k1_10(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT163r2_11(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT283k1_12(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT283r1_13(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT131r1_14(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT131r2_15(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT193r1_16(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT193r2_17(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT233k1_18(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT233r1_19(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP128r1_20(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP128r2_21(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP160r2_22(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP192k1_23(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP224k1_24(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP224r1_25(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP384r1_26(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP521r1_27(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT409k1_28(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT409r1_29(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT571k1_30(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecT571r1_31(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP192r1_32(),
	SecObjectIdentifiers_t2823853D647F566400C5ABCDBAF12213293859F8_StaticFields::get_offset_of_SecP256r1_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof (RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C), -1, sizeof(RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5422[20] = 
{
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_rosstandart_0(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_1(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3411_12_256_2(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3411_12_512_3(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_hmac_gost_3411_12_256_4(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_hmac_gost_3411_12_512_5(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_256_6(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_512_7(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_signwithdigest_gost_3410_12_256_8(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_signwithdigest_gost_3410_12_512_9(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_agreement_10(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_agreement_gost_3410_12_256_11(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_agreement_gost_3410_12_512_12(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_256_paramSet_13(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_256_paramSetA_14(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_512_paramSet_15(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_512_paramSetA_16(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_512_paramSetB_17(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_3410_12_512_paramSetC_18(),
	RosstandartObjectIdentifiers_t07A4384B5023BFD3F04D980D40DB18FDACCF6D9C_StaticFields::get_offset_of_id_tc26_gost_28147_param_Z_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof (AttributePkcs_tB0FC1E66550949351C65BC4B249730B879B5673D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5423[2] = 
{
	AttributePkcs_tB0FC1E66550949351C65BC4B249730B879B5673D::get_offset_of_attrType_2(),
	AttributePkcs_tB0FC1E66550949351C65BC4B249730B879B5673D::get_offset_of_attrValues_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof (AuthenticatedSafe_tDB866D3A83929A01FA9AD0E0186B92126BD6925A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5424[1] = 
{
	AuthenticatedSafe_tDB866D3A83929A01FA9AD0E0186B92126BD6925A::get_offset_of_info_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof (CertBag_tB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5425[2] = 
{
	CertBag_tB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123::get_offset_of_certID_2(),
	CertBag_tB8E7A23DBBEEB2AD9025FACBD0FC3AA21BE6E123::get_offset_of_certValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5426[3] = 
{
	CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990::get_offset_of_reqInfo_2(),
	CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990::get_offset_of_sigAlgId_3(),
	CertificationRequest_t3FC3548FB0929D05CBF794C0534713167411C990::get_offset_of_sigBits_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5427[4] = 
{
	CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667::get_offset_of_version_2(),
	CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667::get_offset_of_subject_3(),
	CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667::get_offset_of_subjectPKInfo_4(),
	CertificationRequestInfo_t759BB4EEBAD8DE9061EA99E2154C0CA1D2B37667::get_offset_of_attributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5428[2] = 
{
	ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333::get_offset_of_contentType_2(),
	ContentInfo_t9964D2D1E644A18E982B32D4D2B255CA1247B333::get_offset_of_content_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5429[3] = 
{
	DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03::get_offset_of_p_2(),
	DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03::get_offset_of_g_3(),
	DHParameter_t0E9A1766D60FE777DC842EA57E8B59E5E3D88F03::get_offset_of_l_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (EncryptedData_tAB3D0E458B2974A7FDB4997162AF8AC419C099A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5430[1] = 
{
	EncryptedData_tAB3D0E458B2974A7FDB4997162AF8AC419C099A3::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5431[2] = 
{
	EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873::get_offset_of_algId_2(),
	EncryptedPrivateKeyInfo_tF57C0CFDBCD2013FD26FB114662389F3A9B4D873::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (EncryptionScheme_tB525AB51C24FAA672B8A2A5953FD373A651BE9B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5433[2] = 
{
	IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9::get_offset_of_name_2(),
	IssuerAndSerialNumber_tA42DF49E4C5B939581E73EFAB4D0A41C8D1A04B9::get_offset_of_certSerialNumber_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (KeyDerivationFunc_t2661E9BA4E148CAE30795033FE2607D190E9409F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5435[3] = 
{
	MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180::get_offset_of_digInfo_2(),
	MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180::get_offset_of_salt_3(),
	MacData_t5250DED1EA72B51F5EC9622D1376FD5191E48180::get_offset_of_iterationCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof (PbeParameter_tD22BA15864F16F467555F0B0D8A1C501FAC3344B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5436[2] = 
{
	PbeParameter_tD22BA15864F16F467555F0B0D8A1C501FAC3344B::get_offset_of_salt_2(),
	PbeParameter_tD22BA15864F16F467555F0B0D8A1C501FAC3344B::get_offset_of_iterationCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (PbeS2Parameters_t94FADE77C15FBF5307484B80E3232EAC7A8F6CBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5437[2] = 
{
	PbeS2Parameters_t94FADE77C15FBF5307484B80E3232EAC7A8F6CBC::get_offset_of_func_2(),
	PbeS2Parameters_t94FADE77C15FBF5307484B80E3232EAC7A8F6CBC::get_offset_of_scheme_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E), -1, sizeof(Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5438[5] = 
{
	Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E_StaticFields::get_offset_of_algid_hmacWithSHA1_2(),
	Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E::get_offset_of_octStr_3(),
	Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E::get_offset_of_iterationCount_4(),
	Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E::get_offset_of_keyLength_5(),
	Pbkdf2Params_t7B10C98E4886B7EB59613AB54F1901952FB94A8E::get_offset_of_prf_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (Pfx_t54484D6B5406C8B1391FC7907B97E2060AC29BF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5439[2] = 
{
	Pfx_t54484D6B5406C8B1391FC7907B97E2060AC29BF3::get_offset_of_contentInfo_2(),
	Pfx_t54484D6B5406C8B1391FC7907B97E2060AC29BF3::get_offset_of_macData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (Pkcs12PbeParams_t05847F5A3EFC046E9D30EE859A8640DD1B40E59D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5440[2] = 
{
	Pkcs12PbeParams_t05847F5A3EFC046E9D30EE859A8640DD1B40E59D::get_offset_of_iterations_2(),
	Pkcs12PbeParams_t05847F5A3EFC046E9D30EE859A8640DD1B40E59D::get_offset_of_iv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924), -1, sizeof(PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5441[146] = 
{
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs1Oid_1(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_RsaEncryption_2(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_MD2WithRsaEncryption_3(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_MD4WithRsaEncryption_4(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_MD5WithRsaEncryption_5(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha1WithRsaEncryption_6(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SrsaOaepEncryptionSet_7(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdRsaesOaep_8(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdMgf1_9(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdPSpecified_10(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdRsassaPss_11(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha256WithRsaEncryption_12(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha384WithRsaEncryption_13(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha512WithRsaEncryption_14(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha224WithRsaEncryption_15(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha512_224WithRSAEncryption_16(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Sha512_256WithRSAEncryption_17(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_DhKeyAgreement_19(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithMD2AndDesCbc_21(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithMD2AndRC2Cbc_22(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithMD5AndDesCbc_23(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithMD5AndRC2Cbc_24(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithSha1AndDesCbc_25(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithSha1AndRC2Cbc_26(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdPbeS2_27(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdPbkdf2_28(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_DesEde3Cbc_30(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_RC2Cbc_31(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_rc4_32(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_MD2_34(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_MD4_35(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_MD5_36(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdHmacWithSha1_37(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdHmacWithSha224_38(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdHmacWithSha256_39(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdHmacWithSha384_40(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdHmacWithSha512_41(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Data_43(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SignedData_44(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_EnvelopedData_45(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SignedAndEnvelopedData_46(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_DigestedData_47(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_EncryptedData_48(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtEmailAddress_50(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtUnstructuredName_51(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtContentType_52(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtMessageDigest_53(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtSigningTime_54(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtCounterSignature_55(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtChallengePassword_56(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtUnstructuredAddress_57(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtExtendedCertificateAttributes_58(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtSigningDescription_59(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtExtensionRequest_60(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtSmimeCapabilities_61(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdSmime_62(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtFriendlyName_63(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs9AtLocalKeyID_64(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_X509CertType_65(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_X509Certificate_67(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SdsiCertificate_68(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_X509Crl_70(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlg_71(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlgEsdh_72(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlgCms3DesWrap_73(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlgCmsRC2Wrap_74(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlgPwriKek_75(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlgSsdh_76(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdRsaKem_77(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAlgAeadChaCha20Poly1305_78(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PreferSignedData_79(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_CannotDecryptAny_80(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SmimeCapabilitiesVersions_81(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAReceiptRequest_82(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCTAuthData_84(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCTTstInfo_85(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCTCompressedData_86(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCTAuthEnvelopedData_87(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCTTimestampedData_88(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCtiEtsProofOfOrigin_90(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCtiEtsProofOfReceipt_91(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCtiEtsProofOfDelivery_92(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCtiEtsProofOfSender_93(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCtiEtsProofOfApproval_94(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdCtiEtsProofOfCreation_95(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAOid_97(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAContentHint_98(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAMsgSigDigest_99(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAContentReference_100(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEncrypKeyPref_101(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAASigningCertificate_102(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAASigningCertificateV2_103(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAContentIdentifier_104(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAASignatureTimeStampToken_105(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsSigPolicyID_106(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsCommitmentType_107(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsSignerLocation_108(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsSignerAttr_109(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsOtherSigCert_110(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsContentTimestamp_111(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsCertificateRefs_112(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsRevocationRefs_113(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsCertValues_114(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsRevocationValues_115(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsEscTimeStamp_116(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsCertCrlTimestamp_117(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAEtsArchiveTimestamp_118(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAADecryptKeyID_119(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAImplCryptoAlgs_120(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAAsymmDecryptKeyID_121(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAImplCompressAlgs_122(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAACommunityIdentifiers_123(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAASigPolicyID_124(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAACommitmentType_125(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAASignerLocation_126(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdAAOtherSigCert_127(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdSpqEtsUri_129(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_IdSpqEtsUNotice_130(),
	0,
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_KeyBag_133(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_Pkcs8ShroudedKeyBag_134(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_CertBag_135(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_CrlBag_136(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SecretBag_137(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_SafeContentsBag_138(),
	0,
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithShaAnd128BitRC4_140(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithShaAnd40BitRC4_141(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithShaAnd3KeyTripleDesCbc_142(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithShaAnd2KeyTripleDesCbc_143(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbeWithShaAnd128BitRC2Cbc_144(),
	PkcsObjectIdentifiers_t3288C8E8A020DBADDE8AD3CE0BD094EA7926E924_StaticFields::get_offset_of_PbewithShaAnd40BitRC2Cbc_145(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5442[5] = 
{
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317::get_offset_of_version_2(),
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317::get_offset_of_privateKeyAlgorithm_3(),
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317::get_offset_of_privateKey_4(),
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317::get_offset_of_attributes_5(),
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317::get_offset_of_publicKey_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (RC2CbcParameter_t1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5443[2] = 
{
	RC2CbcParameter_t1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8::get_offset_of_version_2(),
	RC2CbcParameter_t1065E97DB4FC298B9D5FA5D6B41995E28FCFBAF8::get_offset_of_iv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7), -1, sizeof(RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5444[6] = 
{
	RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7::get_offset_of_hashAlgorithm_2(),
	RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7::get_offset_of_maskGenAlgorithm_3(),
	RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7::get_offset_of_pSourceAlgorithm_4(),
	RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields::get_offset_of_DefaultHashAlgorithm_5(),
	RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields::get_offset_of_DefaultMaskGenFunction_6(),
	RsaesOaepParameters_t961CFDFDA6F2ADF6040601D70E851771BBBB36E7_StaticFields::get_offset_of_DefaultPSourceAlgorithm_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5445[8] = 
{
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_modulus_2(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_publicExponent_3(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_privateExponent_4(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_prime1_5(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_prime2_6(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_exponent1_7(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_exponent2_8(),
	RsaPrivateKeyStructure_t91E6AAFFC5467FD1C8EE106E9FCF890DCF00D71D::get_offset_of_coefficient_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A), -1, sizeof(RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5446[8] = 
{
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A::get_offset_of_hashAlgorithm_2(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A::get_offset_of_maskGenAlgorithm_3(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A::get_offset_of_saltLength_4(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A::get_offset_of_trailerField_5(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields::get_offset_of_DefaultHashAlgorithm_6(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields::get_offset_of_DefaultMaskGenFunction_7(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields::get_offset_of_DefaultSaltLength_8(),
	RsassaPssParameters_tCFC29EB4E0C80BD3E6CDE778CF23BA257B62D30A_StaticFields::get_offset_of_DefaultTrailerField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5447[3] = 
{
	SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757::get_offset_of_bagID_2(),
	SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757::get_offset_of_bagValue_3(),
	SafeBag_t9DFBE82DAE47DB338435F0F2F2020E5E56E58757::get_offset_of_bagAttributes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof (SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5448[6] = 
{
	SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B::get_offset_of_version_2(),
	SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B::get_offset_of_digestAlgorithms_3(),
	SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B::get_offset_of_contentInfo_4(),
	SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B::get_offset_of_certificates_5(),
	SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B::get_offset_of_crls_6(),
	SignedData_tE673D80201B53C3B123936B534830EDECE8A2A3B::get_offset_of_signerInfos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5449[7] = 
{
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_version_2(),
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_issuerAndSerialNumber_3(),
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_digAlgorithm_4(),
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_authenticatedAttributes_5(),
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_digEncryptionAlgorithm_6(),
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_encryptedDigest_7(),
	SignerInfo_tB2BB1A7730898B3B3D998B182726BC04A6B382FB::get_offset_of_unauthenticatedAttributes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (ElGamalParameter_tE0A6853A4B770C3C8FDF6D43751487B7DDA54158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5450[2] = 
{
	ElGamalParameter_tE0A6853A4B770C3C8FDF6D43751487B7DDA54158::get_offset_of_p_2(),
	ElGamalParameter_tE0A6853A4B770C3C8FDF6D43751487B7DDA54158::get_offset_of_g_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D), -1, sizeof(OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5451[12] = 
{
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_MD4WithRsa_0(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_MD5WithRsa_1(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_MD4WithRsaEncryption_2(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_DesEcb_3(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_DesCbc_4(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_DesOfb_5(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_DesCfb_6(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_DesEde_7(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_IdSha1_8(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_DsaWithSha1_9(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_Sha1WithRsa_10(),
	OiwObjectIdentifiers_tC330CEB3A5408A5AE425AD7396D1A07FD019141D_StaticFields::get_offset_of_ElGamalAlgorithm_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[4] = 
{
	BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED::get_offset_of_tbsResponseData_2(),
	BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED::get_offset_of_signatureAlgorithm_3(),
	BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED::get_offset_of_signature_4(),
	BasicOcspResponse_t7EFCFB5EB615E106A534DFCA49C6893CB232B9ED::get_offset_of_certs_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (CertID_tD10877523D531F998848FBD48D854308134F2918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5453[4] = 
{
	CertID_tD10877523D531F998848FBD48D854308134F2918::get_offset_of_hashAlgorithm_2(),
	CertID_tD10877523D531F998848FBD48D854308134F2918::get_offset_of_issuerNameHash_3(),
	CertID_tD10877523D531F998848FBD48D854308134F2918::get_offset_of_issuerKeyHash_4(),
	CertID_tD10877523D531F998848FBD48D854308134F2918::get_offset_of_serialNumber_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof (CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5454[2] = 
{
	CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3::get_offset_of_tagNo_2(),
	CertStatus_t1B56353B64EAFF52BBB746E9A7EE6F1D2B6529B3::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof (CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5455[3] = 
{
	CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE::get_offset_of_crlUrl_2(),
	CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE::get_offset_of_crlNum_3(),
	CrlID_t63CDF185EE71A10AFD728E56D0557FD39C0F81CE::get_offset_of_crlTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2), -1, sizeof(OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5456[9] = 
{
	0,
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcsp_1(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspBasic_2(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspNonce_3(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspCrl_4(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspResponse_5(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspNocheck_6(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspArchiveCutoff_7(),
	OcspObjectIdentifiers_t6569EBEC8B9EC365E84DAA746351A7AC215EB3D2_StaticFields::get_offset_of_PkixOcspServiceLocator_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5457[2] = 
{
	OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C::get_offset_of_tbsRequest_2(),
	OcspRequest_tD617A6CB72C32FA21B8D537CE63C574A01AB7B0C::get_offset_of_optionalSignature_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5458[2] = 
{
	OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4::get_offset_of_responseStatus_2(),
	OcspResponse_t9A21A05E94CA23C6ADC9FC5BE4FA30C97926E1E4::get_offset_of_responseBytes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (OcspResponseStatus_tC6359981CAFEA382DA6D82A96DB875BCC72D4567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5459[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5460[2] = 
{
	Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2::get_offset_of_reqCert_2(),
	Request_t7DF96ABF21F115CA62C829DDCD0FCBD43BE84FE2::get_offset_of_singleRequestExtensions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof (ResponderID_t26B99AFE9E4E05948990F461880598D304592346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5461[1] = 
{
	ResponderID_t26B99AFE9E4E05948990F461880598D304592346::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5462[2] = 
{
	ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1::get_offset_of_responseType_2(),
	ResponseBytes_tDE75206E26774444407DB0BF4858B95D8D2BE4E1::get_offset_of_response_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8), -1, sizeof(ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5463[7] = 
{
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8_StaticFields::get_offset_of_V1_2(),
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8::get_offset_of_versionPresent_3(),
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8::get_offset_of_version_4(),
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8::get_offset_of_responderID_5(),
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8::get_offset_of_producedAt_6(),
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8::get_offset_of_responses_7(),
	ResponseData_t916C4C9BE5BC78D45B4FBE2673924B1B19F1CBF8::get_offset_of_responseExtensions_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5464[2] = 
{
	RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B::get_offset_of_revocationTime_2(),
	RevokedInfo_tA91D4DF893E7B2B3FF3203185A60F9BC799EA82B::get_offset_of_revocationReason_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (ServiceLocator_t1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5465[2] = 
{
	ServiceLocator_t1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03::get_offset_of_issuer_2(),
	ServiceLocator_t1D159C85C8B8BFBB11AEA5DB3CCA94EAB64DBB03::get_offset_of_locator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof (Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5466[3] = 
{
	Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278::get_offset_of_signatureAlgorithm_2(),
	Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278::get_offset_of_signatureValue_3(),
	Signature_t229E902D74284E84E51BEDE6D24BB7D0D50F8278::get_offset_of_certs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5467[5] = 
{
	SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5::get_offset_of_certID_2(),
	SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5::get_offset_of_certStatus_3(),
	SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5::get_offset_of_thisUpdate_4(),
	SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5::get_offset_of_nextUpdate_5(),
	SingleResponse_tCC53D74D61AFFD871093D41C706AD239EEBD36C5::get_offset_of_singleExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0), -1, sizeof(TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5468[6] = 
{
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0_StaticFields::get_offset_of_V1_2(),
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0::get_offset_of_version_3(),
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0::get_offset_of_requestorName_4(),
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0::get_offset_of_requestList_5(),
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0::get_offset_of_requestExtensions_6(),
	TbsRequest_t3D10DD9716C0C92C615CC9C303CB4C07230900D0::get_offset_of_versionSet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9), -1, sizeof(NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5469[6] = 
{
	NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields::get_offset_of_IdCamellia128Cbc_0(),
	NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields::get_offset_of_IdCamellia192Cbc_1(),
	NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields::get_offset_of_IdCamellia256Cbc_2(),
	NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields::get_offset_of_IdCamellia128Wrap_3(),
	NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields::get_offset_of_IdCamellia192Wrap_4(),
	NttObjectIdentifiers_t83D07721D9CD5346AB08E436C66DF5C97854CFF9_StaticFields::get_offset_of_IdCamellia256Wrap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344), -1, sizeof(NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5470[2] = 
{
	NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344_StaticFields::get_offset_of_objIds_0(),
	NistNamedCurves_tD0C397D70281A0EDD03CD5DAEF2CA33554317344_StaticFields::get_offset_of_names_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A), -1, sizeof(NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5471[57] = 
{
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_NistAlgorithm_0(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_HashAlgs_1(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha256_2(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha384_3(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha512_4(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha224_5(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha512_224_6(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha512_256_7(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha3_224_8(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha3_256_9(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha3_384_10(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdSha3_512_11(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdShake128_12(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdShake256_13(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdHMacWithSha3_224_14(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdHMacWithSha3_256_15(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdHMacWithSha3_384_16(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdHMacWithSha3_512_17(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_Aes_18(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Ecb_19(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Cbc_20(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Ofb_21(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Cfb_22(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Wrap_23(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Gcm_24(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes128Ccm_25(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Ecb_26(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Cbc_27(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Ofb_28(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Cfb_29(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Wrap_30(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Gcm_31(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes192Ccm_32(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Ecb_33(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Cbc_34(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Ofb_35(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Cfb_36(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Wrap_37(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Gcm_38(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdAes256Ccm_39(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdDsaWithSha2_40(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_DsaWithSha224_41(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_DsaWithSha256_42(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_DsaWithSha384_43(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_DsaWithSha512_44(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdDsaWithSha3_224_45(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdDsaWithSha3_256_46(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdDsaWithSha3_384_47(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdDsaWithSha3_512_48(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdEcdsaWithSha3_224_49(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdEcdsaWithSha3_256_50(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdEcdsaWithSha3_384_51(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdEcdsaWithSha3_512_52(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdRsassaPkcs1V15WithSha3_224_53(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdRsassaPkcs1V15WithSha3_256_54(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdRsassaPkcs1V15WithSha3_384_55(),
	NistObjectIdentifiers_t5946473F810F7AF197B5FEF06B8AD010DA965C2A_StaticFields::get_offset_of_IdRsassaPkcs1V15WithSha3_512_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5472[3] = 
{
	PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E::get_offset_of_pkacSeq_2(),
	PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E::get_offset_of_spki_3(),
	PublicKeyAndChallenge_tD76E4D0962738CFF58387D1BA44AF6069F93335E::get_offset_of_challenge_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (Cast5CbcParameters_tC0A8D78706D80E81D7E1A960D22474755DDC1991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5473[2] = 
{
	Cast5CbcParameters_tC0A8D78706D80E81D7E1A960D22474755DDC1991::get_offset_of_keyLength_2(),
	Cast5CbcParameters_tC0A8D78706D80E81D7E1A960D22474755DDC1991::get_offset_of_iv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (IdeaCbcPar_t8F7E384542D30D87569D73B634D5E758EB03237D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5474[1] = 
{
	IdeaCbcPar_t8F7E384542D30D87569D73B634D5E758EB03237D::get_offset_of_iv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A), -1, sizeof(MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5475[38] = 
{
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_Netscape_0(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeCertType_1(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeBaseUrl_2(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeRevocationUrl_3(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeCARevocationUrl_4(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeRenewalUrl_5(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeCAPolicyUrl_6(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeSslServerName_7(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NetscapeCertComment_8(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_Verisign_9(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_VerisignCzagExtension_10(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_VerisignPrivate_6_9_11(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_VerisignOnSiteJurisdictionHash_12(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_VerisignBitString_6_13_13(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_VerisignDnbDunsNumber_14(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_VerisignIssStrongCrypto_15(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_Novell_16(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_NovellSecurityAttribs_17(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_Entrust_18(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_EntrustVersionExtension_19(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cast5CBC_20(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_as_sys_sec_alg_ideaCBC_21(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cryptlib_22(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cryptlib_algorithm_23(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_ECB_24(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_CBC_25(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_CFB_26(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_cryptlib_algorithm_blowfish_OFB_27(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_blake2_28(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2b160_29(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2b256_30(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2b384_31(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2b512_32(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2s128_33(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2s160_34(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2s224_35(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_blake2s256_36(),
	MiscObjectIdentifiers_t434A9E0801266F7EF5B8236EDB06B959A0C7896A_StaticFields::get_offset_of_id_scrypt_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (NetscapeCertType_t4911BEF472F66879BE93F19FDFAA51F63B4BC5F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5476[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (NetscapeRevocationUrl_t2E69775386EEF895AADBF27CC9F12F1A54B9B7B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (VerisignCzagExtension_tECD1C5E797412DDD6F43C6F8E816EDE658C303CD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof (MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA), -1, sizeof(MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5479[7] = 
{
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_Microsoft_0(),
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_MicrosoftCertTemplateV1_1(),
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_MicrosoftCAVersion_2(),
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_MicrosoftPrevCACertHash_3(),
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_MicrosoftCrlNextPublish_4(),
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_MicrosoftCertTemplateV2_5(),
	MicrosoftObjectIdentifiers_tFBBEB641CB296ECA0AFAD2D639DD14DED28B42BA_StaticFields::get_offset_of_MicrosoftAppPolicies_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42), -1, sizeof(KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5480[2] = 
{
	KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_StaticFields::get_offset_of_IdSeedCbc_0(),
	KisaObjectIdentifiers_tD0D9C416E210FA4858BEE3D970CD6B7A599A7B42_StaticFields::get_offset_of_IdNpkiAppCmsSeedWrap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308), -1, sizeof(IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5481[20] = 
{
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMtt_0(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttCP_1(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttCPAccredited_2(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttAT_3(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATDateOfCertGen_4(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATProcuration_5(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATAdmission_6(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATMonetaryLimit_7(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATDeclarationOfMajority_8(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATIccsn_9(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATPKReference_10(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATRestriction_11(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATRetrieveIfAllowed_12(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATRequestedCertificate_13(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATNamingAuthorities_14(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATCertInDirSince_15(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATCertHash_16(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATNameAtBirth_17(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATAdditionalInformation_18(),
	IsisMttObjectIdentifiers_t15A1A1C61A20D72DE84B26DF8855DA7B8349B308_StaticFields::get_offset_of_IdIsisMttATLiabilityLimitationFlag_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (AdditionalInformationSyntax_t0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5482[1] = 
{
	AdditionalInformationSyntax_t0ACEAC8BA1E9B8DB68EFEC8C7F3301FD54D7198D::get_offset_of_information_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5483[3] = 
{
	Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78::get_offset_of_admissionAuthority_2(),
	Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78::get_offset_of_namingAuthority_3(),
	Admissions_tCCC4D9812D22941DA4A9842A40A21B376A22DA78::get_offset_of_professionInfos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (AdmissionSyntax_tBAB9E07D2191AEDADF4E868C165F187961AD5187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5484[2] = 
{
	AdmissionSyntax_tBAB9E07D2191AEDADF4E868C165F187961AD5187::get_offset_of_admissionAuthority_2(),
	AdmissionSyntax_tBAB9E07D2191AEDADF4E868C165F187961AD5187::get_offset_of_contentsOfAdmissions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (DeclarationOfMajority_t4E08A2FF28B11CE752A6B0E42EE23CB29E668190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5485[1] = 
{
	DeclarationOfMajority_t4E08A2FF28B11CE752A6B0E42EE23CB29E668190::get_offset_of_declaration_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (Choice_tC743D91AEE82E283EEFECA4D68CF5F539ADDF177)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5486[4] = 
{
	Choice_tC743D91AEE82E283EEFECA4D68CF5F539ADDF177::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5487[3] = 
{
	MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524::get_offset_of_currency_2(),
	MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524::get_offset_of_amount_3(),
	MonetaryLimit_t613FB7A16442DB2DEFF21612E85A0A5DADE83524::get_offset_of_exponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof (NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D), -1, sizeof(NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5488[4] = 
{
	NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D_StaticFields::get_offset_of_IdIsisMttATNamingAuthoritiesRechtWirtschaftSteuern_2(),
	NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D::get_offset_of_namingAuthorityID_3(),
	NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D::get_offset_of_namingAuthorityUrl_4(),
	NamingAuthority_tCBD021B53A00647699CB202F35ABAB0637BFA53D::get_offset_of_namingAuthorityText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof (ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5489[4] = 
{
	ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0::get_offset_of_country_2(),
	ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0::get_offset_of_typeOfSubstitution_3(),
	ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0::get_offset_of_thirdPerson_4(),
	ProcurationSyntax_t6A648A8B15671252D9D60AAC6BBFD529F2ACDEC0::get_offset_of_certRef_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A), -1, sizeof(ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5490[24] = 
{
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Rechtsanwltin_2(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Rechtsanwalt_3(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Rechtsbeistand_4(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Steuerberaterin_5(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Steuerberater_6(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Steuerbevollmchtigte_7(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Steuerbevollmchtigter_8(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Notarin_9(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Notar_10(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Notarvertreterin_11(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Notarvertreter_12(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Notariatsverwalterin_13(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Notariatsverwalter_14(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Wirtschaftsprferin_15(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Wirtschaftsprfer_16(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_VereidigteBuchprferin_17(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_VereidigterBuchprfer_18(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Patentanwltin_19(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A_StaticFields::get_offset_of_Patentanwalt_20(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A::get_offset_of_namingAuthority_21(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A::get_offset_of_professionItems_22(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A::get_offset_of_professionOids_23(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A::get_offset_of_registrationNumber_24(),
	ProfessionInfo_t4378B2EE0B720FAA892ABB6688ACB402CCA35F8A::get_offset_of_addProfessionInfo_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (Restriction_tDCC0C2CDB9AAC73C4ABE76673361922169EFD011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5491[1] = 
{
	Restriction_tDCC0C2CDB9AAC73C4ABE76673361922169EFD011::get_offset_of_restriction_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (CertHash_t1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5492[2] = 
{
	CertHash_t1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8::get_offset_of_hashAlgorithm_2(),
	CertHash_t1E5B08FA9A344152A844A4EE6090C13E8C4C7FB8::get_offset_of_certificateHash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5493[3] = 
{
	RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F::get_offset_of_cert_2(),
	RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F::get_offset_of_publicKeyCert_3(),
	RequestedCertificate_t361384DE9AB65E541101A5605E023098790EEE7F::get_offset_of_attributeCert_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (Choice_t53EB0E1C59D68F556567F316F42293FC9C938F18)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5494[4] = 
{
	Choice_t53EB0E1C59D68F556567F316F42293FC9C938F18::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (CscaMasterList_t96760FDDB5C81271096193F96B55A66115140878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5495[2] = 
{
	CscaMasterList_t96760FDDB5C81271096193F96B55A66115140878::get_offset_of_version_2(),
	CscaMasterList_t96760FDDB5C81271096193F96B55A66115140878::get_offset_of_certList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof (DataGroupHash_tD7B19EA56B858B032BCC6973F5ED2BA990035FAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5496[2] = 
{
	DataGroupHash_tD7B19EA56B858B032BCC6973F5ED2BA990035FAE::get_offset_of_dataGroupNumber_2(),
	DataGroupHash_tD7B19EA56B858B032BCC6973F5ED2BA990035FAE::get_offset_of_dataGroupHashValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof (IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F), -1, sizeof(IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5497[10] = 
{
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcao_0(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoMrtd_1(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoMrtdSecurity_2(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoLdsSecurityObject_3(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoCscaMasterList_4(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoCscaMasterListSigningKey_5(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoDocumentTypeList_6(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoAAProtocolObject_7(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoExtensions_8(),
	IcaoObjectIdentifiers_t0165C8775248E294D497F5964B7E5C8494C6093F_StaticFields::get_offset_of_IdIcaoExtensionsNamechangekeyrollover_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof (LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5498[5] = 
{
	0,
	LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838::get_offset_of_version_3(),
	LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838::get_offset_of_digestAlgorithmIdentifier_4(),
	LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838::get_offset_of_datagroupHash_5(),
	LdsSecurityObject_tD2A8B21611B0AB9288F6C7CE40249F8BD6E0F838::get_offset_of_versionInfo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5499[2] = 
{
	LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63::get_offset_of_ldsVersion_2(),
	LdsVersionInfo_t2E232DD335C469BA0E583F9FEF848280EBED4F63::get_offset_of_unicodeVersion_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
