﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.Connections.ConnectionBase
struct ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4;
// BestHTTP.Connections.HTTP2.HPACKEncoder
struct HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9;
// BestHTTP.Connections.HTTP2.HPACKEncoder/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1;
// BestHTTP.Connections.HTTP2.HTTP2SettingsManager
struct HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1;
// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry
struct HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7;
// BestHTTP.Connections.HTTP2.HTTP2Stream
struct HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF;
// BestHTTP.Connections.HTTP2.HeaderTable
struct HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5;
// BestHTTP.Connections.HTTP2.IFrameDataView
struct IFrameDataView_tF2DA4EF55DB18E02D0DDBF7E1C05BD1BD7FC4B92;
// BestHTTP.Connections.HTTPConnection
struct HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9;
// BestHTTP.Connections.KeepAliveHeader
struct KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920;
// BestHTTP.Connections.TCPConnector
struct TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B;
// BestHTTP.Core.HostDefinition
struct HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21;
// BestHTTP.Core.IHTTPRequestHandler
struct IHTTPRequestHandler_tBFA8B42C67506E0EACA044E6E5AD58801B5307CE;
// BestHTTP.Core.IProtocol
struct IProtocol_t0377FA2F84AEC7E09847106259D784F77EACE419;
// BestHTTP.Decompression.Crc.CRC32
struct CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC;
// BestHTTP.Decompression.Zlib.DeflateManager
struct DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E;
// BestHTTP.Decompression.Zlib.DeflateManager/CompressFunc
struct CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E;
// BestHTTP.Decompression.Zlib.DeflateManager/Config
struct Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB;
// BestHTTP.Decompression.Zlib.DeflateManager/Config[]
struct ConfigU5BU5D_tAE7DC78D8DD526715732DB37F1C1154179719178;
// BestHTTP.Decompression.Zlib.GZipStream
struct GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7;
// BestHTTP.Decompression.Zlib.InfTree
struct InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6;
// BestHTTP.Decompression.Zlib.InflateBlocks
struct InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15;
// BestHTTP.Decompression.Zlib.InflateCodes
struct InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC;
// BestHTTP.Decompression.Zlib.InflateManager
struct InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD;
// BestHTTP.Decompression.Zlib.StaticTree
struct StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0;
// BestHTTP.Decompression.Zlib.ZTree
struct ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13;
// BestHTTP.Decompression.Zlib.ZlibBaseStream
struct ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38;
// BestHTTP.Decompression.Zlib.ZlibCodec
struct ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A;
// BestHTTP.Extensions.BufferPoolMemoryStream
struct BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC;
// BestHTTP.Extensions.IHeartbeat[]
struct IHeartbeatU5BU5D_t2550BA90BD32197CE2F37AA55C4EF4B0C6AD16E1;
// BestHTTP.HTTPRequest
struct HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837;
// BestHTTP.HTTPResponse
struct HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB;
// BestHTTP.PlatformSupport.TcpClient.General.TcpClient
struct TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617;
// System.Action`1<BestHTTP.Core.ConnectionEventInfo>
struct Action_1_tC207FCCDFE31F6495C09EE41EB67B346F42AF049;
// System.Action`1<BestHTTP.Core.PluginEventInfo>
struct Action_1_t9B11B4F7A179E9F674BBA57321EE32F736BD49DE;
// System.Action`1<BestHTTP.Core.ProtocolEventInfo>
struct Action_1_t012B562196929040C71BDB30A1B7A031E471D0BC;
// System.Action`1<BestHTTP.Core.RequestEventInfo>
struct Action_1_t64DF736E94D00D5F7933482B079D3BDC98860698;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.ConnectionEventInfo>
struct ConcurrentQueue_1_t6C073C63A909107DDACAFB5C55AC8CF15EC6AF7F;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.PluginEventInfo>
struct ConcurrentQueue_1_tB693C2FEE1FE38C812373B81CED3FD71AB8287F3;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.ProtocolEventInfo>
struct ConcurrentQueue_1_t3AADEAB764C0BDEAEAC0BF6C39EAFC8842ECF2D2;
// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.RequestEventInfo>
struct ConcurrentQueue_1_t596AA864B0C0E402BD7BDF817F8FCBD34298B8A5;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.Core.HostConnection>
struct Dictionary_2_tBB5B8896620D13E61E7E3B12E91E62652AA5E7C1;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.Core.HostDefinition>
struct Dictionary_2_t41DB8FA778FF8009386296141AA288F5966739C0;
// System.Collections.Generic.List`1<BestHTTP.Connections.ConnectionBase>
struct List_1_tB7BE8F4CA25E208914CC3A18604F5F8FCC49E282;
// System.Collections.Generic.List`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload>
struct List_1_t81EF1D41B45FE7683A924495B47003A131DED228;
// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie>
struct List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25;
// System.Collections.Generic.List`1<BestHTTP.Core.HostConnection>
struct List_1_t98DFD9B0F2F6770489C396A6BEA60792821BE051;
// System.Collections.Generic.List`1<BestHTTP.Core.IProtocol>
struct List_1_t8821BDE04C04A5D4AF6707BF406345C49FDD9AD5;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>
struct List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0;
// System.Collections.Generic.List`1<BestHTTP.Extensions.IHeartbeat>
struct List_1_t5F1CA41A6CC7FD05868DFCEADF61962EE2DC39D1;
// System.Collections.Generic.List`1<BestHTTP.Extensions.TimerData>
struct List_1_tC0022CEFDA1D43B46DF6CA58ED7809EDFA729294;
// System.Collections.Generic.List`1<BestHTTP.HTTPRequest>
struct List_1_t0E64F6F010761063FA9757A1456A883274512FE8;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_tE32E8B1595B24244D0951765C6620ABB76B04421;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t7EE965B791A606D187CCB69569A433D4CBB36879;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.Stream[]
struct StreamU5BU5D_tE894588303A08E84A534F20B4AF3F1455AC73689;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.SByte[]
struct SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CONNECTIONHELPER_TE1139CB9236241F577AB7C8E9C1268331CE2CDD5_H
#define CONNECTIONHELPER_TE1139CB9236241F577AB7C8E9C1268331CE2CDD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.ConnectionHelper
struct  ConnectionHelper_tE1139CB9236241F577AB7C8E9C1268331CE2CDD5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONHELPER_TE1139CB9236241F577AB7C8E9C1268331CE2CDD5_H
#ifndef BUFFERHELPER_T551451CFF303B6D41F20DBA1F09605ABD7EC6458_H
#define BUFFERHELPER_T551451CFF303B6D41F20DBA1F09605ABD7EC6458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.BufferHelper
struct  BufferHelper_t551451CFF303B6D41F20DBA1F09605ABD7EC6458  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERHELPER_T551451CFF303B6D41F20DBA1F09605ABD7EC6458_H
#ifndef COMMONFRAMEVIEW_T3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7_H
#define COMMONFRAMEVIEW_T3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.CommonFrameView
struct  CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7  : public RuntimeObject
{
public:
	// System.Int64 BestHTTP.Connections.HTTP2.CommonFrameView::<Length>k__BackingField
	int64_t ___U3CLengthU3Ek__BackingField_0;
	// System.Int64 BestHTTP.Connections.HTTP2.CommonFrameView::<Position>k__BackingField
	int64_t ___U3CPositionU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<BestHTTP.Connections.HTTP2.HTTP2FrameHeaderAndPayload> BestHTTP.Connections.HTTP2.CommonFrameView::frames
	List_1_t81EF1D41B45FE7683A924495B47003A131DED228 * ___frames_2;
	// System.Int32 BestHTTP.Connections.HTTP2.CommonFrameView::currentFrameIdx
	int32_t ___currentFrameIdx_3;
	// System.Byte[] BestHTTP.Connections.HTTP2.CommonFrameView::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_4;
	// System.UInt32 BestHTTP.Connections.HTTP2.CommonFrameView::offset
	uint32_t ___offset_5;
	// System.UInt32 BestHTTP.Connections.HTTP2.CommonFrameView::maxOffset
	uint32_t ___maxOffset_6;

public:
	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___U3CLengthU3Ek__BackingField_0)); }
	inline int64_t get_U3CLengthU3Ek__BackingField_0() const { return ___U3CLengthU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CLengthU3Ek__BackingField_0() { return &___U3CLengthU3Ek__BackingField_0; }
	inline void set_U3CLengthU3Ek__BackingField_0(int64_t value)
	{
		___U3CLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___U3CPositionU3Ek__BackingField_1)); }
	inline int64_t get_U3CPositionU3Ek__BackingField_1() const { return ___U3CPositionU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CPositionU3Ek__BackingField_1() { return &___U3CPositionU3Ek__BackingField_1; }
	inline void set_U3CPositionU3Ek__BackingField_1(int64_t value)
	{
		___U3CPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_frames_2() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___frames_2)); }
	inline List_1_t81EF1D41B45FE7683A924495B47003A131DED228 * get_frames_2() const { return ___frames_2; }
	inline List_1_t81EF1D41B45FE7683A924495B47003A131DED228 ** get_address_of_frames_2() { return &___frames_2; }
	inline void set_frames_2(List_1_t81EF1D41B45FE7683A924495B47003A131DED228 * value)
	{
		___frames_2 = value;
		Il2CppCodeGenWriteBarrier((&___frames_2), value);
	}

	inline static int32_t get_offset_of_currentFrameIdx_3() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___currentFrameIdx_3)); }
	inline int32_t get_currentFrameIdx_3() const { return ___currentFrameIdx_3; }
	inline int32_t* get_address_of_currentFrameIdx_3() { return &___currentFrameIdx_3; }
	inline void set_currentFrameIdx_3(int32_t value)
	{
		___currentFrameIdx_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___data_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_4() const { return ___data_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___offset_5)); }
	inline uint32_t get_offset_5() const { return ___offset_5; }
	inline uint32_t* get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(uint32_t value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of_maxOffset_6() { return static_cast<int32_t>(offsetof(CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7, ___maxOffset_6)); }
	inline uint32_t get_maxOffset_6() const { return ___maxOffset_6; }
	inline uint32_t* get_address_of_maxOffset_6() { return &___maxOffset_6; }
	inline void set_maxOffset_6(uint32_t value)
	{
		___maxOffset_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONFRAMEVIEW_T3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7_H
#ifndef HPACKENCODER_T8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9_H
#define HPACKENCODER_T8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HPACKEncoder
struct  HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9  : public RuntimeObject
{
public:
	// BestHTTP.Connections.HTTP2.HTTP2SettingsManager BestHTTP.Connections.HTTP2.HPACKEncoder::settingsRegistry
	HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * ___settingsRegistry_0;
	// BestHTTP.Connections.HTTP2.HeaderTable BestHTTP.Connections.HTTP2.HPACKEncoder::requestTable
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 * ___requestTable_1;
	// BestHTTP.Connections.HTTP2.HeaderTable BestHTTP.Connections.HTTP2.HPACKEncoder::responseTable
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 * ___responseTable_2;

public:
	inline static int32_t get_offset_of_settingsRegistry_0() { return static_cast<int32_t>(offsetof(HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9, ___settingsRegistry_0)); }
	inline HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * get_settingsRegistry_0() const { return ___settingsRegistry_0; }
	inline HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 ** get_address_of_settingsRegistry_0() { return &___settingsRegistry_0; }
	inline void set_settingsRegistry_0(HTTP2SettingsManager_t44508FC8160A79CF7E3D6BB1A0C467E75E9209F1 * value)
	{
		___settingsRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&___settingsRegistry_0), value);
	}

	inline static int32_t get_offset_of_requestTable_1() { return static_cast<int32_t>(offsetof(HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9, ___requestTable_1)); }
	inline HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 * get_requestTable_1() const { return ___requestTable_1; }
	inline HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 ** get_address_of_requestTable_1() { return &___requestTable_1; }
	inline void set_requestTable_1(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 * value)
	{
		___requestTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___requestTable_1), value);
	}

	inline static int32_t get_offset_of_responseTable_2() { return static_cast<int32_t>(offsetof(HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9, ___responseTable_2)); }
	inline HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 * get_responseTable_2() const { return ___responseTable_2; }
	inline HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 ** get_address_of_responseTable_2() { return &___responseTable_2; }
	inline void set_responseTable_2(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5 * value)
	{
		___responseTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPACKENCODER_T8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1_H
#define U3CU3EC__DISPLAYCLASS4_0_T9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1  : public RuntimeObject
{
public:
	// BestHTTP.Connections.HTTP2.HPACKEncoder BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_0::<>4__this
	HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * ___U3CU3E4__this_0;
	// BestHTTP.Connections.HTTP2.HTTP2Stream BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_0::context
	HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF * ___context_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1, ___U3CU3E4__this_0)); }
	inline HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1, ___context_1)); }
	inline HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF * get_context_1() const { return ___context_1; }
	inline HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(HTTP2Stream_t38D20F5421551C9D095E63974F5AE1B25E301BEF * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1_H
#ifndef U3CU3EC__DISPLAYCLASS4_1_T9732EC3123084A31351DB2F73598B0053E6F7AA7_H
#define U3CU3EC__DISPLAYCLASS4_1_T9732EC3123084A31351DB2F73598B0053E6F7AA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_1
struct  U3CU3Ec__DisplayClass4_1_t9732EC3123084A31351DB2F73598B0053E6F7AA7  : public RuntimeObject
{
public:
	// BestHTTP.Extensions.BufferPoolMemoryStream BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_1::bufferStream
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * ___bufferStream_0;
	// BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_0 BestHTTP.Connections.HTTP2.HPACKEncoder_<>c__DisplayClass4_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_bufferStream_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t9732EC3123084A31351DB2F73598B0053E6F7AA7, ___bufferStream_0)); }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * get_bufferStream_0() const { return ___bufferStream_0; }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC ** get_address_of_bufferStream_0() { return &___bufferStream_0; }
	inline void set_bufferStream_0(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * value)
	{
		___bufferStream_0 = value;
		Il2CppCodeGenWriteBarrier((&___bufferStream_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t9732EC3123084A31351DB2F73598B0053E6F7AA7, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_1_T9732EC3123084A31351DB2F73598B0053E6F7AA7_H
#ifndef HTTP2FRAMEHELPER_TC2BF6C90BD0E7A12FE22B855037969293E63498F_H
#define HTTP2FRAMEHELPER_TC2BF6C90BD0E7A12FE22B855037969293E63498F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2FrameHelper
struct  HTTP2FrameHelper_tC2BF6C90BD0E7A12FE22B855037969293E63498F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2FRAMEHELPER_TC2BF6C90BD0E7A12FE22B855037969293E63498F_H
#ifndef HEADERTABLE_T363CC8C859928DB856863FE5A23D54E340A9D8D5_H
#define HEADERTABLE_T363CC8C859928DB856863FE5A23D54E340A9D8D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HeaderTable
struct  HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5  : public RuntimeObject
{
public:
	// System.UInt32 BestHTTP.Connections.HTTP2.HeaderTable::<DynamicTableSize>k__BackingField
	uint32_t ___U3CDynamicTableSizeU3Ek__BackingField_2;
	// System.UInt32 BestHTTP.Connections.HTTP2.HeaderTable::_maxDynamicTableSize
	uint32_t ____maxDynamicTableSize_3;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> BestHTTP.Connections.HTTP2.HeaderTable::DynamicTable
	List_1_tE32E8B1595B24244D0951765C6620ABB76B04421 * ___DynamicTable_4;
	// BestHTTP.Connections.HTTP2.HTTP2SettingsRegistry BestHTTP.Connections.HTTP2.HeaderTable::settingsRegistry
	HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * ___settingsRegistry_5;

public:
	inline static int32_t get_offset_of_U3CDynamicTableSizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5, ___U3CDynamicTableSizeU3Ek__BackingField_2)); }
	inline uint32_t get_U3CDynamicTableSizeU3Ek__BackingField_2() const { return ___U3CDynamicTableSizeU3Ek__BackingField_2; }
	inline uint32_t* get_address_of_U3CDynamicTableSizeU3Ek__BackingField_2() { return &___U3CDynamicTableSizeU3Ek__BackingField_2; }
	inline void set_U3CDynamicTableSizeU3Ek__BackingField_2(uint32_t value)
	{
		___U3CDynamicTableSizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of__maxDynamicTableSize_3() { return static_cast<int32_t>(offsetof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5, ____maxDynamicTableSize_3)); }
	inline uint32_t get__maxDynamicTableSize_3() const { return ____maxDynamicTableSize_3; }
	inline uint32_t* get_address_of__maxDynamicTableSize_3() { return &____maxDynamicTableSize_3; }
	inline void set__maxDynamicTableSize_3(uint32_t value)
	{
		____maxDynamicTableSize_3 = value;
	}

	inline static int32_t get_offset_of_DynamicTable_4() { return static_cast<int32_t>(offsetof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5, ___DynamicTable_4)); }
	inline List_1_tE32E8B1595B24244D0951765C6620ABB76B04421 * get_DynamicTable_4() const { return ___DynamicTable_4; }
	inline List_1_tE32E8B1595B24244D0951765C6620ABB76B04421 ** get_address_of_DynamicTable_4() { return &___DynamicTable_4; }
	inline void set_DynamicTable_4(List_1_tE32E8B1595B24244D0951765C6620ABB76B04421 * value)
	{
		___DynamicTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___DynamicTable_4), value);
	}

	inline static int32_t get_offset_of_settingsRegistry_5() { return static_cast<int32_t>(offsetof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5, ___settingsRegistry_5)); }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * get_settingsRegistry_5() const { return ___settingsRegistry_5; }
	inline HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 ** get_address_of_settingsRegistry_5() { return &___settingsRegistry_5; }
	inline void set_settingsRegistry_5(HTTP2SettingsRegistry_tD50F0B37859BDCAE91FBEFB0B7A6D05616894CB7 * value)
	{
		___settingsRegistry_5 = value;
		Il2CppCodeGenWriteBarrier((&___settingsRegistry_5), value);
	}
};

struct HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5_StaticFields
{
public:
	// System.String[] BestHTTP.Connections.HTTP2.HeaderTable::StaticTableValues
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___StaticTableValues_0;
	// System.String[] BestHTTP.Connections.HTTP2.HeaderTable::StaticTable
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___StaticTable_1;

public:
	inline static int32_t get_offset_of_StaticTableValues_0() { return static_cast<int32_t>(offsetof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5_StaticFields, ___StaticTableValues_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_StaticTableValues_0() const { return ___StaticTableValues_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_StaticTableValues_0() { return &___StaticTableValues_0; }
	inline void set_StaticTableValues_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___StaticTableValues_0 = value;
		Il2CppCodeGenWriteBarrier((&___StaticTableValues_0), value);
	}

	inline static int32_t get_offset_of_StaticTable_1() { return static_cast<int32_t>(offsetof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5_StaticFields, ___StaticTable_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_StaticTable_1() const { return ___StaticTable_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_StaticTable_1() { return &___StaticTable_1; }
	inline void set_StaticTable_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___StaticTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___StaticTable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERTABLE_T363CC8C859928DB856863FE5A23D54E340A9D8D5_H
#ifndef HTTPPROTOCOLFACTORY_T833A71496A51A056FB023081A5114248B2AF1F98_H
#define HTTPPROTOCOLFACTORY_T833A71496A51A056FB023081A5114248B2AF1F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTPProtocolFactory
struct  HTTPProtocolFactory_t833A71496A51A056FB023081A5114248B2AF1F98  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPROTOCOLFACTORY_T833A71496A51A056FB023081A5114248B2AF1F98_H
#ifndef TCPCONNECTOR_T9ACB940EBAF5660B87F6B43FE52751F7EB2D779B_H
#define TCPCONNECTOR_T9ACB940EBAF5660B87F6B43FE52751F7EB2D779B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.TCPConnector
struct  TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B  : public RuntimeObject
{
public:
	// System.String BestHTTP.Connections.TCPConnector::<NegotiatedProtocol>k__BackingField
	String_t* ___U3CNegotiatedProtocolU3Ek__BackingField_0;
	// BestHTTP.PlatformSupport.TcpClient.General.TcpClient BestHTTP.Connections.TCPConnector::<Client>k__BackingField
	TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617 * ___U3CClientU3Ek__BackingField_1;
	// System.IO.Stream BestHTTP.Connections.TCPConnector::<Stream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CStreamU3Ek__BackingField_2;
	// System.Boolean BestHTTP.Connections.TCPConnector::<LeaveOpen>k__BackingField
	bool ___U3CLeaveOpenU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNegotiatedProtocolU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B, ___U3CNegotiatedProtocolU3Ek__BackingField_0)); }
	inline String_t* get_U3CNegotiatedProtocolU3Ek__BackingField_0() const { return ___U3CNegotiatedProtocolU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNegotiatedProtocolU3Ek__BackingField_0() { return &___U3CNegotiatedProtocolU3Ek__BackingField_0; }
	inline void set_U3CNegotiatedProtocolU3Ek__BackingField_0(String_t* value)
	{
		___U3CNegotiatedProtocolU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNegotiatedProtocolU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B, ___U3CClientU3Ek__BackingField_1)); }
	inline TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617 * get_U3CClientU3Ek__BackingField_1() const { return ___U3CClientU3Ek__BackingField_1; }
	inline TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617 ** get_address_of_U3CClientU3Ek__BackingField_1() { return &___U3CClientU3Ek__BackingField_1; }
	inline void set_U3CClientU3Ek__BackingField_1(TcpClient_t3503711DD74B893F80B5BFDEEB3593CFBF520617 * value)
	{
		___U3CClientU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CStreamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B, ___U3CStreamU3Ek__BackingField_2)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CStreamU3Ek__BackingField_2() const { return ___U3CStreamU3Ek__BackingField_2; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CStreamU3Ek__BackingField_2() { return &___U3CStreamU3Ek__BackingField_2; }
	inline void set_U3CStreamU3Ek__BackingField_2(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CStreamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStreamU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CLeaveOpenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B, ___U3CLeaveOpenU3Ek__BackingField_3)); }
	inline bool get_U3CLeaveOpenU3Ek__BackingField_3() const { return ___U3CLeaveOpenU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CLeaveOpenU3Ek__BackingField_3() { return &___U3CLeaveOpenU3Ek__BackingField_3; }
	inline void set_U3CLeaveOpenU3Ek__BackingField_3(bool value)
	{
		___U3CLeaveOpenU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCPCONNECTOR_T9ACB940EBAF5660B87F6B43FE52751F7EB2D779B_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD_H
#define U3CU3EC__DISPLAYCLASS18_0_T398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.TCPConnector_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD  : public RuntimeObject
{
public:
	// BestHTTP.HTTPRequest BestHTTP.Connections.TCPConnector_<>c__DisplayClass18_0::request
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD, ___request_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_request_0() const { return ___request_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD_H
#ifndef U3CU3EC_T6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_H
#define U3CU3EC_T6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Cookies.Cookie_<>c
struct  U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_StaticFields
{
public:
	// BestHTTP.Cookies.Cookie_<>c BestHTTP.Cookies.Cookie_<>c::<>9
	U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D * ___U3CU3E9_0;
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Cookies.Cookie_<>c::<>9__61_0
	Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * ___U3CU3E9__61_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__61_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_StaticFields, ___U3CU3E9__61_0_1)); }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * get_U3CU3E9__61_0_1() const { return ___U3CU3E9__61_0_1; }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 ** get_address_of_U3CU3E9__61_0_1() { return &___U3CU3E9__61_0_1; }
	inline void set_U3CU3E9__61_0_1(Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * value)
	{
		___U3CU3E9__61_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__61_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_H
#ifndef ALTSVCEVENTINFO_TC9A7F933E6B23EE5B9646F898AF6F689CA84C210_H
#define ALTSVCEVENTINFO_TC9A7F933E6B23EE5B9646F898AF6F689CA84C210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.AltSvcEventInfo
struct  AltSvcEventInfo_tC9A7F933E6B23EE5B9646F898AF6F689CA84C210  : public RuntimeObject
{
public:
	// System.String BestHTTP.Core.AltSvcEventInfo::Host
	String_t* ___Host_0;
	// BestHTTP.HTTPResponse BestHTTP.Core.AltSvcEventInfo::Response
	HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * ___Response_1;

public:
	inline static int32_t get_offset_of_Host_0() { return static_cast<int32_t>(offsetof(AltSvcEventInfo_tC9A7F933E6B23EE5B9646F898AF6F689CA84C210, ___Host_0)); }
	inline String_t* get_Host_0() const { return ___Host_0; }
	inline String_t** get_address_of_Host_0() { return &___Host_0; }
	inline void set_Host_0(String_t* value)
	{
		___Host_0 = value;
		Il2CppCodeGenWriteBarrier((&___Host_0), value);
	}

	inline static int32_t get_offset_of_Response_1() { return static_cast<int32_t>(offsetof(AltSvcEventInfo_tC9A7F933E6B23EE5B9646F898AF6F689CA84C210, ___Response_1)); }
	inline HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * get_Response_1() const { return ___Response_1; }
	inline HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB ** get_address_of_Response_1() { return &___Response_1; }
	inline void set_Response_1(HTTPResponse_t884F650C82582A1F54E9A53B1AA131F76D12DDDB * value)
	{
		___Response_1 = value;
		Il2CppCodeGenWriteBarrier((&___Response_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALTSVCEVENTINFO_TC9A7F933E6B23EE5B9646F898AF6F689CA84C210_H
#ifndef CONNECTIONEVENTHELPER_TC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_H
#define CONNECTIONEVENTHELPER_TC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.ConnectionEventHelper
struct  ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F  : public RuntimeObject
{
public:

public:
};

struct ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_StaticFields
{
public:
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.ConnectionEventInfo> BestHTTP.Core.ConnectionEventHelper::connectionEventQueue
	ConcurrentQueue_1_t6C073C63A909107DDACAFB5C55AC8CF15EC6AF7F * ___connectionEventQueue_0;
	// System.Action`1<BestHTTP.Core.ConnectionEventInfo> BestHTTP.Core.ConnectionEventHelper::OnEvent
	Action_1_tC207FCCDFE31F6495C09EE41EB67B346F42AF049 * ___OnEvent_1;

public:
	inline static int32_t get_offset_of_connectionEventQueue_0() { return static_cast<int32_t>(offsetof(ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_StaticFields, ___connectionEventQueue_0)); }
	inline ConcurrentQueue_1_t6C073C63A909107DDACAFB5C55AC8CF15EC6AF7F * get_connectionEventQueue_0() const { return ___connectionEventQueue_0; }
	inline ConcurrentQueue_1_t6C073C63A909107DDACAFB5C55AC8CF15EC6AF7F ** get_address_of_connectionEventQueue_0() { return &___connectionEventQueue_0; }
	inline void set_connectionEventQueue_0(ConcurrentQueue_1_t6C073C63A909107DDACAFB5C55AC8CF15EC6AF7F * value)
	{
		___connectionEventQueue_0 = value;
		Il2CppCodeGenWriteBarrier((&___connectionEventQueue_0), value);
	}

	inline static int32_t get_offset_of_OnEvent_1() { return static_cast<int32_t>(offsetof(ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_StaticFields, ___OnEvent_1)); }
	inline Action_1_tC207FCCDFE31F6495C09EE41EB67B346F42AF049 * get_OnEvent_1() const { return ___OnEvent_1; }
	inline Action_1_tC207FCCDFE31F6495C09EE41EB67B346F42AF049 ** get_address_of_OnEvent_1() { return &___OnEvent_1; }
	inline void set_OnEvent_1(Action_1_tC207FCCDFE31F6495C09EE41EB67B346F42AF049 * value)
	{
		___OnEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnEvent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONEVENTHELPER_TC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_H
#ifndef HOSTDEFINITION_T380062F048C1276C64E042501E07CCDE9322BB21_H
#define HOSTDEFINITION_T380062F048C1276C64E042501E07CCDE9322BB21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.HostDefinition
struct  HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21  : public RuntimeObject
{
public:
	// System.String BestHTTP.Core.HostDefinition::<Host>k__BackingField
	String_t* ___U3CHostU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<BestHTTP.Core.HostConnection> BestHTTP.Core.HostDefinition::Alternates
	List_1_t98DFD9B0F2F6770489C396A6BEA60792821BE051 * ___Alternates_1;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.Core.HostConnection> BestHTTP.Core.HostDefinition::hostConnectionVariant
	Dictionary_2_tBB5B8896620D13E61E7E3B12E91E62652AA5E7C1 * ___hostConnectionVariant_2;

public:
	inline static int32_t get_offset_of_U3CHostU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21, ___U3CHostU3Ek__BackingField_0)); }
	inline String_t* get_U3CHostU3Ek__BackingField_0() const { return ___U3CHostU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CHostU3Ek__BackingField_0() { return &___U3CHostU3Ek__BackingField_0; }
	inline void set_U3CHostU3Ek__BackingField_0(String_t* value)
	{
		___U3CHostU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_Alternates_1() { return static_cast<int32_t>(offsetof(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21, ___Alternates_1)); }
	inline List_1_t98DFD9B0F2F6770489C396A6BEA60792821BE051 * get_Alternates_1() const { return ___Alternates_1; }
	inline List_1_t98DFD9B0F2F6770489C396A6BEA60792821BE051 ** get_address_of_Alternates_1() { return &___Alternates_1; }
	inline void set_Alternates_1(List_1_t98DFD9B0F2F6770489C396A6BEA60792821BE051 * value)
	{
		___Alternates_1 = value;
		Il2CppCodeGenWriteBarrier((&___Alternates_1), value);
	}

	inline static int32_t get_offset_of_hostConnectionVariant_2() { return static_cast<int32_t>(offsetof(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21, ___hostConnectionVariant_2)); }
	inline Dictionary_2_tBB5B8896620D13E61E7E3B12E91E62652AA5E7C1 * get_hostConnectionVariant_2() const { return ___hostConnectionVariant_2; }
	inline Dictionary_2_tBB5B8896620D13E61E7E3B12E91E62652AA5E7C1 ** get_address_of_hostConnectionVariant_2() { return &___hostConnectionVariant_2; }
	inline void set_hostConnectionVariant_2(Dictionary_2_tBB5B8896620D13E61E7E3B12E91E62652AA5E7C1 * value)
	{
		___hostConnectionVariant_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostConnectionVariant_2), value);
	}
};

struct HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21_StaticFields
{
public:
	// System.Text.StringBuilder BestHTTP.Core.HostDefinition::keyBuilder
	StringBuilder_t * ___keyBuilder_3;
	// System.Threading.ReaderWriterLockSlim BestHTTP.Core.HostDefinition::keyBuilderLock
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___keyBuilderLock_4;

public:
	inline static int32_t get_offset_of_keyBuilder_3() { return static_cast<int32_t>(offsetof(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21_StaticFields, ___keyBuilder_3)); }
	inline StringBuilder_t * get_keyBuilder_3() const { return ___keyBuilder_3; }
	inline StringBuilder_t ** get_address_of_keyBuilder_3() { return &___keyBuilder_3; }
	inline void set_keyBuilder_3(StringBuilder_t * value)
	{
		___keyBuilder_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyBuilder_3), value);
	}

	inline static int32_t get_offset_of_keyBuilderLock_4() { return static_cast<int32_t>(offsetof(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21_StaticFields, ___keyBuilderLock_4)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_keyBuilderLock_4() const { return ___keyBuilderLock_4; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_keyBuilderLock_4() { return &___keyBuilderLock_4; }
	inline void set_keyBuilderLock_4(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___keyBuilderLock_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyBuilderLock_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTDEFINITION_T380062F048C1276C64E042501E07CCDE9322BB21_H
#ifndef HOSTMANAGER_T25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_H
#define HOSTMANAGER_T25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.HostManager
struct  HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC  : public RuntimeObject
{
public:

public:
};

struct HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields
{
public:
	// System.String BestHTTP.Core.HostManager::LibraryPath
	String_t* ___LibraryPath_1;
	// System.Boolean BestHTTP.Core.HostManager::IsSaveAndLoadSupported
	bool ___IsSaveAndLoadSupported_2;
	// System.Boolean BestHTTP.Core.HostManager::IsLoaded
	bool ___IsLoaded_3;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.Core.HostDefinition> BestHTTP.Core.HostManager::hosts
	Dictionary_2_t41DB8FA778FF8009386296141AA288F5966739C0 * ___hosts_4;

public:
	inline static int32_t get_offset_of_LibraryPath_1() { return static_cast<int32_t>(offsetof(HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields, ___LibraryPath_1)); }
	inline String_t* get_LibraryPath_1() const { return ___LibraryPath_1; }
	inline String_t** get_address_of_LibraryPath_1() { return &___LibraryPath_1; }
	inline void set_LibraryPath_1(String_t* value)
	{
		___LibraryPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___LibraryPath_1), value);
	}

	inline static int32_t get_offset_of_IsSaveAndLoadSupported_2() { return static_cast<int32_t>(offsetof(HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields, ___IsSaveAndLoadSupported_2)); }
	inline bool get_IsSaveAndLoadSupported_2() const { return ___IsSaveAndLoadSupported_2; }
	inline bool* get_address_of_IsSaveAndLoadSupported_2() { return &___IsSaveAndLoadSupported_2; }
	inline void set_IsSaveAndLoadSupported_2(bool value)
	{
		___IsSaveAndLoadSupported_2 = value;
	}

	inline static int32_t get_offset_of_IsLoaded_3() { return static_cast<int32_t>(offsetof(HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields, ___IsLoaded_3)); }
	inline bool get_IsLoaded_3() const { return ___IsLoaded_3; }
	inline bool* get_address_of_IsLoaded_3() { return &___IsLoaded_3; }
	inline void set_IsLoaded_3(bool value)
	{
		___IsLoaded_3 = value;
	}

	inline static int32_t get_offset_of_hosts_4() { return static_cast<int32_t>(offsetof(HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields, ___hosts_4)); }
	inline Dictionary_2_t41DB8FA778FF8009386296141AA288F5966739C0 * get_hosts_4() const { return ___hosts_4; }
	inline Dictionary_2_t41DB8FA778FF8009386296141AA288F5966739C0 ** get_address_of_hosts_4() { return &___hosts_4; }
	inline void set_hosts_4(Dictionary_2_t41DB8FA778FF8009386296141AA288F5966739C0 * value)
	{
		___hosts_4 = value;
		Il2CppCodeGenWriteBarrier((&___hosts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTMANAGER_T25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_H
#ifndef PLUGINEVENTHELPER_T4600946ADF75381D11718A87FBAFDA1883035554_H
#define PLUGINEVENTHELPER_T4600946ADF75381D11718A87FBAFDA1883035554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.PluginEventHelper
struct  PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554  : public RuntimeObject
{
public:

public:
};

struct PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554_StaticFields
{
public:
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.PluginEventInfo> BestHTTP.Core.PluginEventHelper::pluginEvents
	ConcurrentQueue_1_tB693C2FEE1FE38C812373B81CED3FD71AB8287F3 * ___pluginEvents_0;
	// System.Action`1<BestHTTP.Core.PluginEventInfo> BestHTTP.Core.PluginEventHelper::OnEvent
	Action_1_t9B11B4F7A179E9F674BBA57321EE32F736BD49DE * ___OnEvent_1;

public:
	inline static int32_t get_offset_of_pluginEvents_0() { return static_cast<int32_t>(offsetof(PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554_StaticFields, ___pluginEvents_0)); }
	inline ConcurrentQueue_1_tB693C2FEE1FE38C812373B81CED3FD71AB8287F3 * get_pluginEvents_0() const { return ___pluginEvents_0; }
	inline ConcurrentQueue_1_tB693C2FEE1FE38C812373B81CED3FD71AB8287F3 ** get_address_of_pluginEvents_0() { return &___pluginEvents_0; }
	inline void set_pluginEvents_0(ConcurrentQueue_1_tB693C2FEE1FE38C812373B81CED3FD71AB8287F3 * value)
	{
		___pluginEvents_0 = value;
		Il2CppCodeGenWriteBarrier((&___pluginEvents_0), value);
	}

	inline static int32_t get_offset_of_OnEvent_1() { return static_cast<int32_t>(offsetof(PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554_StaticFields, ___OnEvent_1)); }
	inline Action_1_t9B11B4F7A179E9F674BBA57321EE32F736BD49DE * get_OnEvent_1() const { return ___OnEvent_1; }
	inline Action_1_t9B11B4F7A179E9F674BBA57321EE32F736BD49DE ** get_address_of_OnEvent_1() { return &___OnEvent_1; }
	inline void set_OnEvent_1(Action_1_t9B11B4F7A179E9F674BBA57321EE32F736BD49DE * value)
	{
		___OnEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnEvent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINEVENTHELPER_T4600946ADF75381D11718A87FBAFDA1883035554_H
#ifndef PROTOCOLEVENTHELPER_T7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_H
#define PROTOCOLEVENTHELPER_T7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.ProtocolEventHelper
struct  ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79  : public RuntimeObject
{
public:

public:
};

struct ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields
{
public:
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.ProtocolEventInfo> BestHTTP.Core.ProtocolEventHelper::protocolEvents
	ConcurrentQueue_1_t3AADEAB764C0BDEAEAC0BF6C39EAFC8842ECF2D2 * ___protocolEvents_0;
	// System.Collections.Generic.List`1<BestHTTP.Core.IProtocol> BestHTTP.Core.ProtocolEventHelper::ActiveProtocols
	List_1_t8821BDE04C04A5D4AF6707BF406345C49FDD9AD5 * ___ActiveProtocols_1;
	// System.Action`1<BestHTTP.Core.ProtocolEventInfo> BestHTTP.Core.ProtocolEventHelper::OnEvent
	Action_1_t012B562196929040C71BDB30A1B7A031E471D0BC * ___OnEvent_2;

public:
	inline static int32_t get_offset_of_protocolEvents_0() { return static_cast<int32_t>(offsetof(ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields, ___protocolEvents_0)); }
	inline ConcurrentQueue_1_t3AADEAB764C0BDEAEAC0BF6C39EAFC8842ECF2D2 * get_protocolEvents_0() const { return ___protocolEvents_0; }
	inline ConcurrentQueue_1_t3AADEAB764C0BDEAEAC0BF6C39EAFC8842ECF2D2 ** get_address_of_protocolEvents_0() { return &___protocolEvents_0; }
	inline void set_protocolEvents_0(ConcurrentQueue_1_t3AADEAB764C0BDEAEAC0BF6C39EAFC8842ECF2D2 * value)
	{
		___protocolEvents_0 = value;
		Il2CppCodeGenWriteBarrier((&___protocolEvents_0), value);
	}

	inline static int32_t get_offset_of_ActiveProtocols_1() { return static_cast<int32_t>(offsetof(ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields, ___ActiveProtocols_1)); }
	inline List_1_t8821BDE04C04A5D4AF6707BF406345C49FDD9AD5 * get_ActiveProtocols_1() const { return ___ActiveProtocols_1; }
	inline List_1_t8821BDE04C04A5D4AF6707BF406345C49FDD9AD5 ** get_address_of_ActiveProtocols_1() { return &___ActiveProtocols_1; }
	inline void set_ActiveProtocols_1(List_1_t8821BDE04C04A5D4AF6707BF406345C49FDD9AD5 * value)
	{
		___ActiveProtocols_1 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveProtocols_1), value);
	}

	inline static int32_t get_offset_of_OnEvent_2() { return static_cast<int32_t>(offsetof(ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields, ___OnEvent_2)); }
	inline Action_1_t012B562196929040C71BDB30A1B7A031E471D0BC * get_OnEvent_2() const { return ___OnEvent_2; }
	inline Action_1_t012B562196929040C71BDB30A1B7A031E471D0BC ** get_address_of_OnEvent_2() { return &___OnEvent_2; }
	inline void set_OnEvent_2(Action_1_t012B562196929040C71BDB30A1B7A031E471D0BC * value)
	{
		___OnEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLEVENTHELPER_T7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_H
#ifndef REQUESTEVENTHELPER_TD5E4B7D8F15524DFAD455423114A03BDD32FB509_H
#define REQUESTEVENTHELPER_TD5E4B7D8F15524DFAD455423114A03BDD32FB509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.RequestEventHelper
struct  RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509  : public RuntimeObject
{
public:

public:
};

struct RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509_StaticFields
{
public:
	// System.Collections.Concurrent.ConcurrentQueue`1<BestHTTP.Core.RequestEventInfo> BestHTTP.Core.RequestEventHelper::requestEventQueue
	ConcurrentQueue_1_t596AA864B0C0E402BD7BDF817F8FCBD34298B8A5 * ___requestEventQueue_0;
	// System.Action`1<BestHTTP.Core.RequestEventInfo> BestHTTP.Core.RequestEventHelper::OnEvent
	Action_1_t64DF736E94D00D5F7933482B079D3BDC98860698 * ___OnEvent_1;

public:
	inline static int32_t get_offset_of_requestEventQueue_0() { return static_cast<int32_t>(offsetof(RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509_StaticFields, ___requestEventQueue_0)); }
	inline ConcurrentQueue_1_t596AA864B0C0E402BD7BDF817F8FCBD34298B8A5 * get_requestEventQueue_0() const { return ___requestEventQueue_0; }
	inline ConcurrentQueue_1_t596AA864B0C0E402BD7BDF817F8FCBD34298B8A5 ** get_address_of_requestEventQueue_0() { return &___requestEventQueue_0; }
	inline void set_requestEventQueue_0(ConcurrentQueue_1_t596AA864B0C0E402BD7BDF817F8FCBD34298B8A5 * value)
	{
		___requestEventQueue_0 = value;
		Il2CppCodeGenWriteBarrier((&___requestEventQueue_0), value);
	}

	inline static int32_t get_offset_of_OnEvent_1() { return static_cast<int32_t>(offsetof(RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509_StaticFields, ___OnEvent_1)); }
	inline Action_1_t64DF736E94D00D5F7933482B079D3BDC98860698 * get_OnEvent_1() const { return ___OnEvent_1; }
	inline Action_1_t64DF736E94D00D5F7933482B079D3BDC98860698 ** get_address_of_OnEvent_1() { return &___OnEvent_1; }
	inline void set_OnEvent_1(Action_1_t64DF736E94D00D5F7933482B079D3BDC98860698 * value)
	{
		___OnEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnEvent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTEVENTHELPER_TD5E4B7D8F15524DFAD455423114A03BDD32FB509_H
#ifndef CRC32_T0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC_H
#define CRC32_T0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Crc.CRC32
struct  CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC  : public RuntimeObject
{
public:
	// System.UInt32 BestHTTP.Decompression.Crc.CRC32::dwPolynomial
	uint32_t ___dwPolynomial_0;
	// System.Int64 BestHTTP.Decompression.Crc.CRC32::_TotalBytesRead
	int64_t ____TotalBytesRead_1;
	// System.Boolean BestHTTP.Decompression.Crc.CRC32::reverseBits
	bool ___reverseBits_2;
	// System.UInt32[] BestHTTP.Decompression.Crc.CRC32::crc32Table
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___crc32Table_3;
	// System.UInt32 BestHTTP.Decompression.Crc.CRC32::_register
	uint32_t ____register_5;

public:
	inline static int32_t get_offset_of_dwPolynomial_0() { return static_cast<int32_t>(offsetof(CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC, ___dwPolynomial_0)); }
	inline uint32_t get_dwPolynomial_0() const { return ___dwPolynomial_0; }
	inline uint32_t* get_address_of_dwPolynomial_0() { return &___dwPolynomial_0; }
	inline void set_dwPolynomial_0(uint32_t value)
	{
		___dwPolynomial_0 = value;
	}

	inline static int32_t get_offset_of__TotalBytesRead_1() { return static_cast<int32_t>(offsetof(CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC, ____TotalBytesRead_1)); }
	inline int64_t get__TotalBytesRead_1() const { return ____TotalBytesRead_1; }
	inline int64_t* get_address_of__TotalBytesRead_1() { return &____TotalBytesRead_1; }
	inline void set__TotalBytesRead_1(int64_t value)
	{
		____TotalBytesRead_1 = value;
	}

	inline static int32_t get_offset_of_reverseBits_2() { return static_cast<int32_t>(offsetof(CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC, ___reverseBits_2)); }
	inline bool get_reverseBits_2() const { return ___reverseBits_2; }
	inline bool* get_address_of_reverseBits_2() { return &___reverseBits_2; }
	inline void set_reverseBits_2(bool value)
	{
		___reverseBits_2 = value;
	}

	inline static int32_t get_offset_of_crc32Table_3() { return static_cast<int32_t>(offsetof(CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC, ___crc32Table_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_crc32Table_3() const { return ___crc32Table_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_crc32Table_3() { return &___crc32Table_3; }
	inline void set_crc32Table_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___crc32Table_3 = value;
		Il2CppCodeGenWriteBarrier((&___crc32Table_3), value);
	}

	inline static int32_t get_offset_of__register_5() { return static_cast<int32_t>(offsetof(CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC, ____register_5)); }
	inline uint32_t get__register_5() const { return ____register_5; }
	inline uint32_t* get_address_of__register_5() { return &____register_5; }
	inline void set__register_5(uint32_t value)
	{
		____register_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC_H
#ifndef GZIPDECOMPRESSOR_TEE54B282EA5B93F6DA8819F0E2C188016C5B865C_H
#define GZIPDECOMPRESSOR_TEE54B282EA5B93F6DA8819F0E2C188016C5B865C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.GZipDecompressor
struct  GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C  : public RuntimeObject
{
public:
	// BestHTTP.Extensions.BufferPoolMemoryStream BestHTTP.Decompression.GZipDecompressor::decompressorInputStream
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * ___decompressorInputStream_0;
	// BestHTTP.Extensions.BufferPoolMemoryStream BestHTTP.Decompression.GZipDecompressor::decompressorOutputStream
	BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * ___decompressorOutputStream_1;
	// BestHTTP.Decompression.Zlib.GZipStream BestHTTP.Decompression.GZipDecompressor::decompressorGZipStream
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7 * ___decompressorGZipStream_2;
	// System.Int32 BestHTTP.Decompression.GZipDecompressor::MinLengthToDecompress
	int32_t ___MinLengthToDecompress_3;

public:
	inline static int32_t get_offset_of_decompressorInputStream_0() { return static_cast<int32_t>(offsetof(GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C, ___decompressorInputStream_0)); }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * get_decompressorInputStream_0() const { return ___decompressorInputStream_0; }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC ** get_address_of_decompressorInputStream_0() { return &___decompressorInputStream_0; }
	inline void set_decompressorInputStream_0(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * value)
	{
		___decompressorInputStream_0 = value;
		Il2CppCodeGenWriteBarrier((&___decompressorInputStream_0), value);
	}

	inline static int32_t get_offset_of_decompressorOutputStream_1() { return static_cast<int32_t>(offsetof(GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C, ___decompressorOutputStream_1)); }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * get_decompressorOutputStream_1() const { return ___decompressorOutputStream_1; }
	inline BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC ** get_address_of_decompressorOutputStream_1() { return &___decompressorOutputStream_1; }
	inline void set_decompressorOutputStream_1(BufferPoolMemoryStream_t65F3670F655A8995A8BEF13533F4237D2A6CD5AC * value)
	{
		___decompressorOutputStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___decompressorOutputStream_1), value);
	}

	inline static int32_t get_offset_of_decompressorGZipStream_2() { return static_cast<int32_t>(offsetof(GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C, ___decompressorGZipStream_2)); }
	inline GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7 * get_decompressorGZipStream_2() const { return ___decompressorGZipStream_2; }
	inline GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7 ** get_address_of_decompressorGZipStream_2() { return &___decompressorGZipStream_2; }
	inline void set_decompressorGZipStream_2(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7 * value)
	{
		___decompressorGZipStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___decompressorGZipStream_2), value);
	}

	inline static int32_t get_offset_of_MinLengthToDecompress_3() { return static_cast<int32_t>(offsetof(GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C, ___MinLengthToDecompress_3)); }
	inline int32_t get_MinLengthToDecompress_3() const { return ___MinLengthToDecompress_3; }
	inline int32_t* get_address_of_MinLengthToDecompress_3() { return &___MinLengthToDecompress_3; }
	inline void set_MinLengthToDecompress_3(int32_t value)
	{
		___MinLengthToDecompress_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPDECOMPRESSOR_TEE54B282EA5B93F6DA8819F0E2C188016C5B865C_H
#ifndef ADLER_T11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_H
#define ADLER_T11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.Adler
struct  Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023  : public RuntimeObject
{
public:

public:
};

struct Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_StaticFields
{
public:
	// System.UInt32 BestHTTP.Decompression.Zlib.Adler::BASE
	uint32_t ___BASE_0;
	// System.Int32 BestHTTP.Decompression.Zlib.Adler::NMAX
	int32_t ___NMAX_1;

public:
	inline static int32_t get_offset_of_BASE_0() { return static_cast<int32_t>(offsetof(Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_StaticFields, ___BASE_0)); }
	inline uint32_t get_BASE_0() const { return ___BASE_0; }
	inline uint32_t* get_address_of_BASE_0() { return &___BASE_0; }
	inline void set_BASE_0(uint32_t value)
	{
		___BASE_0 = value;
	}

	inline static int32_t get_offset_of_NMAX_1() { return static_cast<int32_t>(offsetof(Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_StaticFields, ___NMAX_1)); }
	inline int32_t get_NMAX_1() const { return ___NMAX_1; }
	inline int32_t* get_address_of_NMAX_1() { return &___NMAX_1; }
	inline void set_NMAX_1(int32_t value)
	{
		___NMAX_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLER_T11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_H
#ifndef INFTREE_TC36932870D092D06D6C8AED352BA90AA9CFF84E6_H
#define INFTREE_TC36932870D092D06D6C8AED352BA90AA9CFF84E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InfTree
struct  InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6  : public RuntimeObject
{
public:
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::hn
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hn_19;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::v
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___v_20;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::c
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___c_21;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::r
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___r_22;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::u
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___u_23;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::x
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___x_24;

public:
	inline static int32_t get_offset_of_hn_19() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6, ___hn_19)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hn_19() const { return ___hn_19; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hn_19() { return &___hn_19; }
	inline void set_hn_19(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hn_19 = value;
		Il2CppCodeGenWriteBarrier((&___hn_19), value);
	}

	inline static int32_t get_offset_of_v_20() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6, ___v_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_v_20() const { return ___v_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_v_20() { return &___v_20; }
	inline void set_v_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___v_20 = value;
		Il2CppCodeGenWriteBarrier((&___v_20), value);
	}

	inline static int32_t get_offset_of_c_21() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6, ___c_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_c_21() const { return ___c_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_c_21() { return &___c_21; }
	inline void set_c_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___c_21 = value;
		Il2CppCodeGenWriteBarrier((&___c_21), value);
	}

	inline static int32_t get_offset_of_r_22() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6, ___r_22)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_r_22() const { return ___r_22; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_r_22() { return &___r_22; }
	inline void set_r_22(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___r_22 = value;
		Il2CppCodeGenWriteBarrier((&___r_22), value);
	}

	inline static int32_t get_offset_of_u_23() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6, ___u_23)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_u_23() const { return ___u_23; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_u_23() { return &___u_23; }
	inline void set_u_23(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___u_23 = value;
		Il2CppCodeGenWriteBarrier((&___u_23), value);
	}

	inline static int32_t get_offset_of_x_24() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6, ___x_24)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_x_24() const { return ___x_24; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_x_24() { return &___x_24; }
	inline void set_x_24(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___x_24 = value;
		Il2CppCodeGenWriteBarrier((&___x_24), value);
	}
};

struct InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields
{
public:
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::fixed_tl
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fixed_tl_12;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::fixed_td
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fixed_td_13;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::cplens
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cplens_14;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::cplext
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cplext_15;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::cpdist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cpdist_16;
	// System.Int32[] BestHTTP.Decompression.Zlib.InfTree::cpdext
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cpdext_17;

public:
	inline static int32_t get_offset_of_fixed_tl_12() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields, ___fixed_tl_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fixed_tl_12() const { return ___fixed_tl_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fixed_tl_12() { return &___fixed_tl_12; }
	inline void set_fixed_tl_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fixed_tl_12 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_tl_12), value);
	}

	inline static int32_t get_offset_of_fixed_td_13() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields, ___fixed_td_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fixed_td_13() const { return ___fixed_td_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fixed_td_13() { return &___fixed_td_13; }
	inline void set_fixed_td_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fixed_td_13 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_td_13), value);
	}

	inline static int32_t get_offset_of_cplens_14() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields, ___cplens_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cplens_14() const { return ___cplens_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cplens_14() { return &___cplens_14; }
	inline void set_cplens_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cplens_14 = value;
		Il2CppCodeGenWriteBarrier((&___cplens_14), value);
	}

	inline static int32_t get_offset_of_cplext_15() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields, ___cplext_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cplext_15() const { return ___cplext_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cplext_15() { return &___cplext_15; }
	inline void set_cplext_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cplext_15 = value;
		Il2CppCodeGenWriteBarrier((&___cplext_15), value);
	}

	inline static int32_t get_offset_of_cpdist_16() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields, ___cpdist_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cpdist_16() const { return ___cpdist_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cpdist_16() { return &___cpdist_16; }
	inline void set_cpdist_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cpdist_16 = value;
		Il2CppCodeGenWriteBarrier((&___cpdist_16), value);
	}

	inline static int32_t get_offset_of_cpdext_17() { return static_cast<int32_t>(offsetof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields, ___cpdext_17)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cpdext_17() const { return ___cpdext_17; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cpdext_17() { return &___cpdext_17; }
	inline void set_cpdext_17(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cpdext_17 = value;
		Il2CppCodeGenWriteBarrier((&___cpdext_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFTREE_TC36932870D092D06D6C8AED352BA90AA9CFF84E6_H
#ifndef INFLATECODES_T29F253554B2F53C42CA4D43C5F77CA23B5D50FAC_H
#define INFLATECODES_T29F253554B2F53C42CA4D43C5F77CA23B5D50FAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InflateCodes
struct  InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::mode
	int32_t ___mode_10;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::len
	int32_t ___len_11;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateCodes::tree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tree_12;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::tree_index
	int32_t ___tree_index_13;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::need
	int32_t ___need_14;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::lit
	int32_t ___lit_15;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::bitsToGet
	int32_t ___bitsToGet_16;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::dist
	int32_t ___dist_17;
	// System.Byte BestHTTP.Decompression.Zlib.InflateCodes::lbits
	uint8_t ___lbits_18;
	// System.Byte BestHTTP.Decompression.Zlib.InflateCodes::dbits
	uint8_t ___dbits_19;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateCodes::ltree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ltree_20;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::ltree_index
	int32_t ___ltree_index_21;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateCodes::dtree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___dtree_22;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateCodes::dtree_index
	int32_t ___dtree_index_23;

public:
	inline static int32_t get_offset_of_mode_10() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___mode_10)); }
	inline int32_t get_mode_10() const { return ___mode_10; }
	inline int32_t* get_address_of_mode_10() { return &___mode_10; }
	inline void set_mode_10(int32_t value)
	{
		___mode_10 = value;
	}

	inline static int32_t get_offset_of_len_11() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___len_11)); }
	inline int32_t get_len_11() const { return ___len_11; }
	inline int32_t* get_address_of_len_11() { return &___len_11; }
	inline void set_len_11(int32_t value)
	{
		___len_11 = value;
	}

	inline static int32_t get_offset_of_tree_12() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___tree_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tree_12() const { return ___tree_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tree_12() { return &___tree_12; }
	inline void set_tree_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tree_12 = value;
		Il2CppCodeGenWriteBarrier((&___tree_12), value);
	}

	inline static int32_t get_offset_of_tree_index_13() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___tree_index_13)); }
	inline int32_t get_tree_index_13() const { return ___tree_index_13; }
	inline int32_t* get_address_of_tree_index_13() { return &___tree_index_13; }
	inline void set_tree_index_13(int32_t value)
	{
		___tree_index_13 = value;
	}

	inline static int32_t get_offset_of_need_14() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___need_14)); }
	inline int32_t get_need_14() const { return ___need_14; }
	inline int32_t* get_address_of_need_14() { return &___need_14; }
	inline void set_need_14(int32_t value)
	{
		___need_14 = value;
	}

	inline static int32_t get_offset_of_lit_15() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___lit_15)); }
	inline int32_t get_lit_15() const { return ___lit_15; }
	inline int32_t* get_address_of_lit_15() { return &___lit_15; }
	inline void set_lit_15(int32_t value)
	{
		___lit_15 = value;
	}

	inline static int32_t get_offset_of_bitsToGet_16() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___bitsToGet_16)); }
	inline int32_t get_bitsToGet_16() const { return ___bitsToGet_16; }
	inline int32_t* get_address_of_bitsToGet_16() { return &___bitsToGet_16; }
	inline void set_bitsToGet_16(int32_t value)
	{
		___bitsToGet_16 = value;
	}

	inline static int32_t get_offset_of_dist_17() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___dist_17)); }
	inline int32_t get_dist_17() const { return ___dist_17; }
	inline int32_t* get_address_of_dist_17() { return &___dist_17; }
	inline void set_dist_17(int32_t value)
	{
		___dist_17 = value;
	}

	inline static int32_t get_offset_of_lbits_18() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___lbits_18)); }
	inline uint8_t get_lbits_18() const { return ___lbits_18; }
	inline uint8_t* get_address_of_lbits_18() { return &___lbits_18; }
	inline void set_lbits_18(uint8_t value)
	{
		___lbits_18 = value;
	}

	inline static int32_t get_offset_of_dbits_19() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___dbits_19)); }
	inline uint8_t get_dbits_19() const { return ___dbits_19; }
	inline uint8_t* get_address_of_dbits_19() { return &___dbits_19; }
	inline void set_dbits_19(uint8_t value)
	{
		___dbits_19 = value;
	}

	inline static int32_t get_offset_of_ltree_20() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___ltree_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ltree_20() const { return ___ltree_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ltree_20() { return &___ltree_20; }
	inline void set_ltree_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ltree_20 = value;
		Il2CppCodeGenWriteBarrier((&___ltree_20), value);
	}

	inline static int32_t get_offset_of_ltree_index_21() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___ltree_index_21)); }
	inline int32_t get_ltree_index_21() const { return ___ltree_index_21; }
	inline int32_t* get_address_of_ltree_index_21() { return &___ltree_index_21; }
	inline void set_ltree_index_21(int32_t value)
	{
		___ltree_index_21 = value;
	}

	inline static int32_t get_offset_of_dtree_22() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___dtree_22)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_dtree_22() const { return ___dtree_22; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_dtree_22() { return &___dtree_22; }
	inline void set_dtree_22(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___dtree_22 = value;
		Il2CppCodeGenWriteBarrier((&___dtree_22), value);
	}

	inline static int32_t get_offset_of_dtree_index_23() { return static_cast<int32_t>(offsetof(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC, ___dtree_index_23)); }
	inline int32_t get_dtree_index_23() const { return ___dtree_index_23; }
	inline int32_t* get_address_of_dtree_index_23() { return &___dtree_index_23; }
	inline void set_dtree_index_23(int32_t value)
	{
		___dtree_index_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATECODES_T29F253554B2F53C42CA4D43C5F77CA23B5D50FAC_H
#ifndef INTERNALCONSTANTS_T1A8E71379CABACD2E899C44214612731EA5A2916_H
#define INTERNALCONSTANTS_T1A8E71379CABACD2E899C44214612731EA5A2916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InternalConstants
struct  InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916  : public RuntimeObject
{
public:

public:
};

struct InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::MAX_BITS
	int32_t ___MAX_BITS_0;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::BL_CODES
	int32_t ___BL_CODES_1;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::D_CODES
	int32_t ___D_CODES_2;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::LITERALS
	int32_t ___LITERALS_3;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::LENGTH_CODES
	int32_t ___LENGTH_CODES_4;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::L_CODES
	int32_t ___L_CODES_5;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::MAX_BL_BITS
	int32_t ___MAX_BL_BITS_6;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::REP_3_6
	int32_t ___REP_3_6_7;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::REPZ_3_10
	int32_t ___REPZ_3_10_8;
	// System.Int32 BestHTTP.Decompression.Zlib.InternalConstants::REPZ_11_138
	int32_t ___REPZ_11_138_9;

public:
	inline static int32_t get_offset_of_MAX_BITS_0() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___MAX_BITS_0)); }
	inline int32_t get_MAX_BITS_0() const { return ___MAX_BITS_0; }
	inline int32_t* get_address_of_MAX_BITS_0() { return &___MAX_BITS_0; }
	inline void set_MAX_BITS_0(int32_t value)
	{
		___MAX_BITS_0 = value;
	}

	inline static int32_t get_offset_of_BL_CODES_1() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___BL_CODES_1)); }
	inline int32_t get_BL_CODES_1() const { return ___BL_CODES_1; }
	inline int32_t* get_address_of_BL_CODES_1() { return &___BL_CODES_1; }
	inline void set_BL_CODES_1(int32_t value)
	{
		___BL_CODES_1 = value;
	}

	inline static int32_t get_offset_of_D_CODES_2() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___D_CODES_2)); }
	inline int32_t get_D_CODES_2() const { return ___D_CODES_2; }
	inline int32_t* get_address_of_D_CODES_2() { return &___D_CODES_2; }
	inline void set_D_CODES_2(int32_t value)
	{
		___D_CODES_2 = value;
	}

	inline static int32_t get_offset_of_LITERALS_3() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___LITERALS_3)); }
	inline int32_t get_LITERALS_3() const { return ___LITERALS_3; }
	inline int32_t* get_address_of_LITERALS_3() { return &___LITERALS_3; }
	inline void set_LITERALS_3(int32_t value)
	{
		___LITERALS_3 = value;
	}

	inline static int32_t get_offset_of_LENGTH_CODES_4() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___LENGTH_CODES_4)); }
	inline int32_t get_LENGTH_CODES_4() const { return ___LENGTH_CODES_4; }
	inline int32_t* get_address_of_LENGTH_CODES_4() { return &___LENGTH_CODES_4; }
	inline void set_LENGTH_CODES_4(int32_t value)
	{
		___LENGTH_CODES_4 = value;
	}

	inline static int32_t get_offset_of_L_CODES_5() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___L_CODES_5)); }
	inline int32_t get_L_CODES_5() const { return ___L_CODES_5; }
	inline int32_t* get_address_of_L_CODES_5() { return &___L_CODES_5; }
	inline void set_L_CODES_5(int32_t value)
	{
		___L_CODES_5 = value;
	}

	inline static int32_t get_offset_of_MAX_BL_BITS_6() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___MAX_BL_BITS_6)); }
	inline int32_t get_MAX_BL_BITS_6() const { return ___MAX_BL_BITS_6; }
	inline int32_t* get_address_of_MAX_BL_BITS_6() { return &___MAX_BL_BITS_6; }
	inline void set_MAX_BL_BITS_6(int32_t value)
	{
		___MAX_BL_BITS_6 = value;
	}

	inline static int32_t get_offset_of_REP_3_6_7() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___REP_3_6_7)); }
	inline int32_t get_REP_3_6_7() const { return ___REP_3_6_7; }
	inline int32_t* get_address_of_REP_3_6_7() { return &___REP_3_6_7; }
	inline void set_REP_3_6_7(int32_t value)
	{
		___REP_3_6_7 = value;
	}

	inline static int32_t get_offset_of_REPZ_3_10_8() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___REPZ_3_10_8)); }
	inline int32_t get_REPZ_3_10_8() const { return ___REPZ_3_10_8; }
	inline int32_t* get_address_of_REPZ_3_10_8() { return &___REPZ_3_10_8; }
	inline void set_REPZ_3_10_8(int32_t value)
	{
		___REPZ_3_10_8 = value;
	}

	inline static int32_t get_offset_of_REPZ_11_138_9() { return static_cast<int32_t>(offsetof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields, ___REPZ_11_138_9)); }
	inline int32_t get_REPZ_11_138_9() const { return ___REPZ_11_138_9; }
	inline int32_t* get_address_of_REPZ_11_138_9() { return &___REPZ_11_138_9; }
	inline void set_REPZ_11_138_9(int32_t value)
	{
		___REPZ_11_138_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONSTANTS_T1A8E71379CABACD2E899C44214612731EA5A2916_H
#ifndef INTERNALINFLATECONSTANTS_T2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_H
#define INTERNALINFLATECONSTANTS_T2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InternalInflateConstants
struct  InternalInflateConstants_t2EBF3160F7661B7094A73B38FEE4C5C11B644A3B  : public RuntimeObject
{
public:

public:
};

struct InternalInflateConstants_t2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_StaticFields
{
public:
	// System.Int32[] BestHTTP.Decompression.Zlib.InternalInflateConstants::InflateMask
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___InflateMask_0;

public:
	inline static int32_t get_offset_of_InflateMask_0() { return static_cast<int32_t>(offsetof(InternalInflateConstants_t2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_StaticFields, ___InflateMask_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_InflateMask_0() const { return ___InflateMask_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_InflateMask_0() { return &___InflateMask_0; }
	inline void set_InflateMask_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___InflateMask_0 = value;
		Il2CppCodeGenWriteBarrier((&___InflateMask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALINFLATECONSTANTS_T2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_H
#ifndef SHAREDUTILS_T2627060D8748C0DF33B4EA236BB7DF99B5BE4DD7_H
#define SHAREDUTILS_T2627060D8748C0DF33B4EA236BB7DF99B5BE4DD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.SharedUtils
struct  SharedUtils_t2627060D8748C0DF33B4EA236BB7DF99B5BE4DD7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDUTILS_T2627060D8748C0DF33B4EA236BB7DF99B5BE4DD7_H
#ifndef STATICTREE_T3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_H
#define STATICTREE_T3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.StaticTree
struct  StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0  : public RuntimeObject
{
public:
	// System.Int16[] BestHTTP.Decompression.Zlib.StaticTree::treeCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___treeCodes_5;
	// System.Int32[] BestHTTP.Decompression.Zlib.StaticTree::extraBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extraBits_6;
	// System.Int32 BestHTTP.Decompression.Zlib.StaticTree::extraBase
	int32_t ___extraBase_7;
	// System.Int32 BestHTTP.Decompression.Zlib.StaticTree::elems
	int32_t ___elems_8;
	// System.Int32 BestHTTP.Decompression.Zlib.StaticTree::maxLength
	int32_t ___maxLength_9;

public:
	inline static int32_t get_offset_of_treeCodes_5() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0, ___treeCodes_5)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_treeCodes_5() const { return ___treeCodes_5; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_treeCodes_5() { return &___treeCodes_5; }
	inline void set_treeCodes_5(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___treeCodes_5 = value;
		Il2CppCodeGenWriteBarrier((&___treeCodes_5), value);
	}

	inline static int32_t get_offset_of_extraBits_6() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0, ___extraBits_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extraBits_6() const { return ___extraBits_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extraBits_6() { return &___extraBits_6; }
	inline void set_extraBits_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extraBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___extraBits_6), value);
	}

	inline static int32_t get_offset_of_extraBase_7() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0, ___extraBase_7)); }
	inline int32_t get_extraBase_7() const { return ___extraBase_7; }
	inline int32_t* get_address_of_extraBase_7() { return &___extraBase_7; }
	inline void set_extraBase_7(int32_t value)
	{
		___extraBase_7 = value;
	}

	inline static int32_t get_offset_of_elems_8() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0, ___elems_8)); }
	inline int32_t get_elems_8() const { return ___elems_8; }
	inline int32_t* get_address_of_elems_8() { return &___elems_8; }
	inline void set_elems_8(int32_t value)
	{
		___elems_8 = value;
	}

	inline static int32_t get_offset_of_maxLength_9() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0, ___maxLength_9)); }
	inline int32_t get_maxLength_9() const { return ___maxLength_9; }
	inline int32_t* get_address_of_maxLength_9() { return &___maxLength_9; }
	inline void set_maxLength_9(int32_t value)
	{
		___maxLength_9 = value;
	}
};

struct StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields
{
public:
	// System.Int16[] BestHTTP.Decompression.Zlib.StaticTree::lengthAndLiteralsTreeCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___lengthAndLiteralsTreeCodes_0;
	// System.Int16[] BestHTTP.Decompression.Zlib.StaticTree::distTreeCodes
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___distTreeCodes_1;
	// BestHTTP.Decompression.Zlib.StaticTree BestHTTP.Decompression.Zlib.StaticTree::Literals
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * ___Literals_2;
	// BestHTTP.Decompression.Zlib.StaticTree BestHTTP.Decompression.Zlib.StaticTree::Distances
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * ___Distances_3;
	// BestHTTP.Decompression.Zlib.StaticTree BestHTTP.Decompression.Zlib.StaticTree::BitLengths
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * ___BitLengths_4;

public:
	inline static int32_t get_offset_of_lengthAndLiteralsTreeCodes_0() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields, ___lengthAndLiteralsTreeCodes_0)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_lengthAndLiteralsTreeCodes_0() const { return ___lengthAndLiteralsTreeCodes_0; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_lengthAndLiteralsTreeCodes_0() { return &___lengthAndLiteralsTreeCodes_0; }
	inline void set_lengthAndLiteralsTreeCodes_0(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___lengthAndLiteralsTreeCodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___lengthAndLiteralsTreeCodes_0), value);
	}

	inline static int32_t get_offset_of_distTreeCodes_1() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields, ___distTreeCodes_1)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_distTreeCodes_1() const { return ___distTreeCodes_1; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_distTreeCodes_1() { return &___distTreeCodes_1; }
	inline void set_distTreeCodes_1(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___distTreeCodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___distTreeCodes_1), value);
	}

	inline static int32_t get_offset_of_Literals_2() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields, ___Literals_2)); }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * get_Literals_2() const { return ___Literals_2; }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 ** get_address_of_Literals_2() { return &___Literals_2; }
	inline void set_Literals_2(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * value)
	{
		___Literals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Literals_2), value);
	}

	inline static int32_t get_offset_of_Distances_3() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields, ___Distances_3)); }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * get_Distances_3() const { return ___Distances_3; }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 ** get_address_of_Distances_3() { return &___Distances_3; }
	inline void set_Distances_3(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * value)
	{
		___Distances_3 = value;
		Il2CppCodeGenWriteBarrier((&___Distances_3), value);
	}

	inline static int32_t get_offset_of_BitLengths_4() { return static_cast<int32_t>(offsetof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields, ___BitLengths_4)); }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * get_BitLengths_4() const { return ___BitLengths_4; }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 ** get_address_of_BitLengths_4() { return &___BitLengths_4; }
	inline void set_BitLengths_4(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * value)
	{
		___BitLengths_4 = value;
		Il2CppCodeGenWriteBarrier((&___BitLengths_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICTREE_T3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_H
#ifndef ZTREE_T67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_H
#define ZTREE_T67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZTree
struct  ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13  : public RuntimeObject
{
public:
	// System.Int16[] BestHTTP.Decompression.Zlib.ZTree::dyn_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_tree_10;
	// System.Int32 BestHTTP.Decompression.Zlib.ZTree::max_code
	int32_t ___max_code_11;
	// BestHTTP.Decompression.Zlib.StaticTree BestHTTP.Decompression.Zlib.ZTree::staticTree
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * ___staticTree_12;

public:
	inline static int32_t get_offset_of_dyn_tree_10() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13, ___dyn_tree_10)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_tree_10() const { return ___dyn_tree_10; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_tree_10() { return &___dyn_tree_10; }
	inline void set_dyn_tree_10(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_tree_10 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_tree_10), value);
	}

	inline static int32_t get_offset_of_max_code_11() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13, ___max_code_11)); }
	inline int32_t get_max_code_11() const { return ___max_code_11; }
	inline int32_t* get_address_of_max_code_11() { return &___max_code_11; }
	inline void set_max_code_11(int32_t value)
	{
		___max_code_11 = value;
	}

	inline static int32_t get_offset_of_staticTree_12() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13, ___staticTree_12)); }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * get_staticTree_12() const { return ___staticTree_12; }
	inline StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 ** get_address_of_staticTree_12() { return &___staticTree_12; }
	inline void set_staticTree_12(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0 * value)
	{
		___staticTree_12 = value;
		Il2CppCodeGenWriteBarrier((&___staticTree_12), value);
	}
};

struct ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.ZTree::HEAP_SIZE
	int32_t ___HEAP_SIZE_0;
	// System.Int32[] BestHTTP.Decompression.Zlib.ZTree::ExtraLengthBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ExtraLengthBits_1;
	// System.Int32[] BestHTTP.Decompression.Zlib.ZTree::ExtraDistanceBits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ExtraDistanceBits_2;
	// System.Int32[] BestHTTP.Decompression.Zlib.ZTree::extra_blbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_blbits_3;
	// System.SByte[] BestHTTP.Decompression.Zlib.ZTree::bl_order
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___bl_order_4;
	// System.SByte[] BestHTTP.Decompression.Zlib.ZTree::_dist_code
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ____dist_code_6;
	// System.SByte[] BestHTTP.Decompression.Zlib.ZTree::LengthCode
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___LengthCode_7;
	// System.Int32[] BestHTTP.Decompression.Zlib.ZTree::LengthBase
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___LengthBase_8;
	// System.Int32[] BestHTTP.Decompression.Zlib.ZTree::DistanceBase
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DistanceBase_9;

public:
	inline static int32_t get_offset_of_HEAP_SIZE_0() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___HEAP_SIZE_0)); }
	inline int32_t get_HEAP_SIZE_0() const { return ___HEAP_SIZE_0; }
	inline int32_t* get_address_of_HEAP_SIZE_0() { return &___HEAP_SIZE_0; }
	inline void set_HEAP_SIZE_0(int32_t value)
	{
		___HEAP_SIZE_0 = value;
	}

	inline static int32_t get_offset_of_ExtraLengthBits_1() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___ExtraLengthBits_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ExtraLengthBits_1() const { return ___ExtraLengthBits_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ExtraLengthBits_1() { return &___ExtraLengthBits_1; }
	inline void set_ExtraLengthBits_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ExtraLengthBits_1 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraLengthBits_1), value);
	}

	inline static int32_t get_offset_of_ExtraDistanceBits_2() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___ExtraDistanceBits_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ExtraDistanceBits_2() const { return ___ExtraDistanceBits_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ExtraDistanceBits_2() { return &___ExtraDistanceBits_2; }
	inline void set_ExtraDistanceBits_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ExtraDistanceBits_2 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraDistanceBits_2), value);
	}

	inline static int32_t get_offset_of_extra_blbits_3() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___extra_blbits_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_blbits_3() const { return ___extra_blbits_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_blbits_3() { return &___extra_blbits_3; }
	inline void set_extra_blbits_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_blbits_3 = value;
		Il2CppCodeGenWriteBarrier((&___extra_blbits_3), value);
	}

	inline static int32_t get_offset_of_bl_order_4() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___bl_order_4)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_bl_order_4() const { return ___bl_order_4; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_bl_order_4() { return &___bl_order_4; }
	inline void set_bl_order_4(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___bl_order_4 = value;
		Il2CppCodeGenWriteBarrier((&___bl_order_4), value);
	}

	inline static int32_t get_offset_of__dist_code_6() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ____dist_code_6)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get__dist_code_6() const { return ____dist_code_6; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of__dist_code_6() { return &____dist_code_6; }
	inline void set__dist_code_6(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		____dist_code_6 = value;
		Il2CppCodeGenWriteBarrier((&____dist_code_6), value);
	}

	inline static int32_t get_offset_of_LengthCode_7() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___LengthCode_7)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_LengthCode_7() const { return ___LengthCode_7; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_LengthCode_7() { return &___LengthCode_7; }
	inline void set_LengthCode_7(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___LengthCode_7 = value;
		Il2CppCodeGenWriteBarrier((&___LengthCode_7), value);
	}

	inline static int32_t get_offset_of_LengthBase_8() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___LengthBase_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_LengthBase_8() const { return ___LengthBase_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_LengthBase_8() { return &___LengthBase_8; }
	inline void set_LengthBase_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___LengthBase_8 = value;
		Il2CppCodeGenWriteBarrier((&___LengthBase_8), value);
	}

	inline static int32_t get_offset_of_DistanceBase_9() { return static_cast<int32_t>(offsetof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields, ___DistanceBase_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DistanceBase_9() const { return ___DistanceBase_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DistanceBase_9() { return &___DistanceBase_9; }
	inline void set_DistanceBase_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DistanceBase_9 = value;
		Il2CppCodeGenWriteBarrier((&___DistanceBase_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZTREE_T67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_H
#ifndef ZLIBCONSTANTS_TC69AFB69F94D17A6DA7D761E8802FE7637D51077_H
#define ZLIBCONSTANTS_TC69AFB69F94D17A6DA7D761E8802FE7637D51077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibConstants
struct  ZlibConstants_tC69AFB69F94D17A6DA7D761E8802FE7637D51077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBCONSTANTS_TC69AFB69F94D17A6DA7D761E8802FE7637D51077_H
#ifndef EXCEPTIONHELPER_T9EC8868CD3B945BEB8DFF883051C49A5A5C4C14A_H
#define EXCEPTIONHELPER_T9EC8868CD3B945BEB8DFF883051C49A5A5C4C14A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.ExceptionHelper
struct  ExceptionHelper_t9EC8868CD3B945BEB8DFF883051C49A5A5C4C14A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHELPER_T9EC8868CD3B945BEB8DFF883051C49A5A5C4C14A_H
#ifndef EXTENSIONS_T3BA94BC6AA8F519236CE76ABB51D675D9D819F47_H
#define EXTENSIONS_T3BA94BC6AA8F519236CE76ABB51D675D9D819F47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.Extensions
struct  Extensions_t3BA94BC6AA8F519236CE76ABB51D675D9D819F47  : public RuntimeObject
{
public:

public:
};

struct Extensions_t3BA94BC6AA8F519236CE76ABB51D675D9D819F47_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex BestHTTP.Extensions.Extensions::validIpV4AddressRegex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___validIpV4AddressRegex_0;

public:
	inline static int32_t get_offset_of_validIpV4AddressRegex_0() { return static_cast<int32_t>(offsetof(Extensions_t3BA94BC6AA8F519236CE76ABB51D675D9D819F47_StaticFields, ___validIpV4AddressRegex_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_validIpV4AddressRegex_0() const { return ___validIpV4AddressRegex_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_validIpV4AddressRegex_0() { return &___validIpV4AddressRegex_0; }
	inline void set_validIpV4AddressRegex_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___validIpV4AddressRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___validIpV4AddressRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T3BA94BC6AA8F519236CE76ABB51D675D9D819F47_H
#ifndef U3CU3EC_T63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_H
#define U3CU3EC_T63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.Extensions_<>c
struct  U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields
{
public:
	// BestHTTP.Extensions.Extensions_<>c BestHTTP.Extensions.Extensions_<>c::<>9
	U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8 * ___U3CU3E9_0;
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Extensions.Extensions_<>c::<>9__22_0
	Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * ___U3CU3E9__22_0_1;
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Extensions.Extensions_<>c::<>9__26_0
	Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * ___U3CU3E9__26_0_2;
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Extensions.Extensions_<>c::<>9__27_0
	Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * ___U3CU3E9__27_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields, ___U3CU3E9__22_0_1)); }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * get_U3CU3E9__22_0_1() const { return ___U3CU3E9__22_0_1; }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 ** get_address_of_U3CU3E9__22_0_1() { return &___U3CU3E9__22_0_1; }
	inline void set_U3CU3E9__22_0_1(Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * value)
	{
		___U3CU3E9__22_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields, ___U3CU3E9__26_0_2)); }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * get_U3CU3E9__26_0_2() const { return ___U3CU3E9__26_0_2; }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 ** get_address_of_U3CU3E9__26_0_2() { return &___U3CU3E9__26_0_2; }
	inline void set_U3CU3E9__26_0_2(Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * value)
	{
		___U3CU3E9__26_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields, ___U3CU3E9__27_0_3)); }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * get_U3CU3E9__27_0_3() const { return ___U3CU3E9__27_0_3; }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 ** get_address_of_U3CU3E9__27_0_3() { return &___U3CU3E9__27_0_3; }
	inline void set_U3CU3E9__27_0_3(Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * value)
	{
		___U3CU3E9__27_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_TF278E802C182085BBD73C29F20E8EFB11BD39D20_H
#define U3CU3EC__DISPLAYCLASS20_0_TF278E802C182085BBD73C29F20E8EFB11BD39D20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.Extensions_<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_tF278E802C182085BBD73C29F20E8EFB11BD39D20  : public RuntimeObject
{
public:
	// System.Char BestHTTP.Extensions.Extensions_<>c__DisplayClass20_0::block
	Il2CppChar ___block_0;

public:
	inline static int32_t get_offset_of_block_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tF278E802C182085BBD73C29F20E8EFB11BD39D20, ___block_0)); }
	inline Il2CppChar get_block_0() const { return ___block_0; }
	inline Il2CppChar* get_address_of_block_0() { return &___block_0; }
	inline void set_block_0(Il2CppChar value)
	{
		___block_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_TF278E802C182085BBD73C29F20E8EFB11BD39D20_H
#ifndef HEADERVALUE_T7483492AC97FE9524DDD56A1D48F23886C14EEF4_H
#define HEADERVALUE_T7483492AC97FE9524DDD56A1D48F23886C14EEF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.HeaderValue
struct  HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4  : public RuntimeObject
{
public:
	// System.String BestHTTP.Extensions.HeaderValue::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// System.String BestHTTP.Extensions.HeaderValue::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.HeaderValue::<Options>k__BackingField
	List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 * ___U3COptionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4, ___U3CKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4, ___U3CValueU3Ek__BackingField_1)); }
	inline String_t* get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(String_t* value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4, ___U3COptionsU3Ek__BackingField_2)); }
	inline List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 * get_U3COptionsU3Ek__BackingField_2() const { return ___U3COptionsU3Ek__BackingField_2; }
	inline List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 ** get_address_of_U3COptionsU3Ek__BackingField_2() { return &___U3COptionsU3Ek__BackingField_2; }
	inline void set_U3COptionsU3Ek__BackingField_2(List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 * value)
	{
		___U3COptionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERVALUE_T7483492AC97FE9524DDD56A1D48F23886C14EEF4_H
#ifndef U3CU3EC_TA16BA987839C415A845108B9F19DCD62E29421E4_H
#define U3CU3EC_TA16BA987839C415A845108B9F19DCD62E29421E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.HeaderValue_<>c
struct  U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4_StaticFields
{
public:
	// BestHTTP.Extensions.HeaderValue_<>c BestHTTP.Extensions.HeaderValue_<>c::<>9
	U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4 * ___U3CU3E9_0;
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Extensions.HeaderValue_<>c::<>9__18_0
	Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA16BA987839C415A845108B9F19DCD62E29421E4_H
#ifndef KEYVALUEPAIRLIST_TA44BB082F0E5204B4C0611544C79F7ADED926E4D_H
#define KEYVALUEPAIRLIST_TA44BB082F0E5204B4C0611544C79F7ADED926E4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.KeyValuePairList
struct  KeyValuePairList_tA44BB082F0E5204B4C0611544C79F7ADED926E4D  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.KeyValuePairList::<Values>k__BackingField
	List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 * ___U3CValuesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValuesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KeyValuePairList_tA44BB082F0E5204B4C0611544C79F7ADED926E4D, ___U3CValuesU3Ek__BackingField_0)); }
	inline List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 * get_U3CValuesU3Ek__BackingField_0() const { return ___U3CValuesU3Ek__BackingField_0; }
	inline List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 ** get_address_of_U3CValuesU3Ek__BackingField_0() { return &___U3CValuesU3Ek__BackingField_0; }
	inline void set_U3CValuesU3Ek__BackingField_0(List_1_tF4A5FAF6B2636CB2578D9D4095F42A7D1F5EB6F0 * value)
	{
		___U3CValuesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValuesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRLIST_TA44BB082F0E5204B4C0611544C79F7ADED926E4D_H
#ifndef TIMER_T1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_H
#define TIMER_T1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.Timer
struct  Timer_t1885FA76F05D09EC4ACEBF512FCC4CB89C74D350  : public RuntimeObject
{
public:

public:
};

struct Timer_t1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_StaticFields
{
public:
	// System.Collections.Generic.List`1<BestHTTP.Extensions.TimerData> BestHTTP.Extensions.Timer::Timers
	List_1_tC0022CEFDA1D43B46DF6CA58ED7809EDFA729294 * ___Timers_0;

public:
	inline static int32_t get_offset_of_Timers_0() { return static_cast<int32_t>(offsetof(Timer_t1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_StaticFields, ___Timers_0)); }
	inline List_1_tC0022CEFDA1D43B46DF6CA58ED7809EDFA729294 * get_Timers_0() const { return ___Timers_0; }
	inline List_1_tC0022CEFDA1D43B46DF6CA58ED7809EDFA729294 ** get_address_of_Timers_0() { return &___Timers_0; }
	inline void set_Timers_0(List_1_tC0022CEFDA1D43B46DF6CA58ED7809EDFA729294 * value)
	{
		___Timers_0 = value;
		Il2CppCodeGenWriteBarrier((&___Timers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_H
#ifndef U3CU3EC_T8C8042B670333B5A9DD79B56B78BE494A99F86A3_H
#define U3CU3EC_T8C8042B670333B5A9DD79B56B78BE494A99F86A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.WWWAuthenticateHeaderParser_<>c
struct  U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3_StaticFields
{
public:
	// BestHTTP.Extensions.WWWAuthenticateHeaderParser_<>c BestHTTP.Extensions.WWWAuthenticateHeaderParser_<>c::<>9
	U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3 * ___U3CU3E9_0;
	// System.Func`2<System.Char,System.Boolean> BestHTTP.Extensions.WWWAuthenticateHeaderParser_<>c::<>9__1_0
	Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t24B247E3BCF7C1702C22AAE02DEA4686EF64C505 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8C8042B670333B5A9DD79B56B78BE494A99F86A3_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DATAFRAMEVIEW_T8CE41B4E17D7EF6814BB11D42A6EADD4C8D1EA09_H
#define DATAFRAMEVIEW_T8CE41B4E17D7EF6814BB11D42A6EADD4C8D1EA09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.DataFrameView
struct  DataFrameView_t8CE41B4E17D7EF6814BB11D42A6EADD4C8D1EA09  : public CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAFRAMEVIEW_T8CE41B4E17D7EF6814BB11D42A6EADD4C8D1EA09_H
#ifndef HEADERFRAMEVIEW_TE6AA500089659CFC19E854DF1051591BCB812532_H
#define HEADERFRAMEVIEW_TE6AA500089659CFC19E854DF1051591BCB812532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HeaderFrameView
struct  HeaderFrameView_tE6AA500089659CFC19E854DF1051591BCB812532  : public CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERFRAMEVIEW_TE6AA500089659CFC19E854DF1051591BCB812532_H
#ifndef HOSTCONNECTIONKEY_TA8107B3A146F89B83919F1CB13120EA675438A83_H
#define HOSTCONNECTIONKEY_TA8107B3A146F89B83919F1CB13120EA675438A83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.HostConnectionKey
struct  HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83 
{
public:
	// System.String BestHTTP.Core.HostConnectionKey::Host
	String_t* ___Host_0;
	// System.String BestHTTP.Core.HostConnectionKey::Connection
	String_t* ___Connection_1;

public:
	inline static int32_t get_offset_of_Host_0() { return static_cast<int32_t>(offsetof(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83, ___Host_0)); }
	inline String_t* get_Host_0() const { return ___Host_0; }
	inline String_t** get_address_of_Host_0() { return &___Host_0; }
	inline void set_Host_0(String_t* value)
	{
		___Host_0 = value;
		Il2CppCodeGenWriteBarrier((&___Host_0), value);
	}

	inline static int32_t get_offset_of_Connection_1() { return static_cast<int32_t>(offsetof(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83, ___Connection_1)); }
	inline String_t* get_Connection_1() const { return ___Connection_1; }
	inline String_t** get_address_of_Connection_1() { return &___Connection_1; }
	inline void set_Connection_1(String_t* value)
	{
		___Connection_1 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Core.HostConnectionKey
struct HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83_marshaled_pinvoke
{
	char* ___Host_0;
	char* ___Connection_1;
};
// Native definition for COM marshalling of BestHTTP.Core.HostConnectionKey
struct HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83_marshaled_com
{
	Il2CppChar* ___Host_0;
	Il2CppChar* ___Connection_1;
};
#endif // HOSTCONNECTIONKEY_TA8107B3A146F89B83919F1CB13120EA675438A83_H
#ifndef PROTOCOLEVENTINFO_T2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E_H
#define PROTOCOLEVENTINFO_T2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.ProtocolEventInfo
struct  ProtocolEventInfo_t2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E 
{
public:
	// BestHTTP.Core.IProtocol BestHTTP.Core.ProtocolEventInfo::Source
	RuntimeObject* ___Source_0;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(ProtocolEventInfo_t2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E, ___Source_0)); }
	inline RuntimeObject* get_Source_0() const { return ___Source_0; }
	inline RuntimeObject** get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(RuntimeObject* value)
	{
		___Source_0 = value;
		Il2CppCodeGenWriteBarrier((&___Source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Core.ProtocolEventInfo
struct ProtocolEventInfo_t2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E_marshaled_pinvoke
{
	RuntimeObject* ___Source_0;
};
// Native definition for COM marshalling of BestHTTP.Core.ProtocolEventInfo
struct ProtocolEventInfo_t2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E_marshaled_com
{
	RuntimeObject* ___Source_0;
};
#endif // PROTOCOLEVENTINFO_T2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E_H
#ifndef DECOMPRESSEDDATA_T3AD3F697A70A63DB3C0635F0B649C1AD50974D1E_H
#define DECOMPRESSEDDATA_T3AD3F697A70A63DB3C0635F0B649C1AD50974D1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.DecompressedData
struct  DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E 
{
public:
	// System.Byte[] BestHTTP.Decompression.DecompressedData::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.Int32 BestHTTP.Decompression.DecompressedData::Length
	int32_t ___Length_1;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSEDDATA_T3AD3F697A70A63DB3C0635F0B649C1AD50974D1E_H
#ifndef ZLIBEXCEPTION_T83268FBCA130A193BD463A661A95A84588BC4EB6_H
#define ZLIBEXCEPTION_T83268FBCA130A193BD463A661A95A84588BC4EB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibException
struct  ZlibException_t83268FBCA130A193BD463A661A95A84588BC4EB6  : public Exception_t
{
public:
	static const Il2CppGuid CLSID;

public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBEXCEPTION_T83268FBCA130A193BD463A661A95A84588BC4EB6_H
#ifndef HEADERPARSER_T8FDB3DA1551A3A21DFF16E85B6290B2AED6F6FFB_H
#define HEADERPARSER_T8FDB3DA1551A3A21DFF16E85B6290B2AED6F6FFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.HeaderParser
struct  HeaderParser_t8FDB3DA1551A3A21DFF16E85B6290B2AED6F6FFB  : public KeyValuePairList_tA44BB082F0E5204B4C0611544C79F7ADED926E4D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERPARSER_T8FDB3DA1551A3A21DFF16E85B6290B2AED6F6FFB_H
#ifndef WWWAUTHENTICATEHEADERPARSER_T244B24CB03CFC347DD40D003A2F1E5013C87A591_H
#define WWWAUTHENTICATEHEADERPARSER_T244B24CB03CFC347DD40D003A2F1E5013C87A591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.WWWAuthenticateHeaderParser
struct  WWWAuthenticateHeaderParser_t244B24CB03CFC347DD40D003A2F1E5013C87A591  : public KeyValuePairList_tA44BB082F0E5204B4C0611544C79F7ADED926E4D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWAUTHENTICATEHEADERPARSER_T244B24CB03CFC347DD40D003A2F1E5013C87A591_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FRAMESASSTREAMVIEW_TCBCE022A234E147E1ECD422DC1B01727659E5957_H
#define FRAMESASSTREAMVIEW_TCBCE022A234E147E1ECD422DC1B01727659E5957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.FramesAsStreamView
struct  FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.Connections.HTTP2.IFrameDataView BestHTTP.Connections.HTTP2.FramesAsStreamView::view
	RuntimeObject* ___view_5;

public:
	inline static int32_t get_offset_of_view_5() { return static_cast<int32_t>(offsetof(FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957, ___view_5)); }
	inline RuntimeObject* get_view_5() const { return ___view_5; }
	inline RuntimeObject** get_address_of_view_5() { return &___view_5; }
	inline void set_view_5(RuntimeObject* value)
	{
		___view_5 = value;
		Il2CppCodeGenWriteBarrier((&___view_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESASSTREAMVIEW_TCBCE022A234E147E1ECD422DC1B01727659E5957_H
#ifndef HTTP2DATAFLAGS_TD2254934552888DA529F7B6906FB98C9231F482F_H
#define HTTP2DATAFLAGS_TD2254934552888DA529F7B6906FB98C9231F482F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2DataFlags
struct  HTTP2DataFlags_tD2254934552888DA529F7B6906FB98C9231F482F 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2DataFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2DataFlags_tD2254934552888DA529F7B6906FB98C9231F482F, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2DATAFLAGS_TD2254934552888DA529F7B6906FB98C9231F482F_H
#ifndef HTTP2ERRORCODES_T8E50DD699F02848CEEC314A10BF7DB159CB64757_H
#define HTTP2ERRORCODES_T8E50DD699F02848CEEC314A10BF7DB159CB64757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2ErrorCodes
struct  HTTP2ErrorCodes_t8E50DD699F02848CEEC314A10BF7DB159CB64757 
{
public:
	// System.Int32 BestHTTP.Connections.HTTP2.HTTP2ErrorCodes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2ErrorCodes_t8E50DD699F02848CEEC314A10BF7DB159CB64757, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2ERRORCODES_T8E50DD699F02848CEEC314A10BF7DB159CB64757_H
#ifndef HTTP2FRAMETYPES_T89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4_H
#define HTTP2FRAMETYPES_T89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2FrameTypes
struct  HTTP2FrameTypes_t89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2FrameTypes::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2FrameTypes_t89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2FRAMETYPES_T89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4_H
#ifndef HTTP2HEADERSFLAGS_T6080E811301BB24A2029861029A336B69F008063_H
#define HTTP2HEADERSFLAGS_T6080E811301BB24A2029861029A336B69F008063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2HeadersFlags
struct  HTTP2HeadersFlags_t6080E811301BB24A2029861029A336B69F008063 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2HeadersFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2HeadersFlags_t6080E811301BB24A2029861029A336B69F008063, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2HEADERSFLAGS_T6080E811301BB24A2029861029A336B69F008063_H
#ifndef HTTP2PINGFLAGS_T8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154_H
#define HTTP2PINGFLAGS_T8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2PingFlags
struct  HTTP2PingFlags_t8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2PingFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2PingFlags_t8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2PINGFLAGS_T8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154_H
#ifndef HTTP2PUSHPROMISEFLAGS_T8C70B5A01735940F53941761683DF443DCF3FBE8_H
#define HTTP2PUSHPROMISEFLAGS_T8C70B5A01735940F53941761683DF443DCF3FBE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2PushPromiseFlags
struct  HTTP2PushPromiseFlags_t8C70B5A01735940F53941761683DF443DCF3FBE8 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2PushPromiseFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2PushPromiseFlags_t8C70B5A01735940F53941761683DF443DCF3FBE8, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2PUSHPROMISEFLAGS_T8C70B5A01735940F53941761683DF443DCF3FBE8_H
#ifndef HTTP2SETTINGSFLAGS_TD34C407BE277F6BEE53FEC149B3250B6F48ADE5F_H
#define HTTP2SETTINGSFLAGS_TD34C407BE277F6BEE53FEC149B3250B6F48ADE5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP2.HTTP2SettingsFlags
struct  HTTP2SettingsFlags_tD34C407BE277F6BEE53FEC149B3250B6F48ADE5F 
{
public:
	// System.Byte BestHTTP.Connections.HTTP2.HTTP2SettingsFlags::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTP2SettingsFlags_tD34C407BE277F6BEE53FEC149B3250B6F48ADE5F, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP2SETTINGSFLAGS_TD34C407BE277F6BEE53FEC149B3250B6F48ADE5F_H
#ifndef HTTPCONNECTIONSTATES_T15890FE47E949A7C91BBEBB636FD3B35C0A128CA_H
#define HTTPCONNECTIONSTATES_T15890FE47E949A7C91BBEBB636FD3B35C0A128CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTPConnectionStates
struct  HTTPConnectionStates_t15890FE47E949A7C91BBEBB636FD3B35C0A128CA 
{
public:
	// System.Int32 BestHTTP.Connections.HTTPConnectionStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTPConnectionStates_t15890FE47E949A7C91BBEBB636FD3B35C0A128CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTIONSTATES_T15890FE47E949A7C91BBEBB636FD3B35C0A128CA_H
#ifndef SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#define SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.SupportedProtocols
struct  SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2 
{
public:
	// System.Int32 BestHTTP.Connections.SupportedProtocols::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTEDPROTOCOLS_T85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2_H
#ifndef COOKIE_TA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE_H
#define COOKIE_TA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Cookies.Cookie
struct  Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE  : public RuntimeObject
{
public:
	// System.String BestHTTP.Cookies.Cookie::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.String BestHTTP.Cookies.Cookie::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_2;
	// System.DateTime BestHTTP.Cookies.Cookie::<Date>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CDateU3Ek__BackingField_3;
	// System.DateTime BestHTTP.Cookies.Cookie::<LastAccess>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CLastAccessU3Ek__BackingField_4;
	// System.DateTime BestHTTP.Cookies.Cookie::<Expires>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CExpiresU3Ek__BackingField_5;
	// System.Int64 BestHTTP.Cookies.Cookie::<MaxAge>k__BackingField
	int64_t ___U3CMaxAgeU3Ek__BackingField_6;
	// System.Boolean BestHTTP.Cookies.Cookie::<IsSession>k__BackingField
	bool ___U3CIsSessionU3Ek__BackingField_7;
	// System.String BestHTTP.Cookies.Cookie::<Domain>k__BackingField
	String_t* ___U3CDomainU3Ek__BackingField_8;
	// System.String BestHTTP.Cookies.Cookie::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_9;
	// System.Boolean BestHTTP.Cookies.Cookie::<IsSecure>k__BackingField
	bool ___U3CIsSecureU3Ek__BackingField_10;
	// System.Boolean BestHTTP.Cookies.Cookie::<IsHttpOnly>k__BackingField
	bool ___U3CIsHttpOnlyU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CDateU3Ek__BackingField_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CDateU3Ek__BackingField_3() const { return ___U3CDateU3Ek__BackingField_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CDateU3Ek__BackingField_3() { return &___U3CDateU3Ek__BackingField_3; }
	inline void set_U3CDateU3Ek__BackingField_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CDateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLastAccessU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CLastAccessU3Ek__BackingField_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CLastAccessU3Ek__BackingField_4() const { return ___U3CLastAccessU3Ek__BackingField_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CLastAccessU3Ek__BackingField_4() { return &___U3CLastAccessU3Ek__BackingField_4; }
	inline void set_U3CLastAccessU3Ek__BackingField_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CLastAccessU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CExpiresU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CExpiresU3Ek__BackingField_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CExpiresU3Ek__BackingField_5() const { return ___U3CExpiresU3Ek__BackingField_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CExpiresU3Ek__BackingField_5() { return &___U3CExpiresU3Ek__BackingField_5; }
	inline void set_U3CExpiresU3Ek__BackingField_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CExpiresU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMaxAgeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CMaxAgeU3Ek__BackingField_6)); }
	inline int64_t get_U3CMaxAgeU3Ek__BackingField_6() const { return ___U3CMaxAgeU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CMaxAgeU3Ek__BackingField_6() { return &___U3CMaxAgeU3Ek__BackingField_6; }
	inline void set_U3CMaxAgeU3Ek__BackingField_6(int64_t value)
	{
		___U3CMaxAgeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsSessionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CIsSessionU3Ek__BackingField_7)); }
	inline bool get_U3CIsSessionU3Ek__BackingField_7() const { return ___U3CIsSessionU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsSessionU3Ek__BackingField_7() { return &___U3CIsSessionU3Ek__BackingField_7; }
	inline void set_U3CIsSessionU3Ek__BackingField_7(bool value)
	{
		___U3CIsSessionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CDomainU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CDomainU3Ek__BackingField_8)); }
	inline String_t* get_U3CDomainU3Ek__BackingField_8() const { return ___U3CDomainU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CDomainU3Ek__BackingField_8() { return &___U3CDomainU3Ek__BackingField_8; }
	inline void set_U3CDomainU3Ek__BackingField_8(String_t* value)
	{
		___U3CDomainU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDomainU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CPathU3Ek__BackingField_9)); }
	inline String_t* get_U3CPathU3Ek__BackingField_9() const { return ___U3CPathU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_9() { return &___U3CPathU3Ek__BackingField_9; }
	inline void set_U3CPathU3Ek__BackingField_9(String_t* value)
	{
		___U3CPathU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIsSecureU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CIsSecureU3Ek__BackingField_10)); }
	inline bool get_U3CIsSecureU3Ek__BackingField_10() const { return ___U3CIsSecureU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsSecureU3Ek__BackingField_10() { return &___U3CIsSecureU3Ek__BackingField_10; }
	inline void set_U3CIsSecureU3Ek__BackingField_10(bool value)
	{
		___U3CIsSecureU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CIsHttpOnlyU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE, ___U3CIsHttpOnlyU3Ek__BackingField_11)); }
	inline bool get_U3CIsHttpOnlyU3Ek__BackingField_11() const { return ___U3CIsHttpOnlyU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsHttpOnlyU3Ek__BackingField_11() { return &___U3CIsHttpOnlyU3Ek__BackingField_11; }
	inline void set_U3CIsHttpOnlyU3Ek__BackingField_11(bool value)
	{
		___U3CIsHttpOnlyU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_TA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE_H
#ifndef CONNECTIONEVENTS_T9601EEE41D3C1450E1BF068ACEDFED1084B1966C_H
#define CONNECTIONEVENTS_T9601EEE41D3C1450E1BF068ACEDFED1084B1966C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.ConnectionEvents
struct  ConnectionEvents_t9601EEE41D3C1450E1BF068ACEDFED1084B1966C 
{
public:
	// System.Int32 BestHTTP.Core.ConnectionEvents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionEvents_t9601EEE41D3C1450E1BF068ACEDFED1084B1966C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONEVENTS_T9601EEE41D3C1450E1BF068ACEDFED1084B1966C_H
#ifndef HOSTPROTOCOLSUPPORT_T5615FF28D20CE674E184ED8B61719B9B7B4FC194_H
#define HOSTPROTOCOLSUPPORT_T5615FF28D20CE674E184ED8B61719B9B7B4FC194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.HostProtocolSupport
struct  HostProtocolSupport_t5615FF28D20CE674E184ED8B61719B9B7B4FC194 
{
public:
	// System.Byte BestHTTP.Core.HostProtocolSupport::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HostProtocolSupport_t5615FF28D20CE674E184ED8B61719B9B7B4FC194, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTPROTOCOLSUPPORT_T5615FF28D20CE674E184ED8B61719B9B7B4FC194_H
#ifndef PLUGINEVENTS_T137DDD8C83EED93E02C51F009423E1DF3869157D_H
#define PLUGINEVENTS_T137DDD8C83EED93E02C51F009423E1DF3869157D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.PluginEvents
struct  PluginEvents_t137DDD8C83EED93E02C51F009423E1DF3869157D 
{
public:
	// System.Int32 BestHTTP.Core.PluginEvents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PluginEvents_t137DDD8C83EED93E02C51F009423E1DF3869157D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINEVENTS_T137DDD8C83EED93E02C51F009423E1DF3869157D_H
#ifndef REQUESTEVENTS_T6D8964F57566A9976F63562DE0E52803276DFAA3_H
#define REQUESTEVENTS_T6D8964F57566A9976F63562DE0E52803276DFAA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.RequestEvents
struct  RequestEvents_t6D8964F57566A9976F63562DE0E52803276DFAA3 
{
public:
	// System.Int32 BestHTTP.Core.RequestEvents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestEvents_t6D8964F57566A9976F63562DE0E52803276DFAA3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTEVENTS_T6D8964F57566A9976F63562DE0E52803276DFAA3_H
#ifndef BLOCKSTATE_T10334335ECA45B3FECF26DD45FD97E6C56EE3C95_H
#define BLOCKSTATE_T10334335ECA45B3FECF26DD45FD97E6C56EE3C95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.BlockState
struct  BlockState_t10334335ECA45B3FECF26DD45FD97E6C56EE3C95 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.BlockState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlockState_t10334335ECA45B3FECF26DD45FD97E6C56EE3C95, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKSTATE_T10334335ECA45B3FECF26DD45FD97E6C56EE3C95_H
#ifndef COMPRESSIONLEVEL_T84967AC2C882EA97443AE1BFB4278A7F916C352A_H
#define COMPRESSIONLEVEL_T84967AC2C882EA97443AE1BFB4278A7F916C352A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.CompressionLevel
struct  CompressionLevel_t84967AC2C882EA97443AE1BFB4278A7F916C352A 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.CompressionLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionLevel_t84967AC2C882EA97443AE1BFB4278A7F916C352A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONLEVEL_T84967AC2C882EA97443AE1BFB4278A7F916C352A_H
#ifndef COMPRESSIONMODE_T05A631508E1E2CB54FDC5ABAA2493C779E30F802_H
#define COMPRESSIONMODE_T05A631508E1E2CB54FDC5ABAA2493C779E30F802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.CompressionMode
struct  CompressionMode_t05A631508E1E2CB54FDC5ABAA2493C779E30F802 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.CompressionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionMode_t05A631508E1E2CB54FDC5ABAA2493C779E30F802, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMODE_T05A631508E1E2CB54FDC5ABAA2493C779E30F802_H
#ifndef COMPRESSIONSTRATEGY_TD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6_H
#define COMPRESSIONSTRATEGY_TD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.CompressionStrategy
struct  CompressionStrategy_tD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.CompressionStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionStrategy_tD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONSTRATEGY_TD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6_H
#ifndef DEFLATEFLAVOR_T904294071FFF1282DC14B3F7B1D1E91318B23363_H
#define DEFLATEFLAVOR_T904294071FFF1282DC14B3F7B1D1E91318B23363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.DeflateFlavor
struct  DeflateFlavor_t904294071FFF1282DC14B3F7B1D1E91318B23363 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateFlavor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeflateFlavor_t904294071FFF1282DC14B3F7B1D1E91318B23363, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEFLAVOR_T904294071FFF1282DC14B3F7B1D1E91318B23363_H
#ifndef DEFLATESTREAM_T9BA10060EDB89E82509A5A440801A119107AD6CA_H
#define DEFLATESTREAM_T9BA10060EDB89E82509A5A440801A119107AD6CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.DeflateStream
struct  DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.Decompression.Zlib.ZlibBaseStream BestHTTP.Decompression.Zlib.DeflateStream::_baseStream
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 * ____baseStream_5;
	// System.IO.Stream BestHTTP.Decompression.Zlib.DeflateStream::_innerStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____innerStream_6;
	// System.Boolean BestHTTP.Decompression.Zlib.DeflateStream::_disposed
	bool ____disposed_7;

public:
	inline static int32_t get_offset_of__baseStream_5() { return static_cast<int32_t>(offsetof(DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA, ____baseStream_5)); }
	inline ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 * get__baseStream_5() const { return ____baseStream_5; }
	inline ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 ** get_address_of__baseStream_5() { return &____baseStream_5; }
	inline void set__baseStream_5(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 * value)
	{
		____baseStream_5 = value;
		Il2CppCodeGenWriteBarrier((&____baseStream_5), value);
	}

	inline static int32_t get_offset_of__innerStream_6() { return static_cast<int32_t>(offsetof(DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA, ____innerStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__innerStream_6() const { return ____innerStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__innerStream_6() { return &____innerStream_6; }
	inline void set__innerStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____innerStream_6 = value;
		Il2CppCodeGenWriteBarrier((&____innerStream_6), value);
	}

	inline static int32_t get_offset_of__disposed_7() { return static_cast<int32_t>(offsetof(DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA, ____disposed_7)); }
	inline bool get__disposed_7() const { return ____disposed_7; }
	inline bool* get_address_of__disposed_7() { return &____disposed_7; }
	inline void set__disposed_7(bool value)
	{
		____disposed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAM_T9BA10060EDB89E82509A5A440801A119107AD6CA_H
#ifndef FLUSHTYPE_T427087D15993FCE1D628C73FE8EF53E749CD08B1_H
#define FLUSHTYPE_T427087D15993FCE1D628C73FE8EF53E749CD08B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.FlushType
struct  FlushType_t427087D15993FCE1D628C73FE8EF53E749CD08B1 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.FlushType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FlushType_t427087D15993FCE1D628C73FE8EF53E749CD08B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLUSHTYPE_T427087D15993FCE1D628C73FE8EF53E749CD08B1_H
#ifndef INFLATEBLOCKMODE_T03E84E5B1DD885FE24B28CFD8E2731D71832E1BD_H
#define INFLATEBLOCKMODE_T03E84E5B1DD885FE24B28CFD8E2731D71832E1BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InflateBlocks_InflateBlockMode
struct  InflateBlockMode_t03E84E5B1DD885FE24B28CFD8E2731D71832E1BD 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks_InflateBlockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InflateBlockMode_t03E84E5B1DD885FE24B28CFD8E2731D71832E1BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEBLOCKMODE_T03E84E5B1DD885FE24B28CFD8E2731D71832E1BD_H
#ifndef INFLATEMANAGERMODE_T65CE847FF7D989A88C3D8D916A6C2E438701063C_H
#define INFLATEMANAGERMODE_T65CE847FF7D989A88C3D8D916A6C2E438701063C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InflateManager_InflateManagerMode
struct  InflateManagerMode_t65CE847FF7D989A88C3D8D916A6C2E438701063C 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.InflateManager_InflateManagerMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InflateManagerMode_t65CE847FF7D989A88C3D8D916A6C2E438701063C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEMANAGERMODE_T65CE847FF7D989A88C3D8D916A6C2E438701063C_H
#ifndef STREAMMODE_T148B1E1604579D2FD6E2842F89B56AE0D3ABBC10_H
#define STREAMMODE_T148B1E1604579D2FD6E2842F89B56AE0D3ABBC10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibBaseStream_StreamMode
struct  StreamMode_t148B1E1604579D2FD6E2842F89B56AE0D3ABBC10 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream_StreamMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamMode_t148B1E1604579D2FD6E2842F89B56AE0D3ABBC10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMMODE_T148B1E1604579D2FD6E2842F89B56AE0D3ABBC10_H
#ifndef ZLIBSTREAMFLAVOR_T77BA119D0E78B10D9A22F6A9D5782F89878F2CCF_H
#define ZLIBSTREAMFLAVOR_T77BA119D0E78B10D9A22F6A9D5782F89878F2CCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibStreamFlavor
struct  ZlibStreamFlavor_t77BA119D0E78B10D9A22F6A9D5782F89878F2CCF 
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibStreamFlavor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ZlibStreamFlavor_t77BA119D0E78B10D9A22F6A9D5782F89878F2CCF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBSTREAMFLAVOR_T77BA119D0E78B10D9A22F6A9D5782F89878F2CCF_H
#ifndef HEARTBEATMANAGER_TDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4_H
#define HEARTBEATMANAGER_TDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.HeartbeatManager
struct  HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4  : public RuntimeObject
{
public:
	// System.Threading.ReaderWriterLockSlim BestHTTP.Extensions.HeartbeatManager::rwLock
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___rwLock_0;
	// System.Collections.Generic.List`1<BestHTTP.Extensions.IHeartbeat> BestHTTP.Extensions.HeartbeatManager::Heartbeats
	List_1_t5F1CA41A6CC7FD05868DFCEADF61962EE2DC39D1 * ___Heartbeats_1;
	// BestHTTP.Extensions.IHeartbeat[] BestHTTP.Extensions.HeartbeatManager::UpdateArray
	IHeartbeatU5BU5D_t2550BA90BD32197CE2F37AA55C4EF4B0C6AD16E1* ___UpdateArray_2;
	// System.DateTime BestHTTP.Extensions.HeartbeatManager::LastUpdate
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___LastUpdate_3;

public:
	inline static int32_t get_offset_of_rwLock_0() { return static_cast<int32_t>(offsetof(HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4, ___rwLock_0)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_rwLock_0() const { return ___rwLock_0; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_rwLock_0() { return &___rwLock_0; }
	inline void set_rwLock_0(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___rwLock_0 = value;
		Il2CppCodeGenWriteBarrier((&___rwLock_0), value);
	}

	inline static int32_t get_offset_of_Heartbeats_1() { return static_cast<int32_t>(offsetof(HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4, ___Heartbeats_1)); }
	inline List_1_t5F1CA41A6CC7FD05868DFCEADF61962EE2DC39D1 * get_Heartbeats_1() const { return ___Heartbeats_1; }
	inline List_1_t5F1CA41A6CC7FD05868DFCEADF61962EE2DC39D1 ** get_address_of_Heartbeats_1() { return &___Heartbeats_1; }
	inline void set_Heartbeats_1(List_1_t5F1CA41A6CC7FD05868DFCEADF61962EE2DC39D1 * value)
	{
		___Heartbeats_1 = value;
		Il2CppCodeGenWriteBarrier((&___Heartbeats_1), value);
	}

	inline static int32_t get_offset_of_UpdateArray_2() { return static_cast<int32_t>(offsetof(HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4, ___UpdateArray_2)); }
	inline IHeartbeatU5BU5D_t2550BA90BD32197CE2F37AA55C4EF4B0C6AD16E1* get_UpdateArray_2() const { return ___UpdateArray_2; }
	inline IHeartbeatU5BU5D_t2550BA90BD32197CE2F37AA55C4EF4B0C6AD16E1** get_address_of_UpdateArray_2() { return &___UpdateArray_2; }
	inline void set_UpdateArray_2(IHeartbeatU5BU5D_t2550BA90BD32197CE2F37AA55C4EF4B0C6AD16E1* value)
	{
		___UpdateArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateArray_2), value);
	}

	inline static int32_t get_offset_of_LastUpdate_3() { return static_cast<int32_t>(offsetof(HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4, ___LastUpdate_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_LastUpdate_3() const { return ___LastUpdate_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_LastUpdate_3() { return &___LastUpdate_3; }
	inline void set_LastUpdate_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___LastUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTBEATMANAGER_TDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4_H
#ifndef READONLYBUFFEREDSTREAM_T214D7BD318E4B654384F31266BAE156AAB983324_H
#define READONLYBUFFEREDSTREAM_T214D7BD318E4B654384F31266BAE156AAB983324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.ReadOnlyBufferedStream
struct  ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream BestHTTP.Extensions.ReadOnlyBufferedStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Byte[] BestHTTP.Extensions.ReadOnlyBufferedStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_7;
	// System.Int32 BestHTTP.Extensions.ReadOnlyBufferedStream::available
	int32_t ___available_8;
	// System.Int32 BestHTTP.Extensions.ReadOnlyBufferedStream::pos
	int32_t ___pos_9;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324, ___buf_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_available_8() { return static_cast<int32_t>(offsetof(ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324, ___available_8)); }
	inline int32_t get_available_8() const { return ___available_8; }
	inline int32_t* get_address_of_available_8() { return &___available_8; }
	inline void set_available_8(int32_t value)
	{
		___available_8 = value;
	}

	inline static int32_t get_offset_of_pos_9() { return static_cast<int32_t>(offsetof(ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324, ___pos_9)); }
	inline int32_t get_pos_9() const { return ___pos_9; }
	inline int32_t* get_address_of_pos_9() { return &___pos_9; }
	inline void set_pos_9(int32_t value)
	{
		___pos_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYBUFFEREDSTREAM_T214D7BD318E4B654384F31266BAE156AAB983324_H
#ifndef STREAMLIST_T358C1789B197633EEF33AEC72AE42E364A55BE0E_H
#define STREAMLIST_T358C1789B197633EEF33AEC72AE42E364A55BE0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.StreamList
struct  StreamList_t358C1789B197633EEF33AEC72AE42E364A55BE0E  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream[] BestHTTP.Extensions.StreamList::Streams
	StreamU5BU5D_tE894588303A08E84A534F20B4AF3F1455AC73689* ___Streams_5;
	// System.Int32 BestHTTP.Extensions.StreamList::CurrentIdx
	int32_t ___CurrentIdx_6;

public:
	inline static int32_t get_offset_of_Streams_5() { return static_cast<int32_t>(offsetof(StreamList_t358C1789B197633EEF33AEC72AE42E364A55BE0E, ___Streams_5)); }
	inline StreamU5BU5D_tE894588303A08E84A534F20B4AF3F1455AC73689* get_Streams_5() const { return ___Streams_5; }
	inline StreamU5BU5D_tE894588303A08E84A534F20B4AF3F1455AC73689** get_address_of_Streams_5() { return &___Streams_5; }
	inline void set_Streams_5(StreamU5BU5D_tE894588303A08E84A534F20B4AF3F1455AC73689* value)
	{
		___Streams_5 = value;
		Il2CppCodeGenWriteBarrier((&___Streams_5), value);
	}

	inline static int32_t get_offset_of_CurrentIdx_6() { return static_cast<int32_t>(offsetof(StreamList_t358C1789B197633EEF33AEC72AE42E364A55BE0E, ___CurrentIdx_6)); }
	inline int32_t get_CurrentIdx_6() const { return ___CurrentIdx_6; }
	inline int32_t* get_address_of_CurrentIdx_6() { return &___CurrentIdx_6; }
	inline void set_CurrentIdx_6(int32_t value)
	{
		___CurrentIdx_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMLIST_T358C1789B197633EEF33AEC72AE42E364A55BE0E_H
#ifndef WRITEONLYBUFFEREDSTREAM_T9085FEDBB09B2D242603674F6411E2CCA7B6A7E6_H
#define WRITEONLYBUFFEREDSTREAM_T9085FEDBB09B2D242603674F6411E2CCA7B6A7E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.WriteOnlyBufferedStream
struct  WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Int32 BestHTTP.Extensions.WriteOnlyBufferedStream::_position
	int32_t ____position_5;
	// System.Byte[] BestHTTP.Extensions.WriteOnlyBufferedStream::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_6;
	// System.IO.Stream BestHTTP.Extensions.WriteOnlyBufferedStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_7;

public:
	inline static int32_t get_offset_of__position_5() { return static_cast<int32_t>(offsetof(WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6, ____position_5)); }
	inline int32_t get__position_5() const { return ____position_5; }
	inline int32_t* get_address_of__position_5() { return &____position_5; }
	inline void set__position_5(int32_t value)
	{
		____position_5 = value;
	}

	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6, ___buffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_6() const { return ___buffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_stream_7() { return static_cast<int32_t>(offsetof(WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6, ___stream_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_7() const { return ___stream_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_7() { return &___stream_7; }
	inline void set_stream_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_7 = value;
		Il2CppCodeGenWriteBarrier((&___stream_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEONLYBUFFEREDSTREAM_T9085FEDBB09B2D242603674F6411E2CCA7B6A7E6_H
#ifndef HTTPREQUESTSTATES_T42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E_H
#define HTTPREQUESTSTATES_T42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.HTTPRequestStates
struct  HTTPRequestStates_t42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E 
{
public:
	// System.Int32 BestHTTP.HTTPRequestStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HTTPRequestStates_t42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTSTATES_T42DCEBBE398E881CD0B9EA14E7978F2A65ECE45E_H
#ifndef SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#define SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.ShutdownTypes
struct  ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A 
{
public:
	// System.Int32 BestHTTP.ShutdownTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShutdownTypes_tEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHUTDOWNTYPES_TEC9E8F60108C0E67B381EDF65D3706DCF92CBB5A_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef CONNECTIONBASE_TBAD90BD1D60A66AACEE5A446EB0344942C791CB4_H
#define CONNECTIONBASE_TBAD90BD1D60A66AACEE5A446EB0344942C791CB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.ConnectionBase
struct  ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4  : public RuntimeObject
{
public:
	// System.String BestHTTP.Connections.ConnectionBase::<ServerAddress>k__BackingField
	String_t* ___U3CServerAddressU3Ek__BackingField_0;
	// BestHTTP.Connections.HTTPConnectionStates BestHTTP.Connections.ConnectionBase::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_1;
	// BestHTTP.HTTPRequest BestHTTP.Connections.ConnectionBase::<CurrentRequest>k__BackingField
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___U3CCurrentRequestU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.Connections.ConnectionBase::<KeepAliveTime>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CKeepAliveTimeU3Ek__BackingField_3;
	// System.DateTime BestHTTP.Connections.ConnectionBase::<StartTime>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CStartTimeU3Ek__BackingField_4;
	// System.Uri BestHTTP.Connections.ConnectionBase::<LastProcessedUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CLastProcessedUriU3Ek__BackingField_5;
	// System.DateTime BestHTTP.Connections.ConnectionBase::<LastProcessTime>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CLastProcessTimeU3Ek__BackingField_6;
	// System.Boolean BestHTTP.Connections.ConnectionBase::IsThreaded
	bool ___IsThreaded_7;
	// BestHTTP.ShutdownTypes BestHTTP.Connections.ConnectionBase::<ShutdownType>k__BackingField
	int32_t ___U3CShutdownTypeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CServerAddressU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CServerAddressU3Ek__BackingField_0)); }
	inline String_t* get_U3CServerAddressU3Ek__BackingField_0() const { return ___U3CServerAddressU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CServerAddressU3Ek__BackingField_0() { return &___U3CServerAddressU3Ek__BackingField_0; }
	inline void set_U3CServerAddressU3Ek__BackingField_0(String_t* value)
	{
		___U3CServerAddressU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerAddressU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CStateU3Ek__BackingField_1)); }
	inline int32_t get_U3CStateU3Ek__BackingField_1() const { return ___U3CStateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_1() { return &___U3CStateU3Ek__BackingField_1; }
	inline void set_U3CStateU3Ek__BackingField_1(int32_t value)
	{
		___U3CStateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CCurrentRequestU3Ek__BackingField_2)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_U3CCurrentRequestU3Ek__BackingField_2() const { return ___U3CCurrentRequestU3Ek__BackingField_2; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_U3CCurrentRequestU3Ek__BackingField_2() { return &___U3CCurrentRequestU3Ek__BackingField_2; }
	inline void set_U3CCurrentRequestU3Ek__BackingField_2(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___U3CCurrentRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentRequestU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CKeepAliveTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CKeepAliveTimeU3Ek__BackingField_3)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CKeepAliveTimeU3Ek__BackingField_3() const { return ___U3CKeepAliveTimeU3Ek__BackingField_3; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CKeepAliveTimeU3Ek__BackingField_3() { return &___U3CKeepAliveTimeU3Ek__BackingField_3; }
	inline void set_U3CKeepAliveTimeU3Ek__BackingField_3(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CKeepAliveTimeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CStartTimeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CStartTimeU3Ek__BackingField_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CStartTimeU3Ek__BackingField_4() const { return ___U3CStartTimeU3Ek__BackingField_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CStartTimeU3Ek__BackingField_4() { return &___U3CStartTimeU3Ek__BackingField_4; }
	inline void set_U3CStartTimeU3Ek__BackingField_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CStartTimeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLastProcessedUriU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CLastProcessedUriU3Ek__BackingField_5)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CLastProcessedUriU3Ek__BackingField_5() const { return ___U3CLastProcessedUriU3Ek__BackingField_5; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CLastProcessedUriU3Ek__BackingField_5() { return &___U3CLastProcessedUriU3Ek__BackingField_5; }
	inline void set_U3CLastProcessedUriU3Ek__BackingField_5(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CLastProcessedUriU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastProcessedUriU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CLastProcessTimeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CLastProcessTimeU3Ek__BackingField_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CLastProcessTimeU3Ek__BackingField_6() const { return ___U3CLastProcessTimeU3Ek__BackingField_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CLastProcessTimeU3Ek__BackingField_6() { return &___U3CLastProcessTimeU3Ek__BackingField_6; }
	inline void set_U3CLastProcessTimeU3Ek__BackingField_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CLastProcessTimeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_IsThreaded_7() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___IsThreaded_7)); }
	inline bool get_IsThreaded_7() const { return ___IsThreaded_7; }
	inline bool* get_address_of_IsThreaded_7() { return &___IsThreaded_7; }
	inline void set_IsThreaded_7(bool value)
	{
		___IsThreaded_7 = value;
	}

	inline static int32_t get_offset_of_U3CShutdownTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4, ___U3CShutdownTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CShutdownTypeU3Ek__BackingField_8() const { return ___U3CShutdownTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CShutdownTypeU3Ek__BackingField_8() { return &___U3CShutdownTypeU3Ek__BackingField_8; }
	inline void set_U3CShutdownTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CShutdownTypeU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONBASE_TBAD90BD1D60A66AACEE5A446EB0344942C791CB4_H
#ifndef HTTP1HANDLER_TD2B25124976187B833834C9D67EB678FA9B1622D_H
#define HTTP1HANDLER_TD2B25124976187B833834C9D67EB678FA9B1622D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTP1Handler
struct  HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D  : public RuntimeObject
{
public:
	// BestHTTP.Connections.KeepAliveHeader BestHTTP.Connections.HTTP1Handler::_keepAlive
	KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920 * ____keepAlive_0;
	// BestHTTP.Connections.HTTPConnection BestHTTP.Connections.HTTP1Handler::conn
	HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 * ___conn_1;
	// BestHTTP.ShutdownTypes BestHTTP.Connections.HTTP1Handler::<ShutdownType>k__BackingField
	int32_t ___U3CShutdownTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__keepAlive_0() { return static_cast<int32_t>(offsetof(HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D, ____keepAlive_0)); }
	inline KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920 * get__keepAlive_0() const { return ____keepAlive_0; }
	inline KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920 ** get_address_of__keepAlive_0() { return &____keepAlive_0; }
	inline void set__keepAlive_0(KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920 * value)
	{
		____keepAlive_0 = value;
		Il2CppCodeGenWriteBarrier((&____keepAlive_0), value);
	}

	inline static int32_t get_offset_of_conn_1() { return static_cast<int32_t>(offsetof(HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D, ___conn_1)); }
	inline HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 * get_conn_1() const { return ___conn_1; }
	inline HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 ** get_address_of_conn_1() { return &___conn_1; }
	inline void set_conn_1(HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9 * value)
	{
		___conn_1 = value;
		Il2CppCodeGenWriteBarrier((&___conn_1), value);
	}

	inline static int32_t get_offset_of_U3CShutdownTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D, ___U3CShutdownTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CShutdownTypeU3Ek__BackingField_2() const { return ___U3CShutdownTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CShutdownTypeU3Ek__BackingField_2() { return &___U3CShutdownTypeU3Ek__BackingField_2; }
	inline void set_U3CShutdownTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CShutdownTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP1HANDLER_TD2B25124976187B833834C9D67EB678FA9B1622D_H
#ifndef KEEPALIVEHEADER_TF37ABCF8589D89BF96060E326A4C9B1789A4B920_H
#define KEEPALIVEHEADER_TF37ABCF8589D89BF96060E326A4C9B1789A4B920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.KeepAliveHeader
struct  KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920  : public RuntimeObject
{
public:
	// System.TimeSpan BestHTTP.Connections.KeepAliveHeader::<TimeOut>k__BackingField
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___U3CTimeOutU3Ek__BackingField_0;
	// System.Int32 BestHTTP.Connections.KeepAliveHeader::<MaxRequests>k__BackingField
	int32_t ___U3CMaxRequestsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTimeOutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920, ___U3CTimeOutU3Ek__BackingField_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_U3CTimeOutU3Ek__BackingField_0() const { return ___U3CTimeOutU3Ek__BackingField_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_U3CTimeOutU3Ek__BackingField_0() { return &___U3CTimeOutU3Ek__BackingField_0; }
	inline void set_U3CTimeOutU3Ek__BackingField_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___U3CTimeOutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxRequestsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920, ___U3CMaxRequestsU3Ek__BackingField_1)); }
	inline int32_t get_U3CMaxRequestsU3Ek__BackingField_1() const { return ___U3CMaxRequestsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CMaxRequestsU3Ek__BackingField_1() { return &___U3CMaxRequestsU3Ek__BackingField_1; }
	inline void set_U3CMaxRequestsU3Ek__BackingField_1(int32_t value)
	{
		___U3CMaxRequestsU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEHEADER_TF37ABCF8589D89BF96060E326A4C9B1789A4B920_H
#ifndef COOKIEJAR_T03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_H
#define COOKIEJAR_T03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Cookies.CookieJar
struct  CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1  : public RuntimeObject
{
public:

public:
};

struct CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields
{
public:
	// System.TimeSpan BestHTTP.Cookies.CookieJar::AccessThreshold
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___AccessThreshold_1;
	// System.Collections.Generic.List`1<BestHTTP.Cookies.Cookie> BestHTTP.Cookies.CookieJar::Cookies
	List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * ___Cookies_2;
	// System.String BestHTTP.Cookies.CookieJar::<CookieFolder>k__BackingField
	String_t* ___U3CCookieFolderU3Ek__BackingField_3;
	// System.String BestHTTP.Cookies.CookieJar::<LibraryPath>k__BackingField
	String_t* ___U3CLibraryPathU3Ek__BackingField_4;
	// System.Threading.ReaderWriterLockSlim BestHTTP.Cookies.CookieJar::rwLock
	ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * ___rwLock_5;
	// System.Boolean BestHTTP.Cookies.CookieJar::_isSavingSupported
	bool ____isSavingSupported_6;
	// System.Boolean BestHTTP.Cookies.CookieJar::IsSupportCheckDone
	bool ___IsSupportCheckDone_7;
	// System.Boolean BestHTTP.Cookies.CookieJar::Loaded
	bool ___Loaded_8;

public:
	inline static int32_t get_offset_of_AccessThreshold_1() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___AccessThreshold_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_AccessThreshold_1() const { return ___AccessThreshold_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_AccessThreshold_1() { return &___AccessThreshold_1; }
	inline void set_AccessThreshold_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___AccessThreshold_1 = value;
	}

	inline static int32_t get_offset_of_Cookies_2() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___Cookies_2)); }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * get_Cookies_2() const { return ___Cookies_2; }
	inline List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 ** get_address_of_Cookies_2() { return &___Cookies_2; }
	inline void set_Cookies_2(List_1_tBDF40FE726C4D6DFC179C497952DFA9865528C25 * value)
	{
		___Cookies_2 = value;
		Il2CppCodeGenWriteBarrier((&___Cookies_2), value);
	}

	inline static int32_t get_offset_of_U3CCookieFolderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___U3CCookieFolderU3Ek__BackingField_3)); }
	inline String_t* get_U3CCookieFolderU3Ek__BackingField_3() const { return ___U3CCookieFolderU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CCookieFolderU3Ek__BackingField_3() { return &___U3CCookieFolderU3Ek__BackingField_3; }
	inline void set_U3CCookieFolderU3Ek__BackingField_3(String_t* value)
	{
		___U3CCookieFolderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookieFolderU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CLibraryPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___U3CLibraryPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CLibraryPathU3Ek__BackingField_4() const { return ___U3CLibraryPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CLibraryPathU3Ek__BackingField_4() { return &___U3CLibraryPathU3Ek__BackingField_4; }
	inline void set_U3CLibraryPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CLibraryPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLibraryPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_rwLock_5() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___rwLock_5)); }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * get_rwLock_5() const { return ___rwLock_5; }
	inline ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 ** get_address_of_rwLock_5() { return &___rwLock_5; }
	inline void set_rwLock_5(ReaderWriterLockSlim_tD820AC67812C645B2F8C16ABB4DE694A19D6A315 * value)
	{
		___rwLock_5 = value;
		Il2CppCodeGenWriteBarrier((&___rwLock_5), value);
	}

	inline static int32_t get_offset_of__isSavingSupported_6() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ____isSavingSupported_6)); }
	inline bool get__isSavingSupported_6() const { return ____isSavingSupported_6; }
	inline bool* get_address_of__isSavingSupported_6() { return &____isSavingSupported_6; }
	inline void set__isSavingSupported_6(bool value)
	{
		____isSavingSupported_6 = value;
	}

	inline static int32_t get_offset_of_IsSupportCheckDone_7() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___IsSupportCheckDone_7)); }
	inline bool get_IsSupportCheckDone_7() const { return ___IsSupportCheckDone_7; }
	inline bool* get_address_of_IsSupportCheckDone_7() { return &___IsSupportCheckDone_7; }
	inline void set_IsSupportCheckDone_7(bool value)
	{
		___IsSupportCheckDone_7 = value;
	}

	inline static int32_t get_offset_of_Loaded_8() { return static_cast<int32_t>(offsetof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields, ___Loaded_8)); }
	inline bool get_Loaded_8() const { return ___Loaded_8; }
	inline bool* get_address_of_Loaded_8() { return &___Loaded_8; }
	inline void set_Loaded_8(bool value)
	{
		___Loaded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEJAR_T03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_H
#ifndef CONNECTIONEVENTINFO_T1643CAE7EB52BEA644B4430E6B4D663A31E5C042_H
#define CONNECTIONEVENTINFO_T1643CAE7EB52BEA644B4430E6B4D663A31E5C042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.ConnectionEventInfo
struct  ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042 
{
public:
	// BestHTTP.Connections.ConnectionBase BestHTTP.Core.ConnectionEventInfo::Source
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4 * ___Source_0;
	// BestHTTP.Core.ConnectionEvents BestHTTP.Core.ConnectionEventInfo::Event
	int32_t ___Event_1;
	// BestHTTP.Connections.HTTPConnectionStates BestHTTP.Core.ConnectionEventInfo::State
	int32_t ___State_2;
	// BestHTTP.Core.HostProtocolSupport BestHTTP.Core.ConnectionEventInfo::ProtocolSupport
	uint8_t ___ProtocolSupport_3;

public:
	inline static int32_t get_offset_of_Source_0() { return static_cast<int32_t>(offsetof(ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042, ___Source_0)); }
	inline ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4 * get_Source_0() const { return ___Source_0; }
	inline ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4 ** get_address_of_Source_0() { return &___Source_0; }
	inline void set_Source_0(ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4 * value)
	{
		___Source_0 = value;
		Il2CppCodeGenWriteBarrier((&___Source_0), value);
	}

	inline static int32_t get_offset_of_Event_1() { return static_cast<int32_t>(offsetof(ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042, ___Event_1)); }
	inline int32_t get_Event_1() const { return ___Event_1; }
	inline int32_t* get_address_of_Event_1() { return &___Event_1; }
	inline void set_Event_1(int32_t value)
	{
		___Event_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_ProtocolSupport_3() { return static_cast<int32_t>(offsetof(ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042, ___ProtocolSupport_3)); }
	inline uint8_t get_ProtocolSupport_3() const { return ___ProtocolSupport_3; }
	inline uint8_t* get_address_of_ProtocolSupport_3() { return &___ProtocolSupport_3; }
	inline void set_ProtocolSupport_3(uint8_t value)
	{
		___ProtocolSupport_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Core.ConnectionEventInfo
struct ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042_marshaled_pinvoke
{
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4 * ___Source_0;
	int32_t ___Event_1;
	int32_t ___State_2;
	uint8_t ___ProtocolSupport_3;
};
// Native definition for COM marshalling of BestHTTP.Core.ConnectionEventInfo
struct ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042_marshaled_com
{
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4 * ___Source_0;
	int32_t ___Event_1;
	int32_t ___State_2;
	uint8_t ___ProtocolSupport_3;
};
#endif // CONNECTIONEVENTINFO_T1643CAE7EB52BEA644B4430E6B4D663A31E5C042_H
#ifndef HOSTCONNECTION_TAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59_H
#define HOSTCONNECTION_TAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.HostConnection
struct  HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59  : public RuntimeObject
{
public:
	// BestHTTP.Core.HostDefinition BestHTTP.Core.HostConnection::<Host>k__BackingField
	HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21 * ___U3CHostU3Ek__BackingField_0;
	// System.String BestHTTP.Core.HostConnection::<VariantId>k__BackingField
	String_t* ___U3CVariantIdU3Ek__BackingField_1;
	// BestHTTP.Core.HostProtocolSupport BestHTTP.Core.HostConnection::<ProtocolSupport>k__BackingField
	uint8_t ___U3CProtocolSupportU3Ek__BackingField_2;
	// System.DateTime BestHTTP.Core.HostConnection::<LastProtocolSupportUpdate>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CLastProtocolSupportUpdateU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<BestHTTP.Connections.ConnectionBase> BestHTTP.Core.HostConnection::Connections
	List_1_tB7BE8F4CA25E208914CC3A18604F5F8FCC49E282 * ___Connections_4;
	// System.Collections.Generic.List`1<BestHTTP.HTTPRequest> BestHTTP.Core.HostConnection::Queue
	List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * ___Queue_5;

public:
	inline static int32_t get_offset_of_U3CHostU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59, ___U3CHostU3Ek__BackingField_0)); }
	inline HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21 * get_U3CHostU3Ek__BackingField_0() const { return ___U3CHostU3Ek__BackingField_0; }
	inline HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21 ** get_address_of_U3CHostU3Ek__BackingField_0() { return &___U3CHostU3Ek__BackingField_0; }
	inline void set_U3CHostU3Ek__BackingField_0(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21 * value)
	{
		___U3CHostU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CVariantIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59, ___U3CVariantIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CVariantIdU3Ek__BackingField_1() const { return ___U3CVariantIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CVariantIdU3Ek__BackingField_1() { return &___U3CVariantIdU3Ek__BackingField_1; }
	inline void set_U3CVariantIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CVariantIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariantIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CProtocolSupportU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59, ___U3CProtocolSupportU3Ek__BackingField_2)); }
	inline uint8_t get_U3CProtocolSupportU3Ek__BackingField_2() const { return ___U3CProtocolSupportU3Ek__BackingField_2; }
	inline uint8_t* get_address_of_U3CProtocolSupportU3Ek__BackingField_2() { return &___U3CProtocolSupportU3Ek__BackingField_2; }
	inline void set_U3CProtocolSupportU3Ek__BackingField_2(uint8_t value)
	{
		___U3CProtocolSupportU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLastProtocolSupportUpdateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59, ___U3CLastProtocolSupportUpdateU3Ek__BackingField_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CLastProtocolSupportUpdateU3Ek__BackingField_3() const { return ___U3CLastProtocolSupportUpdateU3Ek__BackingField_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CLastProtocolSupportUpdateU3Ek__BackingField_3() { return &___U3CLastProtocolSupportUpdateU3Ek__BackingField_3; }
	inline void set_U3CLastProtocolSupportUpdateU3Ek__BackingField_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CLastProtocolSupportUpdateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_Connections_4() { return static_cast<int32_t>(offsetof(HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59, ___Connections_4)); }
	inline List_1_tB7BE8F4CA25E208914CC3A18604F5F8FCC49E282 * get_Connections_4() const { return ___Connections_4; }
	inline List_1_tB7BE8F4CA25E208914CC3A18604F5F8FCC49E282 ** get_address_of_Connections_4() { return &___Connections_4; }
	inline void set_Connections_4(List_1_tB7BE8F4CA25E208914CC3A18604F5F8FCC49E282 * value)
	{
		___Connections_4 = value;
		Il2CppCodeGenWriteBarrier((&___Connections_4), value);
	}

	inline static int32_t get_offset_of_Queue_5() { return static_cast<int32_t>(offsetof(HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59, ___Queue_5)); }
	inline List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * get_Queue_5() const { return ___Queue_5; }
	inline List_1_t0E64F6F010761063FA9757A1456A883274512FE8 ** get_address_of_Queue_5() { return &___Queue_5; }
	inline void set_Queue_5(List_1_t0E64F6F010761063FA9757A1456A883274512FE8 * value)
	{
		___Queue_5 = value;
		Il2CppCodeGenWriteBarrier((&___Queue_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSTCONNECTION_TAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59_H
#ifndef PLUGINEVENTINFO_T0BB194CED5BE1E47EF336C2513A4C36D9341A1AF_H
#define PLUGINEVENTINFO_T0BB194CED5BE1E47EF336C2513A4C36D9341A1AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.PluginEventInfo
struct  PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF 
{
public:
	// BestHTTP.Core.PluginEvents BestHTTP.Core.PluginEventInfo::Event
	int32_t ___Event_0;
	// System.Object BestHTTP.Core.PluginEventInfo::Payload
	RuntimeObject * ___Payload_1;

public:
	inline static int32_t get_offset_of_Event_0() { return static_cast<int32_t>(offsetof(PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF, ___Event_0)); }
	inline int32_t get_Event_0() const { return ___Event_0; }
	inline int32_t* get_address_of_Event_0() { return &___Event_0; }
	inline void set_Event_0(int32_t value)
	{
		___Event_0 = value;
	}

	inline static int32_t get_offset_of_Payload_1() { return static_cast<int32_t>(offsetof(PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF, ___Payload_1)); }
	inline RuntimeObject * get_Payload_1() const { return ___Payload_1; }
	inline RuntimeObject ** get_address_of_Payload_1() { return &___Payload_1; }
	inline void set_Payload_1(RuntimeObject * value)
	{
		___Payload_1 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Core.PluginEventInfo
struct PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF_marshaled_pinvoke
{
	int32_t ___Event_0;
	Il2CppIUnknown* ___Payload_1;
};
// Native definition for COM marshalling of BestHTTP.Core.PluginEventInfo
struct PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF_marshaled_com
{
	int32_t ___Event_0;
	Il2CppIUnknown* ___Payload_1;
};
#endif // PLUGINEVENTINFO_T0BB194CED5BE1E47EF336C2513A4C36D9341A1AF_H
#ifndef REQUESTEVENTINFO_T2A620ED76110D0FF7B09D86171C961B9524B8EED_H
#define REQUESTEVENTINFO_T2A620ED76110D0FF7B09D86171C961B9524B8EED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Core.RequestEventInfo
struct  RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED 
{
public:
	// BestHTTP.HTTPRequest BestHTTP.Core.RequestEventInfo::SourceRequest
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___SourceRequest_0;
	// BestHTTP.Core.RequestEvents BestHTTP.Core.RequestEventInfo::Event
	int32_t ___Event_1;
	// BestHTTP.HTTPRequestStates BestHTTP.Core.RequestEventInfo::State
	int32_t ___State_2;
	// System.Int64 BestHTTP.Core.RequestEventInfo::Progress
	int64_t ___Progress_3;
	// System.Int64 BestHTTP.Core.RequestEventInfo::ProgressLength
	int64_t ___ProgressLength_4;
	// System.Byte[] BestHTTP.Core.RequestEventInfo::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_5;
	// System.Int32 BestHTTP.Core.RequestEventInfo::DataLength
	int32_t ___DataLength_6;

public:
	inline static int32_t get_offset_of_SourceRequest_0() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___SourceRequest_0)); }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * get_SourceRequest_0() const { return ___SourceRequest_0; }
	inline HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 ** get_address_of_SourceRequest_0() { return &___SourceRequest_0; }
	inline void set_SourceRequest_0(HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * value)
	{
		___SourceRequest_0 = value;
		Il2CppCodeGenWriteBarrier((&___SourceRequest_0), value);
	}

	inline static int32_t get_offset_of_Event_1() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___Event_1)); }
	inline int32_t get_Event_1() const { return ___Event_1; }
	inline int32_t* get_address_of_Event_1() { return &___Event_1; }
	inline void set_Event_1(int32_t value)
	{
		___Event_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_Progress_3() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___Progress_3)); }
	inline int64_t get_Progress_3() const { return ___Progress_3; }
	inline int64_t* get_address_of_Progress_3() { return &___Progress_3; }
	inline void set_Progress_3(int64_t value)
	{
		___Progress_3 = value;
	}

	inline static int32_t get_offset_of_ProgressLength_4() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___ProgressLength_4)); }
	inline int64_t get_ProgressLength_4() const { return ___ProgressLength_4; }
	inline int64_t* get_address_of_ProgressLength_4() { return &___ProgressLength_4; }
	inline void set_ProgressLength_4(int64_t value)
	{
		___ProgressLength_4 = value;
	}

	inline static int32_t get_offset_of_Data_5() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___Data_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_5() const { return ___Data_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_5() { return &___Data_5; }
	inline void set_Data_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_5 = value;
		Il2CppCodeGenWriteBarrier((&___Data_5), value);
	}

	inline static int32_t get_offset_of_DataLength_6() { return static_cast<int32_t>(offsetof(RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED, ___DataLength_6)); }
	inline int32_t get_DataLength_6() const { return ___DataLength_6; }
	inline int32_t* get_address_of_DataLength_6() { return &___DataLength_6; }
	inline void set_DataLength_6(int32_t value)
	{
		___DataLength_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Core.RequestEventInfo
struct RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED_marshaled_pinvoke
{
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___SourceRequest_0;
	int32_t ___Event_1;
	int32_t ___State_2;
	int64_t ___Progress_3;
	int64_t ___ProgressLength_4;
	uint8_t* ___Data_5;
	int32_t ___DataLength_6;
};
// Native definition for COM marshalling of BestHTTP.Core.RequestEventInfo
struct RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED_marshaled_com
{
	HTTPRequest_t0D36DD78536FE0BFB9BB3F13DAE13CB7A63D2837 * ___SourceRequest_0;
	int32_t ___Event_1;
	int32_t ___State_2;
	int64_t ___Progress_3;
	int64_t ___ProgressLength_4;
	uint8_t* ___Data_5;
	int32_t ___DataLength_6;
};
#endif // REQUESTEVENTINFO_T2A620ED76110D0FF7B09D86171C961B9524B8EED_H
#ifndef DEFLATEMANAGER_T4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_H
#define DEFLATEMANAGER_T4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.DeflateManager
struct  DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E  : public RuntimeObject
{
public:
	// BestHTTP.Decompression.Zlib.DeflateManager_CompressFunc BestHTTP.Decompression.Zlib.DeflateManager::DeflateFunction
	CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E * ___DeflateFunction_2;
	// BestHTTP.Decompression.Zlib.ZlibCodec BestHTTP.Decompression.Zlib.DeflateManager::_codec
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * ____codec_21;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::status
	int32_t ___status_22;
	// System.Byte[] BestHTTP.Decompression.Zlib.DeflateManager::pending
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pending_23;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::nextPending
	int32_t ___nextPending_24;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::pendingCount
	int32_t ___pendingCount_25;
	// System.SByte BestHTTP.Decompression.Zlib.DeflateManager::data_type
	int8_t ___data_type_26;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::last_flush
	int32_t ___last_flush_27;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::w_size
	int32_t ___w_size_28;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::w_bits
	int32_t ___w_bits_29;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::w_mask
	int32_t ___w_mask_30;
	// System.Byte[] BestHTTP.Decompression.Zlib.DeflateManager::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_31;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::window_size
	int32_t ___window_size_32;
	// System.Int16[] BestHTTP.Decompression.Zlib.DeflateManager::prev
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___prev_33;
	// System.Int16[] BestHTTP.Decompression.Zlib.DeflateManager::head
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___head_34;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::ins_h
	int32_t ___ins_h_35;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::hash_size
	int32_t ___hash_size_36;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::hash_bits
	int32_t ___hash_bits_37;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::hash_mask
	int32_t ___hash_mask_38;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::hash_shift
	int32_t ___hash_shift_39;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::block_start
	int32_t ___block_start_40;
	// BestHTTP.Decompression.Zlib.DeflateManager_Config BestHTTP.Decompression.Zlib.DeflateManager::config
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB * ___config_41;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::match_length
	int32_t ___match_length_42;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::prev_match
	int32_t ___prev_match_43;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::match_available
	int32_t ___match_available_44;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::strstart
	int32_t ___strstart_45;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::match_start
	int32_t ___match_start_46;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::lookahead
	int32_t ___lookahead_47;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::prev_length
	int32_t ___prev_length_48;
	// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.Decompression.Zlib.DeflateManager::compressionLevel
	int32_t ___compressionLevel_49;
	// BestHTTP.Decompression.Zlib.CompressionStrategy BestHTTP.Decompression.Zlib.DeflateManager::compressionStrategy
	int32_t ___compressionStrategy_50;
	// System.Int16[] BestHTTP.Decompression.Zlib.DeflateManager::dyn_ltree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_ltree_51;
	// System.Int16[] BestHTTP.Decompression.Zlib.DeflateManager::dyn_dtree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_dtree_52;
	// System.Int16[] BestHTTP.Decompression.Zlib.DeflateManager::bl_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bl_tree_53;
	// BestHTTP.Decompression.Zlib.ZTree BestHTTP.Decompression.Zlib.DeflateManager::treeLiterals
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * ___treeLiterals_54;
	// BestHTTP.Decompression.Zlib.ZTree BestHTTP.Decompression.Zlib.DeflateManager::treeDistances
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * ___treeDistances_55;
	// BestHTTP.Decompression.Zlib.ZTree BestHTTP.Decompression.Zlib.DeflateManager::treeBitLengths
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * ___treeBitLengths_56;
	// System.Int16[] BestHTTP.Decompression.Zlib.DeflateManager::bl_count
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bl_count_57;
	// System.Int32[] BestHTTP.Decompression.Zlib.DeflateManager::heap
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___heap_58;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::heap_len
	int32_t ___heap_len_59;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::heap_max
	int32_t ___heap_max_60;
	// System.SByte[] BestHTTP.Decompression.Zlib.DeflateManager::depth
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___depth_61;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::_lengthOffset
	int32_t ____lengthOffset_62;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::lit_bufsize
	int32_t ___lit_bufsize_63;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::last_lit
	int32_t ___last_lit_64;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::_distanceOffset
	int32_t ____distanceOffset_65;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::opt_len
	int32_t ___opt_len_66;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::static_len
	int32_t ___static_len_67;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::matches
	int32_t ___matches_68;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::last_eob_len
	int32_t ___last_eob_len_69;
	// System.Int16 BestHTTP.Decompression.Zlib.DeflateManager::bi_buf
	int16_t ___bi_buf_70;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::bi_valid
	int32_t ___bi_valid_71;
	// System.Boolean BestHTTP.Decompression.Zlib.DeflateManager::Rfc1950BytesEmitted
	bool ___Rfc1950BytesEmitted_72;
	// System.Boolean BestHTTP.Decompression.Zlib.DeflateManager::_WantRfc1950HeaderBytes
	bool ____WantRfc1950HeaderBytes_73;

public:
	inline static int32_t get_offset_of_DeflateFunction_2() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___DeflateFunction_2)); }
	inline CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E * get_DeflateFunction_2() const { return ___DeflateFunction_2; }
	inline CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E ** get_address_of_DeflateFunction_2() { return &___DeflateFunction_2; }
	inline void set_DeflateFunction_2(CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E * value)
	{
		___DeflateFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___DeflateFunction_2), value);
	}

	inline static int32_t get_offset_of__codec_21() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ____codec_21)); }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * get__codec_21() const { return ____codec_21; }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A ** get_address_of__codec_21() { return &____codec_21; }
	inline void set__codec_21(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * value)
	{
		____codec_21 = value;
		Il2CppCodeGenWriteBarrier((&____codec_21), value);
	}

	inline static int32_t get_offset_of_status_22() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___status_22)); }
	inline int32_t get_status_22() const { return ___status_22; }
	inline int32_t* get_address_of_status_22() { return &___status_22; }
	inline void set_status_22(int32_t value)
	{
		___status_22 = value;
	}

	inline static int32_t get_offset_of_pending_23() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___pending_23)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pending_23() const { return ___pending_23; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pending_23() { return &___pending_23; }
	inline void set_pending_23(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pending_23 = value;
		Il2CppCodeGenWriteBarrier((&___pending_23), value);
	}

	inline static int32_t get_offset_of_nextPending_24() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___nextPending_24)); }
	inline int32_t get_nextPending_24() const { return ___nextPending_24; }
	inline int32_t* get_address_of_nextPending_24() { return &___nextPending_24; }
	inline void set_nextPending_24(int32_t value)
	{
		___nextPending_24 = value;
	}

	inline static int32_t get_offset_of_pendingCount_25() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___pendingCount_25)); }
	inline int32_t get_pendingCount_25() const { return ___pendingCount_25; }
	inline int32_t* get_address_of_pendingCount_25() { return &___pendingCount_25; }
	inline void set_pendingCount_25(int32_t value)
	{
		___pendingCount_25 = value;
	}

	inline static int32_t get_offset_of_data_type_26() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___data_type_26)); }
	inline int8_t get_data_type_26() const { return ___data_type_26; }
	inline int8_t* get_address_of_data_type_26() { return &___data_type_26; }
	inline void set_data_type_26(int8_t value)
	{
		___data_type_26 = value;
	}

	inline static int32_t get_offset_of_last_flush_27() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___last_flush_27)); }
	inline int32_t get_last_flush_27() const { return ___last_flush_27; }
	inline int32_t* get_address_of_last_flush_27() { return &___last_flush_27; }
	inline void set_last_flush_27(int32_t value)
	{
		___last_flush_27 = value;
	}

	inline static int32_t get_offset_of_w_size_28() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___w_size_28)); }
	inline int32_t get_w_size_28() const { return ___w_size_28; }
	inline int32_t* get_address_of_w_size_28() { return &___w_size_28; }
	inline void set_w_size_28(int32_t value)
	{
		___w_size_28 = value;
	}

	inline static int32_t get_offset_of_w_bits_29() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___w_bits_29)); }
	inline int32_t get_w_bits_29() const { return ___w_bits_29; }
	inline int32_t* get_address_of_w_bits_29() { return &___w_bits_29; }
	inline void set_w_bits_29(int32_t value)
	{
		___w_bits_29 = value;
	}

	inline static int32_t get_offset_of_w_mask_30() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___w_mask_30)); }
	inline int32_t get_w_mask_30() const { return ___w_mask_30; }
	inline int32_t* get_address_of_w_mask_30() { return &___w_mask_30; }
	inline void set_w_mask_30(int32_t value)
	{
		___w_mask_30 = value;
	}

	inline static int32_t get_offset_of_window_31() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___window_31)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_31() const { return ___window_31; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_31() { return &___window_31; }
	inline void set_window_31(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_31 = value;
		Il2CppCodeGenWriteBarrier((&___window_31), value);
	}

	inline static int32_t get_offset_of_window_size_32() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___window_size_32)); }
	inline int32_t get_window_size_32() const { return ___window_size_32; }
	inline int32_t* get_address_of_window_size_32() { return &___window_size_32; }
	inline void set_window_size_32(int32_t value)
	{
		___window_size_32 = value;
	}

	inline static int32_t get_offset_of_prev_33() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___prev_33)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_prev_33() const { return ___prev_33; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_prev_33() { return &___prev_33; }
	inline void set_prev_33(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___prev_33 = value;
		Il2CppCodeGenWriteBarrier((&___prev_33), value);
	}

	inline static int32_t get_offset_of_head_34() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___head_34)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_head_34() const { return ___head_34; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_head_34() { return &___head_34; }
	inline void set_head_34(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___head_34 = value;
		Il2CppCodeGenWriteBarrier((&___head_34), value);
	}

	inline static int32_t get_offset_of_ins_h_35() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___ins_h_35)); }
	inline int32_t get_ins_h_35() const { return ___ins_h_35; }
	inline int32_t* get_address_of_ins_h_35() { return &___ins_h_35; }
	inline void set_ins_h_35(int32_t value)
	{
		___ins_h_35 = value;
	}

	inline static int32_t get_offset_of_hash_size_36() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___hash_size_36)); }
	inline int32_t get_hash_size_36() const { return ___hash_size_36; }
	inline int32_t* get_address_of_hash_size_36() { return &___hash_size_36; }
	inline void set_hash_size_36(int32_t value)
	{
		___hash_size_36 = value;
	}

	inline static int32_t get_offset_of_hash_bits_37() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___hash_bits_37)); }
	inline int32_t get_hash_bits_37() const { return ___hash_bits_37; }
	inline int32_t* get_address_of_hash_bits_37() { return &___hash_bits_37; }
	inline void set_hash_bits_37(int32_t value)
	{
		___hash_bits_37 = value;
	}

	inline static int32_t get_offset_of_hash_mask_38() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___hash_mask_38)); }
	inline int32_t get_hash_mask_38() const { return ___hash_mask_38; }
	inline int32_t* get_address_of_hash_mask_38() { return &___hash_mask_38; }
	inline void set_hash_mask_38(int32_t value)
	{
		___hash_mask_38 = value;
	}

	inline static int32_t get_offset_of_hash_shift_39() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___hash_shift_39)); }
	inline int32_t get_hash_shift_39() const { return ___hash_shift_39; }
	inline int32_t* get_address_of_hash_shift_39() { return &___hash_shift_39; }
	inline void set_hash_shift_39(int32_t value)
	{
		___hash_shift_39 = value;
	}

	inline static int32_t get_offset_of_block_start_40() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___block_start_40)); }
	inline int32_t get_block_start_40() const { return ___block_start_40; }
	inline int32_t* get_address_of_block_start_40() { return &___block_start_40; }
	inline void set_block_start_40(int32_t value)
	{
		___block_start_40 = value;
	}

	inline static int32_t get_offset_of_config_41() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___config_41)); }
	inline Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB * get_config_41() const { return ___config_41; }
	inline Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB ** get_address_of_config_41() { return &___config_41; }
	inline void set_config_41(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB * value)
	{
		___config_41 = value;
		Il2CppCodeGenWriteBarrier((&___config_41), value);
	}

	inline static int32_t get_offset_of_match_length_42() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___match_length_42)); }
	inline int32_t get_match_length_42() const { return ___match_length_42; }
	inline int32_t* get_address_of_match_length_42() { return &___match_length_42; }
	inline void set_match_length_42(int32_t value)
	{
		___match_length_42 = value;
	}

	inline static int32_t get_offset_of_prev_match_43() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___prev_match_43)); }
	inline int32_t get_prev_match_43() const { return ___prev_match_43; }
	inline int32_t* get_address_of_prev_match_43() { return &___prev_match_43; }
	inline void set_prev_match_43(int32_t value)
	{
		___prev_match_43 = value;
	}

	inline static int32_t get_offset_of_match_available_44() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___match_available_44)); }
	inline int32_t get_match_available_44() const { return ___match_available_44; }
	inline int32_t* get_address_of_match_available_44() { return &___match_available_44; }
	inline void set_match_available_44(int32_t value)
	{
		___match_available_44 = value;
	}

	inline static int32_t get_offset_of_strstart_45() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___strstart_45)); }
	inline int32_t get_strstart_45() const { return ___strstart_45; }
	inline int32_t* get_address_of_strstart_45() { return &___strstart_45; }
	inline void set_strstart_45(int32_t value)
	{
		___strstart_45 = value;
	}

	inline static int32_t get_offset_of_match_start_46() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___match_start_46)); }
	inline int32_t get_match_start_46() const { return ___match_start_46; }
	inline int32_t* get_address_of_match_start_46() { return &___match_start_46; }
	inline void set_match_start_46(int32_t value)
	{
		___match_start_46 = value;
	}

	inline static int32_t get_offset_of_lookahead_47() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___lookahead_47)); }
	inline int32_t get_lookahead_47() const { return ___lookahead_47; }
	inline int32_t* get_address_of_lookahead_47() { return &___lookahead_47; }
	inline void set_lookahead_47(int32_t value)
	{
		___lookahead_47 = value;
	}

	inline static int32_t get_offset_of_prev_length_48() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___prev_length_48)); }
	inline int32_t get_prev_length_48() const { return ___prev_length_48; }
	inline int32_t* get_address_of_prev_length_48() { return &___prev_length_48; }
	inline void set_prev_length_48(int32_t value)
	{
		___prev_length_48 = value;
	}

	inline static int32_t get_offset_of_compressionLevel_49() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___compressionLevel_49)); }
	inline int32_t get_compressionLevel_49() const { return ___compressionLevel_49; }
	inline int32_t* get_address_of_compressionLevel_49() { return &___compressionLevel_49; }
	inline void set_compressionLevel_49(int32_t value)
	{
		___compressionLevel_49 = value;
	}

	inline static int32_t get_offset_of_compressionStrategy_50() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___compressionStrategy_50)); }
	inline int32_t get_compressionStrategy_50() const { return ___compressionStrategy_50; }
	inline int32_t* get_address_of_compressionStrategy_50() { return &___compressionStrategy_50; }
	inline void set_compressionStrategy_50(int32_t value)
	{
		___compressionStrategy_50 = value;
	}

	inline static int32_t get_offset_of_dyn_ltree_51() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___dyn_ltree_51)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_ltree_51() const { return ___dyn_ltree_51; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_ltree_51() { return &___dyn_ltree_51; }
	inline void set_dyn_ltree_51(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_ltree_51 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_ltree_51), value);
	}

	inline static int32_t get_offset_of_dyn_dtree_52() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___dyn_dtree_52)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_dtree_52() const { return ___dyn_dtree_52; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_dtree_52() { return &___dyn_dtree_52; }
	inline void set_dyn_dtree_52(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_dtree_52 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_dtree_52), value);
	}

	inline static int32_t get_offset_of_bl_tree_53() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___bl_tree_53)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bl_tree_53() const { return ___bl_tree_53; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bl_tree_53() { return &___bl_tree_53; }
	inline void set_bl_tree_53(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bl_tree_53 = value;
		Il2CppCodeGenWriteBarrier((&___bl_tree_53), value);
	}

	inline static int32_t get_offset_of_treeLiterals_54() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___treeLiterals_54)); }
	inline ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * get_treeLiterals_54() const { return ___treeLiterals_54; }
	inline ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 ** get_address_of_treeLiterals_54() { return &___treeLiterals_54; }
	inline void set_treeLiterals_54(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * value)
	{
		___treeLiterals_54 = value;
		Il2CppCodeGenWriteBarrier((&___treeLiterals_54), value);
	}

	inline static int32_t get_offset_of_treeDistances_55() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___treeDistances_55)); }
	inline ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * get_treeDistances_55() const { return ___treeDistances_55; }
	inline ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 ** get_address_of_treeDistances_55() { return &___treeDistances_55; }
	inline void set_treeDistances_55(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * value)
	{
		___treeDistances_55 = value;
		Il2CppCodeGenWriteBarrier((&___treeDistances_55), value);
	}

	inline static int32_t get_offset_of_treeBitLengths_56() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___treeBitLengths_56)); }
	inline ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * get_treeBitLengths_56() const { return ___treeBitLengths_56; }
	inline ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 ** get_address_of_treeBitLengths_56() { return &___treeBitLengths_56; }
	inline void set_treeBitLengths_56(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13 * value)
	{
		___treeBitLengths_56 = value;
		Il2CppCodeGenWriteBarrier((&___treeBitLengths_56), value);
	}

	inline static int32_t get_offset_of_bl_count_57() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___bl_count_57)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bl_count_57() const { return ___bl_count_57; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bl_count_57() { return &___bl_count_57; }
	inline void set_bl_count_57(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bl_count_57 = value;
		Il2CppCodeGenWriteBarrier((&___bl_count_57), value);
	}

	inline static int32_t get_offset_of_heap_58() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___heap_58)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_heap_58() const { return ___heap_58; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_heap_58() { return &___heap_58; }
	inline void set_heap_58(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___heap_58 = value;
		Il2CppCodeGenWriteBarrier((&___heap_58), value);
	}

	inline static int32_t get_offset_of_heap_len_59() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___heap_len_59)); }
	inline int32_t get_heap_len_59() const { return ___heap_len_59; }
	inline int32_t* get_address_of_heap_len_59() { return &___heap_len_59; }
	inline void set_heap_len_59(int32_t value)
	{
		___heap_len_59 = value;
	}

	inline static int32_t get_offset_of_heap_max_60() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___heap_max_60)); }
	inline int32_t get_heap_max_60() const { return ___heap_max_60; }
	inline int32_t* get_address_of_heap_max_60() { return &___heap_max_60; }
	inline void set_heap_max_60(int32_t value)
	{
		___heap_max_60 = value;
	}

	inline static int32_t get_offset_of_depth_61() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___depth_61)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_depth_61() const { return ___depth_61; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_depth_61() { return &___depth_61; }
	inline void set_depth_61(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___depth_61 = value;
		Il2CppCodeGenWriteBarrier((&___depth_61), value);
	}

	inline static int32_t get_offset_of__lengthOffset_62() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ____lengthOffset_62)); }
	inline int32_t get__lengthOffset_62() const { return ____lengthOffset_62; }
	inline int32_t* get_address_of__lengthOffset_62() { return &____lengthOffset_62; }
	inline void set__lengthOffset_62(int32_t value)
	{
		____lengthOffset_62 = value;
	}

	inline static int32_t get_offset_of_lit_bufsize_63() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___lit_bufsize_63)); }
	inline int32_t get_lit_bufsize_63() const { return ___lit_bufsize_63; }
	inline int32_t* get_address_of_lit_bufsize_63() { return &___lit_bufsize_63; }
	inline void set_lit_bufsize_63(int32_t value)
	{
		___lit_bufsize_63 = value;
	}

	inline static int32_t get_offset_of_last_lit_64() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___last_lit_64)); }
	inline int32_t get_last_lit_64() const { return ___last_lit_64; }
	inline int32_t* get_address_of_last_lit_64() { return &___last_lit_64; }
	inline void set_last_lit_64(int32_t value)
	{
		___last_lit_64 = value;
	}

	inline static int32_t get_offset_of__distanceOffset_65() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ____distanceOffset_65)); }
	inline int32_t get__distanceOffset_65() const { return ____distanceOffset_65; }
	inline int32_t* get_address_of__distanceOffset_65() { return &____distanceOffset_65; }
	inline void set__distanceOffset_65(int32_t value)
	{
		____distanceOffset_65 = value;
	}

	inline static int32_t get_offset_of_opt_len_66() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___opt_len_66)); }
	inline int32_t get_opt_len_66() const { return ___opt_len_66; }
	inline int32_t* get_address_of_opt_len_66() { return &___opt_len_66; }
	inline void set_opt_len_66(int32_t value)
	{
		___opt_len_66 = value;
	}

	inline static int32_t get_offset_of_static_len_67() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___static_len_67)); }
	inline int32_t get_static_len_67() const { return ___static_len_67; }
	inline int32_t* get_address_of_static_len_67() { return &___static_len_67; }
	inline void set_static_len_67(int32_t value)
	{
		___static_len_67 = value;
	}

	inline static int32_t get_offset_of_matches_68() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___matches_68)); }
	inline int32_t get_matches_68() const { return ___matches_68; }
	inline int32_t* get_address_of_matches_68() { return &___matches_68; }
	inline void set_matches_68(int32_t value)
	{
		___matches_68 = value;
	}

	inline static int32_t get_offset_of_last_eob_len_69() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___last_eob_len_69)); }
	inline int32_t get_last_eob_len_69() const { return ___last_eob_len_69; }
	inline int32_t* get_address_of_last_eob_len_69() { return &___last_eob_len_69; }
	inline void set_last_eob_len_69(int32_t value)
	{
		___last_eob_len_69 = value;
	}

	inline static int32_t get_offset_of_bi_buf_70() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___bi_buf_70)); }
	inline int16_t get_bi_buf_70() const { return ___bi_buf_70; }
	inline int16_t* get_address_of_bi_buf_70() { return &___bi_buf_70; }
	inline void set_bi_buf_70(int16_t value)
	{
		___bi_buf_70 = value;
	}

	inline static int32_t get_offset_of_bi_valid_71() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___bi_valid_71)); }
	inline int32_t get_bi_valid_71() const { return ___bi_valid_71; }
	inline int32_t* get_address_of_bi_valid_71() { return &___bi_valid_71; }
	inline void set_bi_valid_71(int32_t value)
	{
		___bi_valid_71 = value;
	}

	inline static int32_t get_offset_of_Rfc1950BytesEmitted_72() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ___Rfc1950BytesEmitted_72)); }
	inline bool get_Rfc1950BytesEmitted_72() const { return ___Rfc1950BytesEmitted_72; }
	inline bool* get_address_of_Rfc1950BytesEmitted_72() { return &___Rfc1950BytesEmitted_72; }
	inline void set_Rfc1950BytesEmitted_72(bool value)
	{
		___Rfc1950BytesEmitted_72 = value;
	}

	inline static int32_t get_offset_of__WantRfc1950HeaderBytes_73() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E, ____WantRfc1950HeaderBytes_73)); }
	inline bool get__WantRfc1950HeaderBytes_73() const { return ____WantRfc1950HeaderBytes_73; }
	inline bool* get_address_of__WantRfc1950HeaderBytes_73() { return &____WantRfc1950HeaderBytes_73; }
	inline void set__WantRfc1950HeaderBytes_73(bool value)
	{
		____WantRfc1950HeaderBytes_73 = value;
	}
};

struct DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::MEM_LEVEL_MAX
	int32_t ___MEM_LEVEL_MAX_0;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::MEM_LEVEL_DEFAULT
	int32_t ___MEM_LEVEL_DEFAULT_1;
	// System.String[] BestHTTP.Decompression.Zlib.DeflateManager::_ErrorMessage
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____ErrorMessage_3;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::PRESET_DICT
	int32_t ___PRESET_DICT_4;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::INIT_STATE
	int32_t ___INIT_STATE_5;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::BUSY_STATE
	int32_t ___BUSY_STATE_6;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::FINISH_STATE
	int32_t ___FINISH_STATE_7;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::Z_DEFLATED
	int32_t ___Z_DEFLATED_8;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::STORED_BLOCK
	int32_t ___STORED_BLOCK_9;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::STATIC_TREES
	int32_t ___STATIC_TREES_10;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::DYN_TREES
	int32_t ___DYN_TREES_11;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::Z_BINARY
	int32_t ___Z_BINARY_12;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::Z_ASCII
	int32_t ___Z_ASCII_13;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::Z_UNKNOWN
	int32_t ___Z_UNKNOWN_14;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::Buf_size
	int32_t ___Buf_size_15;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::MIN_MATCH
	int32_t ___MIN_MATCH_16;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::MAX_MATCH
	int32_t ___MAX_MATCH_17;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::MIN_LOOKAHEAD
	int32_t ___MIN_LOOKAHEAD_18;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::HEAP_SIZE
	int32_t ___HEAP_SIZE_19;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager::END_BLOCK
	int32_t ___END_BLOCK_20;

public:
	inline static int32_t get_offset_of_MEM_LEVEL_MAX_0() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___MEM_LEVEL_MAX_0)); }
	inline int32_t get_MEM_LEVEL_MAX_0() const { return ___MEM_LEVEL_MAX_0; }
	inline int32_t* get_address_of_MEM_LEVEL_MAX_0() { return &___MEM_LEVEL_MAX_0; }
	inline void set_MEM_LEVEL_MAX_0(int32_t value)
	{
		___MEM_LEVEL_MAX_0 = value;
	}

	inline static int32_t get_offset_of_MEM_LEVEL_DEFAULT_1() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___MEM_LEVEL_DEFAULT_1)); }
	inline int32_t get_MEM_LEVEL_DEFAULT_1() const { return ___MEM_LEVEL_DEFAULT_1; }
	inline int32_t* get_address_of_MEM_LEVEL_DEFAULT_1() { return &___MEM_LEVEL_DEFAULT_1; }
	inline void set_MEM_LEVEL_DEFAULT_1(int32_t value)
	{
		___MEM_LEVEL_DEFAULT_1 = value;
	}

	inline static int32_t get_offset_of__ErrorMessage_3() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ____ErrorMessage_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__ErrorMessage_3() const { return ____ErrorMessage_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__ErrorMessage_3() { return &____ErrorMessage_3; }
	inline void set__ErrorMessage_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____ErrorMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&____ErrorMessage_3), value);
	}

	inline static int32_t get_offset_of_PRESET_DICT_4() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___PRESET_DICT_4)); }
	inline int32_t get_PRESET_DICT_4() const { return ___PRESET_DICT_4; }
	inline int32_t* get_address_of_PRESET_DICT_4() { return &___PRESET_DICT_4; }
	inline void set_PRESET_DICT_4(int32_t value)
	{
		___PRESET_DICT_4 = value;
	}

	inline static int32_t get_offset_of_INIT_STATE_5() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___INIT_STATE_5)); }
	inline int32_t get_INIT_STATE_5() const { return ___INIT_STATE_5; }
	inline int32_t* get_address_of_INIT_STATE_5() { return &___INIT_STATE_5; }
	inline void set_INIT_STATE_5(int32_t value)
	{
		___INIT_STATE_5 = value;
	}

	inline static int32_t get_offset_of_BUSY_STATE_6() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___BUSY_STATE_6)); }
	inline int32_t get_BUSY_STATE_6() const { return ___BUSY_STATE_6; }
	inline int32_t* get_address_of_BUSY_STATE_6() { return &___BUSY_STATE_6; }
	inline void set_BUSY_STATE_6(int32_t value)
	{
		___BUSY_STATE_6 = value;
	}

	inline static int32_t get_offset_of_FINISH_STATE_7() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___FINISH_STATE_7)); }
	inline int32_t get_FINISH_STATE_7() const { return ___FINISH_STATE_7; }
	inline int32_t* get_address_of_FINISH_STATE_7() { return &___FINISH_STATE_7; }
	inline void set_FINISH_STATE_7(int32_t value)
	{
		___FINISH_STATE_7 = value;
	}

	inline static int32_t get_offset_of_Z_DEFLATED_8() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___Z_DEFLATED_8)); }
	inline int32_t get_Z_DEFLATED_8() const { return ___Z_DEFLATED_8; }
	inline int32_t* get_address_of_Z_DEFLATED_8() { return &___Z_DEFLATED_8; }
	inline void set_Z_DEFLATED_8(int32_t value)
	{
		___Z_DEFLATED_8 = value;
	}

	inline static int32_t get_offset_of_STORED_BLOCK_9() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___STORED_BLOCK_9)); }
	inline int32_t get_STORED_BLOCK_9() const { return ___STORED_BLOCK_9; }
	inline int32_t* get_address_of_STORED_BLOCK_9() { return &___STORED_BLOCK_9; }
	inline void set_STORED_BLOCK_9(int32_t value)
	{
		___STORED_BLOCK_9 = value;
	}

	inline static int32_t get_offset_of_STATIC_TREES_10() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___STATIC_TREES_10)); }
	inline int32_t get_STATIC_TREES_10() const { return ___STATIC_TREES_10; }
	inline int32_t* get_address_of_STATIC_TREES_10() { return &___STATIC_TREES_10; }
	inline void set_STATIC_TREES_10(int32_t value)
	{
		___STATIC_TREES_10 = value;
	}

	inline static int32_t get_offset_of_DYN_TREES_11() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___DYN_TREES_11)); }
	inline int32_t get_DYN_TREES_11() const { return ___DYN_TREES_11; }
	inline int32_t* get_address_of_DYN_TREES_11() { return &___DYN_TREES_11; }
	inline void set_DYN_TREES_11(int32_t value)
	{
		___DYN_TREES_11 = value;
	}

	inline static int32_t get_offset_of_Z_BINARY_12() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___Z_BINARY_12)); }
	inline int32_t get_Z_BINARY_12() const { return ___Z_BINARY_12; }
	inline int32_t* get_address_of_Z_BINARY_12() { return &___Z_BINARY_12; }
	inline void set_Z_BINARY_12(int32_t value)
	{
		___Z_BINARY_12 = value;
	}

	inline static int32_t get_offset_of_Z_ASCII_13() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___Z_ASCII_13)); }
	inline int32_t get_Z_ASCII_13() const { return ___Z_ASCII_13; }
	inline int32_t* get_address_of_Z_ASCII_13() { return &___Z_ASCII_13; }
	inline void set_Z_ASCII_13(int32_t value)
	{
		___Z_ASCII_13 = value;
	}

	inline static int32_t get_offset_of_Z_UNKNOWN_14() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___Z_UNKNOWN_14)); }
	inline int32_t get_Z_UNKNOWN_14() const { return ___Z_UNKNOWN_14; }
	inline int32_t* get_address_of_Z_UNKNOWN_14() { return &___Z_UNKNOWN_14; }
	inline void set_Z_UNKNOWN_14(int32_t value)
	{
		___Z_UNKNOWN_14 = value;
	}

	inline static int32_t get_offset_of_Buf_size_15() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___Buf_size_15)); }
	inline int32_t get_Buf_size_15() const { return ___Buf_size_15; }
	inline int32_t* get_address_of_Buf_size_15() { return &___Buf_size_15; }
	inline void set_Buf_size_15(int32_t value)
	{
		___Buf_size_15 = value;
	}

	inline static int32_t get_offset_of_MIN_MATCH_16() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___MIN_MATCH_16)); }
	inline int32_t get_MIN_MATCH_16() const { return ___MIN_MATCH_16; }
	inline int32_t* get_address_of_MIN_MATCH_16() { return &___MIN_MATCH_16; }
	inline void set_MIN_MATCH_16(int32_t value)
	{
		___MIN_MATCH_16 = value;
	}

	inline static int32_t get_offset_of_MAX_MATCH_17() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___MAX_MATCH_17)); }
	inline int32_t get_MAX_MATCH_17() const { return ___MAX_MATCH_17; }
	inline int32_t* get_address_of_MAX_MATCH_17() { return &___MAX_MATCH_17; }
	inline void set_MAX_MATCH_17(int32_t value)
	{
		___MAX_MATCH_17 = value;
	}

	inline static int32_t get_offset_of_MIN_LOOKAHEAD_18() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___MIN_LOOKAHEAD_18)); }
	inline int32_t get_MIN_LOOKAHEAD_18() const { return ___MIN_LOOKAHEAD_18; }
	inline int32_t* get_address_of_MIN_LOOKAHEAD_18() { return &___MIN_LOOKAHEAD_18; }
	inline void set_MIN_LOOKAHEAD_18(int32_t value)
	{
		___MIN_LOOKAHEAD_18 = value;
	}

	inline static int32_t get_offset_of_HEAP_SIZE_19() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___HEAP_SIZE_19)); }
	inline int32_t get_HEAP_SIZE_19() const { return ___HEAP_SIZE_19; }
	inline int32_t* get_address_of_HEAP_SIZE_19() { return &___HEAP_SIZE_19; }
	inline void set_HEAP_SIZE_19(int32_t value)
	{
		___HEAP_SIZE_19 = value;
	}

	inline static int32_t get_offset_of_END_BLOCK_20() { return static_cast<int32_t>(offsetof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields, ___END_BLOCK_20)); }
	inline int32_t get_END_BLOCK_20() const { return ___END_BLOCK_20; }
	inline int32_t* get_address_of_END_BLOCK_20() { return &___END_BLOCK_20; }
	inline void set_END_BLOCK_20(int32_t value)
	{
		___END_BLOCK_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEMANAGER_T4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_H
#ifndef CONFIG_TCD85FC125BF17A25D93946759151A7F3DB9869BB_H
#define CONFIG_TCD85FC125BF17A25D93946759151A7F3DB9869BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.DeflateManager_Config
struct  Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager_Config::GoodLength
	int32_t ___GoodLength_0;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager_Config::MaxLazy
	int32_t ___MaxLazy_1;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager_Config::NiceLength
	int32_t ___NiceLength_2;
	// System.Int32 BestHTTP.Decompression.Zlib.DeflateManager_Config::MaxChainLength
	int32_t ___MaxChainLength_3;
	// BestHTTP.Decompression.Zlib.DeflateFlavor BestHTTP.Decompression.Zlib.DeflateManager_Config::Flavor
	int32_t ___Flavor_4;

public:
	inline static int32_t get_offset_of_GoodLength_0() { return static_cast<int32_t>(offsetof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB, ___GoodLength_0)); }
	inline int32_t get_GoodLength_0() const { return ___GoodLength_0; }
	inline int32_t* get_address_of_GoodLength_0() { return &___GoodLength_0; }
	inline void set_GoodLength_0(int32_t value)
	{
		___GoodLength_0 = value;
	}

	inline static int32_t get_offset_of_MaxLazy_1() { return static_cast<int32_t>(offsetof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB, ___MaxLazy_1)); }
	inline int32_t get_MaxLazy_1() const { return ___MaxLazy_1; }
	inline int32_t* get_address_of_MaxLazy_1() { return &___MaxLazy_1; }
	inline void set_MaxLazy_1(int32_t value)
	{
		___MaxLazy_1 = value;
	}

	inline static int32_t get_offset_of_NiceLength_2() { return static_cast<int32_t>(offsetof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB, ___NiceLength_2)); }
	inline int32_t get_NiceLength_2() const { return ___NiceLength_2; }
	inline int32_t* get_address_of_NiceLength_2() { return &___NiceLength_2; }
	inline void set_NiceLength_2(int32_t value)
	{
		___NiceLength_2 = value;
	}

	inline static int32_t get_offset_of_MaxChainLength_3() { return static_cast<int32_t>(offsetof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB, ___MaxChainLength_3)); }
	inline int32_t get_MaxChainLength_3() const { return ___MaxChainLength_3; }
	inline int32_t* get_address_of_MaxChainLength_3() { return &___MaxChainLength_3; }
	inline void set_MaxChainLength_3(int32_t value)
	{
		___MaxChainLength_3 = value;
	}

	inline static int32_t get_offset_of_Flavor_4() { return static_cast<int32_t>(offsetof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB, ___Flavor_4)); }
	inline int32_t get_Flavor_4() const { return ___Flavor_4; }
	inline int32_t* get_address_of_Flavor_4() { return &___Flavor_4; }
	inline void set_Flavor_4(int32_t value)
	{
		___Flavor_4 = value;
	}
};

struct Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB_StaticFields
{
public:
	// BestHTTP.Decompression.Zlib.DeflateManager_Config[] BestHTTP.Decompression.Zlib.DeflateManager_Config::Table
	ConfigU5BU5D_tAE7DC78D8DD526715732DB37F1C1154179719178* ___Table_5;

public:
	inline static int32_t get_offset_of_Table_5() { return static_cast<int32_t>(offsetof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB_StaticFields, ___Table_5)); }
	inline ConfigU5BU5D_tAE7DC78D8DD526715732DB37F1C1154179719178* get_Table_5() const { return ___Table_5; }
	inline ConfigU5BU5D_tAE7DC78D8DD526715732DB37F1C1154179719178** get_address_of_Table_5() { return &___Table_5; }
	inline void set_Table_5(ConfigU5BU5D_tAE7DC78D8DD526715732DB37F1C1154179719178* value)
	{
		___Table_5 = value;
		Il2CppCodeGenWriteBarrier((&___Table_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_TCD85FC125BF17A25D93946759151A7F3DB9869BB_H
#ifndef GZIPSTREAM_TD0851D7DBA946F5E8122600760642CC0BC168BD7_H
#define GZIPSTREAM_TD0851D7DBA946F5E8122600760642CC0BC168BD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.GZipStream
struct  GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Nullable`1<System.DateTime> BestHTTP.Decompression.Zlib.GZipStream::LastModified
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___LastModified_5;
	// System.Int32 BestHTTP.Decompression.Zlib.GZipStream::_headerByteCount
	int32_t ____headerByteCount_6;
	// BestHTTP.Decompression.Zlib.ZlibBaseStream BestHTTP.Decompression.Zlib.GZipStream::_baseStream
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 * ____baseStream_7;
	// System.Boolean BestHTTP.Decompression.Zlib.GZipStream::_disposed
	bool ____disposed_8;
	// System.Boolean BestHTTP.Decompression.Zlib.GZipStream::_firstReadDone
	bool ____firstReadDone_9;
	// System.String BestHTTP.Decompression.Zlib.GZipStream::_FileName
	String_t* ____FileName_10;
	// System.String BestHTTP.Decompression.Zlib.GZipStream::_Comment
	String_t* ____Comment_11;
	// System.Int32 BestHTTP.Decompression.Zlib.GZipStream::_Crc32
	int32_t ____Crc32_12;

public:
	inline static int32_t get_offset_of_LastModified_5() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ___LastModified_5)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_LastModified_5() const { return ___LastModified_5; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_LastModified_5() { return &___LastModified_5; }
	inline void set_LastModified_5(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___LastModified_5 = value;
	}

	inline static int32_t get_offset_of__headerByteCount_6() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____headerByteCount_6)); }
	inline int32_t get__headerByteCount_6() const { return ____headerByteCount_6; }
	inline int32_t* get_address_of__headerByteCount_6() { return &____headerByteCount_6; }
	inline void set__headerByteCount_6(int32_t value)
	{
		____headerByteCount_6 = value;
	}

	inline static int32_t get_offset_of__baseStream_7() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____baseStream_7)); }
	inline ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 * get__baseStream_7() const { return ____baseStream_7; }
	inline ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 ** get_address_of__baseStream_7() { return &____baseStream_7; }
	inline void set__baseStream_7(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38 * value)
	{
		____baseStream_7 = value;
		Il2CppCodeGenWriteBarrier((&____baseStream_7), value);
	}

	inline static int32_t get_offset_of__disposed_8() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____disposed_8)); }
	inline bool get__disposed_8() const { return ____disposed_8; }
	inline bool* get_address_of__disposed_8() { return &____disposed_8; }
	inline void set__disposed_8(bool value)
	{
		____disposed_8 = value;
	}

	inline static int32_t get_offset_of__firstReadDone_9() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____firstReadDone_9)); }
	inline bool get__firstReadDone_9() const { return ____firstReadDone_9; }
	inline bool* get_address_of__firstReadDone_9() { return &____firstReadDone_9; }
	inline void set__firstReadDone_9(bool value)
	{
		____firstReadDone_9 = value;
	}

	inline static int32_t get_offset_of__FileName_10() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____FileName_10)); }
	inline String_t* get__FileName_10() const { return ____FileName_10; }
	inline String_t** get_address_of__FileName_10() { return &____FileName_10; }
	inline void set__FileName_10(String_t* value)
	{
		____FileName_10 = value;
		Il2CppCodeGenWriteBarrier((&____FileName_10), value);
	}

	inline static int32_t get_offset_of__Comment_11() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____Comment_11)); }
	inline String_t* get__Comment_11() const { return ____Comment_11; }
	inline String_t** get_address_of__Comment_11() { return &____Comment_11; }
	inline void set__Comment_11(String_t* value)
	{
		____Comment_11 = value;
		Il2CppCodeGenWriteBarrier((&____Comment_11), value);
	}

	inline static int32_t get_offset_of__Crc32_12() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7, ____Crc32_12)); }
	inline int32_t get__Crc32_12() const { return ____Crc32_12; }
	inline int32_t* get_address_of__Crc32_12() { return &____Crc32_12; }
	inline void set__Crc32_12(int32_t value)
	{
		____Crc32_12 = value;
	}
};

struct GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7_StaticFields
{
public:
	// System.DateTime BestHTTP.Decompression.Zlib.GZipStream::_unixEpoch
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____unixEpoch_13;
	// System.Text.Encoding BestHTTP.Decompression.Zlib.GZipStream::iso8859dash1
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___iso8859dash1_14;

public:
	inline static int32_t get_offset_of__unixEpoch_13() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7_StaticFields, ____unixEpoch_13)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__unixEpoch_13() const { return ____unixEpoch_13; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__unixEpoch_13() { return &____unixEpoch_13; }
	inline void set__unixEpoch_13(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____unixEpoch_13 = value;
	}

	inline static int32_t get_offset_of_iso8859dash1_14() { return static_cast<int32_t>(offsetof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7_StaticFields, ___iso8859dash1_14)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_iso8859dash1_14() const { return ___iso8859dash1_14; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_iso8859dash1_14() { return &___iso8859dash1_14; }
	inline void set_iso8859dash1_14(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___iso8859dash1_14 = value;
		Il2CppCodeGenWriteBarrier((&___iso8859dash1_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPSTREAM_TD0851D7DBA946F5E8122600760642CC0BC168BD7_H
#ifndef INFLATEBLOCKS_T54D239004644B230C64198B8574D98A3B865DA15_H
#define INFLATEBLOCKS_T54D239004644B230C64198B8574D98A3B865DA15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InflateBlocks
struct  InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15  : public RuntimeObject
{
public:
	// BestHTTP.Decompression.Zlib.InflateBlocks_InflateBlockMode BestHTTP.Decompression.Zlib.InflateBlocks::mode
	int32_t ___mode_2;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::left
	int32_t ___left_3;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::table
	int32_t ___table_4;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::index
	int32_t ___index_5;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateBlocks::blens
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___blens_6;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateBlocks::bb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bb_7;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateBlocks::tb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tb_8;
	// BestHTTP.Decompression.Zlib.InflateCodes BestHTTP.Decompression.Zlib.InflateBlocks::codes
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC * ___codes_9;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::last
	int32_t ___last_10;
	// BestHTTP.Decompression.Zlib.ZlibCodec BestHTTP.Decompression.Zlib.InflateBlocks::_codec
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * ____codec_11;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::bitk
	int32_t ___bitk_12;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::bitb
	int32_t ___bitb_13;
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateBlocks::hufts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hufts_14;
	// System.Byte[] BestHTTP.Decompression.Zlib.InflateBlocks::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_15;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::end
	int32_t ___end_16;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::readAt
	int32_t ___readAt_17;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateBlocks::writeAt
	int32_t ___writeAt_18;
	// System.Object BestHTTP.Decompression.Zlib.InflateBlocks::checkfn
	RuntimeObject * ___checkfn_19;
	// System.UInt32 BestHTTP.Decompression.Zlib.InflateBlocks::check
	uint32_t ___check_20;
	// BestHTTP.Decompression.Zlib.InfTree BestHTTP.Decompression.Zlib.InflateBlocks::inftree
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6 * ___inftree_21;

public:
	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_left_3() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___left_3)); }
	inline int32_t get_left_3() const { return ___left_3; }
	inline int32_t* get_address_of_left_3() { return &___left_3; }
	inline void set_left_3(int32_t value)
	{
		___left_3 = value;
	}

	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___table_4)); }
	inline int32_t get_table_4() const { return ___table_4; }
	inline int32_t* get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(int32_t value)
	{
		___table_4 = value;
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_blens_6() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___blens_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_blens_6() const { return ___blens_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_blens_6() { return &___blens_6; }
	inline void set_blens_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___blens_6 = value;
		Il2CppCodeGenWriteBarrier((&___blens_6), value);
	}

	inline static int32_t get_offset_of_bb_7() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___bb_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bb_7() const { return ___bb_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bb_7() { return &___bb_7; }
	inline void set_bb_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bb_7 = value;
		Il2CppCodeGenWriteBarrier((&___bb_7), value);
	}

	inline static int32_t get_offset_of_tb_8() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___tb_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tb_8() const { return ___tb_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tb_8() { return &___tb_8; }
	inline void set_tb_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tb_8 = value;
		Il2CppCodeGenWriteBarrier((&___tb_8), value);
	}

	inline static int32_t get_offset_of_codes_9() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___codes_9)); }
	inline InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC * get_codes_9() const { return ___codes_9; }
	inline InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC ** get_address_of_codes_9() { return &___codes_9; }
	inline void set_codes_9(InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC * value)
	{
		___codes_9 = value;
		Il2CppCodeGenWriteBarrier((&___codes_9), value);
	}

	inline static int32_t get_offset_of_last_10() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___last_10)); }
	inline int32_t get_last_10() const { return ___last_10; }
	inline int32_t* get_address_of_last_10() { return &___last_10; }
	inline void set_last_10(int32_t value)
	{
		___last_10 = value;
	}

	inline static int32_t get_offset_of__codec_11() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ____codec_11)); }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * get__codec_11() const { return ____codec_11; }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A ** get_address_of__codec_11() { return &____codec_11; }
	inline void set__codec_11(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * value)
	{
		____codec_11 = value;
		Il2CppCodeGenWriteBarrier((&____codec_11), value);
	}

	inline static int32_t get_offset_of_bitk_12() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___bitk_12)); }
	inline int32_t get_bitk_12() const { return ___bitk_12; }
	inline int32_t* get_address_of_bitk_12() { return &___bitk_12; }
	inline void set_bitk_12(int32_t value)
	{
		___bitk_12 = value;
	}

	inline static int32_t get_offset_of_bitb_13() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___bitb_13)); }
	inline int32_t get_bitb_13() const { return ___bitb_13; }
	inline int32_t* get_address_of_bitb_13() { return &___bitb_13; }
	inline void set_bitb_13(int32_t value)
	{
		___bitb_13 = value;
	}

	inline static int32_t get_offset_of_hufts_14() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___hufts_14)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hufts_14() const { return ___hufts_14; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hufts_14() { return &___hufts_14; }
	inline void set_hufts_14(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hufts_14 = value;
		Il2CppCodeGenWriteBarrier((&___hufts_14), value);
	}

	inline static int32_t get_offset_of_window_15() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___window_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_15() const { return ___window_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_15() { return &___window_15; }
	inline void set_window_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_15 = value;
		Il2CppCodeGenWriteBarrier((&___window_15), value);
	}

	inline static int32_t get_offset_of_end_16() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___end_16)); }
	inline int32_t get_end_16() const { return ___end_16; }
	inline int32_t* get_address_of_end_16() { return &___end_16; }
	inline void set_end_16(int32_t value)
	{
		___end_16 = value;
	}

	inline static int32_t get_offset_of_readAt_17() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___readAt_17)); }
	inline int32_t get_readAt_17() const { return ___readAt_17; }
	inline int32_t* get_address_of_readAt_17() { return &___readAt_17; }
	inline void set_readAt_17(int32_t value)
	{
		___readAt_17 = value;
	}

	inline static int32_t get_offset_of_writeAt_18() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___writeAt_18)); }
	inline int32_t get_writeAt_18() const { return ___writeAt_18; }
	inline int32_t* get_address_of_writeAt_18() { return &___writeAt_18; }
	inline void set_writeAt_18(int32_t value)
	{
		___writeAt_18 = value;
	}

	inline static int32_t get_offset_of_checkfn_19() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___checkfn_19)); }
	inline RuntimeObject * get_checkfn_19() const { return ___checkfn_19; }
	inline RuntimeObject ** get_address_of_checkfn_19() { return &___checkfn_19; }
	inline void set_checkfn_19(RuntimeObject * value)
	{
		___checkfn_19 = value;
		Il2CppCodeGenWriteBarrier((&___checkfn_19), value);
	}

	inline static int32_t get_offset_of_check_20() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___check_20)); }
	inline uint32_t get_check_20() const { return ___check_20; }
	inline uint32_t* get_address_of_check_20() { return &___check_20; }
	inline void set_check_20(uint32_t value)
	{
		___check_20 = value;
	}

	inline static int32_t get_offset_of_inftree_21() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15, ___inftree_21)); }
	inline InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6 * get_inftree_21() const { return ___inftree_21; }
	inline InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6 ** get_address_of_inftree_21() { return &___inftree_21; }
	inline void set_inftree_21(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6 * value)
	{
		___inftree_21 = value;
		Il2CppCodeGenWriteBarrier((&___inftree_21), value);
	}
};

struct InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15_StaticFields
{
public:
	// System.Int32[] BestHTTP.Decompression.Zlib.InflateBlocks::border
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___border_1;

public:
	inline static int32_t get_offset_of_border_1() { return static_cast<int32_t>(offsetof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15_StaticFields, ___border_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_border_1() const { return ___border_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_border_1() { return &___border_1; }
	inline void set_border_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___border_1 = value;
		Il2CppCodeGenWriteBarrier((&___border_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEBLOCKS_T54D239004644B230C64198B8574D98A3B865DA15_H
#ifndef INFLATEMANAGER_TA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_H
#define INFLATEMANAGER_TA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.InflateManager
struct  InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD  : public RuntimeObject
{
public:
	// BestHTTP.Decompression.Zlib.InflateManager_InflateManagerMode BestHTTP.Decompression.Zlib.InflateManager::mode
	int32_t ___mode_2;
	// BestHTTP.Decompression.Zlib.ZlibCodec BestHTTP.Decompression.Zlib.InflateManager::_codec
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * ____codec_3;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateManager::method
	int32_t ___method_4;
	// System.UInt32 BestHTTP.Decompression.Zlib.InflateManager::computedCheck
	uint32_t ___computedCheck_5;
	// System.UInt32 BestHTTP.Decompression.Zlib.InflateManager::expectedCheck
	uint32_t ___expectedCheck_6;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateManager::marker
	int32_t ___marker_7;
	// System.Boolean BestHTTP.Decompression.Zlib.InflateManager::_handleRfc1950HeaderBytes
	bool ____handleRfc1950HeaderBytes_8;
	// System.Int32 BestHTTP.Decompression.Zlib.InflateManager::wbits
	int32_t ___wbits_9;
	// BestHTTP.Decompression.Zlib.InflateBlocks BestHTTP.Decompression.Zlib.InflateManager::blocks
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15 * ___blocks_10;

public:
	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of__codec_3() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ____codec_3)); }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * get__codec_3() const { return ____codec_3; }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A ** get_address_of__codec_3() { return &____codec_3; }
	inline void set__codec_3(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * value)
	{
		____codec_3 = value;
		Il2CppCodeGenWriteBarrier((&____codec_3), value);
	}

	inline static int32_t get_offset_of_method_4() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___method_4)); }
	inline int32_t get_method_4() const { return ___method_4; }
	inline int32_t* get_address_of_method_4() { return &___method_4; }
	inline void set_method_4(int32_t value)
	{
		___method_4 = value;
	}

	inline static int32_t get_offset_of_computedCheck_5() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___computedCheck_5)); }
	inline uint32_t get_computedCheck_5() const { return ___computedCheck_5; }
	inline uint32_t* get_address_of_computedCheck_5() { return &___computedCheck_5; }
	inline void set_computedCheck_5(uint32_t value)
	{
		___computedCheck_5 = value;
	}

	inline static int32_t get_offset_of_expectedCheck_6() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___expectedCheck_6)); }
	inline uint32_t get_expectedCheck_6() const { return ___expectedCheck_6; }
	inline uint32_t* get_address_of_expectedCheck_6() { return &___expectedCheck_6; }
	inline void set_expectedCheck_6(uint32_t value)
	{
		___expectedCheck_6 = value;
	}

	inline static int32_t get_offset_of_marker_7() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___marker_7)); }
	inline int32_t get_marker_7() const { return ___marker_7; }
	inline int32_t* get_address_of_marker_7() { return &___marker_7; }
	inline void set_marker_7(int32_t value)
	{
		___marker_7 = value;
	}

	inline static int32_t get_offset_of__handleRfc1950HeaderBytes_8() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ____handleRfc1950HeaderBytes_8)); }
	inline bool get__handleRfc1950HeaderBytes_8() const { return ____handleRfc1950HeaderBytes_8; }
	inline bool* get_address_of__handleRfc1950HeaderBytes_8() { return &____handleRfc1950HeaderBytes_8; }
	inline void set__handleRfc1950HeaderBytes_8(bool value)
	{
		____handleRfc1950HeaderBytes_8 = value;
	}

	inline static int32_t get_offset_of_wbits_9() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___wbits_9)); }
	inline int32_t get_wbits_9() const { return ___wbits_9; }
	inline int32_t* get_address_of_wbits_9() { return &___wbits_9; }
	inline void set_wbits_9(int32_t value)
	{
		___wbits_9 = value;
	}

	inline static int32_t get_offset_of_blocks_10() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD, ___blocks_10)); }
	inline InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15 * get_blocks_10() const { return ___blocks_10; }
	inline InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15 ** get_address_of_blocks_10() { return &___blocks_10; }
	inline void set_blocks_10(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15 * value)
	{
		___blocks_10 = value;
		Il2CppCodeGenWriteBarrier((&___blocks_10), value);
	}
};

struct InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_StaticFields
{
public:
	// System.Byte[] BestHTTP.Decompression.Zlib.InflateManager::mark
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mark_11;

public:
	inline static int32_t get_offset_of_mark_11() { return static_cast<int32_t>(offsetof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_StaticFields, ___mark_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mark_11() const { return ___mark_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mark_11() { return &___mark_11; }
	inline void set_mark_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mark_11 = value;
		Il2CppCodeGenWriteBarrier((&___mark_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEMANAGER_TA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_H
#ifndef ZLIBBASESTREAM_T188967618B2DFC2C8FBD2161F6857DD5FC7EED38_H
#define ZLIBBASESTREAM_T188967618B2DFC2C8FBD2161F6857DD5FC7EED38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibBaseStream
struct  ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// BestHTTP.Decompression.Zlib.ZlibCodec BestHTTP.Decompression.Zlib.ZlibBaseStream::_z
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * ____z_5;
	// BestHTTP.Decompression.Zlib.ZlibBaseStream_StreamMode BestHTTP.Decompression.Zlib.ZlibBaseStream::_streamMode
	int32_t ____streamMode_6;
	// BestHTTP.Decompression.Zlib.FlushType BestHTTP.Decompression.Zlib.ZlibBaseStream::_flushMode
	int32_t ____flushMode_7;
	// BestHTTP.Decompression.Zlib.ZlibStreamFlavor BestHTTP.Decompression.Zlib.ZlibBaseStream::_flavor
	int32_t ____flavor_8;
	// BestHTTP.Decompression.Zlib.CompressionMode BestHTTP.Decompression.Zlib.ZlibBaseStream::_compressionMode
	int32_t ____compressionMode_9;
	// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.Decompression.Zlib.ZlibBaseStream::_level
	int32_t ____level_10;
	// System.Boolean BestHTTP.Decompression.Zlib.ZlibBaseStream::_leaveOpen
	bool ____leaveOpen_11;
	// System.Byte[] BestHTTP.Decompression.Zlib.ZlibBaseStream::_workingBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____workingBuffer_12;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream::_bufferSize
	int32_t ____bufferSize_13;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream::windowBitsMax
	int32_t ___windowBitsMax_14;
	// System.Byte[] BestHTTP.Decompression.Zlib.ZlibBaseStream::_buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buf1_15;
	// System.IO.Stream BestHTTP.Decompression.Zlib.ZlibBaseStream::_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____stream_16;
	// BestHTTP.Decompression.Zlib.CompressionStrategy BestHTTP.Decompression.Zlib.ZlibBaseStream::Strategy
	int32_t ___Strategy_17;
	// BestHTTP.Decompression.Crc.CRC32 BestHTTP.Decompression.Zlib.ZlibBaseStream::crc
	CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC * ___crc_18;
	// System.String BestHTTP.Decompression.Zlib.ZlibBaseStream::_GzipFileName
	String_t* ____GzipFileName_19;
	// System.String BestHTTP.Decompression.Zlib.ZlibBaseStream::_GzipComment
	String_t* ____GzipComment_20;
	// System.DateTime BestHTTP.Decompression.Zlib.ZlibBaseStream::_GzipMtime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____GzipMtime_21;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibBaseStream::_gzipHeaderByteCount
	int32_t ____gzipHeaderByteCount_22;
	// System.Boolean BestHTTP.Decompression.Zlib.ZlibBaseStream::nomoreinput
	bool ___nomoreinput_23;

public:
	inline static int32_t get_offset_of__z_5() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____z_5)); }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * get__z_5() const { return ____z_5; }
	inline ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A ** get_address_of__z_5() { return &____z_5; }
	inline void set__z_5(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A * value)
	{
		____z_5 = value;
		Il2CppCodeGenWriteBarrier((&____z_5), value);
	}

	inline static int32_t get_offset_of__streamMode_6() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____streamMode_6)); }
	inline int32_t get__streamMode_6() const { return ____streamMode_6; }
	inline int32_t* get_address_of__streamMode_6() { return &____streamMode_6; }
	inline void set__streamMode_6(int32_t value)
	{
		____streamMode_6 = value;
	}

	inline static int32_t get_offset_of__flushMode_7() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____flushMode_7)); }
	inline int32_t get__flushMode_7() const { return ____flushMode_7; }
	inline int32_t* get_address_of__flushMode_7() { return &____flushMode_7; }
	inline void set__flushMode_7(int32_t value)
	{
		____flushMode_7 = value;
	}

	inline static int32_t get_offset_of__flavor_8() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____flavor_8)); }
	inline int32_t get__flavor_8() const { return ____flavor_8; }
	inline int32_t* get_address_of__flavor_8() { return &____flavor_8; }
	inline void set__flavor_8(int32_t value)
	{
		____flavor_8 = value;
	}

	inline static int32_t get_offset_of__compressionMode_9() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____compressionMode_9)); }
	inline int32_t get__compressionMode_9() const { return ____compressionMode_9; }
	inline int32_t* get_address_of__compressionMode_9() { return &____compressionMode_9; }
	inline void set__compressionMode_9(int32_t value)
	{
		____compressionMode_9 = value;
	}

	inline static int32_t get_offset_of__level_10() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____level_10)); }
	inline int32_t get__level_10() const { return ____level_10; }
	inline int32_t* get_address_of__level_10() { return &____level_10; }
	inline void set__level_10(int32_t value)
	{
		____level_10 = value;
	}

	inline static int32_t get_offset_of__leaveOpen_11() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____leaveOpen_11)); }
	inline bool get__leaveOpen_11() const { return ____leaveOpen_11; }
	inline bool* get_address_of__leaveOpen_11() { return &____leaveOpen_11; }
	inline void set__leaveOpen_11(bool value)
	{
		____leaveOpen_11 = value;
	}

	inline static int32_t get_offset_of__workingBuffer_12() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____workingBuffer_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__workingBuffer_12() const { return ____workingBuffer_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__workingBuffer_12() { return &____workingBuffer_12; }
	inline void set__workingBuffer_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____workingBuffer_12 = value;
		Il2CppCodeGenWriteBarrier((&____workingBuffer_12), value);
	}

	inline static int32_t get_offset_of__bufferSize_13() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____bufferSize_13)); }
	inline int32_t get__bufferSize_13() const { return ____bufferSize_13; }
	inline int32_t* get_address_of__bufferSize_13() { return &____bufferSize_13; }
	inline void set__bufferSize_13(int32_t value)
	{
		____bufferSize_13 = value;
	}

	inline static int32_t get_offset_of_windowBitsMax_14() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ___windowBitsMax_14)); }
	inline int32_t get_windowBitsMax_14() const { return ___windowBitsMax_14; }
	inline int32_t* get_address_of_windowBitsMax_14() { return &___windowBitsMax_14; }
	inline void set_windowBitsMax_14(int32_t value)
	{
		___windowBitsMax_14 = value;
	}

	inline static int32_t get_offset_of__buf1_15() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____buf1_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buf1_15() const { return ____buf1_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buf1_15() { return &____buf1_15; }
	inline void set__buf1_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buf1_15 = value;
		Il2CppCodeGenWriteBarrier((&____buf1_15), value);
	}

	inline static int32_t get_offset_of__stream_16() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____stream_16)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__stream_16() const { return ____stream_16; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__stream_16() { return &____stream_16; }
	inline void set__stream_16(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____stream_16 = value;
		Il2CppCodeGenWriteBarrier((&____stream_16), value);
	}

	inline static int32_t get_offset_of_Strategy_17() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ___Strategy_17)); }
	inline int32_t get_Strategy_17() const { return ___Strategy_17; }
	inline int32_t* get_address_of_Strategy_17() { return &___Strategy_17; }
	inline void set_Strategy_17(int32_t value)
	{
		___Strategy_17 = value;
	}

	inline static int32_t get_offset_of_crc_18() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ___crc_18)); }
	inline CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC * get_crc_18() const { return ___crc_18; }
	inline CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC ** get_address_of_crc_18() { return &___crc_18; }
	inline void set_crc_18(CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC * value)
	{
		___crc_18 = value;
		Il2CppCodeGenWriteBarrier((&___crc_18), value);
	}

	inline static int32_t get_offset_of__GzipFileName_19() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____GzipFileName_19)); }
	inline String_t* get__GzipFileName_19() const { return ____GzipFileName_19; }
	inline String_t** get_address_of__GzipFileName_19() { return &____GzipFileName_19; }
	inline void set__GzipFileName_19(String_t* value)
	{
		____GzipFileName_19 = value;
		Il2CppCodeGenWriteBarrier((&____GzipFileName_19), value);
	}

	inline static int32_t get_offset_of__GzipComment_20() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____GzipComment_20)); }
	inline String_t* get__GzipComment_20() const { return ____GzipComment_20; }
	inline String_t** get_address_of__GzipComment_20() { return &____GzipComment_20; }
	inline void set__GzipComment_20(String_t* value)
	{
		____GzipComment_20 = value;
		Il2CppCodeGenWriteBarrier((&____GzipComment_20), value);
	}

	inline static int32_t get_offset_of__GzipMtime_21() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____GzipMtime_21)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__GzipMtime_21() const { return ____GzipMtime_21; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__GzipMtime_21() { return &____GzipMtime_21; }
	inline void set__GzipMtime_21(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____GzipMtime_21 = value;
	}

	inline static int32_t get_offset_of__gzipHeaderByteCount_22() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ____gzipHeaderByteCount_22)); }
	inline int32_t get__gzipHeaderByteCount_22() const { return ____gzipHeaderByteCount_22; }
	inline int32_t* get_address_of__gzipHeaderByteCount_22() { return &____gzipHeaderByteCount_22; }
	inline void set__gzipHeaderByteCount_22(int32_t value)
	{
		____gzipHeaderByteCount_22 = value;
	}

	inline static int32_t get_offset_of_nomoreinput_23() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38, ___nomoreinput_23)); }
	inline bool get_nomoreinput_23() const { return ___nomoreinput_23; }
	inline bool* get_address_of_nomoreinput_23() { return &___nomoreinput_23; }
	inline void set_nomoreinput_23(bool value)
	{
		___nomoreinput_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBBASESTREAM_T188967618B2DFC2C8FBD2161F6857DD5FC7EED38_H
#ifndef ZLIBCODEC_T71C6443857F9F9DB2D19EB730514438EEDBE242A_H
#define ZLIBCODEC_T71C6443857F9F9DB2D19EB730514438EEDBE242A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.ZlibCodec
struct  ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.Decompression.Zlib.ZlibCodec::InputBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___InputBuffer_0;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibCodec::NextIn
	int32_t ___NextIn_1;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibCodec::AvailableBytesIn
	int32_t ___AvailableBytesIn_2;
	// System.Int64 BestHTTP.Decompression.Zlib.ZlibCodec::TotalBytesIn
	int64_t ___TotalBytesIn_3;
	// System.Byte[] BestHTTP.Decompression.Zlib.ZlibCodec::OutputBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OutputBuffer_4;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibCodec::NextOut
	int32_t ___NextOut_5;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibCodec::AvailableBytesOut
	int32_t ___AvailableBytesOut_6;
	// System.Int64 BestHTTP.Decompression.Zlib.ZlibCodec::TotalBytesOut
	int64_t ___TotalBytesOut_7;
	// System.String BestHTTP.Decompression.Zlib.ZlibCodec::Message
	String_t* ___Message_8;
	// BestHTTP.Decompression.Zlib.DeflateManager BestHTTP.Decompression.Zlib.ZlibCodec::dstate
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E * ___dstate_9;
	// BestHTTP.Decompression.Zlib.InflateManager BestHTTP.Decompression.Zlib.ZlibCodec::istate
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD * ___istate_10;
	// System.UInt32 BestHTTP.Decompression.Zlib.ZlibCodec::_Adler32
	uint32_t ____Adler32_11;
	// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.Decompression.Zlib.ZlibCodec::CompressLevel
	int32_t ___CompressLevel_12;
	// System.Int32 BestHTTP.Decompression.Zlib.ZlibCodec::WindowBits
	int32_t ___WindowBits_13;
	// BestHTTP.Decompression.Zlib.CompressionStrategy BestHTTP.Decompression.Zlib.ZlibCodec::Strategy
	int32_t ___Strategy_14;

public:
	inline static int32_t get_offset_of_InputBuffer_0() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___InputBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_InputBuffer_0() const { return ___InputBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_InputBuffer_0() { return &___InputBuffer_0; }
	inline void set_InputBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___InputBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___InputBuffer_0), value);
	}

	inline static int32_t get_offset_of_NextIn_1() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___NextIn_1)); }
	inline int32_t get_NextIn_1() const { return ___NextIn_1; }
	inline int32_t* get_address_of_NextIn_1() { return &___NextIn_1; }
	inline void set_NextIn_1(int32_t value)
	{
		___NextIn_1 = value;
	}

	inline static int32_t get_offset_of_AvailableBytesIn_2() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___AvailableBytesIn_2)); }
	inline int32_t get_AvailableBytesIn_2() const { return ___AvailableBytesIn_2; }
	inline int32_t* get_address_of_AvailableBytesIn_2() { return &___AvailableBytesIn_2; }
	inline void set_AvailableBytesIn_2(int32_t value)
	{
		___AvailableBytesIn_2 = value;
	}

	inline static int32_t get_offset_of_TotalBytesIn_3() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___TotalBytesIn_3)); }
	inline int64_t get_TotalBytesIn_3() const { return ___TotalBytesIn_3; }
	inline int64_t* get_address_of_TotalBytesIn_3() { return &___TotalBytesIn_3; }
	inline void set_TotalBytesIn_3(int64_t value)
	{
		___TotalBytesIn_3 = value;
	}

	inline static int32_t get_offset_of_OutputBuffer_4() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___OutputBuffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OutputBuffer_4() const { return ___OutputBuffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OutputBuffer_4() { return &___OutputBuffer_4; }
	inline void set_OutputBuffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OutputBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___OutputBuffer_4), value);
	}

	inline static int32_t get_offset_of_NextOut_5() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___NextOut_5)); }
	inline int32_t get_NextOut_5() const { return ___NextOut_5; }
	inline int32_t* get_address_of_NextOut_5() { return &___NextOut_5; }
	inline void set_NextOut_5(int32_t value)
	{
		___NextOut_5 = value;
	}

	inline static int32_t get_offset_of_AvailableBytesOut_6() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___AvailableBytesOut_6)); }
	inline int32_t get_AvailableBytesOut_6() const { return ___AvailableBytesOut_6; }
	inline int32_t* get_address_of_AvailableBytesOut_6() { return &___AvailableBytesOut_6; }
	inline void set_AvailableBytesOut_6(int32_t value)
	{
		___AvailableBytesOut_6 = value;
	}

	inline static int32_t get_offset_of_TotalBytesOut_7() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___TotalBytesOut_7)); }
	inline int64_t get_TotalBytesOut_7() const { return ___TotalBytesOut_7; }
	inline int64_t* get_address_of_TotalBytesOut_7() { return &___TotalBytesOut_7; }
	inline void set_TotalBytesOut_7(int64_t value)
	{
		___TotalBytesOut_7 = value;
	}

	inline static int32_t get_offset_of_Message_8() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___Message_8)); }
	inline String_t* get_Message_8() const { return ___Message_8; }
	inline String_t** get_address_of_Message_8() { return &___Message_8; }
	inline void set_Message_8(String_t* value)
	{
		___Message_8 = value;
		Il2CppCodeGenWriteBarrier((&___Message_8), value);
	}

	inline static int32_t get_offset_of_dstate_9() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___dstate_9)); }
	inline DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E * get_dstate_9() const { return ___dstate_9; }
	inline DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E ** get_address_of_dstate_9() { return &___dstate_9; }
	inline void set_dstate_9(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E * value)
	{
		___dstate_9 = value;
		Il2CppCodeGenWriteBarrier((&___dstate_9), value);
	}

	inline static int32_t get_offset_of_istate_10() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___istate_10)); }
	inline InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD * get_istate_10() const { return ___istate_10; }
	inline InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD ** get_address_of_istate_10() { return &___istate_10; }
	inline void set_istate_10(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD * value)
	{
		___istate_10 = value;
		Il2CppCodeGenWriteBarrier((&___istate_10), value);
	}

	inline static int32_t get_offset_of__Adler32_11() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ____Adler32_11)); }
	inline uint32_t get__Adler32_11() const { return ____Adler32_11; }
	inline uint32_t* get_address_of__Adler32_11() { return &____Adler32_11; }
	inline void set__Adler32_11(uint32_t value)
	{
		____Adler32_11 = value;
	}

	inline static int32_t get_offset_of_CompressLevel_12() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___CompressLevel_12)); }
	inline int32_t get_CompressLevel_12() const { return ___CompressLevel_12; }
	inline int32_t* get_address_of_CompressLevel_12() { return &___CompressLevel_12; }
	inline void set_CompressLevel_12(int32_t value)
	{
		___CompressLevel_12 = value;
	}

	inline static int32_t get_offset_of_WindowBits_13() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___WindowBits_13)); }
	inline int32_t get_WindowBits_13() const { return ___WindowBits_13; }
	inline int32_t* get_address_of_WindowBits_13() { return &___WindowBits_13; }
	inline void set_WindowBits_13(int32_t value)
	{
		___WindowBits_13 = value;
	}

	inline static int32_t get_offset_of_Strategy_14() { return static_cast<int32_t>(offsetof(ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A, ___Strategy_14)); }
	inline int32_t get_Strategy_14() const { return ___Strategy_14; }
	inline int32_t* get_address_of_Strategy_14() { return &___Strategy_14; }
	inline void set_Strategy_14(int32_t value)
	{
		___Strategy_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBCODEC_T71C6443857F9F9DB2D19EB730514438EEDBE242A_H
#ifndef TIMERDATA_TF665DC908A53FF58B28A5956B9726A9FC1AA6920_H
#define TIMERDATA_TF665DC908A53FF58B28A5956B9726A9FC1AA6920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Extensions.TimerData
struct  TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920 
{
public:
	// System.DateTime BestHTTP.Extensions.TimerData::Created
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___Created_0;
	// System.TimeSpan BestHTTP.Extensions.TimerData::Interval
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Interval_1;
	// System.Object BestHTTP.Extensions.TimerData::Context
	RuntimeObject * ___Context_2;
	// System.Func`2<System.Object,System.Boolean> BestHTTP.Extensions.TimerData::OnTimer
	Func_2_t7EE965B791A606D187CCB69569A433D4CBB36879 * ___OnTimer_3;

public:
	inline static int32_t get_offset_of_Created_0() { return static_cast<int32_t>(offsetof(TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920, ___Created_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_Created_0() const { return ___Created_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_Created_0() { return &___Created_0; }
	inline void set_Created_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___Created_0 = value;
	}

	inline static int32_t get_offset_of_Interval_1() { return static_cast<int32_t>(offsetof(TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920, ___Interval_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Interval_1() const { return ___Interval_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Interval_1() { return &___Interval_1; }
	inline void set_Interval_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Interval_1 = value;
	}

	inline static int32_t get_offset_of_Context_2() { return static_cast<int32_t>(offsetof(TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920, ___Context_2)); }
	inline RuntimeObject * get_Context_2() const { return ___Context_2; }
	inline RuntimeObject ** get_address_of_Context_2() { return &___Context_2; }
	inline void set_Context_2(RuntimeObject * value)
	{
		___Context_2 = value;
		Il2CppCodeGenWriteBarrier((&___Context_2), value);
	}

	inline static int32_t get_offset_of_OnTimer_3() { return static_cast<int32_t>(offsetof(TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920, ___OnTimer_3)); }
	inline Func_2_t7EE965B791A606D187CCB69569A433D4CBB36879 * get_OnTimer_3() const { return ___OnTimer_3; }
	inline Func_2_t7EE965B791A606D187CCB69569A433D4CBB36879 ** get_address_of_OnTimer_3() { return &___OnTimer_3; }
	inline void set_OnTimer_3(Func_2_t7EE965B791A606D187CCB69569A433D4CBB36879 * value)
	{
		___OnTimer_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTimer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of BestHTTP.Extensions.TimerData
struct TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920_marshaled_pinvoke
{
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___Created_0;
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Interval_1;
	Il2CppIUnknown* ___Context_2;
	Il2CppMethodPointer ___OnTimer_3;
};
// Native definition for COM marshalling of BestHTTP.Extensions.TimerData
struct TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920_marshaled_com
{
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___Created_0;
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Interval_1;
	Il2CppIUnknown* ___Context_2;
	Il2CppMethodPointer ___OnTimer_3;
};
#endif // TIMERDATA_TF665DC908A53FF58B28A5956B9726A9FC1AA6920_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef FILECONNECTION_T3FDD69326E3F1A69226BB3142B47D4DD64443288_H
#define FILECONNECTION_T3FDD69326E3F1A69226BB3142B47D4DD64443288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.FileConnection
struct  FileConnection_t3FDD69326E3F1A69226BB3142B47D4DD64443288  : public ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILECONNECTION_T3FDD69326E3F1A69226BB3142B47D4DD64443288_H
#ifndef HTTPCONNECTION_TAE87854D8312FADAD8A98A3FEFE45A31C82A60C9_H
#define HTTPCONNECTION_TAE87854D8312FADAD8A98A3FEFE45A31C82A60C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Connections.HTTPConnection
struct  HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9  : public ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4
{
public:
	// BestHTTP.Connections.TCPConnector BestHTTP.Connections.HTTPConnection::connector
	TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B * ___connector_9;
	// BestHTTP.Core.IHTTPRequestHandler BestHTTP.Connections.HTTPConnection::requestHandler
	RuntimeObject* ___requestHandler_10;

public:
	inline static int32_t get_offset_of_connector_9() { return static_cast<int32_t>(offsetof(HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9, ___connector_9)); }
	inline TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B * get_connector_9() const { return ___connector_9; }
	inline TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B ** get_address_of_connector_9() { return &___connector_9; }
	inline void set_connector_9(TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B * value)
	{
		___connector_9 = value;
		Il2CppCodeGenWriteBarrier((&___connector_9), value);
	}

	inline static int32_t get_offset_of_requestHandler_10() { return static_cast<int32_t>(offsetof(HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9, ___requestHandler_10)); }
	inline RuntimeObject* get_requestHandler_10() const { return ___requestHandler_10; }
	inline RuntimeObject** get_address_of_requestHandler_10() { return &___requestHandler_10; }
	inline void set_requestHandler_10(RuntimeObject* value)
	{
		___requestHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___requestHandler_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTION_TAE87854D8312FADAD8A98A3FEFE45A31C82A60C9_H
#ifndef COMPRESSFUNC_T38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E_H
#define COMPRESSFUNC_T38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.Decompression.Zlib.DeflateManager_CompressFunc
struct  CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSFUNC_T38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5700 = { sizeof (Extensions_t3BA94BC6AA8F519236CE76ABB51D675D9D819F47), -1, sizeof(Extensions_t3BA94BC6AA8F519236CE76ABB51D675D9D819F47_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5700[1] = 
{
	Extensions_t3BA94BC6AA8F519236CE76ABB51D675D9D819F47_StaticFields::get_offset_of_validIpV4AddressRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5701 = { sizeof (U3CU3Ec__DisplayClass20_0_tF278E802C182085BBD73C29F20E8EFB11BD39D20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5701[1] = 
{
	U3CU3Ec__DisplayClass20_0_tF278E802C182085BBD73C29F20E8EFB11BD39D20::get_offset_of_block_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5702 = { sizeof (U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8), -1, sizeof(U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5702[4] = 
{
	U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields::get_offset_of_U3CU3E9__22_0_1(),
	U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields::get_offset_of_U3CU3E9__26_0_2(),
	U3CU3Ec_t63ABAF05D70C4DCD364040BA0A89772C5E4AA5D8_StaticFields::get_offset_of_U3CU3E9__27_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5703 = { sizeof (ExceptionHelper_t9EC8868CD3B945BEB8DFF883051C49A5A5C4C14A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5704 = { sizeof (HeaderParser_t8FDB3DA1551A3A21DFF16E85B6290B2AED6F6FFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5705 = { sizeof (HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5705[3] = 
{
	HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4::get_offset_of_U3CKeyU3Ek__BackingField_0(),
	HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4::get_offset_of_U3CValueU3Ek__BackingField_1(),
	HeaderValue_t7483492AC97FE9524DDD56A1D48F23886C14EEF4::get_offset_of_U3COptionsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5706 = { sizeof (U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4), -1, sizeof(U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5706[2] = 
{
	U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA16BA987839C415A845108B9F19DCD62E29421E4_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5707 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5708 = { sizeof (HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5708[4] = 
{
	HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4::get_offset_of_rwLock_0(),
	HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4::get_offset_of_Heartbeats_1(),
	HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4::get_offset_of_UpdateArray_2(),
	HeartbeatManager_tDD30DF2D1EBC9252ADEB50A7689E4BB2146A6DF4::get_offset_of_LastUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5709 = { sizeof (KeyValuePairList_tA44BB082F0E5204B4C0611544C79F7ADED926E4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5709[1] = 
{
	KeyValuePairList_tA44BB082F0E5204B4C0611544C79F7ADED926E4D::get_offset_of_U3CValuesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5710 = { sizeof (ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5710[5] = 
{
	ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324::get_offset_of_stream_5(),
	0,
	ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324::get_offset_of_buf_7(),
	ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324::get_offset_of_available_8(),
	ReadOnlyBufferedStream_t214D7BD318E4B654384F31266BAE156AAB983324::get_offset_of_pos_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5711 = { sizeof (StreamList_t358C1789B197633EEF33AEC72AE42E364A55BE0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5711[2] = 
{
	StreamList_t358C1789B197633EEF33AEC72AE42E364A55BE0E::get_offset_of_Streams_5(),
	StreamList_t358C1789B197633EEF33AEC72AE42E364A55BE0E::get_offset_of_CurrentIdx_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5712 = { sizeof (TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5712[4] = 
{
	TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920::get_offset_of_Created_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920::get_offset_of_Interval_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920::get_offset_of_Context_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_tF665DC908A53FF58B28A5956B9726A9FC1AA6920::get_offset_of_OnTimer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5713 = { sizeof (Timer_t1885FA76F05D09EC4ACEBF512FCC4CB89C74D350), -1, sizeof(Timer_t1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5713[1] = 
{
	Timer_t1885FA76F05D09EC4ACEBF512FCC4CB89C74D350_StaticFields::get_offset_of_Timers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5714 = { sizeof (WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5714[3] = 
{
	WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6::get_offset_of__position_5(),
	WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6::get_offset_of_buffer_6(),
	WriteOnlyBufferedStream_t9085FEDBB09B2D242603674F6411E2CCA7B6A7E6::get_offset_of_stream_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5715 = { sizeof (WWWAuthenticateHeaderParser_t244B24CB03CFC347DD40D003A2F1E5013C87A591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5716 = { sizeof (U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3), -1, sizeof(U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5716[2] = 
{
	U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8C8042B670333B5A9DD79B56B78BE494A99F86A3_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5717 = { sizeof (DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E)+ sizeof (RuntimeObject), sizeof(DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E ), 0, 0 };
extern const int32_t g_FieldOffsetTable5717[2] = 
{
	DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E::get_offset_of_Data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecompressedData_t3AD3F697A70A63DB3C0635F0B649C1AD50974D1E::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5718 = { sizeof (GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5718[4] = 
{
	GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C::get_offset_of_decompressorInputStream_0(),
	GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C::get_offset_of_decompressorOutputStream_1(),
	GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C::get_offset_of_decompressorGZipStream_2(),
	GZipDecompressor_tEE54B282EA5B93F6DA8819F0E2C188016C5B865C::get_offset_of_MinLengthToDecompress_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5719 = { sizeof (BlockState_t10334335ECA45B3FECF26DD45FD97E6C56EE3C95)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5719[5] = 
{
	BlockState_t10334335ECA45B3FECF26DD45FD97E6C56EE3C95::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5720 = { sizeof (DeflateFlavor_t904294071FFF1282DC14B3F7B1D1E91318B23363)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5720[4] = 
{
	DeflateFlavor_t904294071FFF1282DC14B3F7B1D1E91318B23363::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5721 = { sizeof (DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E), -1, sizeof(DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5721[74] = 
{
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_MEM_LEVEL_MAX_0(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_MEM_LEVEL_DEFAULT_1(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_DeflateFunction_2(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of__ErrorMessage_3(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_PRESET_DICT_4(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_INIT_STATE_5(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_BUSY_STATE_6(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_FINISH_STATE_7(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_Z_DEFLATED_8(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_STORED_BLOCK_9(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_STATIC_TREES_10(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_DYN_TREES_11(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_Z_BINARY_12(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_Z_ASCII_13(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_Z_UNKNOWN_14(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_Buf_size_15(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_MIN_MATCH_16(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_MAX_MATCH_17(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_MIN_LOOKAHEAD_18(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_HEAP_SIZE_19(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E_StaticFields::get_offset_of_END_BLOCK_20(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of__codec_21(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_status_22(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_pending_23(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_nextPending_24(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_pendingCount_25(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_data_type_26(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_last_flush_27(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_w_size_28(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_w_bits_29(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_w_mask_30(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_window_31(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_window_size_32(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_prev_33(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_head_34(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_ins_h_35(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_hash_size_36(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_hash_bits_37(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_hash_mask_38(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_hash_shift_39(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_block_start_40(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_config_41(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_match_length_42(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_prev_match_43(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_match_available_44(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_strstart_45(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_match_start_46(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_lookahead_47(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_prev_length_48(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_compressionLevel_49(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_compressionStrategy_50(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_dyn_ltree_51(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_dyn_dtree_52(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_bl_tree_53(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_treeLiterals_54(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_treeDistances_55(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_treeBitLengths_56(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_bl_count_57(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_heap_58(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_heap_len_59(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_heap_max_60(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_depth_61(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of__lengthOffset_62(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_lit_bufsize_63(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_last_lit_64(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of__distanceOffset_65(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_opt_len_66(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_static_len_67(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_matches_68(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_last_eob_len_69(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_bi_buf_70(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_bi_valid_71(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of_Rfc1950BytesEmitted_72(),
	DeflateManager_t4DE789CAE8055C2E8D3F94F7C2D081F4DC8BD31E::get_offset_of__WantRfc1950HeaderBytes_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5722 = { sizeof (CompressFunc_t38D3CFF7D0482BC14ADFA778A095A5A59AB83A4E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5723 = { sizeof (Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB), -1, sizeof(Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5723[6] = 
{
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB::get_offset_of_GoodLength_0(),
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB::get_offset_of_MaxLazy_1(),
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB::get_offset_of_NiceLength_2(),
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB::get_offset_of_MaxChainLength_3(),
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB::get_offset_of_Flavor_4(),
	Config_tCD85FC125BF17A25D93946759151A7F3DB9869BB_StaticFields::get_offset_of_Table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5724 = { sizeof (DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5724[3] = 
{
	DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA::get_offset_of__baseStream_5(),
	DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA::get_offset_of__innerStream_6(),
	DeflateStream_t9BA10060EDB89E82509A5A440801A119107AD6CA::get_offset_of__disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5725 = { sizeof (GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7), -1, sizeof(GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5725[10] = 
{
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of_LastModified_5(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__headerByteCount_6(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__baseStream_7(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__disposed_8(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__firstReadDone_9(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__FileName_10(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__Comment_11(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7::get_offset_of__Crc32_12(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7_StaticFields::get_offset_of__unixEpoch_13(),
	GZipStream_tD0851D7DBA946F5E8122600760642CC0BC168BD7_StaticFields::get_offset_of_iso8859dash1_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5726 = { sizeof (InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15), -1, sizeof(InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5726[22] = 
{
	0,
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15_StaticFields::get_offset_of_border_1(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_mode_2(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_left_3(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_table_4(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_index_5(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_blens_6(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_bb_7(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_tb_8(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_codes_9(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_last_10(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of__codec_11(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_bitk_12(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_bitb_13(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_hufts_14(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_window_15(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_end_16(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_readAt_17(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_writeAt_18(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_checkfn_19(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_check_20(),
	InflateBlocks_t54D239004644B230C64198B8574D98A3B865DA15::get_offset_of_inftree_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5727 = { sizeof (InflateBlockMode_t03E84E5B1DD885FE24B28CFD8E2731D71832E1BD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5727[11] = 
{
	InflateBlockMode_t03E84E5B1DD885FE24B28CFD8E2731D71832E1BD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5728 = { sizeof (InternalInflateConstants_t2EBF3160F7661B7094A73B38FEE4C5C11B644A3B), -1, sizeof(InternalInflateConstants_t2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5728[1] = 
{
	InternalInflateConstants_t2EBF3160F7661B7094A73B38FEE4C5C11B644A3B_StaticFields::get_offset_of_InflateMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5729 = { sizeof (InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5729[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_mode_10(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_len_11(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_tree_12(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_tree_index_13(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_need_14(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_lit_15(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_bitsToGet_16(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_dist_17(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_lbits_18(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_dbits_19(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_ltree_20(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_ltree_index_21(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_dtree_22(),
	InflateCodes_t29F253554B2F53C42CA4D43C5F77CA23B5D50FAC::get_offset_of_dtree_index_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5730 = { sizeof (InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD), -1, sizeof(InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5730[12] = 
{
	0,
	0,
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_mode_2(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of__codec_3(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_method_4(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_computedCheck_5(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_expectedCheck_6(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_marker_7(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of__handleRfc1950HeaderBytes_8(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_wbits_9(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD::get_offset_of_blocks_10(),
	InflateManager_tA92DE0E72F973BAD30B1BD0C146AFE558A99C9CD_StaticFields::get_offset_of_mark_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5731 = { sizeof (InflateManagerMode_t65CE847FF7D989A88C3D8D916A6C2E438701063C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5731[15] = 
{
	InflateManagerMode_t65CE847FF7D989A88C3D8D916A6C2E438701063C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5732 = { sizeof (InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6), -1, sizeof(InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5732[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields::get_offset_of_fixed_tl_12(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields::get_offset_of_fixed_td_13(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields::get_offset_of_cplens_14(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields::get_offset_of_cplext_15(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields::get_offset_of_cpdist_16(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6_StaticFields::get_offset_of_cpdext_17(),
	0,
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6::get_offset_of_hn_19(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6::get_offset_of_v_20(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6::get_offset_of_c_21(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6::get_offset_of_r_22(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6::get_offset_of_u_23(),
	InfTree_tC36932870D092D06D6C8AED352BA90AA9CFF84E6::get_offset_of_x_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5733 = { sizeof (FlushType_t427087D15993FCE1D628C73FE8EF53E749CD08B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5733[6] = 
{
	FlushType_t427087D15993FCE1D628C73FE8EF53E749CD08B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5734 = { sizeof (CompressionLevel_t84967AC2C882EA97443AE1BFB4278A7F916C352A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5734[15] = 
{
	CompressionLevel_t84967AC2C882EA97443AE1BFB4278A7F916C352A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5735 = { sizeof (CompressionStrategy_tD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5735[4] = 
{
	CompressionStrategy_tD763D6B922F93AEB53B3C755DC6DBEF1AB2DF3F6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5736 = { sizeof (CompressionMode_t05A631508E1E2CB54FDC5ABAA2493C779E30F802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5736[3] = 
{
	CompressionMode_t05A631508E1E2CB54FDC5ABAA2493C779E30F802::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5737 = { sizeof (ZlibException_t83268FBCA130A193BD463A661A95A84588BC4EB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5738 = { sizeof (SharedUtils_t2627060D8748C0DF33B4EA236BB7DF99B5BE4DD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5739 = { sizeof (InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916), -1, sizeof(InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5739[10] = 
{
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_MAX_BITS_0(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_BL_CODES_1(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_D_CODES_2(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_LITERALS_3(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_LENGTH_CODES_4(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_L_CODES_5(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_MAX_BL_BITS_6(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_REP_3_6_7(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_REPZ_3_10_8(),
	InternalConstants_t1A8E71379CABACD2E899C44214612731EA5A2916_StaticFields::get_offset_of_REPZ_11_138_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5740 = { sizeof (StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0), -1, sizeof(StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5740[10] = 
{
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields::get_offset_of_lengthAndLiteralsTreeCodes_0(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields::get_offset_of_distTreeCodes_1(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields::get_offset_of_Literals_2(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields::get_offset_of_Distances_3(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0_StaticFields::get_offset_of_BitLengths_4(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0::get_offset_of_treeCodes_5(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0::get_offset_of_extraBits_6(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0::get_offset_of_extraBase_7(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0::get_offset_of_elems_8(),
	StaticTree_t3A26627A5945E90DDA8578A7BAC969FE35EF3AB0::get_offset_of_maxLength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5741 = { sizeof (Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023), -1, sizeof(Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5741[2] = 
{
	Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_StaticFields::get_offset_of_BASE_0(),
	Adler_t11AD4EA6FAB0A9DF5A527782F7672119B3E7E023_StaticFields::get_offset_of_NMAX_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5742 = { sizeof (ZlibStreamFlavor_t77BA119D0E78B10D9A22F6A9D5782F89878F2CCF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5742[4] = 
{
	ZlibStreamFlavor_t77BA119D0E78B10D9A22F6A9D5782F89878F2CCF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5743 = { sizeof (ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5743[19] = 
{
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__z_5(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__streamMode_6(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__flushMode_7(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__flavor_8(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__compressionMode_9(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__level_10(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__leaveOpen_11(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__workingBuffer_12(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__bufferSize_13(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of_windowBitsMax_14(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__buf1_15(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__stream_16(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of_Strategy_17(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of_crc_18(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__GzipFileName_19(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__GzipComment_20(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__GzipMtime_21(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of__gzipHeaderByteCount_22(),
	ZlibBaseStream_t188967618B2DFC2C8FBD2161F6857DD5FC7EED38::get_offset_of_nomoreinput_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5744 = { sizeof (StreamMode_t148B1E1604579D2FD6E2842F89B56AE0D3ABBC10)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5744[4] = 
{
	StreamMode_t148B1E1604579D2FD6E2842F89B56AE0D3ABBC10::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5745 = { sizeof (ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5745[15] = 
{
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_InputBuffer_0(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_NextIn_1(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_AvailableBytesIn_2(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_TotalBytesIn_3(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_OutputBuffer_4(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_NextOut_5(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_AvailableBytesOut_6(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_TotalBytesOut_7(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_Message_8(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_dstate_9(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_istate_10(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of__Adler32_11(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_CompressLevel_12(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_WindowBits_13(),
	ZlibCodec_t71C6443857F9F9DB2D19EB730514438EEDBE242A::get_offset_of_Strategy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5746 = { sizeof (ZlibConstants_tC69AFB69F94D17A6DA7D761E8802FE7637D51077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5746[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5747 = { sizeof (ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13), -1, sizeof(ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5747[13] = 
{
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_HEAP_SIZE_0(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_ExtraLengthBits_1(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_ExtraDistanceBits_2(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_extra_blbits_3(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_bl_order_4(),
	0,
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of__dist_code_6(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_LengthCode_7(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_LengthBase_8(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13_StaticFields::get_offset_of_DistanceBase_9(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13::get_offset_of_dyn_tree_10(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13::get_offset_of_max_code_11(),
	ZTree_t67EF0091E8FCAD6DD3642B7233E0AF5991FCFF13::get_offset_of_staticTree_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5748 = { sizeof (CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5748[6] = 
{
	CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC::get_offset_of_dwPolynomial_0(),
	CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC::get_offset_of__TotalBytesRead_1(),
	CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC::get_offset_of_reverseBits_2(),
	CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC::get_offset_of_crc32Table_3(),
	0,
	CRC32_t0D1387BE19FA9B6296EA472D22D9EDB41B81D5AC::get_offset_of__register_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5749 = { sizeof (ConnectionEvents_t9601EEE41D3C1450E1BF068ACEDFED1084B1966C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5749[3] = 
{
	ConnectionEvents_t9601EEE41D3C1450E1BF068ACEDFED1084B1966C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5750 = { sizeof (ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5750[4] = 
{
	ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042::get_offset_of_Source_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042::get_offset_of_Event_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042::get_offset_of_State_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConnectionEventInfo_t1643CAE7EB52BEA644B4430E6B4D663A31E5C042::get_offset_of_ProtocolSupport_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5751 = { sizeof (ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F), -1, sizeof(ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5751[2] = 
{
	ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_StaticFields::get_offset_of_connectionEventQueue_0(),
	ConnectionEventHelper_tC486E9C0ABBA291FE2DBE2718F0E48DB00240F2F_StaticFields::get_offset_of_OnEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5752 = { sizeof (HostProtocolSupport_t5615FF28D20CE674E184ED8B61719B9B7B4FC194)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5752[4] = 
{
	HostProtocolSupport_t5615FF28D20CE674E184ED8B61719B9B7B4FC194::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5753 = { sizeof (HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5753[6] = 
{
	HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59::get_offset_of_U3CHostU3Ek__BackingField_0(),
	HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59::get_offset_of_U3CVariantIdU3Ek__BackingField_1(),
	HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59::get_offset_of_U3CProtocolSupportU3Ek__BackingField_2(),
	HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59::get_offset_of_U3CLastProtocolSupportUpdateU3Ek__BackingField_3(),
	HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59::get_offset_of_Connections_4(),
	HostConnection_tAE2449D2BF83FB6D4F5DFFA1C6C13FBB17985E59::get_offset_of_Queue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5754 = { sizeof (HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21), -1, sizeof(HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5754[5] = 
{
	HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21::get_offset_of_U3CHostU3Ek__BackingField_0(),
	HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21::get_offset_of_Alternates_1(),
	HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21::get_offset_of_hostConnectionVariant_2(),
	HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21_StaticFields::get_offset_of_keyBuilder_3(),
	HostDefinition_t380062F048C1276C64E042501E07CCDE9322BB21_StaticFields::get_offset_of_keyBuilderLock_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5755 = { sizeof (HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC), -1, sizeof(HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5755[5] = 
{
	0,
	HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields::get_offset_of_LibraryPath_1(),
	HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields::get_offset_of_IsSaveAndLoadSupported_2(),
	HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields::get_offset_of_IsLoaded_3(),
	HostManager_t25D0FE84CF7CDF4B41F389ADCB25E3E7415C67BC_StaticFields::get_offset_of_hosts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5756 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5757 = { sizeof (HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83)+ sizeof (RuntimeObject), sizeof(HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5757[2] = 
{
	HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83::get_offset_of_Host_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HostConnectionKey_tA8107B3A146F89B83919F1CB13120EA675438A83::get_offset_of_Connection_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5758 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5759 = { sizeof (PluginEvents_t137DDD8C83EED93E02C51F009423E1DF3869157D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5759[4] = 
{
	PluginEvents_t137DDD8C83EED93E02C51F009423E1DF3869157D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5760 = { sizeof (PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF)+ sizeof (RuntimeObject), sizeof(PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5760[2] = 
{
	PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF::get_offset_of_Event_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PluginEventInfo_t0BB194CED5BE1E47EF336C2513A4C36D9341A1AF::get_offset_of_Payload_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5761 = { sizeof (PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554), -1, sizeof(PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5761[2] = 
{
	PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554_StaticFields::get_offset_of_pluginEvents_0(),
	PluginEventHelper_t4600946ADF75381D11718A87FBAFDA1883035554_StaticFields::get_offset_of_OnEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5762 = { sizeof (AltSvcEventInfo_tC9A7F933E6B23EE5B9646F898AF6F689CA84C210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5762[2] = 
{
	AltSvcEventInfo_tC9A7F933E6B23EE5B9646F898AF6F689CA84C210::get_offset_of_Host_0(),
	AltSvcEventInfo_tC9A7F933E6B23EE5B9646F898AF6F689CA84C210::get_offset_of_Response_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5763 = { sizeof (ProtocolEventInfo_t2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5763[1] = 
{
	ProtocolEventInfo_t2CB5FF956704EA7CB8A7D4AC85ED9F4CFA83A96E::get_offset_of_Source_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5764 = { sizeof (ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79), -1, sizeof(ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5764[3] = 
{
	ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields::get_offset_of_protocolEvents_0(),
	ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields::get_offset_of_ActiveProtocols_1(),
	ProtocolEventHelper_t7D3B87D17A87957F3D3432B6671FA55A2DDE7A79_StaticFields::get_offset_of_OnEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5765 = { sizeof (RequestEvents_t6D8964F57566A9976F63562DE0E52803276DFAA3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5765[8] = 
{
	RequestEvents_t6D8964F57566A9976F63562DE0E52803276DFAA3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5766 = { sizeof (RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5766[7] = 
{
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_SourceRequest_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_Event_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_State_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_Progress_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_ProgressLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_Data_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RequestEventInfo_t2A620ED76110D0FF7B09D86171C961B9524B8EED::get_offset_of_DataLength_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5767 = { sizeof (RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509), -1, sizeof(RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5767[2] = 
{
	RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509_StaticFields::get_offset_of_requestEventQueue_0(),
	RequestEventHelper_tD5E4B7D8F15524DFAD455423114A03BDD32FB509_StaticFields::get_offset_of_OnEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5768 = { sizeof (Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5768[12] = 
{
	0,
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CValueU3Ek__BackingField_2(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CDateU3Ek__BackingField_3(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CLastAccessU3Ek__BackingField_4(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CMaxAgeU3Ek__BackingField_6(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CIsSessionU3Ek__BackingField_7(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CDomainU3Ek__BackingField_8(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CPathU3Ek__BackingField_9(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CIsSecureU3Ek__BackingField_10(),
	Cookie_tA24A98C5AA3B87CC2F2E91CB074F180BE4E2E9FE::get_offset_of_U3CIsHttpOnlyU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5769 = { sizeof (U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D), -1, sizeof(U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5769[2] = 
{
	U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6CCEC5DF99D8ED13856FDF86FC6B2DADB0AFDC9D_StaticFields::get_offset_of_U3CU3E9__61_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5770 = { sizeof (CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1), -1, sizeof(CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5770[9] = 
{
	0,
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_AccessThreshold_1(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_Cookies_2(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_U3CCookieFolderU3Ek__BackingField_3(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_U3CLibraryPathU3Ek__BackingField_4(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_rwLock_5(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of__isSavingSupported_6(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_IsSupportCheckDone_7(),
	CookieJar_t03EBE180AA4439C871514FB3FC3FD0BFDB5FB8F1_StaticFields::get_offset_of_Loaded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5771 = { sizeof (ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5771[9] = 
{
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CServerAddressU3Ek__BackingField_0(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CStateU3Ek__BackingField_1(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CCurrentRequestU3Ek__BackingField_2(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CKeepAliveTimeU3Ek__BackingField_3(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CStartTimeU3Ek__BackingField_4(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CLastProcessedUriU3Ek__BackingField_5(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CLastProcessTimeU3Ek__BackingField_6(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_IsThreaded_7(),
	ConnectionBase_tBAD90BD1D60A66AACEE5A446EB0344942C791CB4::get_offset_of_U3CShutdownTypeU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5772 = { sizeof (KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5772[2] = 
{
	KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920::get_offset_of_U3CTimeOutU3Ek__BackingField_0(),
	KeepAliveHeader_tF37ABCF8589D89BF96060E326A4C9B1789A4B920::get_offset_of_U3CMaxRequestsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5773 = { sizeof (ConnectionHelper_tE1139CB9236241F577AB7C8E9C1268331CE2CDD5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5774 = { sizeof (FileConnection_t3FDD69326E3F1A69226BB3142B47D4DD64443288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5775 = { sizeof (HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5775[3] = 
{
	HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D::get_offset_of__keepAlive_0(),
	HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D::get_offset_of_conn_1(),
	HTTP1Handler_tD2B25124976187B833834C9D67EB678FA9B1622D::get_offset_of_U3CShutdownTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5776 = { sizeof (HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5776[2] = 
{
	HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9::get_offset_of_connector_9(),
	HTTPConnection_tAE87854D8312FADAD8A98A3FEFE45A31C82A60C9::get_offset_of_requestHandler_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5777 = { sizeof (HTTPConnectionStates_t15890FE47E949A7C91BBEBB636FD3B35C0A128CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5777[7] = 
{
	HTTPConnectionStates_t15890FE47E949A7C91BBEBB636FD3B35C0A128CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5778 = { sizeof (SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5778[5] = 
{
	SupportedProtocols_t85E02E40A0871BE0C46EFBCD8FFFF1E589D7DBE2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5779 = { sizeof (HTTPProtocolFactory_t833A71496A51A056FB023081A5114248B2AF1F98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5779[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5780 = { sizeof (TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5780[4] = 
{
	TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B::get_offset_of_U3CNegotiatedProtocolU3Ek__BackingField_0(),
	TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B::get_offset_of_U3CClientU3Ek__BackingField_1(),
	TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B::get_offset_of_U3CStreamU3Ek__BackingField_2(),
	TCPConnector_t9ACB940EBAF5660B87F6B43FE52751F7EB2D779B::get_offset_of_U3CLeaveOpenU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5781 = { sizeof (U3CU3Ec__DisplayClass18_0_t398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5781[1] = 
{
	U3CU3Ec__DisplayClass18_0_t398786CFD1AD2B1534AC6D6A1B2603DF3A7CBFCD::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5782 = { sizeof (BufferHelper_t551451CFF303B6D41F20DBA1F09605ABD7EC6458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5783 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5784 = { sizeof (CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5784[7] = 
{
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_U3CLengthU3Ek__BackingField_0(),
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_frames_2(),
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_currentFrameIdx_3(),
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_data_4(),
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_offset_5(),
	CommonFrameView_t3DFE1C1571D5E801185E4CD01AD49DFCE4759BD7::get_offset_of_maxOffset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5785 = { sizeof (HeaderFrameView_tE6AA500089659CFC19E854DF1051591BCB812532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5786 = { sizeof (DataFrameView_t8CE41B4E17D7EF6814BB11D42A6EADD4C8D1EA09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5787 = { sizeof (FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5787[1] = 
{
	FramesAsStreamView_tCBCE022A234E147E1ECD422DC1B01727659E5957::get_offset_of_view_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5788 = { sizeof (HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5), -1, sizeof(HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5788[6] = 
{
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5_StaticFields::get_offset_of_StaticTableValues_0(),
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5_StaticFields::get_offset_of_StaticTable_1(),
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5::get_offset_of_U3CDynamicTableSizeU3Ek__BackingField_2(),
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5::get_offset_of__maxDynamicTableSize_3(),
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5::get_offset_of_DynamicTable_4(),
	HeaderTable_t363CC8C859928DB856863FE5A23D54E340A9D8D5::get_offset_of_settingsRegistry_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5789 = { sizeof (HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5789[3] = 
{
	HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9::get_offset_of_settingsRegistry_0(),
	HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9::get_offset_of_requestTable_1(),
	HPACKEncoder_t8A88034CD75E2978D0B0D3580C9DFEABAE3EF2E9::get_offset_of_responseTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5790 = { sizeof (U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5790[2] = 
{
	U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass4_0_t9BC2DB6173FFA5F901538FB561B3B5D769BD6FE1::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5791 = { sizeof (U3CU3Ec__DisplayClass4_1_t9732EC3123084A31351DB2F73598B0053E6F7AA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5791[2] = 
{
	U3CU3Ec__DisplayClass4_1_t9732EC3123084A31351DB2F73598B0053E6F7AA7::get_offset_of_bufferStream_0(),
	U3CU3Ec__DisplayClass4_1_t9732EC3123084A31351DB2F73598B0053E6F7AA7::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5792 = { sizeof (HTTP2ErrorCodes_t8E50DD699F02848CEEC314A10BF7DB159CB64757)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5792[15] = 
{
	HTTP2ErrorCodes_t8E50DD699F02848CEEC314A10BF7DB159CB64757::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5793 = { sizeof (HTTP2FrameHelper_tC2BF6C90BD0E7A12FE22B855037969293E63498F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5794 = { sizeof (HTTP2FrameTypes_t89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5794[12] = 
{
	HTTP2FrameTypes_t89FDF2C58A08EB43CE4A419B3F1471D6DC72AFE4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5795 = { sizeof (HTTP2DataFlags_tD2254934552888DA529F7B6906FB98C9231F482F)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5795[4] = 
{
	HTTP2DataFlags_tD2254934552888DA529F7B6906FB98C9231F482F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5796 = { sizeof (HTTP2HeadersFlags_t6080E811301BB24A2029861029A336B69F008063)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5796[6] = 
{
	HTTP2HeadersFlags_t6080E811301BB24A2029861029A336B69F008063::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5797 = { sizeof (HTTP2SettingsFlags_tD34C407BE277F6BEE53FEC149B3250B6F48ADE5F)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5797[3] = 
{
	HTTP2SettingsFlags_tD34C407BE277F6BEE53FEC149B3250B6F48ADE5F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5798 = { sizeof (HTTP2PushPromiseFlags_t8C70B5A01735940F53941761683DF443DCF3FBE8)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5798[4] = 
{
	HTTP2PushPromiseFlags_t8C70B5A01735940F53941761683DF443DCF3FBE8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5799 = { sizeof (HTTP2PingFlags_t8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5799[3] = 
{
	HTTP2PingFlags_t8DAC90F20DC6B5ED2B4C2075B7882B940BC2C154::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
