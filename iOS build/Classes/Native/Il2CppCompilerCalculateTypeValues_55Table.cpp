﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString
struct Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence
struct Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser
struct Asn1SequenceParser_t77615613D1D55866EDAB2563A8321E678C0FF76D;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObjectParser
struct Asn1TaggedObjectParser_tB061059DC6FD6F2C0E5B595081275C4C2A818AD6;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attributes
struct Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo
struct ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser
struct ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo
struct EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData
struct EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber
struct IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier
struct KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorIdentifierOrKey
struct OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo
struct OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute
struct OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientIdentifier
struct RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientKeyIdentifier
struct RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampTokenEvidence
struct TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest
struct CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate
struct CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.Controls
struct Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue
struct EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.OptionalValidity
struct OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue
struct PKMacValue_t2354860B8647226235395857CE076879CE35713C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKeyInput
struct PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.ProofOfPossession
struct ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters
struct Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString
struct DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean
struct DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime
struct DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String
struct DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtcTime
struct DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String
struct DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlIdentifier
struct CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlListID
struct CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspIdentifier
struct OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspListID
struct OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash
struct OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHashAlgAndValue
struct OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevRefs
struct OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevVals
struct OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyId
struct SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IAsn1Convertible
struct IAsn1Convertible_tD051F8D281E048429853C46F73738612D7859F7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID
struct ResponderID_t26B99AFE9E4E05948990F461880598D304592346;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo
struct PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString
struct DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate
struct AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName
struct GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial
struct IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo
struct SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time
struct Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions
struct X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name
struct X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters
struct X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.String
struct String_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#define ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable
struct  Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1ENCODABLE_TE2DE940D11501485013A91380763A719FA1D38BB_H
#ifndef ATTRIBUTETABLE_TEC92A1C170225F681A9CAFD570D69CC94FEF74E0_H
#define ATTRIBUTETABLE_TEC92A1C170225F681A9CAFD570D69CC94FEF74E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable
struct  AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AttributeTable::attributes
	RuntimeObject* ___attributes_0;

public:
	inline static int32_t get_offset_of_attributes_0() { return static_cast<int32_t>(offsetof(AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0, ___attributes_0)); }
	inline RuntimeObject* get_attributes_0() const { return ___attributes_0; }
	inline RuntimeObject** get_address_of_attributes_0() { return &___attributes_0; }
	inline void set_attributes_0(RuntimeObject* value)
	{
		___attributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETABLE_TEC92A1C170225F681A9CAFD570D69CC94FEF74E0_H
#ifndef AUTHENVELOPEDDATAPARSER_TB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0_H
#define AUTHENVELOPEDDATAPARSER_TB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedDataParser
struct  AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedDataParser::seq
	RuntimeObject* ___seq_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedDataParser::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IAsn1Convertible BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedDataParser::nextObject
	RuntimeObject* ___nextObject_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedDataParser::originatorInfoCalled
	bool ___originatorInfoCalled_3;

public:
	inline static int32_t get_offset_of_seq_0() { return static_cast<int32_t>(offsetof(AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0, ___seq_0)); }
	inline RuntimeObject* get_seq_0() const { return ___seq_0; }
	inline RuntimeObject** get_address_of_seq_0() { return &___seq_0; }
	inline void set_seq_0(RuntimeObject* value)
	{
		___seq_0 = value;
		Il2CppCodeGenWriteBarrier((&___seq_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0, ___version_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_1() const { return ___version_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_1 = value;
		Il2CppCodeGenWriteBarrier((&___version_1), value);
	}

	inline static int32_t get_offset_of_nextObject_2() { return static_cast<int32_t>(offsetof(AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0, ___nextObject_2)); }
	inline RuntimeObject* get_nextObject_2() const { return ___nextObject_2; }
	inline RuntimeObject** get_address_of_nextObject_2() { return &___nextObject_2; }
	inline void set_nextObject_2(RuntimeObject* value)
	{
		___nextObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextObject_2), value);
	}

	inline static int32_t get_offset_of_originatorInfoCalled_3() { return static_cast<int32_t>(offsetof(AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0, ___originatorInfoCalled_3)); }
	inline bool get_originatorInfoCalled_3() const { return ___originatorInfoCalled_3; }
	inline bool* get_address_of_originatorInfoCalled_3() { return &___originatorInfoCalled_3; }
	inline void set_originatorInfoCalled_3(bool value)
	{
		___originatorInfoCalled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENVELOPEDDATAPARSER_TB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0_H
#ifndef AUTHENTICATEDDATAPARSER_TAC489D0114431C09BAC377DF6BE0FEDF2462D80A_H
#define AUTHENTICATEDDATAPARSER_TAC489D0114431C09BAC377DF6BE0FEDF2462D80A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser
struct  AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser::seq
	RuntimeObject* ___seq_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IAsn1Convertible BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser::nextObject
	RuntimeObject* ___nextObject_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedDataParser::originatorInfoCalled
	bool ___originatorInfoCalled_3;

public:
	inline static int32_t get_offset_of_seq_0() { return static_cast<int32_t>(offsetof(AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A, ___seq_0)); }
	inline RuntimeObject* get_seq_0() const { return ___seq_0; }
	inline RuntimeObject** get_address_of_seq_0() { return &___seq_0; }
	inline void set_seq_0(RuntimeObject* value)
	{
		___seq_0 = value;
		Il2CppCodeGenWriteBarrier((&___seq_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A, ___version_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_1() const { return ___version_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_1 = value;
		Il2CppCodeGenWriteBarrier((&___version_1), value);
	}

	inline static int32_t get_offset_of_nextObject_2() { return static_cast<int32_t>(offsetof(AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A, ___nextObject_2)); }
	inline RuntimeObject* get_nextObject_2() const { return ___nextObject_2; }
	inline RuntimeObject** get_address_of_nextObject_2() { return &___nextObject_2; }
	inline void set_nextObject_2(RuntimeObject* value)
	{
		___nextObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextObject_2), value);
	}

	inline static int32_t get_offset_of_originatorInfoCalled_3() { return static_cast<int32_t>(offsetof(AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A, ___originatorInfoCalled_3)); }
	inline bool get_originatorInfoCalled_3() const { return ___originatorInfoCalled_3; }
	inline bool* get_address_of_originatorInfoCalled_3() { return &___originatorInfoCalled_3; }
	inline void set_originatorInfoCalled_3(bool value)
	{
		___originatorInfoCalled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDDATAPARSER_TAC489D0114431C09BAC377DF6BE0FEDF2462D80A_H
#ifndef CMSATTRIBUTES_TFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_H
#define CMSATTRIBUTES_TFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsAttributes
struct  CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29  : public RuntimeObject
{
public:

public:
};

struct CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsAttributes::ContentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ContentType_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsAttributes::MessageDigest
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___MessageDigest_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsAttributes::SigningTime
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SigningTime_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsAttributes::CounterSignature
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CounterSignature_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsAttributes::ContentHint
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ContentHint_4;

public:
	inline static int32_t get_offset_of_ContentType_0() { return static_cast<int32_t>(offsetof(CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields, ___ContentType_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ContentType_0() const { return ___ContentType_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ContentType_0() { return &___ContentType_0; }
	inline void set_ContentType_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ContentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ContentType_0), value);
	}

	inline static int32_t get_offset_of_MessageDigest_1() { return static_cast<int32_t>(offsetof(CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields, ___MessageDigest_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_MessageDigest_1() const { return ___MessageDigest_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_MessageDigest_1() { return &___MessageDigest_1; }
	inline void set_MessageDigest_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___MessageDigest_1 = value;
		Il2CppCodeGenWriteBarrier((&___MessageDigest_1), value);
	}

	inline static int32_t get_offset_of_SigningTime_2() { return static_cast<int32_t>(offsetof(CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields, ___SigningTime_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SigningTime_2() const { return ___SigningTime_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SigningTime_2() { return &___SigningTime_2; }
	inline void set_SigningTime_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SigningTime_2 = value;
		Il2CppCodeGenWriteBarrier((&___SigningTime_2), value);
	}

	inline static int32_t get_offset_of_CounterSignature_3() { return static_cast<int32_t>(offsetof(CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields, ___CounterSignature_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CounterSignature_3() const { return ___CounterSignature_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CounterSignature_3() { return &___CounterSignature_3; }
	inline void set_CounterSignature_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CounterSignature_3 = value;
		Il2CppCodeGenWriteBarrier((&___CounterSignature_3), value);
	}

	inline static int32_t get_offset_of_ContentHint_4() { return static_cast<int32_t>(offsetof(CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields, ___ContentHint_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ContentHint_4() const { return ___ContentHint_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ContentHint_4() { return &___ContentHint_4; }
	inline void set_ContentHint_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ContentHint_4 = value;
		Il2CppCodeGenWriteBarrier((&___ContentHint_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSATTRIBUTES_TFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_H
#ifndef CMSOBJECTIDENTIFIERS_T75C947E70271646CF2A87A78EE9D98682AD50FC1_H
#define CMSOBJECTIDENTIFIERS_T75C947E70271646CF2A87A78EE9D98682AD50FC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers
struct  CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1  : public RuntimeObject
{
public:

public:
};

struct CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::Data
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Data_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::SignedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SignedData_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::EnvelopedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EnvelopedData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::SignedAndEnvelopedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SignedAndEnvelopedData_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::DigestedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DigestedData_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::EncryptedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EncryptedData_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::AuthenticatedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___AuthenticatedData_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::CompressedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CompressedData_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::AuthEnvelopedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___AuthEnvelopedData_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::timestampedData
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___timestampedData_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::id_ri
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_ri_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::id_ri_ocsp_response
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_ri_ocsp_response_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CmsObjectIdentifiers::id_ri_scvp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_ri_scvp_12;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___Data_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Data_0() const { return ___Data_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___Data_0), value);
	}

	inline static int32_t get_offset_of_SignedData_1() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___SignedData_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SignedData_1() const { return ___SignedData_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SignedData_1() { return &___SignedData_1; }
	inline void set_SignedData_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SignedData_1 = value;
		Il2CppCodeGenWriteBarrier((&___SignedData_1), value);
	}

	inline static int32_t get_offset_of_EnvelopedData_2() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___EnvelopedData_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EnvelopedData_2() const { return ___EnvelopedData_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EnvelopedData_2() { return &___EnvelopedData_2; }
	inline void set_EnvelopedData_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EnvelopedData_2 = value;
		Il2CppCodeGenWriteBarrier((&___EnvelopedData_2), value);
	}

	inline static int32_t get_offset_of_SignedAndEnvelopedData_3() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___SignedAndEnvelopedData_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SignedAndEnvelopedData_3() const { return ___SignedAndEnvelopedData_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SignedAndEnvelopedData_3() { return &___SignedAndEnvelopedData_3; }
	inline void set_SignedAndEnvelopedData_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SignedAndEnvelopedData_3 = value;
		Il2CppCodeGenWriteBarrier((&___SignedAndEnvelopedData_3), value);
	}

	inline static int32_t get_offset_of_DigestedData_4() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___DigestedData_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DigestedData_4() const { return ___DigestedData_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DigestedData_4() { return &___DigestedData_4; }
	inline void set_DigestedData_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DigestedData_4 = value;
		Il2CppCodeGenWriteBarrier((&___DigestedData_4), value);
	}

	inline static int32_t get_offset_of_EncryptedData_5() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___EncryptedData_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EncryptedData_5() const { return ___EncryptedData_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EncryptedData_5() { return &___EncryptedData_5; }
	inline void set_EncryptedData_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EncryptedData_5 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptedData_5), value);
	}

	inline static int32_t get_offset_of_AuthenticatedData_6() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___AuthenticatedData_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_AuthenticatedData_6() const { return ___AuthenticatedData_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_AuthenticatedData_6() { return &___AuthenticatedData_6; }
	inline void set_AuthenticatedData_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___AuthenticatedData_6 = value;
		Il2CppCodeGenWriteBarrier((&___AuthenticatedData_6), value);
	}

	inline static int32_t get_offset_of_CompressedData_7() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___CompressedData_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CompressedData_7() const { return ___CompressedData_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CompressedData_7() { return &___CompressedData_7; }
	inline void set_CompressedData_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CompressedData_7 = value;
		Il2CppCodeGenWriteBarrier((&___CompressedData_7), value);
	}

	inline static int32_t get_offset_of_AuthEnvelopedData_8() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___AuthEnvelopedData_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_AuthEnvelopedData_8() const { return ___AuthEnvelopedData_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_AuthEnvelopedData_8() { return &___AuthEnvelopedData_8; }
	inline void set_AuthEnvelopedData_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___AuthEnvelopedData_8 = value;
		Il2CppCodeGenWriteBarrier((&___AuthEnvelopedData_8), value);
	}

	inline static int32_t get_offset_of_timestampedData_9() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___timestampedData_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_timestampedData_9() const { return ___timestampedData_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_timestampedData_9() { return &___timestampedData_9; }
	inline void set_timestampedData_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___timestampedData_9 = value;
		Il2CppCodeGenWriteBarrier((&___timestampedData_9), value);
	}

	inline static int32_t get_offset_of_id_ri_10() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___id_ri_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_ri_10() const { return ___id_ri_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_ri_10() { return &___id_ri_10; }
	inline void set_id_ri_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_ri_10 = value;
		Il2CppCodeGenWriteBarrier((&___id_ri_10), value);
	}

	inline static int32_t get_offset_of_id_ri_ocsp_response_11() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___id_ri_ocsp_response_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_ri_ocsp_response_11() const { return ___id_ri_ocsp_response_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_ri_ocsp_response_11() { return &___id_ri_ocsp_response_11; }
	inline void set_id_ri_ocsp_response_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_ri_ocsp_response_11 = value;
		Il2CppCodeGenWriteBarrier((&___id_ri_ocsp_response_11), value);
	}

	inline static int32_t get_offset_of_id_ri_scvp_12() { return static_cast<int32_t>(offsetof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields, ___id_ri_scvp_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_ri_scvp_12() const { return ___id_ri_scvp_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_ri_scvp_12() { return &___id_ri_scvp_12; }
	inline void set_id_ri_scvp_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_ri_scvp_12 = value;
		Il2CppCodeGenWriteBarrier((&___id_ri_scvp_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMSOBJECTIDENTIFIERS_T75C947E70271646CF2A87A78EE9D98682AD50FC1_H
#ifndef COMPRESSEDDATAPARSER_T859B2FBA14F16FE0C2EA1313CACDD97B63837ACB_H
#define COMPRESSEDDATAPARSER_T859B2FBA14F16FE0C2EA1313CACDD97B63837ACB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedDataParser
struct  CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedDataParser::_version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ____version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedDataParser::_compressionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ____compressionAlgorithm_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedDataParser::_encapContentInfo
	ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * ____encapContentInfo_2;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB, ____version_0)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get__version_0() const { return ____version_0; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((&____version_0), value);
	}

	inline static int32_t get_offset_of__compressionAlgorithm_1() { return static_cast<int32_t>(offsetof(CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB, ____compressionAlgorithm_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get__compressionAlgorithm_1() const { return ____compressionAlgorithm_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of__compressionAlgorithm_1() { return &____compressionAlgorithm_1; }
	inline void set__compressionAlgorithm_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		____compressionAlgorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____compressionAlgorithm_1), value);
	}

	inline static int32_t get_offset_of__encapContentInfo_2() { return static_cast<int32_t>(offsetof(CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB, ____encapContentInfo_2)); }
	inline ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * get__encapContentInfo_2() const { return ____encapContentInfo_2; }
	inline ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC ** get_address_of__encapContentInfo_2() { return &____encapContentInfo_2; }
	inline void set__encapContentInfo_2(ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC * value)
	{
		____encapContentInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&____encapContentInfo_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSEDDATAPARSER_T859B2FBA14F16FE0C2EA1313CACDD97B63837ACB_H
#ifndef CONTENTINFOPARSER_TF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC_H
#define CONTENTINFOPARSER_TF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser
struct  ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser::contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___contentType_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObjectParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfoParser::content
	RuntimeObject* ___content_1;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC, ___contentType_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_contentType_0() const { return ___contentType_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___contentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTINFOPARSER_TF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC_H
#ifndef ENCRYPTEDCONTENTINFOPARSER_T0F83DCE4C053E418405C23DBE874853332A740C7_H
#define ENCRYPTEDCONTENTINFOPARSER_T0F83DCE4C053E418405C23DBE874853332A740C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfoParser
struct  EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfoParser::_contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ____contentType_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfoParser::_contentEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ____contentEncryptionAlgorithm_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1TaggedObjectParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfoParser::_encryptedContent
	RuntimeObject* ____encryptedContent_2;

public:
	inline static int32_t get_offset_of__contentType_0() { return static_cast<int32_t>(offsetof(EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7, ____contentType_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get__contentType_0() const { return ____contentType_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of__contentType_0() { return &____contentType_0; }
	inline void set__contentType_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		____contentType_0 = value;
		Il2CppCodeGenWriteBarrier((&____contentType_0), value);
	}

	inline static int32_t get_offset_of__contentEncryptionAlgorithm_1() { return static_cast<int32_t>(offsetof(EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7, ____contentEncryptionAlgorithm_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get__contentEncryptionAlgorithm_1() const { return ____contentEncryptionAlgorithm_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of__contentEncryptionAlgorithm_1() { return &____contentEncryptionAlgorithm_1; }
	inline void set__contentEncryptionAlgorithm_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		____contentEncryptionAlgorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____contentEncryptionAlgorithm_1), value);
	}

	inline static int32_t get_offset_of__encryptedContent_2() { return static_cast<int32_t>(offsetof(EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7, ____encryptedContent_2)); }
	inline RuntimeObject* get__encryptedContent_2() const { return ____encryptedContent_2; }
	inline RuntimeObject** get_address_of__encryptedContent_2() { return &____encryptedContent_2; }
	inline void set__encryptedContent_2(RuntimeObject* value)
	{
		____encryptedContent_2 = value;
		Il2CppCodeGenWriteBarrier((&____encryptedContent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDCONTENTINFOPARSER_T0F83DCE4C053E418405C23DBE874853332A740C7_H
#ifndef ENVELOPEDDATAPARSER_TE83D89D81BB3074AEDF43F52716E9E883114109F_H
#define ENVELOPEDDATAPARSER_TE83D89D81BB3074AEDF43F52716E9E883114109F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser
struct  EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1SequenceParser BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser::_seq
	RuntimeObject* ____seq_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser::_version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ____version_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.IAsn1Convertible BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser::_nextObject
	RuntimeObject* ____nextObject_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedDataParser::_originatorInfoCalled
	bool ____originatorInfoCalled_3;

public:
	inline static int32_t get_offset_of__seq_0() { return static_cast<int32_t>(offsetof(EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F, ____seq_0)); }
	inline RuntimeObject* get__seq_0() const { return ____seq_0; }
	inline RuntimeObject** get_address_of__seq_0() { return &____seq_0; }
	inline void set__seq_0(RuntimeObject* value)
	{
		____seq_0 = value;
		Il2CppCodeGenWriteBarrier((&____seq_0), value);
	}

	inline static int32_t get_offset_of__version_1() { return static_cast<int32_t>(offsetof(EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F, ____version_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get__version_1() const { return ____version_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of__version_1() { return &____version_1; }
	inline void set__version_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		____version_1 = value;
		Il2CppCodeGenWriteBarrier((&____version_1), value);
	}

	inline static int32_t get_offset_of__nextObject_2() { return static_cast<int32_t>(offsetof(EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F, ____nextObject_2)); }
	inline RuntimeObject* get__nextObject_2() const { return ____nextObject_2; }
	inline RuntimeObject** get_address_of__nextObject_2() { return &____nextObject_2; }
	inline void set__nextObject_2(RuntimeObject* value)
	{
		____nextObject_2 = value;
		Il2CppCodeGenWriteBarrier((&____nextObject_2), value);
	}

	inline static int32_t get_offset_of__originatorInfoCalled_3() { return static_cast<int32_t>(offsetof(EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F, ____originatorInfoCalled_3)); }
	inline bool get__originatorInfoCalled_3() const { return ____originatorInfoCalled_3; }
	inline bool* get_address_of__originatorInfoCalled_3() { return &____originatorInfoCalled_3; }
	inline void set__originatorInfoCalled_3(bool value)
	{
		____originatorInfoCalled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVELOPEDDATAPARSER_TE83D89D81BB3074AEDF43F52716E9E883114109F_H
#ifndef CERTTEMPLATEBUILDER_T4C84B0E1A9854C94E36CA67B764AA629E5E1B363_H
#define CERTTEMPLATEBUILDER_T4C84B0E1A9854C94E36CA67B764AA629E5E1B363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder
struct  CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::signingAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signingAlg_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.OptionalValidity BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::validity
	OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 * ___validity_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::publicKey
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___publicKey_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::issuerUID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUID_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::subjectUID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___subjectUID_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplateBuilder::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_9;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___version_0)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_0() const { return ___version_0; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_serialNumber_1() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___serialNumber_1)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_1() const { return ___serialNumber_1; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_1() { return &___serialNumber_1; }
	inline void set_serialNumber_1(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_1 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_1), value);
	}

	inline static int32_t get_offset_of_signingAlg_2() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___signingAlg_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signingAlg_2() const { return ___signingAlg_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signingAlg_2() { return &___signingAlg_2; }
	inline void set_signingAlg_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signingAlg_2 = value;
		Il2CppCodeGenWriteBarrier((&___signingAlg_2), value);
	}

	inline static int32_t get_offset_of_issuer_3() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___issuer_3)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_3() const { return ___issuer_3; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_3() { return &___issuer_3; }
	inline void set_issuer_3(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_3), value);
	}

	inline static int32_t get_offset_of_validity_4() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___validity_4)); }
	inline OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 * get_validity_4() const { return ___validity_4; }
	inline OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 ** get_address_of_validity_4() { return &___validity_4; }
	inline void set_validity_4(OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 * value)
	{
		___validity_4 = value;
		Il2CppCodeGenWriteBarrier((&___validity_4), value);
	}

	inline static int32_t get_offset_of_subject_5() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___subject_5)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_5() const { return ___subject_5; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_5() { return &___subject_5; }
	inline void set_subject_5(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_5 = value;
		Il2CppCodeGenWriteBarrier((&___subject_5), value);
	}

	inline static int32_t get_offset_of_publicKey_6() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___publicKey_6)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_publicKey_6() const { return ___publicKey_6; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_publicKey_6() { return &___publicKey_6; }
	inline void set_publicKey_6(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___publicKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_6), value);
	}

	inline static int32_t get_offset_of_issuerUID_7() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___issuerUID_7)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUID_7() const { return ___issuerUID_7; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUID_7() { return &___issuerUID_7; }
	inline void set_issuerUID_7(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUID_7 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUID_7), value);
	}

	inline static int32_t get_offset_of_subjectUID_8() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___subjectUID_8)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_subjectUID_8() const { return ___subjectUID_8; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_subjectUID_8() { return &___subjectUID_8; }
	inline void set_subjectUID_8(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___subjectUID_8 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUID_8), value);
	}

	inline static int32_t get_offset_of_extensions_9() { return static_cast<int32_t>(offsetof(CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363, ___extensions_9)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_9() const { return ___extensions_9; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_9() { return &___extensions_9; }
	inline void set_extensions_9(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_9 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTTEMPLATEBUILDER_T4C84B0E1A9854C94E36CA67B764AA629E5E1B363_H
#ifndef CRMFOBJECTIDENTIFIERS_TC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_H
#define CRMFOBJECTIDENTIFIERS_TC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers
struct  CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA  : public RuntimeObject
{
public:

public:
};

struct CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_pkix
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_pkix_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_pkip
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_pkip_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_regCtrl
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_regCtrl_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_regCtrl_regToken
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_regCtrl_regToken_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_regCtrl_authenticator
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_regCtrl_authenticator_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_regCtrl_pkiPublicationInfo
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_regCtrl_pkiPublicationInfo_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_regCtrl_pkiArchiveOptions
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_regCtrl_pkiArchiveOptions_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CrmfObjectIdentifiers::id_ct_encKeyWithID
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_ct_encKeyWithID_7;

public:
	inline static int32_t get_offset_of_id_pkix_0() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_pkix_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_pkix_0() const { return ___id_pkix_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_pkix_0() { return &___id_pkix_0; }
	inline void set_id_pkix_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_pkix_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_pkix_0), value);
	}

	inline static int32_t get_offset_of_id_pkip_1() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_pkip_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_pkip_1() const { return ___id_pkip_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_pkip_1() { return &___id_pkip_1; }
	inline void set_id_pkip_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_pkip_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_pkip_1), value);
	}

	inline static int32_t get_offset_of_id_regCtrl_2() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_regCtrl_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_regCtrl_2() const { return ___id_regCtrl_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_regCtrl_2() { return &___id_regCtrl_2; }
	inline void set_id_regCtrl_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_regCtrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_regCtrl_2), value);
	}

	inline static int32_t get_offset_of_id_regCtrl_regToken_3() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_regCtrl_regToken_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_regCtrl_regToken_3() const { return ___id_regCtrl_regToken_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_regCtrl_regToken_3() { return &___id_regCtrl_regToken_3; }
	inline void set_id_regCtrl_regToken_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_regCtrl_regToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_regCtrl_regToken_3), value);
	}

	inline static int32_t get_offset_of_id_regCtrl_authenticator_4() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_regCtrl_authenticator_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_regCtrl_authenticator_4() const { return ___id_regCtrl_authenticator_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_regCtrl_authenticator_4() { return &___id_regCtrl_authenticator_4; }
	inline void set_id_regCtrl_authenticator_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_regCtrl_authenticator_4 = value;
		Il2CppCodeGenWriteBarrier((&___id_regCtrl_authenticator_4), value);
	}

	inline static int32_t get_offset_of_id_regCtrl_pkiPublicationInfo_5() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_regCtrl_pkiPublicationInfo_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_regCtrl_pkiPublicationInfo_5() const { return ___id_regCtrl_pkiPublicationInfo_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_regCtrl_pkiPublicationInfo_5() { return &___id_regCtrl_pkiPublicationInfo_5; }
	inline void set_id_regCtrl_pkiPublicationInfo_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_regCtrl_pkiPublicationInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___id_regCtrl_pkiPublicationInfo_5), value);
	}

	inline static int32_t get_offset_of_id_regCtrl_pkiArchiveOptions_6() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_regCtrl_pkiArchiveOptions_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_regCtrl_pkiArchiveOptions_6() const { return ___id_regCtrl_pkiArchiveOptions_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_regCtrl_pkiArchiveOptions_6() { return &___id_regCtrl_pkiArchiveOptions_6; }
	inline void set_id_regCtrl_pkiArchiveOptions_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_regCtrl_pkiArchiveOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_regCtrl_pkiArchiveOptions_6), value);
	}

	inline static int32_t get_offset_of_id_ct_encKeyWithID_7() { return static_cast<int32_t>(offsetof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields, ___id_ct_encKeyWithID_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_ct_encKeyWithID_7() const { return ___id_ct_encKeyWithID_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_ct_encKeyWithID_7() { return &___id_ct_encKeyWithID_7; }
	inline void set_id_ct_encKeyWithID_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_ct_encKeyWithID_7 = value;
		Il2CppCodeGenWriteBarrier((&___id_ct_encKeyWithID_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRMFOBJECTIDENTIFIERS_TC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_H
#ifndef CRYPTOPROOBJECTIDENTIFIERS_TF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_H
#define CRYPTOPROOBJECTIDENTIFIERS_TF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers
struct  CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB  : public RuntimeObject
{
public:

public:
};

struct CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3411_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411Hmac
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3411Hmac_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR28147Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR28147Cbc_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::ID_Gost28147_89_CryptoPro_A_ParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ID_Gost28147_89_CryptoPro_A_ParamSet_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x2001_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411x94WithGostR3410x94
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3411x94WithGostR3410x94_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411x94WithGostR3410x2001
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3411x94WithGostR3410x2001_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3411x94CryptoProParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3411x94CryptoProParamSet_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProA_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProB_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProC_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProD
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProD_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProXchA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProXchA_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProXchB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProXchB_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x94CryptoProXchC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x94CryptoProXchC_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x2001CryptoProA_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x2001CryptoProB_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x2001CryptoProC_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProXchA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x2001CryptoProXchA_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostR3410x2001CryptoProXchB
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostR3410x2001CryptoProXchB_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostElSgDH3410Default
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostElSgDH3410Default_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers::GostElSgDH3410x1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GostElSgDH3410x1_23;

public:
	inline static int32_t get_offset_of_GostR3411_1() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3411_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3411_1() const { return ___GostR3411_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3411_1() { return &___GostR3411_1; }
	inline void set_GostR3411_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3411_1 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411_1), value);
	}

	inline static int32_t get_offset_of_GostR3411Hmac_2() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3411Hmac_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3411Hmac_2() const { return ___GostR3411Hmac_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3411Hmac_2() { return &___GostR3411Hmac_2; }
	inline void set_GostR3411Hmac_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3411Hmac_2 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411Hmac_2), value);
	}

	inline static int32_t get_offset_of_GostR28147Cbc_3() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR28147Cbc_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR28147Cbc_3() const { return ___GostR28147Cbc_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR28147Cbc_3() { return &___GostR28147Cbc_3; }
	inline void set_GostR28147Cbc_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR28147Cbc_3 = value;
		Il2CppCodeGenWriteBarrier((&___GostR28147Cbc_3), value);
	}

	inline static int32_t get_offset_of_ID_Gost28147_89_CryptoPro_A_ParamSet_4() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___ID_Gost28147_89_CryptoPro_A_ParamSet_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ID_Gost28147_89_CryptoPro_A_ParamSet_4() const { return ___ID_Gost28147_89_CryptoPro_A_ParamSet_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ID_Gost28147_89_CryptoPro_A_ParamSet_4() { return &___ID_Gost28147_89_CryptoPro_A_ParamSet_4; }
	inline void set_ID_Gost28147_89_CryptoPro_A_ParamSet_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ID_Gost28147_89_CryptoPro_A_ParamSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___ID_Gost28147_89_CryptoPro_A_ParamSet_4), value);
	}

	inline static int32_t get_offset_of_GostR3410x94_5() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94_5() const { return ___GostR3410x94_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94_5() { return &___GostR3410x94_5; }
	inline void set_GostR3410x94_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94_5 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94_5), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001_6() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x2001_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x2001_6() const { return ___GostR3410x2001_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x2001_6() { return &___GostR3410x2001_6; }
	inline void set_GostR3410x2001_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x2001_6 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001_6), value);
	}

	inline static int32_t get_offset_of_GostR3411x94WithGostR3410x94_7() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3411x94WithGostR3410x94_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3411x94WithGostR3410x94_7() const { return ___GostR3411x94WithGostR3410x94_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3411x94WithGostR3410x94_7() { return &___GostR3411x94WithGostR3410x94_7; }
	inline void set_GostR3411x94WithGostR3410x94_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3411x94WithGostR3410x94_7 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411x94WithGostR3410x94_7), value);
	}

	inline static int32_t get_offset_of_GostR3411x94WithGostR3410x2001_8() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3411x94WithGostR3410x2001_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3411x94WithGostR3410x2001_8() const { return ___GostR3411x94WithGostR3410x2001_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3411x94WithGostR3410x2001_8() { return &___GostR3411x94WithGostR3410x2001_8; }
	inline void set_GostR3411x94WithGostR3410x2001_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3411x94WithGostR3410x2001_8 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411x94WithGostR3410x2001_8), value);
	}

	inline static int32_t get_offset_of_GostR3411x94CryptoProParamSet_9() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3411x94CryptoProParamSet_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3411x94CryptoProParamSet_9() const { return ___GostR3411x94CryptoProParamSet_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3411x94CryptoProParamSet_9() { return &___GostR3411x94CryptoProParamSet_9; }
	inline void set_GostR3411x94CryptoProParamSet_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3411x94CryptoProParamSet_9 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3411x94CryptoProParamSet_9), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProA_10() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProA_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProA_10() const { return ___GostR3410x94CryptoProA_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProA_10() { return &___GostR3410x94CryptoProA_10; }
	inline void set_GostR3410x94CryptoProA_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProA_10 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProA_10), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProB_11() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProB_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProB_11() const { return ___GostR3410x94CryptoProB_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProB_11() { return &___GostR3410x94CryptoProB_11; }
	inline void set_GostR3410x94CryptoProB_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProB_11 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProB_11), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProC_12() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProC_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProC_12() const { return ___GostR3410x94CryptoProC_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProC_12() { return &___GostR3410x94CryptoProC_12; }
	inline void set_GostR3410x94CryptoProC_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProC_12 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProC_12), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProD_13() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProD_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProD_13() const { return ___GostR3410x94CryptoProD_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProD_13() { return &___GostR3410x94CryptoProD_13; }
	inline void set_GostR3410x94CryptoProD_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProD_13 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProD_13), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProXchA_14() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProXchA_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProXchA_14() const { return ___GostR3410x94CryptoProXchA_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProXchA_14() { return &___GostR3410x94CryptoProXchA_14; }
	inline void set_GostR3410x94CryptoProXchA_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProXchA_14 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProXchA_14), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProXchB_15() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProXchB_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProXchB_15() const { return ___GostR3410x94CryptoProXchB_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProXchB_15() { return &___GostR3410x94CryptoProXchB_15; }
	inline void set_GostR3410x94CryptoProXchB_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProXchB_15 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProXchB_15), value);
	}

	inline static int32_t get_offset_of_GostR3410x94CryptoProXchC_16() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x94CryptoProXchC_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x94CryptoProXchC_16() const { return ___GostR3410x94CryptoProXchC_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x94CryptoProXchC_16() { return &___GostR3410x94CryptoProXchC_16; }
	inline void set_GostR3410x94CryptoProXchC_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x94CryptoProXchC_16 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x94CryptoProXchC_16), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProA_17() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x2001CryptoProA_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x2001CryptoProA_17() const { return ___GostR3410x2001CryptoProA_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x2001CryptoProA_17() { return &___GostR3410x2001CryptoProA_17; }
	inline void set_GostR3410x2001CryptoProA_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x2001CryptoProA_17 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProA_17), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProB_18() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x2001CryptoProB_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x2001CryptoProB_18() const { return ___GostR3410x2001CryptoProB_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x2001CryptoProB_18() { return &___GostR3410x2001CryptoProB_18; }
	inline void set_GostR3410x2001CryptoProB_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x2001CryptoProB_18 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProB_18), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProC_19() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x2001CryptoProC_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x2001CryptoProC_19() const { return ___GostR3410x2001CryptoProC_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x2001CryptoProC_19() { return &___GostR3410x2001CryptoProC_19; }
	inline void set_GostR3410x2001CryptoProC_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x2001CryptoProC_19 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProC_19), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProXchA_20() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x2001CryptoProXchA_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x2001CryptoProXchA_20() const { return ___GostR3410x2001CryptoProXchA_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x2001CryptoProXchA_20() { return &___GostR3410x2001CryptoProXchA_20; }
	inline void set_GostR3410x2001CryptoProXchA_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x2001CryptoProXchA_20 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProXchA_20), value);
	}

	inline static int32_t get_offset_of_GostR3410x2001CryptoProXchB_21() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostR3410x2001CryptoProXchB_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostR3410x2001CryptoProXchB_21() const { return ___GostR3410x2001CryptoProXchB_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostR3410x2001CryptoProXchB_21() { return &___GostR3410x2001CryptoProXchB_21; }
	inline void set_GostR3410x2001CryptoProXchB_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostR3410x2001CryptoProXchB_21 = value;
		Il2CppCodeGenWriteBarrier((&___GostR3410x2001CryptoProXchB_21), value);
	}

	inline static int32_t get_offset_of_GostElSgDH3410Default_22() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostElSgDH3410Default_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostElSgDH3410Default_22() const { return ___GostElSgDH3410Default_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostElSgDH3410Default_22() { return &___GostElSgDH3410Default_22; }
	inline void set_GostElSgDH3410Default_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostElSgDH3410Default_22 = value;
		Il2CppCodeGenWriteBarrier((&___GostElSgDH3410Default_22), value);
	}

	inline static int32_t get_offset_of_GostElSgDH3410x1_23() { return static_cast<int32_t>(offsetof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields, ___GostElSgDH3410x1_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GostElSgDH3410x1_23() const { return ___GostElSgDH3410x1_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GostElSgDH3410x1_23() { return &___GostElSgDH3410x1_23; }
	inline void set_GostElSgDH3410x1_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GostElSgDH3410x1_23 = value;
		Il2CppCodeGenWriteBarrier((&___GostElSgDH3410x1_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOPROOBJECTIDENTIFIERS_TF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_H
#ifndef ECGOST3410NAMEDCURVES_TA479D3D002C7EB97072E90B18DEB39F6D01226E4_H
#define ECGOST3410NAMEDCURVES_TA479D3D002C7EB97072E90B18DEB39F6D01226E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves
struct  ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4  : public RuntimeObject
{
public:

public:
};

struct ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves::parameters
	RuntimeObject* ___parameters_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410NamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields, ___parameters_1)); }
	inline RuntimeObject* get_parameters_1() const { return ___parameters_1; }
	inline RuntimeObject** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(RuntimeObject* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECGOST3410NAMEDCURVES_TA479D3D002C7EB97072E90B18DEB39F6D01226E4_H
#ifndef GOST3410NAMEDPARAMETERS_TB5309CE384AA22B1A3BB3A766E96717DA28B8387_H
#define GOST3410NAMEDPARAMETERS_TB5309CE384AA22B1A3BB3A766E96717DA28B8387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters
struct  Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387  : public RuntimeObject
{
public:

public:
};

struct Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::parameters
	RuntimeObject* ___parameters_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::cryptoProA
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * ___cryptoProA_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::cryptoProB
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * ___cryptoProB_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410NamedParameters::cryptoProXchA
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * ___cryptoProXchA_4;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields, ___parameters_1)); }
	inline RuntimeObject* get_parameters_1() const { return ___parameters_1; }
	inline RuntimeObject** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(RuntimeObject* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_cryptoProA_2() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields, ___cryptoProA_2)); }
	inline Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * get_cryptoProA_2() const { return ___cryptoProA_2; }
	inline Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 ** get_address_of_cryptoProA_2() { return &___cryptoProA_2; }
	inline void set_cryptoProA_2(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * value)
	{
		___cryptoProA_2 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoProA_2), value);
	}

	inline static int32_t get_offset_of_cryptoProB_3() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields, ___cryptoProB_3)); }
	inline Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * get_cryptoProB_3() const { return ___cryptoProB_3; }
	inline Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 ** get_address_of_cryptoProB_3() { return &___cryptoProB_3; }
	inline void set_cryptoProB_3(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * value)
	{
		___cryptoProB_3 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoProB_3), value);
	}

	inline static int32_t get_offset_of_cryptoProXchA_4() { return static_cast<int32_t>(offsetof(Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields, ___cryptoProXchA_4)); }
	inline Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * get_cryptoProXchA_4() const { return ___cryptoProXchA_4; }
	inline Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 ** get_address_of_cryptoProXchA_4() { return &___cryptoProXchA_4; }
	inline void set_cryptoProXchA_4(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85 * value)
	{
		___cryptoProXchA_4 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoProXchA_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410NAMEDPARAMETERS_TB5309CE384AA22B1A3BB3A766E96717DA28B8387_H
#ifndef EACOBJECTIDENTIFIERS_T81EF2239B5EB4C6E61C40D16E4A01A494605F747_H
#define EACOBJECTIDENTIFIERS_T81EF2239B5EB4C6E61C40D16E4A01A494605F747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers
struct  EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747  : public RuntimeObject
{
public:

public:
};

struct EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::bsi_de
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___bsi_de_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_PK
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_PK_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_PK_DH
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_PK_DH_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_PK_ECDH
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_PK_ECDH_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_CA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_CA_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_CA_DH
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_CA_DH_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_CA_DH_3DES_CBC_CBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_CA_DH_3DES_CBC_CBC_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_CA_ECDH
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_CA_ECDH_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_CA_ECDH_3DES_CBC_CBC
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_CA_ECDH_3DES_CBC_CBC_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_RSA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_RSA_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_RSA_v1_5_SHA_1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_RSA_v1_5_SHA_1_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_RSA_v1_5_SHA_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_RSA_v1_5_SHA_256_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_RSA_PSS_SHA_1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_RSA_PSS_SHA_1_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_RSA_PSS_SHA_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_RSA_PSS_SHA_256_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_ECDSA
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_ECDSA_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_ECDSA_SHA_1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_ECDSA_SHA_1_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_ECDSA_SHA_224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_ECDSA_SHA_224_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_ECDSA_SHA_256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_ECDSA_SHA_256_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_ECDSA_SHA_384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_ECDSA_SHA_384_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Eac.EacObjectIdentifiers::id_TA_ECDSA_SHA_512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_TA_ECDSA_SHA_512_20;

public:
	inline static int32_t get_offset_of_bsi_de_0() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___bsi_de_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_bsi_de_0() const { return ___bsi_de_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_bsi_de_0() { return &___bsi_de_0; }
	inline void set_bsi_de_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___bsi_de_0 = value;
		Il2CppCodeGenWriteBarrier((&___bsi_de_0), value);
	}

	inline static int32_t get_offset_of_id_PK_1() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_PK_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_PK_1() const { return ___id_PK_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_PK_1() { return &___id_PK_1; }
	inline void set_id_PK_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_PK_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_PK_1), value);
	}

	inline static int32_t get_offset_of_id_PK_DH_2() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_PK_DH_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_PK_DH_2() const { return ___id_PK_DH_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_PK_DH_2() { return &___id_PK_DH_2; }
	inline void set_id_PK_DH_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_PK_DH_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_PK_DH_2), value);
	}

	inline static int32_t get_offset_of_id_PK_ECDH_3() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_PK_ECDH_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_PK_ECDH_3() const { return ___id_PK_ECDH_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_PK_ECDH_3() { return &___id_PK_ECDH_3; }
	inline void set_id_PK_ECDH_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_PK_ECDH_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_PK_ECDH_3), value);
	}

	inline static int32_t get_offset_of_id_CA_4() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_CA_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_CA_4() const { return ___id_CA_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_CA_4() { return &___id_CA_4; }
	inline void set_id_CA_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_CA_4 = value;
		Il2CppCodeGenWriteBarrier((&___id_CA_4), value);
	}

	inline static int32_t get_offset_of_id_CA_DH_5() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_CA_DH_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_CA_DH_5() const { return ___id_CA_DH_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_CA_DH_5() { return &___id_CA_DH_5; }
	inline void set_id_CA_DH_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_CA_DH_5 = value;
		Il2CppCodeGenWriteBarrier((&___id_CA_DH_5), value);
	}

	inline static int32_t get_offset_of_id_CA_DH_3DES_CBC_CBC_6() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_CA_DH_3DES_CBC_CBC_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_CA_DH_3DES_CBC_CBC_6() const { return ___id_CA_DH_3DES_CBC_CBC_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_CA_DH_3DES_CBC_CBC_6() { return &___id_CA_DH_3DES_CBC_CBC_6; }
	inline void set_id_CA_DH_3DES_CBC_CBC_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_CA_DH_3DES_CBC_CBC_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_CA_DH_3DES_CBC_CBC_6), value);
	}

	inline static int32_t get_offset_of_id_CA_ECDH_7() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_CA_ECDH_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_CA_ECDH_7() const { return ___id_CA_ECDH_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_CA_ECDH_7() { return &___id_CA_ECDH_7; }
	inline void set_id_CA_ECDH_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_CA_ECDH_7 = value;
		Il2CppCodeGenWriteBarrier((&___id_CA_ECDH_7), value);
	}

	inline static int32_t get_offset_of_id_CA_ECDH_3DES_CBC_CBC_8() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_CA_ECDH_3DES_CBC_CBC_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_CA_ECDH_3DES_CBC_CBC_8() const { return ___id_CA_ECDH_3DES_CBC_CBC_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_CA_ECDH_3DES_CBC_CBC_8() { return &___id_CA_ECDH_3DES_CBC_CBC_8; }
	inline void set_id_CA_ECDH_3DES_CBC_CBC_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_CA_ECDH_3DES_CBC_CBC_8 = value;
		Il2CppCodeGenWriteBarrier((&___id_CA_ECDH_3DES_CBC_CBC_8), value);
	}

	inline static int32_t get_offset_of_id_TA_9() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_9() const { return ___id_TA_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_9() { return &___id_TA_9; }
	inline void set_id_TA_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_9 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_9), value);
	}

	inline static int32_t get_offset_of_id_TA_RSA_10() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_RSA_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_RSA_10() const { return ___id_TA_RSA_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_RSA_10() { return &___id_TA_RSA_10; }
	inline void set_id_TA_RSA_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_RSA_10 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_RSA_10), value);
	}

	inline static int32_t get_offset_of_id_TA_RSA_v1_5_SHA_1_11() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_RSA_v1_5_SHA_1_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_RSA_v1_5_SHA_1_11() const { return ___id_TA_RSA_v1_5_SHA_1_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_RSA_v1_5_SHA_1_11() { return &___id_TA_RSA_v1_5_SHA_1_11; }
	inline void set_id_TA_RSA_v1_5_SHA_1_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_RSA_v1_5_SHA_1_11 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_RSA_v1_5_SHA_1_11), value);
	}

	inline static int32_t get_offset_of_id_TA_RSA_v1_5_SHA_256_12() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_RSA_v1_5_SHA_256_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_RSA_v1_5_SHA_256_12() const { return ___id_TA_RSA_v1_5_SHA_256_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_RSA_v1_5_SHA_256_12() { return &___id_TA_RSA_v1_5_SHA_256_12; }
	inline void set_id_TA_RSA_v1_5_SHA_256_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_RSA_v1_5_SHA_256_12 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_RSA_v1_5_SHA_256_12), value);
	}

	inline static int32_t get_offset_of_id_TA_RSA_PSS_SHA_1_13() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_RSA_PSS_SHA_1_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_RSA_PSS_SHA_1_13() const { return ___id_TA_RSA_PSS_SHA_1_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_RSA_PSS_SHA_1_13() { return &___id_TA_RSA_PSS_SHA_1_13; }
	inline void set_id_TA_RSA_PSS_SHA_1_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_RSA_PSS_SHA_1_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_RSA_PSS_SHA_1_13), value);
	}

	inline static int32_t get_offset_of_id_TA_RSA_PSS_SHA_256_14() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_RSA_PSS_SHA_256_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_RSA_PSS_SHA_256_14() const { return ___id_TA_RSA_PSS_SHA_256_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_RSA_PSS_SHA_256_14() { return &___id_TA_RSA_PSS_SHA_256_14; }
	inline void set_id_TA_RSA_PSS_SHA_256_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_RSA_PSS_SHA_256_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_RSA_PSS_SHA_256_14), value);
	}

	inline static int32_t get_offset_of_id_TA_ECDSA_15() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_ECDSA_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_ECDSA_15() const { return ___id_TA_ECDSA_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_ECDSA_15() { return &___id_TA_ECDSA_15; }
	inline void set_id_TA_ECDSA_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_ECDSA_15 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_ECDSA_15), value);
	}

	inline static int32_t get_offset_of_id_TA_ECDSA_SHA_1_16() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_ECDSA_SHA_1_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_ECDSA_SHA_1_16() const { return ___id_TA_ECDSA_SHA_1_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_ECDSA_SHA_1_16() { return &___id_TA_ECDSA_SHA_1_16; }
	inline void set_id_TA_ECDSA_SHA_1_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_ECDSA_SHA_1_16 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_ECDSA_SHA_1_16), value);
	}

	inline static int32_t get_offset_of_id_TA_ECDSA_SHA_224_17() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_ECDSA_SHA_224_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_ECDSA_SHA_224_17() const { return ___id_TA_ECDSA_SHA_224_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_ECDSA_SHA_224_17() { return &___id_TA_ECDSA_SHA_224_17; }
	inline void set_id_TA_ECDSA_SHA_224_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_ECDSA_SHA_224_17 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_ECDSA_SHA_224_17), value);
	}

	inline static int32_t get_offset_of_id_TA_ECDSA_SHA_256_18() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_ECDSA_SHA_256_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_ECDSA_SHA_256_18() const { return ___id_TA_ECDSA_SHA_256_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_ECDSA_SHA_256_18() { return &___id_TA_ECDSA_SHA_256_18; }
	inline void set_id_TA_ECDSA_SHA_256_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_ECDSA_SHA_256_18 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_ECDSA_SHA_256_18), value);
	}

	inline static int32_t get_offset_of_id_TA_ECDSA_SHA_384_19() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_ECDSA_SHA_384_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_ECDSA_SHA_384_19() const { return ___id_TA_ECDSA_SHA_384_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_ECDSA_SHA_384_19() { return &___id_TA_ECDSA_SHA_384_19; }
	inline void set_id_TA_ECDSA_SHA_384_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_ECDSA_SHA_384_19 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_ECDSA_SHA_384_19), value);
	}

	inline static int32_t get_offset_of_id_TA_ECDSA_SHA_512_20() { return static_cast<int32_t>(offsetof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields, ___id_TA_ECDSA_SHA_512_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_TA_ECDSA_SHA_512_20() const { return ___id_TA_ECDSA_SHA_512_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_TA_ECDSA_SHA_512_20() { return &___id_TA_ECDSA_SHA_512_20; }
	inline void set_id_TA_ECDSA_SHA_512_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_TA_ECDSA_SHA_512_20 = value;
		Il2CppCodeGenWriteBarrier((&___id_TA_ECDSA_SHA_512_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EACOBJECTIDENTIFIERS_T81EF2239B5EB4C6E61C40D16E4A01A494605F747_H
#ifndef EDECOBJECTIDENTIFIERS_T7FF38B186AFA54002FF60BD83076FA93D591EE80_H
#define EDECOBJECTIDENTIFIERS_T7FF38B186AFA54002FF60BD83076FA93D591EE80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.EdEC.EdECObjectIdentifiers
struct  EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80  : public RuntimeObject
{
public:

public:
};

struct EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.EdEC.EdECObjectIdentifiers::id_edwards_curve_algs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_edwards_curve_algs_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.EdEC.EdECObjectIdentifiers::id_X25519
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_X25519_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.EdEC.EdECObjectIdentifiers::id_X448
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_X448_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.EdEC.EdECObjectIdentifiers::id_Ed25519
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_Ed25519_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.EdEC.EdECObjectIdentifiers::id_Ed448
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_Ed448_4;

public:
	inline static int32_t get_offset_of_id_edwards_curve_algs_0() { return static_cast<int32_t>(offsetof(EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields, ___id_edwards_curve_algs_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_edwards_curve_algs_0() const { return ___id_edwards_curve_algs_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_edwards_curve_algs_0() { return &___id_edwards_curve_algs_0; }
	inline void set_id_edwards_curve_algs_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_edwards_curve_algs_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_edwards_curve_algs_0), value);
	}

	inline static int32_t get_offset_of_id_X25519_1() { return static_cast<int32_t>(offsetof(EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields, ___id_X25519_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_X25519_1() const { return ___id_X25519_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_X25519_1() { return &___id_X25519_1; }
	inline void set_id_X25519_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_X25519_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_X25519_1), value);
	}

	inline static int32_t get_offset_of_id_X448_2() { return static_cast<int32_t>(offsetof(EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields, ___id_X448_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_X448_2() const { return ___id_X448_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_X448_2() { return &___id_X448_2; }
	inline void set_id_X448_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_X448_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_X448_2), value);
	}

	inline static int32_t get_offset_of_id_Ed25519_3() { return static_cast<int32_t>(offsetof(EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields, ___id_Ed25519_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_Ed25519_3() const { return ___id_Ed25519_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_Ed25519_3() { return &___id_Ed25519_3; }
	inline void set_id_Ed25519_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_Ed25519_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_Ed25519_3), value);
	}

	inline static int32_t get_offset_of_id_Ed448_4() { return static_cast<int32_t>(offsetof(EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields, ___id_Ed448_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_Ed448_4() const { return ___id_Ed448_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_Ed448_4() { return &___id_Ed448_4; }
	inline void set_id_Ed448_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_Ed448_4 = value;
		Il2CppCodeGenWriteBarrier((&___id_Ed448_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDECOBJECTIDENTIFIERS_T7FF38B186AFA54002FF60BD83076FA93D591EE80_H
#ifndef COMMITMENTTYPEIDENTIFIER_TA699B23B186FED32C10332E740438616E1B063FB_H
#define COMMITMENTTYPEIDENTIFIER_TA699B23B186FED32C10332E740438616E1B063FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier
struct  CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB  : public RuntimeObject
{
public:

public:
};

struct CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier::ProofOfOrigin
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ProofOfOrigin_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier::ProofOfReceipt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ProofOfReceipt_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier::ProofOfDelivery
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ProofOfDelivery_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier::ProofOfSender
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ProofOfSender_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier::ProofOfApproval
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ProofOfApproval_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIdentifier::ProofOfCreation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ProofOfCreation_5;

public:
	inline static int32_t get_offset_of_ProofOfOrigin_0() { return static_cast<int32_t>(offsetof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields, ___ProofOfOrigin_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ProofOfOrigin_0() const { return ___ProofOfOrigin_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ProofOfOrigin_0() { return &___ProofOfOrigin_0; }
	inline void set_ProofOfOrigin_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ProofOfOrigin_0 = value;
		Il2CppCodeGenWriteBarrier((&___ProofOfOrigin_0), value);
	}

	inline static int32_t get_offset_of_ProofOfReceipt_1() { return static_cast<int32_t>(offsetof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields, ___ProofOfReceipt_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ProofOfReceipt_1() const { return ___ProofOfReceipt_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ProofOfReceipt_1() { return &___ProofOfReceipt_1; }
	inline void set_ProofOfReceipt_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ProofOfReceipt_1 = value;
		Il2CppCodeGenWriteBarrier((&___ProofOfReceipt_1), value);
	}

	inline static int32_t get_offset_of_ProofOfDelivery_2() { return static_cast<int32_t>(offsetof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields, ___ProofOfDelivery_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ProofOfDelivery_2() const { return ___ProofOfDelivery_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ProofOfDelivery_2() { return &___ProofOfDelivery_2; }
	inline void set_ProofOfDelivery_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ProofOfDelivery_2 = value;
		Il2CppCodeGenWriteBarrier((&___ProofOfDelivery_2), value);
	}

	inline static int32_t get_offset_of_ProofOfSender_3() { return static_cast<int32_t>(offsetof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields, ___ProofOfSender_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ProofOfSender_3() const { return ___ProofOfSender_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ProofOfSender_3() { return &___ProofOfSender_3; }
	inline void set_ProofOfSender_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ProofOfSender_3 = value;
		Il2CppCodeGenWriteBarrier((&___ProofOfSender_3), value);
	}

	inline static int32_t get_offset_of_ProofOfApproval_4() { return static_cast<int32_t>(offsetof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields, ___ProofOfApproval_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ProofOfApproval_4() const { return ___ProofOfApproval_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ProofOfApproval_4() { return &___ProofOfApproval_4; }
	inline void set_ProofOfApproval_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ProofOfApproval_4 = value;
		Il2CppCodeGenWriteBarrier((&___ProofOfApproval_4), value);
	}

	inline static int32_t get_offset_of_ProofOfCreation_5() { return static_cast<int32_t>(offsetof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields, ___ProofOfCreation_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ProofOfCreation_5() const { return ___ProofOfCreation_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ProofOfCreation_5() { return &___ProofOfCreation_5; }
	inline void set_ProofOfCreation_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ProofOfCreation_5 = value;
		Il2CppCodeGenWriteBarrier((&___ProofOfCreation_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMITMENTTYPEIDENTIFIER_TA699B23B186FED32C10332E740438616E1B063FB_H
#ifndef ESFATTRIBUTES_T8E93F9992831B8953EDBDB732792B578DF0354F4_H
#define ESFATTRIBUTES_T8E93F9992831B8953EDBDB732792B578DF0354F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes
struct  EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4  : public RuntimeObject
{
public:

public:
};

struct EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::SigPolicyId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SigPolicyId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::CommitmentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CommitmentType_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::SignerLocation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SignerLocation_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::SignerAttr
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___SignerAttr_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::OtherSigCert
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___OtherSigCert_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::ContentTimestamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ContentTimestamp_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::CertificateRefs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CertificateRefs_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::RevocationRefs
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RevocationRefs_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::CertValues
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CertValues_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::RevocationValues
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___RevocationValues_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::EscTimeStamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EscTimeStamp_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::CertCrlTimestamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___CertCrlTimestamp_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::ArchiveTimestamp
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ArchiveTimestamp_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.EsfAttributes::ArchiveTimestampV2
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ArchiveTimestampV2_13;

public:
	inline static int32_t get_offset_of_SigPolicyId_0() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___SigPolicyId_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SigPolicyId_0() const { return ___SigPolicyId_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SigPolicyId_0() { return &___SigPolicyId_0; }
	inline void set_SigPolicyId_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SigPolicyId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SigPolicyId_0), value);
	}

	inline static int32_t get_offset_of_CommitmentType_1() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___CommitmentType_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CommitmentType_1() const { return ___CommitmentType_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CommitmentType_1() { return &___CommitmentType_1; }
	inline void set_CommitmentType_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CommitmentType_1 = value;
		Il2CppCodeGenWriteBarrier((&___CommitmentType_1), value);
	}

	inline static int32_t get_offset_of_SignerLocation_2() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___SignerLocation_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SignerLocation_2() const { return ___SignerLocation_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SignerLocation_2() { return &___SignerLocation_2; }
	inline void set_SignerLocation_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SignerLocation_2 = value;
		Il2CppCodeGenWriteBarrier((&___SignerLocation_2), value);
	}

	inline static int32_t get_offset_of_SignerAttr_3() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___SignerAttr_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_SignerAttr_3() const { return ___SignerAttr_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_SignerAttr_3() { return &___SignerAttr_3; }
	inline void set_SignerAttr_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___SignerAttr_3 = value;
		Il2CppCodeGenWriteBarrier((&___SignerAttr_3), value);
	}

	inline static int32_t get_offset_of_OtherSigCert_4() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___OtherSigCert_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_OtherSigCert_4() const { return ___OtherSigCert_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_OtherSigCert_4() { return &___OtherSigCert_4; }
	inline void set_OtherSigCert_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___OtherSigCert_4 = value;
		Il2CppCodeGenWriteBarrier((&___OtherSigCert_4), value);
	}

	inline static int32_t get_offset_of_ContentTimestamp_5() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___ContentTimestamp_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ContentTimestamp_5() const { return ___ContentTimestamp_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ContentTimestamp_5() { return &___ContentTimestamp_5; }
	inline void set_ContentTimestamp_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ContentTimestamp_5 = value;
		Il2CppCodeGenWriteBarrier((&___ContentTimestamp_5), value);
	}

	inline static int32_t get_offset_of_CertificateRefs_6() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___CertificateRefs_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CertificateRefs_6() const { return ___CertificateRefs_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CertificateRefs_6() { return &___CertificateRefs_6; }
	inline void set_CertificateRefs_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CertificateRefs_6 = value;
		Il2CppCodeGenWriteBarrier((&___CertificateRefs_6), value);
	}

	inline static int32_t get_offset_of_RevocationRefs_7() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___RevocationRefs_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RevocationRefs_7() const { return ___RevocationRefs_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RevocationRefs_7() { return &___RevocationRefs_7; }
	inline void set_RevocationRefs_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RevocationRefs_7 = value;
		Il2CppCodeGenWriteBarrier((&___RevocationRefs_7), value);
	}

	inline static int32_t get_offset_of_CertValues_8() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___CertValues_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CertValues_8() const { return ___CertValues_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CertValues_8() { return &___CertValues_8; }
	inline void set_CertValues_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CertValues_8 = value;
		Il2CppCodeGenWriteBarrier((&___CertValues_8), value);
	}

	inline static int32_t get_offset_of_RevocationValues_9() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___RevocationValues_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_RevocationValues_9() const { return ___RevocationValues_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_RevocationValues_9() { return &___RevocationValues_9; }
	inline void set_RevocationValues_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___RevocationValues_9 = value;
		Il2CppCodeGenWriteBarrier((&___RevocationValues_9), value);
	}

	inline static int32_t get_offset_of_EscTimeStamp_10() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___EscTimeStamp_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EscTimeStamp_10() const { return ___EscTimeStamp_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EscTimeStamp_10() { return &___EscTimeStamp_10; }
	inline void set_EscTimeStamp_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EscTimeStamp_10 = value;
		Il2CppCodeGenWriteBarrier((&___EscTimeStamp_10), value);
	}

	inline static int32_t get_offset_of_CertCrlTimestamp_11() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___CertCrlTimestamp_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_CertCrlTimestamp_11() const { return ___CertCrlTimestamp_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_CertCrlTimestamp_11() { return &___CertCrlTimestamp_11; }
	inline void set_CertCrlTimestamp_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___CertCrlTimestamp_11 = value;
		Il2CppCodeGenWriteBarrier((&___CertCrlTimestamp_11), value);
	}

	inline static int32_t get_offset_of_ArchiveTimestamp_12() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___ArchiveTimestamp_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ArchiveTimestamp_12() const { return ___ArchiveTimestamp_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ArchiveTimestamp_12() { return &___ArchiveTimestamp_12; }
	inline void set_ArchiveTimestamp_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ArchiveTimestamp_12 = value;
		Il2CppCodeGenWriteBarrier((&___ArchiveTimestamp_12), value);
	}

	inline static int32_t get_offset_of_ArchiveTimestampV2_13() { return static_cast<int32_t>(offsetof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields, ___ArchiveTimestampV2_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ArchiveTimestampV2_13() const { return ___ArchiveTimestampV2_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ArchiveTimestampV2_13() { return &___ArchiveTimestampV2_13; }
	inline void set_ArchiveTimestampV2_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ArchiveTimestampV2_13 = value;
		Il2CppCodeGenWriteBarrier((&___ArchiveTimestampV2_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESFATTRIBUTES_T8E93F9992831B8953EDBDB732792B578DF0354F4_H
#ifndef GMNAMEDCURVES_T71DD7AC79305C4638D6C58B02B58864B0D34216A_H
#define GMNAMEDCURVES_T71DD7AC79305C4638D6C58B02B58864B0D34216A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves
struct  GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A  : public RuntimeObject
{
public:

public:
};

struct GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves::objIds
	RuntimeObject* ___objIds_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves::curves
	RuntimeObject* ___curves_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves::names
	RuntimeObject* ___names_2;

public:
	inline static int32_t get_offset_of_objIds_0() { return static_cast<int32_t>(offsetof(GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields, ___objIds_0)); }
	inline RuntimeObject* get_objIds_0() const { return ___objIds_0; }
	inline RuntimeObject** get_address_of_objIds_0() { return &___objIds_0; }
	inline void set_objIds_0(RuntimeObject* value)
	{
		___objIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___objIds_0), value);
	}

	inline static int32_t get_offset_of_curves_1() { return static_cast<int32_t>(offsetof(GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields, ___curves_1)); }
	inline RuntimeObject* get_curves_1() const { return ___curves_1; }
	inline RuntimeObject** get_address_of_curves_1() { return &___curves_1; }
	inline void set_curves_1(RuntimeObject* value)
	{
		___curves_1 = value;
		Il2CppCodeGenWriteBarrier((&___curves_1), value);
	}

	inline static int32_t get_offset_of_names_2() { return static_cast<int32_t>(offsetof(GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields, ___names_2)); }
	inline RuntimeObject* get_names_2() const { return ___names_2; }
	inline RuntimeObject** get_address_of_names_2() { return &___names_2; }
	inline void set_names_2(RuntimeObject* value)
	{
		___names_2 = value;
		Il2CppCodeGenWriteBarrier((&___names_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMNAMEDCURVES_T71DD7AC79305C4638D6C58B02B58864B0D34216A_H
#ifndef GMOBJECTIDENTIFIERS_T4B6050E725BA3831B60398A4586BE6B260B6EEF7_H
#define GMOBJECTIDENTIFIERS_T4B6050E725BA3831B60398A4586BE6B260B6EEF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers
struct  GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7  : public RuntimeObject
{
public:

public:
};

struct GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm_scheme
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm_scheme_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm6_ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm6_ecb_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm6_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm6_cbc_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm6_ofb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm6_ofb128_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm6_cfb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm6_cfb128_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm1_ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm1_ecb_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm1_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm1_cbc_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm1_ofb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm1_ofb128_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm1_cfb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm1_cfb128_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm1_cfb1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm1_cfb1_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm1_cfb8
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm1_cfb8_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::ssf33_ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ssf33_ecb_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::ssf33_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ssf33_cbc_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::ssf33_ofb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ssf33_ofb128_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::ssf33_cfb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ssf33_cfb128_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::ssf33_cfb1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ssf33_cfb1_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::ssf33_cfb8
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___ssf33_cfb8_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_ecb_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_cbc_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_ofb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_ofb128_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_cfb128
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_cfb128_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_cfb1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_cfb1_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_cfb8
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_cfb8_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_ctr
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_ctr_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_gcm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_gcm_24;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_ccm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_ccm_25;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_xts
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_xts_26;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_wrap
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_wrap_27;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_wrap_pad
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_wrap_pad_28;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sms4_ocb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sms4_ocb_29;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm5_30;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2p256v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2p256v1_31;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_32;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2exchange
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2exchange_33;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_34;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::wapip192v1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___wapip192v1_35;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_recommendedParameters
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_recommendedParameters_36;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_specifiedParameters
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_specifiedParameters_37;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_sm3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_sm3_38;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_sha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_sha1_39;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_sha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_sha224_40;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_sha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_sha256_41;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_sha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_sha384_42;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_sha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_sha512_43;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_rmd160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_rmd160_44;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_whirlpool
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_whirlpool_45;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_blake2b512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_blake2b512_46;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_blake2s256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_blake2s256_47;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2encrypt_with_md5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2encrypt_with_md5_48;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::id_sm9PublicKey
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___id_sm9PublicKey_49;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm9sign
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm9sign_50;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm9keyagreement
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm9keyagreement_51;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm9encrypt
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm9encrypt_52;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm3_53;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::hmac_sm3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___hmac_sm3_54;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_sm3
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_sm3_55;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_sha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_sha1_56;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_sha256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_sha256_57;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_sha512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_sha512_58;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_sha224
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_sha224_59;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_sha384
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_sha384_60;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_rmd160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_rmd160_61;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_whirlpool
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_whirlpool_62;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_blake2b512
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_blake2b512_63;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMObjectIdentifiers::sm2sign_with_blake2s256
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sm2sign_with_blake2s256_64;

public:
	inline static int32_t get_offset_of_sm_scheme_0() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm_scheme_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm_scheme_0() const { return ___sm_scheme_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm_scheme_0() { return &___sm_scheme_0; }
	inline void set_sm_scheme_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm_scheme_0 = value;
		Il2CppCodeGenWriteBarrier((&___sm_scheme_0), value);
	}

	inline static int32_t get_offset_of_sm6_ecb_1() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm6_ecb_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm6_ecb_1() const { return ___sm6_ecb_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm6_ecb_1() { return &___sm6_ecb_1; }
	inline void set_sm6_ecb_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm6_ecb_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm6_ecb_1), value);
	}

	inline static int32_t get_offset_of_sm6_cbc_2() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm6_cbc_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm6_cbc_2() const { return ___sm6_cbc_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm6_cbc_2() { return &___sm6_cbc_2; }
	inline void set_sm6_cbc_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm6_cbc_2 = value;
		Il2CppCodeGenWriteBarrier((&___sm6_cbc_2), value);
	}

	inline static int32_t get_offset_of_sm6_ofb128_3() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm6_ofb128_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm6_ofb128_3() const { return ___sm6_ofb128_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm6_ofb128_3() { return &___sm6_ofb128_3; }
	inline void set_sm6_ofb128_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm6_ofb128_3 = value;
		Il2CppCodeGenWriteBarrier((&___sm6_ofb128_3), value);
	}

	inline static int32_t get_offset_of_sm6_cfb128_4() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm6_cfb128_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm6_cfb128_4() const { return ___sm6_cfb128_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm6_cfb128_4() { return &___sm6_cfb128_4; }
	inline void set_sm6_cfb128_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm6_cfb128_4 = value;
		Il2CppCodeGenWriteBarrier((&___sm6_cfb128_4), value);
	}

	inline static int32_t get_offset_of_sm1_ecb_5() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm1_ecb_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm1_ecb_5() const { return ___sm1_ecb_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm1_ecb_5() { return &___sm1_ecb_5; }
	inline void set_sm1_ecb_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm1_ecb_5 = value;
		Il2CppCodeGenWriteBarrier((&___sm1_ecb_5), value);
	}

	inline static int32_t get_offset_of_sm1_cbc_6() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm1_cbc_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm1_cbc_6() const { return ___sm1_cbc_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm1_cbc_6() { return &___sm1_cbc_6; }
	inline void set_sm1_cbc_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm1_cbc_6 = value;
		Il2CppCodeGenWriteBarrier((&___sm1_cbc_6), value);
	}

	inline static int32_t get_offset_of_sm1_ofb128_7() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm1_ofb128_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm1_ofb128_7() const { return ___sm1_ofb128_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm1_ofb128_7() { return &___sm1_ofb128_7; }
	inline void set_sm1_ofb128_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm1_ofb128_7 = value;
		Il2CppCodeGenWriteBarrier((&___sm1_ofb128_7), value);
	}

	inline static int32_t get_offset_of_sm1_cfb128_8() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm1_cfb128_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm1_cfb128_8() const { return ___sm1_cfb128_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm1_cfb128_8() { return &___sm1_cfb128_8; }
	inline void set_sm1_cfb128_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm1_cfb128_8 = value;
		Il2CppCodeGenWriteBarrier((&___sm1_cfb128_8), value);
	}

	inline static int32_t get_offset_of_sm1_cfb1_9() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm1_cfb1_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm1_cfb1_9() const { return ___sm1_cfb1_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm1_cfb1_9() { return &___sm1_cfb1_9; }
	inline void set_sm1_cfb1_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm1_cfb1_9 = value;
		Il2CppCodeGenWriteBarrier((&___sm1_cfb1_9), value);
	}

	inline static int32_t get_offset_of_sm1_cfb8_10() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm1_cfb8_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm1_cfb8_10() const { return ___sm1_cfb8_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm1_cfb8_10() { return &___sm1_cfb8_10; }
	inline void set_sm1_cfb8_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm1_cfb8_10 = value;
		Il2CppCodeGenWriteBarrier((&___sm1_cfb8_10), value);
	}

	inline static int32_t get_offset_of_ssf33_ecb_11() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___ssf33_ecb_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ssf33_ecb_11() const { return ___ssf33_ecb_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ssf33_ecb_11() { return &___ssf33_ecb_11; }
	inline void set_ssf33_ecb_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ssf33_ecb_11 = value;
		Il2CppCodeGenWriteBarrier((&___ssf33_ecb_11), value);
	}

	inline static int32_t get_offset_of_ssf33_cbc_12() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___ssf33_cbc_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ssf33_cbc_12() const { return ___ssf33_cbc_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ssf33_cbc_12() { return &___ssf33_cbc_12; }
	inline void set_ssf33_cbc_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ssf33_cbc_12 = value;
		Il2CppCodeGenWriteBarrier((&___ssf33_cbc_12), value);
	}

	inline static int32_t get_offset_of_ssf33_ofb128_13() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___ssf33_ofb128_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ssf33_ofb128_13() const { return ___ssf33_ofb128_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ssf33_ofb128_13() { return &___ssf33_ofb128_13; }
	inline void set_ssf33_ofb128_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ssf33_ofb128_13 = value;
		Il2CppCodeGenWriteBarrier((&___ssf33_ofb128_13), value);
	}

	inline static int32_t get_offset_of_ssf33_cfb128_14() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___ssf33_cfb128_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ssf33_cfb128_14() const { return ___ssf33_cfb128_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ssf33_cfb128_14() { return &___ssf33_cfb128_14; }
	inline void set_ssf33_cfb128_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ssf33_cfb128_14 = value;
		Il2CppCodeGenWriteBarrier((&___ssf33_cfb128_14), value);
	}

	inline static int32_t get_offset_of_ssf33_cfb1_15() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___ssf33_cfb1_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ssf33_cfb1_15() const { return ___ssf33_cfb1_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ssf33_cfb1_15() { return &___ssf33_cfb1_15; }
	inline void set_ssf33_cfb1_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ssf33_cfb1_15 = value;
		Il2CppCodeGenWriteBarrier((&___ssf33_cfb1_15), value);
	}

	inline static int32_t get_offset_of_ssf33_cfb8_16() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___ssf33_cfb8_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_ssf33_cfb8_16() const { return ___ssf33_cfb8_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_ssf33_cfb8_16() { return &___ssf33_cfb8_16; }
	inline void set_ssf33_cfb8_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___ssf33_cfb8_16 = value;
		Il2CppCodeGenWriteBarrier((&___ssf33_cfb8_16), value);
	}

	inline static int32_t get_offset_of_sms4_ecb_17() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_ecb_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_ecb_17() const { return ___sms4_ecb_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_ecb_17() { return &___sms4_ecb_17; }
	inline void set_sms4_ecb_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_ecb_17 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_ecb_17), value);
	}

	inline static int32_t get_offset_of_sms4_cbc_18() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_cbc_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_cbc_18() const { return ___sms4_cbc_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_cbc_18() { return &___sms4_cbc_18; }
	inline void set_sms4_cbc_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_cbc_18 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_cbc_18), value);
	}

	inline static int32_t get_offset_of_sms4_ofb128_19() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_ofb128_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_ofb128_19() const { return ___sms4_ofb128_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_ofb128_19() { return &___sms4_ofb128_19; }
	inline void set_sms4_ofb128_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_ofb128_19 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_ofb128_19), value);
	}

	inline static int32_t get_offset_of_sms4_cfb128_20() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_cfb128_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_cfb128_20() const { return ___sms4_cfb128_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_cfb128_20() { return &___sms4_cfb128_20; }
	inline void set_sms4_cfb128_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_cfb128_20 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_cfb128_20), value);
	}

	inline static int32_t get_offset_of_sms4_cfb1_21() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_cfb1_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_cfb1_21() const { return ___sms4_cfb1_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_cfb1_21() { return &___sms4_cfb1_21; }
	inline void set_sms4_cfb1_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_cfb1_21 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_cfb1_21), value);
	}

	inline static int32_t get_offset_of_sms4_cfb8_22() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_cfb8_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_cfb8_22() const { return ___sms4_cfb8_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_cfb8_22() { return &___sms4_cfb8_22; }
	inline void set_sms4_cfb8_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_cfb8_22 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_cfb8_22), value);
	}

	inline static int32_t get_offset_of_sms4_ctr_23() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_ctr_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_ctr_23() const { return ___sms4_ctr_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_ctr_23() { return &___sms4_ctr_23; }
	inline void set_sms4_ctr_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_ctr_23 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_ctr_23), value);
	}

	inline static int32_t get_offset_of_sms4_gcm_24() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_gcm_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_gcm_24() const { return ___sms4_gcm_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_gcm_24() { return &___sms4_gcm_24; }
	inline void set_sms4_gcm_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_gcm_24 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_gcm_24), value);
	}

	inline static int32_t get_offset_of_sms4_ccm_25() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_ccm_25)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_ccm_25() const { return ___sms4_ccm_25; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_ccm_25() { return &___sms4_ccm_25; }
	inline void set_sms4_ccm_25(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_ccm_25 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_ccm_25), value);
	}

	inline static int32_t get_offset_of_sms4_xts_26() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_xts_26)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_xts_26() const { return ___sms4_xts_26; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_xts_26() { return &___sms4_xts_26; }
	inline void set_sms4_xts_26(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_xts_26 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_xts_26), value);
	}

	inline static int32_t get_offset_of_sms4_wrap_27() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_wrap_27)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_wrap_27() const { return ___sms4_wrap_27; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_wrap_27() { return &___sms4_wrap_27; }
	inline void set_sms4_wrap_27(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_wrap_27 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_wrap_27), value);
	}

	inline static int32_t get_offset_of_sms4_wrap_pad_28() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_wrap_pad_28)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_wrap_pad_28() const { return ___sms4_wrap_pad_28; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_wrap_pad_28() { return &___sms4_wrap_pad_28; }
	inline void set_sms4_wrap_pad_28(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_wrap_pad_28 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_wrap_pad_28), value);
	}

	inline static int32_t get_offset_of_sms4_ocb_29() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sms4_ocb_29)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sms4_ocb_29() const { return ___sms4_ocb_29; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sms4_ocb_29() { return &___sms4_ocb_29; }
	inline void set_sms4_ocb_29(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sms4_ocb_29 = value;
		Il2CppCodeGenWriteBarrier((&___sms4_ocb_29), value);
	}

	inline static int32_t get_offset_of_sm5_30() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm5_30)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm5_30() const { return ___sm5_30; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm5_30() { return &___sm5_30; }
	inline void set_sm5_30(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm5_30 = value;
		Il2CppCodeGenWriteBarrier((&___sm5_30), value);
	}

	inline static int32_t get_offset_of_sm2p256v1_31() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2p256v1_31)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2p256v1_31() const { return ___sm2p256v1_31; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2p256v1_31() { return &___sm2p256v1_31; }
	inline void set_sm2p256v1_31(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2p256v1_31 = value;
		Il2CppCodeGenWriteBarrier((&___sm2p256v1_31), value);
	}

	inline static int32_t get_offset_of_sm2sign_32() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_32)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_32() const { return ___sm2sign_32; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_32() { return &___sm2sign_32; }
	inline void set_sm2sign_32(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_32 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_32), value);
	}

	inline static int32_t get_offset_of_sm2exchange_33() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2exchange_33)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2exchange_33() const { return ___sm2exchange_33; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2exchange_33() { return &___sm2exchange_33; }
	inline void set_sm2exchange_33(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2exchange_33 = value;
		Il2CppCodeGenWriteBarrier((&___sm2exchange_33), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_34() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_34)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_34() const { return ___sm2encrypt_34; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_34() { return &___sm2encrypt_34; }
	inline void set_sm2encrypt_34(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_34 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_34), value);
	}

	inline static int32_t get_offset_of_wapip192v1_35() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___wapip192v1_35)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_wapip192v1_35() const { return ___wapip192v1_35; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_wapip192v1_35() { return &___wapip192v1_35; }
	inline void set_wapip192v1_35(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___wapip192v1_35 = value;
		Il2CppCodeGenWriteBarrier((&___wapip192v1_35), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_recommendedParameters_36() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_recommendedParameters_36)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_recommendedParameters_36() const { return ___sm2encrypt_recommendedParameters_36; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_recommendedParameters_36() { return &___sm2encrypt_recommendedParameters_36; }
	inline void set_sm2encrypt_recommendedParameters_36(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_recommendedParameters_36 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_recommendedParameters_36), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_specifiedParameters_37() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_specifiedParameters_37)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_specifiedParameters_37() const { return ___sm2encrypt_specifiedParameters_37; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_specifiedParameters_37() { return &___sm2encrypt_specifiedParameters_37; }
	inline void set_sm2encrypt_specifiedParameters_37(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_specifiedParameters_37 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_specifiedParameters_37), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_sm3_38() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_sm3_38)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_sm3_38() const { return ___sm2encrypt_with_sm3_38; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_sm3_38() { return &___sm2encrypt_with_sm3_38; }
	inline void set_sm2encrypt_with_sm3_38(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_sm3_38 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_sm3_38), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_sha1_39() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_sha1_39)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_sha1_39() const { return ___sm2encrypt_with_sha1_39; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_sha1_39() { return &___sm2encrypt_with_sha1_39; }
	inline void set_sm2encrypt_with_sha1_39(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_sha1_39 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_sha1_39), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_sha224_40() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_sha224_40)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_sha224_40() const { return ___sm2encrypt_with_sha224_40; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_sha224_40() { return &___sm2encrypt_with_sha224_40; }
	inline void set_sm2encrypt_with_sha224_40(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_sha224_40 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_sha224_40), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_sha256_41() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_sha256_41)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_sha256_41() const { return ___sm2encrypt_with_sha256_41; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_sha256_41() { return &___sm2encrypt_with_sha256_41; }
	inline void set_sm2encrypt_with_sha256_41(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_sha256_41 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_sha256_41), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_sha384_42() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_sha384_42)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_sha384_42() const { return ___sm2encrypt_with_sha384_42; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_sha384_42() { return &___sm2encrypt_with_sha384_42; }
	inline void set_sm2encrypt_with_sha384_42(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_sha384_42 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_sha384_42), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_sha512_43() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_sha512_43)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_sha512_43() const { return ___sm2encrypt_with_sha512_43; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_sha512_43() { return &___sm2encrypt_with_sha512_43; }
	inline void set_sm2encrypt_with_sha512_43(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_sha512_43 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_sha512_43), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_rmd160_44() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_rmd160_44)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_rmd160_44() const { return ___sm2encrypt_with_rmd160_44; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_rmd160_44() { return &___sm2encrypt_with_rmd160_44; }
	inline void set_sm2encrypt_with_rmd160_44(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_rmd160_44 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_rmd160_44), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_whirlpool_45() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_whirlpool_45)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_whirlpool_45() const { return ___sm2encrypt_with_whirlpool_45; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_whirlpool_45() { return &___sm2encrypt_with_whirlpool_45; }
	inline void set_sm2encrypt_with_whirlpool_45(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_whirlpool_45 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_whirlpool_45), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_blake2b512_46() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_blake2b512_46)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_blake2b512_46() const { return ___sm2encrypt_with_blake2b512_46; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_blake2b512_46() { return &___sm2encrypt_with_blake2b512_46; }
	inline void set_sm2encrypt_with_blake2b512_46(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_blake2b512_46 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_blake2b512_46), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_blake2s256_47() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_blake2s256_47)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_blake2s256_47() const { return ___sm2encrypt_with_blake2s256_47; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_blake2s256_47() { return &___sm2encrypt_with_blake2s256_47; }
	inline void set_sm2encrypt_with_blake2s256_47(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_blake2s256_47 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_blake2s256_47), value);
	}

	inline static int32_t get_offset_of_sm2encrypt_with_md5_48() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2encrypt_with_md5_48)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2encrypt_with_md5_48() const { return ___sm2encrypt_with_md5_48; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2encrypt_with_md5_48() { return &___sm2encrypt_with_md5_48; }
	inline void set_sm2encrypt_with_md5_48(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2encrypt_with_md5_48 = value;
		Il2CppCodeGenWriteBarrier((&___sm2encrypt_with_md5_48), value);
	}

	inline static int32_t get_offset_of_id_sm9PublicKey_49() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___id_sm9PublicKey_49)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_id_sm9PublicKey_49() const { return ___id_sm9PublicKey_49; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_id_sm9PublicKey_49() { return &___id_sm9PublicKey_49; }
	inline void set_id_sm9PublicKey_49(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___id_sm9PublicKey_49 = value;
		Il2CppCodeGenWriteBarrier((&___id_sm9PublicKey_49), value);
	}

	inline static int32_t get_offset_of_sm9sign_50() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm9sign_50)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm9sign_50() const { return ___sm9sign_50; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm9sign_50() { return &___sm9sign_50; }
	inline void set_sm9sign_50(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm9sign_50 = value;
		Il2CppCodeGenWriteBarrier((&___sm9sign_50), value);
	}

	inline static int32_t get_offset_of_sm9keyagreement_51() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm9keyagreement_51)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm9keyagreement_51() const { return ___sm9keyagreement_51; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm9keyagreement_51() { return &___sm9keyagreement_51; }
	inline void set_sm9keyagreement_51(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm9keyagreement_51 = value;
		Il2CppCodeGenWriteBarrier((&___sm9keyagreement_51), value);
	}

	inline static int32_t get_offset_of_sm9encrypt_52() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm9encrypt_52)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm9encrypt_52() const { return ___sm9encrypt_52; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm9encrypt_52() { return &___sm9encrypt_52; }
	inline void set_sm9encrypt_52(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm9encrypt_52 = value;
		Il2CppCodeGenWriteBarrier((&___sm9encrypt_52), value);
	}

	inline static int32_t get_offset_of_sm3_53() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm3_53)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm3_53() const { return ___sm3_53; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm3_53() { return &___sm3_53; }
	inline void set_sm3_53(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm3_53 = value;
		Il2CppCodeGenWriteBarrier((&___sm3_53), value);
	}

	inline static int32_t get_offset_of_hmac_sm3_54() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___hmac_sm3_54)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_hmac_sm3_54() const { return ___hmac_sm3_54; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_hmac_sm3_54() { return &___hmac_sm3_54; }
	inline void set_hmac_sm3_54(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___hmac_sm3_54 = value;
		Il2CppCodeGenWriteBarrier((&___hmac_sm3_54), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_sm3_55() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_sm3_55)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_sm3_55() const { return ___sm2sign_with_sm3_55; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_sm3_55() { return &___sm2sign_with_sm3_55; }
	inline void set_sm2sign_with_sm3_55(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_sm3_55 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_sm3_55), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_sha1_56() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_sha1_56)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_sha1_56() const { return ___sm2sign_with_sha1_56; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_sha1_56() { return &___sm2sign_with_sha1_56; }
	inline void set_sm2sign_with_sha1_56(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_sha1_56 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_sha1_56), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_sha256_57() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_sha256_57)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_sha256_57() const { return ___sm2sign_with_sha256_57; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_sha256_57() { return &___sm2sign_with_sha256_57; }
	inline void set_sm2sign_with_sha256_57(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_sha256_57 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_sha256_57), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_sha512_58() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_sha512_58)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_sha512_58() const { return ___sm2sign_with_sha512_58; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_sha512_58() { return &___sm2sign_with_sha512_58; }
	inline void set_sm2sign_with_sha512_58(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_sha512_58 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_sha512_58), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_sha224_59() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_sha224_59)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_sha224_59() const { return ___sm2sign_with_sha224_59; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_sha224_59() { return &___sm2sign_with_sha224_59; }
	inline void set_sm2sign_with_sha224_59(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_sha224_59 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_sha224_59), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_sha384_60() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_sha384_60)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_sha384_60() const { return ___sm2sign_with_sha384_60; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_sha384_60() { return &___sm2sign_with_sha384_60; }
	inline void set_sm2sign_with_sha384_60(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_sha384_60 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_sha384_60), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_rmd160_61() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_rmd160_61)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_rmd160_61() const { return ___sm2sign_with_rmd160_61; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_rmd160_61() { return &___sm2sign_with_rmd160_61; }
	inline void set_sm2sign_with_rmd160_61(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_rmd160_61 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_rmd160_61), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_whirlpool_62() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_whirlpool_62)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_whirlpool_62() const { return ___sm2sign_with_whirlpool_62; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_whirlpool_62() { return &___sm2sign_with_whirlpool_62; }
	inline void set_sm2sign_with_whirlpool_62(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_whirlpool_62 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_whirlpool_62), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_blake2b512_63() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_blake2b512_63)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_blake2b512_63() const { return ___sm2sign_with_blake2b512_63; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_blake2b512_63() { return &___sm2sign_with_blake2b512_63; }
	inline void set_sm2sign_with_blake2b512_63(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_blake2b512_63 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_blake2b512_63), value);
	}

	inline static int32_t get_offset_of_sm2sign_with_blake2s256_64() { return static_cast<int32_t>(offsetof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields, ___sm2sign_with_blake2s256_64)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sm2sign_with_blake2s256_64() const { return ___sm2sign_with_blake2s256_64; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sm2sign_with_blake2s256_64() { return &___sm2sign_with_blake2s256_64; }
	inline void set_sm2sign_with_blake2s256_64(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sm2sign_with_blake2s256_64 = value;
		Il2CppCodeGenWriteBarrier((&___sm2sign_with_blake2s256_64), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GMOBJECTIDENTIFIERS_T4B6050E725BA3831B60398A4586BE6B260B6EEF7_H
#ifndef GNUOBJECTIDENTIFIERS_T848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_H
#define GNUOBJECTIDENTIFIERS_T848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers
struct  GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC  : public RuntimeObject
{
public:

public:
};

struct GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Gnu
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Gnu_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::GnuPG
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GnuPG_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Notation
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Notation_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::PkaAddress
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___PkaAddress_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::GnuRadar
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___GnuRadar_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::DigestAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___DigestAlgorithm_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Tiger192
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Tiger192_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::EncryptionAlgorithm
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EncryptionAlgorithm_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent128Ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent128Ecb_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent128Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent128Cbc_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent128Ofb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent128Ofb_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent128Cfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent128Cfb_12;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent192Ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent192Ecb_13;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent192Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent192Cbc_14;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent192Ofb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent192Ofb_15;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent192Cfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent192Cfb_16;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent256Ecb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent256Ecb_17;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent256Cbc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent256Cbc_18;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent256Ofb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent256Ofb_19;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Serpent256Cfb
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Serpent256Cfb_20;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Crc
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Crc_21;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Crc32
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Crc32_22;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::EllipticCurve
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___EllipticCurve_23;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Gnu.GnuObjectIdentifiers::Ed25519
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___Ed25519_24;

public:
	inline static int32_t get_offset_of_Gnu_0() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Gnu_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Gnu_0() const { return ___Gnu_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Gnu_0() { return &___Gnu_0; }
	inline void set_Gnu_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Gnu_0 = value;
		Il2CppCodeGenWriteBarrier((&___Gnu_0), value);
	}

	inline static int32_t get_offset_of_GnuPG_1() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___GnuPG_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GnuPG_1() const { return ___GnuPG_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GnuPG_1() { return &___GnuPG_1; }
	inline void set_GnuPG_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GnuPG_1 = value;
		Il2CppCodeGenWriteBarrier((&___GnuPG_1), value);
	}

	inline static int32_t get_offset_of_Notation_2() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Notation_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Notation_2() const { return ___Notation_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Notation_2() { return &___Notation_2; }
	inline void set_Notation_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Notation_2 = value;
		Il2CppCodeGenWriteBarrier((&___Notation_2), value);
	}

	inline static int32_t get_offset_of_PkaAddress_3() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___PkaAddress_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_PkaAddress_3() const { return ___PkaAddress_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_PkaAddress_3() { return &___PkaAddress_3; }
	inline void set_PkaAddress_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___PkaAddress_3 = value;
		Il2CppCodeGenWriteBarrier((&___PkaAddress_3), value);
	}

	inline static int32_t get_offset_of_GnuRadar_4() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___GnuRadar_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_GnuRadar_4() const { return ___GnuRadar_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_GnuRadar_4() { return &___GnuRadar_4; }
	inline void set_GnuRadar_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___GnuRadar_4 = value;
		Il2CppCodeGenWriteBarrier((&___GnuRadar_4), value);
	}

	inline static int32_t get_offset_of_DigestAlgorithm_5() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___DigestAlgorithm_5)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_DigestAlgorithm_5() const { return ___DigestAlgorithm_5; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_DigestAlgorithm_5() { return &___DigestAlgorithm_5; }
	inline void set_DigestAlgorithm_5(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___DigestAlgorithm_5 = value;
		Il2CppCodeGenWriteBarrier((&___DigestAlgorithm_5), value);
	}

	inline static int32_t get_offset_of_Tiger192_6() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Tiger192_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Tiger192_6() const { return ___Tiger192_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Tiger192_6() { return &___Tiger192_6; }
	inline void set_Tiger192_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Tiger192_6 = value;
		Il2CppCodeGenWriteBarrier((&___Tiger192_6), value);
	}

	inline static int32_t get_offset_of_EncryptionAlgorithm_7() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___EncryptionAlgorithm_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EncryptionAlgorithm_7() const { return ___EncryptionAlgorithm_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EncryptionAlgorithm_7() { return &___EncryptionAlgorithm_7; }
	inline void set_EncryptionAlgorithm_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EncryptionAlgorithm_7 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptionAlgorithm_7), value);
	}

	inline static int32_t get_offset_of_Serpent_8() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent_8() const { return ___Serpent_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent_8() { return &___Serpent_8; }
	inline void set_Serpent_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent_8 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent_8), value);
	}

	inline static int32_t get_offset_of_Serpent128Ecb_9() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent128Ecb_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent128Ecb_9() const { return ___Serpent128Ecb_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent128Ecb_9() { return &___Serpent128Ecb_9; }
	inline void set_Serpent128Ecb_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent128Ecb_9 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent128Ecb_9), value);
	}

	inline static int32_t get_offset_of_Serpent128Cbc_10() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent128Cbc_10)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent128Cbc_10() const { return ___Serpent128Cbc_10; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent128Cbc_10() { return &___Serpent128Cbc_10; }
	inline void set_Serpent128Cbc_10(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent128Cbc_10 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent128Cbc_10), value);
	}

	inline static int32_t get_offset_of_Serpent128Ofb_11() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent128Ofb_11)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent128Ofb_11() const { return ___Serpent128Ofb_11; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent128Ofb_11() { return &___Serpent128Ofb_11; }
	inline void set_Serpent128Ofb_11(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent128Ofb_11 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent128Ofb_11), value);
	}

	inline static int32_t get_offset_of_Serpent128Cfb_12() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent128Cfb_12)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent128Cfb_12() const { return ___Serpent128Cfb_12; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent128Cfb_12() { return &___Serpent128Cfb_12; }
	inline void set_Serpent128Cfb_12(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent128Cfb_12 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent128Cfb_12), value);
	}

	inline static int32_t get_offset_of_Serpent192Ecb_13() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent192Ecb_13)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent192Ecb_13() const { return ___Serpent192Ecb_13; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent192Ecb_13() { return &___Serpent192Ecb_13; }
	inline void set_Serpent192Ecb_13(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent192Ecb_13 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent192Ecb_13), value);
	}

	inline static int32_t get_offset_of_Serpent192Cbc_14() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent192Cbc_14)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent192Cbc_14() const { return ___Serpent192Cbc_14; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent192Cbc_14() { return &___Serpent192Cbc_14; }
	inline void set_Serpent192Cbc_14(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent192Cbc_14 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent192Cbc_14), value);
	}

	inline static int32_t get_offset_of_Serpent192Ofb_15() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent192Ofb_15)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent192Ofb_15() const { return ___Serpent192Ofb_15; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent192Ofb_15() { return &___Serpent192Ofb_15; }
	inline void set_Serpent192Ofb_15(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent192Ofb_15 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent192Ofb_15), value);
	}

	inline static int32_t get_offset_of_Serpent192Cfb_16() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent192Cfb_16)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent192Cfb_16() const { return ___Serpent192Cfb_16; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent192Cfb_16() { return &___Serpent192Cfb_16; }
	inline void set_Serpent192Cfb_16(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent192Cfb_16 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent192Cfb_16), value);
	}

	inline static int32_t get_offset_of_Serpent256Ecb_17() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent256Ecb_17)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent256Ecb_17() const { return ___Serpent256Ecb_17; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent256Ecb_17() { return &___Serpent256Ecb_17; }
	inline void set_Serpent256Ecb_17(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent256Ecb_17 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent256Ecb_17), value);
	}

	inline static int32_t get_offset_of_Serpent256Cbc_18() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent256Cbc_18)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent256Cbc_18() const { return ___Serpent256Cbc_18; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent256Cbc_18() { return &___Serpent256Cbc_18; }
	inline void set_Serpent256Cbc_18(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent256Cbc_18 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent256Cbc_18), value);
	}

	inline static int32_t get_offset_of_Serpent256Ofb_19() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent256Ofb_19)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent256Ofb_19() const { return ___Serpent256Ofb_19; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent256Ofb_19() { return &___Serpent256Ofb_19; }
	inline void set_Serpent256Ofb_19(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent256Ofb_19 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent256Ofb_19), value);
	}

	inline static int32_t get_offset_of_Serpent256Cfb_20() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Serpent256Cfb_20)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Serpent256Cfb_20() const { return ___Serpent256Cfb_20; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Serpent256Cfb_20() { return &___Serpent256Cfb_20; }
	inline void set_Serpent256Cfb_20(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Serpent256Cfb_20 = value;
		Il2CppCodeGenWriteBarrier((&___Serpent256Cfb_20), value);
	}

	inline static int32_t get_offset_of_Crc_21() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Crc_21)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Crc_21() const { return ___Crc_21; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Crc_21() { return &___Crc_21; }
	inline void set_Crc_21(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Crc_21 = value;
		Il2CppCodeGenWriteBarrier((&___Crc_21), value);
	}

	inline static int32_t get_offset_of_Crc32_22() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Crc32_22)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Crc32_22() const { return ___Crc32_22; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Crc32_22() { return &___Crc32_22; }
	inline void set_Crc32_22(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Crc32_22 = value;
		Il2CppCodeGenWriteBarrier((&___Crc32_22), value);
	}

	inline static int32_t get_offset_of_EllipticCurve_23() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___EllipticCurve_23)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_EllipticCurve_23() const { return ___EllipticCurve_23; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_EllipticCurve_23() { return &___EllipticCurve_23; }
	inline void set_EllipticCurve_23(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___EllipticCurve_23 = value;
		Il2CppCodeGenWriteBarrier((&___EllipticCurve_23), value);
	}

	inline static int32_t get_offset_of_Ed25519_24() { return static_cast<int32_t>(offsetof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields, ___Ed25519_24)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_Ed25519_24() const { return ___Ed25519_24; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_Ed25519_24() { return &___Ed25519_24; }
	inline void set_Ed25519_24(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___Ed25519_24 = value;
		Il2CppCodeGenWriteBarrier((&___Ed25519_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GNUOBJECTIDENTIFIERS_T848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_H
#ifndef IANAOBJECTIDENTIFIERS_T628BEE30F309063831C36EBBFDB4F2F39911442E_H
#define IANAOBJECTIDENTIFIERS_T628BEE30F309063831C36EBBFDB4F2F39911442E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers
struct  IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E  : public RuntimeObject
{
public:

public:
};

struct IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::IsakmpOakley
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___IsakmpOakley_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacMD5
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___HmacMD5_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacSha1
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___HmacSha1_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacTiger
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___HmacTiger_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Iana.IanaObjectIdentifiers::HmacRipeMD160
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___HmacRipeMD160_4;

public:
	inline static int32_t get_offset_of_IsakmpOakley_0() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields, ___IsakmpOakley_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_IsakmpOakley_0() const { return ___IsakmpOakley_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_IsakmpOakley_0() { return &___IsakmpOakley_0; }
	inline void set_IsakmpOakley_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___IsakmpOakley_0 = value;
		Il2CppCodeGenWriteBarrier((&___IsakmpOakley_0), value);
	}

	inline static int32_t get_offset_of_HmacMD5_1() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields, ___HmacMD5_1)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_HmacMD5_1() const { return ___HmacMD5_1; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_HmacMD5_1() { return &___HmacMD5_1; }
	inline void set_HmacMD5_1(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___HmacMD5_1 = value;
		Il2CppCodeGenWriteBarrier((&___HmacMD5_1), value);
	}

	inline static int32_t get_offset_of_HmacSha1_2() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields, ___HmacSha1_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_HmacSha1_2() const { return ___HmacSha1_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_HmacSha1_2() { return &___HmacSha1_2; }
	inline void set_HmacSha1_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___HmacSha1_2 = value;
		Il2CppCodeGenWriteBarrier((&___HmacSha1_2), value);
	}

	inline static int32_t get_offset_of_HmacTiger_3() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields, ___HmacTiger_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_HmacTiger_3() const { return ___HmacTiger_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_HmacTiger_3() { return &___HmacTiger_3; }
	inline void set_HmacTiger_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___HmacTiger_3 = value;
		Il2CppCodeGenWriteBarrier((&___HmacTiger_3), value);
	}

	inline static int32_t get_offset_of_HmacRipeMD160_4() { return static_cast<int32_t>(offsetof(IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields, ___HmacRipeMD160_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_HmacRipeMD160_4() const { return ___HmacRipeMD160_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_HmacRipeMD160_4() { return &___HmacRipeMD160_4; }
	inline void set_HmacRipeMD160_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___HmacRipeMD160_4 = value;
		Il2CppCodeGenWriteBarrier((&___HmacRipeMD160_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IANAOBJECTIDENTIFIERS_T628BEE30F309063831C36EBBFDB4F2F39911442E_H
#ifndef X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#define X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder
struct  X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder::parameters
	X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB, ___parameters_0)); }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * get_parameters_0() const { return ___parameters_0; }
	inline X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(X9ECParameters_t87A644D587E32F7498181513E6DADDB7F7AC4A86 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X9ECPARAMETERSHOLDER_TA26C05F393965FC0677EE16D76EFCF698FCDEFCB_H
#ifndef ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#define ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object
struct  Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1OBJECT_T20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E_H
#ifndef ATTRIBUTE_TEB73DC2A1B61AE4935F463D0E09A6D976D724B34_H
#define ATTRIBUTE_TEB73DC2A1B61AE4935F463D0E09A6D976D724B34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attribute
struct  Attribute_tEB73DC2A1B61AE4935F463D0E09A6D976D724B34  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attribute::attrType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___attrType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attribute::attrValues
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attrValues_3;

public:
	inline static int32_t get_offset_of_attrType_2() { return static_cast<int32_t>(offsetof(Attribute_tEB73DC2A1B61AE4935F463D0E09A6D976D724B34, ___attrType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_attrType_2() const { return ___attrType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_attrType_2() { return &___attrType_2; }
	inline void set_attrType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___attrType_2 = value;
		Il2CppCodeGenWriteBarrier((&___attrType_2), value);
	}

	inline static int32_t get_offset_of_attrValues_3() { return static_cast<int32_t>(offsetof(Attribute_tEB73DC2A1B61AE4935F463D0E09A6D976D724B34, ___attrValues_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attrValues_3() const { return ___attrValues_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attrValues_3() { return &___attrValues_3; }
	inline void set_attrValues_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attrValues_3 = value;
		Il2CppCodeGenWriteBarrier((&___attrValues_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TEB73DC2A1B61AE4935F463D0E09A6D976D724B34_H
#ifndef ATTRIBUTES_T39AE0974185A2F49463E4112262D3CEC2CB9C7F0_H
#define ATTRIBUTES_T39AE0974185A2F49463E4112262D3CEC2CB9C7F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attributes
struct  Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attributes::attributes
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___attributes_2;

public:
	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0, ___attributes_2)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_attributes_2() const { return ___attributes_2; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTES_T39AE0974185A2F49463E4112262D3CEC2CB9C7F0_H
#ifndef AUTHENVELOPEDDATA_T7224956C3F79DB63620A87C1E332445EC3F8D172_H
#define AUTHENVELOPEDDATA_T7224956C3F79DB63620A87C1E332445EC3F8D172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData
struct  AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::originatorInfo
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * ___originatorInfo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::recipientInfos
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___recipientInfos_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::authEncryptedContentInfo
	EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * ___authEncryptedContentInfo_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::authAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___authAttrs_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::mac
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___mac_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthEnvelopedData::unauthAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unauthAttrs_8;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_originatorInfo_3() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___originatorInfo_3)); }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * get_originatorInfo_3() const { return ___originatorInfo_3; }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F ** get_address_of_originatorInfo_3() { return &___originatorInfo_3; }
	inline void set_originatorInfo_3(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * value)
	{
		___originatorInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___originatorInfo_3), value);
	}

	inline static int32_t get_offset_of_recipientInfos_4() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___recipientInfos_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_recipientInfos_4() const { return ___recipientInfos_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_recipientInfos_4() { return &___recipientInfos_4; }
	inline void set_recipientInfos_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___recipientInfos_4 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfos_4), value);
	}

	inline static int32_t get_offset_of_authEncryptedContentInfo_5() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___authEncryptedContentInfo_5)); }
	inline EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * get_authEncryptedContentInfo_5() const { return ___authEncryptedContentInfo_5; }
	inline EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E ** get_address_of_authEncryptedContentInfo_5() { return &___authEncryptedContentInfo_5; }
	inline void set_authEncryptedContentInfo_5(EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * value)
	{
		___authEncryptedContentInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___authEncryptedContentInfo_5), value);
	}

	inline static int32_t get_offset_of_authAttrs_6() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___authAttrs_6)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_authAttrs_6() const { return ___authAttrs_6; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_authAttrs_6() { return &___authAttrs_6; }
	inline void set_authAttrs_6(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___authAttrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___authAttrs_6), value);
	}

	inline static int32_t get_offset_of_mac_7() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___mac_7)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_mac_7() const { return ___mac_7; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_mac_7() { return &___mac_7; }
	inline void set_mac_7(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___mac_7 = value;
		Il2CppCodeGenWriteBarrier((&___mac_7), value);
	}

	inline static int32_t get_offset_of_unauthAttrs_8() { return static_cast<int32_t>(offsetof(AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172, ___unauthAttrs_8)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unauthAttrs_8() const { return ___unauthAttrs_8; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unauthAttrs_8() { return &___unauthAttrs_8; }
	inline void set_unauthAttrs_8(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unauthAttrs_8 = value;
		Il2CppCodeGenWriteBarrier((&___unauthAttrs_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENVELOPEDDATA_T7224956C3F79DB63620A87C1E332445EC3F8D172_H
#ifndef AUTHENTICATEDDATA_T9AA4C6281375BFF1CC345E32B6088F95DC803EA1_H
#define AUTHENTICATEDDATA_T9AA4C6281375BFF1CC345E32B6088F95DC803EA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData
struct  AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::originatorInfo
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * ___originatorInfo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::recipientInfos
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___recipientInfos_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::macAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___macAlgorithm_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::digestAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___digestAlgorithm_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::encapsulatedContentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___encapsulatedContentInfo_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::authAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___authAttrs_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::mac
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___mac_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.AuthenticatedData::unauthAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unauthAttrs_10;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_originatorInfo_3() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___originatorInfo_3)); }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * get_originatorInfo_3() const { return ___originatorInfo_3; }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F ** get_address_of_originatorInfo_3() { return &___originatorInfo_3; }
	inline void set_originatorInfo_3(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * value)
	{
		___originatorInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___originatorInfo_3), value);
	}

	inline static int32_t get_offset_of_recipientInfos_4() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___recipientInfos_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_recipientInfos_4() const { return ___recipientInfos_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_recipientInfos_4() { return &___recipientInfos_4; }
	inline void set_recipientInfos_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___recipientInfos_4 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfos_4), value);
	}

	inline static int32_t get_offset_of_macAlgorithm_5() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___macAlgorithm_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_macAlgorithm_5() const { return ___macAlgorithm_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_macAlgorithm_5() { return &___macAlgorithm_5; }
	inline void set_macAlgorithm_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___macAlgorithm_5 = value;
		Il2CppCodeGenWriteBarrier((&___macAlgorithm_5), value);
	}

	inline static int32_t get_offset_of_digestAlgorithm_6() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___digestAlgorithm_6)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_digestAlgorithm_6() const { return ___digestAlgorithm_6; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_digestAlgorithm_6() { return &___digestAlgorithm_6; }
	inline void set_digestAlgorithm_6(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___digestAlgorithm_6 = value;
		Il2CppCodeGenWriteBarrier((&___digestAlgorithm_6), value);
	}

	inline static int32_t get_offset_of_encapsulatedContentInfo_7() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___encapsulatedContentInfo_7)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_encapsulatedContentInfo_7() const { return ___encapsulatedContentInfo_7; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_encapsulatedContentInfo_7() { return &___encapsulatedContentInfo_7; }
	inline void set_encapsulatedContentInfo_7(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___encapsulatedContentInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___encapsulatedContentInfo_7), value);
	}

	inline static int32_t get_offset_of_authAttrs_8() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___authAttrs_8)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_authAttrs_8() const { return ___authAttrs_8; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_authAttrs_8() { return &___authAttrs_8; }
	inline void set_authAttrs_8(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___authAttrs_8 = value;
		Il2CppCodeGenWriteBarrier((&___authAttrs_8), value);
	}

	inline static int32_t get_offset_of_mac_9() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___mac_9)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_mac_9() const { return ___mac_9; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_mac_9() { return &___mac_9; }
	inline void set_mac_9(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___mac_9 = value;
		Il2CppCodeGenWriteBarrier((&___mac_9), value);
	}

	inline static int32_t get_offset_of_unauthAttrs_10() { return static_cast<int32_t>(offsetof(AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1, ___unauthAttrs_10)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unauthAttrs_10() const { return ___unauthAttrs_10; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unauthAttrs_10() { return &___unauthAttrs_10; }
	inline void set_unauthAttrs_10(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unauthAttrs_10 = value;
		Il2CppCodeGenWriteBarrier((&___unauthAttrs_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATEDDATA_T9AA4C6281375BFF1CC345E32B6088F95DC803EA1_H
#ifndef COMPRESSEDDATA_T3EA0EE1F60A13558F587E3D4CACA08B001739888_H
#define COMPRESSEDDATA_T3EA0EE1F60A13558F587E3D4CACA08B001739888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedData
struct  CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedData::compressionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___compressionAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.CompressedData::encapContentInfo
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * ___encapContentInfo_4;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_compressionAlgorithm_3() { return static_cast<int32_t>(offsetof(CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888, ___compressionAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_compressionAlgorithm_3() const { return ___compressionAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_compressionAlgorithm_3() { return &___compressionAlgorithm_3; }
	inline void set_compressionAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___compressionAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___compressionAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_encapContentInfo_4() { return static_cast<int32_t>(offsetof(CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888, ___encapContentInfo_4)); }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * get_encapContentInfo_4() const { return ___encapContentInfo_4; }
	inline ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 ** get_address_of_encapContentInfo_4() { return &___encapContentInfo_4; }
	inline void set_encapContentInfo_4(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42 * value)
	{
		___encapContentInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___encapContentInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSEDDATA_T3EA0EE1F60A13558F587E3D4CACA08B001739888_H
#ifndef CONTENTINFO_T0CE96735DA3BC2263C09F12714466925DD4A1B42_H
#define CONTENTINFO_T0CE96735DA3BC2263C09F12714466925DD4A1B42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo
struct  ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo::contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___contentType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.ContentInfo::content
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___content_3;

public:
	inline static int32_t get_offset_of_contentType_2() { return static_cast<int32_t>(offsetof(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42, ___contentType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_contentType_2() const { return ___contentType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_contentType_2() { return &___contentType_2; }
	inline void set_contentType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___contentType_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_2), value);
	}

	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42, ___content_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_content_3() const { return ___content_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier((&___content_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTINFO_T0CE96735DA3BC2263C09F12714466925DD4A1B42_H
#ifndef ENCRYPTEDCONTENTINFO_TCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E_H
#define ENCRYPTEDCONTENTINFO_TCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo
struct  EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo::contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___contentType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo::contentEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___contentEncryptionAlgorithm_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo::encryptedContent
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedContent_4;

public:
	inline static int32_t get_offset_of_contentType_2() { return static_cast<int32_t>(offsetof(EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E, ___contentType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_contentType_2() const { return ___contentType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_contentType_2() { return &___contentType_2; }
	inline void set_contentType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___contentType_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_2), value);
	}

	inline static int32_t get_offset_of_contentEncryptionAlgorithm_3() { return static_cast<int32_t>(offsetof(EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E, ___contentEncryptionAlgorithm_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_contentEncryptionAlgorithm_3() const { return ___contentEncryptionAlgorithm_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_contentEncryptionAlgorithm_3() { return &___contentEncryptionAlgorithm_3; }
	inline void set_contentEncryptionAlgorithm_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___contentEncryptionAlgorithm_3 = value;
		Il2CppCodeGenWriteBarrier((&___contentEncryptionAlgorithm_3), value);
	}

	inline static int32_t get_offset_of_encryptedContent_4() { return static_cast<int32_t>(offsetof(EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E, ___encryptedContent_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedContent_4() const { return ___encryptedContent_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedContent_4() { return &___encryptedContent_4; }
	inline void set_encryptedContent_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedContent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDCONTENTINFO_TCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E_H
#ifndef ENCRYPTEDDATA_T4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44_H
#define ENCRYPTEDDATA_T4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedData
struct  EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedData::encryptedContentInfo
	EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * ___encryptedContentInfo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedData::unprotectedAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unprotectedAttrs_4;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_encryptedContentInfo_3() { return static_cast<int32_t>(offsetof(EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44, ___encryptedContentInfo_3)); }
	inline EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * get_encryptedContentInfo_3() const { return ___encryptedContentInfo_3; }
	inline EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E ** get_address_of_encryptedContentInfo_3() { return &___encryptedContentInfo_3; }
	inline void set_encryptedContentInfo_3(EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * value)
	{
		___encryptedContentInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedContentInfo_3), value);
	}

	inline static int32_t get_offset_of_unprotectedAttrs_4() { return static_cast<int32_t>(offsetof(EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44, ___unprotectedAttrs_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unprotectedAttrs_4() const { return ___unprotectedAttrs_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unprotectedAttrs_4() { return &___unprotectedAttrs_4; }
	inline void set_unprotectedAttrs_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unprotectedAttrs_4 = value;
		Il2CppCodeGenWriteBarrier((&___unprotectedAttrs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDDATA_T4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44_H
#ifndef ENVELOPEDDATA_T8827C3D99E7EC283E429E5C8E535E134F7DD9594_H
#define ENVELOPEDDATA_T8827C3D99E7EC283E429E5C8E535E134F7DD9594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData
struct  EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData::originatorInfo
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * ___originatorInfo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData::recipientInfos
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___recipientInfos_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EncryptedContentInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData::encryptedContentInfo
	EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * ___encryptedContentInfo_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData::unprotectedAttrs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___unprotectedAttrs_6;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_originatorInfo_3() { return static_cast<int32_t>(offsetof(EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594, ___originatorInfo_3)); }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * get_originatorInfo_3() const { return ___originatorInfo_3; }
	inline OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F ** get_address_of_originatorInfo_3() { return &___originatorInfo_3; }
	inline void set_originatorInfo_3(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F * value)
	{
		___originatorInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___originatorInfo_3), value);
	}

	inline static int32_t get_offset_of_recipientInfos_4() { return static_cast<int32_t>(offsetof(EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594, ___recipientInfos_4)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_recipientInfos_4() const { return ___recipientInfos_4; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_recipientInfos_4() { return &___recipientInfos_4; }
	inline void set_recipientInfos_4(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___recipientInfos_4 = value;
		Il2CppCodeGenWriteBarrier((&___recipientInfos_4), value);
	}

	inline static int32_t get_offset_of_encryptedContentInfo_5() { return static_cast<int32_t>(offsetof(EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594, ___encryptedContentInfo_5)); }
	inline EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * get_encryptedContentInfo_5() const { return ___encryptedContentInfo_5; }
	inline EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E ** get_address_of_encryptedContentInfo_5() { return &___encryptedContentInfo_5; }
	inline void set_encryptedContentInfo_5(EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E * value)
	{
		___encryptedContentInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedContentInfo_5), value);
	}

	inline static int32_t get_offset_of_unprotectedAttrs_6() { return static_cast<int32_t>(offsetof(EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594, ___unprotectedAttrs_6)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_unprotectedAttrs_6() const { return ___unprotectedAttrs_6; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_unprotectedAttrs_6() { return &___unprotectedAttrs_6; }
	inline void set_unprotectedAttrs_6(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___unprotectedAttrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___unprotectedAttrs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVELOPEDDATA_T8827C3D99E7EC283E429E5C8E535E134F7DD9594_H
#ifndef EVIDENCE_T6897C267406A516689941A0FF07964FF20B44F07_H
#define EVIDENCE_T6897C267406A516689941A0FF07964FF20B44F07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Evidence
struct  Evidence_t6897C267406A516689941A0FF07964FF20B44F07  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.TimeStampTokenEvidence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Evidence::tstEvidence
	TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98 * ___tstEvidence_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Evidence::otherEvidence
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___otherEvidence_3;

public:
	inline static int32_t get_offset_of_tstEvidence_2() { return static_cast<int32_t>(offsetof(Evidence_t6897C267406A516689941A0FF07964FF20B44F07, ___tstEvidence_2)); }
	inline TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98 * get_tstEvidence_2() const { return ___tstEvidence_2; }
	inline TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98 ** get_address_of_tstEvidence_2() { return &___tstEvidence_2; }
	inline void set_tstEvidence_2(TimeStampTokenEvidence_t20CF5580B1920E8F80264B9321AC3D1AF7A02B98 * value)
	{
		___tstEvidence_2 = value;
		Il2CppCodeGenWriteBarrier((&___tstEvidence_2), value);
	}

	inline static int32_t get_offset_of_otherEvidence_3() { return static_cast<int32_t>(offsetof(Evidence_t6897C267406A516689941A0FF07964FF20B44F07, ___otherEvidence_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_otherEvidence_3() const { return ___otherEvidence_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_otherEvidence_3() { return &___otherEvidence_3; }
	inline void set_otherEvidence_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___otherEvidence_3 = value;
		Il2CppCodeGenWriteBarrier((&___otherEvidence_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVIDENCE_T6897C267406A516689941A0FF07964FF20B44F07_H
#ifndef ISSUERANDSERIALNUMBER_T6159295C18C7D8295C75D0244C193FF88B2262BC_H
#define ISSUERANDSERIALNUMBER_T6159295C18C7D8295C75D0244C193FF88B2262BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber
struct  IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber::name
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___name_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC, ___name_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_name_2() const { return ___name_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_serialNumber_3() { return static_cast<int32_t>(offsetof(IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC, ___serialNumber_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_3() const { return ___serialNumber_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_3() { return &___serialNumber_3; }
	inline void set_serialNumber_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_3 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISSUERANDSERIALNUMBER_T6159295C18C7D8295C75D0244C193FF88B2262BC_H
#ifndef KEKIDENTIFIER_T13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658_H
#define KEKIDENTIFIER_T13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier
struct  KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier::keyIdentifier
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___keyIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier::date
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___date_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OtherKeyAttribute BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier::other
	OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB * ___other_4;

public:
	inline static int32_t get_offset_of_keyIdentifier_2() { return static_cast<int32_t>(offsetof(KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658, ___keyIdentifier_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_keyIdentifier_2() const { return ___keyIdentifier_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_keyIdentifier_2() { return &___keyIdentifier_2; }
	inline void set_keyIdentifier_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___keyIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyIdentifier_2), value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658, ___date_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_date_3() const { return ___date_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___date_3 = value;
		Il2CppCodeGenWriteBarrier((&___date_3), value);
	}

	inline static int32_t get_offset_of_other_4() { return static_cast<int32_t>(offsetof(KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658, ___other_4)); }
	inline OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB * get_other_4() const { return ___other_4; }
	inline OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB ** get_address_of_other_4() { return &___other_4; }
	inline void set_other_4(OtherKeyAttribute_tACF39B83B18775C6C4011AB47D7F021E183E60AB * value)
	{
		___other_4 = value;
		Il2CppCodeGenWriteBarrier((&___other_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEKIDENTIFIER_T13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658_H
#ifndef KEKRECIPIENTINFO_T6BF48133D8AFC48D834E59F40E8EF723FC3604E6_H
#define KEKRECIPIENTINFO_T6BF48133D8AFC48D834E59F40E8EF723FC3604E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo
struct  KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo::kekID
	KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 * ___kekID_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo::keyEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyEncryptionAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KekRecipientInfo::encryptedKey
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedKey_5;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_kekID_3() { return static_cast<int32_t>(offsetof(KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6, ___kekID_3)); }
	inline KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 * get_kekID_3() const { return ___kekID_3; }
	inline KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 ** get_address_of_kekID_3() { return &___kekID_3; }
	inline void set_kekID_3(KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658 * value)
	{
		___kekID_3 = value;
		Il2CppCodeGenWriteBarrier((&___kekID_3), value);
	}

	inline static int32_t get_offset_of_keyEncryptionAlgorithm_4() { return static_cast<int32_t>(offsetof(KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6, ___keyEncryptionAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyEncryptionAlgorithm_4() const { return ___keyEncryptionAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyEncryptionAlgorithm_4() { return &___keyEncryptionAlgorithm_4; }
	inline void set_keyEncryptionAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyEncryptionAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_encryptedKey_5() { return static_cast<int32_t>(offsetof(KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6, ___encryptedKey_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedKey_5() const { return ___encryptedKey_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedKey_5() { return &___encryptedKey_5; }
	inline void set_encryptedKey_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEKRECIPIENTINFO_T6BF48133D8AFC48D834E59F40E8EF723FC3604E6_H
#ifndef KEYAGREERECIPIENTIDENTIFIER_TE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9_H
#define KEYAGREERECIPIENTIDENTIFIER_TE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientIdentifier
struct  KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.IssuerAndSerialNumber BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientIdentifier::issuerSerial
	IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * ___issuerSerial_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientKeyIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientIdentifier::rKeyID
	RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164 * ___rKeyID_3;

public:
	inline static int32_t get_offset_of_issuerSerial_2() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9, ___issuerSerial_2)); }
	inline IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * get_issuerSerial_2() const { return ___issuerSerial_2; }
	inline IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC ** get_address_of_issuerSerial_2() { return &___issuerSerial_2; }
	inline void set_issuerSerial_2(IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC * value)
	{
		___issuerSerial_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuerSerial_2), value);
	}

	inline static int32_t get_offset_of_rKeyID_3() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9, ___rKeyID_3)); }
	inline RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164 * get_rKeyID_3() const { return ___rKeyID_3; }
	inline RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164 ** get_address_of_rKeyID_3() { return &___rKeyID_3; }
	inline void set_rKeyID_3(RecipientKeyIdentifier_t42AD99C1DBF8D28884B823B3315F51777F16C164 * value)
	{
		___rKeyID_3 = value;
		Il2CppCodeGenWriteBarrier((&___rKeyID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYAGREERECIPIENTIDENTIFIER_TE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9_H
#ifndef KEYAGREERECIPIENTINFO_TBC1E1FEE80DAF0E5C4E62E844BF522D59605420E_H
#define KEYAGREERECIPIENTINFO_TBC1E1FEE80DAF0E5C4E62E844BF522D59605420E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo
struct  KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorIdentifierOrKey BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo::originator
	OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E * ___originator_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo::ukm
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___ukm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo::keyEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyEncryptionAlgorithm_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyAgreeRecipientInfo::recipientEncryptedKeys
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___recipientEncryptedKeys_6;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_originator_3() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E, ___originator_3)); }
	inline OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E * get_originator_3() const { return ___originator_3; }
	inline OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E ** get_address_of_originator_3() { return &___originator_3; }
	inline void set_originator_3(OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E * value)
	{
		___originator_3 = value;
		Il2CppCodeGenWriteBarrier((&___originator_3), value);
	}

	inline static int32_t get_offset_of_ukm_4() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E, ___ukm_4)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_ukm_4() const { return ___ukm_4; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_ukm_4() { return &___ukm_4; }
	inline void set_ukm_4(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___ukm_4 = value;
		Il2CppCodeGenWriteBarrier((&___ukm_4), value);
	}

	inline static int32_t get_offset_of_keyEncryptionAlgorithm_5() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E, ___keyEncryptionAlgorithm_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyEncryptionAlgorithm_5() const { return ___keyEncryptionAlgorithm_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyEncryptionAlgorithm_5() { return &___keyEncryptionAlgorithm_5; }
	inline void set_keyEncryptionAlgorithm_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyEncryptionAlgorithm_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionAlgorithm_5), value);
	}

	inline static int32_t get_offset_of_recipientEncryptedKeys_6() { return static_cast<int32_t>(offsetof(KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E, ___recipientEncryptedKeys_6)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_recipientEncryptedKeys_6() const { return ___recipientEncryptedKeys_6; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_recipientEncryptedKeys_6() { return &___recipientEncryptedKeys_6; }
	inline void set_recipientEncryptedKeys_6(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___recipientEncryptedKeys_6 = value;
		Il2CppCodeGenWriteBarrier((&___recipientEncryptedKeys_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYAGREERECIPIENTINFO_TBC1E1FEE80DAF0E5C4E62E844BF522D59605420E_H
#ifndef KEYTRANSRECIPIENTINFO_T2048CFC2470F33A81481679C4FE23F005D96A8F9_H
#define KEYTRANSRECIPIENTINFO_T2048CFC2470F33A81481679C4FE23F005D96A8F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo
struct  KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.RecipientIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo::rid
	RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570 * ___rid_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo::keyEncryptionAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyEncryptionAlgorithm_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.KeyTransRecipientInfo::encryptedKey
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___encryptedKey_5;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9, ___version_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_2() const { return ___version_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_rid_3() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9, ___rid_3)); }
	inline RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570 * get_rid_3() const { return ___rid_3; }
	inline RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570 ** get_address_of_rid_3() { return &___rid_3; }
	inline void set_rid_3(RecipientIdentifier_t774592C8CB7234B60B03C10B73D4964324CCF570 * value)
	{
		___rid_3 = value;
		Il2CppCodeGenWriteBarrier((&___rid_3), value);
	}

	inline static int32_t get_offset_of_keyEncryptionAlgorithm_4() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9, ___keyEncryptionAlgorithm_4)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyEncryptionAlgorithm_4() const { return ___keyEncryptionAlgorithm_4; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyEncryptionAlgorithm_4() { return &___keyEncryptionAlgorithm_4; }
	inline void set_keyEncryptionAlgorithm_4(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyEncryptionAlgorithm_4 = value;
		Il2CppCodeGenWriteBarrier((&___keyEncryptionAlgorithm_4), value);
	}

	inline static int32_t get_offset_of_encryptedKey_5() { return static_cast<int32_t>(offsetof(KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9, ___encryptedKey_5)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_encryptedKey_5() const { return ___encryptedKey_5; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_encryptedKey_5() { return &___encryptedKey_5; }
	inline void set_encryptedKey_5(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___encryptedKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYTRANSRECIPIENTINFO_T2048CFC2470F33A81481679C4FE23F005D96A8F9_H
#ifndef METADATA_T43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A_H
#define METADATA_T43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData
struct  MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBoolean BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData::hashProtected
	DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * ___hashProtected_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData::fileName
	DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * ___fileName_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerIA5String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData::mediaType
	DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * ___mediaType_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.Attributes BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.MetaData::otherMetaData
	Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0 * ___otherMetaData_5;

public:
	inline static int32_t get_offset_of_hashProtected_2() { return static_cast<int32_t>(offsetof(MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A, ___hashProtected_2)); }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * get_hashProtected_2() const { return ___hashProtected_2; }
	inline DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C ** get_address_of_hashProtected_2() { return &___hashProtected_2; }
	inline void set_hashProtected_2(DerBoolean_t6A51CBBA2A75845DFD8ACAACAFA5B0D8B42DBC3C * value)
	{
		___hashProtected_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashProtected_2), value);
	}

	inline static int32_t get_offset_of_fileName_3() { return static_cast<int32_t>(offsetof(MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A, ___fileName_3)); }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * get_fileName_3() const { return ___fileName_3; }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 ** get_address_of_fileName_3() { return &___fileName_3; }
	inline void set_fileName_3(DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * value)
	{
		___fileName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_3), value);
	}

	inline static int32_t get_offset_of_mediaType_4() { return static_cast<int32_t>(offsetof(MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A, ___mediaType_4)); }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * get_mediaType_4() const { return ___mediaType_4; }
	inline DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE ** get_address_of_mediaType_4() { return &___mediaType_4; }
	inline void set_mediaType_4(DerIA5String_tB7145189E4E76E79FD67198F52692D7B119415DE * value)
	{
		___mediaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_4), value);
	}

	inline static int32_t get_offset_of_otherMetaData_5() { return static_cast<int32_t>(offsetof(MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A, ___otherMetaData_5)); }
	inline Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0 * get_otherMetaData_5() const { return ___otherMetaData_5; }
	inline Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0 ** get_address_of_otherMetaData_5() { return &___otherMetaData_5; }
	inline void set_otherMetaData_5(Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0 * value)
	{
		___otherMetaData_5 = value;
		Il2CppCodeGenWriteBarrier((&___otherMetaData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATA_T43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A_H
#ifndef ORIGINATORIDENTIFIERORKEY_TBE9FA95E6C292D9CF0E0236984129F890D891F3E_H
#define ORIGINATORIDENTIFIERORKEY_TBE9FA95E6C292D9CF0E0236984129F890D891F3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorIdentifierOrKey
struct  OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorIdentifierOrKey::id
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___id_2;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E, ___id_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_id_2() const { return ___id_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINATORIDENTIFIERORKEY_TBE9FA95E6C292D9CF0E0236984129F890D891F3E_H
#ifndef ORIGINATORINFO_TAB84D6D318A415FF288A4E699F9A6770DE67427F_H
#define ORIGINATORINFO_TAB84D6D318A415FF288A4E699F9A6770DE67427F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo
struct  OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo::certs
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___certs_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Set BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorInfo::crls
	Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * ___crls_3;

public:
	inline static int32_t get_offset_of_certs_2() { return static_cast<int32_t>(offsetof(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F, ___certs_2)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_certs_2() const { return ___certs_2; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_certs_2() { return &___certs_2; }
	inline void set_certs_2(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___certs_2 = value;
		Il2CppCodeGenWriteBarrier((&___certs_2), value);
	}

	inline static int32_t get_offset_of_crls_3() { return static_cast<int32_t>(offsetof(OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F, ___crls_3)); }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * get_crls_3() const { return ___crls_3; }
	inline Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 ** get_address_of_crls_3() { return &___crls_3; }
	inline void set_crls_3(Asn1Set_tB1993FB3F4A7D15B52B13DEAB204AAD8CEC01A29 * value)
	{
		___crls_3 = value;
		Il2CppCodeGenWriteBarrier((&___crls_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINATORINFO_TAB84D6D318A415FF288A4E699F9A6770DE67427F_H
#ifndef ORIGINATORPUBLICKEY_T860ECE14956B5E7E4A98C44017B2529C39C8F209_H
#define ORIGINATORPUBLICKEY_T860ECE14956B5E7E4A98C44017B2529C39C8F209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorPublicKey
struct  OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorPublicKey::mAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___mAlgorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.OriginatorPublicKey::mPublicKey
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___mPublicKey_3;

public:
	inline static int32_t get_offset_of_mAlgorithm_2() { return static_cast<int32_t>(offsetof(OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209, ___mAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_mAlgorithm_2() const { return ___mAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_mAlgorithm_2() { return &___mAlgorithm_2; }
	inline void set_mAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___mAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___mAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_mPublicKey_3() { return static_cast<int32_t>(offsetof(OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209, ___mPublicKey_3)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_mPublicKey_3() const { return ___mPublicKey_3; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_mPublicKey_3() { return &___mPublicKey_3; }
	inline void set_mPublicKey_3(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___mPublicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPublicKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINATORPUBLICKEY_T860ECE14956B5E7E4A98C44017B2529C39C8F209_H
#ifndef ATTRIBUTETYPEANDVALUE_T2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E_H
#define ATTRIBUTETYPEANDVALUE_T2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.AttributeTypeAndValue
struct  AttributeTypeAndValue_t2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.AttributeTypeAndValue::type
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___type_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.AttributeTypeAndValue::value
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___value_3;

public:
	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_t2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E, ___type_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_type_2() const { return ___type_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((&___type_2), value);
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(AttributeTypeAndValue_t2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E, ___value_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_value_3() const { return ___value_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTETYPEANDVALUE_T2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E_H
#ifndef CERTID_TF9231977B4488C3D74B45E577937A80CB6D4577D_H
#define CERTID_TF9231977B4488C3D74B45E577937A80CB6D4577D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertId
struct  CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertId::issuer
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___issuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertId::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_3;

public:
	inline static int32_t get_offset_of_issuer_2() { return static_cast<int32_t>(offsetof(CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D, ___issuer_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_issuer_2() const { return ___issuer_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_issuer_2() { return &___issuer_2; }
	inline void set_issuer_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___issuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_2), value);
	}

	inline static int32_t get_offset_of_serialNumber_3() { return static_cast<int32_t>(offsetof(CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D, ___serialNumber_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_3() const { return ___serialNumber_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_3() { return &___serialNumber_3; }
	inline void set_serialNumber_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_3 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTID_TF9231977B4488C3D74B45E577937A80CB6D4577D_H
#ifndef CERTREQMESSAGES_T48C84096D883D692DD2D39E41B8C10E256637BCC_H
#define CERTREQMESSAGES_T48C84096D883D692DD2D39E41B8C10E256637BCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMessages
struct  CertReqMessages_t48C84096D883D692DD2D39E41B8C10E256637BCC  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMessages::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(CertReqMessages_t48C84096D883D692DD2D39E41B8C10E256637BCC, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTREQMESSAGES_T48C84096D883D692DD2D39E41B8C10E256637BCC_H
#ifndef CERTREQMSG_T06B6861581FDEE6E1BCB01AAB5BE90D057C8376E_H
#define CERTREQMSG_T06B6861581FDEE6E1BCB01AAB5BE90D057C8376E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMsg
struct  CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMsg::certReq
	CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 * ___certReq_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.ProofOfPossession BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMsg::popo
	ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E * ___popo_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertReqMsg::regInfo
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___regInfo_4;

public:
	inline static int32_t get_offset_of_certReq_2() { return static_cast<int32_t>(offsetof(CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E, ___certReq_2)); }
	inline CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 * get_certReq_2() const { return ___certReq_2; }
	inline CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 ** get_address_of_certReq_2() { return &___certReq_2; }
	inline void set_certReq_2(CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85 * value)
	{
		___certReq_2 = value;
		Il2CppCodeGenWriteBarrier((&___certReq_2), value);
	}

	inline static int32_t get_offset_of_popo_3() { return static_cast<int32_t>(offsetof(CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E, ___popo_3)); }
	inline ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E * get_popo_3() const { return ___popo_3; }
	inline ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E ** get_address_of_popo_3() { return &___popo_3; }
	inline void set_popo_3(ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E * value)
	{
		___popo_3 = value;
		Il2CppCodeGenWriteBarrier((&___popo_3), value);
	}

	inline static int32_t get_offset_of_regInfo_4() { return static_cast<int32_t>(offsetof(CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E, ___regInfo_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_regInfo_4() const { return ___regInfo_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_regInfo_4() { return &___regInfo_4; }
	inline void set_regInfo_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___regInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___regInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTREQMSG_T06B6861581FDEE6E1BCB01AAB5BE90D057C8376E_H
#ifndef CERTREQUEST_T7F64480A50BA13FA9653D8294D7630EC972C0F85_H
#define CERTREQUEST_T7F64480A50BA13FA9653D8294D7630EC972C0F85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest
struct  CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest::certReqId
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___certReqId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest::certTemplate
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C * ___certTemplate_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.Controls BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertRequest::controls
	Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B * ___controls_4;

public:
	inline static int32_t get_offset_of_certReqId_2() { return static_cast<int32_t>(offsetof(CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85, ___certReqId_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_certReqId_2() const { return ___certReqId_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_certReqId_2() { return &___certReqId_2; }
	inline void set_certReqId_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___certReqId_2 = value;
		Il2CppCodeGenWriteBarrier((&___certReqId_2), value);
	}

	inline static int32_t get_offset_of_certTemplate_3() { return static_cast<int32_t>(offsetof(CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85, ___certTemplate_3)); }
	inline CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C * get_certTemplate_3() const { return ___certTemplate_3; }
	inline CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C ** get_address_of_certTemplate_3() { return &___certTemplate_3; }
	inline void set_certTemplate_3(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C * value)
	{
		___certTemplate_3 = value;
		Il2CppCodeGenWriteBarrier((&___certTemplate_3), value);
	}

	inline static int32_t get_offset_of_controls_4() { return static_cast<int32_t>(offsetof(CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85, ___controls_4)); }
	inline Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B * get_controls_4() const { return ___controls_4; }
	inline Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B ** get_address_of_controls_4() { return &___controls_4; }
	inline void set_controls_4(Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B * value)
	{
		___controls_4 = value;
		Il2CppCodeGenWriteBarrier((&___controls_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTREQUEST_T7F64480A50BA13FA9653D8294D7630EC972C0F85_H
#ifndef CERTTEMPLATE_T1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C_H
#define CERTTEMPLATE_T1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate
struct  CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::seq
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___seq_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::version
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___version_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::serialNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___serialNumber_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::signingAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___signingAlg_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::issuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___issuer_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.OptionalValidity BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::validity
	OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 * ___validity_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::subject
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___subject_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::publicKey
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___publicKey_9;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::issuerUID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___issuerUID_10;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::subjectUID
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___subjectUID_11;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Extensions BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.CertTemplate::extensions
	X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * ___extensions_12;

public:
	inline static int32_t get_offset_of_seq_2() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___seq_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_seq_2() const { return ___seq_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_seq_2() { return &___seq_2; }
	inline void set_seq_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___seq_2 = value;
		Il2CppCodeGenWriteBarrier((&___seq_2), value);
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___version_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_version_3() const { return ___version_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}

	inline static int32_t get_offset_of_serialNumber_4() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___serialNumber_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_serialNumber_4() const { return ___serialNumber_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_serialNumber_4() { return &___serialNumber_4; }
	inline void set_serialNumber_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___serialNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___serialNumber_4), value);
	}

	inline static int32_t get_offset_of_signingAlg_5() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___signingAlg_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_signingAlg_5() const { return ___signingAlg_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_signingAlg_5() { return &___signingAlg_5; }
	inline void set_signingAlg_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___signingAlg_5 = value;
		Il2CppCodeGenWriteBarrier((&___signingAlg_5), value);
	}

	inline static int32_t get_offset_of_issuer_6() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___issuer_6)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_issuer_6() const { return ___issuer_6; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_issuer_6() { return &___issuer_6; }
	inline void set_issuer_6(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___issuer_6 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_6), value);
	}

	inline static int32_t get_offset_of_validity_7() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___validity_7)); }
	inline OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 * get_validity_7() const { return ___validity_7; }
	inline OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 ** get_address_of_validity_7() { return &___validity_7; }
	inline void set_validity_7(OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3 * value)
	{
		___validity_7 = value;
		Il2CppCodeGenWriteBarrier((&___validity_7), value);
	}

	inline static int32_t get_offset_of_subject_8() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___subject_8)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_subject_8() const { return ___subject_8; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_subject_8() { return &___subject_8; }
	inline void set_subject_8(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___subject_8 = value;
		Il2CppCodeGenWriteBarrier((&___subject_8), value);
	}

	inline static int32_t get_offset_of_publicKey_9() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___publicKey_9)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_publicKey_9() const { return ___publicKey_9; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_publicKey_9() { return &___publicKey_9; }
	inline void set_publicKey_9(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___publicKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_9), value);
	}

	inline static int32_t get_offset_of_issuerUID_10() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___issuerUID_10)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_issuerUID_10() const { return ___issuerUID_10; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_issuerUID_10() { return &___issuerUID_10; }
	inline void set_issuerUID_10(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___issuerUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUID_10), value);
	}

	inline static int32_t get_offset_of_subjectUID_11() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___subjectUID_11)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_subjectUID_11() const { return ___subjectUID_11; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_subjectUID_11() { return &___subjectUID_11; }
	inline void set_subjectUID_11(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___subjectUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUID_11), value);
	}

	inline static int32_t get_offset_of_extensions_12() { return static_cast<int32_t>(offsetof(CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C, ___extensions_12)); }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * get_extensions_12() const { return ___extensions_12; }
	inline X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 ** get_address_of_extensions_12() { return &___extensions_12; }
	inline void set_extensions_12(X509Extensions_tFBB7387546053A219E86A448C813BF7042984B02 * value)
	{
		___extensions_12 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTTEMPLATE_T1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C_H
#ifndef CONTROLS_T688340530A41FAB7480C51FA96CC9E1732E3FD2B_H
#define CONTROLS_T688340530A41FAB7480C51FA96CC9E1732E3FD2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.Controls
struct  Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.Controls::content
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___content_2;

public:
	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B, ___content_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_content_2() const { return ___content_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLS_T688340530A41FAB7480C51FA96CC9E1732E3FD2B_H
#ifndef ENCKEYWITHID_TC7DC5DF35A711B3FEA02B639C1C178E44E69051E_H
#define ENCKEYWITHID_TC7DC5DF35A711B3FEA02B639C1C178E44E69051E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncKeyWithID
struct  EncKeyWithID_tC7DC5DF35A711B3FEA02B639C1C178E44E69051E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Pkcs.PrivateKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncKeyWithID::privKeyInfo
	PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 * ___privKeyInfo_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncKeyWithID::identifier
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___identifier_3;

public:
	inline static int32_t get_offset_of_privKeyInfo_2() { return static_cast<int32_t>(offsetof(EncKeyWithID_tC7DC5DF35A711B3FEA02B639C1C178E44E69051E, ___privKeyInfo_2)); }
	inline PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 * get_privKeyInfo_2() const { return ___privKeyInfo_2; }
	inline PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 ** get_address_of_privKeyInfo_2() { return &___privKeyInfo_2; }
	inline void set_privKeyInfo_2(PrivateKeyInfo_t294A934F04B4B3879E71E81DB82B28066A10D317 * value)
	{
		___privKeyInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___privKeyInfo_2), value);
	}

	inline static int32_t get_offset_of_identifier_3() { return static_cast<int32_t>(offsetof(EncKeyWithID_tC7DC5DF35A711B3FEA02B639C1C178E44E69051E, ___identifier_3)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_identifier_3() const { return ___identifier_3; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_identifier_3() { return &___identifier_3; }
	inline void set_identifier_3(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___identifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCKEYWITHID_TC7DC5DF35A711B3FEA02B639C1C178E44E69051E_H
#ifndef ENCRYPTEDKEY_T9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3_H
#define ENCRYPTEDKEY_T9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedKey
struct  EncryptedKey_t9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Cms.EnvelopedData BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedKey::envelopedData
	EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594 * ___envelopedData_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedKey::encryptedValue
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * ___encryptedValue_3;

public:
	inline static int32_t get_offset_of_envelopedData_2() { return static_cast<int32_t>(offsetof(EncryptedKey_t9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3, ___envelopedData_2)); }
	inline EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594 * get_envelopedData_2() const { return ___envelopedData_2; }
	inline EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594 ** get_address_of_envelopedData_2() { return &___envelopedData_2; }
	inline void set_envelopedData_2(EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594 * value)
	{
		___envelopedData_2 = value;
		Il2CppCodeGenWriteBarrier((&___envelopedData_2), value);
	}

	inline static int32_t get_offset_of_encryptedValue_3() { return static_cast<int32_t>(offsetof(EncryptedKey_t9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3, ___encryptedValue_3)); }
	inline EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * get_encryptedValue_3() const { return ___encryptedValue_3; }
	inline EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 ** get_address_of_encryptedValue_3() { return &___encryptedValue_3; }
	inline void set_encryptedValue_3(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66 * value)
	{
		___encryptedValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDKEY_T9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3_H
#ifndef ENCRYPTEDVALUE_TD5EF992957BBA6A065198DD6B8C20C38FC336D66_H
#define ENCRYPTEDVALUE_TD5EF992957BBA6A065198DD6B8C20C38FC336D66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue
struct  EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue::intendedAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___intendedAlg_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue::symmAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___symmAlg_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue::encSymmKey
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___encSymmKey_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue::keyAlg
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___keyAlg_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue::valueHint
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___valueHint_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.EncryptedValue::encValue
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___encValue_7;

public:
	inline static int32_t get_offset_of_intendedAlg_2() { return static_cast<int32_t>(offsetof(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66, ___intendedAlg_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_intendedAlg_2() const { return ___intendedAlg_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_intendedAlg_2() { return &___intendedAlg_2; }
	inline void set_intendedAlg_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___intendedAlg_2 = value;
		Il2CppCodeGenWriteBarrier((&___intendedAlg_2), value);
	}

	inline static int32_t get_offset_of_symmAlg_3() { return static_cast<int32_t>(offsetof(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66, ___symmAlg_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_symmAlg_3() const { return ___symmAlg_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_symmAlg_3() { return &___symmAlg_3; }
	inline void set_symmAlg_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___symmAlg_3 = value;
		Il2CppCodeGenWriteBarrier((&___symmAlg_3), value);
	}

	inline static int32_t get_offset_of_encSymmKey_4() { return static_cast<int32_t>(offsetof(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66, ___encSymmKey_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_encSymmKey_4() const { return ___encSymmKey_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_encSymmKey_4() { return &___encSymmKey_4; }
	inline void set_encSymmKey_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___encSymmKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___encSymmKey_4), value);
	}

	inline static int32_t get_offset_of_keyAlg_5() { return static_cast<int32_t>(offsetof(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66, ___keyAlg_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_keyAlg_5() const { return ___keyAlg_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_keyAlg_5() { return &___keyAlg_5; }
	inline void set_keyAlg_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___keyAlg_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyAlg_5), value);
	}

	inline static int32_t get_offset_of_valueHint_6() { return static_cast<int32_t>(offsetof(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66, ___valueHint_6)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_valueHint_6() const { return ___valueHint_6; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_valueHint_6() { return &___valueHint_6; }
	inline void set_valueHint_6(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___valueHint_6 = value;
		Il2CppCodeGenWriteBarrier((&___valueHint_6), value);
	}

	inline static int32_t get_offset_of_encValue_7() { return static_cast<int32_t>(offsetof(EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66, ___encValue_7)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_encValue_7() const { return ___encValue_7; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_encValue_7() { return &___encValue_7; }
	inline void set_encValue_7(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___encValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___encValue_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDVALUE_TD5EF992957BBA6A065198DD6B8C20C38FC336D66_H
#ifndef OPTIONALVALIDITY_TE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3_H
#define OPTIONALVALIDITY_TE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.OptionalValidity
struct  OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.OptionalValidity::notBefore
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___notBefore_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.Time BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.OptionalValidity::notAfter
	Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * ___notAfter_3;

public:
	inline static int32_t get_offset_of_notBefore_2() { return static_cast<int32_t>(offsetof(OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3, ___notBefore_2)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_notBefore_2() const { return ___notBefore_2; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_notBefore_2() { return &___notBefore_2; }
	inline void set_notBefore_2(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___notBefore_2 = value;
		Il2CppCodeGenWriteBarrier((&___notBefore_2), value);
	}

	inline static int32_t get_offset_of_notAfter_3() { return static_cast<int32_t>(offsetof(OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3, ___notAfter_3)); }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * get_notAfter_3() const { return ___notAfter_3; }
	inline Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A ** get_address_of_notAfter_3() { return &___notAfter_3; }
	inline void set_notAfter_3(Time_t74086E2BF904AA38D0EBD7CD379053B6F33C011A * value)
	{
		___notAfter_3 = value;
		Il2CppCodeGenWriteBarrier((&___notAfter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONALVALIDITY_TE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3_H
#ifndef PKMACVALUE_T2354860B8647226235395857CE076879CE35713C_H
#define PKMACVALUE_T2354860B8647226235395857CE076879CE35713C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue
struct  PKMacValue_t2354860B8647226235395857CE076879CE35713C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue::algID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue::macValue
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___macValue_3;

public:
	inline static int32_t get_offset_of_algID_2() { return static_cast<int32_t>(offsetof(PKMacValue_t2354860B8647226235395857CE076879CE35713C, ___algID_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algID_2() const { return ___algID_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algID_2() { return &___algID_2; }
	inline void set_algID_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algID_2 = value;
		Il2CppCodeGenWriteBarrier((&___algID_2), value);
	}

	inline static int32_t get_offset_of_macValue_3() { return static_cast<int32_t>(offsetof(PKMacValue_t2354860B8647226235395857CE076879CE35713C, ___macValue_3)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_macValue_3() const { return ___macValue_3; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_macValue_3() { return &___macValue_3; }
	inline void set_macValue_3(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___macValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___macValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKMACVALUE_T2354860B8647226235395857CE076879CE35713C_H
#ifndef PKIARCHIVEOPTIONS_T11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482_H
#define PKIARCHIVEOPTIONS_T11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiArchiveOptions
struct  PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiArchiveOptions::value
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482, ___value_5)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_value_5() const { return ___value_5; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___value_5 = value;
		Il2CppCodeGenWriteBarrier((&___value_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIARCHIVEOPTIONS_T11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482_H
#ifndef PKIPUBLICATIONINFO_T61EE698B09F3C95DFE0520755D3F4FA2CB059950_H
#define PKIPUBLICATIONINFO_T61EE698B09F3C95DFE0520755D3F4FA2CB059950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiPublicationInfo
struct  PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiPublicationInfo::action
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___action_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PkiPublicationInfo::pubInfos
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___pubInfos_3;

public:
	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950, ___action_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_action_2() const { return ___action_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_pubInfos_3() { return static_cast<int32_t>(offsetof(PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950, ___pubInfos_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_pubInfos_3() const { return ___pubInfos_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_pubInfos_3() { return &___pubInfos_3; }
	inline void set_pubInfos_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___pubInfos_3 = value;
		Il2CppCodeGenWriteBarrier((&___pubInfos_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKIPUBLICATIONINFO_T61EE698B09F3C95DFE0520755D3F4FA2CB059950_H
#ifndef POPOPRIVKEY_T9A923197C93F6D58810D84652635D9CB6D647B95_H
#define POPOPRIVKEY_T9A923197C93F6D58810D84652635D9CB6D647B95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoPrivKey
struct  PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoPrivKey::tagNo
	int32_t ___tagNo_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoPrivKey::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_8;

public:
	inline static int32_t get_offset_of_tagNo_7() { return static_cast<int32_t>(offsetof(PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95, ___tagNo_7)); }
	inline int32_t get_tagNo_7() const { return ___tagNo_7; }
	inline int32_t* get_address_of_tagNo_7() { return &___tagNo_7; }
	inline void set_tagNo_7(int32_t value)
	{
		___tagNo_7 = value;
	}

	inline static int32_t get_offset_of_obj_8() { return static_cast<int32_t>(offsetof(PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95, ___obj_8)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_8() const { return ___obj_8; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_8() { return &___obj_8; }
	inline void set_obj_8(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPOPRIVKEY_T9A923197C93F6D58810D84652635D9CB6D647B95_H
#ifndef POPOSIGNINGKEY_T48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175_H
#define POPOSIGNINGKEY_T48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKey
struct  PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKeyInput BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKey::poposkInput
	PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F * ___poposkInput_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKey::algorithmIdentifier
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algorithmIdentifier_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerBitString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKey::signature
	DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * ___signature_4;

public:
	inline static int32_t get_offset_of_poposkInput_2() { return static_cast<int32_t>(offsetof(PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175, ___poposkInput_2)); }
	inline PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F * get_poposkInput_2() const { return ___poposkInput_2; }
	inline PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F ** get_address_of_poposkInput_2() { return &___poposkInput_2; }
	inline void set_poposkInput_2(PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F * value)
	{
		___poposkInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___poposkInput_2), value);
	}

	inline static int32_t get_offset_of_algorithmIdentifier_3() { return static_cast<int32_t>(offsetof(PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175, ___algorithmIdentifier_3)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algorithmIdentifier_3() const { return ___algorithmIdentifier_3; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algorithmIdentifier_3() { return &___algorithmIdentifier_3; }
	inline void set_algorithmIdentifier_3(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algorithmIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___algorithmIdentifier_3), value);
	}

	inline static int32_t get_offset_of_signature_4() { return static_cast<int32_t>(offsetof(PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175, ___signature_4)); }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * get_signature_4() const { return ___signature_4; }
	inline DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 ** get_address_of_signature_4() { return &___signature_4; }
	inline void set_signature_4(DerBitString_t96C9BD19660FE417056A0EEB0187771D7EB10374 * value)
	{
		___signature_4 = value;
		Il2CppCodeGenWriteBarrier((&___signature_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPOSIGNINGKEY_T48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175_H
#ifndef POPOSIGNINGKEYINPUT_T7A29E2A223CC642FD0B381323E88F1C16FD1A00F_H
#define POPOSIGNINGKEYINPUT_T7A29E2A223CC642FD0B381323E88F1C16FD1A00F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKeyInput
struct  PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKeyInput::sender
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___sender_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PKMacValue BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKeyInput::publicKeyMac
	PKMacValue_t2354860B8647226235395857CE076879CE35713C * ___publicKeyMac_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.SubjectPublicKeyInfo BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.PopoSigningKeyInput::publicKey
	SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * ___publicKey_4;

public:
	inline static int32_t get_offset_of_sender_2() { return static_cast<int32_t>(offsetof(PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F, ___sender_2)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_sender_2() const { return ___sender_2; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_sender_2() { return &___sender_2; }
	inline void set_sender_2(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___sender_2 = value;
		Il2CppCodeGenWriteBarrier((&___sender_2), value);
	}

	inline static int32_t get_offset_of_publicKeyMac_3() { return static_cast<int32_t>(offsetof(PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F, ___publicKeyMac_3)); }
	inline PKMacValue_t2354860B8647226235395857CE076879CE35713C * get_publicKeyMac_3() const { return ___publicKeyMac_3; }
	inline PKMacValue_t2354860B8647226235395857CE076879CE35713C ** get_address_of_publicKeyMac_3() { return &___publicKeyMac_3; }
	inline void set_publicKeyMac_3(PKMacValue_t2354860B8647226235395857CE076879CE35713C * value)
	{
		___publicKeyMac_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyMac_3), value);
	}

	inline static int32_t get_offset_of_publicKey_4() { return static_cast<int32_t>(offsetof(PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F, ___publicKey_4)); }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * get_publicKey_4() const { return ___publicKey_4; }
	inline SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 ** get_address_of_publicKey_4() { return &___publicKey_4; }
	inline void set_publicKey_4(SubjectPublicKeyInfo_t881A355EADDE5414A74A5F5D2FD4A64D21D91F90 * value)
	{
		___publicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPOSIGNINGKEYINPUT_T7A29E2A223CC642FD0B381323E88F1C16FD1A00F_H
#ifndef PROOFOFPOSSESSION_T973ECCBA591187FB3A2BDF98B4C11008329F845E_H
#define PROOFOFPOSSESSION_T973ECCBA591187FB3A2BDF98B4C11008329F845E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.ProofOfPossession
struct  ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.ProofOfPossession::tagNo
	int32_t ___tagNo_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.ProofOfPossession::obj
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___obj_7;

public:
	inline static int32_t get_offset_of_tagNo_6() { return static_cast<int32_t>(offsetof(ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E, ___tagNo_6)); }
	inline int32_t get_tagNo_6() const { return ___tagNo_6; }
	inline int32_t* get_address_of_tagNo_6() { return &___tagNo_6; }
	inline void set_tagNo_6(int32_t value)
	{
		___tagNo_6 = value;
	}

	inline static int32_t get_offset_of_obj_7() { return static_cast<int32_t>(offsetof(ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E, ___obj_7)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_obj_7() const { return ___obj_7; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_obj_7() { return &___obj_7; }
	inline void set_obj_7(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___obj_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROOFOFPOSSESSION_T973ECCBA591187FB3A2BDF98B4C11008329F845E_H
#ifndef SINGLEPUBINFO_TABDD49B93083C05AA436BC16E728EB5FC7C8EDA5_H
#define SINGLEPUBINFO_TABDD49B93083C05AA436BC16E728EB5FC7C8EDA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SinglePubInfo
struct  SinglePubInfo_tABDD49B93083C05AA436BC16E728EB5FC7C8EDA5  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SinglePubInfo::pubMethod
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___pubMethod_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.GeneralName BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SinglePubInfo::pubLocation
	GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * ___pubLocation_3;

public:
	inline static int32_t get_offset_of_pubMethod_2() { return static_cast<int32_t>(offsetof(SinglePubInfo_tABDD49B93083C05AA436BC16E728EB5FC7C8EDA5, ___pubMethod_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_pubMethod_2() const { return ___pubMethod_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_pubMethod_2() { return &___pubMethod_2; }
	inline void set_pubMethod_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___pubMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&___pubMethod_2), value);
	}

	inline static int32_t get_offset_of_pubLocation_3() { return static_cast<int32_t>(offsetof(SinglePubInfo_tABDD49B93083C05AA436BC16E728EB5FC7C8EDA5, ___pubLocation_3)); }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * get_pubLocation_3() const { return ___pubLocation_3; }
	inline GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B ** get_address_of_pubLocation_3() { return &___pubLocation_3; }
	inline void set_pubLocation_3(GeneralName_t613B894A93E71463E938C46C4F1A2EDDA72BAF7B * value)
	{
		___pubLocation_3 = value;
		Il2CppCodeGenWriteBarrier((&___pubLocation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEPUBINFO_TABDD49B93083C05AA436BC16E728EB5FC7C8EDA5_H
#ifndef ECGOST3410PARAMSETPARAMETERS_TE1115E1467F0615E73766B1925AFA7CDD2D657FA_H
#define ECGOST3410PARAMSETPARAMETERS_TE1115E1467F0615E73766B1925AFA7CDD2D657FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters
struct  ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters::p
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___p_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters::q
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___q_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters::a
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___a_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters::b
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___b_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters::x
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___x_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.ECGost3410ParamSetParameters::y
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___y_7;

public:
	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA, ___p_2)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_p_2() const { return ___p_2; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier((&___p_2), value);
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA, ___q_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_q_3() const { return ___q_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___q_3 = value;
		Il2CppCodeGenWriteBarrier((&___q_3), value);
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA, ___a_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_a_4() const { return ___a_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___a_4 = value;
		Il2CppCodeGenWriteBarrier((&___a_4), value);
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA, ___b_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_b_5() const { return ___b_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___b_5 = value;
		Il2CppCodeGenWriteBarrier((&___b_5), value);
	}

	inline static int32_t get_offset_of_x_6() { return static_cast<int32_t>(offsetof(ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA, ___x_6)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_x_6() const { return ___x_6; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_x_6() { return &___x_6; }
	inline void set_x_6(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___x_6 = value;
		Il2CppCodeGenWriteBarrier((&___x_6), value);
	}

	inline static int32_t get_offset_of_y_7() { return static_cast<int32_t>(offsetof(ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA, ___y_7)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_y_7() const { return ___y_7; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_y_7() { return &___y_7; }
	inline void set_y_7(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___y_7 = value;
		Il2CppCodeGenWriteBarrier((&___y_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECGOST3410PARAMSETPARAMETERS_TE1115E1467F0615E73766B1925AFA7CDD2D657FA_H
#ifndef GOST28147PARAMETERS_TBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E_H
#define GOST28147PARAMETERS_TBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost28147Parameters
struct  Gost28147Parameters_tBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost28147Parameters::iv
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___iv_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost28147Parameters::paramSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___paramSet_3;

public:
	inline static int32_t get_offset_of_iv_2() { return static_cast<int32_t>(offsetof(Gost28147Parameters_tBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E, ___iv_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_iv_2() const { return ___iv_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_iv_2() { return &___iv_2; }
	inline void set_iv_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___iv_2 = value;
		Il2CppCodeGenWriteBarrier((&___iv_2), value);
	}

	inline static int32_t get_offset_of_paramSet_3() { return static_cast<int32_t>(offsetof(Gost28147Parameters_tBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E, ___paramSet_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_paramSet_3() const { return ___paramSet_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_paramSet_3() { return &___paramSet_3; }
	inline void set_paramSet_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___paramSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___paramSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST28147PARAMETERS_TBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E_H
#ifndef GOST3410PARAMSETPARAMETERS_T3E7F86850C0B8D84550194C3E607F103A7C41E85_H
#define GOST3410PARAMSETPARAMETERS_T3E7F86850C0B8D84550194C3E607F103A7C41E85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters
struct  Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::keySize
	int32_t ___keySize_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::p
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___p_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::q
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___q_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410ParamSetParameters::a
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___a_5;

public:
	inline static int32_t get_offset_of_keySize_2() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85, ___keySize_2)); }
	inline int32_t get_keySize_2() const { return ___keySize_2; }
	inline int32_t* get_address_of_keySize_2() { return &___keySize_2; }
	inline void set_keySize_2(int32_t value)
	{
		___keySize_2 = value;
	}

	inline static int32_t get_offset_of_p_3() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85, ___p_3)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_p_3() const { return ___p_3; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_p_3() { return &___p_3; }
	inline void set_p_3(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___p_3 = value;
		Il2CppCodeGenWriteBarrier((&___p_3), value);
	}

	inline static int32_t get_offset_of_q_4() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85, ___q_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_q_4() const { return ___q_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_q_4() { return &___q_4; }
	inline void set_q_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___q_4 = value;
		Il2CppCodeGenWriteBarrier((&___q_4), value);
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85, ___a_5)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_a_5() const { return ___a_5; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___a_5 = value;
		Il2CppCodeGenWriteBarrier((&___a_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PARAMSETPARAMETERS_T3E7F86850C0B8D84550194C3E607F103A7C41E85_H
#ifndef GOST3410PUBLICKEYALGPARAMETERS_TC49BE7AD18FC9DDE163864F74D309DF13D10C2B1_H
#define GOST3410PUBLICKEYALGPARAMETERS_TC49BE7AD18FC9DDE163864F74D309DF13D10C2B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters
struct  Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters::publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___publicKeyParamSet_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters::digestParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___digestParamSet_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.CryptoPro.Gost3410PublicKeyAlgParameters::encryptionParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___encryptionParamSet_4;

public:
	inline static int32_t get_offset_of_publicKeyParamSet_2() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1, ___publicKeyParamSet_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_publicKeyParamSet_2() const { return ___publicKeyParamSet_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_publicKeyParamSet_2() { return &___publicKeyParamSet_2; }
	inline void set_publicKeyParamSet_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___publicKeyParamSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_2), value);
	}

	inline static int32_t get_offset_of_digestParamSet_3() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1, ___digestParamSet_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_digestParamSet_3() const { return ___digestParamSet_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_digestParamSet_3() { return &___digestParamSet_3; }
	inline void set_digestParamSet_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___digestParamSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___digestParamSet_3), value);
	}

	inline static int32_t get_offset_of_encryptionParamSet_4() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1, ___encryptionParamSet_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_encryptionParamSet_4() const { return ___encryptionParamSet_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_encryptionParamSet_4() { return &___encryptionParamSet_4; }
	inline void set_encryptionParamSet_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___encryptionParamSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptionParamSet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PUBLICKEYALGPARAMETERS_TC49BE7AD18FC9DDE163864F74D309DF13D10C2B1_H
#ifndef CERTIFICATEVALUES_TEB1467FD3FB72D052E4FB29012F86B162A1D08A0_H
#define CERTIFICATEVALUES_TEB1467FD3FB72D052E4FB29012F86B162A1D08A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CertificateValues
struct  CertificateValues_tEB1467FD3FB72D052E4FB29012F86B162A1D08A0  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CertificateValues::certificates
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certificates_2;

public:
	inline static int32_t get_offset_of_certificates_2() { return static_cast<int32_t>(offsetof(CertificateValues_tEB1467FD3FB72D052E4FB29012F86B162A1D08A0, ___certificates_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certificates_2() const { return ___certificates_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certificates_2() { return &___certificates_2; }
	inline void set_certificates_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certificates_2 = value;
		Il2CppCodeGenWriteBarrier((&___certificates_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALUES_TEB1467FD3FB72D052E4FB29012F86B162A1D08A0_H
#ifndef COMMITMENTTYPEINDICATION_T1E85285E40C83CC6E4A892793146F1E99954B992_H
#define COMMITMENTTYPEINDICATION_T1E85285E40C83CC6E4A892793146F1E99954B992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIndication
struct  CommitmentTypeIndication_t1E85285E40C83CC6E4A892793146F1E99954B992  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIndication::commitmentTypeId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___commitmentTypeId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeIndication::commitmentTypeQualifier
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___commitmentTypeQualifier_3;

public:
	inline static int32_t get_offset_of_commitmentTypeId_2() { return static_cast<int32_t>(offsetof(CommitmentTypeIndication_t1E85285E40C83CC6E4A892793146F1E99954B992, ___commitmentTypeId_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_commitmentTypeId_2() const { return ___commitmentTypeId_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_commitmentTypeId_2() { return &___commitmentTypeId_2; }
	inline void set_commitmentTypeId_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___commitmentTypeId_2 = value;
		Il2CppCodeGenWriteBarrier((&___commitmentTypeId_2), value);
	}

	inline static int32_t get_offset_of_commitmentTypeQualifier_3() { return static_cast<int32_t>(offsetof(CommitmentTypeIndication_t1E85285E40C83CC6E4A892793146F1E99954B992, ___commitmentTypeQualifier_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_commitmentTypeQualifier_3() const { return ___commitmentTypeQualifier_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_commitmentTypeQualifier_3() { return &___commitmentTypeQualifier_3; }
	inline void set_commitmentTypeQualifier_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___commitmentTypeQualifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___commitmentTypeQualifier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMITMENTTYPEINDICATION_T1E85285E40C83CC6E4A892793146F1E99954B992_H
#ifndef COMMITMENTTYPEQUALIFIER_T1989ED9E1B155B8E9FDB65037958C4A368755916_H
#define COMMITMENTTYPEQUALIFIER_T1989ED9E1B155B8E9FDB65037958C4A368755916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeQualifier
struct  CommitmentTypeQualifier_t1989ED9E1B155B8E9FDB65037958C4A368755916  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeQualifier::commitmentTypeIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___commitmentTypeIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CommitmentTypeQualifier::qualifier
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___qualifier_3;

public:
	inline static int32_t get_offset_of_commitmentTypeIdentifier_2() { return static_cast<int32_t>(offsetof(CommitmentTypeQualifier_t1989ED9E1B155B8E9FDB65037958C4A368755916, ___commitmentTypeIdentifier_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_commitmentTypeIdentifier_2() const { return ___commitmentTypeIdentifier_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_commitmentTypeIdentifier_2() { return &___commitmentTypeIdentifier_2; }
	inline void set_commitmentTypeIdentifier_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___commitmentTypeIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___commitmentTypeIdentifier_2), value);
	}

	inline static int32_t get_offset_of_qualifier_3() { return static_cast<int32_t>(offsetof(CommitmentTypeQualifier_t1989ED9E1B155B8E9FDB65037958C4A368755916, ___qualifier_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_qualifier_3() const { return ___qualifier_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_qualifier_3() { return &___qualifier_3; }
	inline void set_qualifier_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___qualifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___qualifier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMITMENTTYPEQUALIFIER_T1989ED9E1B155B8E9FDB65037958C4A368755916_H
#ifndef COMPLETECERTIFICATEREFS_TF663169D20693C6AB43CF1B3018E0CB128757399_H
#define COMPLETECERTIFICATEREFS_TF663169D20693C6AB43CF1B3018E0CB128757399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CompleteCertificateRefs
struct  CompleteCertificateRefs_tF663169D20693C6AB43CF1B3018E0CB128757399  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CompleteCertificateRefs::otherCertIDs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___otherCertIDs_2;

public:
	inline static int32_t get_offset_of_otherCertIDs_2() { return static_cast<int32_t>(offsetof(CompleteCertificateRefs_tF663169D20693C6AB43CF1B3018E0CB128757399, ___otherCertIDs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_otherCertIDs_2() const { return ___otherCertIDs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_otherCertIDs_2() { return &___otherCertIDs_2; }
	inline void set_otherCertIDs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___otherCertIDs_2 = value;
		Il2CppCodeGenWriteBarrier((&___otherCertIDs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETECERTIFICATEREFS_TF663169D20693C6AB43CF1B3018E0CB128757399_H
#ifndef COMPLETEREVOCATIONREFS_T6A58040CABF742B76E3D34B75F3FC099B3BCCDC8_H
#define COMPLETEREVOCATIONREFS_T6A58040CABF742B76E3D34B75F3FC099B3BCCDC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CompleteRevocationRefs
struct  CompleteRevocationRefs_t6A58040CABF742B76E3D34B75F3FC099B3BCCDC8  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CompleteRevocationRefs::crlOcspRefs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___crlOcspRefs_2;

public:
	inline static int32_t get_offset_of_crlOcspRefs_2() { return static_cast<int32_t>(offsetof(CompleteRevocationRefs_t6A58040CABF742B76E3D34B75F3FC099B3BCCDC8, ___crlOcspRefs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_crlOcspRefs_2() const { return ___crlOcspRefs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_crlOcspRefs_2() { return &___crlOcspRefs_2; }
	inline void set_crlOcspRefs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___crlOcspRefs_2 = value;
		Il2CppCodeGenWriteBarrier((&___crlOcspRefs_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETEREVOCATIONREFS_T6A58040CABF742B76E3D34B75F3FC099B3BCCDC8_H
#ifndef CRLIDENTIFIER_T43A1C40B41952282F3F148EAB6C1FCA30AE69960_H
#define CRLIDENTIFIER_T43A1C40B41952282F3F148EAB6C1FCA30AE69960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlIdentifier
struct  CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.X509Name BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlIdentifier::crlIssuer
	X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * ___crlIssuer_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtcTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlIdentifier::crlIssuedTime
	DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E * ___crlIssuedTime_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlIdentifier::crlNumber
	DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * ___crlNumber_4;

public:
	inline static int32_t get_offset_of_crlIssuer_2() { return static_cast<int32_t>(offsetof(CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960, ___crlIssuer_2)); }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * get_crlIssuer_2() const { return ___crlIssuer_2; }
	inline X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 ** get_address_of_crlIssuer_2() { return &___crlIssuer_2; }
	inline void set_crlIssuer_2(X509Name_t0D6EDC5B877B327C75FEDE783010E4C3843534D4 * value)
	{
		___crlIssuer_2 = value;
		Il2CppCodeGenWriteBarrier((&___crlIssuer_2), value);
	}

	inline static int32_t get_offset_of_crlIssuedTime_3() { return static_cast<int32_t>(offsetof(CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960, ___crlIssuedTime_3)); }
	inline DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E * get_crlIssuedTime_3() const { return ___crlIssuedTime_3; }
	inline DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E ** get_address_of_crlIssuedTime_3() { return &___crlIssuedTime_3; }
	inline void set_crlIssuedTime_3(DerUtcTime_tA01ABCC8C23A24C97DAF3837085ECD0B95C8D77E * value)
	{
		___crlIssuedTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___crlIssuedTime_3), value);
	}

	inline static int32_t get_offset_of_crlNumber_4() { return static_cast<int32_t>(offsetof(CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960, ___crlNumber_4)); }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * get_crlNumber_4() const { return ___crlNumber_4; }
	inline DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 ** get_address_of_crlNumber_4() { return &___crlNumber_4; }
	inline void set_crlNumber_4(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96 * value)
	{
		___crlNumber_4 = value;
		Il2CppCodeGenWriteBarrier((&___crlNumber_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLIDENTIFIER_T43A1C40B41952282F3F148EAB6C1FCA30AE69960_H
#ifndef CRLLISTID_T5BAB351EEBD3738751F7FBBD1923CA60BC7708A9_H
#define CRLLISTID_T5BAB351EEBD3738751F7FBBD1923CA60BC7708A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlListID
struct  CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlListID::crls
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___crls_2;

public:
	inline static int32_t get_offset_of_crls_2() { return static_cast<int32_t>(offsetof(CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9, ___crls_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_crls_2() const { return ___crls_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_crls_2() { return &___crls_2; }
	inline void set_crls_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___crls_2 = value;
		Il2CppCodeGenWriteBarrier((&___crls_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLLISTID_T5BAB351EEBD3738751F7FBBD1923CA60BC7708A9_H
#ifndef CRLOCSPREF_T999C0D1BBDCB128F59D60183DE8E9A4AB141AE17_H
#define CRLOCSPREF_T999C0D1BBDCB128F59D60183DE8E9A4AB141AE17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlOcspRef
struct  CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlListID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlOcspRef::crlids
	CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9 * ___crlids_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspListID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlOcspRef::ocspids
	OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36 * ___ocspids_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevRefs BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlOcspRef::otherRev
	OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C * ___otherRev_4;

public:
	inline static int32_t get_offset_of_crlids_2() { return static_cast<int32_t>(offsetof(CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17, ___crlids_2)); }
	inline CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9 * get_crlids_2() const { return ___crlids_2; }
	inline CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9 ** get_address_of_crlids_2() { return &___crlids_2; }
	inline void set_crlids_2(CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9 * value)
	{
		___crlids_2 = value;
		Il2CppCodeGenWriteBarrier((&___crlids_2), value);
	}

	inline static int32_t get_offset_of_ocspids_3() { return static_cast<int32_t>(offsetof(CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17, ___ocspids_3)); }
	inline OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36 * get_ocspids_3() const { return ___ocspids_3; }
	inline OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36 ** get_address_of_ocspids_3() { return &___ocspids_3; }
	inline void set_ocspids_3(OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36 * value)
	{
		___ocspids_3 = value;
		Il2CppCodeGenWriteBarrier((&___ocspids_3), value);
	}

	inline static int32_t get_offset_of_otherRev_4() { return static_cast<int32_t>(offsetof(CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17, ___otherRev_4)); }
	inline OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C * get_otherRev_4() const { return ___otherRev_4; }
	inline OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C ** get_address_of_otherRev_4() { return &___otherRev_4; }
	inline void set_otherRev_4(OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C * value)
	{
		___otherRev_4 = value;
		Il2CppCodeGenWriteBarrier((&___otherRev_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLOCSPREF_T999C0D1BBDCB128F59D60183DE8E9A4AB141AE17_H
#ifndef CRLVALIDATEDID_TF1817EEB9F620BF1044DFFE42174FE14BE8E983B_H
#define CRLVALIDATEDID_TF1817EEB9F620BF1044DFFE42174FE14BE8E983B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlValidatedID
struct  CrlValidatedID_tF1817EEB9F620BF1044DFFE42174FE14BE8E983B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlValidatedID::crlHash
	OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * ___crlHash_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.CrlValidatedID::crlIdentifier
	CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960 * ___crlIdentifier_3;

public:
	inline static int32_t get_offset_of_crlHash_2() { return static_cast<int32_t>(offsetof(CrlValidatedID_tF1817EEB9F620BF1044DFFE42174FE14BE8E983B, ___crlHash_2)); }
	inline OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * get_crlHash_2() const { return ___crlHash_2; }
	inline OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 ** get_address_of_crlHash_2() { return &___crlHash_2; }
	inline void set_crlHash_2(OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * value)
	{
		___crlHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___crlHash_2), value);
	}

	inline static int32_t get_offset_of_crlIdentifier_3() { return static_cast<int32_t>(offsetof(CrlValidatedID_tF1817EEB9F620BF1044DFFE42174FE14BE8E983B, ___crlIdentifier_3)); }
	inline CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960 * get_crlIdentifier_3() const { return ___crlIdentifier_3; }
	inline CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960 ** get_address_of_crlIdentifier_3() { return &___crlIdentifier_3; }
	inline void set_crlIdentifier_3(CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960 * value)
	{
		___crlIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___crlIdentifier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRLVALIDATEDID_TF1817EEB9F620BF1044DFFE42174FE14BE8E983B_H
#ifndef OCSPIDENTIFIER_TAD1EF4BCDC2A5C9754A217C2380B252DF9067E83_H
#define OCSPIDENTIFIER_TAD1EF4BCDC2A5C9754A217C2380B252DF9067E83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspIdentifier
struct  OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ocsp.ResponderID BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspIdentifier::ocspResponderID
	ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * ___ocspResponderID_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerGeneralizedTime BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspIdentifier::producedAt
	DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * ___producedAt_3;

public:
	inline static int32_t get_offset_of_ocspResponderID_2() { return static_cast<int32_t>(offsetof(OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83, ___ocspResponderID_2)); }
	inline ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * get_ocspResponderID_2() const { return ___ocspResponderID_2; }
	inline ResponderID_t26B99AFE9E4E05948990F461880598D304592346 ** get_address_of_ocspResponderID_2() { return &___ocspResponderID_2; }
	inline void set_ocspResponderID_2(ResponderID_t26B99AFE9E4E05948990F461880598D304592346 * value)
	{
		___ocspResponderID_2 = value;
		Il2CppCodeGenWriteBarrier((&___ocspResponderID_2), value);
	}

	inline static int32_t get_offset_of_producedAt_3() { return static_cast<int32_t>(offsetof(OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83, ___producedAt_3)); }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * get_producedAt_3() const { return ___producedAt_3; }
	inline DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A ** get_address_of_producedAt_3() { return &___producedAt_3; }
	inline void set_producedAt_3(DerGeneralizedTime_tC685B4F90AFB44A874F0D7C16787EB079FB81A6A * value)
	{
		___producedAt_3 = value;
		Il2CppCodeGenWriteBarrier((&___producedAt_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPIDENTIFIER_TAD1EF4BCDC2A5C9754A217C2380B252DF9067E83_H
#ifndef OCSPLISTID_T8B44191179E526A02CA5C412B8F1E892F968AD36_H
#define OCSPLISTID_T8B44191179E526A02CA5C412B8F1E892F968AD36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspListID
struct  OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspListID::ocspResponses
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___ocspResponses_2;

public:
	inline static int32_t get_offset_of_ocspResponses_2() { return static_cast<int32_t>(offsetof(OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36, ___ocspResponses_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_ocspResponses_2() const { return ___ocspResponses_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_ocspResponses_2() { return &___ocspResponses_2; }
	inline void set_ocspResponses_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___ocspResponses_2 = value;
		Il2CppCodeGenWriteBarrier((&___ocspResponses_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPLISTID_T8B44191179E526A02CA5C412B8F1E892F968AD36_H
#ifndef OCSPRESPONSESID_TE7E3463C3FC01F094D4BDD547573219BDD808737_H
#define OCSPRESPONSESID_TE7E3463C3FC01F094D4BDD547573219BDD808737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspResponsesID
struct  OcspResponsesID_tE7E3463C3FC01F094D4BDD547573219BDD808737  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspResponsesID::ocspIdentifier
	OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83 * ___ocspIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OcspResponsesID::ocspRepHash
	OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * ___ocspRepHash_3;

public:
	inline static int32_t get_offset_of_ocspIdentifier_2() { return static_cast<int32_t>(offsetof(OcspResponsesID_tE7E3463C3FC01F094D4BDD547573219BDD808737, ___ocspIdentifier_2)); }
	inline OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83 * get_ocspIdentifier_2() const { return ___ocspIdentifier_2; }
	inline OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83 ** get_address_of_ocspIdentifier_2() { return &___ocspIdentifier_2; }
	inline void set_ocspIdentifier_2(OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83 * value)
	{
		___ocspIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___ocspIdentifier_2), value);
	}

	inline static int32_t get_offset_of_ocspRepHash_3() { return static_cast<int32_t>(offsetof(OcspResponsesID_tE7E3463C3FC01F094D4BDD547573219BDD808737, ___ocspRepHash_3)); }
	inline OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * get_ocspRepHash_3() const { return ___ocspRepHash_3; }
	inline OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 ** get_address_of_ocspRepHash_3() { return &___ocspRepHash_3; }
	inline void set_ocspRepHash_3(OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * value)
	{
		___ocspRepHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___ocspRepHash_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCSPRESPONSESID_TE7E3463C3FC01F094D4BDD547573219BDD808737_H
#ifndef OTHERCERTID_T918B62BEAAD2243F225E9E3532C51D84BF7855DC_H
#define OTHERCERTID_T918B62BEAAD2243F225E9E3532C51D84BF7855DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherCertID
struct  OtherCertID_t918B62BEAAD2243F225E9E3532C51D84BF7855DC  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherCertID::otherCertHash
	OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * ___otherCertHash_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherCertID::issuerSerial
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___issuerSerial_3;

public:
	inline static int32_t get_offset_of_otherCertHash_2() { return static_cast<int32_t>(offsetof(OtherCertID_t918B62BEAAD2243F225E9E3532C51D84BF7855DC, ___otherCertHash_2)); }
	inline OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * get_otherCertHash_2() const { return ___otherCertHash_2; }
	inline OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 ** get_address_of_otherCertHash_2() { return &___otherCertHash_2; }
	inline void set_otherCertHash_2(OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8 * value)
	{
		___otherCertHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___otherCertHash_2), value);
	}

	inline static int32_t get_offset_of_issuerSerial_3() { return static_cast<int32_t>(offsetof(OtherCertID_t918B62BEAAD2243F225E9E3532C51D84BF7855DC, ___issuerSerial_3)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_issuerSerial_3() const { return ___issuerSerial_3; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_issuerSerial_3() { return &___issuerSerial_3; }
	inline void set_issuerSerial_3(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___issuerSerial_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuerSerial_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERCERTID_T918B62BEAAD2243F225E9E3532C51D84BF7855DC_H
#ifndef OTHERHASH_TFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8_H
#define OTHERHASH_TFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash
struct  OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash::sha1Hash
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___sha1Hash_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHashAlgAndValue BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHash::otherHash
	OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F * ___otherHash_3;

public:
	inline static int32_t get_offset_of_sha1Hash_2() { return static_cast<int32_t>(offsetof(OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8, ___sha1Hash_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_sha1Hash_2() const { return ___sha1Hash_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_sha1Hash_2() { return &___sha1Hash_2; }
	inline void set_sha1Hash_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___sha1Hash_2 = value;
		Il2CppCodeGenWriteBarrier((&___sha1Hash_2), value);
	}

	inline static int32_t get_offset_of_otherHash_3() { return static_cast<int32_t>(offsetof(OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8, ___otherHash_3)); }
	inline OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F * get_otherHash_3() const { return ___otherHash_3; }
	inline OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F ** get_address_of_otherHash_3() { return &___otherHash_3; }
	inline void set_otherHash_3(OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F * value)
	{
		___otherHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___otherHash_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERHASH_TFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8_H
#ifndef OTHERHASHALGANDVALUE_T4DEA06CC642ACCC7124772E895B4F2606C7A965F_H
#define OTHERHASHALGANDVALUE_T4DEA06CC642ACCC7124772E895B4F2606C7A965F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHashAlgAndValue
struct  OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHashAlgAndValue::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHashAlgAndValue::hashValue
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___hashValue_3;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_hashValue_3() { return static_cast<int32_t>(offsetof(OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F, ___hashValue_3)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_hashValue_3() const { return ___hashValue_3; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_hashValue_3() { return &___hashValue_3; }
	inline void set_hashValue_3(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___hashValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___hashValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERHASHALGANDVALUE_T4DEA06CC642ACCC7124772E895B4F2606C7A965F_H
#ifndef OTHERREVREFS_TDFB4E3B09507FF418B55BE56C83F431769FAC15C_H
#define OTHERREVREFS_TDFB4E3B09507FF418B55BE56C83F431769FAC15C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevRefs
struct  OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevRefs::otherRevRefType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___otherRevRefType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevRefs::otherRevRefs
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___otherRevRefs_3;

public:
	inline static int32_t get_offset_of_otherRevRefType_2() { return static_cast<int32_t>(offsetof(OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C, ___otherRevRefType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_otherRevRefType_2() const { return ___otherRevRefType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_otherRevRefType_2() { return &___otherRevRefType_2; }
	inline void set_otherRevRefType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___otherRevRefType_2 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevRefType_2), value);
	}

	inline static int32_t get_offset_of_otherRevRefs_3() { return static_cast<int32_t>(offsetof(OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C, ___otherRevRefs_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_otherRevRefs_3() const { return ___otherRevRefs_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_otherRevRefs_3() { return &___otherRevRefs_3; }
	inline void set_otherRevRefs_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___otherRevRefs_3 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevRefs_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERREVREFS_TDFB4E3B09507FF418B55BE56C83F431769FAC15C_H
#ifndef OTHERREVVALS_T419B7E046F5C79AC5F4D65538EF8B9B044CD6F99_H
#define OTHERREVVALS_T419B7E046F5C79AC5F4D65538EF8B9B044CD6F99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevVals
struct  OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevVals::otherRevValType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___otherRevValType_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevVals::otherRevVals
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___otherRevVals_3;

public:
	inline static int32_t get_offset_of_otherRevValType_2() { return static_cast<int32_t>(offsetof(OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99, ___otherRevValType_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_otherRevValType_2() const { return ___otherRevValType_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_otherRevValType_2() { return &___otherRevValType_2; }
	inline void set_otherRevValType_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___otherRevValType_2 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevValType_2), value);
	}

	inline static int32_t get_offset_of_otherRevVals_3() { return static_cast<int32_t>(offsetof(OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99, ___otherRevVals_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_otherRevVals_3() const { return ___otherRevVals_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_otherRevVals_3() { return &___otherRevVals_3; }
	inline void set_otherRevVals_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___otherRevVals_3 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevVals_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERREVVALS_T419B7E046F5C79AC5F4D65538EF8B9B044CD6F99_H
#ifndef OTHERSIGNINGCERTIFICATE_T2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3_H
#define OTHERSIGNINGCERTIFICATE_T2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherSigningCertificate
struct  OtherSigningCertificate_t2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherSigningCertificate::certs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certs_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherSigningCertificate::policies
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___policies_3;

public:
	inline static int32_t get_offset_of_certs_2() { return static_cast<int32_t>(offsetof(OtherSigningCertificate_t2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3, ___certs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certs_2() const { return ___certs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certs_2() { return &___certs_2; }
	inline void set_certs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certs_2 = value;
		Il2CppCodeGenWriteBarrier((&___certs_2), value);
	}

	inline static int32_t get_offset_of_policies_3() { return static_cast<int32_t>(offsetof(OtherSigningCertificate_t2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3, ___policies_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_policies_3() const { return ___policies_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_policies_3() { return &___policies_3; }
	inline void set_policies_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___policies_3 = value;
		Il2CppCodeGenWriteBarrier((&___policies_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERSIGNINGCERTIFICATE_T2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3_H
#ifndef REVOCATIONVALUES_TCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55_H
#define REVOCATIONVALUES_TCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.RevocationValues
struct  RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.RevocationValues::crlVals
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___crlVals_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.RevocationValues::ocspVals
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___ocspVals_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherRevVals BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.RevocationValues::otherRevVals
	OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99 * ___otherRevVals_4;

public:
	inline static int32_t get_offset_of_crlVals_2() { return static_cast<int32_t>(offsetof(RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55, ___crlVals_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_crlVals_2() const { return ___crlVals_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_crlVals_2() { return &___crlVals_2; }
	inline void set_crlVals_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___crlVals_2 = value;
		Il2CppCodeGenWriteBarrier((&___crlVals_2), value);
	}

	inline static int32_t get_offset_of_ocspVals_3() { return static_cast<int32_t>(offsetof(RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55, ___ocspVals_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_ocspVals_3() const { return ___ocspVals_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_ocspVals_3() { return &___ocspVals_3; }
	inline void set_ocspVals_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___ocspVals_3 = value;
		Il2CppCodeGenWriteBarrier((&___ocspVals_3), value);
	}

	inline static int32_t get_offset_of_otherRevVals_4() { return static_cast<int32_t>(offsetof(RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55, ___otherRevVals_4)); }
	inline OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99 * get_otherRevVals_4() const { return ___otherRevVals_4; }
	inline OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99 ** get_address_of_otherRevVals_4() { return &___otherRevVals_4; }
	inline void set_otherRevVals_4(OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99 * value)
	{
		___otherRevVals_4 = value;
		Il2CppCodeGenWriteBarrier((&___otherRevVals_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVOCATIONVALUES_TCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55_H
#ifndef SIGPOLICYQUALIFIERINFO_T43A41A5356ECAF49B0DB414FA95B95016A00C518_H
#define SIGPOLICYQUALIFIERINFO_T43A41A5356ECAF49B0DB414FA95B95016A00C518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SigPolicyQualifierInfo
struct  SigPolicyQualifierInfo_t43A41A5356ECAF49B0DB414FA95B95016A00C518  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SigPolicyQualifierInfo::sigPolicyQualifierId
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sigPolicyQualifierId_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Object BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SigPolicyQualifierInfo::sigQualifier
	Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * ___sigQualifier_3;

public:
	inline static int32_t get_offset_of_sigPolicyQualifierId_2() { return static_cast<int32_t>(offsetof(SigPolicyQualifierInfo_t43A41A5356ECAF49B0DB414FA95B95016A00C518, ___sigPolicyQualifierId_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sigPolicyQualifierId_2() const { return ___sigPolicyQualifierId_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sigPolicyQualifierId_2() { return &___sigPolicyQualifierId_2; }
	inline void set_sigPolicyQualifierId_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sigPolicyQualifierId_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigPolicyQualifierId_2), value);
	}

	inline static int32_t get_offset_of_sigQualifier_3() { return static_cast<int32_t>(offsetof(SigPolicyQualifierInfo_t43A41A5356ECAF49B0DB414FA95B95016A00C518, ___sigQualifier_3)); }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * get_sigQualifier_3() const { return ___sigQualifier_3; }
	inline Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E ** get_address_of_sigQualifier_3() { return &___sigQualifier_3; }
	inline void set_sigQualifier_3(Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E * value)
	{
		___sigQualifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigQualifier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGPOLICYQUALIFIERINFO_T43A41A5356ECAF49B0DB414FA95B95016A00C518_H
#ifndef SIGNATUREPOLICYID_T67FE48B82D232944F97BD575B0917EE6C3F5C6A9_H
#define SIGNATUREPOLICYID_T67FE48B82D232944F97BD575B0917EE6C3F5C6A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyId
struct  SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyId::sigPolicyIdentifier
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___sigPolicyIdentifier_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.OtherHashAlgAndValue BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyId::sigPolicyHash
	OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F * ___sigPolicyHash_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyId::sigPolicyQualifiers
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___sigPolicyQualifiers_4;

public:
	inline static int32_t get_offset_of_sigPolicyIdentifier_2() { return static_cast<int32_t>(offsetof(SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9, ___sigPolicyIdentifier_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_sigPolicyIdentifier_2() const { return ___sigPolicyIdentifier_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_sigPolicyIdentifier_2() { return &___sigPolicyIdentifier_2; }
	inline void set_sigPolicyIdentifier_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___sigPolicyIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigPolicyIdentifier_2), value);
	}

	inline static int32_t get_offset_of_sigPolicyHash_3() { return static_cast<int32_t>(offsetof(SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9, ___sigPolicyHash_3)); }
	inline OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F * get_sigPolicyHash_3() const { return ___sigPolicyHash_3; }
	inline OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F ** get_address_of_sigPolicyHash_3() { return &___sigPolicyHash_3; }
	inline void set_sigPolicyHash_3(OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F * value)
	{
		___sigPolicyHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___sigPolicyHash_3), value);
	}

	inline static int32_t get_offset_of_sigPolicyQualifiers_4() { return static_cast<int32_t>(offsetof(SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9, ___sigPolicyQualifiers_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_sigPolicyQualifiers_4() const { return ___sigPolicyQualifiers_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_sigPolicyQualifiers_4() { return &___sigPolicyQualifiers_4; }
	inline void set_sigPolicyQualifiers_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___sigPolicyQualifiers_4 = value;
		Il2CppCodeGenWriteBarrier((&___sigPolicyQualifiers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREPOLICYID_T67FE48B82D232944F97BD575B0917EE6C3F5C6A9_H
#ifndef SIGNATUREPOLICYIDENTIFIER_T2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32_H
#define SIGNATUREPOLICYIDENTIFIER_T2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyIdentifier
struct  SignaturePolicyIdentifier_t2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyId BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignaturePolicyIdentifier::sigPolicy
	SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9 * ___sigPolicy_2;

public:
	inline static int32_t get_offset_of_sigPolicy_2() { return static_cast<int32_t>(offsetof(SignaturePolicyIdentifier_t2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32, ___sigPolicy_2)); }
	inline SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9 * get_sigPolicy_2() const { return ___sigPolicy_2; }
	inline SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9 ** get_address_of_sigPolicy_2() { return &___sigPolicy_2; }
	inline void set_sigPolicy_2(SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9 * value)
	{
		___sigPolicy_2 = value;
		Il2CppCodeGenWriteBarrier((&___sigPolicy_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREPOLICYIDENTIFIER_T2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32_H
#ifndef SIGNERATTRIBUTE_TBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18_H
#define SIGNERATTRIBUTE_TBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerAttribute
struct  SignerAttribute_tBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerAttribute::claimedAttributes
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___claimedAttributes_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AttributeCertificate BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerAttribute::certifiedAttributes
	AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * ___certifiedAttributes_3;

public:
	inline static int32_t get_offset_of_claimedAttributes_2() { return static_cast<int32_t>(offsetof(SignerAttribute_tBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18, ___claimedAttributes_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_claimedAttributes_2() const { return ___claimedAttributes_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_claimedAttributes_2() { return &___claimedAttributes_2; }
	inline void set_claimedAttributes_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___claimedAttributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___claimedAttributes_2), value);
	}

	inline static int32_t get_offset_of_certifiedAttributes_3() { return static_cast<int32_t>(offsetof(SignerAttribute_tBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18, ___certifiedAttributes_3)); }
	inline AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * get_certifiedAttributes_3() const { return ___certifiedAttributes_3; }
	inline AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B ** get_address_of_certifiedAttributes_3() { return &___certifiedAttributes_3; }
	inline void set_certifiedAttributes_3(AttributeCertificate_t0DD6CD134E59614B589B667A1033ABCE2F43AD7B * value)
	{
		___certifiedAttributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___certifiedAttributes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERATTRIBUTE_TBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18_H
#ifndef SIGNERLOCATION_T1D2BF17C812C530689FE079184A1D689BFD3C7C6_H
#define SIGNERLOCATION_T1D2BF17C812C530689FE079184A1D689BFD3C7C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerLocation
struct  SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerLocation::countryName
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___countryName_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X500.DirectoryString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerLocation::localityName
	DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * ___localityName_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Esf.SignerLocation::postalAddress
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___postalAddress_4;

public:
	inline static int32_t get_offset_of_countryName_2() { return static_cast<int32_t>(offsetof(SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6, ___countryName_2)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_countryName_2() const { return ___countryName_2; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_countryName_2() { return &___countryName_2; }
	inline void set_countryName_2(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___countryName_2 = value;
		Il2CppCodeGenWriteBarrier((&___countryName_2), value);
	}

	inline static int32_t get_offset_of_localityName_3() { return static_cast<int32_t>(offsetof(SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6, ___localityName_3)); }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * get_localityName_3() const { return ___localityName_3; }
	inline DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B ** get_address_of_localityName_3() { return &___localityName_3; }
	inline void set_localityName_3(DirectoryString_t55BD84D10FDF8A6794760B8C6D70ADC242AC012B * value)
	{
		___localityName_3 = value;
		Il2CppCodeGenWriteBarrier((&___localityName_3), value);
	}

	inline static int32_t get_offset_of_postalAddress_4() { return static_cast<int32_t>(offsetof(SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6, ___postalAddress_4)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_postalAddress_4() const { return ___postalAddress_4; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_postalAddress_4() { return &___postalAddress_4; }
	inline void set_postalAddress_4(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___postalAddress_4 = value;
		Il2CppCodeGenWriteBarrier((&___postalAddress_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERLOCATION_T1D2BF17C812C530689FE079184A1D689BFD3C7C6_H
#ifndef CONTENTHINTS_T701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070_H
#define CONTENTHINTS_T701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.ContentHints
struct  ContentHints_t701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerUtf8String BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.ContentHints::contentDescription
	DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * ___contentDescription_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.ContentHints::contentType
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___contentType_3;

public:
	inline static int32_t get_offset_of_contentDescription_2() { return static_cast<int32_t>(offsetof(ContentHints_t701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070, ___contentDescription_2)); }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * get_contentDescription_2() const { return ___contentDescription_2; }
	inline DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 ** get_address_of_contentDescription_2() { return &___contentDescription_2; }
	inline void set_contentDescription_2(DerUtf8String_tB210D5A249CA8CB39DF88A0F5B064601FF1ECF15 * value)
	{
		___contentDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentDescription_2), value);
	}

	inline static int32_t get_offset_of_contentType_3() { return static_cast<int32_t>(offsetof(ContentHints_t701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070, ___contentType_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_contentType_3() const { return ___contentType_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_contentType_3() { return &___contentType_3; }
	inline void set_contentType_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___contentType_3 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTHINTS_T701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070_H
#ifndef CONTENTIDENTIFIER_TF6476ED68CBC248125236E314E72F753A8100E84_H
#define CONTENTIDENTIFIER_TF6476ED68CBC248125236E314E72F753A8100E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.ContentIdentifier
struct  ContentIdentifier_tF6476ED68CBC248125236E314E72F753A8100E84  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.ContentIdentifier::value
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___value_2;

public:
	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(ContentIdentifier_tF6476ED68CBC248125236E314E72F753A8100E84, ___value_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_value_2() const { return ___value_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___value_2 = value;
		Il2CppCodeGenWriteBarrier((&___value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTIDENTIFIER_TF6476ED68CBC248125236E314E72F753A8100E84_H
#ifndef ESSCERTID_TD45CD4EA0FFB464B3C4B32E1BA1542586AE13960_H
#define ESSCERTID_TD45CD4EA0FFB464B3C4B32E1BA1542586AE13960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertID
struct  EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1OctetString BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertID::certHash
	Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * ___certHash_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertID::issuerSerial
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___issuerSerial_3;

public:
	inline static int32_t get_offset_of_certHash_2() { return static_cast<int32_t>(offsetof(EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960, ___certHash_2)); }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * get_certHash_2() const { return ___certHash_2; }
	inline Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B ** get_address_of_certHash_2() { return &___certHash_2; }
	inline void set_certHash_2(Asn1OctetString_t013D79AEF2791863689C38F8305C12D7F540024B * value)
	{
		___certHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___certHash_2), value);
	}

	inline static int32_t get_offset_of_issuerSerial_3() { return static_cast<int32_t>(offsetof(EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960, ___issuerSerial_3)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_issuerSerial_3() const { return ___issuerSerial_3; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_issuerSerial_3() { return &___issuerSerial_3; }
	inline void set_issuerSerial_3(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___issuerSerial_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuerSerial_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESSCERTID_TD45CD4EA0FFB464B3C4B32E1BA1542586AE13960_H
#ifndef ESSCERTIDV2_T3F5274C11D0F0BB045B437505E04C13C2DC2A02F_H
#define ESSCERTIDV2_T3F5274C11D0F0BB045B437505E04C13C2DC2A02F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2
struct  EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2::hashAlgorithm
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___hashAlgorithm_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2::certHash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___certHash_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2::issuerSerial
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___issuerSerial_4;

public:
	inline static int32_t get_offset_of_hashAlgorithm_2() { return static_cast<int32_t>(offsetof(EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F, ___hashAlgorithm_2)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_hashAlgorithm_2() const { return ___hashAlgorithm_2; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_hashAlgorithm_2() { return &___hashAlgorithm_2; }
	inline void set_hashAlgorithm_2(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___hashAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___hashAlgorithm_2), value);
	}

	inline static int32_t get_offset_of_certHash_3() { return static_cast<int32_t>(offsetof(EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F, ___certHash_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_certHash_3() const { return ___certHash_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_certHash_3() { return &___certHash_3; }
	inline void set_certHash_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___certHash_3 = value;
		Il2CppCodeGenWriteBarrier((&___certHash_3), value);
	}

	inline static int32_t get_offset_of_issuerSerial_4() { return static_cast<int32_t>(offsetof(EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F, ___issuerSerial_4)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_issuerSerial_4() const { return ___issuerSerial_4; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_issuerSerial_4() { return &___issuerSerial_4; }
	inline void set_issuerSerial_4(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___issuerSerial_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuerSerial_4), value);
	}
};

struct EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.EssCertIDv2::DefaultAlgID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___DefaultAlgID_5;

public:
	inline static int32_t get_offset_of_DefaultAlgID_5() { return static_cast<int32_t>(offsetof(EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F_StaticFields, ___DefaultAlgID_5)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_DefaultAlgID_5() const { return ___DefaultAlgID_5; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_DefaultAlgID_5() { return &___DefaultAlgID_5; }
	inline void set_DefaultAlgID_5(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___DefaultAlgID_5 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultAlgID_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESSCERTIDV2_T3F5274C11D0F0BB045B437505E04C13C2DC2A02F_H
#ifndef OTHERCERTID_T3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F_H
#define OTHERCERTID_T3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.OtherCertID
struct  OtherCertID_t3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Encodable BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.OtherCertID::otherCertHash
	Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * ___otherCertHash_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.IssuerSerial BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.OtherCertID::issuerSerial
	IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * ___issuerSerial_3;

public:
	inline static int32_t get_offset_of_otherCertHash_2() { return static_cast<int32_t>(offsetof(OtherCertID_t3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F, ___otherCertHash_2)); }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * get_otherCertHash_2() const { return ___otherCertHash_2; }
	inline Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB ** get_address_of_otherCertHash_2() { return &___otherCertHash_2; }
	inline void set_otherCertHash_2(Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB * value)
	{
		___otherCertHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___otherCertHash_2), value);
	}

	inline static int32_t get_offset_of_issuerSerial_3() { return static_cast<int32_t>(offsetof(OtherCertID_t3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F, ___issuerSerial_3)); }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * get_issuerSerial_3() const { return ___issuerSerial_3; }
	inline IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C ** get_address_of_issuerSerial_3() { return &___issuerSerial_3; }
	inline void set_issuerSerial_3(IssuerSerial_t317A1FBECBF989427611290BC189C88AD6BEDA5C * value)
	{
		___issuerSerial_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuerSerial_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERCERTID_T3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F_H
#ifndef OTHERSIGNINGCERTIFICATE_T9B34871F5B88E22C2A3D9CAB36B5E617F4265631_H
#define OTHERSIGNINGCERTIFICATE_T9B34871F5B88E22C2A3D9CAB36B5E617F4265631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.OtherSigningCertificate
struct  OtherSigningCertificate_t9B34871F5B88E22C2A3D9CAB36B5E617F4265631  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.OtherSigningCertificate::certs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certs_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.OtherSigningCertificate::policies
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___policies_3;

public:
	inline static int32_t get_offset_of_certs_2() { return static_cast<int32_t>(offsetof(OtherSigningCertificate_t9B34871F5B88E22C2A3D9CAB36B5E617F4265631, ___certs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certs_2() const { return ___certs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certs_2() { return &___certs_2; }
	inline void set_certs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certs_2 = value;
		Il2CppCodeGenWriteBarrier((&___certs_2), value);
	}

	inline static int32_t get_offset_of_policies_3() { return static_cast<int32_t>(offsetof(OtherSigningCertificate_t9B34871F5B88E22C2A3D9CAB36B5E617F4265631, ___policies_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_policies_3() const { return ___policies_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_policies_3() { return &___policies_3; }
	inline void set_policies_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___policies_3 = value;
		Il2CppCodeGenWriteBarrier((&___policies_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OTHERSIGNINGCERTIFICATE_T9B34871F5B88E22C2A3D9CAB36B5E617F4265631_H
#ifndef SIGNINGCERTIFICATE_T6A4F6338791ED9D2195235B5700A360FDC892542_H
#define SIGNINGCERTIFICATE_T6A4F6338791ED9D2195235B5700A360FDC892542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.SigningCertificate
struct  SigningCertificate_t6A4F6338791ED9D2195235B5700A360FDC892542  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.SigningCertificate::certs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certs_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.SigningCertificate::policies
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___policies_3;

public:
	inline static int32_t get_offset_of_certs_2() { return static_cast<int32_t>(offsetof(SigningCertificate_t6A4F6338791ED9D2195235B5700A360FDC892542, ___certs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certs_2() const { return ___certs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certs_2() { return &___certs_2; }
	inline void set_certs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certs_2 = value;
		Il2CppCodeGenWriteBarrier((&___certs_2), value);
	}

	inline static int32_t get_offset_of_policies_3() { return static_cast<int32_t>(offsetof(SigningCertificate_t6A4F6338791ED9D2195235B5700A360FDC892542, ___policies_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_policies_3() const { return ___policies_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_policies_3() { return &___policies_3; }
	inline void set_policies_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___policies_3 = value;
		Il2CppCodeGenWriteBarrier((&___policies_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNINGCERTIFICATE_T6A4F6338791ED9D2195235B5700A360FDC892542_H
#ifndef SIGNINGCERTIFICATEV2_T46059D698627E8CDAE8DD2480EAEA5388368A44B_H
#define SIGNINGCERTIFICATEV2_T46059D698627E8CDAE8DD2480EAEA5388368A44B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.SigningCertificateV2
struct  SigningCertificateV2_t46059D698627E8CDAE8DD2480EAEA5388368A44B  : public Asn1Encodable_tE2DE940D11501485013A91380763A719FA1D38BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.SigningCertificateV2::certs
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___certs_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Sequence BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Ess.SigningCertificateV2::policies
	Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * ___policies_3;

public:
	inline static int32_t get_offset_of_certs_2() { return static_cast<int32_t>(offsetof(SigningCertificateV2_t46059D698627E8CDAE8DD2480EAEA5388368A44B, ___certs_2)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_certs_2() const { return ___certs_2; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_certs_2() { return &___certs_2; }
	inline void set_certs_2(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___certs_2 = value;
		Il2CppCodeGenWriteBarrier((&___certs_2), value);
	}

	inline static int32_t get_offset_of_policies_3() { return static_cast<int32_t>(offsetof(SigningCertificateV2_t46059D698627E8CDAE8DD2480EAEA5388368A44B, ___policies_3)); }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * get_policies_3() const { return ___policies_3; }
	inline Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 ** get_address_of_policies_3() { return &___policies_3; }
	inline void set_policies_3(Asn1Sequence_t8D18DC0674425C28D79ACDD26FD19D10BD62C205 * value)
	{
		___policies_3 = value;
		Il2CppCodeGenWriteBarrier((&___policies_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNINGCERTIFICATEV2_T46059D698627E8CDAE8DD2480EAEA5388368A44B_H
#ifndef SM2P256V1HOLDER_T50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_H
#define SM2P256V1HOLDER_T50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves_SM2P256V1Holder
struct  SM2P256V1Holder_t50DB69E424E5C97E4E63B59DA19EC910D7E1FE42  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct SM2P256V1Holder_t50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves_SM2P256V1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(SM2P256V1Holder_t50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2P256V1HOLDER_T50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_H
#ifndef WAPIP192V1HOLDER_T077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_H
#define WAPIP192V1HOLDER_T077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves_WapiP192V1Holder
struct  WapiP192V1Holder_t077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4  : public X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB
{
public:

public:
};

struct WapiP192V1Holder_t077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X9.X9ECParametersHolder BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.GM.GMNamedCurves_WapiP192V1Holder::Instance
	X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * ___Instance_1;

public:
	inline static int32_t get_offset_of_Instance_1() { return static_cast<int32_t>(offsetof(WapiP192V1Holder_t077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_StaticFields, ___Instance_1)); }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * get_Instance_1() const { return ___Instance_1; }
	inline X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB ** get_address_of_Instance_1() { return &___Instance_1; }
	inline void set_Instance_1(X9ECParametersHolder_tA26C05F393965FC0677EE16D76EFCF698FCDEFCB * value)
	{
		___Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAPIP192V1HOLDER_T077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_H
#ifndef DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#define DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger
struct  DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96  : public Asn1Object_t20F7CA4013D7F9E7C374B2361CAD8B743F0C1A4E
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerInteger::start
	int32_t ___start_6;

public:
	inline static int32_t get_offset_of_bytes_5() { return static_cast<int32_t>(offsetof(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96, ___bytes_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_5() const { return ___bytes_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_5() { return &___bytes_5; }
	inline void set_bytes_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_5 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_5), value);
	}

	inline static int32_t get_offset_of_start_6() { return static_cast<int32_t>(offsetof(DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96, ___start_6)); }
	inline int32_t get_start_6() const { return ___start_6; }
	inline int32_t* get_address_of_start_6() { return &___start_6; }
	inline void set_start_6(int32_t value)
	{
		___start_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERINTEGER_TD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96_H
#ifndef SUBSEQUENTMESSAGE_T220D29A6CC1439A4E8C73576801AB199191E1313_H
#define SUBSEQUENTMESSAGE_T220D29A6CC1439A4E8C73576801AB199191E1313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SubsequentMessage
struct  SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313  : public DerInteger_tD564BF7D2BD4A3D7CDC32DFF6461DF9E858BBC96
{
public:

public:
};

struct SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SubsequentMessage BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SubsequentMessage::encrCert
	SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 * ___encrCert_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SubsequentMessage BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Crmf.SubsequentMessage::challengeResp
	SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 * ___challengeResp_8;

public:
	inline static int32_t get_offset_of_encrCert_7() { return static_cast<int32_t>(offsetof(SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313_StaticFields, ___encrCert_7)); }
	inline SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 * get_encrCert_7() const { return ___encrCert_7; }
	inline SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 ** get_address_of_encrCert_7() { return &___encrCert_7; }
	inline void set_encrCert_7(SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 * value)
	{
		___encrCert_7 = value;
		Il2CppCodeGenWriteBarrier((&___encrCert_7), value);
	}

	inline static int32_t get_offset_of_challengeResp_8() { return static_cast<int32_t>(offsetof(SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313_StaticFields, ___challengeResp_8)); }
	inline SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 * get_challengeResp_8() const { return ___challengeResp_8; }
	inline SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 ** get_address_of_challengeResp_8() { return &___challengeResp_8; }
	inline void set_challengeResp_8(SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313 * value)
	{
		___challengeResp_8 = value;
		Il2CppCodeGenWriteBarrier((&___challengeResp_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSEQUENTMESSAGE_T220D29A6CC1439A4E8C73576801AB199191E1313_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof (IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E), -1, sizeof(IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5500[5] = 
{
	IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields::get_offset_of_IsakmpOakley_0(),
	IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields::get_offset_of_HmacMD5_1(),
	IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields::get_offset_of_HmacSha1_2(),
	IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields::get_offset_of_HmacTiger_3(),
	IanaObjectIdentifiers_t628BEE30F309063831C36EBBFDB4F2F39911442E_StaticFields::get_offset_of_HmacRipeMD160_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof (GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC), -1, sizeof(GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5501[25] = 
{
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Gnu_0(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_GnuPG_1(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Notation_2(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_PkaAddress_3(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_GnuRadar_4(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_DigestAlgorithm_5(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Tiger192_6(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_EncryptionAlgorithm_7(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent_8(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent128Ecb_9(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent128Cbc_10(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent128Ofb_11(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent128Cfb_12(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent192Ecb_13(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent192Cbc_14(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent192Ofb_15(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent192Cfb_16(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent256Ecb_17(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent256Cbc_18(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent256Ofb_19(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Serpent256Cfb_20(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Crc_21(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Crc32_22(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_EllipticCurve_23(),
	GnuObjectIdentifiers_t848229E7DFA64CDD9553C3E5287CDD2450DE9DAC_StaticFields::get_offset_of_Ed25519_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof (GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A), -1, sizeof(GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5502[3] = 
{
	GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields::get_offset_of_objIds_0(),
	GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields::get_offset_of_curves_1(),
	GMNamedCurves_t71DD7AC79305C4638D6C58B02B58864B0D34216A_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof (SM2P256V1Holder_t50DB69E424E5C97E4E63B59DA19EC910D7E1FE42), -1, sizeof(SM2P256V1Holder_t50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5503[1] = 
{
	SM2P256V1Holder_t50DB69E424E5C97E4E63B59DA19EC910D7E1FE42_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof (WapiP192V1Holder_t077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4), -1, sizeof(WapiP192V1Holder_t077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5504[1] = 
{
	WapiP192V1Holder_t077AAFA1F26BBA929BBB0B8AB47657E2A6302BF4_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof (GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7), -1, sizeof(GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5505[65] = 
{
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm_scheme_0(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm6_ecb_1(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm6_cbc_2(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm6_ofb128_3(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm6_cfb128_4(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm1_ecb_5(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm1_cbc_6(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm1_ofb128_7(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm1_cfb128_8(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm1_cfb1_9(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm1_cfb8_10(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_ssf33_ecb_11(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_ssf33_cbc_12(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_ssf33_ofb128_13(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_ssf33_cfb128_14(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_ssf33_cfb1_15(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_ssf33_cfb8_16(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_ecb_17(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_cbc_18(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_ofb128_19(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_cfb128_20(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_cfb1_21(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_cfb8_22(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_ctr_23(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_gcm_24(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_ccm_25(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_xts_26(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_wrap_27(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_wrap_pad_28(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sms4_ocb_29(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm5_30(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2p256v1_31(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_32(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2exchange_33(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_34(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_wapip192v1_35(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_recommendedParameters_36(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_specifiedParameters_37(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_sm3_38(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_sha1_39(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_sha224_40(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_sha256_41(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_sha384_42(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_sha512_43(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_rmd160_44(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_whirlpool_45(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_blake2b512_46(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_blake2s256_47(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2encrypt_with_md5_48(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_id_sm9PublicKey_49(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm9sign_50(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm9keyagreement_51(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm9encrypt_52(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm3_53(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_hmac_sm3_54(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_sm3_55(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_sha1_56(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_sha256_57(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_sha512_58(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_sha224_59(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_sha384_60(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_rmd160_61(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_whirlpool_62(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_blake2b512_63(),
	GMObjectIdentifiers_t4B6050E725BA3831B60398A4586BE6B260B6EEF7_StaticFields::get_offset_of_sm2sign_with_blake2s256_64(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof (ContentHints_t701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5506[2] = 
{
	ContentHints_t701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070::get_offset_of_contentDescription_2(),
	ContentHints_t701D4C8EEA606954FD08E8E3A5B8CF5CDDC35070::get_offset_of_contentType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof (ContentIdentifier_tF6476ED68CBC248125236E314E72F753A8100E84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5507[1] = 
{
	ContentIdentifier_tF6476ED68CBC248125236E314E72F753A8100E84::get_offset_of_value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { sizeof (EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5508[2] = 
{
	EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960::get_offset_of_certHash_2(),
	EssCertID_tD45CD4EA0FFB464B3C4B32E1BA1542586AE13960::get_offset_of_issuerSerial_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { sizeof (EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F), -1, sizeof(EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5509[4] = 
{
	EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F::get_offset_of_hashAlgorithm_2(),
	EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F::get_offset_of_certHash_3(),
	EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F::get_offset_of_issuerSerial_4(),
	EssCertIDv2_t3F5274C11D0F0BB045B437505E04C13C2DC2A02F_StaticFields::get_offset_of_DefaultAlgID_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof (OtherCertID_t3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5510[2] = 
{
	OtherCertID_t3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F::get_offset_of_otherCertHash_2(),
	OtherCertID_t3079D3F7ABAB664EA1FEDDBCC68F52550EC80F5F::get_offset_of_issuerSerial_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof (OtherSigningCertificate_t9B34871F5B88E22C2A3D9CAB36B5E617F4265631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5511[2] = 
{
	OtherSigningCertificate_t9B34871F5B88E22C2A3D9CAB36B5E617F4265631::get_offset_of_certs_2(),
	OtherSigningCertificate_t9B34871F5B88E22C2A3D9CAB36B5E617F4265631::get_offset_of_policies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof (SigningCertificate_t6A4F6338791ED9D2195235B5700A360FDC892542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5512[2] = 
{
	SigningCertificate_t6A4F6338791ED9D2195235B5700A360FDC892542::get_offset_of_certs_2(),
	SigningCertificate_t6A4F6338791ED9D2195235B5700A360FDC892542::get_offset_of_policies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof (SigningCertificateV2_t46059D698627E8CDAE8DD2480EAEA5388368A44B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5513[2] = 
{
	SigningCertificateV2_t46059D698627E8CDAE8DD2480EAEA5388368A44B::get_offset_of_certs_2(),
	SigningCertificateV2_t46059D698627E8CDAE8DD2480EAEA5388368A44B::get_offset_of_policies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof (CertificateValues_tEB1467FD3FB72D052E4FB29012F86B162A1D08A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5514[1] = 
{
	CertificateValues_tEB1467FD3FB72D052E4FB29012F86B162A1D08A0::get_offset_of_certificates_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof (CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB), -1, sizeof(CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5515[6] = 
{
	CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields::get_offset_of_ProofOfOrigin_0(),
	CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields::get_offset_of_ProofOfReceipt_1(),
	CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields::get_offset_of_ProofOfDelivery_2(),
	CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields::get_offset_of_ProofOfSender_3(),
	CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields::get_offset_of_ProofOfApproval_4(),
	CommitmentTypeIdentifier_tA699B23B186FED32C10332E740438616E1B063FB_StaticFields::get_offset_of_ProofOfCreation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof (CommitmentTypeIndication_t1E85285E40C83CC6E4A892793146F1E99954B992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5516[2] = 
{
	CommitmentTypeIndication_t1E85285E40C83CC6E4A892793146F1E99954B992::get_offset_of_commitmentTypeId_2(),
	CommitmentTypeIndication_t1E85285E40C83CC6E4A892793146F1E99954B992::get_offset_of_commitmentTypeQualifier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof (CommitmentTypeQualifier_t1989ED9E1B155B8E9FDB65037958C4A368755916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5517[2] = 
{
	CommitmentTypeQualifier_t1989ED9E1B155B8E9FDB65037958C4A368755916::get_offset_of_commitmentTypeIdentifier_2(),
	CommitmentTypeQualifier_t1989ED9E1B155B8E9FDB65037958C4A368755916::get_offset_of_qualifier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof (CompleteCertificateRefs_tF663169D20693C6AB43CF1B3018E0CB128757399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5518[1] = 
{
	CompleteCertificateRefs_tF663169D20693C6AB43CF1B3018E0CB128757399::get_offset_of_otherCertIDs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof (CompleteRevocationRefs_t6A58040CABF742B76E3D34B75F3FC099B3BCCDC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5519[1] = 
{
	CompleteRevocationRefs_t6A58040CABF742B76E3D34B75F3FC099B3BCCDC8::get_offset_of_crlOcspRefs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof (CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5520[3] = 
{
	CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960::get_offset_of_crlIssuer_2(),
	CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960::get_offset_of_crlIssuedTime_3(),
	CrlIdentifier_t43A1C40B41952282F3F148EAB6C1FCA30AE69960::get_offset_of_crlNumber_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { sizeof (CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5521[1] = 
{
	CrlListID_t5BAB351EEBD3738751F7FBBD1923CA60BC7708A9::get_offset_of_crls_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof (CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5522[3] = 
{
	CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17::get_offset_of_crlids_2(),
	CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17::get_offset_of_ocspids_3(),
	CrlOcspRef_t999C0D1BBDCB128F59D60183DE8E9A4AB141AE17::get_offset_of_otherRev_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof (CrlValidatedID_tF1817EEB9F620BF1044DFFE42174FE14BE8E983B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5523[2] = 
{
	CrlValidatedID_tF1817EEB9F620BF1044DFFE42174FE14BE8E983B::get_offset_of_crlHash_2(),
	CrlValidatedID_tF1817EEB9F620BF1044DFFE42174FE14BE8E983B::get_offset_of_crlIdentifier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { sizeof (EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4), -1, sizeof(EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5524[14] = 
{
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_SigPolicyId_0(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_CommitmentType_1(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_SignerLocation_2(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_SignerAttr_3(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_OtherSigCert_4(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_ContentTimestamp_5(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_CertificateRefs_6(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_RevocationRefs_7(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_CertValues_8(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_RevocationValues_9(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_EscTimeStamp_10(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_CertCrlTimestamp_11(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_ArchiveTimestamp_12(),
	EsfAttributes_t8E93F9992831B8953EDBDB732792B578DF0354F4_StaticFields::get_offset_of_ArchiveTimestampV2_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof (OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5525[2] = 
{
	OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83::get_offset_of_ocspResponderID_2(),
	OcspIdentifier_tAD1EF4BCDC2A5C9754A217C2380B252DF9067E83::get_offset_of_producedAt_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof (OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5526[1] = 
{
	OcspListID_t8B44191179E526A02CA5C412B8F1E892F968AD36::get_offset_of_ocspResponses_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof (OcspResponsesID_tE7E3463C3FC01F094D4BDD547573219BDD808737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5527[2] = 
{
	OcspResponsesID_tE7E3463C3FC01F094D4BDD547573219BDD808737::get_offset_of_ocspIdentifier_2(),
	OcspResponsesID_tE7E3463C3FC01F094D4BDD547573219BDD808737::get_offset_of_ocspRepHash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof (OtherCertID_t918B62BEAAD2243F225E9E3532C51D84BF7855DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5528[2] = 
{
	OtherCertID_t918B62BEAAD2243F225E9E3532C51D84BF7855DC::get_offset_of_otherCertHash_2(),
	OtherCertID_t918B62BEAAD2243F225E9E3532C51D84BF7855DC::get_offset_of_issuerSerial_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof (OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5529[2] = 
{
	OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8::get_offset_of_sha1Hash_2(),
	OtherHash_tFCD1E6D1E0D9F847844CC0DA3498FB7BCFBE0FF8::get_offset_of_otherHash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof (OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5530[2] = 
{
	OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F::get_offset_of_hashAlgorithm_2(),
	OtherHashAlgAndValue_t4DEA06CC642ACCC7124772E895B4F2606C7A965F::get_offset_of_hashValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof (OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5531[2] = 
{
	OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C::get_offset_of_otherRevRefType_2(),
	OtherRevRefs_tDFB4E3B09507FF418B55BE56C83F431769FAC15C::get_offset_of_otherRevRefs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof (OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5532[2] = 
{
	OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99::get_offset_of_otherRevValType_2(),
	OtherRevVals_t419B7E046F5C79AC5F4D65538EF8B9B044CD6F99::get_offset_of_otherRevVals_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof (OtherSigningCertificate_t2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5533[2] = 
{
	OtherSigningCertificate_t2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3::get_offset_of_certs_2(),
	OtherSigningCertificate_t2AA09A3E3ACE3A21E824BFD1CEA0109A9C8266E3::get_offset_of_policies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof (RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5534[3] = 
{
	RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55::get_offset_of_crlVals_2(),
	RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55::get_offset_of_ocspVals_3(),
	RevocationValues_tCBAC69C4DDC7E5EAF86A89B8E3A3D958E1B24D55::get_offset_of_otherRevVals_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { sizeof (SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5535[3] = 
{
	SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9::get_offset_of_sigPolicyIdentifier_2(),
	SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9::get_offset_of_sigPolicyHash_3(),
	SignaturePolicyId_t67FE48B82D232944F97BD575B0917EE6C3F5C6A9::get_offset_of_sigPolicyQualifiers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof (SignaturePolicyIdentifier_t2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5536[1] = 
{
	SignaturePolicyIdentifier_t2EA0C9771ED1CFAC07F4FE1C0C9A747A6DEC8E32::get_offset_of_sigPolicy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof (SignerAttribute_tBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5537[2] = 
{
	SignerAttribute_tBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18::get_offset_of_claimedAttributes_2(),
	SignerAttribute_tBD7B66E6F030D8CE966E7A121DCCF2B9065ECA18::get_offset_of_certifiedAttributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { sizeof (SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5538[3] = 
{
	SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6::get_offset_of_countryName_2(),
	SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6::get_offset_of_localityName_3(),
	SignerLocation_t1D2BF17C812C530689FE079184A1D689BFD3C7C6::get_offset_of_postalAddress_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof (SigPolicyQualifierInfo_t43A41A5356ECAF49B0DB414FA95B95016A00C518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5539[2] = 
{
	SigPolicyQualifierInfo_t43A41A5356ECAF49B0DB414FA95B95016A00C518::get_offset_of_sigPolicyQualifierId_2(),
	SigPolicyQualifierInfo_t43A41A5356ECAF49B0DB414FA95B95016A00C518::get_offset_of_sigQualifier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof (EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80), -1, sizeof(EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5540[5] = 
{
	EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields::get_offset_of_id_edwards_curve_algs_0(),
	EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields::get_offset_of_id_X25519_1(),
	EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields::get_offset_of_id_X448_2(),
	EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields::get_offset_of_id_Ed25519_3(),
	EdECObjectIdentifiers_t7FF38B186AFA54002FF60BD83076FA93D591EE80_StaticFields::get_offset_of_id_Ed448_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof (EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747), -1, sizeof(EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5541[21] = 
{
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_bsi_de_0(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_PK_1(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_PK_DH_2(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_PK_ECDH_3(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_CA_4(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_CA_DH_5(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_CA_DH_3DES_CBC_CBC_6(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_CA_ECDH_7(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_CA_ECDH_3DES_CBC_CBC_8(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_9(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_RSA_10(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_RSA_v1_5_SHA_1_11(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_RSA_v1_5_SHA_256_12(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_RSA_PSS_SHA_1_13(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_RSA_PSS_SHA_256_14(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_ECDSA_15(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_ECDSA_SHA_1_16(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_ECDSA_SHA_224_17(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_ECDSA_SHA_256_18(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_ECDSA_SHA_384_19(),
	EacObjectIdentifiers_t81EF2239B5EB4C6E61C40D16E4A01A494605F747_StaticFields::get_offset_of_id_TA_ECDSA_SHA_512_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof (CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB), -1, sizeof(CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5542[24] = 
{
	0,
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3411_1(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3411Hmac_2(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR28147Cbc_3(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_ID_Gost28147_89_CryptoPro_A_ParamSet_4(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94_5(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x2001_6(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3411x94WithGostR3410x94_7(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3411x94WithGostR3410x2001_8(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3411x94CryptoProParamSet_9(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProA_10(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProB_11(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProC_12(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProD_13(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProXchA_14(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProXchB_15(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x94CryptoProXchC_16(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x2001CryptoProA_17(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x2001CryptoProB_18(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x2001CryptoProC_19(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x2001CryptoProXchA_20(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostR3410x2001CryptoProXchB_21(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostElSgDH3410Default_22(),
	CryptoProObjectIdentifiers_tF37C22EAC324820E76AC35A88C9990BCFE9D6AAB_StaticFields::get_offset_of_GostElSgDH3410x1_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof (ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4), -1, sizeof(ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5543[3] = 
{
	ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields::get_offset_of_objIds_0(),
	ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields::get_offset_of_parameters_1(),
	ECGost3410NamedCurves_tA479D3D002C7EB97072E90B18DEB39F6D01226E4_StaticFields::get_offset_of_names_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof (ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5544[6] = 
{
	ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA::get_offset_of_p_2(),
	ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA::get_offset_of_q_3(),
	ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA::get_offset_of_a_4(),
	ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA::get_offset_of_b_5(),
	ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA::get_offset_of_x_6(),
	ECGost3410ParamSetParameters_tE1115E1467F0615E73766B1925AFA7CDD2D657FA::get_offset_of_y_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof (Gost28147Parameters_tBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5545[2] = 
{
	Gost28147Parameters_tBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E::get_offset_of_iv_2(),
	Gost28147Parameters_tBC1D3496F0660D6BC7EBFDECBBDA5250F69CB16E::get_offset_of_paramSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof (Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387), -1, sizeof(Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5546[5] = 
{
	Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields::get_offset_of_objIds_0(),
	Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields::get_offset_of_parameters_1(),
	Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields::get_offset_of_cryptoProA_2(),
	Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields::get_offset_of_cryptoProB_3(),
	Gost3410NamedParameters_tB5309CE384AA22B1A3BB3A766E96717DA28B8387_StaticFields::get_offset_of_cryptoProXchA_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof (Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5547[4] = 
{
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85::get_offset_of_keySize_2(),
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85::get_offset_of_p_3(),
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85::get_offset_of_q_4(),
	Gost3410ParamSetParameters_t3E7F86850C0B8D84550194C3E607F103A7C41E85::get_offset_of_a_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof (Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5548[3] = 
{
	Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1::get_offset_of_publicKeyParamSet_2(),
	Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1::get_offset_of_digestParamSet_3(),
	Gost3410PublicKeyAlgParameters_tC49BE7AD18FC9DDE163864F74D309DF13D10C2B1::get_offset_of_encryptionParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof (AttributeTypeAndValue_t2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5549[2] = 
{
	AttributeTypeAndValue_t2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E::get_offset_of_type_2(),
	AttributeTypeAndValue_t2B4BC4BCEA6E533D8E7B88BEEABA351F7016AE1E::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof (CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5550[2] = 
{
	CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D::get_offset_of_issuer_2(),
	CertId_tF9231977B4488C3D74B45E577937A80CB6D4577D::get_offset_of_serialNumber_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof (CertReqMessages_t48C84096D883D692DD2D39E41B8C10E256637BCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5551[1] = 
{
	CertReqMessages_t48C84096D883D692DD2D39E41B8C10E256637BCC::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof (CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5552[3] = 
{
	CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E::get_offset_of_certReq_2(),
	CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E::get_offset_of_popo_3(),
	CertReqMsg_t06B6861581FDEE6E1BCB01AAB5BE90D057C8376E::get_offset_of_regInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof (CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5553[3] = 
{
	CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85::get_offset_of_certReqId_2(),
	CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85::get_offset_of_certTemplate_3(),
	CertRequest_t7F64480A50BA13FA9653D8294D7630EC972C0F85::get_offset_of_controls_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof (CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5554[11] = 
{
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_seq_2(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_version_3(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_serialNumber_4(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_signingAlg_5(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_issuer_6(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_validity_7(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_subject_8(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_publicKey_9(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_issuerUID_10(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_subjectUID_11(),
	CertTemplate_t1B7DF1CEB50247D68DEF803F8D6EC02D4E11841C::get_offset_of_extensions_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof (CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5555[10] = 
{
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_version_0(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_serialNumber_1(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_signingAlg_2(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_issuer_3(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_validity_4(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_subject_5(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_publicKey_6(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_issuerUID_7(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_subjectUID_8(),
	CertTemplateBuilder_t4C84B0E1A9854C94E36CA67B764AA629E5E1B363::get_offset_of_extensions_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { sizeof (Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5556[1] = 
{
	Controls_t688340530A41FAB7480C51FA96CC9E1732E3FD2B::get_offset_of_content_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof (CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA), -1, sizeof(CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5557[8] = 
{
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_pkix_0(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_pkip_1(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_regCtrl_2(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_regCtrl_regToken_3(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_regCtrl_authenticator_4(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_regCtrl_pkiPublicationInfo_5(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_regCtrl_pkiArchiveOptions_6(),
	CrmfObjectIdentifiers_tC601F3FAFBDDE9F9E061DC9CA9E7FC856320A9FA_StaticFields::get_offset_of_id_ct_encKeyWithID_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof (EncKeyWithID_tC7DC5DF35A711B3FEA02B639C1C178E44E69051E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5558[2] = 
{
	EncKeyWithID_tC7DC5DF35A711B3FEA02B639C1C178E44E69051E::get_offset_of_privKeyInfo_2(),
	EncKeyWithID_tC7DC5DF35A711B3FEA02B639C1C178E44E69051E::get_offset_of_identifier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof (EncryptedKey_t9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5559[2] = 
{
	EncryptedKey_t9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3::get_offset_of_envelopedData_2(),
	EncryptedKey_t9E704B0C0255FFFDE56E98F3DDEF66C599D58BA3::get_offset_of_encryptedValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof (EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5560[6] = 
{
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66::get_offset_of_intendedAlg_2(),
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66::get_offset_of_symmAlg_3(),
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66::get_offset_of_encSymmKey_4(),
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66::get_offset_of_keyAlg_5(),
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66::get_offset_of_valueHint_6(),
	EncryptedValue_tD5EF992957BBA6A065198DD6B8C20C38FC336D66::get_offset_of_encValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof (OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5561[2] = 
{
	OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3::get_offset_of_notBefore_2(),
	OptionalValidity_tE58BCC4E2A4ED7889ABA897FF4475E03DFBF68E3::get_offset_of_notAfter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof (PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5562[4] = 
{
	0,
	0,
	0,
	PkiArchiveOptions_t11553A43A825C1AA4A4B17DE4F3DB9E84DF9B482::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof (PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5563[2] = 
{
	PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950::get_offset_of_action_2(),
	PkiPublicationInfo_t61EE698B09F3C95DFE0520755D3F4FA2CB059950::get_offset_of_pubInfos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof (PKMacValue_t2354860B8647226235395857CE076879CE35713C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5564[2] = 
{
	PKMacValue_t2354860B8647226235395857CE076879CE35713C::get_offset_of_algID_2(),
	PKMacValue_t2354860B8647226235395857CE076879CE35713C::get_offset_of_macValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { sizeof (PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5565[7] = 
{
	0,
	0,
	0,
	0,
	0,
	PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95::get_offset_of_tagNo_7(),
	PopoPrivKey_t9A923197C93F6D58810D84652635D9CB6D647B95::get_offset_of_obj_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { sizeof (PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5566[3] = 
{
	PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175::get_offset_of_poposkInput_2(),
	PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175::get_offset_of_algorithmIdentifier_3(),
	PopoSigningKey_t48FAE179F66B29DAE761F4BB0B08B3B6AE0EF175::get_offset_of_signature_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof (PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5567[3] = 
{
	PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F::get_offset_of_sender_2(),
	PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F::get_offset_of_publicKeyMac_3(),
	PopoSigningKeyInput_t7A29E2A223CC642FD0B381323E88F1C16FD1A00F::get_offset_of_publicKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof (ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5568[6] = 
{
	0,
	0,
	0,
	0,
	ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E::get_offset_of_tagNo_6(),
	ProofOfPossession_t973ECCBA591187FB3A2BDF98B4C11008329F845E::get_offset_of_obj_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof (SinglePubInfo_tABDD49B93083C05AA436BC16E728EB5FC7C8EDA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5569[2] = 
{
	SinglePubInfo_tABDD49B93083C05AA436BC16E728EB5FC7C8EDA5::get_offset_of_pubMethod_2(),
	SinglePubInfo_tABDD49B93083C05AA436BC16E728EB5FC7C8EDA5::get_offset_of_pubLocation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof (SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313), -1, sizeof(SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5570[2] = 
{
	SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313_StaticFields::get_offset_of_encrCert_7(),
	SubsequentMessage_t220D29A6CC1439A4E8C73576801AB199191E1313_StaticFields::get_offset_of_challengeResp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof (Attribute_tEB73DC2A1B61AE4935F463D0E09A6D976D724B34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5571[2] = 
{
	Attribute_tEB73DC2A1B61AE4935F463D0E09A6D976D724B34::get_offset_of_attrType_2(),
	Attribute_tEB73DC2A1B61AE4935F463D0E09A6D976D724B34::get_offset_of_attrValues_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof (Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5572[1] = 
{
	Attributes_t39AE0974185A2F49463E4112262D3CEC2CB9C7F0::get_offset_of_attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof (AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5573[1] = 
{
	AttributeTable_tEC92A1C170225F681A9CAFD570D69CC94FEF74E0::get_offset_of_attributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof (AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5574[9] = 
{
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_version_2(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_originatorInfo_3(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_recipientInfos_4(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_macAlgorithm_5(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_digestAlgorithm_6(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_encapsulatedContentInfo_7(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_authAttrs_8(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_mac_9(),
	AuthenticatedData_t9AA4C6281375BFF1CC345E32B6088F95DC803EA1::get_offset_of_unauthAttrs_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof (AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5575[4] = 
{
	AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A::get_offset_of_seq_0(),
	AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A::get_offset_of_version_1(),
	AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A::get_offset_of_nextObject_2(),
	AuthenticatedDataParser_tAC489D0114431C09BAC377DF6BE0FEDF2462D80A::get_offset_of_originatorInfoCalled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof (AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5576[7] = 
{
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_version_2(),
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_originatorInfo_3(),
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_recipientInfos_4(),
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_authEncryptedContentInfo_5(),
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_authAttrs_6(),
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_mac_7(),
	AuthEnvelopedData_t7224956C3F79DB63620A87C1E332445EC3F8D172::get_offset_of_unauthAttrs_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { sizeof (AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5577[4] = 
{
	AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0::get_offset_of_seq_0(),
	AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0::get_offset_of_version_1(),
	AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0::get_offset_of_nextObject_2(),
	AuthEnvelopedDataParser_tB3EAB355F283EA2E7864DA447D6AEB24AE3D79B0::get_offset_of_originatorInfoCalled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof (CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29), -1, sizeof(CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5578[5] = 
{
	CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields::get_offset_of_ContentType_0(),
	CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields::get_offset_of_MessageDigest_1(),
	CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields::get_offset_of_SigningTime_2(),
	CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields::get_offset_of_CounterSignature_3(),
	CmsAttributes_tFCFBD0DE1DD3D2884BE6169F851BF07F52026F29_StaticFields::get_offset_of_ContentHint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof (CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1), -1, sizeof(CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5579[13] = 
{
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_Data_0(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_SignedData_1(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_EnvelopedData_2(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_SignedAndEnvelopedData_3(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_DigestedData_4(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_EncryptedData_5(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_AuthenticatedData_6(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_CompressedData_7(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_AuthEnvelopedData_8(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_timestampedData_9(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_id_ri_10(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_id_ri_ocsp_response_11(),
	CmsObjectIdentifiers_t75C947E70271646CF2A87A78EE9D98682AD50FC1_StaticFields::get_offset_of_id_ri_scvp_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof (CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5580[3] = 
{
	CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888::get_offset_of_version_2(),
	CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888::get_offset_of_compressionAlgorithm_3(),
	CompressedData_t3EA0EE1F60A13558F587E3D4CACA08B001739888::get_offset_of_encapContentInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof (CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5581[3] = 
{
	CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB::get_offset_of__version_0(),
	CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB::get_offset_of__compressionAlgorithm_1(),
	CompressedDataParser_t859B2FBA14F16FE0C2EA1313CACDD97B63837ACB::get_offset_of__encapContentInfo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof (ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5582[2] = 
{
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42::get_offset_of_contentType_2(),
	ContentInfo_t0CE96735DA3BC2263C09F12714466925DD4A1B42::get_offset_of_content_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof (ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5583[2] = 
{
	ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC::get_offset_of_contentType_0(),
	ContentInfoParser_tF0FC6154D3994C9FE01E2BB1F83BE08F8A31F6DC::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof (EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5584[3] = 
{
	EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E::get_offset_of_contentType_2(),
	EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E::get_offset_of_contentEncryptionAlgorithm_3(),
	EncryptedContentInfo_tCDB7DE5FFD52FE63DD57B7B0788E03315ED5103E::get_offset_of_encryptedContent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof (EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5585[3] = 
{
	EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7::get_offset_of__contentType_0(),
	EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7::get_offset_of__contentEncryptionAlgorithm_1(),
	EncryptedContentInfoParser_t0F83DCE4C053E418405C23DBE874853332A740C7::get_offset_of__encryptedContent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof (EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5586[3] = 
{
	EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44::get_offset_of_version_2(),
	EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44::get_offset_of_encryptedContentInfo_3(),
	EncryptedData_t4E62B4038A1DABE771B68C3F1D8F434DCC3A8B44::get_offset_of_unprotectedAttrs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof (EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5587[5] = 
{
	EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594::get_offset_of_version_2(),
	EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594::get_offset_of_originatorInfo_3(),
	EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594::get_offset_of_recipientInfos_4(),
	EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594::get_offset_of_encryptedContentInfo_5(),
	EnvelopedData_t8827C3D99E7EC283E429E5C8E535E134F7DD9594::get_offset_of_unprotectedAttrs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof (EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5588[4] = 
{
	EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F::get_offset_of__seq_0(),
	EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F::get_offset_of__version_1(),
	EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F::get_offset_of__nextObject_2(),
	EnvelopedDataParser_tE83D89D81BB3074AEDF43F52716E9E883114109F::get_offset_of__originatorInfoCalled_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof (Evidence_t6897C267406A516689941A0FF07964FF20B44F07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5589[2] = 
{
	Evidence_t6897C267406A516689941A0FF07964FF20B44F07::get_offset_of_tstEvidence_2(),
	Evidence_t6897C267406A516689941A0FF07964FF20B44F07::get_offset_of_otherEvidence_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof (IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5590[2] = 
{
	IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC::get_offset_of_name_2(),
	IssuerAndSerialNumber_t6159295C18C7D8295C75D0244C193FF88B2262BC::get_offset_of_serialNumber_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof (KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5591[3] = 
{
	KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658::get_offset_of_keyIdentifier_2(),
	KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658::get_offset_of_date_3(),
	KekIdentifier_t13E1806BE5B4A79B4F09B9CCE05BEB2B3228E658::get_offset_of_other_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof (KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5592[4] = 
{
	KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6::get_offset_of_version_2(),
	KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6::get_offset_of_kekID_3(),
	KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6::get_offset_of_keyEncryptionAlgorithm_4(),
	KekRecipientInfo_t6BF48133D8AFC48D834E59F40E8EF723FC3604E6::get_offset_of_encryptedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof (KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5593[2] = 
{
	KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9::get_offset_of_issuerSerial_2(),
	KeyAgreeRecipientIdentifier_tE1BCEDD8BF953D70EA3264CA4AAF812F04FB36E9::get_offset_of_rKeyID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof (KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5594[5] = 
{
	KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E::get_offset_of_version_2(),
	KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E::get_offset_of_originator_3(),
	KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E::get_offset_of_ukm_4(),
	KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E::get_offset_of_keyEncryptionAlgorithm_5(),
	KeyAgreeRecipientInfo_tBC1E1FEE80DAF0E5C4E62E844BF522D59605420E::get_offset_of_recipientEncryptedKeys_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof (KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5595[4] = 
{
	KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9::get_offset_of_version_2(),
	KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9::get_offset_of_rid_3(),
	KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9::get_offset_of_keyEncryptionAlgorithm_4(),
	KeyTransRecipientInfo_t2048CFC2470F33A81481679C4FE23F005D96A8F9::get_offset_of_encryptedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof (MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5596[4] = 
{
	MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A::get_offset_of_hashProtected_2(),
	MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A::get_offset_of_fileName_3(),
	MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A::get_offset_of_mediaType_4(),
	MetaData_t43063EFD956FFB06BE9E1F7AB0A831CB6B2E2D0A::get_offset_of_otherMetaData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof (OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5597[1] = 
{
	OriginatorIdentifierOrKey_tBE9FA95E6C292D9CF0E0236984129F890D891F3E::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof (OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5598[2] = 
{
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F::get_offset_of_certs_2(),
	OriginatorInfo_tAB84D6D318A415FF288A4E699F9A6770DE67427F::get_offset_of_crls_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof (OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5599[2] = 
{
	OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209::get_offset_of_mAlgorithm_2(),
	OriginatorPublicKey_t860ECE14956B5E7E4A98C44017B2529C39C8F209::get_offset_of_mPublicKey_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
