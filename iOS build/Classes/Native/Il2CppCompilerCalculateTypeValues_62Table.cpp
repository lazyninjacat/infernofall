﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3;
// Newtonsoft.Json.JsonReader
struct JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623;
// Newtonsoft.Json.JsonTextWriter
struct JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867;
// Newtonsoft.Json.Linq.JArray
struct JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB;
// Newtonsoft.Json.Linq.JObject
struct JObject_t786AF07B1009334856B0362BBC48EEF68C81C585;
// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F;
// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D;
// Newtonsoft.Json.Linq.JToken
struct JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344;
// Newtonsoft.Json.Linq.JValue
struct JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF;
// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter
struct ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B;
// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter
struct ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F;
// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter
struct ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3;
// Newtonsoft.Json.Linq.JsonPath.FieldFilter
struct FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B;
// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter
struct FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B;
// Newtonsoft.Json.Linq.JsonPath.QueryExpression
struct QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F;
// Newtonsoft.Json.Linq.JsonPath.QueryFilter
struct QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF;
// Newtonsoft.Json.Linq.JsonPath.ScanFilter
struct ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_tC743A5579C4FEE7AABBE8063B8A13C44C524E10C;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_tEAEB2EE1239E8F13092235DB634358C5BDE6EB8C;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t8CE53BA9C98C0CE46845BBEB4FEF453F676C0743;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>
struct ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>
struct ThreadSafeStore_2_tC340361EB699198C223919D1635F850FEC1CBC31;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>
struct ThreadSafeStore_2_t93819CC1D004B6F647763930BB932DB7DDF99C11;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>>
struct Dictionary_2_t1C588111C95B675E14518EE35497D6BB383A7979;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_ColorGradient>
struct Dictionary_2_t3D8062B54231EF452E0BDF087D00AE161A359E80;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset>
struct Dictionary_2_t5027D1A6C399063D7319CDF3347CBDF7F5E660CA;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset>
struct Dictionary_2_t4EB591B9FEEC3EFCF9551F1834394DC59DFECDEC;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t894EF327CB2DFFC12CADBC804C7907CDA55E898C;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken>
struct Dictionary_2_t36CA3484108026EE9A1EC21D3D860B8C8F80611B;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType>
struct Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t133B7916368C2F565BDFF2674430B992DB49577F;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_tA3DFFFD6133C0C5607FFA19B3E4E7395E872113C;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>>
struct IEnumerator_1_tD9B1737A1A9261F2D2838143F686F007E8555CF4;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_tF9FBB84AB59D8D1F589F22CD75FAABD6EADF106C;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct IList_1_t9B22655AFF504060624BC452ED9E6B257AEF3A2B;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct IList_1_tC7EAA70C1362B15CBB553EEEE046436A96ADB7E3;
// System.Collections.Generic.LinkedList`1<System.Action>
struct LinkedList_1_t0C09473089448E5DF6F815467B91B1D104D38C76;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty>
struct List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken>
struct List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken>
struct List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter>
struct List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.QueryExpression>
struct List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty>
struct Func_2_t3D1D7D4034F6D7BB9962E4CB6723F27F67E28C26;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Func_2_t103CCE97502A49E59199C1C02AF4B40DA45FE783;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String>
struct Func_2_tE62144C0B5815E8169B8090701233215B93D50B7;
// System.Func`2<System.Object,System.Type>
struct Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IO.BinaryWriter
struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3;
// System.IO.StringWriter
struct StringWriter_t194EF1526E072B93984370042AA80926C2EB6139;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_Glyph
struct TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E;
// TMPro.TMP_SubMesh[]
struct TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// TMPro.TextMeshPro
struct TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_tFE12AC6A01EC7B573E971252DAB696F355F76B6B;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;




#ifndef U3CMODULEU3E_TB181AC380D679257A9DFE61F653774B69E5DFF6D_H
#define U3CMODULEU3E_TB181AC380D679257A9DFE61F653774B69E5DFF6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB181AC380D679257A9DFE61F653774B69E5DFF6D 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB181AC380D679257A9DFE61F653774B69E5DFF6D_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BSONOBJECTID_TF24F67A9B09CB213513B5218DF725B4E9AF3379C_H
#define BSONOBJECTID_TF24F67A9B09CB213513B5218DF725B4E9AF3379C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObjectId
struct  BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C  : public RuntimeObject
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::<Value>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C, ___U3CValueU3Ek__BackingField_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTID_TF24F67A9B09CB213513B5218DF725B4E9AF3379C_H
#ifndef BSONPROPERTY_T82747119CE439C13F6809CD9F8D815C3CE429583_H
#define BSONPROPERTY_T82747119CE439C13F6809CD9F8D815C3CE429583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonProperty
struct  BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::<Name>k__BackingField
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * ___U3CNameU3Ek__BackingField_0;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::<Value>k__BackingField
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583, ___U3CNameU3Ek__BackingField_0)); }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A ** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583, ___U3CValueU3Ek__BackingField_1)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONPROPERTY_T82747119CE439C13F6809CD9F8D815C3CE429583_H
#ifndef BSONTOKEN_TEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3_H
#define BSONTOKEN_TEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonToken
struct  BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::<Parent>k__BackingField
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ___U3CParentU3Ek__BackingField_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonToken::<CalculatedSize>k__BackingField
	int32_t ___U3CCalculatedSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3, ___U3CParentU3Ek__BackingField_0)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3, ___U3CCalculatedSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCalculatedSizeU3Ek__BackingField_1() const { return ___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCalculatedSizeU3Ek__BackingField_1() { return &___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline void set_U3CCalculatedSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCalculatedSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTOKEN_TEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3_H
#ifndef JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#define JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T61C31ABA417A4314A2582BF0404CE41267A61BE6_H
#ifndef EXTENSIONS_TEB3365B098821F2F5284FC73DCACC106323A89C8_H
#define EXTENSIONS_TEB3365B098821F2F5284FC73DCACC106323A89C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.Extensions
struct  Extensions_tEB3365B098821F2F5284FC73DCACC106323A89C8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_TEB3365B098821F2F5284FC73DCACC106323A89C8_H
#ifndef JTOKENREFERENCEEQUALITYCOMPARER_T9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_H
#define JTOKENREFERENCEEQUALITYCOMPARER_T9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer_JTokenReferenceEqualityComparer
struct  JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A  : public RuntimeObject
{
public:

public:
};

struct JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JContainer_JTokenReferenceEqualityComparer Newtonsoft.Json.Linq.JContainer_JTokenReferenceEqualityComparer::Instance
	JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_StaticFields, ___Instance_0)); }
	inline JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A * get_Instance_0() const { return ___Instance_0; }
	inline JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREFERENCEEQUALITYCOMPARER_T9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_H
#ifndef JPROPERTYLIST_TAC5DCFB03C7879ED6FCFD74977E76474C918200F_H
#define JPROPERTYLIST_TAC5DCFB03C7879ED6FCFD74977E76474C918200F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty_JPropertyList
struct  JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList::_token
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____token_0;

public:
	inline static int32_t get_offset_of__token_0() { return static_cast<int32_t>(offsetof(JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F, ____token_0)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__token_0() const { return ____token_0; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__token_0() { return &____token_0; }
	inline void set__token_0(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____token_0 = value;
		Il2CppCodeGenWriteBarrier((&____token_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYLIST_TAC5DCFB03C7879ED6FCFD74977E76474C918200F_H
#ifndef U3CGETENUMERATORU3ED__1_T74F96992BB129149414620E86F10CFDF4C199AF8_H
#define U3CGETENUMERATORU3ED__1_T74F96992BB129149414620E86F10CFDF4C199AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1
struct  U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JProperty_JPropertyList Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>4__this
	JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8, ___U3CU3E4__this_2)); }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__1_T74F96992BB129149414620E86F10CFDF4C199AF8_H
#ifndef JTOKEN_TE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_H
#define JTOKEN_TE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken
struct  JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::_parent
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ____parent_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_previous
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____previous_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_next
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____next_2;
	// System.Object Newtonsoft.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_3;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____parent_0)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get__parent_0() const { return ____parent_0; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__previous_1() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____previous_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__previous_1() const { return ____previous_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__previous_1() { return &____previous_1; }
	inline void set__previous_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____previous_1 = value;
		Il2CppCodeGenWriteBarrier((&____previous_1), value);
	}

	inline static int32_t get_offset_of__next_2() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____next_2)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__next_2() const { return ____next_2; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__next_2() { return &____next_2; }
	inline void set__next_2(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____next_2 = value;
		Il2CppCodeGenWriteBarrier((&____next_2), value);
	}

	inline static int32_t get_offset_of__annotations_3() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02, ____annotations_3)); }
	inline RuntimeObject * get__annotations_3() const { return ____annotations_3; }
	inline RuntimeObject ** get_address_of__annotations_3() { return &____annotations_3; }
	inline void set__annotations_3(RuntimeObject * value)
	{
		____annotations_3 = value;
		Il2CppCodeGenWriteBarrier((&____annotations_3), value);
	}
};

struct JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___BooleanTypes_4;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___NumberTypes_5;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___StringTypes_6;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___GuidTypes_7;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___TimeSpanTypes_8;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___UriTypes_9;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___CharTypes_10;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___DateTimeTypes_11;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* ___BytesTypes_12;

public:
	inline static int32_t get_offset_of_BooleanTypes_4() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___BooleanTypes_4)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_BooleanTypes_4() const { return ___BooleanTypes_4; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_BooleanTypes_4() { return &___BooleanTypes_4; }
	inline void set_BooleanTypes_4(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___BooleanTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanTypes_4), value);
	}

	inline static int32_t get_offset_of_NumberTypes_5() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___NumberTypes_5)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_NumberTypes_5() const { return ___NumberTypes_5; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_NumberTypes_5() { return &___NumberTypes_5; }
	inline void set_NumberTypes_5(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___NumberTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___NumberTypes_5), value);
	}

	inline static int32_t get_offset_of_StringTypes_6() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___StringTypes_6)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_StringTypes_6() const { return ___StringTypes_6; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_StringTypes_6() { return &___StringTypes_6; }
	inline void set_StringTypes_6(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___StringTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___StringTypes_6), value);
	}

	inline static int32_t get_offset_of_GuidTypes_7() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___GuidTypes_7)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_GuidTypes_7() const { return ___GuidTypes_7; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_GuidTypes_7() { return &___GuidTypes_7; }
	inline void set_GuidTypes_7(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___GuidTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___GuidTypes_7), value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_8() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___TimeSpanTypes_8)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_TimeSpanTypes_8() const { return ___TimeSpanTypes_8; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_TimeSpanTypes_8() { return &___TimeSpanTypes_8; }
	inline void set_TimeSpanTypes_8(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___TimeSpanTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanTypes_8), value);
	}

	inline static int32_t get_offset_of_UriTypes_9() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___UriTypes_9)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_UriTypes_9() const { return ___UriTypes_9; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_UriTypes_9() { return &___UriTypes_9; }
	inline void set_UriTypes_9(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___UriTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriTypes_9), value);
	}

	inline static int32_t get_offset_of_CharTypes_10() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___CharTypes_10)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_CharTypes_10() const { return ___CharTypes_10; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_CharTypes_10() { return &___CharTypes_10; }
	inline void set_CharTypes_10(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___CharTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___CharTypes_10), value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_11() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___DateTimeTypes_11)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_DateTimeTypes_11() const { return ___DateTimeTypes_11; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_DateTimeTypes_11() { return &___DateTimeTypes_11; }
	inline void set_DateTimeTypes_11(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___DateTimeTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeTypes_11), value);
	}

	inline static int32_t get_offset_of_BytesTypes_12() { return static_cast<int32_t>(offsetof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields, ___BytesTypes_12)); }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* get_BytesTypes_12() const { return ___BytesTypes_12; }
	inline JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344** get_address_of_BytesTypes_12() { return &___BytesTypes_12; }
	inline void set_BytesTypes_12(JTokenTypeU5BU5D_t95EDBAB8E7D19E83A29F71593B6FEC1F54E17344* value)
	{
		___BytesTypes_12 = value;
		Il2CppCodeGenWriteBarrier((&___BytesTypes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKEN_TE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_H
#ifndef U3CGETANCESTORSU3ED__41_TA86F8A2E067567D67E95D02FD20F096E7F14B840_H
#define U3CGETANCESTORSU3ED__41_TA86F8A2E067567D67E95D02FD20F096E7F14B840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41
struct  U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::self
	bool ___self_3;
	// System.Boolean Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>3__self
	bool ___U3CU3E3__self_4;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<>4__this
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken_<GetAncestors>d__41::<current>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CcurrentU3E5__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CU3E4__this_5)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840, ___U3CcurrentU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CcurrentU3E5__1_6() const { return ___U3CcurrentU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CcurrentU3E5__1_6() { return &___U3CcurrentU3E5__1_6; }
	inline void set_U3CcurrentU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CcurrentU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E5__1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETANCESTORSU3ED__41_TA86F8A2E067567D67E95D02FD20F096E7F14B840_H
#ifndef LINEINFOANNOTATION_T275C937C347C8A3C93FFD9CAC915903CDEDDF40D_H
#define LINEINFOANNOTATION_T275C937C347C8A3C93FFD9CAC915903CDEDDF40D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_LineInfoAnnotation
struct  LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::LineNumber
	int32_t ___LineNumber_0;
	// System.Int32 Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::LinePosition
	int32_t ___LinePosition_1;

public:
	inline static int32_t get_offset_of_LineNumber_0() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D, ___LineNumber_0)); }
	inline int32_t get_LineNumber_0() const { return ___LineNumber_0; }
	inline int32_t* get_address_of_LineNumber_0() { return &___LineNumber_0; }
	inline void set_LineNumber_0(int32_t value)
	{
		___LineNumber_0 = value;
	}

	inline static int32_t get_offset_of_LinePosition_1() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D, ___LinePosition_1)); }
	inline int32_t get_LinePosition_1() const { return ___LinePosition_1; }
	inline int32_t* get_address_of_LinePosition_1() { return &___LinePosition_1; }
	inline void set_LinePosition_1(int32_t value)
	{
		___LinePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOANNOTATION_T275C937C347C8A3C93FFD9CAC915903CDEDDF40D_H
#ifndef U3CEXECUTEFILTERU3ED__4_T49283F96CCAA85028EA2C8F9402DA5D35998E1D7_H
#define U3CEXECUTEFILTERU3ED__4_T49283F96CCAA85028EA2C8F9402DA5D35998E1D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>4__this
	ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B * ___U3CU3E4__this_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_7;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<t>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__1_8;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter_<ExecuteFilter>d__4::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E4__this_5)); }
	inline ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___errorWhenNoMatch_6)); }
	inline bool get_errorWhenNoMatch_6() const { return ___errorWhenNoMatch_6; }
	inline bool* get_address_of_errorWhenNoMatch_6() { return &___errorWhenNoMatch_6; }
	inline void set_errorWhenNoMatch_6(bool value)
	{
		___errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E3__errorWhenNoMatch_7)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_7() const { return ___U3CU3E3__errorWhenNoMatch_7; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_7() { return &___U3CU3E3__errorWhenNoMatch_7; }
	inline void set_U3CU3E3__errorWhenNoMatch_7(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CtU3E5__1_8)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__1_8() const { return ___U3CtU3E5__1_8; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__1_8() { return &___U3CtU3E5__1_8; }
	inline void set_U3CtU3E5__1_8(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7, ___U3CU3E7__wrap2_10)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_10() const { return ___U3CU3E7__wrap2_10; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_10() { return &___U3CU3E7__wrap2_10; }
	inline void set_U3CU3E7__wrap2_10(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T49283F96CCAA85028EA2C8F9402DA5D35998E1D7_H
#ifndef U3CEXECUTEFILTERU3ED__12_TBCB91730C7554E4E134042CDDD1550F32438D3F8_H
#define U3CEXECUTEFILTERU3ED__12_TBCB91730C7554E4E134042CDDD1550F32438D3F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12
struct  U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>4__this
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::current
	RuntimeObject* ___current_4;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>3__current
	RuntimeObject* ___U3CU3E3__current_5;
	// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<a>5__1
	JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 * ___U3CaU3E5__1_6;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<i>5__2
	int32_t ___U3CiU3E5__2_7;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<stepCount>5__3
	int32_t ___U3CstepCountU3E5__3_8;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<stopIndex>5__4
	int32_t ___U3CstopIndexU3E5__4_9;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<positiveStep>5__5
	bool ___U3CpositiveStepU3E5__5_10;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::errorWhenNoMatch
	bool ___errorWhenNoMatch_11;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_12;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<t>5__6
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__6_13;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter_<ExecuteFilter>d__12::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E4__this_3)); }
	inline ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___current_4)); }
	inline RuntimeObject* get_current_4() const { return ___current_4; }
	inline RuntimeObject** get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(RuntimeObject* value)
	{
		___current_4 = value;
		Il2CppCodeGenWriteBarrier((&___current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E3__current_5)); }
	inline RuntimeObject* get_U3CU3E3__current_5() const { return ___U3CU3E3__current_5; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_5() { return &___U3CU3E3__current_5; }
	inline void set_U3CU3E3__current_5(RuntimeObject* value)
	{
		___U3CU3E3__current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_5), value);
	}

	inline static int32_t get_offset_of_U3CaU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CaU3E5__1_6)); }
	inline JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 * get_U3CaU3E5__1_6() const { return ___U3CaU3E5__1_6; }
	inline JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 ** get_address_of_U3CaU3E5__1_6() { return &___U3CaU3E5__1_6; }
	inline void set_U3CaU3E5__1_6(JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11 * value)
	{
		___U3CaU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CiU3E5__2_7)); }
	inline int32_t get_U3CiU3E5__2_7() const { return ___U3CiU3E5__2_7; }
	inline int32_t* get_address_of_U3CiU3E5__2_7() { return &___U3CiU3E5__2_7; }
	inline void set_U3CiU3E5__2_7(int32_t value)
	{
		___U3CiU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CstepCountU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CstepCountU3E5__3_8)); }
	inline int32_t get_U3CstepCountU3E5__3_8() const { return ___U3CstepCountU3E5__3_8; }
	inline int32_t* get_address_of_U3CstepCountU3E5__3_8() { return &___U3CstepCountU3E5__3_8; }
	inline void set_U3CstepCountU3E5__3_8(int32_t value)
	{
		___U3CstepCountU3E5__3_8 = value;
	}

	inline static int32_t get_offset_of_U3CstopIndexU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CstopIndexU3E5__4_9)); }
	inline int32_t get_U3CstopIndexU3E5__4_9() const { return ___U3CstopIndexU3E5__4_9; }
	inline int32_t* get_address_of_U3CstopIndexU3E5__4_9() { return &___U3CstopIndexU3E5__4_9; }
	inline void set_U3CstopIndexU3E5__4_9(int32_t value)
	{
		___U3CstopIndexU3E5__4_9 = value;
	}

	inline static int32_t get_offset_of_U3CpositiveStepU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CpositiveStepU3E5__5_10)); }
	inline bool get_U3CpositiveStepU3E5__5_10() const { return ___U3CpositiveStepU3E5__5_10; }
	inline bool* get_address_of_U3CpositiveStepU3E5__5_10() { return &___U3CpositiveStepU3E5__5_10; }
	inline void set_U3CpositiveStepU3E5__5_10(bool value)
	{
		___U3CpositiveStepU3E5__5_10 = value;
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___errorWhenNoMatch_11)); }
	inline bool get_errorWhenNoMatch_11() const { return ___errorWhenNoMatch_11; }
	inline bool* get_address_of_errorWhenNoMatch_11() { return &___errorWhenNoMatch_11; }
	inline void set_errorWhenNoMatch_11(bool value)
	{
		___errorWhenNoMatch_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E3__errorWhenNoMatch_12)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_12() const { return ___U3CU3E3__errorWhenNoMatch_12; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_12() { return &___U3CU3E3__errorWhenNoMatch_12; }
	inline void set_U3CU3E3__errorWhenNoMatch_12(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_12 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__6_13() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CtU3E5__6_13)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__6_13() const { return ___U3CtU3E5__6_13; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__6_13() { return &___U3CtU3E5__6_13; }
	inline void set_U3CtU3E5__6_13(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__6_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_14() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8, ___U3CU3E7__wrap1_14)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_14() const { return ___U3CU3E7__wrap1_14; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_14() { return &___U3CU3E7__wrap1_14; }
	inline void set_U3CU3E7__wrap1_14(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__12_TBCB91730C7554E4E134042CDDD1550F32438D3F8_H
#ifndef U3CEXECUTEFILTERU3ED__4_TAF0C3B976817618E87E3152BBD9B3A1DF5D92610_H
#define U3CEXECUTEFILTERU3ED__4_TAF0C3B976817618E87E3152BBD9B3A1DF5D92610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.FieldFilter Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>4__this
	FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B * ___U3CU3E4__this_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_7;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<o>5__1
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CoU3E5__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<t>5__2
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__2_9;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_10;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JsonPath.FieldFilter_<ExecuteFilter>d__4::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E4__this_5)); }
	inline FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___errorWhenNoMatch_6)); }
	inline bool get_errorWhenNoMatch_6() const { return ___errorWhenNoMatch_6; }
	inline bool* get_address_of_errorWhenNoMatch_6() { return &___errorWhenNoMatch_6; }
	inline void set_errorWhenNoMatch_6(bool value)
	{
		___errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E3__errorWhenNoMatch_7)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_7() const { return ___U3CU3E3__errorWhenNoMatch_7; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_7() { return &___U3CU3E3__errorWhenNoMatch_7; }
	inline void set_U3CU3E3__errorWhenNoMatch_7(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CoU3E5__1_8)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CoU3E5__1_8() const { return ___U3CoU3E5__1_8; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CoU3E5__1_8() { return &___U3CoU3E5__1_8; }
	inline void set_U3CoU3E5__1_8(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CoU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CtU3E5__2_9)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E7__wrap1_10)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_10() const { return ___U3CU3E7__wrap1_10; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_10() { return &___U3CU3E7__wrap1_10; }
	inline void set_U3CU3E7__wrap1_10(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610, ___U3CU3E7__wrap2_11)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_11() const { return ___U3CU3E7__wrap2_11; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_11() { return &___U3CU3E7__wrap2_11; }
	inline void set_U3CU3E7__wrap2_11(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_TAF0C3B976817618E87E3152BBD9B3A1DF5D92610_H
#ifndef U3CU3EC_T7AF93AC964FCE6A240C24249983367E897CC068D_H
#define U3CU3EC_T7AF93AC964FCE6A240C24249983367E897CC068D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c
struct  U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c::<>9
	U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<>c::<>9__4_0
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7AF93AC964FCE6A240C24249983367E897CC068D_H
#ifndef JPATH_T3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7_H
#define JPATH_T3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.JPath
struct  JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.JPath::_expression
	String_t* ____expression_0;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter> Newtonsoft.Json.Linq.JsonPath.JPath::<Filters>k__BackingField
	List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * ___U3CFiltersU3Ek__BackingField_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.JPath::_currentIndex
	int32_t ____currentIndex_2;

public:
	inline static int32_t get_offset_of__expression_0() { return static_cast<int32_t>(offsetof(JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7, ____expression_0)); }
	inline String_t* get__expression_0() const { return ____expression_0; }
	inline String_t** get_address_of__expression_0() { return &____expression_0; }
	inline void set__expression_0(String_t* value)
	{
		____expression_0 = value;
		Il2CppCodeGenWriteBarrier((&____expression_0), value);
	}

	inline static int32_t get_offset_of_U3CFiltersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7, ___U3CFiltersU3Ek__BackingField_1)); }
	inline List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * get_U3CFiltersU3Ek__BackingField_1() const { return ___U3CFiltersU3Ek__BackingField_1; }
	inline List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 ** get_address_of_U3CFiltersU3Ek__BackingField_1() { return &___U3CFiltersU3Ek__BackingField_1; }
	inline void set_U3CFiltersU3Ek__BackingField_1(List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * value)
	{
		___U3CFiltersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFiltersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__currentIndex_2() { return static_cast<int32_t>(offsetof(JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7, ____currentIndex_2)); }
	inline int32_t get__currentIndex_2() const { return ____currentIndex_2; }
	inline int32_t* get_address_of__currentIndex_2() { return &____currentIndex_2; }
	inline void set__currentIndex_2(int32_t value)
	{
		____currentIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPATH_T3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7_H
#ifndef PATHFILTER_TF860A9811AE814CCD54914A44C2A08AA07FECA80_H
#define PATHFILTER_TF860A9811AE814CCD54914A44C2A08AA07FECA80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.PathFilter
struct  PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHFILTER_TF860A9811AE814CCD54914A44C2A08AA07FECA80_H
#ifndef U3CEXECUTEFILTERU3ED__4_T1092E9A17F5C737B43A85A54B82042D32C7E0683_H
#define U3CEXECUTEFILTERU3ED__4_T1092E9A17F5C737B43A85A54B82042D32C7E0683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.QueryFilter Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>4__this
	QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF * ___U3CU3E4__this_5;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_6;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter_<ExecuteFilter>d__4::<>7__wrap2
	RuntimeObject* ___U3CU3E7__wrap2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E4__this_5)); }
	inline QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E7__wrap1_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683, ___U3CU3E7__wrap2_7)); }
	inline RuntimeObject* get_U3CU3E7__wrap2_7() const { return ___U3CU3E7__wrap2_7; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap2_7() { return &___U3CU3E7__wrap2_7; }
	inline void set_U3CU3E7__wrap2_7(RuntimeObject* value)
	{
		___U3CU3E7__wrap2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T1092E9A17F5C737B43A85A54B82042D32C7E0683_H
#ifndef U3CEXECUTEFILTERU3ED__4_T69C2964074761EFD7077E24C57036C7DC5E33510_H
#define U3CEXECUTEFILTERU3ED__4_T69C2964074761EFD7077E24C57036C7DC5E33510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.ScanFilter Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>4__this
	ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<root>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CrootU3E5__1_6;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<value>5__2
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CvalueU3E5__2_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E4__this_5)); }
	inline ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CrootU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CrootU3E5__1_6() const { return ___U3CrootU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CrootU3E5__1_6() { return &___U3CrootU3E5__1_6; }
	inline void set_U3CrootU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CrootU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CvalueU3E5__2_7)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CvalueU3E5__2_7() const { return ___U3CvalueU3E5__2_7; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CvalueU3E5__2_7() { return &___U3CvalueU3E5__2_7; }
	inline void set_U3CvalueU3E5__2_7(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CvalueU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510, ___U3CU3E7__wrap1_8)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_8() const { return ___U3CU3E7__wrap1_8; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_8() { return &___U3CU3E7__wrap1_8; }
	inline void set_U3CU3E7__wrap1_8(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T69C2964074761EFD7077E24C57036C7DC5E33510_H
#ifndef JSONSERIALIZERINTERNALBASE_TFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C_H
#define JSONSERIALIZERINTERNALBASE_TFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * ____currentErrorContext_0;
	// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC * ____mappings_1;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * ___Serializer_2;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ____currentErrorContext_0)); }
	inline ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t6A9F04D13CC2BC4A8C660E4EE31E3E15842615E3 * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ____mappings_1)); }
	inline BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_tC3E8B842AE8E453FFE00B2EF657AE37125AE16DC * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ___Serializer_2)); }
	inline JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2 * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_TFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C_H
#ifndef U3CU3EC_T6B7E5372E513FB62AF3A99179A0B07B4335668C5_H
#define U3CU3EC_T6B7E5372E513FB62AF3A99179A0B07B4335668C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c
struct  U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9
	U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5 * ___U3CU3E9_0;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__36_0
	Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 * ___U3CU3E9__36_0_1;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__36_2
	Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 * ___U3CU3E9__36_2_2;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__41_0
	Func_2_t3D1D7D4034F6D7BB9962E4CB6723F27F67E28C26 * ___U3CU3E9__41_0_3;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__41_1
	Func_2_t103CCE97502A49E59199C1C02AF4B40DA45FE783 * ___U3CU3E9__41_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields, ___U3CU3E9__36_0_1)); }
	inline Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 * get_U3CU3E9__36_0_1() const { return ___U3CU3E9__36_0_1; }
	inline Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 ** get_address_of_U3CU3E9__36_0_1() { return &___U3CU3E9__36_0_1; }
	inline void set_U3CU3E9__36_0_1(Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 * value)
	{
		___U3CU3E9__36_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields, ___U3CU3E9__36_2_2)); }
	inline Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 * get_U3CU3E9__36_2_2() const { return ___U3CU3E9__36_2_2; }
	inline Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 ** get_address_of_U3CU3E9__36_2_2() { return &___U3CU3E9__36_2_2; }
	inline void set_U3CU3E9__36_2_2(Func_2_tE62144C0B5815E8169B8090701233215B93D50B7 * value)
	{
		___U3CU3E9__36_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields, ___U3CU3E9__41_0_3)); }
	inline Func_2_t3D1D7D4034F6D7BB9962E4CB6723F27F67E28C26 * get_U3CU3E9__41_0_3() const { return ___U3CU3E9__41_0_3; }
	inline Func_2_t3D1D7D4034F6D7BB9962E4CB6723F27F67E28C26 ** get_address_of_U3CU3E9__41_0_3() { return &___U3CU3E9__41_0_3; }
	inline void set_U3CU3E9__41_0_3(Func_2_t3D1D7D4034F6D7BB9962E4CB6723F27F67E28C26 * value)
	{
		___U3CU3E9__41_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields, ___U3CU3E9__41_1_4)); }
	inline Func_2_t103CCE97502A49E59199C1C02AF4B40DA45FE783 * get_U3CU3E9__41_1_4() const { return ___U3CU3E9__41_1_4; }
	inline Func_2_t103CCE97502A49E59199C1C02AF4B40DA45FE783 ** get_address_of_U3CU3E9__41_1_4() { return &___U3CU3E9__41_1_4; }
	inline void set_U3CU3E9__41_1_4(Func_2_t103CCE97502A49E59199C1C02AF4B40DA45FE783 * value)
	{
		___U3CU3E9__41_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6B7E5372E513FB62AF3A99179A0B07B4335668C5_H
#ifndef U3CU3EC_T011636F89FC21CC23D4D3CD646E81A316766A9A4_H
#define U3CU3EC_T011636F89FC21CC23D4D3CD646E81A316766A9A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector_<>c
struct  U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonTypeReflector_<>c Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::<>9
	U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::<>9__18_1
	Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * ___U3CU3E9__18_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4_StaticFields, ___U3CU3E9__18_1_1)); }
	inline Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * get_U3CU3E9__18_1_1() const { return ___U3CU3E9__18_1_1; }
	inline Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E ** get_address_of_U3CU3E9__18_1_1() { return &___U3CU3E9__18_1_1; }
	inline void set_U3CU3E9__18_1_1(Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * value)
	{
		___U3CU3E9__18_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T011636F89FC21CC23D4D3CD646E81A316766A9A4_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T23EF8A18E83AE533FE61177F9AE74EE045D2B5DB_H
#define U3CU3EC__DISPLAYCLASS18_0_T23EF8A18E83AE533FE61177F9AE74EE045D2B5DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t23EF8A18E83AE533FE61177F9AE74EE045D2B5DB  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass18_0::converterType
	Type_t * ___converterType_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass18_0::defaultConstructor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___defaultConstructor_1;

public:
	inline static int32_t get_offset_of_converterType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t23EF8A18E83AE533FE61177F9AE74EE045D2B5DB, ___converterType_0)); }
	inline Type_t * get_converterType_0() const { return ___converterType_0; }
	inline Type_t ** get_address_of_converterType_0() { return &___converterType_0; }
	inline void set_converterType_0(Type_t * value)
	{
		___converterType_0 = value;
		Il2CppCodeGenWriteBarrier((&___converterType_0), value);
	}

	inline static int32_t get_offset_of_defaultConstructor_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t23EF8A18E83AE533FE61177F9AE74EE045D2B5DB, ___defaultConstructor_1)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_defaultConstructor_1() const { return ___defaultConstructor_1; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_defaultConstructor_1() { return &___defaultConstructor_1; }
	inline void set_defaultConstructor_1(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___defaultConstructor_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultConstructor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T23EF8A18E83AE533FE61177F9AE74EE045D2B5DB_H
#ifndef REFLECTIONATTRIBUTEPROVIDER_T3AC467A13E733204C9F67A4944CB06B4855D8D55_H
#define REFLECTIONATTRIBUTEPROVIDER_T3AC467A13E733204C9F67A4944CB06B4855D8D55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionAttributeProvider
struct  ReflectionAttributeProvider_t3AC467A13E733204C9F67A4944CB06B4855D8D55  : public RuntimeObject
{
public:
	// System.Object Newtonsoft.Json.Serialization.ReflectionAttributeProvider::_attributeProvider
	RuntimeObject * ____attributeProvider_0;

public:
	inline static int32_t get_offset_of__attributeProvider_0() { return static_cast<int32_t>(offsetof(ReflectionAttributeProvider_t3AC467A13E733204C9F67A4944CB06B4855D8D55, ____attributeProvider_0)); }
	inline RuntimeObject * get__attributeProvider_0() const { return ____attributeProvider_0; }
	inline RuntimeObject ** get_address_of__attributeProvider_0() { return &____attributeProvider_0; }
	inline void set__attributeProvider_0(RuntimeObject * value)
	{
		____attributeProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____attributeProvider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONATTRIBUTEPROVIDER_T3AC467A13E733204C9F67A4944CB06B4855D8D55_H
#ifndef REFLECTIONVALUEPROVIDER_T7A572BCA761C655FA9CB80438DD694A4B7549E52_H
#define REFLECTIONVALUEPROVIDER_T7A572BCA761C655FA9CB80438DD694A4B7549E52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionValueProvider
struct  ReflectionValueProvider_t7A572BCA761C655FA9CB80438DD694A4B7549E52  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.ReflectionValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(ReflectionValueProvider_t7A572BCA761C655FA9CB80438DD694A4B7549E52, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONVALUEPROVIDER_T7A572BCA761C655FA9CB80438DD694A4B7549E52_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef COLLECTION_1_TAD079810F33B890096F68D9C86BC53652D53E465_H
#define COLLECTION_1_TAD079810F33B890096F68D9C86BC53652D53E465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JToken>
struct  Collection_1_tAD079810F33B890096F68D9C86BC53652D53E465  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_tAD079810F33B890096F68D9C86BC53652D53E465, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_tAD079810F33B890096F68D9C86BC53652D53E465, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_TAD079810F33B890096F68D9C86BC53652D53E465_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef FASTACTION_T270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB_H
#define FASTACTION_T270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FastAction
struct  FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<System.Action> TMPro.FastAction::delegates
	LinkedList_1_t0C09473089448E5DF6F815467B91B1D104D38C76 * ___delegates_0;
	// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>> TMPro.FastAction::lookup
	Dictionary_2_t1C588111C95B675E14518EE35497D6BB383A7979 * ___lookup_1;

public:
	inline static int32_t get_offset_of_delegates_0() { return static_cast<int32_t>(offsetof(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB, ___delegates_0)); }
	inline LinkedList_1_t0C09473089448E5DF6F815467B91B1D104D38C76 * get_delegates_0() const { return ___delegates_0; }
	inline LinkedList_1_t0C09473089448E5DF6F815467B91B1D104D38C76 ** get_address_of_delegates_0() { return &___delegates_0; }
	inline void set_delegates_0(LinkedList_1_t0C09473089448E5DF6F815467B91B1D104D38C76 * value)
	{
		___delegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_0), value);
	}

	inline static int32_t get_offset_of_lookup_1() { return static_cast<int32_t>(offsetof(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB, ___lookup_1)); }
	inline Dictionary_2_t1C588111C95B675E14518EE35497D6BB383A7979 * get_lookup_1() const { return ___lookup_1; }
	inline Dictionary_2_t1C588111C95B675E14518EE35497D6BB383A7979 ** get_address_of_lookup_1() { return &___lookup_1; }
	inline void set_lookup_1(Dictionary_2_t1C588111C95B675E14518EE35497D6BB383A7979 * value)
	{
		___lookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTACTION_T270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB_H
#ifndef MATERIALREFERENCEMANAGER_TDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_H
#define MATERIALREFERENCEMANAGER_TDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReferenceManager
struct  MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.MaterialReferenceManager::m_FontMaterialReferenceLookup
	Dictionary_2_t894EF327CB2DFFC12CADBC804C7907CDA55E898C * ___m_FontMaterialReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset> TMPro.MaterialReferenceManager::m_FontAssetReferenceLookup
	Dictionary_2_t5027D1A6C399063D7319CDF3347CBDF7F5E660CA * ___m_FontAssetReferenceLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset> TMPro.MaterialReferenceManager::m_SpriteAssetReferenceLookup
	Dictionary_2_t4EB591B9FEEC3EFCF9551F1834394DC59DFECDEC * ___m_SpriteAssetReferenceLookup_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_ColorGradient> TMPro.MaterialReferenceManager::m_ColorGradientReferenceLookup
	Dictionary_2_t3D8062B54231EF452E0BDF087D00AE161A359E80 * ___m_ColorGradientReferenceLookup_4;

public:
	inline static int32_t get_offset_of_m_FontMaterialReferenceLookup_1() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_FontMaterialReferenceLookup_1)); }
	inline Dictionary_2_t894EF327CB2DFFC12CADBC804C7907CDA55E898C * get_m_FontMaterialReferenceLookup_1() const { return ___m_FontMaterialReferenceLookup_1; }
	inline Dictionary_2_t894EF327CB2DFFC12CADBC804C7907CDA55E898C ** get_address_of_m_FontMaterialReferenceLookup_1() { return &___m_FontMaterialReferenceLookup_1; }
	inline void set_m_FontMaterialReferenceLookup_1(Dictionary_2_t894EF327CB2DFFC12CADBC804C7907CDA55E898C * value)
	{
		___m_FontMaterialReferenceLookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontMaterialReferenceLookup_1), value);
	}

	inline static int32_t get_offset_of_m_FontAssetReferenceLookup_2() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_FontAssetReferenceLookup_2)); }
	inline Dictionary_2_t5027D1A6C399063D7319CDF3347CBDF7F5E660CA * get_m_FontAssetReferenceLookup_2() const { return ___m_FontAssetReferenceLookup_2; }
	inline Dictionary_2_t5027D1A6C399063D7319CDF3347CBDF7F5E660CA ** get_address_of_m_FontAssetReferenceLookup_2() { return &___m_FontAssetReferenceLookup_2; }
	inline void set_m_FontAssetReferenceLookup_2(Dictionary_2_t5027D1A6C399063D7319CDF3347CBDF7F5E660CA * value)
	{
		___m_FontAssetReferenceLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontAssetReferenceLookup_2), value);
	}

	inline static int32_t get_offset_of_m_SpriteAssetReferenceLookup_3() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_SpriteAssetReferenceLookup_3)); }
	inline Dictionary_2_t4EB591B9FEEC3EFCF9551F1834394DC59DFECDEC * get_m_SpriteAssetReferenceLookup_3() const { return ___m_SpriteAssetReferenceLookup_3; }
	inline Dictionary_2_t4EB591B9FEEC3EFCF9551F1834394DC59DFECDEC ** get_address_of_m_SpriteAssetReferenceLookup_3() { return &___m_SpriteAssetReferenceLookup_3; }
	inline void set_m_SpriteAssetReferenceLookup_3(Dictionary_2_t4EB591B9FEEC3EFCF9551F1834394DC59DFECDEC * value)
	{
		___m_SpriteAssetReferenceLookup_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteAssetReferenceLookup_3), value);
	}

	inline static int32_t get_offset_of_m_ColorGradientReferenceLookup_4() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC, ___m_ColorGradientReferenceLookup_4)); }
	inline Dictionary_2_t3D8062B54231EF452E0BDF087D00AE161A359E80 * get_m_ColorGradientReferenceLookup_4() const { return ___m_ColorGradientReferenceLookup_4; }
	inline Dictionary_2_t3D8062B54231EF452E0BDF087D00AE161A359E80 ** get_address_of_m_ColorGradientReferenceLookup_4() { return &___m_ColorGradientReferenceLookup_4; }
	inline void set_m_ColorGradientReferenceLookup_4(Dictionary_2_t3D8062B54231EF452E0BDF087D00AE161A359E80 * value)
	{
		___m_ColorGradientReferenceLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorGradientReferenceLookup_4), value);
	}
};

struct MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields
{
public:
	// TMPro.MaterialReferenceManager TMPro.MaterialReferenceManager::s_Instance
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields, ___s_Instance_0)); }
	inline MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC * get_s_Instance_0() const { return ___s_Instance_0; }
	inline MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALREFERENCEMANAGER_TDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_H
#ifndef TMP_COMPATIBILITY_T44C9537249F15C4E525D25FFAF101997F82887E8_H
#define TMP_COMPATIBILITY_T44C9537249F15C4E525D25FFAF101997F82887E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Compatibility
struct  TMP_Compatibility_t44C9537249F15C4E525D25FFAF101997F82887E8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COMPATIBILITY_T44C9537249F15C4E525D25FFAF101997F82887E8_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T5D70094105EE31D680149823953357EE017EEE87_H
#define __STATICARRAYINITTYPESIZEU3D10_T5D70094105EE31D680149823953357EE017EEE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10
struct  __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T5D70094105EE31D680149823953357EE017EEE87_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_TD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637_H
#define __STATICARRAYINITTYPESIZEU3D12_TD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_TD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T5F1B9C4994D974B6D60CDFBEA3BB033669C9D472_H
#define __STATICARRAYINITTYPESIZEU3D28_T5F1B9C4994D974B6D60CDFBEA3BB033669C9D472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28
struct  __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T5F1B9C4994D974B6D60CDFBEA3BB033669C9D472_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T8E069480386AD2369CDA8BC3B64741395333F064_H
#define __STATICARRAYINITTYPESIZEU3D52_T8E069480386AD2369CDA8BC3B64741395333F064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52
struct  __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T8E069480386AD2369CDA8BC3B64741395333F064_H
#ifndef __STATICARRAYINITTYPESIZEU3D6_T68DC18882637DDE0E36BBD1FB7ABFE61687A6F98_H
#define __STATICARRAYINITTYPESIZEU3D6_T68DC18882637DDE0E36BBD1FB7ABFE61687A6F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6
struct  __StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98__padding[6];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D6_T68DC18882637DDE0E36BBD1FB7ABFE61687A6F98_H
#ifndef BSONARRAY_TFFCE8804FBA87DECA9A84178C668305D8E89B6A7_H
#define BSONARRAY_TFFCE8804FBA87DECA9A84178C668305D8E89B6A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonArray
struct  BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonArray::_children
	List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7, ____children_2)); }
	inline List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 * get__children_2() const { return ____children_2; }
	inline List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t68BC38D388C4DC6640F9F3B1E87BDAE4EFA5E6E3 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONARRAY_TFFCE8804FBA87DECA9A84178C668305D8E89B6A7_H
#ifndef BSONOBJECT_TFB8AF55378FE99FCE476FF65B1667A0D45F93518_H
#define BSONOBJECT_TFB8AF55378FE99FCE476FF65B1667A0D45F93518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObject
struct  BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::_children
	List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518, ____children_2)); }
	inline List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 * get__children_2() const { return ____children_2; }
	inline List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t2496F27A4736C5D9542D33FE9FF2F0A3AF8F2566 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECT_TFB8AF55378FE99FCE476FF65B1667A0D45F93518_H
#ifndef BSONREGEX_T5C64E5D5DABD795CAC97069165054859A0B938D3_H
#define BSONREGEX_T5C64E5D5DABD795CAC97069165054859A0B938D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonRegex
struct  BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Pattern>k__BackingField
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * ___U3CPatternU3Ek__BackingField_2;
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Options>k__BackingField
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3, ___U3CPatternU3Ek__BackingField_2)); }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A ** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3, ___U3COptionsU3Ek__BackingField_3)); }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREGEX_T5C64E5D5DABD795CAC97069165054859A0B938D3_H
#ifndef BINARYCONVERTER_T9DA25409355A29DFE356701CAD581E231D13DEF6_H
#define BINARYCONVERTER_T9DA25409355A29DFE356701CAD581E231D13DEF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BinaryConverter
struct  BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.BinaryConverter::_reflectionObject
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6, ____reflectionObject_0)); }
	inline ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T9DA25409355A29DFE356701CAD581E231D13DEF6_H
#ifndef BSONOBJECTIDCONVERTER_T9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812_H
#define BSONOBJECTIDCONVERTER_T9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BsonObjectIdConverter
struct  BsonObjectIdConverter_t9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTIDCONVERTER_T9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812_H
#ifndef KEYVALUEPAIRCONVERTER_TD36E6790020AC069084D9D08BA6159A38E5E5557_H
#define KEYVALUEPAIRCONVERTER_TD36E6790020AC069084D9D08BA6159A38E5E5557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.KeyValuePairConverter
struct  KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

struct KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject> Newtonsoft.Json.Converters.KeyValuePairConverter::ReflectionObjectPerType
	ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E * ___ReflectionObjectPerType_0;

public:
	inline static int32_t get_offset_of_ReflectionObjectPerType_0() { return static_cast<int32_t>(offsetof(KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields, ___ReflectionObjectPerType_0)); }
	inline ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E * get_ReflectionObjectPerType_0() const { return ___ReflectionObjectPerType_0; }
	inline ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E ** get_address_of_ReflectionObjectPerType_0() { return &___ReflectionObjectPerType_0; }
	inline void set_ReflectionObjectPerType_0(ThreadSafeStore_2_t383F3E9472520F192BABC81C35BAC871CE4CC93E * value)
	{
		___ReflectionObjectPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionObjectPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRCONVERTER_TD36E6790020AC069084D9D08BA6159A38E5E5557_H
#ifndef REGEXCONVERTER_T043C392623207631E6876907924EEA55F1AC05EE_H
#define REGEXCONVERTER_T043C392623207631E6876907924EEA55F1AC05EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.RegexConverter
struct  RegexConverter_t043C392623207631E6876907924EEA55F1AC05EE  : public JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCONVERTER_T043C392623207631E6876907924EEA55F1AC05EE_H
#ifndef JCONTAINER_TF4CD2E574503C709DEF18A04B79B264B83746DAB_H
#define JCONTAINER_TF4CD2E574503C709DEF18A04B79B264B83746DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer
struct  JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB  : public JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02
{
public:
	// System.Object Newtonsoft.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_13;
	// System.Boolean Newtonsoft.Json.Linq.JContainer::_busy
	bool ____busy_14;

public:
	inline static int32_t get_offset_of__syncRoot_13() { return static_cast<int32_t>(offsetof(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB, ____syncRoot_13)); }
	inline RuntimeObject * get__syncRoot_13() const { return ____syncRoot_13; }
	inline RuntimeObject ** get_address_of__syncRoot_13() { return &____syncRoot_13; }
	inline void set__syncRoot_13(RuntimeObject * value)
	{
		____syncRoot_13 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_13), value);
	}

	inline static int32_t get_offset_of__busy_14() { return static_cast<int32_t>(offsetof(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB, ____busy_14)); }
	inline bool get__busy_14() const { return ____busy_14; }
	inline bool* get_address_of__busy_14() { return &____busy_14; }
	inline void set__busy_14(bool value)
	{
		____busy_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONTAINER_TF4CD2E574503C709DEF18A04B79B264B83746DAB_H
#ifndef JPROPERTYKEYEDCOLLECTION_T013DFBF6A88616F3C011C00B60291F2EAB75483D_H
#define JPROPERTYKEYEDCOLLECTION_T013DFBF6A88616F3C011C00B60291F2EAB75483D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct  JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D  : public Collection_1_tAD079810F33B890096F68D9C86BC53652D53E465
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JPropertyKeyedCollection::_dictionary
	Dictionary_2_t36CA3484108026EE9A1EC21D3D860B8C8F80611B * ____dictionary_3;

public:
	inline static int32_t get_offset_of__dictionary_3() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D, ____dictionary_3)); }
	inline Dictionary_2_t36CA3484108026EE9A1EC21D3D860B8C8F80611B * get__dictionary_3() const { return ____dictionary_3; }
	inline Dictionary_2_t36CA3484108026EE9A1EC21D3D860B8C8F80611B ** get_address_of__dictionary_3() { return &____dictionary_3; }
	inline void set__dictionary_3(Dictionary_2_t36CA3484108026EE9A1EC21D3D860B8C8F80611B * value)
	{
		____dictionary_3 = value;
		Il2CppCodeGenWriteBarrier((&____dictionary_3), value);
	}
};

struct JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.String> Newtonsoft.Json.Linq.JPropertyKeyedCollection::Comparer
	RuntimeObject* ___Comparer_2;

public:
	inline static int32_t get_offset_of_Comparer_2() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D_StaticFields, ___Comparer_2)); }
	inline RuntimeObject* get_Comparer_2() const { return ___Comparer_2; }
	inline RuntimeObject** get_address_of_Comparer_2() { return &___Comparer_2; }
	inline void set_Comparer_2(RuntimeObject* value)
	{
		___Comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Comparer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYKEYEDCOLLECTION_T013DFBF6A88616F3C011C00B60291F2EAB75483D_H
#ifndef ARRAYMULTIPLEINDEXFILTER_TF9FF764DDCB79A819281D799112F6E930B75E99F_H
#define ARRAYMULTIPLEINDEXFILTER_TF9FF764DDCB79A819281D799112F6E930B75E99F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter
struct  ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Collections.Generic.List`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter::<Indexes>k__BackingField
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___U3CIndexesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndexesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F, ___U3CIndexesU3Ek__BackingField_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_U3CIndexesU3Ek__BackingField_0() const { return ___U3CIndexesU3Ek__BackingField_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_U3CIndexesU3Ek__BackingField_0() { return &___U3CIndexesU3Ek__BackingField_0; }
	inline void set_U3CIndexesU3Ek__BackingField_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___U3CIndexesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYMULTIPLEINDEXFILTER_TF9FF764DDCB79A819281D799112F6E930B75E99F_H
#ifndef FIELDFILTER_T2B4992D5A1D10B7A905F49C62609097AED91A13B_H
#define FIELDFILTER_T2B4992D5A1D10B7A905F49C62609097AED91A13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldFilter
struct  FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.FieldFilter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDFILTER_T2B4992D5A1D10B7A905F49C62609097AED91A13B_H
#ifndef FIELDMULTIPLEFILTER_TE61362AE319B8C510FD11900AD0A0B79EC08CD6B_H
#define FIELDMULTIPLEFILTER_TE61362AE319B8C510FD11900AD0A0B79EC08CD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter
struct  FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Collections.Generic.List`1<System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter::<Names>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CNamesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNamesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B, ___U3CNamesU3Ek__BackingField_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CNamesU3Ek__BackingField_0() const { return ___U3CNamesU3Ek__BackingField_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CNamesU3Ek__BackingField_0() { return &___U3CNamesU3Ek__BackingField_0; }
	inline void set_U3CNamesU3Ek__BackingField_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CNamesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDMULTIPLEFILTER_TE61362AE319B8C510FD11900AD0A0B79EC08CD6B_H
#ifndef QUERYFILTER_T6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF_H
#define QUERYFILTER_T6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryFilter
struct  QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// Newtonsoft.Json.Linq.JsonPath.QueryExpression Newtonsoft.Json.Linq.JsonPath.QueryFilter::<Expression>k__BackingField
	QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F * ___U3CExpressionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExpressionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF, ___U3CExpressionU3Ek__BackingField_0)); }
	inline QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F * get_U3CExpressionU3Ek__BackingField_0() const { return ___U3CExpressionU3Ek__BackingField_0; }
	inline QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F ** get_address_of_U3CExpressionU3Ek__BackingField_0() { return &___U3CExpressionU3Ek__BackingField_0; }
	inline void set_U3CExpressionU3Ek__BackingField_0(QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F * value)
	{
		___U3CExpressionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYFILTER_T6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF_H
#ifndef SCANFILTER_T1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D_H
#define SCANFILTER_T1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ScanFilter
struct  ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.ScanFilter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANFILTER_T1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D_H
#ifndef JSONSERIALIZERINTERNALWRITER_T9C7372EFA368E434DC3880D355199C7FA529129A_H
#define JSONSERIALIZERINTERNALWRITER_T9C7372EFA368E434DC3880D355199C7FA529129A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct  JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A  : public JsonSerializerInternalBase_tFB8CD442034B1296BD68BB8CE5F2AAC246CAC78C
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootType
	Type_t * ____rootType_5;
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootLevel
	int32_t ____rootLevel_6;
	// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_serializeStack
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____serializeStack_7;

public:
	inline static int32_t get_offset_of__rootType_5() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A, ____rootType_5)); }
	inline Type_t * get__rootType_5() const { return ____rootType_5; }
	inline Type_t ** get_address_of__rootType_5() { return &____rootType_5; }
	inline void set__rootType_5(Type_t * value)
	{
		____rootType_5 = value;
		Il2CppCodeGenWriteBarrier((&____rootType_5), value);
	}

	inline static int32_t get_offset_of__rootLevel_6() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A, ____rootLevel_6)); }
	inline int32_t get__rootLevel_6() const { return ____rootLevel_6; }
	inline int32_t* get_address_of__rootLevel_6() { return &____rootLevel_6; }
	inline void set__rootLevel_6(int32_t value)
	{
		____rootLevel_6 = value;
	}

	inline static int32_t get_offset_of__serializeStack_7() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A, ____serializeStack_7)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__serializeStack_7() const { return ____serializeStack_7; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__serializeStack_7() { return &____serializeStack_7; }
	inline void set__serializeStack_7(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____serializeStack_7 = value;
		Il2CppCodeGenWriteBarrier((&____serializeStack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALWRITER_T9C7372EFA368E434DC3880D355199C7FA529129A_H
#ifndef ONERRORATTRIBUTE_TFEC5B0C0CC1339ADD7E096DCE8E204AE2C21541C_H
#define ONERRORATTRIBUTE_TFEC5B0C0CC1339ADD7E096DCE8E204AE2C21541C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.OnErrorAttribute
struct  OnErrorAttribute_tFEC5B0C0CC1339ADD7E096DCE8E204AE2C21541C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORATTRIBUTE_TFEC5B0C0CC1339ADD7E096DCE8E204AE2C21541C_H
#ifndef KEYVALUEPAIR_2_TFAF037D8F6143C33EA6D34A46D518C184C5FAC3C_H
#define KEYVALUEPAIR_2_TFAF037D8F6143C33EA6D34A46D518C184C5FAC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>
struct  KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C, ___value_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_value_1() const { return ___value_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_TFAF037D8F6143C33EA6D34A46D518C184C5FAC3C_H
#ifndef ENUMERATOR_T1A13F370EC7EA46EA20204D8881CCE685A3C348C_H
#define ENUMERATOR_T1A13F370EC7EA46EA20204D8881CCE685A3C348C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.Int32>
struct  Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___list_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_list_0() const { return ___list_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1A13F370EC7EA46EA20204D8881CCE685A3C348C_H
#ifndef ENUMERATOR_T2EF2387642F9C5048CDE9DCC21C37470592FFC0D_H
#define ENUMERATOR_T2EF2387642F9C5048CDE9DCC21C37470592FFC0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.String>
struct  Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2EF2387642F9C5048CDE9DCC21C37470592FFC0D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#define TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#ifndef TMP_XMLTAGSTACK_1_T2E9DCE707626EAF04E59BA503BA8FF207C8E5605_H
#define TMP_XMLTAGSTACK_1_T2E9DCE707626EAF04E59BA503BA8FF207C8E5605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___itemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2E9DCE707626EAF04E59BA503BA8FF207C8E5605_H
#ifndef TMP_XMLTAGSTACK_1_T6154B3FE95201F122D06B29F910282D903233B3E_H
#define TMP_XMLTAGSTACK_1_T6154B3FE95201F122D06B29F910282D903233B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___itemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T6154B3FE95201F122D06B29F910282D903233B3E_H
#ifndef TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#define TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D6 <PrivateImplementationDetails>::3DE43C11C7130AF9014115BCDC2584DFE6B50579
	__StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98  ___3DE43C11C7130AF9014115BCDC2584DFE6B50579_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::ADFD2E1C801C825415DD53F4F2F72A13B389313C
	__StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637  ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10 <PrivateImplementationDetails>::D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB
	__StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87  ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52 <PrivateImplementationDetails>::DD3AEFEADB1CD615F3017763F1568179FEE640B0
	__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_4;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52 <PrivateImplementationDetails>::E92B39D8233061927D9ACDE54665E68E7535635A
	__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  ___E92B39D8233061927D9ACDE54665E68E7535635A_5;

public:
	inline static int32_t get_offset_of_U33DE43C11C7130AF9014115BCDC2584DFE6B50579_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___3DE43C11C7130AF9014115BCDC2584DFE6B50579_0)); }
	inline __StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98  get_U33DE43C11C7130AF9014115BCDC2584DFE6B50579_0() const { return ___3DE43C11C7130AF9014115BCDC2584DFE6B50579_0; }
	inline __StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98 * get_address_of_U33DE43C11C7130AF9014115BCDC2584DFE6B50579_0() { return &___3DE43C11C7130AF9014115BCDC2584DFE6B50579_0; }
	inline void set_U33DE43C11C7130AF9014115BCDC2584DFE6B50579_0(__StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98  value)
	{
		___3DE43C11C7130AF9014115BCDC2584DFE6B50579_0 = value;
	}

	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_1)); }
	inline __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_1() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_1; }
	inline __StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_1() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_1; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_1(__StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_1 = value;
	}

	inline static int32_t get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_2)); }
	inline __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637  get_ADFD2E1C801C825415DD53F4F2F72A13B389313C_2() const { return ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_2; }
	inline __StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637 * get_address_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_2() { return &___ADFD2E1C801C825415DD53F4F2F72A13B389313C_2; }
	inline void set_ADFD2E1C801C825415DD53F4F2F72A13B389313C_2(__StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637  value)
	{
		___ADFD2E1C801C825415DD53F4F2F72A13B389313C_2 = value;
	}

	inline static int32_t get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3)); }
	inline __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87  get_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3() const { return ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3; }
	inline __StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87 * get_address_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3() { return &___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3; }
	inline void set_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3(__StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87  value)
	{
		___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3 = value;
	}

	inline static int32_t get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_4)); }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  get_DD3AEFEADB1CD615F3017763F1568179FEE640B0_4() const { return ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_4; }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 * get_address_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_4() { return &___DD3AEFEADB1CD615F3017763F1568179FEE640B0_4; }
	inline void set_DD3AEFEADB1CD615F3017763F1568179FEE640B0_4(__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  value)
	{
		___DD3AEFEADB1CD615F3017763F1568179FEE640B0_4 = value;
	}

	inline static int32_t get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields, ___E92B39D8233061927D9ACDE54665E68E7535635A_5)); }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  get_E92B39D8233061927D9ACDE54665E68E7535635A_5() const { return ___E92B39D8233061927D9ACDE54665E68E7535635A_5; }
	inline __StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 * get_address_of_E92B39D8233061927D9ACDE54665E68E7535635A_5() { return &___E92B39D8233061927D9ACDE54665E68E7535635A_5; }
	inline void set_E92B39D8233061927D9ACDE54665E68E7535635A_5(__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064  value)
	{
		___E92B39D8233061927D9ACDE54665E68E7535635A_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_H
#ifndef BSONBINARYTYPE_T82EF959604654BA62C48C95135EC2F8A896928E3_H
#define BSONBINARYTYPE_T82EF959604654BA62C48C95135EC2F8A896928E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryType
struct  BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3 
{
public:
	// System.Byte Newtonsoft.Json.Bson.BsonBinaryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYTYPE_T82EF959604654BA62C48C95135EC2F8A896928E3_H
#ifndef BSONTYPE_TBF3253E8D497F6E8F668EB77C7B6345854BE5538_H
#define BSONTYPE_TBF3253E8D497F6E8F668EB77C7B6345854BE5538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonType
struct  BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538 
{
public:
	// System.SByte Newtonsoft.Json.Bson.BsonType::value__
	int8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538, ___value___2)); }
	inline int8_t get_value___2() const { return ___value___2; }
	inline int8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTYPE_TBF3253E8D497F6E8F668EB77C7B6345854BE5538_H
#ifndef CONSTRUCTORHANDLING_TC9A0E967603307F5C573320F3D7CDDA57CFFEA88_H
#define CONSTRUCTORHANDLING_TC9A0E967603307F5C573320F3D7CDDA57CFFEA88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_tC9A0E967603307F5C573320F3D7CDDA57CFFEA88 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstructorHandling_tC9A0E967603307F5C573320F3D7CDDA57CFFEA88, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_TC9A0E967603307F5C573320F3D7CDDA57CFFEA88_H
#ifndef DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#define DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifndef DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#define DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateParseHandling_t92CAAF933C60203330E9D46525F2AB358233C385, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T92CAAF933C60203330E9D46525F2AB358233C385_H
#ifndef DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#define DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifndef DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#define DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_tD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_TD17BEA87172DFB1A12FF6C8C61AA4C3CEC807DC2_H
#ifndef FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#define FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifndef FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#define FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatParseHandling_t30D6C6D7BFF162AB10E59EE83DA342E206B6F205, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T30D6C6D7BFF162AB10E59EE83DA342E206B6F205_H
#ifndef FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#define FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifndef JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#define JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifndef STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#define STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader_State
struct  State_tF808A376CD3F052C3C1BE586C60C34C534ADB010 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tF808A376CD3F052C3C1BE586C60C34C534ADB010, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TF808A376CD3F052C3C1BE586C60C34C534ADB010_H
#ifndef JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#define JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_tF87828040BB18D17E612A0D4AA51B0995156561B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_TF87828040BB18D17E612A0D4AA51B0995156561B_H
#ifndef STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#define STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter_State
struct  State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifndef COMMENTHANDLING_T8A00D794DF4DCFECD5B91F4CF29B500159D703D9_H
#define COMMENTHANDLING_T8A00D794DF4DCFECD5B91F4CF29B500159D703D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.CommentHandling
struct  CommentHandling_t8A00D794DF4DCFECD5B91F4CF29B500159D703D9 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.CommentHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CommentHandling_t8A00D794DF4DCFECD5B91F4CF29B500159D703D9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENTHANDLING_T8A00D794DF4DCFECD5B91F4CF29B500159D703D9_H
#ifndef JARRAY_T1CE13821116F9B501573275C6BDD9FB254E65F11_H
#define JARRAY_T1CE13821116F9B501573275C6BDD9FB254E65F11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JArray
struct  JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::_values
	List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * ____values_15;

public:
	inline static int32_t get_offset_of__values_15() { return static_cast<int32_t>(offsetof(JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11, ____values_15)); }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * get__values_15() const { return ____values_15; }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C ** get_address_of__values_15() { return &____values_15; }
	inline void set__values_15(List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * value)
	{
		____values_15 = value;
		Il2CppCodeGenWriteBarrier((&____values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JARRAY_T1CE13821116F9B501573275C6BDD9FB254E65F11_H
#ifndef JCONSTRUCTOR_T9F741A9F4DC238BBAC6980237CBBEA9730BA9775_H
#define JCONSTRUCTOR_T9F741A9F4DC238BBAC6980237CBBEA9730BA9775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JConstructor
struct  JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// System.String Newtonsoft.Json.Linq.JConstructor::_name
	String_t* ____name_15;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::_values
	List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * ____values_16;

public:
	inline static int32_t get_offset_of__name_15() { return static_cast<int32_t>(offsetof(JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775, ____name_15)); }
	inline String_t* get__name_15() const { return ____name_15; }
	inline String_t** get_address_of__name_15() { return &____name_15; }
	inline void set__name_15(String_t* value)
	{
		____name_15 = value;
		Il2CppCodeGenWriteBarrier((&____name_15), value);
	}

	inline static int32_t get_offset_of__values_16() { return static_cast<int32_t>(offsetof(JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775, ____values_16)); }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * get__values_16() const { return ____values_16; }
	inline List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C ** get_address_of__values_16() { return &____values_16; }
	inline void set__values_16(List_1_t29FF67BC2CD2EEBC234D6E03E125122CC8730B5C * value)
	{
		____values_16 = value;
		Il2CppCodeGenWriteBarrier((&____values_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONSTRUCTOR_T9F741A9F4DC238BBAC6980237CBBEA9730BA9775_H
#ifndef JOBJECT_T786AF07B1009334856B0362BBC48EEF68C81C585_H
#define JOBJECT_T786AF07B1009334856B0362BBC48EEF68C81C585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject
struct  JObject_t786AF07B1009334856B0362BBC48EEF68C81C585  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// Newtonsoft.Json.Linq.JPropertyKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D * ____properties_15;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanged_16;

public:
	inline static int32_t get_offset_of__properties_15() { return static_cast<int32_t>(offsetof(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585, ____properties_15)); }
	inline JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D * get__properties_15() const { return ____properties_15; }
	inline JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D ** get_address_of__properties_15() { return &____properties_15; }
	inline void set__properties_15(JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D * value)
	{
		____properties_15 = value;
		Il2CppCodeGenWriteBarrier((&____properties_15), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_16() { return static_cast<int32_t>(offsetof(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585, ___PropertyChanged_16)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanged_16() const { return ___PropertyChanged_16; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanged_16() { return &___PropertyChanged_16; }
	inline void set_PropertyChanged_16(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanged_16 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBJECT_T786AF07B1009334856B0362BBC48EEF68C81C585_H
#ifndef U3CGETENUMERATORU3ED__54_TB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53_H
#define U3CGETENUMERATORU3ED__54_TB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__54
struct  U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__54::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__54::<>2__current
	KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C  ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__54::<>4__this
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CU3E4__this_2;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__54::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_tFAF037D8F6143C33EA6D34A46D518C184C5FAC3C  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53, ___U3CU3E4__this_2)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__54_TB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53_H
#ifndef JPROPERTY_T127765B5AB6D281C5B77FAF5A4F26BB33C89398A_H
#define JPROPERTY_T127765B5AB6D281C5B77FAF5A4F26BB33C89398A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty
struct  JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A  : public JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB
{
public:
	// Newtonsoft.Json.Linq.JProperty_JPropertyList Newtonsoft.Json.Linq.JProperty::_content
	JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * ____content_15;
	// System.String Newtonsoft.Json.Linq.JProperty::_name
	String_t* ____name_16;

public:
	inline static int32_t get_offset_of__content_15() { return static_cast<int32_t>(offsetof(JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A, ____content_15)); }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * get__content_15() const { return ____content_15; }
	inline JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F ** get_address_of__content_15() { return &____content_15; }
	inline void set__content_15(JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F * value)
	{
		____content_15 = value;
		Il2CppCodeGenWriteBarrier((&____content_15), value);
	}

	inline static int32_t get_offset_of__name_16() { return static_cast<int32_t>(offsetof(JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A, ____name_16)); }
	inline String_t* get__name_16() const { return ____name_16; }
	inline String_t** get_address_of__name_16() { return &____name_16; }
	inline void set__name_16(String_t* value)
	{
		____name_16 = value;
		Il2CppCodeGenWriteBarrier((&____name_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTY_T127765B5AB6D281C5B77FAF5A4F26BB33C89398A_H
#ifndef JTOKENTYPE_T6E74963952426FE99DA37BAEFD84A99710412C9E_H
#define JTOKENTYPE_T6E74963952426FE99DA37BAEFD84A99710412C9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenType
struct  JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JTokenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_T6E74963952426FE99DA37BAEFD84A99710412C9E_H
#ifndef ARRAYINDEXFILTER_T4B8AB90AF78B3976E09BEA34466153A1CDE2E12B_H
#define ARRAYINDEXFILTER_T4B8AB90AF78B3976E09BEA34466153A1CDE2E12B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter
struct  ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter::<Index>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CIndexU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B, ___U3CIndexU3Ek__BackingField_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYINDEXFILTER_T4B8AB90AF78B3976E09BEA34466153A1CDE2E12B_H
#ifndef U3CEXECUTEFILTERU3ED__4_TC82AE708D5CE2B8917E4AA90413E6CA8BA574131_H
#define U3CEXECUTEFILTERU3ED__4_TC82AE708D5CE2B8917E4AA90413E6CA8BA574131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>4__this
	ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<t>5__1
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__1_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_7;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_8;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_9;
	// System.Collections.Generic.List`1_Enumerator<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter_<ExecuteFilter>d__4::<>7__wrap2
	Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  ___U3CU3E7__wrap2_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E4__this_5)); }
	inline ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CtU3E5__1_6)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__1_6() const { return ___U3CtU3E5__1_6; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__1_6() { return &___U3CtU3E5__1_6; }
	inline void set_U3CtU3E5__1_6(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___errorWhenNoMatch_7)); }
	inline bool get_errorWhenNoMatch_7() const { return ___errorWhenNoMatch_7; }
	inline bool* get_address_of_errorWhenNoMatch_7() { return &___errorWhenNoMatch_7; }
	inline void set_errorWhenNoMatch_7(bool value)
	{
		___errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E3__errorWhenNoMatch_8)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_8() const { return ___U3CU3E3__errorWhenNoMatch_8; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_8() { return &___U3CU3E3__errorWhenNoMatch_8; }
	inline void set_U3CU3E3__errorWhenNoMatch_8(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E7__wrap1_9)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131, ___U3CU3E7__wrap2_10)); }
	inline Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  get_U3CU3E7__wrap2_10() const { return ___U3CU3E7__wrap2_10; }
	inline Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C * get_address_of_U3CU3E7__wrap2_10() { return &___U3CU3E7__wrap2_10; }
	inline void set_U3CU3E7__wrap2_10(Enumerator_t1A13F370EC7EA46EA20204D8881CCE685A3C348C  value)
	{
		___U3CU3E7__wrap2_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_TC82AE708D5CE2B8917E4AA90413E6CA8BA574131_H
#ifndef ARRAYSLICEFILTER_TC5035758E75DB3EF9EAA182D561A726F46397BE3_H
#define ARRAYSLICEFILTER_TC5035758E75DB3EF9EAA182D561A726F46397BE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter
struct  ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3  : public PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80
{
public:
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<Start>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CStartU3Ek__BackingField_0;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<End>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CEndU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<Step>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CStepU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3, ___U3CStartU3Ek__BackingField_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CStartU3Ek__BackingField_0() const { return ___U3CStartU3Ek__BackingField_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CStartU3Ek__BackingField_0() { return &___U3CStartU3Ek__BackingField_0; }
	inline void set_U3CStartU3Ek__BackingField_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CStartU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEndU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3, ___U3CEndU3Ek__BackingField_1)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CEndU3Ek__BackingField_1() const { return ___U3CEndU3Ek__BackingField_1; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CEndU3Ek__BackingField_1() { return &___U3CEndU3Ek__BackingField_1; }
	inline void set_U3CEndU3Ek__BackingField_1(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CEndU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStepU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3, ___U3CStepU3Ek__BackingField_2)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CStepU3Ek__BackingField_2() const { return ___U3CStepU3Ek__BackingField_2; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CStepU3Ek__BackingField_2() { return &___U3CStepU3Ek__BackingField_2; }
	inline void set_U3CStepU3Ek__BackingField_2(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CStepU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSLICEFILTER_TC5035758E75DB3EF9EAA182D561A726F46397BE3_H
#ifndef U3CEXECUTEFILTERU3ED__4_T0C1DEC1C597C26BA75BE873F98008D01240AE9BB_H
#define U3CEXECUTEFILTERU3ED__4_T0C1DEC1C597C26BA75BE873F98008D01240AE9BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>2__current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>4__this
	FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<o>5__1
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CoU3E5__1_6;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_7;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_8;
	// System.String Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<name>5__2
	String_t* ___U3CnameU3E5__2_9;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<t>5__3
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___U3CtU3E5__3_10;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_11;
	// System.Collections.Generic.List`1_Enumerator<System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter_<ExecuteFilter>d__4::<>7__wrap2
	Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D  ___U3CU3E7__wrap2_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E2__current_1)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E4__this_5)); }
	inline FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CoU3E5__1_6)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CoU3E5__1_6() const { return ___U3CoU3E5__1_6; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CoU3E5__1_6() { return &___U3CoU3E5__1_6; }
	inline void set_U3CoU3E5__1_6(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CoU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___errorWhenNoMatch_7)); }
	inline bool get_errorWhenNoMatch_7() const { return ___errorWhenNoMatch_7; }
	inline bool* get_address_of_errorWhenNoMatch_7() { return &___errorWhenNoMatch_7; }
	inline void set_errorWhenNoMatch_7(bool value)
	{
		___errorWhenNoMatch_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E3__errorWhenNoMatch_8)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_8() const { return ___U3CU3E3__errorWhenNoMatch_8; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_8() { return &___U3CU3E3__errorWhenNoMatch_8; }
	inline void set_U3CU3E3__errorWhenNoMatch_8(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_8 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CnameU3E5__2_9)); }
	inline String_t* get_U3CnameU3E5__2_9() const { return ___U3CnameU3E5__2_9; }
	inline String_t** get_address_of_U3CnameU3E5__2_9() { return &___U3CnameU3E5__2_9; }
	inline void set_U3CnameU3E5__2_9(String_t* value)
	{
		___U3CnameU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CtU3E5__3_10)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_U3CtU3E5__3_10() const { return ___U3CtU3E5__3_10; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_U3CtU3E5__3_10() { return &___U3CtU3E5__3_10; }
	inline void set_U3CtU3E5__3_10(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___U3CtU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E7__wrap1_11)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_11() const { return ___U3CU3E7__wrap1_11; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_11() { return &___U3CU3E7__wrap1_11; }
	inline void set_U3CU3E7__wrap1_11(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB, ___U3CU3E7__wrap2_12)); }
	inline Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D  get_U3CU3E7__wrap2_12() const { return ___U3CU3E7__wrap2_12; }
	inline Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D * get_address_of_U3CU3E7__wrap2_12() { return &___U3CU3E7__wrap2_12; }
	inline void set_U3CU3E7__wrap2_12(Enumerator_t2EF2387642F9C5048CDE9DCC21C37470592FFC0D  value)
	{
		___U3CU3E7__wrap2_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T0C1DEC1C597C26BA75BE873F98008D01240AE9BB_H
#ifndef QUERYOPERATOR_T91AB4C16055C42C3D23EBCE0B21564266F4B8130_H
#define QUERYOPERATOR_T91AB4C16055C42C3D23EBCE0B21564266F4B8130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryOperator
struct  QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYOPERATOR_T91AB4C16055C42C3D23EBCE0B21564266F4B8130_H
#ifndef LINEINFOHANDLING_TA0B927B127EBD4A855BD742AA5690775964A4B46_H
#define LINEINFOHANDLING_TA0B927B127EBD4A855BD742AA5690775964A4B46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.LineInfoHandling
struct  LineInfoHandling_tA0B927B127EBD4A855BD742AA5690775964A4B46 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.LineInfoHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineInfoHandling_tA0B927B127EBD4A855BD742AA5690775964A4B46, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOHANDLING_TA0B927B127EBD4A855BD742AA5690775964A4B46_H
#ifndef METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#define METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#ifndef MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#define MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#ifndef NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#define NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifndef OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#define OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifndef PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#define PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#ifndef READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#define READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_tB7E23FF443C2B2B019D4EE3B5CD8F78C55487874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_TB7E23FF443C2B2B019D4EE3B5CD8F78C55487874_H
#ifndef REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#define REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifndef JSONCONTRACTTYPE_T5DEDE285EB2CD0B99C86F0F53728B7433E66516F_H
#define JSONCONTRACTTYPE_T5DEDE285EB2CD0B99C86F0F53728B7433E66516F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContractType
struct  JsonContractType_t5DEDE285EB2CD0B99C86F0F53728B7433E66516F 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonContractType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContractType_t5DEDE285EB2CD0B99C86F0F53728B7433E66516F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T5DEDE285EB2CD0B99C86F0F53728B7433E66516F_H
#ifndef JSONTYPEREFLECTOR_T1AFF9A68583A2FDB0C90964398020E65ED986FC1_H
#define JSONTYPEREFLECTOR_T1AFF9A68583A2FDB0C90964398020E65ED986FC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector
struct  JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1  : public RuntimeObject
{
public:

public:
};

struct JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_fullyTrusted
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____fullyTrusted_0;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>> Newtonsoft.Json.Serialization.JsonTypeReflector::JsonConverterCreatorCache
	ThreadSafeStore_2_tC340361EB699198C223919D1635F850FEC1CBC31 * ___JsonConverterCreatorCache_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector::AssociatedMetadataTypesCache
	ThreadSafeStore_2_t93819CC1D004B6F647763930BB932DB7DDF99C11 * ___AssociatedMetadataTypesCache_2;
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Serialization.JsonTypeReflector::_metadataTypeAttributeReflectionObject
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * ____metadataTypeAttributeReflectionObject_3;

public:
	inline static int32_t get_offset_of__fullyTrusted_0() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields, ____fullyTrusted_0)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__fullyTrusted_0() const { return ____fullyTrusted_0; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__fullyTrusted_0() { return &____fullyTrusted_0; }
	inline void set__fullyTrusted_0(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____fullyTrusted_0 = value;
	}

	inline static int32_t get_offset_of_JsonConverterCreatorCache_1() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields, ___JsonConverterCreatorCache_1)); }
	inline ThreadSafeStore_2_tC340361EB699198C223919D1635F850FEC1CBC31 * get_JsonConverterCreatorCache_1() const { return ___JsonConverterCreatorCache_1; }
	inline ThreadSafeStore_2_tC340361EB699198C223919D1635F850FEC1CBC31 ** get_address_of_JsonConverterCreatorCache_1() { return &___JsonConverterCreatorCache_1; }
	inline void set_JsonConverterCreatorCache_1(ThreadSafeStore_2_tC340361EB699198C223919D1635F850FEC1CBC31 * value)
	{
		___JsonConverterCreatorCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___JsonConverterCreatorCache_1), value);
	}

	inline static int32_t get_offset_of_AssociatedMetadataTypesCache_2() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields, ___AssociatedMetadataTypesCache_2)); }
	inline ThreadSafeStore_2_t93819CC1D004B6F647763930BB932DB7DDF99C11 * get_AssociatedMetadataTypesCache_2() const { return ___AssociatedMetadataTypesCache_2; }
	inline ThreadSafeStore_2_t93819CC1D004B6F647763930BB932DB7DDF99C11 ** get_address_of_AssociatedMetadataTypesCache_2() { return &___AssociatedMetadataTypesCache_2; }
	inline void set_AssociatedMetadataTypesCache_2(ThreadSafeStore_2_t93819CC1D004B6F647763930BB932DB7DDF99C11 * value)
	{
		___AssociatedMetadataTypesCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___AssociatedMetadataTypesCache_2), value);
	}

	inline static int32_t get_offset_of__metadataTypeAttributeReflectionObject_3() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields, ____metadataTypeAttributeReflectionObject_3)); }
	inline ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * get__metadataTypeAttributeReflectionObject_3() const { return ____metadataTypeAttributeReflectionObject_3; }
	inline ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D ** get_address_of__metadataTypeAttributeReflectionObject_3() { return &____metadataTypeAttributeReflectionObject_3; }
	inline void set__metadataTypeAttributeReflectionObject_3(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D * value)
	{
		____metadataTypeAttributeReflectionObject_3 = value;
		Il2CppCodeGenWriteBarrier((&____metadataTypeAttributeReflectionObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPEREFLECTOR_T1AFF9A68583A2FDB0C90964398020E65ED986FC1_H
#ifndef STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#define STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifndef TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#define TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifndef PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#define PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#ifndef DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#define DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifndef FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#define FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#define COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorMode
struct  ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3 
{
public:
	// System.Int32 TMPro.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifndef COLORTWEENMODE_TD6E324EC6173EE67693D01411BF27B40D547F6A6_H
#define COLORTWEENMODE_TD6E324EC6173EE67693D01411BF27B40D547F6A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween_ColorTweenMode
struct  ColorTweenMode_tD6E324EC6173EE67693D01411BF27B40D547F6A6 
{
public:
	// System.Int32 TMPro.ColorTween_ColorTweenMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorTweenMode_tD6E324EC6173EE67693D01411BF27B40D547F6A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_TD6E324EC6173EE67693D01411BF27B40D547F6A6_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#define MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifndef ANCHORPOSITIONS_T3503D57135628F82EB0DC13CA870189F1FF61A45_H
#define ANCHORPOSITIONS_T3503D57135628F82EB0DC13CA870189F1FF61A45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Compatibility_AnchorPositions
struct  AnchorPositions_t3503D57135628F82EB0DC13CA870189F1FF61A45 
{
public:
	// System.Int32 TMPro.TMP_Compatibility_AnchorPositions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnchorPositions_t3503D57135628F82EB0DC13CA870189F1FF61A45, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORPOSITIONS_T3503D57135628F82EB0DC13CA870189F1FF61A45_H
#ifndef TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#define TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_TextInputSources
struct  TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4 
{
public:
	// System.Int32 TMPro.TMP_Text_TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#define TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___m_defaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#ifndef TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#define TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___itemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___m_defaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef TEXTCONTAINERANCHORS_T8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D_H
#define TEXTCONTAINERANCHORS_T8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainerAnchors
struct  TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D 
{
public:
	// System.Int32 TMPro.TextContainerAnchors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINERANCHORS_T8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D_H
#ifndef TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#define TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifndef TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#define TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifndef TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#define TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#define VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef BSONBINARYWRITER_T3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_H
#define BSONBINARYWRITER_T3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryWriter
struct  BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80  : public RuntimeObject
{
public:
	// System.IO.BinaryWriter Newtonsoft.Json.Bson.BsonBinaryWriter::_writer
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Bson.BsonBinaryWriter::_largeByteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____largeByteBuffer_2;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::<DateTimeKindHandling>k__BackingField
	int32_t ___U3CDateTimeKindHandlingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80, ____writer_1)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__largeByteBuffer_2() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80, ____largeByteBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__largeByteBuffer_2() const { return ____largeByteBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__largeByteBuffer_2() { return &____largeByteBuffer_2; }
	inline void set__largeByteBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____largeByteBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_2), value);
	}

	inline static int32_t get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80, ___U3CDateTimeKindHandlingU3Ek__BackingField_3)); }
	inline int32_t get_U3CDateTimeKindHandlingU3Ek__BackingField_3() const { return ___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return &___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline void set_U3CDateTimeKindHandlingU3Ek__BackingField_3(int32_t value)
	{
		___U3CDateTimeKindHandlingU3Ek__BackingField_3 = value;
	}
};

struct BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields
{
public:
	// System.Text.Encoding Newtonsoft.Json.Bson.BsonBinaryWriter::Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___Encoding_0;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields, ___Encoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___Encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYWRITER_T3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_H
#ifndef BSONVALUE_TB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0_H
#define BSONVALUE_TB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonValue
struct  BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0  : public BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3
{
public:
	// System.Object Newtonsoft.Json.Bson.BsonValue::_value
	RuntimeObject * ____value_2;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::_type
	int8_t ____type_3;

public:
	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0, ____type_3)); }
	inline int8_t get__type_3() const { return ____type_3; }
	inline int8_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int8_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONVALUE_TB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0_H
#ifndef JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#define JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifndef JVALUE_T69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF_H
#define JVALUE_T69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JValue
struct  JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF  : public JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02
{
public:
	// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::_valueType
	int32_t ____valueType_13;
	// System.Object Newtonsoft.Json.Linq.JValue::_value
	RuntimeObject * ____value_14;

public:
	inline static int32_t get_offset_of__valueType_13() { return static_cast<int32_t>(offsetof(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF, ____valueType_13)); }
	inline int32_t get__valueType_13() const { return ____valueType_13; }
	inline int32_t* get_address_of__valueType_13() { return &____valueType_13; }
	inline void set__valueType_13(int32_t value)
	{
		____valueType_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF, ____value_14)); }
	inline RuntimeObject * get__value_14() const { return ____value_14; }
	inline RuntimeObject ** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(RuntimeObject * value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JVALUE_T69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF_H
#ifndef JSONLOADSETTINGS_TC733151662103C5983D4F8ADBD871AC405A6504F_H
#define JSONLOADSETTINGS_TC733151662103C5983D4F8ADBD871AC405A6504F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonLoadSettings
struct  JsonLoadSettings_tC733151662103C5983D4F8ADBD871AC405A6504F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::_commentHandling
	int32_t ____commentHandling_0;
	// Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::_lineInfoHandling
	int32_t ____lineInfoHandling_1;

public:
	inline static int32_t get_offset_of__commentHandling_0() { return static_cast<int32_t>(offsetof(JsonLoadSettings_tC733151662103C5983D4F8ADBD871AC405A6504F, ____commentHandling_0)); }
	inline int32_t get__commentHandling_0() const { return ____commentHandling_0; }
	inline int32_t* get_address_of__commentHandling_0() { return &____commentHandling_0; }
	inline void set__commentHandling_0(int32_t value)
	{
		____commentHandling_0 = value;
	}

	inline static int32_t get_offset_of__lineInfoHandling_1() { return static_cast<int32_t>(offsetof(JsonLoadSettings_tC733151662103C5983D4F8ADBD871AC405A6504F, ____lineInfoHandling_1)); }
	inline int32_t get__lineInfoHandling_1() const { return ____lineInfoHandling_1; }
	inline int32_t* get_address_of__lineInfoHandling_1() { return &____lineInfoHandling_1; }
	inline void set__lineInfoHandling_1(int32_t value)
	{
		____lineInfoHandling_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLOADSETTINGS_TC733151662103C5983D4F8ADBD871AC405A6504F_H
#ifndef QUERYEXPRESSION_T08B8769F946D3E05F6E9DB46393F0B9828244E7F_H
#define QUERYEXPRESSION_T08B8769F946D3E05F6E9DB46393F0B9828244E7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryExpression
struct  QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JsonPath.QueryOperator Newtonsoft.Json.Linq.JsonPath.QueryExpression::<Operator>k__BackingField
	int32_t ___U3COperatorU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3COperatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F, ___U3COperatorU3Ek__BackingField_0)); }
	inline int32_t get_U3COperatorU3Ek__BackingField_0() const { return ___U3COperatorU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3COperatorU3Ek__BackingField_0() { return &___U3COperatorU3Ek__BackingField_0; }
	inline void set_U3COperatorU3Ek__BackingField_0(int32_t value)
	{
		___U3COperatorU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYEXPRESSION_T08B8769F946D3E05F6E9DB46393F0B9828244E7F_H
#ifndef JSONCONTRACT_T3521BC52C95CBCD89F85AF8A46062314C12D0843_H
#define JSONCONTRACT_T3521BC52C95CBCD89F85AF8A46062314C12D0843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract
struct  JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Newtonsoft.Json.ReadType Newtonsoft.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Newtonsoft.Json.Serialization.JsonContractType Newtonsoft.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_16;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CConverterU3Ek__BackingField_17;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onDeserializedCallbacks_9)); }
	inline List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_tD367AB7435AA7E4291BFF9F4DDCEC85A99625497 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t61C31ABA417A4314A2582BF0404CE41267A61BE6 * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_T3521BC52C95CBCD89F85AF8A46062314C12D0843_H
#ifndef NULLABLE_1_T0AFF4EB4F9100E8542FAE5E9B748C6AB47878902_H
#define NULLABLE_1_T0AFF4EB4F9100E8542FAE5E9B748C6AB47878902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0AFF4EB4F9100E8542FAE5E9B748C6AB47878902_H
#ifndef NULLABLE_1_T828D7B2DA38B853504C533AE33E0F2948573994A_H
#define NULLABLE_1_T828D7B2DA38B853504C533AE33E0F2948573994A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T828D7B2DA38B853504C533AE33E0F2948573994A_H
#ifndef NULLABLE_1_T3612CCC2209E4540E3BECB46C69104D5FBBF03BB_H
#define NULLABLE_1_T3612CCC2209E4540E3BECB46C69104D5FBBF03BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3612CCC2209E4540E3BECB46C69104D5FBBF03BB_H
#ifndef NULLABLE_1_T1AB0BBC00715984FC97B99A7BD21F61F1F102415_H
#define NULLABLE_1_T1AB0BBC00715984FC97B99A7BD21F61F1F102415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1AB0BBC00715984FC97B99A7BD21F61F1F102415_H
#ifndef NULLABLE_1_TE21E9586B51188362D4719A62CF768EF13E54990_H
#define NULLABLE_1_TE21E9586B51188362D4719A62CF768EF13E54990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE21E9586B51188362D4719A62CF768EF13E54990_H
#ifndef NULLABLE_1_T96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70_H
#define NULLABLE_1_T96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70_H
#ifndef NULLABLE_1_T4B852454D2D7DBCB31FC1154137ACFD46DA02BD8_H
#define NULLABLE_1_T4B852454D2D7DBCB31FC1154137ACFD46DA02BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4B852454D2D7DBCB31FC1154137ACFD46DA02BD8_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef COLORTWEEN_T60F356714C7D61B6F660900D4017054A166402D2_H
#define COLORTWEEN_T60F356714C7D61B6F660900D4017054A166402D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween
struct  ColorTween_t60F356714C7D61B6F660900D4017054A166402D2 
{
public:
	// TMPro.ColorTween_ColorTweenCallback TMPro.ColorTween::m_Target
	ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773 * ___m_Target_0;
	// UnityEngine.Color TMPro.ColorTween::m_StartColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	// UnityEngine.Color TMPro.ColorTween::m_TargetColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	// TMPro.ColorTween_ColorTweenMode TMPro.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single TMPro.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean TMPro.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t60F356714C7D61B6F660900D4017054A166402D2, ___m_Target_0)); }
	inline ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t60F356714C7D61B6F660900D4017054A166402D2, ___m_StartColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t60F356714C7D61B6F660900D4017054A166402D2, ___m_TargetColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t60F356714C7D61B6F660900D4017054A166402D2, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t60F356714C7D61B6F660900D4017054A166402D2, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t60F356714C7D61B6F660900D4017054A166402D2, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.ColorTween
struct ColorTween_t60F356714C7D61B6F660900D4017054A166402D2_marshaled_pinvoke
{
	ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of TMPro.ColorTween
struct ColorTween_t60F356714C7D61B6F660900D4017054A166402D2_marshaled_com
{
	ColorTweenCallback_t92CAE6511C7B9BD4766AF80296DBCB38FFD7F773 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T60F356714C7D61B6F660900D4017054A166402D2_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#define TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef BSONBINARY_TD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982_H
#define BSONBINARY_TD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinary
struct  BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982  : public BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryType Newtonsoft.Json.Bson.BsonBinary::<BinaryType>k__BackingField
	uint8_t ___U3CBinaryTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinaryTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982, ___U3CBinaryTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CBinaryTypeU3Ek__BackingField_4() const { return ___U3CBinaryTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CBinaryTypeU3Ek__BackingField_4() { return &___U3CBinaryTypeU3Ek__BackingField_4; }
	inline void set_U3CBinaryTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CBinaryTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARY_TD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982_H
#ifndef BSONSTRING_T87A7877CE01E3E2FA2B118C4A617FA16159CBC0A_H
#define BSONSTRING_T87A7877CE01E3E2FA2B118C4A617FA16159CBC0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonString
struct  BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A  : public BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonString::<ByteCount>k__BackingField
	int32_t ___U3CByteCountU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Bson.BsonString::<IncludeLength>k__BackingField
	bool ___U3CIncludeLengthU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A, ___U3CByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CByteCountU3Ek__BackingField_4() const { return ___U3CByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CByteCountU3Ek__BackingField_4() { return &___U3CByteCountU3Ek__BackingField_4; }
	inline void set_U3CByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeLengthU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A, ___U3CIncludeLengthU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeLengthU3Ek__BackingField_5() const { return ___U3CIncludeLengthU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeLengthU3Ek__BackingField_5() { return &___U3CIncludeLengthU3Ek__BackingField_5; }
	inline void set_U3CIncludeLengthU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeLengthU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONSTRING_T87A7877CE01E3E2FA2B118C4A617FA16159CBC0A_H
#ifndef JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#define JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader_State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____currentPosition_4)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____culture_5)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____maxDepth_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ____stack_12)); }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * get__stack_12() const { return ____stack_12; }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T95FAA240CCEA60ED65C0B29721A2DA20512B5636_H
#ifndef JSONSERIALIZER_T22D7DE9C2207B3F59CE86171E843ADA94B898623_H
#define JSONSERIALIZER_T22D7DE9C2207B3F59CE86171E843ADA94B898623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormat
	int32_t ____typeNameAssemblyFormat_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::_binder
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ____binder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____typeNameAssemblyFormat_1)); }
	inline int32_t get__typeNameAssemblyFormat_1() const { return ____typeNameAssemblyFormat_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormat_1() { return &____typeNameAssemblyFormat_1; }
	inline void set__typeNameAssemblyFormat_1(int32_t value)
	{
		____typeNameAssemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____converters_10)); }
	inline JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_t88900312A11AA971C2C6438DD622F3E8067BA4E3 * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__binder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____binder_14)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get__binder_14() const { return ____binder_14; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of__binder_14() { return &____binder_14; }
	inline void set__binder_14(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		____binder_14 = value;
		Il2CppCodeGenWriteBarrier((&____binder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____context_15)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get__context_15() const { return ____context_15; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____formatting_17)); }
	inline Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t96A8E1D44C3DA62112A3A16D7B011B8D09A7DD70  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateFormatHandling_18)); }
	inline Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t0AFF4EB4F9100E8542FAE5E9B748C6AB47878902  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_t3612CCC2209E4540E3BECB46C69104D5FBBF03BB  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateParseHandling_20)); }
	inline Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t828D7B2DA38B853504C533AE33E0F2948573994A  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____floatFormatHandling_21)); }
	inline Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t1AB0BBC00715984FC97B99A7BD21F61F1F102415  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____floatParseHandling_22)); }
	inline Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_tE21E9586B51188362D4719A62CF768EF13E54990  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____stringEscapeHandling_23)); }
	inline Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8 * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t4B852454D2D7DBCB31FC1154137ACFD46DA02BD8  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____culture_24)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____maxDepth_25)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____checkAdditionalContent_27)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623, ___Error_30)); }
	inline EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_tE83EC2B50EB1DC60799D2BFE73CCEA5083E06648 * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T22D7DE9C2207B3F59CE86171E843ADA94B898623_H
#ifndef JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#define JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter_State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stack_2)); }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBF641BBAF7DFF674220860102C874E20DB3EB403 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentPosition_3)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifndef JRAW_TCBA8F0819C86FF85ED57CD25E164D4ED1837175C_H
#define JRAW_TCBA8F0819C86FF85ED57CD25E164D4ED1837175C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JRaw
struct  JRaw_tCBA8F0819C86FF85ED57CD25E164D4ED1837175C  : public JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JRAW_TCBA8F0819C86FF85ED57CD25E164D4ED1837175C_H
#ifndef BOOLEANQUERYEXPRESSION_T6F2B15128729297EA0C686FD823C18D7616D667F_H
#define BOOLEANQUERYEXPRESSION_T6F2B15128729297EA0C686FD823C18D7616D667F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression
struct  BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F  : public QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter> Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression::<Path>k__BackingField
	List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * ___U3CPathU3Ek__BackingField_1;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression::<Value>k__BackingField
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F, ___U3CPathU3Ek__BackingField_1)); }
	inline List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * get_U3CPathU3Ek__BackingField_1() const { return ___U3CPathU3Ek__BackingField_1; }
	inline List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 ** get_address_of_U3CPathU3Ek__BackingField_1() { return &___U3CPathU3Ek__BackingField_1; }
	inline void set_U3CPathU3Ek__BackingField_1(List_1_t36919C030DB41B8F0BE34CC4C96991F1D1379557 * value)
	{
		___U3CPathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F, ___U3CValueU3Ek__BackingField_2)); }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF ** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANQUERYEXPRESSION_T6F2B15128729297EA0C686FD823C18D7616D667F_H
#ifndef COMPOSITEEXPRESSION_T3153D0FCDDFBADAA4ED86DCB1527031D389F91E9_H
#define COMPOSITEEXPRESSION_T3153D0FCDDFBADAA4ED86DCB1527031D389F91E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.CompositeExpression
struct  CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9  : public QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.QueryExpression> Newtonsoft.Json.Linq.JsonPath.CompositeExpression::<Expressions>k__BackingField
	List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 * ___U3CExpressionsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExpressionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9, ___U3CExpressionsU3Ek__BackingField_1)); }
	inline List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 * get_U3CExpressionsU3Ek__BackingField_1() const { return ___U3CExpressionsU3Ek__BackingField_1; }
	inline List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 ** get_address_of_U3CExpressionsU3Ek__BackingField_1() { return &___U3CExpressionsU3Ek__BackingField_1; }
	inline void set_U3CExpressionsU3Ek__BackingField_1(List_1_tACC9B8F07DB2C436197EF4D23781FF22DD6ED615 * value)
	{
		___U3CExpressionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITEEXPRESSION_T3153D0FCDDFBADAA4ED86DCB1527031D389F91E9_H
#ifndef JSONPRIMITIVECONTRACT_TF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_H
#define JSONPRIMITIVECONTRACT_TF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C  : public JsonContract_t3521BC52C95CBCD89F85AF8A46062314C12D0843
{
public:
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType> Newtonsoft.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_tF31652FE57012DCD6A44563BE36D73B726EE914D * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_TF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C_H
#ifndef TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#define TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_4;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_6;

public:
	inline static int32_t get_offset_of_hashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___hashCode_4)); }
	inline int32_t get_hashCode_4() const { return ___hashCode_4; }
	inline int32_t* get_address_of_hashCode_4() { return &___hashCode_4; }
	inline void set_hashCode_4(int32_t value)
	{
		___hashCode_4 = value;
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_materialHashCode_6() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___materialHashCode_6)); }
	inline int32_t get_materialHashCode_6() const { return ___materialHashCode_6; }
	inline int32_t* get_address_of_materialHashCode_6() { return &___materialHashCode_6; }
	inline void set_materialHashCode_6(int32_t value)
	{
		___materialHashCode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#ifndef TMP_COLORGRADIENT_TEA29C4736B1786301A803B6C0FB30107A10D79B7_H
#define TMP_COLORGRADIENT_TEA29C4736B1786301A803B6C0FB30107A10D79B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ColorGradient
struct  TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// TMPro.ColorMode TMPro.TMP_ColorGradient::colorMode
	int32_t ___colorMode_4;
	// UnityEngine.Color TMPro.TMP_ColorGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_5;
	// UnityEngine.Color TMPro.TMP_ColorGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_6;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_7;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_8;

public:
	inline static int32_t get_offset_of_colorMode_4() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___colorMode_4)); }
	inline int32_t get_colorMode_4() const { return ___colorMode_4; }
	inline int32_t* get_address_of_colorMode_4() { return &___colorMode_4; }
	inline void set_colorMode_4(int32_t value)
	{
		___colorMode_4 = value;
	}

	inline static int32_t get_offset_of_topLeft_5() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___topLeft_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_5() const { return ___topLeft_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_5() { return &___topLeft_5; }
	inline void set_topLeft_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_5 = value;
	}

	inline static int32_t get_offset_of_topRight_6() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___topRight_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_6() const { return ___topRight_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_6() { return &___topRight_6; }
	inline void set_topRight_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_6 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_7() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___bottomLeft_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_7() const { return ___bottomLeft_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_7() { return &___bottomLeft_7; }
	inline void set_bottomLeft_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_7 = value;
	}

	inline static int32_t get_offset_of_bottomRight_8() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7, ___bottomRight_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_8() const { return ___bottomRight_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_8() { return &___bottomRight_8; }
	inline void set_bottomRight_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_8 = value;
	}
};

struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields
{
public:
	// UnityEngine.Color TMPro.TMP_ColorGradient::k_DefaultColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___k_DefaultColor_10;

public:
	inline static int32_t get_offset_of_k_DefaultColor_10() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields, ___k_DefaultColor_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_k_DefaultColor_10() const { return ___k_DefaultColor_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_k_DefaultColor_10() { return &___k_DefaultColor_10; }
	inline void set_k_DefaultColor_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___k_DefaultColor_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COLORGRADIENT_TEA29C4736B1786301A803B6C0FB30107A10D79B7_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___sizeStack_39;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___indentStack_40;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___styleStack_42;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___baselineStack_43;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___actionStack_44;
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___sizeStack_39;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___indentStack_40;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___styleStack_42;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___baselineStack_43;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___actionStack_44;
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef BSONWRITER_TD3EA6BE4366DF3B2C0E30C83318262FED969CE11_H
#define BSONWRITER_TD3EA6BE4366DF3B2C0E30C83318262FED969CE11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonWriter
struct  BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11  : public JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryWriter Newtonsoft.Json.Bson.BsonWriter::_writer
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 * ____writer_13;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_root
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ____root_14;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_parent
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * ____parent_15;
	// System.String Newtonsoft.Json.Bson.BsonWriter::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____writer_13)); }
	inline BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 * get__writer_13() const { return ____writer_13; }
	inline BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__root_14() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____root_14)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get__root_14() const { return ____root_14; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of__root_14() { return &____root_14; }
	inline void set__root_14(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		____root_14 = value;
		Il2CppCodeGenWriteBarrier((&____root_14), value);
	}

	inline static int32_t get_offset_of__parent_15() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____parent_15)); }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * get__parent_15() const { return ____parent_15; }
	inline BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 ** get_address_of__parent_15() { return &____parent_15; }
	inline void set__parent_15(BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3 * value)
	{
		____parent_15 = value;
		Il2CppCodeGenWriteBarrier((&____parent_15), value);
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONWRITER_TD3EA6BE4366DF3B2C0E30C83318262FED969CE11_H
#ifndef JTOKENREADER_T64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3_H
#define JTOKENREADER_T64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenReader
struct  JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3  : public JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636
{
public:
	// System.String Newtonsoft.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_15;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_root
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____root_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_parent
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____parent_17;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____current_18;

public:
	inline static int32_t get_offset_of__initialPath_15() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____initialPath_15)); }
	inline String_t* get__initialPath_15() const { return ____initialPath_15; }
	inline String_t** get_address_of__initialPath_15() { return &____initialPath_15; }
	inline void set__initialPath_15(String_t* value)
	{
		____initialPath_15 = value;
		Il2CppCodeGenWriteBarrier((&____initialPath_15), value);
	}

	inline static int32_t get_offset_of__root_16() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____root_16)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__root_16() const { return ____root_16; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__root_16() { return &____root_16; }
	inline void set__root_16(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____root_16 = value;
		Il2CppCodeGenWriteBarrier((&____root_16), value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____parent_17)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__parent_17() const { return ____parent_17; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier((&____parent_17), value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3, ____current_18)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__current_18() const { return ____current_18; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier((&____current_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREADER_T64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3_H
#ifndef JTOKENWRITER_TC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4_H
#define JTOKENWRITER_TC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenWriter
struct  JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4  : public JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_token
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ____token_13;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_parent
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * ____parent_14;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JTokenWriter::_value
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * ____value_15;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::_current
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ____current_16;

public:
	inline static int32_t get_offset_of__token_13() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____token_13)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get__token_13() const { return ____token_13; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of__token_13() { return &____token_13; }
	inline void set__token_13(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		____token_13 = value;
		Il2CppCodeGenWriteBarrier((&____token_13), value);
	}

	inline static int32_t get_offset_of__parent_14() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____parent_14)); }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * get__parent_14() const { return ____parent_14; }
	inline JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB ** get_address_of__parent_14() { return &____parent_14; }
	inline void set__parent_14(JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB * value)
	{
		____parent_14 = value;
		Il2CppCodeGenWriteBarrier((&____parent_14), value);
	}

	inline static int32_t get_offset_of__value_15() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____value_15)); }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * get__value_15() const { return ____value_15; }
	inline JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF ** get_address_of__value_15() { return &____value_15; }
	inline void set__value_15(JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF * value)
	{
		____value_15 = value;
		Il2CppCodeGenWriteBarrier((&____value_15), value);
	}

	inline static int32_t get_offset_of__current_16() { return static_cast<int32_t>(offsetof(JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4, ____current_16)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get__current_16() const { return ____current_16; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of__current_16() { return &____current_16; }
	inline void set__current_16(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		____current_16 = value;
		Il2CppCodeGenWriteBarrier((&____current_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENWRITER_TC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4_H
#ifndef JSONSERIALIZERPROXY_T6D455EB811A66C7B8471A2911C84354FDB2A80E2_H
#define JSONSERIALIZERPROXY_T6D455EB811A66C7B8471A2911C84354FDB2A80E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct  JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2  : public JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerReader
	JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D * ____serializerReader_31;
	// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerWriter
	JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A * ____serializerWriter_32;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializer
	JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * ____serializer_33;

public:
	inline static int32_t get_offset_of__serializerReader_31() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2, ____serializerReader_31)); }
	inline JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D * get__serializerReader_31() const { return ____serializerReader_31; }
	inline JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D ** get_address_of__serializerReader_31() { return &____serializerReader_31; }
	inline void set__serializerReader_31(JsonSerializerInternalReader_t8DF542A0658E40C69F7C9A5F8D013D794B9AD78D * value)
	{
		____serializerReader_31 = value;
		Il2CppCodeGenWriteBarrier((&____serializerReader_31), value);
	}

	inline static int32_t get_offset_of__serializerWriter_32() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2, ____serializerWriter_32)); }
	inline JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A * get__serializerWriter_32() const { return ____serializerWriter_32; }
	inline JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A ** get_address_of__serializerWriter_32() { return &____serializerWriter_32; }
	inline void set__serializerWriter_32(JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A * value)
	{
		____serializerWriter_32 = value;
		Il2CppCodeGenWriteBarrier((&____serializerWriter_32), value);
	}

	inline static int32_t get_offset_of__serializer_33() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2, ____serializer_33)); }
	inline JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * get__serializer_33() const { return ____serializer_33; }
	inline JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 ** get_address_of__serializer_33() { return &____serializer_33; }
	inline void set__serializer_33(JsonSerializer_t22D7DE9C2207B3F59CE86171E843ADA94B898623 * value)
	{
		____serializer_33 = value;
		Il2CppCodeGenWriteBarrier((&____serializer_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERPROXY_T6D455EB811A66C7B8471A2911C84354FDB2A80E2_H
#ifndef JSONSTRINGCONTRACT_T7070636F94D5F28C05BA2B07887DA4572C420ACB_H
#define JSONSTRINGCONTRACT_T7070636F94D5F28C05BA2B07887DA4572C420ACB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonStringContract
struct  JsonStringContract_t7070636F94D5F28C05BA2B07887DA4572C420ACB  : public JsonPrimitiveContract_tF7A9BA6F3217837EA965502D8AB3E24DAA20EC8C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRINGCONTRACT_T7070636F94D5F28C05BA2B07887DA4572C420ACB_H
#ifndef TRACEJSONREADER_TF4819EAB44B6955D240FEAB54F53DF1D572723F4_H
#define TRACEJSONREADER_TF4819EAB44B6955D240FEAB54F53DF1D572723F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.TraceJsonReader
struct  TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4  : public JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636
{
public:
	// Newtonsoft.Json.JsonReader Newtonsoft.Json.Serialization.TraceJsonReader::_innerReader
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * ____innerReader_15;
	// Newtonsoft.Json.JsonTextWriter Newtonsoft.Json.Serialization.TraceJsonReader::_textWriter
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 * ____textWriter_16;
	// System.IO.StringWriter Newtonsoft.Json.Serialization.TraceJsonReader::_sw
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * ____sw_17;

public:
	inline static int32_t get_offset_of__innerReader_15() { return static_cast<int32_t>(offsetof(TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4, ____innerReader_15)); }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * get__innerReader_15() const { return ____innerReader_15; }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 ** get_address_of__innerReader_15() { return &____innerReader_15; }
	inline void set__innerReader_15(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * value)
	{
		____innerReader_15 = value;
		Il2CppCodeGenWriteBarrier((&____innerReader_15), value);
	}

	inline static int32_t get_offset_of__textWriter_16() { return static_cast<int32_t>(offsetof(TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4, ____textWriter_16)); }
	inline JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 * get__textWriter_16() const { return ____textWriter_16; }
	inline JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 ** get_address_of__textWriter_16() { return &____textWriter_16; }
	inline void set__textWriter_16(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 * value)
	{
		____textWriter_16 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_16), value);
	}

	inline static int32_t get_offset_of__sw_17() { return static_cast<int32_t>(offsetof(TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4, ____sw_17)); }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * get__sw_17() const { return ____sw_17; }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 ** get_address_of__sw_17() { return &____sw_17; }
	inline void set__sw_17(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * value)
	{
		____sw_17 = value;
		Il2CppCodeGenWriteBarrier((&____sw_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONREADER_TF4819EAB44B6955D240FEAB54F53DF1D572723F4_H
#ifndef TRACEJSONWRITER_T03C3948B6EF80A4B977F14F581757202D464DB6D_H
#define TRACEJSONWRITER_T03C3948B6EF80A4B977F14F581757202D464DB6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.TraceJsonWriter
struct  TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D  : public JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76
{
public:
	// Newtonsoft.Json.JsonWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_innerWriter
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76 * ____innerWriter_13;
	// Newtonsoft.Json.JsonTextWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_textWriter
	JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 * ____textWriter_14;
	// System.IO.StringWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_sw
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * ____sw_15;

public:
	inline static int32_t get_offset_of__innerWriter_13() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D, ____innerWriter_13)); }
	inline JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76 * get__innerWriter_13() const { return ____innerWriter_13; }
	inline JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76 ** get_address_of__innerWriter_13() { return &____innerWriter_13; }
	inline void set__innerWriter_13(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76 * value)
	{
		____innerWriter_13 = value;
		Il2CppCodeGenWriteBarrier((&____innerWriter_13), value);
	}

	inline static int32_t get_offset_of__textWriter_14() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D, ____textWriter_14)); }
	inline JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 * get__textWriter_14() const { return ____textWriter_14; }
	inline JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 ** get_address_of__textWriter_14() { return &____textWriter_14; }
	inline void set__textWriter_14(JsonTextWriter_t44179CD34465F52DCBDDA019383D527CD84CFF01 * value)
	{
		____textWriter_14 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_14), value);
	}

	inline static int32_t get_offset_of__sw_15() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D, ____sw_15)); }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * get__sw_15() const { return ____sw_15; }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 ** get_address_of__sw_15() { return &____sw_15; }
	inline void set__sw_15(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * value)
	{
		____sw_15 = value;
		Il2CppCodeGenWriteBarrier((&____sw_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONWRITER_T03C3948B6EF80A4B977F14F581757202D464DB6D_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef TEXTCONTAINER_TF5E5EB56D152102B19C27607F84847CA594391DE_H
#define TEXTCONTAINER_TF5E5EB56D152102B19C27607F84847CA594391DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainer
struct  TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean TMPro.TextContainer::m_hasChanged
	bool ___m_hasChanged_4;
	// UnityEngine.Vector2 TMPro.TextContainer::m_pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_pivot_5;
	// TMPro.TextContainerAnchors TMPro.TextContainer::m_anchorPosition
	int32_t ___m_anchorPosition_6;
	// UnityEngine.Rect TMPro.TextContainer::m_rect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_rect_7;
	// System.Boolean TMPro.TextContainer::m_isDefaultWidth
	bool ___m_isDefaultWidth_8;
	// System.Boolean TMPro.TextContainer::m_isDefaultHeight
	bool ___m_isDefaultHeight_9;
	// System.Boolean TMPro.TextContainer::m_isAutoFitting
	bool ___m_isAutoFitting_10;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_corners_11;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_worldCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_worldCorners_12;
	// UnityEngine.Vector4 TMPro.TextContainer::m_margins
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margins_13;
	// UnityEngine.RectTransform TMPro.TextContainer::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_14;
	// TMPro.TextMeshPro TMPro.TextContainer::m_textMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_textMeshPro_16;

public:
	inline static int32_t get_offset_of_m_hasChanged_4() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_hasChanged_4)); }
	inline bool get_m_hasChanged_4() const { return ___m_hasChanged_4; }
	inline bool* get_address_of_m_hasChanged_4() { return &___m_hasChanged_4; }
	inline void set_m_hasChanged_4(bool value)
	{
		___m_hasChanged_4 = value;
	}

	inline static int32_t get_offset_of_m_pivot_5() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_pivot_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_pivot_5() const { return ___m_pivot_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_pivot_5() { return &___m_pivot_5; }
	inline void set_m_pivot_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_pivot_5 = value;
	}

	inline static int32_t get_offset_of_m_anchorPosition_6() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_anchorPosition_6)); }
	inline int32_t get_m_anchorPosition_6() const { return ___m_anchorPosition_6; }
	inline int32_t* get_address_of_m_anchorPosition_6() { return &___m_anchorPosition_6; }
	inline void set_m_anchorPosition_6(int32_t value)
	{
		___m_anchorPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_rect_7() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_rect_7)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_rect_7() const { return ___m_rect_7; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_rect_7() { return &___m_rect_7; }
	inline void set_m_rect_7(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_rect_7 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultWidth_8() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_isDefaultWidth_8)); }
	inline bool get_m_isDefaultWidth_8() const { return ___m_isDefaultWidth_8; }
	inline bool* get_address_of_m_isDefaultWidth_8() { return &___m_isDefaultWidth_8; }
	inline void set_m_isDefaultWidth_8(bool value)
	{
		___m_isDefaultWidth_8 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultHeight_9() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_isDefaultHeight_9)); }
	inline bool get_m_isDefaultHeight_9() const { return ___m_isDefaultHeight_9; }
	inline bool* get_address_of_m_isDefaultHeight_9() { return &___m_isDefaultHeight_9; }
	inline void set_m_isDefaultHeight_9(bool value)
	{
		___m_isDefaultHeight_9 = value;
	}

	inline static int32_t get_offset_of_m_isAutoFitting_10() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_isAutoFitting_10)); }
	inline bool get_m_isAutoFitting_10() const { return ___m_isAutoFitting_10; }
	inline bool* get_address_of_m_isAutoFitting_10() { return &___m_isAutoFitting_10; }
	inline void set_m_isAutoFitting_10(bool value)
	{
		___m_isAutoFitting_10 = value;
	}

	inline static int32_t get_offset_of_m_corners_11() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_corners_11)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_corners_11() const { return ___m_corners_11; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_corners_11() { return &___m_corners_11; }
	inline void set_m_corners_11(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_corners_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_corners_11), value);
	}

	inline static int32_t get_offset_of_m_worldCorners_12() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_worldCorners_12)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_worldCorners_12() const { return ___m_worldCorners_12; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_worldCorners_12() { return &___m_worldCorners_12; }
	inline void set_m_worldCorners_12(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_worldCorners_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_worldCorners_12), value);
	}

	inline static int32_t get_offset_of_m_margins_13() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_margins_13)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margins_13() const { return ___m_margins_13; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margins_13() { return &___m_margins_13; }
	inline void set_m_margins_13(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margins_13 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_14() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_rectTransform_14)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_14() const { return ___m_rectTransform_14; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_14() { return &___m_rectTransform_14; }
	inline void set_m_rectTransform_14(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_14), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_16() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE, ___m_textMeshPro_16)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_textMeshPro_16() const { return ___m_textMeshPro_16; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_textMeshPro_16() { return &___m_textMeshPro_16; }
	inline void set_m_textMeshPro_16(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_textMeshPro_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_16), value);
	}
};

struct TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TextContainer::k_defaultSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_defaultSize_15;

public:
	inline static int32_t get_offset_of_k_defaultSize_15() { return static_cast<int32_t>(offsetof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields, ___k_defaultSize_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_defaultSize_15() const { return ___k_defaultSize_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_defaultSize_15() { return &___k_defaultSize_15; }
	inline void set_k_defaultSize_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_defaultSize_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINER_TF5E5EB56D152102B19C27607F84847CA594391DE_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#define TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_highlightColor_50;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_51;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_52;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  ___m_fontColorGradient_53;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_fontColorGradientPreset_54;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_55;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_56;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_spriteColor_58;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_59;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_faceColor_60;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_outlineColor_61;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_62;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_63;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_64;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_65;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___m_sizeStack_66;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_67;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_68;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___m_fontWeightStack_69;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_70;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_71;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_72;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_73;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_74;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_75;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_76;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___m_fontStyleStack_77;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_78;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_79;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_80;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___m_lineJustificationStack_81;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_textContainerLocalCorners_82;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_83;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_84;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_85;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_86;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_88;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_89;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_90;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_91;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_92;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_93;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_94;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_95;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_96;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_97;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_98;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_99;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_100;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_101;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_linkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_103;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_104;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_105;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_106;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_107;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_108;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_109;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_110;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_111;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_113;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_115;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_116;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_117;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_118;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_119;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_120;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_122;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_123;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_124;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_125;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_126;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margin_127;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_128;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_129;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_130;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_131;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_132;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___m_textInfo_133;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_134;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_135;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_136;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_137;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_138;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_139;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_140;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_141;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * ___m_spriteAnimator_142;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_143;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_144;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_145;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_146;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_147;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_148;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___m_LayoutElement_149;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_150;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_151;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_152;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_153;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_154;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_155;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_156;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_157;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_158;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_159;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_160;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_161;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_162;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_163;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_164;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_165;
	// TMPro.TMP_Text_TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_166;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_167;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_168;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_169;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_htmlTag_170;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_tFE12AC6A01EC7B573E971252DAB696F355F76B6B* ___m_xmlAttribute_171;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_attributeParameterValues_172;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_173;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_174;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___m_indentStack_175;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_176;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_177;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_FXMatrix_178;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_179;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_char_buffer_180;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___m_internalCharacterInfo_181;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_input_CharArray_182;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_183;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_184;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedWordWrapState_185;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedLineState_186;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_187;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_188;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_191;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_192;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_193;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_194;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_195;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_196;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_197;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_198;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_199;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_200;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_201;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___m_meshExtents_202;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_htmlColor_203;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___m_colorStack_204;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___m_underlineColorStack_205;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___m_strikethroughColorStack_206;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___m_highlightColorStack_207;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_colorGradientPreset_208;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___m_colorGradientStack_209;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_210;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_211;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___m_styleStack_212;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___m_actionStack_213;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_214;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_215;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___m_baselineOffsetStack_216;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_217;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_218;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___m_cached_TextElement_219;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C * ___m_cached_Underline_GlyphInfo_220;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C * ___m_cached_Ellipsis_GlyphInfo_221;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_defaultSpriteAsset_222;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_currentSpriteAsset_223;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_224;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_225;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_226;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_227;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___k_Power_228;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sharedMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterial_36)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceStack_39)); }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterial_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor32_45)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor_46)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColor_48)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColor_49)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColor_50)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_51() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableVertexGradient_51)); }
	inline bool get_m_enableVertexGradient_51() const { return ___m_enableVertexGradient_51; }
	inline bool* get_address_of_m_enableVertexGradient_51() { return &___m_enableVertexGradient_51; }
	inline void set_m_enableVertexGradient_51(bool value)
	{
		___m_enableVertexGradient_51 = value;
	}

	inline static int32_t get_offset_of_m_colorMode_52() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorMode_52)); }
	inline int32_t get_m_colorMode_52() const { return ___m_colorMode_52; }
	inline int32_t* get_address_of_m_colorMode_52() { return &___m_colorMode_52; }
	inline void set_m_colorMode_52(int32_t value)
	{
		___m_colorMode_52 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_53() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradient_53)); }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  get_m_fontColorGradient_53() const { return ___m_fontColorGradient_53; }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A * get_address_of_m_fontColorGradient_53() { return &___m_fontColorGradient_53; }
	inline void set_m_fontColorGradient_53(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  value)
	{
		___m_fontColorGradient_53 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_54() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradientPreset_54)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_fontColorGradientPreset_54() const { return ___m_fontColorGradientPreset_54; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_fontColorGradientPreset_54() { return &___m_fontColorGradientPreset_54; }
	inline void set_m_fontColorGradientPreset_54(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_fontColorGradientPreset_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_54), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_55() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAsset_55)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_55() const { return ___m_spriteAsset_55; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_55() { return &___m_spriteAsset_55; }
	inline void set_m_spriteAsset_55(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_55), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_56() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintAllSprites_56)); }
	inline bool get_m_tintAllSprites_56() const { return ___m_tintAllSprites_56; }
	inline bool* get_address_of_m_tintAllSprites_56() { return &___m_tintAllSprites_56; }
	inline void set_m_tintAllSprites_56(bool value)
	{
		___m_tintAllSprites_56 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_57() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintSprite_57)); }
	inline bool get_m_tintSprite_57() const { return ___m_tintSprite_57; }
	inline bool* get_address_of_m_tintSprite_57() { return &___m_tintSprite_57; }
	inline void set_m_tintSprite_57(bool value)
	{
		___m_tintSprite_57 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_58() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteColor_58)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_spriteColor_58() const { return ___m_spriteColor_58; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_spriteColor_58() { return &___m_spriteColor_58; }
	inline void set_m_spriteColor_58(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_spriteColor_58 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_59() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overrideHtmlColors_59)); }
	inline bool get_m_overrideHtmlColors_59() const { return ___m_overrideHtmlColors_59; }
	inline bool* get_address_of_m_overrideHtmlColors_59() { return &___m_overrideHtmlColors_59; }
	inline void set_m_overrideHtmlColors_59(bool value)
	{
		___m_overrideHtmlColors_59 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_60() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_faceColor_60)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_faceColor_60() const { return ___m_faceColor_60; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_faceColor_60() { return &___m_faceColor_60; }
	inline void set_m_faceColor_60(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_faceColor_60 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_61() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineColor_61)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_outlineColor_61() const { return ___m_outlineColor_61; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_outlineColor_61() { return &___m_outlineColor_61; }
	inline void set_m_outlineColor_61(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_outlineColor_61 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_62() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineWidth_62)); }
	inline float get_m_outlineWidth_62() const { return ___m_outlineWidth_62; }
	inline float* get_address_of_m_outlineWidth_62() { return &___m_outlineWidth_62; }
	inline void set_m_outlineWidth_62(float value)
	{
		___m_outlineWidth_62 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_63() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSize_63)); }
	inline float get_m_fontSize_63() const { return ___m_fontSize_63; }
	inline float* get_address_of_m_fontSize_63() { return &___m_fontSize_63; }
	inline void set_m_fontSize_63(float value)
	{
		___m_fontSize_63 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_64() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontSize_64)); }
	inline float get_m_currentFontSize_64() const { return ___m_currentFontSize_64; }
	inline float* get_address_of_m_currentFontSize_64() { return &___m_currentFontSize_64; }
	inline void set_m_currentFontSize_64(float value)
	{
		___m_currentFontSize_64 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_65() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeBase_65)); }
	inline float get_m_fontSizeBase_65() const { return ___m_fontSizeBase_65; }
	inline float* get_address_of_m_fontSizeBase_65() { return &___m_fontSizeBase_65; }
	inline void set_m_fontSizeBase_65(float value)
	{
		___m_fontSizeBase_65 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_66() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sizeStack_66)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_m_sizeStack_66() const { return ___m_sizeStack_66; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_m_sizeStack_66() { return &___m_sizeStack_66; }
	inline void set_m_sizeStack_66(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___m_sizeStack_66 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_67() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeight_67)); }
	inline int32_t get_m_fontWeight_67() const { return ___m_fontWeight_67; }
	inline int32_t* get_address_of_m_fontWeight_67() { return &___m_fontWeight_67; }
	inline void set_m_fontWeight_67(int32_t value)
	{
		___m_fontWeight_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_68() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeightInternal_68)); }
	inline int32_t get_m_fontWeightInternal_68() const { return ___m_fontWeightInternal_68; }
	inline int32_t* get_address_of_m_fontWeightInternal_68() { return &___m_fontWeightInternal_68; }
	inline void set_m_fontWeightInternal_68(int32_t value)
	{
		___m_fontWeightInternal_68 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_69() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeightStack_69)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_m_fontWeightStack_69() const { return ___m_fontWeightStack_69; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_m_fontWeightStack_69() { return &___m_fontWeightStack_69; }
	inline void set_m_fontWeightStack_69(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___m_fontWeightStack_69 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_70() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableAutoSizing_70)); }
	inline bool get_m_enableAutoSizing_70() const { return ___m_enableAutoSizing_70; }
	inline bool* get_address_of_m_enableAutoSizing_70() { return &___m_enableAutoSizing_70; }
	inline void set_m_enableAutoSizing_70(bool value)
	{
		___m_enableAutoSizing_70 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_71() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxFontSize_71)); }
	inline float get_m_maxFontSize_71() const { return ___m_maxFontSize_71; }
	inline float* get_address_of_m_maxFontSize_71() { return &___m_maxFontSize_71; }
	inline void set_m_maxFontSize_71(float value)
	{
		___m_maxFontSize_71 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_72() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minFontSize_72)); }
	inline float get_m_minFontSize_72() const { return ___m_minFontSize_72; }
	inline float* get_address_of_m_minFontSize_72() { return &___m_minFontSize_72; }
	inline void set_m_minFontSize_72(float value)
	{
		___m_minFontSize_72 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_73() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMin_73)); }
	inline float get_m_fontSizeMin_73() const { return ___m_fontSizeMin_73; }
	inline float* get_address_of_m_fontSizeMin_73() { return &___m_fontSizeMin_73; }
	inline void set_m_fontSizeMin_73(float value)
	{
		___m_fontSizeMin_73 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_74() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMax_74)); }
	inline float get_m_fontSizeMax_74() const { return ___m_fontSizeMax_74; }
	inline float* get_address_of_m_fontSizeMax_74() { return &___m_fontSizeMax_74; }
	inline void set_m_fontSizeMax_74(float value)
	{
		___m_fontSizeMax_74 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_75() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyle_75)); }
	inline int32_t get_m_fontStyle_75() const { return ___m_fontStyle_75; }
	inline int32_t* get_address_of_m_fontStyle_75() { return &___m_fontStyle_75; }
	inline void set_m_fontStyle_75(int32_t value)
	{
		___m_fontStyle_75 = value;
	}

	inline static int32_t get_offset_of_m_style_76() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_style_76)); }
	inline int32_t get_m_style_76() const { return ___m_style_76; }
	inline int32_t* get_address_of_m_style_76() { return &___m_style_76; }
	inline void set_m_style_76(int32_t value)
	{
		___m_style_76 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_77() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyleStack_77)); }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  get_m_fontStyleStack_77() const { return ___m_fontStyleStack_77; }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 * get_address_of_m_fontStyleStack_77() { return &___m_fontStyleStack_77; }
	inline void set_m_fontStyleStack_77(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  value)
	{
		___m_fontStyleStack_77 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_78() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingBold_78)); }
	inline bool get_m_isUsingBold_78() const { return ___m_isUsingBold_78; }
	inline bool* get_address_of_m_isUsingBold_78() { return &___m_isUsingBold_78; }
	inline void set_m_isUsingBold_78(bool value)
	{
		___m_isUsingBold_78 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_79() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textAlignment_79)); }
	inline int32_t get_m_textAlignment_79() const { return ___m_textAlignment_79; }
	inline int32_t* get_address_of_m_textAlignment_79() { return &___m_textAlignment_79; }
	inline void set_m_textAlignment_79(int32_t value)
	{
		___m_textAlignment_79 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_80() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustification_80)); }
	inline int32_t get_m_lineJustification_80() const { return ___m_lineJustification_80; }
	inline int32_t* get_address_of_m_lineJustification_80() { return &___m_lineJustification_80; }
	inline void set_m_lineJustification_80(int32_t value)
	{
		___m_lineJustification_80 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_81() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustificationStack_81)); }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  get_m_lineJustificationStack_81() const { return ___m_lineJustificationStack_81; }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C * get_address_of_m_lineJustificationStack_81() { return &___m_lineJustificationStack_81; }
	inline void set_m_lineJustificationStack_81(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  value)
	{
		___m_lineJustificationStack_81 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_82() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textContainerLocalCorners_82)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_textContainerLocalCorners_82() const { return ___m_textContainerLocalCorners_82; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_textContainerLocalCorners_82() { return &___m_textContainerLocalCorners_82; }
	inline void set_m_textContainerLocalCorners_82(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_textContainerLocalCorners_82 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_82), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_83() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isAlignmentEnumConverted_83)); }
	inline bool get_m_isAlignmentEnumConverted_83() const { return ___m_isAlignmentEnumConverted_83; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_83() { return &___m_isAlignmentEnumConverted_83; }
	inline void set_m_isAlignmentEnumConverted_83(bool value)
	{
		___m_isAlignmentEnumConverted_83 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterSpacing_84)); }
	inline float get_m_characterSpacing_84() const { return ___m_characterSpacing_84; }
	inline float* get_address_of_m_characterSpacing_84() { return &___m_characterSpacing_84; }
	inline void set_m_characterSpacing_84(float value)
	{
		___m_characterSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cSpacing_85)); }
	inline float get_m_cSpacing_85() const { return ___m_cSpacing_85; }
	inline float* get_address_of_m_cSpacing_85() { return &___m_cSpacing_85; }
	inline void set_m_cSpacing_85(float value)
	{
		___m_cSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_monoSpacing_86)); }
	inline float get_m_monoSpacing_86() const { return ___m_monoSpacing_86; }
	inline float* get_address_of_m_monoSpacing_86() { return &___m_monoSpacing_86; }
	inline void set_m_monoSpacing_86(float value)
	{
		___m_monoSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordSpacing_87)); }
	inline float get_m_wordSpacing_87() const { return ___m_wordSpacing_87; }
	inline float* get_address_of_m_wordSpacing_87() { return &___m_wordSpacing_87; }
	inline void set_m_wordSpacing_87(float value)
	{
		___m_wordSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_88() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacing_88)); }
	inline float get_m_lineSpacing_88() const { return ___m_lineSpacing_88; }
	inline float* get_address_of_m_lineSpacing_88() { return &___m_lineSpacing_88; }
	inline void set_m_lineSpacing_88(float value)
	{
		___m_lineSpacing_88 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_89() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingDelta_89)); }
	inline float get_m_lineSpacingDelta_89() const { return ___m_lineSpacingDelta_89; }
	inline float* get_address_of_m_lineSpacingDelta_89() { return &___m_lineSpacingDelta_89; }
	inline void set_m_lineSpacingDelta_89(float value)
	{
		___m_lineSpacingDelta_89 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_90() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineHeight_90)); }
	inline float get_m_lineHeight_90() const { return ___m_lineHeight_90; }
	inline float* get_address_of_m_lineHeight_90() { return &___m_lineHeight_90; }
	inline void set_m_lineHeight_90(float value)
	{
		___m_lineHeight_90 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_91() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingMax_91)); }
	inline float get_m_lineSpacingMax_91() const { return ___m_lineSpacingMax_91; }
	inline float* get_address_of_m_lineSpacingMax_91() { return &___m_lineSpacingMax_91; }
	inline void set_m_lineSpacingMax_91(float value)
	{
		___m_lineSpacingMax_91 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_92() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_paragraphSpacing_92)); }
	inline float get_m_paragraphSpacing_92() const { return ___m_paragraphSpacing_92; }
	inline float* get_address_of_m_paragraphSpacing_92() { return &___m_paragraphSpacing_92; }
	inline void set_m_paragraphSpacing_92(float value)
	{
		___m_paragraphSpacing_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_93() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthMaxAdj_93)); }
	inline float get_m_charWidthMaxAdj_93() const { return ___m_charWidthMaxAdj_93; }
	inline float* get_address_of_m_charWidthMaxAdj_93() { return &___m_charWidthMaxAdj_93; }
	inline void set_m_charWidthMaxAdj_93(float value)
	{
		___m_charWidthMaxAdj_93 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_94() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthAdjDelta_94)); }
	inline float get_m_charWidthAdjDelta_94() const { return ___m_charWidthAdjDelta_94; }
	inline float* get_address_of_m_charWidthAdjDelta_94() { return &___m_charWidthAdjDelta_94; }
	inline void set_m_charWidthAdjDelta_94(float value)
	{
		___m_charWidthAdjDelta_94 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_95() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableWordWrapping_95)); }
	inline bool get_m_enableWordWrapping_95() const { return ___m_enableWordWrapping_95; }
	inline bool* get_address_of_m_enableWordWrapping_95() { return &___m_enableWordWrapping_95; }
	inline void set_m_enableWordWrapping_95(bool value)
	{
		___m_enableWordWrapping_95 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_96() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCharacterWrappingEnabled_96)); }
	inline bool get_m_isCharacterWrappingEnabled_96() const { return ___m_isCharacterWrappingEnabled_96; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_96() { return &___m_isCharacterWrappingEnabled_96; }
	inline void set_m_isCharacterWrappingEnabled_96(bool value)
	{
		___m_isCharacterWrappingEnabled_96 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_97() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNonBreakingSpace_97)); }
	inline bool get_m_isNonBreakingSpace_97() const { return ___m_isNonBreakingSpace_97; }
	inline bool* get_address_of_m_isNonBreakingSpace_97() { return &___m_isNonBreakingSpace_97; }
	inline void set_m_isNonBreakingSpace_97(bool value)
	{
		___m_isNonBreakingSpace_97 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_98() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isIgnoringAlignment_98)); }
	inline bool get_m_isIgnoringAlignment_98() const { return ___m_isIgnoringAlignment_98; }
	inline bool* get_address_of_m_isIgnoringAlignment_98() { return &___m_isIgnoringAlignment_98; }
	inline void set_m_isIgnoringAlignment_98(bool value)
	{
		___m_isIgnoringAlignment_98 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_99() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordWrappingRatios_99)); }
	inline float get_m_wordWrappingRatios_99() const { return ___m_wordWrappingRatios_99; }
	inline float* get_address_of_m_wordWrappingRatios_99() { return &___m_wordWrappingRatios_99; }
	inline void set_m_wordWrappingRatios_99(float value)
	{
		___m_wordWrappingRatios_99 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_100() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overflowMode_100)); }
	inline int32_t get_m_overflowMode_100() const { return ___m_overflowMode_100; }
	inline int32_t* get_address_of_m_overflowMode_100() { return &___m_overflowMode_100; }
	inline void set_m_overflowMode_100(int32_t value)
	{
		___m_overflowMode_100 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_101() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstOverflowCharacterIndex_101)); }
	inline int32_t get_m_firstOverflowCharacterIndex_101() const { return ___m_firstOverflowCharacterIndex_101; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_101() { return &___m_firstOverflowCharacterIndex_101; }
	inline void set_m_firstOverflowCharacterIndex_101(int32_t value)
	{
		___m_firstOverflowCharacterIndex_101 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_linkedTextComponent_102)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_linkedTextComponent_102() const { return ___m_linkedTextComponent_102; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_linkedTextComponent_102() { return &___m_linkedTextComponent_102; }
	inline void set_m_linkedTextComponent_102(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_linkedTextComponent_102 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_102), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_103() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLinkedTextComponent_103)); }
	inline bool get_m_isLinkedTextComponent_103() const { return ___m_isLinkedTextComponent_103; }
	inline bool* get_address_of_m_isLinkedTextComponent_103() { return &___m_isLinkedTextComponent_103; }
	inline void set_m_isLinkedTextComponent_103(bool value)
	{
		___m_isLinkedTextComponent_103 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_104() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isTextTruncated_104)); }
	inline bool get_m_isTextTruncated_104() const { return ___m_isTextTruncated_104; }
	inline bool* get_address_of_m_isTextTruncated_104() { return &___m_isTextTruncated_104; }
	inline void set_m_isTextTruncated_104(bool value)
	{
		___m_isTextTruncated_104 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_105() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableKerning_105)); }
	inline bool get_m_enableKerning_105() const { return ___m_enableKerning_105; }
	inline bool* get_address_of_m_enableKerning_105() { return &___m_enableKerning_105; }
	inline void set_m_enableKerning_105(bool value)
	{
		___m_enableKerning_105 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_106() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableExtraPadding_106)); }
	inline bool get_m_enableExtraPadding_106() const { return ___m_enableExtraPadding_106; }
	inline bool* get_address_of_m_enableExtraPadding_106() { return &___m_enableExtraPadding_106; }
	inline void set_m_enableExtraPadding_106(bool value)
	{
		___m_enableExtraPadding_106 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_107() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___checkPaddingRequired_107)); }
	inline bool get_checkPaddingRequired_107() const { return ___checkPaddingRequired_107; }
	inline bool* get_address_of_checkPaddingRequired_107() { return &___checkPaddingRequired_107; }
	inline void set_checkPaddingRequired_107(bool value)
	{
		___checkPaddingRequired_107 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_108() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRichText_108)); }
	inline bool get_m_isRichText_108() const { return ___m_isRichText_108; }
	inline bool* get_address_of_m_isRichText_108() { return &___m_isRichText_108; }
	inline void set_m_isRichText_108(bool value)
	{
		___m_isRichText_108 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_109() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_parseCtrlCharacters_109)); }
	inline bool get_m_parseCtrlCharacters_109() const { return ___m_parseCtrlCharacters_109; }
	inline bool* get_address_of_m_parseCtrlCharacters_109() { return &___m_parseCtrlCharacters_109; }
	inline void set_m_parseCtrlCharacters_109(bool value)
	{
		___m_parseCtrlCharacters_109 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_110() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOverlay_110)); }
	inline bool get_m_isOverlay_110() const { return ___m_isOverlay_110; }
	inline bool* get_address_of_m_isOverlay_110() { return &___m_isOverlay_110; }
	inline void set_m_isOverlay_110(bool value)
	{
		___m_isOverlay_110 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_111() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOrthographic_111)); }
	inline bool get_m_isOrthographic_111() const { return ___m_isOrthographic_111; }
	inline bool* get_address_of_m_isOrthographic_111() { return &___m_isOrthographic_111; }
	inline void set_m_isOrthographic_111(bool value)
	{
		___m_isOrthographic_111 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_112() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCullingEnabled_112)); }
	inline bool get_m_isCullingEnabled_112() const { return ___m_isCullingEnabled_112; }
	inline bool* get_address_of_m_isCullingEnabled_112() { return &___m_isCullingEnabled_112; }
	inline void set_m_isCullingEnabled_112(bool value)
	{
		___m_isCullingEnabled_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreRectMaskCulling_113)); }
	inline bool get_m_ignoreRectMaskCulling_113() const { return ___m_ignoreRectMaskCulling_113; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_113() { return &___m_ignoreRectMaskCulling_113; }
	inline void set_m_ignoreRectMaskCulling_113(bool value)
	{
		___m_ignoreRectMaskCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_114() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreCulling_114)); }
	inline bool get_m_ignoreCulling_114() const { return ___m_ignoreCulling_114; }
	inline bool* get_address_of_m_ignoreCulling_114() { return &___m_ignoreCulling_114; }
	inline void set_m_ignoreCulling_114(bool value)
	{
		___m_ignoreCulling_114 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_horizontalMapping_115)); }
	inline int32_t get_m_horizontalMapping_115() const { return ___m_horizontalMapping_115; }
	inline int32_t* get_address_of_m_horizontalMapping_115() { return &___m_horizontalMapping_115; }
	inline void set_m_horizontalMapping_115(int32_t value)
	{
		___m_horizontalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_116() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticalMapping_116)); }
	inline int32_t get_m_verticalMapping_116() const { return ___m_verticalMapping_116; }
	inline int32_t* get_address_of_m_verticalMapping_116() { return &___m_verticalMapping_116; }
	inline void set_m_verticalMapping_116(int32_t value)
	{
		___m_verticalMapping_116 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_117() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_uvLineOffset_117)); }
	inline float get_m_uvLineOffset_117() const { return ___m_uvLineOffset_117; }
	inline float* get_address_of_m_uvLineOffset_117() { return &___m_uvLineOffset_117; }
	inline void set_m_uvLineOffset_117(float value)
	{
		___m_uvLineOffset_117 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_118() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderMode_118)); }
	inline int32_t get_m_renderMode_118() const { return ___m_renderMode_118; }
	inline int32_t* get_address_of_m_renderMode_118() { return &___m_renderMode_118; }
	inline void set_m_renderMode_118(int32_t value)
	{
		___m_renderMode_118 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_119() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_geometrySortingOrder_119)); }
	inline int32_t get_m_geometrySortingOrder_119() const { return ___m_geometrySortingOrder_119; }
	inline int32_t* get_address_of_m_geometrySortingOrder_119() { return &___m_geometrySortingOrder_119; }
	inline void set_m_geometrySortingOrder_119(int32_t value)
	{
		___m_geometrySortingOrder_119 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_120() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacter_120)); }
	inline int32_t get_m_firstVisibleCharacter_120() const { return ___m_firstVisibleCharacter_120; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_120() { return &___m_firstVisibleCharacter_120; }
	inline void set_m_firstVisibleCharacter_120(int32_t value)
	{
		___m_firstVisibleCharacter_120 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_121() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleCharacters_121)); }
	inline int32_t get_m_maxVisibleCharacters_121() const { return ___m_maxVisibleCharacters_121; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_121() { return &___m_maxVisibleCharacters_121; }
	inline void set_m_maxVisibleCharacters_121(int32_t value)
	{
		___m_maxVisibleCharacters_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_122() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleWords_122)); }
	inline int32_t get_m_maxVisibleWords_122() const { return ___m_maxVisibleWords_122; }
	inline int32_t* get_address_of_m_maxVisibleWords_122() { return &___m_maxVisibleWords_122; }
	inline void set_m_maxVisibleWords_122(int32_t value)
	{
		___m_maxVisibleWords_122 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_123() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleLines_123)); }
	inline int32_t get_m_maxVisibleLines_123() const { return ___m_maxVisibleLines_123; }
	inline int32_t* get_address_of_m_maxVisibleLines_123() { return &___m_maxVisibleLines_123; }
	inline void set_m_maxVisibleLines_123(int32_t value)
	{
		___m_maxVisibleLines_123 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_124() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_useMaxVisibleDescender_124)); }
	inline bool get_m_useMaxVisibleDescender_124() const { return ___m_useMaxVisibleDescender_124; }
	inline bool* get_address_of_m_useMaxVisibleDescender_124() { return &___m_useMaxVisibleDescender_124; }
	inline void set_m_useMaxVisibleDescender_124(bool value)
	{
		___m_useMaxVisibleDescender_124 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_125() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageToDisplay_125)); }
	inline int32_t get_m_pageToDisplay_125() const { return ___m_pageToDisplay_125; }
	inline int32_t* get_address_of_m_pageToDisplay_125() { return &___m_pageToDisplay_125; }
	inline void set_m_pageToDisplay_125(int32_t value)
	{
		___m_pageToDisplay_125 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_126() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNewPage_126)); }
	inline bool get_m_isNewPage_126() const { return ___m_isNewPage_126; }
	inline bool* get_address_of_m_isNewPage_126() { return &___m_isNewPage_126; }
	inline void set_m_isNewPage_126(bool value)
	{
		___m_isNewPage_126 = value;
	}

	inline static int32_t get_offset_of_m_margin_127() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_margin_127)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margin_127() const { return ___m_margin_127; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margin_127() { return &___m_margin_127; }
	inline void set_m_margin_127(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margin_127 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_128() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginLeft_128)); }
	inline float get_m_marginLeft_128() const { return ___m_marginLeft_128; }
	inline float* get_address_of_m_marginLeft_128() { return &___m_marginLeft_128; }
	inline void set_m_marginLeft_128(float value)
	{
		___m_marginLeft_128 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_129() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginRight_129)); }
	inline float get_m_marginRight_129() const { return ___m_marginRight_129; }
	inline float* get_address_of_m_marginRight_129() { return &___m_marginRight_129; }
	inline void set_m_marginRight_129(float value)
	{
		___m_marginRight_129 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_130() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginWidth_130)); }
	inline float get_m_marginWidth_130() const { return ___m_marginWidth_130; }
	inline float* get_address_of_m_marginWidth_130() { return &___m_marginWidth_130; }
	inline void set_m_marginWidth_130(float value)
	{
		___m_marginWidth_130 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_131() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginHeight_131)); }
	inline float get_m_marginHeight_131() const { return ___m_marginHeight_131; }
	inline float* get_address_of_m_marginHeight_131() { return &___m_marginHeight_131; }
	inline void set_m_marginHeight_131(float value)
	{
		___m_marginHeight_131 = value;
	}

	inline static int32_t get_offset_of_m_width_132() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_width_132)); }
	inline float get_m_width_132() const { return ___m_width_132; }
	inline float* get_address_of_m_width_132() { return &___m_width_132; }
	inline void set_m_width_132(float value)
	{
		___m_width_132 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_133() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textInfo_133)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_m_textInfo_133() const { return ___m_textInfo_133; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_m_textInfo_133() { return &___m_textInfo_133; }
	inline void set_m_textInfo_133(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___m_textInfo_133 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_133), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_134() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_havePropertiesChanged_134)); }
	inline bool get_m_havePropertiesChanged_134() const { return ___m_havePropertiesChanged_134; }
	inline bool* get_address_of_m_havePropertiesChanged_134() { return &___m_havePropertiesChanged_134; }
	inline void set_m_havePropertiesChanged_134(bool value)
	{
		___m_havePropertiesChanged_134 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_135() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingLegacyAnimationComponent_135)); }
	inline bool get_m_isUsingLegacyAnimationComponent_135() const { return ___m_isUsingLegacyAnimationComponent_135; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_135() { return &___m_isUsingLegacyAnimationComponent_135; }
	inline void set_m_isUsingLegacyAnimationComponent_135(bool value)
	{
		___m_isUsingLegacyAnimationComponent_135 = value;
	}

	inline static int32_t get_offset_of_m_transform_136() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_transform_136)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_136() const { return ___m_transform_136; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_136() { return &___m_transform_136; }
	inline void set_m_transform_136(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_136 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_136), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_137() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_rectTransform_137)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_137() const { return ___m_rectTransform_137; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_137() { return &___m_rectTransform_137; }
	inline void set_m_rectTransform_137(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_137), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_138() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___U3CautoSizeTextContainerU3Ek__BackingField_138)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_138() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_138; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_138() { return &___U3CautoSizeTextContainerU3Ek__BackingField_138; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_138(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_138 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_139() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_autoSizeTextContainer_139)); }
	inline bool get_m_autoSizeTextContainer_139() const { return ___m_autoSizeTextContainer_139; }
	inline bool* get_address_of_m_autoSizeTextContainer_139() { return &___m_autoSizeTextContainer_139; }
	inline void set_m_autoSizeTextContainer_139(bool value)
	{
		___m_autoSizeTextContainer_139 = value;
	}

	inline static int32_t get_offset_of_m_mesh_140() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_mesh_140)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_140() const { return ___m_mesh_140; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_140() { return &___m_mesh_140; }
	inline void set_m_mesh_140(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_140 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_140), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_141() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isVolumetricText_141)); }
	inline bool get_m_isVolumetricText_141() const { return ___m_isVolumetricText_141; }
	inline bool* get_address_of_m_isVolumetricText_141() { return &___m_isVolumetricText_141; }
	inline void set_m_isVolumetricText_141(bool value)
	{
		___m_isVolumetricText_141 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_142() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimator_142)); }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * get_m_spriteAnimator_142() const { return ___m_spriteAnimator_142; }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 ** get_address_of_m_spriteAnimator_142() { return &___m_spriteAnimator_142; }
	inline void set_m_spriteAnimator_142(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * value)
	{
		___m_spriteAnimator_142 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_142), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_143() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleHeight_143)); }
	inline float get_m_flexibleHeight_143() const { return ___m_flexibleHeight_143; }
	inline float* get_address_of_m_flexibleHeight_143() { return &___m_flexibleHeight_143; }
	inline void set_m_flexibleHeight_143(float value)
	{
		___m_flexibleHeight_143 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleWidth_144)); }
	inline float get_m_flexibleWidth_144() const { return ___m_flexibleWidth_144; }
	inline float* get_address_of_m_flexibleWidth_144() { return &___m_flexibleWidth_144; }
	inline void set_m_flexibleWidth_144(float value)
	{
		___m_flexibleWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_145() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minWidth_145)); }
	inline float get_m_minWidth_145() const { return ___m_minWidth_145; }
	inline float* get_address_of_m_minWidth_145() { return &___m_minWidth_145; }
	inline void set_m_minWidth_145(float value)
	{
		___m_minWidth_145 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_146() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minHeight_146)); }
	inline float get_m_minHeight_146() const { return ___m_minHeight_146; }
	inline float* get_address_of_m_minHeight_146() { return &___m_minHeight_146; }
	inline void set_m_minHeight_146(float value)
	{
		___m_minHeight_146 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_147() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxWidth_147)); }
	inline float get_m_maxWidth_147() const { return ___m_maxWidth_147; }
	inline float* get_address_of_m_maxWidth_147() { return &___m_maxWidth_147; }
	inline void set_m_maxWidth_147(float value)
	{
		___m_maxWidth_147 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_148() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxHeight_148)); }
	inline float get_m_maxHeight_148() const { return ___m_maxHeight_148; }
	inline float* get_address_of_m_maxHeight_148() { return &___m_maxHeight_148; }
	inline void set_m_maxHeight_148(float value)
	{
		___m_maxHeight_148 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_149() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_LayoutElement_149)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_m_LayoutElement_149() const { return ___m_LayoutElement_149; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_m_LayoutElement_149() { return &___m_LayoutElement_149; }
	inline void set_m_LayoutElement_149(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___m_LayoutElement_149 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_149), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_150() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredWidth_150)); }
	inline float get_m_preferredWidth_150() const { return ___m_preferredWidth_150; }
	inline float* get_address_of_m_preferredWidth_150() { return &___m_preferredWidth_150; }
	inline void set_m_preferredWidth_150(float value)
	{
		___m_preferredWidth_150 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_151() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedWidth_151)); }
	inline float get_m_renderedWidth_151() const { return ___m_renderedWidth_151; }
	inline float* get_address_of_m_renderedWidth_151() { return &___m_renderedWidth_151; }
	inline void set_m_renderedWidth_151(float value)
	{
		___m_renderedWidth_151 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_152() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredWidthDirty_152)); }
	inline bool get_m_isPreferredWidthDirty_152() const { return ___m_isPreferredWidthDirty_152; }
	inline bool* get_address_of_m_isPreferredWidthDirty_152() { return &___m_isPreferredWidthDirty_152; }
	inline void set_m_isPreferredWidthDirty_152(bool value)
	{
		___m_isPreferredWidthDirty_152 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_153() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredHeight_153)); }
	inline float get_m_preferredHeight_153() const { return ___m_preferredHeight_153; }
	inline float* get_address_of_m_preferredHeight_153() { return &___m_preferredHeight_153; }
	inline void set_m_preferredHeight_153(float value)
	{
		___m_preferredHeight_153 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_154() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedHeight_154)); }
	inline float get_m_renderedHeight_154() const { return ___m_renderedHeight_154; }
	inline float* get_address_of_m_renderedHeight_154() { return &___m_renderedHeight_154; }
	inline void set_m_renderedHeight_154(float value)
	{
		___m_renderedHeight_154 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_155() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredHeightDirty_155)); }
	inline bool get_m_isPreferredHeightDirty_155() const { return ___m_isPreferredHeightDirty_155; }
	inline bool* get_address_of_m_isPreferredHeightDirty_155() { return &___m_isPreferredHeightDirty_155; }
	inline void set_m_isPreferredHeightDirty_155(bool value)
	{
		___m_isPreferredHeightDirty_155 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_156() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculatingPreferredValues_156)); }
	inline bool get_m_isCalculatingPreferredValues_156() const { return ___m_isCalculatingPreferredValues_156; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_156() { return &___m_isCalculatingPreferredValues_156; }
	inline void set_m_isCalculatingPreferredValues_156(bool value)
	{
		___m_isCalculatingPreferredValues_156 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_157() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_recursiveCount_157)); }
	inline int32_t get_m_recursiveCount_157() const { return ___m_recursiveCount_157; }
	inline int32_t* get_address_of_m_recursiveCount_157() { return &___m_recursiveCount_157; }
	inline void set_m_recursiveCount_157(int32_t value)
	{
		___m_recursiveCount_157 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_158() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutPriority_158)); }
	inline int32_t get_m_layoutPriority_158() const { return ___m_layoutPriority_158; }
	inline int32_t* get_address_of_m_layoutPriority_158() { return &___m_layoutPriority_158; }
	inline void set_m_layoutPriority_158(int32_t value)
	{
		___m_layoutPriority_158 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_159() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculateSizeRequired_159)); }
	inline bool get_m_isCalculateSizeRequired_159() const { return ___m_isCalculateSizeRequired_159; }
	inline bool* get_address_of_m_isCalculateSizeRequired_159() { return &___m_isCalculateSizeRequired_159; }
	inline void set_m_isCalculateSizeRequired_159(bool value)
	{
		___m_isCalculateSizeRequired_159 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_160() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLayoutDirty_160)); }
	inline bool get_m_isLayoutDirty_160() const { return ___m_isLayoutDirty_160; }
	inline bool* get_address_of_m_isLayoutDirty_160() { return &___m_isLayoutDirty_160; }
	inline void set_m_isLayoutDirty_160(bool value)
	{
		___m_isLayoutDirty_160 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticesAlreadyDirty_161)); }
	inline bool get_m_verticesAlreadyDirty_161() const { return ___m_verticesAlreadyDirty_161; }
	inline bool* get_address_of_m_verticesAlreadyDirty_161() { return &___m_verticesAlreadyDirty_161; }
	inline void set_m_verticesAlreadyDirty_161(bool value)
	{
		___m_verticesAlreadyDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_162() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutAlreadyDirty_162)); }
	inline bool get_m_layoutAlreadyDirty_162() const { return ___m_layoutAlreadyDirty_162; }
	inline bool* get_address_of_m_layoutAlreadyDirty_162() { return &___m_layoutAlreadyDirty_162; }
	inline void set_m_layoutAlreadyDirty_162(bool value)
	{
		___m_layoutAlreadyDirty_162 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_163() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isAwake_163)); }
	inline bool get_m_isAwake_163() const { return ___m_isAwake_163; }
	inline bool* get_address_of_m_isAwake_163() { return &___m_isAwake_163; }
	inline void set_m_isAwake_163(bool value)
	{
		___m_isAwake_163 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_164() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isWaitingOnResourceLoad_164)); }
	inline bool get_m_isWaitingOnResourceLoad_164() const { return ___m_isWaitingOnResourceLoad_164; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_164() { return &___m_isWaitingOnResourceLoad_164; }
	inline void set_m_isWaitingOnResourceLoad_164(bool value)
	{
		___m_isWaitingOnResourceLoad_164 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_165() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isInputParsingRequired_165)); }
	inline bool get_m_isInputParsingRequired_165() const { return ___m_isInputParsingRequired_165; }
	inline bool* get_address_of_m_isInputParsingRequired_165() { return &___m_isInputParsingRequired_165; }
	inline void set_m_isInputParsingRequired_165(bool value)
	{
		___m_isInputParsingRequired_165 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_166() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_inputSource_166)); }
	inline int32_t get_m_inputSource_166() const { return ___m_inputSource_166; }
	inline int32_t* get_address_of_m_inputSource_166() { return &___m_inputSource_166; }
	inline void set_m_inputSource_166(int32_t value)
	{
		___m_inputSource_166 = value;
	}

	inline static int32_t get_offset_of_old_text_167() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___old_text_167)); }
	inline String_t* get_old_text_167() const { return ___old_text_167; }
	inline String_t** get_address_of_old_text_167() { return &___old_text_167; }
	inline void set_old_text_167(String_t* value)
	{
		___old_text_167 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_167), value);
	}

	inline static int32_t get_offset_of_m_fontScale_168() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScale_168)); }
	inline float get_m_fontScale_168() const { return ___m_fontScale_168; }
	inline float* get_address_of_m_fontScale_168() { return &___m_fontScale_168; }
	inline void set_m_fontScale_168(float value)
	{
		___m_fontScale_168 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_169() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScaleMultiplier_169)); }
	inline float get_m_fontScaleMultiplier_169() const { return ___m_fontScaleMultiplier_169; }
	inline float* get_address_of_m_fontScaleMultiplier_169() { return &___m_fontScaleMultiplier_169; }
	inline void set_m_fontScaleMultiplier_169(float value)
	{
		___m_fontScaleMultiplier_169 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_170() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlTag_170)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_htmlTag_170() const { return ___m_htmlTag_170; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_htmlTag_170() { return &___m_htmlTag_170; }
	inline void set_m_htmlTag_170(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_htmlTag_170 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_170), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_171() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xmlAttribute_171)); }
	inline XML_TagAttributeU5BU5D_tFE12AC6A01EC7B573E971252DAB696F355F76B6B* get_m_xmlAttribute_171() const { return ___m_xmlAttribute_171; }
	inline XML_TagAttributeU5BU5D_tFE12AC6A01EC7B573E971252DAB696F355F76B6B** get_address_of_m_xmlAttribute_171() { return &___m_xmlAttribute_171; }
	inline void set_m_xmlAttribute_171(XML_TagAttributeU5BU5D_tFE12AC6A01EC7B573E971252DAB696F355F76B6B* value)
	{
		___m_xmlAttribute_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_171), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_172() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_attributeParameterValues_172)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_attributeParameterValues_172() const { return ___m_attributeParameterValues_172; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_attributeParameterValues_172() { return &___m_attributeParameterValues_172; }
	inline void set_m_attributeParameterValues_172(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_attributeParameterValues_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_172), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_173() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_LineIndent_173)); }
	inline float get_tag_LineIndent_173() const { return ___tag_LineIndent_173; }
	inline float* get_address_of_tag_LineIndent_173() { return &___tag_LineIndent_173; }
	inline void set_tag_LineIndent_173(float value)
	{
		___tag_LineIndent_173 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_174() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_Indent_174)); }
	inline float get_tag_Indent_174() const { return ___tag_Indent_174; }
	inline float* get_address_of_tag_Indent_174() { return &___tag_Indent_174; }
	inline void set_tag_Indent_174(float value)
	{
		___tag_Indent_174 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_175() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_indentStack_175)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_m_indentStack_175() const { return ___m_indentStack_175; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_m_indentStack_175() { return &___m_indentStack_175; }
	inline void set_m_indentStack_175(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___m_indentStack_175 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_176() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_NoParsing_176)); }
	inline bool get_tag_NoParsing_176() const { return ___tag_NoParsing_176; }
	inline bool* get_address_of_tag_NoParsing_176() { return &___tag_NoParsing_176; }
	inline void set_tag_NoParsing_176(bool value)
	{
		___tag_NoParsing_176 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_177() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isParsingText_177)); }
	inline bool get_m_isParsingText_177() const { return ___m_isParsingText_177; }
	inline bool* get_address_of_m_isParsingText_177() { return &___m_isParsingText_177; }
	inline void set_m_isParsingText_177(bool value)
	{
		___m_isParsingText_177 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_178() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FXMatrix_178)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_FXMatrix_178() const { return ___m_FXMatrix_178; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_FXMatrix_178() { return &___m_FXMatrix_178; }
	inline void set_m_FXMatrix_178(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_FXMatrix_178 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_179() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isFXMatrixSet_179)); }
	inline bool get_m_isFXMatrixSet_179() const { return ___m_isFXMatrixSet_179; }
	inline bool* get_address_of_m_isFXMatrixSet_179() { return &___m_isFXMatrixSet_179; }
	inline void set_m_isFXMatrixSet_179(bool value)
	{
		___m_isFXMatrixSet_179 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_180() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_char_buffer_180)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_char_buffer_180() const { return ___m_char_buffer_180; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_char_buffer_180() { return &___m_char_buffer_180; }
	inline void set_m_char_buffer_180(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_char_buffer_180 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_180), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_181() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_internalCharacterInfo_181)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_m_internalCharacterInfo_181() const { return ___m_internalCharacterInfo_181; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_m_internalCharacterInfo_181() { return &___m_internalCharacterInfo_181; }
	inline void set_m_internalCharacterInfo_181(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___m_internalCharacterInfo_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_181), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_182() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_input_CharArray_182)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_input_CharArray_182() const { return ___m_input_CharArray_182; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_input_CharArray_182() { return &___m_input_CharArray_182; }
	inline void set_m_input_CharArray_182(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_input_CharArray_182 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_182), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_183() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charArray_Length_183)); }
	inline int32_t get_m_charArray_Length_183() const { return ___m_charArray_Length_183; }
	inline int32_t* get_address_of_m_charArray_Length_183() { return &___m_charArray_Length_183; }
	inline void set_m_charArray_Length_183(int32_t value)
	{
		___m_charArray_Length_183 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_184() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_totalCharacterCount_184)); }
	inline int32_t get_m_totalCharacterCount_184() const { return ___m_totalCharacterCount_184; }
	inline int32_t* get_address_of_m_totalCharacterCount_184() { return &___m_totalCharacterCount_184; }
	inline void set_m_totalCharacterCount_184(int32_t value)
	{
		___m_totalCharacterCount_184 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_185() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedWordWrapState_185)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedWordWrapState_185() const { return ___m_SavedWordWrapState_185; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedWordWrapState_185() { return &___m_SavedWordWrapState_185; }
	inline void set_m_SavedWordWrapState_185(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedWordWrapState_185 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_186() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedLineState_186)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedLineState_186() const { return ___m_SavedLineState_186; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedLineState_186() { return &___m_SavedLineState_186; }
	inline void set_m_SavedLineState_186(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedLineState_186 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_187() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterCount_187)); }
	inline int32_t get_m_characterCount_187() const { return ___m_characterCount_187; }
	inline int32_t* get_address_of_m_characterCount_187() { return &___m_characterCount_187; }
	inline void set_m_characterCount_187(int32_t value)
	{
		___m_characterCount_187 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_188() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstCharacterOfLine_188)); }
	inline int32_t get_m_firstCharacterOfLine_188() const { return ___m_firstCharacterOfLine_188; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_188() { return &___m_firstCharacterOfLine_188; }
	inline void set_m_firstCharacterOfLine_188(int32_t value)
	{
		___m_firstCharacterOfLine_188 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacterOfLine_189)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_189() const { return ___m_firstVisibleCharacterOfLine_189; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_189() { return &___m_firstVisibleCharacterOfLine_189; }
	inline void set_m_firstVisibleCharacterOfLine_189(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastCharacterOfLine_190)); }
	inline int32_t get_m_lastCharacterOfLine_190() const { return ___m_lastCharacterOfLine_190; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_190() { return &___m_lastCharacterOfLine_190; }
	inline void set_m_lastCharacterOfLine_190(int32_t value)
	{
		___m_lastCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_191() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastVisibleCharacterOfLine_191)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_191() const { return ___m_lastVisibleCharacterOfLine_191; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_191() { return &___m_lastVisibleCharacterOfLine_191; }
	inline void set_m_lastVisibleCharacterOfLine_191(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_191 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_192() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineNumber_192)); }
	inline int32_t get_m_lineNumber_192() const { return ___m_lineNumber_192; }
	inline int32_t* get_address_of_m_lineNumber_192() { return &___m_lineNumber_192; }
	inline void set_m_lineNumber_192(int32_t value)
	{
		___m_lineNumber_192 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_193() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineVisibleCharacterCount_193)); }
	inline int32_t get_m_lineVisibleCharacterCount_193() const { return ___m_lineVisibleCharacterCount_193; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_193() { return &___m_lineVisibleCharacterCount_193; }
	inline void set_m_lineVisibleCharacterCount_193(int32_t value)
	{
		___m_lineVisibleCharacterCount_193 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_194() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageNumber_194)); }
	inline int32_t get_m_pageNumber_194() const { return ___m_pageNumber_194; }
	inline int32_t* get_address_of_m_pageNumber_194() { return &___m_pageNumber_194; }
	inline void set_m_pageNumber_194(int32_t value)
	{
		___m_pageNumber_194 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_195() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxAscender_195)); }
	inline float get_m_maxAscender_195() const { return ___m_maxAscender_195; }
	inline float* get_address_of_m_maxAscender_195() { return &___m_maxAscender_195; }
	inline void set_m_maxAscender_195(float value)
	{
		___m_maxAscender_195 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_196() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxCapHeight_196)); }
	inline float get_m_maxCapHeight_196() const { return ___m_maxCapHeight_196; }
	inline float* get_address_of_m_maxCapHeight_196() { return &___m_maxCapHeight_196; }
	inline void set_m_maxCapHeight_196(float value)
	{
		___m_maxCapHeight_196 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_197() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxDescender_197)); }
	inline float get_m_maxDescender_197() const { return ___m_maxDescender_197; }
	inline float* get_address_of_m_maxDescender_197() { return &___m_maxDescender_197; }
	inline void set_m_maxDescender_197(float value)
	{
		___m_maxDescender_197 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineAscender_198)); }
	inline float get_m_maxLineAscender_198() const { return ___m_maxLineAscender_198; }
	inline float* get_address_of_m_maxLineAscender_198() { return &___m_maxLineAscender_198; }
	inline void set_m_maxLineAscender_198(float value)
	{
		___m_maxLineAscender_198 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineDescender_199)); }
	inline float get_m_maxLineDescender_199() const { return ___m_maxLineDescender_199; }
	inline float* get_address_of_m_maxLineDescender_199() { return &___m_maxLineDescender_199; }
	inline void set_m_maxLineDescender_199(float value)
	{
		___m_maxLineDescender_199 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_200() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_startOfLineAscender_200)); }
	inline float get_m_startOfLineAscender_200() const { return ___m_startOfLineAscender_200; }
	inline float* get_address_of_m_startOfLineAscender_200() { return &___m_startOfLineAscender_200; }
	inline void set_m_startOfLineAscender_200(float value)
	{
		___m_startOfLineAscender_200 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_201() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineOffset_201)); }
	inline float get_m_lineOffset_201() const { return ___m_lineOffset_201; }
	inline float* get_address_of_m_lineOffset_201() { return &___m_lineOffset_201; }
	inline void set_m_lineOffset_201(float value)
	{
		___m_lineOffset_201 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_202() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_meshExtents_202)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_m_meshExtents_202() const { return ___m_meshExtents_202; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_m_meshExtents_202() { return &___m_meshExtents_202; }
	inline void set_m_meshExtents_202(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___m_meshExtents_202 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_203() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlColor_203)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_htmlColor_203() const { return ___m_htmlColor_203; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_htmlColor_203() { return &___m_htmlColor_203; }
	inline void set_m_htmlColor_203(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_htmlColor_203 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_204() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorStack_204)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_m_colorStack_204() const { return ___m_colorStack_204; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_m_colorStack_204() { return &___m_colorStack_204; }
	inline void set_m_colorStack_204(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___m_colorStack_204 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColorStack_205)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_m_underlineColorStack_205() const { return ___m_underlineColorStack_205; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_m_underlineColorStack_205() { return &___m_underlineColorStack_205; }
	inline void set_m_underlineColorStack_205(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___m_underlineColorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColorStack_206)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_m_strikethroughColorStack_206() const { return ___m_strikethroughColorStack_206; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_m_strikethroughColorStack_206() { return &___m_strikethroughColorStack_206; }
	inline void set_m_strikethroughColorStack_206(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___m_strikethroughColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_207() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColorStack_207)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_m_highlightColorStack_207() const { return ___m_highlightColorStack_207; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_m_highlightColorStack_207() { return &___m_highlightColorStack_207; }
	inline void set_m_highlightColorStack_207(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___m_highlightColorStack_207 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_208() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientPreset_208)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_colorGradientPreset_208() const { return ___m_colorGradientPreset_208; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_colorGradientPreset_208() { return &___m_colorGradientPreset_208; }
	inline void set_m_colorGradientPreset_208(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_colorGradientPreset_208 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_208), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_209() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientStack_209)); }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  get_m_colorGradientStack_209() const { return ___m_colorGradientStack_209; }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 * get_address_of_m_colorGradientStack_209() { return &___m_colorGradientStack_209; }
	inline void set_m_colorGradientStack_209(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  value)
	{
		___m_colorGradientStack_209 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_210() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tabSpacing_210)); }
	inline float get_m_tabSpacing_210() const { return ___m_tabSpacing_210; }
	inline float* get_address_of_m_tabSpacing_210() { return &___m_tabSpacing_210; }
	inline void set_m_tabSpacing_210(float value)
	{
		___m_tabSpacing_210 = value;
	}

	inline static int32_t get_offset_of_m_spacing_211() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spacing_211)); }
	inline float get_m_spacing_211() const { return ___m_spacing_211; }
	inline float* get_address_of_m_spacing_211() { return &___m_spacing_211; }
	inline void set_m_spacing_211(float value)
	{
		___m_spacing_211 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_styleStack_212)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_m_styleStack_212() const { return ___m_styleStack_212; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_m_styleStack_212() { return &___m_styleStack_212; }
	inline void set_m_styleStack_212(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___m_styleStack_212 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_213() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_actionStack_213)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_m_actionStack_213() const { return ___m_actionStack_213; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_m_actionStack_213() { return &___m_actionStack_213; }
	inline void set_m_actionStack_213(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___m_actionStack_213 = value;
	}

	inline static int32_t get_offset_of_m_padding_214() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_padding_214)); }
	inline float get_m_padding_214() const { return ___m_padding_214; }
	inline float* get_address_of_m_padding_214() { return &___m_padding_214; }
	inline void set_m_padding_214(float value)
	{
		___m_padding_214 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_215() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffset_215)); }
	inline float get_m_baselineOffset_215() const { return ___m_baselineOffset_215; }
	inline float* get_address_of_m_baselineOffset_215() { return &___m_baselineOffset_215; }
	inline void set_m_baselineOffset_215(float value)
	{
		___m_baselineOffset_215 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_216() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffsetStack_216)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_m_baselineOffsetStack_216() const { return ___m_baselineOffsetStack_216; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_m_baselineOffsetStack_216() { return &___m_baselineOffsetStack_216; }
	inline void set_m_baselineOffsetStack_216(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___m_baselineOffsetStack_216 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_217() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xAdvance_217)); }
	inline float get_m_xAdvance_217() const { return ___m_xAdvance_217; }
	inline float* get_address_of_m_xAdvance_217() { return &___m_xAdvance_217; }
	inline void set_m_xAdvance_217(float value)
	{
		___m_xAdvance_217 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_218() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textElementType_218)); }
	inline int32_t get_m_textElementType_218() const { return ___m_textElementType_218; }
	inline int32_t* get_address_of_m_textElementType_218() { return &___m_textElementType_218; }
	inline void set_m_textElementType_218(int32_t value)
	{
		___m_textElementType_218 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_219() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_TextElement_219)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_m_cached_TextElement_219() const { return ___m_cached_TextElement_219; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_m_cached_TextElement_219() { return &___m_cached_TextElement_219; }
	inline void set_m_cached_TextElement_219(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___m_cached_TextElement_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_219), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_220() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Underline_GlyphInfo_220)); }
	inline TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C * get_m_cached_Underline_GlyphInfo_220() const { return ___m_cached_Underline_GlyphInfo_220; }
	inline TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C ** get_address_of_m_cached_Underline_GlyphInfo_220() { return &___m_cached_Underline_GlyphInfo_220; }
	inline void set_m_cached_Underline_GlyphInfo_220(TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C * value)
	{
		___m_cached_Underline_GlyphInfo_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_220), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_221() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Ellipsis_GlyphInfo_221)); }
	inline TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C * get_m_cached_Ellipsis_GlyphInfo_221() const { return ___m_cached_Ellipsis_GlyphInfo_221; }
	inline TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C ** get_address_of_m_cached_Ellipsis_GlyphInfo_221() { return &___m_cached_Ellipsis_GlyphInfo_221; }
	inline void set_m_cached_Ellipsis_GlyphInfo_221(TMP_Glyph_tCAA5E0C262A3DAF50537C03D9EA90B51B05BA62C * value)
	{
		___m_cached_Ellipsis_GlyphInfo_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_221), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_222() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_defaultSpriteAsset_222)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_defaultSpriteAsset_222() const { return ___m_defaultSpriteAsset_222; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_defaultSpriteAsset_222() { return &___m_defaultSpriteAsset_222; }
	inline void set_m_defaultSpriteAsset_222(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_defaultSpriteAsset_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_222), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_223() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentSpriteAsset_223)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_currentSpriteAsset_223() const { return ___m_currentSpriteAsset_223; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_currentSpriteAsset_223() { return &___m_currentSpriteAsset_223; }
	inline void set_m_currentSpriteAsset_223(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_currentSpriteAsset_223 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_223), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_224() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteCount_224)); }
	inline int32_t get_m_spriteCount_224() const { return ___m_spriteCount_224; }
	inline int32_t* get_address_of_m_spriteCount_224() { return &___m_spriteCount_224; }
	inline void set_m_spriteCount_224(int32_t value)
	{
		___m_spriteCount_224 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_225() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteIndex_225)); }
	inline int32_t get_m_spriteIndex_225() const { return ___m_spriteIndex_225; }
	inline int32_t* get_address_of_m_spriteIndex_225() { return &___m_spriteIndex_225; }
	inline void set_m_spriteIndex_225(int32_t value)
	{
		___m_spriteIndex_225 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_226() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimationID_226)); }
	inline int32_t get_m_spriteAnimationID_226() const { return ___m_spriteAnimationID_226; }
	inline int32_t* get_address_of_m_spriteAnimationID_226() { return &___m_spriteAnimationID_226; }
	inline void set_m_spriteAnimationID_226(int32_t value)
	{
		___m_spriteAnimationID_226 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_227() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreActiveState_227)); }
	inline bool get_m_ignoreActiveState_227() const { return ___m_ignoreActiveState_227; }
	inline bool* get_address_of_m_ignoreActiveState_227() { return &___m_ignoreActiveState_227; }
	inline void set_m_ignoreActiveState_227(bool value)
	{
		___m_ignoreActiveState_227 = value;
	}

	inline static int32_t get_offset_of_k_Power_228() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___k_Power_228)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_k_Power_228() const { return ___k_Power_228; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_k_Power_228() { return &___k_Power_228; }
	inline void set_k_Power_228(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___k_Power_228 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_228), value);
	}
};

struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargePositiveVector2_229;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargeNegativeVector2_230;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_231;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_232;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_233;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_234;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_229() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveVector2_229)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargePositiveVector2_229() const { return ___k_LargePositiveVector2_229; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargePositiveVector2_229() { return &___k_LargePositiveVector2_229; }
	inline void set_k_LargePositiveVector2_229(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargePositiveVector2_229 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_230() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeVector2_230)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargeNegativeVector2_230() const { return ___k_LargeNegativeVector2_230; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargeNegativeVector2_230() { return &___k_LargeNegativeVector2_230; }
	inline void set_k_LargeNegativeVector2_230(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargeNegativeVector2_230 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_231() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveFloat_231)); }
	inline float get_k_LargePositiveFloat_231() const { return ___k_LargePositiveFloat_231; }
	inline float* get_address_of_k_LargePositiveFloat_231() { return &___k_LargePositiveFloat_231; }
	inline void set_k_LargePositiveFloat_231(float value)
	{
		___k_LargePositiveFloat_231 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_232() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeFloat_232)); }
	inline float get_k_LargeNegativeFloat_232() const { return ___k_LargeNegativeFloat_232; }
	inline float* get_address_of_k_LargeNegativeFloat_232() { return &___k_LargeNegativeFloat_232; }
	inline void set_k_LargeNegativeFloat_232(float value)
	{
		___k_LargeNegativeFloat_232 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_233() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveInt_233)); }
	inline int32_t get_k_LargePositiveInt_233() const { return ___k_LargePositiveInt_233; }
	inline int32_t* get_address_of_k_LargePositiveInt_233() { return &___k_LargePositiveInt_233; }
	inline void set_k_LargePositiveInt_233(int32_t value)
	{
		___k_LargePositiveInt_233 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_234() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeInt_234)); }
	inline int32_t get_k_LargeNegativeInt_234() const { return ___k_LargeNegativeInt_234; }
	inline int32_t* get_address_of_k_LargeNegativeInt_234() { return &___k_LargeNegativeInt_234; }
	inline void set_k_LargeNegativeInt_234(int32_t value)
	{
		___k_LargeNegativeInt_234 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifndef TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#define TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshPro
struct  TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2  : public TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7
{
public:
	// System.Boolean TMPro.TextMeshPro::m_currentAutoSizeMode
	bool ___m_currentAutoSizeMode_235;
	// System.Boolean TMPro.TextMeshPro::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_236;
	// System.Single TMPro.TextMeshPro::m_previousLossyScaleY
	float ___m_previousLossyScaleY_237;
	// UnityEngine.Renderer TMPro.TextMeshPro::m_renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_renderer_238;
	// UnityEngine.MeshFilter TMPro.TextMeshPro::m_meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___m_meshFilter_239;
	// System.Boolean TMPro.TextMeshPro::m_isFirstAllocation
	bool ___m_isFirstAllocation_240;
	// System.Int32 TMPro.TextMeshPro::m_max_characters
	int32_t ___m_max_characters_241;
	// System.Int32 TMPro.TextMeshPro::m_max_numberOfLines
	int32_t ___m_max_numberOfLines_242;
	// UnityEngine.Bounds TMPro.TextMeshPro::m_default_bounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___m_default_bounds_243;
	// TMPro.TMP_SubMesh[] TMPro.TextMeshPro::m_subTextObjects
	TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* ___m_subTextObjects_244;
	// System.Boolean TMPro.TextMeshPro::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_245;
	// System.Boolean TMPro.TextMeshPro::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_246;
	// TMPro.MaskingTypes TMPro.TextMeshPro::m_maskType
	int32_t ___m_maskType_247;
	// UnityEngine.Matrix4x4 TMPro.TextMeshPro::m_EnvMapMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_EnvMapMatrix_248;
	// UnityEngine.Vector3[] TMPro.TextMeshPro::m_RectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_RectTransformCorners_249;
	// System.Boolean TMPro.TextMeshPro::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_250;
	// System.Int32 TMPro.TextMeshPro::loopCountA
	int32_t ___loopCountA_251;

public:
	inline static int32_t get_offset_of_m_currentAutoSizeMode_235() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_currentAutoSizeMode_235)); }
	inline bool get_m_currentAutoSizeMode_235() const { return ___m_currentAutoSizeMode_235; }
	inline bool* get_address_of_m_currentAutoSizeMode_235() { return &___m_currentAutoSizeMode_235; }
	inline void set_m_currentAutoSizeMode_235(bool value)
	{
		___m_currentAutoSizeMode_235 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_236() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_hasFontAssetChanged_236)); }
	inline bool get_m_hasFontAssetChanged_236() const { return ___m_hasFontAssetChanged_236; }
	inline bool* get_address_of_m_hasFontAssetChanged_236() { return &___m_hasFontAssetChanged_236; }
	inline void set_m_hasFontAssetChanged_236(bool value)
	{
		___m_hasFontAssetChanged_236 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_237() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_previousLossyScaleY_237)); }
	inline float get_m_previousLossyScaleY_237() const { return ___m_previousLossyScaleY_237; }
	inline float* get_address_of_m_previousLossyScaleY_237() { return &___m_previousLossyScaleY_237; }
	inline void set_m_previousLossyScaleY_237(float value)
	{
		___m_previousLossyScaleY_237 = value;
	}

	inline static int32_t get_offset_of_m_renderer_238() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_renderer_238)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_renderer_238() const { return ___m_renderer_238; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_renderer_238() { return &___m_renderer_238; }
	inline void set_m_renderer_238(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_renderer_238 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_238), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_239() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_meshFilter_239)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_m_meshFilter_239() const { return ___m_meshFilter_239; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_m_meshFilter_239() { return &___m_meshFilter_239; }
	inline void set_m_meshFilter_239(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___m_meshFilter_239 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_239), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_240() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isFirstAllocation_240)); }
	inline bool get_m_isFirstAllocation_240() const { return ___m_isFirstAllocation_240; }
	inline bool* get_address_of_m_isFirstAllocation_240() { return &___m_isFirstAllocation_240; }
	inline void set_m_isFirstAllocation_240(bool value)
	{
		___m_isFirstAllocation_240 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_241() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_max_characters_241)); }
	inline int32_t get_m_max_characters_241() const { return ___m_max_characters_241; }
	inline int32_t* get_address_of_m_max_characters_241() { return &___m_max_characters_241; }
	inline void set_m_max_characters_241(int32_t value)
	{
		___m_max_characters_241 = value;
	}

	inline static int32_t get_offset_of_m_max_numberOfLines_242() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_max_numberOfLines_242)); }
	inline int32_t get_m_max_numberOfLines_242() const { return ___m_max_numberOfLines_242; }
	inline int32_t* get_address_of_m_max_numberOfLines_242() { return &___m_max_numberOfLines_242; }
	inline void set_m_max_numberOfLines_242(int32_t value)
	{
		___m_max_numberOfLines_242 = value;
	}

	inline static int32_t get_offset_of_m_default_bounds_243() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_default_bounds_243)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_m_default_bounds_243() const { return ___m_default_bounds_243; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_m_default_bounds_243() { return &___m_default_bounds_243; }
	inline void set_m_default_bounds_243(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___m_default_bounds_243 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_244() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_subTextObjects_244)); }
	inline TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* get_m_subTextObjects_244() const { return ___m_subTextObjects_244; }
	inline TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD** get_address_of_m_subTextObjects_244() { return &___m_subTextObjects_244; }
	inline void set_m_subTextObjects_244(TMP_SubMeshU5BU5D_t1847E144072AA6E3FEB91A5E855C564CE48448FD* value)
	{
		___m_subTextObjects_244 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_244), value);
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_245() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isMaskingEnabled_245)); }
	inline bool get_m_isMaskingEnabled_245() const { return ___m_isMaskingEnabled_245; }
	inline bool* get_address_of_m_isMaskingEnabled_245() { return &___m_isMaskingEnabled_245; }
	inline void set_m_isMaskingEnabled_245(bool value)
	{
		___m_isMaskingEnabled_245 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_246() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___isMaskUpdateRequired_246)); }
	inline bool get_isMaskUpdateRequired_246() const { return ___isMaskUpdateRequired_246; }
	inline bool* get_address_of_isMaskUpdateRequired_246() { return &___isMaskUpdateRequired_246; }
	inline void set_isMaskUpdateRequired_246(bool value)
	{
		___isMaskUpdateRequired_246 = value;
	}

	inline static int32_t get_offset_of_m_maskType_247() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_maskType_247)); }
	inline int32_t get_m_maskType_247() const { return ___m_maskType_247; }
	inline int32_t* get_address_of_m_maskType_247() { return &___m_maskType_247; }
	inline void set_m_maskType_247(int32_t value)
	{
		___m_maskType_247 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_248() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_EnvMapMatrix_248)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_EnvMapMatrix_248() const { return ___m_EnvMapMatrix_248; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_EnvMapMatrix_248() { return &___m_EnvMapMatrix_248; }
	inline void set_m_EnvMapMatrix_248(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_EnvMapMatrix_248 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_249() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_RectTransformCorners_249)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_RectTransformCorners_249() const { return ___m_RectTransformCorners_249; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_RectTransformCorners_249() { return &___m_RectTransformCorners_249; }
	inline void set_m_RectTransformCorners_249(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_RectTransformCorners_249 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_249), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_250() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___m_isRegisteredForEvents_250)); }
	inline bool get_m_isRegisteredForEvents_250() const { return ___m_isRegisteredForEvents_250; }
	inline bool* get_address_of_m_isRegisteredForEvents_250() { return &___m_isRegisteredForEvents_250; }
	inline void set_m_isRegisteredForEvents_250(bool value)
	{
		___m_isRegisteredForEvents_250 = value;
	}

	inline static int32_t get_offset_of_loopCountA_251() { return static_cast<int32_t>(offsetof(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2, ___loopCountA_251)); }
	inline int32_t get_loopCountA_251() const { return ___loopCountA_251; }
	inline int32_t* get_address_of_loopCountA_251() { return &___loopCountA_251; }
	inline void set_loopCountA_251(int32_t value)
	{
		___loopCountA_251 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPRO_T6FF60D9DCAF295045FE47C014CC855C5784752E2_H
#ifndef TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#define TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438  : public TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_235;
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_236;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* ___m_subTextObjects_237;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_238;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_RectTransformCorners_239;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_canvasRenderer_240;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_canvas_241;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_242;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_243;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_244;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_baseMaterial_245;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_246;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_247;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_maskOffset_248;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_EnvMapMatrix_249;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_250;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_251;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_252;

public:
	inline static int32_t get_offset_of_m_isRebuildingLayout_235() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isRebuildingLayout_235)); }
	inline bool get_m_isRebuildingLayout_235() const { return ___m_isRebuildingLayout_235; }
	inline bool* get_address_of_m_isRebuildingLayout_235() { return &___m_isRebuildingLayout_235; }
	inline void set_m_isRebuildingLayout_235(bool value)
	{
		___m_isRebuildingLayout_235 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_236() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_hasFontAssetChanged_236)); }
	inline bool get_m_hasFontAssetChanged_236() const { return ___m_hasFontAssetChanged_236; }
	inline bool* get_address_of_m_hasFontAssetChanged_236() { return &___m_hasFontAssetChanged_236; }
	inline void set_m_hasFontAssetChanged_236(bool value)
	{
		___m_hasFontAssetChanged_236 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_237() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_subTextObjects_237)); }
	inline TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* get_m_subTextObjects_237() const { return ___m_subTextObjects_237; }
	inline TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E** get_address_of_m_subTextObjects_237() { return &___m_subTextObjects_237; }
	inline void set_m_subTextObjects_237(TMP_SubMeshUIU5BU5D_tB20103A3891C74028E821AA6857CD89D59C9A87E* value)
	{
		___m_subTextObjects_237 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_237), value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_238() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_previousLossyScaleY_238)); }
	inline float get_m_previousLossyScaleY_238() const { return ___m_previousLossyScaleY_238; }
	inline float* get_address_of_m_previousLossyScaleY_238() { return &___m_previousLossyScaleY_238; }
	inline void set_m_previousLossyScaleY_238(float value)
	{
		___m_previousLossyScaleY_238 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_239() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_RectTransformCorners_239)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_RectTransformCorners_239() const { return ___m_RectTransformCorners_239; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_RectTransformCorners_239() { return &___m_RectTransformCorners_239; }
	inline void set_m_RectTransformCorners_239(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_RectTransformCorners_239 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_239), value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_240() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_canvasRenderer_240)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_canvasRenderer_240() const { return ___m_canvasRenderer_240; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_canvasRenderer_240() { return &___m_canvasRenderer_240; }
	inline void set_m_canvasRenderer_240(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_canvasRenderer_240 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_240), value);
	}

	inline static int32_t get_offset_of_m_canvas_241() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_canvas_241)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_canvas_241() const { return ___m_canvas_241; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_canvas_241() { return &___m_canvas_241; }
	inline void set_m_canvas_241(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_canvas_241 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_241), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_242() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isFirstAllocation_242)); }
	inline bool get_m_isFirstAllocation_242() const { return ___m_isFirstAllocation_242; }
	inline bool* get_address_of_m_isFirstAllocation_242() { return &___m_isFirstAllocation_242; }
	inline void set_m_isFirstAllocation_242(bool value)
	{
		___m_isFirstAllocation_242 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_243() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_max_characters_243)); }
	inline int32_t get_m_max_characters_243() const { return ___m_max_characters_243; }
	inline int32_t* get_address_of_m_max_characters_243() { return &___m_max_characters_243; }
	inline void set_m_max_characters_243(int32_t value)
	{
		___m_max_characters_243 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_244() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isMaskingEnabled_244)); }
	inline bool get_m_isMaskingEnabled_244() const { return ___m_isMaskingEnabled_244; }
	inline bool* get_address_of_m_isMaskingEnabled_244() { return &___m_isMaskingEnabled_244; }
	inline void set_m_isMaskingEnabled_244(bool value)
	{
		___m_isMaskingEnabled_244 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_245() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_baseMaterial_245)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_baseMaterial_245() const { return ___m_baseMaterial_245; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_baseMaterial_245() { return &___m_baseMaterial_245; }
	inline void set_m_baseMaterial_245(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_baseMaterial_245 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseMaterial_245), value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_246() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isScrollRegionSet_246)); }
	inline bool get_m_isScrollRegionSet_246() const { return ___m_isScrollRegionSet_246; }
	inline bool* get_address_of_m_isScrollRegionSet_246() { return &___m_isScrollRegionSet_246; }
	inline void set_m_isScrollRegionSet_246(bool value)
	{
		___m_isScrollRegionSet_246 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_247() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_stencilID_247)); }
	inline int32_t get_m_stencilID_247() const { return ___m_stencilID_247; }
	inline int32_t* get_address_of_m_stencilID_247() { return &___m_stencilID_247; }
	inline void set_m_stencilID_247(int32_t value)
	{
		___m_stencilID_247 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_248() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_maskOffset_248)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_maskOffset_248() const { return ___m_maskOffset_248; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_maskOffset_248() { return &___m_maskOffset_248; }
	inline void set_m_maskOffset_248(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_maskOffset_248 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_249() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_EnvMapMatrix_249)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_EnvMapMatrix_249() const { return ___m_EnvMapMatrix_249; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_EnvMapMatrix_249() { return &___m_EnvMapMatrix_249; }
	inline void set_m_EnvMapMatrix_249(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_EnvMapMatrix_249 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_250() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_isRegisteredForEvents_250)); }
	inline bool get_m_isRegisteredForEvents_250() const { return ___m_isRegisteredForEvents_250; }
	inline bool* get_address_of_m_isRegisteredForEvents_250() { return &___m_isRegisteredForEvents_250; }
	inline void set_m_isRegisteredForEvents_250(bool value)
	{
		___m_isRegisteredForEvents_250 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_251() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___m_recursiveCountA_251)); }
	inline int32_t get_m_recursiveCountA_251() const { return ___m_recursiveCountA_251; }
	inline int32_t* get_address_of_m_recursiveCountA_251() { return &___m_recursiveCountA_251; }
	inline void set_m_recursiveCountA_251(int32_t value)
	{
		___m_recursiveCountA_251 = value;
	}

	inline static int32_t get_offset_of_loopCountA_252() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438, ___loopCountA_252)); }
	inline int32_t get_loopCountA_252() const { return ___loopCountA_252; }
	inline int32_t* get_address_of_loopCountA_252() { return &___loopCountA_252; }
	inline void set_loopCountA_252(int32_t value)
	{
		___loopCountA_252 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROUGUI_TBA60B913AB6151F8563F7078AD67EB6458129438_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6200 = { sizeof (U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5), -1, sizeof(U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6200[5] = 
{
	U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields::get_offset_of_U3CU3E9__41_0_3(),
	U3CU3Ec_t6B7E5372E513FB62AF3A99179A0B07B4335668C5_StaticFields::get_offset_of_U3CU3E9__41_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6201 = { sizeof (JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6201[3] = 
{
	JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_t9C7372EFA368E434DC3880D355199C7FA529129A::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6202 = { sizeof (JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6202[3] = 
{
	JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t6D455EB811A66C7B8471A2911C84354FDB2A80E2::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6203 = { sizeof (JsonStringContract_t7070636F94D5F28C05BA2B07887DA4572C420ACB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6204 = { sizeof (JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1), -1, sizeof(JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6204[4] = 
{
	JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields::get_offset_of__fullyTrusted_0(),
	JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields::get_offset_of_JsonConverterCreatorCache_1(),
	JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields::get_offset_of_AssociatedMetadataTypesCache_2(),
	JsonTypeReflector_t1AFF9A68583A2FDB0C90964398020E65ED986FC1_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6205 = { sizeof (U3CU3Ec__DisplayClass18_0_t23EF8A18E83AE533FE61177F9AE74EE045D2B5DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6205[2] = 
{
	U3CU3Ec__DisplayClass18_0_t23EF8A18E83AE533FE61177F9AE74EE045D2B5DB::get_offset_of_converterType_0(),
	U3CU3Ec__DisplayClass18_0_t23EF8A18E83AE533FE61177F9AE74EE045D2B5DB::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6206 = { sizeof (U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4), -1, sizeof(U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6206[2] = 
{
	U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t011636F89FC21CC23D4D3CD646E81A316766A9A4_StaticFields::get_offset_of_U3CU3E9__18_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6207 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6208 = { sizeof (OnErrorAttribute_tFEC5B0C0CC1339ADD7E096DCE8E204AE2C21541C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6209 = { sizeof (ReflectionAttributeProvider_t3AC467A13E733204C9F67A4944CB06B4855D8D55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6209[1] = 
{
	ReflectionAttributeProvider_t3AC467A13E733204C9F67A4944CB06B4855D8D55::get_offset_of__attributeProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6210 = { sizeof (ReflectionValueProvider_t7A572BCA761C655FA9CB80438DD694A4B7549E52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6210[1] = 
{
	ReflectionValueProvider_t7A572BCA761C655FA9CB80438DD694A4B7549E52::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6211 = { sizeof (TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6211[3] = 
{
	TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4::get_offset_of__innerReader_15(),
	TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4::get_offset_of__textWriter_16(),
	TraceJsonReader_tF4819EAB44B6955D240FEAB54F53DF1D572723F4::get_offset_of__sw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6212 = { sizeof (TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6212[3] = 
{
	TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D::get_offset_of__innerWriter_13(),
	TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D::get_offset_of__textWriter_14(),
	TraceJsonWriter_t03C3948B6EF80A4B977F14F581757202D464DB6D::get_offset_of__sw_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6213 = { sizeof (CommentHandling_t8A00D794DF4DCFECD5B91F4CF29B500159D703D9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6213[3] = 
{
	CommentHandling_t8A00D794DF4DCFECD5B91F4CF29B500159D703D9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6214 = { sizeof (LineInfoHandling_tA0B927B127EBD4A855BD742AA5690775964A4B46)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6214[3] = 
{
	LineInfoHandling_tA0B927B127EBD4A855BD742AA5690775964A4B46::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6215 = { sizeof (Extensions_tEB3365B098821F2F5284FC73DCACC106323A89C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6216 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6217 = { sizeof (JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6217[1] = 
{
	JArray_t1CE13821116F9B501573275C6BDD9FB254E65F11::get_offset_of__values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6218 = { sizeof (JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6218[2] = 
{
	JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775::get_offset_of__name_15(),
	JConstructor_t9F741A9F4DC238BBAC6980237CBBEA9730BA9775::get_offset_of__values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6219 = { sizeof (JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6219[2] = 
{
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB::get_offset_of__syncRoot_13(),
	JContainer_tF4CD2E574503C709DEF18A04B79B264B83746DAB::get_offset_of__busy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6220 = { sizeof (JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A), -1, sizeof(JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6220[1] = 
{
	JTokenReferenceEqualityComparer_t9FD2EC92D8DE56CDDF7D30BFD0FC24F6EF853B2A_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6221 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6221[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6222 = { sizeof (JObject_t786AF07B1009334856B0362BBC48EEF68C81C585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6222[2] = 
{
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585::get_offset_of__properties_15(),
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585::get_offset_of_PropertyChanged_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6223 = { sizeof (U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6223[4] = 
{
	U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__54_tB725EA473E3AAC9C6E9B8FDA0DE8F2E1EE4CDD53::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6224 = { sizeof (JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6224[2] = 
{
	JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A::get_offset_of__content_15(),
	JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A::get_offset_of__name_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6225 = { sizeof (JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6225[1] = 
{
	JPropertyList_tAC5DCFB03C7879ED6FCFD74977E76474C918200F::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6226 = { sizeof (U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6226[3] = 
{
	U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_t74F96992BB129149414620E86F10CFDF4C199AF8::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6227 = { sizeof (JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D), -1, sizeof(JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6227[2] = 
{
	JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_t013DFBF6A88616F3C011C00B60291F2EAB75483D::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6228 = { sizeof (JRaw_tCBA8F0819C86FF85ED57CD25E164D4ED1837175C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6229 = { sizeof (JsonLoadSettings_tC733151662103C5983D4F8ADBD871AC405A6504F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6229[2] = 
{
	JsonLoadSettings_tC733151662103C5983D4F8ADBD871AC405A6504F::get_offset_of__commentHandling_0(),
	JsonLoadSettings_tC733151662103C5983D4F8ADBD871AC405A6504F::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6230 = { sizeof (JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02), -1, sizeof(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6230[13] = 
{
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__parent_0(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__previous_1(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__next_2(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02::get_offset_of__annotations_3(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_StringTypes_6(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_UriTypes_9(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_CharTypes_10(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6231 = { sizeof (LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6231[2] = 
{
	LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t275C937C347C8A3C93FFD9CAC915903CDEDDF40D::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6232 = { sizeof (U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6232[7] = 
{
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E1__state_0(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E2__current_1(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_self_3(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E3__self_4(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CU3E4__this_5(),
	U3CGetAncestorsU3Ed__41_tA86F8A2E067567D67E95D02FD20F096E7F14B840::get_offset_of_U3CcurrentU3E5__1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6233 = { sizeof (JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6233[4] = 
{
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__initialPath_15(),
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__root_16(),
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__parent_17(),
	JTokenReader_t64F9A91FCD01D0A1FB881D2E8AD41AEC2E59D4C3::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6234 = { sizeof (JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6234[19] = 
{
	JTokenType_t6E74963952426FE99DA37BAEFD84A99710412C9E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6235 = { sizeof (JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6235[4] = 
{
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__token_13(),
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__parent_14(),
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__value_15(),
	JTokenWriter_tC1A2B10A61DBDF1F970ADA08BE3976BE7662BFE4::get_offset_of__current_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6236 = { sizeof (JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6236[2] = 
{
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF::get_offset_of__valueType_13(),
	JValue_t69F069DA12BD51A8FEB36C2EDA5D258B7FE2C4BF::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6237 = { sizeof (ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6237[1] = 
{
	ArrayIndexFilter_t4B8AB90AF78B3976E09BEA34466153A1CDE2E12B::get_offset_of_U3CIndexU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6238 = { sizeof (U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6238[11] = 
{
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E3__errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CtU3E5__1_8(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E7__wrap1_9(),
	U3CExecuteFilterU3Ed__4_t49283F96CCAA85028EA2C8F9402DA5D35998E1D7::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6239 = { sizeof (ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6239[1] = 
{
	ArrayMultipleIndexFilter_tF9FF764DDCB79A819281D799112F6E930B75E99F::get_offset_of_U3CIndexesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6240 = { sizeof (U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6240[11] = 
{
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CtU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E3__errorWhenNoMatch_8(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E7__wrap1_9(),
	U3CExecuteFilterU3Ed__4_tC82AE708D5CE2B8917E4AA90413E6CA8BA574131::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6241 = { sizeof (ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6241[3] = 
{
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3::get_offset_of_U3CStartU3Ek__BackingField_0(),
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3::get_offset_of_U3CEndU3Ek__BackingField_1(),
	ArraySliceFilter_tC5035758E75DB3EF9EAA182D561A726F46397BE3::get_offset_of_U3CStepU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6242 = { sizeof (U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6242[15] = 
{
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E4__this_3(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_current_4(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E3__current_5(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CaU3E5__1_6(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CiU3E5__2_7(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CstepCountU3E5__3_8(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CstopIndexU3E5__4_9(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CpositiveStepU3E5__5_10(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_errorWhenNoMatch_11(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E3__errorWhenNoMatch_12(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CtU3E5__6_13(),
	U3CExecuteFilterU3Ed__12_tBCB91730C7554E4E134042CDDD1550F32438D3F8::get_offset_of_U3CU3E7__wrap1_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6243 = { sizeof (FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6243[1] = 
{
	FieldFilter_t2B4992D5A1D10B7A905F49C62609097AED91A13B::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6244 = { sizeof (U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6244[12] = 
{
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E3__errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CoU3E5__1_8(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E7__wrap1_10(),
	U3CExecuteFilterU3Ed__4_tAF0C3B976817618E87E3152BBD9B3A1DF5D92610::get_offset_of_U3CU3E7__wrap2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6245 = { sizeof (FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6245[1] = 
{
	FieldMultipleFilter_tE61362AE319B8C510FD11900AD0A0B79EC08CD6B::get_offset_of_U3CNamesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6246 = { sizeof (U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D), -1, sizeof(U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6246[2] = 
{
	U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7AF93AC964FCE6A240C24249983367E897CC068D_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6247 = { sizeof (U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6247[13] = 
{
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CoU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E3__errorWhenNoMatch_8(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CnameU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CtU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E7__wrap1_11(),
	U3CExecuteFilterU3Ed__4_t0C1DEC1C597C26BA75BE873F98008D01240AE9BB::get_offset_of_U3CU3E7__wrap2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6248 = { sizeof (JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6248[3] = 
{
	JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7::get_offset_of__expression_0(),
	JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7::get_offset_of_U3CFiltersU3Ek__BackingField_1(),
	JPath_t3A0A64CF9AA620DDBE9D3C20AE75A7AE1E9225D7::get_offset_of__currentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6249 = { sizeof (PathFilter_tF860A9811AE814CCD54914A44C2A08AA07FECA80), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6250 = { sizeof (QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6250[11] = 
{
	QueryOperator_t91AB4C16055C42C3D23EBCE0B21564266F4B8130::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6251 = { sizeof (QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6251[1] = 
{
	QueryExpression_t08B8769F946D3E05F6E9DB46393F0B9828244E7F::get_offset_of_U3COperatorU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6252 = { sizeof (CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6252[1] = 
{
	CompositeExpression_t3153D0FCDDFBADAA4ED86DCB1527031D389F91E9::get_offset_of_U3CExpressionsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6253 = { sizeof (BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6253[2] = 
{
	BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F::get_offset_of_U3CPathU3Ek__BackingField_1(),
	BooleanQueryExpression_t6F2B15128729297EA0C686FD823C18D7616D667F::get_offset_of_U3CValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6254 = { sizeof (QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6254[1] = 
{
	QueryFilter_t6BBC6AA8C49E2AEED63087A9FCEB306F022C22AF::get_offset_of_U3CExpressionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6255 = { sizeof (U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6255[8] = 
{
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E7__wrap1_6(),
	U3CExecuteFilterU3Ed__4_t1092E9A17F5C737B43A85A54B82042D32C7E0683::get_offset_of_U3CU3E7__wrap2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6256 = { sizeof (ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6256[1] = 
{
	ScanFilter_t1FC1A02EA817F7D53FCA21E7FBB5DF326FC03F3D::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6257 = { sizeof (U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6257[9] = 
{
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CrootU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CvalueU3E5__2_7(),
	U3CExecuteFilterU3Ed__4_t69C2964074761EFD7077E24C57036C7DC5E33510::get_offset_of_U3CU3E7__wrap1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6258 = { sizeof (BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6258[1] = 
{
	BinaryConverter_t9DA25409355A29DFE356701CAD581E231D13DEF6::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6259 = { sizeof (BsonObjectIdConverter_t9DA35F1AB243B9FEA9CF962B4C3AAD720EB7A812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6260 = { sizeof (KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557), -1, sizeof(KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6260[1] = 
{
	KeyValuePairConverter_tD36E6790020AC069084D9D08BA6159A38E5E5557_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6261 = { sizeof (RegexConverter_t043C392623207631E6876907924EEA55F1AC05EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6262 = { sizeof (BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6262[8] = 
{
	BsonBinaryType_t82EF959604654BA62C48C95135EC2F8A896928E3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6263 = { sizeof (BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80), -1, sizeof(BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6263[4] = 
{
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80::get_offset_of__writer_1(),
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t3572084F4EE9D0ACA3EAE31EF1C0DF358D080D80::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6264 = { sizeof (BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6264[1] = 
{
	BsonObjectId_tF24F67A9B09CB213513B5218DF725B4E9AF3379C::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6265 = { sizeof (BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6265[2] = 
{
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_tEB681A5D0E53F8005DC31BAEFDE6481BBE80C2D3::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6266 = { sizeof (BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6266[1] = 
{
	BsonObject_tFB8AF55378FE99FCE476FF65B1667A0D45F93518::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6267 = { sizeof (BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6267[1] = 
{
	BsonArray_tFFCE8804FBA87DECA9A84178C668305D8E89B6A7::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6268 = { sizeof (BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6268[2] = 
{
	BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0::get_offset_of__value_2(),
	BsonValue_tB2A4632B6C4CAF0DB566AA14D13E0C0526328CF0::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6269 = { sizeof (BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6269[2] = 
{
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t87A7877CE01E3E2FA2B118C4A617FA16159CBC0A::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6270 = { sizeof (BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6270[1] = 
{
	BsonBinary_tD19C8F767A5AADB1A3B81A4BFA2ABECBC8082982::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6271 = { sizeof (BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6271[2] = 
{
	BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t5C64E5D5DABD795CAC97069165054859A0B938D3::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6272 = { sizeof (BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6272[2] = 
{
	BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t82747119CE439C13F6809CD9F8D815C3CE429583::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6273 = { sizeof (BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6273[21] = 
{
	BsonType_tBF3253E8D497F6E8F668EB77C7B6345854BE5538::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6274 = { sizeof (BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6274[4] = 
{
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__writer_13(),
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__root_14(),
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__parent_15(),
	BsonWriter_tD3EA6BE4366DF3B2C0E30C83318262FED969CE11::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6275 = { sizeof (U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6275[6] = 
{
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_U33DE43C11C7130AF9014115BCDC2584DFE6B50579_0(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_1(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_2(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_4(),
	U3CPrivateImplementationDetailsU3E_t1D1ACC86E2F3D4A2C7CA568000402E9AC4556D7E_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6276 = { sizeof (__StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D6_t68DC18882637DDE0E36BBD1FB7ABFE61687A6F98 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6277 = { sizeof (__StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t5D70094105EE31D680149823953357EE017EEE87 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6278 = { sizeof (__StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_tD27AE99D2D1EEDB86AC51A63B9AD0CA1DB6D8637 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6279 = { sizeof (__StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t5F1B9C4994D974B6D60CDFBEA3BB033669C9D472 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6280 = { sizeof (__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D52_t8E069480386AD2369CDA8BC3B64741395333F064 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6281 = { sizeof (U3CModuleU3E_tB181AC380D679257A9DFE61F653774B69E5DFF6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6282 = { sizeof (FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6282[2] = 
{
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB::get_offset_of_delegates_0(),
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB::get_offset_of_lookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6283 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6283[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6284 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6284[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6285 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6285[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6286 = { sizeof (MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC), -1, sizeof(MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6286[5] = 
{
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC_StaticFields::get_offset_of_s_Instance_0(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_FontMaterialReferenceLookup_1(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_FontAssetReferenceLookup_2(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_SpriteAssetReferenceLookup_3(),
	MaterialReferenceManager_tDA4B0CC78F04A2EFE2865CABFBBDBE18A49FC1DC::get_offset_of_m_ColorGradientReferenceLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6287 = { sizeof (MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6287[9] = 
{
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_fontAsset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_spriteAsset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_isDefaultMaterial_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_isFallbackMaterial_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_fallbackMaterial_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_padding_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F::get_offset_of_referenceCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6288 = { sizeof (TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6288[11] = 
{
	TextContainerAnchors_t8550A7F0F785AEC1D1588AD8104A3E2CE7C1270D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6289 = { sizeof (TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE), -1, sizeof(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6289[13] = 
{
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_hasChanged_4(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_pivot_5(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_anchorPosition_6(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_rect_7(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_isDefaultWidth_8(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_isDefaultHeight_9(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_isAutoFitting_10(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_corners_11(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_worldCorners_12(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_margins_13(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_rectTransform_14(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE_StaticFields::get_offset_of_k_defaultSize_15(),
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE::get_offset_of_m_textMeshPro_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6290 = { sizeof (TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6290[17] = 
{
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_currentAutoSizeMode_235(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_hasFontAssetChanged_236(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_previousLossyScaleY_237(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_renderer_238(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_meshFilter_239(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_isFirstAllocation_240(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_max_characters_241(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_max_numberOfLines_242(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_default_bounds_243(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_subTextObjects_244(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_isMaskingEnabled_245(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_isMaskUpdateRequired_246(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_maskType_247(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_EnvMapMatrix_248(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_RectTransformCorners_249(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_m_isRegisteredForEvents_250(),
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2::get_offset_of_loopCountA_251(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6291 = { sizeof (TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6291[18] = 
{
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isRebuildingLayout_235(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_hasFontAssetChanged_236(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_subTextObjects_237(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_previousLossyScaleY_238(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_RectTransformCorners_239(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_canvasRenderer_240(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_canvas_241(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isFirstAllocation_242(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_max_characters_243(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isMaskingEnabled_244(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_baseMaterial_245(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isScrollRegionSet_246(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_stencilID_247(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_maskOffset_248(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_EnvMapMatrix_249(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_isRegisteredForEvents_250(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_m_recursiveCountA_251(),
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438::get_offset_of_loopCountA_252(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6292 = { sizeof (TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6292[3] = 
{
	TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D::get_offset_of_hashCode_4(),
	TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D::get_offset_of_material_5(),
	TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D::get_offset_of_materialHashCode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6293 = { sizeof (ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6293[5] = 
{
	ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6294 = { sizeof (TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7), -1, sizeof(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6294[7] = 
{
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_colorMode_4(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_topLeft_5(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_topRight_6(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_bottomLeft_7(),
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7::get_offset_of_bottomRight_8(),
	0,
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7_StaticFields::get_offset_of_k_DefaultColor_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6295 = { sizeof (TMP_Compatibility_t44C9537249F15C4E525D25FFAF101997F82887E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6296 = { sizeof (AnchorPositions_t3503D57135628F82EB0DC13CA870189F1FF61A45)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6296[12] = 
{
	AnchorPositions_t3503D57135628F82EB0DC13CA870189F1FF61A45::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6297 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6298 = { sizeof (ColorTween_t60F356714C7D61B6F660900D4017054A166402D2)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6298[6] = 
{
	ColorTween_t60F356714C7D61B6F660900D4017054A166402D2::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t60F356714C7D61B6F660900D4017054A166402D2::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t60F356714C7D61B6F660900D4017054A166402D2::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t60F356714C7D61B6F660900D4017054A166402D2::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t60F356714C7D61B6F660900D4017054A166402D2::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t60F356714C7D61B6F660900D4017054A166402D2::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6299 = { sizeof (ColorTweenMode_tD6E324EC6173EE67693D01411BF27B40D547F6A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6299[4] = 
{
	ColorTweenMode_tD6E324EC6173EE67693D01411BF27B40D547F6A6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
