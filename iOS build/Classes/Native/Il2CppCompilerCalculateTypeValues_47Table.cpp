﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null
struct Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_t5E47CAAC0C7B909FEE19C9C4D4342AFC8B15F719;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_t391586A9268E9DABF6C6158169C448C4243FA87C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher
struct IBufferedCipher_tF573FBDFB2ABDB6CF7049013D91BED1EABEEAA3A;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters
struct ICipherParameters_t1E1B86752982ED2CFC2683B4C1A0BC0CDB77345B;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IKeyWrapper
struct IKeyWrapper_t04E00864E3E09ADE25D8EA669A75F58DC5C7CA50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream
struct CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerSink
struct SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner
struct ISigner_tB728365C47647B1E736C0C715DACEBA9341F6CDD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding
struct IBlockCipherPadding_t480DA3A1FAC2CCBCFC08B3AEA35E4338731AB7FA;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHValidationParameters
struct DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters
struct DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters
struct DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters
struct ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters
struct ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters
struct Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters
struct Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger
struct BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t6071986B64066FBF97315592BE971355FA584A39;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570;
// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet
struct ISet_t585F03B00DBE773170901D2ABF9576187FAF92B5;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASYMMETRICKEYPARAMETER_T4F2CA810DA69334DB5E985540B64958EE26DF316_H
#define ASYMMETRICKEYPARAMETER_T4F2CA810DA69334DB5E985540B64958EE26DF316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct  AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter::privateKey
	bool ___privateKey_0;

public:
	inline static int32_t get_offset_of_privateKey_0() { return static_cast<int32_t>(offsetof(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316, ___privateKey_0)); }
	inline bool get_privateKey_0() const { return ___privateKey_0; }
	inline bool* get_address_of_privateKey_0() { return &___privateKey_0; }
	inline void set_privateKey_0(bool value)
	{
		___privateKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICKEYPARAMETER_T4F2CA810DA69334DB5E985540B64958EE26DF316_H
#ifndef BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#define BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedCipherBase
struct  BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F  : public RuntimeObject
{
public:

public:
};

struct BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedCipherBase::EmptyBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBuffer_0;

public:
	inline static int32_t get_offset_of_EmptyBuffer_0() { return static_cast<int32_t>(offsetof(BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_StaticFields, ___EmptyBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBuffer_0() const { return ___EmptyBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBuffer_0() { return &___EmptyBuffer_0; }
	inline void set_EmptyBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDCIPHERBASE_T09AC93EC11CD6BE4023CE20E80E6A87B95C0716F_H
#ifndef KEYGENERATIONPARAMETERS_TC65EF43115EA8E516AD7410DCF741410A28744DC_H
#define KEYGENERATIONPARAMETERS_TC65EF43115EA8E516AD7410DCF741410A28744DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.KeyGenerationParameters
struct  KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.KeyGenerationParameters::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.KeyGenerationParameters::strength
	int32_t ___strength_1;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC, ___strength_1)); }
	inline int32_t get_strength_1() const { return ___strength_1; }
	inline int32_t* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(int32_t value)
	{
		___strength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATIONPARAMETERS_TC65EF43115EA8E516AD7410DCF741410A28744DC_H
#ifndef ASN1CIPHERBUILDERWITHKEY_T6F3882D30EAED60278C261779CCC4D8DE035D04A_H
#define ASN1CIPHERBUILDERWITHKEY_T6F3882D30EAED60278C261779CCC4D8DE035D04A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1CipherBuilderWithKey
struct  Asn1CipherBuilderWithKey_t6F3882D30EAED60278C261779CCC4D8DE035D04A  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1CipherBuilderWithKey::encKey
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___encKey_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1CipherBuilderWithKey::algorithmIdentifier
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algorithmIdentifier_1;

public:
	inline static int32_t get_offset_of_encKey_0() { return static_cast<int32_t>(offsetof(Asn1CipherBuilderWithKey_t6F3882D30EAED60278C261779CCC4D8DE035D04A, ___encKey_0)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_encKey_0() const { return ___encKey_0; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_encKey_0() { return &___encKey_0; }
	inline void set_encKey_0(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___encKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___encKey_0), value);
	}

	inline static int32_t get_offset_of_algorithmIdentifier_1() { return static_cast<int32_t>(offsetof(Asn1CipherBuilderWithKey_t6F3882D30EAED60278C261779CCC4D8DE035D04A, ___algorithmIdentifier_1)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algorithmIdentifier_1() const { return ___algorithmIdentifier_1; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algorithmIdentifier_1() { return &___algorithmIdentifier_1; }
	inline void set_algorithmIdentifier_1(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algorithmIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithmIdentifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1CIPHERBUILDERWITHKEY_T6F3882D30EAED60278C261779CCC4D8DE035D04A_H
#ifndef ASN1KEYWRAPPER_T8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA_H
#define ASN1KEYWRAPPER_T8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1KeyWrapper
struct  Asn1KeyWrapper_t8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA  : public RuntimeObject
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1KeyWrapper::algorithm
	String_t* ___algorithm_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IKeyWrapper BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1KeyWrapper::wrapper
	RuntimeObject* ___wrapper_1;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(Asn1KeyWrapper_t8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA, ___algorithm_0)); }
	inline String_t* get_algorithm_0() const { return ___algorithm_0; }
	inline String_t** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(String_t* value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_wrapper_1() { return static_cast<int32_t>(offsetof(Asn1KeyWrapper_t8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA, ___wrapper_1)); }
	inline RuntimeObject* get_wrapper_1() const { return ___wrapper_1; }
	inline RuntimeObject** get_address_of_wrapper_1() { return &___wrapper_1; }
	inline void set_wrapper_1(RuntimeObject* value)
	{
		___wrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1KEYWRAPPER_T8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA_H
#ifndef ASN1SIGNATUREFACTORY_T187B7704786D411A22847D24C5F313A98D8B9B09_H
#define ASN1SIGNATUREFACTORY_T187B7704786D411A22847D24C5F313A98D8B9B09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory
struct  Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::algID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algID_0;
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::algorithm
	String_t* ___algorithm_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::privateKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___privateKey_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1SignatureFactory::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_3;

public:
	inline static int32_t get_offset_of_algID_0() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09, ___algID_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algID_0() const { return ___algID_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algID_0() { return &___algID_0; }
	inline void set_algID_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algID_0 = value;
		Il2CppCodeGenWriteBarrier((&___algID_0), value);
	}

	inline static int32_t get_offset_of_algorithm_1() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09, ___algorithm_1)); }
	inline String_t* get_algorithm_1() const { return ___algorithm_1; }
	inline String_t** get_address_of_algorithm_1() { return &___algorithm_1; }
	inline void set_algorithm_1(String_t* value)
	{
		___algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_1), value);
	}

	inline static int32_t get_offset_of_privateKey_2() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09, ___privateKey_2)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_privateKey_2() const { return ___privateKey_2; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_privateKey_2() { return &___privateKey_2; }
	inline void set_privateKey_2(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___privateKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___privateKey_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09, ___random_3)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1SIGNATUREFACTORY_T187B7704786D411A22847D24C5F313A98D8B9B09_H
#ifndef ASN1VERIFIERFACTORY_TD81A626F1B292CBBEB88C6E5F1EC604A66B2261B_H
#define ASN1VERIFIERFACTORY_TD81A626F1B292CBBEB88C6E5F1EC604A66B2261B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactory
struct  Asn1VerifierFactory_tD81A626F1B292CBBEB88C6E5F1EC604A66B2261B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactory::algID
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algID_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactory::publicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___publicKey_1;

public:
	inline static int32_t get_offset_of_algID_0() { return static_cast<int32_t>(offsetof(Asn1VerifierFactory_tD81A626F1B292CBBEB88C6E5F1EC604A66B2261B, ___algID_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algID_0() const { return ___algID_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algID_0() { return &___algID_0; }
	inline void set_algID_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algID_0 = value;
		Il2CppCodeGenWriteBarrier((&___algID_0), value);
	}

	inline static int32_t get_offset_of_publicKey_1() { return static_cast<int32_t>(offsetof(Asn1VerifierFactory_tD81A626F1B292CBBEB88C6E5F1EC604A66B2261B, ___publicKey_1)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_publicKey_1() const { return ___publicKey_1; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_publicKey_1() { return &___publicKey_1; }
	inline void set_publicKey_1(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___publicKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1VERIFIERFACTORY_TD81A626F1B292CBBEB88C6E5F1EC604A66B2261B_H
#ifndef ASN1VERIFIERFACTORYPROVIDER_T38B79CCF7189D3F6EAAFD405ED172DC451D4022F_H
#define ASN1VERIFIERFACTORYPROVIDER_T38B79CCF7189D3F6EAAFD405ED172DC451D4022F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactoryProvider
struct  Asn1VerifierFactoryProvider_t38B79CCF7189D3F6EAAFD405ED172DC451D4022F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.AsymmetricKeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.Asn1VerifierFactoryProvider::publicKey
	AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * ___publicKey_0;

public:
	inline static int32_t get_offset_of_publicKey_0() { return static_cast<int32_t>(offsetof(Asn1VerifierFactoryProvider_t38B79CCF7189D3F6EAAFD405ED172DC451D4022F, ___publicKey_0)); }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * get_publicKey_0() const { return ___publicKey_0; }
	inline AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 ** get_address_of_publicKey_0() { return &___publicKey_0; }
	inline void set_publicKey_0(AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316 * value)
	{
		___publicKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1VERIFIERFACTORYPROVIDER_T38B79CCF7189D3F6EAAFD405ED172DC451D4022F_H
#ifndef BUFFEREDCIPHERWRAPPER_T8E695A9EB33E2251EFA53AF37B8005E282FE7F55_H
#define BUFFEREDCIPHERWRAPPER_T8E695A9EB33E2251EFA53AF37B8005E282FE7F55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.BufferedCipherWrapper
struct  BufferedCipherWrapper_t8E695A9EB33E2251EFA53AF37B8005E282FE7F55  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBufferedCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.BufferedCipherWrapper::bufferedCipher
	RuntimeObject* ___bufferedCipher_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.CipherStream BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.BufferedCipherWrapper::stream
	CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 * ___stream_1;

public:
	inline static int32_t get_offset_of_bufferedCipher_0() { return static_cast<int32_t>(offsetof(BufferedCipherWrapper_t8E695A9EB33E2251EFA53AF37B8005E282FE7F55, ___bufferedCipher_0)); }
	inline RuntimeObject* get_bufferedCipher_0() const { return ___bufferedCipher_0; }
	inline RuntimeObject** get_address_of_bufferedCipher_0() { return &___bufferedCipher_0; }
	inline void set_bufferedCipher_0(RuntimeObject* value)
	{
		___bufferedCipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___bufferedCipher_0), value);
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(BufferedCipherWrapper_t8E695A9EB33E2251EFA53AF37B8005E282FE7F55, ___stream_1)); }
	inline CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 * get_stream_1() const { return ___stream_1; }
	inline CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(CipherStream_t13FBA777CFF6E34F7F648F22EFAB7A48CDA0E0E5 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDCIPHERWRAPPER_T8E695A9EB33E2251EFA53AF37B8005E282FE7F55_H
#ifndef DEFAULTSIGNATURECALCULATOR_T4F2B735D91C53E6BB3A5E5D26131006E5A63FB77_H
#define DEFAULTSIGNATURECALCULATOR_T4F2B735D91C53E6BB3A5E5D26131006E5A63FB77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultSignatureCalculator
struct  DefaultSignatureCalculator_t4F2B735D91C53E6BB3A5E5D26131006E5A63FB77  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerSink BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultSignatureCalculator::mSignerSink
	SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA * ___mSignerSink_0;

public:
	inline static int32_t get_offset_of_mSignerSink_0() { return static_cast<int32_t>(offsetof(DefaultSignatureCalculator_t4F2B735D91C53E6BB3A5E5D26131006E5A63FB77, ___mSignerSink_0)); }
	inline SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA * get_mSignerSink_0() const { return ___mSignerSink_0; }
	inline SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA ** get_address_of_mSignerSink_0() { return &___mSignerSink_0; }
	inline void set_mSignerSink_0(SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA * value)
	{
		___mSignerSink_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSignerSink_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSIGNATURECALCULATOR_T4F2B735D91C53E6BB3A5E5D26131006E5A63FB77_H
#ifndef DEFAULTSIGNATURERESULT_T698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605_H
#define DEFAULTSIGNATURERESULT_T698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultSignatureResult
struct  DefaultSignatureResult_t698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultSignatureResult::mSigner
	RuntimeObject* ___mSigner_0;

public:
	inline static int32_t get_offset_of_mSigner_0() { return static_cast<int32_t>(offsetof(DefaultSignatureResult_t698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605, ___mSigner_0)); }
	inline RuntimeObject* get_mSigner_0() const { return ___mSigner_0; }
	inline RuntimeObject** get_address_of_mSigner_0() { return &___mSigner_0; }
	inline void set_mSigner_0(RuntimeObject* value)
	{
		___mSigner_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSigner_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSIGNATURERESULT_T698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605_H
#ifndef DEFAULTVERIFIERCALCULATOR_T262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF_H
#define DEFAULTVERIFIERCALCULATOR_T262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultVerifierCalculator
struct  DefaultVerifierCalculator_t262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IO.SignerSink BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultVerifierCalculator::mSignerSink
	SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA * ___mSignerSink_0;

public:
	inline static int32_t get_offset_of_mSignerSink_0() { return static_cast<int32_t>(offsetof(DefaultVerifierCalculator_t262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF, ___mSignerSink_0)); }
	inline SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA * get_mSignerSink_0() const { return ___mSignerSink_0; }
	inline SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA ** get_address_of_mSignerSink_0() { return &___mSignerSink_0; }
	inline void set_mSignerSink_0(SignerSink_t8C4F195F439F894E5F574AB9D73C0FB58112E4BA * value)
	{
		___mSignerSink_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSignerSink_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVERIFIERCALCULATOR_T262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF_H
#ifndef DEFAULTVERIFIERRESULT_T5BC74071A5A8777E7034F82570FDCCD3D06DA889_H
#define DEFAULTVERIFIERRESULT_T5BC74071A5A8777E7034F82570FDCCD3D06DA889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultVerifierResult
struct  DefaultVerifierResult_t5BC74071A5A8777E7034F82570FDCCD3D06DA889  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ISigner BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.DefaultVerifierResult::mSigner
	RuntimeObject* ___mSigner_0;

public:
	inline static int32_t get_offset_of_mSigner_0() { return static_cast<int32_t>(offsetof(DefaultVerifierResult_t5BC74071A5A8777E7034F82570FDCCD3D06DA889, ___mSigner_0)); }
	inline RuntimeObject* get_mSigner_0() const { return ___mSigner_0; }
	inline RuntimeObject** get_address_of_mSigner_0() { return &___mSigner_0; }
	inline void set_mSigner_0(RuntimeObject* value)
	{
		___mSigner_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSigner_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVERIFIERRESULT_T5BC74071A5A8777E7034F82570FDCCD3D06DA889_H
#ifndef KEYWRAPPERUTIL_T802932A483358268281449792AA9AB369C581915_H
#define KEYWRAPPERUTIL_T802932A483358268281449792AA9AB369C581915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.KeyWrapperUtil
struct  KeyWrapperUtil_t802932A483358268281449792AA9AB369C581915  : public RuntimeObject
{
public:

public:
};

struct KeyWrapperUtil_t802932A483358268281449792AA9AB369C581915_StaticFields
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.KeyWrapperUtil::providerMap
	RuntimeObject* ___providerMap_0;

public:
	inline static int32_t get_offset_of_providerMap_0() { return static_cast<int32_t>(offsetof(KeyWrapperUtil_t802932A483358268281449792AA9AB369C581915_StaticFields, ___providerMap_0)); }
	inline RuntimeObject* get_providerMap_0() const { return ___providerMap_0; }
	inline RuntimeObject** get_address_of_providerMap_0() { return &___providerMap_0; }
	inline void set_providerMap_0(RuntimeObject* value)
	{
		___providerMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___providerMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWRAPPERUTIL_T802932A483358268281449792AA9AB369C581915_H
#ifndef RSAOAEPWRAPPER_TCA776C88195418E3188B9A20AB6020BDBEF0C19B_H
#define RSAOAEPWRAPPER_TCA776C88195418E3188B9A20AB6020BDBEF0C19B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.RsaOaepWrapper
struct  RsaOaepWrapper_tCA776C88195418E3188B9A20AB6020BDBEF0C19B  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.RsaOaepWrapper::algId
	AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * ___algId_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IAsymmetricBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.RsaOaepWrapper::engine
	RuntimeObject* ___engine_1;

public:
	inline static int32_t get_offset_of_algId_0() { return static_cast<int32_t>(offsetof(RsaOaepWrapper_tCA776C88195418E3188B9A20AB6020BDBEF0C19B, ___algId_0)); }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * get_algId_0() const { return ___algId_0; }
	inline AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 ** get_address_of_algId_0() { return &___algId_0; }
	inline void set_algId_0(AlgorithmIdentifier_tCD0D4BB1FE94EAFD06C4C39FDC733AC890A969D3 * value)
	{
		___algId_0 = value;
		Il2CppCodeGenWriteBarrier((&___algId_0), value);
	}

	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(RsaOaepWrapper_tCA776C88195418E3188B9A20AB6020BDBEF0C19B, ___engine_1)); }
	inline RuntimeObject* get_engine_1() const { return ___engine_1; }
	inline RuntimeObject** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(RuntimeObject* value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAOAEPWRAPPER_TCA776C88195418E3188B9A20AB6020BDBEF0C19B_H
#ifndef RSAOAEPWRAPPERPROVIDER_T93774444FF1BF5B541170264C3EF9408B4EBFFEB_H
#define RSAOAEPWRAPPERPROVIDER_T93774444FF1BF5B541170264C3EF9408B4EBFFEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.RsaOaepWrapperProvider
struct  RsaOaepWrapperProvider_t93774444FF1BF5B541170264C3EF9408B4EBFFEB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.RsaOaepWrapperProvider::digestOid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___digestOid_0;

public:
	inline static int32_t get_offset_of_digestOid_0() { return static_cast<int32_t>(offsetof(RsaOaepWrapperProvider_t93774444FF1BF5B541170264C3EF9408B4EBFFEB, ___digestOid_0)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_digestOid_0() const { return ___digestOid_0; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_digestOid_0() { return &___digestOid_0; }
	inline void set_digestOid_0(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___digestOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___digestOid_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAOAEPWRAPPERPROVIDER_T93774444FF1BF5B541170264C3EF9408B4EBFFEB_H
#ifndef X509UTILITIES_T74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_H
#define X509UTILITIES_T74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.X509Utilities
struct  X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512  : public RuntimeObject
{
public:

public:
};

struct X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Asn1Null BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.X509Utilities::derNull
	Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * ___derNull_0;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.X509Utilities::algorithms
	RuntimeObject* ___algorithms_1;
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.X509Utilities::exParams
	RuntimeObject* ___exParams_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections.ISet BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Operators.X509Utilities::noParams
	RuntimeObject* ___noParams_3;

public:
	inline static int32_t get_offset_of_derNull_0() { return static_cast<int32_t>(offsetof(X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields, ___derNull_0)); }
	inline Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * get_derNull_0() const { return ___derNull_0; }
	inline Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 ** get_address_of_derNull_0() { return &___derNull_0; }
	inline void set_derNull_0(Asn1Null_t3E113E0C5A984D4CC7EF8FC4965BCC19E8D035C3 * value)
	{
		___derNull_0 = value;
		Il2CppCodeGenWriteBarrier((&___derNull_0), value);
	}

	inline static int32_t get_offset_of_algorithms_1() { return static_cast<int32_t>(offsetof(X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields, ___algorithms_1)); }
	inline RuntimeObject* get_algorithms_1() const { return ___algorithms_1; }
	inline RuntimeObject** get_address_of_algorithms_1() { return &___algorithms_1; }
	inline void set_algorithms_1(RuntimeObject* value)
	{
		___algorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_1), value);
	}

	inline static int32_t get_offset_of_exParams_2() { return static_cast<int32_t>(offsetof(X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields, ___exParams_2)); }
	inline RuntimeObject* get_exParams_2() const { return ___exParams_2; }
	inline RuntimeObject** get_address_of_exParams_2() { return &___exParams_2; }
	inline void set_exParams_2(RuntimeObject* value)
	{
		___exParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___exParams_2), value);
	}

	inline static int32_t get_offset_of_noParams_3() { return static_cast<int32_t>(offsetof(X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields, ___noParams_3)); }
	inline RuntimeObject* get_noParams_3() const { return ___noParams_3; }
	inline RuntimeObject** get_address_of_noParams_3() { return &___noParams_3; }
	inline void set_noParams_3(RuntimeObject* value)
	{
		___noParams_3 = value;
		Il2CppCodeGenWriteBarrier((&___noParams_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509UTILITIES_T74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_H
#ifndef ISO10126D2PADDING_T330B1A4EC476C1029D6E6B53E3721FF16A3856AF_H
#define ISO10126D2PADDING_T330B1A4EC476C1029D6E6B53E3721FF16A3856AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.ISO10126d2Padding
struct  ISO10126d2Padding_t330B1A4EC476C1029D6E6B53E3721FF16A3856AF  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.ISO10126d2Padding::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(ISO10126d2Padding_t330B1A4EC476C1029D6E6B53E3721FF16A3856AF, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO10126D2PADDING_T330B1A4EC476C1029D6E6B53E3721FF16A3856AF_H
#ifndef ISO7816D4PADDING_T415A35B77A1949B24FFDD1C389E52FA9C377E866_H
#define ISO7816D4PADDING_T415A35B77A1949B24FFDD1C389E52FA9C377E866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.ISO7816d4Padding
struct  ISO7816d4Padding_t415A35B77A1949B24FFDD1C389E52FA9C377E866  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO7816D4PADDING_T415A35B77A1949B24FFDD1C389E52FA9C377E866_H
#ifndef PKCS7PADDING_T770374C8871A08871A8CD32E793C43DD785B7912_H
#define PKCS7PADDING_T770374C8871A08871A8CD32E793C43DD785B7912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.Pkcs7Padding
struct  Pkcs7Padding_t770374C8871A08871A8CD32E793C43DD785B7912  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS7PADDING_T770374C8871A08871A8CD32E793C43DD785B7912_H
#ifndef TBCPADDING_T56BCA9C7439648B66B64620D690A875E2757329E_H
#define TBCPADDING_T56BCA9C7439648B66B64620D690A875E2757329E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.TbcPadding
struct  TbcPadding_t56BCA9C7439648B66B64620D690A875E2757329E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TBCPADDING_T56BCA9C7439648B66B64620D690A875E2757329E_H
#ifndef X923PADDING_T2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C_H
#define X923PADDING_T2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.X923Padding
struct  X923Padding_t2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.X923Padding::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_0;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(X923Padding_t2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C, ___random_0)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X923PADDING_T2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C_H
#ifndef ZEROBYTEPADDING_T53C60734473757BDEECC745647E2BFD3F03E27B2_H
#define ZEROBYTEPADDING_T53C60734473757BDEECC745647E2BFD3F03E27B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.ZeroBytePadding
struct  ZeroBytePadding_t53C60734473757BDEECC745647E2BFD3F03E27B2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZEROBYTEPADDING_T53C60734473757BDEECC745647E2BFD3F03E27B2_H
#ifndef DHPARAMETERS_TC2CDBB48EE0ABA9685B888746C76E72685DD2E67_H
#define DHPARAMETERS_TC2CDBB48EE0ABA9685B888746C76E72685DD2E67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters
struct  DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::j
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___j_4;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::m
	int32_t ___m_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::l
	int32_t ___l_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHValidationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters::validation
	DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB * ___validation_7;

public:
	inline static int32_t get_offset_of_p_1() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___p_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_1() const { return ___p_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_1() { return &___p_1; }
	inline void set_p_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_1 = value;
		Il2CppCodeGenWriteBarrier((&___p_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___g_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_2() const { return ___g_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___q_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_3() const { return ___q_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_3 = value;
		Il2CppCodeGenWriteBarrier((&___q_3), value);
	}

	inline static int32_t get_offset_of_j_4() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___j_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_j_4() const { return ___j_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_j_4() { return &___j_4; }
	inline void set_j_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___j_4 = value;
		Il2CppCodeGenWriteBarrier((&___j_4), value);
	}

	inline static int32_t get_offset_of_m_5() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___m_5)); }
	inline int32_t get_m_5() const { return ___m_5; }
	inline int32_t* get_address_of_m_5() { return &___m_5; }
	inline void set_m_5(int32_t value)
	{
		___m_5 = value;
	}

	inline static int32_t get_offset_of_l_6() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___l_6)); }
	inline int32_t get_l_6() const { return ___l_6; }
	inline int32_t* get_address_of_l_6() { return &___l_6; }
	inline void set_l_6(int32_t value)
	{
		___l_6 = value;
	}

	inline static int32_t get_offset_of_validation_7() { return static_cast<int32_t>(offsetof(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67, ___validation_7)); }
	inline DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB * get_validation_7() const { return ___validation_7; }
	inline DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB ** get_address_of_validation_7() { return &___validation_7; }
	inline void set_validation_7(DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB * value)
	{
		___validation_7 = value;
		Il2CppCodeGenWriteBarrier((&___validation_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPARAMETERS_TC2CDBB48EE0ABA9685B888746C76E72685DD2E67_H
#ifndef DHVALIDATIONPARAMETERS_T92186C1B030FD60B8BDC80B439C1375BEF4239DB_H
#define DHVALIDATIONPARAMETERS_T92186C1B030FD60B8BDC80B439C1375BEF4239DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHValidationParameters
struct  DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHValidationParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHValidationParameters::counter
	int32_t ___counter_1;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB, ___seed_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_0() const { return ___seed_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}

	inline static int32_t get_offset_of_counter_1() { return static_cast<int32_t>(offsetof(DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB, ___counter_1)); }
	inline int32_t get_counter_1() const { return ___counter_1; }
	inline int32_t* get_address_of_counter_1() { return &___counter_1; }
	inline void set_counter_1(int32_t value)
	{
		___counter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHVALIDATIONPARAMETERS_T92186C1B030FD60B8BDC80B439C1375BEF4239DB_H
#ifndef DSAPARAMETERGENERATIONPARAMETERS_T49A86E2CB443A80D9211265D28EC5E38C96C842D_H
#define DSAPARAMETERGENERATIONPARAMETERS_T49A86E2CB443A80D9211265D28EC5E38C96C842D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameterGenerationParameters
struct  DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameterGenerationParameters::l
	int32_t ___l_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameterGenerationParameters::n
	int32_t ___n_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameterGenerationParameters::certainty
	int32_t ___certainty_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameterGenerationParameters::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_5;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameterGenerationParameters::usageIndex
	int32_t ___usageIndex_6;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D, ___l_2)); }
	inline int32_t get_l_2() const { return ___l_2; }
	inline int32_t* get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(int32_t value)
	{
		___l_2 = value;
	}

	inline static int32_t get_offset_of_n_3() { return static_cast<int32_t>(offsetof(DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D, ___n_3)); }
	inline int32_t get_n_3() const { return ___n_3; }
	inline int32_t* get_address_of_n_3() { return &___n_3; }
	inline void set_n_3(int32_t value)
	{
		___n_3 = value;
	}

	inline static int32_t get_offset_of_certainty_4() { return static_cast<int32_t>(offsetof(DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D, ___certainty_4)); }
	inline int32_t get_certainty_4() const { return ___certainty_4; }
	inline int32_t* get_address_of_certainty_4() { return &___certainty_4; }
	inline void set_certainty_4(int32_t value)
	{
		___certainty_4 = value;
	}

	inline static int32_t get_offset_of_random_5() { return static_cast<int32_t>(offsetof(DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D, ___random_5)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_5() const { return ___random_5; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_5() { return &___random_5; }
	inline void set_random_5(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_5 = value;
		Il2CppCodeGenWriteBarrier((&___random_5), value);
	}

	inline static int32_t get_offset_of_usageIndex_6() { return static_cast<int32_t>(offsetof(DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D, ___usageIndex_6)); }
	inline int32_t get_usageIndex_6() const { return ___usageIndex_6; }
	inline int32_t* get_address_of_usageIndex_6() { return &___usageIndex_6; }
	inline void set_usageIndex_6(int32_t value)
	{
		___usageIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPARAMETERGENERATIONPARAMETERS_T49A86E2CB443A80D9211265D28EC5E38C96C842D_H
#ifndef DSAPARAMETERS_T61302B2C4C47647A233F4DC9423258244C712534_H
#define DSAPARAMETERS_T61302B2C4C47647A233F4DC9423258244C712534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters
struct  DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters::validation
	DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E * ___validation_3;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534, ___p_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_0() const { return ___p_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534, ___q_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_1() const { return ___q_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534, ___g_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_2() const { return ___g_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}

	inline static int32_t get_offset_of_validation_3() { return static_cast<int32_t>(offsetof(DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534, ___validation_3)); }
	inline DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E * get_validation_3() const { return ___validation_3; }
	inline DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E ** get_address_of_validation_3() { return &___validation_3; }
	inline void set_validation_3(DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E * value)
	{
		___validation_3 = value;
		Il2CppCodeGenWriteBarrier((&___validation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPARAMETERS_T61302B2C4C47647A233F4DC9423258244C712534_H
#ifndef DSAVALIDATIONPARAMETERS_TCF23DA7777D305A869ECA66E80DF0A3D76948F9E_H
#define DSAVALIDATIONPARAMETERS_TCF23DA7777D305A869ECA66E80DF0A3D76948F9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters
struct  DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters::counter
	int32_t ___counter_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters::usageIndex
	int32_t ___usageIndex_2;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E, ___seed_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_0() const { return ___seed_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}

	inline static int32_t get_offset_of_counter_1() { return static_cast<int32_t>(offsetof(DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E, ___counter_1)); }
	inline int32_t get_counter_1() const { return ___counter_1; }
	inline int32_t* get_address_of_counter_1() { return &___counter_1; }
	inline void set_counter_1(int32_t value)
	{
		___counter_1 = value;
	}

	inline static int32_t get_offset_of_usageIndex_2() { return static_cast<int32_t>(offsetof(DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E, ___usageIndex_2)); }
	inline int32_t get_usageIndex_2() const { return ___usageIndex_2; }
	inline int32_t* get_address_of_usageIndex_2() { return &___usageIndex_2; }
	inline void set_usageIndex_2(int32_t value)
	{
		___usageIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAVALIDATIONPARAMETERS_TCF23DA7777D305A869ECA66E80DF0A3D76948F9E_H
#ifndef ECDOMAINPARAMETERS_T287E469316C77EE39705286EB543B0CFB25F675C_H
#define ECDOMAINPARAMETERS_T287E469316C77EE39705286EB543B0CFB25F675C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct  ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECCurve BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::curve
	ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * ___curve_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::g
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___g_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::n
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___n_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::h
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___h_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::hInv
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___hInv_5;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C, ___curve_0)); }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * get_curve_0() const { return ___curve_0; }
	inline ECCurve_t6071986B64066FBF97315592BE971355FA584A39 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(ECCurve_t6071986B64066FBF97315592BE971355FA584A39 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_seed_1() { return static_cast<int32_t>(offsetof(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C, ___seed_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_1() const { return ___seed_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_1() { return &___seed_1; }
	inline void set_seed_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_1 = value;
		Il2CppCodeGenWriteBarrier((&___seed_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C, ___g_2)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_g_2() const { return ___g_2; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}

	inline static int32_t get_offset_of_n_3() { return static_cast<int32_t>(offsetof(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C, ___n_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_n_3() const { return ___n_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_n_3() { return &___n_3; }
	inline void set_n_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___n_3 = value;
		Il2CppCodeGenWriteBarrier((&___n_3), value);
	}

	inline static int32_t get_offset_of_h_4() { return static_cast<int32_t>(offsetof(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C, ___h_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_h_4() const { return ___h_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_h_4() { return &___h_4; }
	inline void set_h_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___h_4 = value;
		Il2CppCodeGenWriteBarrier((&___h_4), value);
	}

	inline static int32_t get_offset_of_hInv_5() { return static_cast<int32_t>(offsetof(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C, ___hInv_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_hInv_5() const { return ___hInv_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_hInv_5() { return &___hInv_5; }
	inline void set_hInv_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___hInv_5 = value;
		Il2CppCodeGenWriteBarrier((&___hInv_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDOMAINPARAMETERS_T287E469316C77EE39705286EB543B0CFB25F675C_H
#ifndef ELGAMALPARAMETERS_T4899578B25063061E5CBDC44EFE67936D6CB831F_H
#define ELGAMALPARAMETERS_T4899578B25063061E5CBDC44EFE67936D6CB831F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters
struct  ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters::l
	int32_t ___l_2;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F, ___p_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_0() const { return ___p_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F, ___g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_1() const { return ___g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F, ___l_2)); }
	inline int32_t get_l_2() const { return ___l_2; }
	inline int32_t* get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(int32_t value)
	{
		___l_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPARAMETERS_T4899578B25063061E5CBDC44EFE67936D6CB831F_H
#ifndef GOST3410PARAMETERS_TA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD_H
#define GOST3410PARAMETERS_TA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters
struct  Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::a
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___a_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::validation
	Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C * ___validation_3;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD, ___p_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_0() const { return ___p_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD, ___q_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_1() const { return ___q_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_a_2() { return static_cast<int32_t>(offsetof(Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD, ___a_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_a_2() const { return ___a_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_a_2() { return &___a_2; }
	inline void set_a_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___a_2 = value;
		Il2CppCodeGenWriteBarrier((&___a_2), value);
	}

	inline static int32_t get_offset_of_validation_3() { return static_cast<int32_t>(offsetof(Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD, ___validation_3)); }
	inline Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C * get_validation_3() const { return ___validation_3; }
	inline Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C ** get_address_of_validation_3() { return &___validation_3; }
	inline void set_validation_3(Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C * value)
	{
		___validation_3 = value;
		Il2CppCodeGenWriteBarrier((&___validation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PARAMETERS_TA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD_H
#ifndef GOST3410VALIDATIONPARAMETERS_TC6593A30220CEC644C5042BD9FEF96F7FFBBF80C_H
#define GOST3410VALIDATIONPARAMETERS_TC6593A30220CEC644C5042BD9FEF96F7FFBBF80C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters
struct  Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::x0
	int32_t ___x0_0;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::c
	int32_t ___c_1;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::x0L
	int64_t ___x0L_2;
	// System.Int64 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::cL
	int64_t ___cL_3;

public:
	inline static int32_t get_offset_of_x0_0() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C, ___x0_0)); }
	inline int32_t get_x0_0() const { return ___x0_0; }
	inline int32_t* get_address_of_x0_0() { return &___x0_0; }
	inline void set_x0_0(int32_t value)
	{
		___x0_0 = value;
	}

	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C, ___c_1)); }
	inline int32_t get_c_1() const { return ___c_1; }
	inline int32_t* get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(int32_t value)
	{
		___c_1 = value;
	}

	inline static int32_t get_offset_of_x0L_2() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C, ___x0L_2)); }
	inline int64_t get_x0L_2() const { return ___x0L_2; }
	inline int64_t* get_address_of_x0L_2() { return &___x0L_2; }
	inline void set_x0L_2(int64_t value)
	{
		___x0L_2 = value;
	}

	inline static int32_t get_offset_of_cL_3() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C, ___cL_3)); }
	inline int64_t get_cL_3() const { return ___cL_3; }
	inline int64_t* get_address_of_cL_3() { return &___cL_3; }
	inline void set_cL_3(int64_t value)
	{
		___cL_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410VALIDATIONPARAMETERS_TC6593A30220CEC644C5042BD9FEF96F7FFBBF80C_H
#ifndef HKDFPARAMETERS_TDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412_H
#define HKDFPARAMETERS_TDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.HkdfParameters
struct  HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.HkdfParameters::ikm
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ikm_0;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.HkdfParameters::skipExpand
	bool ___skipExpand_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.HkdfParameters::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_2;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.HkdfParameters::info
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___info_3;

public:
	inline static int32_t get_offset_of_ikm_0() { return static_cast<int32_t>(offsetof(HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412, ___ikm_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ikm_0() const { return ___ikm_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ikm_0() { return &___ikm_0; }
	inline void set_ikm_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ikm_0 = value;
		Il2CppCodeGenWriteBarrier((&___ikm_0), value);
	}

	inline static int32_t get_offset_of_skipExpand_1() { return static_cast<int32_t>(offsetof(HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412, ___skipExpand_1)); }
	inline bool get_skipExpand_1() const { return ___skipExpand_1; }
	inline bool* get_address_of_skipExpand_1() { return &___skipExpand_1; }
	inline void set_skipExpand_1(bool value)
	{
		___skipExpand_1 = value;
	}

	inline static int32_t get_offset_of_salt_2() { return static_cast<int32_t>(offsetof(HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412, ___salt_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_2() const { return ___salt_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_2() { return &___salt_2; }
	inline void set_salt_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_2 = value;
		Il2CppCodeGenWriteBarrier((&___salt_2), value);
	}

	inline static int32_t get_offset_of_info_3() { return static_cast<int32_t>(offsetof(HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412, ___info_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_info_3() const { return ___info_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_info_3() { return &___info_3; }
	inline void set_info_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___info_3 = value;
		Il2CppCodeGenWriteBarrier((&___info_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HKDFPARAMETERS_TDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412_H
#ifndef IESPARAMETERS_TE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9_H
#define IESPARAMETERS_TE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesParameters
struct  IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesParameters::derivation
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___derivation_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesParameters::encoding
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encoding_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesParameters::macKeySize
	int32_t ___macKeySize_2;

public:
	inline static int32_t get_offset_of_derivation_0() { return static_cast<int32_t>(offsetof(IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9, ___derivation_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_derivation_0() const { return ___derivation_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_derivation_0() { return &___derivation_0; }
	inline void set_derivation_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___derivation_0 = value;
		Il2CppCodeGenWriteBarrier((&___derivation_0), value);
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9, ___encoding_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encoding_1() const { return ___encoding_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_1), value);
	}

	inline static int32_t get_offset_of_macKeySize_2() { return static_cast<int32_t>(offsetof(IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9, ___macKeySize_2)); }
	inline int32_t get_macKeySize_2() const { return ___macKeySize_2; }
	inline int32_t* get_address_of_macKeySize_2() { return &___macKeySize_2; }
	inline void set_macKeySize_2(int32_t value)
	{
		___macKeySize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IESPARAMETERS_TE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9_H
#ifndef ISO18033KDFPARAMETERS_T652F8A1E3074919A62DE5FFCCB38DA96406524C3_H
#define ISO18033KDFPARAMETERS_T652F8A1E3074919A62DE5FFCCB38DA96406524C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Iso18033KdfParameters
struct  Iso18033KdfParameters_t652F8A1E3074919A62DE5FFCCB38DA96406524C3  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Iso18033KdfParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_0;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(Iso18033KdfParameters_t652F8A1E3074919A62DE5FFCCB38DA96406524C3, ___seed_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_0() const { return ___seed_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISO18033KDFPARAMETERS_T652F8A1E3074919A62DE5FFCCB38DA96406524C3_H
#ifndef KDFCOUNTERPARAMETERS_T0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3_H
#define KDFCOUNTERPARAMETERS_T0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfCounterParameters
struct  KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfCounterParameters::ki
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ki_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfCounterParameters::fixedInputDataCounterPrefix
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputDataCounterPrefix_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfCounterParameters::fixedInputDataCounterSuffix
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputDataCounterSuffix_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfCounterParameters::r
	int32_t ___r_3;

public:
	inline static int32_t get_offset_of_ki_0() { return static_cast<int32_t>(offsetof(KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3, ___ki_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ki_0() const { return ___ki_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ki_0() { return &___ki_0; }
	inline void set_ki_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ki_0 = value;
		Il2CppCodeGenWriteBarrier((&___ki_0), value);
	}

	inline static int32_t get_offset_of_fixedInputDataCounterPrefix_1() { return static_cast<int32_t>(offsetof(KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3, ___fixedInputDataCounterPrefix_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputDataCounterPrefix_1() const { return ___fixedInputDataCounterPrefix_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputDataCounterPrefix_1() { return &___fixedInputDataCounterPrefix_1; }
	inline void set_fixedInputDataCounterPrefix_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputDataCounterPrefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputDataCounterPrefix_1), value);
	}

	inline static int32_t get_offset_of_fixedInputDataCounterSuffix_2() { return static_cast<int32_t>(offsetof(KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3, ___fixedInputDataCounterSuffix_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputDataCounterSuffix_2() const { return ___fixedInputDataCounterSuffix_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputDataCounterSuffix_2() { return &___fixedInputDataCounterSuffix_2; }
	inline void set_fixedInputDataCounterSuffix_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputDataCounterSuffix_2 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputDataCounterSuffix_2), value);
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3, ___r_3)); }
	inline int32_t get_r_3() const { return ___r_3; }
	inline int32_t* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(int32_t value)
	{
		___r_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFCOUNTERPARAMETERS_T0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3_H
#ifndef KDFDOUBLEPIPELINEITERATIONPARAMETERS_TD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_H
#define KDFDOUBLEPIPELINEITERATIONPARAMETERS_TD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfDoublePipelineIterationParameters
struct  KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfDoublePipelineIterationParameters::ki
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ki_1;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfDoublePipelineIterationParameters::useCounter
	bool ___useCounter_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfDoublePipelineIterationParameters::r
	int32_t ___r_3;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfDoublePipelineIterationParameters::fixedInputData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputData_4;

public:
	inline static int32_t get_offset_of_ki_1() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A, ___ki_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ki_1() const { return ___ki_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ki_1() { return &___ki_1; }
	inline void set_ki_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ki_1 = value;
		Il2CppCodeGenWriteBarrier((&___ki_1), value);
	}

	inline static int32_t get_offset_of_useCounter_2() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A, ___useCounter_2)); }
	inline bool get_useCounter_2() const { return ___useCounter_2; }
	inline bool* get_address_of_useCounter_2() { return &___useCounter_2; }
	inline void set_useCounter_2(bool value)
	{
		___useCounter_2 = value;
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A, ___r_3)); }
	inline int32_t get_r_3() const { return ___r_3; }
	inline int32_t* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(int32_t value)
	{
		___r_3 = value;
	}

	inline static int32_t get_offset_of_fixedInputData_4() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A, ___fixedInputData_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputData_4() const { return ___fixedInputData_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputData_4() { return &___fixedInputData_4; }
	inline void set_fixedInputData_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputData_4 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputData_4), value);
	}
};

struct KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfDoublePipelineIterationParameters::UNUSED_R
	int32_t ___UNUSED_R_0;

public:
	inline static int32_t get_offset_of_UNUSED_R_0() { return static_cast<int32_t>(offsetof(KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_StaticFields, ___UNUSED_R_0)); }
	inline int32_t get_UNUSED_R_0() const { return ___UNUSED_R_0; }
	inline int32_t* get_address_of_UNUSED_R_0() { return &___UNUSED_R_0; }
	inline void set_UNUSED_R_0(int32_t value)
	{
		___UNUSED_R_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFDOUBLEPIPELINEITERATIONPARAMETERS_TD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_H
#ifndef KDFFEEDBACKPARAMETERS_T946A85C47127FC4E406A55D75D429D6A04ED26BF_H
#define KDFFEEDBACKPARAMETERS_T946A85C47127FC4E406A55D75D429D6A04ED26BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters
struct  KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters::ki
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ki_1;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters::useCounter
	bool ___useCounter_3;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters::r
	int32_t ___r_4;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters::fixedInputData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___fixedInputData_5;

public:
	inline static int32_t get_offset_of_ki_1() { return static_cast<int32_t>(offsetof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF, ___ki_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ki_1() const { return ___ki_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ki_1() { return &___ki_1; }
	inline void set_ki_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ki_1 = value;
		Il2CppCodeGenWriteBarrier((&___ki_1), value);
	}

	inline static int32_t get_offset_of_iv_2() { return static_cast<int32_t>(offsetof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF, ___iv_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_2() const { return ___iv_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_2() { return &___iv_2; }
	inline void set_iv_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_2 = value;
		Il2CppCodeGenWriteBarrier((&___iv_2), value);
	}

	inline static int32_t get_offset_of_useCounter_3() { return static_cast<int32_t>(offsetof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF, ___useCounter_3)); }
	inline bool get_useCounter_3() const { return ___useCounter_3; }
	inline bool* get_address_of_useCounter_3() { return &___useCounter_3; }
	inline void set_useCounter_3(bool value)
	{
		___useCounter_3 = value;
	}

	inline static int32_t get_offset_of_r_4() { return static_cast<int32_t>(offsetof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF, ___r_4)); }
	inline int32_t get_r_4() const { return ___r_4; }
	inline int32_t* get_address_of_r_4() { return &___r_4; }
	inline void set_r_4(int32_t value)
	{
		___r_4 = value;
	}

	inline static int32_t get_offset_of_fixedInputData_5() { return static_cast<int32_t>(offsetof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF, ___fixedInputData_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_fixedInputData_5() const { return ___fixedInputData_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_fixedInputData_5() { return &___fixedInputData_5; }
	inline void set_fixedInputData_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___fixedInputData_5 = value;
		Il2CppCodeGenWriteBarrier((&___fixedInputData_5), value);
	}
};

struct KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfFeedbackParameters::UNUSED_R
	int32_t ___UNUSED_R_0;

public:
	inline static int32_t get_offset_of_UNUSED_R_0() { return static_cast<int32_t>(offsetof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF_StaticFields, ___UNUSED_R_0)); }
	inline int32_t get_UNUSED_R_0() const { return ___UNUSED_R_0; }
	inline int32_t* get_address_of_UNUSED_R_0() { return &___UNUSED_R_0; }
	inline void set_UNUSED_R_0(int32_t value)
	{
		___UNUSED_R_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFFEEDBACKPARAMETERS_T946A85C47127FC4E406A55D75D429D6A04ED26BF_H
#ifndef KDFPARAMETERS_T5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C_H
#define KDFPARAMETERS_T5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfParameters
struct  KdfParameters_t5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfParameters::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KdfParameters::shared
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___shared_1;

public:
	inline static int32_t get_offset_of_iv_0() { return static_cast<int32_t>(offsetof(KdfParameters_t5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C, ___iv_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_0() const { return ___iv_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_0() { return &___iv_0; }
	inline void set_iv_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_0 = value;
		Il2CppCodeGenWriteBarrier((&___iv_0), value);
	}

	inline static int32_t get_offset_of_shared_1() { return static_cast<int32_t>(offsetof(KdfParameters_t5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C, ___shared_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_shared_1() const { return ___shared_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_shared_1() { return &___shared_1; }
	inline void set_shared_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___shared_1 = value;
		Il2CppCodeGenWriteBarrier((&___shared_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KDFPARAMETERS_T5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C_H
#ifndef KEYPARAMETER_TB6DE772ACC72C04D32FFC0DD4AF033F54998D41F_H
#define KEYPARAMETER_TB6DE772ACC72C04D32FFC0DD4AF033F54998D41F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct  KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F, ___key_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_0() const { return ___key_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYPARAMETER_TB6DE772ACC72C04D32FFC0DD4AF033F54998D41F_H
#ifndef MGFPARAMETERS_T5F8B84C67C715344E4843CA5A7DA77C290E2943E_H
#define MGFPARAMETERS_T5F8B84C67C715344E4843CA5A7DA77C290E2943E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MgfParameters
struct  MgfParameters_t5F8B84C67C715344E4843CA5A7DA77C290E2943E  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MgfParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_0;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(MgfParameters_t5F8B84C67C715344E4843CA5A7DA77C290E2943E, ___seed_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_0() const { return ___seed_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MGFPARAMETERS_T5F8B84C67C715344E4843CA5A7DA77C290E2943E_H
#ifndef MQVPRIVATEPARAMETERS_TEB0B554F5B5D1525D06D47F25F611671B39B8DD7_H
#define MQVPRIVATEPARAMETERS_TEB0B554F5B5D1525D06D47F25F611671B39B8DD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPrivateParameters
struct  MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPrivateParameters::staticPrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___staticPrivateKey_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPrivateParameters::ephemeralPrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___ephemeralPrivateKey_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPrivateParameters::ephemeralPublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___ephemeralPublicKey_2;

public:
	inline static int32_t get_offset_of_staticPrivateKey_0() { return static_cast<int32_t>(offsetof(MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7, ___staticPrivateKey_0)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_staticPrivateKey_0() const { return ___staticPrivateKey_0; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_staticPrivateKey_0() { return &___staticPrivateKey_0; }
	inline void set_staticPrivateKey_0(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___staticPrivateKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___staticPrivateKey_0), value);
	}

	inline static int32_t get_offset_of_ephemeralPrivateKey_1() { return static_cast<int32_t>(offsetof(MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7, ___ephemeralPrivateKey_1)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_ephemeralPrivateKey_1() const { return ___ephemeralPrivateKey_1; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_ephemeralPrivateKey_1() { return &___ephemeralPrivateKey_1; }
	inline void set_ephemeralPrivateKey_1(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___ephemeralPrivateKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeralPrivateKey_1), value);
	}

	inline static int32_t get_offset_of_ephemeralPublicKey_2() { return static_cast<int32_t>(offsetof(MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7, ___ephemeralPublicKey_2)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_ephemeralPublicKey_2() const { return ___ephemeralPublicKey_2; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_ephemeralPublicKey_2() { return &___ephemeralPublicKey_2; }
	inline void set_ephemeralPublicKey_2(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___ephemeralPublicKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeralPublicKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MQVPRIVATEPARAMETERS_TEB0B554F5B5D1525D06D47F25F611671B39B8DD7_H
#ifndef MQVPUBLICPARAMETERS_T414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB_H
#define MQVPUBLICPARAMETERS_T414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPublicParameters
struct  MqvPublicParameters_t414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPublicParameters::staticPublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___staticPublicKey_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.MqvPublicParameters::ephemeralPublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___ephemeralPublicKey_1;

public:
	inline static int32_t get_offset_of_staticPublicKey_0() { return static_cast<int32_t>(offsetof(MqvPublicParameters_t414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB, ___staticPublicKey_0)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_staticPublicKey_0() const { return ___staticPublicKey_0; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_staticPublicKey_0() { return &___staticPublicKey_0; }
	inline void set_staticPublicKey_0(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___staticPublicKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___staticPublicKey_0), value);
	}

	inline static int32_t get_offset_of_ephemeralPublicKey_1() { return static_cast<int32_t>(offsetof(MqvPublicParameters_t414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB, ___ephemeralPublicKey_1)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_ephemeralPublicKey_1() const { return ___ephemeralPublicKey_1; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_ephemeralPublicKey_1() { return &___ephemeralPublicKey_1; }
	inline void set_ephemeralPublicKey_1(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___ephemeralPublicKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___ephemeralPublicKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MQVPUBLICPARAMETERS_T414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB_H
#ifndef PARAMETERSWITHID_T75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99_H
#define PARAMETERSWITHID_T75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithID
struct  ParametersWithID_t75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithID::parameters
	RuntimeObject* ___parameters_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithID::id
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___id_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithID_t75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ParametersWithID_t75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99, ___id_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_id_1() const { return ___id_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHID_T75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99_H
#ifndef PARAMETERSWITHIV_TF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF_H
#define PARAMETERSWITHIV_TF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV
struct  ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV::parameters
	RuntimeObject* ___parameters_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithIV::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_iv_1() { return static_cast<int32_t>(offsetof(ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF, ___iv_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_1() const { return ___iv_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_1() { return &___iv_1; }
	inline void set_iv_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_1 = value;
		Il2CppCodeGenWriteBarrier((&___iv_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHIV_TF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF_H
#ifndef PARAMETERSWITHRANDOM_TD20AFD747DC0851C0F83D7E031D98D0D81B6FB36_H
#define PARAMETERSWITHRANDOM_TD20AFD747DC0851C0F83D7E031D98D0D81B6FB36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithRandom
struct  ParametersWithRandom_tD20AFD747DC0851C0F83D7E031D98D0D81B6FB36  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithRandom::parameters
	RuntimeObject* ___parameters_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Security.SecureRandom BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithRandom::random
	SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * ___random_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithRandom_tD20AFD747DC0851C0F83D7E031D98D0D81B6FB36, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(ParametersWithRandom_tD20AFD747DC0851C0F83D7E031D98D0D81B6FB36, ___random_1)); }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t5520D5E8543D2000539BD07077884C7FB17A9570 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHRANDOM_TD20AFD747DC0851C0F83D7E031D98D0D81B6FB36_H
#ifndef PARAMETERSWITHSBOX_T2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA_H
#define PARAMETERSWITHSBOX_T2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithSBox
struct  ParametersWithSBox_t2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithSBox::parameters
	RuntimeObject* ___parameters_0;
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithSBox::sBox
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sBox_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithSBox_t2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_sBox_1() { return static_cast<int32_t>(offsetof(ParametersWithSBox_t2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA, ___sBox_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sBox_1() const { return ___sBox_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sBox_1() { return &___sBox_1; }
	inline void set_sBox_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sBox_1 = value;
		Il2CppCodeGenWriteBarrier((&___sBox_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHSBOX_T2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA_H
#ifndef PARAMETERSWITHSALT_TB8CF4A4C41A9BB671A30AF1B326CA09C18257201_H
#define PARAMETERSWITHSALT_TB8CF4A4C41A9BB671A30AF1B326CA09C18257201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithSalt
struct  ParametersWithSalt_tB8CF4A4C41A9BB671A30AF1B326CA09C18257201  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithSalt::salt
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___salt_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.ICipherParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ParametersWithSalt::parameters
	RuntimeObject* ___parameters_1;

public:
	inline static int32_t get_offset_of_salt_0() { return static_cast<int32_t>(offsetof(ParametersWithSalt_tB8CF4A4C41A9BB671A30AF1B326CA09C18257201, ___salt_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_salt_0() const { return ___salt_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_salt_0() { return &___salt_0; }
	inline void set_salt_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___salt_0 = value;
		Il2CppCodeGenWriteBarrier((&___salt_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ParametersWithSalt_tB8CF4A4C41A9BB671A30AF1B326CA09C18257201, ___parameters_1)); }
	inline RuntimeObject* get_parameters_1() const { return ___parameters_1; }
	inline RuntimeObject** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(RuntimeObject* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHSALT_TB8CF4A4C41A9BB671A30AF1B326CA09C18257201_H
#ifndef RSABLINDINGPARAMETERS_T9FF63A05ACE679FB85798FB7E65B0EE256123195_H
#define RSABLINDINGPARAMETERS_T9FF63A05ACE679FB85798FB7E65B0EE256123195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaBlindingParameters
struct  RsaBlindingParameters_t9FF63A05ACE679FB85798FB7E65B0EE256123195  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaBlindingParameters::publicKey
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * ___publicKey_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaBlindingParameters::blindingFactor
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___blindingFactor_1;

public:
	inline static int32_t get_offset_of_publicKey_0() { return static_cast<int32_t>(offsetof(RsaBlindingParameters_t9FF63A05ACE679FB85798FB7E65B0EE256123195, ___publicKey_0)); }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * get_publicKey_0() const { return ___publicKey_0; }
	inline RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 ** get_address_of_publicKey_0() { return &___publicKey_0; }
	inline void set_publicKey_0(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061 * value)
	{
		___publicKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_0), value);
	}

	inline static int32_t get_offset_of_blindingFactor_1() { return static_cast<int32_t>(offsetof(RsaBlindingParameters_t9FF63A05ACE679FB85798FB7E65B0EE256123195, ___blindingFactor_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_blindingFactor_1() const { return ___blindingFactor_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_blindingFactor_1() { return &___blindingFactor_1; }
	inline void set_blindingFactor_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___blindingFactor_1 = value;
		Il2CppCodeGenWriteBarrier((&___blindingFactor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSABLINDINGPARAMETERS_T9FF63A05ACE679FB85798FB7E65B0EE256123195_H
#ifndef SM2KEYEXCHANGEPRIVATEPARAMETERS_TF53EDD1ADECB805A8228E58A8DC7FDD901A85074_H
#define SM2KEYEXCHANGEPRIVATEPARAMETERS_TF53EDD1ADECB805A8228E58A8DC7FDD901A85074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePrivateParameters
struct  SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePrivateParameters::mInitiator
	bool ___mInitiator_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePrivateParameters::mStaticPrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mStaticPrivateKey_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePrivateParameters::mStaticPublicPoint
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___mStaticPublicPoint_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePrivateParameters::mEphemeralPrivateKey
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * ___mEphemeralPrivateKey_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePrivateParameters::mEphemeralPublicPoint
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___mEphemeralPublicPoint_4;

public:
	inline static int32_t get_offset_of_mInitiator_0() { return static_cast<int32_t>(offsetof(SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074, ___mInitiator_0)); }
	inline bool get_mInitiator_0() const { return ___mInitiator_0; }
	inline bool* get_address_of_mInitiator_0() { return &___mInitiator_0; }
	inline void set_mInitiator_0(bool value)
	{
		___mInitiator_0 = value;
	}

	inline static int32_t get_offset_of_mStaticPrivateKey_1() { return static_cast<int32_t>(offsetof(SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074, ___mStaticPrivateKey_1)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mStaticPrivateKey_1() const { return ___mStaticPrivateKey_1; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mStaticPrivateKey_1() { return &___mStaticPrivateKey_1; }
	inline void set_mStaticPrivateKey_1(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mStaticPrivateKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStaticPrivateKey_1), value);
	}

	inline static int32_t get_offset_of_mStaticPublicPoint_2() { return static_cast<int32_t>(offsetof(SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074, ___mStaticPublicPoint_2)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_mStaticPublicPoint_2() const { return ___mStaticPublicPoint_2; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_mStaticPublicPoint_2() { return &___mStaticPublicPoint_2; }
	inline void set_mStaticPublicPoint_2(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___mStaticPublicPoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___mStaticPublicPoint_2), value);
	}

	inline static int32_t get_offset_of_mEphemeralPrivateKey_3() { return static_cast<int32_t>(offsetof(SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074, ___mEphemeralPrivateKey_3)); }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * get_mEphemeralPrivateKey_3() const { return ___mEphemeralPrivateKey_3; }
	inline ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB ** get_address_of_mEphemeralPrivateKey_3() { return &___mEphemeralPrivateKey_3; }
	inline void set_mEphemeralPrivateKey_3(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB * value)
	{
		___mEphemeralPrivateKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___mEphemeralPrivateKey_3), value);
	}

	inline static int32_t get_offset_of_mEphemeralPublicPoint_4() { return static_cast<int32_t>(offsetof(SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074, ___mEphemeralPublicPoint_4)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_mEphemeralPublicPoint_4() const { return ___mEphemeralPublicPoint_4; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_mEphemeralPublicPoint_4() { return &___mEphemeralPublicPoint_4; }
	inline void set_mEphemeralPublicPoint_4(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___mEphemeralPublicPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___mEphemeralPublicPoint_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2KEYEXCHANGEPRIVATEPARAMETERS_TF53EDD1ADECB805A8228E58A8DC7FDD901A85074_H
#ifndef SM2KEYEXCHANGEPUBLICPARAMETERS_T13AB9E3780843E3790430C619A364EB5AFAB4346_H
#define SM2KEYEXCHANGEPUBLICPARAMETERS_T13AB9E3780843E3790430C619A364EB5AFAB4346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePublicParameters
struct  SM2KeyExchangePublicParameters_t13AB9E3780843E3790430C619A364EB5AFAB4346  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePublicParameters::mStaticPublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___mStaticPublicKey_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SM2KeyExchangePublicParameters::mEphemeralPublicKey
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * ___mEphemeralPublicKey_1;

public:
	inline static int32_t get_offset_of_mStaticPublicKey_0() { return static_cast<int32_t>(offsetof(SM2KeyExchangePublicParameters_t13AB9E3780843E3790430C619A364EB5AFAB4346, ___mStaticPublicKey_0)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_mStaticPublicKey_0() const { return ___mStaticPublicKey_0; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_mStaticPublicKey_0() { return &___mStaticPublicKey_0; }
	inline void set_mStaticPublicKey_0(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___mStaticPublicKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___mStaticPublicKey_0), value);
	}

	inline static int32_t get_offset_of_mEphemeralPublicKey_1() { return static_cast<int32_t>(offsetof(SM2KeyExchangePublicParameters_t13AB9E3780843E3790430C619A364EB5AFAB4346, ___mEphemeralPublicKey_1)); }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * get_mEphemeralPublicKey_1() const { return ___mEphemeralPublicKey_1; }
	inline ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 ** get_address_of_mEphemeralPublicKey_1() { return &___mEphemeralPublicKey_1; }
	inline void set_mEphemeralPublicKey_1(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2 * value)
	{
		___mEphemeralPublicKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___mEphemeralPublicKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SM2KEYEXCHANGEPUBLICPARAMETERS_T13AB9E3780843E3790430C619A364EB5AFAB4346_H
#ifndef SKEINPARAMETERS_T1161D508A7E57A7FF552002C6A965D88FEAA30CE_H
#define SKEINPARAMETERS_T1161D508A7E57A7FF552002C6A965D88FEAA30CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SkeinParameters
struct  SkeinParameters_t1161D508A7E57A7FF552002C6A965D88FEAA30CE  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SkeinParameters::parameters
	RuntimeObject* ___parameters_8;

public:
	inline static int32_t get_offset_of_parameters_8() { return static_cast<int32_t>(offsetof(SkeinParameters_t1161D508A7E57A7FF552002C6A965D88FEAA30CE, ___parameters_8)); }
	inline RuntimeObject* get_parameters_8() const { return ___parameters_8; }
	inline RuntimeObject** get_address_of_parameters_8() { return &___parameters_8; }
	inline void set_parameters_8(RuntimeObject* value)
	{
		___parameters_8 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEINPARAMETERS_T1161D508A7E57A7FF552002C6A965D88FEAA30CE_H
#ifndef BUILDER_T38FB7A5A57D5FBF9309ED347F94E11F0476ADC83_H
#define BUILDER_T38FB7A5A57D5FBF9309ED347F94E11F0476ADC83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SkeinParameters_Builder
struct  Builder_t38FB7A5A57D5FBF9309ED347F94E11F0476ADC83  : public RuntimeObject
{
public:
	// System.Collections.IDictionary BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.SkeinParameters_Builder::parameters
	RuntimeObject* ___parameters_0;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(Builder_t38FB7A5A57D5FBF9309ED347F94E11F0476ADC83, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T38FB7A5A57D5FBF9309ED347F94E11F0476ADC83_H
#ifndef SRP6GROUPPARAMETERS_T61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464_H
#define SRP6GROUPPARAMETERS_T61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters
struct  Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464  : public RuntimeObject
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters::n
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___n_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Srp6GroupParameters::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_1;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464, ___n_0)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_n_0() const { return ___n_0; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___n_0 = value;
		Il2CppCodeGenWriteBarrier((&___n_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464, ___g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_1() const { return ___g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SRP6GROUPPARAMETERS_T61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464_H
#ifndef TWEAKABLEBLOCKCIPHERPARAMETERS_T29A387C39129AEFF81B62207DEB81E3036F31488_H
#define TWEAKABLEBLOCKCIPHERPARAMETERS_T29A387C39129AEFF81B62207DEB81E3036F31488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.TweakableBlockCipherParameters
struct  TweakableBlockCipherParameters_t29A387C39129AEFF81B62207DEB81E3036F31488  : public RuntimeObject
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.TweakableBlockCipherParameters::tweak
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tweak_0;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.KeyParameter BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.TweakableBlockCipherParameters::key
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * ___key_1;

public:
	inline static int32_t get_offset_of_tweak_0() { return static_cast<int32_t>(offsetof(TweakableBlockCipherParameters_t29A387C39129AEFF81B62207DEB81E3036F31488, ___tweak_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tweak_0() const { return ___tweak_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tweak_0() { return &___tweak_0; }
	inline void set_tweak_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tweak_0 = value;
		Il2CppCodeGenWriteBarrier((&___tweak_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(TweakableBlockCipherParameters_t29A387C39129AEFF81B62207DEB81E3036F31488, ___key_1)); }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * get_key_1() const { return ___key_1; }
	inline KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKABLEBLOCKCIPHERPARAMETERS_T29A387C39129AEFF81B62207DEB81E3036F31488_H
#ifndef BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#define BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher
struct  BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B  : public BufferedCipherBase_t09AC93EC11CD6BE4023CE20E80E6A87B95C0716F
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::bufOff
	int32_t ___bufOff_2;
	// System.Boolean BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::forEncryption
	bool ___forEncryption_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.IBlockCipher BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.BufferedBlockCipher::cipher
	RuntimeObject* ___cipher_4;

public:
	inline static int32_t get_offset_of_buf_1() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___buf_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_1() const { return ___buf_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_1() { return &___buf_1; }
	inline void set_buf_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_1 = value;
		Il2CppCodeGenWriteBarrier((&___buf_1), value);
	}

	inline static int32_t get_offset_of_bufOff_2() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___bufOff_2)); }
	inline int32_t get_bufOff_2() const { return ___bufOff_2; }
	inline int32_t* get_address_of_bufOff_2() { return &___bufOff_2; }
	inline void set_bufOff_2(int32_t value)
	{
		___bufOff_2 = value;
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDBLOCKCIPHER_T96540DEA3889C80D38AA07E627C1E32EEDCD161B_H
#ifndef DHKEYGENERATIONPARAMETERS_T50085347D3474105C013CCBC0548EF56AF2F2960_H
#define DHKEYGENERATIONPARAMETERS_T50085347D3474105C013CCBC0548EF56AF2F2960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters
struct  DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters::parameters
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___parameters_2;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960, ___parameters_2)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_parameters_2() const { return ___parameters_2; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYGENERATIONPARAMETERS_T50085347D3474105C013CCBC0548EF56AF2F2960_H
#ifndef DHKEYPARAMETERS_T30526534F8A1CD79F62C7E83D46A44471D950F05_H
#define DHKEYPARAMETERS_T30526534F8A1CD79F62C7E83D46A44471D950F05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyParameters
struct  DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyParameters::parameters
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * ___parameters_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHKeyParameters::algorithmOid
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___algorithmOid_2;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05, ___parameters_1)); }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * get_parameters_1() const { return ___parameters_1; }
	inline DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_algorithmOid_2() { return static_cast<int32_t>(offsetof(DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05, ___algorithmOid_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_algorithmOid_2() const { return ___algorithmOid_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_algorithmOid_2() { return &___algorithmOid_2; }
	inline void set_algorithmOid_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___algorithmOid_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithmOid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYPARAMETERS_T30526534F8A1CD79F62C7E83D46A44471D950F05_H
#ifndef DESPARAMETERS_T5FE03E9F68A7690CA07C671D4A1275E127E376CD_H
#define DESPARAMETERS_T5FE03E9F68A7690CA07C671D4A1275E127E376CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DesParameters
struct  DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD  : public KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F
{
public:

public:
};

struct DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD_StaticFields
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DesParameters::DES_weak_keys
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___DES_weak_keys_3;

public:
	inline static int32_t get_offset_of_DES_weak_keys_3() { return static_cast<int32_t>(offsetof(DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD_StaticFields, ___DES_weak_keys_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_DES_weak_keys_3() const { return ___DES_weak_keys_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_DES_weak_keys_3() { return &___DES_weak_keys_3; }
	inline void set_DES_weak_keys_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___DES_weak_keys_3 = value;
		Il2CppCodeGenWriteBarrier((&___DES_weak_keys_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESPARAMETERS_T5FE03E9F68A7690CA07C671D4A1275E127E376CD_H
#ifndef DSAKEYGENERATIONPARAMETERS_T4F8D351530FCA2EAF46A994C4E26FF4772C3D707_H
#define DSAKEYGENERATIONPARAMETERS_T4F8D351530FCA2EAF46A994C4E26FF4772C3D707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyGenerationParameters
struct  DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyGenerationParameters::parameters
	DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 * ___parameters_2;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707, ___parameters_2)); }
	inline DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 * get_parameters_2() const { return ___parameters_2; }
	inline DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAKEYGENERATIONPARAMETERS_T4F8D351530FCA2EAF46A994C4E26FF4772C3D707_H
#ifndef DSAKEYPARAMETERS_T2F888CF39888FC27EEBA7C537DA165CC4CDB5043_H
#define DSAKEYPARAMETERS_T2F888CF39888FC27EEBA7C537DA165CC4CDB5043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters
struct  DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters::parameters
	DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 * ___parameters_1;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043, ___parameters_1)); }
	inline DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 * get_parameters_1() const { return ___parameters_1; }
	inline DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAKEYPARAMETERS_T2F888CF39888FC27EEBA7C537DA165CC4CDB5043_H
#ifndef ECKEYGENERATIONPARAMETERS_T25868784B11E49365A5AEF64EE1964F424E48DC4_H
#define ECKEYGENERATIONPARAMETERS_T25868784B11E49365A5AEF64EE1964F424E48DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyGenerationParameters
struct  ECKeyGenerationParameters_t25868784B11E49365A5AEF64EE1964F424E48DC4  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyGenerationParameters::domainParams
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * ___domainParams_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyGenerationParameters::publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___publicKeyParamSet_3;

public:
	inline static int32_t get_offset_of_domainParams_2() { return static_cast<int32_t>(offsetof(ECKeyGenerationParameters_t25868784B11E49365A5AEF64EE1964F424E48DC4, ___domainParams_2)); }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * get_domainParams_2() const { return ___domainParams_2; }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C ** get_address_of_domainParams_2() { return &___domainParams_2; }
	inline void set_domainParams_2(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * value)
	{
		___domainParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___domainParams_2), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_3() { return static_cast<int32_t>(offsetof(ECKeyGenerationParameters_t25868784B11E49365A5AEF64EE1964F424E48DC4, ___publicKeyParamSet_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_publicKeyParamSet_3() const { return ___publicKeyParamSet_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_publicKeyParamSet_3() { return &___publicKeyParamSet_3; }
	inline void set_publicKeyParamSet_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___publicKeyParamSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECKEYGENERATIONPARAMETERS_T25868784B11E49365A5AEF64EE1964F424E48DC4_H
#ifndef ECKEYPARAMETERS_TD7169A9268EEA24D8539790EFDF085A7BEFD92A4_H
#define ECKEYPARAMETERS_TD7169A9268EEA24D8539790EFDF085A7BEFD92A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters
struct  ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.String BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::algorithm
	String_t* ___algorithm_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECDomainParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::parameters
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * ___parameters_3;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___publicKeyParamSet_4;

public:
	inline static int32_t get_offset_of_algorithm_2() { return static_cast<int32_t>(offsetof(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4, ___algorithm_2)); }
	inline String_t* get_algorithm_2() const { return ___algorithm_2; }
	inline String_t** get_address_of_algorithm_2() { return &___algorithm_2; }
	inline void set_algorithm_2(String_t* value)
	{
		___algorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_2), value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4, ___parameters_3)); }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * get_parameters_3() const { return ___parameters_3; }
	inline ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C ** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C * value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_3), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_4() { return static_cast<int32_t>(offsetof(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4, ___publicKeyParamSet_4)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_publicKeyParamSet_4() const { return ___publicKeyParamSet_4; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_publicKeyParamSet_4() { return &___publicKeyParamSet_4; }
	inline void set_publicKeyParamSet_4(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___publicKeyParamSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_4), value);
	}
};

struct ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4_StaticFields
{
public:
	// System.String[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::algorithms
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___algorithms_1;

public:
	inline static int32_t get_offset_of_algorithms_1() { return static_cast<int32_t>(offsetof(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4_StaticFields, ___algorithms_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_algorithms_1() const { return ___algorithms_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_algorithms_1() { return &___algorithms_1; }
	inline void set_algorithms_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___algorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECKEYPARAMETERS_TD7169A9268EEA24D8539790EFDF085A7BEFD92A4_H
#ifndef ECNAMEDDOMAINPARAMETERS_T2FF719D618AF7F27C295790427588AEEA7C6130C_H
#define ECNAMEDDOMAINPARAMETERS_T2FF719D618AF7F27C295790427588AEEA7C6130C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECNamedDomainParameters
struct  ECNamedDomainParameters_t2FF719D618AF7F27C295790427588AEEA7C6130C  : public ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECNamedDomainParameters::name
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___name_6;

public:
	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(ECNamedDomainParameters_t2FF719D618AF7F27C295790427588AEEA7C6130C, ___name_6)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_name_6() const { return ___name_6; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECNAMEDDOMAINPARAMETERS_T2FF719D618AF7F27C295790427588AEEA7C6130C_H
#ifndef ED25519KEYGENERATIONPARAMETERS_T0F845E438E1B920F65A02FD1CA16EB0B96CC4AA9_H
#define ED25519KEYGENERATIONPARAMETERS_T0F845E438E1B920F65A02FD1CA16EB0B96CC4AA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519KeyGenerationParameters
struct  Ed25519KeyGenerationParameters_t0F845E438E1B920F65A02FD1CA16EB0B96CC4AA9  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519KEYGENERATIONPARAMETERS_T0F845E438E1B920F65A02FD1CA16EB0B96CC4AA9_H
#ifndef ED25519PRIVATEKEYPARAMETERS_TDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_H
#define ED25519PRIVATEKEYPARAMETERS_TDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters
struct  Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_3;

public:
	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2, ___data_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_3() const { return ___data_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

struct Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters::KeySize
	int32_t ___KeySize_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PrivateKeyParameters::SignatureSize
	int32_t ___SignatureSize_2;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}

	inline static int32_t get_offset_of_SignatureSize_2() { return static_cast<int32_t>(offsetof(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_StaticFields, ___SignatureSize_2)); }
	inline int32_t get_SignatureSize_2() const { return ___SignatureSize_2; }
	inline int32_t* get_address_of_SignatureSize_2() { return &___SignatureSize_2; }
	inline void set_SignatureSize_2(int32_t value)
	{
		___SignatureSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519PRIVATEKEYPARAMETERS_TDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_H
#ifndef ED25519PUBLICKEYPARAMETERS_T08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_H
#define ED25519PUBLICKEYPARAMETERS_T08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters
struct  Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3, ___data_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_2() const { return ___data_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed25519PublicKeyParameters::KeySize
	int32_t ___KeySize_1;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED25519PUBLICKEYPARAMETERS_T08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_H
#ifndef ED448KEYGENERATIONPARAMETERS_TA45CC70C374EA6F3B547E9B2B6FF94E300B714E1_H
#define ED448KEYGENERATIONPARAMETERS_TA45CC70C374EA6F3B547E9B2B6FF94E300B714E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448KeyGenerationParameters
struct  Ed448KeyGenerationParameters_tA45CC70C374EA6F3B547E9B2B6FF94E300B714E1  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448KEYGENERATIONPARAMETERS_TA45CC70C374EA6F3B547E9B2B6FF94E300B714E1_H
#ifndef ED448PRIVATEKEYPARAMETERS_TCAD4F3779488E120C11341492A4B0E47BAB4FC7D_H
#define ED448PRIVATEKEYPARAMETERS_TCAD4F3779488E120C11341492A4B0E47BAB4FC7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters
struct  Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_3;

public:
	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D, ___data_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_3() const { return ___data_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

struct Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters::KeySize
	int32_t ___KeySize_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PrivateKeyParameters::SignatureSize
	int32_t ___SignatureSize_2;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}

	inline static int32_t get_offset_of_SignatureSize_2() { return static_cast<int32_t>(offsetof(Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D_StaticFields, ___SignatureSize_2)); }
	inline int32_t get_SignatureSize_2() const { return ___SignatureSize_2; }
	inline int32_t* get_address_of_SignatureSize_2() { return &___SignatureSize_2; }
	inline void set_SignatureSize_2(int32_t value)
	{
		___SignatureSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448PRIVATEKEYPARAMETERS_TCAD4F3779488E120C11341492A4B0E47BAB4FC7D_H
#ifndef ED448PUBLICKEYPARAMETERS_T61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_H
#define ED448PUBLICKEYPARAMETERS_T61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PublicKeyParameters
struct  Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PublicKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B, ___data_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_2() const { return ___data_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Ed448PublicKeyParameters::KeySize
	int32_t ___KeySize_1;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ED448PUBLICKEYPARAMETERS_T61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_H
#ifndef ELGAMALKEYGENERATIONPARAMETERS_T6CF94D2D230374AB3FEDDA99200304B0749409D0_H
#define ELGAMALKEYGENERATIONPARAMETERS_T6CF94D2D230374AB3FEDDA99200304B0749409D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyGenerationParameters
struct  ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyGenerationParameters::parameters
	ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F * ___parameters_2;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0, ___parameters_2)); }
	inline ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F * get_parameters_2() const { return ___parameters_2; }
	inline ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALKEYGENERATIONPARAMETERS_T6CF94D2D230374AB3FEDDA99200304B0749409D0_H
#ifndef ELGAMALKEYPARAMETERS_T7810C31A0559BEA387ACAC593E129F0A179CF7BB_H
#define ELGAMALKEYPARAMETERS_T7810C31A0559BEA387ACAC593E129F0A179CF7BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyParameters
struct  ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalParameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalKeyParameters::parameters
	ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F * ___parameters_1;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB, ___parameters_1)); }
	inline ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F * get_parameters_1() const { return ___parameters_1; }
	inline ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALKEYPARAMETERS_T7810C31A0559BEA387ACAC593E129F0A179CF7BB_H
#ifndef GOST3410KEYGENERATIONPARAMETERS_T224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8_H
#define GOST3410KEYGENERATIONPARAMETERS_T224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyGenerationParameters
struct  Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyGenerationParameters::parameters
	Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD * ___parameters_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyGenerationParameters::publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___publicKeyParamSet_3;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8, ___parameters_2)); }
	inline Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD * get_parameters_2() const { return ___parameters_2; }
	inline Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_3() { return static_cast<int32_t>(offsetof(Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8, ___publicKeyParamSet_3)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_publicKeyParamSet_3() const { return ___publicKeyParamSet_3; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_publicKeyParamSet_3() { return &___publicKeyParamSet_3; }
	inline void set_publicKeyParamSet_3(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___publicKeyParamSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410KEYGENERATIONPARAMETERS_T224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8_H
#ifndef GOST3410KEYPARAMETERS_T5F3A9D8B8F093472F8C7FEE592B46AB964910186_H
#define GOST3410KEYPARAMETERS_T5F3A9D8B8F093472F8C7FEE592B46AB964910186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters
struct  Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters::parameters
	Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD * ___parameters_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters::publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ___publicKeyParamSet_2;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186, ___parameters_1)); }
	inline Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD * get_parameters_1() const { return ___parameters_1; }
	inline Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_2() { return static_cast<int32_t>(offsetof(Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186, ___publicKeyParamSet_2)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get_publicKeyParamSet_2() const { return ___publicKeyParamSet_2; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of_publicKeyParamSet_2() { return &___publicKeyParamSet_2; }
	inline void set_publicKeyParamSet_2(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		___publicKeyParamSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410KEYPARAMETERS_T5F3A9D8B8F093472F8C7FEE592B46AB964910186_H
#ifndef IESWITHCIPHERPARAMETERS_T48E566D48377A1743A77C0695EE0C4E55A3A960C_H
#define IESWITHCIPHERPARAMETERS_T48E566D48377A1743A77C0695EE0C4E55A3A960C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesWithCipherParameters
struct  IesWithCipherParameters_t48E566D48377A1743A77C0695EE0C4E55A3A960C  : public IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.IesWithCipherParameters::cipherKeySize
	int32_t ___cipherKeySize_3;

public:
	inline static int32_t get_offset_of_cipherKeySize_3() { return static_cast<int32_t>(offsetof(IesWithCipherParameters_t48E566D48377A1743A77C0695EE0C4E55A3A960C, ___cipherKeySize_3)); }
	inline int32_t get_cipherKeySize_3() const { return ___cipherKeySize_3; }
	inline int32_t* get_address_of_cipherKeySize_3() { return &___cipherKeySize_3; }
	inline void set_cipherKeySize_3(int32_t value)
	{
		___cipherKeySize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IESWITHCIPHERPARAMETERS_T48E566D48377A1743A77C0695EE0C4E55A3A960C_H
#ifndef NACCACHESTERNKEYGENERATIONPARAMETERS_T4A5485405836465214ED400316F6D3061340FA09_H
#define NACCACHESTERNKEYGENERATIONPARAMETERS_T4A5485405836465214ED400316F6D3061340FA09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyGenerationParameters
struct  NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyGenerationParameters::certainty
	int32_t ___certainty_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyGenerationParameters::countSmallPrimes
	int32_t ___countSmallPrimes_3;

public:
	inline static int32_t get_offset_of_certainty_2() { return static_cast<int32_t>(offsetof(NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09, ___certainty_2)); }
	inline int32_t get_certainty_2() const { return ___certainty_2; }
	inline int32_t* get_address_of_certainty_2() { return &___certainty_2; }
	inline void set_certainty_2(int32_t value)
	{
		___certainty_2 = value;
	}

	inline static int32_t get_offset_of_countSmallPrimes_3() { return static_cast<int32_t>(offsetof(NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09, ___countSmallPrimes_3)); }
	inline int32_t get_countSmallPrimes_3() const { return ___countSmallPrimes_3; }
	inline int32_t* get_address_of_countSmallPrimes_3() { return &___countSmallPrimes_3; }
	inline void set_countSmallPrimes_3(int32_t value)
	{
		___countSmallPrimes_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NACCACHESTERNKEYGENERATIONPARAMETERS_T4A5485405836465214ED400316F6D3061340FA09_H
#ifndef NACCACHESTERNKEYPARAMETERS_TC6C264C943C2C7FF44064DE0539DAB31159703B7_H
#define NACCACHESTERNKEYPARAMETERS_TC6C264C943C2C7FF44064DE0539DAB31159703B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyParameters
struct  NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyParameters::g
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___g_1;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyParameters::n
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___n_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternKeyParameters::lowerSigmaBound
	int32_t ___lowerSigmaBound_3;

public:
	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7, ___g_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_g_1() const { return ___g_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_n_2() { return static_cast<int32_t>(offsetof(NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7, ___n_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_n_2() const { return ___n_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_n_2() { return &___n_2; }
	inline void set_n_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___n_2 = value;
		Il2CppCodeGenWriteBarrier((&___n_2), value);
	}

	inline static int32_t get_offset_of_lowerSigmaBound_3() { return static_cast<int32_t>(offsetof(NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7, ___lowerSigmaBound_3)); }
	inline int32_t get_lowerSigmaBound_3() const { return ___lowerSigmaBound_3; }
	inline int32_t* get_address_of_lowerSigmaBound_3() { return &___lowerSigmaBound_3; }
	inline void set_lowerSigmaBound_3(int32_t value)
	{
		___lowerSigmaBound_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NACCACHESTERNKEYPARAMETERS_TC6C264C943C2C7FF44064DE0539DAB31159703B7_H
#ifndef RC2PARAMETERS_TB51A8CA0415E4E3B5B942EF74B377CF937163E87_H
#define RC2PARAMETERS_TB51A8CA0415E4E3B5B942EF74B377CF937163E87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RC2Parameters
struct  RC2Parameters_tB51A8CA0415E4E3B5B942EF74B377CF937163E87  : public KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RC2Parameters::bits
	int32_t ___bits_1;

public:
	inline static int32_t get_offset_of_bits_1() { return static_cast<int32_t>(offsetof(RC2Parameters_tB51A8CA0415E4E3B5B942EF74B377CF937163E87, ___bits_1)); }
	inline int32_t get_bits_1() const { return ___bits_1; }
	inline int32_t* get_address_of_bits_1() { return &___bits_1; }
	inline void set_bits_1(int32_t value)
	{
		___bits_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC2PARAMETERS_TB51A8CA0415E4E3B5B942EF74B377CF937163E87_H
#ifndef RC5PARAMETERS_T7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC_H
#define RC5PARAMETERS_T7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RC5Parameters
struct  RC5Parameters_t7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC  : public KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RC5Parameters::rounds
	int32_t ___rounds_1;

public:
	inline static int32_t get_offset_of_rounds_1() { return static_cast<int32_t>(offsetof(RC5Parameters_t7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC, ___rounds_1)); }
	inline int32_t get_rounds_1() const { return ___rounds_1; }
	inline int32_t* get_address_of_rounds_1() { return &___rounds_1; }
	inline void set_rounds_1(int32_t value)
	{
		___rounds_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC5PARAMETERS_T7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC_H
#ifndef RSAKEYGENERATIONPARAMETERS_TADAFF6887525BB5FE50D94B53E1B8D8032DB08AD_H
#define RSAKEYGENERATIONPARAMETERS_TADAFF6887525BB5FE50D94B53E1B8D8032DB08AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyGenerationParameters
struct  RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyGenerationParameters::publicExponent
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___publicExponent_2;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyGenerationParameters::certainty
	int32_t ___certainty_3;

public:
	inline static int32_t get_offset_of_publicExponent_2() { return static_cast<int32_t>(offsetof(RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD, ___publicExponent_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_publicExponent_2() const { return ___publicExponent_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_publicExponent_2() { return &___publicExponent_2; }
	inline void set_publicExponent_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___publicExponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___publicExponent_2), value);
	}

	inline static int32_t get_offset_of_certainty_3() { return static_cast<int32_t>(offsetof(RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD, ___certainty_3)); }
	inline int32_t get_certainty_3() const { return ___certainty_3; }
	inline int32_t* get_address_of_certainty_3() { return &___certainty_3; }
	inline void set_certainty_3(int32_t value)
	{
		___certainty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAKEYGENERATIONPARAMETERS_TADAFF6887525BB5FE50D94B53E1B8D8032DB08AD_H
#ifndef RSAKEYPARAMETERS_T1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_H
#define RSAKEYPARAMETERS_T1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct  RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters::modulus
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___modulus_2;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters::exponent
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___exponent_3;

public:
	inline static int32_t get_offset_of_modulus_2() { return static_cast<int32_t>(offsetof(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061, ___modulus_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_modulus_2() const { return ___modulus_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_modulus_2() { return &___modulus_2; }
	inline void set_modulus_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___modulus_2 = value;
		Il2CppCodeGenWriteBarrier((&___modulus_2), value);
	}

	inline static int32_t get_offset_of_exponent_3() { return static_cast<int32_t>(offsetof(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061, ___exponent_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_exponent_3() const { return ___exponent_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_exponent_3() { return &___exponent_3; }
	inline void set_exponent_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___exponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___exponent_3), value);
	}
};

struct RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_StaticFields
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters::SmallPrimesProduct
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___SmallPrimesProduct_1;

public:
	inline static int32_t get_offset_of_SmallPrimesProduct_1() { return static_cast<int32_t>(offsetof(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_StaticFields, ___SmallPrimesProduct_1)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_SmallPrimesProduct_1() const { return ___SmallPrimesProduct_1; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_SmallPrimesProduct_1() { return &___SmallPrimesProduct_1; }
	inline void set_SmallPrimesProduct_1(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___SmallPrimesProduct_1 = value;
		Il2CppCodeGenWriteBarrier((&___SmallPrimesProduct_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAKEYPARAMETERS_T1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_H
#ifndef X25519KEYGENERATIONPARAMETERS_TB44C7EA7F4EB85F01C9A6E1A0048FC0E218DB471_H
#define X25519KEYGENERATIONPARAMETERS_TB44C7EA7F4EB85F01C9A6E1A0048FC0E218DB471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519KeyGenerationParameters
struct  X25519KeyGenerationParameters_tB44C7EA7F4EB85F01C9A6E1A0048FC0E218DB471  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519KEYGENERATIONPARAMETERS_TB44C7EA7F4EB85F01C9A6E1A0048FC0E218DB471_H
#ifndef X25519PRIVATEKEYPARAMETERS_TB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_H
#define X25519PRIVATEKEYPARAMETERS_TB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PrivateKeyParameters
struct  X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PrivateKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_3;

public:
	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0, ___data_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_3() const { return ___data_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

struct X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PrivateKeyParameters::KeySize
	int32_t ___KeySize_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PrivateKeyParameters::SecretSize
	int32_t ___SecretSize_2;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}

	inline static int32_t get_offset_of_SecretSize_2() { return static_cast<int32_t>(offsetof(X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_StaticFields, ___SecretSize_2)); }
	inline int32_t get_SecretSize_2() const { return ___SecretSize_2; }
	inline int32_t* get_address_of_SecretSize_2() { return &___SecretSize_2; }
	inline void set_SecretSize_2(int32_t value)
	{
		___SecretSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519PRIVATEKEYPARAMETERS_TB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_H
#ifndef X25519PUBLICKEYPARAMETERS_T1741D1868EFA3BC449195D42F71BE97C3E4D59C2_H
#define X25519PUBLICKEYPARAMETERS_T1741D1868EFA3BC449195D42F71BE97C3E4D59C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PublicKeyParameters
struct  X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PublicKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2, ___data_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_2() const { return ___data_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X25519PublicKeyParameters::KeySize
	int32_t ___KeySize_1;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X25519PUBLICKEYPARAMETERS_T1741D1868EFA3BC449195D42F71BE97C3E4D59C2_H
#ifndef X448KEYGENERATIONPARAMETERS_T89F52EB945059497F148B39180F9A94B531615B6_H
#define X448KEYGENERATIONPARAMETERS_T89F52EB945059497F148B39180F9A94B531615B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448KeyGenerationParameters
struct  X448KeyGenerationParameters_t89F52EB945059497F148B39180F9A94B531615B6  : public KeyGenerationParameters_tC65EF43115EA8E516AD7410DCF741410A28744DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448KEYGENERATIONPARAMETERS_T89F52EB945059497F148B39180F9A94B531615B6_H
#ifndef X448PRIVATEKEYPARAMETERS_T4C72FF160390201C8FE5B492E01BD9CAD7B7C165_H
#define X448PRIVATEKEYPARAMETERS_T4C72FF160390201C8FE5B492E01BD9CAD7B7C165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PrivateKeyParameters
struct  X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PrivateKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_3;

public:
	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165, ___data_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_3() const { return ___data_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

struct X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PrivateKeyParameters::KeySize
	int32_t ___KeySize_1;
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PrivateKeyParameters::SecretSize
	int32_t ___SecretSize_2;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}

	inline static int32_t get_offset_of_SecretSize_2() { return static_cast<int32_t>(offsetof(X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165_StaticFields, ___SecretSize_2)); }
	inline int32_t get_SecretSize_2() const { return ___SecretSize_2; }
	inline int32_t* get_address_of_SecretSize_2() { return &___SecretSize_2; }
	inline void set_SecretSize_2(int32_t value)
	{
		___SecretSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448PRIVATEKEYPARAMETERS_T4C72FF160390201C8FE5B492E01BD9CAD7B7C165_H
#ifndef X448PUBLICKEYPARAMETERS_T0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_H
#define X448PUBLICKEYPARAMETERS_T0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PublicKeyParameters
struct  X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD  : public AsymmetricKeyParameter_t4F2CA810DA69334DB5E985540B64958EE26DF316
{
public:
	// System.Byte[] BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PublicKeyParameters::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD, ___data_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_2() const { return ___data_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_StaticFields
{
public:
	// System.Int32 BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.X448PublicKeyParameters::KeySize
	int32_t ___KeySize_1;

public:
	inline static int32_t get_offset_of_KeySize_1() { return static_cast<int32_t>(offsetof(X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_StaticFields, ___KeySize_1)); }
	inline int32_t get_KeySize_1() const { return ___KeySize_1; }
	inline int32_t* get_address_of_KeySize_1() { return &___KeySize_1; }
	inline void set_KeySize_1(int32_t value)
	{
		___KeySize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X448PUBLICKEYPARAMETERS_T0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_H
#ifndef PADDEDBUFFEREDBLOCKCIPHER_T026570B7948EF512AE70762D6513367D025D623F_H
#define PADDEDBUFFEREDBLOCKCIPHER_T026570B7948EF512AE70762D6513367D025D623F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.PaddedBufferedBlockCipher
struct  PaddedBufferedBlockCipher_t026570B7948EF512AE70762D6513367D025D623F  : public BufferedBlockCipher_t96540DEA3889C80D38AA07E627C1E32EEDCD161B
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Paddings.PaddedBufferedBlockCipher::padding
	RuntimeObject* ___padding_5;

public:
	inline static int32_t get_offset_of_padding_5() { return static_cast<int32_t>(offsetof(PaddedBufferedBlockCipher_t026570B7948EF512AE70762D6513367D025D623F, ___padding_5)); }
	inline RuntimeObject* get_padding_5() const { return ___padding_5; }
	inline RuntimeObject** get_address_of_padding_5() { return &___padding_5; }
	inline void set_padding_5(RuntimeObject* value)
	{
		___padding_5 = value;
		Il2CppCodeGenWriteBarrier((&___padding_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDEDBUFFEREDBLOCKCIPHER_T026570B7948EF512AE70762D6513367D025D623F_H
#ifndef DHPRIVATEKEYPARAMETERS_T4B25C84AD935D7BE9C3C3B860658729B5E8BBE33_H
#define DHPRIVATEKEYPARAMETERS_T4B25C84AD935D7BE9C3C3B860658729B5E8BBE33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct  DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33  : public DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters::x
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x_3;

public:
	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33, ___x_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x_3() const { return ___x_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x_3 = value;
		Il2CppCodeGenWriteBarrier((&___x_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPRIVATEKEYPARAMETERS_T4B25C84AD935D7BE9C3C3B860658729B5E8BBE33_H
#ifndef DHPUBLICKEYPARAMETERS_TA7F6F86A383110CC0DBE92FE44283FA0888BBA18_H
#define DHPUBLICKEYPARAMETERS_TA7F6F86A383110CC0DBE92FE44283FA0888BBA18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters
struct  DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18  : public DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters::y
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___y_3;

public:
	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18, ___y_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_y_3() const { return ___y_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___y_3 = value;
		Il2CppCodeGenWriteBarrier((&___y_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPUBLICKEYPARAMETERS_TA7F6F86A383110CC0DBE92FE44283FA0888BBA18_H
#ifndef DESEDEPARAMETERS_T0CF5C81339D0A618EBD596DC4E6F45A95795BEB7_H
#define DESEDEPARAMETERS_T0CF5C81339D0A618EBD596DC4E6F45A95795BEB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DesEdeParameters
struct  DesEdeParameters_t0CF5C81339D0A618EBD596DC4E6F45A95795BEB7  : public DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESEDEPARAMETERS_T0CF5C81339D0A618EBD596DC4E6F45A95795BEB7_H
#ifndef DSAPRIVATEKEYPARAMETERS_T4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9_H
#define DSAPRIVATEKEYPARAMETERS_T4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaPrivateKeyParameters
struct  DsaPrivateKeyParameters_t4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9  : public DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaPrivateKeyParameters::x
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(DsaPrivateKeyParameters_t4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9, ___x_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x_2() const { return ___x_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPRIVATEKEYPARAMETERS_T4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9_H
#ifndef DSAPUBLICKEYPARAMETERS_T52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96_H
#define DSAPUBLICKEYPARAMETERS_T52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaPublicKeyParameters
struct  DsaPublicKeyParameters_t52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96  : public DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.DsaPublicKeyParameters::y
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___y_2;

public:
	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(DsaPublicKeyParameters_t52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96, ___y_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_y_2() const { return ___y_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___y_2 = value;
		Il2CppCodeGenWriteBarrier((&___y_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPUBLICKEYPARAMETERS_T52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96_H
#ifndef ECGOST3410PARAMETERS_T608B36DB5B436EFFB885CDAB749EBE2AF10CA045_H
#define ECGOST3410PARAMETERS_T608B36DB5B436EFFB885CDAB749EBE2AF10CA045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECGost3410Parameters
struct  ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045  : public ECNamedDomainParameters_t2FF719D618AF7F27C295790427588AEEA7C6130C
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECGost3410Parameters::_publicKeyParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ____publicKeyParamSet_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECGost3410Parameters::_digestParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ____digestParamSet_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.DerObjectIdentifier BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECGost3410Parameters::_encryptionParamSet
	DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * ____encryptionParamSet_9;

public:
	inline static int32_t get_offset_of__publicKeyParamSet_7() { return static_cast<int32_t>(offsetof(ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045, ____publicKeyParamSet_7)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get__publicKeyParamSet_7() const { return ____publicKeyParamSet_7; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of__publicKeyParamSet_7() { return &____publicKeyParamSet_7; }
	inline void set__publicKeyParamSet_7(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		____publicKeyParamSet_7 = value;
		Il2CppCodeGenWriteBarrier((&____publicKeyParamSet_7), value);
	}

	inline static int32_t get_offset_of__digestParamSet_8() { return static_cast<int32_t>(offsetof(ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045, ____digestParamSet_8)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get__digestParamSet_8() const { return ____digestParamSet_8; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of__digestParamSet_8() { return &____digestParamSet_8; }
	inline void set__digestParamSet_8(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		____digestParamSet_8 = value;
		Il2CppCodeGenWriteBarrier((&____digestParamSet_8), value);
	}

	inline static int32_t get_offset_of__encryptionParamSet_9() { return static_cast<int32_t>(offsetof(ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045, ____encryptionParamSet_9)); }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * get__encryptionParamSet_9() const { return ____encryptionParamSet_9; }
	inline DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 ** get_address_of__encryptionParamSet_9() { return &____encryptionParamSet_9; }
	inline void set__encryptionParamSet_9(DerObjectIdentifier_t6D50C7785FE0773A3C6CFF7A550A85885C2D2676 * value)
	{
		____encryptionParamSet_9 = value;
		Il2CppCodeGenWriteBarrier((&____encryptionParamSet_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECGOST3410PARAMETERS_T608B36DB5B436EFFB885CDAB749EBE2AF10CA045_H
#ifndef ECPRIVATEKEYPARAMETERS_TF780A8BEB02E0945949DF77CA6768735B4959DEB_H
#define ECPRIVATEKEYPARAMETERS_TF780A8BEB02E0945949DF77CA6768735B4959DEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct  ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB  : public ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters::d
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___d_5;

public:
	inline static int32_t get_offset_of_d_5() { return static_cast<int32_t>(offsetof(ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB, ___d_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_d_5() const { return ___d_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_d_5() { return &___d_5; }
	inline void set_d_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___d_5 = value;
		Il2CppCodeGenWriteBarrier((&___d_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPRIVATEKEYPARAMETERS_TF780A8BEB02E0945949DF77CA6768735B4959DEB_H
#ifndef ECPUBLICKEYPARAMETERS_T65CE4BCD4C8A559651EBF253648BB213BB8664A2_H
#define ECPUBLICKEYPARAMETERS_T65CE4BCD4C8A559651EBF253648BB213BB8664A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters
struct  ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2  : public ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.EC.ECPoint BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters::q
	ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * ___q_5;

public:
	inline static int32_t get_offset_of_q_5() { return static_cast<int32_t>(offsetof(ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2, ___q_5)); }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * get_q_5() const { return ___q_5; }
	inline ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E ** get_address_of_q_5() { return &___q_5; }
	inline void set_q_5(ECPoint_t76C9C9A58D681A59C0795B257095B198DDC5518E * value)
	{
		___q_5 = value;
		Il2CppCodeGenWriteBarrier((&___q_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPUBLICKEYPARAMETERS_T65CE4BCD4C8A559651EBF253648BB213BB8664A2_H
#ifndef ELGAMALPRIVATEKEYPARAMETERS_TB86FAD53F1E0EC2B21761880F4FEF8DBE758220C_H
#define ELGAMALPRIVATEKEYPARAMETERS_TB86FAD53F1E0EC2B21761880F4FEF8DBE758220C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalPrivateKeyParameters
struct  ElGamalPrivateKeyParameters_tB86FAD53F1E0EC2B21761880F4FEF8DBE758220C  : public ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalPrivateKeyParameters::x
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(ElGamalPrivateKeyParameters_tB86FAD53F1E0EC2B21761880F4FEF8DBE758220C, ___x_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x_2() const { return ___x_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPRIVATEKEYPARAMETERS_TB86FAD53F1E0EC2B21761880F4FEF8DBE758220C_H
#ifndef ELGAMALPUBLICKEYPARAMETERS_TBAD68401F43C6387A79EA443917EE2A002B44B71_H
#define ELGAMALPUBLICKEYPARAMETERS_TBAD68401F43C6387A79EA443917EE2A002B44B71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalPublicKeyParameters
struct  ElGamalPublicKeyParameters_tBAD68401F43C6387A79EA443917EE2A002B44B71  : public ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.ElGamalPublicKeyParameters::y
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___y_2;

public:
	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(ElGamalPublicKeyParameters_tBAD68401F43C6387A79EA443917EE2A002B44B71, ___y_2)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_y_2() const { return ___y_2; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___y_2 = value;
		Il2CppCodeGenWriteBarrier((&___y_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPUBLICKEYPARAMETERS_TBAD68401F43C6387A79EA443917EE2A002B44B71_H
#ifndef GOST3410PRIVATEKEYPARAMETERS_TB9208FD77EF044CDA619A9A67EC53E8A910614BC_H
#define GOST3410PRIVATEKEYPARAMETERS_TB9208FD77EF044CDA619A9A67EC53E8A910614BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410PrivateKeyParameters
struct  Gost3410PrivateKeyParameters_tB9208FD77EF044CDA619A9A67EC53E8A910614BC  : public Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410PrivateKeyParameters::x
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___x_3;

public:
	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(Gost3410PrivateKeyParameters_tB9208FD77EF044CDA619A9A67EC53E8A910614BC, ___x_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_x_3() const { return ___x_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___x_3 = value;
		Il2CppCodeGenWriteBarrier((&___x_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PRIVATEKEYPARAMETERS_TB9208FD77EF044CDA619A9A67EC53E8A910614BC_H
#ifndef GOST3410PUBLICKEYPARAMETERS_T54EF9C7A71690A23E9BF0D5240DDF2D335D9431D_H
#define GOST3410PUBLICKEYPARAMETERS_T54EF9C7A71690A23E9BF0D5240DDF2D335D9431D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410PublicKeyParameters
struct  Gost3410PublicKeyParameters_t54EF9C7A71690A23E9BF0D5240DDF2D335D9431D  : public Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.Gost3410PublicKeyParameters::y
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___y_3;

public:
	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyParameters_t54EF9C7A71690A23E9BF0D5240DDF2D335D9431D, ___y_3)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_y_3() const { return ___y_3; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___y_3 = value;
		Il2CppCodeGenWriteBarrier((&___y_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PUBLICKEYPARAMETERS_T54EF9C7A71690A23E9BF0D5240DDF2D335D9431D_H
#ifndef NACCACHESTERNPRIVATEKEYPARAMETERS_TC3405563CEE5B2335A494F1A0E01C8D0A90421CB_H
#define NACCACHESTERNPRIVATEKEYPARAMETERS_TC3405563CEE5B2335A494F1A0E01C8D0A90421CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternPrivateKeyParameters
struct  NaccacheSternPrivateKeyParameters_tC3405563CEE5B2335A494F1A0E01C8D0A90421CB  : public NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternPrivateKeyParameters::phiN
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___phiN_4;
	// System.Collections.IList BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.NaccacheSternPrivateKeyParameters::smallPrimes
	RuntimeObject* ___smallPrimes_5;

public:
	inline static int32_t get_offset_of_phiN_4() { return static_cast<int32_t>(offsetof(NaccacheSternPrivateKeyParameters_tC3405563CEE5B2335A494F1A0E01C8D0A90421CB, ___phiN_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_phiN_4() const { return ___phiN_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_phiN_4() { return &___phiN_4; }
	inline void set_phiN_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___phiN_4 = value;
		Il2CppCodeGenWriteBarrier((&___phiN_4), value);
	}

	inline static int32_t get_offset_of_smallPrimes_5() { return static_cast<int32_t>(offsetof(NaccacheSternPrivateKeyParameters_tC3405563CEE5B2335A494F1A0E01C8D0A90421CB, ___smallPrimes_5)); }
	inline RuntimeObject* get_smallPrimes_5() const { return ___smallPrimes_5; }
	inline RuntimeObject** get_address_of_smallPrimes_5() { return &___smallPrimes_5; }
	inline void set_smallPrimes_5(RuntimeObject* value)
	{
		___smallPrimes_5 = value;
		Il2CppCodeGenWriteBarrier((&___smallPrimes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NACCACHESTERNPRIVATEKEYPARAMETERS_TC3405563CEE5B2335A494F1A0E01C8D0A90421CB_H
#ifndef RSAPRIVATECRTKEYPARAMETERS_TD379637B7AFC5F37C765A75F5E3341F9AA36BC72_H
#define RSAPRIVATECRTKEYPARAMETERS_TD379637B7AFC5F37C765A75F5E3341F9AA36BC72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters
struct  RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72  : public RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061
{
public:
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::e
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___e_4;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::p
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___p_5;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::q
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___q_6;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::dP
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___dP_7;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::dQ
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___dQ_8;
	// BestHTTP.SecureProtocol.Org.BouncyCastle.Math.BigInteger BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::qInv
	BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * ___qInv_9;

public:
	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72, ___e_4)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_e_4() const { return ___e_4; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((&___e_4), value);
	}

	inline static int32_t get_offset_of_p_5() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72, ___p_5)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_p_5() const { return ___p_5; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_p_5() { return &___p_5; }
	inline void set_p_5(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___p_5 = value;
		Il2CppCodeGenWriteBarrier((&___p_5), value);
	}

	inline static int32_t get_offset_of_q_6() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72, ___q_6)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_q_6() const { return ___q_6; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_q_6() { return &___q_6; }
	inline void set_q_6(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___q_6 = value;
		Il2CppCodeGenWriteBarrier((&___q_6), value);
	}

	inline static int32_t get_offset_of_dP_7() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72, ___dP_7)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_dP_7() const { return ___dP_7; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_dP_7() { return &___dP_7; }
	inline void set_dP_7(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___dP_7 = value;
		Il2CppCodeGenWriteBarrier((&___dP_7), value);
	}

	inline static int32_t get_offset_of_dQ_8() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72, ___dQ_8)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_dQ_8() const { return ___dQ_8; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_dQ_8() { return &___dQ_8; }
	inline void set_dQ_8(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___dQ_8 = value;
		Il2CppCodeGenWriteBarrier((&___dQ_8), value);
	}

	inline static int32_t get_offset_of_qInv_9() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72, ___qInv_9)); }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * get_qInv_9() const { return ___qInv_9; }
	inline BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 ** get_address_of_qInv_9() { return &___qInv_9; }
	inline void set_qInv_9(BigInteger_t8778D39F01FE244D83FC7EEEE80DFFFFD2700E50 * value)
	{
		___qInv_9 = value;
		Il2CppCodeGenWriteBarrier((&___qInv_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPRIVATECRTKEYPARAMETERS_TD379637B7AFC5F37C765A75F5E3341F9AA36BC72_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4700 = { sizeof (DesEdeParameters_t0CF5C81339D0A618EBD596DC4E6F45A95795BEB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4700[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4701 = { sizeof (DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD), -1, sizeof(DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4701[3] = 
{
	0,
	0,
	DesParameters_t5FE03E9F68A7690CA07C671D4A1275E127E376CD_StaticFields::get_offset_of_DES_weak_keys_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4702 = { sizeof (DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4702[1] = 
{
	DHKeyGenerationParameters_t50085347D3474105C013CCBC0548EF56AF2F2960::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4703 = { sizeof (DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4703[2] = 
{
	DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05::get_offset_of_parameters_1(),
	DHKeyParameters_t30526534F8A1CD79F62C7E83D46A44471D950F05::get_offset_of_algorithmOid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4704 = { sizeof (DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4704[8] = 
{
	0,
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_p_1(),
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_g_2(),
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_q_3(),
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_j_4(),
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_m_5(),
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_l_6(),
	DHParameters_tC2CDBB48EE0ABA9685B888746C76E72685DD2E67::get_offset_of_validation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4705 = { sizeof (DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4705[1] = 
{
	DHPrivateKeyParameters_t4B25C84AD935D7BE9C3C3B860658729B5E8BBE33::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4706 = { sizeof (DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4706[1] = 
{
	DHPublicKeyParameters_tA7F6F86A383110CC0DBE92FE44283FA0888BBA18::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4707 = { sizeof (DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4707[2] = 
{
	DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB::get_offset_of_seed_0(),
	DHValidationParameters_t92186C1B030FD60B8BDC80B439C1375BEF4239DB::get_offset_of_counter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4708 = { sizeof (DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4708[1] = 
{
	DsaKeyGenerationParameters_t4F8D351530FCA2EAF46A994C4E26FF4772C3D707::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4709 = { sizeof (DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4709[1] = 
{
	DsaKeyParameters_t2F888CF39888FC27EEBA7C537DA165CC4CDB5043::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4710 = { sizeof (DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4710[7] = 
{
	0,
	0,
	DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D::get_offset_of_l_2(),
	DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D::get_offset_of_n_3(),
	DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D::get_offset_of_certainty_4(),
	DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D::get_offset_of_random_5(),
	DsaParameterGenerationParameters_t49A86E2CB443A80D9211265D28EC5E38C96C842D::get_offset_of_usageIndex_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4711 = { sizeof (DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4711[4] = 
{
	DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534::get_offset_of_p_0(),
	DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534::get_offset_of_q_1(),
	DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534::get_offset_of_g_2(),
	DsaParameters_t61302B2C4C47647A233F4DC9423258244C712534::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4712 = { sizeof (DsaPrivateKeyParameters_t4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4712[1] = 
{
	DsaPrivateKeyParameters_t4BFC784B8317E4AFD230B88B64FD8E2EFB7A84A9::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4713 = { sizeof (DsaPublicKeyParameters_t52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4713[1] = 
{
	DsaPublicKeyParameters_t52A25BE10A5FE870B54A0BCB4DA5A0AFE85E1A96::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4714 = { sizeof (DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4714[3] = 
{
	DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E::get_offset_of_seed_0(),
	DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E::get_offset_of_counter_1(),
	DsaValidationParameters_tCF23DA7777D305A869ECA66E80DF0A3D76948F9E::get_offset_of_usageIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4715 = { sizeof (ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4715[6] = 
{
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C::get_offset_of_curve_0(),
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C::get_offset_of_seed_1(),
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C::get_offset_of_g_2(),
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C::get_offset_of_n_3(),
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C::get_offset_of_h_4(),
	ECDomainParameters_t287E469316C77EE39705286EB543B0CFB25F675C::get_offset_of_hInv_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4716 = { sizeof (ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4716[3] = 
{
	ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045::get_offset_of__publicKeyParamSet_7(),
	ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045::get_offset_of__digestParamSet_8(),
	ECGost3410Parameters_t608B36DB5B436EFFB885CDAB749EBE2AF10CA045::get_offset_of__encryptionParamSet_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4717 = { sizeof (ECKeyGenerationParameters_t25868784B11E49365A5AEF64EE1964F424E48DC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4717[2] = 
{
	ECKeyGenerationParameters_t25868784B11E49365A5AEF64EE1964F424E48DC4::get_offset_of_domainParams_2(),
	ECKeyGenerationParameters_t25868784B11E49365A5AEF64EE1964F424E48DC4::get_offset_of_publicKeyParamSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4718 = { sizeof (ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4), -1, sizeof(ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4718[4] = 
{
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4_StaticFields::get_offset_of_algorithms_1(),
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4::get_offset_of_algorithm_2(),
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4::get_offset_of_parameters_3(),
	ECKeyParameters_tD7169A9268EEA24D8539790EFDF085A7BEFD92A4::get_offset_of_publicKeyParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4719 = { sizeof (ECNamedDomainParameters_t2FF719D618AF7F27C295790427588AEEA7C6130C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4719[1] = 
{
	ECNamedDomainParameters_t2FF719D618AF7F27C295790427588AEEA7C6130C::get_offset_of_name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4720 = { sizeof (ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4720[1] = 
{
	ECPrivateKeyParameters_tF780A8BEB02E0945949DF77CA6768735B4959DEB::get_offset_of_d_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4721 = { sizeof (ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4721[1] = 
{
	ECPublicKeyParameters_t65CE4BCD4C8A559651EBF253648BB213BB8664A2::get_offset_of_q_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4722 = { sizeof (Ed25519KeyGenerationParameters_t0F845E438E1B920F65A02FD1CA16EB0B96CC4AA9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4723 = { sizeof (Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2), -1, sizeof(Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4723[3] = 
{
	Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_StaticFields::get_offset_of_KeySize_1(),
	Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2_StaticFields::get_offset_of_SignatureSize_2(),
	Ed25519PrivateKeyParameters_tDC6A2BE278E55BE5CF2C9987AF51A0679602B2C2::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4724 = { sizeof (Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3), -1, sizeof(Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4724[2] = 
{
	Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3_StaticFields::get_offset_of_KeySize_1(),
	Ed25519PublicKeyParameters_t08D7A9A6A51B1294356CDB750C83E13A5CBBA2F3::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4725 = { sizeof (Ed448KeyGenerationParameters_tA45CC70C374EA6F3B547E9B2B6FF94E300B714E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4726 = { sizeof (Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D), -1, sizeof(Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4726[3] = 
{
	Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D_StaticFields::get_offset_of_KeySize_1(),
	Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D_StaticFields::get_offset_of_SignatureSize_2(),
	Ed448PrivateKeyParameters_tCAD4F3779488E120C11341492A4B0E47BAB4FC7D::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4727 = { sizeof (Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B), -1, sizeof(Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4727[2] = 
{
	Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B_StaticFields::get_offset_of_KeySize_1(),
	Ed448PublicKeyParameters_t61B93C0612B5B99EAD9767ED6A59A4F962F7E80B::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4728 = { sizeof (ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4728[1] = 
{
	ElGamalKeyGenerationParameters_t6CF94D2D230374AB3FEDDA99200304B0749409D0::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4729 = { sizeof (ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4729[1] = 
{
	ElGamalKeyParameters_t7810C31A0559BEA387ACAC593E129F0A179CF7BB::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4730 = { sizeof (ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4730[3] = 
{
	ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F::get_offset_of_p_0(),
	ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F::get_offset_of_g_1(),
	ElGamalParameters_t4899578B25063061E5CBDC44EFE67936D6CB831F::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4731 = { sizeof (ElGamalPrivateKeyParameters_tB86FAD53F1E0EC2B21761880F4FEF8DBE758220C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4731[1] = 
{
	ElGamalPrivateKeyParameters_tB86FAD53F1E0EC2B21761880F4FEF8DBE758220C::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4732 = { sizeof (ElGamalPublicKeyParameters_tBAD68401F43C6387A79EA443917EE2A002B44B71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4732[1] = 
{
	ElGamalPublicKeyParameters_tBAD68401F43C6387A79EA443917EE2A002B44B71::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4733 = { sizeof (Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4733[2] = 
{
	Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8::get_offset_of_parameters_2(),
	Gost3410KeyGenerationParameters_t224CD9217AA6CBD910DC7DDEB7427DFD5C3237D8::get_offset_of_publicKeyParamSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4734 = { sizeof (Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4734[2] = 
{
	Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186::get_offset_of_parameters_1(),
	Gost3410KeyParameters_t5F3A9D8B8F093472F8C7FEE592B46AB964910186::get_offset_of_publicKeyParamSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4735 = { sizeof (Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4735[4] = 
{
	Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD::get_offset_of_p_0(),
	Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD::get_offset_of_q_1(),
	Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD::get_offset_of_a_2(),
	Gost3410Parameters_tA94CA8C82149106253AEAF4B4F0D1ACB1CC87CAD::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4736 = { sizeof (Gost3410PrivateKeyParameters_tB9208FD77EF044CDA619A9A67EC53E8A910614BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4736[1] = 
{
	Gost3410PrivateKeyParameters_tB9208FD77EF044CDA619A9A67EC53E8A910614BC::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4737 = { sizeof (Gost3410PublicKeyParameters_t54EF9C7A71690A23E9BF0D5240DDF2D335D9431D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4737[1] = 
{
	Gost3410PublicKeyParameters_t54EF9C7A71690A23E9BF0D5240DDF2D335D9431D::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4738 = { sizeof (Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4738[4] = 
{
	Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C::get_offset_of_x0_0(),
	Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C::get_offset_of_c_1(),
	Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C::get_offset_of_x0L_2(),
	Gost3410ValidationParameters_tC6593A30220CEC644C5042BD9FEF96F7FFBBF80C::get_offset_of_cL_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4739 = { sizeof (HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4739[4] = 
{
	HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412::get_offset_of_ikm_0(),
	HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412::get_offset_of_skipExpand_1(),
	HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412::get_offset_of_salt_2(),
	HkdfParameters_tDCBBC50CA4B7A2D0F3B8759E152E60DCAC66A412::get_offset_of_info_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4740 = { sizeof (IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4740[3] = 
{
	IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9::get_offset_of_derivation_0(),
	IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9::get_offset_of_encoding_1(),
	IesParameters_tE48597AA27E20B508E9B1CFBD9CFA7C5E95F51A9::get_offset_of_macKeySize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4741 = { sizeof (IesWithCipherParameters_t48E566D48377A1743A77C0695EE0C4E55A3A960C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4741[1] = 
{
	IesWithCipherParameters_t48E566D48377A1743A77C0695EE0C4E55A3A960C::get_offset_of_cipherKeySize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4742 = { sizeof (Iso18033KdfParameters_t652F8A1E3074919A62DE5FFCCB38DA96406524C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4742[1] = 
{
	Iso18033KdfParameters_t652F8A1E3074919A62DE5FFCCB38DA96406524C3::get_offset_of_seed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4743 = { sizeof (KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4743[4] = 
{
	KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3::get_offset_of_ki_0(),
	KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3::get_offset_of_fixedInputDataCounterPrefix_1(),
	KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3::get_offset_of_fixedInputDataCounterSuffix_2(),
	KdfCounterParameters_t0BE74AF72F0AF75AD56789922A5B18EBB7F8ABC3::get_offset_of_r_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4744 = { sizeof (KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A), -1, sizeof(KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4744[5] = 
{
	KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A_StaticFields::get_offset_of_UNUSED_R_0(),
	KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A::get_offset_of_ki_1(),
	KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A::get_offset_of_useCounter_2(),
	KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A::get_offset_of_r_3(),
	KdfDoublePipelineIterationParameters_tD6FEF0A05887FA21928DC7DB9263ACA164CAB88A::get_offset_of_fixedInputData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4745 = { sizeof (KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF), -1, sizeof(KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4745[6] = 
{
	KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF_StaticFields::get_offset_of_UNUSED_R_0(),
	KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF::get_offset_of_ki_1(),
	KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF::get_offset_of_iv_2(),
	KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF::get_offset_of_useCounter_3(),
	KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF::get_offset_of_r_4(),
	KdfFeedbackParameters_t946A85C47127FC4E406A55D75D429D6A04ED26BF::get_offset_of_fixedInputData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4746 = { sizeof (KdfParameters_t5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4746[2] = 
{
	KdfParameters_t5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C::get_offset_of_iv_0(),
	KdfParameters_t5AC8FFC4F38CB6C347B9028AC4FEE6EEF770FF2C::get_offset_of_shared_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4747 = { sizeof (KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4747[1] = 
{
	KeyParameter_tB6DE772ACC72C04D32FFC0DD4AF033F54998D41F::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4748 = { sizeof (MgfParameters_t5F8B84C67C715344E4843CA5A7DA77C290E2943E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4748[1] = 
{
	MgfParameters_t5F8B84C67C715344E4843CA5A7DA77C290E2943E::get_offset_of_seed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4749 = { sizeof (MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4749[3] = 
{
	MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7::get_offset_of_staticPrivateKey_0(),
	MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7::get_offset_of_ephemeralPrivateKey_1(),
	MqvPrivateParameters_tEB0B554F5B5D1525D06D47F25F611671B39B8DD7::get_offset_of_ephemeralPublicKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4750 = { sizeof (MqvPublicParameters_t414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4750[2] = 
{
	MqvPublicParameters_t414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB::get_offset_of_staticPublicKey_0(),
	MqvPublicParameters_t414260F28BEA1EF162D67F0ECCAA309B4A9E0CFB::get_offset_of_ephemeralPublicKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4751 = { sizeof (NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4751[2] = 
{
	NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09::get_offset_of_certainty_2(),
	NaccacheSternKeyGenerationParameters_t4A5485405836465214ED400316F6D3061340FA09::get_offset_of_countSmallPrimes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4752 = { sizeof (NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4752[3] = 
{
	NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7::get_offset_of_g_1(),
	NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7::get_offset_of_n_2(),
	NaccacheSternKeyParameters_tC6C264C943C2C7FF44064DE0539DAB31159703B7::get_offset_of_lowerSigmaBound_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4753 = { sizeof (NaccacheSternPrivateKeyParameters_tC3405563CEE5B2335A494F1A0E01C8D0A90421CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4753[2] = 
{
	NaccacheSternPrivateKeyParameters_tC3405563CEE5B2335A494F1A0E01C8D0A90421CB::get_offset_of_phiN_4(),
	NaccacheSternPrivateKeyParameters_tC3405563CEE5B2335A494F1A0E01C8D0A90421CB::get_offset_of_smallPrimes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4754 = { sizeof (ParametersWithID_t75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4754[2] = 
{
	ParametersWithID_t75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99::get_offset_of_parameters_0(),
	ParametersWithID_t75101CDA93844994CEED6E5B0AB7B3FBE8FDFD99::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4755 = { sizeof (ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4755[2] = 
{
	ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF::get_offset_of_parameters_0(),
	ParametersWithIV_tF99B387ECC76C2CF62CA5FF6C4E4C2F1E81189FF::get_offset_of_iv_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4756 = { sizeof (ParametersWithRandom_tD20AFD747DC0851C0F83D7E031D98D0D81B6FB36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4756[2] = 
{
	ParametersWithRandom_tD20AFD747DC0851C0F83D7E031D98D0D81B6FB36::get_offset_of_parameters_0(),
	ParametersWithRandom_tD20AFD747DC0851C0F83D7E031D98D0D81B6FB36::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4757 = { sizeof (ParametersWithSalt_tB8CF4A4C41A9BB671A30AF1B326CA09C18257201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4757[2] = 
{
	ParametersWithSalt_tB8CF4A4C41A9BB671A30AF1B326CA09C18257201::get_offset_of_salt_0(),
	ParametersWithSalt_tB8CF4A4C41A9BB671A30AF1B326CA09C18257201::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4758 = { sizeof (ParametersWithSBox_t2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4758[2] = 
{
	ParametersWithSBox_t2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA::get_offset_of_parameters_0(),
	ParametersWithSBox_t2F29182D49A6B45FBC86B5ACE0B0085F2B5A21AA::get_offset_of_sBox_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4759 = { sizeof (RC2Parameters_tB51A8CA0415E4E3B5B942EF74B377CF937163E87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4759[1] = 
{
	RC2Parameters_tB51A8CA0415E4E3B5B942EF74B377CF937163E87::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4760 = { sizeof (RC5Parameters_t7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4760[1] = 
{
	RC5Parameters_t7926D2D6ACF00A0C2A91C668A15CA86ADEC3F1BC::get_offset_of_rounds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4761 = { sizeof (RsaBlindingParameters_t9FF63A05ACE679FB85798FB7E65B0EE256123195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4761[2] = 
{
	RsaBlindingParameters_t9FF63A05ACE679FB85798FB7E65B0EE256123195::get_offset_of_publicKey_0(),
	RsaBlindingParameters_t9FF63A05ACE679FB85798FB7E65B0EE256123195::get_offset_of_blindingFactor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4762 = { sizeof (RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4762[2] = 
{
	RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD::get_offset_of_publicExponent_2(),
	RsaKeyGenerationParameters_tADAFF6887525BB5FE50D94B53E1B8D8032DB08AD::get_offset_of_certainty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4763 = { sizeof (RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061), -1, sizeof(RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4763[3] = 
{
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061_StaticFields::get_offset_of_SmallPrimesProduct_1(),
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061::get_offset_of_modulus_2(),
	RsaKeyParameters_t1EA2F8E01D30BD1B10C376D0E6BB2510030EA061::get_offset_of_exponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4764 = { sizeof (RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4764[6] = 
{
	RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72::get_offset_of_e_4(),
	RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72::get_offset_of_p_5(),
	RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72::get_offset_of_q_6(),
	RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72::get_offset_of_dP_7(),
	RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72::get_offset_of_dQ_8(),
	RsaPrivateCrtKeyParameters_tD379637B7AFC5F37C765A75F5E3341F9AA36BC72::get_offset_of_qInv_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4765 = { sizeof (SkeinParameters_t1161D508A7E57A7FF552002C6A965D88FEAA30CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4765[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SkeinParameters_t1161D508A7E57A7FF552002C6A965D88FEAA30CE::get_offset_of_parameters_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4766 = { sizeof (Builder_t38FB7A5A57D5FBF9309ED347F94E11F0476ADC83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4766[1] = 
{
	Builder_t38FB7A5A57D5FBF9309ED347F94E11F0476ADC83::get_offset_of_parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4767 = { sizeof (SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4767[5] = 
{
	SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074::get_offset_of_mInitiator_0(),
	SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074::get_offset_of_mStaticPrivateKey_1(),
	SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074::get_offset_of_mStaticPublicPoint_2(),
	SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074::get_offset_of_mEphemeralPrivateKey_3(),
	SM2KeyExchangePrivateParameters_tF53EDD1ADECB805A8228E58A8DC7FDD901A85074::get_offset_of_mEphemeralPublicPoint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4768 = { sizeof (SM2KeyExchangePublicParameters_t13AB9E3780843E3790430C619A364EB5AFAB4346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4768[2] = 
{
	SM2KeyExchangePublicParameters_t13AB9E3780843E3790430C619A364EB5AFAB4346::get_offset_of_mStaticPublicKey_0(),
	SM2KeyExchangePublicParameters_t13AB9E3780843E3790430C619A364EB5AFAB4346::get_offset_of_mEphemeralPublicKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4769 = { sizeof (Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4769[2] = 
{
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464::get_offset_of_n_0(),
	Srp6GroupParameters_t61D9D4DF2B8B26B3ADB45AF4ECADE71B0072F464::get_offset_of_g_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4770 = { sizeof (TweakableBlockCipherParameters_t29A387C39129AEFF81B62207DEB81E3036F31488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4770[2] = 
{
	TweakableBlockCipherParameters_t29A387C39129AEFF81B62207DEB81E3036F31488::get_offset_of_tweak_0(),
	TweakableBlockCipherParameters_t29A387C39129AEFF81B62207DEB81E3036F31488::get_offset_of_key_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4771 = { sizeof (X25519KeyGenerationParameters_tB44C7EA7F4EB85F01C9A6E1A0048FC0E218DB471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4772 = { sizeof (X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0), -1, sizeof(X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4772[3] = 
{
	X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_StaticFields::get_offset_of_KeySize_1(),
	X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0_StaticFields::get_offset_of_SecretSize_2(),
	X25519PrivateKeyParameters_tB935E1360D1A56F3A47472DEDAE9CA34912A4CE0::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4773 = { sizeof (X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2), -1, sizeof(X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4773[2] = 
{
	X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2_StaticFields::get_offset_of_KeySize_1(),
	X25519PublicKeyParameters_t1741D1868EFA3BC449195D42F71BE97C3E4D59C2::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4774 = { sizeof (X448KeyGenerationParameters_t89F52EB945059497F148B39180F9A94B531615B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4775 = { sizeof (X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165), -1, sizeof(X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4775[3] = 
{
	X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165_StaticFields::get_offset_of_KeySize_1(),
	X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165_StaticFields::get_offset_of_SecretSize_2(),
	X448PrivateKeyParameters_t4C72FF160390201C8FE5B492E01BD9CAD7B7C165::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4776 = { sizeof (X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD), -1, sizeof(X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4776[2] = 
{
	X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD_StaticFields::get_offset_of_KeySize_1(),
	X448PublicKeyParameters_t0D3A7B5AF2F4C18852183CA5FB7BABBF1497B6FD::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4778 = { sizeof (ISO10126d2Padding_t330B1A4EC476C1029D6E6B53E3721FF16A3856AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4778[1] = 
{
	ISO10126d2Padding_t330B1A4EC476C1029D6E6B53E3721FF16A3856AF::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4779 = { sizeof (ISO7816d4Padding_t415A35B77A1949B24FFDD1C389E52FA9C377E866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4780 = { sizeof (PaddedBufferedBlockCipher_t026570B7948EF512AE70762D6513367D025D623F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4780[1] = 
{
	PaddedBufferedBlockCipher_t026570B7948EF512AE70762D6513367D025D623F::get_offset_of_padding_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4781 = { sizeof (Pkcs7Padding_t770374C8871A08871A8CD32E793C43DD785B7912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4782 = { sizeof (TbcPadding_t56BCA9C7439648B66B64620D690A875E2757329E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4783 = { sizeof (X923Padding_t2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4783[1] = 
{
	X923Padding_t2B99D85CBCEC4F6044BC5C1AF2132812ED061C5C::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4784 = { sizeof (ZeroBytePadding_t53C60734473757BDEECC745647E2BFD3F03E27B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4785 = { sizeof (Asn1CipherBuilderWithKey_t6F3882D30EAED60278C261779CCC4D8DE035D04A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4785[2] = 
{
	Asn1CipherBuilderWithKey_t6F3882D30EAED60278C261779CCC4D8DE035D04A::get_offset_of_encKey_0(),
	Asn1CipherBuilderWithKey_t6F3882D30EAED60278C261779CCC4D8DE035D04A::get_offset_of_algorithmIdentifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4786 = { sizeof (BufferedCipherWrapper_t8E695A9EB33E2251EFA53AF37B8005E282FE7F55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4786[2] = 
{
	BufferedCipherWrapper_t8E695A9EB33E2251EFA53AF37B8005E282FE7F55::get_offset_of_bufferedCipher_0(),
	BufferedCipherWrapper_t8E695A9EB33E2251EFA53AF37B8005E282FE7F55::get_offset_of_stream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4787 = { sizeof (Asn1KeyWrapper_t8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4787[2] = 
{
	Asn1KeyWrapper_t8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA::get_offset_of_algorithm_0(),
	Asn1KeyWrapper_t8A59A165F7211B955C1ACBAD56D42CE6BC47E8EA::get_offset_of_wrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4788 = { sizeof (KeyWrapperUtil_t802932A483358268281449792AA9AB369C581915), -1, sizeof(KeyWrapperUtil_t802932A483358268281449792AA9AB369C581915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4788[1] = 
{
	KeyWrapperUtil_t802932A483358268281449792AA9AB369C581915_StaticFields::get_offset_of_providerMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4789 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4790 = { sizeof (RsaOaepWrapper_tCA776C88195418E3188B9A20AB6020BDBEF0C19B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4790[2] = 
{
	RsaOaepWrapper_tCA776C88195418E3188B9A20AB6020BDBEF0C19B::get_offset_of_algId_0(),
	RsaOaepWrapper_tCA776C88195418E3188B9A20AB6020BDBEF0C19B::get_offset_of_engine_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4791 = { sizeof (RsaOaepWrapperProvider_t93774444FF1BF5B541170264C3EF9408B4EBFFEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4791[1] = 
{
	RsaOaepWrapperProvider_t93774444FF1BF5B541170264C3EF9408B4EBFFEB::get_offset_of_digestOid_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4792 = { sizeof (X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512), -1, sizeof(X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4792[4] = 
{
	X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields::get_offset_of_derNull_0(),
	X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields::get_offset_of_algorithms_1(),
	X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields::get_offset_of_exParams_2(),
	X509Utilities_t74DE6D9FF2E9EB94231CB3527C9627E2EEE0A512_StaticFields::get_offset_of_noParams_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4793 = { sizeof (Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4793[4] = 
{
	Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09::get_offset_of_algID_0(),
	Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09::get_offset_of_algorithm_1(),
	Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09::get_offset_of_privateKey_2(),
	Asn1SignatureFactory_t187B7704786D411A22847D24C5F313A98D8B9B09::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4794 = { sizeof (Asn1VerifierFactory_tD81A626F1B292CBBEB88C6E5F1EC604A66B2261B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4794[2] = 
{
	Asn1VerifierFactory_tD81A626F1B292CBBEB88C6E5F1EC604A66B2261B::get_offset_of_algID_0(),
	Asn1VerifierFactory_tD81A626F1B292CBBEB88C6E5F1EC604A66B2261B::get_offset_of_publicKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4795 = { sizeof (Asn1VerifierFactoryProvider_t38B79CCF7189D3F6EAAFD405ED172DC451D4022F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4795[1] = 
{
	Asn1VerifierFactoryProvider_t38B79CCF7189D3F6EAAFD405ED172DC451D4022F::get_offset_of_publicKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4796 = { sizeof (DefaultSignatureCalculator_t4F2B735D91C53E6BB3A5E5D26131006E5A63FB77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4796[1] = 
{
	DefaultSignatureCalculator_t4F2B735D91C53E6BB3A5E5D26131006E5A63FB77::get_offset_of_mSignerSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4797 = { sizeof (DefaultSignatureResult_t698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4797[1] = 
{
	DefaultSignatureResult_t698DE6D1DE57DAD7CF3FB8DA00D4FF8273CCD605::get_offset_of_mSigner_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4798 = { sizeof (DefaultVerifierCalculator_t262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4798[1] = 
{
	DefaultVerifierCalculator_t262C4BFAC63E4F8A3EA7E89A7A9D3C9D24DF3ECF::get_offset_of_mSignerSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4799 = { sizeof (DefaultVerifierResult_t5BC74071A5A8777E7034F82570FDCCD3D06DA889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4799[1] = 
{
	DefaultVerifierResult_t5BC74071A5A8777E7034F82570FDCCD3D06DA889::get_offset_of_mSigner_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
