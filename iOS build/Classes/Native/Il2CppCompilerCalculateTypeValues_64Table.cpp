﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// IndividualVideoDataDemo
struct IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1;
// Newtonsoft.Json.Linq.JObject
struct JObject_t786AF07B1009334856B0362BBC48EEF68C81C585;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.String>
struct Action_1_t19CAF500829927B30EC94F39939F749E4919669E;
// System.Action`1<YoutubeChannel[]>
struct Action_1_tD73791EE6B43F1321439ACC8583C8983BCFCAD6E;
// System.Action`1<YoutubeComments[]>
struct Action_1_t7942E6C460FE9C8561ECEC02C035275A648E97A9;
// System.Action`1<YoutubeData>
struct Action_1_t9B4B05046062A7D2297C5AD2FBB47148F601A1CB;
// System.Action`1<YoutubeData[]>
struct Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472;
// System.Action`1<YoutubePlaylistItems[]>
struct Action_1_tCA045A458C1E10FB3B86DFA5564560AC0505B845;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3;
// System.Collections.Generic.List`1<YoutubeData>
struct List_1_t7A6300D520BCEB6B1E2EBFBCECE00056B526E0DB;
// System.Collections.Generic.List`1<YoutubeLight.VideoInfo>
struct List_1_t7334C42E62574D2DD9BDF1730A044AA1CEECEC26;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Light[]
struct LightU5BU5D_t0127F29C5C02312DE2DDA721E3AF8CE925297D45;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// YoutubeAPIManager
struct YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA;
// YoutubeApiGetUnlimitedVideos
struct YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295;
// YoutubeChannelUI
struct YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15;
// YoutubeChannel[]
struct YoutubeChannelU5BU5D_t764C46D6F1C16A738F5C14BE6CB8EA36A5819095;
// YoutubeComments[]
struct YoutubeCommentsU5BU5D_t404F53A20F66D5C10BFD71572D5CF42B58189391;
// YoutubeContentDetails
struct YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C;
// YoutubeData
struct YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524;
// YoutubeData[]
struct YoutubeDataU5BU5D_t88CB245129BD34373C8542E7C2D6D99C5A62F246;
// YoutubePlayer
struct YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9;
// YoutubePlayer/DownloadUrlResponse
struct DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7;
// YoutubePlayer/ExtractionInfo
struct ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491;
// YoutubePlayer/YoutubeResultIds
struct YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72;
// YoutubePlayerLivestream
struct YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781;
// YoutubePlayerLivestream/DownloadUrlResponse
struct DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0;
// YoutubePlaylistItems[]
struct YoutubePlaylistItemsU5BU5D_t0B56487C233EB3272A08008F13D1C0FF2E09FDE9;
// YoutubeSnippet
struct YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40;
// YoutubeStatistics
struct YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC;
// YoutubeVideoUi
struct YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802;
// YoutubeVideoUi[]
struct YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXTENSIONS_T4F879846ED647EF193E870DE09265831DF591835_H
#define EXTENSIONS_T4F879846ED647EF193E870DE09265831DF591835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Extensions
struct  Extensions_t4F879846ED647EF193E870DE09265831DF591835  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T4F879846ED647EF193E870DE09265831DF591835_H
#ifndef U3CDOWNLOADTHUMBU3ED__15_T24CE11043438A7D4D4DDC54ACF6098D68FD0006A_H
#define U3CDOWNLOADTHUMBU3ED__15_T24CE11043438A7D4D4DDC54ACF6098D68FD0006A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IndividualVideoDataDemo_<DownloadThumb>d__15
struct  U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A  : public RuntimeObject
{
public:
	// System.Int32 IndividualVideoDataDemo_<DownloadThumb>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object IndividualVideoDataDemo_<DownloadThumb>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String IndividualVideoDataDemo_<DownloadThumb>d__15::url
	String_t* ___url_2;
	// IndividualVideoDataDemo IndividualVideoDataDemo_<DownloadThumb>d__15::<>4__this
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1 * ___U3CU3E4__this_3;
	// UnityEngine.WWW IndividualVideoDataDemo_<DownloadThumb>d__15::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A, ___U3CU3E4__this_3)); }
	inline IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADTHUMBU3ED__15_T24CE11043438A7D4D4DDC54ACF6098D68FD0006A_H
#ifndef CHARACTERLISTITEM_T2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967_H
#define CHARACTERLISTITEM_T2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonNetSample_CharacterListItem
struct  CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967  : public RuntimeObject
{
public:
	// System.Int32 JsonNetSample_CharacterListItem::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.String JsonNetSample_CharacterListItem::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Int32 JsonNetSample_CharacterListItem::<Level>k__BackingField
	int32_t ___U3CLevelU3Ek__BackingField_2;
	// System.String JsonNetSample_CharacterListItem::<Class>k__BackingField
	String_t* ___U3CClassU3Ek__BackingField_3;
	// System.String JsonNetSample_CharacterListItem::<Sex>k__BackingField
	String_t* ___U3CSexU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CLevelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967, ___U3CLevelU3Ek__BackingField_2)); }
	inline int32_t get_U3CLevelU3Ek__BackingField_2() const { return ___U3CLevelU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLevelU3Ek__BackingField_2() { return &___U3CLevelU3Ek__BackingField_2; }
	inline void set_U3CLevelU3Ek__BackingField_2(int32_t value)
	{
		___U3CLevelU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CClassU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967, ___U3CClassU3Ek__BackingField_3)); }
	inline String_t* get_U3CClassU3Ek__BackingField_3() const { return ___U3CClassU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CClassU3Ek__BackingField_3() { return &___U3CClassU3Ek__BackingField_3; }
	inline void set_U3CClassU3Ek__BackingField_3(String_t* value)
	{
		___U3CClassU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClassU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CSexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967, ___U3CSexU3Ek__BackingField_4)); }
	inline String_t* get_U3CSexU3Ek__BackingField_4() const { return ___U3CSexU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CSexU3Ek__BackingField_4() { return &___U3CSexU3Ek__BackingField_4; }
	inline void set_U3CSexU3Ek__BackingField_4(String_t* value)
	{
		___U3CSexU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSexU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERLISTITEM_T2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#define SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_Padding
	int32_t ___ID_Padding_16;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_18;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_19;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_21;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_22;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_26;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_27;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_30;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_31;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_32;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_34;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_36;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_37;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_46;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_47;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_48;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_49;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_50;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_53;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_54;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_56;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_57;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_58;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_59;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_Padding_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Padding_16)); }
	inline int32_t get_ID_Padding_16() const { return ___ID_Padding_16; }
	inline int32_t* get_address_of_ID_Padding_16() { return &___ID_Padding_16; }
	inline void set_ID_Padding_16(int32_t value)
	{
		___ID_Padding_16 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GradientScale_17)); }
	inline int32_t get_ID_GradientScale_17() const { return ___ID_GradientScale_17; }
	inline int32_t* get_address_of_ID_GradientScale_17() { return &___ID_GradientScale_17; }
	inline void set_ID_GradientScale_17(int32_t value)
	{
		___ID_GradientScale_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleX_18)); }
	inline int32_t get_ID_ScaleX_18() const { return ___ID_ScaleX_18; }
	inline int32_t* get_address_of_ID_ScaleX_18() { return &___ID_ScaleX_18; }
	inline void set_ID_ScaleX_18(int32_t value)
	{
		___ID_ScaleX_18 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleY_19)); }
	inline int32_t get_ID_ScaleY_19() const { return ___ID_ScaleY_19; }
	inline int32_t* get_address_of_ID_ScaleY_19() { return &___ID_ScaleY_19; }
	inline void set_ID_ScaleY_19(int32_t value)
	{
		___ID_ScaleY_19 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_PerspectiveFilter_20)); }
	inline int32_t get_ID_PerspectiveFilter_20() const { return ___ID_PerspectiveFilter_20; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_20() { return &___ID_PerspectiveFilter_20; }
	inline void set_ID_PerspectiveFilter_20(int32_t value)
	{
		___ID_PerspectiveFilter_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_TextureWidth_21)); }
	inline int32_t get_ID_TextureWidth_21() const { return ___ID_TextureWidth_21; }
	inline int32_t* get_address_of_ID_TextureWidth_21() { return &___ID_TextureWidth_21; }
	inline void set_ID_TextureWidth_21(int32_t value)
	{
		___ID_TextureWidth_21 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_TextureHeight_22)); }
	inline int32_t get_ID_TextureHeight_22() const { return ___ID_TextureHeight_22; }
	inline int32_t* get_address_of_ID_TextureHeight_22() { return &___ID_TextureHeight_22; }
	inline void set_ID_TextureHeight_22(int32_t value)
	{
		___ID_TextureHeight_22 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_BevelAmount_23)); }
	inline int32_t get_ID_BevelAmount_23() const { return ___ID_BevelAmount_23; }
	inline int32_t* get_address_of_ID_BevelAmount_23() { return &___ID_BevelAmount_23; }
	inline void set_ID_BevelAmount_23(int32_t value)
	{
		___ID_BevelAmount_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowColor_24)); }
	inline int32_t get_ID_GlowColor_24() const { return ___ID_GlowColor_24; }
	inline int32_t* get_address_of_ID_GlowColor_24() { return &___ID_GlowColor_24; }
	inline void set_ID_GlowColor_24(int32_t value)
	{
		___ID_GlowColor_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowOffset_25)); }
	inline int32_t get_ID_GlowOffset_25() const { return ___ID_GlowOffset_25; }
	inline int32_t* get_address_of_ID_GlowOffset_25() { return &___ID_GlowOffset_25; }
	inline void set_ID_GlowOffset_25(int32_t value)
	{
		___ID_GlowOffset_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowPower_26)); }
	inline int32_t get_ID_GlowPower_26() const { return ___ID_GlowPower_26; }
	inline int32_t* get_address_of_ID_GlowPower_26() { return &___ID_GlowPower_26; }
	inline void set_ID_GlowPower_26(int32_t value)
	{
		___ID_GlowPower_26 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowOuter_27)); }
	inline int32_t get_ID_GlowOuter_27() const { return ___ID_GlowOuter_27; }
	inline int32_t* get_address_of_ID_GlowOuter_27() { return &___ID_GlowOuter_27; }
	inline void set_ID_GlowOuter_27(int32_t value)
	{
		___ID_GlowOuter_27 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_LightAngle_28)); }
	inline int32_t get_ID_LightAngle_28() const { return ___ID_LightAngle_28; }
	inline int32_t* get_address_of_ID_LightAngle_28() { return &___ID_LightAngle_28; }
	inline void set_ID_LightAngle_28(int32_t value)
	{
		___ID_LightAngle_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMap_29)); }
	inline int32_t get_ID_EnvMap_29() const { return ___ID_EnvMap_29; }
	inline int32_t* get_address_of_ID_EnvMap_29() { return &___ID_EnvMap_29; }
	inline void set_ID_EnvMap_29(int32_t value)
	{
		___ID_EnvMap_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMatrix_30)); }
	inline int32_t get_ID_EnvMatrix_30() const { return ___ID_EnvMatrix_30; }
	inline int32_t* get_address_of_ID_EnvMatrix_30() { return &___ID_EnvMatrix_30; }
	inline void set_ID_EnvMatrix_30(int32_t value)
	{
		___ID_EnvMatrix_30 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMatrixRotation_31)); }
	inline int32_t get_ID_EnvMatrixRotation_31() const { return ___ID_EnvMatrixRotation_31; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_31() { return &___ID_EnvMatrixRotation_31; }
	inline void set_ID_EnvMatrixRotation_31(int32_t value)
	{
		___ID_EnvMatrixRotation_31 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskCoord_32)); }
	inline int32_t get_ID_MaskCoord_32() const { return ___ID_MaskCoord_32; }
	inline int32_t* get_address_of_ID_MaskCoord_32() { return &___ID_MaskCoord_32; }
	inline void set_ID_MaskCoord_32(int32_t value)
	{
		___ID_MaskCoord_32 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ClipRect_33)); }
	inline int32_t get_ID_ClipRect_33() const { return ___ID_ClipRect_33; }
	inline int32_t* get_address_of_ID_ClipRect_33() { return &___ID_ClipRect_33; }
	inline void set_ID_ClipRect_33(int32_t value)
	{
		___ID_ClipRect_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskSoftnessX_34)); }
	inline int32_t get_ID_MaskSoftnessX_34() const { return ___ID_MaskSoftnessX_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_34() { return &___ID_MaskSoftnessX_34; }
	inline void set_ID_MaskSoftnessX_34(int32_t value)
	{
		___ID_MaskSoftnessX_34 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskSoftnessY_35)); }
	inline int32_t get_ID_MaskSoftnessY_35() const { return ___ID_MaskSoftnessY_35; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_35() { return &___ID_MaskSoftnessY_35; }
	inline void set_ID_MaskSoftnessY_35(int32_t value)
	{
		___ID_MaskSoftnessY_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_VertexOffsetX_36)); }
	inline int32_t get_ID_VertexOffsetX_36() const { return ___ID_VertexOffsetX_36; }
	inline int32_t* get_address_of_ID_VertexOffsetX_36() { return &___ID_VertexOffsetX_36; }
	inline void set_ID_VertexOffsetX_36(int32_t value)
	{
		___ID_VertexOffsetX_36 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_VertexOffsetY_37)); }
	inline int32_t get_ID_VertexOffsetY_37() const { return ___ID_VertexOffsetY_37; }
	inline int32_t* get_address_of_ID_VertexOffsetY_37() { return &___ID_VertexOffsetY_37; }
	inline void set_ID_VertexOffsetY_37(int32_t value)
	{
		___ID_VertexOffsetY_37 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UseClipRect_38)); }
	inline int32_t get_ID_UseClipRect_38() const { return ___ID_UseClipRect_38; }
	inline int32_t* get_address_of_ID_UseClipRect_38() { return &___ID_UseClipRect_38; }
	inline void set_ID_UseClipRect_38(int32_t value)
	{
		___ID_UseClipRect_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilID_39)); }
	inline int32_t get_ID_StencilID_39() const { return ___ID_StencilID_39; }
	inline int32_t* get_address_of_ID_StencilID_39() { return &___ID_StencilID_39; }
	inline void set_ID_StencilID_39(int32_t value)
	{
		___ID_StencilID_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilOp_40)); }
	inline int32_t get_ID_StencilOp_40() const { return ___ID_StencilOp_40; }
	inline int32_t* get_address_of_ID_StencilOp_40() { return &___ID_StencilOp_40; }
	inline void set_ID_StencilOp_40(int32_t value)
	{
		___ID_StencilOp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilComp_41)); }
	inline int32_t get_ID_StencilComp_41() const { return ___ID_StencilComp_41; }
	inline int32_t* get_address_of_ID_StencilComp_41() { return &___ID_StencilComp_41; }
	inline void set_ID_StencilComp_41(int32_t value)
	{
		___ID_StencilComp_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilReadMask_42)); }
	inline int32_t get_ID_StencilReadMask_42() const { return ___ID_StencilReadMask_42; }
	inline int32_t* get_address_of_ID_StencilReadMask_42() { return &___ID_StencilReadMask_42; }
	inline void set_ID_StencilReadMask_42(int32_t value)
	{
		___ID_StencilReadMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilWriteMask_43)); }
	inline int32_t get_ID_StencilWriteMask_43() const { return ___ID_StencilWriteMask_43; }
	inline int32_t* get_address_of_ID_StencilWriteMask_43() { return &___ID_StencilWriteMask_43; }
	inline void set_ID_StencilWriteMask_43(int32_t value)
	{
		___ID_StencilWriteMask_43 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ShaderFlags_44)); }
	inline int32_t get_ID_ShaderFlags_44() const { return ___ID_ShaderFlags_44; }
	inline int32_t* get_address_of_ID_ShaderFlags_44() { return &___ID_ShaderFlags_44; }
	inline void set_ID_ShaderFlags_44(int32_t value)
	{
		___ID_ShaderFlags_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_A_45)); }
	inline int32_t get_ID_ScaleRatio_A_45() const { return ___ID_ScaleRatio_A_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_45() { return &___ID_ScaleRatio_A_45; }
	inline void set_ID_ScaleRatio_A_45(int32_t value)
	{
		___ID_ScaleRatio_A_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_B_46)); }
	inline int32_t get_ID_ScaleRatio_B_46() const { return ___ID_ScaleRatio_B_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_46() { return &___ID_ScaleRatio_B_46; }
	inline void set_ID_ScaleRatio_B_46(int32_t value)
	{
		___ID_ScaleRatio_B_46 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_C_47)); }
	inline int32_t get_ID_ScaleRatio_C_47() const { return ___ID_ScaleRatio_C_47; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_47() { return &___ID_ScaleRatio_C_47; }
	inline void set_ID_ScaleRatio_C_47(int32_t value)
	{
		___ID_ScaleRatio_C_47 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Bevel_48)); }
	inline String_t* get_Keyword_Bevel_48() const { return ___Keyword_Bevel_48; }
	inline String_t** get_address_of_Keyword_Bevel_48() { return &___Keyword_Bevel_48; }
	inline void set_Keyword_Bevel_48(String_t* value)
	{
		___Keyword_Bevel_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Glow_49)); }
	inline String_t* get_Keyword_Glow_49() const { return ___Keyword_Glow_49; }
	inline String_t** get_address_of_Keyword_Glow_49() { return &___Keyword_Glow_49; }
	inline void set_Keyword_Glow_49(String_t* value)
	{
		___Keyword_Glow_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Underlay_50)); }
	inline String_t* get_Keyword_Underlay_50() const { return ___Keyword_Underlay_50; }
	inline String_t** get_address_of_Keyword_Underlay_50() { return &___Keyword_Underlay_50; }
	inline void set_Keyword_Underlay_50(String_t* value)
	{
		___Keyword_Underlay_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_50), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Ratios_51)); }
	inline String_t* get_Keyword_Ratios_51() const { return ___Keyword_Ratios_51; }
	inline String_t** get_address_of_Keyword_Ratios_51() { return &___Keyword_Ratios_51; }
	inline void set_Keyword_Ratios_51(String_t* value)
	{
		___Keyword_Ratios_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_SOFT_52)); }
	inline String_t* get_Keyword_MASK_SOFT_52() const { return ___Keyword_MASK_SOFT_52; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_52() { return &___Keyword_MASK_SOFT_52; }
	inline void set_Keyword_MASK_SOFT_52(String_t* value)
	{
		___Keyword_MASK_SOFT_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_HARD_53)); }
	inline String_t* get_Keyword_MASK_HARD_53() const { return ___Keyword_MASK_HARD_53; }
	inline String_t** get_address_of_Keyword_MASK_HARD_53() { return &___Keyword_MASK_HARD_53; }
	inline void set_Keyword_MASK_HARD_53(String_t* value)
	{
		___Keyword_MASK_HARD_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_53), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_TEX_54)); }
	inline String_t* get_Keyword_MASK_TEX_54() const { return ___Keyword_MASK_TEX_54; }
	inline String_t** get_address_of_Keyword_MASK_TEX_54() { return &___Keyword_MASK_TEX_54; }
	inline void set_Keyword_MASK_TEX_54(String_t* value)
	{
		___Keyword_MASK_TEX_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_54), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Outline_55)); }
	inline String_t* get_Keyword_Outline_55() const { return ___Keyword_Outline_55; }
	inline String_t** get_address_of_Keyword_Outline_55() { return &___Keyword_Outline_55; }
	inline void set_Keyword_Outline_55(String_t* value)
	{
		___Keyword_Outline_55 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ShaderTag_ZTestMode_56)); }
	inline String_t* get_ShaderTag_ZTestMode_56() const { return ___ShaderTag_ZTestMode_56; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_56() { return &___ShaderTag_ZTestMode_56; }
	inline void set_ShaderTag_ZTestMode_56(String_t* value)
	{
		___ShaderTag_ZTestMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_56), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ShaderTag_CullMode_57)); }
	inline String_t* get_ShaderTag_CullMode_57() const { return ___ShaderTag_CullMode_57; }
	inline String_t** get_address_of_ShaderTag_CullMode_57() { return &___ShaderTag_CullMode_57; }
	inline void set_ShaderTag_CullMode_57(String_t* value)
	{
		___ShaderTag_CullMode_57 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_57), value);
	}

	inline static int32_t get_offset_of_m_clamp_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___m_clamp_58)); }
	inline float get_m_clamp_58() const { return ___m_clamp_58; }
	inline float* get_address_of_m_clamp_58() { return &___m_clamp_58; }
	inline void set_m_clamp_58(float value)
	{
		___m_clamp_58 = value;
	}

	inline static int32_t get_offset_of_isInitialized_59() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___isInitialized_59)); }
	inline bool get_isInitialized_59() const { return ___isInitialized_59; }
	inline bool* get_address_of_isInitialized_59() { return &___isInitialized_59; }
	inline void set_isInitialized_59(bool value)
	{
		___isInitialized_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#ifndef TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#define TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t2549189919276EB833F83EA937AB992420E1B199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifndef SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#define SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject
struct  SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker_SpriteData> TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject::frames
	List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E, ___frames_0)); }
	inline List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * get_frames_0() const { return ___frames_0; }
	inline List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t5FAD5AAF16F630BF50EF436E06D812B87B3CC0F3 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifndef TMP_FONTUTILITIES_TEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_H
#define TMP_FONTUTILITIES_TEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_TEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_H
#ifndef UISETEXTENSIONS_TC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_H
#define UISETEXTENSIONS_TC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISetExtensions
struct  UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC  : public RuntimeObject
{
public:

public:
};

struct UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields
{
public:
	// System.Reflection.MethodInfo UISetExtensions::toggleSetMethod
	MethodInfo_t * ___toggleSetMethod_0;
	// System.Reflection.MethodInfo UISetExtensions::sliderSetMethod
	MethodInfo_t * ___sliderSetMethod_1;
	// System.Reflection.MethodInfo UISetExtensions::scrollbarSetMethod
	MethodInfo_t * ___scrollbarSetMethod_2;
	// System.Reflection.FieldInfo UISetExtensions::dropdownValueField
	FieldInfo_t * ___dropdownValueField_3;
	// System.Reflection.MethodInfo UISetExtensions::dropdownRefreshMethod
	MethodInfo_t * ___dropdownRefreshMethod_4;

public:
	inline static int32_t get_offset_of_toggleSetMethod_0() { return static_cast<int32_t>(offsetof(UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields, ___toggleSetMethod_0)); }
	inline MethodInfo_t * get_toggleSetMethod_0() const { return ___toggleSetMethod_0; }
	inline MethodInfo_t ** get_address_of_toggleSetMethod_0() { return &___toggleSetMethod_0; }
	inline void set_toggleSetMethod_0(MethodInfo_t * value)
	{
		___toggleSetMethod_0 = value;
		Il2CppCodeGenWriteBarrier((&___toggleSetMethod_0), value);
	}

	inline static int32_t get_offset_of_sliderSetMethod_1() { return static_cast<int32_t>(offsetof(UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields, ___sliderSetMethod_1)); }
	inline MethodInfo_t * get_sliderSetMethod_1() const { return ___sliderSetMethod_1; }
	inline MethodInfo_t ** get_address_of_sliderSetMethod_1() { return &___sliderSetMethod_1; }
	inline void set_sliderSetMethod_1(MethodInfo_t * value)
	{
		___sliderSetMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&___sliderSetMethod_1), value);
	}

	inline static int32_t get_offset_of_scrollbarSetMethod_2() { return static_cast<int32_t>(offsetof(UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields, ___scrollbarSetMethod_2)); }
	inline MethodInfo_t * get_scrollbarSetMethod_2() const { return ___scrollbarSetMethod_2; }
	inline MethodInfo_t ** get_address_of_scrollbarSetMethod_2() { return &___scrollbarSetMethod_2; }
	inline void set_scrollbarSetMethod_2(MethodInfo_t * value)
	{
		___scrollbarSetMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbarSetMethod_2), value);
	}

	inline static int32_t get_offset_of_dropdownValueField_3() { return static_cast<int32_t>(offsetof(UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields, ___dropdownValueField_3)); }
	inline FieldInfo_t * get_dropdownValueField_3() const { return ___dropdownValueField_3; }
	inline FieldInfo_t ** get_address_of_dropdownValueField_3() { return &___dropdownValueField_3; }
	inline void set_dropdownValueField_3(FieldInfo_t * value)
	{
		___dropdownValueField_3 = value;
		Il2CppCodeGenWriteBarrier((&___dropdownValueField_3), value);
	}

	inline static int32_t get_offset_of_dropdownRefreshMethod_4() { return static_cast<int32_t>(offsetof(UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields, ___dropdownRefreshMethod_4)); }
	inline MethodInfo_t * get_dropdownRefreshMethod_4() const { return ___dropdownRefreshMethod_4; }
	inline MethodInfo_t ** get_address_of_dropdownRefreshMethod_4() { return &___dropdownRefreshMethod_4; }
	inline void set_dropdownRefreshMethod_4(MethodInfo_t * value)
	{
		___dropdownRefreshMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___dropdownRefreshMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISETEXTENSIONS_TC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_H
#ifndef U3CGETTRENDINGVIDEOSU3ED__21_T4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF_H
#define U3CGETTRENDINGVIDEOSU3ED__21_T4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<GetTrendingVideos>d__21
struct  U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<GetTrendingVideos>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<GetTrendingVideos>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<GetTrendingVideos>d__21::regionCode
	String_t* ___regionCode_2;
	// System.Int32 YoutubeAPIManager_<GetTrendingVideos>d__21::maxresult
	int32_t ___maxresult_3;
	// YoutubeAPIManager YoutubeAPIManager_<GetTrendingVideos>d__21::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_4;
	// System.Action`1<YoutubeData[]> YoutubeAPIManager_<GetTrendingVideos>d__21::callback
	Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * ___callback_5;
	// UnityEngine.WWW YoutubeAPIManager_<GetTrendingVideos>d__21::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_regionCode_2() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___regionCode_2)); }
	inline String_t* get_regionCode_2() const { return ___regionCode_2; }
	inline String_t** get_address_of_regionCode_2() { return &___regionCode_2; }
	inline void set_regionCode_2(String_t* value)
	{
		___regionCode_2 = value;
		Il2CppCodeGenWriteBarrier((&___regionCode_2), value);
	}

	inline static int32_t get_offset_of_maxresult_3() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___maxresult_3)); }
	inline int32_t get_maxresult_3() const { return ___maxresult_3; }
	inline int32_t* get_address_of_maxresult_3() { return &___maxresult_3; }
	inline void set_maxresult_3(int32_t value)
	{
		___maxresult_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___U3CU3E4__this_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___callback_5)); }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * get_callback_5() const { return ___callback_5; }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF, ___U3CcallU3E5__2_6)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_6() const { return ___U3CcallU3E5__2_6; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_6() { return &___U3CcallU3E5__2_6; }
	inline void set_U3CcallU3E5__2_6(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTRENDINGVIDEOSU3ED__21_T4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF_H
#ifndef U3CGETVIDEOSFROMCHANNELU3ED__16_TED90C0A165D0B07025B4D47BFE4782A4CD65B05A_H
#define U3CGETVIDEOSFROMCHANNELU3ED__16_TED90C0A165D0B07025B4D47BFE4782A4CD65B05A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<GetVideosFromChannel>d__16
struct  U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<GetVideosFromChannel>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<GetVideosFromChannel>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<GetVideosFromChannel>d__16::channelId
	String_t* ___channelId_2;
	// System.Int32 YoutubeAPIManager_<GetVideosFromChannel>d__16::maxResults
	int32_t ___maxResults_3;
	// YoutubeAPIManager YoutubeAPIManager_<GetVideosFromChannel>d__16::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_4;
	// System.Action`1<YoutubeData[]> YoutubeAPIManager_<GetVideosFromChannel>d__16::callback
	Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * ___callback_5;
	// UnityEngine.WWW YoutubeAPIManager_<GetVideosFromChannel>d__16::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_channelId_2() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___channelId_2)); }
	inline String_t* get_channelId_2() const { return ___channelId_2; }
	inline String_t** get_address_of_channelId_2() { return &___channelId_2; }
	inline void set_channelId_2(String_t* value)
	{
		___channelId_2 = value;
		Il2CppCodeGenWriteBarrier((&___channelId_2), value);
	}

	inline static int32_t get_offset_of_maxResults_3() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___maxResults_3)); }
	inline int32_t get_maxResults_3() const { return ___maxResults_3; }
	inline int32_t* get_address_of_maxResults_3() { return &___maxResults_3; }
	inline void set_maxResults_3(int32_t value)
	{
		___maxResults_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___U3CU3E4__this_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___callback_5)); }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * get_callback_5() const { return ___callback_5; }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A, ___U3CcallU3E5__2_6)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_6() const { return ___U3CcallU3E5__2_6; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_6() { return &___U3CcallU3E5__2_6; }
	inline void set_U3CcallU3E5__2_6(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETVIDEOSFROMCHANNELU3ED__16_TED90C0A165D0B07025B4D47BFE4782A4CD65B05A_H
#ifndef U3CLOADSINGLEVIDEOU3ED__24_T4969A6D725FD3074A3352ABABCB983F8E4ED80DA_H
#define U3CLOADSINGLEVIDEOU3ED__24_T4969A6D725FD3074A3352ABABCB983F8E4ED80DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<LoadSingleVideo>d__24
struct  U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<LoadSingleVideo>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<LoadSingleVideo>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<LoadSingleVideo>d__24::videoId
	String_t* ___videoId_2;
	// YoutubeAPIManager YoutubeAPIManager_<LoadSingleVideo>d__24::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_3;
	// System.Action`1<YoutubeData> YoutubeAPIManager_<LoadSingleVideo>d__24::callback
	Action_1_t9B4B05046062A7D2297C5AD2FBB47148F601A1CB * ___callback_4;
	// UnityEngine.WWW YoutubeAPIManager_<LoadSingleVideo>d__24::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_videoId_2() { return static_cast<int32_t>(offsetof(U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA, ___videoId_2)); }
	inline String_t* get_videoId_2() const { return ___videoId_2; }
	inline String_t** get_address_of_videoId_2() { return &___videoId_2; }
	inline void set_videoId_2(String_t* value)
	{
		___videoId_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA, ___U3CU3E4__this_3)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA, ___callback_4)); }
	inline Action_1_t9B4B05046062A7D2297C5AD2FBB47148F601A1CB * get_callback_4() const { return ___callback_4; }
	inline Action_1_t9B4B05046062A7D2297C5AD2FBB47148F601A1CB ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t9B4B05046062A7D2297C5AD2FBB47148F601A1CB * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA, ___U3CcallU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_5() const { return ___U3CcallU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_5() { return &___U3CcallU3E5__2_5; }
	inline void set_U3CcallU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSINGLEVIDEOU3ED__24_T4969A6D725FD3074A3352ABABCB983F8E4ED80DA_H
#ifndef U3CYOUTUBECALLCOMMENTSU3ED__18_TF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9_H
#define U3CYOUTUBECALLCOMMENTSU3ED__18_TF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<YoutubeCallComments>d__18
struct  U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<YoutubeCallComments>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<YoutubeCallComments>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<YoutubeCallComments>d__18::videoId
	String_t* ___videoId_2;
	// YoutubeAPIManager YoutubeAPIManager_<YoutubeCallComments>d__18::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_3;
	// System.Action`1<YoutubeComments[]> YoutubeAPIManager_<YoutubeCallComments>d__18::callback
	Action_1_t7942E6C460FE9C8561ECEC02C035275A648E97A9 * ___callback_4;
	// UnityEngine.WWW YoutubeAPIManager_<YoutubeCallComments>d__18::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_videoId_2() { return static_cast<int32_t>(offsetof(U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9, ___videoId_2)); }
	inline String_t* get_videoId_2() const { return ___videoId_2; }
	inline String_t** get_address_of_videoId_2() { return &___videoId_2; }
	inline void set_videoId_2(String_t* value)
	{
		___videoId_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9, ___U3CU3E4__this_3)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9, ___callback_4)); }
	inline Action_1_t7942E6C460FE9C8561ECEC02C035275A648E97A9 * get_callback_4() const { return ___callback_4; }
	inline Action_1_t7942E6C460FE9C8561ECEC02C035275A648E97A9 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t7942E6C460FE9C8561ECEC02C035275A648E97A9 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9, ___U3CcallU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_5() const { return ___U3CcallU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_5() { return &___U3CcallU3E5__2_5; }
	inline void set_U3CcallU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBECALLCOMMENTSU3ED__18_TF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9_H
#ifndef U3CYOUTUBECALLPLAYLISTU3ED__17_T4325628F49A1C23F82A17756BE6BEF97BFCA0E94_H
#define U3CYOUTUBECALLPLAYLISTU3ED__17_T4325628F49A1C23F82A17756BE6BEF97BFCA0E94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<YoutubeCallPlaylist>d__17
struct  U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<YoutubeCallPlaylist>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<YoutubeCallPlaylist>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<YoutubeCallPlaylist>d__17::playlistId
	String_t* ___playlistId_2;
	// System.Int32 YoutubeAPIManager_<YoutubeCallPlaylist>d__17::maxResults
	int32_t ___maxResults_3;
	// YoutubeAPIManager YoutubeAPIManager_<YoutubeCallPlaylist>d__17::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_4;
	// System.Action`1<YoutubePlaylistItems[]> YoutubeAPIManager_<YoutubeCallPlaylist>d__17::callback
	Action_1_tCA045A458C1E10FB3B86DFA5564560AC0505B845 * ___callback_5;
	// UnityEngine.WWW YoutubeAPIManager_<YoutubeCallPlaylist>d__17::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_playlistId_2() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___playlistId_2)); }
	inline String_t* get_playlistId_2() const { return ___playlistId_2; }
	inline String_t** get_address_of_playlistId_2() { return &___playlistId_2; }
	inline void set_playlistId_2(String_t* value)
	{
		___playlistId_2 = value;
		Il2CppCodeGenWriteBarrier((&___playlistId_2), value);
	}

	inline static int32_t get_offset_of_maxResults_3() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___maxResults_3)); }
	inline int32_t get_maxResults_3() const { return ___maxResults_3; }
	inline int32_t* get_address_of_maxResults_3() { return &___maxResults_3; }
	inline void set_maxResults_3(int32_t value)
	{
		___maxResults_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___U3CU3E4__this_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___callback_5)); }
	inline Action_1_tCA045A458C1E10FB3B86DFA5564560AC0505B845 * get_callback_5() const { return ___callback_5; }
	inline Action_1_tCA045A458C1E10FB3B86DFA5564560AC0505B845 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_1_tCA045A458C1E10FB3B86DFA5564560AC0505B845 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94, ___U3CcallU3E5__2_6)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_6() const { return ___U3CcallU3E5__2_6; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_6() { return &___U3CcallU3E5__2_6; }
	inline void set_U3CcallU3E5__2_6(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBECALLPLAYLISTU3ED__17_T4325628F49A1C23F82A17756BE6BEF97BFCA0E94_H
#ifndef U3CYOUTUBEGETNEXTPAGEU3ED__7_T7A33CC2A4851DA348511522C14F6D3F34C0674E2_H
#define U3CYOUTUBEGETNEXTPAGEU3ED__7_T7A33CC2A4851DA348511522C14F6D3F34C0674E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeApiGetUnlimitedVideos_<YoutubeGetNextPage>d__7
struct  U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2  : public RuntimeObject
{
public:
	// System.Int32 YoutubeApiGetUnlimitedVideos_<YoutubeGetNextPage>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeApiGetUnlimitedVideos_<YoutubeGetNextPage>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubeApiGetUnlimitedVideos YoutubeApiGetUnlimitedVideos_<YoutubeGetNextPage>d__7::<>4__this
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 * ___U3CU3E4__this_2;
	// System.String YoutubeApiGetUnlimitedVideos_<YoutubeGetNextPage>d__7::pageToken
	String_t* ___pageToken_3;
	// UnityEngine.WWW YoutubeApiGetUnlimitedVideos_<YoutubeGetNextPage>d__7::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2, ___U3CU3E4__this_2)); }
	inline YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_pageToken_3() { return static_cast<int32_t>(offsetof(U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2, ___pageToken_3)); }
	inline String_t* get_pageToken_3() const { return ___pageToken_3; }
	inline String_t** get_address_of_pageToken_3() { return &___pageToken_3; }
	inline void set_pageToken_3(String_t* value)
	{
		___pageToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___pageToken_3), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2, ___U3CcallU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_4() const { return ___U3CcallU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_4() { return &___U3CcallU3E5__2_4; }
	inline void set_U3CcallU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBEGETNEXTPAGEU3ED__7_T7A33CC2A4851DA348511522C14F6D3F34C0674E2_H
#ifndef U3CYOUTUBESEARCHU3ED__6_TB4446374E0BA2B2B2CD411FC1BED56E000D3C824_H
#define U3CYOUTUBESEARCHU3ED__6_TB4446374E0BA2B2B2CD411FC1BED56E000D3C824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeApiGetUnlimitedVideos_<YoutubeSearch>d__6
struct  U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824  : public RuntimeObject
{
public:
	// System.Int32 YoutubeApiGetUnlimitedVideos_<YoutubeSearch>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeApiGetUnlimitedVideos_<YoutubeSearch>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeApiGetUnlimitedVideos_<YoutubeSearch>d__6::keyword
	String_t* ___keyword_2;
	// YoutubeApiGetUnlimitedVideos YoutubeApiGetUnlimitedVideos_<YoutubeSearch>d__6::<>4__this
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 * ___U3CU3E4__this_3;
	// UnityEngine.WWW YoutubeApiGetUnlimitedVideos_<YoutubeSearch>d__6::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_keyword_2() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824, ___keyword_2)); }
	inline String_t* get_keyword_2() const { return ___keyword_2; }
	inline String_t** get_address_of_keyword_2() { return &___keyword_2; }
	inline void set_keyword_2(String_t* value)
	{
		___keyword_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyword_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824, ___U3CU3E4__this_3)); }
	inline YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824, ___U3CcallU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_4() const { return ___U3CcallU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_4() { return &___U3CcallU3E5__2_4; }
	inline void set_U3CcallU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBESEARCHU3ED__6_TB4446374E0BA2B2B2CD411FC1BED56E000D3C824_H
#ifndef YOUTUBECHANNEL_T8E1C162D1D96B813100C732916DD0FCDA7A9C935_H
#define YOUTUBECHANNEL_T8E1C162D1D96B813100C732916DD0FCDA7A9C935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeChannel
struct  YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935  : public RuntimeObject
{
public:
	// System.String YoutubeChannel::id
	String_t* ___id_0;
	// System.String YoutubeChannel::title
	String_t* ___title_1;
	// System.String YoutubeChannel::description
	String_t* ___description_2;
	// System.String YoutubeChannel::thumbnail
	String_t* ___thumbnail_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}

	inline static int32_t get_offset_of_thumbnail_3() { return static_cast<int32_t>(offsetof(YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935, ___thumbnail_3)); }
	inline String_t* get_thumbnail_3() const { return ___thumbnail_3; }
	inline String_t** get_address_of_thumbnail_3() { return &___thumbnail_3; }
	inline void set_thumbnail_3(String_t* value)
	{
		___thumbnail_3 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBECHANNEL_T8E1C162D1D96B813100C732916DD0FCDA7A9C935_H
#ifndef U3CDOWNLOADTHUMBU3ED__6_T33182CE7027674691EEA22DC0B5D6902B7D1C1DB_H
#define U3CDOWNLOADTHUMBU3ED__6_T33182CE7027674691EEA22DC0B5D6902B7D1C1DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeChannelUI_<DownloadThumb>d__6
struct  U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB  : public RuntimeObject
{
public:
	// System.Int32 YoutubeChannelUI_<DownloadThumb>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeChannelUI_<DownloadThumb>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubeChannelUI YoutubeChannelUI_<DownloadThumb>d__6::<>4__this
	YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15 * ___U3CU3E4__this_2;
	// UnityEngine.WWW YoutubeChannelUI_<DownloadThumb>d__6::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB, ___U3CU3E4__this_2)); }
	inline YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB, ___U3CwwwU3E5__2_3)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_3() const { return ___U3CwwwU3E5__2_3; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_3() { return &___U3CwwwU3E5__2_3; }
	inline void set_U3CwwwU3E5__2_3(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADTHUMBU3ED__6_T33182CE7027674691EEA22DC0B5D6902B7D1C1DB_H
#ifndef YOUTUBECOMMENTS_TA42BB030AE54B8FA33EBC72E5DF29192FAE33340_H
#define YOUTUBECOMMENTS_TA42BB030AE54B8FA33EBC72E5DF29192FAE33340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeComments
struct  YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340  : public RuntimeObject
{
public:
	// System.String YoutubeComments::authorDisplayName
	String_t* ___authorDisplayName_0;
	// System.String YoutubeComments::authorProfileImageUrl
	String_t* ___authorProfileImageUrl_1;
	// System.String YoutubeComments::authorChannelUrl
	String_t* ___authorChannelUrl_2;
	// System.String YoutubeComments::authorChannelId
	String_t* ___authorChannelId_3;
	// System.String YoutubeComments::videoId
	String_t* ___videoId_4;
	// System.String YoutubeComments::textDisplay
	String_t* ___textDisplay_5;
	// System.String YoutubeComments::textOriginal
	String_t* ___textOriginal_6;
	// System.Boolean YoutubeComments::canRate
	bool ___canRate_7;
	// System.String YoutubeComments::viewerRating
	String_t* ___viewerRating_8;
	// System.Int32 YoutubeComments::likeCount
	int32_t ___likeCount_9;
	// System.String YoutubeComments::publishedAt
	String_t* ___publishedAt_10;
	// System.String YoutubeComments::updatedAt
	String_t* ___updatedAt_11;

public:
	inline static int32_t get_offset_of_authorDisplayName_0() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___authorDisplayName_0)); }
	inline String_t* get_authorDisplayName_0() const { return ___authorDisplayName_0; }
	inline String_t** get_address_of_authorDisplayName_0() { return &___authorDisplayName_0; }
	inline void set_authorDisplayName_0(String_t* value)
	{
		___authorDisplayName_0 = value;
		Il2CppCodeGenWriteBarrier((&___authorDisplayName_0), value);
	}

	inline static int32_t get_offset_of_authorProfileImageUrl_1() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___authorProfileImageUrl_1)); }
	inline String_t* get_authorProfileImageUrl_1() const { return ___authorProfileImageUrl_1; }
	inline String_t** get_address_of_authorProfileImageUrl_1() { return &___authorProfileImageUrl_1; }
	inline void set_authorProfileImageUrl_1(String_t* value)
	{
		___authorProfileImageUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&___authorProfileImageUrl_1), value);
	}

	inline static int32_t get_offset_of_authorChannelUrl_2() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___authorChannelUrl_2)); }
	inline String_t* get_authorChannelUrl_2() const { return ___authorChannelUrl_2; }
	inline String_t** get_address_of_authorChannelUrl_2() { return &___authorChannelUrl_2; }
	inline void set_authorChannelUrl_2(String_t* value)
	{
		___authorChannelUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___authorChannelUrl_2), value);
	}

	inline static int32_t get_offset_of_authorChannelId_3() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___authorChannelId_3)); }
	inline String_t* get_authorChannelId_3() const { return ___authorChannelId_3; }
	inline String_t** get_address_of_authorChannelId_3() { return &___authorChannelId_3; }
	inline void set_authorChannelId_3(String_t* value)
	{
		___authorChannelId_3 = value;
		Il2CppCodeGenWriteBarrier((&___authorChannelId_3), value);
	}

	inline static int32_t get_offset_of_videoId_4() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___videoId_4)); }
	inline String_t* get_videoId_4() const { return ___videoId_4; }
	inline String_t** get_address_of_videoId_4() { return &___videoId_4; }
	inline void set_videoId_4(String_t* value)
	{
		___videoId_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_4), value);
	}

	inline static int32_t get_offset_of_textDisplay_5() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___textDisplay_5)); }
	inline String_t* get_textDisplay_5() const { return ___textDisplay_5; }
	inline String_t** get_address_of_textDisplay_5() { return &___textDisplay_5; }
	inline void set_textDisplay_5(String_t* value)
	{
		___textDisplay_5 = value;
		Il2CppCodeGenWriteBarrier((&___textDisplay_5), value);
	}

	inline static int32_t get_offset_of_textOriginal_6() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___textOriginal_6)); }
	inline String_t* get_textOriginal_6() const { return ___textOriginal_6; }
	inline String_t** get_address_of_textOriginal_6() { return &___textOriginal_6; }
	inline void set_textOriginal_6(String_t* value)
	{
		___textOriginal_6 = value;
		Il2CppCodeGenWriteBarrier((&___textOriginal_6), value);
	}

	inline static int32_t get_offset_of_canRate_7() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___canRate_7)); }
	inline bool get_canRate_7() const { return ___canRate_7; }
	inline bool* get_address_of_canRate_7() { return &___canRate_7; }
	inline void set_canRate_7(bool value)
	{
		___canRate_7 = value;
	}

	inline static int32_t get_offset_of_viewerRating_8() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___viewerRating_8)); }
	inline String_t* get_viewerRating_8() const { return ___viewerRating_8; }
	inline String_t** get_address_of_viewerRating_8() { return &___viewerRating_8; }
	inline void set_viewerRating_8(String_t* value)
	{
		___viewerRating_8 = value;
		Il2CppCodeGenWriteBarrier((&___viewerRating_8), value);
	}

	inline static int32_t get_offset_of_likeCount_9() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___likeCount_9)); }
	inline int32_t get_likeCount_9() const { return ___likeCount_9; }
	inline int32_t* get_address_of_likeCount_9() { return &___likeCount_9; }
	inline void set_likeCount_9(int32_t value)
	{
		___likeCount_9 = value;
	}

	inline static int32_t get_offset_of_publishedAt_10() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___publishedAt_10)); }
	inline String_t* get_publishedAt_10() const { return ___publishedAt_10; }
	inline String_t** get_address_of_publishedAt_10() { return &___publishedAt_10; }
	inline void set_publishedAt_10(String_t* value)
	{
		___publishedAt_10 = value;
		Il2CppCodeGenWriteBarrier((&___publishedAt_10), value);
	}

	inline static int32_t get_offset_of_updatedAt_11() { return static_cast<int32_t>(offsetof(YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340, ___updatedAt_11)); }
	inline String_t* get_updatedAt_11() const { return ___updatedAt_11; }
	inline String_t** get_address_of_updatedAt_11() { return &___updatedAt_11; }
	inline void set_updatedAt_11(String_t* value)
	{
		___updatedAt_11 = value;
		Il2CppCodeGenWriteBarrier((&___updatedAt_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBECOMMENTS_TA42BB030AE54B8FA33EBC72E5DF29192FAE33340_H
#ifndef YOUTUBECONTENTDETAILS_T06A3D3B07A786053F00B37186018FBEA013FD48C_H
#define YOUTUBECONTENTDETAILS_T06A3D3B07A786053F00B37186018FBEA013FD48C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeContentDetails
struct  YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C  : public RuntimeObject
{
public:
	// System.String YoutubeContentDetails::duration
	String_t* ___duration_0;
	// System.String YoutubeContentDetails::dimension
	String_t* ___dimension_1;
	// System.String YoutubeContentDetails::definition
	String_t* ___definition_2;
	// System.String YoutubeContentDetails::caption
	String_t* ___caption_3;
	// System.String YoutubeContentDetails::licensedContent
	String_t* ___licensedContent_4;
	// System.String YoutubeContentDetails::projection
	String_t* ___projection_5;
	// System.Boolean YoutubeContentDetails::ageRestrict
	bool ___ageRestrict_6;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___duration_0)); }
	inline String_t* get_duration_0() const { return ___duration_0; }
	inline String_t** get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(String_t* value)
	{
		___duration_0 = value;
		Il2CppCodeGenWriteBarrier((&___duration_0), value);
	}

	inline static int32_t get_offset_of_dimension_1() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___dimension_1)); }
	inline String_t* get_dimension_1() const { return ___dimension_1; }
	inline String_t** get_address_of_dimension_1() { return &___dimension_1; }
	inline void set_dimension_1(String_t* value)
	{
		___dimension_1 = value;
		Il2CppCodeGenWriteBarrier((&___dimension_1), value);
	}

	inline static int32_t get_offset_of_definition_2() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___definition_2)); }
	inline String_t* get_definition_2() const { return ___definition_2; }
	inline String_t** get_address_of_definition_2() { return &___definition_2; }
	inline void set_definition_2(String_t* value)
	{
		___definition_2 = value;
		Il2CppCodeGenWriteBarrier((&___definition_2), value);
	}

	inline static int32_t get_offset_of_caption_3() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___caption_3)); }
	inline String_t* get_caption_3() const { return ___caption_3; }
	inline String_t** get_address_of_caption_3() { return &___caption_3; }
	inline void set_caption_3(String_t* value)
	{
		___caption_3 = value;
		Il2CppCodeGenWriteBarrier((&___caption_3), value);
	}

	inline static int32_t get_offset_of_licensedContent_4() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___licensedContent_4)); }
	inline String_t* get_licensedContent_4() const { return ___licensedContent_4; }
	inline String_t** get_address_of_licensedContent_4() { return &___licensedContent_4; }
	inline void set_licensedContent_4(String_t* value)
	{
		___licensedContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___licensedContent_4), value);
	}

	inline static int32_t get_offset_of_projection_5() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___projection_5)); }
	inline String_t* get_projection_5() const { return ___projection_5; }
	inline String_t** get_address_of_projection_5() { return &___projection_5; }
	inline void set_projection_5(String_t* value)
	{
		___projection_5 = value;
		Il2CppCodeGenWriteBarrier((&___projection_5), value);
	}

	inline static int32_t get_offset_of_ageRestrict_6() { return static_cast<int32_t>(offsetof(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C, ___ageRestrict_6)); }
	inline bool get_ageRestrict_6() const { return ___ageRestrict_6; }
	inline bool* get_address_of_ageRestrict_6() { return &___ageRestrict_6; }
	inline void set_ageRestrict_6(bool value)
	{
		___ageRestrict_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBECONTENTDETAILS_T06A3D3B07A786053F00B37186018FBEA013FD48C_H
#ifndef YOUTUBEDATA_T41A1FB14F9A09E08F9EA36BB9E8557CE547E1524_H
#define YOUTUBEDATA_T41A1FB14F9A09E08F9EA36BB9E8557CE547E1524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeData
struct  YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524  : public RuntimeObject
{
public:
	// YoutubeSnippet YoutubeData::snippet
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 * ___snippet_0;
	// YoutubeStatistics YoutubeData::statistics
	YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC * ___statistics_1;
	// YoutubeContentDetails YoutubeData::contentDetails
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C * ___contentDetails_2;
	// System.String YoutubeData::id
	String_t* ___id_3;

public:
	inline static int32_t get_offset_of_snippet_0() { return static_cast<int32_t>(offsetof(YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524, ___snippet_0)); }
	inline YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 * get_snippet_0() const { return ___snippet_0; }
	inline YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 ** get_address_of_snippet_0() { return &___snippet_0; }
	inline void set_snippet_0(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 * value)
	{
		___snippet_0 = value;
		Il2CppCodeGenWriteBarrier((&___snippet_0), value);
	}

	inline static int32_t get_offset_of_statistics_1() { return static_cast<int32_t>(offsetof(YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524, ___statistics_1)); }
	inline YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC * get_statistics_1() const { return ___statistics_1; }
	inline YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC ** get_address_of_statistics_1() { return &___statistics_1; }
	inline void set_statistics_1(YoutubeStatistics_t0A77F85B28DCD4F915828E5C116C4BFFE316C3AC * value)
	{
		___statistics_1 = value;
		Il2CppCodeGenWriteBarrier((&___statistics_1), value);
	}

	inline static int32_t get_offset_of_contentDetails_2() { return static_cast<int32_t>(offsetof(YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524, ___contentDetails_2)); }
	inline YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C * get_contentDetails_2() const { return ___contentDetails_2; }
	inline YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C ** get_address_of_contentDetails_2() { return &___contentDetails_2; }
	inline void set_contentDetails_2(YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C * value)
	{
		___contentDetails_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentDetails_2), value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEDATA_T41A1FB14F9A09E08F9EA36BB9E8557CE547E1524_H
#ifndef U3CU3EC__DISPLAYCLASS187_0_T7EACB7EA4F8792767286527B2621FDE4A93CE611_H
#define U3CU3EC__DISPLAYCLASS187_0_T7EACB7EA4F8792767286527B2621FDE4A93CE611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<>c__DisplayClass187_0
struct  U3CU3Ec__DisplayClass187_0_t7EACB7EA4F8792767286527B2621FDE4A93CE611  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<>c__DisplayClass187_0::formatCode
	int32_t ___formatCode_0;

public:
	inline static int32_t get_offset_of_formatCode_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass187_0_t7EACB7EA4F8792767286527B2621FDE4A93CE611, ___formatCode_0)); }
	inline int32_t get_formatCode_0() const { return ___formatCode_0; }
	inline int32_t* get_address_of_formatCode_0() { return &___formatCode_0; }
	inline void set_formatCode_0(int32_t value)
	{
		___formatCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS187_0_T7EACB7EA4F8792767286527B2621FDE4A93CE611_H
#ifndef U3CDOWNLOADTHUMBNAILU3ED__51_TEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A_H
#define U3CDOWNLOADTHUMBNAILU3ED__51_TEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<DownloadThumbnail>d__51
struct  U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<DownloadThumbnail>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<DownloadThumbnail>d__51::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubePlayer_<DownloadThumbnail>d__51::videoId
	String_t* ___videoId_2;
	// YoutubePlayer YoutubePlayer_<DownloadThumbnail>d__51::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_3;
	// UnityEngine.WWW YoutubePlayer_<DownloadThumbnail>d__51::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_videoId_2() { return static_cast<int32_t>(offsetof(U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A, ___videoId_2)); }
	inline String_t* get_videoId_2() const { return ___videoId_2; }
	inline String_t** get_address_of_videoId_2() { return &___videoId_2; }
	inline void set_videoId_2(String_t* value)
	{
		___videoId_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A, ___U3CU3E4__this_3)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADTHUMBNAILU3ED__51_TEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A_H
#ifndef U3CDOWNLOADURLU3ED__194_TCD0A2AD909B9A64499B2BABCDAEBA43690E61283_H
#define U3CDOWNLOADURLU3ED__194_TCD0A2AD909B9A64499B2BABCDAEBA43690E61283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<DownloadUrl>d__194
struct  U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<DownloadUrl>d__194::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<DownloadUrl>d__194::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubePlayer_<DownloadUrl>d__194::url
	String_t* ___url_2;
	// UnityEngine.Networking.UnityWebRequest YoutubePlayer_<DownloadUrl>d__194::<request>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CrequestU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283, ___U3CrequestU3E5__2_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CrequestU3E5__2_3() const { return ___U3CrequestU3E5__2_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CrequestU3E5__2_3() { return &___U3CrequestU3E5__2_3; }
	inline void set_U3CrequestU3E5__2_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CrequestU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADURLU3ED__194_TCD0A2AD909B9A64499B2BABCDAEBA43690E61283_H
#ifndef U3CDOWNLOADYOUTUBEURLU3ED__195_T32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB_H
#define U3CDOWNLOADYOUTUBEURLU3ED__195_T32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<DownloadYoutubeUrl>d__195
struct  U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<DownloadYoutubeUrl>d__195::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<DownloadYoutubeUrl>d__195::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<DownloadYoutubeUrl>d__195::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;
	// System.String YoutubePlayer_<DownloadYoutubeUrl>d__195::url
	String_t* ___url_3;
	// System.Action YoutubePlayer_<DownloadYoutubeUrl>d__195::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_4;
	// YoutubePlayer YoutubePlayer_<DownloadYoutubeUrl>d__195::player
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___player_5;
	// UnityEngine.Networking.UnityWebRequest YoutubePlayer_<DownloadYoutubeUrl>d__195::<request>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CrequestU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___callback_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_4() const { return ___callback_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___player_5)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_player_5() const { return ___player_5; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB, ___U3CrequestU3E5__2_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CrequestU3E5__2_6() const { return ___U3CrequestU3E5__2_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CrequestU3E5__2_6() { return &___U3CrequestU3E5__2_6; }
	inline void set_U3CrequestU3E5__2_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CrequestU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADYOUTUBEURLU3ED__195_T32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB_H
#ifndef U3CDOWNLOADERU3ED__140_TB855A798EC8F1E3F7675B096A7DB4C5A17C90353_H
#define U3CDOWNLOADERU3ED__140_TB855A798EC8F1E3F7675B096A7DB4C5A17C90353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<Downloader>d__140
struct  U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<Downloader>d__140::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<Downloader>d__140::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubePlayer_<Downloader>d__140::uri
	String_t* ___uri_2;
	// YoutubePlayer YoutubePlayer_<Downloader>d__140::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_3;
	// System.Boolean YoutubePlayer_<Downloader>d__140::audio
	bool ___audio_4;
	// UnityEngine.Networking.UnityWebRequest YoutubePlayer_<Downloader>d__140::<request>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CrequestU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353, ___uri_2)); }
	inline String_t* get_uri_2() const { return ___uri_2; }
	inline String_t** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(String_t* value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353, ___U3CU3E4__this_3)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_audio_4() { return static_cast<int32_t>(offsetof(U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353, ___audio_4)); }
	inline bool get_audio_4() const { return ___audio_4; }
	inline bool* get_address_of_audio_4() { return &___audio_4; }
	inline void set_audio_4(bool value)
	{
		___audio_4 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353, ___U3CrequestU3E5__2_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CrequestU3E5__2_5() const { return ___U3CrequestU3E5__2_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CrequestU3E5__2_5() { return &___U3CrequestU3E5__2_5; }
	inline void set_U3CrequestU3E5__2_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CrequestU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADERU3ED__140_TB855A798EC8F1E3F7675B096A7DB4C5A17C90353_H
#ifndef U3CEXTRACTDOWNLOADURLSU3ED__182_TAE74DB3B9B67506155E2C1026736D23DE92C72E0_H
#define U3CEXTRACTDOWNLOADURLSU3ED__182_TAE74DB3B9B67506155E2C1026736D23DE92C72E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<ExtractDownloadUrls>d__182
struct  U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<ExtractDownloadUrls>d__182::<>1__state
	int32_t ___U3CU3E1__state_0;
	// YoutubePlayer_ExtractionInfo YoutubePlayer_<ExtractDownloadUrls>d__182::<>2__current
	ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491 * ___U3CU3E2__current_1;
	// System.Int32 YoutubePlayer_<ExtractDownloadUrls>d__182::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JObject YoutubePlayer_<ExtractDownloadUrls>d__182::json
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___json_3;
	// Newtonsoft.Json.Linq.JObject YoutubePlayer_<ExtractDownloadUrls>d__182::<>3__json
	JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * ___U3CU3E3__json_4;
	// System.String[] YoutubePlayer_<ExtractDownloadUrls>d__182::<>7__wrap1
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CU3E7__wrap1_5;
	// System.Int32 YoutubePlayer_<ExtractDownloadUrls>d__182::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___U3CU3E2__current_1)); }
	inline ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_json_3() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___json_3)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_json_3() const { return ___json_3; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_json_3() { return &___json_3; }
	inline void set_json_3(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___json_3 = value;
		Il2CppCodeGenWriteBarrier((&___json_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__json_4() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___U3CU3E3__json_4)); }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * get_U3CU3E3__json_4() const { return ___U3CU3E3__json_4; }
	inline JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 ** get_address_of_U3CU3E3__json_4() { return &___U3CU3E3__json_4; }
	inline void set_U3CU3E3__json_4(JObject_t786AF07B1009334856B0362BBC48EEF68C81C585 * value)
	{
		___U3CU3E3__json_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__json_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_5() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___U3CU3E7__wrap1_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CU3E7__wrap1_5() const { return ___U3CU3E7__wrap1_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CU3E7__wrap1_5() { return &___U3CU3E7__wrap1_5; }
	inline void set_U3CU3E7__wrap1_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CU3E7__wrap1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_6() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0, ___U3CU3E7__wrap2_6)); }
	inline int32_t get_U3CU3E7__wrap2_6() const { return ___U3CU3E7__wrap2_6; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_6() { return &___U3CU3E7__wrap2_6; }
	inline void set_U3CU3E7__wrap2_6(int32_t value)
	{
		___U3CU3E7__wrap2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXTRACTDOWNLOADURLSU3ED__182_TAE74DB3B9B67506155E2C1026736D23DE92C72E0_H
#ifndef U3CHANDHELDPLAYBACKU3ED__81_T659482E3A9ACE1F8CB7FF34642EF62495C8FC108_H
#define U3CHANDHELDPLAYBACKU3ED__81_T659482E3A9ACE1F8CB7FF34642EF62495C8FC108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<HandHeldPlayback>d__81
struct  U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<HandHeldPlayback>d__81::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<HandHeldPlayback>d__81::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<HandHeldPlayback>d__81::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDHELDPLAYBACKU3ED__81_T659482E3A9ACE1F8CB7FF34642EF62495C8FC108_H
#ifndef U3CPLAYNOWU3ED__207_T99425F2CEE479AB8F56537277461D6A0447B2306_H
#define U3CPLAYNOWU3ED__207_T99425F2CEE479AB8F56537277461D6A0447B2306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<PlayNow>d__207
struct  U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<PlayNow>d__207::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<PlayNow>d__207::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<PlayNow>d__207::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYNOWU3ED__207_T99425F2CEE479AB8F56537277461D6A0447B2306_H
#ifndef U3CPLAYNOWFROMTIMEU3ED__210_T06C47CF211837535D1070ABB68A485CE45010DF3_H
#define U3CPLAYNOWFROMTIMEU3ED__210_T06C47CF211837535D1070ABB68A485CE45010DF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<PlayNowFromTime>d__210
struct  U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<PlayNowFromTime>d__210::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<PlayNowFromTime>d__210::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single YoutubePlayer_<PlayNowFromTime>d__210::time
	float ___time_2;
	// YoutubePlayer YoutubePlayer_<PlayNowFromTime>d__210::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3, ___U3CU3E4__this_3)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYNOWFROMTIMEU3ED__210_T06C47CF211837535D1070ABB68A485CE45010DF3_H
#ifndef U3CRELEASEDROPU3ED__209_TEEA5B023C0032076F3E5478ACB01326D952D16D2_H
#define U3CRELEASEDROPU3ED__209_TEEA5B023C0032076F3E5478ACB01326D952D16D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<ReleaseDrop>d__209
struct  U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<ReleaseDrop>d__209::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<ReleaseDrop>d__209::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<ReleaseDrop>d__209::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELEASEDROPU3ED__209_TEEA5B023C0032076F3E5478ACB01326D952D16D2_H
#ifndef U3CSEEKFINISHEDU3ED__132_TBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0_H
#define U3CSEEKFINISHEDU3ED__132_TBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<SeekFinished>d__132
struct  U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<SeekFinished>d__132::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<SeekFinished>d__132::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<SeekFinished>d__132::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSEEKFINISHEDU3ED__132_TBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0_H
#ifndef U3CVIDEOSEEKCALLU3ED__201_T0143670D3E26C95F15A35F1B2F1F4F12A111F846_H
#define U3CVIDEOSEEKCALLU3ED__201_T0143670D3E26C95F15A35F1B2F1F4F12A111F846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<VideoSeekCall>d__201
struct  U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<VideoSeekCall>d__201::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<VideoSeekCall>d__201::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<VideoSeekCall>d__201::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVIDEOSEEKCALLU3ED__201_T0143670D3E26C95F15A35F1B2F1F4F12A111F846_H
#ifndef U3CWAITSYNCU3ED__206_T3DC05EB559174E5F0D350FDCB46E71689E072114_H
#define U3CWAITSYNCU3ED__206_T3DC05EB559174E5F0D350FDCB46E71689E072114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<WaitSync>d__206
struct  U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<WaitSync>d__206::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<WaitSync>d__206::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<WaitSync>d__206::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITSYNCU3ED__206_T3DC05EB559174E5F0D350FDCB46E71689E072114_H
#ifndef U3CWAITTHINGSGETDONEU3ED__59_TCF51A7B26F7641BD5FE165CD206BE8FF7021BC00_H
#define U3CWAITTHINGSGETDONEU3ED__59_TCF51A7B26F7641BD5FE165CD206BE8FF7021BC00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<WaitThingsGetDone>d__59
struct  U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<WaitThingsGetDone>d__59::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<WaitThingsGetDone>d__59::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<WaitThingsGetDone>d__59::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITTHINGSGETDONEU3ED__59_TCF51A7B26F7641BD5FE165CD206BE8FF7021BC00_H
#ifndef U3CWEBGLPLAYU3ED__145_TA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB_H
#define U3CWEBGLPLAYU3ED__145_TA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<WebGLPlay>d__145
struct  U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<WebGLPlay>d__145::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<WebGLPlay>d__145::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayer YoutubePlayer_<WebGLPlay>d__145::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB, ___U3CU3E4__this_2)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWEBGLPLAYU3ED__145_TA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB_H
#ifndef U3CWEBGLREQUESTU3ED__142_T84AF1382F8D485BBA4D46531834EFF86A7ACE78D_H
#define U3CWEBGLREQUESTU3ED__142_T84AF1382F8D485BBA4D46531834EFF86A7ACE78D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<WebGlRequest>d__142
struct  U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<WebGlRequest>d__142::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<WebGlRequest>d__142::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubePlayer_<WebGlRequest>d__142::videoID
	String_t* ___videoID_2;
	// YoutubePlayer YoutubePlayer_<WebGlRequest>d__142::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_3;
	// UnityEngine.WWW YoutubePlayer_<WebGlRequest>d__142::<request>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CrequestU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_videoID_2() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D, ___videoID_2)); }
	inline String_t* get_videoID_2() const { return ___videoID_2; }
	inline String_t** get_address_of_videoID_2() { return &___videoID_2; }
	inline void set_videoID_2(String_t* value)
	{
		___videoID_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoID_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D, ___U3CU3E4__this_3)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D, ___U3CrequestU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CrequestU3E5__2_4() const { return ___U3CrequestU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CrequestU3E5__2_4() { return &___U3CrequestU3E5__2_4; }
	inline void set_U3CrequestU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CrequestU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWEBGLREQUESTU3ED__142_T84AF1382F8D485BBA4D46531834EFF86A7ACE78D_H
#ifndef U3CWEBGLREQUESTU3ED__175_T4B885684C3E719E14DF26D2467119104AF1B20FC_H
#define U3CWEBGLREQUESTU3ED__175_T4B885684C3E719E14DF26D2467119104AF1B20FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<WebGlRequest>d__175
struct  U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<WebGlRequest>d__175::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<WebGlRequest>d__175::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubePlayer_<WebGlRequest>d__175::host
	String_t* ___host_2;
	// System.String YoutubePlayer_<WebGlRequest>d__175::id
	String_t* ___id_3;
	// System.Action`1<System.String> YoutubePlayer_<WebGlRequest>d__175::callback
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___callback_4;
	// UnityEngine.WWW YoutubePlayer_<WebGlRequest>d__175::<request>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CrequestU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_host_2() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC, ___host_2)); }
	inline String_t* get_host_2() const { return ___host_2; }
	inline String_t** get_address_of_host_2() { return &___host_2; }
	inline void set_host_2(String_t* value)
	{
		___host_2 = value;
		Il2CppCodeGenWriteBarrier((&___host_2), value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC, ___callback_4)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_callback_4() const { return ___callback_4; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC, ___U3CrequestU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CrequestU3E5__2_5() const { return ___U3CrequestU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CrequestU3E5__2_5() { return &___U3CrequestU3E5__2_5; }
	inline void set_U3CrequestU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CrequestU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWEBGLREQUESTU3ED__175_T4B885684C3E719E14DF26D2467119104AF1B20FC_H
#ifndef U3CWEBREQUESTU3ED__86_TAE5EFCA98759805C5CD546964C99F34D62F2AB1C_H
#define U3CWEBREQUESTU3ED__86_TAE5EFCA98759805C5CD546964C99F34D62F2AB1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_<WebRequest>d__86
struct  U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayer_<WebRequest>d__86::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayer_<WebRequest>d__86::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubePlayer_<WebRequest>d__86::videoID
	String_t* ___videoID_2;
	// YoutubePlayer YoutubePlayer_<WebRequest>d__86::<>4__this
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___U3CU3E4__this_3;
	// UnityEngine.WWW YoutubePlayer_<WebRequest>d__86::<request>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CrequestU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_videoID_2() { return static_cast<int32_t>(offsetof(U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C, ___videoID_2)); }
	inline String_t* get_videoID_2() const { return ___videoID_2; }
	inline String_t** get_address_of_videoID_2() { return &___videoID_2; }
	inline void set_videoID_2(String_t* value)
	{
		___videoID_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoID_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C, ___U3CU3E4__this_3)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C, ___U3CrequestU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CrequestU3E5__2_4() const { return ___U3CrequestU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CrequestU3E5__2_4() { return &___U3CrequestU3E5__2_4; }
	inline void set_U3CrequestU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CrequestU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWEBREQUESTU3ED__86_TAE5EFCA98759805C5CD546964C99F34D62F2AB1C_H
#ifndef DOWNLOADURLRESPONSE_T20EAB227B69EBCB61EAE43F7186B528FDDA510D7_H
#define DOWNLOADURLRESPONSE_T20EAB227B69EBCB61EAE43F7186B528FDDA510D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_DownloadUrlResponse
struct  DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7  : public RuntimeObject
{
public:
	// System.String YoutubePlayer_DownloadUrlResponse::data
	String_t* ___data_0;
	// System.Boolean YoutubePlayer_DownloadUrlResponse::isValid
	bool ___isValid_1;
	// System.Int64 YoutubePlayer_DownloadUrlResponse::httpCode
	int64_t ___httpCode_2;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7, ___data_0)); }
	inline String_t* get_data_0() const { return ___data_0; }
	inline String_t** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(String_t* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_isValid_1() { return static_cast<int32_t>(offsetof(DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7, ___isValid_1)); }
	inline bool get_isValid_1() const { return ___isValid_1; }
	inline bool* get_address_of_isValid_1() { return &___isValid_1; }
	inline void set_isValid_1(bool value)
	{
		___isValid_1 = value;
	}

	inline static int32_t get_offset_of_httpCode_2() { return static_cast<int32_t>(offsetof(DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7, ___httpCode_2)); }
	inline int64_t get_httpCode_2() const { return ___httpCode_2; }
	inline int64_t* get_address_of_httpCode_2() { return &___httpCode_2; }
	inline void set_httpCode_2(int64_t value)
	{
		___httpCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADURLRESPONSE_T20EAB227B69EBCB61EAE43F7186B528FDDA510D7_H
#ifndef EXTRACTIONINFO_TDD54A07FBB92A8C2B87FAF7AC44CAF3284799491_H
#define EXTRACTIONINFO_TDD54A07FBB92A8C2B87FAF7AC44CAF3284799491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_ExtractionInfo
struct  ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491  : public RuntimeObject
{
public:
	// System.Boolean YoutubePlayer_ExtractionInfo::<RequiresDecryption>k__BackingField
	bool ___U3CRequiresDecryptionU3Ek__BackingField_0;
	// System.Uri YoutubePlayer_ExtractionInfo::<Uri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CUriU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491, ___U3CRequiresDecryptionU3Ek__BackingField_0)); }
	inline bool get_U3CRequiresDecryptionU3Ek__BackingField_0() const { return ___U3CRequiresDecryptionU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CRequiresDecryptionU3Ek__BackingField_0() { return &___U3CRequiresDecryptionU3Ek__BackingField_0; }
	inline void set_U3CRequiresDecryptionU3Ek__BackingField_0(bool value)
	{
		___U3CRequiresDecryptionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491, ___U3CUriU3Ek__BackingField_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CUriU3Ek__BackingField_1() const { return ___U3CUriU3Ek__BackingField_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CUriU3Ek__BackingField_1() { return &___U3CUriU3Ek__BackingField_1; }
	inline void set_U3CUriU3Ek__BackingField_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CUriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUriU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRACTIONINFO_TDD54A07FBB92A8C2B87FAF7AC44CAF3284799491_H
#ifndef HTML5PLAYERRESULT_T8002594BF852CDA0BA81A05ABFFA54142DF3650D_H
#define HTML5PLAYERRESULT_T8002594BF852CDA0BA81A05ABFFA54142DF3650D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_Html5PlayerResult
struct  Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D  : public RuntimeObject
{
public:
	// System.String YoutubePlayer_Html5PlayerResult::scriptName
	String_t* ___scriptName_0;
	// System.String YoutubePlayer_Html5PlayerResult::result
	String_t* ___result_1;
	// System.Boolean YoutubePlayer_Html5PlayerResult::isValid
	bool ___isValid_2;

public:
	inline static int32_t get_offset_of_scriptName_0() { return static_cast<int32_t>(offsetof(Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D, ___scriptName_0)); }
	inline String_t* get_scriptName_0() const { return ___scriptName_0; }
	inline String_t** get_address_of_scriptName_0() { return &___scriptName_0; }
	inline void set_scriptName_0(String_t* value)
	{
		___scriptName_0 = value;
		Il2CppCodeGenWriteBarrier((&___scriptName_0), value);
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D, ___result_1)); }
	inline String_t* get_result_1() const { return ___result_1; }
	inline String_t** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(String_t* value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}

	inline static int32_t get_offset_of_isValid_2() { return static_cast<int32_t>(offsetof(Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D, ___isValid_2)); }
	inline bool get_isValid_2() const { return ___isValid_2; }
	inline bool* get_address_of_isValid_2() { return &___isValid_2; }
	inline void set_isValid_2(bool value)
	{
		___isValid_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTML5PLAYERRESULT_T8002594BF852CDA0BA81A05ABFFA54142DF3650D_H
#ifndef YOUTUBERESULTIDS_T871F84F29672F0B866DB03E724C9C6F616DDCA72_H
#define YOUTUBERESULTIDS_T871F84F29672F0B866DB03E724C9C6F616DDCA72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_YoutubeResultIds
struct  YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72  : public RuntimeObject
{
public:
	// System.String YoutubePlayer_YoutubeResultIds::lowQuality
	String_t* ___lowQuality_0;
	// System.String YoutubePlayer_YoutubeResultIds::standardQuality
	String_t* ___standardQuality_1;
	// System.String YoutubePlayer_YoutubeResultIds::mediumQuality
	String_t* ___mediumQuality_2;
	// System.String YoutubePlayer_YoutubeResultIds::hdQuality
	String_t* ___hdQuality_3;
	// System.String YoutubePlayer_YoutubeResultIds::fullHdQuality
	String_t* ___fullHdQuality_4;
	// System.String YoutubePlayer_YoutubeResultIds::ultraHdQuality
	String_t* ___ultraHdQuality_5;
	// System.String YoutubePlayer_YoutubeResultIds::bestFormatWithAudioIncluded
	String_t* ___bestFormatWithAudioIncluded_6;
	// System.String YoutubePlayer_YoutubeResultIds::audioUrl
	String_t* ___audioUrl_7;

public:
	inline static int32_t get_offset_of_lowQuality_0() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___lowQuality_0)); }
	inline String_t* get_lowQuality_0() const { return ___lowQuality_0; }
	inline String_t** get_address_of_lowQuality_0() { return &___lowQuality_0; }
	inline void set_lowQuality_0(String_t* value)
	{
		___lowQuality_0 = value;
		Il2CppCodeGenWriteBarrier((&___lowQuality_0), value);
	}

	inline static int32_t get_offset_of_standardQuality_1() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___standardQuality_1)); }
	inline String_t* get_standardQuality_1() const { return ___standardQuality_1; }
	inline String_t** get_address_of_standardQuality_1() { return &___standardQuality_1; }
	inline void set_standardQuality_1(String_t* value)
	{
		___standardQuality_1 = value;
		Il2CppCodeGenWriteBarrier((&___standardQuality_1), value);
	}

	inline static int32_t get_offset_of_mediumQuality_2() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___mediumQuality_2)); }
	inline String_t* get_mediumQuality_2() const { return ___mediumQuality_2; }
	inline String_t** get_address_of_mediumQuality_2() { return &___mediumQuality_2; }
	inline void set_mediumQuality_2(String_t* value)
	{
		___mediumQuality_2 = value;
		Il2CppCodeGenWriteBarrier((&___mediumQuality_2), value);
	}

	inline static int32_t get_offset_of_hdQuality_3() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___hdQuality_3)); }
	inline String_t* get_hdQuality_3() const { return ___hdQuality_3; }
	inline String_t** get_address_of_hdQuality_3() { return &___hdQuality_3; }
	inline void set_hdQuality_3(String_t* value)
	{
		___hdQuality_3 = value;
		Il2CppCodeGenWriteBarrier((&___hdQuality_3), value);
	}

	inline static int32_t get_offset_of_fullHdQuality_4() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___fullHdQuality_4)); }
	inline String_t* get_fullHdQuality_4() const { return ___fullHdQuality_4; }
	inline String_t** get_address_of_fullHdQuality_4() { return &___fullHdQuality_4; }
	inline void set_fullHdQuality_4(String_t* value)
	{
		___fullHdQuality_4 = value;
		Il2CppCodeGenWriteBarrier((&___fullHdQuality_4), value);
	}

	inline static int32_t get_offset_of_ultraHdQuality_5() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___ultraHdQuality_5)); }
	inline String_t* get_ultraHdQuality_5() const { return ___ultraHdQuality_5; }
	inline String_t** get_address_of_ultraHdQuality_5() { return &___ultraHdQuality_5; }
	inline void set_ultraHdQuality_5(String_t* value)
	{
		___ultraHdQuality_5 = value;
		Il2CppCodeGenWriteBarrier((&___ultraHdQuality_5), value);
	}

	inline static int32_t get_offset_of_bestFormatWithAudioIncluded_6() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___bestFormatWithAudioIncluded_6)); }
	inline String_t* get_bestFormatWithAudioIncluded_6() const { return ___bestFormatWithAudioIncluded_6; }
	inline String_t** get_address_of_bestFormatWithAudioIncluded_6() { return &___bestFormatWithAudioIncluded_6; }
	inline void set_bestFormatWithAudioIncluded_6(String_t* value)
	{
		___bestFormatWithAudioIncluded_6 = value;
		Il2CppCodeGenWriteBarrier((&___bestFormatWithAudioIncluded_6), value);
	}

	inline static int32_t get_offset_of_audioUrl_7() { return static_cast<int32_t>(offsetof(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72, ___audioUrl_7)); }
	inline String_t* get_audioUrl_7() const { return ___audioUrl_7; }
	inline String_t** get_address_of_audioUrl_7() { return &___audioUrl_7; }
	inline void set_audioUrl_7(String_t* value)
	{
		___audioUrl_7 = value;
		Il2CppCodeGenWriteBarrier((&___audioUrl_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBERESULTIDS_T871F84F29672F0B866DB03E724C9C6F616DDCA72_H
#ifndef U3CDOWNLOADYOUTUBEURLU3ED__5_T6ED7DD5817602D7073D03BD1DD00BBBE7FDED421_H
#define U3CDOWNLOADYOUTUBEURLU3ED__5_T6ED7DD5817602D7073D03BD1DD00BBBE7FDED421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5
struct  U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421  : public RuntimeObject
{
public:
	// System.Int32 YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubePlayerLivestream YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5::<>4__this
	YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781 * ___U3CU3E4__this_2;
	// System.String YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5::url
	String_t* ___url_3;
	// System.Action`1<System.String> YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5::callback
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___callback_4;
	// UnityEngine.Networking.UnityWebRequest YoutubePlayerLivestream_<DownloadYoutubeUrl>d__5::<request>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CrequestU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421, ___U3CU3E4__this_2)); }
	inline YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421, ___callback_4)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_callback_4() const { return ___callback_4; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421, ___U3CrequestU3E5__2_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CrequestU3E5__2_5() const { return ___U3CrequestU3E5__2_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CrequestU3E5__2_5() { return &___U3CrequestU3E5__2_5; }
	inline void set_U3CrequestU3E5__2_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CrequestU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADYOUTUBEURLU3ED__5_T6ED7DD5817602D7073D03BD1DD00BBBE7FDED421_H
#ifndef DOWNLOADURLRESPONSE_T0001923CAE83A206E558485738FDC2CA4EABE3C0_H
#define DOWNLOADURLRESPONSE_T0001923CAE83A206E558485738FDC2CA4EABE3C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayerLivestream_DownloadUrlResponse
struct  DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0  : public RuntimeObject
{
public:
	// System.String YoutubePlayerLivestream_DownloadUrlResponse::data
	String_t* ___data_0;
	// System.Boolean YoutubePlayerLivestream_DownloadUrlResponse::isValid
	bool ___isValid_1;
	// System.Int64 YoutubePlayerLivestream_DownloadUrlResponse::httpCode
	int64_t ___httpCode_2;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0, ___data_0)); }
	inline String_t* get_data_0() const { return ___data_0; }
	inline String_t** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(String_t* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_isValid_1() { return static_cast<int32_t>(offsetof(DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0, ___isValid_1)); }
	inline bool get_isValid_1() const { return ___isValid_1; }
	inline bool* get_address_of_isValid_1() { return &___isValid_1; }
	inline void set_isValid_1(bool value)
	{
		___isValid_1 = value;
	}

	inline static int32_t get_offset_of_httpCode_2() { return static_cast<int32_t>(offsetof(DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0, ___httpCode_2)); }
	inline int64_t get_httpCode_2() const { return ___httpCode_2; }
	inline int64_t* get_address_of_httpCode_2() { return &___httpCode_2; }
	inline void set_httpCode_2(int64_t value)
	{
		___httpCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADURLRESPONSE_T0001923CAE83A206E558485738FDC2CA4EABE3C0_H
#ifndef YOUTUBEPLAYLISTITEMS_T1BDD77262A552453743D02BCC20386029F735935_H
#define YOUTUBEPLAYLISTITEMS_T1BDD77262A552453743D02BCC20386029F735935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlaylistItems
struct  YoutubePlaylistItems_t1BDD77262A552453743D02BCC20386029F735935  : public RuntimeObject
{
public:
	// System.String YoutubePlaylistItems::videoId
	String_t* ___videoId_0;
	// YoutubeSnippet YoutubePlaylistItems::snippet
	YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 * ___snippet_1;

public:
	inline static int32_t get_offset_of_videoId_0() { return static_cast<int32_t>(offsetof(YoutubePlaylistItems_t1BDD77262A552453743D02BCC20386029F735935, ___videoId_0)); }
	inline String_t* get_videoId_0() const { return ___videoId_0; }
	inline String_t** get_address_of_videoId_0() { return &___videoId_0; }
	inline void set_videoId_0(String_t* value)
	{
		___videoId_0 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_0), value);
	}

	inline static int32_t get_offset_of_snippet_1() { return static_cast<int32_t>(offsetof(YoutubePlaylistItems_t1BDD77262A552453743D02BCC20386029F735935, ___snippet_1)); }
	inline YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 * get_snippet_1() const { return ___snippet_1; }
	inline YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 ** get_address_of_snippet_1() { return &___snippet_1; }
	inline void set_snippet_1(YoutubeSnippet_t1BAA2844CEC9DC0DBB3D742F7E9785582DEBBF40 * value)
	{
		___snippet_1 = value;
		Il2CppCodeGenWriteBarrier((&___snippet_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEPLAYLISTITEMS_T1BDD77262A552453743D02BCC20386029F735935_H
#ifndef U3CDOWNLOADTHUMBU3ED__9_TDB6343F840E57517A65748A9BA7C0095F3EB4C38_H
#define U3CDOWNLOADTHUMBU3ED__9_TDB6343F840E57517A65748A9BA7C0095F3EB4C38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeVideoUi_<DownloadThumb>d__9
struct  U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38  : public RuntimeObject
{
public:
	// System.Int32 YoutubeVideoUi_<DownloadThumb>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeVideoUi_<DownloadThumb>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// YoutubeVideoUi YoutubeVideoUi_<DownloadThumb>d__9::<>4__this
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 * ___U3CU3E4__this_2;
	// UnityEngine.WWW YoutubeVideoUi_<DownloadThumb>d__9::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38, ___U3CU3E4__this_2)); }
	inline YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38, ___U3CwwwU3E5__2_3)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_3() const { return ___U3CwwwU3E5__2_3; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_3() { return &___U3CwwwU3E5__2_3; }
	inline void set_U3CwwwU3E5__2_3(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADTHUMBU3ED__9_TDB6343F840E57517A65748A9BA7C0095F3EB4C38_H
#ifndef U3CPLAYVIDEOU3ED__7_T0314F38A3A765DBD38851DD189D6E08BA747F34A_H
#define U3CPLAYVIDEOU3ED__7_T0314F38A3A765DBD38851DD189D6E08BA747F34A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeVideoUi_<PlayVideo>d__7
struct  U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A  : public RuntimeObject
{
public:
	// System.Int32 YoutubeVideoUi_<PlayVideo>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeVideoUi_<PlayVideo>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeVideoUi_<PlayVideo>d__7::url
	String_t* ___url_2;
	// YoutubeVideoUi YoutubeVideoUi_<PlayVideo>d__7::<>4__this
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A, ___U3CU3E4__this_3)); }
	inline YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYVIDEOU3ED__7_T0314F38A3A765DBD38851DD189D6E08BA747F34A_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#define __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#define __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#define SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame
struct  SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifndef SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#define SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize
struct  SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifndef TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#define TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_TC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8_H
#ifndef TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#define TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkID_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifndef TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#define TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifndef TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#define TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifndef TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#define TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifndef TMP_XMLTAGSTACK_1_T2E9DCE707626EAF04E59BA503BA8FF207C8E5605_H
#define TMP_XMLTAGSTACK_1_T2E9DCE707626EAF04E59BA503BA8FF207C8E5605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___itemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2E9DCE707626EAF04E59BA503BA8FF207C8E5605_H
#ifndef TMP_XMLTAGSTACK_1_T6154B3FE95201F122D06B29F910282D903233B3E_H
#define TMP_XMLTAGSTACK_1_T6154B3FE95201F122D06B29F910282D903233B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___itemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T6154B3FE95201F122D06B29F910282D903233B3E_H
#ifndef TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#define TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T357AC63D4EC290220DBE5C99C30F9FD6FEF05063_H
#ifndef TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#define TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifndef DISABLINGTYPE_TD5EF3B874B72B55398354A91A8F5D85D03D2CAF2_H
#define DISABLINGTYPE_TD5EF3B874B72B55398354A91A8F5D85D03D2CAF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawIfAttribute_DisablingType
struct  DisablingType_tD5EF3B874B72B55398354A91A8F5D85D03D2CAF2 
{
public:
	// System.Int32 DrawIfAttribute_DisablingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisablingType_tD5EF3B874B72B55398354A91A8F5D85D03D2CAF2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLINGTYPE_TD5EF3B874B72B55398354A91A8F5D85D03D2CAF2_H
#ifndef PRODUCT_T4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA_H
#define PRODUCT_T4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonNetSample_Product
struct  Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA  : public RuntimeObject
{
public:
	// System.String JsonNetSample_Product::Name
	String_t* ___Name_0;
	// System.DateTime JsonNetSample_Product::ExpiryDate
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___ExpiryDate_1;
	// System.Decimal JsonNetSample_Product::Price
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Price_2;
	// System.String[] JsonNetSample_Product::Sizes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___Sizes_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ExpiryDate_1() { return static_cast<int32_t>(offsetof(Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA, ___ExpiryDate_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_ExpiryDate_1() const { return ___ExpiryDate_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_ExpiryDate_1() { return &___ExpiryDate_1; }
	inline void set_ExpiryDate_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___ExpiryDate_1 = value;
	}

	inline static int32_t get_offset_of_Price_2() { return static_cast<int32_t>(offsetof(Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA, ___Price_2)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Price_2() const { return ___Price_2; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Price_2() { return &___Price_2; }
	inline void set_Price_2(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Price_2 = value;
	}

	inline static int32_t get_offset_of_Sizes_3() { return static_cast<int32_t>(offsetof(Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA, ___Sizes_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_Sizes_3() const { return ___Sizes_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_Sizes_3() { return &___Sizes_3; }
	inline void set_Sizes_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___Sizes_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sizes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCT_T4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA_H
#ifndef VIDEOSIDE_T79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80_H
#define VIDEOSIDE_T79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactingLights_VideoSide
struct  VideoSide_t79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80 
{
public:
	// System.Int32 ReactingLights_VideoSide::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoSide_t79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOSIDE_T79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80_H
#ifndef NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#define NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3290384E361396B3724B88B498CBF637D7E87B78_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#define MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifndef SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#define SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifndef SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#define SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteData
struct  SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::frame
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::spriteSourceSize
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::sourceSize
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___frame_1)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___spriteSourceSize_4)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___sourceSize_5)); }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___pivot_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
#endif // SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#define TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv_1() const { return ___uv_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv2_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv4_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___color_4)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_4() const { return ___color_4; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#define TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifndef TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#define TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99, ___m_defaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_TE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99_H
#ifndef TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#define TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___itemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314, ___m_defaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T27223C90F10F186D303BFD74CFAA4A10F1C06314_H
#ifndef TAGTYPE_T4B5E4B6EA37C80B2B56D26467FD3ED332288E03B_H
#define TAGTYPE_T4B5E4B6EA37C80B2B56D26467FD3ED332288E03B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t4B5E4B6EA37C80B2B56D26467FD3ED332288E03B 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagType_t4B5E4B6EA37C80B2B56D26467FD3ED332288E03B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T4B5E4B6EA37C80B2B56D26467FD3ED332288E03B_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef YOUTUBESAFESEARCHFILTER_T11F93AA4D2DC960FEDA19E3CA6B78513966DB612_H
#define YOUTUBESAFESEARCHFILTER_T11F93AA4D2DC960FEDA19E3CA6B78513966DB612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_YoutubeSafeSearchFilter
struct  YoutubeSafeSearchFilter_t11F93AA4D2DC960FEDA19E3CA6B78513966DB612 
{
public:
	// System.Int32 YoutubeAPIManager_YoutubeSafeSearchFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(YoutubeSafeSearchFilter_t11F93AA4D2DC960FEDA19E3CA6B78513966DB612, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBESAFESEARCHFILTER_T11F93AA4D2DC960FEDA19E3CA6B78513966DB612_H
#ifndef YOUTUBESEARCHORDERFILTER_T93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5_H
#define YOUTUBESEARCHORDERFILTER_T93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_YoutubeSearchOrderFilter
struct  YoutubeSearchOrderFilter_t93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5 
{
public:
	// System.Int32 YoutubeAPIManager_YoutubeSearchOrderFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(YoutubeSearchOrderFilter_t93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBESEARCHORDERFILTER_T93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5_H
#ifndef LAYOUT3D_TC9C3DC3E0D332034238175DDD3BFFA1EE0C24010_H
#define LAYOUT3D_TC9C3DC3E0D332034238175DDD3BFFA1EE0C24010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_Layout3D
struct  Layout3D_tC9C3DC3E0D332034238175DDD3BFFA1EE0C24010 
{
public:
	// System.Int32 YoutubePlayer_Layout3D::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Layout3D_tC9C3DC3E0D332034238175DDD3BFFA1EE0C24010, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUT3D_TC9C3DC3E0D332034238175DDD3BFFA1EE0C24010_H
#ifndef YOUTUBEVIDEOQUALITY_T07911A2842B065614CD64C03F5F15B28524D3084_H
#define YOUTUBEVIDEOQUALITY_T07911A2842B065614CD64C03F5F15B28524D3084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer_YoutubeVideoQuality
struct  YoutubeVideoQuality_t07911A2842B065614CD64C03F5F15B28524D3084 
{
public:
	// System.Int32 YoutubePlayer_YoutubeVideoQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(YoutubeVideoQuality_t07911A2842B065614CD64C03F5F15B28524D3084, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEVIDEOQUALITY_T07911A2842B065614CD64C03F5F15B28524D3084_H
#ifndef DRAWIFATTRIBUTE_TFB34D5F85832EDDEA37958732F33081167B7EA19_H
#define DRAWIFATTRIBUTE_TFB34D5F85832EDDEA37958732F33081167B7EA19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawIfAttribute
struct  DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.String DrawIfAttribute::<comparedPropertyName>k__BackingField
	String_t* ___U3CcomparedPropertyNameU3Ek__BackingField_0;
	// System.Object DrawIfAttribute::<comparedValue>k__BackingField
	RuntimeObject * ___U3CcomparedValueU3Ek__BackingField_1;
	// DrawIfAttribute_DisablingType DrawIfAttribute::<disablingType>k__BackingField
	int32_t ___U3CdisablingTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcomparedPropertyNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19, ___U3CcomparedPropertyNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CcomparedPropertyNameU3Ek__BackingField_0() const { return ___U3CcomparedPropertyNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CcomparedPropertyNameU3Ek__BackingField_0() { return &___U3CcomparedPropertyNameU3Ek__BackingField_0; }
	inline void set_U3CcomparedPropertyNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CcomparedPropertyNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcomparedPropertyNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcomparedValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19, ___U3CcomparedValueU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CcomparedValueU3Ek__BackingField_1() const { return ___U3CcomparedValueU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CcomparedValueU3Ek__BackingField_1() { return &___U3CcomparedValueU3Ek__BackingField_1; }
	inline void set_U3CcomparedValueU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CcomparedValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcomparedValueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdisablingTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19, ___U3CdisablingTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CdisablingTypeU3Ek__BackingField_2() const { return ___U3CdisablingTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CdisablingTypeU3Ek__BackingField_2() { return &___U3CdisablingTypeU3Ek__BackingField_2; }
	inline void set_U3CdisablingTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CdisablingTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWIFATTRIBUTE_TFB34D5F85832EDDEA37958732F33081167B7EA19_H
#ifndef MOVIE_TAAD3A7928035C2C117885E753D47DAF5A01CD9F4_H
#define MOVIE_TAAD3A7928035C2C117885E753D47DAF5A01CD9F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonNetSample_Movie
struct  Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4  : public RuntimeObject
{
public:
	// System.String JsonNetSample_Movie::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String JsonNetSample_Movie::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;
	// System.String JsonNetSample_Movie::<Classification>k__BackingField
	String_t* ___U3CClassificationU3Ek__BackingField_2;
	// System.String JsonNetSample_Movie::<Studio>k__BackingField
	String_t* ___U3CStudioU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> JsonNetSample_Movie::<ReleaseDate>k__BackingField
	Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  ___U3CReleaseDateU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.String> JsonNetSample_Movie::<ReleaseCountries>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CReleaseCountriesU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CClassificationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4, ___U3CClassificationU3Ek__BackingField_2)); }
	inline String_t* get_U3CClassificationU3Ek__BackingField_2() const { return ___U3CClassificationU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CClassificationU3Ek__BackingField_2() { return &___U3CClassificationU3Ek__BackingField_2; }
	inline void set_U3CClassificationU3Ek__BackingField_2(String_t* value)
	{
		___U3CClassificationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClassificationU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CStudioU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4, ___U3CStudioU3Ek__BackingField_3)); }
	inline String_t* get_U3CStudioU3Ek__BackingField_3() const { return ___U3CStudioU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStudioU3Ek__BackingField_3() { return &___U3CStudioU3Ek__BackingField_3; }
	inline void set_U3CStudioU3Ek__BackingField_3(String_t* value)
	{
		___U3CStudioU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStudioU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CReleaseDateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4, ___U3CReleaseDateU3Ek__BackingField_4)); }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  get_U3CReleaseDateU3Ek__BackingField_4() const { return ___U3CReleaseDateU3Ek__BackingField_4; }
	inline Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78 * get_address_of_U3CReleaseDateU3Ek__BackingField_4() { return &___U3CReleaseDateU3Ek__BackingField_4; }
	inline void set_U3CReleaseDateU3Ek__BackingField_4(Nullable_1_t3290384E361396B3724B88B498CBF637D7E87B78  value)
	{
		___U3CReleaseDateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CReleaseCountriesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4, ___U3CReleaseCountriesU3Ek__BackingField_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CReleaseCountriesU3Ek__BackingField_5() const { return ___U3CReleaseCountriesU3Ek__BackingField_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CReleaseCountriesU3Ek__BackingField_5() { return &___U3CReleaseCountriesU3Ek__BackingField_5; }
	inline void set_U3CReleaseCountriesU3Ek__BackingField_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CReleaseCountriesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReleaseCountriesU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVIE_TAAD3A7928035C2C117885E753D47DAF5A01CD9F4_H
#ifndef TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#define TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___textElement_3)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___fontAsset_4)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___material_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_7() const { return ___material_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TL_14)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BL_15)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TR_16)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BR_17)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topLeft_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomLeft_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topRight_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomRight_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___color_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_29() const { return ___color_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_3;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_4;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_17;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_3;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_4;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_17;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#define TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T79B32594D8EFF8CB8078E16347606D8C097A225C_H
#ifndef XML_TAGATTRIBUTE_TE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E_H
#define XML_TAGATTRIBUTE_TE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_TE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef U3CYOUTUBESEARCHU3ED__22_T893DAAAA3B860CFED41948EF46136B5EBBB40155_H
#define U3CYOUTUBESEARCHU3ED__22_T893DAAAA3B860CFED41948EF46136B5EBBB40155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<YoutubeSearch>d__22
struct  U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<YoutubeSearch>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<YoutubeSearch>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<YoutubeSearch>d__22::keyword
	String_t* ___keyword_2;
	// YoutubeAPIManager_YoutubeSearchOrderFilter YoutubeAPIManager_<YoutubeSearch>d__22::order
	int32_t ___order_3;
	// YoutubeAPIManager_YoutubeSafeSearchFilter YoutubeAPIManager_<YoutubeSearch>d__22::safeSearch
	int32_t ___safeSearch_4;
	// System.Int32 YoutubeAPIManager_<YoutubeSearch>d__22::maxresult
	int32_t ___maxresult_5;
	// System.String YoutubeAPIManager_<YoutubeSearch>d__22::customFilters
	String_t* ___customFilters_6;
	// YoutubeAPIManager YoutubeAPIManager_<YoutubeSearch>d__22::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_7;
	// System.Action`1<YoutubeData[]> YoutubeAPIManager_<YoutubeSearch>d__22::callback
	Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * ___callback_8;
	// UnityEngine.WWW YoutubeAPIManager_<YoutubeSearch>d__22::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_keyword_2() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___keyword_2)); }
	inline String_t* get_keyword_2() const { return ___keyword_2; }
	inline String_t** get_address_of_keyword_2() { return &___keyword_2; }
	inline void set_keyword_2(String_t* value)
	{
		___keyword_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyword_2), value);
	}

	inline static int32_t get_offset_of_order_3() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___order_3)); }
	inline int32_t get_order_3() const { return ___order_3; }
	inline int32_t* get_address_of_order_3() { return &___order_3; }
	inline void set_order_3(int32_t value)
	{
		___order_3 = value;
	}

	inline static int32_t get_offset_of_safeSearch_4() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___safeSearch_4)); }
	inline int32_t get_safeSearch_4() const { return ___safeSearch_4; }
	inline int32_t* get_address_of_safeSearch_4() { return &___safeSearch_4; }
	inline void set_safeSearch_4(int32_t value)
	{
		___safeSearch_4 = value;
	}

	inline static int32_t get_offset_of_maxresult_5() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___maxresult_5)); }
	inline int32_t get_maxresult_5() const { return ___maxresult_5; }
	inline int32_t* get_address_of_maxresult_5() { return &___maxresult_5; }
	inline void set_maxresult_5(int32_t value)
	{
		___maxresult_5 = value;
	}

	inline static int32_t get_offset_of_customFilters_6() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___customFilters_6)); }
	inline String_t* get_customFilters_6() const { return ___customFilters_6; }
	inline String_t** get_address_of_customFilters_6() { return &___customFilters_6; }
	inline void set_customFilters_6(String_t* value)
	{
		___customFilters_6 = value;
		Il2CppCodeGenWriteBarrier((&___customFilters_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___U3CU3E4__this_7)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_callback_8() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___callback_8)); }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * get_callback_8() const { return ___callback_8; }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 ** get_address_of_callback_8() { return &___callback_8; }
	inline void set_callback_8(Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * value)
	{
		___callback_8 = value;
		Il2CppCodeGenWriteBarrier((&___callback_8), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155, ___U3CcallU3E5__2_9)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_9() const { return ___U3CcallU3E5__2_9; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_9() { return &___U3CcallU3E5__2_9; }
	inline void set_U3CcallU3E5__2_9(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBESEARCHU3ED__22_T893DAAAA3B860CFED41948EF46136B5EBBB40155_H
#ifndef U3CYOUTUBESEARCHBYLOCATIONU3ED__23_T06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378_H
#define U3CYOUTUBESEARCHBYLOCATIONU3ED__23_T06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<YoutubeSearchByLocation>d__23
struct  U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<YoutubeSearchByLocation>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<YoutubeSearchByLocation>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<YoutubeSearchByLocation>d__23::keyword
	String_t* ___keyword_2;
	// YoutubeAPIManager_YoutubeSearchOrderFilter YoutubeAPIManager_<YoutubeSearchByLocation>d__23::order
	int32_t ___order_3;
	// YoutubeAPIManager_YoutubeSafeSearchFilter YoutubeAPIManager_<YoutubeSearchByLocation>d__23::safeSearch
	int32_t ___safeSearch_4;
	// System.Int32 YoutubeAPIManager_<YoutubeSearchByLocation>d__23::locationRadius
	int32_t ___locationRadius_5;
	// System.Single YoutubeAPIManager_<YoutubeSearchByLocation>d__23::latitude
	float ___latitude_6;
	// System.Single YoutubeAPIManager_<YoutubeSearchByLocation>d__23::longitude
	float ___longitude_7;
	// System.Int32 YoutubeAPIManager_<YoutubeSearchByLocation>d__23::maxResult
	int32_t ___maxResult_8;
	// System.String YoutubeAPIManager_<YoutubeSearchByLocation>d__23::customFilters
	String_t* ___customFilters_9;
	// YoutubeAPIManager YoutubeAPIManager_<YoutubeSearchByLocation>d__23::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_10;
	// System.Action`1<YoutubeData[]> YoutubeAPIManager_<YoutubeSearchByLocation>d__23::callback
	Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * ___callback_11;
	// UnityEngine.WWW YoutubeAPIManager_<YoutubeSearchByLocation>d__23::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_keyword_2() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___keyword_2)); }
	inline String_t* get_keyword_2() const { return ___keyword_2; }
	inline String_t** get_address_of_keyword_2() { return &___keyword_2; }
	inline void set_keyword_2(String_t* value)
	{
		___keyword_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyword_2), value);
	}

	inline static int32_t get_offset_of_order_3() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___order_3)); }
	inline int32_t get_order_3() const { return ___order_3; }
	inline int32_t* get_address_of_order_3() { return &___order_3; }
	inline void set_order_3(int32_t value)
	{
		___order_3 = value;
	}

	inline static int32_t get_offset_of_safeSearch_4() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___safeSearch_4)); }
	inline int32_t get_safeSearch_4() const { return ___safeSearch_4; }
	inline int32_t* get_address_of_safeSearch_4() { return &___safeSearch_4; }
	inline void set_safeSearch_4(int32_t value)
	{
		___safeSearch_4 = value;
	}

	inline static int32_t get_offset_of_locationRadius_5() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___locationRadius_5)); }
	inline int32_t get_locationRadius_5() const { return ___locationRadius_5; }
	inline int32_t* get_address_of_locationRadius_5() { return &___locationRadius_5; }
	inline void set_locationRadius_5(int32_t value)
	{
		___locationRadius_5 = value;
	}

	inline static int32_t get_offset_of_latitude_6() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___latitude_6)); }
	inline float get_latitude_6() const { return ___latitude_6; }
	inline float* get_address_of_latitude_6() { return &___latitude_6; }
	inline void set_latitude_6(float value)
	{
		___latitude_6 = value;
	}

	inline static int32_t get_offset_of_longitude_7() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___longitude_7)); }
	inline float get_longitude_7() const { return ___longitude_7; }
	inline float* get_address_of_longitude_7() { return &___longitude_7; }
	inline void set_longitude_7(float value)
	{
		___longitude_7 = value;
	}

	inline static int32_t get_offset_of_maxResult_8() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___maxResult_8)); }
	inline int32_t get_maxResult_8() const { return ___maxResult_8; }
	inline int32_t* get_address_of_maxResult_8() { return &___maxResult_8; }
	inline void set_maxResult_8(int32_t value)
	{
		___maxResult_8 = value;
	}

	inline static int32_t get_offset_of_customFilters_9() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___customFilters_9)); }
	inline String_t* get_customFilters_9() const { return ___customFilters_9; }
	inline String_t** get_address_of_customFilters_9() { return &___customFilters_9; }
	inline void set_customFilters_9(String_t* value)
	{
		___customFilters_9 = value;
		Il2CppCodeGenWriteBarrier((&___customFilters_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_10() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___U3CU3E4__this_10)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_10() const { return ___U3CU3E4__this_10; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_10() { return &___U3CU3E4__this_10; }
	inline void set_U3CU3E4__this_10(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_10), value);
	}

	inline static int32_t get_offset_of_callback_11() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___callback_11)); }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * get_callback_11() const { return ___callback_11; }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 ** get_address_of_callback_11() { return &___callback_11; }
	inline void set_callback_11(Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * value)
	{
		___callback_11 = value;
		Il2CppCodeGenWriteBarrier((&___callback_11), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_12() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378, ___U3CcallU3E5__2_12)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_12() const { return ___U3CcallU3E5__2_12; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_12() { return &___U3CcallU3E5__2_12; }
	inline void set_U3CcallU3E5__2_12(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBESEARCHBYLOCATIONU3ED__23_T06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378_H
#ifndef U3CYOUTUBESEARCHCHANNELU3ED__20_T6DD609AF81381188637C022C719A691A88A14A01_H
#define U3CYOUTUBESEARCHCHANNELU3ED__20_T6DD609AF81381188637C022C719A691A88A14A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<YoutubeSearchChannel>d__20
struct  U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<YoutubeSearchChannel>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<YoutubeSearchChannel>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<YoutubeSearchChannel>d__20::keyword
	String_t* ___keyword_2;
	// YoutubeAPIManager_YoutubeSearchOrderFilter YoutubeAPIManager_<YoutubeSearchChannel>d__20::order
	int32_t ___order_3;
	// YoutubeAPIManager_YoutubeSafeSearchFilter YoutubeAPIManager_<YoutubeSearchChannel>d__20::safeSearch
	int32_t ___safeSearch_4;
	// System.Int32 YoutubeAPIManager_<YoutubeSearchChannel>d__20::maxresult
	int32_t ___maxresult_5;
	// YoutubeAPIManager YoutubeAPIManager_<YoutubeSearchChannel>d__20::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_6;
	// System.Action`1<YoutubeChannel[]> YoutubeAPIManager_<YoutubeSearchChannel>d__20::callback
	Action_1_tD73791EE6B43F1321439ACC8583C8983BCFCAD6E * ___callback_7;
	// UnityEngine.WWW YoutubeAPIManager_<YoutubeSearchChannel>d__20::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_keyword_2() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___keyword_2)); }
	inline String_t* get_keyword_2() const { return ___keyword_2; }
	inline String_t** get_address_of_keyword_2() { return &___keyword_2; }
	inline void set_keyword_2(String_t* value)
	{
		___keyword_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyword_2), value);
	}

	inline static int32_t get_offset_of_order_3() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___order_3)); }
	inline int32_t get_order_3() const { return ___order_3; }
	inline int32_t* get_address_of_order_3() { return &___order_3; }
	inline void set_order_3(int32_t value)
	{
		___order_3 = value;
	}

	inline static int32_t get_offset_of_safeSearch_4() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___safeSearch_4)); }
	inline int32_t get_safeSearch_4() const { return ___safeSearch_4; }
	inline int32_t* get_address_of_safeSearch_4() { return &___safeSearch_4; }
	inline void set_safeSearch_4(int32_t value)
	{
		___safeSearch_4 = value;
	}

	inline static int32_t get_offset_of_maxresult_5() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___maxresult_5)); }
	inline int32_t get_maxresult_5() const { return ___maxresult_5; }
	inline int32_t* get_address_of_maxresult_5() { return &___maxresult_5; }
	inline void set_maxresult_5(int32_t value)
	{
		___maxresult_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___U3CU3E4__this_6)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_6), value);
	}

	inline static int32_t get_offset_of_callback_7() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___callback_7)); }
	inline Action_1_tD73791EE6B43F1321439ACC8583C8983BCFCAD6E * get_callback_7() const { return ___callback_7; }
	inline Action_1_tD73791EE6B43F1321439ACC8583C8983BCFCAD6E ** get_address_of_callback_7() { return &___callback_7; }
	inline void set_callback_7(Action_1_tD73791EE6B43F1321439ACC8583C8983BCFCAD6E * value)
	{
		___callback_7 = value;
		Il2CppCodeGenWriteBarrier((&___callback_7), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01, ___U3CcallU3E5__2_8)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_8() const { return ___U3CcallU3E5__2_8; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_8() { return &___U3CcallU3E5__2_8; }
	inline void set_U3CcallU3E5__2_8(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBESEARCHCHANNELU3ED__20_T6DD609AF81381188637C022C719A691A88A14A01_H
#ifndef U3CYOUTUBESEARCHUSINGCATEGORYU3ED__19_T6733188493E29DDFD48B9DD12742840C6FE16824_H
#define U3CYOUTUBESEARCHUSINGCATEGORYU3ED__19_T6733188493E29DDFD48B9DD12742840C6FE16824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19
struct  U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824  : public RuntimeObject
{
public:
	// System.Int32 YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::keyword
	String_t* ___keyword_2;
	// System.String YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::category
	String_t* ___category_3;
	// YoutubeAPIManager_YoutubeSearchOrderFilter YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::order
	int32_t ___order_4;
	// YoutubeAPIManager_YoutubeSafeSearchFilter YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::safeSearch
	int32_t ___safeSearch_5;
	// System.Int32 YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::maxresult
	int32_t ___maxresult_6;
	// YoutubeAPIManager YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::<>4__this
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___U3CU3E4__this_7;
	// System.Action`1<YoutubeData[]> YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::callback
	Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * ___callback_8;
	// UnityEngine.WWW YoutubeAPIManager_<YoutubeSearchUsingCategory>d__19::<call>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CcallU3E5__2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_keyword_2() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___keyword_2)); }
	inline String_t* get_keyword_2() const { return ___keyword_2; }
	inline String_t** get_address_of_keyword_2() { return &___keyword_2; }
	inline void set_keyword_2(String_t* value)
	{
		___keyword_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyword_2), value);
	}

	inline static int32_t get_offset_of_category_3() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___category_3)); }
	inline String_t* get_category_3() const { return ___category_3; }
	inline String_t** get_address_of_category_3() { return &___category_3; }
	inline void set_category_3(String_t* value)
	{
		___category_3 = value;
		Il2CppCodeGenWriteBarrier((&___category_3), value);
	}

	inline static int32_t get_offset_of_order_4() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___order_4)); }
	inline int32_t get_order_4() const { return ___order_4; }
	inline int32_t* get_address_of_order_4() { return &___order_4; }
	inline void set_order_4(int32_t value)
	{
		___order_4 = value;
	}

	inline static int32_t get_offset_of_safeSearch_5() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___safeSearch_5)); }
	inline int32_t get_safeSearch_5() const { return ___safeSearch_5; }
	inline int32_t* get_address_of_safeSearch_5() { return &___safeSearch_5; }
	inline void set_safeSearch_5(int32_t value)
	{
		___safeSearch_5 = value;
	}

	inline static int32_t get_offset_of_maxresult_6() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___maxresult_6)); }
	inline int32_t get_maxresult_6() const { return ___maxresult_6; }
	inline int32_t* get_address_of_maxresult_6() { return &___maxresult_6; }
	inline void set_maxresult_6(int32_t value)
	{
		___maxresult_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___U3CU3E4__this_7)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_callback_8() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___callback_8)); }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * get_callback_8() const { return ___callback_8; }
	inline Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 ** get_address_of_callback_8() { return &___callback_8; }
	inline void set_callback_8(Action_1_tBD7D919FE789205EAFC294E8172E4703242AA472 * value)
	{
		___callback_8 = value;
		Il2CppCodeGenWriteBarrier((&___callback_8), value);
	}

	inline static int32_t get_offset_of_U3CcallU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824, ___U3CcallU3E5__2_9)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CcallU3E5__2_9() const { return ___U3CcallU3E5__2_9; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CcallU3E5__2_9() { return &___U3CcallU3E5__2_9; }
	inline void set_U3CcallU3E5__2_9(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CcallU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcallU3E5__2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CYOUTUBESEARCHUSINGCATEGORYU3ED__19_T6733188493E29DDFD48B9DD12742840C6FE16824_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___sizeStack_39;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___indentStack_40;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___styleStack_42;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___baselineStack_43;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___actionStack_44;
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_BasicXmlTagStack_tC5F410B9A4A8A8DE6351FA5F0FBF92F8E2CC8BF8  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___colorStack_34;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t27223C90F10F186D303BFD74CFAA4A10F1C06314  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t357AC63D4EC290220DBE5C99C30F9FD6FEF05063  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___sizeStack_39;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___indentStack_40;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___styleStack_42;
	TMP_XmlTagStack_1_t6154B3FE95201F122D06B29F910282D903233B3E  ___baselineStack_43;
	TMP_XmlTagStack_1_t2E9DCE707626EAF04E59BA503BA8FF207C8E5605  ___actionStack_44;
	TMP_XmlTagStack_1_tE3F1A1DBF5C066777F7FD28E182B6A14FCCC8A99  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t79B32594D8EFF8CB8078E16347606D8C097A225C  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CHANNELSEARCHDEMO_T097478CD98051F166C5A2053C1B7A0158B5A64A1_H
#define CHANNELSEARCHDEMO_T097478CD98051F166C5A2053C1B7A0158B5A64A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChannelSearchDemo
struct  ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubeAPIManager ChannelSearchDemo::youtubeapi
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___youtubeapi_4;
	// UnityEngine.UI.Text ChannelSearchDemo::searchField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___searchField_5;
	// UnityEngine.UI.Dropdown ChannelSearchDemo::mainFilters
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___mainFilters_6;
	// UnityEngine.GameObject[] ChannelSearchDemo::channelListUI
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___channelListUI_7;
	// UnityEngine.GameObject[] ChannelSearchDemo::videoListUI
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___videoListUI_8;
	// UnityEngine.GameObject ChannelSearchDemo::videoUIResult
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoUIResult_9;
	// UnityEngine.GameObject ChannelSearchDemo::channelUIResult
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___channelUIResult_10;
	// UnityEngine.GameObject ChannelSearchDemo::mainUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainUI_11;

public:
	inline static int32_t get_offset_of_youtubeapi_4() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___youtubeapi_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_youtubeapi_4() const { return ___youtubeapi_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_youtubeapi_4() { return &___youtubeapi_4; }
	inline void set_youtubeapi_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___youtubeapi_4 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeapi_4), value);
	}

	inline static int32_t get_offset_of_searchField_5() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___searchField_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_searchField_5() const { return ___searchField_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_searchField_5() { return &___searchField_5; }
	inline void set_searchField_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___searchField_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchField_5), value);
	}

	inline static int32_t get_offset_of_mainFilters_6() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___mainFilters_6)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_mainFilters_6() const { return ___mainFilters_6; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_mainFilters_6() { return &___mainFilters_6; }
	inline void set_mainFilters_6(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___mainFilters_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainFilters_6), value);
	}

	inline static int32_t get_offset_of_channelListUI_7() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___channelListUI_7)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_channelListUI_7() const { return ___channelListUI_7; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_channelListUI_7() { return &___channelListUI_7; }
	inline void set_channelListUI_7(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___channelListUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___channelListUI_7), value);
	}

	inline static int32_t get_offset_of_videoListUI_8() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___videoListUI_8)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_videoListUI_8() const { return ___videoListUI_8; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_videoListUI_8() { return &___videoListUI_8; }
	inline void set_videoListUI_8(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___videoListUI_8 = value;
		Il2CppCodeGenWriteBarrier((&___videoListUI_8), value);
	}

	inline static int32_t get_offset_of_videoUIResult_9() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___videoUIResult_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoUIResult_9() const { return ___videoUIResult_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoUIResult_9() { return &___videoUIResult_9; }
	inline void set_videoUIResult_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoUIResult_9 = value;
		Il2CppCodeGenWriteBarrier((&___videoUIResult_9), value);
	}

	inline static int32_t get_offset_of_channelUIResult_10() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___channelUIResult_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_channelUIResult_10() const { return ___channelUIResult_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_channelUIResult_10() { return &___channelUIResult_10; }
	inline void set_channelUIResult_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___channelUIResult_10 = value;
		Il2CppCodeGenWriteBarrier((&___channelUIResult_10), value);
	}

	inline static int32_t get_offset_of_mainUI_11() { return static_cast<int32_t>(offsetof(ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1, ___mainUI_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainUI_11() const { return ___mainUI_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainUI_11() { return &___mainUI_11; }
	inline void set_mainUI_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainUI_11 = value;
		Il2CppCodeGenWriteBarrier((&___mainUI_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELSEARCHDEMO_T097478CD98051F166C5A2053C1B7A0158B5A64A1_H
#ifndef COMMENTSDEMO_TB49AFB20698B4E468598865F0028D96D4D53286F_H
#define COMMENTSDEMO_TB49AFB20698B4E468598865F0028D96D4D53286F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommentsDemo
struct  CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubeAPIManager CommentsDemo::youtubeapi
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___youtubeapi_4;
	// UnityEngine.UI.Text CommentsDemo::videoIdInput
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoIdInput_5;
	// UnityEngine.UI.Text CommentsDemo::commentsTextArea
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___commentsTextArea_6;

public:
	inline static int32_t get_offset_of_youtubeapi_4() { return static_cast<int32_t>(offsetof(CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F, ___youtubeapi_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_youtubeapi_4() const { return ___youtubeapi_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_youtubeapi_4() { return &___youtubeapi_4; }
	inline void set_youtubeapi_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___youtubeapi_4 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeapi_4), value);
	}

	inline static int32_t get_offset_of_videoIdInput_5() { return static_cast<int32_t>(offsetof(CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F, ___videoIdInput_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoIdInput_5() const { return ___videoIdInput_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoIdInput_5() { return &___videoIdInput_5; }
	inline void set_videoIdInput_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoIdInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoIdInput_5), value);
	}

	inline static int32_t get_offset_of_commentsTextArea_6() { return static_cast<int32_t>(offsetof(CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F, ___commentsTextArea_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_commentsTextArea_6() const { return ___commentsTextArea_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_commentsTextArea_6() { return &___commentsTextArea_6; }
	inline void set_commentsTextArea_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___commentsTextArea_6 = value;
		Il2CppCodeGenWriteBarrier((&___commentsTextArea_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENTSDEMO_TB49AFB20698B4E468598865F0028D96D4D53286F_H
#ifndef INDIVIDUALVIDEODATADEMO_T01047817DF72C0BD49492079C4A1B1F961A6D4F1_H
#define INDIVIDUALVIDEODATADEMO_T01047817DF72C0BD49492079C4A1B1F961A6D4F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IndividualVideoDataDemo
struct  IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubeAPIManager IndividualVideoDataDemo::youtubeapi
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___youtubeapi_4;
	// UnityEngine.UI.Text IndividualVideoDataDemo::videoIdInput
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoIdInput_5;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_title_6;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_description_7;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_duration
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_duration_8;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_likes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_likes_9;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_dislikes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_dislikes_10;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_favorites
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_favorites_11;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_comments
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_comments_12;
	// UnityEngine.UI.Text IndividualVideoDataDemo::UI_views
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UI_views_13;
	// UnityEngine.UI.Image IndividualVideoDataDemo::UI_thumbnail
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___UI_thumbnail_14;

public:
	inline static int32_t get_offset_of_youtubeapi_4() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___youtubeapi_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_youtubeapi_4() const { return ___youtubeapi_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_youtubeapi_4() { return &___youtubeapi_4; }
	inline void set_youtubeapi_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___youtubeapi_4 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeapi_4), value);
	}

	inline static int32_t get_offset_of_videoIdInput_5() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___videoIdInput_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoIdInput_5() const { return ___videoIdInput_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoIdInput_5() { return &___videoIdInput_5; }
	inline void set_videoIdInput_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoIdInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoIdInput_5), value);
	}

	inline static int32_t get_offset_of_UI_title_6() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_title_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_title_6() const { return ___UI_title_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_title_6() { return &___UI_title_6; }
	inline void set_UI_title_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_title_6 = value;
		Il2CppCodeGenWriteBarrier((&___UI_title_6), value);
	}

	inline static int32_t get_offset_of_UI_description_7() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_description_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_description_7() const { return ___UI_description_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_description_7() { return &___UI_description_7; }
	inline void set_UI_description_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_description_7 = value;
		Il2CppCodeGenWriteBarrier((&___UI_description_7), value);
	}

	inline static int32_t get_offset_of_UI_duration_8() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_duration_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_duration_8() const { return ___UI_duration_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_duration_8() { return &___UI_duration_8; }
	inline void set_UI_duration_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_duration_8 = value;
		Il2CppCodeGenWriteBarrier((&___UI_duration_8), value);
	}

	inline static int32_t get_offset_of_UI_likes_9() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_likes_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_likes_9() const { return ___UI_likes_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_likes_9() { return &___UI_likes_9; }
	inline void set_UI_likes_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_likes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UI_likes_9), value);
	}

	inline static int32_t get_offset_of_UI_dislikes_10() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_dislikes_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_dislikes_10() const { return ___UI_dislikes_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_dislikes_10() { return &___UI_dislikes_10; }
	inline void set_UI_dislikes_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_dislikes_10 = value;
		Il2CppCodeGenWriteBarrier((&___UI_dislikes_10), value);
	}

	inline static int32_t get_offset_of_UI_favorites_11() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_favorites_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_favorites_11() const { return ___UI_favorites_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_favorites_11() { return &___UI_favorites_11; }
	inline void set_UI_favorites_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_favorites_11 = value;
		Il2CppCodeGenWriteBarrier((&___UI_favorites_11), value);
	}

	inline static int32_t get_offset_of_UI_comments_12() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_comments_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_comments_12() const { return ___UI_comments_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_comments_12() { return &___UI_comments_12; }
	inline void set_UI_comments_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_comments_12 = value;
		Il2CppCodeGenWriteBarrier((&___UI_comments_12), value);
	}

	inline static int32_t get_offset_of_UI_views_13() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_views_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UI_views_13() const { return ___UI_views_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UI_views_13() { return &___UI_views_13; }
	inline void set_UI_views_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UI_views_13 = value;
		Il2CppCodeGenWriteBarrier((&___UI_views_13), value);
	}

	inline static int32_t get_offset_of_UI_thumbnail_14() { return static_cast<int32_t>(offsetof(IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1, ___UI_thumbnail_14)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_UI_thumbnail_14() const { return ___UI_thumbnail_14; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_UI_thumbnail_14() { return &___UI_thumbnail_14; }
	inline void set_UI_thumbnail_14(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___UI_thumbnail_14 = value;
		Il2CppCodeGenWriteBarrier((&___UI_thumbnail_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDIVIDUALVIDEODATADEMO_T01047817DF72C0BD49492079C4A1B1F961A6D4F1_H
#ifndef JSONNETSAMPLE_TD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B_H
#define JSONNETSAMPLE_TD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonNetSample
struct  JsonNetSample_tD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text JsonNetSample::Output
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Output_4;

public:
	inline static int32_t get_offset_of_Output_4() { return static_cast<int32_t>(offsetof(JsonNetSample_tD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B, ___Output_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Output_4() const { return ___Output_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Output_4() { return &___Output_4; }
	inline void set_Output_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Output_4 = value;
		Il2CppCodeGenWriteBarrier((&___Output_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNETSAMPLE_TD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B_H
#ifndef LOADVIDEOTITLETOTEXT_TFA2AA750483C80117F612FD194AF6AEE72DD68C8_H
#define LOADVIDEOTITLETOTEXT_TFA2AA750483C80117F612FD194AF6AEE72DD68C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadVideoTitleToText
struct  LoadVideoTitleToText_tFA2AA750483C80117F612FD194AF6AEE72DD68C8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh LoadVideoTitleToText::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// YoutubePlayer LoadVideoTitleToText::player
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___player_5;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(LoadVideoTitleToText_tFA2AA750483C80117F612FD194AF6AEE72DD68C8, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(LoadVideoTitleToText_tFA2AA750483C80117F612FD194AF6AEE72DD68C8, ___player_5)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_player_5() const { return ___player_5; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADVIDEOTITLETOTEXT_TFA2AA750483C80117F612FD194AF6AEE72DD68C8_H
#ifndef PAUSEICON_T791F62301726B18B3D32766456DB0563A7C98C2B_H
#define PAUSEICON_T791F62301726B18B3D32766456DB0563A7C98C2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseIcon
struct  PauseIcon_t791F62301726B18B3D32766456DB0563A7C98C2B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubePlayer PauseIcon::p
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___p_4;
	// UnityEngine.UI.Text PauseIcon::label
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___label_5;

public:
	inline static int32_t get_offset_of_p_4() { return static_cast<int32_t>(offsetof(PauseIcon_t791F62301726B18B3D32766456DB0563A7C98C2B, ___p_4)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_p_4() const { return ___p_4; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_p_4() { return &___p_4; }
	inline void set_p_4(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___p_4 = value;
		Il2CppCodeGenWriteBarrier((&___p_4), value);
	}

	inline static int32_t get_offset_of_label_5() { return static_cast<int32_t>(offsetof(PauseIcon_t791F62301726B18B3D32766456DB0563A7C98C2B, ___label_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_label_5() const { return ___label_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_label_5() { return &___label_5; }
	inline void set_label_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___label_5 = value;
		Il2CppCodeGenWriteBarrier((&___label_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEICON_T791F62301726B18B3D32766456DB0563A7C98C2B_H
#ifndef PLAYLISTDEMO_TB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F_H
#define PLAYLISTDEMO_TB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaylistDemo
struct  PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubeAPIManager PlaylistDemo::youtubeapi
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___youtubeapi_4;
	// UnityEngine.UI.Text PlaylistDemo::searchField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___searchField_5;
	// YoutubeVideoUi[] PlaylistDemo::videoListUI
	YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1* ___videoListUI_6;
	// UnityEngine.GameObject PlaylistDemo::videoUIResult
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoUIResult_7;
	// UnityEngine.GameObject PlaylistDemo::mainUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainUI_8;

public:
	inline static int32_t get_offset_of_youtubeapi_4() { return static_cast<int32_t>(offsetof(PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F, ___youtubeapi_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_youtubeapi_4() const { return ___youtubeapi_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_youtubeapi_4() { return &___youtubeapi_4; }
	inline void set_youtubeapi_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___youtubeapi_4 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeapi_4), value);
	}

	inline static int32_t get_offset_of_searchField_5() { return static_cast<int32_t>(offsetof(PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F, ___searchField_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_searchField_5() const { return ___searchField_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_searchField_5() { return &___searchField_5; }
	inline void set_searchField_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___searchField_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchField_5), value);
	}

	inline static int32_t get_offset_of_videoListUI_6() { return static_cast<int32_t>(offsetof(PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F, ___videoListUI_6)); }
	inline YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1* get_videoListUI_6() const { return ___videoListUI_6; }
	inline YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1** get_address_of_videoListUI_6() { return &___videoListUI_6; }
	inline void set_videoListUI_6(YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1* value)
	{
		___videoListUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoListUI_6), value);
	}

	inline static int32_t get_offset_of_videoUIResult_7() { return static_cast<int32_t>(offsetof(PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F, ___videoUIResult_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoUIResult_7() const { return ___videoUIResult_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoUIResult_7() { return &___videoUIResult_7; }
	inline void set_videoUIResult_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoUIResult_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoUIResult_7), value);
	}

	inline static int32_t get_offset_of_mainUI_8() { return static_cast<int32_t>(offsetof(PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F, ___mainUI_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainUI_8() const { return ___mainUI_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainUI_8() { return &___mainUI_8; }
	inline void set_mainUI_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainUI_8 = value;
		Il2CppCodeGenWriteBarrier((&___mainUI_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYLISTDEMO_TB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F_H
#ifndef REACTINGLIGHTS_T6B6F94D4EB341521400A2ED402AADE75A0ACB995_H
#define REACTINGLIGHTS_T6B6F94D4EB341521400A2ED402AADE75A0ACB995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactingLights
struct  ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Video.VideoPlayer ReactingLights::videoSource
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * ___videoSource_4;
	// UnityEngine.Light[] ReactingLights::lights
	LightU5BU5D_t0127F29C5C02312DE2DDA721E3AF8CE925297D45* ___lights_5;
	// UnityEngine.Color ReactingLights::averageColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___averageColor_6;
	// UnityEngine.Texture2D ReactingLights::tex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___tex_7;
	// ReactingLights_VideoSide ReactingLights::videoSide
	int32_t ___videoSide_8;
	// System.Boolean ReactingLights::createTexture
	bool ___createTexture_9;

public:
	inline static int32_t get_offset_of_videoSource_4() { return static_cast<int32_t>(offsetof(ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995, ___videoSource_4)); }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * get_videoSource_4() const { return ___videoSource_4; }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 ** get_address_of_videoSource_4() { return &___videoSource_4; }
	inline void set_videoSource_4(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * value)
	{
		___videoSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoSource_4), value);
	}

	inline static int32_t get_offset_of_lights_5() { return static_cast<int32_t>(offsetof(ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995, ___lights_5)); }
	inline LightU5BU5D_t0127F29C5C02312DE2DDA721E3AF8CE925297D45* get_lights_5() const { return ___lights_5; }
	inline LightU5BU5D_t0127F29C5C02312DE2DDA721E3AF8CE925297D45** get_address_of_lights_5() { return &___lights_5; }
	inline void set_lights_5(LightU5BU5D_t0127F29C5C02312DE2DDA721E3AF8CE925297D45* value)
	{
		___lights_5 = value;
		Il2CppCodeGenWriteBarrier((&___lights_5), value);
	}

	inline static int32_t get_offset_of_averageColor_6() { return static_cast<int32_t>(offsetof(ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995, ___averageColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_averageColor_6() const { return ___averageColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_averageColor_6() { return &___averageColor_6; }
	inline void set_averageColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___averageColor_6 = value;
	}

	inline static int32_t get_offset_of_tex_7() { return static_cast<int32_t>(offsetof(ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995, ___tex_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_tex_7() const { return ___tex_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_tex_7() { return &___tex_7; }
	inline void set_tex_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___tex_7 = value;
		Il2CppCodeGenWriteBarrier((&___tex_7), value);
	}

	inline static int32_t get_offset_of_videoSide_8() { return static_cast<int32_t>(offsetof(ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995, ___videoSide_8)); }
	inline int32_t get_videoSide_8() const { return ___videoSide_8; }
	inline int32_t* get_address_of_videoSide_8() { return &___videoSide_8; }
	inline void set_videoSide_8(int32_t value)
	{
		___videoSide_8 = value;
	}

	inline static int32_t get_offset_of_createTexture_9() { return static_cast<int32_t>(offsetof(ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995, ___createTexture_9)); }
	inline bool get_createTexture_9() const { return ___createTexture_9; }
	inline bool* get_address_of_createTexture_9() { return &___createTexture_9; }
	inline void set_createTexture_9(bool value)
	{
		___createTexture_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTINGLIGHTS_T6B6F94D4EB341521400A2ED402AADE75A0ACB995_H
#ifndef SCROLLINGUVS_LAYERS_TA721733151A507101E41565EDCF13A2E95CA4C4B_H
#define SCROLLINGUVS_LAYERS_TA721733151A507101E41565EDCF13A2E95CA4C4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollingUVs_Layers
struct  ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 ScrollingUVs_Layers::uvAnimationRate
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uvAnimationRate_4;
	// System.String ScrollingUVs_Layers::textureName
	String_t* ___textureName_5;
	// UnityEngine.Vector2 ScrollingUVs_Layers::uvOffset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uvOffset_6;

public:
	inline static int32_t get_offset_of_uvAnimationRate_4() { return static_cast<int32_t>(offsetof(ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B, ___uvAnimationRate_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uvAnimationRate_4() const { return ___uvAnimationRate_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uvAnimationRate_4() { return &___uvAnimationRate_4; }
	inline void set_uvAnimationRate_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uvAnimationRate_4 = value;
	}

	inline static int32_t get_offset_of_textureName_5() { return static_cast<int32_t>(offsetof(ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B, ___textureName_5)); }
	inline String_t* get_textureName_5() const { return ___textureName_5; }
	inline String_t** get_address_of_textureName_5() { return &___textureName_5; }
	inline void set_textureName_5(String_t* value)
	{
		___textureName_5 = value;
		Il2CppCodeGenWriteBarrier((&___textureName_5), value);
	}

	inline static int32_t get_offset_of_uvOffset_6() { return static_cast<int32_t>(offsetof(ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B, ___uvOffset_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uvOffset_6() const { return ___uvOffset_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uvOffset_6() { return &___uvOffset_6; }
	inline void set_uvOffset_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uvOffset_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINGUVS_LAYERS_TA721733151A507101E41565EDCF13A2E95CA4C4B_H
#ifndef VIDEOPROGRESSBAR_TCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D_H
#define VIDEOPROGRESSBAR_TCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoProgressBar
struct  VideoProgressBar_tCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubePlayer VideoProgressBar::player
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * ___player_4;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(VideoProgressBar_tCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D, ___player_4)); }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * get_player_4() const { return ___player_4; }
	inline YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPROGRESSBAR_TCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D_H
#ifndef VIDEOSEARCHDEMO_TFF78D62C194041157F1E22E124D2AC1D9D0406F6_H
#define VIDEOSEARCHDEMO_TFF78D62C194041157F1E22E124D2AC1D9D0406F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoSearchDemo
struct  VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubeAPIManager VideoSearchDemo::youtubeapi
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * ___youtubeapi_4;
	// UnityEngine.UI.Text VideoSearchDemo::searchField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___searchField_5;
	// UnityEngine.UI.Text VideoSearchDemo::categoryField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___categoryField_6;
	// UnityEngine.UI.Toggle VideoSearchDemo::categoryFilter
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___categoryFilter_7;
	// UnityEngine.UI.Dropdown VideoSearchDemo::mainFilters
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___mainFilters_8;
	// YoutubeVideoUi[] VideoSearchDemo::videoListUI
	YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1* ___videoListUI_9;
	// UnityEngine.GameObject VideoSearchDemo::videoUIResult
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoUIResult_10;
	// UnityEngine.GameObject VideoSearchDemo::mainUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainUI_11;
	// System.String VideoSearchDemo::customFilter
	String_t* ___customFilter_12;
	// System.String VideoSearchDemo::regionCode
	String_t* ___regionCode_13;

public:
	inline static int32_t get_offset_of_youtubeapi_4() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___youtubeapi_4)); }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * get_youtubeapi_4() const { return ___youtubeapi_4; }
	inline YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA ** get_address_of_youtubeapi_4() { return &___youtubeapi_4; }
	inline void set_youtubeapi_4(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA * value)
	{
		___youtubeapi_4 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeapi_4), value);
	}

	inline static int32_t get_offset_of_searchField_5() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___searchField_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_searchField_5() const { return ___searchField_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_searchField_5() { return &___searchField_5; }
	inline void set_searchField_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___searchField_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchField_5), value);
	}

	inline static int32_t get_offset_of_categoryField_6() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___categoryField_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_categoryField_6() const { return ___categoryField_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_categoryField_6() { return &___categoryField_6; }
	inline void set_categoryField_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___categoryField_6 = value;
		Il2CppCodeGenWriteBarrier((&___categoryField_6), value);
	}

	inline static int32_t get_offset_of_categoryFilter_7() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___categoryFilter_7)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_categoryFilter_7() const { return ___categoryFilter_7; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_categoryFilter_7() { return &___categoryFilter_7; }
	inline void set_categoryFilter_7(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___categoryFilter_7 = value;
		Il2CppCodeGenWriteBarrier((&___categoryFilter_7), value);
	}

	inline static int32_t get_offset_of_mainFilters_8() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___mainFilters_8)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_mainFilters_8() const { return ___mainFilters_8; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_mainFilters_8() { return &___mainFilters_8; }
	inline void set_mainFilters_8(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___mainFilters_8 = value;
		Il2CppCodeGenWriteBarrier((&___mainFilters_8), value);
	}

	inline static int32_t get_offset_of_videoListUI_9() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___videoListUI_9)); }
	inline YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1* get_videoListUI_9() const { return ___videoListUI_9; }
	inline YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1** get_address_of_videoListUI_9() { return &___videoListUI_9; }
	inline void set_videoListUI_9(YoutubeVideoUiU5BU5D_tA282F12395412A56F60820BA9FDFF90EC53A53C1* value)
	{
		___videoListUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___videoListUI_9), value);
	}

	inline static int32_t get_offset_of_videoUIResult_10() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___videoUIResult_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoUIResult_10() const { return ___videoUIResult_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoUIResult_10() { return &___videoUIResult_10; }
	inline void set_videoUIResult_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoUIResult_10 = value;
		Il2CppCodeGenWriteBarrier((&___videoUIResult_10), value);
	}

	inline static int32_t get_offset_of_mainUI_11() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___mainUI_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainUI_11() const { return ___mainUI_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainUI_11() { return &___mainUI_11; }
	inline void set_mainUI_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainUI_11 = value;
		Il2CppCodeGenWriteBarrier((&___mainUI_11), value);
	}

	inline static int32_t get_offset_of_customFilter_12() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___customFilter_12)); }
	inline String_t* get_customFilter_12() const { return ___customFilter_12; }
	inline String_t** get_address_of_customFilter_12() { return &___customFilter_12; }
	inline void set_customFilter_12(String_t* value)
	{
		___customFilter_12 = value;
		Il2CppCodeGenWriteBarrier((&___customFilter_12), value);
	}

	inline static int32_t get_offset_of_regionCode_13() { return static_cast<int32_t>(offsetof(VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6, ___regionCode_13)); }
	inline String_t* get_regionCode_13() const { return ___regionCode_13; }
	inline String_t** get_address_of_regionCode_13() { return &___regionCode_13; }
	inline void set_regionCode_13(String_t* value)
	{
		___regionCode_13 = value;
		Il2CppCodeGenWriteBarrier((&___regionCode_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOSEARCHDEMO_TFF78D62C194041157F1E22E124D2AC1D9D0406F6_H
#ifndef YT_ROTATECAMERA_T29D99C8D2F2F0AF832170C0B54DB754655E77051_H
#define YT_ROTATECAMERA_T29D99C8D2F2F0AF832170C0B54DB754655E77051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YT_RotateCamera
struct  YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single YT_RotateCamera::speedH
	float ___speedH_4;
	// System.Single YT_RotateCamera::speedV
	float ___speedV_5;
	// System.Single YT_RotateCamera::yaw
	float ___yaw_6;
	// System.Single YT_RotateCamera::pitch
	float ___pitch_7;

public:
	inline static int32_t get_offset_of_speedH_4() { return static_cast<int32_t>(offsetof(YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051, ___speedH_4)); }
	inline float get_speedH_4() const { return ___speedH_4; }
	inline float* get_address_of_speedH_4() { return &___speedH_4; }
	inline void set_speedH_4(float value)
	{
		___speedH_4 = value;
	}

	inline static int32_t get_offset_of_speedV_5() { return static_cast<int32_t>(offsetof(YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051, ___speedV_5)); }
	inline float get_speedV_5() const { return ___speedV_5; }
	inline float* get_address_of_speedV_5() { return &___speedV_5; }
	inline void set_speedV_5(float value)
	{
		___speedV_5 = value;
	}

	inline static int32_t get_offset_of_yaw_6() { return static_cast<int32_t>(offsetof(YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051, ___yaw_6)); }
	inline float get_yaw_6() const { return ___yaw_6; }
	inline float* get_address_of_yaw_6() { return &___yaw_6; }
	inline void set_yaw_6(float value)
	{
		___yaw_6 = value;
	}

	inline static int32_t get_offset_of_pitch_7() { return static_cast<int32_t>(offsetof(YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051, ___pitch_7)); }
	inline float get_pitch_7() const { return ___pitch_7; }
	inline float* get_address_of_pitch_7() { return &___pitch_7; }
	inline void set_pitch_7(float value)
	{
		___pitch_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YT_ROTATECAMERA_T29D99C8D2F2F0AF832170C0B54DB754655E77051_H
#ifndef YOUTUBEAPIMANAGER_T923AAEDD3919D7A60CA5CB3ECDC100897B9026AA_H
#define YOUTUBEAPIMANAGER_T923AAEDD3919D7A60CA5CB3ECDC100897B9026AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeAPIManager
struct  YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// YoutubeData YoutubeAPIManager::data
	YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524 * ___data_4;
	// YoutubeData[] YoutubeAPIManager::searchResults
	YoutubeDataU5BU5D_t88CB245129BD34373C8542E7C2D6D99C5A62F246* ___searchResults_5;
	// YoutubeComments[] YoutubeAPIManager::comments
	YoutubeCommentsU5BU5D_t404F53A20F66D5C10BFD71572D5CF42B58189391* ___comments_6;
	// YoutubePlaylistItems[] YoutubeAPIManager::playslistItems
	YoutubePlaylistItemsU5BU5D_t0B56487C233EB3272A08008F13D1C0FF2E09FDE9* ___playslistItems_7;
	// YoutubeChannel[] YoutubeAPIManager::channels
	YoutubeChannelU5BU5D_t764C46D6F1C16A738F5C14BE6CB8EA36A5819095* ___channels_8;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA, ___data_4)); }
	inline YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524 * get_data_4() const { return ___data_4; }
	inline YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_searchResults_5() { return static_cast<int32_t>(offsetof(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA, ___searchResults_5)); }
	inline YoutubeDataU5BU5D_t88CB245129BD34373C8542E7C2D6D99C5A62F246* get_searchResults_5() const { return ___searchResults_5; }
	inline YoutubeDataU5BU5D_t88CB245129BD34373C8542E7C2D6D99C5A62F246** get_address_of_searchResults_5() { return &___searchResults_5; }
	inline void set_searchResults_5(YoutubeDataU5BU5D_t88CB245129BD34373C8542E7C2D6D99C5A62F246* value)
	{
		___searchResults_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchResults_5), value);
	}

	inline static int32_t get_offset_of_comments_6() { return static_cast<int32_t>(offsetof(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA, ___comments_6)); }
	inline YoutubeCommentsU5BU5D_t404F53A20F66D5C10BFD71572D5CF42B58189391* get_comments_6() const { return ___comments_6; }
	inline YoutubeCommentsU5BU5D_t404F53A20F66D5C10BFD71572D5CF42B58189391** get_address_of_comments_6() { return &___comments_6; }
	inline void set_comments_6(YoutubeCommentsU5BU5D_t404F53A20F66D5C10BFD71572D5CF42B58189391* value)
	{
		___comments_6 = value;
		Il2CppCodeGenWriteBarrier((&___comments_6), value);
	}

	inline static int32_t get_offset_of_playslistItems_7() { return static_cast<int32_t>(offsetof(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA, ___playslistItems_7)); }
	inline YoutubePlaylistItemsU5BU5D_t0B56487C233EB3272A08008F13D1C0FF2E09FDE9* get_playslistItems_7() const { return ___playslistItems_7; }
	inline YoutubePlaylistItemsU5BU5D_t0B56487C233EB3272A08008F13D1C0FF2E09FDE9** get_address_of_playslistItems_7() { return &___playslistItems_7; }
	inline void set_playslistItems_7(YoutubePlaylistItemsU5BU5D_t0B56487C233EB3272A08008F13D1C0FF2E09FDE9* value)
	{
		___playslistItems_7 = value;
		Il2CppCodeGenWriteBarrier((&___playslistItems_7), value);
	}

	inline static int32_t get_offset_of_channels_8() { return static_cast<int32_t>(offsetof(YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA, ___channels_8)); }
	inline YoutubeChannelU5BU5D_t764C46D6F1C16A738F5C14BE6CB8EA36A5819095* get_channels_8() const { return ___channels_8; }
	inline YoutubeChannelU5BU5D_t764C46D6F1C16A738F5C14BE6CB8EA36A5819095** get_address_of_channels_8() { return &___channels_8; }
	inline void set_channels_8(YoutubeChannelU5BU5D_t764C46D6F1C16A738F5C14BE6CB8EA36A5819095* value)
	{
		___channels_8 = value;
		Il2CppCodeGenWriteBarrier((&___channels_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEAPIMANAGER_T923AAEDD3919D7A60CA5CB3ECDC100897B9026AA_H
#ifndef YOUTUBEAPIGETUNLIMITEDVIDEOS_TDCD3F252F6094ACEC1812B242D3D1D63FD6AE295_H
#define YOUTUBEAPIGETUNLIMITEDVIDEOS_TDCD3F252F6094ACEC1812B242D3D1D63FD6AE295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeApiGetUnlimitedVideos
struct  YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String YoutubeApiGetUnlimitedVideos::APIKey
	String_t* ___APIKey_4;
	// System.String YoutubeApiGetUnlimitedVideos::searchKeyword
	String_t* ___searchKeyword_5;
	// System.Collections.Generic.List`1<YoutubeData> YoutubeApiGetUnlimitedVideos::searchResults
	List_1_t7A6300D520BCEB6B1E2EBFBCECE00056B526E0DB * ___searchResults_6;
	// System.Int32 YoutubeApiGetUnlimitedVideos::currentResults
	int32_t ___currentResults_7;
	// System.Int32 YoutubeApiGetUnlimitedVideos::maxResult
	int32_t ___maxResult_8;

public:
	inline static int32_t get_offset_of_APIKey_4() { return static_cast<int32_t>(offsetof(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295, ___APIKey_4)); }
	inline String_t* get_APIKey_4() const { return ___APIKey_4; }
	inline String_t** get_address_of_APIKey_4() { return &___APIKey_4; }
	inline void set_APIKey_4(String_t* value)
	{
		___APIKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___APIKey_4), value);
	}

	inline static int32_t get_offset_of_searchKeyword_5() { return static_cast<int32_t>(offsetof(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295, ___searchKeyword_5)); }
	inline String_t* get_searchKeyword_5() const { return ___searchKeyword_5; }
	inline String_t** get_address_of_searchKeyword_5() { return &___searchKeyword_5; }
	inline void set_searchKeyword_5(String_t* value)
	{
		___searchKeyword_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchKeyword_5), value);
	}

	inline static int32_t get_offset_of_searchResults_6() { return static_cast<int32_t>(offsetof(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295, ___searchResults_6)); }
	inline List_1_t7A6300D520BCEB6B1E2EBFBCECE00056B526E0DB * get_searchResults_6() const { return ___searchResults_6; }
	inline List_1_t7A6300D520BCEB6B1E2EBFBCECE00056B526E0DB ** get_address_of_searchResults_6() { return &___searchResults_6; }
	inline void set_searchResults_6(List_1_t7A6300D520BCEB6B1E2EBFBCECE00056B526E0DB * value)
	{
		___searchResults_6 = value;
		Il2CppCodeGenWriteBarrier((&___searchResults_6), value);
	}

	inline static int32_t get_offset_of_currentResults_7() { return static_cast<int32_t>(offsetof(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295, ___currentResults_7)); }
	inline int32_t get_currentResults_7() const { return ___currentResults_7; }
	inline int32_t* get_address_of_currentResults_7() { return &___currentResults_7; }
	inline void set_currentResults_7(int32_t value)
	{
		___currentResults_7 = value;
	}

	inline static int32_t get_offset_of_maxResult_8() { return static_cast<int32_t>(offsetof(YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295, ___maxResult_8)); }
	inline int32_t get_maxResult_8() const { return ___maxResult_8; }
	inline int32_t* get_address_of_maxResult_8() { return &___maxResult_8; }
	inline void set_maxResult_8(int32_t value)
	{
		___maxResult_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEAPIGETUNLIMITEDVIDEOS_TDCD3F252F6094ACEC1812B242D3D1D63FD6AE295_H
#ifndef YOUTUBECHANNELUI_T9C4465D3BCE9B58F4E22406034474E4112C32D15_H
#define YOUTUBECHANNELUI_T9C4465D3BCE9B58F4E22406034474E4112C32D15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeChannelUI
struct  YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text YoutubeChannelUI::videoName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoName_4;
	// System.String YoutubeChannelUI::videoId
	String_t* ___videoId_5;
	// System.String YoutubeChannelUI::thumbUrl
	String_t* ___thumbUrl_6;
	// UnityEngine.UI.Image YoutubeChannelUI::videoThumb
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___videoThumb_7;

public:
	inline static int32_t get_offset_of_videoName_4() { return static_cast<int32_t>(offsetof(YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15, ___videoName_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoName_4() const { return ___videoName_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoName_4() { return &___videoName_4; }
	inline void set_videoName_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoName_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoName_4), value);
	}

	inline static int32_t get_offset_of_videoId_5() { return static_cast<int32_t>(offsetof(YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15, ___videoId_5)); }
	inline String_t* get_videoId_5() const { return ___videoId_5; }
	inline String_t** get_address_of_videoId_5() { return &___videoId_5; }
	inline void set_videoId_5(String_t* value)
	{
		___videoId_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_5), value);
	}

	inline static int32_t get_offset_of_thumbUrl_6() { return static_cast<int32_t>(offsetof(YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15, ___thumbUrl_6)); }
	inline String_t* get_thumbUrl_6() const { return ___thumbUrl_6; }
	inline String_t** get_address_of_thumbUrl_6() { return &___thumbUrl_6; }
	inline void set_thumbUrl_6(String_t* value)
	{
		___thumbUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___thumbUrl_6), value);
	}

	inline static int32_t get_offset_of_videoThumb_7() { return static_cast<int32_t>(offsetof(YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15, ___videoThumb_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_videoThumb_7() const { return ___videoThumb_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_videoThumb_7() { return &___videoThumb_7; }
	inline void set_videoThumb_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___videoThumb_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoThumb_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBECHANNELUI_T9C4465D3BCE9B58F4E22406034474E4112C32D15_H
#ifndef YOUTUBELOGO_T85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE_H
#define YOUTUBELOGO_T85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeLogo
struct  YoutubeLogo_t85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String YoutubeLogo::youtubeurl
	String_t* ___youtubeurl_4;

public:
	inline static int32_t get_offset_of_youtubeurl_4() { return static_cast<int32_t>(offsetof(YoutubeLogo_t85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE, ___youtubeurl_4)); }
	inline String_t* get_youtubeurl_4() const { return ___youtubeurl_4; }
	inline String_t** get_address_of_youtubeurl_4() { return &___youtubeurl_4; }
	inline void set_youtubeurl_4(String_t* value)
	{
		___youtubeurl_4 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeurl_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBELOGO_T85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE_H
#ifndef YOUTUBEPLAYER_TD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_H
#define YOUTUBEPLAYER_TD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayer
struct  YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String YoutubePlayer::youtubeUrl
	String_t* ___youtubeUrl_5;
	// System.Boolean YoutubePlayer::startFromSecond
	bool ___startFromSecond_6;
	// System.Int32 YoutubePlayer::startFromSecondTime
	int32_t ___startFromSecondTime_7;
	// System.Boolean YoutubePlayer::autoPlayOnStart
	bool ___autoPlayOnStart_8;
	// System.Boolean YoutubePlayer::autoPlayOnEnable
	bool ___autoPlayOnEnable_9;
	// System.Boolean YoutubePlayer::playUsingInternalDevicePlayer
	bool ___playUsingInternalDevicePlayer_10;
	// System.Boolean YoutubePlayer::loadYoutubeUrlsOnly
	bool ___loadYoutubeUrlsOnly_11;
	// System.Boolean YoutubePlayer::is3DLayoutVideo
	bool ___is3DLayoutVideo_12;
	// YoutubePlayer_Layout3D YoutubePlayer::layout3d
	int32_t ___layout3d_13;
	// UnityEngine.GameObject YoutubePlayer::videoControllerCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoControllerCanvas_14;
	// UnityEngine.Camera YoutubePlayer::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_15;
	// YoutubePlayer_YoutubeVideoQuality YoutubePlayer::videoQuality
	int32_t ___videoQuality_16;
	// UnityEngine.GameObject YoutubePlayer::loadingContent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___loadingContent_17;
	// UnityEngine.Events.UnityEvent YoutubePlayer::OnYoutubeUrlAreReady
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnYoutubeUrlAreReady_18;
	// UnityEngine.Events.UnityEvent YoutubePlayer::OnVideoReadyToStart
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnVideoReadyToStart_19;
	// UnityEngine.Events.UnityEvent YoutubePlayer::OnVideoStarted
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnVideoStarted_20;
	// UnityEngine.Events.UnityEvent YoutubePlayer::OnVideoPaused
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnVideoPaused_21;
	// UnityEngine.Events.UnityEvent YoutubePlayer::OnVideoFinished
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnVideoFinished_22;
	// UnityEngine.GameObject[] YoutubePlayer::objectsToRenderTheVideoImage
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___objectsToRenderTheVideoImage_23;
	// UnityEngine.Video.VideoPlayer YoutubePlayer::videoPlayer
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * ___videoPlayer_24;
	// UnityEngine.Video.VideoPlayer YoutubePlayer::audioPlayer
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * ___audioPlayer_25;
	// System.Boolean YoutubePlayer::debug
	bool ___debug_26;
	// System.String YoutubePlayer::videoUrl
	String_t* ___videoUrl_27;
	// System.String YoutubePlayer::audioUrl
	String_t* ___audioUrl_28;
	// System.Boolean YoutubePlayer::ForceGetWebServer
	bool ___ForceGetWebServer_29;
	// System.Boolean YoutubePlayer::showPlayerControls
	bool ___showPlayerControls_30;
	// System.Int32 YoutubePlayer::maxRequestTime
	int32_t ___maxRequestTime_31;
	// System.Single YoutubePlayer::currentRequestTime
	float ___currentRequestTime_32;
	// System.Int32 YoutubePlayer::retryTimeUntilToRequestFromServer
	int32_t ___retryTimeUntilToRequestFromServer_33;
	// System.Int32 YoutubePlayer::currentRetryTime
	int32_t ___currentRetryTime_34;
	// System.Boolean YoutubePlayer::gettingYoutubeURL
	bool ___gettingYoutubeURL_35;
	// System.Boolean YoutubePlayer::videoAreReadyToPlay
	bool ___videoAreReadyToPlay_36;
	// System.Single YoutubePlayer::lastPlayTime
	float ___lastPlayTime_37;
	// System.Boolean YoutubePlayer::audioDecryptDone
	bool ___audioDecryptDone_38;
	// System.Boolean YoutubePlayer::videoDecryptDone
	bool ___videoDecryptDone_39;
	// System.Boolean YoutubePlayer::videoPrepared
	bool ___videoPrepared_40;
	// System.Boolean YoutubePlayer::audioPrepared
	bool ___audioPrepared_41;
	// System.Boolean YoutubePlayer::isRetry
	bool ___isRetry_42;
	// System.String YoutubePlayer::lastTryVideoId
	String_t* ___lastTryVideoId_43;
	// System.Single YoutubePlayer::lastStartedTime
	float ___lastStartedTime_44;
	// System.Boolean YoutubePlayer::youtubeUrlReady
	bool ___youtubeUrlReady_45;
	// YoutubePlayer_YoutubeResultIds YoutubePlayer::newRequestResults
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 * ___newRequestResults_46;
	// System.Boolean YoutubePlayer::fullscreenModeEnabled
	bool ___fullscreenModeEnabled_50;
	// System.Int64 YoutubePlayer::lastFrame
	int64_t ___lastFrame_51;
	// System.Boolean YoutubePlayer::videoEnded
	bool ___videoEnded_52;
	// System.Boolean YoutubePlayer::noAudioAtacched
	bool ___noAudioAtacched_53;
	// System.String YoutubePlayer::videoTitle
	String_t* ___videoTitle_54;
	// System.Boolean YoutubePlayer::startedFromTime
	bool ___startedFromTime_55;
	// System.Boolean YoutubePlayer::videoStarted
	bool ___videoStarted_56;
	// System.Single YoutubePlayer::lastErrorTime
	float ___lastErrorTime_57;
	// System.Boolean YoutubePlayer::pauseCalled
	bool ___pauseCalled_58;
	// System.Int32 YoutubePlayer::autoHideControlsTime
	int32_t ___autoHideControlsTime_59;
	// UnityEngine.GameObject YoutubePlayer::mainControllerUi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainControllerUi_60;
	// UnityEngine.UI.Image YoutubePlayer::progress
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___progress_61;
	// UnityEngine.UI.Slider YoutubePlayer::volumeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___volumeSlider_62;
	// UnityEngine.UI.Slider YoutubePlayer::playbackSpeed
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___playbackSpeed_63;
	// UnityEngine.UI.Text YoutubePlayer::currentTimeString
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___currentTimeString_64;
	// UnityEngine.UI.Text YoutubePlayer::totalTimeString
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___totalTimeString_65;
	// System.Single YoutubePlayer::totalVideoDuration
	float ___totalVideoDuration_66;
	// System.Single YoutubePlayer::currentVideoDuration
	float ___currentVideoDuration_67;
	// System.Boolean YoutubePlayer::videoSeekDone
	bool ___videoSeekDone_68;
	// System.Boolean YoutubePlayer::videoAudioSeekDone
	bool ___videoAudioSeekDone_69;
	// System.Boolean YoutubePlayer::lowRes
	bool ___lowRes_70;
	// System.Single YoutubePlayer::hideScreenTime
	float ___hideScreenTime_71;
	// System.Single YoutubePlayer::audioDuration
	float ___audioDuration_72;
	// System.Boolean YoutubePlayer::showingPlaybackSpeed
	bool ___showingPlaybackSpeed_73;
	// System.Boolean YoutubePlayer::showingVolume
	bool ___showingVolume_74;
	// System.Boolean YoutubePlayer::seekUsingLowQuality
	bool ___seekUsingLowQuality_75;
	// System.String YoutubePlayer::lsigForVideo
	String_t* ___lsigForVideo_76;
	// System.String YoutubePlayer::lsigForAudio
	String_t* ___lsigForAudio_77;
	// System.Threading.Thread YoutubePlayer::thread1
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___thread1_78;
	// YoutubePlayer_YoutubeResultIds YoutubePlayer::webGlResults
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 * ___webGlResults_79;
	// System.Boolean YoutubePlayer::startedPlayingWebgl
	bool ___startedPlayingWebgl_80;
	// System.String YoutubePlayer::logTest
	String_t* ___logTest_81;
	// System.Boolean YoutubePlayer::isSyncing
	bool ___isSyncing_82;
	// System.Boolean YoutubePlayer::showThumbnailBeforeVideoLoad
	bool ___showThumbnailBeforeVideoLoad_83;
	// System.String YoutubePlayer::thumbnailVideoID
	String_t* ___thumbnailVideoID_84;
	// System.String YoutubePlayer::encryptedSignatureVideo
	String_t* ___encryptedSignatureVideo_87;
	// System.String YoutubePlayer::encryptedSignatureAudio
	String_t* ___encryptedSignatureAudio_88;
	// System.String YoutubePlayer::masterURLForVideo
	String_t* ___masterURLForVideo_89;
	// System.String YoutubePlayer::masterURLForAudio
	String_t* ___masterURLForAudio_90;
	// System.String[] YoutubePlayer::patternNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___patternNames_91;
	// System.Int32 YoutubePlayer::patternIndex
	int32_t ___patternIndex_92;
	// System.Boolean YoutubePlayer::decryptedUrlForVideo
	bool ___decryptedUrlForVideo_93;
	// System.Boolean YoutubePlayer::decryptedUrlForAudio
	bool ___decryptedUrlForAudio_94;
	// System.String YoutubePlayer::decryptedVideoUrlResult
	String_t* ___decryptedVideoUrlResult_95;
	// System.String YoutubePlayer::decryptedAudioUrlResult
	String_t* ___decryptedAudioUrlResult_96;
	// System.Collections.Generic.List`1<YoutubeLight.VideoInfo> YoutubePlayer::youtubeVideoInfos
	List_1_t7334C42E62574D2DD9BDF1730A044AA1CEECEC26 * ___youtubeVideoInfos_97;
	// System.String YoutubePlayer::htmlVersion
	String_t* ___htmlVersion_98;
	// System.String YoutubePlayer::EncryptUrlForVideo
	String_t* ___EncryptUrlForVideo_100;
	// System.String YoutubePlayer::EncryptUrlForAudio
	String_t* ___EncryptUrlForAudio_101;
	// YoutubePlayer_DownloadUrlResponse YoutubePlayer::downloadYoutubeUrlResponse
	DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7 * ___downloadYoutubeUrlResponse_102;
	// System.Boolean YoutubePlayer::waitAudioSeek
	bool ___waitAudioSeek_103;
	// System.Boolean YoutubePlayer::notSeeking
	bool ___notSeeking_104;
	// System.Boolean YoutubePlayer::ignoreDrop
	bool ___ignoreDrop_105;
	// System.Boolean YoutubePlayer::dropAlreadyCalled
	bool ___dropAlreadyCalled_106;
	// System.Boolean YoutubePlayer::checkIfSync
	bool ___checkIfSync_107;

public:
	inline static int32_t get_offset_of_youtubeUrl_5() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___youtubeUrl_5)); }
	inline String_t* get_youtubeUrl_5() const { return ___youtubeUrl_5; }
	inline String_t** get_address_of_youtubeUrl_5() { return &___youtubeUrl_5; }
	inline void set_youtubeUrl_5(String_t* value)
	{
		___youtubeUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeUrl_5), value);
	}

	inline static int32_t get_offset_of_startFromSecond_6() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___startFromSecond_6)); }
	inline bool get_startFromSecond_6() const { return ___startFromSecond_6; }
	inline bool* get_address_of_startFromSecond_6() { return &___startFromSecond_6; }
	inline void set_startFromSecond_6(bool value)
	{
		___startFromSecond_6 = value;
	}

	inline static int32_t get_offset_of_startFromSecondTime_7() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___startFromSecondTime_7)); }
	inline int32_t get_startFromSecondTime_7() const { return ___startFromSecondTime_7; }
	inline int32_t* get_address_of_startFromSecondTime_7() { return &___startFromSecondTime_7; }
	inline void set_startFromSecondTime_7(int32_t value)
	{
		___startFromSecondTime_7 = value;
	}

	inline static int32_t get_offset_of_autoPlayOnStart_8() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___autoPlayOnStart_8)); }
	inline bool get_autoPlayOnStart_8() const { return ___autoPlayOnStart_8; }
	inline bool* get_address_of_autoPlayOnStart_8() { return &___autoPlayOnStart_8; }
	inline void set_autoPlayOnStart_8(bool value)
	{
		___autoPlayOnStart_8 = value;
	}

	inline static int32_t get_offset_of_autoPlayOnEnable_9() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___autoPlayOnEnable_9)); }
	inline bool get_autoPlayOnEnable_9() const { return ___autoPlayOnEnable_9; }
	inline bool* get_address_of_autoPlayOnEnable_9() { return &___autoPlayOnEnable_9; }
	inline void set_autoPlayOnEnable_9(bool value)
	{
		___autoPlayOnEnable_9 = value;
	}

	inline static int32_t get_offset_of_playUsingInternalDevicePlayer_10() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___playUsingInternalDevicePlayer_10)); }
	inline bool get_playUsingInternalDevicePlayer_10() const { return ___playUsingInternalDevicePlayer_10; }
	inline bool* get_address_of_playUsingInternalDevicePlayer_10() { return &___playUsingInternalDevicePlayer_10; }
	inline void set_playUsingInternalDevicePlayer_10(bool value)
	{
		___playUsingInternalDevicePlayer_10 = value;
	}

	inline static int32_t get_offset_of_loadYoutubeUrlsOnly_11() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___loadYoutubeUrlsOnly_11)); }
	inline bool get_loadYoutubeUrlsOnly_11() const { return ___loadYoutubeUrlsOnly_11; }
	inline bool* get_address_of_loadYoutubeUrlsOnly_11() { return &___loadYoutubeUrlsOnly_11; }
	inline void set_loadYoutubeUrlsOnly_11(bool value)
	{
		___loadYoutubeUrlsOnly_11 = value;
	}

	inline static int32_t get_offset_of_is3DLayoutVideo_12() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___is3DLayoutVideo_12)); }
	inline bool get_is3DLayoutVideo_12() const { return ___is3DLayoutVideo_12; }
	inline bool* get_address_of_is3DLayoutVideo_12() { return &___is3DLayoutVideo_12; }
	inline void set_is3DLayoutVideo_12(bool value)
	{
		___is3DLayoutVideo_12 = value;
	}

	inline static int32_t get_offset_of_layout3d_13() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___layout3d_13)); }
	inline int32_t get_layout3d_13() const { return ___layout3d_13; }
	inline int32_t* get_address_of_layout3d_13() { return &___layout3d_13; }
	inline void set_layout3d_13(int32_t value)
	{
		___layout3d_13 = value;
	}

	inline static int32_t get_offset_of_videoControllerCanvas_14() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoControllerCanvas_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoControllerCanvas_14() const { return ___videoControllerCanvas_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoControllerCanvas_14() { return &___videoControllerCanvas_14; }
	inline void set_videoControllerCanvas_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoControllerCanvas_14 = value;
		Il2CppCodeGenWriteBarrier((&___videoControllerCanvas_14), value);
	}

	inline static int32_t get_offset_of_mainCamera_15() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___mainCamera_15)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_15() const { return ___mainCamera_15; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_15() { return &___mainCamera_15; }
	inline void set_mainCamera_15(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_15), value);
	}

	inline static int32_t get_offset_of_videoQuality_16() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoQuality_16)); }
	inline int32_t get_videoQuality_16() const { return ___videoQuality_16; }
	inline int32_t* get_address_of_videoQuality_16() { return &___videoQuality_16; }
	inline void set_videoQuality_16(int32_t value)
	{
		___videoQuality_16 = value;
	}

	inline static int32_t get_offset_of_loadingContent_17() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___loadingContent_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_loadingContent_17() const { return ___loadingContent_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_loadingContent_17() { return &___loadingContent_17; }
	inline void set_loadingContent_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___loadingContent_17 = value;
		Il2CppCodeGenWriteBarrier((&___loadingContent_17), value);
	}

	inline static int32_t get_offset_of_OnYoutubeUrlAreReady_18() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___OnYoutubeUrlAreReady_18)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnYoutubeUrlAreReady_18() const { return ___OnYoutubeUrlAreReady_18; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnYoutubeUrlAreReady_18() { return &___OnYoutubeUrlAreReady_18; }
	inline void set_OnYoutubeUrlAreReady_18(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnYoutubeUrlAreReady_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnYoutubeUrlAreReady_18), value);
	}

	inline static int32_t get_offset_of_OnVideoReadyToStart_19() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___OnVideoReadyToStart_19)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnVideoReadyToStart_19() const { return ___OnVideoReadyToStart_19; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnVideoReadyToStart_19() { return &___OnVideoReadyToStart_19; }
	inline void set_OnVideoReadyToStart_19(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnVideoReadyToStart_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnVideoReadyToStart_19), value);
	}

	inline static int32_t get_offset_of_OnVideoStarted_20() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___OnVideoStarted_20)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnVideoStarted_20() const { return ___OnVideoStarted_20; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnVideoStarted_20() { return &___OnVideoStarted_20; }
	inline void set_OnVideoStarted_20(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnVideoStarted_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnVideoStarted_20), value);
	}

	inline static int32_t get_offset_of_OnVideoPaused_21() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___OnVideoPaused_21)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnVideoPaused_21() const { return ___OnVideoPaused_21; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnVideoPaused_21() { return &___OnVideoPaused_21; }
	inline void set_OnVideoPaused_21(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnVideoPaused_21 = value;
		Il2CppCodeGenWriteBarrier((&___OnVideoPaused_21), value);
	}

	inline static int32_t get_offset_of_OnVideoFinished_22() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___OnVideoFinished_22)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnVideoFinished_22() const { return ___OnVideoFinished_22; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnVideoFinished_22() { return &___OnVideoFinished_22; }
	inline void set_OnVideoFinished_22(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnVideoFinished_22 = value;
		Il2CppCodeGenWriteBarrier((&___OnVideoFinished_22), value);
	}

	inline static int32_t get_offset_of_objectsToRenderTheVideoImage_23() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___objectsToRenderTheVideoImage_23)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_objectsToRenderTheVideoImage_23() const { return ___objectsToRenderTheVideoImage_23; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_objectsToRenderTheVideoImage_23() { return &___objectsToRenderTheVideoImage_23; }
	inline void set_objectsToRenderTheVideoImage_23(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___objectsToRenderTheVideoImage_23 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToRenderTheVideoImage_23), value);
	}

	inline static int32_t get_offset_of_videoPlayer_24() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoPlayer_24)); }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * get_videoPlayer_24() const { return ___videoPlayer_24; }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 ** get_address_of_videoPlayer_24() { return &___videoPlayer_24; }
	inline void set_videoPlayer_24(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * value)
	{
		___videoPlayer_24 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_24), value);
	}

	inline static int32_t get_offset_of_audioPlayer_25() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___audioPlayer_25)); }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * get_audioPlayer_25() const { return ___audioPlayer_25; }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 ** get_address_of_audioPlayer_25() { return &___audioPlayer_25; }
	inline void set_audioPlayer_25(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * value)
	{
		___audioPlayer_25 = value;
		Il2CppCodeGenWriteBarrier((&___audioPlayer_25), value);
	}

	inline static int32_t get_offset_of_debug_26() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___debug_26)); }
	inline bool get_debug_26() const { return ___debug_26; }
	inline bool* get_address_of_debug_26() { return &___debug_26; }
	inline void set_debug_26(bool value)
	{
		___debug_26 = value;
	}

	inline static int32_t get_offset_of_videoUrl_27() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoUrl_27)); }
	inline String_t* get_videoUrl_27() const { return ___videoUrl_27; }
	inline String_t** get_address_of_videoUrl_27() { return &___videoUrl_27; }
	inline void set_videoUrl_27(String_t* value)
	{
		___videoUrl_27 = value;
		Il2CppCodeGenWriteBarrier((&___videoUrl_27), value);
	}

	inline static int32_t get_offset_of_audioUrl_28() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___audioUrl_28)); }
	inline String_t* get_audioUrl_28() const { return ___audioUrl_28; }
	inline String_t** get_address_of_audioUrl_28() { return &___audioUrl_28; }
	inline void set_audioUrl_28(String_t* value)
	{
		___audioUrl_28 = value;
		Il2CppCodeGenWriteBarrier((&___audioUrl_28), value);
	}

	inline static int32_t get_offset_of_ForceGetWebServer_29() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___ForceGetWebServer_29)); }
	inline bool get_ForceGetWebServer_29() const { return ___ForceGetWebServer_29; }
	inline bool* get_address_of_ForceGetWebServer_29() { return &___ForceGetWebServer_29; }
	inline void set_ForceGetWebServer_29(bool value)
	{
		___ForceGetWebServer_29 = value;
	}

	inline static int32_t get_offset_of_showPlayerControls_30() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___showPlayerControls_30)); }
	inline bool get_showPlayerControls_30() const { return ___showPlayerControls_30; }
	inline bool* get_address_of_showPlayerControls_30() { return &___showPlayerControls_30; }
	inline void set_showPlayerControls_30(bool value)
	{
		___showPlayerControls_30 = value;
	}

	inline static int32_t get_offset_of_maxRequestTime_31() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___maxRequestTime_31)); }
	inline int32_t get_maxRequestTime_31() const { return ___maxRequestTime_31; }
	inline int32_t* get_address_of_maxRequestTime_31() { return &___maxRequestTime_31; }
	inline void set_maxRequestTime_31(int32_t value)
	{
		___maxRequestTime_31 = value;
	}

	inline static int32_t get_offset_of_currentRequestTime_32() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___currentRequestTime_32)); }
	inline float get_currentRequestTime_32() const { return ___currentRequestTime_32; }
	inline float* get_address_of_currentRequestTime_32() { return &___currentRequestTime_32; }
	inline void set_currentRequestTime_32(float value)
	{
		___currentRequestTime_32 = value;
	}

	inline static int32_t get_offset_of_retryTimeUntilToRequestFromServer_33() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___retryTimeUntilToRequestFromServer_33)); }
	inline int32_t get_retryTimeUntilToRequestFromServer_33() const { return ___retryTimeUntilToRequestFromServer_33; }
	inline int32_t* get_address_of_retryTimeUntilToRequestFromServer_33() { return &___retryTimeUntilToRequestFromServer_33; }
	inline void set_retryTimeUntilToRequestFromServer_33(int32_t value)
	{
		___retryTimeUntilToRequestFromServer_33 = value;
	}

	inline static int32_t get_offset_of_currentRetryTime_34() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___currentRetryTime_34)); }
	inline int32_t get_currentRetryTime_34() const { return ___currentRetryTime_34; }
	inline int32_t* get_address_of_currentRetryTime_34() { return &___currentRetryTime_34; }
	inline void set_currentRetryTime_34(int32_t value)
	{
		___currentRetryTime_34 = value;
	}

	inline static int32_t get_offset_of_gettingYoutubeURL_35() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___gettingYoutubeURL_35)); }
	inline bool get_gettingYoutubeURL_35() const { return ___gettingYoutubeURL_35; }
	inline bool* get_address_of_gettingYoutubeURL_35() { return &___gettingYoutubeURL_35; }
	inline void set_gettingYoutubeURL_35(bool value)
	{
		___gettingYoutubeURL_35 = value;
	}

	inline static int32_t get_offset_of_videoAreReadyToPlay_36() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoAreReadyToPlay_36)); }
	inline bool get_videoAreReadyToPlay_36() const { return ___videoAreReadyToPlay_36; }
	inline bool* get_address_of_videoAreReadyToPlay_36() { return &___videoAreReadyToPlay_36; }
	inline void set_videoAreReadyToPlay_36(bool value)
	{
		___videoAreReadyToPlay_36 = value;
	}

	inline static int32_t get_offset_of_lastPlayTime_37() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lastPlayTime_37)); }
	inline float get_lastPlayTime_37() const { return ___lastPlayTime_37; }
	inline float* get_address_of_lastPlayTime_37() { return &___lastPlayTime_37; }
	inline void set_lastPlayTime_37(float value)
	{
		___lastPlayTime_37 = value;
	}

	inline static int32_t get_offset_of_audioDecryptDone_38() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___audioDecryptDone_38)); }
	inline bool get_audioDecryptDone_38() const { return ___audioDecryptDone_38; }
	inline bool* get_address_of_audioDecryptDone_38() { return &___audioDecryptDone_38; }
	inline void set_audioDecryptDone_38(bool value)
	{
		___audioDecryptDone_38 = value;
	}

	inline static int32_t get_offset_of_videoDecryptDone_39() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoDecryptDone_39)); }
	inline bool get_videoDecryptDone_39() const { return ___videoDecryptDone_39; }
	inline bool* get_address_of_videoDecryptDone_39() { return &___videoDecryptDone_39; }
	inline void set_videoDecryptDone_39(bool value)
	{
		___videoDecryptDone_39 = value;
	}

	inline static int32_t get_offset_of_videoPrepared_40() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoPrepared_40)); }
	inline bool get_videoPrepared_40() const { return ___videoPrepared_40; }
	inline bool* get_address_of_videoPrepared_40() { return &___videoPrepared_40; }
	inline void set_videoPrepared_40(bool value)
	{
		___videoPrepared_40 = value;
	}

	inline static int32_t get_offset_of_audioPrepared_41() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___audioPrepared_41)); }
	inline bool get_audioPrepared_41() const { return ___audioPrepared_41; }
	inline bool* get_address_of_audioPrepared_41() { return &___audioPrepared_41; }
	inline void set_audioPrepared_41(bool value)
	{
		___audioPrepared_41 = value;
	}

	inline static int32_t get_offset_of_isRetry_42() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___isRetry_42)); }
	inline bool get_isRetry_42() const { return ___isRetry_42; }
	inline bool* get_address_of_isRetry_42() { return &___isRetry_42; }
	inline void set_isRetry_42(bool value)
	{
		___isRetry_42 = value;
	}

	inline static int32_t get_offset_of_lastTryVideoId_43() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lastTryVideoId_43)); }
	inline String_t* get_lastTryVideoId_43() const { return ___lastTryVideoId_43; }
	inline String_t** get_address_of_lastTryVideoId_43() { return &___lastTryVideoId_43; }
	inline void set_lastTryVideoId_43(String_t* value)
	{
		___lastTryVideoId_43 = value;
		Il2CppCodeGenWriteBarrier((&___lastTryVideoId_43), value);
	}

	inline static int32_t get_offset_of_lastStartedTime_44() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lastStartedTime_44)); }
	inline float get_lastStartedTime_44() const { return ___lastStartedTime_44; }
	inline float* get_address_of_lastStartedTime_44() { return &___lastStartedTime_44; }
	inline void set_lastStartedTime_44(float value)
	{
		___lastStartedTime_44 = value;
	}

	inline static int32_t get_offset_of_youtubeUrlReady_45() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___youtubeUrlReady_45)); }
	inline bool get_youtubeUrlReady_45() const { return ___youtubeUrlReady_45; }
	inline bool* get_address_of_youtubeUrlReady_45() { return &___youtubeUrlReady_45; }
	inline void set_youtubeUrlReady_45(bool value)
	{
		___youtubeUrlReady_45 = value;
	}

	inline static int32_t get_offset_of_newRequestResults_46() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___newRequestResults_46)); }
	inline YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 * get_newRequestResults_46() const { return ___newRequestResults_46; }
	inline YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 ** get_address_of_newRequestResults_46() { return &___newRequestResults_46; }
	inline void set_newRequestResults_46(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 * value)
	{
		___newRequestResults_46 = value;
		Il2CppCodeGenWriteBarrier((&___newRequestResults_46), value);
	}

	inline static int32_t get_offset_of_fullscreenModeEnabled_50() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___fullscreenModeEnabled_50)); }
	inline bool get_fullscreenModeEnabled_50() const { return ___fullscreenModeEnabled_50; }
	inline bool* get_address_of_fullscreenModeEnabled_50() { return &___fullscreenModeEnabled_50; }
	inline void set_fullscreenModeEnabled_50(bool value)
	{
		___fullscreenModeEnabled_50 = value;
	}

	inline static int32_t get_offset_of_lastFrame_51() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lastFrame_51)); }
	inline int64_t get_lastFrame_51() const { return ___lastFrame_51; }
	inline int64_t* get_address_of_lastFrame_51() { return &___lastFrame_51; }
	inline void set_lastFrame_51(int64_t value)
	{
		___lastFrame_51 = value;
	}

	inline static int32_t get_offset_of_videoEnded_52() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoEnded_52)); }
	inline bool get_videoEnded_52() const { return ___videoEnded_52; }
	inline bool* get_address_of_videoEnded_52() { return &___videoEnded_52; }
	inline void set_videoEnded_52(bool value)
	{
		___videoEnded_52 = value;
	}

	inline static int32_t get_offset_of_noAudioAtacched_53() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___noAudioAtacched_53)); }
	inline bool get_noAudioAtacched_53() const { return ___noAudioAtacched_53; }
	inline bool* get_address_of_noAudioAtacched_53() { return &___noAudioAtacched_53; }
	inline void set_noAudioAtacched_53(bool value)
	{
		___noAudioAtacched_53 = value;
	}

	inline static int32_t get_offset_of_videoTitle_54() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoTitle_54)); }
	inline String_t* get_videoTitle_54() const { return ___videoTitle_54; }
	inline String_t** get_address_of_videoTitle_54() { return &___videoTitle_54; }
	inline void set_videoTitle_54(String_t* value)
	{
		___videoTitle_54 = value;
		Il2CppCodeGenWriteBarrier((&___videoTitle_54), value);
	}

	inline static int32_t get_offset_of_startedFromTime_55() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___startedFromTime_55)); }
	inline bool get_startedFromTime_55() const { return ___startedFromTime_55; }
	inline bool* get_address_of_startedFromTime_55() { return &___startedFromTime_55; }
	inline void set_startedFromTime_55(bool value)
	{
		___startedFromTime_55 = value;
	}

	inline static int32_t get_offset_of_videoStarted_56() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoStarted_56)); }
	inline bool get_videoStarted_56() const { return ___videoStarted_56; }
	inline bool* get_address_of_videoStarted_56() { return &___videoStarted_56; }
	inline void set_videoStarted_56(bool value)
	{
		___videoStarted_56 = value;
	}

	inline static int32_t get_offset_of_lastErrorTime_57() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lastErrorTime_57)); }
	inline float get_lastErrorTime_57() const { return ___lastErrorTime_57; }
	inline float* get_address_of_lastErrorTime_57() { return &___lastErrorTime_57; }
	inline void set_lastErrorTime_57(float value)
	{
		___lastErrorTime_57 = value;
	}

	inline static int32_t get_offset_of_pauseCalled_58() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___pauseCalled_58)); }
	inline bool get_pauseCalled_58() const { return ___pauseCalled_58; }
	inline bool* get_address_of_pauseCalled_58() { return &___pauseCalled_58; }
	inline void set_pauseCalled_58(bool value)
	{
		___pauseCalled_58 = value;
	}

	inline static int32_t get_offset_of_autoHideControlsTime_59() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___autoHideControlsTime_59)); }
	inline int32_t get_autoHideControlsTime_59() const { return ___autoHideControlsTime_59; }
	inline int32_t* get_address_of_autoHideControlsTime_59() { return &___autoHideControlsTime_59; }
	inline void set_autoHideControlsTime_59(int32_t value)
	{
		___autoHideControlsTime_59 = value;
	}

	inline static int32_t get_offset_of_mainControllerUi_60() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___mainControllerUi_60)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainControllerUi_60() const { return ___mainControllerUi_60; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainControllerUi_60() { return &___mainControllerUi_60; }
	inline void set_mainControllerUi_60(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainControllerUi_60 = value;
		Il2CppCodeGenWriteBarrier((&___mainControllerUi_60), value);
	}

	inline static int32_t get_offset_of_progress_61() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___progress_61)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_progress_61() const { return ___progress_61; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_progress_61() { return &___progress_61; }
	inline void set_progress_61(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___progress_61 = value;
		Il2CppCodeGenWriteBarrier((&___progress_61), value);
	}

	inline static int32_t get_offset_of_volumeSlider_62() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___volumeSlider_62)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_volumeSlider_62() const { return ___volumeSlider_62; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_volumeSlider_62() { return &___volumeSlider_62; }
	inline void set_volumeSlider_62(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___volumeSlider_62 = value;
		Il2CppCodeGenWriteBarrier((&___volumeSlider_62), value);
	}

	inline static int32_t get_offset_of_playbackSpeed_63() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___playbackSpeed_63)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_playbackSpeed_63() const { return ___playbackSpeed_63; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_playbackSpeed_63() { return &___playbackSpeed_63; }
	inline void set_playbackSpeed_63(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___playbackSpeed_63 = value;
		Il2CppCodeGenWriteBarrier((&___playbackSpeed_63), value);
	}

	inline static int32_t get_offset_of_currentTimeString_64() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___currentTimeString_64)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_currentTimeString_64() const { return ___currentTimeString_64; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_currentTimeString_64() { return &___currentTimeString_64; }
	inline void set_currentTimeString_64(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___currentTimeString_64 = value;
		Il2CppCodeGenWriteBarrier((&___currentTimeString_64), value);
	}

	inline static int32_t get_offset_of_totalTimeString_65() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___totalTimeString_65)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_totalTimeString_65() const { return ___totalTimeString_65; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_totalTimeString_65() { return &___totalTimeString_65; }
	inline void set_totalTimeString_65(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___totalTimeString_65 = value;
		Il2CppCodeGenWriteBarrier((&___totalTimeString_65), value);
	}

	inline static int32_t get_offset_of_totalVideoDuration_66() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___totalVideoDuration_66)); }
	inline float get_totalVideoDuration_66() const { return ___totalVideoDuration_66; }
	inline float* get_address_of_totalVideoDuration_66() { return &___totalVideoDuration_66; }
	inline void set_totalVideoDuration_66(float value)
	{
		___totalVideoDuration_66 = value;
	}

	inline static int32_t get_offset_of_currentVideoDuration_67() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___currentVideoDuration_67)); }
	inline float get_currentVideoDuration_67() const { return ___currentVideoDuration_67; }
	inline float* get_address_of_currentVideoDuration_67() { return &___currentVideoDuration_67; }
	inline void set_currentVideoDuration_67(float value)
	{
		___currentVideoDuration_67 = value;
	}

	inline static int32_t get_offset_of_videoSeekDone_68() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoSeekDone_68)); }
	inline bool get_videoSeekDone_68() const { return ___videoSeekDone_68; }
	inline bool* get_address_of_videoSeekDone_68() { return &___videoSeekDone_68; }
	inline void set_videoSeekDone_68(bool value)
	{
		___videoSeekDone_68 = value;
	}

	inline static int32_t get_offset_of_videoAudioSeekDone_69() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___videoAudioSeekDone_69)); }
	inline bool get_videoAudioSeekDone_69() const { return ___videoAudioSeekDone_69; }
	inline bool* get_address_of_videoAudioSeekDone_69() { return &___videoAudioSeekDone_69; }
	inline void set_videoAudioSeekDone_69(bool value)
	{
		___videoAudioSeekDone_69 = value;
	}

	inline static int32_t get_offset_of_lowRes_70() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lowRes_70)); }
	inline bool get_lowRes_70() const { return ___lowRes_70; }
	inline bool* get_address_of_lowRes_70() { return &___lowRes_70; }
	inline void set_lowRes_70(bool value)
	{
		___lowRes_70 = value;
	}

	inline static int32_t get_offset_of_hideScreenTime_71() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___hideScreenTime_71)); }
	inline float get_hideScreenTime_71() const { return ___hideScreenTime_71; }
	inline float* get_address_of_hideScreenTime_71() { return &___hideScreenTime_71; }
	inline void set_hideScreenTime_71(float value)
	{
		___hideScreenTime_71 = value;
	}

	inline static int32_t get_offset_of_audioDuration_72() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___audioDuration_72)); }
	inline float get_audioDuration_72() const { return ___audioDuration_72; }
	inline float* get_address_of_audioDuration_72() { return &___audioDuration_72; }
	inline void set_audioDuration_72(float value)
	{
		___audioDuration_72 = value;
	}

	inline static int32_t get_offset_of_showingPlaybackSpeed_73() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___showingPlaybackSpeed_73)); }
	inline bool get_showingPlaybackSpeed_73() const { return ___showingPlaybackSpeed_73; }
	inline bool* get_address_of_showingPlaybackSpeed_73() { return &___showingPlaybackSpeed_73; }
	inline void set_showingPlaybackSpeed_73(bool value)
	{
		___showingPlaybackSpeed_73 = value;
	}

	inline static int32_t get_offset_of_showingVolume_74() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___showingVolume_74)); }
	inline bool get_showingVolume_74() const { return ___showingVolume_74; }
	inline bool* get_address_of_showingVolume_74() { return &___showingVolume_74; }
	inline void set_showingVolume_74(bool value)
	{
		___showingVolume_74 = value;
	}

	inline static int32_t get_offset_of_seekUsingLowQuality_75() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___seekUsingLowQuality_75)); }
	inline bool get_seekUsingLowQuality_75() const { return ___seekUsingLowQuality_75; }
	inline bool* get_address_of_seekUsingLowQuality_75() { return &___seekUsingLowQuality_75; }
	inline void set_seekUsingLowQuality_75(bool value)
	{
		___seekUsingLowQuality_75 = value;
	}

	inline static int32_t get_offset_of_lsigForVideo_76() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lsigForVideo_76)); }
	inline String_t* get_lsigForVideo_76() const { return ___lsigForVideo_76; }
	inline String_t** get_address_of_lsigForVideo_76() { return &___lsigForVideo_76; }
	inline void set_lsigForVideo_76(String_t* value)
	{
		___lsigForVideo_76 = value;
		Il2CppCodeGenWriteBarrier((&___lsigForVideo_76), value);
	}

	inline static int32_t get_offset_of_lsigForAudio_77() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___lsigForAudio_77)); }
	inline String_t* get_lsigForAudio_77() const { return ___lsigForAudio_77; }
	inline String_t** get_address_of_lsigForAudio_77() { return &___lsigForAudio_77; }
	inline void set_lsigForAudio_77(String_t* value)
	{
		___lsigForAudio_77 = value;
		Il2CppCodeGenWriteBarrier((&___lsigForAudio_77), value);
	}

	inline static int32_t get_offset_of_thread1_78() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___thread1_78)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_thread1_78() const { return ___thread1_78; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_thread1_78() { return &___thread1_78; }
	inline void set_thread1_78(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___thread1_78 = value;
		Il2CppCodeGenWriteBarrier((&___thread1_78), value);
	}

	inline static int32_t get_offset_of_webGlResults_79() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___webGlResults_79)); }
	inline YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 * get_webGlResults_79() const { return ___webGlResults_79; }
	inline YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 ** get_address_of_webGlResults_79() { return &___webGlResults_79; }
	inline void set_webGlResults_79(YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72 * value)
	{
		___webGlResults_79 = value;
		Il2CppCodeGenWriteBarrier((&___webGlResults_79), value);
	}

	inline static int32_t get_offset_of_startedPlayingWebgl_80() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___startedPlayingWebgl_80)); }
	inline bool get_startedPlayingWebgl_80() const { return ___startedPlayingWebgl_80; }
	inline bool* get_address_of_startedPlayingWebgl_80() { return &___startedPlayingWebgl_80; }
	inline void set_startedPlayingWebgl_80(bool value)
	{
		___startedPlayingWebgl_80 = value;
	}

	inline static int32_t get_offset_of_logTest_81() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___logTest_81)); }
	inline String_t* get_logTest_81() const { return ___logTest_81; }
	inline String_t** get_address_of_logTest_81() { return &___logTest_81; }
	inline void set_logTest_81(String_t* value)
	{
		___logTest_81 = value;
		Il2CppCodeGenWriteBarrier((&___logTest_81), value);
	}

	inline static int32_t get_offset_of_isSyncing_82() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___isSyncing_82)); }
	inline bool get_isSyncing_82() const { return ___isSyncing_82; }
	inline bool* get_address_of_isSyncing_82() { return &___isSyncing_82; }
	inline void set_isSyncing_82(bool value)
	{
		___isSyncing_82 = value;
	}

	inline static int32_t get_offset_of_showThumbnailBeforeVideoLoad_83() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___showThumbnailBeforeVideoLoad_83)); }
	inline bool get_showThumbnailBeforeVideoLoad_83() const { return ___showThumbnailBeforeVideoLoad_83; }
	inline bool* get_address_of_showThumbnailBeforeVideoLoad_83() { return &___showThumbnailBeforeVideoLoad_83; }
	inline void set_showThumbnailBeforeVideoLoad_83(bool value)
	{
		___showThumbnailBeforeVideoLoad_83 = value;
	}

	inline static int32_t get_offset_of_thumbnailVideoID_84() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___thumbnailVideoID_84)); }
	inline String_t* get_thumbnailVideoID_84() const { return ___thumbnailVideoID_84; }
	inline String_t** get_address_of_thumbnailVideoID_84() { return &___thumbnailVideoID_84; }
	inline void set_thumbnailVideoID_84(String_t* value)
	{
		___thumbnailVideoID_84 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailVideoID_84), value);
	}

	inline static int32_t get_offset_of_encryptedSignatureVideo_87() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___encryptedSignatureVideo_87)); }
	inline String_t* get_encryptedSignatureVideo_87() const { return ___encryptedSignatureVideo_87; }
	inline String_t** get_address_of_encryptedSignatureVideo_87() { return &___encryptedSignatureVideo_87; }
	inline void set_encryptedSignatureVideo_87(String_t* value)
	{
		___encryptedSignatureVideo_87 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedSignatureVideo_87), value);
	}

	inline static int32_t get_offset_of_encryptedSignatureAudio_88() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___encryptedSignatureAudio_88)); }
	inline String_t* get_encryptedSignatureAudio_88() const { return ___encryptedSignatureAudio_88; }
	inline String_t** get_address_of_encryptedSignatureAudio_88() { return &___encryptedSignatureAudio_88; }
	inline void set_encryptedSignatureAudio_88(String_t* value)
	{
		___encryptedSignatureAudio_88 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedSignatureAudio_88), value);
	}

	inline static int32_t get_offset_of_masterURLForVideo_89() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___masterURLForVideo_89)); }
	inline String_t* get_masterURLForVideo_89() const { return ___masterURLForVideo_89; }
	inline String_t** get_address_of_masterURLForVideo_89() { return &___masterURLForVideo_89; }
	inline void set_masterURLForVideo_89(String_t* value)
	{
		___masterURLForVideo_89 = value;
		Il2CppCodeGenWriteBarrier((&___masterURLForVideo_89), value);
	}

	inline static int32_t get_offset_of_masterURLForAudio_90() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___masterURLForAudio_90)); }
	inline String_t* get_masterURLForAudio_90() const { return ___masterURLForAudio_90; }
	inline String_t** get_address_of_masterURLForAudio_90() { return &___masterURLForAudio_90; }
	inline void set_masterURLForAudio_90(String_t* value)
	{
		___masterURLForAudio_90 = value;
		Il2CppCodeGenWriteBarrier((&___masterURLForAudio_90), value);
	}

	inline static int32_t get_offset_of_patternNames_91() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___patternNames_91)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_patternNames_91() const { return ___patternNames_91; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_patternNames_91() { return &___patternNames_91; }
	inline void set_patternNames_91(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___patternNames_91 = value;
		Il2CppCodeGenWriteBarrier((&___patternNames_91), value);
	}

	inline static int32_t get_offset_of_patternIndex_92() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___patternIndex_92)); }
	inline int32_t get_patternIndex_92() const { return ___patternIndex_92; }
	inline int32_t* get_address_of_patternIndex_92() { return &___patternIndex_92; }
	inline void set_patternIndex_92(int32_t value)
	{
		___patternIndex_92 = value;
	}

	inline static int32_t get_offset_of_decryptedUrlForVideo_93() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___decryptedUrlForVideo_93)); }
	inline bool get_decryptedUrlForVideo_93() const { return ___decryptedUrlForVideo_93; }
	inline bool* get_address_of_decryptedUrlForVideo_93() { return &___decryptedUrlForVideo_93; }
	inline void set_decryptedUrlForVideo_93(bool value)
	{
		___decryptedUrlForVideo_93 = value;
	}

	inline static int32_t get_offset_of_decryptedUrlForAudio_94() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___decryptedUrlForAudio_94)); }
	inline bool get_decryptedUrlForAudio_94() const { return ___decryptedUrlForAudio_94; }
	inline bool* get_address_of_decryptedUrlForAudio_94() { return &___decryptedUrlForAudio_94; }
	inline void set_decryptedUrlForAudio_94(bool value)
	{
		___decryptedUrlForAudio_94 = value;
	}

	inline static int32_t get_offset_of_decryptedVideoUrlResult_95() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___decryptedVideoUrlResult_95)); }
	inline String_t* get_decryptedVideoUrlResult_95() const { return ___decryptedVideoUrlResult_95; }
	inline String_t** get_address_of_decryptedVideoUrlResult_95() { return &___decryptedVideoUrlResult_95; }
	inline void set_decryptedVideoUrlResult_95(String_t* value)
	{
		___decryptedVideoUrlResult_95 = value;
		Il2CppCodeGenWriteBarrier((&___decryptedVideoUrlResult_95), value);
	}

	inline static int32_t get_offset_of_decryptedAudioUrlResult_96() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___decryptedAudioUrlResult_96)); }
	inline String_t* get_decryptedAudioUrlResult_96() const { return ___decryptedAudioUrlResult_96; }
	inline String_t** get_address_of_decryptedAudioUrlResult_96() { return &___decryptedAudioUrlResult_96; }
	inline void set_decryptedAudioUrlResult_96(String_t* value)
	{
		___decryptedAudioUrlResult_96 = value;
		Il2CppCodeGenWriteBarrier((&___decryptedAudioUrlResult_96), value);
	}

	inline static int32_t get_offset_of_youtubeVideoInfos_97() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___youtubeVideoInfos_97)); }
	inline List_1_t7334C42E62574D2DD9BDF1730A044AA1CEECEC26 * get_youtubeVideoInfos_97() const { return ___youtubeVideoInfos_97; }
	inline List_1_t7334C42E62574D2DD9BDF1730A044AA1CEECEC26 ** get_address_of_youtubeVideoInfos_97() { return &___youtubeVideoInfos_97; }
	inline void set_youtubeVideoInfos_97(List_1_t7334C42E62574D2DD9BDF1730A044AA1CEECEC26 * value)
	{
		___youtubeVideoInfos_97 = value;
		Il2CppCodeGenWriteBarrier((&___youtubeVideoInfos_97), value);
	}

	inline static int32_t get_offset_of_htmlVersion_98() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___htmlVersion_98)); }
	inline String_t* get_htmlVersion_98() const { return ___htmlVersion_98; }
	inline String_t** get_address_of_htmlVersion_98() { return &___htmlVersion_98; }
	inline void set_htmlVersion_98(String_t* value)
	{
		___htmlVersion_98 = value;
		Il2CppCodeGenWriteBarrier((&___htmlVersion_98), value);
	}

	inline static int32_t get_offset_of_EncryptUrlForVideo_100() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___EncryptUrlForVideo_100)); }
	inline String_t* get_EncryptUrlForVideo_100() const { return ___EncryptUrlForVideo_100; }
	inline String_t** get_address_of_EncryptUrlForVideo_100() { return &___EncryptUrlForVideo_100; }
	inline void set_EncryptUrlForVideo_100(String_t* value)
	{
		___EncryptUrlForVideo_100 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptUrlForVideo_100), value);
	}

	inline static int32_t get_offset_of_EncryptUrlForAudio_101() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___EncryptUrlForAudio_101)); }
	inline String_t* get_EncryptUrlForAudio_101() const { return ___EncryptUrlForAudio_101; }
	inline String_t** get_address_of_EncryptUrlForAudio_101() { return &___EncryptUrlForAudio_101; }
	inline void set_EncryptUrlForAudio_101(String_t* value)
	{
		___EncryptUrlForAudio_101 = value;
		Il2CppCodeGenWriteBarrier((&___EncryptUrlForAudio_101), value);
	}

	inline static int32_t get_offset_of_downloadYoutubeUrlResponse_102() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___downloadYoutubeUrlResponse_102)); }
	inline DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7 * get_downloadYoutubeUrlResponse_102() const { return ___downloadYoutubeUrlResponse_102; }
	inline DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7 ** get_address_of_downloadYoutubeUrlResponse_102() { return &___downloadYoutubeUrlResponse_102; }
	inline void set_downloadYoutubeUrlResponse_102(DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7 * value)
	{
		___downloadYoutubeUrlResponse_102 = value;
		Il2CppCodeGenWriteBarrier((&___downloadYoutubeUrlResponse_102), value);
	}

	inline static int32_t get_offset_of_waitAudioSeek_103() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___waitAudioSeek_103)); }
	inline bool get_waitAudioSeek_103() const { return ___waitAudioSeek_103; }
	inline bool* get_address_of_waitAudioSeek_103() { return &___waitAudioSeek_103; }
	inline void set_waitAudioSeek_103(bool value)
	{
		___waitAudioSeek_103 = value;
	}

	inline static int32_t get_offset_of_notSeeking_104() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___notSeeking_104)); }
	inline bool get_notSeeking_104() const { return ___notSeeking_104; }
	inline bool* get_address_of_notSeeking_104() { return &___notSeeking_104; }
	inline void set_notSeeking_104(bool value)
	{
		___notSeeking_104 = value;
	}

	inline static int32_t get_offset_of_ignoreDrop_105() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___ignoreDrop_105)); }
	inline bool get_ignoreDrop_105() const { return ___ignoreDrop_105; }
	inline bool* get_address_of_ignoreDrop_105() { return &___ignoreDrop_105; }
	inline void set_ignoreDrop_105(bool value)
	{
		___ignoreDrop_105 = value;
	}

	inline static int32_t get_offset_of_dropAlreadyCalled_106() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___dropAlreadyCalled_106)); }
	inline bool get_dropAlreadyCalled_106() const { return ___dropAlreadyCalled_106; }
	inline bool* get_address_of_dropAlreadyCalled_106() { return &___dropAlreadyCalled_106; }
	inline void set_dropAlreadyCalled_106(bool value)
	{
		___dropAlreadyCalled_106 = value;
	}

	inline static int32_t get_offset_of_checkIfSync_107() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9, ___checkIfSync_107)); }
	inline bool get_checkIfSync_107() const { return ___checkIfSync_107; }
	inline bool* get_address_of_checkIfSync_107() { return &___checkIfSync_107; }
	inline void set_checkIfSync_107(bool value)
	{
		___checkIfSync_107 = value;
	}
};

struct YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_StaticFields
{
public:
	// System.String YoutubePlayer::SignatureQuery
	String_t* ___SignatureQuery_86;
	// System.String YoutubePlayer::sp
	String_t* ___sp_99;

public:
	inline static int32_t get_offset_of_SignatureQuery_86() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_StaticFields, ___SignatureQuery_86)); }
	inline String_t* get_SignatureQuery_86() const { return ___SignatureQuery_86; }
	inline String_t** get_address_of_SignatureQuery_86() { return &___SignatureQuery_86; }
	inline void set_SignatureQuery_86(String_t* value)
	{
		___SignatureQuery_86 = value;
		Il2CppCodeGenWriteBarrier((&___SignatureQuery_86), value);
	}

	inline static int32_t get_offset_of_sp_99() { return static_cast<int32_t>(offsetof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_StaticFields, ___sp_99)); }
	inline String_t* get_sp_99() const { return ___sp_99; }
	inline String_t** get_address_of_sp_99() { return &___sp_99; }
	inline void set_sp_99(String_t* value)
	{
		___sp_99 = value;
		Il2CppCodeGenWriteBarrier((&___sp_99), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEPLAYER_TD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_H
#ifndef YOUTUBEPLAYERLIVESTREAM_T01CC6BD2B4B6081ADFD54AE62A1854935E833781_H
#define YOUTUBEPLAYERLIVESTREAM_T01CC6BD2B4B6081ADFD54AE62A1854935E833781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubePlayerLivestream
struct  YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String YoutubePlayerLivestream::_livestreamUrl
	String_t* ____livestreamUrl_4;
	// YoutubePlayerLivestream_DownloadUrlResponse YoutubePlayerLivestream::downloadYoutubeUrlResponse
	DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0 * ___downloadYoutubeUrlResponse_5;

public:
	inline static int32_t get_offset_of__livestreamUrl_4() { return static_cast<int32_t>(offsetof(YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781, ____livestreamUrl_4)); }
	inline String_t* get__livestreamUrl_4() const { return ____livestreamUrl_4; }
	inline String_t** get_address_of__livestreamUrl_4() { return &____livestreamUrl_4; }
	inline void set__livestreamUrl_4(String_t* value)
	{
		____livestreamUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&____livestreamUrl_4), value);
	}

	inline static int32_t get_offset_of_downloadYoutubeUrlResponse_5() { return static_cast<int32_t>(offsetof(YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781, ___downloadYoutubeUrlResponse_5)); }
	inline DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0 * get_downloadYoutubeUrlResponse_5() const { return ___downloadYoutubeUrlResponse_5; }
	inline DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0 ** get_address_of_downloadYoutubeUrlResponse_5() { return &___downloadYoutubeUrlResponse_5; }
	inline void set_downloadYoutubeUrlResponse_5(DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0 * value)
	{
		___downloadYoutubeUrlResponse_5 = value;
		Il2CppCodeGenWriteBarrier((&___downloadYoutubeUrlResponse_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEPLAYERLIVESTREAM_T01CC6BD2B4B6081ADFD54AE62A1854935E833781_H
#ifndef YOUTUBEVIDEOUI_TB74D00EFC66A561BADB0121E1D454186B559E802_H
#define YOUTUBEVIDEOUI_TB74D00EFC66A561BADB0121E1D454186B559E802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeVideoUi
struct  YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text YoutubeVideoUi::videoName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoName_4;
	// System.String YoutubeVideoUi::videoId
	String_t* ___videoId_5;
	// System.String YoutubeVideoUi::thumbUrl
	String_t* ___thumbUrl_6;
	// UnityEngine.UI.Image YoutubeVideoUi::videoThumb
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___videoThumb_7;
	// UnityEngine.GameObject YoutubeVideoUi::mainUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainUI_8;

public:
	inline static int32_t get_offset_of_videoName_4() { return static_cast<int32_t>(offsetof(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802, ___videoName_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoName_4() const { return ___videoName_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoName_4() { return &___videoName_4; }
	inline void set_videoName_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoName_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoName_4), value);
	}

	inline static int32_t get_offset_of_videoId_5() { return static_cast<int32_t>(offsetof(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802, ___videoId_5)); }
	inline String_t* get_videoId_5() const { return ___videoId_5; }
	inline String_t** get_address_of_videoId_5() { return &___videoId_5; }
	inline void set_videoId_5(String_t* value)
	{
		___videoId_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoId_5), value);
	}

	inline static int32_t get_offset_of_thumbUrl_6() { return static_cast<int32_t>(offsetof(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802, ___thumbUrl_6)); }
	inline String_t* get_thumbUrl_6() const { return ___thumbUrl_6; }
	inline String_t** get_address_of_thumbUrl_6() { return &___thumbUrl_6; }
	inline void set_thumbUrl_6(String_t* value)
	{
		___thumbUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___thumbUrl_6), value);
	}

	inline static int32_t get_offset_of_videoThumb_7() { return static_cast<int32_t>(offsetof(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802, ___videoThumb_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_videoThumb_7() const { return ___videoThumb_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_videoThumb_7() { return &___videoThumb_7; }
	inline void set_videoThumb_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___videoThumb_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoThumb_7), value);
	}

	inline static int32_t get_offset_of_mainUI_8() { return static_cast<int32_t>(offsetof(YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802, ___mainUI_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainUI_8() const { return ___mainUI_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainUI_8() { return &___mainUI_8; }
	inline void set_mainUI_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainUI_8 = value;
		Il2CppCodeGenWriteBarrier((&___mainUI_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOUTUBEVIDEOUI_TB74D00EFC66A561BADB0121E1D454186B559E802_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6400 = { sizeof (TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F), -1, sizeof(TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6400[1] = 
{
	TMP_FontUtilities_tEAE7AD89B734C4BFCC635A65A05ED0BEB690935F_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6401 = { sizeof (TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6401[8] = 
{
	TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6402 = { sizeof (TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6402[35] = 
{
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6403 = { sizeof (TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6403[5] = 
{
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6404 = { sizeof (VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A)+ sizeof (RuntimeObject), sizeof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable6404[4] = 
{
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6405 = { sizeof (TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6405[5] = 
{
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6406 = { sizeof (TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6406[7] = 
{
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6407 = { sizeof (TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6407[4] = 
{
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6408 = { sizeof (TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB ), 0, 0 };
extern const int32_t g_FieldOffsetTable6408[3] = 
{
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6409 = { sizeof (Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3)+ sizeof (RuntimeObject), sizeof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6409[2] = 
{
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6410 = { sizeof (Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6410[2] = 
{
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6411 = { sizeof (WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6411[55] = 
{
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6412 = { sizeof (TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5)+ sizeof (RuntimeObject), sizeof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6412[3] = 
{
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6413 = { sizeof (XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E ), 0, 0 };
extern const int32_t g_FieldOffsetTable6413[5] = 
{
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_tE129B1DB312EBE65B24529E9CD5BCBE1DB75BF8E::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6414 = { sizeof (ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8), -1, sizeof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6414[60] = 
{
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Padding_16(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GradientScale_17(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleX_18(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleY_19(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_PerspectiveFilter_20(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_TextureWidth_21(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_TextureHeight_22(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_BevelAmount_23(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowColor_24(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowOffset_25(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowPower_26(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowOuter_27(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_LightAngle_28(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMap_29(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMatrix_30(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMatrixRotation_31(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskCoord_32(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ClipRect_33(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskSoftnessX_34(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskSoftnessY_35(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_VertexOffsetX_36(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_VertexOffsetY_37(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UseClipRect_38(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilID_39(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilOp_40(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilComp_41(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilReadMask_42(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilWriteMask_43(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ShaderFlags_44(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_A_45(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_B_46(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_C_47(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Bevel_48(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Glow_49(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Underlay_50(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Ratios_51(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_SOFT_52(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_HARD_53(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_TEX_54(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Outline_55(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ShaderTag_ZTestMode_56(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ShaderTag_CullMode_57(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_m_clamp_58(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_isInitialized_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6415 = { sizeof (SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6415[3] = 
{
	SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6416 = { sizeof (TexturePacker_t2549189919276EB833F83EA937AB992420E1B199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6417 = { sizeof (SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04)+ sizeof (RuntimeObject), sizeof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6417[4] = 
{
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6418 = { sizeof (SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E)+ sizeof (RuntimeObject), sizeof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E ), 0, 0 };
extern const int32_t g_FieldOffsetTable6418[2] = 
{
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6419 = { sizeof (SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728)+ sizeof (RuntimeObject), sizeof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6419[7] = 
{
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6420 = { sizeof (SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6420[1] = 
{
	SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6421 = { sizeof (U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6421[2] = 
{
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6422 = { sizeof (__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6423 = { sizeof (__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6424 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6425 = { sizeof (ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6425[3] = 
{
	ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B::get_offset_of_uvAnimationRate_4(),
	ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B::get_offset_of_textureName_5(),
	ScrollingUVs_Layers_tA721733151A507101E41565EDCF13A2E95CA4C4B::get_offset_of_uvOffset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6426 = { sizeof (DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6426[3] = 
{
	DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19::get_offset_of_U3CcomparedPropertyNameU3Ek__BackingField_0(),
	DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19::get_offset_of_U3CcomparedValueU3Ek__BackingField_1(),
	DrawIfAttribute_tFB34D5F85832EDDEA37958732F33081167B7EA19::get_offset_of_U3CdisablingTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6427 = { sizeof (DisablingType_tD5EF3B874B72B55398354A91A8F5D85D03D2CAF2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6427[3] = 
{
	DisablingType_tD5EF3B874B72B55398354A91A8F5D85D03D2CAF2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6428 = { sizeof (ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6428[6] = 
{
	ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995::get_offset_of_videoSource_4(),
	ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995::get_offset_of_lights_5(),
	ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995::get_offset_of_averageColor_6(),
	ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995::get_offset_of_tex_7(),
	ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995::get_offset_of_videoSide_8(),
	ReactingLights_t6B6F94D4EB341521400A2ED402AADE75A0ACB995::get_offset_of_createTexture_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6429 = { sizeof (VideoSide_t79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6429[6] = 
{
	VideoSide_t79E705C1B69A5ECF085C3A4D68EA3D55CFA39C80::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6430 = { sizeof (UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC), -1, sizeof(UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6430[5] = 
{
	UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields::get_offset_of_toggleSetMethod_0(),
	UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields::get_offset_of_sliderSetMethod_1(),
	UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields::get_offset_of_scrollbarSetMethod_2(),
	UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields::get_offset_of_dropdownValueField_3(),
	UISetExtensions_tC9B8E93ACC6666CF5DD67C9A289680DAC76FDFBC_StaticFields::get_offset_of_dropdownRefreshMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6431 = { sizeof (JsonNetSample_tD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6431[1] = 
{
	JsonNetSample_tD413A2DCFAB0E8D30B22A7D33E834B9F9FD8CE1B::get_offset_of_Output_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6432 = { sizeof (Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6432[4] = 
{
	Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA::get_offset_of_Name_0(),
	Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA::get_offset_of_ExpiryDate_1(),
	Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA::get_offset_of_Price_2(),
	Product_t4C0E89F6AB824AE32A5B23CE9FC4775D52CEBFBA::get_offset_of_Sizes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6433 = { sizeof (CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6433[5] = 
{
	CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967::get_offset_of_U3CIdU3Ek__BackingField_0(),
	CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967::get_offset_of_U3CNameU3Ek__BackingField_1(),
	CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967::get_offset_of_U3CLevelU3Ek__BackingField_2(),
	CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967::get_offset_of_U3CClassU3Ek__BackingField_3(),
	CharacterListItem_t2D65C4C6E16DEDC74FF8D2B02E51C6344DC8A967::get_offset_of_U3CSexU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6434 = { sizeof (Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6434[6] = 
{
	Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
	Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4::get_offset_of_U3CClassificationU3Ek__BackingField_2(),
	Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4::get_offset_of_U3CStudioU3Ek__BackingField_3(),
	Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4::get_offset_of_U3CReleaseDateU3Ek__BackingField_4(),
	Movie_tAAD3A7928035C2C117885E753D47DAF5A01CD9F4::get_offset_of_U3CReleaseCountriesU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6435 = { sizeof (LoadVideoTitleToText_tFA2AA750483C80117F612FD194AF6AEE72DD68C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6435[2] = 
{
	LoadVideoTitleToText_tFA2AA750483C80117F612FD194AF6AEE72DD68C8::get_offset_of_textMesh_4(),
	LoadVideoTitleToText_tFA2AA750483C80117F612FD194AF6AEE72DD68C8::get_offset_of_player_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6436 = { sizeof (PauseIcon_t791F62301726B18B3D32766456DB0563A7C98C2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6436[2] = 
{
	PauseIcon_t791F62301726B18B3D32766456DB0563A7C98C2B::get_offset_of_p_4(),
	PauseIcon_t791F62301726B18B3D32766456DB0563A7C98C2B::get_offset_of_label_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6437 = { sizeof (VideoProgressBar_tCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6437[1] = 
{
	VideoProgressBar_tCFF949AA01DC1BC796406CDA2BB2E0513BBDD18D::get_offset_of_player_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6438 = { sizeof (YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6438[4] = 
{
	YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051::get_offset_of_speedH_4(),
	YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051::get_offset_of_speedV_5(),
	YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051::get_offset_of_yaw_6(),
	YT_RotateCamera_t29D99C8D2F2F0AF832170C0B54DB754655E77051::get_offset_of_pitch_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6439 = { sizeof (YoutubeLogo_t85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6439[1] = 
{
	YoutubeLogo_t85DCBBCC7CD6C5C344665DF708FE0FD0A7543BEE::get_offset_of_youtubeurl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6440 = { sizeof (YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9), -1, sizeof(YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6440[104] = 
{
	0,
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_youtubeUrl_5(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_startFromSecond_6(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_startFromSecondTime_7(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_autoPlayOnStart_8(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_autoPlayOnEnable_9(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_playUsingInternalDevicePlayer_10(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_loadYoutubeUrlsOnly_11(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_is3DLayoutVideo_12(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_layout3d_13(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoControllerCanvas_14(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_mainCamera_15(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoQuality_16(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_loadingContent_17(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_OnYoutubeUrlAreReady_18(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_OnVideoReadyToStart_19(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_OnVideoStarted_20(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_OnVideoPaused_21(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_OnVideoFinished_22(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_objectsToRenderTheVideoImage_23(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoPlayer_24(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_audioPlayer_25(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_debug_26(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoUrl_27(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_audioUrl_28(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_ForceGetWebServer_29(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_showPlayerControls_30(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_maxRequestTime_31(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_currentRequestTime_32(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_retryTimeUntilToRequestFromServer_33(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_currentRetryTime_34(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_gettingYoutubeURL_35(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoAreReadyToPlay_36(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lastPlayTime_37(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_audioDecryptDone_38(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoDecryptDone_39(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoPrepared_40(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_audioPrepared_41(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_isRetry_42(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lastTryVideoId_43(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lastStartedTime_44(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_youtubeUrlReady_45(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_newRequestResults_46(),
	0,
	0,
	0,
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_fullscreenModeEnabled_50(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lastFrame_51(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoEnded_52(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_noAudioAtacched_53(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoTitle_54(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_startedFromTime_55(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoStarted_56(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lastErrorTime_57(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_pauseCalled_58(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_autoHideControlsTime_59(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_mainControllerUi_60(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_progress_61(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_volumeSlider_62(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_playbackSpeed_63(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_currentTimeString_64(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_totalTimeString_65(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_totalVideoDuration_66(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_currentVideoDuration_67(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoSeekDone_68(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_videoAudioSeekDone_69(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lowRes_70(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_hideScreenTime_71(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_audioDuration_72(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_showingPlaybackSpeed_73(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_showingVolume_74(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_seekUsingLowQuality_75(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lsigForVideo_76(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_lsigForAudio_77(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_thread1_78(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_webGlResults_79(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_startedPlayingWebgl_80(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_logTest_81(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_isSyncing_82(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_showThumbnailBeforeVideoLoad_83(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_thumbnailVideoID_84(),
	0,
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_StaticFields::get_offset_of_SignatureQuery_86(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_encryptedSignatureVideo_87(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_encryptedSignatureAudio_88(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_masterURLForVideo_89(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_masterURLForAudio_90(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_patternNames_91(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_patternIndex_92(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_decryptedUrlForVideo_93(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_decryptedUrlForAudio_94(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_decryptedVideoUrlResult_95(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_decryptedAudioUrlResult_96(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_youtubeVideoInfos_97(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_htmlVersion_98(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9_StaticFields::get_offset_of_sp_99(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_EncryptUrlForVideo_100(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_EncryptUrlForAudio_101(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_downloadYoutubeUrlResponse_102(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_waitAudioSeek_103(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_notSeeking_104(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_ignoreDrop_105(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_dropAlreadyCalled_106(),
	YoutubePlayer_tD78E150C0D855269B1D2D6A1C66FFAB98F7B60C9::get_offset_of_checkIfSync_107(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6441 = { sizeof (YoutubeVideoQuality_t07911A2842B065614CD64C03F5F15B28524D3084)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6441[6] = 
{
	YoutubeVideoQuality_t07911A2842B065614CD64C03F5F15B28524D3084::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6442 = { sizeof (Layout3D_tC9C3DC3E0D332034238175DDD3BFFA1EE0C24010)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6442[3] = 
{
	Layout3D_tC9C3DC3E0D332034238175DDD3BFFA1EE0C24010::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6443 = { sizeof (YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6443[8] = 
{
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_lowQuality_0(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_standardQuality_1(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_mediumQuality_2(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_hdQuality_3(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_fullHdQuality_4(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_ultraHdQuality_5(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_bestFormatWithAudioIncluded_6(),
	YoutubeResultIds_t871F84F29672F0B866DB03E724C9C6F616DDCA72::get_offset_of_audioUrl_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6444 = { sizeof (Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6444[3] = 
{
	Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D::get_offset_of_scriptName_0(),
	Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D::get_offset_of_result_1(),
	Html5PlayerResult_t8002594BF852CDA0BA81A05ABFFA54142DF3650D::get_offset_of_isValid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6445 = { sizeof (DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6445[3] = 
{
	DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7::get_offset_of_data_0(),
	DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7::get_offset_of_isValid_1(),
	DownloadUrlResponse_t20EAB227B69EBCB61EAE43F7186B528FDDA510D7::get_offset_of_httpCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6446 = { sizeof (ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6446[2] = 
{
	ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491::get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_0(),
	ExtractionInfo_tDD54A07FBB92A8C2B87FAF7AC44CAF3284799491::get_offset_of_U3CUriU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6447 = { sizeof (U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6447[5] = 
{
	U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A::get_offset_of_videoId_2(),
	U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A::get_offset_of_U3CU3E4__this_3(),
	U3CDownloadThumbnailU3Ed__51_tEE1E3F9F866C76E07EFB2E6FFC39D66BB9B2E67A::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6448 = { sizeof (U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6448[3] = 
{
	U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00::get_offset_of_U3CU3E1__state_0(),
	U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00::get_offset_of_U3CU3E2__current_1(),
	U3CWaitThingsGetDoneU3Ed__59_tCF51A7B26F7641BD5FE165CD206BE8FF7021BC00::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6449 = { sizeof (U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6449[3] = 
{
	U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108::get_offset_of_U3CU3E1__state_0(),
	U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108::get_offset_of_U3CU3E2__current_1(),
	U3CHandHeldPlaybackU3Ed__81_t659482E3A9ACE1F8CB7FF34642EF62495C8FC108::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6450 = { sizeof (U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6450[5] = 
{
	U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C::get_offset_of_U3CU3E1__state_0(),
	U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C::get_offset_of_U3CU3E2__current_1(),
	U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C::get_offset_of_videoID_2(),
	U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C::get_offset_of_U3CU3E4__this_3(),
	U3CWebRequestU3Ed__86_tAE5EFCA98759805C5CD546964C99F34D62F2AB1C::get_offset_of_U3CrequestU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6451 = { sizeof (U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6451[3] = 
{
	U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0::get_offset_of_U3CU3E1__state_0(),
	U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0::get_offset_of_U3CU3E2__current_1(),
	U3CSeekFinishedU3Ed__132_tBB333F2AF9CF9FDF4E626DE5920BD97916B2C0A0::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6452 = { sizeof (U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6452[6] = 
{
	U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353::get_offset_of_U3CU3E1__state_0(),
	U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353::get_offset_of_U3CU3E2__current_1(),
	U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353::get_offset_of_uri_2(),
	U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353::get_offset_of_U3CU3E4__this_3(),
	U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353::get_offset_of_audio_4(),
	U3CDownloaderU3Ed__140_tB855A798EC8F1E3F7675B096A7DB4C5A17C90353::get_offset_of_U3CrequestU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6453 = { sizeof (U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6453[5] = 
{
	U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D::get_offset_of_U3CU3E1__state_0(),
	U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D::get_offset_of_U3CU3E2__current_1(),
	U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D::get_offset_of_videoID_2(),
	U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D::get_offset_of_U3CU3E4__this_3(),
	U3CWebGlRequestU3Ed__142_t84AF1382F8D485BBA4D46531834EFF86A7ACE78D::get_offset_of_U3CrequestU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6454 = { sizeof (U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6454[3] = 
{
	U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB::get_offset_of_U3CU3E1__state_0(),
	U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB::get_offset_of_U3CU3E2__current_1(),
	U3CWebGLPlayU3Ed__145_tA6FE6BF55BF9E29D0358B94EEF2D94B2AB8671BB::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6455 = { sizeof (U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6455[6] = 
{
	U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC::get_offset_of_U3CU3E1__state_0(),
	U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC::get_offset_of_U3CU3E2__current_1(),
	U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC::get_offset_of_host_2(),
	U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC::get_offset_of_id_3(),
	U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC::get_offset_of_callback_4(),
	U3CWebGlRequestU3Ed__175_t4B885684C3E719E14DF26D2467119104AF1B20FC::get_offset_of_U3CrequestU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6456 = { sizeof (U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6456[7] = 
{
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_U3CU3E1__state_0(),
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_U3CU3E2__current_1(),
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_json_3(),
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_U3CU3E3__json_4(),
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_U3CU3E7__wrap1_5(),
	U3CExtractDownloadUrlsU3Ed__182_tAE74DB3B9B67506155E2C1026736D23DE92C72E0::get_offset_of_U3CU3E7__wrap2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6457 = { sizeof (U3CU3Ec__DisplayClass187_0_t7EACB7EA4F8792767286527B2621FDE4A93CE611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6457[1] = 
{
	U3CU3Ec__DisplayClass187_0_t7EACB7EA4F8792767286527B2621FDE4A93CE611::get_offset_of_formatCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6458 = { sizeof (U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6458[4] = 
{
	U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283::get_offset_of_url_2(),
	U3CDownloadUrlU3Ed__194_tCD0A2AD909B9A64499B2BABCDAEBA43690E61283::get_offset_of_U3CrequestU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6459 = { sizeof (U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6459[7] = 
{
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_url_3(),
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_callback_4(),
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_player_5(),
	U3CDownloadYoutubeUrlU3Ed__195_t32DC3A2FF0DE128C58CB924D4E8F49B29E4ADBDB::get_offset_of_U3CrequestU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6460 = { sizeof (U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6460[3] = 
{
	U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846::get_offset_of_U3CU3E1__state_0(),
	U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846::get_offset_of_U3CU3E2__current_1(),
	U3CVideoSeekCallU3Ed__201_t0143670D3E26C95F15A35F1B2F1F4F12A111F846::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6461 = { sizeof (U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6461[3] = 
{
	U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114::get_offset_of_U3CU3E1__state_0(),
	U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114::get_offset_of_U3CU3E2__current_1(),
	U3CWaitSyncU3Ed__206_t3DC05EB559174E5F0D350FDCB46E71689E072114::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6462 = { sizeof (U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6462[3] = 
{
	U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306::get_offset_of_U3CU3E1__state_0(),
	U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306::get_offset_of_U3CU3E2__current_1(),
	U3CPlayNowU3Ed__207_t99425F2CEE479AB8F56537277461D6A0447B2306::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6463 = { sizeof (U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6463[3] = 
{
	U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2::get_offset_of_U3CU3E1__state_0(),
	U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2::get_offset_of_U3CU3E2__current_1(),
	U3CReleaseDropU3Ed__209_tEEA5B023C0032076F3E5478ACB01326D952D16D2::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6464 = { sizeof (U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6464[4] = 
{
	U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3::get_offset_of_U3CU3E1__state_0(),
	U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3::get_offset_of_U3CU3E2__current_1(),
	U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3::get_offset_of_time_2(),
	U3CPlayNowFromTimeU3Ed__210_t06C47CF211837535D1070ABB68A485CE45010DF3::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6465 = { sizeof (Extensions_t4F879846ED647EF193E870DE09265831DF591835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6466 = { sizeof (YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6466[2] = 
{
	YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781::get_offset_of__livestreamUrl_4(),
	YoutubePlayerLivestream_t01CC6BD2B4B6081ADFD54AE62A1854935E833781::get_offset_of_downloadYoutubeUrlResponse_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6467 = { sizeof (DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6467[3] = 
{
	DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0::get_offset_of_data_0(),
	DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0::get_offset_of_isValid_1(),
	DownloadUrlResponse_t0001923CAE83A206E558485738FDC2CA4EABE3C0::get_offset_of_httpCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6468 = { sizeof (U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6468[6] = 
{
	U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421::get_offset_of_url_3(),
	U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421::get_offset_of_callback_4(),
	U3CDownloadYoutubeUrlU3Ed__5_t6ED7DD5817602D7073D03BD1DD00BBBE7FDED421::get_offset_of_U3CrequestU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6469 = { sizeof (ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6469[8] = 
{
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_youtubeapi_4(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_searchField_5(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_mainFilters_6(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_channelListUI_7(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_videoListUI_8(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_videoUIResult_9(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_channelUIResult_10(),
	ChannelSearchDemo_t097478CD98051F166C5A2053C1B7A0158B5A64A1::get_offset_of_mainUI_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6470 = { sizeof (CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6470[3] = 
{
	CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F::get_offset_of_youtubeapi_4(),
	CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F::get_offset_of_videoIdInput_5(),
	CommentsDemo_tB49AFB20698B4E468598865F0028D96D4D53286F::get_offset_of_commentsTextArea_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6471 = { sizeof (IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6471[11] = 
{
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_youtubeapi_4(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_videoIdInput_5(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_title_6(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_description_7(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_duration_8(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_likes_9(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_dislikes_10(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_favorites_11(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_comments_12(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_views_13(),
	IndividualVideoDataDemo_t01047817DF72C0BD49492079C4A1B1F961A6D4F1::get_offset_of_UI_thumbnail_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6472 = { sizeof (U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6472[5] = 
{
	U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A::get_offset_of_url_2(),
	U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A::get_offset_of_U3CU3E4__this_3(),
	U3CDownloadThumbU3Ed__15_t24CE11043438A7D4D4DDC54ACF6098D68FD0006A::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6473 = { sizeof (PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6473[5] = 
{
	PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F::get_offset_of_youtubeapi_4(),
	PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F::get_offset_of_searchField_5(),
	PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F::get_offset_of_videoListUI_6(),
	PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F::get_offset_of_videoUIResult_7(),
	PlaylistDemo_tB97600CD139C5A0CC5A6B3AD0512E27B65F86F7F::get_offset_of_mainUI_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6474 = { sizeof (VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6474[10] = 
{
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_youtubeapi_4(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_searchField_5(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_categoryField_6(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_categoryFilter_7(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_mainFilters_8(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_videoListUI_9(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_videoUIResult_10(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_mainUI_11(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_customFilter_12(),
	VideoSearchDemo_tFF78D62C194041157F1E22E124D2AC1D9D0406F6::get_offset_of_regionCode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6475 = { sizeof (YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6475[5] = 
{
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295::get_offset_of_APIKey_4(),
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295::get_offset_of_searchKeyword_5(),
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295::get_offset_of_searchResults_6(),
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295::get_offset_of_currentResults_7(),
	YoutubeApiGetUnlimitedVideos_tDCD3F252F6094ACEC1812B242D3D1D63FD6AE295::get_offset_of_maxResult_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6476 = { sizeof (U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6476[5] = 
{
	U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824::get_offset_of_keyword_2(),
	U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824::get_offset_of_U3CU3E4__this_3(),
	U3CYoutubeSearchU3Ed__6_tB4446374E0BA2B2B2CD411FC1BED56E000D3C824::get_offset_of_U3CcallU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6477 = { sizeof (U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6477[5] = 
{
	U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2::get_offset_of_U3CU3E4__this_2(),
	U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2::get_offset_of_pageToken_3(),
	U3CYoutubeGetNextPageU3Ed__7_t7A33CC2A4851DA348511522C14F6D3F34C0674E2::get_offset_of_U3CcallU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6478 = { sizeof (YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6478[4] = 
{
	YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15::get_offset_of_videoName_4(),
	YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15::get_offset_of_videoId_5(),
	YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15::get_offset_of_thumbUrl_6(),
	YoutubeChannelUI_t9C4465D3BCE9B58F4E22406034474E4112C32D15::get_offset_of_videoThumb_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6479 = { sizeof (U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6479[4] = 
{
	U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadThumbU3Ed__6_t33182CE7027674691EEA22DC0B5D6902B7D1C1DB::get_offset_of_U3CwwwU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6480 = { sizeof (YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6480[5] = 
{
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802::get_offset_of_videoName_4(),
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802::get_offset_of_videoId_5(),
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802::get_offset_of_thumbUrl_6(),
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802::get_offset_of_videoThumb_7(),
	YoutubeVideoUi_tB74D00EFC66A561BADB0121E1D454186B559E802::get_offset_of_mainUI_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6481 = { sizeof (U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6481[4] = 
{
	U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A::get_offset_of_U3CU3E1__state_0(),
	U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A::get_offset_of_U3CU3E2__current_1(),
	U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A::get_offset_of_url_2(),
	U3CPlayVideoU3Ed__7_t0314F38A3A765DBD38851DD189D6E08BA747F34A::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6482 = { sizeof (U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6482[4] = 
{
	U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38::get_offset_of_U3CU3E1__state_0(),
	U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38::get_offset_of_U3CU3E2__current_1(),
	U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38::get_offset_of_U3CU3E4__this_2(),
	U3CDownloadThumbU3Ed__9_tDB6343F840E57517A65748A9BA7C0095F3EB4C38::get_offset_of_U3CwwwU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6483 = { sizeof (YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6483[6] = 
{
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA::get_offset_of_data_4(),
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA::get_offset_of_searchResults_5(),
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA::get_offset_of_comments_6(),
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA::get_offset_of_playslistItems_7(),
	YoutubeAPIManager_t923AAEDD3919D7A60CA5CB3ECDC100897B9026AA::get_offset_of_channels_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6484 = { sizeof (YoutubeSearchOrderFilter_t93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6484[8] = 
{
	YoutubeSearchOrderFilter_t93CDFDBFCE1ACD3B8E2F5AF6D5A1B91244C598F5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6485 = { sizeof (YoutubeSafeSearchFilter_t11F93AA4D2DC960FEDA19E3CA6B78513966DB612)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6485[4] = 
{
	YoutubeSafeSearchFilter_t11F93AA4D2DC960FEDA19E3CA6B78513966DB612::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6486 = { sizeof (U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6486[7] = 
{
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_U3CU3E1__state_0(),
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_U3CU3E2__current_1(),
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_channelId_2(),
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_maxResults_3(),
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_U3CU3E4__this_4(),
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_callback_5(),
	U3CGetVideosFromChannelU3Ed__16_tED90C0A165D0B07025B4D47BFE4782A4CD65B05A::get_offset_of_U3CcallU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6487 = { sizeof (U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6487[7] = 
{
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_playlistId_2(),
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_maxResults_3(),
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_U3CU3E4__this_4(),
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_callback_5(),
	U3CYoutubeCallPlaylistU3Ed__17_t4325628F49A1C23F82A17756BE6BEF97BFCA0E94::get_offset_of_U3CcallU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6488 = { sizeof (U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6488[6] = 
{
	U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9::get_offset_of_videoId_2(),
	U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9::get_offset_of_U3CU3E4__this_3(),
	U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9::get_offset_of_callback_4(),
	U3CYoutubeCallCommentsU3Ed__18_tF86C3C8E700FB15BD9F5604C5C4894A4D30E17F9::get_offset_of_U3CcallU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6489 = { sizeof (U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6489[10] = 
{
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_keyword_2(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_category_3(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_order_4(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_safeSearch_5(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_maxresult_6(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_U3CU3E4__this_7(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_callback_8(),
	U3CYoutubeSearchUsingCategoryU3Ed__19_t6733188493E29DDFD48B9DD12742840C6FE16824::get_offset_of_U3CcallU3E5__2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6490 = { sizeof (U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6490[9] = 
{
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_keyword_2(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_order_3(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_safeSearch_4(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_maxresult_5(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_U3CU3E4__this_6(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_callback_7(),
	U3CYoutubeSearchChannelU3Ed__20_t6DD609AF81381188637C022C719A691A88A14A01::get_offset_of_U3CcallU3E5__2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6491 = { sizeof (U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6491[7] = 
{
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_U3CU3E1__state_0(),
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_U3CU3E2__current_1(),
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_regionCode_2(),
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_maxresult_3(),
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_U3CU3E4__this_4(),
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_callback_5(),
	U3CGetTrendingVideosU3Ed__21_t4C50004B3466E510CD2D47ED9F5BA013F9F5FDDF::get_offset_of_U3CcallU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6492 = { sizeof (U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6492[10] = 
{
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_keyword_2(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_order_3(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_safeSearch_4(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_maxresult_5(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_customFilters_6(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_U3CU3E4__this_7(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_callback_8(),
	U3CYoutubeSearchU3Ed__22_t893DAAAA3B860CFED41948EF46136B5EBBB40155::get_offset_of_U3CcallU3E5__2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6493 = { sizeof (U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6493[13] = 
{
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_U3CU3E1__state_0(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_U3CU3E2__current_1(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_keyword_2(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_order_3(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_safeSearch_4(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_locationRadius_5(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_latitude_6(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_longitude_7(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_maxResult_8(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_customFilters_9(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_U3CU3E4__this_10(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_callback_11(),
	U3CYoutubeSearchByLocationU3Ed__23_t06BDD0BD397E8549FFEBB4B65971D1CA9D7D4378::get_offset_of_U3CcallU3E5__2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6494 = { sizeof (U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6494[6] = 
{
	U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA::get_offset_of_U3CU3E1__state_0(),
	U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA::get_offset_of_U3CU3E2__current_1(),
	U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA::get_offset_of_videoId_2(),
	U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA::get_offset_of_U3CU3E4__this_3(),
	U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA::get_offset_of_callback_4(),
	U3CLoadSingleVideoU3Ed__24_t4969A6D725FD3074A3352ABABCB983F8E4ED80DA::get_offset_of_U3CcallU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6495 = { sizeof (YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6495[4] = 
{
	YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524::get_offset_of_snippet_0(),
	YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524::get_offset_of_statistics_1(),
	YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524::get_offset_of_contentDetails_2(),
	YoutubeData_t41A1FB14F9A09E08F9EA36BB9E8557CE547E1524::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6496 = { sizeof (YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6496[12] = 
{
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_authorDisplayName_0(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_authorProfileImageUrl_1(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_authorChannelUrl_2(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_authorChannelId_3(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_videoId_4(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_textDisplay_5(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_textOriginal_6(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_canRate_7(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_viewerRating_8(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_likeCount_9(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_publishedAt_10(),
	YoutubeComments_tA42BB030AE54B8FA33EBC72E5DF29192FAE33340::get_offset_of_updatedAt_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6497 = { sizeof (YoutubePlaylistItems_t1BDD77262A552453743D02BCC20386029F735935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6497[2] = 
{
	YoutubePlaylistItems_t1BDD77262A552453743D02BCC20386029F735935::get_offset_of_videoId_0(),
	YoutubePlaylistItems_t1BDD77262A552453743D02BCC20386029F735935::get_offset_of_snippet_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6498 = { sizeof (YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6498[4] = 
{
	YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935::get_offset_of_id_0(),
	YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935::get_offset_of_title_1(),
	YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935::get_offset_of_description_2(),
	YoutubeChannel_t8E1C162D1D96B813100C732916DD0FCDA7A9C935::get_offset_of_thumbnail_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6499 = { sizeof (YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6499[7] = 
{
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_duration_0(),
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_dimension_1(),
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_definition_2(),
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_caption_3(),
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_licensedContent_4(),
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_projection_5(),
	YoutubeContentDetails_t06A3D3B07A786053F00B37186018FBEA013FD48C::get_offset_of_ageRestrict_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
